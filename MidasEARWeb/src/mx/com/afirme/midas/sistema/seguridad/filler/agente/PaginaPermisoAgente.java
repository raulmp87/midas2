package mx.com.afirme.midas.sistema.seguridad.filler.agente;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

public class PaginaPermisoAgente {
	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoAgente(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
		
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE, 15=TE ,16=RC, 17=EM ,18=IM, 19=ET
		
		PaginaPermiso pp;
		
		//AGENTE

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/listar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/listarFiltradoPaginado.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/solicitud/agregar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/solicitud/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos","/MidasWeb/solicitud/listarDocumentos.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentosSolicitud.jsp","/MidasWeb/solicitud/listarDocumentos.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/solicitud/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/borrar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/solicitud/borrar.do")); 
		listaPaginaPermiso.add(pp);


		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/solicitud/mostrarModificar.do")); 
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/modificar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/solicitud/modificar.do"));  
		listaPaginaPermiso.add(pp);


		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/solicitud/mostrarDetalle.do")); 
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/terminar.do")); 
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp","/MidasWeb/solicitud/borrarDocumento.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/solicitud/validarDatosSolicitud.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("acciones.jsp","/MidasWeb/solicitud/mostrarAcciones.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/solicitud/crearOrdenTrabajo.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("resumen.jsp","/MidasWeb/solicitud/mostrarResumen.do"));  
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/poliza/listarPolizaFiltrado.do"));  
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/notificarEmison.do"));  
		listaPaginaPermiso.add(pp);
		//SISTEMA

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/sistema/cargaCombo/listar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/sistema/cargaCombo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/estado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/ciudad.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/colonia.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/codigoPostal.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/municipio.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subRamo.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subGiro.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subGiroRC.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoEquipoContratista.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoMaquinaria.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoEquipoElectronico.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoRecipientePresion.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/monedaTipoPoliza.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/formaPagoMoneda.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cuentaBancoPesos.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cuentaBancoDolares.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/contacto.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/download/descargarArchivo.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/gestionPendientes/listarPendientes.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mail/enviarMail.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/mensaje/verMensaje.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesDanios.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesReaseguro.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesSiniestros.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/remover.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("acerca.jsp","/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/uploadHandler.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/getInfoHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/getIdHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cargaMenu.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/uploadHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getInfoHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getIdHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getFileInformation.do"));  
		listaPaginaPermiso.add(pp); 



		//USUARIO

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("inicio.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("login.jsp","/MidasWeb/sistema/logout.do"));  
		listaPaginaPermiso.add(pp); 


		//CAT�LOGOS

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ajustador/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/aumentovario/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/aumentovario/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/aumentovario/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/aumentovario/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/clasificacionembarcacion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/clasificacionembarcacion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/clasificacionembarcacion/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ajustador/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/contacto/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/contacto/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/contacto/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/contacto/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/contacto/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cuentabanco/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/listarFiltrado.do")); 
		 listaPaginaPermiso.add(pp); 
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/descuento/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/giro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/giro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/giro/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/giro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/giro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girorc/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/girorc/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/girorc/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/girorc/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/girotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girotransporte/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/girotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/girotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/girotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/materialcombustible/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/materialcombustible/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/materialcombustible/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/materialcombustible/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/mediotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/mediotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/mediotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/descuento/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/descuento/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/descuento/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/paistipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregarborrar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/agregarborrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesAsociados.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesPorAsociar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ramo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ramo/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ramo/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ramo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/recargovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/recargovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/recargovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/recargovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/recargovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/recargovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/recargovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/recargovario/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/recargovario/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/recargovario/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/recargovario/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgiro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgiro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subgiro/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subgiro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subgiro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgirorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgirorc/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subgirorc/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subgirorc/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subgirorc/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subramo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subramo/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subramo/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subramo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarDetalle.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipobandera/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipobandera/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipobandera/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipoembarcacion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipoembarcacion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipoembarcacion/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoempaque/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/agregar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoempaque/agregar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/modificar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoempaque/modificar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoempaque/borrar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomuro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomuro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomuro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/borrar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomuro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomuro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipomuro/mostrarBorrar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomuro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipomuro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tiponavegacion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tiponavegacion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tiponavegacion/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoobracivil/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoobracivil/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoobracivil/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoobracivil/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipotecho/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipotecho/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipotecho/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipotecho/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipotecho/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 


		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonacresta/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/zonacresta/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/zonacresta/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listarFiltrado","/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do")); 
		 listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonacresta/viejo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/agregarTr.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/agregarTr.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/agregarTr.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/listarFiltrado.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonahidro/mostrarAgrega.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/zonahidro/mostrarBorrar.do")); 
		 listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/zonahidro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonahidro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		 pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));  
		 listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		 pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/agregar.do")); 
		 listaPaginaPermiso.add(pp); 

		 pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/agregar.do")); 
		 listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonasismo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/zonasismo/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/zonasismo/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonasismo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp);
		
		
		//Permisos para solicitud de endosos
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/endoso/solicitud/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/endoso/solicitud/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/endoso/solicitud/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/endoso/solicitud/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp","/MidasWeb/endoso/solicitud/listarDocumentos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/listarDocumentos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/endoso/solicitud/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/endoso/solicitud/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/endoso/solicitud/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/endoso/solicitud/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/endoso/solicitud/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/endoso/solicitud/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/endoso/solicitud/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/endoso/solicitud/terminar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentos.jsp","/MidasWeb/endoso/solicitud/borrarDocumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/endoso/solicitud/borrarDocumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/endoso/solicitud/validarDatosSolicitud.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/endoso/solicitud/crearOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("resumen.jsp","/MidasWeb/endoso/solicitud/mostrarResumen.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/endoso/mostrarEmbarques.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/endoso/getIncisos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/endoso/getSecciones.do"));
		listaPaginaPermiso.add(pp);		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/endoso/guardarEmbarque.do"));
		listaPaginaPermiso.add(pp);
		
		
		//Cat�logo de colonias
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarColonias.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltrado.do"));
	    listaPaginaPermiso.add(pp);
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/colonia/mostrarModificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/modificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/colonia/mostrarAgregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/validaCodigoPostal.do"));
	    listaPaginaPermiso.add(pp);
	   
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/agregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/colonia/mostrarDetalle.do"));
	    listaPaginaPermiso.add(pp);	    	    
	    
	    //Listar filtrado colonias
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp"," /MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"));
	    listaPaginaPermiso.add(pp);
	    
	    //Buscar/Listar polizas (para solicitud de endoso) 
	    pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("","/MidasWeb/endoso/solicitud/buscarPoliza.do"));
        listaPaginaPermiso.add(pp);
       
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("buscarPoliza.jsp","/MidasWeb/endoso/solicitud/listarPolizaPorEstatus.do"));
        listaPaginaPermiso.add(pp);

        /* Buscar/listar polizas (para solicitudes) */
        pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("buscarPoliza.jsp","/MidasWeb/endoso/solicitud/listarPolizasJson.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("buscarPoliza.jsp","/MidasWeb/endoso/solicitud/mostrarPolizasWindow.do"));  
		listaPaginaPermiso.add(pp);
       
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("buscarPoliza.jsp","/MidasWeb/endoso/solicitud/listarPolizaFiltrado.do"));
        listaPaginaPermiso.add(pp);
        
        //Agregar solicitud a partir de poliza existente
        pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/solicitud/agregarApartirPoliza.do"));  
		listaPaginaPermiso.add(pp);
		
		//Validar si una p�liza puede ser cancelada
		pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("","/MidasWeb/endoso/solicitud/validaparacancelar.do"));
        listaPaginaPermiso.add(pp);
		
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
	    
		/*FIN PERMISOS TEMPORALES PARA EL ROL DE AGENTE*/
		/*****************************************************/
		/*****************************************************/
		
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("solicitudesPolizaGrid.jsp","/MidasWeb/suscripcion/solicitud/buscarSolicitud.action"));
	    listaPaginaPermiso.add(pp);
	    
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("respuestaAsociacion.jsp","/MidasWeb/suscripcion/solicitud/agregarSolicitud.action"));
	    listaPaginaPermiso.add(pp);
	    
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("solicitudesPolizaGrid.jsp","/MidasWeb/suscripcion/solicitud/busquedaRapida.action"));
	    listaPaginaPermiso.add(pp);	    
	    
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("ventanaAsignar.jsp","/MidasWeb/suscripcion/solicitud/asignar/mostrarVentana.action"));
	    listaPaginaPermiso.add(pp);	    

        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina(" ","/MidasWeb/suscripcion/solicitud/asignar/asignar.action"));
	    listaPaginaPermiso.add(pp);	    

        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("ventanaRechazar.jsp","/MidasWeb/suscripcion/solicitud/rechazar/mostrarVentana.action"));
	    listaPaginaPermiso.add(pp);	 

        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina(" ","/MidasWeb/suscripcion/solicitud/rechazar/rechazar.action"));
	    listaPaginaPermiso.add(pp);		 

        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("comentarios.jsp","/MidasWeb/suscripcion/solicitud/comentarios/mostrar.action"));
	    listaPaginaPermiso.add(pp);		    
	    
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("comentariosGrid.jsp","/MidasWeb/suscripcion/solicitud/comentarios/obtenerComentariosDisponibles.action"));
	    listaPaginaPermiso.add(pp);		    

        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("comentarios.jsp","/MidasWeb/suscripcion/solicitud/comentarios/guardarComentarios.action"));
	    listaPaginaPermiso.add(pp);		    

        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("comentarios.jsp","/MidasWeb/suscripcion/solicitud/comentarios/eliminarComentarios.action"));
	    listaPaginaPermiso.add(pp);	    
	    
		return this.listaPaginaPermiso;
		
	}
}