/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas.sistema.seguridad.filler.reportesreaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuReportesReaseguro {

	private List<Menu> listaMenu = null;
		
	public MenuReportesReaseguro() {
		listaMenu = new ArrayList<Menu>();
	}
	
	public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("2"),"m3","Reaseguro", "Menu ppal Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("36"),"m3_6","Reaseguro", "Estados de Cuenta", "/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do|contenido|dhx_init_tabbars()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m5","Reaseguro", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m5_3","Reaseguro", "Reportes Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("39"),"m5_3_1","Reaseguro", "Administrativos", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("40"),"m5_3_1_1","Reaseguro", "Perfil Cartera", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("41"),"m5_3_1_2","Reaseguro", "Trimestral de Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("44"),"m5_3_2","Reaseguro", "Movimientos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("45"),"m5_3_2_1","Reaseguro", "Reporte de Movimientos por Contrato", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("46"),"m5_3_2_2","Reaseguro", "Reporte de Movimientos de Primas por Reasegurador (REAS)", "/MidasWeb/reaseguro/reportes/buscarPoliza.do|contenido|cargarComponentesRptMovtosPorReasegurador()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("47"),"m5_3_2_3","Reaseguro", "Reporte de Soporte de Contratos de una P�liza", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("48"),"m5_3_2_4","Reaseguro", "Reporte de Distribuci�n", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("49"),"m5_3_2_5","Reaseguro", "Reporte de Emisi�n", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("50"),"m5_3_3","Reaseguro", "Siniestros", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("51"),"m5_3_3_1","Reaseguro", "Reporte de Siniestros por Reasegurador (Borderaux)", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("52"),"m5_3_3_2","Reaseguro", "Reporte de Siniestros con Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("53"),"m5_3_3_3","Reaseguro", "Reporte de Siniestros de Reservas Pendientes", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("54"),"m5_3_3_4","Reaseguro", "Reportes de Siniestros de Reservas Pendientes Acumulado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("55"),"m5_3_4","Reaseguro", "Contratos No Proporcionales", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("56"),"m5_3_4_1","Reaseguro", "Reporte de Siniestros por Evento Catastr�fico", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("57"),"m5_3_4_2","Reaseguro", "Reporte de Siniestros Tent Plan", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("58"),"m5_3_4_3","Reaseguro", "Reporte de Siniestros Working Cover", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("59"),"m5_3_5","Reaseguro", "Ingresos y Egresos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("60"),"m5_3_5_1","Reaseguro", "Reporte de Ingresos de Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("61"),"m5_3_5_2","Reaseguro", "Reporte Estad�stica Fiscal Facultativos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("62"),"m5_3_5_3","Reaseguro", "Reporte Estad�stica Fiscal Autom�ticos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("66"),"m5_3_2_6","Reaseguro", "Reporte de C�mulos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("67"),"m5_3_3_4_1","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("68"),"m5_3_3_4_2","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado Detallado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("69"),"m5_3_3_5","Reaseguro", "Reportes de Siniestros de Reservas de Riesgo", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("70"),"m5_3_2_7","Reaseguro", "Reportes de Saldos por Reasegurador", "", true);
		listaMenu.add(menu);

		menu = new Menu(new Integer("71"),"m5_3_2_8","Reaseguro", "Reporte de Saldos por Poliza", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("72"),"m5_3_2_9","Reaseguro", "Reporte de Saldos por Siniestro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("73"),"m5_3_2_10","Reaseguro", "Saldos por Trimestre", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("74"),"m5_3_2_11","Reaseguro", "Negocios facultativos por trimestre", "", true);
		listaMenu.add(menu);
		
		return this.listaMenu;
		
	}
	
}
