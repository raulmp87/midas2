package mx.com.afirme.midas.sistema.seguridad;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas2.domain.sistema.seguridad.SeguridadContext;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

/**
 * Se encarga de crear el SeguridadContext al inicio del request para que
 * se encuentre disponible.
 * 
 * @author amosomar
 */
@Component
public class SeguridadContextFilter implements Filter {

	private SistemaContext sistemaContext;
	private CalculoService calculoService;
	
	@Autowired
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
    public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {		
		//Crear el contexto
		SeguridadContext context = SeguridadContext.getContext();
		
		
		//Inicializar el contexto
		inicializarContexto(request, response, context);
		
		try {
			chain.doFilter(request, response);
		} finally {
			//Borrar el contexto.
			SeguridadContext.borrarContext();
			TimeUtils.setReference(null);
			calculoService.setIdSubRamoC(null);
		}
		
	}

	private void inicializarContexto(ServletRequest request,
			ServletResponse response, SeguridadContext context) {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession(false);

		Usuario usuario = null;
		
		if (session != null) {
			//Obtiene el usuario que pudiera estar en sesion.
			usuario = (Usuario)session.getAttribute(sistemaContext.getUsuarioAccesoMidas());
		} else {
			usuario = (Usuario)request.getAttribute(sistemaContext.getUsuarioAccesoMidas());
		}

		context.setUsuario(usuario);		
	}


	@Override
	public void destroy() {
		
	}

}
