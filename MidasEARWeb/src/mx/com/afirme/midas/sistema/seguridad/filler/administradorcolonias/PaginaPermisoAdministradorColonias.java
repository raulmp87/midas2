/**
 * Clase que llena Paginas y Permisos para el rol de Administador de Colonias
 */
package mx.com.afirme.midas.sistema.seguridad.filler.administradorcolonias;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoAdministradorColonias {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoAdministradorColonias(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
		
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
		
		PaginaPermiso pp;
		
		//////////////////////// Administrador de Colonias ////////////////////////////////////////////////
		
		//Cat�logo de colonias
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarColonias.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltrado.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"));
	    listaPaginaPermiso.add(pp); 
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/colonia/mostrarModificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/modificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/colonia/mostrarAgregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/validaCodigoPostal.do"));
	    listaPaginaPermiso.add(pp);
	   
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/agregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/colonia/mostrarDetalle.do"));
	    listaPaginaPermiso.add(pp);
	    
		return this.listaPaginaPermiso;
	}	
}