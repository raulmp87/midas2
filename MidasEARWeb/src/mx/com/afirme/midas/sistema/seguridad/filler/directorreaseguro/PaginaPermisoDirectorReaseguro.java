package mx.com.afirme.midas.sistema.seguridad.filler.directorreaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;
import mx.com.afirme.midas.sistema.seguridad.filler.PaginaPermisoBase;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoDirectorReaseguro extends PaginaPermisoBase{

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoDirectorReaseguro(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));  //Menu MIDAS
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subRamo.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/estado.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/ciudad.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		/* Cartera */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reportePerfilCartera.jsp", "/MidasWeb/contratos/cartera/reportePerfilCartera.do"));  
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/cartera/rPC.do"));  
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/cartera/agregarRangoSumaAsegurada.do"));  
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/cartera/borrarRangoSumaAsegurada.do"));  
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		/* Cartera */
		
		/* Configuracion Linea Negociacion */
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/linea/listarLineaNegociacion.do"));  
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/mostrarAgregarInicial.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/linea/mostrarAsignarCP.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/contratocuotaparte/seleccionar.do"));
		pp.getPermisos().add(listaPermiso.get(14)); //SE 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/contratocuotaparte/mostrarInicio.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/contratocuotaparte/crearContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/participacion/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/contratocuotaparte/guardarContratoFinal.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/participacion/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoDolares.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoPesos.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contacto.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/linea/cargarParticipacionesCP.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoCuotaParte.jsp", "/MidasWeb/contratos/contratocuotaparte/mostrarRegistrarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarModificar.jsp", "/MidasWeb/contratos/participacion/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacion/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/contratos/participacion/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacion/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/cargarReaseguradores.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //C=
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AC 
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/participacioncorredor/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/contratocuotaparte/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/linea/mostrarAsignarPE.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/seleccionar.do"));
		pp.getPermisos().add(listaPermiso.get(14)); //SE
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoPrimerExcedente.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarInicio.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC		
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarContratoPrimerExcedente.jsp", "/MidasWeb/contratos/contratoprimerexcedente/crearContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/contratoprimerexcedente/listarFiltradoEnGrid.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratos/linea/cargarParticipacionesPE.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/guardarContratoFinal.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarRegistrarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/contratos/linea/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaNegociacion.jsp", "/MidasWeb/contratos/linea/listarLineaNegociacion.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaContrato.jsp", "/MidasWeb/contratos/linea/listarLineaContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaVigencia.jsp", "/MidasWeb/contratos/linea/listarLineaVigencia.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("autorizarLineas.jsp", "/MidasWeb/contratos/linea/autorizarLineas.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/contratos/linea/borrarLineaById.do"));
		pp.getPermisos().add(listaPermiso.get(2)); //BR 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaNegociacion.jsp", "/MidasWeb/contratos/linea/listarLineaNegociacionFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO 
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desautorizarLinea.jsp", "/MidasWeb/contratos/linea/desautorizarLinea.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("Renovar", "/MidasWeb/contratos/linea/renovarLinea.do"));
		pp.getPermisos().add(listaPermiso.get(0)); //AG 
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarExtenderLinea.jsp", "/MidasWeb/contratos/linea/mostrarExtenderVigenciaLinea.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		pp.getPermisos().add(listaPermiso.get(8)); //BU 
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarLineaVigencia.jsp", "/MidasWeb/contratos/linea/listarLineaVigenciaFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		pp.getPermisos().add(listaPermiso.get(8)); //BU
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarLinea.jsp", "/MidasWeb/contratos/linea/listarLineaContratoFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(9)); //CO
		pp.getPermisos().add(listaPermiso.get(8)); //BU 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarExtenderLinea.jsp", "/MidasWeb/contratos/linea/extenderVigenciaLinea.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarContratosPorLinea", "/MidasWeb/contratos/linea/mostrarContratosPorLinea.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarLinea.jsp", "/MidasWeb/contratos/linea/mostrarLinea.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/contratos/contratocuotaparte/mostrarDetalleContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarDetalleContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/contratos/contratoprimerexcedente/mostrarDetalleContrato.do"));
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/contratos/linea/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/autorizarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/cancelarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/regresarRegistrarContratoCuotaParte.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/contratocuotaparte/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratoprimerexcedente/autorizarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratoprimerexcedente/guardarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/guardarContrato.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/contratocuotaparte/mostrarListar.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/contratoprimerexcedente/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/contratoprimerexcedente/mostrarListar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarRegistrarIngresos.jsp","/MidasWeb/contratos/ingresos/mostrarRegistrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/registrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/listarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/agregarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/borrarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/ingresos/modificarIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/linea/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/contratos/linea/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarExtenderLinea.jsp","/MidasWeb/contratos/linea/mostrarExtenderVigenciaLinea.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/contratos/linea/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/guardaSesion.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/desasociarContratoPELinea.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/desasociarContratoCPLinea.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/rptLineasContratos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/guardarParticipacionesCP.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/guardarParticipacionesPE.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/linea/cargarParticipacionesCPEnCrearContrato.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/reporteMovimiento.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		/* Fin Configuracion Linea Negociacion */
		
		/* Catálogos Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listar.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/catalogos/contacto/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);


		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/contacto/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/contacto/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/contacto/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/contacto/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/cuentabanco/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp", "/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		/* Fin Catálogos Reaseguro */
		
		/* Reaseguro */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/administrarEgresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarRegistrarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarEstadosCuentaAdministrarEgresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarEgresosPendientes.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/egresoCancel.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/validarRelacionEgresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/borraRelacionEgresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/agregarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarEstadosCuentaRelacionarEgreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/confirmarPago.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerOrdenesPago.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/rptOrdenPagoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/obtenerReporteSoporteOrdenPago.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/validarBorraRelacionEgresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
				
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPrincipalEstadosCuenta.jsp","/MidasWeb/reaseguro/estadoscuenta/mostrarPrincipalEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(BU));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadosCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaTipoReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarEstadosCuentaTipoReaseguroGrid.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/llenaComboSuscripcion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("principalEdoCtaFacultativo.jsp","/MidasWeb/reaseguro/estadoscuenta/listarEstadoCuentaContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaTipoReaseguro.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("detalleEstadoCuentaContratoFacultativo.jsp","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePagos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWebreaseguro/estadoscuenta/mostrarDetalleAcumulador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaTipoReaseguro/guardarObservaciones.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/guardarObservaciones.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/estadoCuentaContratoFacultativo/mostrarDetallePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/regenerarAcumuladoresReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/estadoscuenta/deshacerMovimientosPorPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/estadoCuentaFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/rptEstadoCuentaMasivo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarRptSaldoReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSaldoReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reporte/rptEstadoCuentaFacultativoMasivo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("administrarIngresos.jsp","/MidasWeb/reaseguro/ingresos/administrarIngresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/validarRelacionIngresoEdosCta.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarIngreso.jsp","/MidasWeb/reaseguro/ingresos/mostrarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/listarEstadosCuentaRelacionarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/relacionarIngreso.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarPolizas.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/obtenerEstadosCuenta.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarEstadosCuentaAdministrarIngresos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/ingresos/mostrarIngresosPendientes.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracionMovimientos.jsp","/MidasWeb/reaseguro/configuracionmovimientos/listar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/solicitud/modificarNotaCobertura.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);

		/* Fin Reaseguro */

		/* Distribucion Masiva */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/distribucionMasiva.do"));
		listaPaginaPermiso.add(pp);
		
		/* Fin Distribucion Masiva */
		
		
		
		/* Contratos Facultativos */
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/listarDocumentos.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/borrarDocumento.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipGeneral.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipGeneral.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipBarco.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipBarco.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipEquipoContratista.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipEquipoContratista.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipTransportesDeCarga.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipTransportesDeCarga.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/mostrarModificarPorcentajesContratos.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/modificarPorcentajesContratos.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/validarTipoSlip.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlipIncisoGrid.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/listarCotizacionDanos.do"));
		pp.getPermisos().add(listaPermiso.get(BU));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/registrarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/cotizafacultativa/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/cobertura/mostrarDetalle.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/agregarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/eliminarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/cobertura/guardarDatosDeLaCobertura.do"));
		pp.getPermisos().add(listaPermiso.get(GU));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/modificarCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/agregarDetalleCotizacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacionCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participaciondetalle/cargarParticipacion.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativa/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativa/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativa/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/borrar.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/mostrarAgregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/mostrarModificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/agregar.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionFacultativaCorredor/modificar.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/borrarParticipacionCorredor.do"));
		pp.getPermisos().add(listaPermiso.get(BR));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/autorizarCotizacion.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/autorizarContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/cancelarContratoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/validarDetalleParticipacionFacultativa.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/mostrarConfiguracionReaseguradores.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/mostrarGridConfiguracionReaseguradores.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/mostrarGridConfiguracionReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/guardarConfiguracionReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(AC));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/mostrarGridFiltroCotizacionesFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/contrato/guardarIdCotizacionEnSesion.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/configuracion/listar.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/poblarTreeView.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/recalcularPorcentajeMontoFacultativo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		/* Fin Contratos Facultativo */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/registrarMovimiento.do"));
		pp.getPermisos().add(listaPermiso.get(AG));
		listaPaginaPermiso.add(pp);
		
		/* Reportes */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptReservasRiesgo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptReservasRiesgo.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptTrimestralReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/movimientosPorContrato.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/buscarPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/reaseguroPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/reporteDistribucion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/reporteEmision.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptCumulos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptCumulos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/siniestrosReaseguradorBorderaux.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/movimientoSiniestroReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/movimientoSiniestrosReservasPendientes.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSiniestrosEventoCatastrofico.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSiniestrosTentPlan.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSiniestrosWorkingCover.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/ingresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/mostrarFiltroRptEstadisticaFiscal.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/mostrarFiltroRptEstadisticaFiscal.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/cartera/rptPerfilCartera.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptTrimestralReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptMovtosPorContrato.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/rptMovtosPorReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/movimiento/rptSoporteContratoPoliza.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptDistribucion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptEmision.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSiniestrosPorReasegurador.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredorOrdenado.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSiniestrosConReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSinReservaPendiente.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSinReservaPendienteAcum.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSiniestrosEventoCatastrofico.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/cargaComboEventoCatastrofico.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSiniestrosTentPlan.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptSiniestrosWorkingCover.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/rptIngresosReaseguro.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/rptEstadisticaFiscal.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratos/rptEstadisticaFiscal.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		/* Fin de Reportes */
		
		/* Slips */
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipGeneral.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipBarco.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipEquipoContratista.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/mostrarEditarSlipTransportesDeCarga.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/borrarDocumento.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/listarDocumentos.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlip.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipAviacion.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCFuncionarios.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipIncendio.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipObraCivil.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/guardarSlipRCConstructores.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/slip/editarSlipIncisoGrid.do"));
		pp.getPermisos().add(listaPermiso.get(CO));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/participacionfacultativa/copiarParticipacionesCoberturaEndosoAnterior.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSinReservaPendienteAcum.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/reportes/mostrarFiltroRptSinReservaPendienteAcumDet.do"));
	    listaPaginaPermiso.add(pp);
		
		/* Fin Slips */
		
	    //Permisos para catálogo de impuestos por residencia fiscal
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/listar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/agregar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/modificar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/borrar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/borrar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarAgregar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarBorrar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarModificar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarDetalle.do"));
	    listaPaginaPermiso.add(pp);
	    
	    //Permisos para mostrar grid de plan de pagos de coberturas
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/mostrarPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/regenerarPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/obtenerEstatusAutorizacionPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);	    	    
	    	
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/contratofacultativo/pagocobertura/autorizarPlanPagosCobertura.do"));
	    listaPaginaPermiso.add(pp);
	    
	    // Permisos para egresos
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarEstadosCuentaAutomaticosEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarEstadosCuentaFacultativosEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarCoberturasEstadosCuentaEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/listarExhibicionesCoberturasEstadosCuentaEgreso.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/cargarComboSecciones.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/cargarComboCoberturas.do"));
	    listaPaginaPermiso.add(pp);
	    	    	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/mostrarAutorizaciones.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/autorizaciones/mostrarAutorizaciones.do"));
	    listaPaginaPermiso.add(pp);

	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/reaseguro/egresos/autorizaciones/autorizarPagoPendiente.do"));
	    listaPaginaPermiso.add(pp);
		return this.listaPaginaPermiso;
	}
	
}
