package mx.com.afirme.midas.sistema;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBHome;
import javax.ejb.EJBObject;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import org.apache.commons.io.IOUtils;




import  mx.com.afirme.midas.sistema.NamingUtil;

final class EJBLocator {

 
  private static final EJBLocator INSTANCE = new EJBLocator();

  private transient String tempStubDirectory =
      System.getProperty("java.io.tmpdir") + "stub";
  
  public static EJBLocator getInstance() {
    return EJBLocator.INSTANCE;
  }

  private EJBLocator() {
   // //LOG.info("Using temp directory: " + this.tempStubDirectory);
    File stubDir = new File(this.tempStubDirectory);
    this.cleanDirectory(stubDir);
    this.setClassPath(stubDir);
  }

  private void setClassPath(File stubDir) {
    Thread currentThread = Thread.currentThread();
    ClassLoader classLoader = currentThread.getContextClassLoader();
    String classLoaderName = classLoader.getClass().getName();
    if(classLoaderName.equals("CompoundClassLoader")) {
      try{
        stubDir.mkdirs();
        List<String> paths = Arrays.asList(
          (String[]) invoke(classLoader, "getPaths"));
        if(!paths.contains(this.tempStubDirectory)){
          Object[] methodParameters = new Object[] {
            new String[] {this.tempStubDirectory}};
          invoke(classLoader, "addPaths", methodParameters);
        } // End of if
      }catch (Exception exception){
        //LOG.info("Unknown error while adding " + this.tempStubDirectory
            //+ " to classpath.", exception);
      } // End of try/catch
    } // End of try/catch
  }

  private void setClassPath(String stubDir) {
    this.setClassPath(new File(stubDir));
  }

  private void cleanDirectory(File directory) {
    try{
      File[] files = directory.listFiles();
      for(int i = 0; i < files.length; i++){
        File file = files[i];
        if(!file.isDirectory()){
          file.delete();
        }else{
          cleanDirectory(file);
        } // End of if/else
      } // End of for()
      directory.delete();
    }catch (NullPointerException npException){
      //LOG.error("Unable to delete directory: " + directory);
    } // End of try/catch
  }

  public File generateStructure(String name) {
    String separator = "/";
    String pkgDir = this.getPackageDirectory(name);
    String dir = this.tempStubDirectory + separator + pkgDir;
    File file = new File(dir);
    if(!file.exists()){
      //LOG.info("Creating directory: " + file.getAbsolutePath());
      file.mkdirs();
    } // End of if
    return file;
  }

  private String getPackageDirectory(String classname) {
    String pkgDir = null;
    String separator = "/";
    String pkg = this.getPackage(classname);
    pkgDir = pkg.replaceAll("(\\.)", separator);
    return pkgDir;
  }

  private String getPackage(String classname) {
    int index = classname.lastIndexOf(".");
    String pkg = classname.substring(0, index);
    return pkg;
  }

  private String getPackage(Class<?> theClass) {
    String cannonicalName = theClass.getCanonicalName();
    return this.getPackage(cannonicalName);
  }

  private String getStubName(Class<?> theClass) {
    StringBuilder stubName = new StringBuilder();
    String simpleName = theClass.getSimpleName();
    String pkg = this.getPackage(theClass);
    stubName.append(pkg);
    stubName.append("._");
    stubName.append(simpleName);
    stubName.append("_Stub");
    return stubName.toString();
  }

  private String getSimpleName(String classname) {
    int index = classname.lastIndexOf(".") + 1;
    return classname.substring(index);
  }

  private File loadClass(String classname, String path) throws SystemException {
    File file = null;
    InputStream instream = null;
    OutputStream ostream = null;
    try{
      this.setClassPath(this.tempStubDirectory);
      File dir = this.generateStructure(classname);
      file = new File(dir, this.getSimpleName(classname) + ".class");
      if(file.exists()){
        return file;
      } // End of if
      URL url = new URL(path + "?classname=" + classname);
      URLConnection urlConnection = url.openConnection();
      urlConnection.setConnectTimeout(1000 * 2);
      instream = urlConnection.getInputStream();
      if(!file.exists()){
        ostream = new FileOutputStream(file);
        file.deleteOnExit();
        IOUtils.copy(instream, ostream);
      }else{
        instream.close();
      } // End of if/else
    }catch (IOException ioException){
      String message = "Classname \"" + classname + "\" not found in " + path;
      //LOG.error(message);
      throw new SystemException(message, ioException);
    } finally{
      IOUtils.closeQuietly(instream);
      IOUtils.closeQuietly(ostream);
    } // End of try/catch
    return file;
  }

  private void loadStub(Class<?> theClass, String reference)
      throws NamingException, SystemException {
    String stub = this.getStubName(theClass);
    try{
      Class.forName(stub);
    }catch (ClassNotFoundException exception){
      String path = this.getClassServer(reference);
      this.loadClass(stub, path);
    } // End of try/catch
  }

  @SuppressWarnings("unchecked")
  private Class<?> getHomeClass(String reference) throws NamingException {
    Class<?> homeClass = null;
    String classname = null;
    Context context = null;
    try{
      context = new InitialContext();
      Map<String, Map<String, String>> classes =
        (Map<String, Map<String, String>>)context.lookup("stub/classes");
      Map<String, String> data =
        (Map<String, String>)classes.get(reference);
      classname = data.get("home");
      homeClass = Class.forName(classname);
    }catch (NamingException nException){
      //LOG.error("Unable to close context: " + context, nException);
    }catch (ClassNotFoundException cnfException){
      //LOG.error("Unable to find class: " + classname, cnfException);
    }finally{
      NamingUtil.closeQuietly(context);
    } // End of try/catch
    return homeClass;
  }

  @SuppressWarnings("unchecked")
  private Class<?> getRemoteClass(String reference) throws NamingException {
    Class<?> homeClass = null;
    String classname = null;
    Context context = null;
    try{
      context = new InitialContext();
      Map<String, Map<String, String>> classes =
        (Map<String, Map<String, String>>)context.lookup("stub/classes");
      Map<String, String> data =
        (Map<String, String>)classes.get(reference);
      classname = (String)data.get("remote");
      homeClass = Class.forName(classname);
    }catch (NamingException nException){
      //LOG.error("Unexpected naming exception while retrieving remote class " +
      		//"for \"" + reference + "\".", nException);
    }catch (ClassNotFoundException cnfException){
      //LOG.error("Unable to find class: " + classname, cnfException);
    }finally{
      NamingUtil.closeQuietly(context);
    } // End of try/catch
    return homeClass;
  }

  @SuppressWarnings("unchecked")
  private String getClassServer(String reference) throws NamingException {
    String classServer = null;
    Context context = null;
    try{
      context = new InitialContext();
      Map<String, Map<String, String>> classes =
        (Map<String, Map<String, String>>)context.lookup("stub/classes");
      Map<String, String> data =
        (Map<String, String>)classes.get(reference);
      classServer = (String)data.get("classServer");
    }catch (NamingException nException){
      //LOG.error("Unexpected naming exception while retrieving class server " +
          //"for \"" + reference + "\".", nException);
    }finally{
      NamingUtil.closeQuietly(context);
    } // End of try/catch
    return classServer;
  }

  @SuppressWarnings("unchecked")
  private String getIIOP(String reference) throws NamingException {
    String iiop = null;
    Context context = null;
    try{
      context = new InitialContext();
      Map<String, Map<String, String>> classes =
        (Map<String, Map<String, String>>)context.lookup("stub/classes");
      Map<String, String> data =
        (Map<String, String>)classes.get(reference);
      iiop = (String)data.get("iiop");
    }catch (NamingException nException){
      //LOG.error("Unable to close context: " + context, nException);
    }finally{
      NamingUtil.closeQuietly(context);
    } // End of try/catch
    return iiop;
  }

  private Context getContext(String reference) throws NamingException {
    String iiop = this.getIIOP(reference);
    Hashtable<String, String> env = new Hashtable<String, String>();
    env.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY,
        "com.ibm.websphere.naming.WsnInitialContextFactory");
    env.put(javax.naming.Context.PROVIDER_URL, iiop);
    return new InitialContext(env);
  }

  private EJBHome getHome(String reference, Class<?> homeClass)
      throws SystemException {
    Object result = null;
    Context context = null;
    try{
      context = this.getContext(reference);
      result = context.lookup(reference);
    }catch (NamingException nException){
      try{
        result = context.lookup("java:comp/env/" + reference);
      }catch (Exception exception){
        throw new SystemException(exception);
      } // End of try/catch
    }finally{
      NamingUtil.closeQuietly(context);
    } // End of try/catch/finally
    return (EJBHome)PortableRemoteObject.narrow(result, homeClass);
  }

  private EJBObject getRemote(String reference, EJBHome home,
      Class<?> remoteClass) throws SystemException {
    Object remote = null;
    try{
      remote = invoke(home, "create");
    }catch (Exception exception){
      throw new SystemException(exception);
    } // End of try/catch
    return (EJBObject)PortableRemoteObject.narrow(remote, remoteClass);
  }

  public EJBObject locateEJB(String reference) throws SystemException {
    EJBObject ejbObject = null;
    try{
      Class<?> remoteClass = this.getRemoteClass(reference);
      Class<?> homeClass = this.getHomeClass(reference);
      this.loadStub(remoteClass, reference);
      this.loadStub(homeClass, reference);
      EJBHome home = this.getHome(reference, homeClass);
      ejbObject = this.getRemote(reference, home, remoteClass);
    }catch (NamingException exception){
      throw new SystemException(exception);
    }// End of try/catch
    return ejbObject;
  }
  
  public static Object invoke(Object instance, String methodName,
	      Object methodParameter) throws IllegalAccessException,
	      InvocationTargetException {
	    return invoke(instance, methodName, new Object[]{methodParameter});
	  }
  
  public static Object invoke(Object instance, String methodName)
  throws IllegalAccessException, InvocationTargetException {
Object[] methodParameters = null;
return invoke(instance, methodName, methodParameters);
}
}
