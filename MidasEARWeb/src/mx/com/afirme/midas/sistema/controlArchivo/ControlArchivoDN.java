package mx.com.afirme.midas.sistema.controlArchivo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

public class ControlArchivoDN {
	private static final ControlArchivoDN INSTANCIA = new ControlArchivoDN();

	public static ControlArchivoDN getInstancia() {
		return ControlArchivoDN.INSTANCIA;
	}

	public ControlArchivoDTO getPorId(BigDecimal id) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(id);
	}

	public List<ControlArchivoDTO> listarTodos() throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.listarTodos();
	}

	public ControlArchivoDTO agregar(ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.agregar(controlArchivoDTO);
	}
	
	public ControlArchivoDTO actualizar(ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.actualizar(controlArchivoDTO);
	}
	
	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO.getIdToControlArchivo());
	}

	public void borrar(ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		controlArchivoSN.borrar(controlArchivoDTO);
	}
	
	public String obtenerNombreArchivoFisico(ControlArchivoDTO controlArchivoDTO) {
		String fileName = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
		return controlArchivoDTO.getIdToControlArchivo().toString() + extension;
	}
}
