package mx.com.afirme.midas.sistema.tareas.reaseguro;

import org.quartz.Job;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author jose luis arellano
 */
public abstract class MidasTareaBase implements Job {
	public static final String ID_TAREA_INICIA_DISTRIBUCION_POLIZAS = "jobIniciarDistribucionPolizas";
	public static final String ID_TAREA_MONITOREO_PAGOS_PENDIENTES = "jobMonitoreoPagosReaseguro";
	public static final String ID_TAREA_REPORTE_REASEGURO = "jobReporteReas";
	
	public static final String ID_TRIGGER_INICIAR_DISTRIBUCION_POLIZAS = "cronTareaIniciarDistribucionPolizas";
	public static final String ID_TRIGGER_MONITOREO_PAGOS_PENDIENTES = "cronTareaMonitoreoPagosPendientes";
	public static final String ID_TRIGGER_REPORTE_REASEGURO = "cronTriggerReporteReas";

	protected String idTarea;
	
	public abstract void iniciaProgramacionTarea(StdSchedulerFactory factory);

	public String getIdTarea() {
		return idTarea;
	}

	public void setIdTarea(String idTarea) {
		this.idTarea = idTarea;
	}
}
