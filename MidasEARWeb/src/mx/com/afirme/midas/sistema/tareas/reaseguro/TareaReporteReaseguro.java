package mx.com.afirme.midas.sistema.tareas.reaseguro;

import java.text.ParseException;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador.MovimientoPrimaReaseguradorDN;
import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle.MovimientoPrimaReaseguradorDetalleDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaReporteReaseguro extends MidasTareaBase{

	public TareaReporteReaseguro(){
		idTarea = MidasTareaBase.ID_TAREA_REPORTE_REASEGURO;
	}
	
	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		
		try {
			LogDeMidasWeb.log("Inicializando tarea quartz en Midas. id tarea: "+idTarea, Level.INFO, null);
			Scheduler scheduler = factory.getScheduler();
			
            // Define una instancia del job
			JobDetail job = new JobDetail(idTarea, Scheduler.DEFAULT_GROUP, TareaReporteReaseguro.class);
			
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger(MidasTareaBase.ID_TRIGGER_REPORTE_REASEGURO, Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_REPORTE_REAS);
				
			//Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);
			LogDeMidasWeb.log("Finaliza inicialización de tarea quartz en Midas. id tarea: "+idTarea, Level.INFO, null);
		} catch (SchedulerException e) {
			LogDeMidasWeb.log("Error al inicializar tarea "+idTarea, Level.SEVERE, e);
		} catch (ParseException e) {
			LogDeMidasWeb.log("Error al inicializar tarea "+idTarea, Level.SEVERE, e);
		} catch(Exception e){
			LogDeMidasWeb.log("Error al inicializar tarea "+idTarea, Level.SEVERE, e);
		}
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Reporte de Reaseguro (reporte REAS). Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
		    
	        MovimientoPrimaReaseguradorDN.getInstancia().generarDocXMLMovtosPorReaseguradorDiaAnterior(Sistema.USUARIO_SISTEMA);
	        MovimientoPrimaReaseguradorDetalleDN.getInstancia().generarDocXMLMovtosPorReaseguradorDetDiaAnterior(Sistema.USUARIO_SISTEMA);
	        
	        LogDeMidasWeb.log("Tarea programada Reporte de Reaseguro (reporte REAS) ejecutada. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	    	
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Reporte de Reaseguro (reporte REAS). Instancia : " 
	    			+ instName + ", Grupo: " + instGroup, Level.WARNING, ex);
	    }
	}

}
