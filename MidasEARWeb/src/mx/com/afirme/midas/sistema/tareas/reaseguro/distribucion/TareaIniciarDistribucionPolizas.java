package mx.com.afirme.midas.sistema.tareas.reaseguro.distribucion;

import java.text.ParseException;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.tareas.reaseguro.MidasTareaBase;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirPolizaDN;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaIniciarDistribucionPolizas extends MidasTareaBase{

	public TareaIniciarDistribucionPolizas(){
		this.idTarea = MidasTareaBase.ID_TAREA_INICIA_DISTRIBUCION_POLIZAS;
	}
	
	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		try {
			Scheduler scheduler = factory.getScheduler();
			
			this.idTarea = MidasTareaBase.ID_TAREA_INICIA_DISTRIBUCION_POLIZAS;
			
			LogDeMidasWeb.log("Inicializando tarea de distribuci�n de p�lizas en reaseguro. id tarea: "+idTarea, Level.INFO, null);
            // Define una instancia del job
			JobDetail job = new JobDetail(idTarea, Scheduler.DEFAULT_GROUP, TareaIniciarDistribucionPolizas.class);
			
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger(MidasTareaBase.ID_TRIGGER_INICIAR_DISTRIBUCION_POLIZAS, Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_INICIO_DISTRIBUCION_POLIZAS);
				
			//Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);
			
			LogDeMidasWeb.log("Finaliza inicializaci�n de tarea de distribuci�n de p�lizas en reaseguro. id tarea: "+idTarea, Level.INFO, null);
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Distribuci�n de p�lizas en Reaseguro. Instancia : " + instName
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	        
	        TemporizadorDistribuirPolizaDN.getInstancia().iniciarDistribucionPolizasPendientes();
	    	
	        LogDeMidasWeb.log("Tarea programada Distribuci�n de p�lizas en Reaseguro ejecutada con �xito. Instancia : " + instName
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Ocurri� una excepcion al ejecutar la tarea programada Distribuci�n de p�lizas en Reaseguro. Instancia : "
	    			+ instName + ", Grupo: " + instGroup, Level.WARNING, ex);
	    }
	}
	
}
