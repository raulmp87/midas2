package mx.com.afirme.midas.sistema.tareas.cotizacion;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.cotizacion.cargamasiva.CargaMasivaDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.excels.GeneraExcelAutoExpedibles;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasiva;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasivaIndividual;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Singleton
@Startup
public class TareaCargaMasiva {
	 
	@Resource
	private TimerService timerService;
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}@EJB
	 private ClientesApiService  clienteRest;
	   
		@Autowired
		@Qualifier("clientesApiServiceEJB")
		public void setClienteRest(ClientesApiService clienteRest) {
			this.clienteRest = clienteRest;
		}
	public static final Logger LOG = Logger.getLogger(TareaCargaMasiva.class);

	public void iniciaProgramacionTarea() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				// 0 0 0 * * ?
				expression.minute(0);
				expression.hour(0);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerCargaMasiva", false));
				
				LOG.info("Timer TimerCargaMasiva configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}

	@Timeout
	public void execute() {
		
		try {			
			LOG.info("Ejecutando tarea programada Carga Masiva.");
			
			//Inicializa Service			
			//Procesa Cargas Pendientes
			System.out.println("Inicia: ");		
			CargaMasivaDN cargaMasivaDN = CargaMasivaDN.getInstancia();
			GeneraExcelCargaMasiva generaExcelCargaMasiva = new GeneraExcelCargaMasiva(cargaMasivaDN.getCargaMasivaService());	
			generaExcelCargaMasiva.setCargaMasivaService(cargaMasivaDN.getCargaMasivaService());
			generaExcelCargaMasiva.procesaCargasPendientes();
			
			//Procesa Cargas Pendientes Individuales
			GeneraExcelCargaMasivaIndividual generaExcelCargaMasivaIndividual = new GeneraExcelCargaMasivaIndividual(cargaMasivaDN.getCargaMasivaService(),clienteRest);
			generaExcelCargaMasivaIndividual.setCargaMasivaService(cargaMasivaDN.getCargaMasivaService());
			generaExcelCargaMasivaIndividual.procesaCargasPendientes();
			
			//Procesa Cargas Pendientes AutoExpedibles
			GeneraExcelAutoExpedibles generaExcelAutoExpedibles = new GeneraExcelAutoExpedibles(cargaMasivaDN.getCargaMasivaService(),clienteRest);
			generaExcelAutoExpedibles.setCargaMasivaService(cargaMasivaDN.getCargaMasivaService());
			generaExcelAutoExpedibles.procesaAutoExpediblesPendientes();
			
			LOG.info("Tarea programada Carga Masiva ejecutada.");

		} catch (Exception ex) {
			LOG.error("Excepcion encontrada mientras se ejecutaba la tarea programada Carga Masiva.", ex);
							
		}

	}
	
	@PostConstruct
	public void initialize() {
		String timerInfo = "TimerCargaMasiva";
		cancelarTemporizador(timerInfo);
		iniciaProgramacionTarea();
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerCargaMasiva");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerCargaMasiva:" + e.getMessage(), e);
		}
	}


}
