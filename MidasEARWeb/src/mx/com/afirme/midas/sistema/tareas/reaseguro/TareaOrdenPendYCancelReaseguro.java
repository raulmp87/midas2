package mx.com.afirme.midas.sistema.tareas.reaseguro;

import java.text.ParseException;
import java.util.logging.Level;


import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.reaseguro.egresos.EgresoReaseguroDN;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaOrdenPendYCancelReaseguro implements Job {

	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		
		try {
			
			Scheduler scheduler = factory.getScheduler();
			
            // Define una instancia del job
			JobDetail job = new JobDetail("jobTareaOrdenPendYCancelReaseguro", Scheduler.DEFAULT_GROUP, TareaOrdenPendYCancelReaseguro.class);
			
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger("cronTareaOrdenPendYCancelReaseguro", Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_ORDENDEPAGOPENDIENTESREASEGURO);
				
			//Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);
            			
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Orden Pend Y Cancel Reaseguro. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
		    try{
		    	EgresoReaseguroDN.getINSTANCIA().procesarOrdenesPendientes();
		    }catch(Exception exx){
		    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Orden Pend Y Cancel Reaseguro. Instancia : " + "procesarOrdenesPendientes"
		    			, Level.WARNING, exx);	
		    }
		    try {
		    	EgresoReaseguroDN.getINSTANCIA().procesarOrdenesCancel();	
		    }catch (Exception exe){
		    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Orden Pend Y Cancel Reaseguro. Instancia : " + "procesarOrdenesCancel"
		    			, Level.WARNING, exe);	
		    	
		    }
	        
	        
	        LogDeMidasWeb.log("Ejecutando tarea programada Orden Pend Y Cancel Reaseguro. ejecutada. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	    	
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Orden Pend Y Cancel Reaseguro. Instancia : " 
	    			+ instName + ", Grupo: " + instGroup, Level.WARNING, ex);
	    }
	}

}
