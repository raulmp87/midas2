package mx.com.afirme.midas.sistema.tareas;

import mx.com.afirme.midas.sistema.tareas.endoso.TareaRehabilitacionEndoso;
import mx.com.afirme.midas.sistema.tareas.reaseguro.TareaOrdenPendYCancelReaseguro;
import mx.com.afirme.midas.sistema.tareas.reaseguro.TareaReporteReaseguro;
import mx.com.afirme.midas.sistema.tareas.reaseguro.distribucion.TareaIniciarDistribucionPolizas;
import mx.com.afirme.midas.sistema.tareas.renovacion.TareaNotificacionRenovacionAutomatica;
import mx.com.afirme.midas.sistema.tareas.siniestros.TareaOrdenDePagoPendientes;
import mx.com.afirme.midas.sistema.tareas.siniestros.TareaReporteSinInformePreeliminar;

import org.quartz.impl.StdSchedulerFactory;



public class ProgramadorTareas {

	
	public ProgramadorTareas() {
	
	}
	
	public void iniciaProgramacionTareas (StdSchedulerFactory factory) {
	
//		TareaEjemplo tareaEjemplo = new TareaEjemplo();
//		tareaEjemplo.iniciaProgramacionTarea(factory);
	
		try{
		TareaNotificacionRenovacionAutomatica renovacionAutomatica = new TareaNotificacionRenovacionAutomatica();
		renovacionAutomatica.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
		
		try{
		TareaReporteReaseguro tareaReporteReaseguro = new TareaReporteReaseguro();
		tareaReporteReaseguro.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
		
		try{
		TareaRehabilitacionEndoso tareaRehabilitacionEndoso = new TareaRehabilitacionEndoso();
		tareaRehabilitacionEndoso.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
		// Tareas para Siniesros
		try{
		TareaReporteSinInformePreeliminar tareaReporteSinInformePreeliminar = new TareaReporteSinInformePreeliminar();
		tareaReporteSinInformePreeliminar.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
		try{
		TareaOrdenDePagoPendientes tareaOrdenDePagoPendientes = new TareaOrdenDePagoPendientes();
		tareaOrdenDePagoPendientes.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
		// Tarea para gestion de las ordenes de pago pendientes y canceladas del modulo de Reaseguro.
		try{
		TareaOrdenPendYCancelReaseguro tareaOrdenPendYCancelReaseguro = new TareaOrdenPendYCancelReaseguro();
		tareaOrdenPendYCancelReaseguro.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
		// Tarea para iniciar distribuci�n de p�lizas del modulo de Reaseguro.
		try{
		TareaIniciarDistribucionPolizas tareaIniciarDistribucionPolizas = new TareaIniciarDistribucionPolizas();
		tareaIniciarDistribucionPolizas.iniciaProgramacionTarea(factory);
		}catch(Exception e){
		}
	}
	
	
}
