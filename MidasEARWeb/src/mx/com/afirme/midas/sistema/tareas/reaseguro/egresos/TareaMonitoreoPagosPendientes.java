package mx.com.afirme.midas.sistema.tareas.reaseguro.egresos;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.reaseguro.egresos.EgresoReaseguroDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;

/**
 * @author jose luis arellano
 */
public class TareaMonitoreoPagosPendientes {
	
	@Resource
	private TimerService timerService;
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	public static final Logger LOG = Logger.getLogger(TareaMonitoreoPagosPendientes.class);
	
	public void iniciaProgramacionTarea () {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				// 0 0 7 ? * MON
				expression.minute(0);
				expression.hour(7);
				expression.dayOfWeek("Mon");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerMonitoreoPagosPendientes", false));
				
				LOG.info("Timer TimerMonitoreoPagosPendientes configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	@Timeout
	public void execute() {
	    try {
	        LOG.info("Ejecutando tarea programada Monitoreo de pagos pendientes en Reaseguro.");
	        
	        EgresoReaseguroDN.getINSTANCIA().envioCorreoMonitoreoPagosPendientes(new Date());
	    	
	        LOG.info("Tarea programada Monitoreo de pagos pendientes en Reaseguro ejecutada con éxito.");
	    } catch (Exception ex) {
	    	LOG.error("Ocurrió una excepcion al ejecutar la tarea programada", ex);
	    }
	}
	
	@PostConstruct
	public void initialize() {
		String timerInfo = "TimerMonitoreoPagosPendientes";
		cancelarTemporizador(timerInfo);
		iniciaProgramacionTarea();
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerMonitoreoPagosPendientes");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerMonitoreoPagosPendientes:" + e.getMessage(), e);
		}
	}
}
