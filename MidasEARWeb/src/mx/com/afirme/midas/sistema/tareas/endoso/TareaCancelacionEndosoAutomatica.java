package mx.com.afirme.midas.sistema.tareas.endoso;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoDN;
import mx.com.afirme.midas.endoso.cancelacion.EndosoCancelableDN;
import mx.com.afirme.midas.endoso.cancelacion.EndosoCancelableDTO;
import mx.com.afirme.midas.endoso.cancelacion.EndosoCancelableId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.poliza.cancelacion.CancelacionPolizaDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.service.bitemporal.endoso.cancelacion.CancelacionBitemporalService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;

@Singleton
@Startup
public class TareaCancelacionEndosoAutomatica {
	
	public static final Logger LOG = Logger.getLogger(TareaCancelacionEndosoAutomatica.class);
	
	@Resource
	private TimerService timerService;
	
	public TareaCancelacionEndosoAutomatica() {
		super();
	}


	public TareaCancelacionEndosoAutomatica(SistemaContext sistemaContext,
			CancelacionBitemporalService cancelacionBitemporalService) {
		super();
		this.sistemaContext = sistemaContext;
	}
	

	public void iniciaProgramacionTarea() {		
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				// 0 0 5 * * ?
				expression.minute(0);
				expression.hour(5);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerCancelacionEndosoAutomatica", false));
				
				LOG.info("Timer TimerCancelacionEndosoAutomatica configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	@Timeout
	public void execute() {

	    try {
	        LOG.info("Ejecutando tarea programada Cancelacion de Endosos Automatica.");
	        
	        cancelaEndososPendientes();
	        
	        LOG.info("Tarea programada Cancelacion de Endosos Automatica ejecutada.");
	    	
	    } catch (Exception ex) {
	    	LOG.error("Excepcion encontrada mientras se ejecutaba la tarea programada Cancelacion de Endosos Automatica.", ex);
	    }
		
	}
	
	public void cancelaEndosoPendienteEjecucionManual () throws Exception{
		try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Cancelacion de Endosos Automatica. Instancia : ", Level.INFO, null);
		    
	
	        cancelaEndososPendientes();
	        
	        LogDeMidasWeb.log("Tarea programada Cancelacion de Endosos Automatica ejecutada. Instancia : ", Level.INFO, null);
	    	
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Cancelacion de Endosos Automatica. Instancia : " 
	    			, Level.WARNING, ex);
	    }
	}
	
	private void cancelaEndososPendientes() throws Exception {
		
		//Parte 1
		//Obtiene de Seycos todos los registros de sus endosos cancelados que no fueron notificados a Midas
		List<EndosoIDTO> endosoReferenciaList = EndosoIDN.getInstancia().obtieneListaCancelables(new Date(), Sistema.USUARIO_SISTEMA);
		
		for (EndosoIDTO endosoReferencia : endosoReferenciaList) {
			if(endosoReferencia.getEsValido().shortValue() == 0){
				//envia un mail de notificacion de error
				StringBuffer sb = new StringBuffer();
				sb.append("TareaCancelacionEndosoAutomatica.cancelaEndososPendientes()");
				sb.append(" | Registro invalido | ");
				sb.append("idToPoliza" + "=" + endosoReferencia.getIdPoliza() + ",");
				sb.append("numeroEndoso" + "=" + endosoReferencia.getNumeroEndosoString()+ ",");
				sb.append("fechaIniVigEnd" + "=" + endosoReferencia.getFechaInicioVigencia() + ",");				
				UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de Cancelacion proceso AUTOMATICO" + " en " + 
						Sistema.AMBIENTE_SISTEMA, sb.toString());				
			}else{
				EndosoCancelableId id = new EndosoCancelableId(endosoReferencia.getIdPoliza(), endosoReferencia.getNumeroEndoso());
				EndosoCancelableDTO endoso = new EndosoCancelableDTO();
				endoso.setId(id);
				
				endoso.setFechaInicioVigencia(endosoReferencia.getFechaInicioVigencia());
				endoso.setEstatusRegistro(EndosoCancelableDTO.ESTATUS_REGISTRO_SIN_PROCESAR);
				endoso.setValorBonificacionComision(endosoReferencia.getBonificacionComision());
				endoso.setValorBonifComRecargoPagoFraccionado(endosoReferencia.getBonificacionComisionRPF());
				endoso.setValorComision(endosoReferencia.getComision());
				endoso.setValorComisionRecargoPagoFraccionado(endosoReferencia.getComisionRPF());
				endoso.setValorDerechos(endosoReferencia.getDerechos());
				endoso.setValorIva(endosoReferencia.getIva());
				endoso.setValorPrimaNeta(endosoReferencia.getPrimaNeta());
				endoso.setValorPrimaTotal(endosoReferencia.getPrimaTotal());
				endoso.setValorRecargoPagoFraccionado(endosoReferencia.getRecargoPF());
				
				try {
					
					//Guarda estos registros en una tabla temporal para su tratamiento, validando antes que no existan en la tabla
					EndosoCancelableDN.getInstancia().notificaCancelacion(endoso);
				} catch (Exception ex) {  
					LogDeMidasWeb.log("Excepcion al Guarda estos registros en una tabla temporal para su tratamiento, validando antes que no existan en la tabla.  IdPoliza" + endosoReferencia.getIdPoliza() + "Endoso "+ endosoReferencia.getNumeroEndoso() , Level.INFO,null);
					LogDeMidasWeb.log("Excepcion al emitir endoso de cancelacion proceso AUTOMATICO", Level.WARNING, ex);
					//envia un mail de notificacion de error
					StringBuffer sb = new StringBuffer();
					sb.append("EndosoCancelableDN.notificaCancelacion");
					sb.append("|");
					sb.append("idToPoliza" + "=" + endosoReferencia.getIdPoliza() + ",");
					sb.append("numeroEndoso" + "=" + endosoReferencia.getNumeroEndoso() + ",");
					UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de Cancelacion proceso AUTOMATICO" + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
				}				
			}
		}
				
		//Parte 2
		//Obtiene de la tabla temporal todos los registros con estatus ESTATUS_REGISTRO_SIN_PROCESAR
		List<EndosoCancelableDTO> endosoCancelableList = EndosoCancelableDN.getInstancia().listaRegistrosSinProcesar();
		
		for (EndosoCancelableDTO endosoCancelableI : endosoCancelableList) {
			
			try {
				//TODO: Verificar que continue en ESTATUS_REGISTRO_SIN_PROCESAR, de lo contrario continuar con el siguiente registro
				EndosoCancelableDTO endosoCancelable = EndosoCancelableDN.getInstancia()
				.encuentraPorIdRefrescado(endosoCancelableI.getId());
				
				if (endosoCancelable.getEstatusRegistro().shortValue() == EndosoCancelableDTO.ESTATUS_REGISTRO_SIN_PROCESAR) {
				
					//Por cada uno lo actualiza a estatus ESTATUS_REGISTRO_EN_PROCESO
					endosoCancelable.setEstatusRegistro(EndosoCancelableDTO.ESTATUS_REGISTRO_EN_PROCESO);
					endosoCancelable = EndosoCancelableDN.getInstancia().actualiza(endosoCancelable);
					LogDeMidasWeb.log("Procesando endoso de cancelacion. IdPoliza " + endosoCancelable.getId().getIdToPoliza() + "Endoso "+ endosoCancelable.getId().getNumeroEndoso() + "Fecha de Vigencia " + endosoCancelable.getFechaInicioVigencia() , Level.INFO,null);
					try {
						//Si se actualizo correctamente, se emite el endoso de cancelacion en Midas
						//Si se trata de cancelacion de endoso, se emite un endoso de cancelacion de endoso
						if (endosoCancelable.getId().getNumeroEndoso().intValue() > 0) {
							CancelacionEndosoDN.getInstancia().cancelarEndoso(endosoCancelable.getId().getIdToPoliza(), 
									endosoCancelable.getId().getNumeroEndoso(), endosoCancelable.getFechaInicioVigencia(), 
									Sistema.USUARIO_SISTEMA);
						} else { //De lo contrario, se emite un endoso de cancelacion de poliza
							CancelacionPolizaDN.getInstancia().cancelacionAutomaticaPolizaEndoso(convierteObjetoEndoso(endosoCancelable));
						}
//						EndosoDN.getInstancia(Sistema.USUARIO_SISTEMA).emiteEndosoCancelacionAutomatica(convierteObjetoEndoso(endosoCancelable));
//						
						
						
						//Si se emitio correctamente, se actualiza el estatus del registro de la tabla temporal a ESTATUS_REGISTRO_PROCESADO
						endosoCancelable.setEstatusRegistro(EndosoCancelableDTO.ESTATUS_REGISTRO_PROCESADO);
						endosoCancelable = EndosoCancelableDN.getInstancia().actualiza(endosoCancelable);
						
					} catch (Exception ex) {
						LogDeMidasWeb.log("Excepcion al emitir endoso de cancelacion. IdPoliza " + endosoCancelable.getId().getIdToPoliza() + "Endoso "+ endosoCancelable.getId().getNumeroEndoso() + "Fecha de Vigencia " + endosoCancelable.getFechaInicioVigencia() , Level.INFO,null);
						LogDeMidasWeb.log("Excepcion al emitir endoso de cancelacion.", Level.WARNING, ex);
						
						//Si no, se actualiza el estatus del registro de la tabla temporal a ESTATUS_EMISION_REGISTRO_FALLO
						endosoCancelable.setEstatusRegistro(EndosoCancelableDTO.ESTATUS_EMISION_REGISTRO_FALLO);
						endosoCancelable = EndosoCancelableDN.getInstancia().actualiza(endosoCancelable);
						
						//envia un mail de notificacion de error
						StringBuffer sb = new StringBuffer();
						sb.append("EndosoDN.emiteEndosoCancelacionAutomatica");
						sb.append("|");
						sb.append("idToPoliza" + "=" + endosoCancelable.getId().getIdToPoliza() + ",");
						sb.append("numeroEndoso" + "=" + endosoCancelable.getId().getNumeroEndoso() + ",");
						sb.append("fechaIniVigEnd" + "=" + endosoCancelable.getFechaInicioVigencia() + ",");
						
						UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de Cancelacion" + " en " + 
								Sistema.AMBIENTE_SISTEMA, sb.toString());
					}
				}
			} catch (Exception ex) {
				//Por lo pronto se asume que puede ser una excepcion de concurrencia, por lo que evaluara el siguiente registro
				//TODO: Verificar y poner la excepcion mas apropiada
			}
		}
			
	}
	
	private EndosoIDTO convierteObjetoEndoso(EndosoCancelableDTO endosoCancelable) {
		
		EndosoIDTO endoso = new EndosoIDTO();
		
		endoso.setIdPoliza(endosoCancelable.getId().getIdToPoliza());
		endoso.setNumeroEndoso(endosoCancelable.getId().getNumeroEndoso());
				
		endoso.setFechaInicioVigencia(endosoCancelable.getFechaInicioVigencia());
		endoso.setBonificacionComision(endosoCancelable.getValorBonificacionComision());
		endoso.setBonificacionComisionRPF(endosoCancelable.getValorBonifComRecargoPagoFraccionado());
		endoso.setComision(endosoCancelable.getValorComision());
		endoso.setComisionRPF(endosoCancelable.getValorComisionRecargoPagoFraccionado());
		endoso.setDerechos(endosoCancelable.getValorDerechos());
		endoso.setIva(endosoCancelable.getValorIva());
		endoso.setPrimaNeta(endosoCancelable.getValorPrimaNeta());
		endoso.setPrimaTotal(endosoCancelable.getValorPrimaTotal());
		endoso.setRecargoPF(endosoCancelable.getValorRecargoPagoFraccionado());
		
		return endoso;
		
	}
	
	@PostConstruct
	public void initialize() {
		String timerInfo = "TimerCancelacionEndosoAutomatica";
		cancelarTemporizador(timerInfo);
		iniciaProgramacionTarea();
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerCancelacionEndosoAutomatica");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerCancelacionEndosoAutomatica:" + e.getMessage(), e);
		}
	}
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

}
