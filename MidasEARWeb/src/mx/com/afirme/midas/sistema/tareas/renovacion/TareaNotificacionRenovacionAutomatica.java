package mx.com.afirme.midas.sistema.tareas.renovacion;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.notificacion.NotificacionRenovacionDN;
import mx.com.afirme.midas.poliza.renovacion.notificacion.NotificacionRenovacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaNotificacionRenovacionAutomatica implements Job {

	public void iniciaProgramacionTarea(StdSchedulerFactory factory) {

		try {

			Scheduler scheduler = factory.getScheduler();

			// Define una instancia del job
			JobDetail job = new JobDetail(
					"jobNotificacionRenovacionAutomatica",
					Scheduler.DEFAULT_GROUP,
					TareaNotificacionRenovacionAutomatica.class);

			// Define una instancia del trigger (cuando y cada cuanto se
			// ejecutara el job)
			Trigger trigger = new CronTrigger(
					"cronTriggerNotificacionRenovacionAutomatica",
					Scheduler.DEFAULT_GROUP,
					Sistema.EXPRESION_CRON_NOTIFICACION_RENOVACION);

			// Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);

		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void execute(JobExecutionContext context)
			throws JobExecutionException {

		String instName = context.getJobDetail().getName();
		String instGroup = context.getJobDetail().getGroup();
		try {
			LogDeMidasWeb
					.log(
							"Ejecutando tarea programada Notificacion de Renovacion Automatica. Instancia : "
									+ instName + ", Grupo: " + instGroup,
							Level.INFO, null);
			if (Sistema.NOTIFICACION_RENOVACION_ACTIVO)
				notificarRenovacionesProximasAVencer();

			enviarNotificacionPolizasNoRenovadas();

			LogDeMidasWeb
					.log(
							"Tarea programada Notificacion de Renovacion Automatica ejecutada. Instancia : "
									+ instName + ", Grupo: " + instGroup,
							Level.INFO, null);

		} catch (Exception ex) {
			LogDeMidasWeb
					.log(
							"Excepcion encontrada mientras se ejecutaba la tarea programada Notificacion de Renovacion Automatica. Instancia : "
									+ instName + ", Grupo: " + instGroup,
							Level.WARNING, ex);
		}

	}

	public void notificaRenovacionEjecucionManual() throws Exception {
		try {
			LogDeMidasWeb
					.log(
							"Ejecutando tarea programada Notificacion de Renovacion Automatica. Instancia : ",
							Level.INFO, null);

			if (Sistema.NOTIFICACION_RENOVACION_ACTIVO)
				notificarRenovacionesProximasAVencer();

			enviarNotificacionPolizasNoRenovadas();

			LogDeMidasWeb
					.log(
							"Tarea programada Notificacion de Renovacion Automatica ejecutada. Instancia : ",
							Level.INFO, null);

		} catch (Exception ex) {
			LogDeMidasWeb
					.log(
							"Excepcion encontrada mientras se ejecutaba la tarea programada Notificacion de Renovacion Automatica. Instancia : ",
							Level.WARNING, ex);
		}
	}

	private void enviarNotificacionPolizasNoRenovadas() throws Exception {
		String[] diasNotificar = Sistema.RENOVACION_POLITICA_DIAS_VENCIMIENTO;
		if (diasNotificar != null && diasNotificar.length > 0) {
			RenovacionPolizaDTO renovacionPolizaDTO = new RenovacionPolizaDTO();
			renovacionPolizaDTO.setCotizacionDTO(new CotizacionDTO());
			for (String dia : diasNotificar) {
				Integer numeroDia = Integer.parseInt(dia);
				Date fechaVencimiento = UtileriasWeb.sumaDias(new Date(),
						numeroDia);
				renovacionPolizaDTO.getCotizacionDTO().setFechaInicioVigencia(
						fechaVencimiento);
				renovacionPolizaDTO.getCotizacionDTO().setFechaFinVigencia(
						fechaVencimiento);
				List<RenovacionPolizaDTO> renovaciones = RenovacionPolizaDN
						.getInstancia().buscarFiltrado(renovacionPolizaDTO,
								Boolean.TRUE);
				
				if(renovaciones != null && renovaciones.size() > 0)
					this.procesarPolizasANotificar(renovaciones, dia,
							RenovacionPolizaDTO.class);
			}
		}
	}

	private void notificarRenovacionesProximasAVencer() throws Exception {
		// 1-Obtener el arreglo de dias que determina la vigencia de las polizas
		// a buscar.
		String[] diasNotificar = Sistema.RENOVACION_POLITICA_DIAS_VENCIMIENTO;
		if (diasNotificar != null && diasNotificar.length > 0) {
			PolizaDTO polizaBase = new PolizaDTO();
			polizaBase.setCotizacionDTO(new CotizacionDTO());
			polizaBase.setClaveEstatus(Sistema.ESTATUS_VIGENTE);
			for (String dia : diasNotificar) {
				Integer numeroDia = Integer.parseInt(dia);
				Date fechaVencimiento = UtileriasWeb.sumaDias(new Date(),
						numeroDia);
				polizaBase.getCotizacionDTO().setFechaInicioVigencia(
						fechaVencimiento);
				polizaBase.getCotizacionDTO().setFechaFinVigencia(
						fechaVencimiento);
				List<PolizaDTO> polizas = RenovacionPolizaDN.getInstancia()
						.buscarFiltrado(polizaBase, Boolean.FALSE, Boolean.FALSE);
				this.procesarPolizasANotificar(polizas, dia, PolizaDTO.class);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void procesarPolizasANotificar(List<?> polizas,
			String diasVencimiento, Class<?> classe) {
		if (classe.getCanonicalName()
				.equals(PolizaDTO.class.getCanonicalName())) {
			for (PolizaDTO poliza : (List<PolizaDTO>) polizas) {
				this.procesarPolizaANotificar(poliza, diasVencimiento);
			}
		} else {
			for (RenovacionPolizaDTO renovacion : (List<RenovacionPolizaDTO>) polizas) {
				this.procesarPolizaANotificar(renovacion.getPolizaDTO(),
						diasVencimiento);
			}

		}
	}

	private void procesarPolizaANotificar(PolizaDTO poliza,
			String diasVencimiento) {
		if (poliza != null) {
			String[] destinatarios = Sistema.RENOVACION_POLITICA_DESTINATARIOS;
			for (String destinatario : destinatarios) {
				try {
					if (this.enviaCorreoNotificaBitacora(poliza, destinatario,
							diasVencimiento)) {
						// exito al enviar correo y guardar la notificacion
						// en la bitacora
						this
								.insertaPolizaANotificar(
										poliza,
										destinatario,
										diasVencimiento,
										NotificacionRenovacionDTO.ESTATUS_REGISTRO_PROCESADO);
					} else {
						// error al enviar correo y guardar en la bitacora
						this
								.insertaPolizaANotificar(
										poliza,
										destinatario,
										diasVencimiento,
										NotificacionRenovacionDTO.ESTATUS_EMISION_REGISTRO_FALLO);
					}
				} catch (ExcepcionDeAccesoADatos e) {
					LogDeMidasWeb.log(
							"Excepcion Insertar la notificacion de la poliza: "
									+ poliza.getNumeroPolizaFormateada(),
							Level.INFO, e);
				} catch (SystemException e) {
					LogDeMidasWeb.log(
							"Excepcion Insertar la notificacion de la poliza: "
									+ poliza.getNumeroPolizaFormateada(),
							Level.INFO, e);
				}
			}

		}
	}

	private void insertaPolizaANotificar(PolizaDTO poliza,
			String destinatarioCorreo, String diasVencimiento,
			Short estausRegistro) throws ExcepcionDeAccesoADatos,
			SystemException {
		NotificacionRenovacionDTO notificacion = new NotificacionRenovacionDTO();

		notificacion.setTipoDestinatario(Short.valueOf(destinatarioCorreo));
		notificacion.setFechaMovimiento(new Date());
		notificacion.setPolizaDTO(poliza);
		notificacion.setEstatusRegistro(estausRegistro);
		notificacion.setDescripcionMovimiento(UtileriasWeb.getMensajeRecurso(
				Sistema.ARCHIVO_RECURSOS,
				"poliza.renovacion.notificacion.automatica", poliza
						.getNumeroPolizaFormateada(), diasVencimiento));
		NotificacionRenovacionDN.getInstancia().agregarNotificacionRenovacion(
				notificacion);

	}

	private boolean enviaCorreoNotificaBitacora(PolizaDTO polizaDTO,
			String tipoDestinatario, String diasVencimiento) {
		RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN
				.getInstancia();
		boolean exito = Boolean.TRUE;
		try {
			exito = renovacionPolizaDN.enviaNotificacionCorreoElectronico(
					polizaDTO, Sistema.USUARIO_SISTEMA, tipoDestinatario,
					diasVencimiento, Boolean.FALSE);
		} catch (SystemException e) {
			exito = Boolean.FALSE;
		}
		return exito;
	}
}
