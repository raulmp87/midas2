package mx.com.afirme.midas.sistema.tareas.reaseguro.distribucion;

import java.text.ParseException;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirPolizaDN;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaDetenerDistribucionPolizas implements Job {

	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		try {
			Scheduler scheduler = factory.getScheduler();
			
            // Define una instancia del job
			JobDetail job = new JobDetail("jobDetenerDistribucionPolizas", Scheduler.DEFAULT_GROUP, TareaDetenerDistribucionPolizas.class);
			
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger("cronTareaDetenerDistribucionPolizas", Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_FIN_DISTRIBUCION_POLIZAS);
				
			//Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Distribuci�n de p�lizas en Reaseguro. Instancia : " + instName
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	        
	        TemporizadorDistribuirPolizaDN.getInstancia().detenerTemporizadoresDistribucionPolizas();
	    	
	        LogDeMidasWeb.log("Tarea programada Distribuci�n de p�lizas en Reaseguro ejecutada con �xito. Instancia : " + instName
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Ocurri� una excepcion al ejecutar la tarea programada Distribuci�n de p�lizas en Reaseguro. Instancia : "
	    			+ instName + ", Grupo: " + instGroup, Level.WARNING, ex);
	    }
	}
	
}
