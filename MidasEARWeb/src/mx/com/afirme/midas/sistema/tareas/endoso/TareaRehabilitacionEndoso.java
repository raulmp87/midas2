package mx.com.afirme.midas.sistema.tareas.endoso;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDN;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableId;
import mx.com.afirme.midas.endoso.rehabilitacion.RehabilitacionEndosoDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.mail.MailAction;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaRehabilitacionEndoso implements Job {

	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		
		try {
			
			Scheduler scheduler = factory.getScheduler();
			
            // Define una instancia del job
			JobDetail job = new JobDetail("jobRehabilitacionEndoso", Scheduler.DEFAULT_GROUP, TareaRehabilitacionEndoso.class);
			
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger("cronTriggerRehabilitacionEndoso", Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_REHABILITACION_ENDOSO);
				
			//Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);
            			
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Rehabilitacion de Endosos. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
		    
	        rehabilitaEndososPendientes(true);
	         
	        LogDeMidasWeb.log("Tarea programada Rehabilitacion de Endosos ejecutada. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	    	
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Rehabilitacion de Endosos. Instancia : " 
	    			+ instName + ", Grupo: " + instGroup, Level.WARNING, ex);
	    }

	}
	
	
	public void rehabilitaEndososPendientes(boolean consultarSeycos) throws Exception {
		
		//Parte 1
		//Obtiene de Seycos todos los registros de sus endosos rehabilitados que no fueron notificados a Midas
		if(consultarSeycos){
			registrarEndososRehabilitables();
		}
				
		//Parte 2
		//Obtiene de la tabla temporal todos los registros con estatus ESTATUS_REGISTRO_SIN_PROCESAR
		procesarEndososRehabilitables();
		
	}
	
	private void registrarEndososRehabilitables() throws ExcepcionDeAccesoADatos, SystemException{
		List<EndosoIDTO> endosoReferenciaList = EndosoIDN.getInstancia().obtieneListaRehabilitables(new Date(), Sistema.USUARIO_SISTEMA);
		StringBuffer sb = null;
		Format formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		for (EndosoIDTO endosoReferencia : endosoReferenciaList) {
			
			if(endosoReferencia.getEsValido().shortValue() == 0){
				//envia un mail de notificacion de error
				sb = new StringBuffer();
				sb.append("TareaRehabilitacionEndoso.rehabilitaEndososPendientes()");
				sb.append("----------------------------------------------------------------<br />" );
				sb.append("Id Poliza" + "= <b><span style=\"color: #333333\">" + endosoReferencia.getIdPoliza() + " </b> ");
				sb.append("No. Endoso" + "= <b><span style=\"color: #333333\">" + endosoReferencia.getNumeroEndosoString()+ "</b> ");
				sb.append("Fecha Inicio Viegncia" + "= <b><span style=\"color: #333333\">" + formatter.format(endosoReferencia.getFechaInicioVigencia()) + "</b> ");
				sb.append("Fecha Sistema" + "= <b><span style=\"color: #333333\">" + formatter.format(new Date()) + "</b> ");
				sb.append("----------------------------------------------------------------<br />" );
				
			}else{
				//Obtiene la fecha de inicio de vigencia del ultimo endoso de esa poliza en Midas. Esto es temporal, se quitara cuando
				// el SP obtieneListaRehabilitables regrese tambien la fecha de inicio de vigencia
//				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(Sistema.USUARIO_SISTEMA).getUltimoEndoso(endosoReferencia.getIdPoliza());
				
				EndosoRehabilitableId id = new EndosoRehabilitableId(endosoReferencia.getIdPoliza(), endosoReferencia.getNumeroEndoso());
				EndosoRehabilitableDTO endoso = new EndosoRehabilitableDTO();
				endoso.setId(id);
				endoso.setClaveTipoEndoso(Sistema.TIPO_ENDOSO_REHABILITACION);
				endoso.setFechaInicioVigencia(endosoReferencia.getFechaInicioVigencia());
				endoso.setEstatusRegistro(EndosoRehabilitableDTO.ESTATUS_REGISTRO_SIN_PROCESAR);
				endoso.setValorBonifComision(endosoReferencia.getBonificacionComision());
				endoso.setValorBonifComisionRPF(endosoReferencia.getBonificacionComisionRPF());
				endoso.setValorComision(endosoReferencia.getComision());
				endoso.setValorComisionRPF(endosoReferencia.getComisionRPF());
				endoso.setValorDerechos(endosoReferencia.getDerechos());
				endoso.setValorIVA(endosoReferencia.getIva());
				endoso.setValorPrimaNeta(endosoReferencia.getPrimaNeta());
				endoso.setValorPrimaTotal(endosoReferencia.getPrimaTotal());
				endoso.setValorRecargoPagoFrac(endosoReferencia.getRecargoPF());
				
				try {
					
					//Guarda estos registros en una tabla temporal para su tratamiento, validando antes que no existan en la tabla
					EndosoRehabilitableDN.getInstancia().notificaRehabilitacion(endoso);
					} catch (Exception ex) {  
						LogDeMidasWeb.log("Excepcion al Guarda estos registros en una tabla temporal para su tratamiento, validando antes que no existan en la tabla.  IdPoliza" + endosoReferencia.getIdPoliza() + "Endoso "+ endosoReferencia.getNumeroEndoso() , Level.INFO,null);
						LogDeMidasWeb.log("Excepcion al emitir endoso de rehabilitacion proceso AUTOMATICO", Level.WARNING, ex);
						//envia un mail de notificacion de error
						StringBuffer sb1 = new StringBuffer();
						sb1.append("EndosoRehabilitableDN.notificaRehabilitacion");
						sb1.append("|");
						sb1.append("idToPoliza" + "=" + endosoReferencia.getIdPoliza() + ",");
						sb1.append("numeroEndoso" + "=" + endosoReferencia.getNumeroEndoso() + ",");
						UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de rehabilitacion proceso AUTOMATICO" + " en " + 
						Sistema.AMBIENTE_SISTEMA, sb1.toString());
				}				
			}
			
			if (Sistema.NOTIFICACION_CORREO_ERROR_REHABILITACION && sb != null){
				
				List<String> destinatarios = new ArrayList<String>();
				destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_MIDAS);
				destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_AFIRME);
				
				MailAction.enviaCorreoConTemplate(destinatarios, "Rehabilitaciones Fallidas en el proceso Automático", sb.toString(), "Administrador del Sistema", null);
			}
			

			
		}
	}
	
	private void procesarEndososRehabilitables() throws Exception {
		
		List<EndosoRehabilitableDTO> endosoRehabilitableList = null;
		
		try{
			endosoRehabilitableList = EndosoRehabilitableDN.getInstancia().listaRegistrosSinProcesar();
		}
		catch(Exception e){
			String error = "Tarea rehabilitación de endosos. Excepcion al consultar la lista de endosos rehabilitables en Midas.";
			LogDeMidasWeb.log(error, Level.SEVERE,e);
			
			//envia un mail de notificacion de error
			StringBuffer sb = new StringBuffer();
			sb.append("EndosoDN.procesarEndososRehabilitables().\n");
			sb.append(error);
			
			UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de Rehabilitacion en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			
			throw e;
		}
		
		for (EndosoRehabilitableDTO endosoRehabilitableI : endosoRehabilitableList) {
			//Verificar que continue en ESTATUS_REGISTRO_SIN_PROCESAR, de lo contrario continuar con el siguiente registro
			EndosoRehabilitableDTO endosoRehabilitable = null;
			try{
				endosoRehabilitable = EndosoRehabilitableDN.getInstancia().encuentraPorIdRefrescado(endosoRehabilitableI.getId());
			}
			catch(Exception e){
				String error = "Tarea rehabilitación de endosos. Excepcion al consultar registro EndosoRehabilitable. IdPoliza " + 
					endosoRehabilitableI.getId().getIdToPoliza() + "Endoso "+ endosoRehabilitableI.getId().getNumeroEndoso() + "Fecha de Vigencia " + endosoRehabilitableI.getFechaInicioVigencia();
				LogDeMidasWeb.log(error , Level.SEVERE,e);
				
				//Si no, se actualiza el estatus del registro de la tabla temporal a ESTATUS_EMISION_REGISTRO_FALLO
				endosoRehabilitable.setEstatusRegistro(EndosoRehabilitableDTO.ESTATUS_EMISION_REGISTRO_FALLO);
				try {
					endosoRehabilitable = EndosoRehabilitableDN.getInstancia().actualiza(endosoRehabilitable);
				} catch (Exception e1) {
				}
				
				//envia un mail de notificacion de error
				StringBuffer sb = new StringBuffer();
				sb.append("EndosoDN.emiteEndosoRehabilitacion");
				sb.append("|");
				sb.append("idToPoliza" + "=" + endosoRehabilitableI.getId().getIdToPoliza() + ",");
				sb.append("numeroEndoso" + "=" + endosoRehabilitableI.getId().getNumeroEndoso() + ",");
				sb.append("fechaIniVigEnd" + "=" + endosoRehabilitableI.getFechaInicioVigencia() + ",");
				
				UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de Rehabilitacion en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			}
			
			if(endosoRehabilitable == null){
				continue;
			}
			
			if (endosoRehabilitable.getEstatusRegistro().shortValue() == EndosoRehabilitableDTO.ESTATUS_REGISTRO_SIN_PROCESAR) {
			
				//Por cada uno lo actualiza a estatus ESTATUS_REGISTRO_EN_PROCESO
				endosoRehabilitable.setEstatusRegistro(EndosoRehabilitableDTO.ESTATUS_REGISTRO_EN_PROCESO);
				try {
					endosoRehabilitable = EndosoRehabilitableDN.getInstancia().actualiza(endosoRehabilitable);
				} catch (Exception e) {
				}
				if(endosoRehabilitable == null || endosoRehabilitable.getEstatusRegistro() == null || 
						endosoRehabilitable.getEstatusRegistro().shortValue() != EndosoRehabilitableDTO.ESTATUS_REGISTRO_EN_PROCESO){
					continue;
				}
				LogDeMidasWeb.log("Procesando endoso de rehabilitacion. IdPoliza " + 
						endosoRehabilitable.getId().getIdToPoliza() + "Endoso "+ endosoRehabilitable.getId().getNumeroEndoso() +
						"Fecha de Vigencia " + endosoRehabilitable.getFechaInicioVigencia() , Level.INFO,null);
				try {
					//Si se actualizo correctamente, se emite el endoso de Rehabilitacion en Midas
					//Si se trata de Rehabilitacion de endoso, se emite un endoso de Rehabilitacion de endoso
					if (endosoRehabilitable.getId().getNumeroEndoso().intValue() > 0) {
						RehabilitacionEndosoDN.getInstancia().rehabilitarEndoso(endosoRehabilitable.getId().getIdToPoliza(), 
								endosoRehabilitable.getId().getNumeroEndoso(), endosoRehabilitable.getFechaInicioVigencia(), 
								Sistema.USUARIO_SISTEMA);
					} else { //De lo contrario, se emite un endoso de Rehabilitacion de poliza
						EndosoDN.getInstancia(Sistema.USUARIO_SISTEMA).emiteEndosoRehabilitacion(endosoRehabilitable);
					}
					
					//Si se emitio correctamente, se actualiza el estatus del registro de la tabla temporal a ESTATUS_REGISTRO_PROCESADO
					endosoRehabilitable.setEstatusRegistro(EndosoRehabilitableDTO.ESTATUS_REGISTRO_PROCESADO);
					endosoRehabilitable = EndosoRehabilitableDN.getInstancia().actualiza(endosoRehabilitable);
					
				} catch (Exception ex) {
					LogDeMidasWeb.log("Excepcion al emitir endoso de rehabilitacion. IdPoliza " + endosoRehabilitable.getId().getIdToPoliza() + "Endoso "+ endosoRehabilitable.getId().getNumeroEndoso() + "Fecha de Vigencia " + endosoRehabilitable.getFechaInicioVigencia() , Level.INFO,null);
					LogDeMidasWeb.log("Excepcion al emitir endoso de rehabilitacion.", Level.WARNING, ex);
					
					//Si no, se actualiza el estatus del registro de la tabla temporal a ESTATUS_EMISION_REGISTRO_FALLO
					endosoRehabilitable.setEstatusRegistro(EndosoRehabilitableDTO.ESTATUS_EMISION_REGISTRO_FALLO);
					try {
						endosoRehabilitable = EndosoRehabilitableDN.getInstancia().actualiza(endosoRehabilitable);
					} catch (Exception e) {
					}
					
					//envia un mail de notificacion de error
					StringBuffer sb = new StringBuffer();
					sb.append("EndosoDN.emiteEndosoRehabilitacion");
					sb.append("|");
					sb.append("idToPoliza" + "=" + endosoRehabilitable.getId().getIdToPoliza() + ",");
					sb.append("numeroEndoso" + "=" + endosoRehabilitable.getId().getNumeroEndoso() + ",");
					sb.append("fechaIniVigEnd" + "=" + endosoRehabilitable.getFechaInicioVigencia() + ",");
					
					UtileriasWeb.enviaCorreoExcepcion("Emitir endoso de Rehabilitacion" + " en " + 
							Sistema.AMBIENTE_SISTEMA, sb.toString());
				}
			}
		}
	}
	
	public EndosoIDTO convierteObjetoEndoso(EndosoRehabilitableDTO endosoRehabilitable) {
		EndosoIDTO endoso = new EndosoIDTO();
		
		endoso.setIdPoliza(endosoRehabilitable.getId().getIdToPoliza());
		endoso.setNumeroEndoso(endosoRehabilitable.getId().getNumeroEndoso());
				
		endoso.setFechaInicioVigencia(endosoRehabilitable.getFechaInicioVigencia());
		endoso.setBonificacionComision(endosoRehabilitable.getValorBonifComision());
		endoso.setBonificacionComisionRPF(endosoRehabilitable.getValorBonifComisionRPF());
		endoso.setComision(endosoRehabilitable.getValorComision());
		endoso.setComisionRPF(endosoRehabilitable.getValorComisionRPF());
		endoso.setDerechos(endosoRehabilitable.getValorDerechos());
		endoso.setIva(endosoRehabilitable.getValorIVA());
		endoso.setPrimaNeta(endosoRehabilitable.getValorPrimaNeta());
		endoso.setPrimaTotal(endosoRehabilitable.getValorPrimaTotal());
		endoso.setRecargoPF(endosoRehabilitable.getValorRecargoPagoFrac());
		
		return endoso;
	}

}
