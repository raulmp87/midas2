package mx.com.afirme.midas.sistema.tareas.siniestros;

import java.text.ParseException;
import java.util.logging.Level;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaOrdenDePagoPendientes implements Job {

	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		
		try {
			
				System.out.println("INICIA iniciaProgramacionTarea - TareaOrdenDePagoPendientes");
			Scheduler scheduler = factory.getScheduler();
			
            // Define una instancia del job
			JobDetail job = new JobDetail("jobOrdenDePagoPendientes", Scheduler.DEFAULT_GROUP, TareaOrdenDePagoPendientes.class);
				System.out.println("Define una instancia del trigger TareaOrdenDePagoPendientes");
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger("tareaOrdenDePagoPendientes", Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_ORDENDEPAGOPENDIENTES);
				System.out.println("Programa el job con el trigger TareaOrdenDePagoPendientes");
			//Programa el job con el trigger
				
				
				
			scheduler.scheduleJob(job, trigger);
				System.out.println("FIN iniciaProgramacionTarea - TareaOrdenDePagoPendientes");
			
            			
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
			System.out.println("INICIA execute - TareaOrdenDePagoPendientes");
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	    	System.out.println("Ejecutando tarea programada Orden De Pago Pendientes de Siniestros. Execute");
	        LogDeMidasWeb.log("Ejecutando tarea programada Orden De Pago Pendientes de Siniestros. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
		    
	        tareaOrdenDePagoPendientes();
	         
	        System.out.println("Tarea programada Orden De Pago Pendientes de Siniestros ejecutada. Execute ");
	        LogDeMidasWeb.log("Tarea programada Orden De Pago Pendientes de Siniestros ejecutada. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	        System.out.println("FIN execute - TareaOrdenDePagoPendientes");
	    	
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea programada Orden De Pago Pendientes de Siniestros. Instancia : " 
	    			+ instName + ", Grupo: " + instGroup, Level.WARNING, ex);
	    }

	}
	
	
	private void tareaOrdenDePagoPendientes() throws Exception {
		
		try {
			OrdenDePagoDN.getInstancia().procesarOrdenesPendientes(Sistema.USUARIO_SISTEMA);
			
			} catch (Exception ex) {
				LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba el metodo OrdenDePagoDN.getInstancia().procesarOrdenesPendientes(String nombreUsuario) la tarea Orden De Pago Pendientes de Siniestros. Instancia : " 
		    			, Level.SEVERE, ex);
			}

	}

}
