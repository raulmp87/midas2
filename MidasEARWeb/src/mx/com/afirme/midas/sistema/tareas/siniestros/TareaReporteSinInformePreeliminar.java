package mx.com.afirme.midas.sistema.tareas.siniestros;

import java.text.ParseException;
import java.util.logging.Level;


import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public class TareaReporteSinInformePreeliminar implements Job {

	public void iniciaProgramacionTarea (StdSchedulerFactory factory) {
		
		try {
			
			Scheduler scheduler = factory.getScheduler();
			
            // Define una instancia del job
			JobDetail job = new JobDetail("jobTareaReporteSinInformePreeliminar", Scheduler.DEFAULT_GROUP, TareaReporteSinInformePreeliminar.class);
			
			//Define una instancia del trigger (cuando y cada cuanto se ejecutara el job)
			Trigger trigger = new CronTrigger("cronTareaReporteSinInformePreeliminar", Scheduler.DEFAULT_GROUP, Sistema.EXPRESION_CRON_REPORTESININFORMEPREELIMINAR);
				
			//Programa el job con el trigger
			scheduler.scheduleJob(job, trigger);
            			
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		String instName = context.getJobDetail().getName();
	    String instGroup = context.getJobDetail().getGroup();
	    try {
	        LogDeMidasWeb.log("Ejecutando tarea programada Reportes Sin Informe Preeliminar de Siniestros. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
		    
	        reporteSinInformePreeliminar();
	         
	        LogDeMidasWeb.log("Tarea programada Reportes Sin Informe Preeliminar de Siniestros ejecutada. Instancia : " + instName 
		    		+ ", Grupo: " + instGroup, Level.INFO, null);
	    	
	    } catch (Exception ex) {
	    	LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba la tarea Reportes Sin Informe Preeliminar de Siniestros. Instancia : " 
	    			+ instName + ", Grupo: " + instGroup, Level.SEVERE, ex);
	    }

	}
	
	
	private void reporteSinInformePreeliminar() throws Exception {
		
		try {
		SiniestroMovimientosDN.getInstancia().enviarNotificacionReporteSinInformePreliminar();
		} catch (Exception ex) {
			LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba el metodo SiniestroMovimientosDN.getInstancia().enviarNotificacionReporteSinInformePreliminar() la tarea Reportes Sin Informe Preeliminar de Siniestros. Instancia : " 
	    			, Level.SEVERE, ex);
		}
			
	}

}
