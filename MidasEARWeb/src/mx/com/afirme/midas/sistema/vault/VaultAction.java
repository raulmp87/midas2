package mx.com.afirme.midas.sistema.vault;

import static mx.com.afirme.midas.sistema.UtileriasWeb.obtenerRutaDocumentosAnexos;
import static mx.com.afirme.midas.sistema.UtileriasWeb.regresaBigDecimal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDTO;
import mx.com.afirme.midas.cenaexpedientes.ManejadorFortimaxCenaAction;
import mx.com.afirme.midas.cgcondiciones.ManejadorFortimaxCgAction;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoEquipoContratistaDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTOId;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTOId;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.SlipRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo.SlipAnexoRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.equipocontratista.anexo.SlipAnexoEquipoContratistaDN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.anexo.SlipAnexoRCFuncionariosDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDN;
import mx.com.afirme.midas.cotizacion.documento.DocPrevencionOperIlicitasDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.documento.DocumentoDigitalRangosDN;
import mx.com.afirme.midas.danios.reportes.reportercs.documento.DocumentoDigitalRangosDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.esquemas.DocumentoDigitalEsquemasDN;
import mx.com.afirme.midas.danios.reportes.reportercs.esquemas.DocumentoDigitalEsquemasDTO;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentoanexo.DocumentoAnexoCgDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.documentos.referencias.DocumentosReferenciasAction;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDN;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.documentos.DocumentoSiniestroDN;
import mx.com.afirme.midas.siniestro.documentos.DocumentoSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerResponse;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.sistema.excel.CargaMasivaCadAmisExcel;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.documentos.anexos.DocumentoAnexoSeccion;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCondiciones;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAAnexos;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;

import com.scand.fileupload.ProgressMonitorFileItemFactory;

@SuppressWarnings("deprecation")
public class VaultAction extends MidasMappingDispatchAction {
	
	private static final Logger LOG = LoggerFactory.getLogger(VaultAction.class);	
	private static final String DEFAULT = "default";
	private FileManagerService fileManagerService;
	private ClientesApiService  clienteRest;
	private EntidadService entidadService;  
	public static final int CLAVE_TIPO_LISTA_MODELO_VEHICULO = 41;
		
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
		}
		
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public VaultAction() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			fileManagerService = serviceLocator.getEJB(FileManagerService.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LOG.info("bean Remoto instanciado", Level.FINEST, null);
	}

	@SuppressWarnings("unchecked")
	public void uploadHandler(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

    	ServletActionContext.getContext().getSession().remove(ControlArchivoDTO.IDTOCONTROLARCHIVO);
		String claveTipo = "";
		String claveNegocio = "";
		String idToCotizacion = "";
		String idToInspeccionIncisoCotizacion = "";
		String uploadFolder = obtenerRutaDocumentosAnexos();
		//Variables usadas para los documentos de siniestro(tipo 14)
		String nombreDocumento="";
		String idTipoDocumento="";
		String idReporteSiniestro="";
		String idDocumentoSiniestro="";
		//Variables usadas por complementar cotizacion
		String idToControlArchivo = "";
		
		String idSolicitud = "";
		boolean isMultipart = FileUpload.isMultipartContent(request);
		if (!isMultipart) {
			LOG.info("Use multipart form to upload a file!");
		} else {
			String fileId = request.getParameter("sessionId").toString().trim();
			FileItemFactory factory = new ProgressMonitorFileItemFactory(
					request, fileId);
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items = upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					if(item.getFieldName().equals("claveTipo")) {
						claveTipo = item.getString();
					}
					if(item.getFieldName().equals("claveNegocio")) {
						claveNegocio = item.getString();
					}
					if(item.getFieldName().equals(CotizacionDTO.IDTOCOTIZACION)) {
						idToCotizacion = item.getString();
					// se agrega peps de la cotizacion
						if(entidadService!=null && idToCotizacion!=null){
							CotizacionDTO	cotizacion =entidadService.findById(CotizacionDTO.class, idToCotizacion);	
							if(cotizacion.getIdToPersonaContratante()!=null){
								
								clienteRest.validatePeps(cotizacion.getIdToPersonaContratante().longValue(),String.valueOf(cotizacion.getIdToCotizacion()));
									
							}
							if(cotizacion.getIdToPersonaAsegurado()!=null){
								
								clienteRest.validatePeps(cotizacion.getIdToPersonaAsegurado().longValue(),String.valueOf(cotizacion.getIdToCotizacion()));
									
							}
						}
					
					}
					if(item.getFieldName().equals("idToInspeccionIncisoCotizacion")) {
						idToInspeccionIncisoCotizacion = item.getString();
							
					}
					//Variables usadas para los documentos de siniestro(tipo 14)
					if(item.getFieldName().equals("nombreDocumento")) {
						nombreDocumento = item.getString();
					}
					if(item.getFieldName().equals("idTipoDocumento")) {
						idTipoDocumento = item.getString();
					}
					if(item.getFieldName().equals("idReporteSiniestro")) {
						idReporteSiniestro = item.getString();
					}
					if(item.getFieldName().equals("idDocumentoSiniestro")) {
						idDocumentoSiniestro = item.getString();
					}
					if(item.getFieldName().equals(ControlArchivoDTO.IDTOCONTROLARCHIVO)) {
						idToControlArchivo = item.getString();
					}
					if(item.getFieldName().equals("idSolicitud")) {
						idSolicitud = item.getString();
					}
				} else {
					String fileName = item.getName();
					int i2 = fileName.lastIndexOf("\\");
					if (i2 > -1)
						fileName = fileName.substring(i2 + 1);
					File dirs = new File(uploadFolder);
					File uploadedFile = new File(dirs, fileName);
					
					HttpSession session = request.getSession();

                    // CG,CENA 
					// no guarda(n) en FS
					int clvTipo  = Integer.parseInt(claveTipo);
					if(clvTipo == 40 || claveTipo.equals(String.valueOf(CLAVE_TIPO_LISTA_MODELO_VEHICULO)) || clvTipo == 16 || clvTipo == 43){
						item.write(uploadedFile);
					}
        			
					Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
        			Object idSolicitudSession = session.getAttribute("idSolicitud");
        			Object idToSeccion = session.getAttribute("idToSeccion");
		        			
					Map<String,Object> parametros = new HashMap<String,Object>();
					parametros.put("tipo",claveTipo);
					parametros.put("highAvailability",true);
					
					switch(clvTipo) {
        					case 0:
        						registrarArchivoSolicitud(fileName,idSolicitudSession.toString(), claveTipo, usuario, item);
        						break;
        					case 1:
        						//Archivos de tipo producto 
        						//Gaveta Producto
        						parametros.put("expediente",session.getAttribute("idToProducto").toString());
        						registrarArchivoProducto(fileName, session.getAttribute("idToProducto").toString(), claveTipo, usuario, item,parametros);
        						break;
        					case 2:
        						//Archivos tipo poliza
        						//Gaveta Producto
        						parametros.put("expediente",session.getAttribute("idToTipoPoliza").toString());
        						registrarArchivoTipoPoliza(fileName, session.getAttribute("idToTipoPoliza").toString(), claveTipo, usuario, item,parametros);
        						break;
        					case 3:
        						//Archivos tipo cobertura -productos
        						//Gaveta Producto
        						parametros.put("expediente",session.getAttribute("idToCobertura").toString());
        						registrarArchivoCobertura(fileName, session.getAttribute("idToCobertura").toString(), claveTipo, usuario, item,parametros);
        						break;
        					case 4:
        						//Gaveta Cotizaciones
        						//Documentos de Cotizaciones
        						parametros.put("expediente",idToCotizacion);
        						registrarDocumentoDigitalComplementario(fileName, idToCotizacion, claveTipo, usuario, request, item,parametros);
        						break;
        					case 5:
        						//Documentos anexos para SLIP
        						registrarArchivoSlip(fileName,session.getAttribute("idToSlip").toString(),
        									session.getAttribute("slipGeneral").toString(), claveTipo,
        									session.getAttribute("numeroInciso"), usuario, item);
        							break;
        					case 6:
        						registrarDocumentoAnexoInspeccionIncisoCotizacion(fileName, idToInspeccionIncisoCotizacion, 
        								claveTipo, usuario, request, item);
        						break;
        					case 7:
        						//Gaveta Producto
        						//archivo de registro CNSF a la cobertura
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 8:
        						//Gaveta Producto
        						//ArchivoRegistroCNSFProducto para producto
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 9:
        						//Gaveta Producto
        						//ArchivoCaratulaPolizaProducto para producto
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 10:
        						//Gaveta Producto
        						//ArchivoCondicionesProductoProducto para producto
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 11:
        						//Gaveta Producto
        						//ArchivoNotaTecnicaProducto para producto
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 12:
        						//Gaveta Producto
        						//ArchivoAnalisisCongruenciaProducto para producto
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 13:
        						//Gaveta Producto
        						//ArchivoDictamenJuridicoProducto para producto
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 14:
        						//ArchivoDocumentoSiniestro
        						registrarArchivoDocumentoSiniestro(fileName, request, usuario, idReporteSiniestro, nombreDocumento, idTipoDocumento, idDocumentoSiniestro, item);
        						break;
        						//Documentos complementar cotizacion
        					case 15:
        						registrarArchivoPrevencionOperacionesIlicitas(fileName, request, claveTipo, idToCotizacion, idTipoDocumento, idToControlArchivo);
        						break;
        					case 16:
        						//Tipo de archivo Autos solicitudes-carga de plantillas-importe recuperables de reaseguradoras-reporte insumos autos
        						//REAS 
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 17:
        						//Valida que sea extension PDF
        						if(fileName.toUpperCase().endsWith(ConstantesReporte.SUFFIX_PDF)) {
        							registrarDocumentoAnexoReaseguroCotizacion(fileName, 
        									session.getAttribute(CotizacionDTO.IDTOCOTIZACION).toString(), claveTipo, usuario, item);
        						} else {
        							throw new Exception("No es PDF");
        						}
        						break;
        					case 18:   
        						//Gaveta Solicitudes
        						parametros.put("expediente",idSolicitud);
        						registrarDocumentoComentario(fileName,idSolicitud, claveTipo, usuario, item,parametros);				
        						break;
        					case 19:
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 20:
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
							case 21:
								//Archivo de agentes
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
							case 22:
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
							case 23:
                                if(fileName.toUpperCase().endsWith(ConstantesReporte.SUFFIX_PDF)) {
                                    registrarArchivoCg(fileName, item, idToSeccion.toString(), claveTipo, usuario);
                                } else {
                                    throw new Exception("No es PDF");
                                }
								break;
							case 24:
								 registrarArchivoCad(fileName, claveTipo, usuario);
								break;
							case 25:
								registrarArchivo(fileName, request, claveTipo, item,parametros);
								break;
        					case 30:
        						//Gaveta Catalogo
        						//Midas Siniestros Condicion Especial
        						parametros.put("expediente",idDocumentoSiniestro);
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
							case 35: 
								//Midas Archivo Importe Vida
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
							case 36:
								//Revisar
								//Gaveta Negocio
								String idNegocio = "";
								try{
									idNegocio = session.getAttribute("idNegocio").toString();
								} catch (NullPointerException e) {
									LOG.error(e.getMessage(), e);
								}
        						parametros.put("expediente",idNegocio);
                                registrarArchivoCena(fileName, item, idNegocio, claveTipo, usuario,item,parametros);
								break;
							case 37:
								//Revisar proceso
								//Gaveta Producto
								//Documentos tipo Seccion - productos
        						parametros.put("expediente",idToSeccion.toString());
                                if(fileName.toUpperCase().endsWith(ConstantesReporte.SUFFIX_PDF)) {
                                	this.registrarDocumentosAnexos(fileName, item, idToSeccion.toString(), claveTipo, usuario ,item,parametros);
                                } else {
                                    throw new Exception("No es PDF");
                                }
								break;
        					case 40: 
        						//Carga Masiva Autos
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case CLAVE_TIPO_LISTA_MODELO_VEHICULO:
        						//Gaveta Producto
        						//Actualizacion de valor factua en Mtto ModeloVehiculo
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        					case 43: 
        						//Carga Masiva Servicio Publico Autos
        						registrarArchivo(fileName, request, claveTipo, item,parametros);
        						break;
        			}					
					
					session.setAttribute(fileId, "-1");	
				}
			}
		}
	}

	/**
     * Metodo para subir archivos con y sin asociacion.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @throws Exception
     */
    @SuppressWarnings({ "deprecation", "unchecked"})
    public void fileUploadAction(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response,Map<String,Object> parametros) throws Exception {
	String claveTipo =null;
	String uploadFolder = obtenerRutaDocumentosAnexos();
	StringBuilder nombreDoc = new StringBuilder();
	boolean isMultipart = FileUpload.isMultipartContent(request);
	if (!isMultipart) {
		System.out.println("Use multipart form to upload a file!");
	} else {
		String fileId = request.getParameter("sessionId").toString().trim();
		FileItemFactory factory = new ProgressMonitorFileItemFactory(request, fileId);
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items = upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					if(item.getFieldName().equals("claveTipo")) {
						claveTipo = item.getString();
					}
					
				} else {
					String fileName = item.getName();
					int i2 = fileName.lastIndexOf("\\");
					if (i2 > -1){
					    fileName = fileName.substring(i2 + 1);
					}
					File dirs = new File(uploadFolder);
					File uploadedFile = new File(dirs, fileName);
					File archivoNuevoNombre;
					HttpSession session = request.getSession();
					//Solo para la accion de AGREGAR... si existe el file en la carpeta de archivos
					//la respalda 					
					if(claveTipo ==null &&	uploadedFile.exists()){
						nombreDoc.delete(0,nombreDoc.length());
					    File archivoAnterior = new File(dirs, fileName);
					    //Si existe se crea un respaldo..
					    Calendar calendario= Calendar.getInstance();
					    calendario.setTime(new Date());
					    StringBuffer agregado=  new StringBuffer();
					    agregado.append("_Resp");
					    agregado.append(calendario.get(Calendar.DAY_OF_MONTH)<=9?"0"+calendario.get(Calendar.DAY_OF_MONTH):calendario.get(Calendar.DAY_OF_MONTH));
					    agregado.append((calendario.get(Calendar.MONTH)+1)<=9?"0"+(calendario.get(Calendar.MONTH)+1):(calendario.get(Calendar.MONTH)+1));
					    agregado.append(calendario.get(Calendar.YEAR));
					    String extension=UtileriasWeb.getExtension(fileName);
					    nombreDoc.append(fileName.substring(0, fileName.lastIndexOf(".")));
					    nombreDoc.append(agregado.toString());
					    archivoNuevoNombre = new File(dirs,nombreDoc.toString()+extension);
					    archivoAnterior.renameTo(archivoNuevoNombre);
		        		}
					//item.write(uploadedFile);
					session.setAttribute(fileId, "-1");
					//Uso para cuando Sea ASOCIAR porque guarda e
					Object archivoIdToSolicitud = session.getAttribute("archivoIdToSolicitud");
					Object archivoIdToCotizacion = session.getAttribute("archivoIdToCotizacion");
					if(archivoIdToSolicitud!=null || 
						session.getAttribute("archivoIdToCotizacion")!= null){
					    Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
        				switch(Integer.parseInt(claveTipo)) {
        					case 0:	
    					    	if(existeLaSolicitud(regresaBigDecimal(archivoIdToSolicitud.toString()))){
    					    	    registrarArchivoSolicitud(fileName, archivoIdToSolicitud.toString(), 
    					    	    		claveTipo, usuario, item);
    					    	}else{
    					    		//Si no existe la solicitud en BD elimina el archivo que se sube
    					    		uploadedFile.delete();
    					    	}
        						break;
        					case 4:
    					    	if(existeLaCotizacion(regresaBigDecimal(archivoIdToCotizacion.toString()))){
    					    	    registrarDocumentoDigitalComplementario(fileName, archivoIdToCotizacion.toString(),
    					    	    		claveTipo, usuario, request, item,parametros);
    					    	}else{
    					    		//Si no existe la cotizacion en la BD elimina el archivo subido
    					    		uploadedFile.delete();
    					    	}
        						break;
        				}
						//elimina variables de la session
						request.getSession(true).removeAttribute("archivoIdToSolicitud");
						request.getSession(true).removeAttribute("archivoIdToCotizacion");
					}else{
					    uploadedFile.delete();
					}
				}
			}
		}
	
	}
    
    /**
     * Metodo para subir archivos sin asociacion.
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @throws Exception
     */
    @SuppressWarnings({ "deprecation", "unchecked","unused"})
    public void fileUploadAsociacionAction(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response,Map<String,Object> parametros) throws Exception {

	String claveTipo = "";
	String idToCotizacion = "";
	String idToInspeccionIncisoCotizacion = "";
	String uploadFolder = obtenerRutaDocumentosAnexos();
	//Variables usadas para los documentos de siniestro(tipo 14)
	String nombreDocumento="";
	String idTipoDocumento="";
	String idReporteSiniestro="";
	String idDocumentoSiniestro="";
	 StringBuilder nombreDoc = new StringBuilder();
	//Variables usadas por complementar cotizacion
	
	String idToControlArchivo = "";
	
		
	boolean isMultipart = FileUpload.isMultipartContent(request);
	if (!isMultipart) {
		System.out.println("Use multipart form to upload a file!");
	} else {
		String fileId = request.getParameter("sessionId").toString().trim();
		FileItemFactory factory = new ProgressMonitorFileItemFactory(request, fileId);
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items = upload.parseRequest(request);
			Iterator<FileItem> iter = items.iterator();
			while (iter.hasNext()) {
				FileItem item = (FileItem) iter.next();
				if (item.isFormField()) {
					if(item.getFieldName().equals("claveTipo")) {
						claveTipo = item.getString();
					}
					
					
				} else {
					String fileName = item.getName();
					int i2 = fileName.lastIndexOf("\\");
					if (i2 > -1){
					    fileName = fileName.substring(i2 + 1);
					}
					File dirs = new File(uploadFolder);
					File uploadedFile = new File(dirs, fileName);
					if(uploadedFile.exists()){
					  nombreDoc.delete(0, nombreDoc.length());
					    File archivoAnterior = new File(dirs, fileName);
					    //Si existe se crea un respaldo..
					    Calendar calendario= Calendar.getInstance();
					    calendario.setTime(new Date());
					    StringBuffer agregado=  new StringBuffer();
					    agregado.append("_Resp");
					    agregado.append(calendario.get(Calendar.DAY_OF_MONTH)<=9?"0"+calendario.get(Calendar.DAY_OF_MONTH):calendario.get(Calendar.DAY_OF_MONTH));
					    agregado.append((calendario.get(Calendar.MONTH)+1)<=9?"0"+(calendario.get(Calendar.MONTH)+1):(calendario.get(Calendar.MONTH)+1));
					    agregado.append(calendario.get(Calendar.YEAR));
					    String extension=UtileriasWeb.getExtension(fileName);
					    nombreDoc.append(fileName.substring(0, fileName.lastIndexOf(".")));
					    nombreDoc.append(agregado.toString());
					    File archivoNuevoNombre = new File(dirs,nombreDoc.toString()+extension);
					    archivoAnterior.renameTo(archivoNuevoNombre);
		        		}
					//item.write(uploadedFile);
					HttpSession session = request.getSession();
					session.setAttribute(fileId, "-1");
					Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
        				
					switch(Integer.parseInt(claveTipo)) {
        					case 0:
        						registrarArchivoSolicitud(fileName, session.getAttribute("archivoIdToSolicitud").toString(), claveTipo, usuario, item);
        						break;
        					case 4:
        						registrarDocumentoDigitalComplementario(fileName,  session.getAttribute("archivoIdToCotizacion").toString(),
        								claveTipo, usuario, request, item,parametros);
        						break;
					}
				}
			}
		}
        	request.getSession().removeAttribute("archivoIdToSolicitud");
        	request.getSession().removeAttribute("archivoIdToCotizacion");
	}
	
	private void registrarArchivo(String fileName,HttpServletRequest request,String claveTipo, FileItem item,Map<String,Object> parametros) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);;
		//Si es carga masiva se guarda en filesystem para su limpieza
		if(claveTipo.equals("40") || claveTipo.equals(String.valueOf(CLAVE_TIPO_LISTA_MODELO_VEHICULO)) || claveTipo.equals("16") || claveTipo.equals("42")){
			guardaArchivoFileSystem(fileName, controlArchivoDTO.getIdToControlArchivo().toString());
		}else if(!claveTipo.equals("43")){
			parametros.put("expediente",controlArchivoDTO.getIdToControlArchivo().toString());
			renombrarArchivo(fileName, controlArchivoDTO, item,parametros);
		}
		request.getSession().setAttribute(ControlArchivoDTO.IDTOCONTROLARCHIVO, controlArchivoDTO.getIdToControlArchivo());
	}

	private void registrarArchivoSlip(String fileName, String idToSlip, String tipoSlip, String claveTipo,
			Object numeroInciso, Usuario usuario, FileItem item) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {		
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
 		renombrarArchivo(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), item);
 		SlipDTO slipDTO = SlipDN.getInstancia().getPorId(new BigDecimal(idToSlip));
		if(tipoSlip.equals("1")){
			// Si no es el Slip Base   
			BigDecimal inciso = null;
			if(numeroInciso!=null){
				inciso = BigDecimal.valueOf(Integer.valueOf(numeroInciso.toString()));
			} 
			
		 	if(slipDTO.getTipoSlip().equals("3")){
				SlipAnexoEquipoContratistaDTO slipAnexoEquipoContratistaDTO = new SlipAnexoEquipoContratistaDTO();
				slipAnexoEquipoContratistaDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
				slipAnexoEquipoContratistaDTO.setSlipDTO(SlipDN.getInstancia().getPorId(regresaBigDecimal(idToSlip)));
				SlipAnexoEquipoContratistaDN.getInstancia().agregar(slipAnexoEquipoContratistaDTO);
			}else if (slipDTO.getTipoSlip().equals("6")){  
				// RC Constructores
				SlipAnexoConstructoresDTO slipAnexoConstructoresDTO = new SlipAnexoConstructoresDTO();
				slipAnexoConstructoresDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
				
				SlipConstructoresDTOId id = new SlipConstructoresDTOId();
				id.setIdToSlip(new BigDecimal(idToSlip));
				id.setNumeroInciso(inciso);
				
				SlipConstructoresDTO slipConstructoresDTO = SlipRCConstructoresDN.getInstancia().getPorId(id);
				
				if(slipConstructoresDTO == null){
					slipConstructoresDTO = new SlipConstructoresDTO();
					slipConstructoresDTO.setId(id);
					slipConstructoresDTO.setSitioTrabajos(" ");
					slipConstructoresDTO.setValorEstimadoObra(0D);
					SlipRCConstructoresDN.getInstancia().agregar(slipConstructoresDTO);
					
				}
				
				slipAnexoConstructoresDTO.setSlipConstructoresDTO(slipConstructoresDTO);				
				SlipAnexoRCConstructoresDN.getInstancia().agregar(slipAnexoConstructoresDTO);

				
	 		}else if (slipDTO.getTipoSlip().equals("7")){
				SlipFuncionarioAnexoDTO slipFuncionarioAnexoDTO = new SlipFuncionarioAnexoDTO();
				slipFuncionarioAnexoDTO.setIdControlArchivo(controlArchivoDTO.getIdToControlArchivo());
				SlipFuncionarioDTO slipFuncionarioDTO = new SlipFuncionarioDTO();
				
				SlipFuncionarioDTOId id = new SlipFuncionarioDTOId();
				id.setIdToSlip(new BigDecimal(idToSlip));
				id.setNumeroInciso(inciso);
				
				slipFuncionarioDTO.setId(id);				

				slipFuncionarioAnexoDTO.setSlipFuncionarioDTO(slipFuncionarioDTO);
				SlipAnexoRCFuncionariosDN.getInstancia().agregar(slipFuncionarioAnexoDTO);
	 		}
		}else{
		  SlipAnexoDTO slipAnexoDTO = new SlipAnexoDTO();
		  slipAnexoDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		  slipAnexoDTO.setSlipDTO(SlipDN.getInstancia().getPorId(regresaBigDecimal(idToSlip)));
	 	  SlipAnexoDN.getInstancia().agregar(slipAnexoDTO);
		}
	}
	
	private void registrarArchivoTipoPoliza(String fileName, String idToTipoPoliza, String claveTipo, 
			Usuario usuario, FileItem item,Map<String,Object> parametros) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		
		renombrarArchivo(fileName, controlArchivoDTO, item,parametros);

		DocumentoAnexoTipoPolizaDTO documentoTipoPolizaDTO = new DocumentoAnexoTipoPolizaDTO();
		documentoTipoPolizaDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoTipoPolizaDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoTipoPolizaDTO.setFechaCreacion(new Date());
		documentoTipoPolizaDTO.setClaveObligatoriedad(Short.valueOf("0"));
		documentoTipoPolizaDTO.setNumeroSecuencia(BigDecimal.ZERO);
		documentoTipoPolizaDTO.setDescripcion(DEFAULT);
		
		documentoTipoPolizaDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		tipoPolizaDTO.setIdToTipoPoliza(regresaBigDecimal(idToTipoPoliza));
		tipoPolizaDTO = TipoPolizaDN.getInstancia().getPorId(tipoPolizaDTO);
		documentoTipoPolizaDTO.setTipoPolizaDTO(tipoPolizaDTO);
		DocumentoAnexoTipoPolizaDN.getInstancia().agregar(documentoTipoPolizaDTO);
	}

	private void registrarArchivoProducto(String fileName, String idToProducto, String claveTipo, 
			Usuario usuario, FileItem item,Map<String,Object> parametros) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		
		renombrarArchivo(fileName, controlArchivoDTO, item,parametros);

		DocumentoAnexoProductoDTO documentoProductoDTO = new DocumentoAnexoProductoDTO();
		documentoProductoDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoProductoDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoProductoDTO.setFechaCreacion(new Date());
		documentoProductoDTO.setClaveObligatoriedad(Short.valueOf("0"));
		documentoProductoDTO.setNumeroSecuencia(BigDecimal.ZERO);
		documentoProductoDTO.setDescripcion(DEFAULT);
		
		documentoProductoDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		
		ProductoDTO productoDTO = new ProductoDTO();
		productoDTO.setIdToProducto(regresaBigDecimal(idToProducto));
		productoDTO = ProductoDN.getInstancia().getPorId(productoDTO);
		documentoProductoDTO.setProductoDTO(productoDTO);
		
		DocumentoAnexoProductoDN.getInstancia().agregar(documentoProductoDTO);
	}

	private void registrarArchivoCg(String fileName, FileItem fileTU, String idToSeccion, String claveTipo, Usuario usuario) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ManejadorFortimaxCgAction manejadorArchivosCg = new ManejadorFortimaxCgAction();
        ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);

		String fortimaxIDFile = manejadorArchivosCg.armarFortimaxIDFile(fileName,controlArchivoDTO.getIdToControlArchivo().toString());
		
		String ext = UtileriasWeb.getExtension(fileName);
		String fmxFileName = fortimaxIDFile + ext; 
		
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		transporteImpresionDTO.setByteArray(fileTU.get());
		manejadorArchivosCg.crearExpedienteFortimax(fortimaxIDFile);
		manejadorArchivosCg.cargarArchivoFortimax(fmxFileName, fortimaxIDFile, transporteImpresionDTO);

		CgCondiciones documentoCgCondiciones = new CgCondiciones();
		documentoCgCondiciones.setNombre(fileName);
		documentoCgCondiciones.setNumeroRegistro(DEFAULT);
		documentoCgCondiciones.setFechaInicio(new Date());
		documentoCgCondiciones.setFechaFin(null);
		documentoCgCondiciones.setActivo(Short.valueOf("0"));
		documentoCgCondiciones.setPortal(Short.valueOf("0"));
        documentoCgCondiciones.setEd(Short.valueOf("0"));
		documentoCgCondiciones.setFechaCreacion(new Date());
		documentoCgCondiciones.setUserId(usuario.getId().toString());
		documentoCgCondiciones.setFechaModificacion(new Date());
		documentoCgCondiciones.setUserModifId(usuario.getId().toString());
		documentoCgCondiciones.setFortimaxDocId(fortimaxIDFile);
		documentoCgCondiciones.setFortimaxDocNombre(fortimaxIDFile);
		documentoCgCondiciones.setFortimaxDocGaveta(manejadorArchivosCg.getGavetaCondicionesg());
		documentoCgCondiciones.setFortimaxDocCarpeta(manejadorArchivosCg.getCarpetaCondicionesg());

		SeccionDTO seccionDTO = new SeccionDTO();
		seccionDTO.setIdToSeccion(regresaBigDecimal(idToSeccion));
		seccionDTO = SeccionDN.getInstancia().getPorId(seccionDTO);

		documentoCgCondiciones.setSeccionDTO(seccionDTO);
		DocumentoAnexoCgDN.getInstancia().agregar(documentoCgCondiciones);
	}
	
	private void registrarArchivoCena(String fileName, FileItem fileTU, String idToNegocio, String claveTipo, 
			Usuario usuario, FileItem item, Map<String,Object> parametros) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ManejadorFortimaxCenaAction manejadorArchivosCena = new ManejadorFortimaxCenaAction();
        ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);

        String fortimaxIDFile = controlArchivoDTO.getIdToControlArchivo().toString();
		
		renombrarArchivo(fileName, controlArchivoDTO, item,parametros);
		
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		transporteImpresionDTO.setByteArray(fileTU.get());
		
		String estructura = manejadorArchivosCena.getGavetaCenaFortimax();
		final String[] data = estructura.split("\\|");
		final String gaveta = data[0];
		final String carpeta = data[1];
		
		NegocioCEAAnexos documentoCENA = new NegocioCEAAnexos();
		documentoCENA.setNombre(fileName);
		documentoCENA.setFechaRegistro(new Date());
		documentoCENA.setUsuarioNombre(usuario.getNombreUsuario().toString());
		documentoCENA.setUsuarioId(usuario.getId().toString());
		documentoCENA.setTexto("");
		documentoCENA.setFortimaxDocId(fortimaxIDFile);
		documentoCENA.setFortimaxDocNombre(fortimaxIDFile);
		documentoCENA.setFortimaxDocGaveta(gaveta);
		documentoCENA.setFortimaxDocCarpeta(carpeta);
		documentoCENA.setIdToNegocio(Long.valueOf(idToNegocio));
		manejadorArchivosCena.guardarCENA(documentoCENA);	
	}

	private void registrarArchivoCobertura(String fileName, String idToCobertura, String claveTipo,
			Usuario usuario, FileItem item,Map<String,Object> parametros) throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		
		renombrarArchivo(fileName, controlArchivoDTO, item,parametros);

		DocumentoAnexoCoberturaDTO documentoCoberturaDTO = new DocumentoAnexoCoberturaDTO();
		documentoCoberturaDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoCoberturaDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoCoberturaDTO.setFechaCreacion(new Date());
		documentoCoberturaDTO.setClaveObligatoriedad(Short.valueOf("0"));
		documentoCoberturaDTO.setNumeroSecuencia(BigDecimal.ZERO);
		documentoCoberturaDTO.setDescripcion(DEFAULT);
		
		documentoCoberturaDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		
		CoberturaDTO coberturaDTO = new CoberturaDTO();
		coberturaDTO.setIdToCobertura(regresaBigDecimal(idToCobertura));
		coberturaDTO = CoberturaDN.getInstancia().getPorId(coberturaDTO);
		documentoCoberturaDTO.setCoberturaDTO(coberturaDTO);
		
		DocumentoAnexoCoberturaDN.getInstancia().agregar(documentoCoberturaDTO);
	}
	
	private void registrarArchivoSolicitud(String fileName, String numeroSolicitud, String claveTipo, 
			Usuario usuario, FileItem item) throws SystemException {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		
		renombrarArchivo(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), item);

		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = new DocumentoDigitalSolicitudDTO();
		documentoDigitalSolicitudDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoDigitalSolicitudDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoDigitalSolicitudDTO.setFechaCreacion(new Date());
		documentoDigitalSolicitudDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
		documentoDigitalSolicitudDTO.setNombreUsuarioModificacion(usuario.getNombre());
		documentoDigitalSolicitudDTO.setFechaModificacion(new Date());
		documentoDigitalSolicitudDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		solicitudDTO.setIdToSolicitud(regresaBigDecimal(numeroSolicitud));
		documentoDigitalSolicitudDTO.setSolicitudDTO(solicitudDTO);
		
		DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
		documentoDigitalSolicitudDN.agregar(documentoDigitalSolicitudDTO);
	}
	
	private DocumentoDigitalRangosDTO registrarArchivoRangos(String fileName, Usuario usuario, String cveNegocio) throws SystemException {
		String ext = "";
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setClaveTipo("0"); 
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		controlArchivoDTO = controlArchivoDN.agregar(controlArchivoDTO);
		
		ext = renombrarArch(fileName, controlArchivoDTO.getIdToControlArchivo().toString());

		DocumentoDigitalRangosDTO documentoDigitalRangosDTO = new DocumentoDigitalRangosDTO();
		documentoDigitalRangosDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoDigitalRangosDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoDigitalRangosDTO.setFechaCreacion(new Date());
		documentoDigitalRangosDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
		documentoDigitalRangosDTO.setNombreUsuarioModificacion(usuario.getNombre());
		documentoDigitalRangosDTO.setFechaModificacion(new Date());
		documentoDigitalRangosDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		documentoDigitalRangosDTO.setCveNegocio(cveNegocio);
		documentoDigitalRangosDTO.setDescripcion("OK");
		documentoDigitalRangosDTO.setExtension(ext);
				
		DocumentoDigitalRangosDN documentoDigitalSolicitudDN = DocumentoDigitalRangosDN.getInstancia();
		documentoDigitalSolicitudDN.agregar(documentoDigitalRangosDTO);
		
		return documentoDigitalRangosDTO;
	}
	
	private DocumentoDigitalEsquemasDTO registrarArchivoEsquemas(String fileName, Usuario usuario) throws SystemException {
		String ext = "";
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setClaveTipo("0"); 
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		controlArchivoDTO = controlArchivoDN.agregar(controlArchivoDTO);
		
		ext = renombrarArch(fileName, controlArchivoDTO.getIdToControlArchivo().toString());

		DocumentoDigitalEsquemasDTO documentoDigitalRangosDTO = new DocumentoDigitalEsquemasDTO();
		documentoDigitalRangosDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoDigitalRangosDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoDigitalRangosDTO.setFechaCreacion(new Date());
		documentoDigitalRangosDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
		documentoDigitalRangosDTO.setNombreUsuarioModificacion(usuario.getNombre());
		documentoDigitalRangosDTO.setFechaModificacion(new Date());
		documentoDigitalRangosDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		documentoDigitalRangosDTO.setDescripcion("OK");
		documentoDigitalRangosDTO.setExtension(ext);
				
		DocumentoDigitalEsquemasDN documentoDigitalSolicitudDN = DocumentoDigitalEsquemasDN.getInstancia();
		documentoDigitalSolicitudDN.agregar(documentoDigitalRangosDTO);
		
		return documentoDigitalRangosDTO;
	}
	
	private void registrarArchivoCad(String fileName, String claveTipo, Usuario usuario) throws SystemException {
		String ext = "";
		CargaMasivaCadAmisExcel<CaducidadesDTO> cargaMasivaEsqExcel = new CargaMasivaCadAmisExcel<CaducidadesDTO>(CaducidadesDTO.class);
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		 
		ext = renombrarArch(fileName, controlArchivoDTO.getIdToControlArchivo().toString());

		if(!cargaMasivaEsqExcel.cargaArchivoEsquemas(controlArchivoDTO.getIdToControlArchivo(),ext)){
			LOG.error("Error Archivo!");
			new ExcepcionDeAccesoADatos("Error!");
		}		
	}
	
	private void registrarArchivoDocumentoSiniestro(String fileName, HttpServletRequest request, Usuario usuario, String idReporteSiniestro,
			String nombreDocumento, String idTipoDocumento, String idDocumentoSiniestro, FileItem item)
			throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		//Se guarda el registro de control del archivo
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setClaveTipo(String.valueOf(ControlArchivoDTO.ARCHIVO_DOCUMENTO_SINIESTRO));
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		controlArchivoDTO = controlArchivoDN.agregar(controlArchivoDTO);
		renombrarArchivo(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), item);
		//Se ponen los datos del documento de siniestro
		DocumentoSiniestroDTO documentoSiniestroDTO = new DocumentoSiniestroDTO();
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		//Se obtiene los datos del reporte
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(regresaBigDecimal(idReporteSiniestro));
		//Si el id del documento no viene vacio entonces se obtiene el documento para actualizar sus datos
		if(!idDocumentoSiniestro.equals("")){
			documentoSiniestroDTO.setIdToDocumentoSiniestro(regresaBigDecimal(idDocumentoSiniestro));
			documentoSiniestroDTO = documentoSiniestroDN.getDocumentoSiniestroPorId(documentoSiniestroDTO);
		}
		documentoSiniestroDTO.setNombreDocumento(nombreDocumento);
		TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO = new TipoDocumentoSiniestroDTO();
		tipoDocumentoSiniestroDTO.setIdTcTipoDocumentoSiniestro(regresaBigDecimal(idTipoDocumento));
		documentoSiniestroDTO.setTipoDocumentoSiniestroDTO(tipoDocumentoSiniestroDTO);
		documentoSiniestroDTO.setIdToControArchivo(controlArchivoDTO.getIdToControlArchivo());
		documentoSiniestroDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		documentoSiniestroDTO.setUrl(fileName);
		documentoSiniestroDTO.setFechaCreacion(new Timestamp(new Date().getTime()));
		documentoSiniestroDTO.setIdTcUsuarioCreacion(BigDecimal.valueOf(usuario.getId()));
		documentoSiniestroDTO.setFechaModificacion(new Timestamp(new Date().getTime()));
		documentoSiniestroDTO.setIdTcUsuarioModificacion(BigDecimal.valueOf(usuario.getId()));
		documentoSiniestroDTO.setRequiereAprobacion(false);
		documentoSiniestroDTO.setAprobado(true);
		//Se guarda el registro en documentos de siniestro
		if(idDocumentoSiniestro.equals("")){
			documentoSiniestroDN.agregar(documentoSiniestroDTO);
		}else{
			documentoSiniestroDN.modificar(documentoSiniestroDTO);
		}
		//Si el documento es una carta de Rechazo entonces se cambia el estatus del reporte para generar su nuevo pendiente
		if(Byte.parseByte(idTipoDocumento) == TipoDocumentoSiniestroDTO.CARTA_RECHAZO){
			ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_APROBAR_CARTA_RECHAZO);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Timestamp(new Date().getTime()));
			reporteSiniestroDTO.setIdTcUsuarioModifica(BigDecimal.valueOf(usuario.getId()));
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
		}		
	}

	private void renombrarArchivo(String fileName, String idToControlArchivo, FileItem item) {
	
		try {
			fileManagerService.uploadFile(fileName, idToControlArchivo, item.get(),true);
			LOG.info("Archivo en FORTIMAX");
		} catch (Exception e) {
			LOG.error("Fallo carga de archivo", Level.FINEST, null);
		}
	}
	
	/**
	 * @param fileName
	 * @param controlArchivoDTO
	 * @param item
	 * 
	 * Metodo para subir a FORTIMAX y registrar su nodo
	 */
	private void renombrarArchivo(String fileName, ControlArchivoDTO controlArchivoDTO, FileItem item, Map<String,Object> parametros) {
		FileManagerResponse result = null;
		try {
			result  = fileManagerService.uploadFileFortimax(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), item.get(),parametros);
			LOG.info("-- uploadHandler() file: [{}] upload result {}"+fileName+result);
			controlArchivoDTO.setCodigoExterno(result.getNodo());
			ControlArchivoDN.getInstancia().actualizar(controlArchivoDTO);
			LOG.info("Nodo del archivo "+result.getNodo());
		} catch (Exception e) {
			LOG.error("Fallo carga de archivo", Level.FINEST, null);
		}	
	}
	
	private void guardaArchivoFileSystem(String fileName, String idToControlArchivo){
		
		String uploadFolder = obtenerRutaDocumentosAnexos();		
		String extension = UtileriasWeb.getExtension(fileName);

		File archivo = new File(uploadFolder + fileName);
		archivo.renameTo(new File(uploadFolder + idToControlArchivo + extension));
	}
	
	private String renombrarArch(String fileName, String idToControlArchivo) {
		String uploadFolder = obtenerRutaDocumentosAnexos();		
		String extension = UtileriasWeb.getExtension(fileName);

		File archivo = new File(uploadFolder + fileName);
		archivo.renameTo(new File(uploadFolder + idToControlArchivo + extension));
		
		return extension;
	}

	public void getInfoHandler(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		HttpSession session = request.getSession();
		
		String transferId = request.getParameter("sessionId").toString().trim();
		String progress = session.getAttribute(transferId).toString();

		LOG.info(progress);
		if(progress.equals("-1")) {
			session.removeAttribute(transferId);
		}
		response.setContentType(ConstantesReporte.TIPO_TXT);
		PrintWriter pw = response.getWriter();
		pw.write(progress);
		pw.flush();
		pw.close();
	}

	public void getIdHandler(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String id = session.getId().toString();
		String transferId = "";
		if(session.getAttribute("numeroSecuencia" + id) == null) {
			session.setAttribute("numeroSecuencia" + id, new Integer(1));
			transferId = "FileUpload.Progress." + id + "-1";
		} else {
			Integer numeroSecuencia = (Integer)session.getAttribute("numeroSecuencia" + id);
			transferId = "FileUpload.Progress." + id + "-" + (numeroSecuencia + 1);
			session.setAttribute("numeroSecuencia" + id, numeroSecuencia + 1);
		}
		session.setAttribute(transferId, "0");

		response.setContentType(ConstantesReporte.TIPO_TXT);
		PrintWriter pw = response.getWriter();
		pw.write(transferId);
		pw.flush();
		pw.close();
	}
	
	public void getFileInformation(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException {
		Object idToControlArchivo = request.getSession().getAttribute(ControlArchivoDTO.IDTOCONTROLARCHIVO);
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
		if (idToControlArchivo != null){
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			try {
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId((BigDecimal)idToControlArchivo);
				buffer.append("1</id><idToControlArchivo><![CDATA["+idToControlArchivo.toString()+"]]></idToControlArchivo><nombreArchivo>" + controlArchivoDTO.getNombreArchivoOriginal()
						+ "</nombreArchivo>");
			} catch (SystemException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		else
			buffer.append("2</id>");
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
		request.getSession().removeAttribute(ControlArchivoDTO.IDTOCONTROLARCHIVO);
	}
	
	private boolean existeLaSolicitud(BigDecimal idToSolicitud){
	    
	    SolicitudDTO solicitudDTO= null;
	    try {
	    	solicitudDTO= SolicitudDN.getInstancia().getPorId(idToSolicitud);
	    } catch (SystemException e) {
			LOG.error(e.getMessage(), e);
	    }
	    
	    return solicitudDTO==null?false:true;
	}
	
	private boolean existeLaCotizacion(BigDecimal idToCotizacion){
	    	CotizacionDTO cotizacionDTO =null;
		try {
		   List<CotizacionDTO> cotizaciones = CotizacionDN.getInstancia("").buscarPorPropiedad(CotizacionDTO.IDTOCOTIZACION, idToCotizacion);
		   if(cotizaciones!=null &&!cotizaciones.isEmpty()){
		       cotizacionDTO= cotizaciones.get(0);
		   }
		} catch (SystemException e) {
			LOG.error(e.getMessage(), e);
		}
		return cotizacionDTO==null ?false:true;
		
	}
	
	
	public void registrarDocumentoDigitalComplementario(String fileName, String idToCotizacion, String claveTipo, 
			Usuario usuario, HttpServletRequest request, FileItem item,Map<String,Object> parametros) throws SystemException {
		if (idToCotizacion != null && !idToCotizacion.equals("")){
			
			ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);//En esta linea se sube el archivo
			
			renombrarArchivo(fileName, controlArchivoDTO, item,parametros);
			
			DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO = new DocumentoDigitalCotizacionDTO();
			documentoDigitalCotizacionDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
			documentoDigitalCotizacionDTO.setControlArchivo(controlArchivoDTO);
			
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = CotizacionDN.getInstancia(usuario.getNombreUsuario()).getPorId(cotizacionDTO);
			
			documentoDigitalCotizacionDTO.setCotizacionDTO(cotizacionDTO);
			documentoDigitalCotizacionDTO.setFechaCreacion(new Date());
			documentoDigitalCotizacionDTO.setIdControlArchivo(controlArchivoDTO.getIdToControlArchivo());
			documentoDigitalCotizacionDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
			
			DocumentoDigitalCotizacionDN.getInstancia().agregar(documentoDigitalCotizacionDTO);
			
			request.getSession().setAttribute(ControlArchivoDTO.IDTOCONTROLARCHIVO, controlArchivoDTO.getIdToControlArchivo());
		}
	}
	
	public void registrarDocumentoAnexoInspeccionIncisoCotizacion(String fileName, String idToInspeccionIncisoCotizacion,String claveTipo,
			Usuario usuario, HttpServletRequest request, FileItem item) throws SystemException {
		if (idToInspeccionIncisoCotizacion != null && !idToInspeccionIncisoCotizacion.equals("")){
		
			ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
			
			renombrarArchivo(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), item);
			
			DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspeccionIncisoCotizacionDTO = 
					new DocumentoAnexoInspeccionIncisoCotizacionDTO(); 
			documentoAnexoInspeccionIncisoCotizacionDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
			documentoAnexoInspeccionIncisoCotizacionDTO.setControlArchivo(controlArchivoDTO);
			documentoAnexoInspeccionIncisoCotizacionDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
			
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion = new InspeccionIncisoCotizacionDTO();
			inspeccionIncisoCotizacion = InspeccionIncisoCotizacionDN.getINSTANCIA().getPorId(new BigDecimal(idToInspeccionIncisoCotizacion));
			
			documentoAnexoInspeccionIncisoCotizacionDTO.setInspeccionIncisoCotizacion(inspeccionIncisoCotizacion);
			
			
			documentoAnexoInspeccionIncisoCotizacionDTO.setFechaCreacion(new Date());
			documentoAnexoInspeccionIncisoCotizacionDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
			
			DocumentoAnexoInspeccionIncisoCotizacionDN.getINSTANCIA().agregar(documentoAnexoInspeccionIncisoCotizacionDTO);
		}
	}

	public static void copyFile(ControlArchivoDTO in, ControlArchivoDTO out) throws SystemException {
		String uploadFolder = obtenerRutaDocumentosAnexos();
		String fileInName = in.getNombreArchivoOriginal();
		String extension = UtileriasWeb.getExtension(fileInName);
		
		File fileIn = new File(uploadFolder + in.getIdToControlArchivo() + extension);
		File fileOut = new File(uploadFolder + out.getIdToControlArchivo().toString() + extension);
		try {
			FileUtils.copyFile(fileIn, fileOut);
		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	private void registrarArchivoPrevencionOperacionesIlicitas(String fileName, HttpServletRequest request, String claveTipo, 
			String idToCotizacion, String idTipoDocumento, String idToControlArchivo) throws SystemException {
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		DocPrevencionOperIlicitasDN docPrevencionOperIlicitasDN = DocPrevencionOperIlicitasDN.getInstancia();
		ControlArchivoDTO controlArchivoDTO = null;
		DocPrevencionOperIlicitasDTO docPrevencionOperIlicitasDTO = null;
		if (!StringUtil.isEmpty(idToControlArchivo)) {
			controlArchivoDTO = controlArchivoDN.getPorId(
					new BigDecimal(idToControlArchivo));
			docPrevencionOperIlicitasDTO = docPrevencionOperIlicitasDN.buscarPorId(
					new BigDecimal(idToControlArchivo));
		}
		
		//No se pudo encontrar en la tabla o es un nuevo registro.
		if (controlArchivoDTO == null || docPrevencionOperIlicitasDTO == null) {
			controlArchivoDTO = new ControlArchivoDTO();
			docPrevencionOperIlicitasDTO = new DocPrevencionOperIlicitasDTO();
		}
		
		controlArchivoDTO.setClaveTipo(claveTipo);
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		docPrevencionOperIlicitasDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));

		//Guardar o actualizar
		if (controlArchivoDTO.getIdToControlArchivo() != null &&
				docPrevencionOperIlicitasDTO.getIdToControlArchivo() != null) {
			controlArchivoDTO = controlArchivoDN.actualizar(controlArchivoDTO);
			docPrevencionOperIlicitasDN.actualizar(docPrevencionOperIlicitasDTO);
		}
		else {
			controlArchivoDTO = controlArchivoDN.agregar(controlArchivoDTO);
			docPrevencionOperIlicitasDTO.setClaveTipoDocumento(Short.parseShort(idTipoDocumento));
			docPrevencionOperIlicitasDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
			docPrevencionOperIlicitasDN.guardar(docPrevencionOperIlicitasDTO);
		}
		//Para continuar con la manera en que se ha estado haciendo (como registrarArchivo())
		request.getSession().setAttribute(ControlArchivoDTO.IDTOCONTROLARCHIVO, 
				controlArchivoDTO.getIdToControlArchivo());
	}
	
	private void registrarDocumentoAnexoReaseguroCotizacion(String fileName, String idToCotizacion, String claveTipo, 
			Usuario usuario, FileItem item)	throws SystemException {
		
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		
		renombrarArchivo(fileName, controlArchivoDTO.getIdToControlArchivo().toString(), item);
		
		DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId
			(regresaBigDecimal(idToCotizacion), controlArchivoDTO.getIdToControlArchivo());
		
		DocumentoAnexoReaseguroCotizacionDTO documentoAnexoReaseguroCotizacionDTO = new DocumentoAnexoReaseguroCotizacionDTO(); 
		
		documentoAnexoReaseguroCotizacionDTO.setId(id);
		documentoAnexoReaseguroCotizacionDTO.setNumeroSecuencia(BigDecimal.ONE);
		documentoAnexoReaseguroCotizacionDTO.setDescripcionDocumentoAnexo(DEFAULT);
		documentoAnexoReaseguroCotizacionDTO.setFechaCreacion(new Date());
		documentoAnexoReaseguroCotizacionDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoAnexoReaseguroCotizacionDTO.setNombreUsuarioCreacion(usuario.getNombreUsuario());
		
		DocumentoAnexoReaseguroCotizacionDN.getInstancia().agregar(documentoAnexoReaseguroCotizacionDTO);
		
	}
	
	private void registrarDocumentoComentario(String fileName, String numeroSolicitud, String claveTipo, Usuario usuario, FileItem item,Map<String,Object> parametros) throws SystemException {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		
		renombrarArchivo(fileName, controlArchivoDTO, item,parametros);

		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = new DocumentoDigitalSolicitudDTO();
		documentoDigitalSolicitudDTO.setCodigoUsuarioCreacion(usuario.getId().toString());
		documentoDigitalSolicitudDTO.setNombreUsuarioCreacion(usuario.getNombre());
		documentoDigitalSolicitudDTO.setFechaCreacion(new Date());
		documentoDigitalSolicitudDTO.setCodigoUsuarioModificacion(usuario.getId().toString());
		documentoDigitalSolicitudDTO.setNombreUsuarioModificacion(usuario.getNombre());
		documentoDigitalSolicitudDTO.setFechaModificacion(new Date());
		documentoDigitalSolicitudDTO.setIdToControlArchivo(controlArchivoDTO.getIdToControlArchivo());
		
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		solicitudDTO.setIdToSolicitud(regresaBigDecimal(numeroSolicitud));
		documentoDigitalSolicitudDTO.setSolicitudDTO(solicitudDTO);
		
		DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
		documentoDigitalSolicitudDN.agregar(documentoDigitalSolicitudDTO);

		ServletActionContext.getContext().getSession().remove(ControlArchivoDTO.IDTOCONTROLARCHIVO);
		ServletActionContext.getContext().getSession().put(ControlArchivoDTO.IDTOCONTROLARCHIVO, 
				documentoDigitalSolicitudDTO.getIdToControlArchivo());	
	}
	
	private void registrarDocumentosAnexos( String fileName, FileItem fileTU, String idToSeccion, String claveTipo, Usuario usuario, FileItem item, Map<String,Object> parametros )  
			throws SystemException, ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ControlArchivoDTO controlArchivoDTO = getControlArchivoDTO(claveTipo, fileName);
		String fortimaxIDFile = controlArchivoDTO.getIdToControlArchivo().toString();
		renombrarArchivo(fileName, controlArchivoDTO, item,parametros);	
		this.guardarDocumentoAnexo(fileName, fortimaxIDFile, usuario, idToSeccion);
	}
	
	private void guardarDocumentoAnexo( String fileName, String fortimaxIDFile, Usuario usuario, String idToSeccion )  throws SystemException{
		DocumentosReferenciasAction manejadorArchivosFortimax = new DocumentosReferenciasAction();
		DocumentoAnexoSeccion documentoAnexoSeccion = new DocumentoAnexoSeccion();
		documentoAnexoSeccion.setClaveObligatoriedad(manejadorArchivosFortimax.CLAVE_OBLIGATORIEDAD);
		documentoAnexoSeccion.setNumeroSecuencia(manejadorArchivosFortimax.NUMERO_SECUENCIA);
		documentoAnexoSeccion.setIdControlArchivo(Long.parseLong(fortimaxIDFile));
		documentoAnexoSeccion.setNombre(fileName);
		documentoAnexoSeccion.setFechaCreacion(new Date());
		documentoAnexoSeccion.setUsuarioId(usuario.getNombreUsuario());
		documentoAnexoSeccion.setDescripcion("");
		documentoAnexoSeccion.setFmxId(fortimaxIDFile);
		documentoAnexoSeccion.setFmxNombre(fortimaxIDFile);
		documentoAnexoSeccion.setFmxGaveta(manejadorArchivosFortimax.getGaveta());
		documentoAnexoSeccion.setFmxCarpeta(manejadorArchivosFortimax.getCarpeta());
		documentoAnexoSeccion.setIdToSeccion(Long.valueOf(idToSeccion));
		manejadorArchivosFortimax.guardarDocumentoAnexoSeccion(documentoAnexoSeccion);		
	}
	
	private ControlArchivoDTO getControlArchivoDTO(String claveTipo,
			String fileName) throws SystemException {
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setClaveTipo(claveTipo);
		controlArchivoDTO.setNombreArchivoOriginal(fileName);
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		return controlArchivoDN.agregar(controlArchivoDTO);
	}
}