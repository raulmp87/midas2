package mx.com.afirme.midas.sistema;

import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author MyEclipse Persistence Tools
 */
public class LogDeMidasWeb {
	
	private static final Logger logger;
	private static int valorlogger;
	
	static {
		logger = Logger.getLogger("MidasWeb");
				try{
				valorlogger=Integer.parseInt(Sistema.FIJA_NIVEL_LOGGER_MIDASWEB);
				}catch ( java.lang.NumberFormatException e)
				{
					valorlogger=Sistema.NIVEL_LOGGER_DEFECTO;
				}
		
		
		switch (valorlogger)
		{
		
		case Sistema.NIVEL_LOGGER_SEVERE:
			logger.setLevel(Level.SEVERE);
			break;
		case Sistema.NIVEL_LOGGER_WARNING:
			logger.setLevel(Level.WARNING);
			break;
		case Sistema.NIVEL_LOGGER_INFO:
			logger.setLevel(Level.INFO);
			break;
		case Sistema.NIVEL_LOGGER_CONFIG:
			logger.setLevel(Level.CONFIG);
			break;
		case Sistema.NIVEL_LOGGER_FINE:
			logger.setLevel(Level.FINE);
			break;
		case Sistema.NIVEL_LOGGER_FINER:
			logger.setLevel(Level.FINER);
			break;
		case Sistema.NIVEL_LOGGER_FINEST:
			logger.setLevel(Level.FINEST);
			break;
		case Sistema.NIVEL_LOGGER_ALL:
			logger.setLevel(Level.ALL);
			break;
		case Sistema.NIVEL_LOGGER_OFF:
			logger.setLevel(Level.OFF);
			break;
		default:
		
			logger.setLevel(Level.ALL);
		
		};
		
		
	}
	
	public static void log(String info, Level level, Throwable ex) {
    	logger.log(level, info, ex);
    }
    
    public static Logger getLogger() {
    	return logger;
    }
    
}
