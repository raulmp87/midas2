package mx.com.afirme.midas.sistema;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class MidasTags extends BodyTagSupport {

	/**
	 * @author Christian Ceballos
	 */
	private static final long serialVersionUID = 5024217887200962424L;
	private String key;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int doStartTag() {
		JspWriter out = pageContext.getOut();
		try {
			out.print("ejemplo de custom tag para midas :p");
		} catch(IOException ioe) {
			System.out.println("Error en ExampleTag: " + ioe);
		}
			return(SKIP_BODY);
	}
	
	
	
	public int doAfterBody() throws JspException {
		super.doAfterBody();
		try {
			BodyContent bc = getBodyContent();
			JspWriter out = bc.getEnclosingWriter();
			String mensaje = "<bean:message key=\"" +getKey()+
			"\"/>";
			System.out.println("Mensaje para prueba de ETIQUETA MIDAS: "+mensaje);
			out.print(mensaje);
		} catch(IOException ioe) {
			System.out.println("Error en ExampleTag: " + ioe);
		}
		return SKIP_BODY;
	}


}
