/**
 * 
 */
package mx.com.afirme.midas.sistema;

import java.util.logging.Level;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;
import mx.com.afirme.midas2.service.emision.ppct.PagoProveedorService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author
 * 
 */
public class MidasSessionListener implements HttpSessionListener {
	private final String ROL_CLIENTE_LIGAS = "Rol_M2_Cliente_Liga_Agente";

	public void sessionCreated(HttpSessionEvent arg0) {
		
		WebApplicationContextUtils
		.getRequiredWebApplicationContext(arg0.getSession().getServletContext())
		.getAutowireCapableBeanFactory().autowireBean(this);
		

	}

	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		HttpSession session = sessionEvent.getSession();
		Usuario usuario = (Usuario) session
				.getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
		if (usuario != null) {
			String userAccessId = usuario.getIdSesionUsuario();
			if (userAccessId != null) {
				try {
					usuarioService.setUsuarioActual(usuario);
					if(!usuarioService.tienePermisoUsuarioActual(ROL_CLIENTE_LIGAS)) {
						UsuarioWSDN.getInstancia().LogOutUsuario(
								usuario.getNombreUsuario(), userAccessId);
						LogDeMidasWeb.log("Se destruye sesión de usuario: " + usuario.getNombreUsuario() , Level.INFO, null);
					} else {
						LogDeMidasWeb.log("No se destruye sesión de usuario: " + usuario.getNombreUsuario() , Level.INFO, null);
					}
				} catch (mx.com.afirme.midas.sistema.SystemException ex) {
					ex.printStackTrace();
				}
			}
		}
		try {
			pagoProveedorService.desasignarRecibosPreeliminares(session.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Autowired
	private PagoProveedorService pagoProveedorService;
	
	@Autowired
	private UsuarioService usuarioService;
	

}
