package mx.com.afirme.midas.sistema.utileriasweb;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;

import org.apache.commons.beanutils.PropertyUtilsBean;

/**
 * 
 * @author jose luis arellano
 */
public class BeanFiller {
	private HashMap<String, String> mapeoAtributos = null;
	@SuppressWarnings("unchecked")
	private Class claseResultado;
	
	public BeanFiller(){
		mapeoAtributos = new HashMap<String, String>();
	}
	
	@SuppressWarnings("unchecked")
	public void estableceMapeoResultados(Class claseResultado, String []atributosClaseResultado, String []atributosClaseOriginal) {
		mapeoAtributos.clear();
		this.claseResultado = claseResultado;
		
		if (atributosClaseResultado.length == atributosClaseOriginal.length) {
			for(int i=0;i<atributosClaseResultado.length;i++){
				mapeoAtributos.put(atributosClaseResultado[i], atributosClaseOriginal[i]);
			}
		}
	}
	
	public Object obtenerResultadoMapeo(Object beanResultado,Object beanEntrada) {
		if(beanEntrada != null && !mapeoAtributos.isEmpty()){
			try {
				if(beanResultado == null){
					beanResultado = claseResultado.newInstance();
				}
			} catch (Exception e) {
				LogDeMidasWeb.log("Ocurrio un error al intentar instanciar un objeto tipo '"+claseResultado+"'.", Level.SEVERE, e);
			}
			PropertyUtilsBean utilsBean = new PropertyUtilsBean();
			for(String nombreAtributo : mapeoAtributos.keySet()){
				Object valor = null;
				try{
					valor = utilsBean.getProperty(beanEntrada, mapeoAtributos.get(nombreAtributo));
				}catch(Exception e){
					LogDeMidasWeb.log("Ocurrio un error al intentar obtener el atributo '"+nombreAtributo+"' del objeto: "+beanEntrada, Level.SEVERE, e);
				}
				if(valor != null){
					try {
						Class claseAtributoResultado = utilsBean.getPropertyType(beanResultado, nombreAtributo);
						Class claseAtributoEntrada = utilsBean.getPropertyType(beanEntrada, mapeoAtributos.get(nombreAtributo));
						utilsBean.setProperty(beanResultado, nombreAtributo,
								obtenerResultadoMapeoAtributo(claseAtributoResultado, claseAtributoEntrada, valor));
					} catch (Exception e) {
						String error = "Ocurrio un error al intentar settear el atributo '"+nombreAtributo+"' al objeto "+beanResultado;
						System.out.println(error);
						LogDeMidasWeb.log(error, Level.SEVERE, e);
					}
				}
				
			}
		}
		return beanResultado;
	}
	
	public void limpiarBean(Object beanResultado){
		if(beanResultado != null && !mapeoAtributos.isEmpty()){
			PropertyUtilsBean utilsBean = new PropertyUtilsBean();
			for(String nombreAtributo : mapeoAtributos.keySet()){
				try{
					utilsBean.setProperty(beanResultado, nombreAtributo,null);
//					metodoGetter = registro.getClass().getMethod(nombreAtributo,sinParametros);
//					valor = metodoGetter.invoke(registro);
				}catch(Exception e){
//					LogDeMidasWeb.log("Ocurri� un error al intentar obtener el atributo '"+nombreAtributo+"' del objeto: "+beanEntrada, Level.SEVERE, e);
//					throw new SystemException("Ocurri� un error al intentar obtener el atributo '"+nombreAtributo+"' del objeto: "+beanEntrada);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private Object obtenerResultadoMapeoAtributo(Class claseAtributoResultado,Class claseAtributoEntrada,Object valorAtributoEntrada) 
				throws InstantiationException, IllegalAccessException, ParseException {
		Object resultadoAtributo = valorAtributoEntrada;
		if(!claseAtributoEntrada.getSimpleName().equals(claseAtributoResultado.getSimpleName()) && valorAtributoEntrada != null){
			String valorEntradaString = getStringFromObject(valorAtributoEntrada);
			if (claseAtributoResultado.getSimpleName().equals("BigDecimal")) {
				resultadoAtributo = new BigDecimal(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Integer")) {
				resultadoAtributo = new Integer(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Short")) {
				resultadoAtributo = new Short(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Long")) {
				resultadoAtributo = new Long(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Float")) {
				resultadoAtributo = new Float(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Double")) {
				resultadoAtributo = new Double(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Date")) {
				resultadoAtributo = getFechaFromString(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("Boolean")) {
				resultadoAtributo = new Boolean(valorEntradaString);
			}
			else if(claseAtributoResultado.getSimpleName().equals("String")) {
				resultadoAtributo = valorEntradaString.toUpperCase();
			}
			else{
				String error = "El tipo de dato no es soportado por el PobladorMidasBean. claseAtributoSalida: "+claseAtributoResultado
						+", claseAtributoEntrada: "+claseAtributoEntrada+", objeto de entrada: "+valorAtributoEntrada;
				LogDeMidasWeb.log(error, Level.SEVERE, null);
			}
		}
		return resultadoAtributo;
	}
	
	public static Date getFechaFromString(String fecha) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.parse(fecha);
	}
	
	public static String getStringFromObject(Object object){
		String result = null;
		if (object != null){
			if(object instanceof Date){
				Date fecha = (Date) object;
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				result = dateFormat.format(fecha);
			}
			else{
				if(object instanceof BigDecimal){
					result = ((BigDecimal)(object)).toString();
				}
				else if(object instanceof Integer){
					result = ((Integer)(object)).toString();
				}
				else if(object instanceof Double){
					result = ((Double)(object)).toString();
				}
				else if(object instanceof Short){
					result = ((Short)object).toString();
				}
				else if(object instanceof Long){
					result = ((Long)(object)).toString();
				} else
					result = object.toString();
			}
		}
		return result;
	}
	
}