package mx.com.afirme.midas.cotizacionsololectura;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.equipoelectronico.SubtipoEquipoElectronicoDTO;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDN;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subgiro.SubGiroDN;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;
import mx.com.afirme.midas.catalogos.tipomaquinaria.SubTipoMaquinariaDTO;
import mx.com.afirme.midas.catalogos.tipomontajemaquina.SubtipoMontajeMaquinaDTO;
import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.cliente.ClienteForm;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.CotizacionSN;
import mx.com.afirme.midas.cotizacion.OrdenTrabajoDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDN;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDN;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDN;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.DatoIncisoCotizacionSN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionForm;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionUtil;
import mx.com.afirme.midas.cotizacion.inciso.SeccionCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionForm;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.InspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDTO;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDN;
import mx.com.afirme.midas.cotizacion.motivorechazocancelacion.CotizacionRechazoCancelacionDTO;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCoberturaCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenIncisoCotizacionForm;
import mx.com.afirme.midas.cotizacion.resumen.ResumenSeccionCotizacionForm;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotForm;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoForm;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionDN;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.persona.PersonaDN;
import mx.com.afirme.midas.persona.PersonaDTO;
import mx.com.afirme.midas.persona.PersonaForm;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.deducible.DeducibleCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.CoberturaExcluidaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.exclusion.ExclusionCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.requerida.CoberturaRequeridaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.coaseguro.CoaseguroRiesgoCoberturaId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.deducible.DeducibleRiesgoCoberturaId;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.gestionPendientes.GestorPendientesDanos;
import mx.com.afirme.midas.sistema.gestionPendientes.Pendiente;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudAction;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CotizacionSoloLecturaAction extends MidasMappingDispatchAction {
		 
	private static final NumberFormat fCuota = new DecimalFormat("##0.0000");    
	private static final NumberFormat fMonto = new DecimalFormat("$#,##0.00");
	
	/**
	 * Method mostrarDatosInciso
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public void mostrarDatosInciso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// Se obtiene la iformacion de los diversos ramos de la ODT/COT
		// Se obtiene a traves de los ramos asociados al tipo de poliza de la
		// cotizacion
		try {
			String modificar = request.getParameter("modificar");
			Boolean soloLectura = request.getParameter("soloLectura").equals("1")? true : false;
			String disabled = soloLectura? "disabled" : "";
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("cotizacionId")));

			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			// se obtienen la Informaci�n de Ramo-Inciso
			/*List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso = cotizacionDN.getDatosRamoInciso(cotizacionDTO);
			datosInciso.addAll(cotizacionDN.getDatosRiesgoInciso(cotizacionDTO));*/

			// se iteran los objetos obtenidos
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<datosinciso><![CDATA[");

			List<ConfiguracionDatoIncisoCotizacionDTO> configuracion = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
			List<RamoTipoPolizaDTO> ramos = cotizacionDN.listarRamosCotizacion(cotizacionDTO);
			List<ConfiguracionDatoIncisoCotizacionDTO> datosInciso = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();

			int total = 0;
			Map<String,String> mapaIDsCascadeoHijos = new HashMap<String,String>();
			for(RamoTipoPolizaDTO ramo : ramos) {
				boolean trAbierto = false;
				datosInciso = cotizacionDN.getDatosRamoInciso(ramo);
				configuracion.addAll(datosInciso);
				if(!datosInciso.isEmpty()) {
					buffer.append("<div class=\"subtituloIzquierdaDiv\">Ramo: " + ramo.getRamoDTO().getDescripcion() + "</div>");
				}
				buffer.append("<table id= \"desplegarDetalle\">");
				for(int i = 0; i < datosInciso.size(); i++) {
					if(i % 2 == 0 && trAbierto == true) {
						buffer.append("</tr>");
						trAbierto = false;
					}
					if(i % 2 == 0 && trAbierto == false) {
						buffer.append("<tr>");
						trAbierto = true;
					}
					ConfiguracionDatoIncisoCotizacionDTO dato = datosInciso.get(i);

					String value = "";
					if(modificar.equals("1")) {
						DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
						id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));
						id.setIdDato(dato.getId().getIdDato());
						id.setClaveDetalle(dato.getId().getClaveDetalle());
						id.setIdTcRamo(dato.getId().getIdTcRamo());
						id.setIdTcSubramo(dato.getId().getIdTcSubramo());
						id.setIdToCobertura(BigDecimal.ZERO);
						id.setIdToRiesgo(dato.getId().getIdToRiesgo());
						id.setIdToSeccion(BigDecimal.ZERO);
						id.setNumeroSubinciso(BigDecimal.ZERO);
						
						DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
						DatoIncisoCotizacionDTO valor = datoIncisoCotizacionSN.getPorId(id);
						if(valor != null) {
							value = valor.getValor();
						}
					}
					
					buffer.append("<th class='normal' width='20%'>" + dato.getDescripcionEtiqueta() + ": </th>");
					Map atributos = null;
					String name = "datos[" + (total++) + "]";
					String id = "id" + total;
					String onblur = "";
					switch (dato.getClaveTipoValidacion()){
					case 1:	//cuota al millar, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 4)";
						break;
					case 2:	//porcentaje, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 2)";
						break;
					case 3:	//importe. validar 16 digitos con 2 decimales 
						onblur="validarDecimal(this.form."+id+", this.value, 16, 2)";
						break;
					case 4:	//factor, valodar 16 d�gitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 16, 4)";
						break;
					case 5:	//DSMGVDF, validar entero de 8 d�gitos
						onblur="validarDecimal(this.form."+id+", this.value, 8, 0)";
						break;
					}
					switch(dato.getClaveTipoControl()) {
					case 1:
						
//						atributos = new HashMap();
//						atributos.put("class", "cajaTexto");
//						atributos.put("size", "");
//						
//						atributos.put("id", name);
//						atributos.put("name", name);
//						buffer.append("<td>" + IncisoCotizacionUtil.obtenerSelectCatalogo(atributos, dato.getDescripcionClaseRemota(), soloLectura, value) + "</td>");
						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectCatalogo(name,dato.getDescripcionClaseRemota(), soloLectura, value,mapaIDsCascadeoHijos) + "</td>");
						break;
					case 2:
						atributos = new HashMap();
						atributos.put("class", "cajaTexto");
						atributos.put("size", "");
						atributos.put("id", name);
						atributos.put("name",name);
						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectValorFijo(atributos, dato.getIdGrupo(), soloLectura, value) + "</td>");
						break;
					case 3:
						buffer.append("<td width='25%'><input type=\"text\" class=\"cajaTexto\" style=\"text-transform: none;\" value='" + value + "' name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'/></td>");
						break;
					case 4:
						buffer.append("<td width='25%'><textarea class=\"cajaTexto\" style=\"text-transform: none;\" name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'>" + value + "</textarea></td>");
						break;
					}
				}
				if(trAbierto == true) {
					buffer.append("</tr>");
				}
				buffer.append("</table>");
			}
			List<BigDecimal> riesgos = new ArrayList<BigDecimal>();
			// FIX JEAS
			if(!modificar.equals("1") && !soloLectura) {
				riesgos = cotizacionDN.listarRiesgosCotizacion(cotizacionDTO);
			} else {
				riesgos = cotizacionDN.obtenerRiesgosContratadosCotizacion(cotizacionDTO);
			}
			for(BigDecimal idToRiesgo : riesgos) {
				boolean trAbierto = false;
				RiesgoDN riesgoDN = RiesgoDN.getInstancia();
				datosInciso = cotizacionDN.getDatosRiesgoInciso(idToRiesgo);
				configuracion.addAll(datosInciso);
				if(!datosInciso.isEmpty()) {
					buffer.append("<div class=\"subtituloIzquierdaDiv\">Riesgo: " + riesgoDN.getPorId(idToRiesgo).getDescripcion() + "</div>");
				}
				buffer.append("<table id= \"desplegarDetalle\">");
				for(int i = 0; i < datosInciso.size(); i++) {
					if(i % 2 == 0 && trAbierto == true) {
						buffer.append("</tr>");
						trAbierto = false;
					}
					if(i % 2 == 0 && trAbierto == false) {
						buffer.append("<tr>");
						trAbierto = true;
					}
					ConfiguracionDatoIncisoCotizacionDTO dato = datosInciso.get(i);

					String value = "";
					if(modificar.equals("1")) {
						DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
						id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));
						id.setIdDato(dato.getId().getIdDato());
						id.setClaveDetalle(dato.getId().getClaveDetalle());
						id.setIdTcRamo(dato.getId().getIdTcRamo());
						id.setIdTcSubramo(dato.getId().getIdTcSubramo());
						id.setIdToCobertura(BigDecimal.ZERO);
						id.setIdToRiesgo(dato.getId().getIdToRiesgo());
						id.setIdToSeccion(BigDecimal.ZERO);
						id.setNumeroSubinciso(BigDecimal.ZERO);
						
						DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
						DatoIncisoCotizacionDTO valor = datoIncisoCotizacionSN.getPorId(id);
						if(valor != null) {
							value = valor.getValor();
						}
					}
					
					buffer.append("<th class='normal' width='20%'>" + dato.getDescripcionEtiqueta() + ": </th>");
					Map atributos = null;
					String name = "datos[" + (total++) + "]";
					String id = "id" + total;
					String onblur = "";
					switch (dato.getClaveTipoValidacion()){
					case 1:	//cuota al millar, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 4)";
						break;
					case 2:	//porcentaje, validar 8 digitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 8, 2)";
						break;
					case 3:	//importe. validar 16 digitos con 2 decimales 
						onblur="validarDecimal(this.form."+id+", this.value, 16, 2)";
						break;
					case 4:	//factor, valodar 16 d�gitos con 4 decimales
						onblur="validarDecimal(this.form."+id+", this.value, 16, 4)";
						break;
					case 5:	//DSMGVDF, validar entero de 8 d�gitos
						onblur="validarDecimal(this.form."+id+", this.value, 8, 0)";
						break;
					}
					switch(dato.getClaveTipoControl()) {
					case 1:
						atributos = new HashMap();
						atributos.put("class", "cajaTexto");
						atributos.put("size", "");
						atributos.put("id", name);
						atributos.put("name", name);
						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectCatalogo(atributos, dato.getDescripcionClaseRemota(), soloLectura, value) + "</td>");
						break;
					case 2:
						atributos = new HashMap();
						atributos.put("class", "cajaTexto");
						atributos.put("size", "");
						atributos.put("id", name);
						atributos.put("name",name);
						buffer.append("<td width='25%'>" + IncisoCotizacionUtil.obtenerSelectValorFijo(atributos, dato.getIdGrupo(), soloLectura, value) + "</td>");
						break;
					case 3:
						buffer.append("<td width='25%'><input type=\"text\" class=\"cajaTexto\" style=\"text-transform: none;\" value='" + value + "' name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'/></td>");
						break;
					case 4:
						buffer.append("<td width='25%'><textarea class=\"cajaTexto\" style=\"text-transform: none;\" name='" + name + "' onblur='" + onblur + "' " + disabled + " id='" + id + "'>" + value + "</textarea></td>");
						break;
					}
				}
				if(trAbierto == true) {
					buffer.append("</tr>");
				}
				buffer.append("</table>");
			}

			buffer.append("]]></datosinciso>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
			
			HttpSession session = request.getSession();
			session.removeAttribute("configuracion");
			session.setAttribute("configuracion", configuracion);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	private static final String ROL_AUTORIZADOR = UtileriasWeb
	.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
			"midas.usuario.rol.autorizador");
	
	
	public ActionForward mostrarRiesgosPorCobertura(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws SystemException {
	String reglaNavegacion = Sistema.EXITOSO;
	BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCotizacion"));
	BigDecimal idToCobertura = UtileriasWeb.regresaBigDecimal(request.getParameter("idToCobertura"));
	CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
	CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
	
	Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
	for (Rol rol : usuario.getRoles()) {
		if (rol.getDescripcion().equalsIgnoreCase(ROL_AUTORIZADOR)){
			reglaNavegacion = Sistema.ALTERNO;
		}
	}

	CoberturaDN coberturaDN = CoberturaDN.getInstancia();
	CoberturaDTO coberturaDTO = coberturaDN.getPorId(idToCobertura);
	request.setAttribute("nombreCobertura", coberturaDTO.getNombreComercial());
	request.setAttribute("idCotizacion", idToCotizacion.intValue());
	request.setAttribute("fecha", cotizacionDTO.getFechaCreacion());
	return mapping.findForward(reglaNavegacion);
}

/**
 * Method listarRiesgos
 * 
 * @param mapping
 * @param form
 * @param request
 * @param response
 * @return ActionForward
 * @throws IOException 
 */
public void listarRiesgos(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) throws IOException {
	String idToCotizacion = request.getParameter("idToCotizacion");
	String idToSeccion = request.getParameter("idToSeccion");
	String numeroInciso = request.getParameter("numeroInciso");
	String idToCobertura = request.getParameter("idToCobertura");

	try {
		RiesgoCotizacionId id = new RiesgoCotizacionId();
		id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
		id.setIdToSeccion(UtileriasWeb.regresaBigDecimal(idToSeccion));
		id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(numeroInciso));
		id.setIdToCobertura(UtileriasWeb.regresaBigDecimal(idToCobertura));
		RiesgoCotizacionDTO riesgoCotizacionDTO = new RiesgoCotizacionDTO();
		riesgoCotizacionDTO.setId(id);
		RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
		List<RiesgoCotizacionDTO> riesgos = riesgoCotizacionDN.listarFiltrado(riesgoCotizacionDTO);
		MidasJsonBase json = new MidasJsonBase();
		for(RiesgoCotizacionDTO riesgo : riesgos) {
			Short tieneSubIncisos = riesgo.getCoberturaCotizacionDTO().getSeccionCotizacionDTO().getSeccionDTO().getClaveSubIncisos();
			ComisionCotizacionDN comisionCotizacionDN = ComisionCotizacionDN.getInstancia();
			ComisionCotizacionId idComision = new ComisionCotizacionId();
			idComision.setIdToCotizacion(riesgo.getId().getIdToCotizacion());
			idComision.setIdTcSubramo(riesgo.getIdTcSubramo());
			idComision.setTipoPorcentajeComision((short)1);
			ComisionCotizacionDTO comision = comisionCotizacionDN.getPorId(idComision );
			String iconoCoaseguro = "";
			String tooltipCoaseguro = "";
			switch(riesgo.getClaveAutCoaseguro()) {
			case Sistema.AUTORIZACION_NO_REQUERIDA:
				iconoCoaseguro = Sistema.ICONO_BLANCO;
				tooltipCoaseguro = "No requiere autorizaci�n";
				break;
			case Sistema.AUTORIZACION_REQUERIDA:
				iconoCoaseguro = Sistema.ICONO_AMARILLO;
				tooltipCoaseguro = "Autorizaci�n solicitada";
				break;
			case Sistema.AUTORIZADA:
				iconoCoaseguro = Sistema.ICONO_VERDE;
				tooltipCoaseguro = "Autorizado";
				break;
			case Sistema.RECHAZADA:
				iconoCoaseguro = Sistema.ICONO_ROJO;
				tooltipCoaseguro = "Rechazado";
				break;
			}
			String iconoDeducible = "";
			String tooltipDeducible = "";
			switch(riesgo.getClaveAutDeducible()) {
			case Sistema.AUTORIZACION_NO_REQUERIDA:
				iconoDeducible = Sistema.ICONO_BLANCO;
				tooltipDeducible = "No requiere autorizaci�n";
				break;
			case Sistema.AUTORIZACION_REQUERIDA:
				iconoDeducible = Sistema.ICONO_AMARILLO;
				tooltipDeducible = "Autorizaci�n solicitada";
				break;
			case Sistema.AUTORIZADA:
				iconoDeducible = Sistema.ICONO_VERDE;
				tooltipDeducible = "Autorizado";
				break;
			case Sistema.RECHAZADA:
				iconoDeducible = Sistema.ICONO_ROJO;
				tooltipDeducible = "Rechazado";
				break;
			}
			Double valorCoaseguro = 0D;
			Double valorDeducible = 0D;
			if(riesgo.getClaveContrato().shortValue() != 1) {
				CoaseguroRiesgoCoberturaDN coaseguroRiesgoCoberturaDN = CoaseguroRiesgoCoberturaDN.getInstancia();
				CoaseguroRiesgoCoberturaDTO coaseguroRiesgoCoberturaDTO = new CoaseguroRiesgoCoberturaDTO();
				CoaseguroRiesgoCoberturaId idCoaseguro = new CoaseguroRiesgoCoberturaId();
				idCoaseguro.setIdToCobertura(riesgo.getId().getIdToCobertura());
				idCoaseguro.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
				coaseguroRiesgoCoberturaDTO.setId(idCoaseguro );
				List<CoaseguroRiesgoCoberturaDTO> coaseguros = coaseguroRiesgoCoberturaDN.listarFiltrado(coaseguroRiesgoCoberturaDTO);
				for(CoaseguroRiesgoCoberturaDTO coaseguro : coaseguros) {
					if(coaseguro.getClaveDefault().shortValue() == 1) {
						valorCoaseguro = coaseguro.getValor();
					}
				}
				DeducibleRiesgoCoberturaDN deducibleRiesgoCoberturaDN = DeducibleRiesgoCoberturaDN.getInstancia();
				DeducibleRiesgoCoberturaDTO deducibleRiesgoCoberturaDTO = new DeducibleRiesgoCoberturaDTO();
				DeducibleRiesgoCoberturaId idDeducible = new DeducibleRiesgoCoberturaId();
				idDeducible.setIdToCobertura(riesgo.getId().getIdToCobertura());
				idDeducible.setIdToRiesgo(riesgo.getId().getIdToRiesgo());
				deducibleRiesgoCoberturaDTO.setId(idDeducible );
				List<DeducibleRiesgoCoberturaDTO> deducibles = deducibleRiesgoCoberturaDN.listarFiltrado(deducibleRiesgoCoberturaDTO );
				for(DeducibleRiesgoCoberturaDTO deducible : deducibles) {
					if(deducible.getClaveDefault().shortValue() == 1) {
						valorDeducible = deducible.getValor();
					}
				}
			}
			
			MidasJsonRow row = new MidasJsonRow();
			row.setId(riesgo.getId().getIdToCobertura() + "_" + riesgo.getId().getIdToRiesgo());	
			//idToCotizacion,numeroInciso,idToSeccion,idToCobertura,idToRiesgo,idTcSubRamo,claveObligatoriedad,claveTipoSumaAsegurada,"
			//"Riesgo,Contratado,Suma Asegurada,Cuota,Prima Neta,Comision,Coaseguro,,Deducible,
			Double cuotaRiesgo = riesgo.getValorSumaAsegurada() == 0? 0D : riesgo.getValorPrimaNeta() / riesgo.getValorSumaAsegurada() * 1000;
			row.setDatos(riesgo.getId().getIdToCotizacion().toString(),
					riesgo.getId().getNumeroInciso().toString(),
					riesgo.getId().getIdToSeccion().toString(),
					riesgo.getId().getIdToCobertura().toString(),
					riesgo.getId().getIdToRiesgo().toString(),
					riesgo.getIdTcSubramo().toString(),
					riesgo.getRiesgoCoberturaDTO().getClaveObligatoriedad().toString(),
					tieneSubIncisos.shortValue() == 0? riesgo.getRiesgoCoberturaDTO().getClaveTipoSumaAsegurada().toString() : "4",
					riesgo.getRiesgoCoberturaDTO().getDescripcionRiesgoCobertura(),
					riesgo.getClaveContrato().toString(),
					fMonto.format(riesgo.getValorSumaAsegurada()),
					riesgo.getClaveContrato().shortValue() == 1? fCuota.format(cuotaRiesgo) : "",
					riesgo.getClaveContrato().shortValue() == 1? fMonto.format(riesgo.getValorPrimaNeta()): "", 
					riesgo.getClaveContrato().shortValue() == 1? ((comision != null)?comision.getValorComisionCotizacion().toString() : "" ): "", //TODO pendiente comision
					valorCoaseguro > 0? valorCoaseguro.toString() : riesgo.getPorcentajeCoaseguro().toString(),
					MidasJsonRow.generarLineaImagenDataGrid(iconoCoaseguro, tooltipCoaseguro, "void(0);", null),
					valorDeducible > 0? valorDeducible.toString() : riesgo.getPorcentajeDeducible().toString(),
					MidasJsonRow.generarLineaImagenDataGrid(iconoDeducible, tooltipDeducible, "void(0);", null));
			json.addRow(row);
		}
		response.setContentType("text/json");
		System.out.println(json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	} catch (SystemException e) {
		e.printStackTrace();
	}
}
	
	/**
	 * Method mostrarARD
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarARD(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String idToCotizacionS = request.getParameter("idToCotizacion");
		String numeroIncisoS = request.getParameter("numeroInciso");
		String idToSeccionS = request.getParameter("idToSeccion");
		String idToCoberturaS = request.getParameter("idToCobertura");
		String fecha = new String();

		BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
		BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
		BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
		BigDecimal idToCobertura = new BigDecimal(idToCoberturaS);
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		
		try {
			CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(
				idToCotizacion);
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(
				idToCobertura);
			List<AumentoRiesgoCotizacionDTO> aumentos = CoberturaCotizacionDN
				.getInstancia().getAumentosPorCobertura(idToCotizacion,
					numeroInciso, idToSeccion, idToCobertura, nombreUsuario);
			List<DescuentoRiesgoCotizacionDTO> descuentos = CoberturaCotizacionDN
				.getInstancia().getDescuentosPorCobertura(idToCotizacion,
					numeroInciso, idToSeccion, idToCobertura, nombreUsuario);
			List<RecargoRiesgoCotizacionDTO> recargos = CoberturaCotizacionDN
				.getInstancia().getRecargosPorCobertura(idToCotizacion,
					numeroInciso, idToSeccion, idToCobertura, nombreUsuario);
			fecha = dateFormat.format(cotizacionDTO.getFechaCreacion());
			request.setAttribute("tituloCoberturas", coberturaDTO
					.getNombreComercial());
			request.setAttribute("idToCotizacionCadena", UtileriasWeb
					.llenarIzquierda(idToCotizacion.toString(), "0", 8));
			request.setAttribute("fecha", fecha);
			request.setAttribute("aumentos", aumentos);
			request.setAttribute("descuentos", descuentos);
			request.setAttribute("recargos", recargos);
			request.setAttribute("idToCotizacion", idToCotizacion);
			request.setAttribute("numeroInciso", numeroInciso);
			request.setAttribute("idToSeccion", idToSeccion);
			request.setAttribute("idToCobertura", idToCobertura);
			request.setAttribute("aumentosSize", aumentos.size());
			request.setAttribute("descuentosSize", descuentos.size());
			request.setAttribute("recargosSize", recargos.size());

		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	
	
	public ActionForward mostrarCoberturasPorSeccion(ActionMapping mapping,
		ActionForm form, HttpServletRequest request,
		HttpServletResponse response) {
	String reglaNavegacion = Sistema.EXITOSO;
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	String idToCotizacionS = request.getParameter("idToCotizacion");
	String numeroIncisoS = request.getParameter("numeroInciso");
	String idToSeccionS = request.getParameter("idToSeccion");
	String fecha = new String();
	SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
	SeccionDTO seccionDTO = new SeccionDTO();
	CotizacionDTO cotizacionDTO = new CotizacionDTO();
	CotizacionForm cotizacionForm = (CotizacionForm) form;

	SeccionDN seccionDN = SeccionDN.getInstancia();
	CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
	if (numeroIncisoS.trim().length() < 1
			|| idToSeccionS.trim().length() < 1) {
		return mapping.findForward(reglaNavegacion);
	}
	BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
	BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
	BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
	try {
		seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
		seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
		seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
		seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);
		seccionDTO.setIdToSeccion(idToSeccion);
		seccionDTO = seccionDN.getPorId(seccionDTO);
		cotizacionDTO.setIdToCotizacion(idToCotizacion);
		cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
		fecha = dateFormat.format(cotizacionDTO.getFechaCreacion());
		request.setAttribute("tituloCoberturas", seccionDTO
				.getNombreComercial());
		request.setAttribute("idToCotizacion", UtileriasWeb
				.llenarIzquierda(cotizacionDTO.getIdToCotizacion()
						.toString(), "0", 8));
		request.setAttribute("fecha", fecha);
		cotizacionForm.setFechaCreacion(fecha);

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		for (Rol rol : usuario.getRoles()) {
			if (rol.getDescripcion().equalsIgnoreCase(ROL_AUTORIZADOR)) {
				reglaNavegacion = Sistema.ALTERNO;
			}
		}

	} catch (SystemException e) {
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
				request);
	}

	return mapping.findForward(reglaNavegacion);
}

/**
 * Method mostrarCoberturas
 * 
 * @param mapping
 * @param form
 * @param request
 * @param response
 * @return ActionForward
 */
public void mostrarCoberturas(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
	List<CoberturaDTO> listaCoberturas = new ArrayList<CoberturaDTO>();
	String idToCotizacionS = request.getParameter("idToCotizacion");
	String numeroIncisoS = request.getParameter("numeroInciso");
	String idToSeccionS = request.getParameter("idToSeccion");
	SeccionCotizacionDTO seccionCotizacionDTO = new SeccionCotizacionDTO();
	CoberturaDTO coberturaDTO = new CoberturaDTO();
	CoberturaDN coberturaDN = CoberturaDN.getInstancia();
	CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));

	if (numeroIncisoS.trim().length() < 1
			|| idToSeccionS.trim().length() < 1) {
		return;
	}
	BigDecimal idToCotizacion = new BigDecimal(idToCotizacionS);
	BigDecimal numeroInciso = new BigDecimal(numeroIncisoS);
	BigDecimal idToSeccion = new BigDecimal(idToSeccionS);
	List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();

	String json = new String();
	try {
		seccionCotizacionDTO.setId(new SeccionCotizacionDTOId());
		seccionCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
		seccionCotizacionDTO.getId().setNumeroInciso(numeroInciso);
		seccionCotizacionDTO.getId().setIdToSeccion(idToSeccion);

		coberturas = cotizacionDN
				.getCoberturasPorSeccion(seccionCotizacionDTO);
		if (coberturas != null) {
			for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas) {
				coberturaDTO = new CoberturaDTO();
				coberturaDTO.setIdToCobertura(coberturaCotizacionDTO
						.getId().getIdToCobertura());
				coberturaDTO = coberturaDN.getPorId(coberturaDTO);
				listaCoberturas.add(coberturaDTO);
			}
		}
		json = getJsonTablaCobertura(coberturas, listaCoberturas);

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json);
		pw.flush();
		pw.close();
	} catch (SystemException e) {
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
				request);
	} catch (IOException e) {
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
				request);
	}
}

private String getJsonTablaCobertura(
		List<CoberturaCotizacionDTO> coberturas,
		List<CoberturaDTO> listaCoberturas) throws SystemException {
	String listaCoberturaExcluidas = new String();
	String listaCoberturaRequeridas = new String();
	String iconoCoaseguro = new String();
	String iconoDeducible = new String();
	Double totalPrimas = Double.valueOf("0");
	String desglosaRiesgo = new String();
	String sumaAsegurada = new String();
	StringBuilder sendRequest = new StringBuilder("");

	/*
	 * Restricciones La suma asegurada solo es editable si: 1.- La cobertura
	 * NO desglosa Riesgos 2.- La seccion ala que pertenece la cobertura no
	 * acepta subincisos
	 */
	try {
		ExclusionCoberturaDN exclusionCoberturaDN = ExclusionCoberturaDN
				.getInstancia();
		CoberturaRequeridaDN coberturaRequeridaDN = CoberturaRequeridaDN
				.getInstancia();
		List<CoberturaExcluidaDTO> excluidas = new ArrayList<CoberturaExcluidaDTO>();
		List<CoberturaRequeridaDTO> requeridas = new ArrayList<CoberturaRequeridaDTO>();
		CoberturaDTO coberturaDTO = new CoberturaDTO();

		MidasJsonBase json = new MidasJsonBase();
		int index = 0;
		if (coberturas != null && listaCoberturas.size() > 0) {
			for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturas) {
				MidasJsonRow row = new MidasJsonRow();

				listaCoberturaExcluidas = "";
				listaCoberturaRequeridas = "";
				coberturaDTO = (CoberturaDTO) listaCoberturas.get(index);

				requeridas = coberturaRequeridaDN.buscarPorCoberturaBase(coberturaDTO.getIdToCobertura());

				listaCoberturaRequeridas = this.getCoberturasRequeridasCadena(requeridas);

				excluidas = exclusionCoberturaDN.buscarPorCoberturaBase(coberturaDTO.getIdToCobertura());

				listaCoberturaExcluidas = this
						.getCoberturasExcluidasCadena(excluidas);

				if ((coberturaDTO.getClaveDesglosaRiesgos().toString()
						.equalsIgnoreCase("0") || coberturaDTO
						.getClaveDesglosaRiesgos().toString()
						.equalsIgnoreCase("n")))
					desglosaRiesgo = "0";
				else
					desglosaRiesgo = "1";

				// Suma Asegurada
				switch (Integer.valueOf(
						coberturaDTO.getClaveTipoSumaAsegurada())
						.intValue()) {
				case 1:
					sumaAsegurada = fMonto.format(coberturaCotizacionDTO
							.getValorSumaAsegurada());
					break;
				case 2:
					sumaAsegurada = "Amparada";
					break;
				case 3:
					sumaAsegurada = fMonto.format(coberturaCotizacionDTO
						.getValorSumaAsegurada());;
					break;
				}

				// semaforo de coaseguro icono
				String altCoaseguro = "";
				String altDeducible = "";
				switch (coberturaCotizacionDTO.getClaveAutCoaseguro()) {
				case Sistema.AUTORIZACION_NO_REQUERIDA:
					iconoCoaseguro = Sistema.ICONO_BLANCO;
					break;
				case Sistema.AUTORIZACION_REQUERIDA:
					iconoCoaseguro = Sistema.ICONO_AMARILLO;
					altCoaseguro = "Requiere Autorizaci�n";
					break;
				case Sistema.AUTORIZADA:
					iconoCoaseguro = Sistema.ICONO_VERDE;
					altCoaseguro = "Coaseguro Autorizado";
					break;
				case Sistema.RECHAZADA:
					altCoaseguro = "Coaseguro Rechazado";
					iconoCoaseguro = Sistema.ICONO_ROJO;
					break;

				}
				// semaforo de deducible icono
				switch (coberturaCotizacionDTO.getClaveAutDeducible()) {
				case Sistema.AUTORIZACION_NO_REQUERIDA:
					iconoDeducible = Sistema.ICONO_BLANCO;
					break;
				case Sistema.AUTORIZACION_REQUERIDA:
					iconoDeducible = Sistema.ICONO_AMARILLO;
					altDeducible = "Requiere Autorizaci�n";
					break;
				case Sistema.AUTORIZADA:
					iconoDeducible = Sistema.ICONO_VERDE;
					altDeducible = "Deducible Autorizado";
					break;
				case Sistema.RECHAZADA:
					iconoDeducible = Sistema.ICONO_ROJO;
					altDeducible = "Deducible Autorizaci�n";
					break;

				}

				Double valorCoaseguro = 0D;
				Double valorDeducible = 0D;
				if(coberturaCotizacionDTO.getClaveContrato().shortValue() != 1) {
					CoaseguroCoberturaDN coaseguroCoberturaDN = CoaseguroCoberturaDN.getInstancia();
					List<CoaseguroCoberturaDTO> coaseguros = coaseguroCoberturaDN.listarCoaseguros(coberturaCotizacionDTO.getId().getIdToCobertura());
					for(CoaseguroCoberturaDTO coaseguro : coaseguros) {
						if(coaseguro.getClaveDefault().shortValue() == 1) {
							valorCoaseguro = coaseguro.getValor();
						}
					}
					DeducibleCoberturaDN deducibleCoberturaDN = DeducibleCoberturaDN.getInstancia();
					List<DeducibleCoberturaDTO> deducibles = deducibleCoberturaDN.listarDeducibles(coberturaCotizacionDTO.getId().getIdToCobertura());
					for(DeducibleCoberturaDTO deducible : deducibles) {
						if(deducible.getClaveDefault().shortValue() == 1) {
							valorDeducible = deducible.getValor();
						}
					}
				}

				totalPrimas = totalPrimas
						+ coberturaCotizacionDTO.getValorPrimaNeta();

				sendRequest.delete(0, sendRequest.length());
				sendRequest.append("sendRequest(null,'/MidasWeb/cotizacion/cobertura/mostrarARD.do?idToCotizacion=");
				sendRequest.append(coberturaCotizacionDTO.getId()
						.getIdToCotizacion().toString());
				sendRequest.append("&numeroInciso=").append(coberturaCotizacionDTO.getId().getNumeroInciso()
								.toString());
				sendRequest.append("&idToSeccion=").append(coberturaCotizacionDTO.getId().getIdToSeccion())
								.toString();
				sendRequest.append("&idToCobertura=").append(
						coberturaCotizacionDTO.getId().getIdToCobertura()
								.toString());
				sendRequest.append("','configuracion_detalle',null);");

				row.setId(coberturaCotizacionDTO.getId().getIdToSeccion()
						.toString()
						+ "_"
						+ coberturaCotizacionDTO.getId().getIdToCobertura()
								.toString());
				row.setDatos(
						coberturaCotizacionDTO.getId().getIdToCotizacion()
								.toString(),
						coberturaCotizacionDTO.getId().getNumeroInciso()
								.toString(),
						coberturaCotizacionDTO.getId().getIdToSeccion()
								.toString(),
						coberturaCotizacionDTO.getId().getIdToCobertura()
								.toString(),
						coberturaCotizacionDTO.getClaveObligatoriedad()
								.toString(),
						coberturaDTO.getClaveTipoSumaAsegurada(),
						listaCoberturaRequeridas,
						listaCoberturaExcluidas,
						coberturaDTO.getNombreComercial(),
						coberturaCotizacionDTO.getClaveContrato()
								.toString(),
						sumaAsegurada,// suma asegurada
						fCuota.format(coberturaCotizacionDTO
								.getValorCuota() * 1000D),// cuota
						fMonto.format(coberturaCotizacionDTO
								.getValorPrimaNeta()),// prima neta
						"0",// comision
						desglosaRiesgo.equals("1") ? "Segun Riesgo"
								: valorCoaseguro > 0 ? valorCoaseguro.toString()
										: coberturaCotizacionDTO
												.getPorcentajeCoaseguro()
												.toString(),// coaseguro
						MidasJsonRow
								.generarLineaImagenDataGrid(iconoCoaseguro,
										altCoaseguro, "void(0)", null),// Semaforo
						// de
						// autorizacion
						// coaseguro
						desglosaRiesgo.equals("1") ? "Segun Riesgo"
								: valorDeducible > 0 ? valorDeducible.toString()
										: coberturaCotizacionDTO
												.getPorcentajeDeducible()
												.toString(),// Deducible
						MidasJsonRow
								.generarLineaImagenDataGrid(iconoDeducible,
										altDeducible, "void(0)", null),// Semaforo
						// de
						// autorizacion
						// deducible
						coberturaCotizacionDTO.getClaveContrato()
								.intValue() == 1 ? MidasJsonRow
								.generarLineaImagenDataGrid(
										Sistema.ICONO_DETALLE,
										"Aumentos/Recargos/Descuentos",
										sendRequest.toString(), null) : "",
						desglosaRiesgo, coberturaCotizacionDTO
								.getSeccionCotizacionDTO().getSeccionDTO()
								.getClaveSubIncisos().toString());
				json.addRow(row);
				index++;
			}
		}
		return json.toString();
	} catch (SystemException e) {
		throw e;
	}
}
	
	public ActionForward mostrarModificarSubInciso(ActionMapping mapping,
		    ActionForm form, HttpServletRequest request,
		    HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("idSubInciso");
		String ids[] = id.split(",");
		BigDecimal idToCotizacion = new BigDecimal(ids[0]);
		BigDecimal numeroInciso = new BigDecimal(ids[1]);
		BigDecimal idToSeccion = new BigDecimal(ids[2]);
		BigDecimal numeroSubInciso = new BigDecimal(ids[3]);

		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(
			idToCotizacion, numeroInciso, idToSeccion, numeroSubInciso);
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN
			.getInstancia();
		try {
		    SubIncisoCotizacionDTO subIncisoCotizacionDTO = incisoCotizacionDN
			    .getSubIncisoCotizacionPorId(subIncisoCotizacionDTOId);
		    // SubIncisoDTO subIncisoDTO =
		    // incisoCotizacionDN.getSubIncisoPorId(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
		    // this.poblarForm(subIncisoCotizacionDTO, subIncisoDTO,
		    // subIncisoForm);

		    CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
			    .obtieneNombreUsuario(request));
		    CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			    "dd/MM/yyyy");

		    subIncisoForm.set("fechaCreacion", simpleDateFormat
			    .format(cotizacionDTO.getFechaCreacion()));
		    subIncisoForm.set("idToCotizacion", idToCotizacion.toBigInteger()
			    .toString());
		    subIncisoForm.set("idToSeccion", idToSeccion.toBigInteger()
			    .toString());
		    subIncisoForm.set("numeroInciso", numeroInciso.toBigInteger()
			    .toString());
		    subIncisoForm.set("numeroSubInciso", subIncisoCotizacionDTO.getId()
			    .getNumeroSubInciso().toString());
		    subIncisoForm.set("subInciso", subIncisoCotizacionDTO
			    .getDescripcionSubInciso());
		    subIncisoForm.set("sumaAsegurada", subIncisoCotizacionDTO
			    .getValorSumaAsegurada().toString());

		    SeccionDN seccionDN = SeccionDN.getInstancia();
		    SeccionDTO seccionDTO = new SeccionDTO();
		    seccionDTO.setIdToSeccion(idToSeccion);
		    seccionDTO = seccionDN.getPorId(seccionDTO);
		    subIncisoForm.set("seccionNombreComercial", seccionDTO
			    .getNombreComercial());

		    request.setAttribute("idToCotizacion", idToCotizacion.intValue());
		} catch (ExcepcionDeAccesoADatos e) {
		    reglaNavegacion = Sistema.NO_DISPONIBLE;
		    UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
			    request);
		} catch (SystemException e) {
		    reglaNavegacion = Sistema.NO_EXITOSO;
		    UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
			    request);
		}
		return mapping.findForward(reglaNavegacion);
	    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	  @SuppressWarnings("unchecked")
	public void mostrarDatosSubInciso(ActionMapping mapping, ActionForm form,
		    HttpServletRequest request, HttpServletResponse response) {
		try {
		    String modificar = request.getParameter("modificar");
		    Boolean soloLectura = request.getParameter("soloLectura").equals(
			    "1") ? true : false;
		    String disabled = soloLectura ? "disabled" : "";

		    BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(request
			    .getParameter("idToCotizacion"));
		    BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(request
			    .getParameter("numeroInciso"));
		    BigDecimal idToSeccion = UtileriasWeb.regresaBigDecimal(request
			    .getParameter("idToSeccion"));

		    CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
			    .obtieneNombreUsuario(request));

		    // List<ConfiguracionDatoIncisoCotizacionDTO> datosSubInciso =
		    // cotizacionDN.getDatosRiesgoSubInciso(idToCotizacion,
		    // numeroInciso, idToSeccion);
		    // se iteran los objetos obtenidos
		    StringBuffer buffer = new StringBuffer();
		    buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		    buffer.append("<response>");
		    buffer.append("<datossubinciso><![CDATA[");

		    List<ConfiguracionDatoIncisoCotizacionDTO> configuracion = new ArrayList<ConfiguracionDatoIncisoCotizacionDTO>();
		    List<BigDecimal> riesgos = cotizacionDN
			    .listarRiesgosPorSeccionCotizacion(idToCotizacion,
				    numeroInciso, idToSeccion);
		    int total = 0;

		    Map<String, String> mapaIDsCascadeoHijos = new HashMap<String, String>();
		    for (BigDecimal idToRiesgo : riesgos) {
			RiesgoDN riesgoDN = RiesgoDN.getInstancia();
			List<ConfiguracionDatoIncisoCotizacionDTO> datosSubInciso = cotizacionDN
				.getDatosRiesgoSubInciso(idToRiesgo);
			configuracion.addAll(datosSubInciso);
			if (!datosSubInciso.isEmpty()) {
			    buffer
				    .append("<div class=\"subtituloIzquierdaDiv\">Riesgo: "
					    + riesgoDN.getPorId(idToRiesgo)
						    .getDescripcion() + "</div>");
			}
			buffer.append("<table id= \"desplegarDetalle\">");
			boolean trAbierto = false;
			for (int i = 0; i < datosSubInciso.size(); i++) {
			    if (i % 2 == 0 && trAbierto == true) {
				buffer.append("</tr>");
				trAbierto = false;
			    }
			    if (i % 2 == 0 && trAbierto == false) {
				buffer.append("<tr>");
				trAbierto = true;
			    }
			    ConfiguracionDatoIncisoCotizacionDTO dato = datosSubInciso
				    .get(i);

			    String value = "";
			    if (modificar.equals("1")) {
				BigDecimal numeroSubInciso = UtileriasWeb
					.regresaBigDecimal(request
						.getParameter("numeroSubInciso"));
				DatoIncisoCotizacionId id = new DatoIncisoCotizacionId();
				id.setIdToCotizacion(idToCotizacion);
				id.setNumeroInciso(numeroInciso);
				id.setIdDato(dato.getId().getIdDato());
				id.setClaveDetalle(dato.getId().getClaveDetalle());
				id.setIdTcRamo(dato.getId().getIdTcRamo());
				id.setIdTcSubramo(dato.getId().getIdTcSubramo());
				id.setIdToCobertura(BigDecimal.ZERO);
				id.setIdToRiesgo(dato.getId().getIdToRiesgo());
				id.setIdToSeccion(idToSeccion);
				id.setNumeroSubinciso(numeroSubInciso);

				DatoIncisoCotizacionSN datoIncisoCotizacionSN = new DatoIncisoCotizacionSN();
				DatoIncisoCotizacionDTO valor = datoIncisoCotizacionSN
					.getPorId(id);
				if (valor != null) {
				    value = valor.getValor();
				}
			    }

			    buffer.append("<th class='normal' width='20%'>"
				    + dato.getDescripcionEtiqueta() + ": </th>");
			    Map atributos = null;
			    String name = "datos[" + (total++) + "]";
			    String id = "id" + total;
			    String onblur = "";
			    switch (dato.getClaveTipoValidacion()) {
			    case 1: // cuota al millar, validar 8 digitos con 4
				    // decimales
				onblur = "validarDecimal(this.form." + id
					+ ", this.value, 8, 4)";
				break;
			    case 2: // porcentaje, validar 8 digitos con 4 decimales
				onblur = "validarDecimal(this.form." + id
					+ ", this.value, 8, 2)";
				break;
			    case 3: // importe. validar 16 digitos con 2 decimales
				onblur = "validarDecimal(this.form." + id
					+ ", this.value, 16, 2)";
				break;
			    case 4: // factor, valodar 16 d�gitos con 4 decimales
				onblur = "validarDecimal(this.form." + id
					+ ", this.value, 16, 4)";
				break;
			    case 5: // DSMGVDF, validar entero de 8 d�gitos
				onblur = "validarDecimal(this.form." + id
					+ ", this.value, 8, 0)";
				break;
			    }
			    switch (dato.getClaveTipoControl()) {
			    case 1:
				atributos = new HashMap();
				// atributos.put("class", "cajaTexto");
				// atributos.put("size", "");
				// atributos.put("id", name);
				// atributos.put("name", name);
				// buffer.append("<td>" +
				// IncisoCotizacionUtil.obtenerSelectCatalogo(atributos,
				// dato.getDescripcionClaseRemota(), soloLectura, value)
				// + "</td>");
				buffer.append("<td>"
					+ IncisoCotizacionUtil.obtenerSelectCatalogo(
						name, dato.getDescripcionClaseRemota(),
						soloLectura, value,
						mapaIDsCascadeoHijos) + "</td>");
				break;
			    case 2:
				atributos = new HashMap();
				atributos.put("class", "cajaTexto");
				atributos.put("size", "");
				atributos.put("id", name);
				atributos.put("name", name);
				buffer.append("<td>"
					+ IncisoCotizacionUtil.obtenerSelectValorFijo(
						atributos, dato.getIdGrupo(),
						soloLectura, value) + "</td>");
				break;
			    case 3:
				buffer
					.append("<td><input type=\"text\" class=\"cajaTexto\" style=\"text-transform: none;\" value='"
						+ value
						+ "' name='"
						+ name
						+ "' onblur='"
						+ onblur
						+ "' "
						+ disabled + " id='" + id + "'/></td>");
				break;
			    case 4:
				buffer
					.append("<td><textarea class=\"cajaTexto\" style=\"text-transform: none;\" name='"
						+ name
						+ "' onblur='"
						+ onblur
						+ "' "
						+ disabled
						+ " id='"
						+ id
						+ "'>"
						+ value + "</textarea></td>");
				break;
			    }
			}
			if (trAbierto == true) {
			    buffer.append("</tr>");
			}
			buffer.append("</table>");
		    }
		    buffer.append("]]></datossubinciso>");
		    buffer.append("</response>");
		    response.setContentType("text/xml");
		    response.setHeader("Cache-Control", "no-cache");
		    response.setContentLength(buffer.length());
		    response.getWriter().write(buffer.toString());

		    HttpSession session = request.getSession();
		    session.removeAttribute("configuracion");
		    session.setAttribute("configuracion", configuracion);
		} catch (IOException e) {
		    e.printStackTrace();
		} catch (SystemException e) {
		    e.printStackTrace();
		}
	    }

	
	
	 public ActionForward mostrarDetalleSubInciso(ActionMapping mapping, ActionForm form,
		    HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("idSubInciso");
		String ids[] = id.split(",");
		BigDecimal idToCotizacion = new BigDecimal(ids[0]);
		BigDecimal numeroInciso = new BigDecimal(ids[1]);
		BigDecimal idToSeccion = new BigDecimal(ids[2]);
		BigDecimal numeroSubInciso = new BigDecimal(ids[3]);

		SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(
			idToCotizacion, numeroInciso, idToSeccion, numeroSubInciso);
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;
		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN
			.getInstancia();
		try {
		    SubIncisoCotizacionDTO subIncisoCotizacionDTO = incisoCotizacionDN
			    .getSubIncisoCotizacionPorId(subIncisoCotizacionDTOId);
		    // SubIncisoDTO subIncisoDTO =
		    // incisoCotizacionDN.getSubIncisoPorId(subIncisoCotizacionDTO.getId().getNumeroSubInciso());
		    // this.poblarForm(subIncisoCotizacionDTO, subIncisoDTO,
		    // subIncisoForm);

		    CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
			    .obtieneNombreUsuario(request));
		    CotizacionDTO cotizacionDTO = cotizacionDN.getPorId(idToCotizacion);
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			    "dd/MM/yyyy");

		    subIncisoForm.set("fechaCreacion", simpleDateFormat
			    .format(cotizacionDTO.getFechaCreacion()));
		    subIncisoForm.set("idToCotizacion", idToCotizacion.toBigInteger()
			    .toString());
		    subIncisoForm.set("idToSeccion", idToSeccion.toBigInteger()
			    .toString());
		    subIncisoForm.set("numeroInciso", numeroInciso.toBigInteger()
			    .toString());
		    subIncisoForm.set("numeroSubInciso", subIncisoCotizacionDTO.getId()
			    .getNumeroSubInciso().toString());
		    subIncisoForm.set("subInciso", subIncisoCotizacionDTO
			    .getDescripcionSubInciso());
		    subIncisoForm.set("sumaAsegurada", subIncisoCotizacionDTO
			    .getValorSumaAsegurada().toString());

		    SeccionDN seccionDN = SeccionDN.getInstancia();
		    SeccionDTO seccionDTO = new SeccionDTO();
		    seccionDTO.setIdToSeccion(idToSeccion);
		    seccionDTO = seccionDN.getPorId(seccionDTO);
		    subIncisoForm.set("seccionNombreComercial", seccionDTO
			    .getNombreComercial());

		    request.setAttribute("idToCotizacion", idToCotizacion.intValue());
		} catch (ExcepcionDeAccesoADatos e) {
		    reglaNavegacion = Sistema.NO_DISPONIBLE;
		    UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
			    request);
		} catch (SystemException e) {
		    reglaNavegacion = Sistema.NO_EXITOSO;
		    UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
			    request);
		}

		return mapping.findForward(reglaNavegacion);
	    }
	
	
	 /**
	     * Method listarSubIncisosPorSeccion
	     * 
	     * @param mapping
	     * @param form
	     * @param request
	     * @param response
	     * @return ActionForward
	     */
	    public ActionForward listarSubIncisosPorSeccion(ActionMapping mapping,
		    ActionForm form, HttpServletRequest request,
		    HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		String idSeccionCot = request.getParameter("idSeccionCot");
		BigDecimal idToCotizacion;
		BigDecimal numeroInciso;
		BigDecimal idToSeccion;
		SubIncisoForm subIncisoForm = (SubIncisoForm) form;

		List<SubIncisoCotizacionDTO> subIncisoCotizacionDTOLista = new ArrayList<SubIncisoCotizacionDTO>();
		if (!UtileriasWeb.esCadenaVacia(idSeccionCot)) {
		    String[] ids = idSeccionCot.split(",");
		    idToCotizacion = new BigDecimal(ids[0]);
		    numeroInciso = new BigDecimal(ids[1]);
		    idToSeccion = new BigDecimal(ids[2]);

		    subIncisoForm.setIdToSeccion(idToSeccion.toBigInteger().toString());
		    subIncisoForm.setNumeroInciso(numeroInciso.toBigInteger()
			    .toString());

		    CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb
			    .obtieneNombreUsuario(request));
		    SubIncisoCotizacionDTOId subIncisoCotizacionDTOId = new SubIncisoCotizacionDTOId(
			    idToCotizacion, numeroInciso, idToSeccion, null);
		    SubIncisoCotizacionDTO subIncisoCotizacionDTO = new SubIncisoCotizacionDTO();
		    subIncisoCotizacionDTO.setId(subIncisoCotizacionDTOId);
		    
		    try {
			subIncisoCotizacionDTOLista = cotizacionDN
				.listarFiltrado(subIncisoCotizacionDTO);
			request.setAttribute("subIncisoDTOList",
				subIncisoCotizacionDTOLista);
		    } catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
				request);
		    } catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
				request);
		    }
		}

		request.setAttribute("subIncisoCotizacionDTOLista",
			subIncisoCotizacionDTOLista);

		return mapping.findForward(reglaNavegacion);
	    }
	
	
	public ActionForward listarIncisos(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
	String reglaNavegacion = Sistema.EXITOSO;
	try {
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm)form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		String id = request.getParameter("id");
		String origen = request.getParameter("origen");
		
		cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(id));
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
		List<IncisoCotizacionDTO> incisos = cotizacionDN.listarIncisos(cotizacionDTO);
		request.setAttribute("incisos", incisos);
		HttpSession session = request.getSession();
		session.removeAttribute("idCotizacion");
		session.setAttribute("idCotizacion", new Integer(cotizacionDTO.getIdToCotizacion().intValue()));
		session.removeAttribute("fechaCreacion");
		session.setAttribute("fechaCreacion", cotizacionDTO.getFechaCreacion());
		
		session.removeAttribute("origen");
		if(!UtileriasWeb.esCadenaVacia(origen)){
			session.setAttribute("origen", origen);
		}
		if(cotizacionDTO.getIdToCotizacion() != null) {
			incisoCotizacionForm.setIdCotizacion(cotizacionDTO.getIdToCotizacion().toString());
		}
		if(cotizacionDTO.getFechaCreacion() != null) {
			incisoCotizacionForm.setFechaOrdenTrabajo(cotizacionDTO.getFechaCreacion());
		}
	} catch (SystemException e) {
		reglaNavegacion = Sistema.NO_DISPONIBLE;
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
	} catch (mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos e) {
		reglaNavegacion = Sistema.NO_EXITOSO;
		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
	}
	return mapping.findForward(reglaNavegacion);
}

	
	
	public ActionForward mostrarDetalleInciso(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
	String reglaNavegacion = Sistema.EXITOSO;
	try {
		IncisoCotizacionForm incisoCotizacionForm = (IncisoCotizacionForm) form;
		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(request.getParameter("id")));
		id.setNumeroInciso(UtileriasWeb.regresaBigDecimal(request.getParameter("numeroInciso")));

		IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
		incisoCotizacionDTO.setId(id);

		IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
		incisoCotizacionDTO = incisoCotizacionDN.getPorId(incisoCotizacionDTO);


		DireccionDTO direccionDTO =incisoCotizacionDTO.getDireccionDTO();
		DireccionForm direccionForm = new DireccionForm();
		DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
		incisoCotizacionForm.set("direccionGeneral", direccionForm);

		if(incisoCotizacionDTO.getId().getIdToCotizacion() != null) {
			incisoCotizacionForm.set("idCotizacion", incisoCotizacionDTO.getId().getIdToCotizacion().toString());
		}
		if(incisoCotizacionDTO.getId().getNumeroInciso() != null) {
			incisoCotizacionForm.set("numeroInciso", incisoCotizacionDTO.getId().getNumeroInciso().toString());
		}
		HttpSession session = request.getSession();
		Date fechaCreacion = (Date)session.getAttribute("fechaCreacion");
		incisoCotizacionForm.setFechaOrdenTrabajo(fechaCreacion);
	} catch (SystemException e) {
		//mensajeExcepcion((IncisoCotizacionForm)form, e);
		e.printStackTrace();
	}
	return mapping.findForward(reglaNavegacion);
}
	
	
	
	
	
	/**
	 * Method mostrar
	 * @param mapping, form, request. response 
	 * @return ActionForward
	 */
	public ActionForward mostrarComisiones(ActionMapping mapping, ActionForm form, 
		HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String id = request.getParameter("id");
		cotizacionForm.setIdToCotizacion(id);
		String tipo = request.getParameter("tipo");
		
		if (tipo != null){
			if (tipo.equals("comision"))
				reglaNavegacion = "editaPorcentaje";
			else if (tipo.equals("autorizacion"))
				reglaNavegacion = "editaAutorizacion";
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void mostrarComisionesCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		String id = request.getParameter("id").toString();
		String tipoEdicion = request.getParameter("tipoEdicion").toString();
		
		MidasJsonBase json = new MidasJsonBase();
		if (tipoEdicion.equals("comision")){
			cotizacionForm.setComisiones(ComisionCotizacionDN.getInstancia().listarPorCotizacion(UtileriasWeb.regresaBigDecimal(id)));
			if(cotizacionForm.getComisiones()!= null && cotizacionForm.getComisiones().size() > 0) {
				for(ComisionCotizacionDTO actual : cotizacionForm.getComisiones()) {
					SubRamoDTO subRamo = new SubRamoDTO();
					subRamo.setIdTcSubRamo(actual.getId().getIdTcSubramo());
					subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
					String icono = "/MidasWeb/img/b_rechazar.gif^Rechazado^javascript:void(0);^_self";
					String tipoPorcentaje = "RO";
					if (actual.getId().getTipoPorcentajeComision() == 2)
						tipoPorcentaje = "RCI";
					else if (actual.getId().getTipoPorcentajeComision() == 3)
						tipoPorcentaje = "PRR";
										
					//si claveAutComision es 0, la comision fu� autorizada
					if (actual.getClaveAutComision().intValue()==0)
						icono = "/MidasWeb/img/blank.gif^Autorizado";
					//Si es 1, la comisi�n est� pendiente por autorizar
					else if (actual.getClaveAutComision().intValue()==1)
						icono = "/MidasWeb/img/b_pendiente.gif^Pendiente^javascript:void(0);^_self";
					//Verificar el significado de los valores 7 y 8, que son permitidos en este campo
					//La clave 7 corresponde a "autorizada", la clave 8 corresponde a "rechazada"
					else if (actual.getClaveAutComision().intValue() == 7)
						icono = "/MidasWeb/img/b_autorizar.gif^Autorizado^javascript:void(0);^_self";
					
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(id+"|"+actual.getId().getIdTcSubramo()+"|"+actual.getId().getTipoPorcentajeComision());
					row.setDatos(
							subRamo.getRamoDTO().getDescripcion(),
							subRamo.getDescripcionSubRamo(),
							tipoPorcentaje,
							actual.getPorcentajeComisionDefault() + "%",
							actual.getPorcentajeComisionCotizacion().toString(),
							icono
					);
					json.addRow(row);
				}
			}
		}
		
		else if (tipoEdicion.equals("autorizacion")){
			//En el listado de comisiones por autorizar, se deben mostrar tambi�n las comisiones rechazadas.
			/*List<ComisionCotizacionDTO> comisionesPendientes = ComisionCotizacionDN.getInstancia().listarPorClaveAutorizacionPorCotizacion(UtileriasWeb.regresaBigDecimal(id), Short.valueOf("1"));
			List<ComisionCotizacionDTO> comisionesRechazadas = ComisionCotizacionDN.getInstancia().listarPorClaveAutorizacionPorCotizacion(UtileriasWeb.regresaBigDecimal(id), Short.valueOf("7"));
			for (ComisionCotizacionDTO tmp : comisionesRechazadas){
				comisionesPendientes.add(tmp);
			}*/
			cotizacionForm.setComisiones(ComisionCotizacionDN.getInstancia().buscarPendientesPorAutorizar(UtileriasWeb.regresaBigDecimal(id)));
			if(cotizacionForm.getComisiones()!= null && cotizacionForm.getComisiones().size() > 0) {
				for(ComisionCotizacionDTO actual : cotizacionForm.getComisiones()) {
					SubRamoDTO subRamo = new SubRamoDTO();
					subRamo.setIdTcSubRamo(actual.getId().getIdTcSubramo());
					subRamo = SubRamoDN.getInstancia().getSubRamoPorId(subRamo);
					String icono = "/MidasWeb/img/b_rechazar.gif^Rechazado";
					String tipoPorcentaje = "RO";
					if (actual.getId().getTipoPorcentajeComision() == 2)
						tipoPorcentaje = "RCI";
					else if (actual.getId().getTipoPorcentajeComision() == 3)
						tipoPorcentaje = "PRR";
					
					//si claveAutComision es 1, la comision est� pendiente y no se muestra icono
					//Si no es 1, debe ser 8, que significa que fu� rechazado, se toma como rechazada.
					if (actual.getClaveAutComision().intValue()==1)
						icono = "/MidasWeb/img/blank.gif^Pendiente por autorizar";
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(id+"|"+actual.getId().getIdTcSubramo()+"|"+actual.getId().getTipoPorcentajeComision());
					row.setDatos(
							subRamo.getRamoDTO().getDescripcion(),
							subRamo.getDescripcionSubRamo(),
							tipoPorcentaje,
							actual.getPorcentajeComisionDefault() + "%",
							actual.getPorcentajeComisionCotizacion().toString(),
							icono,
							"0"
					);
					json.addRow(row);
				}
			}
		}
		
		response.setContentType("text/json");
		System.out.println("Comisiones de la cotizacion "+id+", : "+json);
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	
	
	
	public ActionForward mostrarResumenCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("id");
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		cotizacionForm.setIdToCotizacion(id);
		cotizacionForm.setListaIncisos(new ArrayList<ResumenIncisoCotizacionForm>());
		if (!UtileriasWeb.esCadenaVacia(id)){
			BigDecimal idToCotizacion;
			try {
				idToCotizacion = UtileriasWeb.regresaBigDecimal(id);
				 
				poblarResumenCotizacionForm(idToCotizacion, cotizacionForm, UtileriasWeb.obtieneNombreUsuario(request));
				
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.getPorId(idToCotizacion);
				cotizacionForm.setBonificacionComision(cotizacionDTO.getPorcentajebonifcomision().toString());
				cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
			} catch (SystemException e) {	e.printStackTrace();}
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	
	
	
	private void poblarResumenCotizacionForm(BigDecimal idToCotizacion,CotizacionForm cotizacionForm, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		List<IncisoCotizacionDTO> listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(idToCotizacion);
		for(IncisoCotizacionDTO incisoCot : listaIncisos){
			ResumenIncisoCotizacionForm resumenIncisoForm = new ResumenIncisoCotizacionForm();
			resumenIncisoForm.setListaSecciones(new ArrayList<ResumenSeccionCotizacionForm>());
			
			List<SeccionCotizacionDTO>  listaSeccionesContratadas = SeccionCotizacionDN.getInstancia().listarPorIncisoId(incisoCot.getId());
			for(SeccionCotizacionDTO seccionCot : listaSeccionesContratadas){
				
				ResumenSeccionCotizacionForm resumenSeccionForm = new ResumenSeccionCotizacionForm();
				
				
				
				resumenSeccionForm.setListaCoberturas(new ArrayList<ResumenCoberturaCotizacionForm>());
				resumenSeccionForm.setPrimaNeta(fMonto.format(seccionCot.getValorPrimaNeta()));
				resumenSeccionForm.setDescripcionSeccion(seccionCot.getSeccionDTO().getNombreComercial());
				
				CoberturaCotizacionDTO coberturaCotTMP = new CoberturaCotizacionDTO();
				coberturaCotTMP.setId(new CoberturaCotizacionId());
				coberturaCotTMP.getId().setIdToSeccion(seccionCot.getId().getIdToSeccion());
				coberturaCotTMP.getId().setIdToCotizacion(idToCotizacion);
				coberturaCotTMP.getId().setNumeroInciso(incisoCot.getId().getNumeroInciso());
				coberturaCotTMP.setClaveContrato((short)1);
				List<CoberturaCotizacionDTO> listaCoberturasContratadas = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotTMP);
				
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas){
					ResumenCoberturaCotizacionForm resumenCoberturaForm = new ResumenCoberturaCotizacionForm();
					if(coberturaCot.getValorSumaAsegurada() > 0) {
						resumenCoberturaForm.setCuota(fCuota.format((coberturaCot.getValorPrimaNeta() / coberturaCot.getValorSumaAsegurada()) * 1000D));
					} else {
						resumenCoberturaForm.setCuota(fCuota.format(0D));
					}
					CoberturaDTO coberturaTMP = CoberturaDN.getInstancia().getPorId(coberturaCot.getId().getIdToCobertura());
					resumenCoberturaForm.setDescripcionCobertura(coberturaTMP.getDescripcion());
					resumenCoberturaForm.setPrimaNeta(fMonto.format(coberturaCot.getValorPrimaNeta()));
					resumenCoberturaForm.setSumaAsegurada(fMonto.format(coberturaCot.getValorSumaAsegurada()));
					resumenSeccionForm.getListaCoberturas().add(resumenCoberturaForm);
				}
				
				resumenIncisoForm.getListaSecciones().add(resumenSeccionForm);
			}
			
			DireccionDTO direccionDTO = incisoCot.getDireccionDTO();
			DireccionForm direccionForm = new DireccionForm();
			DireccionAction.poblarDireccionForm(direccionForm , direccionDTO);
			
			resumenIncisoForm.setDescripcionInciso(direccionForm.toString());
			resumenIncisoForm.setPrimaNeta(fMonto.format(incisoCot.getValorPrimaNeta()));
			
			String descripcionGiroInciso = "No disponible";
			try{
				CoberturaCotizacionDTO coberturaCotTMP = new CoberturaCotizacionDTO();
				coberturaCotTMP.setId(new CoberturaCotizacionId());
				coberturaCotTMP.getId().setIdToCotizacion(idToCotizacion);
				coberturaCotTMP.getId().setNumeroInciso(incisoCot.getId().getNumeroInciso());
				coberturaCotTMP.setClaveContrato((short)1);
				List<CoberturaCotizacionDTO> listaCoberturasContratadas = CoberturaCotizacionDN.getInstancia().listarFiltrado(coberturaCotTMP);
				boolean contieneIncendio = false;
				BigDecimal idTcRamo=null;
				CoberturaCotizacionDTO coberturaTMP = null;
				BigDecimal INCENDIO = new BigDecimal(1d);
				for(CoberturaCotizacionDTO coberturaCot : listaCoberturasContratadas){
					idTcRamo = coberturaCot.getIdTcSubramo();
					coberturaTMP = coberturaCot;
					if (coberturaCot.getIdTcSubramo().compareTo(INCENDIO) == 0){
						contieneIncendio = true;
						break;
					}
				}
				if (contieneIncendio){
					GiroDTO giro = DatoIncisoCotizacionDN.getINSTANCIA().obtenerGiroDTO(idToCotizacion, incisoCot.getId().getNumeroInciso());
					descripcionGiroInciso = giro.getDescripcionGiro();
				}
				else{
					//TODO verificar la clave de obra civil. Se usa el 7 temporalmente
					BigDecimal OBRA_CIVIL_EN_CONSTRUCCION = new BigDecimal(7d);
					BigDecimal EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA = new BigDecimal(6d);
					BigDecimal EQUIPO_ELECTRONICO = new BigDecimal(5d);
					BigDecimal MONTAJE_MAQUINA = new BigDecimal(4d);
					BigDecimal ROTURA_MAQUINARIA = new BigDecimal(3d);
					BigDecimal RESPONSABILIDAD_CIVIL_GENERAL = new BigDecimal(2d);
					if (coberturaTMP.getIdTcSubramo().compareTo(RESPONSABILIDAD_CIVIL_GENERAL) == 0){
						SubGiroRCDTO subGiroRC = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubGiroRC(idToCotizacion,incisoCot.getId().getNumeroInciso(),idTcRamo);
						descripcionGiroInciso = subGiroRC.getGiroRC().getDescripcionGiroRC();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(ROTURA_MAQUINARIA) == 0){
						SubTipoMaquinariaDTO subTipoMaquinariaDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubTipoMaquinaria(idToCotizacion,incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoMaquinariaDTO.getTipoMaquinaria().getDescripcionTipoMaquinaria();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(MONTAJE_MAQUINA) == 0){
						SubtipoMontajeMaquinaDTO subTipoMontajeMaquinaDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubTipoMontajeMaquina(idToCotizacion,incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoMontajeMaquinaDTO.getTipoMontajeMaquinaDTO().getDescripciontipomontajemaq();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(EQUIPO_ELECTRONICO) == 0){
						SubtipoEquipoElectronicoDTO subTipoEquipoElectronicoDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubtipoEquipoElectronico(idToCotizacion,incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoEquipoElectronicoDTO.getEquipoElectronicoDTO().getDescripciontipoeqelectro();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(EQUIPO_CONTRATISTA_Y_MAQUINARIA_PESADA) == 0){
						SubtipoEquipoContratistaDTO subTipoEquipoContratistaDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerSubtipoEquipoContratista(idToCotizacion,incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = subTipoEquipoContratistaDTO.getTipoEquipoContratistaDTO().getDescripcionTipoEqContr();
					}
					else if (coberturaTMP.getIdTcSubramo().compareTo(OBRA_CIVIL_EN_CONSTRUCCION) == 0){
						TipoObraCivilDTO tipoObraCivilDTO = DatoIncisoCotizacionDN.getINSTANCIA().obtenerTipoObraCivil(idToCotizacion,incisoCot.getId().getNumeroInciso(),idTcRamo,null);
						descripcionGiroInciso = tipoObraCivilDTO.getDescripcionTipoObraCivil();
					}//Fin tipo obra civil.
				}
			}catch(Exception e){e.printStackTrace();}
			
			resumenIncisoForm.setDescripcionGiro(descripcionGiroInciso);
			cotizacionForm.getListaIncisos().add(resumenIncisoForm);
			
		}
	}
	
	
	//1.- PRIMER METODO PARA CREAR EL ARBOL DE COTIZACION
	
	/**
	 * Method mostrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */

	public ActionForward mostrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		CotizacionForm cotizacionForm = (CotizacionForm)form;
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		CotizacionSN cotizacionSN;
		ActionForward actionForward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
		String idToCotizacion = "";
		String idToPoliza=null;
		if(request.getParameter("id") != null) {
			idToCotizacion = request.getParameter("id");
		} else {
			idToCotizacion = (String)request.getAttribute("id");
		}
		if(request.getParameter("idPol") != null) {
		    idToPoliza=  (String) request.getParameter("idPol");
		}
		String editaDatoGeneral = request.getParameter("editaDatoGeneral");
		if (!UtileriasWeb.esCadenaVacia(editaDatoGeneral))
			cotizacionForm.setEditaDatoGeneral(editaDatoGeneral);
		String esCotizacion = request.getParameter("esCotizacion");
		
		try {
			cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
			cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
			cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			this.poblarForm(cotizacionDTO, cotizacionForm,request);
			if (!UtileriasWeb.esObjetoNulo(esCotizacion)){
				cotizacionForm.setEsCotizacion(esCotizacion);
				cotizacionForm.setIdToCotizacionFormateada(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.prefijo") + UtileriasWeb.llenarIzquierda(idToCotizacion, "0", 8));
				if(cotizacionDTO.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_ASIGNADA) {
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
					cotizacionSN.modificar(cotizacionDTO);
				}
			}else{
				if(cotizacionDTO.getClaveEstatus().intValue() == Sistema.ESTATUS_ODT_ASIGNADA){
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_ODT_ENPROCESO);
					cotizacionSN.modificar(cotizacionDTO);
				}
				cotizacionForm.setEsCotizacion("0");
				if(cotizacionDTO.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_ASIGNADA) {
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_ENPROCESO);
					cotizacionSN.modificar(cotizacionDTO);
				}				
			}
			request.setAttribute("idToCotizacion", idToCotizacion);

			//Bloque usado para definir si el usuario puede o no visualizar el tag de inisos
			if (cotizacionDTO.getTipoPolizaDTO() != null){
				cotizacionForm.setEditaIncisos((cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null)?"true":"false");
				cotizacionForm.setPermiteCambiarPoliza((cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null)?"false":"true");
			}
			else{
				cotizacionForm.setEditaIncisos("false");
				cotizacionForm.setPermiteCambiarPoliza("true");
			}
			//Bloque usado para definir si el usuario puede o no visualizar el tag de primer riesgo/LUC
			List<IncisoCotizacionDTO> listaIncisos = IncisoCotizacionDN.getInstancia().listarPorCotizacionId(cotizacionDTO.getIdToCotizacion());
			if (listaIncisos != null)
				cotizacionForm.setEditaPrimerRiesgo((listaIncisos.size() > 0)?"true":"false");
			else
				cotizacionForm.setEditaIncisos("false");
			
			//Bloque usado para definir qu� tipo de edici�n realizar� el usuario en las comisiones de la cotizacion,
			//Puede editar el porcentaje de la comision y la autorizacion
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			for (Rol rol : usuario.getRoles()) {
				if (rol.getDescripcion().equals(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
					cotizacionForm.setAutorizacionComision("autorizacion");
					request.setAttribute("autorizacionComision", "autorizacion");
					break;
				}else if (rol.getDescripcion().equals(Sistema.ROL_AGENTE)){
					cotizacionForm.setEdicionComision("comision");
					request.setAttribute("edicionComision", "comision");
					break;
				}else if(rol.getDescripcion().equalsIgnoreCase(Sistema.ROL_SUPERVISOR_SUSCRIPTOR)){
					request.setAttribute("autorizarTexAdicional", "autorizarTexAdicional");
					break;
				}
			}
			
			
			if(idToPoliza!=null){
			List<ReciboDTO> recibosPoliza=ReciboDN.getInstancia(usuario.getNombreUsuario()).consultaRecibos(new BigDecimal(idToPoliza));
        			if(recibosPoliza!=null){
        			    request.setAttribute("presentaRecibos", "1");
        			    cotizacionForm.setIdToPoliza(idToPoliza);
        			}
			}

		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		return actionForward;	
	}	
	
	
	//2.-METODO PARA CARGAR EL ARBOL DE COTIZACION
	/**
	 * Method cargarArbolCotizacion
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward cargarArbolCotizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String idToCotizacion = request.getParameter("idToCotizacion");
		String cotizacion = "";
		String inciso = "";
		String seccion = "";
		String cobertura = "";
		String riesgo = "";
		String separador = "_";
		
		StringBuffer buffer = new StringBuffer();
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
			SeccionDN seccionDN = SeccionDN.getInstancia();
			SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
			CoberturaDN coberturaDN = CoberturaDN.getInstancia();
			CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
			RiesgoCotizacionDN riesgoCotizacionDN = RiesgoCotizacionDN.getInstancia();
			
			CotizacionDTO cotizacionDTO = new CotizacionDTO();			
			SeccionDTO seccionDTO = new SeccionDTO();
			CoberturaDTO coberturaDTO = new CoberturaDTO();		
			
			List<IncisoCotizacionDTO> listaIncisos = new ArrayList<IncisoCotizacionDTO>();
			List<SeccionCotizacionDTO> listaSeccion = new ArrayList<SeccionCotizacionDTO>();
			List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
			List<RiesgoCotizacionDTO> listaRiesgos = new ArrayList<RiesgoCotizacionDTO>();
			
			listaIncisos = incisoCotizacionDN.listarPorCotizacionId(new BigDecimal(idToCotizacion));
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
			String moneda="";
			if(cotizacionDTO.getIdMoneda().equals(new BigDecimal(Sistema.MONEDA_PESOS))){
			    moneda="(MXP)";
			}else if(cotizacionDTO.getIdMoneda().equals(new BigDecimal(Sistema.MONEDA_DOLARES))){
			    moneda="(USD)";
			}
			buffer.append("<tree id=\"0\">\n");
			buffer.append(abrirItem("COT-"+UtileriasWeb.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toString(), "0", 8)+moneda, cotizacionDTO.getIdToCotizacion().toString()));
			if (listaIncisos!=null){
				cotizacion = idToCotizacion;				
				//Creando Incisos
				for (IncisoCotizacionDTO incisoCotizacionDTO : listaIncisos) {
					if (incisoCotizacionDTO!=null && incisoCotizacionDTO.getId()!=null){
						inciso = cotizacion + separador + incisoCotizacionDTO.getId().getNumeroInciso().toString();
						buffer.append(abrirItem(incisoCotizacionDTO.getId().getNumeroInciso().toString(), inciso));
						
						//Creando Secciones
						listaSeccion = seccionCotizacionDN.listarPorIncisoId(incisoCotizacionDTO.getId());
						if (listaSeccion!=null){
							for (SeccionCotizacionDTO seccionCotizacionDTO : listaSeccion) {
								if (seccionCotizacionDTO!=null && seccionCotizacionDTO.getId()!=null){
									seccionDTO.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
									seccionDTO = seccionDN.getPorId(seccionDTO);
									seccion = inciso + separador + seccionCotizacionDTO.getId().getIdToSeccion().toString();
									buffer.append(abrirItem(seccionDTO.getNombreComercial(),seccion));
									
										listaCoberturas = coberturaCotizacionDN.listarPorSeccionCotizacionId(seccionCotizacionDTO.getId(),Boolean.TRUE);
										if(listaCoberturas!=null){

											for (CoberturaCotizacionDTO coberturaCotizacionDTO : listaCoberturas) {
												coberturaDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
												coberturaDTO = coberturaDN.getPorId(coberturaDTO);
												cobertura = seccion + separador + coberturaCotizacionDTO.getId().getIdToCobertura().toString();
												if (coberturaCotizacionDTO.getClaveContrato().intValue()==1){
													buffer.append(abrirItem(coberturaDTO.getNombreComercial(), cobertura));
														listaRiesgos = riesgoCotizacionDN.listarRiesgoContratadosPorCobertura(coberturaCotizacionDTO.getId());
														if(listaRiesgos!=null){
															for (RiesgoCotizacionDTO riesgoCotizacionDTO : listaRiesgos) {
																if (riesgoCotizacionDTO.getId()!=null){
																	riesgo = cobertura + separador + riesgoCotizacionDTO.getId().getIdToRiesgo().toString();
																	buffer.append(abrirItem(riesgoCotizacionDTO.getRiesgoCoberturaDTO().getDescripcionRiesgoCobertura(),riesgo));
																	buffer.append(cerrarItem());
																}
															}
														}
													buffer.append(cerrarItem());
												}
											}
										}										
										
									buffer.append(cerrarItem());
								}
							}
						}
						buffer.append(cerrarItem());
					}//Inciso valido				
				}		
			}
			buffer.append(cerrarItem());
			buffer.append("</tree>");
			request.setAttribute("xmlArbol", buffer.toString());
			reglaNavegacion = Sistema.EXITOSO;
		}catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	
	//3.- METODO PARA VER LA SECCIONES
	/**
	 * Method mostrarModificarSecciones
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarModificarSecciones(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		// Funciona en conjunto con el grid javascript (en la funci�n mostrarSeccionesPorInciso(id){}) para actualizar el estatus de obligatoriedad de una secci�n de un inciso
		String reglaNavegacion = Sistema.EXITOSO;
		SeccionCotForm seccionCotForm = (SeccionCotForm) form;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		String idToCotizacion = request.getParameter("idToCotizacion");
		if (!UtileriasWeb.esObjetoNulo(idToCotizacion) && !UtileriasWeb.esCadenaVacia(idToCotizacion)){
			cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
			try {
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				seccionCotForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion().toString());
				seccionCotForm.setFechaCreacion(dateFormat.format(cotizacionDTO.getFechaCreacion()));
				request.setAttribute("idToCotizacion", UtileriasWeb
						.llenarIzquierda(idToCotizacion, "0", 8));				
				
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
		}else
			reglaNavegacion = Sistema.NO_EXITOSO;
		
		return mapping.findForward(reglaNavegacion);
	}

		 /** Method dummy
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 */
	public void dummy(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response) {

			//Se obtiene la iformacion de los diversos ramos de la ODT/COT
			//Se obtiene a traves de los ramos asociados al tipo de poliza de la cotizacion
			
			//BigDecimal cotizacionId= UtileriasWeb.regresaBigDecimal(request.getParameter("cotizacionId"))
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(new BigDecimal("1"));
			
			try {
				CotizacionDN cotizacionDN =CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				//se obtienen la Informaci�n de Ramo-Inciso
				//cotizacionDN.getDatosRamoInciso(cotizacionDTO);
				//se iteran los objetos obtenidos
				
				StringBuffer buffer = new StringBuffer();
				buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
				buffer.append("<response>");
				buffer.append("<datosinciso><![CDATA[");
				buffer.append("<table><tr><td class=\"titulo\" colspan=\"4\">Aqui los datos obtenidos</td></tr></table>");
				buffer.append("]]></datosinciso>");
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}
		}
		
		private String abrirItem(String texto, String id){
			String item = new String();
			item += "<item text=\"";
			item += texto.replaceAll("\"", "&#34;");
			item += "\" id=\"";
			item += id;
			item += "\">\n";
			return item;
		}
		private String cerrarItem(){
			return "</item>\n";
		}
		/**
		 * Method listarOrdenesTrabajoFiltrado
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward listarOrdenesTrabajoFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			Usuario usuario = getUsuarioMidas(request);
			String claveEstatus = request.getParameter("claveEstatus");
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			try {
				cotizacionDTO.setClaveEstatus(Short.valueOf(claveEstatus));
				OrdenTrabajoDN ordenTrabajoDN = OrdenTrabajoDN.getInstancia(usuario.getNombreUsuario());
				
				request.setAttribute("listaOrdenDeTrabajo", ordenTrabajoDN.listarOrdenesDeTrabajoFiltrado(cotizacionDTO,usuario));
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
			return mapping.findForward(reglaNavegacion);
		}

		/**
		 * Method cargarArbolOrdenTrabajo
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward cargarArbolOrdenTrabajo(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			
			String idToCotizacion = request.getParameter("idToCotizacion");
			String cotizacion = "";
			String inciso = "";
			String seccion = "";
			String cobertura = "";
			String separador = "_";
			
			StringBuffer buffer = new StringBuffer();
			String reglaNavegacion = Sistema.EXITOSO;
			try {
				CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
				SeccionDN seccionDN = SeccionDN.getInstancia();
				SeccionCotizacionDN seccionCotizacionDN = SeccionCotizacionDN.getInstancia();
				CoberturaDN coberturaDN = CoberturaDN.getInstancia();
				CoberturaCotizacionDN coberturaCotizacionDN = CoberturaCotizacionDN.getInstancia();
				
				CotizacionDTO cotizacionDTO = new CotizacionDTO();			
				SeccionDTO seccionDTO = new SeccionDTO();
				CoberturaDTO coberturaDTO = new CoberturaDTO();		
				
				List<IncisoCotizacionDTO> listaIncisos = new ArrayList<IncisoCotizacionDTO>();
				List<SeccionCotizacionDTO> listaSeccion = new ArrayList<SeccionCotizacionDTO>();
				List<CoberturaCotizacionDTO> listaCoberturas = new ArrayList<CoberturaCotizacionDTO>();
				
				listaIncisos = incisoCotizacionDN.listarPorCotizacionId(new BigDecimal(idToCotizacion));
				cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				buffer.append("<tree id=\"0\">\n");
				buffer.append(abrirItem("Orden de Trabajo: D-"+UtileriasWeb.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toString(), "0", 8), cotizacionDTO.getIdToCotizacion().toString()));
				if (listaIncisos!=null){
					cotizacion = idToCotizacion;				
					
					//Creando Incisos
					for (IncisoCotizacionDTO incisoCotizacionDTO : listaIncisos) {
						if (incisoCotizacionDTO!=null && incisoCotizacionDTO.getId()!=null){
							inciso = cotizacion + separador + incisoCotizacionDTO.getId().getNumeroInciso().toString();
							buffer.append(abrirItem(incisoCotizacionDTO.getId().getNumeroInciso().toString(), inciso));
							
							//Creando Secciones
							listaSeccion = seccionCotizacionDN.listarPorIncisoId(incisoCotizacionDTO.getId());
							if (listaSeccion!=null){
								for (SeccionCotizacionDTO seccionCotizacionDTO : listaSeccion) {
									if (seccionCotizacionDTO!=null && seccionCotizacionDTO.getId()!=null){
										seccionDTO.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
										seccionDTO = seccionDN.getPorId(seccionDTO);
										seccion = inciso + separador + seccionCotizacionDTO.getId().getIdToSeccion().toString();
										buffer.append(abrirItem(seccionDTO.getNombreComercial(),seccion));
											//Creando coberturas
											listaCoberturas = coberturaCotizacionDN.listarPorSeccionCotizacionId(seccionCotizacionDTO.getId(),Boolean.TRUE);
											if(listaCoberturas!=null){
													for (CoberturaCotizacionDTO coberturaCotizacionDTO : listaCoberturas) {
														coberturaDTO.setIdToCobertura(coberturaCotizacionDTO.getId().getIdToCobertura());
														coberturaDTO = coberturaDN.getPorId(coberturaDTO);
														cobertura = seccion + separador + coberturaCotizacionDTO.getId().getIdToCobertura().toString();
														if (coberturaCotizacionDTO.getClaveContrato().intValue()==1){
															buffer.append(abrirItem(coberturaDTO.getNombreComercial(), cobertura));
															buffer.append(cerrarItem());
														}
													}
											}										
											
										buffer.append(cerrarItem());
									}
								}
							}
							buffer.append(cerrarItem());
						}//Inciso valido				
					}		
				}
				buffer.append(cerrarItem());
				buffer.append("</tree>");
				request.setAttribute("xmlArbol", buffer.toString());
				reglaNavegacion = Sistema.EXITOSO;
			}catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}
			
			return mapping.findForward(reglaNavegacion);
		}
		
		
		
		/**
		 * Method listarOrdenesTrabajo
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		
		public ActionForward listarOrdenesTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			try {
				Usuario usuario = getUsuarioMidas(request);
				request.setAttribute("listaOrdenDeTrabajo", OrdenTrabajoDN.getInstancia(usuario.getNombreUsuario())
						.listarOrdenesDeTrabajo());
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
			

			return mapping.findForward(reglaNavegacion);
		}
		
		/**
		 * Method listarCotizaciones
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward listarCotizaciones(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			//UsuarioDTO usuarioDTO = new UsuarioDTO();
			//String claveEstatus = "9";
			//CotizacionDTO cotizacionDTO = new CotizacionDTO();
			try {
				//cotizacionDTO.setClaveEstatus(Short.valueOf(claveEstatus));
				//Se deben listar s�lo los registros con estatus de cotizacion
				request.setAttribute("listaCotizacion", CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
						.listarCotizaciones());
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
			return mapping.findForward(reglaNavegacion);
		}
		
		/**
		 * Method listarCotizacionesFiltrado
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward listarCotizacionesFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			Usuario usuario = getUsuarioMidas(request);
			String claveEstatus = request.getParameter("claveEstatus");
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			try {
				cotizacionDTO.setClaveEstatus(Short.valueOf(claveEstatus));
				request.setAttribute("listaCotizacion", CotizacionDN.getInstancia(usuario.getNombreUsuario())
						.listarCotizacionFiltrado(cotizacionDTO,usuario));
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
			return mapping.findForward(reglaNavegacion);
		}
		
		public ActionForward mostrarCancelarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			//UsuarioDTO usuarioDTO = new UsuarioDTO();
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			String idToCotizacion = request.getParameter("idToCotizacion");
			cotizacionForm.setIdToCotizacion(idToCotizacion);
			try {
				List<CotizacionRechazoCancelacionDTO> listaRechazo = CotizacionRechazoCancelacionDN.getInstancia().buscarPorPropiedad("id.idToCotizacion", UtileriasWeb.regresaBigDecimal(idToCotizacion));
				if (listaRechazo != null && !listaRechazo.isEmpty()){
					cotizacionForm.setIdMotivoRechazo(((CotizacionRechazoCancelacionDTO)listaRechazo.get(0)).getId().getIdTcRechazoCancelacion().toBigInteger().toString());
				}
			} catch (ExcepcionDeAccesoADatos e) {
			} catch (SystemException e) {}
			return mapping.findForward(reglaNavegacion);
		}

		public ActionForward mostrarCancelarCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			//UsuarioDTO usuarioDTO = new UsuarioDTO();
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			String idToCotizacion = request.getParameter("idToCotizacion");
			cotizacionForm.setIdToCotizacion(idToCotizacion);
			try {
				List<CotizacionRechazoCancelacionDTO> listaRechazo = CotizacionRechazoCancelacionDN.getInstancia().buscarPorPropiedad("id.idToCotizacion", UtileriasWeb.regresaBigDecimal(idToCotizacion));
				if (listaRechazo != null && !listaRechazo.isEmpty()){
					cotizacionForm.setIdMotivoRechazo(((CotizacionRechazoCancelacionDTO)listaRechazo.get(0)).getId().getIdTcRechazoCancelacion().toBigInteger().toString());
				}
			} catch (ExcepcionDeAccesoADatos e) {
			} catch (SystemException e) {}
			return mapping.findForward(reglaNavegacion);
		}	
		
		public void cancelarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			boolean error=false;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
			try {
				if(!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMotivoRechazo())){
					Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
					OrdenTrabajoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
						.rechazarCancelarOrdenTrabajo(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()), 
														UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdMotivoRechazo()), usuario);
					buffer.append("<id>1</id><description><![CDATA[La orden de trabajo se cancel\u00f3 correctamente.]]></description>");
				}
				else
					buffer.append("<id>2</id><description><![CDATA[Seleccione un motivo de la lista.]]></description>");
			} catch (ExcepcionDeAccesoADatos e) {
				error=true;
				e.printStackTrace();
			} catch (SystemException e) {
				error=true;
				e.printStackTrace();
			} catch (Exception e){
				error=true;
				e.printStackTrace();
			}
			if(error)
				buffer.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al cancelar la orden de trabajo.]]></description>");
			buffer.append("</item></response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			try {
				response.getWriter().write(buffer.toString());
			} catch (IOException e) {e.printStackTrace();}
		}
		
		public void cancelarCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			boolean error=false;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
			try {
				if(!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMotivoRechazo())){
					Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
					CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
						.rechazarCancelarCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()),
													UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdMotivoRechazo()), usuario);
					buffer.append("<id>1</id><description><![CDATA[La Cotizaci\u00f3n se cancel\u00f3 correctamente.]]></description>");
				}
				else
					buffer.append("<id>2</id><description><![CDATA[Seleccione un motivo de la lista.]]></description>");
			} catch (ExcepcionDeAccesoADatos e) {
				error=true;
				e.printStackTrace();
			} catch (SystemException e) {
				error=true;
				e.printStackTrace();
			} catch (Exception e){
				error=true;
				e.printStackTrace();
			}
			if(error)
				buffer.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al cancelar la orden de trabajo.]]></description>");
			buffer.append("</item></response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			try {
				response.getWriter().write(buffer.toString());
			} catch (IOException e) {e.printStackTrace();}
		}
		
		public ActionForward editarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionSN cotizacionSN;
			String idToCotizacion = request.getParameter("id");
			try {
				cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
				cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
				this.poblarForm(cotizacionDTO, cotizacionForm,request);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (Exception e){
				e.printStackTrace();
			}
			return mapping.findForward(reglaNavegacion);
		}

		public ActionForward editarCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionSN cotizacionSN;
			String idToCotizacion = request.getParameter("id");
			try {
				cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
				cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
				this.poblarForm(cotizacionDTO, cotizacionForm,request);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (Exception e){
				e.printStackTrace();
			}
			return mapping.findForward(reglaNavegacion);
		}

		public ActionForward autorizarOrdenTrabajo(ActionMapping mapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			try {
				Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
						request, Sistema.USUARIO_ACCESO_MIDAS);			
				cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
				if(cotizacionForm.getClaveAutRetroacDifer() != null)
					cotizacionDTO.setClaveAutRetroacDifer(Sistema.AUTORIZADA);
				if(cotizacionForm.getClaveAutVigenciaMaxMin() != null)
					cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.AUTORIZADA);
				
				cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				cotizacionDTO.setFechaModificacion(new Date());
				
				CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}

			return mapping.findForward(reglaNavegacion);
		}
		
		public ActionForward rechazarOrdenTrabajo(ActionMapping mapping,
				ActionForm form, HttpServletRequest request,
				HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			try {
				Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
						request, Sistema.USUARIO_ACCESO_MIDAS);		
				
				cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
				if(cotizacionForm.getClaveAutRetroacDifer() != null)
					cotizacionDTO.setClaveAutRetroacDifer(Sistema.RECHAZADA);
				if(cotizacionForm.getClaveAutVigenciaMaxMin() != null)
					cotizacionDTO.setClaveAutVigenciaMaxMin(Sistema.RECHAZADA);
				
				cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				cotizacionDTO.setFechaModificacion(new Date());				
				CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			}

			return mapping.findForward(reglaNavegacion);
		}	
		
		
		public ActionForward guardarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionSN cotizacionSN;
			ActionForward forward;
			BigDecimal idToTipoPolizaAnterior = null;
			BigDecimal idToTipoPolizaNueva = null;
			try {
				cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
				cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
				cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
				if (cotizacionDTO.getTipoPolizaDTO() != null){
					idToTipoPolizaAnterior = cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza();
				}
				Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
				cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				cotizacionDTO.setFechaModificacion(new Date());
				
				poblarDTO(cotizacionForm, cotizacionDTO);

				//OrdenTrabajoDN.getInstancia().actualizarCotizacion(cotizacionDTO);
				if (cotizacionDTO.getTipoPolizaDTO() != null){
					idToTipoPolizaNueva = cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza();
				}
				//Generar las nuevas comisiones y borrar los incisos correspondientes al tipopoliza anterior.
				CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.cambiarTipoPoliza(cotizacionDTO, idToTipoPolizaAnterior, idToTipoPolizaNueva);
				CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (Exception e){
				e.printStackTrace();
			}
			
			if (!UtileriasWeb.esObjetoNulo(cotizacionForm.getEsCotizacion()) && cotizacionForm.getEsCotizacion().equals("1")) {
//				forward = new ActionForward(mapping.findForward(reglaNavegacion));
//				String path;
//				path = forward.getPath();
//				forward.setPath(path + "?esCotizacion=1&id=" + cotizacionDTO.getIdToCotizacion().toString());
				forward = new ActionForward();
				String path;
				path = "/cotizacion/mostrarDatosGenerales.do?id="+cotizacionForm.getIdToCotizacion();
				forward.setPath(path);
				return forward;
			}
			
			return mapping.findForward(reglaNavegacion);
		}
		
		public ActionForward mostrarAsignarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws ExcepcionDeAccesoADatos, SystemException {
			String reglaNavegacion = Sistema.EXITOSO;
			//UsuarioDTO usuarioDTO = new UsuarioDTO();
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			List<Usuario> usuarios = new ArrayList<Usuario>();
			String idToCotizacion = request.getParameter("idToCotizacion");
			String idToSolicitud = request.getParameter("idToSolicitud");
			if (!UtileriasWeb.esCadenaVacia(idToSolicitud)
					&& !idToSolicitud.toLowerCase().startsWith("und")) {
				cotizacionForm.setIdToSolicitud(idToSolicitud);
				if (Integer.parseInt(idToSolicitud) > 0) {
					usuarios = UsuarioWSDN.getInstancia()
							.obtieneUsuariosSinRolesPorNombreRol(
									usuario.getNombreUsuario(),
									usuario.getIdSesionUsuario(),
									Sistema.ROL_SUSCRIPTOR_OT,
									Sistema.ROL_AGENTE_EXTERNO,
									Sistema.ROL_SUSCRIPTOR_EXT);
				} else {
					usuarios = UsuarioWSDN.getInstancia()
							.obtieneUsuariosSinRolesPorNombreRol(
									usuario.getNombreUsuario(),
									usuario.getIdSesionUsuario(),
									Sistema.ROL_EMISOR, Sistema.ROL_AGENTE_EXTERNO,
									Sistema.ROL_SUSCRIPTOR_EXT);
				}
			} else {
				usuarios = UsuarioWSDN.getInstancia()
						.obtieneUsuariosSinRolesPorNombreRol(
								usuario.getNombreUsuario(),
								usuario.getIdSesionUsuario(),
								Sistema.ROL_SUSCRIPTOR_COT,
								Sistema.ROL_AGENTE_EXTERNO,
								Sistema.ROL_SUSCRIPTOR_EXT);
			}
			if(!UtileriasWeb.esCadenaVacia(idToCotizacion)) {
				cotizacionForm.setIdToCotizacion(idToCotizacion);
			}
			cotizacionForm.setUsuarios(usuarios);
			return mapping.findForward(reglaNavegacion);
		}
		
		public void asignarCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			boolean error=false;
			
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionSN cotizacionSN;
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
			try {
				cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
				cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
				cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
				//cotizacionDTO.setCodigoUsuarioEmision(cotizacionForm.getCodigoUsuarioOrdenTrabajo());
				cotizacionDTO.setClaveEstatus((short)15);
				Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_ACCESO_MIDAS);
				cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				cotizacionDTO.setFechaModificacion(new Date());
				OrdenTrabajoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).actualizarCotizacion(cotizacionDTO);
				buffer.append("<id>1</id><description><![CDATA[La orden de trabajo se asign\u00f3 correctamente.]]></description>");
			} catch (ExcepcionDeAccesoADatos e) {
				error=true;
				e.printStackTrace();
			} catch (SystemException e) {
				error=true;
				e.printStackTrace();
			} catch (Exception e){error=true;
				e.printStackTrace();
			}
			if(error)
				buffer.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al cancelar la orden de trabajo.]]></description>");
			buffer.append("</item></response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			try {
				response.getWriter().write(buffer.toString());
			} catch (IOException e) {e.printStackTrace();}
		}
		
		public void asignarOrdenTrabajo(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			boolean error=false;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionSN cotizacionSN;
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
			try {
				if(UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioOrdenTrabajo()))
					if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToSolicitud()))
						if (!cotizacionForm.getIdToSolicitud().equalsIgnoreCase("undefined"))	buffer.append("<id>2</id><description><![CDATA[Seleccione un usuario para asignarle la Orden de trabajo.]]></description>");
						else	buffer.append("<id>2</id><description><![CDATA[Seleccione un usuario para asignarle la Cotizacion.]]></description>");
				else
					buffer.append("<id>2</id><description><![CDATA[Seleccione un usuario para asignarle la Cotizacion.]]></description>");
				else{
					if(!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToSolicitud()) & !cotizacionForm.getIdToSolicitud().equalsIgnoreCase("undefined")) {
						SolicitudDN solicitudDN = SolicitudDN.getInstancia();
						SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToSolicitud()));
		
						Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
						cotizacionDTO = SolicitudAction.crearOrdenTrabajo(solicitudDTO, usuario);
						CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
						cotizacionDTO.setPorcentajebonifcomision(0d);
						cotizacionDTO.setCodigoUsuarioOrdenTrabajo(cotizacionForm.getCodigoUsuarioOrdenTrabajo());
						//Estatus 0 corresponde a Orden de trabajo en asignada
						cotizacionDTO.setClaveEstatus((short)0);
						@SuppressWarnings("unused")
						GregorianCalendar gc = new GregorianCalendar();
						@SuppressWarnings("unused")
						TipoCambioDN tipoCambioDN = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
						//Double tipoCambio = tipoCambioDN.obtieneTipoCambioPorDia(gc.getTime());
						@SuppressWarnings("unused")
						Double tipoCambio = 13.4d;
						//tipoCambioDN.obtieneTipoCambioPorDia(gc.getTime());
					//FIXME Quitar el HC de 13.4 cuando este correcta la interfaz
					cotizacionDTO.setTipoCambio(13.4d);
						//cotizacionDTO.setTipoCambio(tipoCambio != null? tipoCambio: 1d);
						cotizacionDTO = cotizacionDN.agregar(cotizacionDTO);
						cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion().toString());
						buffer.append("<id>1</id><description><![CDATA[La solicitud se asign\u00f3 correctamente.]]></description>");
					}
					else{
						cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
						cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
						cotizacionDTO.setCodigoUsuarioOrdenTrabajo(cotizacionForm.getCodigoUsuarioOrdenTrabajo());
						//Estatus 11 corresponde a Cotizacion asignada
						cotizacionDTO.setClaveEstatus((short)11);
						OrdenTrabajoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).actualizarCotizacion(cotizacionDTO);
						buffer.append("<id>1</id><description><![CDATA[La orden de trabajo se asign\u00f3 correctamente.]]></description>");
					}
				}
			} catch (ExcepcionDeAccesoADatos e) {
				error=true;
				e.printStackTrace();
			} catch (SystemException e) {
				error=true;
				e.printStackTrace();
			} catch (Exception e){error=true;
				e.printStackTrace();
			}
			if(error)
				buffer.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al asignar la orden de trabajo.]]></description>");
			buffer.append("</item></response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			try {
				response.getWriter().write(buffer.toString());
			} catch (IOException e) {e.printStackTrace();}
		}
		
		public ActionForward mostrarPersona(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			String idPadre = request.getParameter("idPadre");
			String tipoDireccion = request.getParameter("tipoDireccion");
			String descripcionPadre = request.getParameter("descripcionPadre");
			Usuario usuario = getUsuarioMidas(request);
			try {
				if (!UtileriasWeb.esCadenaVacia(descripcionPadre) && !UtileriasWeb.esCadenaVacia(tipoDireccion) && (!UtileriasWeb.esCadenaVacia(idPadre))){
					if (descripcionPadre.equals("cotizacion")){
						CotizacionDTO cotizacionDTO = new CotizacionDTO();
						CotizacionSN cotizacionSN = new CotizacionSN(usuario.getNombreUsuario());
						cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idPadre));
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
						/*cotizacionForm.setDireccionGeneral(new DireccionForm());
						cotizacionForm.setPersonaGeneral(new PersonaForm());*/
						cotizacionForm.setClienteGeneral(new ClienteForm());
						cotizacionForm.setIdToCotizacion(idPadre);
						ClienteDTO cliente=null;
						if (tipoDireccion.equals("3")){
							try {

								if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
									cliente = new ClienteDTO();
									cliente.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
									cliente.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
									cliente = ClienteDN.getInstancia().verDetalleCliente(cliente, UtileriasWeb.obtieneNombreUsuario(request));
								}else{
									cliente = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), UtileriasWeb.obtieneNombreUsuario(request));					
								}

							} catch (Exception exc) {
							}
							if (cliente == null)	cliente = new ClienteDTO();
							ClienteAction.poblarClienteForm(cliente, cotizacionForm.getClienteGeneral());
							/*DireccionAction.poblarDireccionForm(cotizacionForm.getDireccionGeneral(), cotizacionDTO.getDireccionAseguradoDTO());
							poblarPersonaForm(cotizacionForm.getPersonaGeneral(), cotizacionDTO.getPersonaAseguradoDTO());*/
							cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
							if (cotizacionDTO.getIdToPersonaAsegurado()!= null && cotizacionDTO.getIdToPersonaAsegurado().doubleValue()>0){
								if(cotizacionDTO.getIdToPersonaContratante() == null || cotizacionDTO.getIdToPersonaContratante().doubleValue() == 0){
									cotizacionDTO.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaAsegurado());
									cotizacionDTO.setIdDomicilioContratante(cotizacionDTO.getIdDomicilioAsegurado());
									cotizacionSN.modificar(cotizacionDTO);
								}
							}						
						}
						else if (tipoDireccion.equals("1")){
							try {
								if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
									cliente = new ClienteDTO();
									cliente.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
									cliente.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
									cliente = ClienteDN.getInstancia().verDetalleCliente(cliente, UtileriasWeb.obtieneNombreUsuario(request));
								}else{
									cliente = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), UtileriasWeb.obtieneNombreUsuario(request));					
								}

							} catch (Exception exc) {
							}
							if (cliente == null)	cliente = new ClienteDTO();
							ClienteAction.poblarClienteForm(cliente, cotizacionForm.getClienteGeneral());
							/*DireccionAction.poblarDireccionForm(cotizacionForm.getDireccionGeneral(), cotizacionDTO.getDireccionContratanteDTO());
							poblarPersonaForm(cotizacionForm.getPersonaGeneral(),cotizacionDTO.getPersonaContratanteDTO());*/
						}
						else if (tipoDireccion.equals("2")){
							cotizacionForm.setDireccionCobro(new DireccionForm());
							DireccionAction.poblarDireccionForm(cotizacionForm.getDireccionCobro(), cotizacionDTO.getDireccionCobroDTO());
						}
						/*cotizacionForm.getDireccionGeneral().setDescripcionPadre(descripcionPadre);
						cotizacionForm.getDireccionGeneral().setCodigoDireccion(tipoDireccion);
						cotizacionForm.getDireccionGeneral().setIdPadre(idPadre);
						cotizacionForm.getPersonaGeneral().setDescripcionPadre(descripcionPadre);
						cotizacionForm.getPersonaGeneral().setCodigoPersona(tipoDireccion);
						cotizacionForm.getPersonaGeneral().setIdPadre(idPadre);*/
						cotizacionForm.getClienteGeneral().setDescripcionPadre(descripcionPadre);
						//cotizacionForm.getClienteGeneral().setTipoDireccion(tipoDireccion);
						cotizacionForm.getClienteGeneral().setCodigoPersona(tipoDireccion);
						cotizacionForm.getClienteGeneral().setIdPadre(idPadre);
						//Se usar� un "0" para identificar que es un nuevo registro
						if (UtileriasWeb.esCadenaVacia(cotizacionForm.getClienteGeneral().getIdCliente()))
							cotizacionForm.getClienteGeneral().setIdCliente("0");
						/*if (UtileriasWeb.esCadenaVacia(cotizacionForm.getDireccionGeneral().getIdToDireccion()))
							cotizacionForm.getDireccionGeneral().setIdToDireccion("0");
						if (UtileriasWeb.esCadenaVacia(cotizacionForm.getPersonaGeneral().getIdToPersona()))
							cotizacionForm.getPersonaGeneral().setIdToPersona("0");*/
					}
				}
				if(request.getParameter("origen") != null) {
					request.setAttribute("origen", "COT");
				}
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (Exception e){
				e.printStackTrace();
			}
			return mapping.findForward(reglaNavegacion);
		}
		
		public ActionForward mostrarAgregarPersona(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			String idPadre = request.getParameter("idPadre");
			String codigoPersona = request.getParameter("codigoPersona");
			String idToPersona = request.getParameter("idToPersona");
			String descripcionPadre = request.getParameter("descripcionPadre");
			PersonaForm personaForm=(PersonaForm) form;
			try {
				if (!UtileriasWeb.esCadenaVacia(idToPersona))
					//Si el id de la direccion es diferente de cero, buscar el registro para mostrarlo en la JSP
					if (!idToPersona.equals("0"))
						poblarPersonaForm(personaForm, PersonaDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idToPersona)));
				//Verificar a qu� entidad pertenecer� la persona
				if (!UtileriasWeb.esCadenaVacia(descripcionPadre) && !UtileriasWeb.esCadenaVacia(idPadre) && !UtileriasWeb.esCadenaVacia(codigoPersona)){
					personaForm.setIdPadre(idPadre);
					personaForm.setCodigoPersona(codigoPersona);
					personaForm.setDescripcionPadre(descripcionPadre);
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
			return mapping.findForward(reglaNavegacion);
		}

		public ActionForward mostrarBuscarPersona(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			String idPadre = request.getParameter("idPadre");
			String codigo = request.getParameter("codigoPersona");
			ClienteForm clienteForm=(ClienteForm) form;
			//Verificar a qu� entidad pertenecer� la persona
			if (!UtileriasWeb.esCadenaVacia(idPadre)){
				clienteForm.setIdPadre(idPadre);
				clienteForm.setCodigoPersona(codigo);
			}
			return mapping.findForward(reglaNavegacion);
		}

		public void agregarPersona(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			PersonaForm personaForm=(PersonaForm) form;
			PersonaDTO personaDTO = new PersonaDTO();
			PersonaDN personaDN = PersonaDN.getInstancia();
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
			try {
				if (!UtileriasWeb.esCadenaVacia(personaForm.getIdToPersona())){	
					if (!personaForm.getIdToPersona().equals("0")){ //Actualizacion de registro
						personaDTO = personaDN.getPorId(UtileriasWeb.regresaBigDecimal(personaForm.getIdToPersona()));
						poblarPersonaDTO(personaDTO, personaForm);
						personaDN.modificar(personaDTO);
						buffer.append("1</id><description><![CDATA[Los datos se actualizaron correctamente.]]></description>");
					}
					else{	//Nuevo registro
						poblarPersonaDTO(personaDTO, personaForm);
						personaDTO.setIdToPersona(null);
						personaDTO = personaDN.agregar(personaDTO);
						buffer.append("1</id><description><![CDATA[Los datos se registraron correctamente.]]></description>");
					}
				}
				//Si se recibe un idPadre, se verifica la descripcion del padre y se registrar� la nueva persona a la entidad correspondiente.
				//La persona ser� la correspondiente al c�digo contenido en el form.
				if (!UtileriasWeb.esCadenaVacia(personaForm.getIdPadre()) && !UtileriasWeb.esCadenaVacia(personaForm.getDescripcionPadre())){
					if (personaForm.getDescripcionPadre().equals("cotizacion")){
						CotizacionDTO cotizacionDTO = new CotizacionDTO();
						cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(personaForm.getIdPadre()));
						CotizacionSN cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
						cotizacionSN.modificar(cotizacionDTO);
						
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						if (cotizacionDTO.getIdToPersonaAsegurado()!= null && cotizacionDTO.getIdToPersonaAsegurado().doubleValue()>0){
							if(cotizacionDTO.getIdToPersonaContratante() == null || cotizacionDTO.getIdToPersonaContratante().doubleValue() == 0){
								cotizacionDTO.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaAsegurado());
								cotizacionSN.modificar(cotizacionDTO);
							}
						}
						switch (Integer.valueOf(personaForm.getCodigoPersona())){
							case 1:	//"1"-Persona contratante.
								//TODO se cambi� la tabla de cotizacion
								//cotizacionDTO.setPersonaContratanteDTO(personaDTO);
								break;
							case 3: //"3"-Persona asegurado
								//TODO se cambi� la tabla de cotizacion
								//cotizacionDTO.setPersonaAseguradoDTO(personaDTO);
								break;
						}
						
					}
					else if (personaForm.getDescripcionPadre().startsWith("proveedorInspeccion")){
						//Si es un nuevo proveedor, a�n no se tiene su ID, se usar� la sesi�n para guardar el DTO.
						if (personaForm.getDescripcionPadre().endsWith("Nuevo")){
							request.getSession().setAttribute("personaProveedor", personaDTO);
						}
						else{
							ProveedorInspeccionDTO proveedorDTO = new ProveedorInspeccionDTO();
							proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().getPorId(UtileriasWeb.regresaBigDecimal(personaForm.getIdPadre()));
							proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
							proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarDireccionDTO(proveedorDTO);
							proveedorDTO.setPersonaDTO(personaDTO);
							ProveedorInspeccionDN.getINSTANCIA().modificar(proveedorDTO);
						}
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (Exception e){
				buffer.append("2</id><description><![CDATA[Ocurri\u00f3 un error al registrar a la persona.\nVerifique los datos.]]></description>");
			}
			buffer.append("</item></response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			try {
				response.getWriter().write(buffer.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public void asignarCliente(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
	        StringBuffer buffer = new StringBuffer();
	        String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
	        String idCliente = request.getParameter("idCliente");
	        String idToCotizacion = request.getParameter("idToCotizacion");
	        String codigoCliente = request.getParameter("codigoCliente");
	        buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");
	        try {
	        	
	            CotizacionDTO cotizacionDTO = new CotizacionDTO();
	            cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario)
	                .getPorId(UtileriasWeb.regresaBigDecimal(idToCotizacion));
	            if (idCliente != null){
	                //Registro existente.
	                if (!idCliente.equals("0")){

						ClienteDTO clienteDTO = null;
						// 3 = Asegurado ; 1 = Contratante
						if (codigoCliente.equals("3")) {
							if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
								clienteDTO = new ClienteDTO();
								clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
								clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
								clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, nombreUsuario);
							}else{
								clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), nombreUsuario);
							}											
						}else if (codigoCliente.equals("1")) {
							if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
								clienteDTO = new ClienteDTO();
								clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
								clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
								clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, nombreUsuario);
							}else{
								clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), nombreUsuario);
							}											
						}
	                    String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	                    nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	                    nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
	                    
	                    // 3 = Asegurado ; 1 = Contratante
	                    if (codigoCliente.equals("3")){
	                        cotizacionDTO.setIdToPersonaAsegurado(UtileriasWeb.regresaBigDecimal(idCliente));                                                
	                        cotizacionDTO.setNombreAsegurado(nombreCliente);
	                    }
	                    else if(codigoCliente.equals("1")){
	                        cotizacionDTO.setIdToPersonaContratante(UtileriasWeb.regresaBigDecimal(idCliente));
	                        cotizacionDTO.setNombreContratante(nombreCliente);
	                    }
	                }
	                //Nuevo Registro
	                else{
	                    ClienteDTO clienteDTO = new ClienteDTO();
	                    CotizacionForm cotizacion = (CotizacionForm) form;
	                    if (cotizacion.getClienteGeneral() != null){
	                        ClienteAction.poblarClienteDTO(cotizacion.getClienteGeneral(), clienteDTO);
	                        BigDecimal idClienteRegistrado = ClienteDN.getInstancia().agregar(clienteDTO, nombreUsuario);
	                        String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	                        nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	                        nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
	                        if (idClienteRegistrado != null){
	                            // 3 = Asegurado ; 1 = Contratante
	                        	if (codigoCliente.equals("3")){
	                                cotizacionDTO.setIdToPersonaAsegurado(idClienteRegistrado);
	                                cotizacionDTO.setNombreAsegurado(nombreCliente);
	                            }
	                            else if(codigoCliente.equals("1")){
	                                cotizacionDTO.setIdToPersonaContratante(idClienteRegistrado);
	                                cotizacionDTO.setNombreContratante(nombreCliente);
	                            }
	                        }
	                        else    throw new Exception();    
	                    }
	                }
	                CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).modificar(cotizacionDTO);
	                buffer.append("1</id><description><![CDATA[La operaci\u00f3n se realiz\u00f3 correctamente.]]></description>");
	            }
	        } catch (SystemException e) {
	            e.printStackTrace();
	        } catch (Exception e){
	            buffer.append("2</id><description><![CDATA[Ocurri\u00f3 un error al registrar al cliente.\nVerifique los datos.]]></description>");
	        }
	        buffer.append("</item></response>");
	        response.setContentType("text/xml");
	        response.setHeader("Cache-Control", "no-cache");
	        response.setContentLength(buffer.length());
	        try {
	            response.getWriter().write(buffer.toString());
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
		/**
		 * Method cargarSeccionesPorInciso
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return void
		 */

		public void cargarSeccionesPorInciso(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			//Funciona en conjunto con el grid javascript (en la funci�n mostrarSeccionesPorInciso(id){}) para cargar las secciones dado un inciso

			String numeroIncisoStr = request.getParameter("numeroInciso");
			String idToCotizacionStr = request.getParameter("idToCotizacion");
			List<SeccionCotizacionDTO> seccionCotizacionDTOList;
			if (!UtileriasWeb.esCadenaVacia(numeroIncisoStr) && !UtileriasWeb.esCadenaVacia(idToCotizacionStr)){
				try {
					BigDecimal numeroInciso = UtileriasWeb.regresaBigDecimal(numeroIncisoStr);
					BigDecimal idToCotizacion = UtileriasWeb.regresaBigDecimal(idToCotizacionStr);
					
					seccionCotizacionDTOList = SeccionCotizacionDN.getInstancia().listarPorCotizacionNumeroInciso(idToCotizacion, numeroInciso);
					
					String json = getJsonSecciones(seccionCotizacionDTOList);
					response.setContentType("text/json");
					PrintWriter pw;
					pw = response.getWriter();
					
					pw.write(json);
					pw.flush();
					pw.close();
					
				} catch (ExcepcionDeAccesoADatos e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				} catch (IOException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				}
			}
			
		}
		
		
		/**
		 * Method modificarSeccion
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return void
		 */
		public void modificarSeccion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			//Funciona en conjunto con el grid javascript (en la funci�n mostrarSeccionesPorInciso(id){}) para actualizar el estatus de obligatoriedad de una secci�n de un inciso
			String claveObligatoriedadStr = request.getParameter("obligatoriedad");
			String claveContratoStr = request.getParameter("contratada");
			
			if (!UtileriasWeb.esCadenaVacia(claveObligatoriedadStr)){
				Integer claveObligatoriedad = new Integer(claveObligatoriedadStr);
				if (claveObligatoriedad == 0 || claveObligatoriedad == 1){
					String id = request.getParameter("idSeccionCot");
					String[] ids= id.split(",");
					BigDecimal idToCotizacion = new BigDecimal(ids[0]);
				    BigDecimal numeroInciso = new BigDecimal(ids[1]);
				    BigDecimal idToSeccion = new BigDecimal(ids[2]);
					SeccionCotizacionDTOId seccionCotizacionDTOId = new SeccionCotizacionDTOId(idToCotizacion, numeroInciso, idToSeccion);
					CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
					SeccionCotizacionDTO seccionCotizacionDTO;
					try {
						seccionCotizacionDTO = cotizacionDN.getSeccionCotizacionPorId(seccionCotizacionDTOId);
							
						Short claveContrato = new Short("0");
						if (claveContratoStr.equals("true"))
							claveContrato = 1;
						IncisoCotizacionDTO incisoCot = new IncisoCotizacionDTO();
						incisoCot.setId(new IncisoCotizacionId());
						incisoCot.getId().setIdToCotizacion(idToCotizacion);
						incisoCot.getId().setNumeroInciso(numeroInciso);
						incisoCot = IncisoCotizacionDN.getInstancia().getPorId(incisoCot);
						seccionCotizacionDTO.setIncisoCotizacionDTO(incisoCot);
						seccionCotizacionDTO.setClaveContrato(claveContrato);
						cotizacionDN.actualizarSeccionCotizacion(seccionCotizacionDTO);
					} catch (SystemException e) {
						UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
					}
					
				}
			}
			
		}
		
		
		/**
		 * Method mostrarDatosGenerales
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward mostrarDatosGenerales(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			CotizacionSN cotizacionSN;
			String idToCotizacion = request.getParameter("id");
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			
			try {
				cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
				cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
				this.poblarForm(cotizacionDTO, cotizacionForm,request);
				
				List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
				documentoDigitalSolicitudDTOList = DocumentoDigitalSolicitudDN.getInstancia().listarDocumentosDigitalesPorSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
				request.setAttribute("documentosList", documentoDigitalSolicitudDTOList);
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
			
			return mapping.findForward(reglaNavegacion);
		}
		
		/**
		 * Method listarDocumentosDigitalesComplementarios
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
	public ActionForward listarDocumentosDigitalesComplementarios(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTOList = new ArrayList<DocumentoDigitalCotizacionDTO>();
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTOList = new ArrayList<DocumentoDigitalSolicitudDTO>();
			CotizacionSN cotizacionSN;
			String idToCotizacion = request.getParameter("id");
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			
			try {
				cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
				cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
				cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
			
				this.poblarForm(cotizacionDTO, cotizacionForm, request);
				cotizacionForm.setIdToCotizacion(idToCotizacion);
				
				documentoDigitalCotizacionDTOList = DocumentoDigitalCotizacionDN.getInstancia().listarDocumentosCotizacionPorCotizacion(cotizacionDTO.getIdToCotizacion());
				documentoDigitalSolicitudDTOList = DocumentoDigitalSolicitudDN.getInstancia().listarDocumentosDigitalesPorSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
				
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
			request.setAttribute("documentosDigitalesSolicitudList", documentoDigitalSolicitudDTOList);
			request.setAttribute("documentosDigitalesCotizacionList", documentoDigitalCotizacionDTOList);
			
			return mapping.findForward(reglaNavegacion);
		}
		
		/**
		 * Method borrarDocumentoComplementario
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward borrarDocumentoComplementario(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			
			ActionForward forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));
			String idToControlArchivo = request.getParameter("idControlArchivo");
			String idToCotizacion = request.getParameter("idToCotizacion");
			String idDocumentoDigitalCotizacion = request.getParameter("idDocumentoDigitalCotizacion");
			if (idToCotizacion != null && !UtileriasWeb.esCadenaVacia(idToCotizacion) && idToControlArchivo != null && !UtileriasWeb.esCadenaVacia(idToControlArchivo)
					&& idDocumentoDigitalCotizacion != null && !UtileriasWeb.esCadenaVacia(idDocumentoDigitalCotizacion)){
				DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO = new DocumentoDigitalCotizacionDTO();
				ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
				
				try {
					documentoDigitalCotizacionDTO = DocumentoDigitalCotizacionDN.getInstancia().getPorId(new BigDecimal(idDocumentoDigitalCotizacion));
					DocumentoDigitalCotizacionDN.getInstancia().borrar(documentoDigitalCotizacionDTO);
					
					controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(new BigDecimal(idToControlArchivo));
					ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);
					String path;
					path = forward.getPath();
					forward.setPath(path + "?id=" + idToCotizacion);
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
					forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
				}
			}
			
			return forward;		
		}
		
		/**
		 * Method modificar
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			
			return mapping.findForward(reglaNavegacion);
		}
		
		
		/**
		 * Method mostrarRegistrarInspeccion
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward mostrarRegistrarInspeccion(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			
			InspeccionForm inspeccionForm = (InspeccionForm) form;
			String idToCotizacion = request.getParameter("idToCotizacion");
			String numeroInciso = request.getParameter("numeroInciso");
			IncisoCotizacionDTO incisoCotizacionDTO = new IncisoCotizacionDTO();
			Usuario usuario = getUsuarioMidas(request);
			//PersonaDTO asegurado = new PersonaDTO();
			AgenteDTO agente = new AgenteDTO();
			ClienteDTO clienteAsegurado = new ClienteDTO();
			try {
				
				CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
					.getPorId(new BigDecimal(idToCotizacion));
				try{

					if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
						clienteAsegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
						clienteAsegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
						clienteAsegurado = ClienteDN.getInstancia().verDetalleCliente(clienteAsegurado, usuario.getNombreUsuario());
					}else{
						clienteAsegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), usuario.getNombreUsuario());
					}					
					
				}catch(Exception exc){}
					
				try{
					Usuario usuario2 = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
					AgenteDN agenteDN = AgenteDN.getInstancia();
					agente = new AgenteDTO();
					agente.setIdTcAgente(Integer.valueOf(cotizacionDTO.getSolicitudDTO().getCodigoAgente().toBigInteger().toString()));
					agente = agenteDN.verDetalleAgente(agente, usuario2.getNombreUsuario());//getPorId(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdAgente()));
				//agente = AgenteDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getCodigoAgente());
				}catch(Exception e){}
				
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				incisoCotizacionId.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
				incisoCotizacionId.setNumeroInciso(new BigDecimal(numeroInciso));
				
				incisoCotizacionDTO.setId(incisoCotizacionId);		
				incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacionDTO);
				InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = null;
				List<InspeccionIncisoCotizacionDTO> inspeccionIncisoCotizacionList = InspeccionIncisoCotizacionDN.getINSTANCIA().getPorIdIncisoCotizacion(incisoCotizacionDTO);
				if (inspeccionIncisoCotizacionList.size() > 0){
					inspeccionIncisoCotizacionDTO = inspeccionIncisoCotizacionList.get(0); // En teor�a solo deber�a regresar 1 registro, aunque el m�todo utilizado en el DN y el SN devuelve una lista
				}
				
				poblarInspeccionForm(clienteAsegurado,agente,incisoCotizacionDTO.getDireccionDTO(), inspeccionForm, inspeccionIncisoCotizacionDTO, request);
				
				
				List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentosAnexosList = new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>();
				if (!UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO) && !UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO.getIdToInspeccionIncisoCotizacion()))
					documentosAnexosList = DocumentoAnexoInspeccionIncisoCotizacionDN.getINSTANCIA().listarPorInspeccion(inspeccionIncisoCotizacionDTO);
					
				request.setAttribute("documentosAnexosList", documentosAnexosList);
				
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
			return mapping.findForward(Sistema.EXITOSO);
		}
		
	public void poblarInspeccionForm(ClienteDTO asegurado, AgenteDTO agente, DireccionDTO direccionInciso, InspeccionForm inspeccionForm, InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO, HttpServletRequest request){
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
			
			inspeccionForm.setFechaActual(dateFormat.format(new Date()));
			
			//Datos de asegurado y agente
			inspeccionForm.setNombre(asegurado.getNombre() + " " + asegurado.getApellidoPaterno() + " " + asegurado.getApellidoMaterno());
			inspeccionForm.setTipoDePersona((asegurado.getClaveTipoPersona()==1)?"Persona f�sica":(asegurado.getClaveTipoPersona()==2)?"Persona moral":"");
			inspeccionForm.setTelefono(asegurado.getTelefono());
			if(agente != null)
				inspeccionForm.setAgenteDeSeguros(agente.getNombre());
			else inspeccionForm.setAgenteDeSeguros("No registrado");
			
			//Datos de la Ubicacion
			if (!UtileriasWeb.esObjetoNulo(direccionInciso)){
				inspeccionForm.setCalleYNumero(direccionInciso.getNombreCalle() + " " + direccionInciso.getNumeroExterior());
			
				inspeccionForm.setColonia(direccionInciso.getNombreColonia());
				inspeccionForm.setCodigoPostal(direccionInciso.getCodigoPostal().toString());
				String ciudadId = StringUtils.leftPad(direccionInciso.getIdMunicipio().toBigInteger().toString(), 5, '0');
				String estadoId = StringUtils.leftPad(direccionInciso.getIdEstado().toBigInteger().toString(),5,'0');
				CiudadDTO ciudad =  CodigoPostalDN.getInstancia().getCiudadPorId(ciudadId);
				EstadoDTO estado = CodigoPostalDN.getInstancia().getEstadoPorId(estadoId);
				inspeccionForm.setCiudad(ciudad.getCityName());
				inspeccionForm.setEstado(estado.getStateName());
			}
			
			if (!UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO)){
				if (!UtileriasWeb.esObjetoNulo(inspeccionIncisoCotizacionDTO.getIdToInspeccionIncisoCotizacion()))
					inspeccionForm.setIdToInspeccionIncisoCotizacion(inspeccionIncisoCotizacionDTO.getIdToInspeccionIncisoCotizacion().toBigInteger().toString());
				else
					inspeccionForm.setIdToInspeccionIncisoCotizacion("-1");
				
				inspeccionForm.setNumeroDeReporte(inspeccionIncisoCotizacionDTO.getNumeroReporte().toBigInteger().toString());
				inspeccionForm.setIdProveedorInspeccion(inspeccionIncisoCotizacionDTO.getProveedorInspeccion().getIdToProveedorInspeccion().toBigInteger().toString());
				inspeccionForm.setPersonaEntrevistada(inspeccionIncisoCotizacionDTO.getNombrePersonaEntrevistada());
				inspeccionForm.setFechaDeLaInspeccion(dateFormat.format(inspeccionIncisoCotizacionDTO.getFechaInspeccion()));
				inspeccionForm.setFechaElaboracionDocumento(dateFormat.format(inspeccionIncisoCotizacionDTO.getFechaReporteInspeccion()));
				inspeccionForm.setResultadoDeLaInspeccion(inspeccionIncisoCotizacionDTO.getClaveResultadoInspeccion().toString());
				inspeccionForm.setComentariosDeLaInspeccion(inspeccionIncisoCotizacionDTO.getComentariosReporte());
			}else{
				inspeccionForm.setIdToInspeccionIncisoCotizacion("-1");
				inspeccionForm.setFechaDeLaInspeccion(dateFormat.format(new Date()));
				inspeccionForm.setFechaElaboracionDocumento(dateFormat.format(new Date()));
			}
			
			if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getIdToCotizacion()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getIdToCotizacion()))
				inspeccionForm.setIdToCotizacionFormateada("COT-" + UtileriasWeb.llenarIzquierda(inspeccionForm.getIdToCotizacion(), "0", 8));
			else
				inspeccionForm.setIdToCotizacionFormateada("No definida");
			
			
		}
		
		/**
		 * Method guardarReporteInspeccion
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward guardarReporteInspeccion(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			String reglaNavegacion = Sistema.EXITOSO;
			InspeccionForm inspeccionForm = (InspeccionForm) form;
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO = new InspeccionIncisoCotizacionDTO();
			request.setAttribute("documentosAnexosList", new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>());
			Usuario usuario = getUsuarioMidas(request);		
			this.poblarInspeccionIncisoCotizacionDTO(inspeccionForm, inspeccionIncisoCotizacionDTO, request);
			
			try {
				inspeccionIncisoCotizacionDTO.setDocumentoAnexoInspeccionIncisoCotizacionList(null);
				inspeccionIncisoCotizacionDTO = InspeccionIncisoCotizacionDN.getINSTANCIA().agregar(inspeccionIncisoCotizacionDTO);
				//PersonaDTO asegurado = null;//inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getPersonaAseguradoDTO();
				AgenteDTO agente = new AgenteDTO();
				try{
					AgenteDN agenteDN = AgenteDN.getInstancia();
					agente.setIdTcAgente(Integer.valueOf(inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getSolicitudDTO().getCodigoAgente().toBigInteger().toString()));
					agente = agenteDN.verDetalleAgente(agente, usuario.getNombreUsuario());//getPorId(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdAgente()));
					//agente = AgenteDN.getInstancia().getPorId(inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getCotizacionDTO().getSolicitudDTO().getCodigoAgente());
				}catch(Exception e){}
				DireccionDTO direccionInciso = inspeccionIncisoCotizacionDTO.getIncisoCotizacion().getDireccionDTO();
				ClienteDTO asegurado = new ClienteDTO();
				try {
					CotizacionDTO cotizacionDTO = inspeccionIncisoCotizacionDTO
					.getIncisoCotizacion().getCotizacionDTO();				
					if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
						asegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
						asegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
						asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, usuario.getNombreUsuario());
					}else{
						asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), usuario.getNombreUsuario());
					}
				} catch (Exception exc) {
				}
				this.poblarInspeccionForm(asegurado, agente, direccionInciso, inspeccionForm, inspeccionIncisoCotizacionDTO, request);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
			
			return mapping.findForward(reglaNavegacion);
		}
		
		
		/**
		 * Method listarDocumentosAnexosInspeccionIncisoCotizacion
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward listarDocumentosAnexosInspeccionIncisoCotizacion(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			String reglaNavegacion = Sistema.EXITOSO;
			
			String idToInspeccionIncisoCotizacion = request.getParameter("id");
			InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacion = new InspeccionIncisoCotizacionDTO();
			inspeccionIncisoCotizacion.setIdToInspeccionIncisoCotizacion(new BigDecimal(idToInspeccionIncisoCotizacion));
			List<DocumentoAnexoInspeccionIncisoCotizacionDTO> documentosAnexosList = new ArrayList<DocumentoAnexoInspeccionIncisoCotizacionDTO>();
			try {
				documentosAnexosList = DocumentoAnexoInspeccionIncisoCotizacionDN.getINSTANCIA().listarPorInspeccion(inspeccionIncisoCotizacion);
				request.setAttribute("documentosAnexosList", documentosAnexosList);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			}
			
			return mapping.findForward(reglaNavegacion);
		}
		
		
		
		protected void poblarForm(CotizacionDTO cotizacionDTO, CotizacionForm cotizacionForm,HttpServletRequest request) throws SystemException{
			if (cotizacionDTO.getIdToCotizacion() != null){
				cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion().toString());
				cotizacionForm.setIdToCotizacionFormateada("COT-" + UtileriasWeb.llenarIzquierda(cotizacionForm.getIdToCotizacion(), "0", 8));
				cotizacionForm.setIdOrdenTrabajoFormateada("ODT-"+ UtileriasWeb.llenarIzquierda(cotizacionForm.getIdToCotizacion(), "0", 8));
			}
			cotizacionForm.setDireccionCobro(new DireccionForm());
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			cotizacionForm.setClienteAsegurado(new ClienteForm());
			if (cotizacionDTO.getIdToPersonaAsegurado() != null){
				ClienteDTO asegurado = null;
				try{
					if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
						asegurado = new ClienteDTO();
						asegurado.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
						asegurado.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
						asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, cotizacionDTO.getCodigoUsuarioCotizacion());
					}else{
						asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), cotizacionDTO.getCodigoUsuarioCotizacion());
					}					
				}catch(Exception exc){asegurado = new ClienteDTO();}
				if (asegurado != null)
					ClienteAction.poblarClienteForm(asegurado, cotizacionForm.getClienteAsegurado());
			}
			cotizacionForm.setClienteContratante(new ClienteForm());
			if (cotizacionDTO.getIdToPersonaContratante() != null){
				ClienteDTO contratante = null;
				try{
					if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
						contratante = new ClienteDTO();
						contratante.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
						contratante.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
						contratante = ClienteDN.getInstancia().verDetalleCliente(contratante, cotizacionDTO.getCodigoUsuarioCotizacion());
					}else{
						contratante = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), cotizacionDTO.getCodigoUsuarioCotizacion());
					}									
				}catch(Exception exc){contratante = new ClienteDTO();}
				if (contratante != null)
					ClienteAction.poblarClienteForm(contratante, cotizacionForm.getClienteContratante());
			}
			Format formatter=null;
			if ( cotizacionDTO.getFechaInicioVigencia() != null){
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				cotizacionForm.setFechaInicioVigencia(formatter.format(cotizacionDTO.getFechaInicioVigencia()));
			}
			if ( cotizacionDTO.getFechaFinVigencia() != null){
				if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy"); 
				cotizacionForm.setFechaFinVigencia(formatter.format(cotizacionDTO.getFechaFinVigencia()));
			}
			if ( cotizacionDTO.getFechaCreacion() != null){
				if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy");
				cotizacionForm.setFechaCreacion(formatter.format(cotizacionDTO.getFechaCreacion()));
			}
			if ( cotizacionDTO.getFechaModificacion() != null){
				if (formatter == null) formatter = new SimpleDateFormat("dd/MM/yyyy");
				cotizacionForm.setFechaModificacion(formatter.format(cotizacionDTO.getFechaModificacion()));
			}
			if(cotizacionDTO.getTipoPolizaDTO() != null) {
				if (cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza() != null){
					cotizacionForm.setIdToTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza().toBigInteger().toString());
					cotizacionForm.setListaMoneda(new MonedaSN().listarMonedasAsociadasTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza()));
					TipoPolizaDTO tipoPolizaDTO = TipoPolizaDN.getInstancia().getPorId(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza());
					cotizacionForm.setTipoDePoliza(tipoPolizaDTO.getDescripcion());
				}
			} else {
				cotizacionForm.setListaMoneda(new ArrayList<MonedaDTO>());
			}
			if (cotizacionDTO.getIdMoneda() != null){
				cotizacionForm.setIdMoneda(cotizacionDTO.getIdMoneda().toBigInteger().toString());
				Short idMoneda = new Short(cotizacionDTO.getIdMoneda().toBigInteger().toString());
				MonedaDTO monedaDTO = MonedaDN.getInstancia().getPorId(idMoneda);
				cotizacionForm.setMoneda(monedaDTO.getDescripcion());
				List<FormaPagoIDTO> listaFormaPago = new ArrayList<FormaPagoIDTO>();
				try{
				//22/12/2009 JLAB, se debe obtener la lista de formas de pago v�lidas para el tipo de moneda de la cotizaci�n.
					listaFormaPago = mx.com.afirme.midas.interfaz.formapago.FormaPagoDN.getInstancia(nombreUsuario).listarTodos(idMoneda);
				}catch(Exception e){}
				cotizacionForm.setListaFormaPago(listaFormaPago);
			}
			if ( cotizacionDTO.getIdFormaPago() != null){
				cotizacionForm.setIdFormaPago(cotizacionDTO.getIdFormaPago().toBigInteger().toString());
				FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
				formaPagoDTO.setIdFormaPago(new Integer(cotizacionDTO.getIdFormaPago().intValue()));
				formaPagoDTO = FormaPagoDN.getInstancia().getPorId(formaPagoDTO);
				cotizacionForm.setFormaPago(formaPagoDTO.getDescripcion());
			}
			if ( cotizacionDTO.getIdMedioPago() != null){
				cotizacionForm.setIdMedioPago(cotizacionDTO.getIdMedioPago().toBigInteger().toString());
				MedioPagoDTO medioPagoDTO = new MedioPagoDTO();
				medioPagoDTO.setIdMedioPago(new Integer(cotizacionDTO.getIdMedioPago().intValue()));
				medioPagoDTO = MedioPagoDN.getInstancia().getPorId(medioPagoDTO);
				try{
					cotizacionForm.setMedioDePago(medioPagoDTO.getDescripcion());
				}catch(Exception exc){cotizacionForm.setMedioDePago("No disponible");}
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getNombreEmpresaContratante()))
				cotizacionForm.setNombreEmpresaContratante(cotizacionDTO.getNombreEmpresaContratante());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getNombreEmpresaAsegurado()))
				cotizacionForm.setNombreEmpresaAsegurado(cotizacionDTO.getNombreEmpresaAsegurado());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getCodigoUsuarioOrdenTrabajo()))
				cotizacionForm.setCodigoUsuarioOrdenTrabajo(cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			//Agente de seguros
			if ( cotizacionDTO.getSolicitudDTO() != null){
				if (cotizacionDTO.getSolicitudDTO().getIdToSolicitud() != null)
					cotizacionForm.setIdToSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud().toString());
				if (cotizacionDTO.getSolicitudDTO().getTelefonoContacto() != null)
					cotizacionForm.setTelefonoContacto(cotizacionDTO.getSolicitudDTO().getTelefonoContacto());
				ProductoDTO productoTMP = new ProductoSN().encontrarPorSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
				cotizacionForm.setProducto(productoTMP.getNombreComercial());
				cotizacionForm.setIdToProducto(productoTMP.getIdToProducto().toString());
				cotizacionForm.setListaTipoPoliza(new TipoPolizaSN().listarVigentesPorIdProducto(productoTMP.getIdToProducto()));
				String nombreSolicitante = "NO DISPONIBLE";
				String apellidoMaterno = cotizacionDTO.getSolicitudDTO().getApellidoMaterno() != null? cotizacionDTO.getSolicitudDTO().getApellidoMaterno() : "";
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoPersona().intValue() == 1)
					nombreSolicitante = cotizacionDTO.getSolicitudDTO().getNombrePersona()+" " +cotizacionDTO.getSolicitudDTO().getApellidoPaterno() +" "+apellidoMaterno;
				else
					nombreSolicitante = cotizacionDTO.getSolicitudDTO().getNombrePersona();
				cotizacionForm.setNombreSolicitante(nombreSolicitante);

				if (cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null){
					try{
						AgenteDN agenteDN = AgenteDN.getInstancia();
						AgenteDTO agenteDTO = new AgenteDTO();
						agenteDTO.setIdTcAgente(Integer.valueOf(cotizacionDTO.getSolicitudDTO().getCodigoAgente().toBigInteger().toString()));
						agenteDTO = agenteDN.verDetalleAgente(agenteDTO, nombreUsuario);
						cotizacionForm.setNombreAgente(agenteDTO.getNombre());
						cotizacionForm.setOficina(agenteDTO.getNumeroOficina());
					}
					catch(Exception exc){
						cotizacionForm.setNombreAgente("NO DISPONIBLE");
						cotizacionForm.setOficina("NO DISPONIBLE");
						}
				}
			}
			if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getCodigoUsuarioCreacion()))
				cotizacionForm.setCodigoUsuarioCreacion(cotizacionDTO.getCodigoUsuarioCreacion());
			if ( !UtileriasWeb.esCadenaVacia(cotizacionDTO.getCodigoUsuarioModificacion()))
				cotizacionForm.setCodigoUsuarioModificacion(cotizacionDTO.getCodigoUsuarioModificacion());
			if ( cotizacionDTO.getClaveEstatus() != null)
				cotizacionForm.setClaveEstatus(cotizacionDTO.getClaveEstatus().toString());
//			cotizacionForm.setListaFormaPago(FormaPagoDN.getInstancia().listarTodos());
			cotizacionForm.setListaMedioPago(MedioPagoDN.getInstancia().listarTodos());
			
			if(cotizacionDTO.getClaveAutRetroacDifer()!= null)
				cotizacionForm.setClaveAutRetroacDifer(cotizacionDTO.getClaveAutRetroacDifer().toString());
			
			if(cotizacionDTO.getClaveAutVigenciaMaxMin() != null)
				cotizacionForm.setClaveAutVigenciaMaxMin(cotizacionDTO.getClaveAutVigenciaMaxMin().toString());

		}
		
		@SuppressWarnings({ "deprecation" })
		protected void poblarDTO(CotizacionForm cotizacionForm, CotizacionDTO cotizacionDTO) throws SystemException{
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToCotizacion()))
					cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToCotizacion()));
				//direcciones
				DireccionDN direccionDN = DireccionDN.getInstancia();

				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToDireccionCobro()))
					cotizacionDTO.setDireccionCobroDTO(direccionDN.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToDireccionCobro())));
				direccionDN = null;
				//personas
				ClienteDN clienteDN = ClienteDN.getInstancia();
				ClienteDTO clienteDTO = null;
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToPersonaAsegurado())){

					if(cotizacionDTO.getIdDomicilioAsegurado() != null && cotizacionDTO.getIdDomicilioAsegurado().intValue() > 0){
						clienteDTO = new ClienteDTO();
						clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaAsegurado());
						clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioAsegurado());
						clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
					}else{
						clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaAsegurado(), cotizacionDTO.getCodigoUsuarioCotizacion());
					}
					if(clienteDTO != null) {
						String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	                    nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	                    nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
	                    cotizacionDTO.setNombreAsegurado(nombreCliente);
					}
				}
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToPersonaContratante())){
					if(cotizacionDTO.getIdDomicilioContratante() != null && cotizacionDTO.getIdDomicilioContratante().intValue() > 0){
						clienteDTO = new ClienteDTO();
						clienteDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
						clienteDTO.setIdDomicilio(cotizacionDTO.getIdDomicilioContratante());
						clienteDTO = ClienteDN.getInstancia().verDetalleCliente(clienteDTO, cotizacionDTO.getCodigoUsuarioCotizacion());
					}else{
						clienteDTO = ClienteDN.getInstancia().verDetalleCliente(cotizacionDTO.getIdToPersonaContratante(), cotizacionDTO.getCodigoUsuarioCotizacion());
					}					
					if(clienteDTO != null) {
						String nombreCliente = clienteDTO.getNombre() != null? clienteDTO.getNombre() + " " : "";
	                    nombreCliente += clienteDTO.getApellidoPaterno() != null? clienteDTO.getApellidoPaterno() + " " : "";
	                    nombreCliente += clienteDTO.getApellidoMaterno() != null? clienteDTO.getApellidoMaterno() : "";
	                    cotizacionDTO.setNombreContratante(nombreCliente);
					}
				}
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToTipoPoliza())){
					TipoPolizaDTO poliza = new TipoPolizaDTO();
					poliza.setIdToTipoPoliza(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToTipoPoliza()));
					cotizacionDTO.setTipoPolizaDTO(TipoPolizaDN.getInstancia().getPorId(poliza));
				}
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToSolicitud()))
					cotizacionDTO.setSolicitudDTO(SolicitudDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToSolicitud())));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMoneda()))
					cotizacionDTO.setIdMoneda(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdMoneda()));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdFormaPago()))
					cotizacionDTO.setIdFormaPago(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdFormaPago()));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getIdMedioPago()))
					cotizacionDTO.setIdMedioPago(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdMedioPago()));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaInicioVigencia())){
					String datos[] = cotizacionForm.getFechaInicioVigencia().split("/");
					Date fecha = new Date();
					fecha.setDate(Integer.valueOf(datos[0]));
					fecha.setMonth(Integer.valueOf(datos[1])-1);
					fecha.setYear(Integer.valueOf(datos[2])-1900);
					cotizacionDTO.setFechaInicioVigencia(fecha);
				}
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaFinVigencia())){
					String datos[] = cotizacionForm.getFechaFinVigencia().split("/");
					Date fecha = new Date();
					fecha.setDate(Integer.valueOf(datos[0]));
					fecha.setMonth(Integer.valueOf(datos[1])-1);
					fecha.setYear(Integer.valueOf(datos[2])-1900);
					cotizacionDTO.setFechaFinVigencia(fecha);
					
				}
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreEmpresaContratante()))
					cotizacionDTO.setNombreEmpresaContratante(cotizacionForm.getNombreEmpresaContratante());
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getNombreEmpresaAsegurado()))
					cotizacionDTO.setNombreEmpresaAsegurado(cotizacionForm.getNombreEmpresaAsegurado());
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioOrdenTrabajo()))
					cotizacionDTO.setCodigoUsuarioOrdenTrabajo(cotizacionForm.getCodigoUsuarioOrdenTrabajo());
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioCotizacion()))
					cotizacionDTO.setCodigoUsuarioCotizacion(cotizacionForm.getCodigoUsuarioCotizacion());
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaCreacion()))
					cotizacionDTO.setFechaCreacion(new Date(cotizacionForm.getFechaCreacion()));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioCreacion()))
					cotizacionDTO.setCodigoUsuarioCreacion(cotizacionForm.getCodigoUsuarioCreacion());
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getFechaModificacion()))
					cotizacionDTO.setFechaModificacion(new Date(cotizacionForm.getFechaModificacion()));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getCodigoUsuarioModificacion()))
					cotizacionDTO.setCodigoUsuarioModificacion(cotizacionForm.getCodigoUsuarioModificacion());
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveEstatus()))
					cotizacionDTO.setClaveEstatus(Short.valueOf(cotizacionForm.getClaveEstatus()));
				if ( !UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveAutRetroacDifer()))
					cotizacionDTO.setClaveAutRetroacDifer(Short.valueOf(cotizacionForm.getClaveAutRetroacDifer()));
				if (!UtileriasWeb.esCadenaVacia(cotizacionForm.getClaveAutVigenciaMaxMin()))
					cotizacionDTO.setClaveAutVigenciaMaxMin(Short.valueOf(cotizacionForm.getClaveAutVigenciaMaxMin()));
			}
		
		public static void poblarPersonaForm(PersonaForm personaForm, PersonaDTO personaDTO){
			if (personaForm != null && personaDTO != null){
				if(personaDTO.getIdToPersona() != null)
					personaForm.setIdToPersona(personaDTO.getIdToPersona().toString());
				if(personaDTO.getClaveTipoPersona() != null){
					personaForm.setClaveTipoPersona(""+personaDTO.getClaveTipoPersona());
					if (personaForm.getClaveTipoPersona().equals("1"))
						personaForm.setDescripcionTipoPersona("Persona f�sica");
					else if (personaForm.getClaveTipoPersona().equals("2"))
						personaForm.setDescripcionTipoPersona("Persona moral");
				}
				if(personaDTO.getNombre() != null)
					personaForm.setNombre(personaDTO.getNombre());
				if(personaDTO.getApellidoPaterno() != null)
					personaForm.setApellidoPaterno(personaDTO.getApellidoPaterno());
				if(personaDTO.getApellidoMaterno() != null)
					personaForm.setApellidoMaterno(personaDTO.getApellidoMaterno());
				if (personaDTO.getFechaNacimiento()!=null){
					Format formatter=null;
					formatter = new SimpleDateFormat("dd/MM/yyyy");
					personaForm.setFechaNacimiento(formatter.format(personaDTO.getFechaNacimiento()));
				}
				if(personaDTO.getCodigoRFC() != null)
					personaForm.setCodigoRFC(personaDTO.getCodigoRFC());
				if (!UtileriasWeb.esCadenaVacia(personaDTO.getEmail()))
					personaForm.setEmail(personaDTO.getEmail());
				if (!UtileriasWeb.esCadenaVacia(personaDTO.getTelefono()))
					personaForm.setTelefono(personaDTO.getTelefono());
			}
		}
		
		@SuppressWarnings("deprecation")
		public static void poblarPersonaDTO(PersonaDTO personaDTO, PersonaForm personaForm) throws SystemException{
			if (personaForm != null && personaDTO != null){
				if (!UtileriasWeb.esCadenaVacia(personaForm.getIdToPersona()))
					personaDTO.setIdToPersona(UtileriasWeb.regresaBigDecimal(personaForm.getIdToPersona()));
				if (!UtileriasWeb.esCadenaVacia(personaForm.getClaveTipoPersona()))
					personaDTO.setClaveTipoPersona(Short.valueOf(personaForm.getClaveTipoPersona()));
				if (!UtileriasWeb.esCadenaVacia(personaForm.getNombre()))
					personaDTO.setNombre(personaForm.getNombre());
				if (!UtileriasWeb.esCadenaVacia(personaForm.getApellidoPaterno()))
					personaDTO.setApellidoPaterno(personaForm.getApellidoPaterno());
				if (!UtileriasWeb.esCadenaVacia(personaForm.getApellidoMaterno()))
					personaDTO.setApellidoMaterno(personaForm.getApellidoMaterno());
				if ( !UtileriasWeb.esCadenaVacia(personaForm.getFechaNacimiento())){
					String datos[] = personaForm.getFechaNacimiento().split("/");
					Date fecha = new Date();
					fecha.setDate(Integer.valueOf(datos[0]));
					fecha.setMonth(Integer.valueOf(datos[1])-1);
					fecha.setYear(Integer.valueOf(datos[2])-1900);
					personaDTO.setFechaNacimiento(fecha);
				}
				if (!UtileriasWeb.esCadenaVacia(personaForm.getCodigoRFC()))
					personaDTO.setCodigoRFC(personaForm.getCodigoRFC());
				if (!UtileriasWeb.esCadenaVacia(personaForm.getEmail()))
					personaDTO.setEmail(personaForm.getEmail());
				if (!UtileriasWeb.esCadenaVacia(personaForm.getTelefono()))
					personaDTO.setTelefono(personaForm.getTelefono());
			}
		}
		
		/**
		 * Method getJsonSecciones
		 * @param seccionCotizacionDTOList (List<SeccionCotizacionDTO>)
		 * @return String
		 * @throws SystemException 
		 * @throws ExcepcionDeAccesoADatos 
		 */
	private String getJsonSecciones(List<SeccionCotizacionDTO> seccionCotizacionDTOList) throws ExcepcionDeAccesoADatos, SystemException{

			MidasJsonBase json = new MidasJsonBase();
			
			if (seccionCotizacionDTOList != null && seccionCotizacionDTOList.size() > 0){
				for (SeccionCotizacionDTO seccionCotizacionDTO : seccionCotizacionDTOList) {
					MidasJsonRow row = new MidasJsonRow();
					SeccionDN seccionDN = SeccionDN.getInstancia();
					SeccionDTO seccionDTO = new SeccionDTO();
					seccionDTO.setIdToSeccion(seccionCotizacionDTO.getId().getIdToSeccion());
					seccionDTO = seccionDN.getPorId(seccionDTO);
					row.setId(seccionCotizacionDTO.getId().getIdToCotizacion() + "," + seccionCotizacionDTO.getId().getNumeroInciso() + "," + seccionCotizacionDTO.getId().getIdToSeccion());
					row.setDatos(seccionDTO.getNombreComercial(),
								 seccionCotizacionDTO.getClaveContrato().toString(),// este campo muestra el estatus del checkbox, si es 0 el estado es no-seleccionado, si es mayor a 1 el estado es seleccionado
								 seccionCotizacionDTO.getClaveObligatoriedad().toString(), //este campo est� oculto, se utiliza en la funcion JS function deshabilitarChecksSeccionesGrid() para deshabilitar los checkbox con claveObligatoriedad mayor a 1
								 seccionCotizacionDTO.getValorSumaAsegurada().toString(),
								 seccionCotizacionDTO.getValorPrimaNeta().toString(),
								 "0",
								 seccionDTO.getClaveBienSeccion().equals("2")?"2":"0"
								);
					json.addRow(row);		
				}
			}
			return json.toString();
		}	
	

		public void poblarInspeccionIncisoCotizacionDTO(InspeccionForm inspeccionForm, InspeccionIncisoCotizacionDTO inspeccionIncisoCotizacionDTO, HttpServletRequest request){
			DateFormat dateFormat = DateFormat.getDateInstance();
			ProveedorInspeccionDTO proveedorInspeccionDTO = new ProveedorInspeccionDTO();
			
			if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getIdToInspeccionIncisoCotizacion()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getIdToInspeccionIncisoCotizacion()))
				inspeccionIncisoCotizacionDTO.setIdToInspeccionIncisoCotizacion(new BigDecimal(inspeccionForm.getIdToInspeccionIncisoCotizacion()));
			
			if (!UtileriasWeb.esCadenaVacia(inspeccionForm.getNumeroInciso()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getNumeroInciso())
					&& !UtileriasWeb.esCadenaVacia(inspeccionForm.getIdToCotizacion()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getIdToCotizacion())){
				IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
				try {
					IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
					incisoCotizacionId.setIdToCotizacion(new BigDecimal(inspeccionForm.getIdToCotizacion()));
					incisoCotizacionId.setNumeroInciso(new BigDecimal(inspeccionForm.getNumeroInciso()));
					incisoCotizacion.setId(incisoCotizacionId);
					incisoCotizacion = IncisoCotizacionDN.getInstancia().getPorId(incisoCotizacion);
					inspeccionIncisoCotizacionDTO.setIncisoCotizacion(incisoCotizacion);
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				}
			}
			
			if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getNumeroDeReporte()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getNumeroDeReporte()))
				inspeccionIncisoCotizacionDTO.setNumeroReporte(new BigDecimal(inspeccionForm.getNumeroDeReporte()));
			
			if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getIdProveedorInspeccion()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getIdProveedorInspeccion())){
				proveedorInspeccionDTO.setIdToProveedorInspeccion(new BigDecimal(inspeccionForm.getIdProveedorInspeccion())); //pendiente de traer por ID
				try {
					proveedorInspeccionDTO = ProveedorInspeccionDN.getINSTANCIA().getPorId(proveedorInspeccionDTO);
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				}
				inspeccionIncisoCotizacionDTO.setProveedorInspeccion(proveedorInspeccionDTO);
			}
			
			inspeccionIncisoCotizacionDTO.setNombrePersonaEntrevistada(inspeccionForm.getPersonaEntrevistada());
			
			try {
				inspeccionIncisoCotizacionDTO.setFechaInspeccion(dateFormat.parse(inspeccionForm.getFechaDeLaInspeccion()));
				inspeccionIncisoCotizacionDTO.setFechaReporteInspeccion(dateFormat.parse(inspeccionForm.getFechaElaboracionDocumento()));
			} catch (ParseException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
			
			if (!UtileriasWeb.esObjetoNulo(inspeccionForm.getResultadoDeLaInspeccion()) && !UtileriasWeb.esCadenaVacia(inspeccionForm.getResultadoDeLaInspeccion()))
				inspeccionIncisoCotizacionDTO.setClaveResultadoInspeccion(new Short(inspeccionForm.getResultadoDeLaInspeccion()));
			
			inspeccionIncisoCotizacionDTO.setComentariosReporte(inspeccionForm.getComentariosDeLaInspeccion());
		}
		
		
		/**
		 * cargarDocAnexo
		 * cargarDocAnexo carga los documentos anexos
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 */
		public ActionForward cargarDocAnexo(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			String reglaNavegacion = Sistema.EXITOSO;
			return mapping.findForward(reglaNavegacion);
		}
		
		
		
		/**
		 * cargarGridAnexos
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 */
		public void cargarGridAnexos(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			DocAnexoCotDN docAnexoCotDN = DocAnexoCotDN.getInstancia();
			
			List<DocAnexoCotDTO> anexos = new ArrayList<DocAnexoCotDTO>();
			
			//List<DocAnexoCotDTO> anexosContratados = new ArrayList<DocAnexoCotDTO>();
			String idToCotizacion = request.getParameter("idToCotizacion");
			String json = "";
			
			try{
				
				anexos = docAnexoCotDN.listarDocumentosAnexosPorCotizacion(UtileriasWeb.regresaBigDecimal(idToCotizacion));
				
				json = getJsonDocAnexos(anexos);
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}catch(IOException e){
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);		
			}
		}
		
		private String getJsonDocAnexos(List<DocAnexoCotDTO> anexos)
			throws SystemException{

			MidasJsonBase json = new MidasJsonBase();
			if (anexos!=null && anexos.size()>0){
				for (DocAnexoCotDTO docAnexoCotDTO : anexos) {
					
					String claveTipoDocAnexo = null;
					
					switch (docAnexoCotDTO.getClaveTipo().intValue()) {
					case 0:
						claveTipoDocAnexo = Sistema.DOC_PRODUCTO;
						break;
					case 1:
						claveTipoDocAnexo = Sistema.DOC_TIPO_POLIZA;
						break;
					case 2:
						claveTipoDocAnexo = Sistema.DOC_COBERTURA;
						break;
					default:
						claveTipoDocAnexo = Sistema.DOC_INVALIDO;
						break;
					}
					
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(docAnexoCotDTO.getId().getIdToControlArchivo().toString());
					row.setDatos(
							docAnexoCotDTO.getId().getIdToCotizacion().toString(),
							docAnexoCotDTO.getClaveObligatoriedad().toString(),
							docAnexoCotDTO.getClaveSeleccion().toString(),
							claveTipoDocAnexo,
							docAnexoCotDTO.getDescripcionDocumentoAnexo()
					);
					json.addRow(row);
					
				}
			}
			
			return json.toString();
		}
		
		@SuppressWarnings("unused")
		private StringBuffer enviaMensajeUsuarioTexAdicional(String id,String descripcion){
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<item>");			
			buffer.append("<id>"+id+"</id><description><![CDATA["+descripcion+"]]></description>");			
			buffer.append("</item>");
			buffer.append("</response>");
			return buffer;
		}
		
		/**
		 * cargarTexAdicionalPorAutorizar
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 */
		public ActionForward cargarTexAdicionalPorAutorizar(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			String reglaNavegacion = Sistema.EXITOSO;
			//TODO falta cargar la cotizacion
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			request.setAttribute("usuario", usuario.getNombreUsuario());
			request.setAttribute("fecha", UtileriasWeb.getFechaString(new Date()));
			return mapping.findForward(reglaNavegacion);
		}
		
		
		
		
		/**
		 * cargarTextosAdicionalesPorAutorizar
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 */
		public void cargarTextosAdicionalesPorAutorizar(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
			List<TexAdicionalCotDTO> textosAdicionales = new ArrayList<TexAdicionalCotDTO>();
			
			String idToCotizacion = request.getParameter("idToCotizacion");
			String json = new String();
			try{
				cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				texAdicionalCotDTO.setCotizacion(cotizacionDTO);
				textosAdicionales = texAdicionalCotDN.listarFiltradoPorAutorizar(texAdicionalCotDTO);
				
				json = getJsonTexAdicionalPorAutorizar(textosAdicionales);
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			}catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}catch(IOException e){
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);		
			}
		}
		
		
		
		/**
		 * cargarTexAdicional
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 */
		public ActionForward cargarTexAdicional(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			String reglaNavegacion = Sistema.EXITOSO;
			//UsuarioDTO usuarioDTO = getUsuarioMidas(request);		
			// TODO: el usuario esta nulo validar con andres

			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			request.setAttribute("usuario", usuario.getNombre());
			request.setAttribute("fecha", UtileriasWeb.getFechaString(new Date()));
			return mapping.findForward(reglaNavegacion);
		}
		
		
		
				
		/**
		 * cargarTextosAdicionales
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 */
		public void cargarTextosAdicionales(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			TexAdicionalCotDN texAdicionalCotDN = TexAdicionalCotDN.getInstancia();
			List<TexAdicionalCotDTO> textosAdicionales = new ArrayList<TexAdicionalCotDTO>();
			
			String idToCotizacion = request.getParameter("idToCotizacion");
			String json = new String();
			try{
				cotizacionDTO.setIdToCotizacion(new BigDecimal(idToCotizacion));
				cotizacionDTO = cotizacionDN.getPorId(cotizacionDTO);
				texAdicionalCotDTO.setCotizacion(cotizacionDTO);
				textosAdicionales = texAdicionalCotDN.listarFiltrado(texAdicionalCotDTO);
				
				json = getJsonTexAdicional(textosAdicionales);
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			}catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}catch(IOException e){
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);		
			}
			
		}
		
		private String getJsonTexAdicional(List<TexAdicionalCotDTO> textosAdicionales)
			throws SystemException{

			MidasJsonBase json = new MidasJsonBase();
			if (textosAdicionales!=null && textosAdicionales.size()>0){
				for (TexAdicionalCotDTO texAdicionalCotDTO : textosAdicionales) {
					
					String iconoClaveAutorizacion = null;
					String nombreUsuarioAutorizacion = null;
					
					if (texAdicionalCotDTO.getCodigoUsuarioAutorizacion() != null) {
						nombreUsuarioAutorizacion = texAdicionalCotDTO.getNombreUsuarioModificacion();
					}
					
					switch (texAdicionalCotDTO.getClaveAutorizacion().intValue()) {
					case 1:
						iconoClaveAutorizacion = "/MidasWeb/img/b_pendiente.gif^Pendiente";
						break;
					case 7:
						iconoClaveAutorizacion =  "/MidasWeb/img/b_autorizar.gif^Autorizado";
						break;					
					case 8:
						iconoClaveAutorizacion = "/MidasWeb/img/b_rechazar.gif^Rechazado";
						break;
					}
									
					MidasJsonRow row = new MidasJsonRow();
					row.setId(texAdicionalCotDTO.getIdToTexAdicionalCot().toString());
					row.setDatos(	texAdicionalCotDTO.getDescripcionTexto(),
							texAdicionalCotDTO.getNombreUsuarioModificacion(),
							nombreUsuarioAutorizacion,
							iconoClaveAutorizacion,
							UtileriasWeb.getFechaString(texAdicionalCotDTO.getFechaCreacion())
					);
					json.addRow(row);
						
				}
			}
			
			return json.toString();
		}
		
		
		private String getJsonTexAdicionalPorAutorizar(List<TexAdicionalCotDTO> textosAdicionales)
		throws SystemException{
			
			MidasJsonBase json = new MidasJsonBase();
			if (textosAdicionales!=null && textosAdicionales.size()>0){
				for (TexAdicionalCotDTO texAdicionalCotDTO : textosAdicionales) {
					
					String iconoClaveAutorizacion = "/MidasWeb/img/b_rechazar.gif^Rechazado";
					
					if (texAdicionalCotDTO.getClaveAutorizacion().shortValue()==1) {
						iconoClaveAutorizacion = "/MidasWeb/img/b_pendiente.gif^Pendiente";
					}
					
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(texAdicionalCotDTO.getIdToTexAdicionalCot().toString());
					row.setDatos(
							"0",
							iconoClaveAutorizacion,
							texAdicionalCotDTO.getDescripcionTexto(),
							texAdicionalCotDTO.getNombreUsuarioCreacion(),
							texAdicionalCotDTO.getNombreUsuarioModificacion(),
							UtileriasWeb.getFechaString(texAdicionalCotDTO.getFechaCreacion()),
							"0"
					);
					json.addRow(row);
					
				}
			}
			
			return json.toString();
		}
		
		/**
		 * Method mostrarValidar
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward mostrarValidar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			return mapping.findForward(Sistema.EXITOSO);
		}
		
		/**
		 * Method mostrarAutorizacionesPendientesPorCotizacion
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward mostrarAutorizacionesPendientesPorCotizacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String idToCotizacion = request.getParameter("id");
			CotizacionForm cotizacionForm = (CotizacionForm) form;
			cotizacionForm.setIdToCotizacion(idToCotizacion);
			if (!UtileriasWeb.esCadenaVacia(idToCotizacion)){
				try {
					CotizacionDTO cotizacion = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
						.getPorId(UtileriasWeb.regresaBigDecimal(idToCotizacion));
					if (cotizacion != null){
						List<Pendiente> listaPendientes = new GestorPendientesDanos().obtenerPendientesPorCotizacion(cotizacion);
						if (listaPendientes != null && listaPendientes.size()>0){
							cotizacionForm.setContienePendientes("true");
							cotizacionForm.setListaPendientes(listaPendientes);
						}
						else{
							cotizacionForm.setContienePendientes("false");
						}
					}
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
			return mapping.findForward(Sistema.EXITOSO);
		}
		
		/**
		 * Method mostrarValidarInspeccion
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward mostrarValidarInspeccion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			
			String idToCotizacion = request.getParameter("idToCotizacion");
			try {
				int consecutivoListaIncisos = 0;
				List<IncisoCotizacionDTO> incisoCotizacionList = IncisoCotizacionDN.getInstancia().getPorPropiedad("id.idToCotizacion", new BigDecimal(idToCotizacion));
				
				// Constantes de INCISOCOTIZACION
				final Short NO_REQUERIDA = 0; // campo claveEstatusInspeccion
				final Short REQUERIDA = 1; // campo claveEstatusInspeccion
				
				final Short CATALOGO = 1;
				final Short VOLUNTARIA = 2; // campo claveTipoOrigenInspeccion
				
				final Short NO_HAY_MENSAJE = 0; // campo claveMensajeInspeccion
				final Short CONSULTAR_SUPERVISOR = 2; // campo claveMensajeInspeccion
				final Short VALIDAR_MADERA_MAS_50_PORCIENTO = 3; // campo claveMensajeInspeccion
				final Short INCISO_EXCLUIDO = 9; // campo claveMensajeInspeccion
				
				// Constantes de SUBGIRO
				final Short EXCLUIDO = 9; // campo claveInspeccion
				final Short INSPECCION = 1; // // campo claveInspeccion, = REQUERIDA
				final Short INSPECCION_SUJETA_A_CONSULTA = 2; // campo claveInspeccion
				final Short INSPECCION_SI_VT_MAYOR_QUE_1MDP = 3; // campo claveInspeccion, VALIDADO POR MARIO GONZALEZ CON JAVIER AYMA EL 5-OCT-2009
				final Short INSPECCION_SI_CONTENIDOS_MAYOR_50_PORCIENTO_MADERA = 4; // campo claveInspeccion
				
				
				final BigDecimal INCENDIO = new BigDecimal("1"); // VALIDADO POR MARIO GONZALEZ CON JAVIER AYMA EL 5-OCT-2009
				final BigDecimal SUBGIRO = new BigDecimal("0"); // // TODO DEFINIR de d�nde proviene esta constante
				
				
				BigDecimal idSeccion = new BigDecimal("0");
				BigDecimal idCobertura = new BigDecimal("0");
				BigDecimal idRiesgo = new BigDecimal("0");
				BigDecimal idSubInciso = new BigDecimal("0");
				BigDecimal idRamo = INCENDIO;
				BigDecimal idSubRamo = new BigDecimal("0");
				Short claveDetalle = new Short("0");
				BigDecimal idDato = SUBGIRO;
				
				for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList) {
					if ((incisoCotizacionDTO.getClaveEstatusInspeccion().equals(NO_REQUERIDA) || incisoCotizacionDTO.getClaveEstatusInspeccion().equals(REQUERIDA)) && !incisoCotizacionDTO.getClaveTipoOrigenInspeccion().equals(VOLUNTARIA)){
						DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
						datoIncisoCotizacionDTO = this.poblarDatoIncisoCotizacionDTO(incisoCotizacionDTO.getId().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso(), idSeccion, idCobertura, idRiesgo, idSubInciso, idRamo, idSubRamo, claveDetalle, idDato);
						datoIncisoCotizacionDTO = DatoIncisoCotizacionDN.getINSTANCIA().getPorId(datoIncisoCotizacionDTO);
						if (!UtileriasWeb.esObjetoNulo(datoIncisoCotizacionDTO)){
							BigDecimal idSubGiro = new BigDecimal(datoIncisoCotizacionDTO.getValor());
							SubGiroDTO subGiroDTO = new SubGiroDTO();
							subGiroDTO.setIdTcSubGiro(idSubGiro);
							subGiroDTO = SubGiroDN.getInstancia().getSubGiroPorId(subGiroDTO);
							Short claveInspeccion = subGiroDTO.getClaveInspeccion();
							
							if (claveInspeccion.equals(EXCLUIDO)){
								incisoCotizacionDTO.setClaveEstatusInspeccion(NO_REQUERIDA);
								incisoCotizacionDTO.setClaveMensajeInspeccion(INCISO_EXCLUIDO);
							}
							if (claveInspeccion.equals(INSPECCION)){
								incisoCotizacionDTO.setClaveEstatusInspeccion(REQUERIDA);
								incisoCotizacionDTO.setClaveMensajeInspeccion(NO_HAY_MENSAJE);
							}
							if (claveInspeccion.equals(INSPECCION_SUJETA_A_CONSULTA)){
								incisoCotizacionDTO.setClaveEstatusInspeccion(NO_REQUERIDA);
								incisoCotizacionDTO.setClaveMensajeInspeccion(CONSULTAR_SUPERVISOR);
							}
							if (claveInspeccion.equals(INSPECCION_SI_VT_MAYOR_QUE_1MDP)){
								List<ParametroGeneralDTO> parametrosGenerales = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("codigoParametroGeneral", "1_MDP"); //TODO Pendiente definir qu� propiedad buscar
								ParametroGeneralDTO mdp = parametrosGenerales.get(0);
								Double sumaAsegurada = IncisoCotizacionDN.getInstancia().calcularSumaAsegurada(incisoCotizacionDTO);
								if (sumaAsegurada.doubleValue() > Double.valueOf(mdp.getValor()).doubleValue())
									incisoCotizacionDTO.setClaveEstatusInspeccion(REQUERIDA);
								else
									incisoCotizacionDTO.setClaveEstatusInspeccion(NO_REQUERIDA);
							}
							if (claveInspeccion.equals(INSPECCION_SI_CONTENIDOS_MAYOR_50_PORCIENTO_MADERA)){
								incisoCotizacionDTO.setClaveEstatusInspeccion(NO_REQUERIDA);
								incisoCotizacionDTO.setClaveMensajeInspeccion(VALIDAR_MADERA_MAS_50_PORCIENTO);
							}
							
						}else
							incisoCotizacionDTO.setClaveEstatusInspeccion(NO_REQUERIDA);
						
						incisoCotizacionDTO.setClaveTipoOrigenInspeccion(CATALOGO);
						incisoCotizacionDTO = IncisoCotizacionDN.getInstancia().modificar(incisoCotizacionDTO);
					}
				}
				StringBuilder ubicacion = new StringBuilder("");
				for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList) {
					ubicacion.delete(0, ubicacion.length());
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccionDTO())){
						ubicacion.append(incisoCotizacionDTO.getDireccionDTO().getNombreCalle()).append(" ").append( 
						incisoCotizacionDTO.getDireccionDTO().getNumeroExterior());
						String ciudad = "";
						if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccionDTO().getIdMunicipio()) && !UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getCiudadPorId(incisoCotizacionDTO.getDireccionDTO().getIdMunicipio().toBigInteger().toString())))
							ciudad = CodigoPostalDN.getInstancia().getCiudadPorId(incisoCotizacionDTO.getDireccionDTO().getIdMunicipio().toBigInteger().toString()).getCityName();
						
						String estado = "";
						String pais = "";
						if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccionDTO().getIdEstado()) && !UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()))){
							estado = CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()).getStateName();
						
							PaisDTO paisDTO = new PaisDTO();
							if (!UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()).getCountryId())){
								paisDTO.setCountryId(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()).getCountryId());
								pais = PaisTipoDestinoTransporteDN.getInstancia().getPaisPorId(paisDTO).getCountryName();
							}
						}
						
						ubicacion.append(", ").append(ciudad).append(", ").append(estado).append(", ").append(pais);
						
					}else{
						ubicacion.append("No definida");
						incisoCotizacionDTO.setDireccionDTO(new DireccionDTO());
					}
					
					incisoCotizacionDTO.getDireccionDTO().setEntreCalles(ubicacion.toString()); //Solamente es usado en tablaValidarInspeccion.jsp para desplegar la ubicaci�n del inciso, solamente transporta la informaci�n, no se realiza operaci�n alguna
					consecutivoListaIncisos++;
					incisoCotizacionDTO.setCodigoUsuarioEstInspeccion(String.valueOf(consecutivoListaIncisos)); //Solamente es usado en tablaValidarInspeccion.jsp para desplegar n�meros consecutivos para los incisos, solamente transporta la informaci�n, no se realiza operaci�n alguna
					
					switch (incisoCotizacionDTO.getClaveEstatusInspeccion()){
						case 0:
							incisoCotizacionDTO.getDireccionDTO().setNombreColonia(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.estatus.noRequiereInspeccion"));
							break;
						case 1:
							incisoCotizacionDTO.getDireccionDTO().setNombreColonia(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.estatus.requiereInspeccion"));
							break;
						case 7:
							incisoCotizacionDTO.getDireccionDTO().setNombreColonia(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.estatus.inspeccionAprobada"));
							break;
						case 8:
							incisoCotizacionDTO.getDireccionDTO().setNombreColonia(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.estatus.inspeccionRechazada"));
							break;
					}
				}
				
				request.setAttribute("incisoCotizacionList", incisoCotizacionList);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
			
			return mapping.findForward(Sistema.EXITOSO);
		}
		
		private DatoIncisoCotizacionDTO poblarDatoIncisoCotizacionDTO(BigDecimal idCotizacion, BigDecimal idInciso, BigDecimal idSeccion, BigDecimal idCobertura, BigDecimal idRiesgo, BigDecimal idSubInciso, BigDecimal idRamo, BigDecimal idSubRamo, Short claveDetalle, BigDecimal idDato){
			DatoIncisoCotizacionId datoIncisoCotizacionId = new DatoIncisoCotizacionId();
			DatoIncisoCotizacionDTO datoIncisoCotizacionDTO = new DatoIncisoCotizacionDTO();
			datoIncisoCotizacionId.setIdToCotizacion(idCotizacion);
			datoIncisoCotizacionId.setNumeroInciso(idInciso);
			datoIncisoCotizacionId.setIdToSeccion(idSeccion);
			datoIncisoCotizacionId.setIdToCobertura(idCobertura);
			datoIncisoCotizacionId.setIdToRiesgo(idRiesgo);
			datoIncisoCotizacionId.setNumeroSubinciso(idSubInciso);
			datoIncisoCotizacionId.setIdTcRamo(idRamo);
			datoIncisoCotizacionId.setIdTcSubramo(idSubRamo);
			datoIncisoCotizacionId.setClaveDetalle(claveDetalle);
			datoIncisoCotizacionId.setIdDato(idDato);
			
			datoIncisoCotizacionDTO.setId(datoIncisoCotizacionId); 
			
			return datoIncisoCotizacionDTO;
		}
		
	


		/**
		 * Method cargarAutorizacionesPendientes
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 */
	public void cargarAutorizacionesPendientes(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String idToCotizacion = request.getParameter("idToCotizacion");
			try {
				List<IncisoCotizacionDTO> incisoCotizacionList = IncisoCotizacionDN.getInstancia().listarAutorizacionesPendientesYRechazadasPorCotizacion(new BigDecimal(idToCotizacion));
				String json = getJsonAutorizacionesPendientes(incisoCotizacionList);
				
				response.setContentType("text/json");
				PrintWriter pw;
				pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (IOException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		
			/**
		 * Method mostrarAutorizacionesPendientes
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 * @return ActionForward
		 */
		public ActionForward mostrarAutorizacionesPendientes(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			String idToCotizacion = request.getParameter("idToCotizacion");
			InspeccionForm inspeccionForm = (InspeccionForm) form;
			inspeccionForm.setIdToCotizacion(idToCotizacion);

			return mapping.findForward(reglaNavegacion);
		}
		
		public String getJsonAutorizacionesPendientes(List<IncisoCotizacionDTO> incisoCotizacionList) throws ExcepcionDeAccesoADatos, SystemException{

			
			MidasJsonBase json = new MidasJsonBase();
			int i = 0;
			if (incisoCotizacionList != null){
				StringBuilder ubicacion = new StringBuilder("");
				for (IncisoCotizacionDTO incisoCotizacionDTO : incisoCotizacionList) {
					i++;
					ubicacion.delete(0,ubicacion.length());
					if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccionDTO())){
						ubicacion.append(incisoCotizacionDTO.getDireccionDTO().getNombreCalle()).append(" ").append(
						incisoCotizacionDTO.getDireccionDTO().getNumeroExterior());
						String ciudad = "";
						if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccionDTO().getIdMunicipio()) && !UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getCiudadPorId(incisoCotizacionDTO.getDireccionDTO().getIdMunicipio().toBigInteger().toString())))
							ciudad = CodigoPostalDN.getInstancia().getCiudadPorId(incisoCotizacionDTO.getDireccionDTO().getIdMunicipio().toBigInteger().toString()).getCityName();
						
						String estado = "";
						String pais = "";
						if (!UtileriasWeb.esObjetoNulo(incisoCotizacionDTO.getDireccionDTO().getIdEstado()) && !UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()))){
							estado = CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()).getStateName();
						
							PaisDTO paisDTO = new PaisDTO();
							if (!UtileriasWeb.esObjetoNulo(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()).getCountryId())){
								paisDTO.setCountryId(CodigoPostalDN.getInstancia().getEstadoPorId(incisoCotizacionDTO.getDireccionDTO().getIdEstado().toBigInteger().toString()).getCountryId());
								pais = PaisTipoDestinoTransporteDN.getInstancia().getPaisPorId(paisDTO).getCountryName();
							}
						}
						
						ubicacion.append(", ").append(ciudad).append(", ").append(estado).append(", ").append(pais);
						
					}else
						ubicacion.append("No definida");
					
					String mensajeRechazada = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.mensaje.solicitudOmisionInspeccionRechazada");
					
					String iconoClaveAutInspeccion = "/MidasWeb/img/blank.png^^javascript: void(0);^_self";
					
					if (incisoCotizacionDTO.getClaveAutInspeccion().intValue() == 8) {
						iconoClaveAutInspeccion = "/MidasWeb/img/b_rechazar.gif^"+mensajeRechazada+"^javascript: void(0);^_self";
					}
					
					MidasJsonRow row = new MidasJsonRow();
					row.setId(incisoCotizacionDTO.getId().getIdToCotizacion() + "_" + incisoCotizacionDTO.getId().getNumeroInciso());
					row.setDatos(
							i + "",
							ubicacion.toString(),
							iconoClaveAutInspeccion,
							"0"
					);
					json.addRow(row);
					
				}
			}
			
			System.out.println("\n\n JSON: " + json);
			
			return json.toString();
		}
		
	
	
	
		private String getCoberturasExcluidasCadena(
			List<CoberturaExcluidaDTO> coberturas) {
			StringBuilder cadenaCoberturas = new StringBuilder("");
		String cadena = "";
		if (coberturas != null) {
			for (CoberturaExcluidaDTO coberturaExcluidaDTO : coberturas) {
				cadenaCoberturas.append(coberturaExcluidaDTO.getId()
						.getIdToCoberturaExcluida().toString()
						).append("_");
			}
			cadena = cadenaCoberturas.toString();
			if (coberturas.size() > 0)
				cadena = cadena.substring(0, cadena.length() - 1);
		}
		return cadena;
	}
	
	private String getCoberturasRequeridasCadena(
			List<CoberturaRequeridaDTO> coberturas) {
		StringBuilder cadenaCoberturas = new StringBuilder("");
		String cadena = "";
		if (coberturas != null) {
			for (CoberturaRequeridaDTO coberturaRequeridaDTO : coberturas) {
				cadenaCoberturas.append(coberturaRequeridaDTO.getId()
						.getIdToCoberturaRequerida().toString()
						).append("_");
			}
			cadena = cadenaCoberturas.toString();
			if (coberturas.size() > 0)
				cadena = cadena.substring(0, cadena.length() - 1);
		}
		return cadena;
	}
}
