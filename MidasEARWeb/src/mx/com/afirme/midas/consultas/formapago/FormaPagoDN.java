package mx.com.afirme.midas.consultas.formapago;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class FormaPagoDN {

	private static final FormaPagoDN INSTANCIA = new FormaPagoDN();
	
	public static FormaPagoDN getInstancia() {

		return FormaPagoDN.INSTANCIA;
	}

	public List<FormaPagoDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		FormaPagoSN formaPagoSN = new FormaPagoSN();
		return formaPagoSN.listarTodos();
	}

	public List<FormaPagoDTO> listarFiltrado(FormaPagoDTO formaPagoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		FormaPagoSN formaPagoSN = new FormaPagoSN();
		return formaPagoSN.listarFiltrado(formaPagoDTO);
	}

	public FormaPagoDTO getPorId(FormaPagoDTO formaPagoDTO) 
			throws ExcepcionDeAccesoADatos, SystemException {
		
		FormaPagoSN formaPagoSN = new FormaPagoSN();
		return formaPagoSN.getPorId(formaPagoDTO);
	}

}
