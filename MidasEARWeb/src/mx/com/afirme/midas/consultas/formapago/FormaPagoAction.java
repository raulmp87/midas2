package mx.com.afirme.midas.consultas.formapago;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class FormaPagoAction  extends MidasMappingDispatchAction{

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		FormaPagoDN formaPagoDN      = FormaPagoDN.getInstancia();
		List<FormaPagoDTO> formaPago = formaPagoDN.listarTodos();
		request.setAttribute("formaPago", formaPago);
		
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		FormaPagoForm formaPagoForm = (FormaPagoForm) form;
		FormaPagoDTO  formaPagoDTO  = new FormaPagoDTO();
		FormaPagoDN   formaPagoDN   = FormaPagoDN.getInstancia();
		
		try{
			this.poblarDTO(formaPagoDTO, formaPagoForm);
			List<FormaPagoDTO> formaPago = formaPagoDN.listarFiltrado(formaPagoDTO);
			request.setAttribute("formaPago", formaPago);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		FormaPagoForm formaPagoForm = (FormaPagoForm) form;
		FormaPagoDTO  formaPagoDTO  = new FormaPagoDTO();
		FormaPagoDN   formaPagoDN   = FormaPagoDN.getInstancia();
		
		try{
			this.poblarDTO(formaPagoDTO,formaPagoForm);
			formaPagoDTO = formaPagoDN.getPorId(formaPagoDTO);
			this.poblarForm(formaPagoDTO, formaPagoForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarForm(FormaPagoDTO formaPagoDTO,
			FormaPagoForm formaPagoForm) {
		
		if(formaPagoDTO.getIdFormaPago() != null)
			formaPagoForm.setIdFormaPago(formaPagoDTO.getIdFormaPago().toString());
		
		if(formaPagoDTO.getDescripcion() != null)
			formaPagoForm.setDescripcion(formaPagoDTO.getDescripcion());
		
	}

	private void poblarDTO(FormaPagoDTO formaPagoDTO,
			FormaPagoForm formaPagoForm) {

		if(!UtileriasWeb.esCadenaVacia(formaPagoForm.getIdFormaPago()))
			formaPagoDTO.setIdFormaPago(Integer.valueOf(formaPagoForm.getIdFormaPago()));
	
		if(!UtileriasWeb.esCadenaVacia(formaPagoForm.getDescripcion()))
			formaPagoDTO.setDescripcion(formaPagoForm.getDescripcion());
			
	}
}
