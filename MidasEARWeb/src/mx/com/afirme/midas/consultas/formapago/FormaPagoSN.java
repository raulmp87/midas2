package mx.com.afirme.midas.consultas.formapago;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class FormaPagoSN {

	private FormaPagoFacadeRemote beanRemoto;
	
	public FormaPagoSN() 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en FormaPagoSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(FormaPagoFacadeRemote.class);
			LogDeMidasWeb.log("bean Remot instanciado", Level.FINEST, null);
		}catch(Exception e){
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<FormaPagoDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos {
	
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<FormaPagoDTO> listarFiltrado(FormaPagoDTO formaPagoDTO) 
			throws ExcepcionDeAccesoADatos {

		try{
			return beanRemoto.listarFiltrado(formaPagoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public FormaPagoDTO getPorId(FormaPagoDTO formaPagoDTO) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findById(formaPagoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
