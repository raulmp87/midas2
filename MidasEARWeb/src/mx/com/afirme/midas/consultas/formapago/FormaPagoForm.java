package mx.com.afirme.midas.consultas.formapago;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class FormaPagoForm extends MidasBaseForm{

	private static final long serialVersionUID = 2701424228541344797L;
	private String idFormaPago;
	private String descripcion;
	
	
	public void setIdFormaPago(String idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public String getIdFormaPago() {
		return idFormaPago;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcion() {
		return descripcion;
	}
}
