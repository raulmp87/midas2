package mx.com.afirme.midas.consultas.rangoluc;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RangoLUCSN {
	private RangoLUCFacadeRemote beanRemoto;

	public RangoLUCSN() throws SystemException {

		try {
			LogDeMidasWeb.log("Entrando en RangoLUCSN - Constructor",
					Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RangoLUCFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}

	public List<RangoLUCDTO> listarTodos() throws ExcepcionDeAccesoADatos {

		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public RangoLUCDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {

		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public RangoLUCDTO getLimiteInferior() throws ExcepcionDeAccesoADatos {

		try {
			return beanRemoto.getLimiteInferior();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
