package mx.com.afirme.midas.consultas.rangoluc;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RangoLUCDN {
	private static final RangoLUCDN INSTANCIA = new RangoLUCDN();

	public static RangoLUCDN getInstancia() {

		return RangoLUCDN.INSTANCIA;
	}

	public RangoLUCDTO getLimiteInferior() throws SystemException,
			ExcepcionDeAccesoADatos {

		RangoLUCSN rangoLUCSN = new RangoLUCSN();
		return rangoLUCSN.getLimiteInferior();
	}
}
