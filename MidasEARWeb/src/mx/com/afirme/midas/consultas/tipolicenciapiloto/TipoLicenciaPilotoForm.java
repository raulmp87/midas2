package mx.com.afirme.midas.consultas.tipolicenciapiloto;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoLicenciaPilotoForm extends MidasBaseForm {

	private static final long serialVersionUID = 3914660185392959583L;

	private String idTipoLicenciaPiloto;
	private String codigoTipoLicenciaPiloto;
	private String descripcionTipoLicenciaPiloto;

	public void setIdTipoLicenciaPiloto(String idTipoLicenciaPiloto) {
		this.idTipoLicenciaPiloto = idTipoLicenciaPiloto;
	}

	public String getIdTipoLicenciaPiloto() {
		return idTipoLicenciaPiloto;
	}

	public void setCodigoTipoLicenciaPiloto(String codigoTipoLicenciaPiloto) {
		this.codigoTipoLicenciaPiloto = codigoTipoLicenciaPiloto;
	}

	public String getCodigoTipoLicenciaPiloto() {
		return codigoTipoLicenciaPiloto;
	}

	public void setDescripcionTipoLicenciaPiloto(
			String descripcionTipoLicenciaPiloto) {
		this.descripcionTipoLicenciaPiloto = descripcionTipoLicenciaPiloto;
	}

	public String getDescripcionTipoLicenciaPiloto() {
		return descripcionTipoLicenciaPiloto;
	}
}
