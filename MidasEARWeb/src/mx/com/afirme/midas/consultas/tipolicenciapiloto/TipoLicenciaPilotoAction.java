package mx.com.afirme.midas.consultas.tipolicenciapiloto;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoLicenciaPilotoAction extends MidasMappingDispatchAction {
	

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoLicenciaPilotoDN tipoLicenciaPilotoDN         = TipoLicenciaPilotoDN.getInstancia();
		List<TipoLicenciaPilotoDTO> tipoLicenciaPilotoDTO = tipoLicenciaPilotoDN.listarTodos();
		request.setAttribute("tipoLicenciaPiloto", tipoLicenciaPilotoDTO);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
	
		TipoLicenciaPilotoForm  tipoLicenciaPilotoForm = (TipoLicenciaPilotoForm) form;
		TipoLicenciaPilotoDTO   tipoLicenciaPilotoDTO  = new TipoLicenciaPilotoDTO();
		TipoLicenciaPilotoDN    tipoLicenciaPilotoDN   = TipoLicenciaPilotoDN.getInstancia();
		
		try{
			this.poblarDTO(tipoLicenciaPilotoForm, tipoLicenciaPilotoDTO);
			request.setAttribute("tipoLicenciaPiloto", tipoLicenciaPilotoDN.listarFiltrado(tipoLicenciaPilotoDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoLicenciaPilotoForm tipoLicenciaPilotoForm = (TipoLicenciaPilotoForm) form;
		TipoLicenciaPilotoDTO  tipoLicenciaPilotoDTO  = new TipoLicenciaPilotoDTO();
		TipoLicenciaPilotoDN   tipoLicenciaPilotoDN   = TipoLicenciaPilotoDN.getInstancia();
		
		try{
			this.poblarDTO(tipoLicenciaPilotoForm, tipoLicenciaPilotoDTO);
			tipoLicenciaPilotoDTO = tipoLicenciaPilotoDN.getPorId(tipoLicenciaPilotoDTO);
			this.poblarForm(tipoLicenciaPilotoDTO,tipoLicenciaPilotoForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	private void poblarForm(TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO,
			TipoLicenciaPilotoForm tipoLicenciaPilotoForm) {
		
		if(tipoLicenciaPilotoDTO.getIdTipoLicenciaPiloto()!= null)
			tipoLicenciaPilotoForm.setIdTipoLicenciaPiloto(tipoLicenciaPilotoDTO.getIdTipoLicenciaPiloto().toString());
		
		if(tipoLicenciaPilotoDTO.getCodigoTipoLicenciaPiloto() != null)
			tipoLicenciaPilotoForm.setCodigoTipoLicenciaPiloto(tipoLicenciaPilotoDTO.getCodigoTipoLicenciaPiloto().toString());
		
		if(tipoLicenciaPilotoDTO.getDescripcionTipoLicenciaPiloto() != null)
			tipoLicenciaPilotoForm.setDescripcionTipoLicenciaPiloto(tipoLicenciaPilotoDTO.getDescripcionTipoLicenciaPiloto());
	}

	private void poblarDTO(TipoLicenciaPilotoForm tipoLicenciaPilotoForm,
			TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO) throws SystemException {
		
		if(!UtileriasWeb.esCadenaVacia(tipoLicenciaPilotoForm.getIdTipoLicenciaPiloto()))
			tipoLicenciaPilotoDTO.setIdTipoLicenciaPiloto(UtileriasWeb.regresaBigDecimal(tipoLicenciaPilotoForm.getIdTipoLicenciaPiloto()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoLicenciaPilotoForm.getCodigoTipoLicenciaPiloto()))
			tipoLicenciaPilotoDTO.setCodigoTipoLicenciaPiloto(UtileriasWeb.regresaBigDecimal(tipoLicenciaPilotoForm.getCodigoTipoLicenciaPiloto()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoLicenciaPilotoForm.getDescripcionTipoLicenciaPiloto()))
			tipoLicenciaPilotoDTO.setDescripcionTipoLicenciaPiloto(tipoLicenciaPilotoForm.getDescripcionTipoLicenciaPiloto().toUpperCase());
	}

}
