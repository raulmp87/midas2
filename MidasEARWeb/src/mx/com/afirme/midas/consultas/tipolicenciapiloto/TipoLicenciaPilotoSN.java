package mx.com.afirme.midas.consultas.tipolicenciapiloto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoLicenciaPilotoSN {

	private TipoLicenciaPilotoFacadeRemote beanRemoto;
	
	public TipoLicenciaPilotoSN () 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en Tipo Servicio TransporteSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoLicenciaPilotoFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch(Exception e){
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<TipoLicenciaPilotoDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<TipoLicenciaPilotoDTO> listadoFiltrado(TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO) 
			throws ExcepcionDeAccesoADatos {
	
		try{
			return beanRemoto.listarFiltrado(tipoLicenciaPilotoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public TipoLicenciaPilotoDTO getPorId(BigDecimal idTipoLicenciaPiloto) 
			throws ExcepcionDeAccesoADatos {
	
		try{
			return beanRemoto.findById(idTipoLicenciaPiloto);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
