package mx.com.afirme.midas.consultas.tipolicenciapiloto;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoLicenciaPilotoDN {
	
	private static final TipoLicenciaPilotoDN INSTANCIA = new TipoLicenciaPilotoDN();

	public static TipoLicenciaPilotoDN getInstancia() {
		
		return TipoLicenciaPilotoDN.INSTANCIA;
	}

	public List<TipoLicenciaPilotoDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoLicenciaPilotoSN tipoLicenciaPilotoSN = new TipoLicenciaPilotoSN();
		return tipoLicenciaPilotoSN.listarTodos();
	}

	public Object listarFiltrado(TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoLicenciaPilotoSN tipoLicenciaPilotoSN = new TipoLicenciaPilotoSN();
		return tipoLicenciaPilotoSN.listadoFiltrado(tipoLicenciaPilotoDTO);
	}

	public TipoLicenciaPilotoDTO getPorId(TipoLicenciaPilotoDTO tipoLicenciaPilotoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoLicenciaPilotoSN tipoLicenciaPilotoSN = new TipoLicenciaPilotoSN();
		return tipoLicenciaPilotoSN.getPorId(tipoLicenciaPilotoDTO.getIdTipoLicenciaPiloto());
	}

}
