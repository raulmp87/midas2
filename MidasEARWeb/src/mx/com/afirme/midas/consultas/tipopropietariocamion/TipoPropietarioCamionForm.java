package mx.com.afirme.midas.consultas.tipopropietariocamion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoPropietarioCamionForm extends MidasBaseForm {

	private static final long serialVersionUID = 5319943511498177696L;

	private String idTipoPropietarioTransporte, codigoTipoPropTransporte,
			descripcionTipoPropietarioTra;

	public void setIdTipoPropietarioTransporte(
			String idTipoPropietarioTransporte) {
		this.idTipoPropietarioTransporte = idTipoPropietarioTransporte;
	}

	public String getIdTipoPropietarioTransporte() {
		return idTipoPropietarioTransporte;
	}

	public void setCodigoTipoPropTransporte(String codigoTipoPropTransporte) {
		this.codigoTipoPropTransporte = codigoTipoPropTransporte;
	}

	public String getCodigoTipoPropTransporte() {
		return codigoTipoPropTransporte;
	}

	public void setDescripcionTipoPropietarioTra(
			String descripcionTipoPropietarioTra) {
		this.descripcionTipoPropietarioTra = descripcionTipoPropietarioTra;
	}

	public String getDescripcionTipoPropietarioTra() {
		return descripcionTipoPropietarioTra;
	}
}
