package mx.com.afirme.midas.consultas.tipopropietariocamion;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class TipoPropietarioCamionAction extends MidasMappingDispatchAction {

	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoPropietarioCamionDN tipoPropietarioCamionDN         = TipoPropietarioCamionDN.getInstancia();
		List<TipoPropietarioCamionDTO> tipoPropietarioCamionDTO = tipoPropietarioCamionDN.listarTodos();
		request.setAttribute("tipoPropietarioCamion", tipoPropietarioCamionDTO);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
	
		TipoPropietarioCamionForm  tipoPropietarioCamionForm = (TipoPropietarioCamionForm) form;
		TipoPropietarioCamionDTO   tipoPropietarioCamionDTO  = new TipoPropietarioCamionDTO();
		TipoPropietarioCamionDN    tipoPropietarioCamionDN   = TipoPropietarioCamionDN.getInstancia();
		
		try{
			this.poblarDTO(tipoPropietarioCamionForm, tipoPropietarioCamionDTO);
			request.setAttribute("tipoPropietarioCamion", tipoPropietarioCamionDN.listarFiltrado(tipoPropietarioCamionDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoPropietarioCamionForm tipoPropietarioCamionForm = (TipoPropietarioCamionForm) form;
		TipoPropietarioCamionDTO  tipoPropietarioCamionDTO  = new TipoPropietarioCamionDTO();
		TipoPropietarioCamionDN   tipoPropietarioCamionDN   = TipoPropietarioCamionDN.getInstancia();
		
		try{
			this.poblarDTO(tipoPropietarioCamionForm, tipoPropietarioCamionDTO);
			tipoPropietarioCamionDTO = tipoPropietarioCamionDN.getPorId(tipoPropietarioCamionDTO);
			this.poblarForm(tipoPropietarioCamionDTO,tipoPropietarioCamionForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarForm(TipoPropietarioCamionDTO tipoPropietarioCamionDTO,
			TipoPropietarioCamionForm tipoPropietarioCamionForm) {
		
		if(tipoPropietarioCamionDTO.getIdTipoPropietarioTransporte()!= null)
			tipoPropietarioCamionForm.setIdTipoPropietarioTransporte(tipoPropietarioCamionDTO.getIdTipoPropietarioTransporte().toString());
		
		if(tipoPropietarioCamionDTO.getCodigoTipoPropTransporte() != null)
			tipoPropietarioCamionForm.setCodigoTipoPropTransporte(tipoPropietarioCamionDTO.getCodigoTipoPropTransporte().toString());
		
		if(tipoPropietarioCamionDTO.getDescripcionTipoPropietarioTra() != null)
			tipoPropietarioCamionForm.setDescripcionTipoPropietarioTra(tipoPropietarioCamionDTO.getDescripcionTipoPropietarioTra());
	}

	private void poblarDTO(TipoPropietarioCamionForm tipoPropietarioCamionForm,
			TipoPropietarioCamionDTO tipoPropietarioCamionDTO) throws SystemException {
		
		if(!UtileriasWeb.esCadenaVacia(tipoPropietarioCamionForm.getIdTipoPropietarioTransporte()))
			tipoPropietarioCamionDTO.setIdTipoPropietarioTransporte(UtileriasWeb.regresaBigDecimal(tipoPropietarioCamionForm.getIdTipoPropietarioTransporte()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoPropietarioCamionForm.getCodigoTipoPropTransporte()))
			tipoPropietarioCamionDTO.setCodigoTipoPropTransporte(UtileriasWeb.regresaBigDecimal(tipoPropietarioCamionForm.getCodigoTipoPropTransporte()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoPropietarioCamionForm.getDescripcionTipoPropietarioTra()))
			tipoPropietarioCamionDTO.setDescripcionTipoPropietarioTra(tipoPropietarioCamionForm.getDescripcionTipoPropietarioTra().toUpperCase());
	}
	
}
