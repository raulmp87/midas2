package mx.com.afirme.midas.consultas.tipopropietariocamion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoPropietarioCamionDN {

	private static final TipoPropietarioCamionDN INTANCIA = new TipoPropietarioCamionDN();
	
	public static TipoPropietarioCamionDN getInstancia() {
		
		return TipoPropietarioCamionDN.INTANCIA;
	}

	public List<TipoPropietarioCamionDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		TipoPropietarioCamionSN tipoPropietarioCamionSN = new TipoPropietarioCamionSN();
		return tipoPropietarioCamionSN.listarTodos();
	}

	public List<TipoPropietarioCamionDTO> listarFiltrado(TipoPropietarioCamionDTO tipoPropietarioCamionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoPropietarioCamionSN tipoPropietarioCamionSN = new TipoPropietarioCamionSN();
		return tipoPropietarioCamionSN.listarFiltrado(tipoPropietarioCamionDTO);
	}

	public TipoPropietarioCamionDTO getPorId(TipoPropietarioCamionDTO tipoPropietarioCamionDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoPropietarioCamionSN tipoPropietarioCamionSN = new TipoPropietarioCamionSN();
		return tipoPropietarioCamionSN.getPorId(tipoPropietarioCamionDTO.getIdTipoPropietarioTransporte());
	}
	

}
