package mx.com.afirme.midas.consultas.tipopropietariocamion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoPropietarioCamionSN {
	
	private TipoPropietarioCamionFacadeRemote beanRemoto;
	
	public TipoPropietarioCamionSN() 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en Tipo Servicio TransporteSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoPropietarioCamionFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch(Exception e){
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}

	public List<TipoPropietarioCamionDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<TipoPropietarioCamionDTO> listarFiltrado(TipoPropietarioCamionDTO tipoPropietarioCamionDTO) 
			throws ExcepcionDeAccesoADatos {
	
		try{
			return beanRemoto.listarFiltrado(tipoPropietarioCamionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public TipoPropietarioCamionDTO getPorId(BigDecimal idTipoPropietarioTransporte) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return null;
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
