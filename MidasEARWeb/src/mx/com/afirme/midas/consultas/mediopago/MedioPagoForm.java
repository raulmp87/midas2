package mx.com.afirme.midas.consultas.mediopago;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class MedioPagoForm extends MidasBaseForm{

	private static final long serialVersionUID = 7001049283633905947L;
	private String idMedioPago;
	private String descripcion;
	
	
	public void setIdMedioPago(String idMedioPago) {
		this.idMedioPago = idMedioPago;
	}
	public String getIdMedioPago() {
		return idMedioPago;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	
	
}
