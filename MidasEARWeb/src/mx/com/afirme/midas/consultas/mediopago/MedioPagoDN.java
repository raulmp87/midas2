package mx.com.afirme.midas.consultas.mediopago;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MedioPagoDN {

	private static final MedioPagoDN INSTANCIA = new MedioPagoDN();
	
	public static MedioPagoDN getInstancia() {
	
		return MedioPagoDN.INSTANCIA;
	}
	

	public List<MedioPagoDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {

		MedioPagoSN medioPagoSN = new MedioPagoSN();
		return medioPagoSN.listarTodos();
	}

	public List<MedioPagoDTO> listarFiltrado(MedioPagoDTO medioPagoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		MedioPagoSN medioPagoSN = new MedioPagoSN();
		return medioPagoSN.listarFiltrado(medioPagoDTO);
	}

	public MedioPagoDTO getPorId(MedioPagoDTO medioPagoDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		MedioPagoSN medioPagoSN = new MedioPagoSN();
		return medioPagoSN.getPorId(medioPagoDTO);
	}
}
