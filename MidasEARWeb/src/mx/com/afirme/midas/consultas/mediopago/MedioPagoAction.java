package mx.com.afirme.midas.consultas.mediopago;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MedioPagoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		MedioPagoDN medioPagoDN      = MedioPagoDN.getInstancia();
		List<MedioPagoDTO> medioPago = medioPagoDN.listarTodos();
		request.setAttribute("medioPago", medioPago);
		
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		MedioPagoForm medioPagoForm = (MedioPagoForm) form;
		MedioPagoDTO  medioPagoDTO  = new MedioPagoDTO();
		MedioPagoDN   medioPagoDN   = MedioPagoDN.getInstancia();
		
		try{
			this.poblarDTO(medioPagoDTO, medioPagoForm);
			List<MedioPagoDTO> medioPago = medioPagoDN.listarFiltrado(medioPagoDTO);
			request.setAttribute("medioPago", medioPago);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		MedioPagoForm medioPagoForm = (MedioPagoForm) form;
		MedioPagoDTO  medioPagoDTO  = new MedioPagoDTO();
		MedioPagoDN   medioPagoDN   = MedioPagoDN.getInstancia();
		
		try{
			this.poblarDTO(medioPagoDTO,medioPagoForm);
			medioPagoDTO = medioPagoDN.getPorId(medioPagoDTO);
			this.poblarForm(medioPagoDTO, medioPagoForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarForm(MedioPagoDTO medioPagoDTO,
			MedioPagoForm medioPagoForm) {
		
		if(medioPagoDTO.getIdMedioPago() != null)
			medioPagoForm.setIdMedioPago(medioPagoDTO.getIdMedioPago().toString());
		
		if(medioPagoDTO.getDescripcion() != null)
			medioPagoForm.setDescripcion(medioPagoDTO.getDescripcion());
		
	}

	private void poblarDTO(MedioPagoDTO medioPagoDTO,
			MedioPagoForm medioPagoForm) {

		if(!UtileriasWeb.esCadenaVacia(medioPagoForm.getIdMedioPago()))
			medioPagoDTO.setIdMedioPago(Integer.valueOf(medioPagoForm.getIdMedioPago()));
	
		if(!UtileriasWeb.esCadenaVacia(medioPagoForm.getDescripcion()))
			medioPagoDTO.setDescripcion(medioPagoForm.getDescripcion());
			
	}

}
