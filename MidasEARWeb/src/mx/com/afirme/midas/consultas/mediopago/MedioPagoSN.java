package mx.com.afirme.midas.consultas.mediopago;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MedioPagoSN {

	private MedioPagoFacadeRemote beanRemoto;
	
	public MedioPagoSN() 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en FormaPagoSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MedioPagoFacadeRemote.class);
			LogDeMidasWeb.log("bean Remot instanciado", Level.FINEST, null);
		}catch(Exception e){
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<MedioPagoDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
		
	}

	public List<MedioPagoDTO> listarFiltrado(MedioPagoDTO medioPagoDTO) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.listarFiltrado(medioPagoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public MedioPagoDTO getPorId(MedioPagoDTO medioPagoDTO) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findById(medioPagoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
