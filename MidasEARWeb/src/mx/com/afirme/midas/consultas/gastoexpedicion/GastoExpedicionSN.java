package mx.com.afirme.midas.consultas.gastoexpedicion;

import java.math.BigDecimal;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class GastoExpedicionSN {
	private GastoExpedicionFacadeRemote beanRemoto;
	
	public GastoExpedicionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(GastoExpedicionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto GastoExpedicionFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public GastoExpedicionDTO getGastoExpedicion(BigDecimal idMoneda,BigDecimal primaNeta) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.getGastoExpedicion(idMoneda,primaNeta);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
