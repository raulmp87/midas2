package mx.com.afirme.midas.consultas.gastoexpedicion;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class GastoExpedicionDN {
	private static final GastoExpedicionDN INSTANCIA = new GastoExpedicionDN();

	public static GastoExpedicionDN getInstancia (){
		return GastoExpedicionDN.INSTANCIA;
	}
	
	public GastoExpedicionDTO getGastoExpedicion(BigDecimal idMoneda,BigDecimal primaNeta) throws ExcepcionDeAccesoADatos, SystemException {
		return new GastoExpedicionSN().getGastoExpedicion(idMoneda, primaNeta);
	}
}
