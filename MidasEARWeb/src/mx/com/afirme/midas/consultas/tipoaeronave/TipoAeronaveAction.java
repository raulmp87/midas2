package mx.com.afirme.midas.consultas.tipoaeronave;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoAeronaveAction extends MidasMappingDispatchAction{
	

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoAeronaveDN tipoAeronaveDN         = TipoAeronaveDN.getInstancia();
		List<TipoAeronaveDTO> tipoAeronaveDTO = tipoAeronaveDN.listarTodos();
		request.setAttribute("tipoAeronave", tipoAeronaveDTO);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
	
		TipoAeronaveForm  tipoAeronaveForm = (TipoAeronaveForm) form;
		TipoAeronaveDTO   tipoAeronaveDTO  = new TipoAeronaveDTO();
		TipoAeronaveDN    tipoAeronaveDN   = TipoAeronaveDN.getInstancia();
		
		try{
			this.poblarDTO(tipoAeronaveForm, tipoAeronaveDTO);
			request.setAttribute("tipoAeronave", tipoAeronaveDN.listarFiltrado(tipoAeronaveDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoAeronaveForm tipoAeronaveForm = (TipoAeronaveForm) form;
		TipoAeronaveDTO  tipoAeronaveDTO  = new TipoAeronaveDTO();
		TipoAeronaveDN   tipoAeronaveDN   = TipoAeronaveDN.getInstancia();
		
		try{
			this.poblarDTO(tipoAeronaveForm, tipoAeronaveDTO);
			tipoAeronaveDTO = tipoAeronaveDN.getPorId(tipoAeronaveDTO);
			this.poblarForm(tipoAeronaveDTO,tipoAeronaveForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarForm(TipoAeronaveDTO tipoAeronaveDTO,
			TipoAeronaveForm tipoAeronaveForm) {
		
		if(tipoAeronaveDTO.getIdTipoAeronave()!= null)
			tipoAeronaveForm.setIdTipoAeronave(tipoAeronaveDTO.getIdTipoAeronave().toString());
		
		if(tipoAeronaveDTO.getCodigoTipoAeronave() != null)
			tipoAeronaveForm.setCodigoTipoAeronave(tipoAeronaveDTO.getCodigoTipoAeronave().toString());
		
		if(tipoAeronaveDTO.getDescripcionTipoAeronave() != null)
			tipoAeronaveForm.setDescripcionTipoAeronave(tipoAeronaveDTO.getDescripcionTipoAeronave());
	}

	private void poblarDTO(TipoAeronaveForm tipoAeronaveForm,
			TipoAeronaveDTO tipoAeronaveDTO) throws SystemException {
		
		if(!UtileriasWeb.esCadenaVacia(tipoAeronaveForm.getIdTipoAeronave()))
			tipoAeronaveDTO.setIdTipoAeronave(UtileriasWeb.regresaBigDecimal(tipoAeronaveForm.getIdTipoAeronave()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoAeronaveForm.getCodigoTipoAeronave()))
			tipoAeronaveDTO.setCodigoTipoAeronave(UtileriasWeb.regresaBigDecimal(tipoAeronaveForm.getCodigoTipoAeronave()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoAeronaveForm.getDescripcionTipoAeronave()))
			tipoAeronaveDTO.setDescripcionTipoAeronave(tipoAeronaveForm.getDescripcionTipoAeronave().toUpperCase());
	}

}
