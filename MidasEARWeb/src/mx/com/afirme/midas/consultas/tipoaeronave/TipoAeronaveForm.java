package mx.com.afirme.midas.consultas.tipoaeronave;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoAeronaveForm extends MidasBaseForm {

	private static final long serialVersionUID = -5144305049185698416L;

	private String idTipoAeronave;
	private String codigoTipoAeronave;
	private String descripcionTipoAeronave;

	public void setIdTipoAeronave(String idTipoAeronave) {
		this.idTipoAeronave = idTipoAeronave;
	}

	public String getIdTipoAeronave() {
		return idTipoAeronave;
	}

	public void setCodigoTipoAeronave(String codigoTipoAeronave) {
		this.codigoTipoAeronave = codigoTipoAeronave;
	}

	public String getCodigoTipoAeronave() {
		return codigoTipoAeronave;
	}

	public void setDescripcionTipoAeronave(String descripcionTipoAeronave) {
		this.descripcionTipoAeronave = descripcionTipoAeronave;
	}

	public String getDescripcionTipoAeronave() {
		return descripcionTipoAeronave;
	}
}
