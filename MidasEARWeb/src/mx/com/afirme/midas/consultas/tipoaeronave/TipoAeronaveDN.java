package mx.com.afirme.midas.consultas.tipoaeronave;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoAeronaveDN {

	private static final TipoAeronaveDN INSTANCIA = new TipoAeronaveDN();

	public static TipoAeronaveDN getInstancia() {
	
		return TipoAeronaveDN.INSTANCIA;
	}

	public List<TipoAeronaveDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoAeronaveSN tipoAeronaveSN = new TipoAeronaveSN();
		return tipoAeronaveSN.listarTodos();
	}

	public Object listarFiltrado(TipoAeronaveDTO tipoAeronaveDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoAeronaveSN tipoAeronaveSN = new TipoAeronaveSN();
		return tipoAeronaveSN.listarFiltrado(tipoAeronaveDTO);
	}

	public TipoAeronaveDTO getPorId(TipoAeronaveDTO tipoAeronaveDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		TipoAeronaveSN tipoAeronaveSN = new TipoAeronaveSN();
		return tipoAeronaveSN.getPorId(tipoAeronaveDTO.getIdTipoAeronave());
	}

}
