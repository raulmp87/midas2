package mx.com.afirme.midas.consultas.tipoaeronave;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoAeronaveSN {

	private TipoAeronaveFacadeRemote beanRemoto;
	
	public TipoAeronaveSN() 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en Tipo Servicio TransporteSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoAeronaveFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch(Exception e){
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<TipoAeronaveDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public Object listarFiltrado(TipoAeronaveDTO tipoAeronaveDTO) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.listarFiltrado(tipoAeronaveDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public TipoAeronaveDTO getPorId(BigDecimal idTipoAeronave) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findById(idTipoAeronave);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
