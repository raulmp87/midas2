package mx.com.afirme.midas.consultas.tipotransporte;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoTransporteDN {

	private static TipoTransporteDN INSTANCIA = new TipoTransporteDN();

	public static TipoTransporteDN getInstancia() {
		
		return TipoTransporteDN.INSTANCIA;
	}

	public List<TipoTransporteDTO> listarTodos()
			throws SystemException, ExcepcionDeAccesoADatos{
		
		TipoTransporteSN tipoTransporteSN = new TipoTransporteSN();
		return tipoTransporteSN.listarTodos();
	}

	public List<TipoTransporteDTO> listarFiltrado(TipoTransporteDTO tipoTransporteDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoTransporteSN tipoTransporteSN = new TipoTransporteSN();
		return tipoTransporteSN.listarFiltrado(tipoTransporteDTO);
	}

	public TipoTransporteDTO getPorId(TipoTransporteDTO tipoTransporteDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {

		TipoTransporteSN tipoTransporteSN = new TipoTransporteSN();
		return tipoTransporteSN.getPorId(tipoTransporteDTO.getIdTipoTransporte());
	}

}
