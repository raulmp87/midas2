package mx.com.afirme.midas.consultas.tipotransporte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoTransporteForm extends MidasBaseForm {

	private static final long serialVersionUID = 4818032637991299614L;

	private String idTipoTransporte, codigoTipoTransporte,
			descripcionTipoTransporte;

	public void setIdTipoTransporte(String idTipoTransporte) {
		this.idTipoTransporte = idTipoTransporte;
	}

	public String getIdTipoTransporte() {
		return idTipoTransporte;
	}

	public void setCodigoTipoTransporte(String codigoTipoTransporte) {
		this.codigoTipoTransporte = codigoTipoTransporte;
	}

	public String getCodigoTipoTransporte() {
		return codigoTipoTransporte;
	}

	public void setDescripcionTipoTransporte(String descripcionTipoTransporte) {
		this.descripcionTipoTransporte = descripcionTipoTransporte;
	}

	public String getDescripcionTipoTransporte() {
		return descripcionTipoTransporte;
	}
}
