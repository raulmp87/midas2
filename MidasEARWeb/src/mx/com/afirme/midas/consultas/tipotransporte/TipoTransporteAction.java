package mx.com.afirme.midas.consultas.tipotransporte;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoTransporteAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoTransporteDN tipoTrasporteDN          = TipoTransporteDN.getInstancia();
		List<TipoTransporteDTO> tipoTrasnporteDTO = tipoTrasporteDN.listarTodos();
		request.setAttribute("tipoTransporte", tipoTrasnporteDTO);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoTransporteForm tipoTransporteForm = (TipoTransporteForm) form;
		TipoTransporteDTO  tipoTransporteDTO  = new TipoTransporteDTO();
		TipoTransporteDN tipoTransporteDN     = TipoTransporteDN.getInstancia();
		
		try{
			this.poblarDTO(tipoTransporteForm, tipoTransporteDTO);
			request.setAttribute("tipoTransporte", tipoTransporteDN.listarFiltrado(tipoTransporteDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoTransporteForm tipoTransporteForm = (TipoTransporteForm) form;
		TipoTransporteDTO  tipoTransporteDTO  = new TipoTransporteDTO();
		TipoTransporteDN tipoTransporteDN     = TipoTransporteDN.getInstancia();
		
		try{
			this.poblarDTO(tipoTransporteForm, tipoTransporteDTO);
			tipoTransporteDTO = tipoTransporteDN.getPorId(tipoTransporteDTO);
			this.poblarForm(tipoTransporteDTO,tipoTransporteForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarForm(TipoTransporteDTO tipoTransporteDTO,
			TipoTransporteForm tipoTransporteForm) {
		
		if(tipoTransporteDTO.getIdTipoTransporte() != null)
			tipoTransporteForm.setIdTipoTransporte(tipoTransporteDTO.getIdTipoTransporte().toString());
		
		if(tipoTransporteDTO.getCodigoTipoTransporte() != null)
			tipoTransporteForm.setCodigoTipoTransporte(tipoTransporteDTO.getCodigoTipoTransporte().toString());
		
		if(tipoTransporteDTO.getDescripcionTipoTransporte() != null)
			tipoTransporteForm.setDescripcionTipoTransporte(tipoTransporteDTO.getDescripcionTipoTransporte());
	}

	private void poblarDTO(TipoTransporteForm tipoTransporteForm,
			TipoTransporteDTO tipoTransporteDTO) throws SystemException {
		
		if(!UtileriasWeb.esCadenaVacia(tipoTransporteForm.getIdTipoTransporte()))
			tipoTransporteDTO.setIdTipoTransporte(UtileriasWeb.regresaBigDecimal(tipoTransporteForm.getIdTipoTransporte()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoTransporteForm.getCodigoTipoTransporte()))
			tipoTransporteDTO.setCodigoTipoTransporte(UtileriasWeb.regresaBigDecimal(tipoTransporteForm.getCodigoTipoTransporte()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoTransporteForm.getDescripcionTipoTransporte()))
			tipoTransporteDTO.setDescripcionTipoTransporte(tipoTransporteForm.getDescripcionTipoTransporte().toUpperCase());
	}
}
