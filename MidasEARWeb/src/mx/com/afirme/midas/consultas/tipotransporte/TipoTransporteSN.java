package mx.com.afirme.midas.consultas.tipotransporte;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoTransporteSN {

	private TipoTransporteFacadeRemote beanRemoto;
	
	public TipoTransporteSN() 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en TipoTransporteSN - Constructor", Level.INFO,null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoTransporteFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	public List<TipoTransporteDTO> listarTodos()
			throws ExcepcionDeAccesoADatos{
				
		try{
			return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TipoTransporteDTO> listarFiltrado(TipoTransporteDTO tipoTransporteDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.listarFiltrado(tipoTransporteDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public TipoTransporteDTO getPorId(BigDecimal idTipoTransporte) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findById(idTipoTransporte);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
