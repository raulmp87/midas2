package mx.com.afirme.midas.consultas.tiposerviciotransporte;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoServicioTransporteAction extends MidasMappingDispatchAction{

	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			this.listarTodos(request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}

	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos {
		
		TipoServicioTransporteDN tipoServicioTrasporteDN          = TipoServicioTransporteDN.getInstancia();
		List<TipoServicioTransporteDTO> tipoServicioTrasnporteDTO = tipoServicioTrasporteDN.listarTodos();
		request.setAttribute("tipoServicioTransporte", tipoServicioTrasnporteDTO);
	}
	
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
	
		TipoServicioTransporteForm tipoServicioTransporteForm = (TipoServicioTransporteForm) form;
		TipoServicioTransporteDTO  tipoServicioTransporteDTO  = new TipoServicioTransporteDTO();
		TipoServicioTransporteDN   tipoServicioTransporteDN   = TipoServicioTransporteDN.getInstancia();
		
		try{
			this.poblarDTO(tipoServicioTransporteForm, tipoServicioTransporteDTO);
			request.setAttribute("tipoServicioTransporte", tipoServicioTransporteDN.listarFiltrado(tipoServicioTransporteDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward getPorId(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		TipoServicioTransporteForm tipoServicioTransporteForm = (TipoServicioTransporteForm) form;
		TipoServicioTransporteDTO  tipoServicioTransporteDTO  = new TipoServicioTransporteDTO();
		TipoServicioTransporteDN   tipoServicioTransporteDN   = TipoServicioTransporteDN.getInstancia();
		
		try{
			this.poblarDTO(tipoServicioTransporteForm, tipoServicioTransporteDTO);
			tipoServicioTransporteDTO = tipoServicioTransporteDN.getPorId(tipoServicioTransporteDTO);
			this.poblarForm(tipoServicioTransporteDTO,tipoServicioTransporteForm);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarForm(TipoServicioTransporteDTO tipoServicioTransporteDTO,
			TipoServicioTransporteForm tipoServicioTransporteForm) {
		
		if(tipoServicioTransporteDTO.getIdTipoServicioTransporte() != null)
			tipoServicioTransporteForm.setIdTipoServicioTransporte(tipoServicioTransporteDTO.getIdTipoServicioTransporte().toString());
		
		if(tipoServicioTransporteDTO.getCodigoTipoServicioTransporte() != null)
			tipoServicioTransporteForm.setCodigoTipoServicioTransporte(tipoServicioTransporteDTO.getCodigoTipoServicioTransporte().toString());
		
		if(tipoServicioTransporteDTO.getDescripcionTipoServicioTra() != null)
			tipoServicioTransporteForm.setDescripcionTipoServicioTra(tipoServicioTransporteDTO.getDescripcionTipoServicioTra());
	}

	private void poblarDTO(TipoServicioTransporteForm tipoServicioTransporteForm,
			TipoServicioTransporteDTO tipoServicioTransporteDTO) throws SystemException {
		
		if(!UtileriasWeb.esCadenaVacia(tipoServicioTransporteForm.getIdTipoServicioTransporte()))
			tipoServicioTransporteDTO.setIdTipoServicioTransporte(UtileriasWeb.regresaBigDecimal(tipoServicioTransporteForm.getIdTipoServicioTransporte()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoServicioTransporteForm.getCodigoTipoServicioTransporte()))
			tipoServicioTransporteDTO.setCodigoTipoServicioTransporte(UtileriasWeb.regresaBigDecimal(tipoServicioTransporteForm.getCodigoTipoServicioTransporte()));
		
		if(!UtileriasWeb.esCadenaVacia(tipoServicioTransporteForm.getDescripcionTipoServicioTra()))
			tipoServicioTransporteDTO.setDescripcionTipoServicioTra(tipoServicioTransporteForm.getDescripcionTipoServicioTra().toUpperCase());
	}
	
}
