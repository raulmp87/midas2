package mx.com.afirme.midas.consultas.tiposerviciotransporte;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoServicioTransporteSN {

	private TipoServicioTransporteFacadeRemote beanRemoto;
	
	public TipoServicioTransporteSN() 
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en Tipo Servicio TransporteSN - Constructor", Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoServicioTransporteFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch(Exception e){
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<TipoServicioTransporteDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<TipoServicioTransporteDTO> listarFiltrado(TipoServicioTransporteDTO tipoServicioTransporteDTO) 
			throws ExcepcionDeAccesoADatos {
	
		try{
			return beanRemoto.listarFiltrado(tipoServicioTransporteDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public TipoServicioTransporteDTO getPorId(BigDecimal idTipoServicioTransporte) 
			throws ExcepcionDeAccesoADatos {
		
		try{
			return beanRemoto.findById(idTipoServicioTransporte);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
