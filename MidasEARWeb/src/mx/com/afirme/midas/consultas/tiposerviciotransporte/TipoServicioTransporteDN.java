package mx.com.afirme.midas.consultas.tiposerviciotransporte;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoServicioTransporteDN {

	private static final TipoServicioTransporteDN INSTANCIA = new TipoServicioTransporteDN();
	
	public static TipoServicioTransporteDN getInstancia() {
	
		return TipoServicioTransporteDN.INSTANCIA;
	}

	public List<TipoServicioTransporteDTO> listarTodos() 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		TipoServicioTransporteSN tipoServicioTransporteSN = new TipoServicioTransporteSN();
		return tipoServicioTransporteSN.listarTodos();
	}

	public Object listarFiltrado(TipoServicioTransporteDTO tipoServicioTransporteDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {

		TipoServicioTransporteSN tipoServicioTransporteSN = new TipoServicioTransporteSN();
		return tipoServicioTransporteSN.listarFiltrado(tipoServicioTransporteDTO);
	}

	public TipoServicioTransporteDTO getPorId(TipoServicioTransporteDTO tipoServicioTransporteDTO) 
			throws SystemException, ExcepcionDeAccesoADatos {
	
		TipoServicioTransporteSN tipoServicioTransporteSN = new TipoServicioTransporteSN();
		return tipoServicioTransporteSN.getPorId(tipoServicioTransporteDTO.getIdTipoServicioTransporte());
	}

}
