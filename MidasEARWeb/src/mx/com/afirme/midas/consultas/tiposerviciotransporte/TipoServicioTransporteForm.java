package mx.com.afirme.midas.consultas.tiposerviciotransporte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TipoServicioTransporteForm extends MidasBaseForm{

	private static final long serialVersionUID = 991290693102466774L;

	private String idTipoServicioTransporte,
				   codigoTipoServicioTransporte,
				   descripcionTipoServicioTra;

	
	public void setIdTipoServicioTransporte(String idTipoServicioTransporte) {
		this.idTipoServicioTransporte = idTipoServicioTransporte;
	}

	public String getIdTipoServicioTransporte() {
		return idTipoServicioTransporte;
	}

	public void setDescripcionTipoServicioTra(String descripcionTipoServicioTra) {
		this.descripcionTipoServicioTra = descripcionTipoServicioTra;
	}

	public String getDescripcionTipoServicioTra() {
		return descripcionTipoServicioTra;
	}

	public void setCodigoTipoServicioTransporte(
			String codigoTipoServicioTransporte) {
		this.codigoTipoServicioTransporte = codigoTipoServicioTransporte;
	}

	public String getCodigoTipoServicioTransporte() {
		return codigoTipoServicioTransporte;
	}
}
