package mx.com.afirme.midas.interfaz.cliente;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.commons.lang.StringUtils;

public class ClienteDN {
	private static final ClienteDN INSTANCIA = new ClienteDN();

	public static ClienteDN getInstancia() {
		return ClienteDN.INSTANCIA;
	}

	public List<ClienteDTO> listarFiltrado(ClienteDTO cliente, Usuario usuario)
			throws SystemException {
		ClienteSN clienteSN = new ClienteSN();
		String claveBusquedaLimitada = "midas.danios.busqueda.clientes.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null && usuario.contieneAtributo(claveBusquedaLimitada)) {
			AtributoUsuario atributoUsuario = usuario
					.obtenerAtributo(claveBusquedaLimitada);
			if (!atributoUsuario.isActivo())
				aplicaFiltrado = false;
		} else {
			aplicaFiltrado = false;
		}
		if (aplicaFiltrado) {
			return clienteSN.listarFiltrado(cliente, StringUtils.rightPad(
					usuario.getNombreUsuario(), 8, " "));
		} else {
			return clienteSN.listarFiltrado(cliente, null);
		}
	}

	public BigDecimal agregar(ClienteDTO cliente, String nombreUsuario)
			throws SystemException {
		ClienteSN clienteSN = new ClienteSN();
		return clienteSN.agregar(cliente, nombreUsuario);
	}

	public ClienteDTO verDetalleCliente(BigDecimal idCliente,
			String nombreUsuario) throws SystemException {
		ClienteSN clienteSN = new ClienteSN();
		return clienteSN.verDetalleCliente(idCliente, nombreUsuario);
	}

	public ClienteDTO verDetalleCliente(ClienteDTO cliente, String nombreUsuario)
			throws SystemException {
		ClienteSN clienteSN = new ClienteSN();

		List<ClienteDTO> clienteDTOs = clienteSN.verDetalleCliente(cliente,
				nombreUsuario);

		if (clienteDTOs != null)
			return clienteDTOs.get(0);
		else
			return null;
	}

	public List<ClienteDTO> buscarPorCURP(String tipoPersona, String curp,
			Usuario usuario) throws SystemException {
		ClienteSN clienteSN = new ClienteSN();
		ClienteDTO cliente = new ClienteDTO();
		cliente.setClaveTipoPersona(Short.valueOf(tipoPersona));
		cliente.setCodigoCURP(curp);
		String claveBusquedaLimitada = "midas.danios.busqueda.clientes.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null && usuario.contieneAtributo(claveBusquedaLimitada)) {
			AtributoUsuario atributoUsuario = usuario
					.obtenerAtributo(claveBusquedaLimitada);
			if (!atributoUsuario.isActivo())
				aplicaFiltrado = false;
		} else {
			aplicaFiltrado = false;
		}
		if (aplicaFiltrado) {
			return clienteSN.verDetalleCliente(cliente, StringUtils.rightPad(
					usuario.getNombreUsuario(), 8, " "));
		} else {
			return clienteSN.verDetalleCliente(cliente, null);
		}
	}

	public ClienteDTO agregarV2(ClienteDTO cliente, String nombreUsuario)
			throws SystemException {
		ClienteSN clienteSN = new ClienteSN();
		return clienteSN.agregarV2(cliente, nombreUsuario);
	}
	
}
