package mx.com.afirme.midas.interfaz.cliente;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ClienteSN {
	private ClienteFacadeRemote beanRemoto;

	public ClienteSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ClienteFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ClienteDTO> listarFiltrado(ClienteDTO entity,
			String nombreUsuario) {
		try {
			return beanRemoto.listarFiltrado(entity, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CLISTA_CLIENTE");
			sb.append("|");
			sb.append("pNombre" + "=" + entity.getNombre() + ",");
			sb.append("pApellido_Paterno" + "=" + entity.getApellidoPaterno() + ",");
			sb.append("pApellido_Materno" + "=" + entity.getApellidoMaterno() + ",");
			sb.append("pRFC" + "=" + entity.getCodigoRFC() + ",");
			sb.append("pTipo_Persona" + "=" + entity.getClaveTipoPersona() + ",");
			sb.append("pId_Cliente" + "=" + entity.getIdCliente() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listado de usuarios en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public BigDecimal agregar(ClienteDTO entity, String nombreUsuario) {
		try {
			return beanRemoto.save(entity, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPGRABAR_CLIENTE");
			sb.append("|");
			sb.append("pNombre" + "=" + entity.getNombre() + ",");
			sb.append("pApellido_Paterno" + "=" + entity.getApellidoPaterno() + ",");
			sb.append("pApellido_Materno" + "=" + entity.getApellidoMaterno() + ",");
			sb.append("pRFC" + "=" + entity.getCodigoRFC() + ",");
			sb.append("pFecha" + "=" + entity.getFechaNacimiento() + ",");
			sb.append("pCodigo_Postal" + "=" + entity.getCodigoPostal() + ",");
			sb.append("pCalle_Numero" + "=" + entity.getNombreCalle() + ",");
			sb.append("pid_colonia" + "=" + entity.getIdColonia() + ",");
			sb.append("pNom_otra_Colonia" + "=" + entity.getNombreColonia() + ",");
			sb.append("pid_Municipio" + "=" + entity.getIdMunicipio() + ",");
			sb.append("pid_Estado" + "=" + entity.getIdEstado() + ",");
			sb.append("pTelefono" + "=" + entity.getTelefono() + ",");
			sb.append("pE_Mail" + "=" + entity.getEmail() + ",");
			sb.append("pId_Usuario" + "=" + nombreUsuario + ",");
			sb.append("pTipo_Persona" + "=" + entity.getClaveTipoPersona() + ",");
			
			
			UtileriasWeb.enviaCorreoExcepcion("Guardado de cliente " + entity.getCodigoRFC() + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public ClienteDTO verDetalleCliente(BigDecimal idCliente, String nombreUsuario) {
		try {
			return beanRemoto.findById(idCliente, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CDETALLE_CLIENTE");
			sb.append("|");
			sb.append("pId_Cliente" + "=" + idCliente + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Detalle de cliente " + idCliente + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ClienteDTO> verDetalleCliente(ClienteDTO entity,
			String nombreUsuario) {
		try {
			return beanRemoto.buscarPorCURP(entity, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STP_CONSCLIENTE");
			sb.append("|");
			sb.append("pCURP" + "=" + entity.getCodigoCURP() + ",");
			sb.append("pTipo_Persona" + "=" + entity.getClaveTipoPersona() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Busqueda por CURP" + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	public ClienteDTO  agregarV2(ClienteDTO entity, String nombreUsuario) {
		try {
			return beanRemoto.saveV2(entity, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPGRABAR_CLIENTE");
			sb.append("|");
			sb.append("pNombre" + "=" + entity.getNombre() + ",");
			sb.append("pApellido_Paterno" + "=" + entity.getApellidoPaterno() + ",");
			sb.append("pApellido_Materno" + "=" + entity.getApellidoMaterno() + ",");
			sb.append("pRFC" + "=" + entity.getCodigoRFC() + ",");
			sb.append("pFecha" + "=" + entity.getFechaNacimiento() + ",");
			sb.append("pCodigo_Postal" + "=" + entity.getCodigoPostal() + ",");
			sb.append("pCalle_Numero" + "=" + entity.getNombreCalle() + ",");
			sb.append("pid_colonia" + "=" + entity.getIdColonia() + ",");
			sb.append("pNom_otra_Colonia" + "=" + entity.getNombreColonia() + ",");
			sb.append("pid_Municipio" + "=" + entity.getIdMunicipio() + ",");
			sb.append("pid_Estado" + "=" + entity.getIdEstado() + ",");
			sb.append("pTelefono" + "=" + entity.getTelefono() + ",");
			sb.append("pE_Mail" + "=" + entity.getEmail() + ",");
			sb.append("pId_Usuario" + "=" + nombreUsuario + ",");
			sb.append("pTipo_Persona" + "=" + entity.getClaveTipoPersona() + ",");
			
			
			UtileriasWeb.enviaCorreoExcepcion("Guardado de cliente " + entity.getCodigoRFC() + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
