package mx.com.afirme.midas.interfaz.solicitudcheque;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SolicitudChequeSN {

private SolicitudChequeFacadeRemote beanRemoto;
	
	public SolicitudChequeSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = (SolicitudChequeFacadeRemote)serviceLocator.getEJB(SolicitudChequeFacadeRemote.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
	public BigDecimal solicitaCheque (SolicitudChequeDTO solicitudCheque, String nombreUsuario) {
		try {
			return beanRemoto.solicitaCheque(solicitudCheque, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPSOLICITAR_CHEQUE");
			sb.append("|");
			sb.append("pId_Pago_Midas" + "=" + solicitudCheque.getIdPago() + ",");
			sb.append("pTrans_cont" + "=" + solicitudCheque.getClaveTransaccionContable() + ",");
			sb.append("pId_moneda" + "=" + solicitudCheque.getIdMoneda() + ",");
			sb.append("pTipo_Cambio" + "=" + solicitudCheque.getTipoCambio() + ",");
			sb.append("pId_prest_serv" + "=" + solicitudCheque.getIdPrestadorServicio() + ",");
			sb.append("pBeneficiario" + "=" + solicitudCheque.getNombreBeneficiario() + ",");
			sb.append("pTipo_pago" + "=" + solicitudCheque.getTipoPago() + ",");
			sb.append("pConcepto_pol" + "=" + solicitudCheque.getConceptoPoliza() + ",");
			sb.append("pUsuario" + "=" + nombreUsuario + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Solicitud de cheque con id Pago:" + solicitudCheque.getIdPago() + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public SolicitudChequeDTO consultaEstatusSolicitudCheque (BigDecimal idPago, String nombreUsuario) {
		try {
			return beanRemoto.consultaEstatusSolicitudCheque(idPago, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPESTATUS_SOL_CHEQUE");
			sb.append("|");
			sb.append("pPago_Midas" + "=" + idPago + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Consulta de estatus de cheque con id Pago:" + idPago + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public String cancelaSolicitudCheque (BigDecimal idPago, String nombreUsuario) {
		try {
			return beanRemoto.cancelaSolicitudCheque(idPago, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.stpCan_Sol_Cheque");
			sb.append("|");
			sb.append("pPagoMidas" + "=" + idPago + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Cancelacion Solicitud de cheque con id Pago:" + idPago + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
}
