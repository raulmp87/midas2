package mx.com.afirme.midas.interfaz.solicitudcheque;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;

public class SolicitudChequeDN {

	private static final SolicitudChequeDN INSTANCIA = new SolicitudChequeDN();
	private static String nombreUsuario;
	
	public static SolicitudChequeDN getInstancia(String nombreUsuario) {
		SolicitudChequeDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	/**
	 * Registra pagos de Midas a Seycos
	 * @param idPago Identificador de Pago de MIDAS
	 * @param claveTransaccionContable Transacci�n Contable (Valor Constante) 'INDEMNIZACIONES' �  'GASTOS'
	 * @param idMoneda Identificador de la moneda
	 * @param tipoCambio Tipo de cambio
	 * @param idPrestadorServicio Identificador Prestador de Servicio
	 * @param nombreBeneficiario Nombre del Beneficiario
	 * @param tipoPago Tipo de Pago 'SC' Solicitud de Cheque  'TB' Transferencia Bancaria
	 * @param conceptoPoliza Concepto de la P�liza Ej. 'PAGO DE SINIESTROS' / id_pago_midas
	 * @return Id de la Solicitud de Cheque
	 * @throws SystemException
	 */
	public BigDecimal solicitaCheque (BigDecimal idPago, String claveTransaccionContable, Short idMoneda,
			Double tipoCambio, BigDecimal idPrestadorServicio, String nombreBeneficiario, String tipoPago,
			String conceptoPoliza) throws SystemException {
		
		SolicitudChequeSN solicitudChequeSN = new SolicitudChequeSN();
		
		SolicitudChequeDTO solicitudCheque = new SolicitudChequeDTO();
		
		solicitudCheque.setIdPago(idPago);
		solicitudCheque.setClaveTransaccionContable(claveTransaccionContable);
		solicitudCheque.setIdMoneda(idMoneda);
		solicitudCheque.setTipoCambio(tipoCambio);
		solicitudCheque.setIdPrestadorServicio(idPrestadorServicio);
		solicitudCheque.setNombreBeneficiario(nombreBeneficiario);
		solicitudCheque.setTipoPago(tipoPago);
		solicitudCheque.setConceptoPoliza(conceptoPoliza);

		return solicitudChequeSN.solicitaCheque(solicitudCheque, SolicitudChequeDN.nombreUsuario);
	}
	
	/**
	 * Consulta el estatus de la solicitud de Cheque
	 * @param idPago Identificador de Pago de MIDAS
	 * @return Estatus de la solicitud de Cheque
	 * @throws SystemException
	 */
	public SolicitudChequeDTO consultaEstatusSolicitudCheque (BigDecimal idPago) throws SystemException {
		
		SolicitudChequeSN solicitudChequeSN = new SolicitudChequeSN();
		return solicitudChequeSN.consultaEstatusSolicitudCheque(idPago, SolicitudChequeDN.nombreUsuario);
	}
	
	/**
	 * Cancela una solicitud de Cheque
	 * @param idPago Identificador de Pago de MIDAS
	 * @return Estatus de la solicitud de Cheque
	 * @throws SystemException
	 */
	public String cancelaSolicitudCheque (BigDecimal idPago) throws SystemException {
		SolicitudChequeSN solicitudChequeSN = new SolicitudChequeSN();
		return solicitudChequeSN.cancelaSolicitudCheque(idPago, SolicitudChequeDN.nombreUsuario);
	}
	
}
