package mx.com.afirme.midas.interfaz.formapago;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class FormaPagoSN {

private FormaPagoFacadeRemote beanRemoto;
	
	public FormaPagoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(FormaPagoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public List<FormaPagoIDTO> listarFiltrado(FormaPagoIDTO formaPago, String nombreUsuario) {
		try {
			return beanRemoto.findByProperty(formaPago, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CIDFORMA_PAGO");
			sb.append("|");
			sb.append("pId_Forma_Pago" + "=" + formaPago.getIdFormaPago() + ",");
			sb.append("pDescripcion" + "=" + formaPago.getDescripcion() + ",");
			sb.append("pId_Moneda" + "=" + formaPago.getIdMoneda() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listado de Formas de pago en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
