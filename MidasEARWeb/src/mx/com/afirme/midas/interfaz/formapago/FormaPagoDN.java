package mx.com.afirme.midas.interfaz.formapago;

import java.util.List;

import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.sistema.SystemException;


public class FormaPagoDN {

	private static final FormaPagoDN INSTANCIA = new FormaPagoDN();
	private static String nombreUsuario;
	
	public static FormaPagoDN getInstancia(String nombreUsuario) {
		FormaPagoDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	public List<FormaPagoIDTO> listarTodos(Short idMoneda)	throws SystemException {
		FormaPagoIDTO formaPago = new FormaPagoIDTO();
		formaPago.setIdFormaPago(null);
		formaPago.setDescripcion(null);
		formaPago.setIdMoneda(idMoneda);
		
		FormaPagoSN formaPagoSN = new FormaPagoSN();
		return formaPagoSN.listarFiltrado(formaPago, FormaPagoDN.nombreUsuario);
	}
	
	public List<FormaPagoIDTO> listarFiltrado(Integer idFormaPago, String descripcion, Short idMoneda) throws SystemException {
		
		FormaPagoIDTO formaPago = new FormaPagoIDTO();
		formaPago.setIdFormaPago(idFormaPago);
		formaPago.setDescripcion(descripcion);
		formaPago.setIdMoneda(idMoneda);
		
		FormaPagoSN formaPagoSN = new FormaPagoSN();
		return formaPagoSN.listarFiltrado(formaPago, FormaPagoDN.nombreUsuario);
	}
	
	public FormaPagoIDTO getPorId(Integer idFormaPago,Short idMoneda) throws SystemException {
		if (idFormaPago == null)
			return null;
		FormaPagoIDTO formaPago = new FormaPagoIDTO();
		formaPago.setIdFormaPago(idFormaPago);
		formaPago.setDescripcion(null);
		formaPago.setIdMoneda(idMoneda);
		
		List<FormaPagoIDTO> listaTMP = new FormaPagoSN().listarFiltrado(formaPago, FormaPagoDN.nombreUsuario);
		if (listaTMP != null && !listaTMP.isEmpty())
			return listaTMP.get(0);
		else
			return null;
	}
}
