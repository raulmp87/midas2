package mx.com.afirme.midas.interfaz.prestadorservicios;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class PrestadorServiciosDN {

	private static final PrestadorServiciosDN INSTANCIA = new PrestadorServiciosDN();
	
	public static PrestadorServiciosDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<PrestadorServiciosDTO> listarPrestadores(
			String nombrePrestadorServicios, String nombreUsuario) throws SystemException {
		
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		PrestadorServiciosDTO prestadorServicios = new PrestadorServiciosDTO();
		prestadorServicios.setNombrePrestador(nombrePrestadorServicios);
		
		return prestadorServiciosSN.listarPrestadores(prestadorServicios, nombreUsuario);
	}
	
	public PrestadorServiciosDTO detallePrestador(
			BigDecimal idPrestadorServicios, String nombreUsuario) throws SystemException {
		
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		
		return prestadorServiciosSN.detallePrestador(idPrestadorServicios, nombreUsuario);
	}

}
