package mx.com.afirme.midas.interfaz.prestadorservicios;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PrestadorServiciosSN {

private PrestadorServiciosInterfazFacadeRemote beanRemoto;
	
	public PrestadorServiciosSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PrestadorServiciosInterfazFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
	public List<PrestadorServiciosDTO> listarPrestadores(
			PrestadorServiciosDTO prestadorServicios, String nombreUsuario) {
		try {
			return beanRemoto.listarPrestadores(prestadorServicios, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.cLista_Prestador");
			sb.append("|");
			sb.append("pNombre" + "=" + prestadorServicios.getNombrePrestador() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listado de prestadores de servicio con nombre :" + prestadorServicios.getNombrePrestador() + 
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public PrestadorServiciosDTO detallePrestador(
			BigDecimal idPrestadorServicios, String nombreUsuario) {
		try {
			return beanRemoto.detallePrestador(idPrestadorServicios, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.cDetalle_Prestador");
			sb.append("|");
			sb.append("pId_Prestador" + "=" + idPrestadorServicios + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Detalle del prestador con id :" + idPrestadorServicios + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
