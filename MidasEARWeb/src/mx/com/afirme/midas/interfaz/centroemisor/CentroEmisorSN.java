package mx.com.afirme.midas.interfaz.centroemisor;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CentroEmisorSN {
	
	private CentroEmisorFacadeRemote beanRemoto;
	
	public CentroEmisorSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CentroEmisorFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}


	public List<CentroEmisorDTO> listarCentrosEmisores(String nombreUsuario) {
		try {
			return beanRemoto.listarCentrosEmisores(nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CCENTRO_EMISOR");
			sb.append("|");
			sb.append("pUsuario" + "=" + nombreUsuario + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listar centros emisores en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}

