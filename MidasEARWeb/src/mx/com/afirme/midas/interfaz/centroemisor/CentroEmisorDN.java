package mx.com.afirme.midas.interfaz.centroemisor;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class CentroEmisorDN {

	private static final CentroEmisorDN INSTANCIA = new CentroEmisorDN();
	private static String nombreUsuario;
	
	public static CentroEmisorDN getInstancia(String nombreUsuario) {
		CentroEmisorDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	public List<CentroEmisorDTO> listarCentrosEmisores() throws SystemException {
		
		CentroEmisorSN centroEmisorSN = new CentroEmisorSN();
		
		return centroEmisorSN.listarCentrosEmisores(CentroEmisorDN.nombreUsuario);
	}
	
}
