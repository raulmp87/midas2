package mx.com.afirme.midas.interfaz.recibo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ReciboDN {

	private static final ReciboDN INSTANCIA = new ReciboDN();
	private static String nombreUsuario;
	
	public static ReciboDN getInstancia(String nombreUsuario) {
		ReciboDN.nombreUsuario = nombreUsuario;
		return ReciboDN.INSTANCIA;
	}
	
	/**
	 * Genera informaci�n de recibos para una p�liza dada
	 * @param idPoliza Id de la p�liza
	 * @return Listado con los Recibos generados
	 * @throws SystemException
	 */
	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza)
			throws SystemException {
		ReciboSN reciboSN = new ReciboSN();
		return reciboSN.emiteRecibosPoliza(idPoliza, ReciboDN.nombreUsuario);
	}
	
	/**
	 * Genera informaci�n de recibos para una p�liza dada, con el n�mero de Endoso dado
	 * @param idPoliza Id de la p�liza
	 * @return Listado con los Recibos generados
	 * @throws SystemException
	 */
	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza,Short numeroEndoso) throws SystemException {
		/**
		 * 21/12/08. Este m�todo est� pendiente de implementarse, ya que actualmente s�lo se recibe el n�mero de p�liza, sin 
		 * tomar en cuenta el n�mero de endoso.
		 */
		ReciboSN reciboSN = new ReciboSN();
		return reciboSN.emiteRecibosPoliza(idPoliza, ReciboDN.nombreUsuario);
	}
	
	/**
	 * Consulta la informaci�n de los Recibos de una P�liza, indicando tambi�n el numero de endoso
	 * @param idPoliza Id de la p�liza
	 * @param numeroEndoso N�mero de endoso espec�fico
	 * @return Los recibos de la p�liza/endoso
	 * @throws SystemException
	 */
	public List<ReciboDTO> consultaRecibos(BigDecimal idPoliza, Short numeroEndoso)
			throws SystemException {
		String numPolizaEndoso = idPoliza + "|" + numeroEndoso;
		ReciboSN reciboSN = new ReciboSN();
		return reciboSN.consultaRecibos(numPolizaEndoso, ReciboDN.nombreUsuario);
	}
	
	/**
	 * Consulta la informaci�n de los Recibos de una P�liza sin endosar
	 * @param idPoliza Id de la p�liza 
	 * @return Los recibos de la p�liza sin endosar
	 * @throws SystemException
	 */
	public List<ReciboDTO> consultaRecibos(BigDecimal idPoliza)
			throws SystemException {
		return consultaRecibos (idPoliza, (short)0);
	}
	
	
	
}
