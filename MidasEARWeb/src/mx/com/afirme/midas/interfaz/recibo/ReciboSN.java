package mx.com.afirme.midas.interfaz.recibo;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ReciboSN {

	private ReciboFacadeRemote beanRemoto;
	
	public ReciboSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReciboFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public List<ReciboDTO> emiteRecibosPoliza(BigDecimal idPoliza, String nombreUsuario) {
		try {
			return beanRemoto.emiteRecibosPoliza(idPoliza, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.stpEmiteRecibosPol");
			sb.append("|");
			sb.append("id_poliza" + "=" + idPoliza + ",");
			
			UtileriasWeb.enviaCorreoExcepcion(UtileriasWeb
					.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratos.linea.tipodistribucion.poliza") + ": " + idPoliza +
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<ReciboDTO> consultaRecibos(String numPolizaEndoso, String nombreUsuario) {
		try {
			return beanRemoto.consultaRecibos(numPolizaEndoso, nombreUsuario);
		} catch (SQLException e) {
			
			numPolizaEndoso = numPolizaEndoso.replace("|", "/");
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.stpConsultaRecibo");
			sb.append("|");
			sb.append("pid_cot_midas(idPoliza/numero Endoso)" + "=" + numPolizaEndoso + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Consulta de recibos de (idPoliza/numero Endoso) " + numPolizaEndoso + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
