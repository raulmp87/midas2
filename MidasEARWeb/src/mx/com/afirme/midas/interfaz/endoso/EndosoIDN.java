package mx.com.afirme.midas.interfaz.endoso;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoIDN {
	private static final EndosoIDN INSTANCIA = new EndosoIDN();

	public static EndosoIDN getInstancia() {
		return INSTANCIA;
	}

	/**
	 * Emite un endoso en Seycos
	 * 
	 * @param numPolizaEndoso
	 *            (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Listado con referencias a los recibos del Endoso emitido
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<EndosoIDTO> emiteEndoso(String numPolizaEndoso,
			String nombreUsuario) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new EndosoSN().emiteEndoso(numPolizaEndoso, nombreUsuario);
	}

	/**
	 * Valida si la poliza es Cancelable a inicio de vigencia
	 * 
	 * @param numPolizaEndoso
	 *            (Id de la p�liza | Numero del Endoso)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @param fechaCancelacion
	 *            La fecha en que se desea cancelar la poliza
	 * @return true si la poliza es Cancelable a inicio de vigencia, false en
	 *         caso contrario. En caso de no ser Cancelable, incluye el motivo
	 *         por lo cual no lo es
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public EndosoIDTO validaEsCancelable(String numPolizaEndoso,
			String nombreUsuario, Date fechaCancelacion, Integer tipoEndoso)
			throws SystemException, ExcepcionDeAccesoADatos {
		return new EndosoSN().validaEsCancelable(numPolizaEndoso,
				nombreUsuario, fechaCancelacion, tipoEndoso);
	}

	/**
	 * Obtiene una lista con los documentos cancelables (con recibos vencidos)
	 * de una fecha determinada
	 * 
	 * @param fechaConsulta
	 *            Fecha de consulta para documentos cancelables (con recibos
	 *            vencidos)
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos cancelables
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<EndosoIDTO> obtieneListaCancelables(Date fechaConsulta,String nombreUsuario) throws SystemException,ExcepcionDeAccesoADatos {
		return new EndosoSN().obtieneListaCancelables(fechaConsulta,nombreUsuario);
	}
	
	public List<EndosoIDTO> obtieneListaCancelablesReporte(Date fechaConsulta,String nombreUsuario) throws SystemException,ExcepcionDeAccesoADatos {
		return new EndosoSN().obtieneListaCancelablesReporte(fechaConsulta,nombreUsuario);
	}

	/**
	 * Obtiene una lista con los documentos rehabilitables en una fecha
	 * determinada
	 * 
	 * @param fechaConsulta
	 *            Fecha de consulta para documentos rehabilitables
	 * @param nombreUsuario
	 *            Nombre del Usuario que realiza la operacion
	 * @return Lista con los documentos rehabilitables
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public List<EndosoIDTO> obtieneListaRehabilitables(Date fechaConsulta,
			String nombreUsuario) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new EndosoSN().obtieneListaRehabilitables(fechaConsulta,
				nombreUsuario);
	}

	public List<EndosoCoberturaDTO> obtieneCoberturasAgrupadas(EndosoId id,
			String nombreUsuario) throws SystemException,
			ExcepcionDeAccesoADatos {
		return new EndosoSN().obtieneCoberturasAgrupadas(id, nombreUsuario);
	}
}
