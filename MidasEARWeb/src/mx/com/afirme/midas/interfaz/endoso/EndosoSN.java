package mx.com.afirme.midas.interfaz.endoso;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoSN {

	private EndosoFacadeRemote beanRemoto;

	public EndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(EndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(e);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<EndosoIDTO> emiteEndoso(String numPolizaEndoso,
			String nombreUsuario) {
		try {
			return beanRemoto.emiteEndoso(numPolizaEndoso, nombreUsuario);
		} catch (SQLException e) {

			numPolizaEndoso = numPolizaEndoso.replace("|", "/");

			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.stpEmiteEndoso");
			sb.append("|");
			sb.append("pIdentificador(idPoliza/numero Endoso)" + "="
					+ numPolizaEndoso + ",");

			UtileriasWeb.enviaCorreoExcepcion(
					"Emision de endoso con (idPoliza/numero Endoso)" + ":"
							+ numPolizaEndoso + " en "
							+ Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoIDTO validaEsCancelable(String numPolizaEndoso,
			String nombreUsuario, Date fechaCancelacion, Integer tipoEndoso) {
		try {
			return beanRemoto.validaEsCancelable(numPolizaEndoso,
					nombreUsuario, fechaCancelacion, tipoEndoso);
		} catch (SQLException e) {

			numPolizaEndoso = numPolizaEndoso.replace("|", "/");

			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.stpPermiteCancelar");
			sb.append("|");
			sb.append("pIdentificador(idPoliza/numero Endoso)" + "="
					+ numPolizaEndoso + ",");

			UtileriasWeb.enviaCorreoExcepcion(
					"Validacion si el (idPoliza/numero Endoso)" + ":"
							+ numPolizaEndoso + " es cancelable " + " en "
							+ Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<EndosoIDTO> obtieneListaCancelables(Date fechaConsulta,String nombreUsuario) {
		try {
			return beanRemoto.obtieneListaCancelables(fechaConsulta,nombreUsuario);
		} catch (SQLException e) {

			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.cLista_Cancelables");
			sb.append("|");
			sb.append("pF_Proceso" + "=" + fechaConsulta + ",");

			UtileriasWeb.enviaCorreoExcepcion("Obtener lista de documentos cancelables con fecha: "
									+ fechaConsulta + " en "+ Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(this.getClass().getCanonicalName(),e);
		}
	}
	
	public List<EndosoIDTO> obtieneListaCancelablesReporte(Date fechaConsulta,String nombreUsuario) {
		try {
			return beanRemoto.obtieneListaCancelablesReporte(fechaConsulta,nombreUsuario);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(this.getClass().getCanonicalName(),e);
		}
	}

	public List<EndosoIDTO> obtieneListaRehabilitables(Date fechaConsulta,
			String nombreUsuario) {
		try {
			return beanRemoto.obtieneListaRehabilitables(fechaConsulta,
					nombreUsuario);
		} catch (SQLException e) {

			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.cLista_Rehab");
			sb.append("|");
			sb.append("pF_Proceso" + "=" + fechaConsulta + ",");

			UtileriasWeb
					.enviaCorreoExcepcion(
							"Obtener lista de documentos rehabilitables con fecha: "
									+ fechaConsulta + " en "
									+ Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<EndosoCoberturaDTO> obtieneCoberturasAgrupadas(EndosoId id,
			String nombreUsuario) {
		try {
			return beanRemoto.obtieneCoberturasAgrupadas(id, nombreUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

}
