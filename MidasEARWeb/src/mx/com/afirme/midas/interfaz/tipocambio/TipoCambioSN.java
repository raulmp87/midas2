package mx.com.afirme.midas.interfaz.tipocambio;

import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoCambioSN {
	private TipoCambioFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public TipoCambioSN(String nombreUsuario) throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoCambioFacadeRemote.class);
			this.nombreUsuario = nombreUsuario;
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
	public Double obtieneTipoCambioPorDia(Date fecha) {
		try {
			return beanRemoto.obtieneTipoCambioPorDia(fecha, this.nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("stpConsTC_dia");
			sb.append("|");
			sb.append("@fecha" + "=" + fecha + ",");
						
			UtileriasWeb.enviaCorreoExcepcion("Tipo de Cambio del dia " + fecha + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	public Double obtieneTipoCambioPorMes(Integer mes, Integer anio) {
		try {
			return beanRemoto.obtieneTipoCambioPorMes(mes, anio, this.nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("stpConsTC_mes");
			sb.append("|");
			sb.append("@mes" + "=" + mes + ",");
			sb.append("@anio" + "=" + anio + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Tipo de Cambio del mes " + mes + " y a�o " + anio+ " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
	
	
}
