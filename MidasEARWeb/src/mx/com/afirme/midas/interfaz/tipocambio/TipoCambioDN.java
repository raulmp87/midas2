package mx.com.afirme.midas.interfaz.tipocambio;

import java.util.Date;
import java.util.GregorianCalendar;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TipoCambioDN {

	private static String nombreUsuario;
	
	private static final TipoCambioDN INSTANCIA = new TipoCambioDN();
	
	public static TipoCambioDN getInstancia(String nombreUsuario) {
		TipoCambioDN.nombreUsuario = nombreUsuario;
		return TipoCambioDN.INSTANCIA;
	}
		
	public Double obtieneTipoCambioPorDia(Date fecha, Short idMoneda) {
		TipoCambioSN tipoCambioSN;
		try {
			
			//Si no es Moneda Nacional
			if (!idMoneda.equals((short)484)) {
			
				//Se le quita la parte de HH:mm:ss a la fecha
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(fecha);
				
				gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
				gc.set(GregorianCalendar.MINUTE, 0);
				gc.set(GregorianCalendar.SECOND, 0);
				gc.set(GregorianCalendar.MILLISECOND, 0);
							
				tipoCambioSN = new TipoCambioSN(TipoCambioDN.nombreUsuario);
				return tipoCambioSN.obtieneTipoCambioPorDia(gc.getTime());
				
			} else { //Es moneda nacional
				return 1d;
			}
			
		} catch (ExcepcionDeAccesoADatos e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		} catch (SystemException e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		}
		return null;
	}
	
	public Double obtieneTipoCambioPorMes(Integer mes, Integer anio, Short idMoneda) {
		TipoCambioSN tipoCambioSN;
		try {
			
			//Si no es Moneda Nacional
			if (!idMoneda.equals((short)484)) {
			
				tipoCambioSN = new TipoCambioSN(TipoCambioDN.nombreUsuario);
				return tipoCambioSN.obtieneTipoCambioPorMes(mes, anio);
				
			} else { //Es moneda nacional
				return 1d;
			}
			
			
		} catch (ExcepcionDeAccesoADatos e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		} catch (SystemException e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		}
		return null;
	}
	
	
}
