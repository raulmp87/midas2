/**
 * 
 */
package mx.com.afirme.midas.interfaz.poliza;

import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 * 
 */
public class PolizaDN {

	private static final PolizaDN INSTANCIA = new PolizaDN();
	private static String nombreUsuario;

	public static PolizaDN getInstancia(String nombreUsuario) {
		PolizaDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}

	public Map<String, String> emitePoliza(CotizacionDTO cotizacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		Map<String, String> mensaje = new HashMap<String, String>();
		try {
			mensaje = new PolizaSN(PolizaDN.nombreUsuario)
					.emitePoliza(cotizacionDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mensaje;
	}

}
