/**
 * 
 */
package mx.com.afirme.midas.interfaz.poliza;

import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 * 
 */
public class PolizaSN {

	private PolizaFacadeRemote beanRemoto;
	private String nombreUsuario;

	public PolizaSN(String nombreUsuario) throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PolizaFacadeRemote.class);
			this.nombreUsuario = nombreUsuario;
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado, nombreUsuario: "+this.nombreUsuario, Level.FINEST, null);
	}

	public Map<String, String> emitePoliza(CotizacionDTO cotizacionDTO) {
		try {
			return beanRemoto.emitePoliza(cotizacionDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}
}
