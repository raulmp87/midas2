package mx.com.afirme.midas.interfaz.remesa;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RemesaSN {

private RemesaFacadeRemote beanRemoto;
	
	public RemesaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RemesaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
	public BigDecimal registraRemesa(BigDecimal idRemesa, String nombreUsuario) {
		try {
			return beanRemoto.registraRemesa(idRemesa, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPREGISTRA_REMESAS");
			sb.append("|");
			sb.append("pId_Remesa" + "=" + idRemesa + ",");
			sb.append("pUsuario" + "=" + nombreUsuario + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Registro de Remesa con id Remesa:" + idRemesa + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
