package mx.com.afirme.midas.interfaz.remesa;

import java.math.BigDecimal;


import mx.com.afirme.midas.sistema.SystemException;

public class RemesaDN {

	private static final RemesaDN INSTANCIA = new RemesaDN();
	
	public static RemesaDN getInstancia() {
		return INSTANCIA;
	}
	
	public BigDecimal registraRemesa(BigDecimal idRemesa, String nombreUsuario) throws SystemException {
		RemesaSN remesaSN = new RemesaSN();
		return remesaSN.registraRemesa(idRemesa, nombreUsuario);
	}
	
}
