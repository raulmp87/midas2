/**
 * 
 */
package mx.com.afirme.midas.interfaz.linea;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class LineaDN {

private String nombreUsuario;
	
	public LineaDN(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	public static LineaDN getInstancia(String nombreUsuario) {
		return new LineaDN(nombreUsuario);
	}
	
	public void agregar(LineaDTO linea) throws ExcepcionDeAccesoADatos, SystemException  {
		LineaSN lineaSN;
		try {
			lineaSN = new LineaSN(this.nombreUsuario);
			lineaSN.agregar(linea);
		} catch (ExcepcionDeAccesoADatos e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		} catch (SystemException e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		}
	}
	
}
