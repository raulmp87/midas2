/**
 * 
 */
package mx.com.afirme.midas.interfaz.asientocontable;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class AsientoContableSN {

	private AsientoContableFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public AsientoContableSN(String nombreUsuario) throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(AsientoContableFacadeRemote.class);
			this.nombreUsuario = nombreUsuario;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable) {
		try {
			return beanRemoto.contabilizaMovimientos(idObjetoContable, claveTransaccionContable, this.nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS");
			sb.append("|");
			sb.append("pIdentificador" + "=" + idObjetoContable + ",");
			sb.append("pCve_trans_cont" + "=" + claveTransaccionContable + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Contabilizar " + claveTransaccionContable + ", id:" +  idObjetoContable + 
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
}
