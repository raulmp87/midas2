/**
 * 
 */
package mx.com.afirme.midas.interfaz.asientocontable;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author andres.avalos
 *
 */
public class AsientoContableDN {

	private static final AsientoContableDN INSTANCIA = new AsientoContableDN();
	private static String nombreUsuario;
	

	public static AsientoContableDN getInstancia(String nombreUsuario) {
		AsientoContableDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	/**
	 * Genera asientos contables para los diferentes movimientos
	 * @param idObjetoContable Id del objeto a contabilizar (Poliza, Siniestro, Endoso, etc...)
	 * @param claveTransaccionContable Clave de transacci�n Contable (Sistema.REASEGURO_EMIS, Sistema.SINIESTRO � Sistema.REASEGURO_SIN)
	 * @return Datos Contables Movimientos Reaseguro Emision, Siniestros o Reaseguro Siniestro
	 * @throws SystemException
	 */
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable) throws SystemException {
		AsientoContableSN asientoContableSN;
		try {
			asientoContableSN = new AsientoContableSN(AsientoContableDN.nombreUsuario);
			return asientoContableSN.contabilizaMovimientos(idObjetoContable, claveTransaccionContable);
		} catch (SystemException e) {
			throw e;
		}
	}
	
}
