package mx.com.afirme.midas.interfaz.consultacobranzapoliza;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConsultaCobranzaPolizaSN {

private ConsultaCobranzaPolizaFacadeRemote beanRemoto;
	
	public ConsultaCobranzaPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = (ConsultaCobranzaPolizaFacadeRemote)serviceLocator.getEJB(ConsultaCobranzaPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public DatosCobranzaPolizaDTO consultaCobranzaPoliza (BigDecimal idToPoliza, String nombreUsuario) {
		try {
			return beanRemoto.consultaCobranzaPoliza(idToPoliza, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPCONSULTAPOLIZA");
			sb.append("|");
			sb.append("pIdentificador" + "=" + idToPoliza + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Consulta de los datos de cobranza de la poliza:" + idToPoliza + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
