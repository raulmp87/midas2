package mx.com.afirme.midas.interfaz.consultacobranzapoliza;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;

public class ConsultaCobranzaPolizaDN {
	
	private static final ConsultaCobranzaPolizaDN INSTANCIA = new ConsultaCobranzaPolizaDN();
	private static String nombreUsuario;
	
	public static ConsultaCobranzaPolizaDN getInstancia(String nombreUsuario) {
		ConsultaCobranzaPolizaDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	
	public DatosCobranzaPolizaDTO consultaCobranzaPoliza (BigDecimal idToPoliza) throws SystemException {
		ConsultaCobranzaPolizaSN consultaCobranzaPolizaSN = new ConsultaCobranzaPolizaSN();
		return consultaCobranzaPolizaSN.consultaCobranzaPoliza(idToPoliza, nombreUsuario);
	}

}
