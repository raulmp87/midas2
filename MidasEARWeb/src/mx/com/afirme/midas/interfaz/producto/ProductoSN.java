/**
 * 
 */
package mx.com.afirme.midas.interfaz.producto;

import java.util.logging.Level;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class ProductoSN {

	private ProductoFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public ProductoSN(String nombreUsuario) throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ProductoFacadeRemote.class);
			this.nombreUsuario = nombreUsuario;
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(ProductoDTO producto) {
		try {
			beanRemoto.save(producto, this.nombreUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
}
