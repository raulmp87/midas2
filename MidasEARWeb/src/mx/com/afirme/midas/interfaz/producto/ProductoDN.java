/**
 * 
 */
package mx.com.afirme.midas.interfaz.producto;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class ProductoDN {
	
private String nombreUsuario;
	
	public ProductoDN(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	public static ProductoDN getInstancia(String nombreUsuario) {
		return new ProductoDN(nombreUsuario);
	}
	
	public void agregar(ProductoDTO producto) throws ExcepcionDeAccesoADatos, SystemException  {
		ProductoSN productoSN;
		try {
			productoSN = new ProductoSN(this.nombreUsuario);
			productoSN.agregar(producto);
		} catch (ExcepcionDeAccesoADatos e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		} catch (SystemException e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		}
	}

}
