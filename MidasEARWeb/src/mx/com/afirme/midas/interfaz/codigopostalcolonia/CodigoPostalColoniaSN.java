package mx.com.afirme.midas.interfaz.codigopostalcolonia;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CodigoPostalColoniaSN {

private CodigoPostalColoniaFacadeRemote beanRemoto;
	
	public CodigoPostalColoniaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CodigoPostalColoniaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
		
	public List<CodigoPostalColoniaDTO> agregaCodigoPostal(CodigoPostalColoniaDTO codigoPostal, String nombreUsuario) {
		try {
			return beanRemoto.agregaCodigoPostal(codigoPostal, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.STPCAT_CODPOST");
			sb.append("|");
			sb.append("pCodigo_Postal" + "=" + codigoPostal.getCodigoPostal() + ",");
			sb.append("pId_colonia" + "=" + codigoPostal.getIdColonia() + ",");
			sb.append("pNombreColonia" + "=" + codigoPostal.getNombreColonia() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Registro de Codigo Postal :" + codigoPostal.getCodigoPostal() + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			
			if (e.getMessage().equals("COLONIA_DUPLICADA")){
				throw new ExcepcionDeAccesoADatos(e.getMessage());
			} else {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
			
		}
	}
	
	public List<CodigoPostalColoniaDTO> listaColonias(CodigoPostalColoniaDTO codigoPostal, String nombreUsuario) {
		try {
			return beanRemoto.listaColonias(codigoPostal, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CLISTA_COLONIAS");
			sb.append("|");
			sb.append("pCodigo_Postal" + "=" + codigoPostal.getCodigoPostal() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listado de Colonias con Codigo Postal :" + codigoPostal.getCodigoPostal() + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
}
