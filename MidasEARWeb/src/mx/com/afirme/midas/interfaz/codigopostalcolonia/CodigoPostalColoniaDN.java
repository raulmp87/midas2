package mx.com.afirme.midas.interfaz.codigopostalcolonia;

import java.util.List;

import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroDN;
import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroDTO;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.mail.MailAction;

public class CodigoPostalColoniaDN {

private static final CodigoPostalColoniaDN INSTANCIA = new CodigoPostalColoniaDN();

	private static final int EXITO_AL_GUARDAR = 0;
	private static final int FALLO_AL_GUARDAR_SEYCOS = 1;
	private static final int FALLO_AL_GUARDAR_MIDAS = 2;
	private static final int FALLO_AL_GUARDAR_SEYCOS_COLONIA_DUPLICADA = 3;
	
	
	public static CodigoPostalColoniaDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<CodigoPostalColoniaDTO> agregaCodigoPostal(CodigoPostalColoniaDTO codigoPostal, String nombreUsuario)  
			throws SystemException {
		CodigoPostalColoniaSN codigoPostalColoniaSN = new CodigoPostalColoniaSN();
		return codigoPostalColoniaSN.agregaCodigoPostal(codigoPostal, nombreUsuario);
	}
	
	public List<CodigoPostalColoniaDTO> listaColonias(String codigoPostal, String nombreUsuario)  
			throws SystemException {
		
		CodigoPostalColoniaDTO codigoPostalDTO = new CodigoPostalColoniaDTO();
		codigoPostalDTO.setCodigoPostal(codigoPostal);
		
		CodigoPostalColoniaSN codigoPostalColoniaSN = new CodigoPostalColoniaSN();
		return codigoPostalColoniaSN.listaColonias(codigoPostalDTO, nombreUsuario);
	}
	
	public int agregarColoniaCodigoPostalZonaHidroZonaSismo(CodigoPostalColoniaDTO codigoPostalColoniaDTO,
		CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO, String
		nombreUsuario) {
	    
	    try {
			List<CodigoPostalColoniaDTO> colonias=
			this.agregaCodigoPostal(codigoPostalColoniaDTO, nombreUsuario);
			if (colonias!= null && !colonias.isEmpty()) {
	        		try {
	        		    CodigoPostalZonaHidroDN.getInstancia().agregarCodigoPostalZonaHidroZonaSismo
	        		    (codigoPostalZonaHidroDTO, codigoPostalZonaSismoDTO);
	        		} catch (Throwable e) {
	        		    MailAction.enviaCorreoNotificacionAdminsMidas("Ocurrio un error registrando los datos de la Colonia :" + codigoPostalZonaHidroDTO.getId().getNombreColonia() + " en " + 
						Sistema.AMBIENTE_SISTEMA, e.getMessage());
	        		    return FALLO_AL_GUARDAR_MIDAS;
	        		}
			} else {
			    return FALLO_AL_GUARDAR_SEYCOS;
			}
	    } catch (Throwable e) {
	    	if (e.getMessage().equals("COLONIA_DUPLICADA")){
	    		return FALLO_AL_GUARDAR_SEYCOS_COLONIA_DUPLICADA;
	    	} else {
	    		return FALLO_AL_GUARDAR_SEYCOS;
	    	}
	    }
	   return EXITO_AL_GUARDAR;
	}
	
	
	public int modificarColoniaCodigoPostalZonaHidroZonaSismo(CodigoPostalColoniaDTO codigoPostalColoniaDTO,
		CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO, CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO, String
		nombreUsuario) {
	    
	    try {
			List<CodigoPostalColoniaDTO> colonias=
			this.agregaCodigoPostal(codigoPostalColoniaDTO, nombreUsuario);
			if (colonias!= null && !colonias.isEmpty()) {
	        		try {
	        		    CodigoPostalZonaHidroDN.getInstancia().modificarCodigoPostalZonaHidroZonaSismo
	        		    (codigoPostalZonaHidroDTO, codigoPostalZonaSismoDTO);
	        		} catch (Throwable e) {
	        		    MailAction.enviaCorreoNotificacionAdminsMidas("Ocurrio un error actualizando los datos de la Colonia :" + codigoPostalZonaHidroDTO.getId().getNombreColonia() + " en " + 
						Sistema.AMBIENTE_SISTEMA, e.getMessage());
	        		    return FALLO_AL_GUARDAR_MIDAS;
	        		}
			} else {
			    return FALLO_AL_GUARDAR_SEYCOS;
			}
	    } catch (Throwable e) {
	    	if (e.getMessage().equals("COLONIA_DUPLICADA")){
	    		return FALLO_AL_GUARDAR_SEYCOS_COLONIA_DUPLICADA;
	    	} else {
	    		return FALLO_AL_GUARDAR_SEYCOS;
	    	}
	    }
	   return EXITO_AL_GUARDAR;
	    
	}
	
	
	
}
