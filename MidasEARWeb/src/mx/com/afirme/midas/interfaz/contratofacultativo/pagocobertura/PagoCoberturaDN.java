package mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class PagoCoberturaDN {

	private static final PagoCoberturaDN INSTANCIA = new PagoCoberturaDN();
	private static String nombreUsuario;
	
	public static PagoCoberturaDN getInstancia(String nombreUsuario) {
		PagoCoberturaDN.nombreUsuario = nombreUsuario;
		return PagoCoberturaDN.INSTANCIA;
	}
	
	/**
	 * Genera un plan de pagos respetando la vigencia, la forma de pago y el monto a pagar dados como parámetros
	 * @param fechaInicioVigencia
	 * @param fechaFinVigencia
	 * @param formaPago
	 * @param montoAPagar 
	 * @return Listado con el plan de pagos generado
	 * @throws SystemException
	 */
	public List<PagoCoberturaDTO> calcularPagos(Date fechaInicialBase, Date fechaFinalBase, int formaPagoBase,
												Date fechaInicioVigencia, Date fechaFinVigencia, int formaPago, 
												double montoAPagar)throws SystemException {
		PagoCoberturaSN pagoCoberturaSN = new PagoCoberturaSN();
		return pagoCoberturaSN.calcularPagos(fechaInicialBase, fechaFinalBase, formaPagoBase,
											 fechaInicioVigencia, fechaFinVigencia, formaPago, 
											 montoAPagar, PagoCoberturaDN.nombreUsuario); 		
	}
}
