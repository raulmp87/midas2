package mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class PagoCoberturaSN {

	private PagoCoberturaFacadeRemote beanRemoto;
	
	public PagoCoberturaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PagoCoberturaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}	
	
	public List<PagoCoberturaDTO> calcularPagos(Date fechaInicialBase, Date fechaFinalBase, int idFormaPagoBase,
												Date fechaInicioVigencia, Date fechaFinVigencia, int formaPago, 
												double montoAPagar, String nombreUsuario){
		try {
			return beanRemoto.calcularPagos(fechaInicialBase, fechaFinalBase, idFormaPagoBase,
											fechaInicioVigencia, fechaFinVigencia, formaPago, 
											montoAPagar, nombreUsuario);		
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}	
}
