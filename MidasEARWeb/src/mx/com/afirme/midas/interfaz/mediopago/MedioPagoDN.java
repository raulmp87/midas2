package mx.com.afirme.midas.interfaz.mediopago;

import java.util.List;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class MedioPagoDN {

	private static final MedioPagoDN INSTANCIA = new MedioPagoDN();
	private static String nombreUsuario;
	
	public static MedioPagoDN getInstancia(String nombreUsuario) {
		MedioPagoDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	public List<MedioPagoDTO> listarTodos()	throws SystemException {
		MedioPagoDTO medioPago = new MedioPagoDTO();
		medioPago.setIdMedioPago(null);
		medioPago.setDescripcion(null);
		
		MedioPagoSN medioPagoSN = new MedioPagoSN();
		return medioPagoSN.listarFiltrado(medioPago, MedioPagoDN.nombreUsuario);
	}
	
	public List<MedioPagoDTO> listarFiltrado(Integer idMedioPago, String descripcion) throws SystemException {
		
		MedioPagoDTO medioPago = new MedioPagoDTO();
		medioPago.setIdMedioPago(idMedioPago);
		medioPago.setDescripcion(descripcion);
		
		MedioPagoSN medioPagoSN = new MedioPagoSN();
		return medioPagoSN.listarFiltrado(medioPago, MedioPagoDN.nombreUsuario);
	}
	
	public MedioPagoDTO getPorId(Integer idMedioPago) throws SystemException {
		if (idMedioPago == null)
			return null;
		MedioPagoDTO medioPago = new MedioPagoDTO();
		medioPago.setIdMedioPago(idMedioPago);
		medioPago.setDescripcion(null);
		
		List<MedioPagoDTO> listaTMP = new MedioPagoSN().listarFiltrado(medioPago, MedioPagoDN.nombreUsuario);
		if (listaTMP != null && !listaTMP.isEmpty())
			return listaTMP.get(0);
		else
			return null;
	}
	
}
