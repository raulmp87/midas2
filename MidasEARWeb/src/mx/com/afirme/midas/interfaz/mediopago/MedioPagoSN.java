package mx.com.afirme.midas.interfaz.mediopago;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MedioPagoSN {

	private MedioPagoFacadeRemote beanRemoto;
	
	public MedioPagoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MedioPagoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	
	public List<MedioPagoDTO> listarFiltrado(MedioPagoDTO medioPago, String nombreUsuario) {
		try {
			return beanRemoto.findByProperty(medioPago, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CIDMEDIO_PAGO");
			sb.append("|");
			sb.append("pId_Medio_Pago" + "=" + medioPago.getIdMedioPago() + ",");
			sb.append("pDescripcion" + "=" + medioPago.getDescripcion() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listado de Medios de Pago en " +
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
