package mx.com.afirme.midas.interfaz.agente;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.interfaz.agente.tipocedula.TipoCedulaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AgenteSN {
	private AgenteFacadeRemote beanRemoto;

	public AgenteSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(AgenteFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<AgenteDTO> listarAgentes(String usuario, String tipoCedula) {
		try {
			return beanRemoto.listarAgentes(usuario, tipoCedula);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CLISTA_AGENTE");
			sb.append("|");
			sb.append("pTipoCedula" + "=" + tipoCedula + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Listar Agentes con tipo cedula " + tipoCedula + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public AgenteDTO verDetalleAgente(AgenteDTO agente, String nombreUsuario) {
		try {
			return beanRemoto.findById(agente.getIdTcAgente(), nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CDETALLE_AGENTE");
			sb.append("|");
			sb.append("pId_Agente" + "=" + agente.getIdTcAgente() + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Detalle de Agentes con id " + agente.getIdTcAgente()+ " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<TipoCedulaDTO> listarTiposCedula(String nombreUsuario) {
		try {
			return beanRemoto.listarTiposCedula(nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CTIPOS_CEDULA");
						
			UtileriasWeb.enviaCorreoExcepcion("Listar tipos de cedula"  + " en " + 
					Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<AgenteDTO> listaEjecutivoAgentes(String idOficina,
			String nombreUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listaEjecutivoAgentes(idOficina, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CLISTA_EJECUTIVO_AGT");
			sb.append("|");
			sb.append("pId_Oficina" + "=" + idOficina + ",");
			
			
			UtileriasWeb.enviaCorreoExcepcion("Obtener lista de Agentes del Ejecutivo con id de Oficina: " + idOficina + 
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<AgenteDTO> listaEjecutivos(String nombreUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listaEjecutivos(nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("SEYCOS.PKG_INT_MIDAS.CLISTA_EJECUTIVO");
			
			UtileriasWeb.enviaCorreoExcepcion("Obtener lista de Ejecutivos " + 
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
}
