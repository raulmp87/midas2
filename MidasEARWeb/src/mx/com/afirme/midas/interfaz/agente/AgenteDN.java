package mx.com.afirme.midas.interfaz.agente;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.interfaz.agente.tipocedula.TipoCedulaDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class AgenteDN {
	private static final AgenteDN INSTANCIA = new AgenteDN();

	public static AgenteDN getInstancia() {
		return AgenteDN.INSTANCIA;
	}

	public List<AgenteDTO> getAgentesPorUsuario(Usuario usuario) throws SystemException{
//	    String nombreUsuarioBusqueda;
//	    if(Sistema.SEGURIDAD_ACTIVADA && Sistema.PERMISOS_SEGURIDAD_ACTIVADOS && (usuario!=null && usuario.getNombreUsuario()!=null)){
//		nombreUsuarioBusqueda= usuario.getNombreUsuario();
//	   }else{
//	       if(!UtileriasWeb.esCadenaVacia(Sistema.USUARIO_TEMPORAL_OBTENER_AGENTES)){
//	        nombreUsuarioBusqueda=Sistema.USUARIO_TEMPORAL_OBTENER_AGENTES;
//	       }else{
//		   return new ArrayList<AgenteDTO>();
//	       }
//	   }
	    
	    return this.listarAgentesPorCedulas(usuario, Sistema.CEDULAS_DE_AGENTES);
	}
	
	
//	public List<AgenteDTO> listarAgentes(Usuario usuario, String tipoCedula)
//			throws SystemException {
//		AgenteSN agenteSN = new AgenteSN();
//		return agenteSN.listarAgentes(usuario, tipoCedula);
//
//	}
	
	public List<AgenteDTO> listarAgentesPorCedulas(Usuario usuario, String... tipoCedulas)throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		List<AgenteDTO> listaResultado = new ArrayList<AgenteDTO>();
		/*for(String cedula : tipoCedulas){
			while(cedula.length()<2)
				cedula = cedula+" ";
			listaResultado = agenteSN.listarAgentes(cedula);
			if (listaResultado != null)
				listaResultado.addAll(listaResultado);
		}*/
		String claveAgentesHabilitados = "midas.danios.agenteshabilitados";
		boolean aplicaFiltradoAgentes = true;
		try {
			if (usuario != null
					&& usuario.contieneAtributo(claveAgentesHabilitados) ) {
				AtributoUsuario atributoAgentesHabilitados = usuario
				.obtenerAtributo(claveAgentesHabilitados);
				if (atributoAgentesHabilitados.isActivo()) {
					String[] idAgentesArray = ((String) atributoAgentesHabilitados
							.getValor()).split(",");
					AgenteDTO agente = new AgenteDTO();
					for(int i = 0; i < idAgentesArray.length; i++){
						agente.setIdTcAgente(Integer.valueOf(idAgentesArray[i]));
						listaResultado.add(verDetalleAgente(agente, usuario.getNombreUsuario()));
					}
				}else{
					aplicaFiltradoAgentes = false;
				}
			}else{
				aplicaFiltradoAgentes = false;
			}
			if(!aplicaFiltradoAgentes){
				listaResultado = agenteSN.listarAgentes(usuario.getNombreUsuario(), null);
				Collections.sort(listaResultado, new Comparator<AgenteDTO>(){
					public int compare(AgenteDTO o1, AgenteDTO o2) {
						return o1.getDescription().compareTo(o2.getDescription());
					}
				});								
			}
			
		}catch (ExcepcionDeAccesoADatos e){
			listaResultado=null;
		}
		return listaResultado;
	}
	
	public AgenteDTO verDetalleAgente(AgenteDTO agente, String nombreUsuario)
	throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		return agenteSN.verDetalleAgente(agente, nombreUsuario);
	}
	
	public List<TipoCedulaDTO> listarTiposCedula(String nombreUsuario) throws SystemException {
		AgenteSN agenteSN = new AgenteSN();
		return agenteSN.listarTiposCedula(nombreUsuario);
	}
	
	public List<AgenteDTO> listaEjecutivoAgentes(String idOficina,
			String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos {
		return new AgenteSN().listaEjecutivoAgentes(idOficina, nombreUsuario);
	}
	
	public List<AgenteDTO> listaEjecutivos(String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos {
		return new AgenteSN().listaEjecutivos(nombreUsuario);
	}
	
	
}
