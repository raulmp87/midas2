package mx.com.afirme.midas.interfaz.moneda;

import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.sistema.SystemException;


public class MonedaDN {

	private static final MonedaDN INSTANCIA = new MonedaDN();
	private static String nombreUsuario;
	
	public static MonedaDN getInstancia(String nombreUsuario) {
		MonedaDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}
	
	public List<MonedaDTO> listarTodos() throws SystemException {
		MonedaSN monedaSN = new MonedaSN();
		return monedaSN.listarTodos(MonedaDN.nombreUsuario);
	}
}
