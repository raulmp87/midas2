/**
 * 
 */
package mx.com.afirme.midas.interfaz.cobertura;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class CoberturaDN {

private String nombreUsuario;
	
	public CoberturaDN(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	public static CoberturaDN getInstancia(String nombreUsuario) {
		return new CoberturaDN(nombreUsuario);
	}
	
	public void agregar(CoberturaDTO cobertura) throws ExcepcionDeAccesoADatos, SystemException  {
		CoberturaSN coberturaSN;
		try {
			coberturaSN = new CoberturaSN(this.nombreUsuario);
			coberturaSN.agregar(cobertura);
		} catch (ExcepcionDeAccesoADatos e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		} catch (SystemException e) {
			//Solo imprimimos la excepcion en la consola para no interrumpir el proceso de MIDAS
			e.printStackTrace();
		}
	}
}
