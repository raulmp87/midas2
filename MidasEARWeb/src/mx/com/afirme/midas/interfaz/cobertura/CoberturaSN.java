/**
 * 
 */
package mx.com.afirme.midas.interfaz.cobertura;

import java.util.logging.Level;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author andres.avalos
 *
 */
public class CoberturaSN {

	private CoberturaFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public CoberturaSN(String nombreUsuario) throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CoberturaFacadeRemote.class);
			this.nombreUsuario = nombreUsuario;
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public void agregar(CoberturaDTO cobertura) {
		try {
			beanRemoto.save(cobertura, this.nombreUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}
	
}
