package mx.com.afirme.midas.tarifa;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TarifaForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idToRiesgo;
	private String idConcepto;
	private String descripcionConcepto;
	private String idBase1;
	private String idBase2;
	private String idBase3;
	private String idBase4;
	private String valor;
	private String descripcionBase1;
	private String descripcionBase2;
	private String descripcionBase3;
	private String descripcionBase4;
	private String version;
	
	public String getIdToRiesgo() {
		return idToRiesgo;
	}

	public void setIdToRiesgo(String idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	public String getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(String idConcepto) {
		this.idConcepto = idConcepto;
	}

	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}

	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}

	public String getIdBase1() {
		return idBase1;
	}

	public void setIdBase1(String idBase1) {
		this.idBase1 = idBase1;
	}

	public String getIdBase2() {
		return idBase2;
	}

	public void setIdBase2(String idBase2) {
		this.idBase2 = idBase2;
	}

	public String getIdBase3() {
		return idBase3;
	}

	public void setIdBase3(String idBase3) {
		this.idBase3 = idBase3;
	}

	public String getIdBase4() {
		return idBase4;
	}

	public void setIdBase4(String idBase4) {
		this.idBase4 = idBase4;
	}
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescripcionBase1() {
		return descripcionBase1;
	}

	public void setDescripcionBase1(String descripcionBase1) {
		this.descripcionBase1 = descripcionBase1;
	}

	public String getDescripcionBase2() {
		return descripcionBase2;
	}

	public void setDescripcionBase2(String descripcionBase2) {
		this.descripcionBase2 = descripcionBase2;
	}

	public String getDescripcionBase3() {
		return descripcionBase3;
	}

	public void setDescripcionBase3(String descripcionBase3) {
		this.descripcionBase3 = descripcionBase3;
	}

	public String getDescripcionBase4() {
		return descripcionBase4;
	}

	public void setDescripcionBase4(String descripcionBase4) {
		this.descripcionBase4 = descripcionBase4;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
