/**
 * 
 */
package mx.com.afirme.midas.tarifa;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author jorge.cano
 * 
 */
public class TarifaSN {
	private TarifaFacadeRemote beanRemoto;

	public TarifaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TarifaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(TarifaDTO tarifaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(tarifaDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		}
	}

	public void borrar(TarifaDTO tarifaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(tarifaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(TarifaDTO tarifaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(tarifaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<TarifaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public TarifaDTO getPorId(TarifaDTO tarifaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(tarifaDTO.getId());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<TarifaDTO> findByIdToRiesgoIdConceptoVersion(BigDecimal idToRiesgo,String idConcepto, Long version){
		return beanRemoto.findByIdToRiesgoIdConceptoVersion(idToRiesgo, idConcepto, version);
	}
}
