/**
 * 
 */
package mx.com.afirme.midas.tarifa.configuracion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ConfiguracionTarifaSN {
	private ConfiguracionTarifaFacadeRemote beanRemoto;

	public ConfiguracionTarifaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(ConfiguracionTarifaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ConfiguracionTarifaDTO> buscarPorPropiedad(
			String nombrePropiedad, Object valor)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public ConfiguracionTarifaDTO getPorId(ConfiguracionTarifaId id)
			throws ExcepcionDeAccesoADatos {

		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionTarifaDTO> listarFiltrado(ConfiguracionTarifaDTO configuracionTarifaDTO){
		return beanRemoto.listarFiltrado(configuracionTarifaDTO);
	}

}
