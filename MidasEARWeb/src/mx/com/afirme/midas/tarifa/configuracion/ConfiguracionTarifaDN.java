package mx.com.afirme.midas.tarifa.configuracion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConfiguracionTarifaDN {
	private static final ConfiguracionTarifaDN INSTANCIA = new ConfiguracionTarifaDN();

	public static ConfiguracionTarifaDN getInstancia() {
		return ConfiguracionTarifaDN.INSTANCIA;
	}

	public ConfiguracionTarifaDTO getPorId(ConfiguracionTarifaId id)
			throws SystemException, ExcepcionDeAccesoADatos {
		ConfiguracionTarifaSN configuracionTarifaSN = new ConfiguracionTarifaSN();

		return configuracionTarifaSN.getPorId(id);
	}

	public List<ConfiguracionTarifaDTO> listarFiltrado(ConfiguracionTarifaDTO configuracionTarifaDTO) throws SystemException{
		return new ConfiguracionTarifaSN().listarFiltrado(configuracionTarifaDTO);
	}
}
