package mx.com.afirme.midas.tarifa;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class TarifaDN {
	private static final TarifaDN INSTANCIA = new TarifaDN();

	public static TarifaDN getInstancia() {
		return TarifaDN.INSTANCIA;
	}

	public List<TarifaDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		TarifaSN tarifaSN = new TarifaSN();
		return tarifaSN.listarTodos();
	}

	public void agregar(TarifaDTO tarifaDTO) throws SystemException,
			ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		TarifaSN tarifaSN = new TarifaSN();
		tarifaSN.agregar(tarifaDTO);
	}

	public void modificar(TarifaDTO tarifaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		TarifaSN tarifaSN = new TarifaSN();
		tarifaSN.modificar(tarifaDTO);
	}

	public TarifaDTO getPorId(TarifaDTO tarifaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		TarifaSN tarifaSN = new TarifaSN();
		return tarifaSN.getPorId(tarifaDTO);
	}

	public void borrar(TarifaDTO tarifaDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		TarifaSN tarifaSN = new TarifaSN();
		tarifaSN.borrar(tarifaDTO);
	}
	
	public List<TarifaDTO> findByIdToRiesgoIdConceptoVersion(BigDecimal idToRiesgo,String idConcepto, Long version) throws SystemException{
		return new TarifaSN().findByIdToRiesgoIdConceptoVersion(idToRiesgo, idConcepto, version);
	}
}
