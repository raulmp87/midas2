/*
 * Generated by MyEclipse Struts
 * Template path: templates/java/JavaClass.vtl
 */
package mx.com.afirme.midas.tarifa;

import static mx.com.afirme.midas.sistema.UtileriasWeb.regresaBigDecimal;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoForm;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDN;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDTO;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaId;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonDTO;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonFacadeRemote;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * MyEclipse Struts Creation date: 06-25-2009
 * 
 */
public class TarifaAction extends MidasMappingDispatchAction {
	
	public static final Logger LOG = Logger.getLogger(TarifaAction.class);
	
	private SistemaContext sistem;
	
	public TarifaAction() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			sistem = serviceLocator.getEJB(SistemaContext.class);
		} catch (Exception e) {
			LOG.error(e);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = sistem.getExitoso();
		TarifaDN tarifaDN = TarifaDN.getInstancia();
		RiesgoForm riesgoForm = (RiesgoForm) form;
		String ids = request.getParameter(TarifaDTO.IDTORIESGO);
		if (ids==null)
			ids=request.getSession().getAttribute(TarifaDTO.IDTORIESGO).toString();
		else
			request.getSession().setAttribute(TarifaDTO.IDTORIESGO, ids);
		
		String version = request.getParameter(TarifaDTO.VERSION);
		if (version==null)
			version=request.getSession().getAttribute(TarifaDTO.VERSION).toString();
		else
			request.getSession().setAttribute(TarifaDTO.VERSION, version);
		
		
		
		int indiceParentesis = ids.lastIndexOf("(");
		String idConcepto = ids.substring(indiceParentesis-2,indiceParentesis);
		String idRiesgo = ids.substring(0,indiceParentesis-2);
		riesgoForm.setIdToRiesgo(idRiesgo);
		riesgoForm.setIdConcepto(idConcepto);
		riesgoForm.setVersion(version);
		try {
			riesgoForm.setTarifas(tarifaDN.findByIdToRiesgoIdConceptoVersion(regresaBigDecimal(riesgoForm.getIdToRiesgo()), 
					riesgoForm.getIdConcepto(), Long.parseLong(version)));
		} catch (SystemException e) {
			LOG.error(e);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = sistem.getExitoso();
		String idToRiesgo = request.getParameter(TarifaDTO.IDTORIESGO);
		String idConcepto = request.getParameter(TarifaDTO.IDCONCEPTO);
		String version = request.getParameter(TarifaDTO.VERSION);
		TarifaForm tarifaForm = new TarifaForm();
		tarifaForm.setIdToRiesgo(idToRiesgo);
		tarifaForm.setIdConcepto(idConcepto);
		tarifaForm.setVersion(version);
		return mapping.findForward(reglaNavegacion);
	}
	
	public void agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		TarifaForm tarifaForm = (TarifaForm) form;
		TarifaDTO tarifaDTO = new TarifaDTO();
		this.poblarDTO(tarifaForm, tarifaDTO);
		TarifaDN tarifaDN = TarifaDN.getInstancia();
		StringBuffer buffer = new StringBuffer();
		
		try {
			TarifaDTO tarifaTMP = null;
			tarifaTMP = tarifaDN.getPorId(tarifaDTO);
			if(tarifaTMP != null){
				tarifaDN.modificar(tarifaDTO);
				mensajeExitoModificar(tarifaForm);
			}
			else{
				tarifaDN.agregar(tarifaDTO);
				mensajeExitoAgregar(tarifaForm);
			}
		} catch (ExcepcionDeAccesoADatos e) {
			mensajeExcepcion(tarifaForm, e);
		} catch (SystemException e) {
			mensajeExcepcion(tarifaForm, e);
		} catch (ExcepcionDeLogicaNegocio e) {
			mensajeExcepcion(tarifaForm, e);
		} catch (Exception e){
			mensajeExcepcion(tarifaForm, e);		
		} finally {
			buffer = getBufferBase(tarifaForm);
		}
		
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			mensajeExcepcion(tarifaForm, e);
		}
	}
	
	public ActionForward mostrarBorrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = sistem.getExitoso();
		TarifaForm tarifaForm = (TarifaForm) form;
		poblarForm(tarifaForm, request);
		try {
			BigDecimal idToRiesgo = regresaBigDecimal(tarifaForm.getIdToRiesgo());
			tarifaForm.setDescripcionBase1(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)1,regresaBigDecimal(tarifaForm.getIdBase1())));
			tarifaForm.setDescripcionBase2(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)2,regresaBigDecimal(tarifaForm.getIdBase2())));
			tarifaForm.setDescripcionBase3(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)3,regresaBigDecimal(tarifaForm.getIdBase3())));
			tarifaForm.setDescripcionBase4(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)4,regresaBigDecimal(tarifaForm.getIdBase4())));
		} catch (ExcepcionDeAccesoADatos e) {
			LOG.error(e);
		} catch (SystemException e) {
			LOG.error(e);
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		TarifaForm tarifaForm = (TarifaForm) form;
		TarifaDTO tarifaDTO = new TarifaDTO();
		this.poblarDTO(tarifaForm, tarifaDTO);
		TarifaDN tarifaDN = TarifaDN.getInstancia();
		StringBuffer buffer = new StringBuffer();
		try {
			tarifaDN.borrar(tarifaDTO);
			mensajeExitoBorrar(tarifaForm);
		} catch (ExcepcionDeAccesoADatos e) {
			mensajeExcepcion(tarifaForm, e);
		} catch (SystemException e) {
			mensajeExcepcion(tarifaForm, e);
		}
		finally {
			buffer = getBufferBase(tarifaForm);
		}
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			mensajeExcepcion(tarifaForm, e);
		}
		return null;
	}
	
	
	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = sistem.getExitoso();
		TarifaForm tarifaForm = (TarifaForm) form;
		poblarForm(tarifaForm, request);
		try {
			BigDecimal idToRiesgo = regresaBigDecimal(tarifaForm.getIdToRiesgo());
			tarifaForm.setDescripcionBase1(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)1,regresaBigDecimal(tarifaForm.getIdBase1())));
			tarifaForm.setDescripcionBase2(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)2,regresaBigDecimal(tarifaForm.getIdBase2())));
			tarifaForm.setDescripcionBase3(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)3,regresaBigDecimal(tarifaForm.getIdBase3())));
			tarifaForm.setDescripcionBase4(getValorBase(idToRiesgo,tarifaForm.getIdConcepto(),(short)4,regresaBigDecimal(tarifaForm.getIdBase4())));
		} catch (ExcepcionDeAccesoADatos e) {
			LOG.error(e);
		} catch (SystemException e) {
			LOG.error(e);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		TarifaForm tarifaForm = (TarifaForm) form;
		TarifaDTO tarifaDTO = new TarifaDTO();
		this.poblarDTO(tarifaForm, tarifaDTO);
		TarifaDN tarifaDN = TarifaDN.getInstancia();
		StringBuffer buffer = new StringBuffer();
		
		try {
			tarifaDN.modificar(tarifaDTO);
			mensajeExitoModificar(tarifaForm);
		} catch (ExcepcionDeAccesoADatos e) {
			mensajeExcepcion(tarifaForm, e);
		} catch (SystemException e) {
			mensajeExcepcion(tarifaForm, e);
		} catch (Exception e){
			mensajeExcepcion(tarifaForm, e);		
		} finally {
			buffer = getBufferBase(tarifaForm);
		}
		
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			mensajeExcepcion(tarifaForm, e);
		}
		
	}

	@SuppressWarnings("rawtypes")
	private String getValorBase(BigDecimal idToRiesgo,String idConcepto,short idDato,BigDecimal idBaseValor){
		String valor="NOT FOUND";
		ConfiguracionTarifaDN confTarifaDN = ConfiguracionTarifaDN.getInstancia();
		ConfiguracionTarifaDTO confTarifa;
		ConfiguracionTarifaId confTarifaId = new ConfiguracionTarifaId();
		
		confTarifaId.setIdToRiesgo(idToRiesgo);
		confTarifaId.setIdConcepto(BigDecimal.valueOf(Double.valueOf(idConcepto)));
		confTarifaId.setIdDato(idDato);
		try {
			confTarifa = confTarifaDN.getPorId(confTarifaId);
			//si es una caja de texto
			if (confTarifa.getClaveTipoControl()==4)	
				valor = ""+idBaseValor;
			//si es un campo oculto
			else if(confTarifa.getClaveTipoControl()==0)
				valor = "";
			else{
				String claseRemota = confTarifa.getClaseRemota().trim();
				MidasInterfaceBase beanBase = ServiceLocator.getInstance().getEJB(claseRemota);
				if (beanBase instanceof CatalogoValorFijoFacadeRemote){
					CatalogoValorFijoId id = new CatalogoValorFijoId((int)confTarifa.getIdGrupo(),idBaseValor.toBigInteger().intValue());
					valor = ((CacheableDTO) beanBase.findById(id)).getDescription();
				}
				else if (beanBase instanceof EscalonFacadeRemote){
					EscalonDTO escalon = (EscalonDTO)beanBase.findById(Double.valueOf(idBaseValor.toString()));
					short claveTipovalidacion = confTarifa.getClaveTipoValidacion();
					if (claveTipovalidacion == 1 || claveTipovalidacion == 4)
						valor = String.format("%.4f - %.4f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
					else if (claveTipovalidacion == 2)
						valor = String.format("%.4f", escalon.getValorRangoInferior())+" % - "+String.format("%.4f",escalon.getValorRangoSuperior())+" % ";
					else if (claveTipovalidacion == 3)
						valor = String.format("$ %.2f - $ %.2f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
					else if (claveTipovalidacion == 5)
						valor = String.format("%.0f - %.0f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
					else
						valor = "" + escalon.getValorRangoInferior()+" - "+ escalon.getValorRangoSuperior();
				}
				else
					valor = ((CacheableDTO) beanBase.findById(idBaseValor)).getDescription();
			}
		} catch (ExcepcionDeAccesoADatos e) {
			LOG.error(e);
		} catch (SystemException e) {
			LOG.error(e);
		} catch (NullPointerException e){
			LOG.error(e);
		} catch (ClassNotFoundException e) {
			LOG.error(e);
		}
		return valor;
	}

	private void poblarForm(TarifaForm tarifaForm, HttpServletRequest request){
		tarifaForm.setIdToRiesgo(request.getParameter(TarifaDTO.IDTORIESGO));
		tarifaForm.setIdConcepto(request.getParameter(TarifaDTO.IDCONCEPTO));
		tarifaForm.setIdBase1(request.getParameter(TarifaDTO.IDBASE1));
		tarifaForm.setIdBase2(request.getParameter(TarifaDTO.IDBASE2));
		tarifaForm.setIdBase3(request.getParameter(TarifaDTO.IDBASE3));
		tarifaForm.setIdBase4(request.getParameter(TarifaDTO.IDBASE4));
		tarifaForm.setValor(request.getParameter(TarifaDTO.VALOR));
		tarifaForm.setVersion(request.getParameter(TarifaDTO.VERSION));
	}
	
	private void poblarDTO(TarifaForm tarifaForm, TarifaDTO tarifaDTO){
		if (tarifaDTO.getId()==null)
			tarifaDTO.setId(new TarifaId());
		try {
			if(tarifaForm.getIdToRiesgo() != null){
				tarifaDTO.getId().setIdToRiesgo(regresaBigDecimal(tarifaForm.getIdToRiesgo()));
				RiesgoDTO riesgo = new RiesgoDTO();
				riesgo.setIdToRiesgo(tarifaDTO.getId().getIdToRiesgo());
				tarifaDTO.setRiesgoDTO(new RiesgoDN().getPorId(riesgo));
			}
			if(tarifaForm.getIdConcepto() != null)
				tarifaDTO.getId().setIdConcepto(tarifaForm.getIdConcepto());
			if(tarifaForm.getIdBase1() != null)
				tarifaDTO.getId().setIdBase1(regresaBigDecimal(tarifaForm.getIdBase1()));
			if(tarifaForm.getIdBase2() != null)
				tarifaDTO.getId().setIdBase2(regresaBigDecimal(tarifaForm.getIdBase2()));
			if(tarifaForm.getIdBase3() != null)
				tarifaDTO.getId().setIdBase3(regresaBigDecimal(tarifaForm.getIdBase3()));
			if(tarifaForm.getIdBase4() != null)
				tarifaDTO.getId().setIdBase4(regresaBigDecimal(tarifaForm.getIdBase4()));
			if(tarifaForm.getVersion() != null)
				tarifaDTO.getId().setVersion(Long.parseLong(tarifaForm.getVersion()));
			if(tarifaForm.getValor() != null)
				tarifaDTO.setValor(regresaBigDecimal(tarifaForm.getValor()));
			
		} catch (SystemException e) {
			LOG.error(e);
		}
	}
	
	private StringBuffer getBufferBase(TarifaForm tarifaForm) {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<id>");
		buffer.append(tarifaForm.getTipoMensaje());
		buffer.append("</id>");
		buffer.append("<description><![CDATA[");
		buffer.append(tarifaForm.getMensaje());
		buffer.append("]]></description>");	
		buffer.append("</item>");
		buffer.append("</response>");
		return buffer;
	}
}