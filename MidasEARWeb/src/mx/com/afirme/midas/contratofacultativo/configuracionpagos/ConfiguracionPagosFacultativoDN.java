package mx.com.afirme.midas.contratofacultativo.configuracionpagos;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.configuracionpagos.ConfiguracionPagosFacultativoDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ConfiguracionPagosFacultativoDN {
	
  private static final ConfiguracionPagosFacultativoDN INSTANCIA = new ConfiguracionPagosFacultativoDN(); 
  
  public static ConfiguracionPagosFacultativoDN getInstancia(){
	  return ConfiguracionPagosFacultativoDN.INSTANCIA;
  }
  
  public void agregar(ConfiguracionPagosFacultativoDTO configuracionPagosFacultativoDTO) throws SystemException{
	  ConfiguracionPagosFacultativoSN configuracionPagosFacultativoSN = new ConfiguracionPagosFacultativoSN();
	  configuracionPagosFacultativoSN.agregar(configuracionPagosFacultativoDTO);
  }
  
  public ConfiguracionPagosFacultativoDTO modificar(ConfiguracionPagosFacultativoDTO configuracionPagosFacultativoDTO) throws SystemException{
	  ConfiguracionPagosFacultativoSN configuracionPagosFacultativoSN = new ConfiguracionPagosFacultativoSN();
	  return configuracionPagosFacultativoSN.modificar(configuracionPagosFacultativoDTO);
  }
  
  public ConfiguracionPagosFacultativoDTO getPorId(BigDecimal idToConfiguracionPagosFacultativo) throws SystemException{
	  ConfiguracionPagosFacultativoSN configuracionPagosFacultativoSN = new ConfiguracionPagosFacultativoSN();
	  return configuracionPagosFacultativoSN.getPorId(idToConfiguracionPagosFacultativo);
  }
  
  public List<ConfiguracionPagosFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor) throws SystemException{
	  ConfiguracionPagosFacultativoSN configuracionPagosFacultativoSN = new ConfiguracionPagosFacultativoSN();
	  
	  return configuracionPagosFacultativoSN.buscarPorPropiedad(propiedad, valor);
  }
  
  public List<ConfiguracionPagosFacultativoDTO> listarPorReasegurador(BigDecimal idtcreaseguradorcorredor, BigDecimal idTmContratoFacultativo) throws SystemException{
	  ConfiguracionPagosFacultativoSN configuracionPagosFacultativoSN = new ConfiguracionPagosFacultativoSN();
	  
	  return configuracionPagosFacultativoSN.listarPorReasegurador(idtcreaseguradorcorredor, idTmContratoFacultativo);
  }
  
}
