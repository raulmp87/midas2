package mx.com.afirme.midas.contratofacultativo.configuracionpagos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.configuracionpagos.ConfiguracionPagosFacultativoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class ConfiguracionPagosFacultativoSN {

	private ConfiguracionPagosFacultativoFacadeRemote beanRemoto;
	
	public ConfiguracionPagosFacultativoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ConfiguracionPagosFacultativoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto ConfiguracionPagosFacultativoFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public void agregar(ConfiguracionPagosFacultativoDTO configuracionPagosFacultativoDTO){
		beanRemoto.save(configuracionPagosFacultativoDTO);
	}
	
	public ConfiguracionPagosFacultativoDTO modificar(ConfiguracionPagosFacultativoDTO configuracionPagosFacultativoDTO){
		return beanRemoto.update(configuracionPagosFacultativoDTO);
	}
	 
	public ConfiguracionPagosFacultativoDTO getPorId(BigDecimal idToConfiguracionPagosFacultativo){
		return beanRemoto.findById(idToConfiguracionPagosFacultativo);
	}
	
	public List<ConfiguracionPagosFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor){
		  
		  return beanRemoto.findByProperty(propiedad, valor);
	}
	
	public List<ConfiguracionPagosFacultativoDTO> listarPorReasegurador(BigDecimal idtcreaseguradorcorredor, BigDecimal idTmContratoFacultativo) throws SystemException{

		return beanRemoto.listarPorReasegurador(idtcreaseguradorcorredor, idTmContratoFacultativo);
	}
	
}
