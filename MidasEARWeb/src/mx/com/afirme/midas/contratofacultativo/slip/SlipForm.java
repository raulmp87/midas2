package mx.com.afirme.midas.contratofacultativo.slip;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cliente.ClienteForm;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionForm;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionForm;
import mx.com.afirme.midas.reaseguro.soporte.slip.AseguradoSoporteDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SlipForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String idToSlip;
    private String idToCotizacion;
    private String idTcSubRamo;
    private String numeroInciso;
    private String idToSeccion;
    private String numeroSubInciso;
    private String tipoDistribucion;
    private String siniestrabilidad;
    private String informacionAdicional;
    private String tipoSlip;
    private String estatus;
    private String estatusCotizacion;
    private CotizacionForm cotizacionForm=new CotizacionForm();
    private List<SlipAnexoDTO> slipDocumentosAnexosDTO = new ArrayList<SlipAnexoDTO>(0);
    private List<ReaseguroIncisoCotizacionForm> listaReaseguroIncisoCotizacion = new ArrayList<ReaseguroIncisoCotizacionForm>();
    private ReaseguroCotizacionForm reaseguroCotizacion;
    private String editaIncisos;
    private String editaSubInisos;
 	private String idTmLineaSoporteReaseguro;
    private String idToSoporteReaseguro;
    private AseguradoSoporteDTO aseguradorSoporteForm = new  AseguradoSoporteDTO();
    private String autorizadoRetencion;
    private ClienteForm cliente;
    private String nombreAsegurado;
    
	public String getIdToSlip() {
		return idToSlip;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public String getTipoDistribucion() {
		return tipoDistribucion;
	}
	public void setTipoDistribucion(String tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}
	public String getSiniestrabilidad() {
		return siniestrabilidad;
	}
	public void setSiniestrabilidad(String siniestrabilidad) {
		this.siniestrabilidad = siniestrabilidad;
	}
	public String getInformacionAdicional() {
		return informacionAdicional;
	}
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}
	public String getTipoSlip() {
		return tipoSlip;
	}
	public void setTipoSlip(String tipoSlip) {
		this.tipoSlip = tipoSlip;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getEstatusCotizacion() {
		return estatusCotizacion;
	}
	public void setEstatusCotizacion(String estatusCotizacion) {
		this.estatusCotizacion = estatusCotizacion;
	}
	public List<SlipAnexoDTO> getSlipDocumentosAnexosDTO() {
		return slipDocumentosAnexosDTO;
	}
	public void setSlipDocumentosAnexosDTO(List<SlipAnexoDTO> slipDocumentosAnexosDTO) {
		this.slipDocumentosAnexosDTO = slipDocumentosAnexosDTO;
	}
	public CotizacionForm getCotizacionForm() {
		return cotizacionForm;
	}
	public void setCotizacionForm(CotizacionForm cotizacionForm) {
		this.cotizacionForm = cotizacionForm;
	}
	public List<ReaseguroIncisoCotizacionForm> getListaReaseguroIncisoCotizacion() {
		return listaReaseguroIncisoCotizacion;
	}
	public void setListaReaseguroIncisoCotizacion(List<ReaseguroIncisoCotizacionForm> listaReaseguroIncisoCotizacion) {
		this.listaReaseguroIncisoCotizacion = listaReaseguroIncisoCotizacion;
	}
	public ReaseguroCotizacionForm getReaseguroCotizacion() {
		return reaseguroCotizacion;
	}
	public void setReaseguroCotizacion(ReaseguroCotizacionForm reaseguroCotizacion) {
		this.reaseguroCotizacion = reaseguroCotizacion;
	}
	public String getEditaIncisos() {
		return editaIncisos;
	}
	public void setEditaIncisos(String editaIncisos) {
		this.editaIncisos = editaIncisos;
	}
	public String getEditaSubInisos() {
		return editaSubInisos;
	}
	public void setEditaSubInisos(String editaSubInisos) {
		this.editaSubInisos = editaSubInisos;
	}
	public String getIdTmLineaSoporteReaseguro() {
	    return idTmLineaSoporteReaseguro;
	}
	public void setIdTmLineaSoporteReaseguro(String idTmLineaSoporteReaseguro) {
		this.idTmLineaSoporteReaseguro = idTmLineaSoporteReaseguro;
	}
	public String getIdToSoporteReaseguro() {
		return idToSoporteReaseguro;
	}
	public void setIdToSoporteReaseguro(String idToSoporteReaseguro) {
		this.idToSoporteReaseguro = idToSoporteReaseguro;
	}
	public AseguradoSoporteDTO getAseguradorSoporteForm() {
		return aseguradorSoporteForm;
	}
	public void setAseguradorSoporteForm(AseguradoSoporteDTO aseguradorSoporteForm) {
		this.aseguradorSoporteForm = aseguradorSoporteForm;
	}
	public String getAutorizadoRetencion() {
		return autorizadoRetencion;
	}
	public void setAutorizadoRetencion(String autorizadoRetencion) {
		this.autorizadoRetencion = autorizadoRetencion;
	}
	public ClienteForm getCliente() {
		return cliente;
	}
	public void setCliente(ClienteForm cliente) {
		this.cliente = cliente;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
}
