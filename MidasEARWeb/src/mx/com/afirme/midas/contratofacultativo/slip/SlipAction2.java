/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
//import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
//import mx.com.afirme.midas.sistema.StringUtil;
//import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author jmartinez
 *
 */
public class SlipAction2 extends MidasMappingDispatchAction {
	
		
	public ActionForward mostrarAviacionSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	
	
	public ActionForward mostrarBarcosSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward mostrarEquipoContratistaSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward mostrarIncedioSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward mostrarObraCivilSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward mostrarRCConstructoresSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward mostrarRCFuncionariosSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward mostrarTransportesDeCargaSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException,ExcepcionDeAccesoADatos {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
}
