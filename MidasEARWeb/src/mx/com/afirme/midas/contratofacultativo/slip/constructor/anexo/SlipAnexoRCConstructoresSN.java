package mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoConstructoresFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAnexoRCConstructoresSN {
	private SlipAnexoConstructoresFacadeRemote beanRemoto;

	public SlipAnexoRCConstructoresSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SlipAnexoConstructoresFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}

	public void  agregar(SlipAnexoConstructoresDTO slipBarcoDTO) throws ExcepcionDeAccesoADatos {
		try {
		 	   beanRemoto.save(slipBarcoDTO);
	 	} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SlipAnexoConstructoresDTO slipBarcoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(slipBarcoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(SlipAnexoConstructoresDTO slipBarcoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(slipBarcoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SlipAnexoConstructoresDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SlipAnexoConstructoresDTO getPorId(BigDecimal idToSlipAnexo) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToSlipAnexo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipAnexoConstructoresDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipAnexoConstructoresDTO> listarAnexosSlip(BigDecimal idToSlip) {
		try {
			return beanRemoto.findByProperty("slipDTO.idToSlip", idToSlip);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SlipAnexoConstructoresDTO> buscarAnexos(BigDecimal idToSlip, BigDecimal numeroInciso){
		try {
			return beanRemoto.buscarAnexos(idToSlip, numeroInciso);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}		
	}
}
