package mx.com.afirme.midas.contratofacultativo.slip.incendio;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipIncendioDN {
	private static final SlipIncendioDN INSTANCIA = new SlipIncendioDN();

	public static SlipIncendioDN getInstancia() {
		return SlipIncendioDN.INSTANCIA;
	}

  
	public void agregar(SlipIncendioDTO slipIncendioDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  SlipSN slipSN = new SlipSN();
		  SlipDTO slipDTO = slipSN.getPorId(slipIncendioDTO.getIdToSlip());
		  slipIncendioDTO.setSlipDTO(slipDTO);
		  new SlipIncendioSN().agregar(slipIncendioDTO);
	}

	public void modificar(SlipIncendioDTO slipIncendioDTO) throws SystemException,ExcepcionDeAccesoADatos {
		SlipSN slipSN = new SlipSN();
	    SlipDTO slipDTO = slipSN.getPorId(slipIncendioDTO.getIdToSlip());
		slipIncendioDTO.setSlipDTO(slipDTO);
		new SlipIncendioSN().modificar(slipIncendioDTO);
	}

	public SlipIncendioDTO getPorId(SlipIncendioDTO slipIncendioDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipIncendioSN().getPorId(slipIncendioDTO.getIdToSlip());
	}
	
	public SlipIncendioDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipIncendioSN().getPorId(idToSlip);
	}

	public void borrar(SlipIncendioDTO slipIncendioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipIncendioSN slipSN = new SlipIncendioSN();
		slipSN.borrar(slipIncendioDTO);
	}

	public List<SlipIncendioDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipIncendioSN().buscarPorPropiedad(propiedad, valor);
	}
}
