package mx.com.afirme.midas.contratofacultativo.slip.obracivil;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipObraCivilDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipObraCivilDN {
	private static final SlipObraCivilDN INSTANCIA = new SlipObraCivilDN();

	public static SlipObraCivilDN getInstancia() {
		return SlipObraCivilDN.INSTANCIA;
	}

  
	public void agregar(SlipObraCivilDTO slipObraCivilDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipObraCivilSN().agregar(slipObraCivilDTO);
	}

	public void modificar(SlipObraCivilDTO slipObraCivilDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipObraCivilSN().modificar(slipObraCivilDTO);
	}

	public SlipObraCivilDTO getPorId(SlipObraCivilDTO slipObraCivilDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipObraCivilSN().getPorId(slipObraCivilDTO.getIdToSlip());
	}
	
	public SlipObraCivilDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipObraCivilSN().getPorId(idToSlip);
	}

	public void borrar(SlipObraCivilDTO slipObraCivilDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipObraCivilSN slipSN = new SlipObraCivilSN();
		slipSN.borrar(slipObraCivilDTO);
	}

	public List<SlipObraCivilDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipObraCivilSN().buscarPorPropiedad(propiedad, valor);
	}
}
