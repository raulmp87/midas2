package mx.com.afirme.midas.contratofacultativo.slip;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ListarSlipAction extends MidasMappingDispatchAction{

	
	public ActionForward listarSlipPendientes(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try{
			@SuppressWarnings("unused")
			List<SlipDTO> slipDTO = new ArrayList<SlipDTO>();
			slipDTO = ListarSlipDN.getInstancia().listarSolicitudesPendientes();
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void listarSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try{
			List<SlipDTO> slipDTO = new ArrayList<SlipDTO>();
			slipDTO = ListarSlipDN.getInstancia().listarSolicitudesPendientes();
			MidasJsonBase json = new MidasJsonBase();
			for(int i = 0; i < slipDTO.size(); i++){
				if(Integer.parseInt(slipDTO.get(i).getEstatus().toBigInteger().toString()) == 0){
					MidasJsonRow row = new MidasJsonRow();
					SlipDTO slipTemp = slipDTO.get(i);
					String id = slipTemp.getIdToSlip().toBigInteger().toString();
					row.setId(id);
					row.setDatos(id,
							slipTemp.getIdToCotizacion().toBigInteger().toString(),
							slipTemp.getIdTcSubRamo().toBigInteger().toString(),
							(slipTemp.getNumeroInciso()==null?" ": slipTemp.getNumeroInciso().toBigInteger().toString()),
							(slipTemp.getNumeroSubInciso()==null?" ": slipTemp.getNumeroSubInciso().toBigInteger().toString()),
							slipTemp.getTipoDistribucion()
							,MidasJsonRow.generarLineaImagenDataGrid("/MidasWeb/img/Edit14.gif","","lineaSeleccionada(" + id + ")",""));
					json.addRow(row);
				}
			
			}
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			
		} catch (ExcepcionDeAccesoADatos e) {
			
		} catch (IOException io){
			
		}
	}
	
	public ActionForward modificarAgregarSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = "";
		int subramo = Integer.parseInt(request.getParameter("idTcSubRamo"));
		switch (subramo){
		case 1:
			reglaNavegacion = "aviacion";
			break;
		case 2:
			reglaNavegacion = "obracivil";
			break;
		case 3:
			reglaNavegacion = "contratista";
			break;
		case 4:
			reglaNavegacion = "barcos";
			break;
		case 5:
			reglaNavegacion = "constructores";
			break;
		case 6:
			reglaNavegacion = "funcionarios";
			break;
		case 7:
			reglaNavegacion = "transporte";
			break;
		case 8:
			reglaNavegacion = "incendio";
			break;
		case 9:
			reglaNavegacion = "general";
			break;
		}
		request.setAttribute("idToSlip", request.getAttribute("cId"));
		return mapping.findForward(reglaNavegacion);
	}
	

}