package mx.com.afirme.midas.contratofacultativo.slip.aviacion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipAviacionDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAviacionDN {
	private static final SlipAviacionDN INSTANCIA = new SlipAviacionDN();

	public static SlipAviacionDN getInstancia() {
		return SlipAviacionDN.INSTANCIA;
	}

  
	public void agregar(SlipAviacionDTO slipAviacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipAviacionSN().agregar(slipAviacionDTO);
	}

	public void modificar(SlipAviacionDTO slipAviacionDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipAviacionSN().modificar(slipAviacionDTO);
	}

	public SlipAviacionDTO getPorId(SlipAviacionDTO slipAviacionDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAviacionSN().getPorId(slipAviacionDTO.getIdToSlip());
	}
	
	public SlipAviacionDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAviacionSN().getPorId(idToSlip);
	}

	public void borrar(SlipAviacionDTO slipAviacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipAviacionSN slipSN = new SlipAviacionSN();
		slipSN.borrar(slipAviacionDTO);
	}

	public List<SlipAviacionDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAviacionSN().buscarPorPropiedad(propiedad, valor);
	}
}
