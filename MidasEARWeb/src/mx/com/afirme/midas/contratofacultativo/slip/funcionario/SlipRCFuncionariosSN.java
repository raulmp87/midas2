package mx.com.afirme.midas.contratofacultativo.slip.funcionario;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;


import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTOId;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipRCFuncionariosSN {
	private SlipFuncionarioFacadeRemote beanRemoto;

	public SlipRCFuncionariosSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SlipFuncionarioFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}

	public void  agregar(SlipFuncionarioDTO SlipFuncionarioDTO) throws ExcepcionDeAccesoADatos {
		try {
		 	   beanRemoto.save(SlipFuncionarioDTO);
	 	} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SlipFuncionarioDTO SlipFuncionarioDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(SlipFuncionarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(SlipFuncionarioDTO SlipFuncionarioDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(SlipFuncionarioDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SlipFuncionarioDTO getPorId(SlipFuncionarioDTOId id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SlipFuncionarioDTO> listarAnexosSlip(BigDecimal idToSlip) {
		try {
			return beanRemoto.findByProperty("slipDTO.idToSlip", idToSlip);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
