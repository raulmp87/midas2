package mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoEquipoContratistaDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipBarcoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAnexoRCConstructoresDN {
	private static final SlipAnexoRCConstructoresDN INSTANCIA = new SlipAnexoRCConstructoresDN();

	public static SlipAnexoRCConstructoresDN getInstancia() {
		return SlipAnexoRCConstructoresDN.INSTANCIA;
	}

  
	public void agregar(SlipAnexoConstructoresDTO slipBarcoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipAnexoRCConstructoresSN().agregar(slipBarcoDTO);
	}

	public void modificar(SlipAnexoConstructoresDTO slipBarcoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipAnexoRCConstructoresSN().modificar(slipBarcoDTO);
	}

	public SlipAnexoConstructoresDTO getPorId(SlipAnexoConstructoresDTO SlipAnexoConstructoresDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoRCConstructoresSN().getPorId(SlipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
	}
	
	public SlipAnexoConstructoresDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoRCConstructoresSN().getPorId(idToSlip);
	}

	public void borrar(SlipAnexoConstructoresDTO slipBarcoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipAnexoRCConstructoresSN slipSN = new SlipAnexoRCConstructoresSN();
		slipSN.borrar(slipBarcoDTO);
	}

	public List<SlipAnexoConstructoresDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoRCConstructoresSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<SlipAnexoConstructoresDTO> buscarAnexos(BigDecimal idToSlip, BigDecimal numeroInciso) throws SystemException, ExcepcionDeAccesoADatos{
		return new SlipAnexoRCConstructoresSN().buscarAnexos(idToSlip,numeroInciso);
		
	}
	
}
