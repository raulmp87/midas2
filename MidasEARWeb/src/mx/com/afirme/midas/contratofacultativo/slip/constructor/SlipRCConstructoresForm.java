/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.constructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.tipomaquinaria.TipoMaquinariaDN;
import mx.com.afirme.midas.catalogos.tipomaquinaria.TipoMaquinariaDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo.SlipAnexoRCConstructoresDN;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAnexoSoporteDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author jmartinez
 *
 */
public class SlipRCConstructoresForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5346970531710498313L;
    private String idToSlip;
	private String numeroInciso;
	private String descripcionObra;
	private String duracionObra;
	private String sitioTrabajo;
	private String colindantes;
	private String sistemasPreventivosContraDanos;
	private String experienciaParaRealizarObra;
	private String valorEstimadoObra;
	private String paraquienRealizaObra;
	private String tipoMaquinaria;
	private String anexarcontratosPlanos;
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the descripcionObra
	 */
	public String getDescripcionObra() {
		return descripcionObra;
	}
	/**
	 * @param descripcionObra the descripcionObra to set
	 */
	public void setDescripcionObra(String descripcionObra) {
		this.descripcionObra = descripcionObra;
	}
	/**
	 * @return the duracionObra
	 */
	public String getDuracionObra() {
		return duracionObra;
	}
	/**
	 * @param duracionObra the duracionObra to set
	 */
	public void setDuracionObra(String duracionObra) {
		this.duracionObra = duracionObra;
	}
	/**
	 * @return the sitioTrabajo
	 */
	public String getSitioTrabajo() {
		return sitioTrabajo;
	}
	/**
	 * @param sitioTrabajo the sitioTrabajo to set
	 */
	public void setSitioTrabajo(String sitioTrabajo) {
		this.sitioTrabajo = sitioTrabajo;
	}
	/**
	 * @return the colindantes
	 */
	public String getColindantes() {
		return colindantes;
	}
	/**
	 * @param colindantes the colindantes to set
	 */
	public void setColindantes(String colindantes) {
		this.colindantes = colindantes;
	}
	/**
	 * @return the sistemasPreventivosContraDanos
	 */
	public String getSistemasPreventivosContraDanos() {
		return sistemasPreventivosContraDanos;
	}
	/**
	 * @param sistemasPreventivosContraDanos the sistemasPreventivosContraDanos to set
	 */
	public void setSistemasPreventivosContraDanos(
			String sistemasPreventivosContraDanos) {
		this.sistemasPreventivosContraDanos = sistemasPreventivosContraDanos;
	}
	/**
	 * @return the experienciaParaRealizarObra
	 */
	public String getExperienciaParaRealizarObra() {
		return experienciaParaRealizarObra;
	}
	/**
	 * @param experienciaParaRealizarObra the experienciaParaRealizarObra to set
	 */
	public void setExperienciaParaRealizarObra(String experienciaParaRealizarObra) {
		this.experienciaParaRealizarObra = experienciaParaRealizarObra;
	}
	/**
	 * @return the valorEstimadoObra
	 */
	public String getValorEstimadoObra() {
		return valorEstimadoObra;
	}
	/**
	 * @param valorEstimadoObra the valorEstimadoObra to set
	 */
	public void setValorEstimadoObra(String valorEstimadoObra) {
		this.valorEstimadoObra = valorEstimadoObra;
	}
	/**
	 * @return the paraquienRealizaObra
	 */
	public String getParaquienRealizaObra() {
		return paraquienRealizaObra;
	}
	/**
	 * @param paraquienRealizaObra the paraquienRealizaObra to set
	 */
	public void setParaquienRealizaObra(String paraquienRealizaObra) {
		this.paraquienRealizaObra = paraquienRealizaObra;
	}
	/**
	 * @return the tipoMaquinaria
	 */
	public String getTipoMaquinaria() {
		return tipoMaquinaria;
	}
	/**
	 * @param tipoMaquinaria the tipoMaquinaria to set
	 */
	public void setTipoMaquinaria(String tipoMaquinaria) {
		this.tipoMaquinaria = tipoMaquinaria;
	}
	/**
	 * @return the anexarcontratosPlanos
	 */
	public String getAnexarcontratosPlanos() {
		return anexarcontratosPlanos;
	}
	/**
	 * @param anexarcontratosPlanos the anexarcontratosPlanos to set
	 */
	public void setAnexarcontratosPlanos(String anexarcontratosPlanos) {
		this.anexarcontratosPlanos = anexarcontratosPlanos;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
 		request.setAttribute("coberturas", 1);
		List<TipoMaquinariaDTO> tipoMaquinariaList = null;
		List<SlipAnexoConstructoresDTO> listaAnexosConstructoresList = null;
		List<SlipAnexoSoporteDTO> slipAnexosSlipConstructoresList = new ArrayList<SlipAnexoSoporteDTO>();
	  	try {
			//listaAnexosConstructoresList = SlipAnexoRCConstructoresDN.getInstancia().buscarPorPropiedad("slipConstructoresDTO.id.idToSlip",new BigDecimal(idToSlip));
			
			listaAnexosConstructoresList = SlipAnexoRCConstructoresDN.getInstancia().buscarAnexos(new BigDecimal(idToSlip), new BigDecimal(numeroInciso));
			
			tipoMaquinariaList = TipoMaquinariaDN.getInstancia().listarTodos();
		 	for(SlipAnexoConstructoresDTO slipAnexoConstructoresDTO :  listaAnexosConstructoresList){
	 			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
	 	 		slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
	 			slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
	 			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoConstructoresDTO.getIdToControlArchivo()); 
	 			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
	 			slipAnexosSlipConstructoresList.add(slipAnexoSoporteDTO);
	 		} 
	  	} catch (ExcepcionDeAccesoADatos e) {
 			e.printStackTrace();
		} catch (SystemException e) {
 			e.printStackTrace();
		}
	 	request.setAttribute("tipoMaquinariaList", tipoMaquinariaList);
		request.setAttribute("anexosConstructores",slipAnexosSlipConstructoresList); 
	 	return errors;
	}
}
