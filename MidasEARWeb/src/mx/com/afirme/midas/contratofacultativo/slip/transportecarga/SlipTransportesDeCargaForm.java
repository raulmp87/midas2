/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.transportecarga;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class SlipTransportesDeCargaForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2356883531528378903L;

	private String idToSlip;
	private String numeroInciso;
	private String lugarOrigen;
	private String lugarDestino;
	private String bienesAsegurados;
	private String tipoEmpaqueEmbalaje;
	private String limiteMaximoEmbarque;
	private String volumenAnualEmbarque;
	private String riesgoAmparados;
	private String mediosTransportes;
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the lugarOrigen
	 */
	public String getLugarOrigen() {
		return lugarOrigen;
	}
	/**
	 * @param lugarOrigen the lugarOrigen to set
	 */
	public void setLugarOrigen(String lugarOrigen) {
		this.lugarOrigen = lugarOrigen;
	}
	/**
	 * @return the lugarDestino
	 */
	public String getLugarDestino() {
		return lugarDestino;
	}
	/**
	 * @param lugarDestino the lugarDestino to set
	 */
	public void setLugarDestino(String lugarDestino) {
		this.lugarDestino = lugarDestino;
	}
	/**
	 * @return the bienesAsegurados
	 */
	public String getBienesAsegurados() {
		return bienesAsegurados;
	}
	/**
	 * @param bienesAsegurados the bienesAsegurados to set
	 */
	public void setBienesAsegurados(String bienesAsegurados) {
		this.bienesAsegurados = bienesAsegurados;
	}
	/**
	 * @return the tipoEmpaqueEmbalaje
	 */
	public String getTipoEmpaqueEmbalaje() {
		return tipoEmpaqueEmbalaje;
	}
	/**
	 * @param tipoEmpaqueEmbalaje the tipoEmpaqueEmbalaje to set
	 */
	public void setTipoEmpaqueEmbalaje(String tipoEmpaqueEmbalaje) {
		this.tipoEmpaqueEmbalaje = tipoEmpaqueEmbalaje;
	}
	/**
	 * @return the limiteMaximoEmbarque
	 */
	public String getLimiteMaximoEmbarque() {
		return limiteMaximoEmbarque;
	}
	/**
	 * @param limiteMaximoEmbarque the limiteMaximoEmbarque to set
	 */
	public void setLimiteMaximoEmbarque(String limiteMaximoEmbarque) {
		this.limiteMaximoEmbarque = limiteMaximoEmbarque;
	}
	/**
	 * @return the volumenAnualEmbarque
	 */
	public String getVolumenAnualEmbarque() {
		return volumenAnualEmbarque;
	}
	/**
	 * @param volumenAnualEmbarque the volumenAnualEmbarque to set
	 */
	public void setVolumenAnualEmbarque(String volumenAnualEmbarque) {
		this.volumenAnualEmbarque = volumenAnualEmbarque;
	}
	/**
	 * @return the riesgoAmparados
	 */
	public String getRiesgoAmparados() {
		return riesgoAmparados;
	}
	/**
	 * @param riesgoAmparados the riesgoAmparados to set
	 */
	public void setRiesgoAmparados(String riesgoAmparados) {
		this.riesgoAmparados = riesgoAmparados;
	}
	/**
	 * @return the mediosTransportes
	 */
	public String getMediosTransportes() {
		return mediosTransportes;
	}
	/**
	 * @param mediosTransportes the mediosTransportes to set
	 */
	public void setMediosTransportes(String mediosTransportes) {
		this.mediosTransportes = mediosTransportes;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	
}
