/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.barco;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.moneda.MonedaSN;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author jmartinez
 *
 */
public class SlipBarcosForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4720969429870414407L;
    private String idToSlip;
	private String numeroInciso;
	private String banderadeConveniencia;
	private String materialdelCasco;
	private String tonelajeBruto;
	private String tipodeEmbarcacion;
	private String tonelajeNeto;
	private String ano;
	private String sumaAsegurada;
	private String areadeNavegacion;
	private String sumaAseguradaCasco;
	private String uso;
	private String sumaAseguradaRC;
	private String puertodeRegistro;
	private String sumaAseguradaValorIncrementado;
	private String clasificacion;
	private String eslora;
	private String serie;
	private String manga;
	private String matricula;
	private String puntal;
	private String lugardeConstruccion;
	private String ultimafechAmttoInspeccion;
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the banderadeConveniencia
	 */
	public String getBanderadeConveniencia() {
		return banderadeConveniencia;
	}
	/**
	 * @param banderadeConveniencia the banderadeConveniencia to set
	 */
	public void setBanderadeConveniencia(String banderadeConveniencia) {
		this.banderadeConveniencia = banderadeConveniencia;
	}
	/**
	 * @return the materialdelCasco
	 */
	public String getMaterialdelCasco() {
		return materialdelCasco;
	}
	/**
	 * @param materialdelCasco the materialdelCasco to set
	 */
	public void setMaterialdelCasco(String materialdelCasco) {
		this.materialdelCasco = materialdelCasco;
	}
	/**
	 * @return the tonelajeBruto
	 */
	public String getTonelajeBruto() {
		return tonelajeBruto;
	}
	/**
	 * @param tonelajeBruto the tonelajeBruto to set
	 */
	public void setTonelajeBruto(String tonelajeBruto) {
		this.tonelajeBruto = tonelajeBruto;
	}
	/**
	 * @return the tipodeEmbarcacion
	 */
	public String getTipodeEmbarcacion() {
		return tipodeEmbarcacion;
	}
	/**
	 * @param tipodeEmbarcacion the tipodeEmbarcacion to set
	 */
	public void setTipodeEmbarcacion(String tipodeEmbarcacion) {
		this.tipodeEmbarcacion = tipodeEmbarcacion;
	}
	/**
	 * @return the tonelajeNeto
	 */
	public String getTonelajeNeto() {
		return tonelajeNeto;
	}
	/**
	 * @param tonelajeNeto the tonelajeNeto to set
	 */
	public void setTonelajeNeto(String tonelajeNeto) {
		this.tonelajeNeto = tonelajeNeto;
	}
	/**
	 * @return the ano
	 */
	public String getAno() {
		return ano;
	}
	/**
	 * @param ano the ano to set
	 */
	public void setAno(String ano) {
		this.ano = ano;
	}
	/**
	 * @return the sumaAsegurada
	 */
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	/**
	 * @param sumaAsegurada the sumaAsegurada to set
	 */
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	/**
	 * @return the areadeNavegacion
	 */
	public String getAreadeNavegacion() {
		return areadeNavegacion;
	}
	/**
	 * @param areadeNavegacion the areadeNavegacion to set
	 */
	public void setAreadeNavegacion(String areadeNavegacion) {
		this.areadeNavegacion = areadeNavegacion;
	}
	/**
	 * @return the sumaAseguradaCasco
	 */
	public String getSumaAseguradaCasco() {
		return sumaAseguradaCasco;
	}
	/**
	 * @param sumaAseguradaCasco the sumaAseguradaCasco to set
	 */
	public void setSumaAseguradaCasco(String sumaAseguradaCasco) {
		this.sumaAseguradaCasco = sumaAseguradaCasco;
	}
	/**
	 * @return the uso
	 */
	public String getUso() {
		return uso;
	}
	/**
	 * @param uso the uso to set
	 */
	public void setUso(String uso) {
		this.uso = uso;
	}
	/**
	 * @return the sumaAseguradaRC
	 */
	public String getSumaAseguradaRC() {
		return sumaAseguradaRC;
	}
	/**
	 * @param sumaAseguradaRC the sumaAseguradaRC to set
	 */
	public void setSumaAseguradaRC(String sumaAseguradaRC) {
		this.sumaAseguradaRC = sumaAseguradaRC;
	}
	/**
	 * @return the puertodeRegistro
	 */
	public String getPuertodeRegistro() {
		return puertodeRegistro;
	}
	/**
	 * @param puertodeRegistro the puertodeRegistro to set
	 */
	public void setPuertodeRegistro(String puertodeRegistro) {
		this.puertodeRegistro = puertodeRegistro;
	}
	/**
	 * @return the sumaAseguradaValorIncrementado
	 */
	public String getSumaAseguradaValorIncrementado() {
		return sumaAseguradaValorIncrementado;
	}
	/**
	 * @param sumaAseguradaValorIncrementado the sumaAseguradaValorIncrementado to set
	 */
	public void setSumaAseguradaValorIncrementado(
			String sumaAseguradaValorIncrementado) {
		this.sumaAseguradaValorIncrementado = sumaAseguradaValorIncrementado;
	}
	/**
	 * @return the clasificacion
	 */
	public String getClasificacion() {
		return clasificacion;
	}
	/**
	 * @param clasificacion the clasificacion to set
	 */
	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}
	/**
	 * @return the eslora
	 */
	public String getEslora() {
		return eslora;
	}
	/**
	 * @param eslora the eslora to set
	 */
	public void setEslora(String eslora) {
		this.eslora = eslora;
	}
	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}
	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}
	/**
	 * @return the manga
	 */
	public String getManga() {
		return manga;
	}
	/**
	 * @param manga the manga to set
	 */
	public void setManga(String manga) {
		this.manga = manga;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the puntal
	 */
	public String getPuntal() {
		return puntal;
	}
	/**
	 * @param puntal the puntal to set
	 */
	public void setPuntal(String puntal) {
		this.puntal = puntal;
	}
	/**
	 * @return the lugardeConstruccion
	 */
	public String getLugardeConstruccion() {
		return lugardeConstruccion;
	}
	/**
	 * @param lugardeConstruccion the lugardeConstruccion to set
	 */
	public void setLugardeConstruccion(String lugardeConstruccion) {
		this.lugardeConstruccion = lugardeConstruccion;
	}
	/**
	 * @return the ultimafechAmttoInspeccion
	 */
	public String getUltimafechAmttoInspeccion() {
		return ultimafechAmttoInspeccion;
	}
	/**
	 * @param ultimafechAmttoInspeccion the ultimafechAmttoInspeccion to set
	 */
	public void setUltimafechAmttoInspeccion(String ultimafechAmttoInspeccion) {
		this.ultimafechAmttoInspeccion = ultimafechAmttoInspeccion;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	} 
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
			if (UtileriasWeb.esCadenaVacia(lugardeConstruccion)){
				request.setAttribute("coberturas", 1);
			}
	 	return errors;
	}
	
	
	
}
