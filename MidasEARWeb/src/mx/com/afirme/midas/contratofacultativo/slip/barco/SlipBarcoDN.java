package mx.com.afirme.midas.contratofacultativo.slip.barco;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipBarcoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipBarcoDN {
	private static final SlipBarcoDN INSTANCIA = new SlipBarcoDN();

	public static SlipBarcoDN getInstancia() {
		return SlipBarcoDN.INSTANCIA;
	}

  
	public void agregar(SlipBarcoDTO slipBarcoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipBarcoSN().agregar(slipBarcoDTO);
	}

	public void modificar(SlipBarcoDTO slipBarcoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipBarcoSN().modificar(slipBarcoDTO);
	}

	public SlipBarcoDTO getPorId(SlipBarcoDTO slipBarcoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipBarcoSN().getPorId(slipBarcoDTO.getIdToSlip());
	}
	
	public SlipBarcoDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipBarcoSN().getPorId(idToSlip);
	}

	public void borrar(SlipBarcoDTO slipBarcoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipBarcoSN slipSN = new SlipBarcoSN();
		slipSN.borrar(slipBarcoDTO);
	}

	public List<SlipBarcoDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipBarcoSN().buscarPorPropiedad(propiedad, valor);
	}
}
