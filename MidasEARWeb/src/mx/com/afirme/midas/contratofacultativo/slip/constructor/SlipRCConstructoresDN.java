package mx.com.afirme.midas.contratofacultativo.slip.constructor;


import java.util.List;

 
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTOId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipRCConstructoresDN {
	private static final SlipRCConstructoresDN INSTANCIA = new SlipRCConstructoresDN();

	public static SlipRCConstructoresDN getInstancia() {
		return SlipRCConstructoresDN.INSTANCIA;
	}

  
	public void agregar(SlipConstructoresDTO slipConstructoresDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipRCConstructoresSN().agregar(slipConstructoresDTO);
	}

	public void modificar(SlipConstructoresDTO slipConstructoresDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipRCConstructoresSN().modificar(slipConstructoresDTO);
	}

	public SlipConstructoresDTO getPorId(SlipConstructoresDTO SlipConstructoresDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipRCConstructoresSN().getPorId(SlipConstructoresDTO.getId());
	}
	
	public SlipConstructoresDTO getPorId(SlipConstructoresDTOId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipRCConstructoresSN().getPorId(id);
	}

	public void borrar(SlipConstructoresDTO slipConstructoresDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipRCConstructoresSN slipSN = new SlipRCConstructoresSN();
		slipSN.borrar(slipConstructoresDTO);
	}

	public List<SlipConstructoresDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipRCConstructoresSN().buscarPorPropiedad(propiedad, valor);
	}
}
