package mx.com.afirme.midas.contratofacultativo.slip.funcionario;

import java.util.List;

 
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTOId;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipRCFuncionariosDN {
	private static final SlipRCFuncionariosDN INSTANCIA = new SlipRCFuncionariosDN();

	public static SlipRCFuncionariosDN getInstancia() {
		return SlipRCFuncionariosDN.INSTANCIA;
	}

  
	public void agregar(SlipFuncionarioDTO SlipFuncionarioDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipRCFuncionariosSN().agregar(SlipFuncionarioDTO);
	}

	public void modificar(SlipFuncionarioDTO SlipFuncionarioDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipRCFuncionariosSN().modificar(SlipFuncionarioDTO);
	}

	/*public SlipFuncionarioDTO getPorId(SlipFuncionarioDTOId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipRCFuncionariosSN().getPorId(id);
	}*/
	
	public SlipFuncionarioDTO getPorId(SlipFuncionarioDTOId id) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipRCFuncionariosSN().getPorId(id);
	}

	public void borrar(SlipFuncionarioDTO SlipFuncionarioDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipRCFuncionariosSN slipSN = new SlipRCFuncionariosSN();
		slipSN.borrar(SlipFuncionarioDTO);
	}

	public List<SlipFuncionarioDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipRCFuncionariosSN().buscarPorPropiedad(propiedad, valor);
	}
}
