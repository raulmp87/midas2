package mx.com.afirme.midas.contratofacultativo.slip.obracivil;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioFacadeRemote;
import mx.com.afirme.midas.contratofacultativo.slip.SlipObraCivilDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipObraCivilFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipObraCivilSN {
	private SlipObraCivilFacadeRemote beanRemoto;

	public SlipObraCivilSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SlipObraCivilFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}

	public void  agregar(SlipObraCivilDTO slipObraCivilDTO) throws ExcepcionDeAccesoADatos {
		try {
		 	   beanRemoto.save(slipObraCivilDTO);
	 	} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SlipObraCivilDTO slipObraCivilDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(slipObraCivilDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(SlipObraCivilDTO slipObraCivilDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(slipObraCivilDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SlipObraCivilDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SlipObraCivilDTO getPorId(BigDecimal idToSlipAnexo) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToSlipAnexo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipObraCivilDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipObraCivilDTO> listarAnexosSlip(BigDecimal idToSlip) {
		try {
			return beanRemoto.findByProperty("slipDTO.idToSlip", idToSlip);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
