/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.general;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author hector
 *
 */
public class SlipGeneralForm extends MidasBaseForm {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7245742476231999067L;
	
	private String label;
    
	private String idToSlip;
	private String numeroInciso;
	private String numeroSubInciso;
	
	private String datosDelBien;
	
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getDatosDelBien() {
		return datosDelBien;
	}
	public void setDatosDelBien(String datosDelBien) {
		this.datosDelBien = datosDelBien;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
    
	
	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
 
	 
}
