package mx.com.afirme.midas.contratofacultativo.slip;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipSN {
	private SlipFacadeRemote beanRemoto;

	public SlipSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SlipFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipFacadeRemote instanciado", Level.FINEST, null);
	}

	public SlipDTO agregar(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(slipDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(slipDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public SlipDTO modificar(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(slipDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SlipDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipDTO> listarCotizacionesPorEstatus(String estatus) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarCotizacionesEstatus(estatus);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SlipDTO getPorId(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToSlip);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}


	public SlipDTO obtenerSlipLineaSoporte(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
		try{
		   return beanRemoto.obtenerSlipLineaSoporte(lineaSoporteReaseguroDTO);
		}catch(Exception e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
 	}
	
	public List<SlipDTO> listarSlipPorEstatus(BigDecimal idToCotizacion, String estatusCotizacion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarSlipPorEstatus(idToCotizacion, estatusCotizacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void modificarPorcentajesContratos(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, SlipDTO slipDTO, ContratoFacultativoDTO contratoFacultativoDTO) {
		beanRemoto.modificarPorcentajesContratos(lineaSoporteReaseguroDTO, slipDTO, contratoFacultativoDTO);
	}
	
	public int obtenerCantidadSlipsPorCotizacion(BigDecimal idToCotizacion){
		return beanRemoto.obtenerCantidadSlipsPorCotizacion(idToCotizacion);
	}
	
	public List<SlipDTO> listarFiltrado(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(slipDTO);
	}
	
	public SlipDTO establecerTipoSlip(SlipDTO slipDTO) {
		return beanRemoto.establecerTipoSlip(slipDTO);
	}
}
