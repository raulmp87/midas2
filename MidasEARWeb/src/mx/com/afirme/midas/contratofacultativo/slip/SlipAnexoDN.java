package mx.com.afirme.midas.contratofacultativo.slip;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo.SlipAnexoRCConstructoresSN;
import mx.com.afirme.midas.contratofacultativo.slip.equipocontratista.anexo.SlipAnexoEquipoContratistaSN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.anexo.SlipAnexoRCFuncionariosSN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAnexoDN {
	private static final SlipAnexoDN INSTANCIA = new SlipAnexoDN();

	public static SlipAnexoDN getInstancia() {
		return SlipAnexoDN.INSTANCIA;
	}

	public List<SlipAnexoDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoSN().listarTodos();
	}

	public SlipAnexoDTO agregar(SlipAnexoDTO slipDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoSN().agregar(slipDTO);
	}

	public void modificar(SlipAnexoDTO slipDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipAnexoSN().modificar(slipDTO);
	}

	public SlipAnexoDTO getPorId(SlipAnexoDTO slipAnexoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoSN().getPorId(slipAnexoDTO.getIdToSlipDocumentoAnexo());
	}
	
	public SlipAnexoDTO getPorId(BigDecimal idToSlipAnexo) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoSN().getPorId(idToSlipAnexo);
	}

	public void borrar(SlipAnexoDTO slipDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipAnexoSN slipSN = new SlipAnexoSN();
		slipSN.borrar(slipDTO);
	}

	public List<SlipAnexoDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoSN().buscarPorPropiedad(propiedad, valor);
	}
	
	
	/**
	 * 
	 * @param idToSlip
	 * @return
	 * @throws SystemException
	 */
	public List<SlipAnexoDTO> listarAnexosSlip(BigDecimal idToSlip) throws SystemException {
		return new SlipAnexoSN().listarAnexosSlip(idToSlip);
	}
	
	/**
	 * 
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public List<SlipAnexoEquipoContratistaDTO> listarAnexosSlipEquipoContratista(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		return new SlipAnexoEquipoContratistaSN().buscarPorPropiedad("slipDTO.idToSlip", idToSlip);
 	}

	/**
	 * 
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public List<SlipAnexoConstructoresDTO> listarAnexosSlipConstructores(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		return new SlipAnexoRCConstructoresSN().buscarPorPropiedad("slipConstructoresDTO.idToSlip", idToSlip);
	}
	
	/**
	 * 
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public List<SlipFuncionarioAnexoDTO> listarAnexosSlipFuncionarios(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		return new SlipAnexoRCFuncionariosSN().buscarPorPropiedad("slipFuncionarioDTO.id.idToSlip", idToSlip);
	}
}
