/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.funcionario;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class SlipRCFuncionariosForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 0L;

	private String idToSlip;
	private String numeroInciso;
	private String cotizacion;
	private String asegurado;
	private String direccion;
	private String vigencia;
	private String interes;
	private String periodoAdicionalNotificaciones;
	private String fechaRetroactiva;
	private String retencion;
	private String limiteTerritorial;
	private String accionLegal;
	private String leyesJurisdiccion;
	private String condicionesGenerales;
	private String consideracionesReaseguro;
	private String garantiaExpresa;
	private String subjetividades;
	private String terminosPagoPrimas;
	private String comision;
	private String impuestos;
	private String siniestralidadAnos;
	private String informacionAdicional;
	private String informacionComplementaria;
	/**
	 * @return the cotizacion
	 */
	public String getCotizacion() {
		return cotizacion;
	}
	/**
	 * @param cotizacion the cotizacion to set
	 */
	public void setCotizacion(String cotizacion) {
		this.cotizacion = cotizacion;
	}
	/**
	 * @return the asegurado
	 */
	public String getAsegurado() {
		return asegurado;
	}
	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the vigencia
	 */
	public String getVigencia() {
		return vigencia;
	}
	/**
	 * @param vigencia the vigencia to set
	 */
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	/**
	 * @return the interes
	 */
	public String getInteres() {
		return interes;
	}
	/**
	 * @param interes the interes to set
	 */
	public void setInteres(String interes) {
		this.interes = interes;
	}
	/**
	 * @return the periodoAdicionalNotificaciones
	 */
	public String getPeriodoAdicionalNotificaciones() {
		return periodoAdicionalNotificaciones;
	}
	/**
	 * @param periodoAdicionalNotificaciones the periodoAdicionalNotificaciones to set
	 */
	public void setPeriodoAdicionalNotificaciones(
			String periodoAdicionalNotificaciones) {
		this.periodoAdicionalNotificaciones = periodoAdicionalNotificaciones;
	}
	/**
	 * @return the fechaRetroactiva
	 */
	public String getFechaRetroactiva() {
		return fechaRetroactiva;
	}
	/**
	 * @param fechaRetroactiva the fechaRetroactiva to set
	 */
	public void setFechaRetroactiva(String fechaRetroactiva) {
		this.fechaRetroactiva = fechaRetroactiva;
	}
	/**
	 * @return the retencion
	 */
	public String getRetencion() {
		return retencion;
	}
	/**
	 * @param retencion the retencion to set
	 */
	public void setRetencion(String retencion) {
		this.retencion = retencion;
	}
	/**
	 * @return the limiteTerritorial
	 */
	public String getLimiteTerritorial() {
		return limiteTerritorial;
	}
	/**
	 * @param limiteTerritorial the limiteTerritorial to set
	 */
	public void setLimiteTerritorial(String limiteTerritorial) {
		this.limiteTerritorial = limiteTerritorial;
	}
	/**
	 * @return the accionLegal
	 */
	public String getAccionLegal() {
		return accionLegal;
	}
	/**
	 * @param accionLegal the accionLegal to set
	 */
	public void setAccionLegal(String accionLegal) {
		this.accionLegal = accionLegal;
	}
	/**
	 * @return the leyesJurisdiccion
	 */
	public String getLeyesJurisdiccion() {
		return leyesJurisdiccion;
	}
	/**
	 * @param leyesJurisdiccion the leyesJurisdiccion to set
	 */
	public void setLeyesJurisdiccion(String leyesJurisdiccion) {
		this.leyesJurisdiccion = leyesJurisdiccion;
	}
	/**
	 * @return the condicionesGenerales
	 */
	public String getCondicionesGenerales() {
		return condicionesGenerales;
	}
	/**
	 * @param condicionesGenerales the condicionesGenerales to set
	 */
	public void setCondicionesGenerales(String condicionesGenerales) {
		this.condicionesGenerales = condicionesGenerales;
	}
	/**
	 * @return the consideracionesReaseguro
	 */
	public String getConsideracionesReaseguro() {
		return consideracionesReaseguro;
	}
	/**
	 * @param consideracionesReaseguro the consideracionesReaseguro to set
	 */
	public void setConsideracionesReaseguro(String consideracionesReaseguro) {
		this.consideracionesReaseguro = consideracionesReaseguro;
	}
	/**
	 * @return the garantiaExpresa
	 */
	public String getGarantiaExpresa() {
		return garantiaExpresa;
	}
	/**
	 * @param garantiaExpresa the garantiaExpresa to set
	 */
	public void setGarantiaExpresa(String garantiaExpresa) {
		this.garantiaExpresa = garantiaExpresa;
	}
	/**
	 * @return the subjetividades
	 */
	public String getSubjetividades() {
		return subjetividades;
	}
	/**
	 * @param subjetividades the subjetividades to set
	 */
	public void setSubjetividades(String subjetividades) {
		this.subjetividades = subjetividades;
	}
	/**
	 * @return the terminosPagoPrimas
	 */
	public String getTerminosPagoPrimas() {
		return terminosPagoPrimas;
	}
	/**
	 * @param terminosPagoPrimas the terminosPagoPrimas to set
	 */
	public void setTerminosPagoPrimas(String terminosPagoPrimas) {
		this.terminosPagoPrimas = terminosPagoPrimas;
	}
	/**
	 * @return the comision
	 */
	public String getComision() {
		return comision;
	}
	/**
	 * @param comision the comision to set
	 */
	public void setComision(String comision) {
		this.comision = comision;
	}
	/**
	 * @return the impuestos
	 */
	public String getImpuestos() {
		return impuestos;
	}
	/**
	 * @param impuestos the impuestos to set
	 */
	public void setImpuestos(String impuestos) {
		this.impuestos = impuestos;
	}
	/**
	 * @return the siniestralidadAnos
	 */
	public String getSiniestralidadAnos() {
		return siniestralidadAnos;
	}
	/**
	 * @param siniestralidadAnos the siniestralidadAnos to set
	 */
	public void setSiniestralidadAnos(String siniestralidadAnos) {
		this.siniestralidadAnos = siniestralidadAnos;
	}
	/**
	 * @return the informacionAdicional
	 */
	public String getInformacionAdicional() {
		return informacionAdicional;
	}
	/**
	 * @param informacionAdicional the informacionAdicional to set
	 */
	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}
	/**
	 * @return the informacionComplementaria
	 */
	public String getInformacionComplementaria() {
		return informacionComplementaria;
	}
	/**
	 * @param informacionComplementaria the informacionComplementaria to set
	 */
	public void setInformacionComplementaria(String informacionComplementaria) {
		this.informacionComplementaria = informacionComplementaria;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
}
