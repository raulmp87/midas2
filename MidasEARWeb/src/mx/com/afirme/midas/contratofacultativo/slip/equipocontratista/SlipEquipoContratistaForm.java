/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.equipocontratista;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class SlipEquipoContratistaForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2049240089593453191L;
	private String idToSlip;
	private String cotizacion;
	private String numeroInciso;
	private String numeroSubinciso;
	private String direccionInciso;
	private String subincisoMayorValor;
	/**
	 * @return the cotizacion
	 */
	public String getCotizacion() {
		return cotizacion;
	}
	/**
	 * @param cotizacion the cotizacion to set
	 */
	public void setCotizacion(String cotizacion) {
		this.cotizacion = cotizacion;
	}
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the numeroSubinciso
	 */
	public String getNumeroSubinciso() {
		return numeroSubinciso;
	}
	/**
	 * @param numeroSubinciso the numeroSubinciso to set
	 */
	public void setNumeroSubinciso(String numeroSubinciso) {
		this.numeroSubinciso = numeroSubinciso;
	}
	/**
	 * @return the direccionInciso
	 */
	public String getDireccionInciso() {
		return direccionInciso;
	}
	/**
	 * @param direccionInciso the direccionInciso to set
	 */
	public void setDireccionInciso(String direccionInciso) {
		this.direccionInciso = direccionInciso;
	}
	/**
	 * @return the subincisoMayorValor
	 */
	public String getSubincisoMayorValor() {
		return subincisoMayorValor;
	}
	/**
	 * @param subincisoMayorValor the subincisoMayorValor to set
	 */
	public void setSubincisoMayorValor(String subincisoMayorValor) {
		this.subincisoMayorValor = subincisoMayorValor;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToSlip() {
		return idToSlip;
	}

	
	
}
