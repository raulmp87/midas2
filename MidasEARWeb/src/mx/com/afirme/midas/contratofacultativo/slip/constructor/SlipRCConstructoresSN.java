package mx.com.afirme.midas.contratofacultativo.slip.constructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTOId;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipRCConstructoresSN {
	private SlipConstructoresFacadeRemote beanRemoto;

	public SlipRCConstructoresSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SlipConstructoresFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}

	public void  agregar(SlipConstructoresDTO slipConstructoresDTO) throws ExcepcionDeAccesoADatos {
		try {
		 	   beanRemoto.save(slipConstructoresDTO);
	 	} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SlipConstructoresDTO slipConstructoresDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(slipConstructoresDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(SlipConstructoresDTO slipConstructoresDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(slipConstructoresDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	@SuppressWarnings("unchecked")
	public List<SlipConstructoresDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SlipConstructoresDTO getPorId(SlipConstructoresDTOId id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SlipConstructoresDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SlipConstructoresDTO> listarAnexosSlip(BigDecimal idToSlip) {
		try {
			return beanRemoto.findByProperty("slipDTO.idToSlip", idToSlip);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
