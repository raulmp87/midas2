package mx.com.afirme.midas.contratofacultativo.slip;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ListarSlipDN {
	private static final ListarSlipDN INSTANCIA = new ListarSlipDN();

	public static ListarSlipDN getInstancia() {
		return ListarSlipDN.INSTANCIA;
	}
	
	public List<SlipDTO> listarSolicitudesPendientes() throws SystemException, ExcepcionDeAccesoADatos {
		return new ListarSlipSN().listarSolicitudesPendientes();
	}

}