package mx.com.afirme.midas.contratofacultativo.slip.funcionario.anexo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAnexoRCFuncionariosDN {
	private static final SlipAnexoRCFuncionariosDN INSTANCIA = new SlipAnexoRCFuncionariosDN();

	public static SlipAnexoRCFuncionariosDN getInstancia() {
		return SlipAnexoRCFuncionariosDN.INSTANCIA;
	}

  
	public void agregar(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipAnexoRCFuncionariosSN().agregar(SlipFuncionarioAnexoDTO);
	}

	public void modificar(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipAnexoRCFuncionariosSN().modificar(SlipFuncionarioAnexoDTO);
	}

	public SlipFuncionarioAnexoDTO getPorId(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoRCFuncionariosSN().getPorId(SlipFuncionarioAnexoDTO.getIdSlipDocumentoAnexo());
	}
	
	public SlipFuncionarioAnexoDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoRCFuncionariosSN().getPorId(idToSlip);
	}

	public void borrar(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipAnexoRCFuncionariosSN slipSN = new SlipAnexoRCFuncionariosSN();
		slipSN.borrar(SlipFuncionarioAnexoDTO);
	}

	public List<SlipFuncionarioAnexoDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoRCFuncionariosSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<SlipFuncionarioAnexoDTO> obtenerAnexosPorSlipInciso(BigDecimal idToSlip,BigDecimal numeroInciso) throws SystemException {
			return new SlipAnexoRCFuncionariosSN().obtenerAnexosPorSlipInciso(idToSlip,numeroInciso);
	}
}
