package mx.com.afirme.midas.contratofacultativo.slip;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.aviacion.SlipAviacionDN;
import mx.com.afirme.midas.contratofacultativo.slip.barco.SlipBarcoDN;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.SlipRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.SlipRCFuncionariosDN;
import mx.com.afirme.midas.contratofacultativo.slip.incendio.SlipIncendioDN;
import mx.com.afirme.midas.contratofacultativo.slip.obracivil.SlipObraCivilDN;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionSN;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionSN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAviacionSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipBarcosSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipEquipoContratistaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipGeneralSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipIncendioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipObraCivilSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipPrimerRiesgoSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCConstructoresSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCFuncionarioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipTransportesSoporteDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipDN {
	private static final SlipDN INSTANCIA = new SlipDN();

	public static SlipDN getInstancia() {
		return SlipDN.INSTANCIA;
	}

	public List<SlipDTO> listarTodos() throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipSN().listarTodos();
	}

	public SlipDTO agregar(SlipDTO slipDTO) throws SystemException, ExcepcionDeAccesoADatos {
		/*
		 * 
		 * 101(INCENDIO) 			:  SlipIncendioSoporteDTO.TIPO
		 * 201(TRANSPORTES DE CARGA):  SlipTransportesSoporteDTO.TIPO
		 * 202(CASCO DE AVION)		:  SlipAviacionSoporteDTO.TIPO
		 * 203(CASCO DE BARCO) 		:  SlipBarcosSoporteDTO.TIPO
		 * 310(EQUIPO DE CONTRATISTA Y MAQUINARIA PESADA):  SlipEquipoContratistaSoporteDTO.TIPO
		 * 312(OBRA CIVIL EN CONSTRUCCION): SlipObraCivilSoporteDTO.TIPO
		 * 502(RESPONSABILIDAD CIVIL AVIONES) : SlipAviacionSoporteDTO.TIPO
		 * 503(RESPONSABILIDAD CIVIL BARCOS)  : SlipBarcosSoporteDTO.TIPO
		 * 506(RESPONSABILIDAD CIVIL CONSEJEROS Y FUNCIONARIOS): SlipRCFuncionarioSoporteDTO.TIPO
		 * 501 (RESPONSABILIDAD CIVIL GENERAL): SlipRCConstructoresDTO.TIPO
		 * ELSE: SlipGeneralSoporteDTO.TIPO
		 * 
		 *  
		 * 
		 */
		SubRamoDTO subRamoDTO ;
		subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(slipDTO.getIdTcSubRamo());
		
		int tipo = Integer.valueOf(slipDTO.getTipoSlip()).intValue();
		
		if(tipo != SlipPrimerRiesgoSoporteDTO.TIPO){
		 	if (subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.INCENDIO){
				slipDTO.setTipoSlip(Integer.valueOf(SlipIncendioSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.TRANSPORTES_CARGA){
				slipDTO.setTipoSlip(Integer.valueOf(SlipTransportesSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.CASCO_AVION){
				slipDTO.setTipoSlip(Integer.valueOf(SlipAviacionSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.CASCO_BARCO){
				slipDTO.setTipoSlip(Integer.valueOf(SlipBarcosSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.EQUIPO_CONTRATISTA_MAQUINARIA_PESADA){
				slipDTO.setTipoSlip(Integer.valueOf(SlipEquipoContratistaSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.OBRA_CIVIL_CONSTRUCCION){
				slipDTO.setTipoSlip(Integer.valueOf(SlipObraCivilSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_AVIONES){
				slipDTO.setTipoSlip(Integer.valueOf(SlipAviacionSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_BARCOS){
				slipDTO.setTipoSlip(Integer.valueOf(SlipBarcosSoporteDTO.TIPO).toString());
			}else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_CONSEJEROS_FUNCIONARIOS){
				slipDTO.setTipoSlip(Integer.valueOf(SlipRCFuncionarioSoporteDTO.TIPO).toString());
			} else if(subRamoDTO.getCodigoSubRamo().intValue() == SlipDTO.RESPONSABILIDAD_CIVIL_GENERAL){
				slipDTO.setTipoSlip(Integer.valueOf(SlipRCConstructoresSoporteDTO.TIPO).toString());
			}else{
				slipDTO.setTipoSlip(Integer.valueOf(SlipGeneralSoporteDTO.TIPO).toString());
			}
		}
		
//		establecerTipoSlip(slipDTO);		
 		return new SlipSN().agregar(slipDTO);
	}

	public SlipDTO modificar(SlipDTO slipDTO) throws SystemException,ExcepcionDeAccesoADatos {
		return new SlipSN().modificar(slipDTO);
	}

	public SlipDTO getPorId(SlipDTO slipDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipSN().getPorId(slipDTO.getIdToSlip());
	}
	
	public SlipDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipSN().getPorId(idToSlip);
	}

	public void borrar(SlipDTO slipDTO) throws SystemException,ExcepcionDeAccesoADatos {
		SlipSN slipSN = new SlipSN();
		slipSN.borrar(slipDTO);
	}

	public List<SlipDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipSN().buscarPorPropiedad(propiedad, valor);
	}
	
	public List<SlipDTO> listarCotizacionEstatus(String estatus)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipSN().listarCotizacionesPorEstatus(estatus);
	}
	
	public List<ReaseguroCotizacionDTO> listarReaseguroCotizacionSlip(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ReaseguroCotizacionDTO entity = new ReaseguroCotizacionDTO();
		ReaseguroCotizacionId key = new ReaseguroCotizacionId();
		key.setIdTcSubRamo(slipDTO.getIdTcSubRamo());
		key.setIdToCotizacion(slipDTO.getIdToCotizacion());
		entity.setId(key);
		return new ReaseguroCotizacionSN().listarFiltrado(entity);
	}
	
	public List<ReaseguroIncisoCotizacionDTO> listarReaseguroIncisoCotizacionSlip(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ReaseguroIncisoCotizacionDTO entity = new ReaseguroIncisoCotizacionDTO();
		ReaseguroIncisoCotizacionId key = new ReaseguroIncisoCotizacionId();
		key.setIdTcSubRamo(slipDTO.getIdTcSubRamo());
		key.setIdToCotizacion(slipDTO.getIdToCotizacion());
		entity.setId(key);
		return new ReaseguroIncisoCotizacionSN().listarFiltrado(entity);
	}
	public SlipDTO obtenerSlipLineaSoporte(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new SlipSN().obtenerSlipLineaSoporte(lineaSoporteReaseguroDTO);
	}

	public List<SlipDTO> listarSlipPorEstatus(BigDecimal idToCotizacion, String estatusCotizacion)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipSN().listarSlipPorEstatus(idToCotizacion, estatusCotizacion);
	}
	
	/**
	 *  
	 * @param idToSlip
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public SlipAviacionDTO obtenerSlipAviacion(BigDecimal idToSlip)throws ExcepcionDeAccesoADatos, SystemException{
		return SlipAviacionDN.getInstancia().getPorId(idToSlip);
	}
	
	/**
	 *  
	 * @param idToSlip
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public SlipBarcoDTO obtenerSlipBarco(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipBarcoDN.getInstancia().getPorId(idToSlip);
	}
	
	/**
	 *  
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public SlipIncendioDTO obtenerSlipIncendio(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipIncendioDN.getInstancia().getPorId(idToSlip);
	}
	
	/**
	 *  
	 * @param idToSlip
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public SlipObraCivilDTO obtenerSlipObraCivil(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipObraCivilDN.getInstancia().getPorId(idToSlip);
	}	
	
	/**
	 *  
	 * @param idToSlip
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public SlipConstructoresDTO obtenerSlipConstructores(SlipConstructoresDTOId id) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipRCConstructoresDN.getInstancia().getPorId(id);
	}		

	/**
	 *  
	 * @param idToSlip
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public List<SlipConstructoresDTO> obtenerListaSlipConstructores(String propiedad,BigDecimal valor) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipRCConstructoresDN.getInstancia().buscarPorPropiedad(propiedad, valor);
	}	
	
	/**
	 *  
	 * @param idToSlip
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public SlipFuncionarioDTO obtenerSlipFuncionario(SlipFuncionarioDTOId id) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipRCFuncionariosDN.getInstancia().getPorId(id);
	}		

	/**
	 *  
	 * @param idToSlip
	 * @return
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public List<SlipFuncionarioDTO> obtenerListaSlipFuncionario(BigDecimal valor) throws ExcepcionDeAccesoADatos, SystemException{
		return SlipRCFuncionariosDN.getInstancia().buscarPorPropiedad("id.idToSlip", valor);
	}		

	public void modificarPorcentajesContratos(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, SlipDTO slipDTO, ContratoFacultativoDTO contratoFacultativoDTO) throws SystemException {
		SlipSN slipSN = new SlipSN();
		slipSN.modificarPorcentajesContratos(lineaSoporteReaseguroDTO, slipDTO, contratoFacultativoDTO);
	}
	
	public int obtenerCantidadSlipsPorCotizacion(BigDecimal idToCotizacion) throws SystemException{
		return new SlipSN().obtenerCantidadSlipsPorCotizacion(idToCotizacion);
	}
	
	public SlipDTO llenarSlip(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SlipDTO slipDTO = new SlipDTO();
		BigDecimal numeroInciso = null;
		BigDecimal numeroSubInciso = null;
		BigDecimal idToSeccion = null;
		LineaDTO lineaDTO = lineaSoporteReaseguroDTO.getLineaDTO();
		int tipoDistribucion = lineaDTO.getTipoDistribucion().intValue();
		slipDTO.setIdToCotizacion(lineaSoporteReaseguroDTO.getIdToCotizacion());	
	    slipDTO.setIdTcSubRamo(lineaDTO.getSubRamo().getIdTcSubRamo());  
	    slipDTO.setTipoDistribucion(lineaDTO.getTipoDistribucion().toString());
	    if(tipoDistribucion == 2){
	    	numeroInciso = BigDecimal.valueOf(LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteReaseguroDTO));
	    	slipDTO.setNumeroInciso(numeroInciso);
	    }else if (tipoDistribucion == 3){
	    	numeroInciso = BigDecimal.valueOf(LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteReaseguroDTO));
	    	numeroSubInciso = BigDecimal.valueOf(LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporteReaseguroDTO));
	    	idToSeccion =  LineaSoporteReaseguroDN.getInstancia().obtenerIdToSeccion(lineaSoporteReaseguroDTO);
	    	slipDTO.setNumeroInciso(numeroInciso);
	    	slipDTO.setNumeroSubInciso(numeroSubInciso);
	    	slipDTO.setIdToSeccion(idToSeccion);
	    }
	    slipDTO.setInformacionAdicional("SLIP" + (lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getDescripcionSubRamo()!= null? 
	    		" para Subramo: "+lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getDescripcionSubRamo() : ""));
	    if(lineaSoporteReaseguroDTO.getEsPrimerRiesgo()){
	    	slipDTO.setTipoSlip(""+SlipPrimerRiesgoSoporteDTO.TIPO);
	    }else{
	    	slipDTO.setTipoSlip(""+SlipGeneralSoporteDTO.TIPO);
	    }
	    slipDTO.setEstatus(BigDecimal.ONE);
	    slipDTO.setEstatusCotizacion(BigDecimal.ZERO);
	    slipDTO.setIdTmLineaSoporteReaseguro(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
	    slipDTO.setIdToSoporteReaseguro(lineaSoporteReaseguroDTO.getId().getIdToSoporteReaseguro());
	    return slipDTO;
   }
	
	public List<SlipDTO> listarFiltrado(SlipDTO slipDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new SlipSN().listarFiltrado(slipDTO);
	}
	
	public SlipDTO establecerTipoSlip(SlipDTO slipDTO) throws SystemException {
		return new SlipSN().establecerTipoSlip(slipDTO);
	}
	
	public SlipDTO buscarPorIdTmLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro)throws SystemException, ExcepcionDeAccesoADatos {
		List<SlipDTO> listaSlips = new SlipSN().buscarPorPropiedad("idTmLineaSoporteReaseguro", idTmLineaSoporteReaseguro);
		SlipDTO slipTMP = null;
		if(listaSlips != null && !listaSlips.isEmpty())
			slipTMP = listaSlips.get(0);
		return slipTMP;
	}
	
}
