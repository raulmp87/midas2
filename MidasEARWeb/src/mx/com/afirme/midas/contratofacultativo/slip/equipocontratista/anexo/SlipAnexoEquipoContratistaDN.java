package mx.com.afirme.midas.contratofacultativo.slip.equipocontratista.anexo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoEquipoContratistaDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipBarcoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAnexoEquipoContratistaDN {
	private static final SlipAnexoEquipoContratistaDN INSTANCIA = new SlipAnexoEquipoContratistaDN();

	 
	
	public static SlipAnexoEquipoContratistaDN getInstancia() {
		return SlipAnexoEquipoContratistaDN.INSTANCIA;
	}

  
	public void agregar(SlipAnexoEquipoContratistaDTO slipBarcoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		  new SlipAnexoEquipoContratistaSN().agregar(slipBarcoDTO);
	}

	public void modificar(SlipAnexoEquipoContratistaDTO slipBarcoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		new SlipAnexoEquipoContratistaSN().modificar(slipBarcoDTO);
	}

	public SlipAnexoEquipoContratistaDTO getPorId(SlipAnexoEquipoContratistaDTO SlipAnexoEquipoContratistaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoEquipoContratistaSN().getPorId(SlipAnexoEquipoContratistaDTO.getIdToSlipDocumentoAnexo());
	}
	
	public SlipAnexoEquipoContratistaDTO getPorId(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoEquipoContratistaSN().getPorId(idToSlip);
	}

	public void borrar(SlipAnexoEquipoContratistaDTO slipBarcoDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		SlipAnexoEquipoContratistaSN slipSN = new SlipAnexoEquipoContratistaSN();
		slipSN.borrar(slipBarcoDTO);
	}

	public List<SlipAnexoEquipoContratistaDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new SlipAnexoEquipoContratistaSN().buscarPorPropiedad(propiedad, valor);
	}
}
