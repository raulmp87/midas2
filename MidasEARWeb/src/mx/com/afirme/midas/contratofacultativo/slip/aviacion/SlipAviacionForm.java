/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.aviacion;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasBaseForm;


/**
 * @author jmartinez
 *
 */
public class SlipAviacionForm extends MidasBaseForm {



	/**
	 * 
	 */
	private static final long serialVersionUID = -1987483540271814685L;
 
	private String idToSlip;
	private String numeroInciso;
	private String matricula;
	private String serie;
	private String uso;
	private String tipoAeronave;
	private String capacidad;
	private String marca;
	private String limitesGeograficos;
	private String modelo;
	private String aeropuertoBase;
	private String ano;
	private String tiposdeAeropuertos;
	private String pagosVoluntarios;
	private String gastosMedicosAParaPasajeros;
	private String gastosMedicosBParaTripulantesyPilotos;
	private String horasTotales;
	private String horasMarcayTipo;
	
	private String subLimites;

	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}
	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}
	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}
	/**
	 * @return the uso
	 */
	public String getUso() {
		return uso;
	}
	/**
	 * @param uso the uso to set
	 */
	public void setUso(String uso) {
		this.uso = uso;
	}
	/**
	 * @return the tipoAeronave
	 */
	public String getTipoAeronave() {
		return tipoAeronave;
	}
	/**
	 * @param tipoAeronave the tipoAeronave to set
	 */
	public void setTipoAeronave(String tipoAeronave) {
		this.tipoAeronave = tipoAeronave;
	}
	/**
	 * @return the capacidad
	 */
	public String getCapacidad() {
		return capacidad;
	}
	/**
	 * @param capacidad the capacidad to set
	 */
	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the limitesGeograficos
	 */
	public String getLimitesGeograficos() {
		return limitesGeograficos;
	}
	/**
	 * @param limitesGeograficos the limitesGeograficos to set
	 */
	public void setLimitesGeograficos(String limitesGeograficos) {
		this.limitesGeograficos = limitesGeograficos;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the aeropuertoBase
	 */
	public String getAeropuertoBase() {
		return aeropuertoBase;
	}
	/**
	 * @param aeropuertoBase the aeropuertoBase to set
	 */
	public void setAeropuertoBase(String aeropuertoBase) {
		this.aeropuertoBase = aeropuertoBase;
	}
	/**
	 * @return the ano
	 */
	public String getAno() {
		return ano;
	}
	/**
	 * @param ano the ano to set
	 */
	public void setAno(String ano) {
		this.ano = ano;
	}
	/**
	 * @return the tiposdeAeropuertos
	 */
	public String getTiposdeAeropuertos() {
		return tiposdeAeropuertos;
	}
	/**
	 * @param tiposdeAeropuertos the tiposdeAeropuertos to set
	 */
	public void setTiposdeAeropuertos(String tiposdeAeropuertos) {
		this.tiposdeAeropuertos = tiposdeAeropuertos;
	}
	/**
	 * @return the pagosVoluntarios
	 */
	public String getPagosVoluntarios() {
		return pagosVoluntarios;
	}
	/**
	 * @param pagosVoluntarios the pagosVoluntarios to set
	 */
	public void setPagosVoluntarios(String pagosVoluntarios) {
		this.pagosVoluntarios = pagosVoluntarios;
	}
	/**
	 * @return the gastosMedicosAParaPasajeros
	 */
	public String getGastosMedicosAParaPasajeros() {
		return gastosMedicosAParaPasajeros;
	}
	/**
	 * @param gastosMedicosAParaPasajeros the gastosMedicosAParaPasajeros to set
	 */
	public void setGastosMedicosAParaPasajeros(String gastosMedicosAParaPasajeros) {
		this.gastosMedicosAParaPasajeros = gastosMedicosAParaPasajeros;
	}
	/**
	 * @return the gastosMedicosBParaTripulantesyPilotos
	 */
	public String getGastosMedicosBParaTripulantesyPilotos() {
		return gastosMedicosBParaTripulantesyPilotos;
	}
	/**
	 * @param gastosMedicosBParaTripulantesyPilotos the gastosMedicosBParaTripulantesyPilotos to set
	 */
	public void setGastosMedicosBParaTripulantesyPilotos(
			String gastosMedicosBParaTripulantesyPilotos) {
		this.gastosMedicosBParaTripulantesyPilotos = gastosMedicosBParaTripulantesyPilotos;
	}
	/**
	 * @return the horasTotales
	 */
	public String getHorasTotales() {
		return horasTotales;
	}
	/**
	 * @param horasTotales the horasTotales to set
	 */
	public void setHorasTotales(String horasTotales) {
		this.horasTotales = horasTotales;
	}
	/**
	 * @return the horasMarcayTipo
	 */
	public String getHorasMarcayTipo() {
		return horasMarcayTipo;
	}
	/**
	 * @param horasMarcayTipo the horasMarcayTipo to set
	 */
	public void setHorasMarcayTipo(String horasMarcayTipo) {
		this.horasMarcayTipo = horasMarcayTipo;
	}
	
	
	public String getIdToSlip() {
		return idToSlip;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}

	public String getSubLimites() {
		return subLimites;
	}
	public void setSubLimites(String subLimites) {
		this.subLimites = subLimites;
	}
	
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
	 	request.setAttribute("coberturas", 1);
		request.setAttribute("informacionPiloto", 1);
	  	return errors;
	}
	

	
	
}
