package mx.com.afirme.midas.contratofacultativo.slip.funcionario.anexo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipAnexoRCFuncionariosSN {
	private SlipFuncionarioAnexoFacadeRemote beanRemoto;

	public SlipAnexoRCFuncionariosSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SlipFuncionarioAnexoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}

	public void  agregar(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws ExcepcionDeAccesoADatos {
		try {
		 	   beanRemoto.save(SlipFuncionarioAnexoDTO);
	 	} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(SlipFuncionarioAnexoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(SlipFuncionarioAnexoDTO SlipFuncionarioAnexoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(SlipFuncionarioAnexoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SlipFuncionarioAnexoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public SlipFuncionarioAnexoDTO getPorId(BigDecimal idToSlipAnexo) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToSlipAnexo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipFuncionarioAnexoDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SlipFuncionarioAnexoDTO> listarAnexosSlip(BigDecimal idToSlip) {
		try {
			return beanRemoto.findByProperty("slipDTO.idToSlip", idToSlip);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SlipFuncionarioAnexoDTO> obtenerAnexosPorSlipInciso(BigDecimal idToSlip,BigDecimal numeroInciso) {
		try {
			return beanRemoto.obtenerAnexosPorSlipInciso(idToSlip,numeroInciso);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
