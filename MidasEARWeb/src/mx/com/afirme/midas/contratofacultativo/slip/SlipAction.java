package mx.com.afirme.midas.contratofacultativo.slip;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.tipomaquinaria.TipoMaquinariaDN;
import mx.com.afirme.midas.catalogos.tipomaquinaria.TipoMaquinariaDTO;
import mx.com.afirme.midas.cliente.ClienteAction;
import mx.com.afirme.midas.cliente.ClienteForm;
import mx.com.afirme.midas.contratofacultativo.slip.aviacion.SlipAviacionDN;
import mx.com.afirme.midas.contratofacultativo.slip.aviacion.SlipAviacionForm;
import mx.com.afirme.midas.contratofacultativo.slip.barco.SlipBarcoDN;
import mx.com.afirme.midas.contratofacultativo.slip.barco.SlipBarcosForm;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.SlipRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.SlipRCConstructoresForm;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo.SlipAnexoRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.equipocontratista.SlipEquipoContratistaForm;
import mx.com.afirme.midas.contratofacultativo.slip.equipocontratista.anexo.SlipAnexoEquipoContratistaDN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.SlipRCFuncionariosDN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.SlipRCFuncionariosForm;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.anexo.SlipAnexoRCFuncionariosDN;
import mx.com.afirme.midas.contratofacultativo.slip.general.SlipGeneralForm;
import mx.com.afirme.midas.contratofacultativo.slip.incendio.SlipIncedioForm;
import mx.com.afirme.midas.contratofacultativo.slip.incendio.SlipIncendioDN;
import mx.com.afirme.midas.contratofacultativo.slip.obracivil.SlipObraCivilDN;
import mx.com.afirme.midas.contratofacultativo.slip.obracivil.SlipObraCivilForm;
import mx.com.afirme.midas.contratofacultativo.slip.transportecarga.SlipTransportesDeCargaForm;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroCotizacionForm;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionForm;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionForm;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionId;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.soporte.SlipSoporteDN;
import mx.com.afirme.midas.reaseguro.soporte.slip.CoberturaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.DetalleSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAnexoSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipBarcosSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCConstructoresSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCFuncionarioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SumaAseguradaSoporteDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * class SlipAction
 * @author hector Sarmiento
 * @since 09 de Diciembre de 2009
 */
public class SlipAction extends MidasMappingDispatchAction {
 	public ActionForward mostrarEditarSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipForm slipForm = (SlipForm) form;
		
		try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			String slipGeneral = request.getParameter("slipGeneral");
			request.getSession().setAttribute("slipGeneral", slipGeneral);
			if (idToSlip != null){
				SlipDTO slipDTO = SlipDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idToSlip));
	  			
				SlipSoporteDTO slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				SoporteDanosDN.getInstancia().getDatosSlip(Integer.valueOf(slipDTO.getTipoDistribucion()),slipDTO.getIdToCotizacion(),
						slipDTO.getNumeroInciso(),slipDTO.getIdToSeccion(), slipDTO.getNumeroSubInciso(),slipDTO.getIdTcSubRamo(),slipSoporteDTO,UtileriasWeb.obtieneNombreUsuario(request));
				/*
				 * Procesa la lista de SlipRCConstructores unicamente cuando el slip es de este tipo.
				 */
//				SlipSoporteDN.getInstancia().procesaRCConstructores(slipSoporteDTO);
				
				SlipSoporteDN.getInstancia().crearListaDetalle(slipSoporteDTO);
  			
		 		// descomentar una vez que soporte tenga los datos slipSoporteDTO = SoporteDanosDN.getInstancia().llenaDatosCotizacion(slipSoporteDTO);

				poblarForm(slipForm,slipDTO, request,slipSoporteDTO);// llena la forma general de los slips
	 		  	List<SlipAnexoSoporteDTO> anexos =  slipSoporteDTO.getAnexos();
	 		  	request.getSession().setAttribute("slipSoporte", slipSoporteDTO);
	 		  	
				request.setAttribute("anexos", anexos);

  		 	}
  		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarEditarSlipBarco(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipBarcosForm slipBarcosForm = (SlipBarcosForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			
			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			
			if (idToSlip != null && slipSoporteDTO != null){
				//slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				slipBarcosForm.setIdToSlip(idToSlip);
				/*
				 * Sumar las coberturas
				 */
				BigDecimal sumaCoberturas = sumarCoberturas(slipSoporteDTO.getSlipBarcos().getListaCoberturas());
				slipSoporteDTO.getSlipBarcos().setSumaAsegurada(sumaCoberturas);
				
				/*
				 * Obtener cobertura del casco
				 */
				slipSoporteDTO.getSlipBarcos().setSumaAseguradaCasco(obtenerSumaAsegurada(slipSoporteDTO.getSlipBarcos().getListaCoberturas(), SlipBarcosSoporteDTO.CASCO_BARCO));
				
				/*
				 * Obtener cobertura de valor incrementado
				 */
				slipSoporteDTO.getSlipBarcos().setSumaAseguradaVI(obtenerSumaAsegurada(slipSoporteDTO.getSlipBarcos().getListaCoberturas(), SlipBarcosSoporteDTO.VALOR_INCREMENTADO));

				/*
				 * Obtener cobertura de responsabilidad civil
				 */
				slipSoporteDTO.getSlipBarcos().setSumaAseguradaRC(obtenerSumaAsegurada(slipSoporteDTO.getSlipBarcos().getListaCoberturas(), SlipBarcosSoporteDTO.RESPONSABILIDAD_CIVIL));
				
				poblarSlipBarco(slipBarcosForm,slipSoporteDTO);
				
				if(slipSoporteDTO.getSlipBarcos() != null){
					request.setAttribute("coberturas", slipSoporteDTO.getSlipBarcos().getListaCoberturas());
				}
 			}
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward mostrarEditarSlipAviacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipAviacionForm slipAviacionForm = (SlipAviacionForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			
			if (idToSlip != null && slipSoporteDTO != null){
				//slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				slipAviacionForm.setIdToSlip(idToSlip);
				poblarSlipAviacion(slipAviacionForm,slipSoporteDTO);
				if(slipSoporteDTO.getSlipAviacion() != null){
					request.setAttribute("coberturas", slipSoporteDTO.getSlipAviacion().getListaCoberturas());				
				}
 			}
			request.setAttribute("informacionPiloto", 1);
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} 
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarEditarSlipGeneral(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipGeneralForm slipGeneralForm = (SlipGeneralForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);

			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			
			if (idToSlip != null && slipSoporteDTO != null){
				//slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				slipGeneralForm.setIdToSlip(idToSlip);
				poblarSlipGeneral(slipGeneralForm,slipSoporteDTO);
				
				if(slipSoporteDTO.getSlipGeneralSoporte() != null){
					request.setAttribute("coberturas", slipSoporteDTO.getSlipGeneralSoporte().getListaCoberturas());
				}
 			}
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	                     
	public ActionForward mostrarEditarEquipoContratista(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipEquipoContratistaForm slipEquipoContratistaForm = (SlipEquipoContratistaForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			String slipGeneral = request.getParameter("slipGeneral");
			request.getSession().setAttribute("slipGeneral", slipGeneral);

			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			if (idToSlip != null && slipSoporteDTO != null){
				slipEquipoContratistaForm.setIdToSlip(idToSlip);
				poblarSlipEquipoContratista(slipEquipoContratistaForm,slipSoporteDTO);
				
				if(slipSoporteDTO.getSlipEquipoConstratista() != null){
					request.setAttribute("coberturas", slipSoporteDTO.getSlipEquipoConstratista().getListaCoberturas());
					request.setAttribute("coberturasInciso", slipSoporteDTO.getSlipEquipoConstratista().getListaCoberturasInciso());
					slipSoporteDTO.getSlipEquipoConstratista().getBienesAsegurados();
					/*
					 * Sumar coberturas del subInciso
					 */
					List<SumaAseguradaSoporteDTO> bienesAsegurados = new ArrayList<SumaAseguradaSoporteDTO>();
					BigDecimal sumaCoberturas = sumarCoberturas(slipSoporteDTO.getSlipEquipoConstratista().getListaCoberturas());
					
					SumaAseguradaSoporteDTO sumaAseguradaSoporteDTO = new SumaAseguradaSoporteDTO();
					sumaAseguradaSoporteDTO.setDescripcion(slipSoporteDTO.getSlipEquipoConstratista().getDescripcionSubInciso());
					sumaAseguradaSoporteDTO.setSumaAsegurada(sumaCoberturas);
					bienesAsegurados.add(sumaAseguradaSoporteDTO);
					request.setAttribute("sumaAseguradaBien", bienesAsegurados);
				}
				request.setAttribute("anexosContratista",slipSoporteDTO.getSlipEquipoConstratista().getAnexos());
			}
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarEditarIncendio(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipIncedioForm slipIncedioForm = (SlipIncedioForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			
			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");
			
			if (idToSlip != null && slipSoporteDTO != null){
				//SlipSoporteDTO slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				slipIncedioForm.setIdToSlip(idToSlip);
				poblarSlipIncendio(slipIncedioForm,slipSoporteDTO);
					if(slipSoporteDTO.getSlipIncendioSoporte() != null){
						request.setAttribute("coberturas", slipSoporteDTO.getSlipIncendioSoporte().getListaCoberturas());
					}
	 			}
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public ActionForward mostrarEditarObraCivil(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipObraCivilForm slipObraCivilForm = (SlipObraCivilForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			
			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			
			if (idToSlip != null && slipSoporteDTO != null){
				
				slipObraCivilForm.setIdToSlip(idToSlip);
				this.poblarSlipObraCivil(slipObraCivilForm,slipSoporteDTO);
				if(slipSoporteDTO.getSlipObraCivilSoporte() != null){
					request.setAttribute("coberturas", slipSoporteDTO.getSlipObraCivilSoporte().getListaCoberturas());				
				}
			}
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * 
	 * @param listaCoberturas
	 * @return
	 */
	private BigDecimal sumarCoberturas(List<CoberturaSoporteDTO> listaCoberturas){
		BigDecimal suma = BigDecimal.ZERO;
		if(listaCoberturas != null){
			for(CoberturaSoporteDTO co : listaCoberturas){
				suma = suma.add(co.getSumaAsegurada());
			}
		}
		return suma;
	}
	
	public ActionForward mostrarEditarRCFuncionarios(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipRCFuncionariosForm slipRCFuncionariosForm = (SlipRCFuncionariosForm) form;
		SlipRCFuncionarioSoporteDTO inciso = null;
	 	try {
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			String slipGeneral = request.getParameter("slipGeneral");
			request.getSession().setAttribute("slipGeneral", slipGeneral);
			int id = Integer.parseInt(request.getParameter("inciso"));
			request.getSession().setAttribute("numeroInciso", id+"");
			//SlipSoporteDTO slipSoporteDTO = null;
			
			SlipSoporteDTO slipSoporteDTO = 
				(SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");
			
			if (idToSlip != null && slipSoporteDTO != null){
				 //slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				slipRCFuncionariosForm.setIdToSlip(idToSlip);
				   //Nuevo4s
				   if(slipSoporteDTO.getSlipRCFuncionarioSoporte()!= null){
					   for(SlipRCFuncionarioSoporteDTO rc : slipSoporteDTO.getSlipRCFuncionarioSoporte()){
						   if(rc.getNumeroInciso().intValue() == id){
							   inciso = rc;
							   break;
						   }
					   }
				   }					
				
				
				this.poblarSlipFuncionarios(slipRCFuncionariosForm,slipSoporteDTO, id, inciso);
				
				//Nuevo
				if(inciso != null){
					request.setAttribute("coberturas", inciso.getListaCoberturas());				
				}
	 		}
			request.setAttribute("anexosFuncionarios",inciso.getAnexos());			
			request.setAttribute("numeroInciso", inciso.getNumeroInciso().toPlainString());			
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarEditarRCConstructores(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipRCConstructoresForm slipRCConstructoresForm = (SlipRCConstructoresForm) form;
		SlipRCConstructoresSoporteDTO inciso = null;
		//SlipSoporteDTO slipSoporteDTO = null;
		try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			String slipGeneral = request.getParameter("slipGeneral");
			request.getSession().setAttribute("slipGeneral", slipGeneral);
			int id = Integer.parseInt(request.getParameter("inciso"));
			request.getSession().setAttribute("numeroInciso", id+"");

			SlipSoporteDTO slipSoporteDTO = 
				(SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			
			if (idToSlip != null && slipSoporteDTO != null){
				//slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				   slipRCConstructoresForm.setIdToSlip(idToSlip);
				
				   //Nuevo
				   if(slipSoporteDTO.getSlipRCConstructoresSoporte()!= null){
					   for(SlipRCConstructoresSoporteDTO rc : slipSoporteDTO.getSlipRCConstructoresSoporte()){
						   if(rc.getNumeroInciso().intValue() == id){
							   inciso = rc;
							   break;
						   }
					   }
				   }				
				
				 //Nuevo
				this.poblarSlipContructores(slipRCConstructoresForm,slipSoporteDTO,id, inciso);
				
				//Nuevo
				if(inciso != null){
					request.setAttribute("coberturas", 
							inciso.getListaCoberturas());				
				}					
				
	 		}
			List<TipoMaquinariaDTO> tipoMaquinariaList = TipoMaquinariaDN.getInstancia().listarTodos();
 			//request.setAttribute("coberturas", 1);
			request.setAttribute("tipoMaquinariaList", tipoMaquinariaList);
			request.setAttribute("anexosConstructores",inciso.getAnexos());
			request.setAttribute("numeroInciso", inciso.getNumeroInciso().toPlainString());
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();

 	 	} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarEditarTransportesDeCarga(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SlipTransportesDeCargaForm slipTransportesDeCargaForm = (SlipTransportesDeCargaForm) form;
	 	try { 
			String idToSlip = request.getParameter("idToSlip");
			request.getSession().setAttribute("idToSlip", idToSlip);
			
			SlipSoporteDTO slipSoporteDTO = 
				(SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");			
			
			if (idToSlip != null && slipSoporteDTO != null){
				//SlipSoporteDTO slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
				slipTransportesDeCargaForm.setIdToSlip(idToSlip);
				this.poblarSlipTransporte(slipTransportesDeCargaForm,slipSoporteDTO);
				
					if(slipSoporteDTO.getSlipTransportesSoporte() != null){
						request.setAttribute("coberturas", 
								slipSoporteDTO.getSlipTransportesSoporte().getListaCoberturas());
						
						
					}
	 			}
			//request.setAttribute("coberturas", 1);
 	 	} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} /*catch (SystemException e) {
			e.printStackTrace();
		}*/
		return mapping.findForward(reglaNavegacion);
	}
	 
 	
	private void poblarSlipFuncionarios(SlipRCFuncionariosForm slipRCFuncionariosForm,SlipSoporteDTO slipSoporteDTO, int id,SlipRCFuncionarioSoporteDTO inciso) {
		
		if(inciso != null){
			if(slipSoporteDTO.getSlipRCFuncionarioSoporte()!= null){
				
			   if(slipSoporteDTO.getIdSlip() != null){
				   slipRCFuncionariosForm.setIdToSlip(slipSoporteDTO.getIdSlip().toPlainString());
			   }
			   
		 	   if(inciso.getNumeroInciso() != null){
		 		  slipRCFuncionariosForm.setNumeroInciso(inciso.getNumeroInciso()+"");	
			   }			   
				
				if(inciso.getAccionLegal() != null){  
					slipRCFuncionariosForm.setAccionLegal(inciso.getAccionLegal()) ;	 
			    }
				if(inciso.getComision() != null){  
					slipRCFuncionariosForm.setComision(inciso.getComision()) ;	 
			    }
				if(inciso.getCondicionesGenerales() != null){  
					slipRCFuncionariosForm.setCondicionesGenerales(inciso.getCondicionesGenerales()) ;	 
			    }
				if(inciso.getCondicionesReaseguro() != null){  
					slipRCFuncionariosForm.setConsideracionesReaseguro(inciso.getCondicionesReaseguro()) ;	 
			    }
				if(inciso.getGarantiaExpresa() != null){  
					slipRCFuncionariosForm.setGarantiaExpresa(inciso.getGarantiaExpresa()) ;	 
			    }
				if(inciso.getImpuestos() != null){  
					slipRCFuncionariosForm.setImpuestos(inciso.getImpuestos() ) ;	 
			    }
				if(inciso.getInteres() != null){  
					slipRCFuncionariosForm.setInteres(inciso.getInteres()) ;	 
			    }
				if(inciso.getLeyesJurisdiccion() != null){  
					slipRCFuncionariosForm.setLeyesJurisdiccion(inciso.getLeyesJurisdiccion()) ;	 
			    }
				if(inciso.getLimiteTerritorial() != null){  
					slipRCFuncionariosForm.setLimiteTerritorial(inciso.getLimiteTerritorial()) ;	 
			    }
				if(inciso.getPeriodoNotificaciones() != null){  
					slipRCFuncionariosForm.setPeriodoAdicionalNotificaciones(inciso.getPeriodoNotificaciones()) ;	 
			    }
				if(inciso.getRetencion() != null){  
					slipRCFuncionariosForm.setRetencion(inciso.getRetencion()) ;	 
			    }
				if(inciso.getSubjetividades() != null){  
					slipRCFuncionariosForm.setSubjetividades(inciso.getSubjetividades()) ;	 
			    }
				if(inciso.getTerminoPrimas() != null){  
					slipRCFuncionariosForm.setTerminosPagoPrimas(inciso.getTerminoPrimas()) ;	 
			    }
				if(inciso.getFechaRetroactiva()!= null){  
					slipRCFuncionariosForm.setFechaRetroactiva(UtileriasWeb.getFechaString(inciso.getFechaRetroactiva()));	 
			    }
		 	}			
		}


		
	}


	private void poblarSlipIncendio(SlipIncedioForm slipIncedioForm, SlipSoporteDTO slipSoporteDTO){
	 	if(slipSoporteDTO.getSlipIncendioSoporte()!= null){
			if(slipSoporteDTO.getSlipIncendioSoporte().getProteccionesContraIncendio() != null){  
				slipIncedioForm.setProteccionContraIncendio(slipSoporteDTO.getSlipIncendioSoporte().getProteccionesContraIncendio());
			  }
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getDireccionRiesgo() != null){
				slipIncedioForm.setDireccionInciso(slipSoporteDTO.getSlipIncendioSoporte().getDireccionRiesgo() );
			}
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getGiroUbicacion() != null){
				slipIncedioForm.setGirodelAsegurado(slipSoporteDTO.getSlipIncendioSoporte().getGiroUbicacion());
			}
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getZonaTerremoto() != null){
				slipIncedioForm.setZonaTerremoto(slipSoporteDTO.getSlipIncendioSoporte().getZonaTerremoto());
			}
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getSumaAseguradaTotal() != null){
				slipIncedioForm.setSumaAseguradaTotal(slipSoporteDTO.getSlipIncendioSoporte().getSumaAseguradaTotal().toPlainString());
			}
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getSumaAseguradaPorUbicacion() != null){
				slipIncedioForm.setSumaAseguradaInciso(slipSoporteDTO.getSlipIncendioSoporte().getSumaAseguradaPorUbicacion().toPlainString());
			}
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getTipoConstruccion() != null){
				slipIncedioForm.setTipoConstruccion(slipSoporteDTO.getSlipIncendioSoporte().getTipoConstruccion());
			}
			
			if(slipSoporteDTO.getSlipIncendioSoporte().getNumeroInciso() != null){
				slipIncedioForm.setNumeroInciso(slipSoporteDTO.getSlipIncendioSoporte().getNumeroInciso().toPlainString());
			}
			
			if(slipSoporteDTO.getIdSlip() != null){
				slipIncedioForm.setIdToSlip(slipSoporteDTO.getIdSlip().toPlainString());
			}			
			
		}
 	}

	private void poblarSlipAviacion(SlipAviacionForm slipAviacionForm, SlipSoporteDTO slipSoporteDTO){
	 	if(slipSoporteDTO.getSlipAviacion()!= null){
	 		if(slipSoporteDTO.getSlipAviacion().getHorasMarcaTipo() != null){
	 			slipAviacionForm.setHorasMarcayTipo(slipSoporteDTO.getSlipAviacion().getHorasMarcaTipo());
	 		}
	 		if(slipSoporteDTO.getSlipAviacion().getPagosMedicosPasajeros() != null){
	 			slipAviacionForm.setGastosMedicosAParaPasajeros(slipSoporteDTO.getSlipAviacion().getPagosMedicosPasajeros().toString());	
	 		}
	 		if(slipSoporteDTO.getSlipAviacion().getLimitesGeograficos() != null){
	 			slipAviacionForm.setLimitesGeograficos(slipSoporteDTO.getSlipAviacion().getLimitesGeograficos());
	 		} 	  	
	 		if(slipSoporteDTO.getSlipAviacion().getAeropuertoBase() != null){
	 	    	slipAviacionForm.setAeropuertoBase(slipSoporteDTO.getSlipAviacion().getAeropuertoBase());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getAnio() != 0){
	 	    	slipAviacionForm.setAno(slipSoporteDTO.getSlipAviacion().getAnio()+"");
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getCapacidad() != null){
	 	    	slipAviacionForm.setCapacidad(slipSoporteDTO.getSlipAviacion().getCapacidad());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getMarca() != null){
	 	    	slipAviacionForm.setMarca(slipSoporteDTO.getSlipAviacion().getMarca());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getMatricula() != null){
	 	    	slipAviacionForm.setMatricula(slipSoporteDTO.getSlipAviacion().getMatricula());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getModelo() != null){
	 	    	slipAviacionForm.setModelo(slipSoporteDTO.getSlipAviacion().getModelo());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getNumeroInciso() != null){
	 	    	slipAviacionForm.setNumeroInciso(slipSoporteDTO.getSlipAviacion().getNumeroInciso()+"");
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getPagosMedicosTripulacion() != null){
	 	    	slipAviacionForm.setGastosMedicosBParaTripulantesyPilotos(slipSoporteDTO.getSlipAviacion().getPagosMedicosTripulacion()+"");
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getPagosVoluntariosPasajeros() != null){
	 	    	slipAviacionForm.setPagosVoluntarios(slipSoporteDTO.getSlipAviacion().getPagosVoluntariosPasajeros()+"");
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getSerie() != null){
	 	    	slipAviacionForm.setSerie(slipSoporteDTO.getSlipAviacion().getSerie());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getTipoAeronave() != 0){
	 	    	slipAviacionForm.setTipoAeronave(slipSoporteDTO.getSlipAviacion().getTipoAeronave()+"");
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getTipoAeropuerto() != null){
	 	    	slipAviacionForm.setTiposdeAeropuertos(slipSoporteDTO.getSlipAviacion().getTipoAeropuerto());
 	 	    }
	 		if(slipSoporteDTO.getSlipAviacion().getUso() != null){
	 	    	slipAviacionForm.setUso(slipSoporteDTO.getSlipAviacion().getUso());
 	 	    }
	 		
	 		if(slipSoporteDTO.getSlipAviacion().getSubLimites() != null){
	 	    	slipAviacionForm.setSubLimites(slipSoporteDTO.getSlipAviacion().getSubLimites());
 	 	    }
	 	
	 	}
 	}
	
	private void poblarSlipGeneral(SlipGeneralForm slipGeneralForm, SlipSoporteDTO slipSoporteDTO){
		
	 	if(slipSoporteDTO.getSlipGeneralSoporte()!= null){
	 		
	 		/*
	 		 * Tipo de Distribución: SUBINCISO
	 		 */
	 		if(slipSoporteDTO.getSlipGeneralSoporte().getSubInciso() != null){
		 		slipGeneralForm.setLabel("Datos del bien:");
		 		
		 		if(slipSoporteDTO.getSlipGeneralSoporte().getDatosBien() != null){
		 			slipGeneralForm.setDatosDelBien(slipSoporteDTO.getSlipGeneralSoporte().getDatosBien());
		 		}
		 		if(slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso() != null){
		 			slipGeneralForm.setNumeroInciso(slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso()+"");
		 		}
		 		if(slipSoporteDTO.getSlipGeneralSoporte().getSubInciso() != null){
		 			slipGeneralForm.setNumeroSubInciso(slipSoporteDTO.getSlipGeneralSoporte().getSubInciso()
		 						+ "("
		 						+ slipSoporteDTO.getSlipGeneralSoporte().getDescripcionSeccion()
		 						+ "-"
		 						+ slipSoporteDTO.getSlipGeneralSoporte().getDescripcionSubInciso()
		 						+ ")");
		 		}
	 		}
	 		
	 		/*
	 		 * Tipo de Distribución: INCISO
	 		 */
	 		if(slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso() != null){
		 		slipGeneralForm.setLabel("Datos de la Ubicaci&oacute;n:");
		 		slipGeneralForm.setNumeroSubInciso("N/A");
		 		
		 		if(slipSoporteDTO.getSlipGeneralSoporte().getDatosBien() != null){
		 			slipGeneralForm.setDatosDelBien(slipSoporteDTO.getSlipGeneralSoporte().getDatosBien());
		 		}
		 		if(slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso() != null){
		 			slipGeneralForm.setNumeroInciso(slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso()+"");
		 		}
	 		}
	 		
		}
	 	
	 	if(slipSoporteDTO.getSlipPrimerRiesgo() != null){
	 		slipGeneralForm.setLabel("Descripci&oacute;n:");
	 		slipGeneralForm.setNumeroInciso("N/A");
	 		slipGeneralForm.setNumeroSubInciso("N/A");

	 		if(slipSoporteDTO.getSlipPrimerRiesgo().getDescripcion() != null){
	 			slipGeneralForm.setDatosDelBien(slipSoporteDTO.getSlipPrimerRiesgo().getDescripcion());
	 		}	 		
	 		
	 	}
	 	
 	}

	private void poblarSlipEquipoContratista(SlipEquipoContratistaForm slipEquipoContratistaForm, SlipSoporteDTO slipSoporteDTO){
	 	if(slipSoporteDTO.getSlipEquipoConstratista()!= null){
	 		if(slipSoporteDTO.getSlipEquipoConstratista().getNumeroInciso() != null){
	 			slipEquipoContratistaForm.setNumeroInciso(slipSoporteDTO.getSlipEquipoConstratista().getNumeroInciso()+"");
	 		}
	 		if(slipSoporteDTO.getSlipEquipoConstratista().getNumeroSubInciso() != null){
	 			slipEquipoContratistaForm.setNumeroSubinciso(slipSoporteDTO.getSlipEquipoConstratista().getNumeroSubInciso()+"");
	 		}
	 		if(slipSoporteDTO.getSlipEquipoConstratista().getUbicacion() != null){
	 			slipEquipoContratistaForm.setDireccionInciso(slipSoporteDTO.getSlipEquipoConstratista().getUbicacion());
	 		}

	 		if(slipSoporteDTO.getSlipEquipoConstratista().getIncisoMayorValor()!= null){
	 			slipEquipoContratistaForm.setSubincisoMayorValor(slipSoporteDTO.getSlipEquipoConstratista().getIncisoMayorValor());
	 		}

	 		if(slipSoporteDTO.getCotizacionDTO() != null && slipSoporteDTO.getCotizacionDTO().getIdToCotizacion()!= null){
	 			slipEquipoContratistaForm.setCotizacion(slipSoporteDTO.getCotizacionDTO().getIdToCotizacion().toPlainString());
	 		}	 		
	 		
		}
 	}
	
	 
	    
   private void poblarSlipContructores(SlipRCConstructoresForm slipRCConstructoresForm, SlipSoporteDTO slipSoporteDTO,int id, SlipRCConstructoresSoporteDTO inciso){
	   
	   if(inciso != null){
		   
		   if(slipSoporteDTO.getIdSlip() != null){
			   slipRCConstructoresForm.setIdToSlip(slipSoporteDTO.getIdSlip().toPlainString());
		   }
		   
	 	  if(inciso.getColindantes() != null){
	 		  slipRCConstructoresForm.setColindantes(inciso.getColindantes());
	 	    }
	 	  if(inciso.getExperienciaLaboral() != null){
	 		 slipRCConstructoresForm.setExperienciaParaRealizarObra(inciso.getExperienciaLaboral());	
	 	    }
	 	  if(inciso.getParaQuienRealizaLaObra() != null){
	 		 slipRCConstructoresForm.setParaquienRealizaObra(inciso.getParaQuienRealizaLaObra());
	 	    }
	 	  if(inciso.getSistemasPrevistos() != null){
	 		 slipRCConstructoresForm.setSistemasPreventivosContraDanos(inciso.getSistemasPrevistos());
	 	    }
	 	  if(inciso.getTipoMaquinaria() != 0){
	 		 slipRCConstructoresForm.setTipoMaquinaria(Integer.valueOf(inciso.getTipoMaquinaria()).toString());	
	 	    }
	 	  if(inciso.getValorEstimadoObra() != null){
	 		 slipRCConstructoresForm.setValorEstimadoObra(inciso.getValorEstimadoObra().toString());	
	 	    }
	 	  if(inciso.getNumeroInciso() != null){
	 		 slipRCConstructoresForm.setNumeroInciso(inciso.getNumeroInciso()+"");	
	 	    }
	 	  if(inciso.getSitioTrabajos() != null){
	 		slipRCConstructoresForm.setSitioTrabajo(inciso.getSitioTrabajos());	
	 	    }
	 	  if(inciso.getDescripcionObra() != null){
		 		slipRCConstructoresForm.setDescripcionObra(inciso.getDescripcionObra());	
		 	}
	 	  if(inciso.getDuracionObra() != null){
		 		slipRCConstructoresForm.setDuracionObra(inciso.getDuracionObra());	
		    }
		}
 	}
			  
	private void poblarSlipTransporte(SlipTransportesDeCargaForm slipTransportesDeCargaForm, SlipSoporteDTO slipSoporteDTO){
	 	if(slipSoporteDTO.getSlipTransportesSoporte()!= null){
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getBienesAsegurados() != null){
	 			slipTransportesDeCargaForm.setBienesAsegurados(slipSoporteDTO.getSlipTransportesSoporte().getBienesAsegurados());
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getDescripcionMedioTransporte() != null){
	 			slipTransportesDeCargaForm.setMediosTransportes(slipSoporteDTO.getSlipTransportesSoporte().getDescripcionMedioTransporte());
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getLimiteMaximoPorEmbarque() != null){
	 			slipTransportesDeCargaForm.setLimiteMaximoEmbarque(slipSoporteDTO.getSlipTransportesSoporte().getLimiteMaximoPorEmbarque()+"");
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getLugarDestino() != null){
	 			slipTransportesDeCargaForm.setLugarDestino(slipSoporteDTO.getSlipTransportesSoporte().getLugarDestino());
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getLugarOrigen() != null){
	 			slipTransportesDeCargaForm.setLugarOrigen(slipSoporteDTO.getSlipTransportesSoporte().getLugarOrigen());
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getNumeroInciso() != null){
	 			slipTransportesDeCargaForm.setNumeroInciso(slipSoporteDTO.getSlipTransportesSoporte().getNumeroInciso()+"");
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getRiesgosAmparados() != null){
	 			slipTransportesDeCargaForm.setRiesgoAmparados(slipSoporteDTO.getSlipTransportesSoporte().getRiesgosAmparados());
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getTipoEmpaque() != null){
	 			slipTransportesDeCargaForm.setTipoEmpaqueEmbalaje(slipSoporteDTO.getSlipTransportesSoporte().getTipoEmpaque());
	 		}
	 		if(slipSoporteDTO.getSlipTransportesSoporte().getVolumenAnualEmbarque() != null){
	 			slipTransportesDeCargaForm.setVolumenAnualEmbarque(slipSoporteDTO.getSlipTransportesSoporte().getVolumenAnualEmbarque()+"");
	 		}
	 		if(slipSoporteDTO.getIdSlip() != null){
	 			slipTransportesDeCargaForm.setIdToSlip(slipSoporteDTO.getIdSlip().toPlainString());
	 		}	 		
		}
 	}
	
	private void poblarSlipBarco(SlipBarcosForm slipBarcosForm, SlipSoporteDTO slipSoporteDTO){
		if(slipSoporteDTO.getSlipBarcos()!= null){
			if(slipSoporteDTO.getSlipBarcos().getLugarConstruccion() != null){
		 	   	slipBarcosForm.setLugardeConstruccion(slipSoporteDTO.getSlipBarcos().getLugarConstruccion());
	 	    }
	 	    if(slipSoporteDTO.getSlipBarcos().getMatricula() != null){
	 	    	slipBarcosForm.setMatricula(slipSoporteDTO.getSlipBarcos().getMatricula());	
	 	    }
	  	    if(slipSoporteDTO.getSlipBarcos().getSerie() != null){
	  	    	slipBarcosForm.setSerie(slipSoporteDTO.getSlipBarcos().getSerie());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getUltimaFechaMttoInspeccion() != null){
	  	 		slipBarcosForm.setUltimafechAmttoInspeccion(UtileriasWeb.getFechaString(slipSoporteDTO.getSlipBarcos().getUltimaFechaMttoInspeccion()));
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getAguasNavegacion() != null){
	  	 		slipBarcosForm.setAreadeNavegacion(slipSoporteDTO.getSlipBarcos().getAguasNavegacion());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getAnioConstruccion() != 0){
	  	 		slipBarcosForm.setAno(slipSoporteDTO.getSlipBarcos().getAnioConstruccion()+"");
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getBandera() != null){
	  	 		slipBarcosForm.setBanderadeConveniencia(slipSoporteDTO.getSlipBarcos().getBandera());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getClasificacion() != null){
	  	 		slipBarcosForm.setClasificacion(slipSoporteDTO.getSlipBarcos().getClasificacion());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getEslora() != null){
	  	 		slipBarcosForm.setEslora(slipSoporteDTO.getSlipBarcos().getEslora());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getManga() != null){
	  	 		slipBarcosForm.setManga(slipSoporteDTO.getSlipBarcos().getManga());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getMaterialCasco() != null){
	  	 		slipBarcosForm.setMaterialdelCasco(slipSoporteDTO.getSlipBarcos().getMaterialCasco());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getNumeroInciso() != null){
	  	 		slipBarcosForm.setNumeroInciso(slipSoporteDTO.getSlipBarcos().getNumeroInciso()+"");
	  	    }
	  	 	if(slipSoporteDTO.getSlipBarcos().getPuertoRegistro() != null){
	  	 		slipBarcosForm.setPuertodeRegistro(slipSoporteDTO.getSlipBarcos().getPuertoRegistro());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getPuntal() != null){
	  	 		slipBarcosForm.setPuntal(slipSoporteDTO.getSlipBarcos().getPuntal());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getSumaAsegurada() != null){
	  	 		slipBarcosForm.setSumaAsegurada(slipSoporteDTO.getSlipBarcos().getSumaAsegurada()+"");
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getSumaAseguradaCasco() != null){
	  	 		slipBarcosForm.setSumaAseguradaCasco(slipSoporteDTO.getSlipBarcos().getSumaAseguradaCasco()+"");
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getSumaAseguradaRC() != null){
	  	 		slipBarcosForm.setSumaAseguradaRC(slipSoporteDTO.getSlipBarcos().getSumaAseguradaRC()+"");
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getSumaAseguradaVI() != null){
	  	 		slipBarcosForm.setSumaAseguradaValorIncrementado(slipSoporteDTO.getSlipBarcos().getSumaAseguradaVI()+"");
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getTipoEmbarcacion() != null){
	  	 		slipBarcosForm.setTipodeEmbarcacion(slipSoporteDTO.getSlipBarcos().getTipoEmbarcacion());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getTonelajeBruto() != null){
	  	 		slipBarcosForm.setTonelajeBruto(slipSoporteDTO.getSlipBarcos().getTonelajeBruto());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getTonelajeNeto() != null){
	  	 		slipBarcosForm.setTonelajeNeto(slipSoporteDTO.getSlipBarcos().getTonelajeNeto());
	  	    } 	
	  	 	if(slipSoporteDTO.getSlipBarcos().getUso() != null){
	  	 		slipBarcosForm.setUso(slipSoporteDTO.getSlipBarcos().getUso());
	  	    } 	
		}
 	}
	
	private void poblarSlipObraCivil(SlipObraCivilForm slipObraCivilForm, SlipSoporteDTO slipSoporteDTO){
		
		if(slipSoporteDTO.getSlipObraCivilSoporte() != null){
			
			/*
			 * Sumar coberturas:
			 */
			BigDecimal sumaAseguradaTotal = sumarCoberturas(slipSoporteDTO.getSlipObraCivilSoporte().getListaCoberturas()); 
			
			slipSoporteDTO.getSlipObraCivilSoporte().setSumaAseguradaObra(sumaAseguradaTotal);
			slipSoporteDTO.getSlipObraCivilSoporte().setSumaTotalAsegurada(sumaAseguradaTotal);
			
			if(slipSoporteDTO.getSlipObraCivilSoporte().getSumaAseguradaObra() != null){
				slipObraCivilForm.setDumaAsegurada(slipSoporteDTO.getSlipObraCivilSoporte().getSumaAseguradaObra().toPlainString());
				slipObraCivilForm.setSumasAseguradasSublimitesAdyacentes(slipSoporteDTO.getSlipObraCivilSoporte().getSumaAseguradaObra().toPlainString());
			}
			
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getSumaTotalAsegurada() != null){
	    		slipObraCivilForm.setSumasAseguradasSublimitesAdyacentes(slipSoporteDTO.getSlipObraCivilSoporte().getSumaTotalAsegurada().toString());
			}
			
			if(slipSoporteDTO.getSlipObraCivilSoporte().getAguasCercanas()!= null){
			   slipObraCivilForm.setRioLagoMarEtcMasCercano(slipSoporteDTO.getSlipObraCivilSoporte().getAguasCercanas());
		    }
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getBeneficiarioPreferente() != null){
	    		slipObraCivilForm.setBeneficiarioPreferente(slipSoporteDTO.getSlipObraCivilSoporte().getBeneficiarioPreferente());
			}	
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getClausulaTerceros() != null){
	    		slipObraCivilForm.setAnadirClausulaConsiderandoTercerosBienesPersonal(slipSoporteDTO.getSlipObraCivilSoporte().getClausulaTerceros());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getDireccionContratista() != null){
	    		slipObraCivilForm.setDireccionContratista(slipSoporteDTO.getSlipObraCivilSoporte().getDireccionContratista());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getEdificiosTerceros() != null){
	    		slipObraCivilForm.setEdificiosyEstructurasdeTercerosquePuedanSerAfectados(slipSoporteDTO.getSlipObraCivilSoporte().getEdificiosTerceros());
			}	
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getExistenEstructurasAdyacentes() != null){
	    		slipObraCivilForm.setExistenciaDeEstructurasAdyacentes(slipSoporteDTO.getSlipObraCivilSoporte().getExistenEstructurasAdyacentes());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getExperienciaContratista() != null){
	    		slipObraCivilForm.setExperienciadelcontratista(slipSoporteDTO.getSlipObraCivilSoporte().getExperienciaContratista());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getFechaTerminacionPeriodoMantenimiento() != null){
	    		slipObraCivilForm.setFechaTerminacionPeriodoMantenimiento(slipSoporteDTO.getSlipObraCivilSoporte().getFechaTerminacionPeriodoMantenimiento());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getIngenieroConsultor() != null){
	    		slipObraCivilForm.setIngenieroConsultor(slipSoporteDTO.getSlipObraCivilSoporte().getIngenieroConsultor());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getMaquinaria() != null){
	    		slipObraCivilForm.setMaquinariaEquiposFijosMoviles(slipSoporteDTO.getSlipObraCivilSoporte().getMaquinaria());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getMetodosMaterialesConstruccion() != null){
	    		slipObraCivilForm.setMetodosymaterialesdeconstruccion(slipSoporteDTO.getSlipObraCivilSoporte().getMetodosMaterialesConstruccion());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getNombreContratista() != null){
	    		slipObraCivilForm.setNombreContratista(slipSoporteDTO.getSlipObraCivilSoporte().getNombreContratista());
	     	}

	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getTituloContrato() != null){
	    		slipObraCivilForm.setTitulodelContrato(slipSoporteDTO.getSlipObraCivilSoporte().getTituloContrato());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getTrabajosPorContrato() != null){
	    		slipObraCivilForm.setTrabajosPorContrato(slipSoporteDTO.getSlipObraCivilSoporte().getTrabajosPorContrato());
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getTrabajoSubContratistas() != null){
	    		slipObraCivilForm.setTrabajosSubcontratista(slipSoporteDTO.getSlipObraCivilSoporte().getTrabajoSubContratistas());	
			}
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getValorPorContrato() != null){
	    		slipObraCivilForm.setValorPorContrato(slipSoporteDTO.getSlipObraCivilSoporte().getValorPorContrato());
			}	 
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getCondicionesMetereologicas() != null){
	    		slipObraCivilForm.setCondicionesMetereologicas(slipSoporteDTO.getSlipObraCivilSoporte().getCondicionesMetereologicas());
			}	 
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getContratistaTienePolizaRC() != null){
	    		slipObraCivilForm.setTienePolizaSeparadadeRC(slipSoporteDTO.getSlipObraCivilSoporte().getContratistaTienePolizaRC());
			}	 
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getCumpleRegulacionesTerremotos() != null){
	    		slipObraCivilForm.setCumpleRegulacionesSobreEstructurasResistentesaTerremotos(slipSoporteDTO.getSlipObraCivilSoporte().getCumpleRegulacionesTerremotos());
			}	 
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getDireccionSubContratista() != null){
	    		slipObraCivilForm.setDireccionSubcontratista(slipSoporteDTO.getSlipObraCivilSoporte().getDireccionSubContratista());
			}	 
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getNombreSubContratista() != null){
	    		slipObraCivilForm.setNombreSubcontratista(slipSoporteDTO.getSlipObraCivilSoporte().getNombreSubContratista());
			}	 
	    	if(slipSoporteDTO.getSlipObraCivilSoporte().getNumeroInciso() != null){
	    		slipObraCivilForm.setNumeroInciso(slipSoporteDTO.getSlipObraCivilSoporte().getNumeroInciso()+"");
			}	  
	    	
	    	
	    	

		
		}
 	}
	
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
		 	String idToSlip = (String) request.getSession().getAttribute("idToSlip");
		 	String slipGeneral = (String) request.getSession().getAttribute("slipGeneral");
		 	SlipDTO slipDTO = SlipDN.getInstancia().getPorId(new BigDecimal(idToSlip));
  			if(slipGeneral.equals("0")){
		 	    List<SlipAnexoDTO> slipAnexos = SlipAnexoDN.getInstancia().listarAnexosSlip(UtileriasWeb.regresaBigDecimal(idToSlip.toString()));;
		 	    List<SlipAnexoSoporteDTO> listaAnexosList = new ArrayList<SlipAnexoSoporteDTO>();
		 	     for(SlipAnexoDTO slipAnexoDTO :  slipAnexos){
 	 	 			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
 	 	 	 		slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoDTO.getIdToSlipDocumentoAnexo());
 	 	 			slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoDTO.getIdToControlArchivo());
 	 	 			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoSoporteDTO.getIdToControlArchivo()); 
 	 	 			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
 	 	 			listaAnexosList.add(slipAnexoSoporteDTO);
 	 	 		  }  	
		 	    request.setAttribute("anexos", listaAnexosList);
  			}else{
  			    if(slipDTO.getTipoSlip().equals("3")){	
  				  List<SlipAnexoEquipoContratistaDTO> slipAnecoEquipoContratista = SlipAnexoEquipoContratistaDN.getInstancia().buscarPorPropiedad("slipDTO.idToSlip", new BigDecimal(idToSlip));
  				  List<SlipAnexoSoporteDTO> listaAnexosEquipoConstratistaList = new ArrayList<SlipAnexoSoporteDTO>();
  	 	  	  	 for(SlipAnexoEquipoContratistaDTO slipAnexoEquipoContratistaDTO :  slipAnecoEquipoContratista){
  	 	 			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
  	 	 	 		slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoEquipoContratistaDTO.getIdToSlipDocumentoAnexo());
  	 	 			slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoEquipoContratistaDTO.getIdToControlArchivo());
  	 	 			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoEquipoContratistaDTO.getIdToControlArchivo()); 
  	 	 			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
  	 	 			listaAnexosEquipoConstratistaList.add(slipAnexoSoporteDTO);
  	 	 		  }  
  	 	      	  request.setAttribute("anexosContratista", listaAnexosEquipoConstratistaList);
  	 	   	      reglaNavegacion = "contratista"; 
  			    }
  			  if(slipDTO.getTipoSlip().equals("6")){ // RC Constructores
  				List<SlipAnexoConstructoresDTO> listaAnexosConstructoresList = SlipAnexoRCConstructoresDN.getInstancia().buscarPorPropiedad("slipConstructoresDTO.id.idToSlip",new BigDecimal(idToSlip));
  	 	  	 	List<SlipAnexoSoporteDTO> slipAnexosSlipConstructoresList = new ArrayList<SlipAnexoSoporteDTO>();
  	 	  	 	
  	 	  	 	// GOOD
  	 	  	 	if(listaAnexosConstructoresList != null && !listaAnexosConstructoresList.isEmpty()){
  	  	 	     	for(SlipAnexoConstructoresDTO slipAnexoConstructoresDTO :  listaAnexosConstructoresList){
  	  	 	 			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
  	  	 	 	 		slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
  	  	 	 			slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
  	  	 	 			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoConstructoresDTO.getIdToControlArchivo()); 
  	  	 	 			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
  	  	 	 			slipAnexosSlipConstructoresList.add(slipAnexoSoporteDTO);
  	  	 	 		}    	 	  	 		
  	 	  	 	}
  	 	  	 	
  	 	  	 	/*
  	 	  	 	List<SlipAnexoConstructoresDTO> listaAnexosConstructoresListTemporal = 
  	 	  	 							(List<SlipAnexoConstructoresDTO>)request.getSession().getAttribute("listaAnexosTemporales");
  	 	  	 	
	  	 	  	if(listaAnexosConstructoresListTemporal != null && !listaAnexosConstructoresListTemporal.isEmpty()){
	  	 	  		for(SlipAnexoConstructoresDTO slipAnexoConstructoresDTO : listaAnexosConstructoresListTemporal){
		  	 	 			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
	  	  	 	 	 		slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
	  	  	 	 			slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
	  	  	 	 			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoConstructoresDTO.getIdToControlArchivo()); 
	  	  	 	 			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
	  	  	 	 			slipAnexosSlipConstructoresList.add(slipAnexoSoporteDTO);  	 	  			
	  	 	  		}
	  	 	  	}
	  	 	  	*/
 
 	 	        request.setAttribute("anexosConstructores", slipAnexosSlipConstructoresList);
	 	   	    reglaNavegacion = "constructores"; 
  			  }
  			if(slipDTO.getTipoSlip().equals("7")){
  				List<SlipFuncionarioAnexoDTO> listaFuncionariosList = SlipAnexoRCFuncionariosDN.getInstancia().buscarPorPropiedad("slipFuncionarioDTO.id.idToSlip", new BigDecimal(idToSlip));
  		  	    List<SlipAnexoSoporteDTO> slipAnexosSlipFuncionariosList = new ArrayList<SlipAnexoSoporteDTO>();
  	 	 	 	for(SlipFuncionarioAnexoDTO slipFuncionarioAnexoDTO :  listaFuncionariosList){
  		 			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
  		 	 		slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipFuncionarioAnexoDTO.getIdSlipDocumentoAnexo());
  		 			slipAnexoSoporteDTO.setIdToControlArchivo(slipFuncionarioAnexoDTO.getIdControlArchivo());
  		 			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipFuncionarioAnexoDTO.getIdControlArchivo()); 
  		 			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
  		 			slipAnexosSlipFuncionariosList.add(slipAnexoSoporteDTO);
  		 		}   
  	 	         request.setAttribute("anexosFuncionarios", slipAnexosSlipFuncionariosList);
	 	   	     reglaNavegacion = "funcionarios"; 
  			  }
  			  
  			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
		    String idToSlip =(String) request.getSession().getAttribute("idToSlip");
		    String slipGeneral =(String) request.getSession().getAttribute("slipGeneral");
		    SlipDTO slipDTO = SlipDN.getInstancia().getPorId(new BigDecimal(idToSlip));
 		    if(slipGeneral.equals("0")){
	 			SlipAnexoDTO slipAnexoDTO = SlipAnexoDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(id));
			 	ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
				controlArchivoDTO.setIdToControlArchivo(slipAnexoDTO.getIdToControlArchivo());
				ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);			
				SlipAnexoDN.getInstancia().borrar(slipAnexoDTO);		    	
		    }else{
		    	if(slipDTO.getTipoSlip().equals("3")){
		    		SlipAnexoEquipoContratistaDTO slipAnexoEquipoContratistaDTO = SlipAnexoEquipoContratistaDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(id));
				 	ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
					controlArchivoDTO.setIdToControlArchivo(slipAnexoEquipoContratistaDTO.getIdToControlArchivo());
					ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);			
					SlipAnexoEquipoContratistaDN.getInstancia().borrar(slipAnexoEquipoContratistaDTO);	
		            reglaNavegacion = "contratista";
		    	}else if(slipDTO.getTipoSlip().equals("6")){
		    		    SlipAnexoRCConstructoresDN slipAnexoRCConstructoresDN = SlipAnexoRCConstructoresDN.getInstancia();
			    		SlipAnexoConstructoresDTO slipAnexoConstructoresDTO = slipAnexoRCConstructoresDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
					 	ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
						controlArchivoDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
						ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);			
						SlipAnexoRCConstructoresDN.getInstancia().borrar(slipAnexoConstructoresDTO);	
			            reglaNavegacion = "constructores";  	
		    	}else if(slipDTO.getTipoSlip().equals("7")){
	    		    SlipAnexoRCFuncionariosDN slipAnexoRCFuncionariosDN = SlipAnexoRCFuncionariosDN.getInstancia();
		    		SlipFuncionarioAnexoDTO slipFuncionarioAnexoDTO = slipAnexoRCFuncionariosDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
				 	ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
					controlArchivoDTO.setIdToControlArchivo(slipFuncionarioAnexoDTO.getIdControlArchivo());
					ControlArchivoDN.getInstancia().borrar(controlArchivoDTO);			
					SlipAnexoRCFuncionariosDN.getInstancia().borrar(slipFuncionarioAnexoDTO);	
		           reglaNavegacion = "constructores";  	
	    	}
		    }
	
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward editarSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		//SlipForm slipForm = (SlipForm) form;
		String idToSlip = request.getParameter("idToSlip");
		HttpSession session = request.getSession();
		session.setAttribute("idToSlip", idToSlip);
		try {
			SlipDTO slipDTO = new SlipDTO();
			slipDTO = SlipDN.getInstancia().getPorId(new BigDecimal(idToSlip));
			SlipForm slipForm = new SlipForm();
			poblarDTO(slipDTO, slipForm);
			SlipDN.getInstancia().modificar(slipDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void guardarSlip(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SlipForm slipForm = (SlipForm) form;
		String idToSlip = request.getParameter("idToSlip");
		HttpSession session = request.getSession();
		session.setAttribute("idToSlip", idToSlip);
	 	String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		
		try {
			SlipDTO slipDTO = SlipDN.getInstancia().getPorId(new BigDecimal(idToSlip));
			//SlipForm slipForm = new SlipForm();
			poblarDTO(slipDTO, slipForm);
			SlipDN.getInstancia().modificar(slipDTO);
 	        id = Sistema.EXITO;
 		 	mensaje = "se guardo correctamente";			
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} finally{
			 UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
		   }
		//return mapping.findForward(reglaNavegacion);
	}	
  	  
 	
  public void guardarSlipBarco(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean agregarSlip = true;
	 	String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		SlipBarcosForm slipForm = (SlipBarcosForm) form;
		BigDecimal idToSlip = new BigDecimal(request.getParameter("idToSlip").toString());
 	    try{
 	       SlipBarcoDTO slipBarcoDTO = null;
		   SlipBarcoDN slipBarcoDN = SlipBarcoDN.getInstancia();     
		   slipBarcoDTO = slipBarcoDN.getPorId(idToSlip);
		   if (slipBarcoDTO == null){
		   	  slipBarcoDTO = new SlipBarcoDTO();
		   	  agregarSlip = false;
		   }
		     poblarSlipBarcoDTO(slipBarcoDTO,slipForm,idToSlip);
		   if(agregarSlip){
		 	  slipBarcoDN.modificar(slipBarcoDTO);   
		   }else{
		  	  slipBarcoDN.agregar(slipBarcoDTO);
		   } 
 	     id = Sistema.EXITO;
	 	 mensaje = "se guardo correctamente";
	     }catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
 	    }finally{
		UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
	   }
		//return mapping.findForward(reglaNavegacion);
	}
	
	public void guardarSlipAviacion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean agregarSlip = true;
 		String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		SlipAviacionForm slipAviacionForm = (SlipAviacionForm) form;
		BigDecimal idToSlip = new BigDecimal(request.getParameter("idToSlip").toString());
 	    try{
            SlipAviacionDTO slipAviacionDTO = null;
 			SlipAviacionDN slipAviacionDN = SlipAviacionDN.getInstancia();     
 			slipAviacionDTO = slipAviacionDN.getPorId(idToSlip);
 			if (slipAviacionDTO == null){
 				  slipAviacionDTO = new SlipAviacionDTO();
 				  agregarSlip = false;
 			  }
 			   poblarSlipAviacionDTO(slipAviacionDTO,slipAviacionForm,idToSlip);
 			if(agregarSlip){
 			   slipAviacionDN.modificar(slipAviacionDTO);   
 			 }else{
 				  slipAviacionDN.agregar(slipAviacionDTO);
 			 }
 	      id = Sistema.EXITO;
	 	  mensaje = "se guardo correctamente";
	     }catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
 	    }finally{
		UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
	   }
	}
	
	public void guardarSlipIncendio(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean agregarSlip = true;
	 	String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		SlipIncedioForm slipIncedioForm = (SlipIncedioForm) form;
		BigDecimal idToSlip = new BigDecimal(request.getParameter("idToSlip").toString());
 	    try{
 	 	   SlipIncendioDTO slipIncendioDTO = null;
		   SlipIncendioDN slipIncendioDN = SlipIncendioDN.getInstancia();     
		   slipIncendioDTO = slipIncendioDN.getPorId(idToSlip);
		   if (slipIncendioDTO == null){
			   slipIncendioDTO = new SlipIncendioDTO();
			  agregarSlip = false;
		   }
		   poblarSlipIncendioDTO(slipIncendioDTO,slipIncedioForm);
		   if(agregarSlip){
			  slipIncendioDN.modificar(slipIncendioDTO);   
		   }else{
	 		 slipIncendioDN.agregar(slipIncendioDTO);
		   }
 	      id = Sistema.EXITO;
	 	  mensaje = "se guardo correctamente";
	     }catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
 	    }finally{
		UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
	   }
	}
	
	public void guardarSlipObraCivil(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean agregarSlip = true;
 		String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		SlipObraCivilForm slipObraCivilForm = (SlipObraCivilForm) form;
		BigDecimal idToSlip = new BigDecimal(request.getParameter("idToSlip").toString());
 	    try{
 	       SlipObraCivilDTO slipObraCivilDTO = null;
 		   SlipObraCivilDN slipObraCivilDN = SlipObraCivilDN.getInstancia();     
 		   slipObraCivilDTO = slipObraCivilDN.getPorId(idToSlip);
 		   if (slipObraCivilDTO == null){
 			  slipObraCivilDTO = new SlipObraCivilDTO();
 			  agregarSlip = false;
 		   }
 		   poblarSlipObraCivilDTO(slipObraCivilDTO,slipObraCivilForm);
 		   if(agregarSlip){
 			  slipObraCivilDN.modificar(slipObraCivilDTO);   
 		   }else{
 			  slipObraCivilDN.agregar(slipObraCivilDTO);
 		   }
 	      id = Sistema.EXITO;
	 	  mensaje = "se guardo correctamente";
	     }catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
 	    }finally{
		UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
	   }
	 }
	
	public void guardarSlipRCConstructores(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean agregarSlip = true;
	 	String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		SlipRCConstructoresForm slipRCConstructoresForm = (SlipRCConstructoresForm) form;
		
		BigDecimal idToSlip = new BigDecimal(request.getParameter("idToSlip").toString());
		BigDecimal numeroInciso = new BigDecimal(request.getParameter("numeroInciso").toString());
 	    try{
 	    	SlipConstructoresDTO slipConstructoresDTO = null;
 			SlipRCConstructoresDN slipRCConstructoresDN = SlipRCConstructoresDN.getInstancia();
 			
 			SlipConstructoresDTOId idSlip = new SlipConstructoresDTOId();
 			idSlip.setIdToSlip(idToSlip);
 			idSlip.setNumeroInciso(numeroInciso);
 			
 			slipConstructoresDTO = slipRCConstructoresDN.getPorId(idSlip);
 			if (slipConstructoresDTO == null) {
 				slipConstructoresDTO = new SlipConstructoresDTO();
 				agregarSlip = false;
 			}
 			poblarSlipContructorDTO(slipConstructoresDTO, slipRCConstructoresForm);
 			if (agregarSlip) {
 				slipRCConstructoresDN.modificar(slipConstructoresDTO);
 			} else {
 				slipRCConstructoresDN.agregar(slipConstructoresDTO);
 			}
 			List<TipoMaquinariaDTO> tipoMaquinariaList = TipoMaquinariaDN.getInstancia().listarTodos();
 	 		request.setAttribute("tipoMaquinariaList", tipoMaquinariaList);
 	        id = Sistema.EXITO;
	 	  mensaje = "se guardo correctamente";
	     }catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
 	    }finally{
		 UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
	   }
		
	}
	
	public void guardarSlipRCFuncionarios(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean agregarSlip = true;
	 	String id = Sistema.ERROR;
		String mensaje = "error al Ejecutar";
		SlipRCFuncionariosForm slipRCFuncionariosForm = (SlipRCFuncionariosForm) form;
		
		
		BigDecimal idToSlip = new BigDecimal(request.getParameter("idToSlip").toString());
		
		
		BigDecimal numeroInciso = new BigDecimal(request.getParameter("numeroInciso").toString());
	
		
 	    try{
 			SlipFuncionarioDTO slipFuncionarioDTO = null;
 			SlipRCFuncionariosDN slipFuncionariosDN = SlipRCFuncionariosDN.getInstancia();
 			
 			SlipFuncionarioDTOId idSlip = new SlipFuncionarioDTOId();
 			idSlip.setIdToSlip(idToSlip);
 			idSlip.setNumeroInciso(numeroInciso);
 			
 			slipFuncionarioDTO = slipFuncionariosDN.getPorId(idSlip);
 			
 			
 			if (slipFuncionarioDTO == null) {
 				slipFuncionarioDTO = new SlipFuncionarioDTO();
 				agregarSlip = false;
 			}
 			poblarSlipFuncionarioDTO(slipFuncionarioDTO, slipRCFuncionariosForm, idToSlip, numeroInciso);
 			if (agregarSlip) {
 				slipFuncionariosDN.modificar(slipFuncionarioDTO);
 			} else {
 				slipFuncionariosDN.agregar(slipFuncionarioDTO);
 			}    
 	      id = Sistema.EXITO;
	 	  mensaje = "se guardo correctamente";
	     }catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
 	    }finally{
		UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
	   }
 	}
	
	
	
	private void poblarSlipFuncionarioDTO(SlipFuncionarioDTO slipFuncionarioDTO, SlipRCFuncionariosForm slipRCFuncionariosForm,BigDecimal idToSlip, BigDecimal numeroInciso) {
		
		if (idToSlip != null && numeroInciso != null){
			SlipFuncionarioDTOId id = new SlipFuncionarioDTOId();
			id.setIdToSlip(idToSlip);
			id.setNumeroInciso(numeroInciso);
 			slipFuncionarioDTO.setId(id);
		}
	 	if(slipRCFuncionariosForm != null){
	 		      slipFuncionarioDTO.setAccionLegal(slipRCFuncionariosForm.getAccionLegal());
	 		      slipFuncionarioDTO.setComision(slipRCFuncionariosForm.getComision());
	 		      slipFuncionarioDTO.setCondicionesReaseguro(slipRCFuncionariosForm.getConsideracionesReaseguro());
	 		      slipFuncionarioDTO.setCondicionesGenerales(slipRCFuncionariosForm.getCondicionesGenerales());
	 		      slipFuncionarioDTO.setGarantiaExpresa(slipRCFuncionariosForm.getGarantiaExpresa());
	 		      slipFuncionarioDTO.setImpuestos(slipRCFuncionariosForm.getImpuestos());
	 		      slipFuncionarioDTO.setInteres(slipRCFuncionariosForm.getInteres());
	 		      slipFuncionarioDTO.setLeyesJurisdiccion(slipRCFuncionariosForm.getLeyesJurisdiccion());
	 		      slipFuncionarioDTO.setLimiteTerritorial(slipRCFuncionariosForm.getLimiteTerritorial());
	 		      slipFuncionarioDTO.setPeriodoNotificaciones(slipRCFuncionariosForm.getPeriodoAdicionalNotificaciones());
	 		      slipFuncionarioDTO.setRetencion(slipRCFuncionariosForm.getRetencion());
	 		      slipFuncionarioDTO.setAccionLegal(slipRCFuncionariosForm.getAccionLegal());
	 		      slipFuncionarioDTO.setSubjetividades(slipRCFuncionariosForm.getSubjetividades());
	 		      slipFuncionarioDTO.setTerminoPrimas(slipRCFuncionariosForm.getTerminosPagoPrimas());
	 		      try {
					slipFuncionarioDTO.setFechaRetroactiva(UtileriasWeb.getFechaFromString(slipRCFuncionariosForm.getFechaRetroactiva()));
				} catch (ParseException e) {
			 		e.printStackTrace();
				}
			  }
 	}

	
 	
	private void poblarSlipContructorDTO(SlipConstructoresDTO slipConstructoresDTO, SlipRCConstructoresForm slipRCConstructoresForm){
		BigDecimal idToSlip = new BigDecimal(slipRCConstructoresForm.getIdToSlip());
		
		BigDecimal numeroInciso = new BigDecimal(slipRCConstructoresForm.getNumeroInciso());
		
		if (idToSlip != null){
			SlipConstructoresDTOId id = new SlipConstructoresDTOId();
			id.setIdToSlip(idToSlip);
			id.setNumeroInciso(numeroInciso);			
 			slipConstructoresDTO.setId(id);
		}
		if(slipRCConstructoresForm != null){
			       slipConstructoresDTO.setColindantes(slipRCConstructoresForm.getColindantes());
			  	   slipConstructoresDTO.setExperienciaLaboral(slipRCConstructoresForm.getExperienciaParaRealizarObra());
			       slipConstructoresDTO.setParaQuienRealizaLaObra(slipRCConstructoresForm.getParaquienRealizaObra());
			  	   slipConstructoresDTO.setSistemasPrevistos(slipRCConstructoresForm.getSistemasPreventivosContraDanos());
			    //
			   if (!StringUtil.isEmpty(slipRCConstructoresForm.getSitioTrabajo())){
				   slipConstructoresDTO.setSitioTrabajos(slipRCConstructoresForm.getSitioTrabajo());
			   }
			 	   slipConstructoresDTO.setTipoMaquinaria(slipRCConstructoresForm.getTipoMaquinaria());
			    //
			   if (!StringUtil.isEmpty(slipRCConstructoresForm.getValorEstimadoObra())){
				   slipConstructoresDTO.setValorEstimadoObra(Double.valueOf(slipRCConstructoresForm.getValorEstimadoObra()));
			   }
			}
	 }
	
	
	private void poblarSlipIncendioDTO(SlipIncendioDTO slipIncendioDTO,SlipIncedioForm slipIncedioForm ){
		BigDecimal idToSlip = new BigDecimal(slipIncedioForm.getIdToSlip());
		if (idToSlip != null){
 			slipIncendioDTO.setIdToSlip(idToSlip);
		} 
	 	if(slipIncedioForm  != null){
		   if (!StringUtil.isEmpty(slipIncedioForm.getProteccionContraIncendio())){
		      slipIncendioDTO.setProteccionesIndencios(slipIncedioForm.getProteccionContraIncendio());
		   }else{
			   slipIncendioDTO.setProteccionesIndencios(null);  
		   }
		}
		
	}
	
	
	private void poblarSlipAviacionDTO(SlipAviacionDTO slipAviacionDTO,SlipAviacionForm slipAviacionForm,BigDecimal idToSlip ){
	 	if (idToSlip != null){
			slipAviacionDTO.setIdToSlip(idToSlip);
		} 
	 	if(slipAviacionForm  != null){
		 	   slipAviacionDTO.setHorasMarcaTipo(slipAviacionForm.getHorasMarcayTipo());
		 	   slipAviacionDTO.setSubLimites(slipAviacionForm.getSubLimites());
		 	  if (!StringUtil.isEmpty(slipAviacionForm.getPagosVoluntarios())){
			       slipAviacionDTO.setPagosPasajeros(Double.valueOf(slipAviacionForm.getPagosVoluntarios()));
		      }else{
		    	  slipAviacionDTO.setPagosPasajeros(null);
		      }
	 		 
		
		}
 	}
 
	private void poblarSlipBarcoDTO(SlipBarcoDTO slipBarcoDTO,SlipBarcosForm slipBarcoForm,BigDecimal idToSlip ){
		if (idToSlip != null){
			slipBarcoDTO.setIdToSlip(idToSlip);
		} 
	 	if(slipBarcoForm!= null){
		   if (!StringUtil.isEmpty(slipBarcoForm.getLugardeConstruccion())){
			   slipBarcoDTO.setLugarConstruccion(slipBarcoForm.getLugardeConstruccion());
		   }
	 		  slipBarcoDTO.setMatricula(slipBarcoForm.getMatricula());
	  		  slipBarcoDTO.setSerie(slipBarcoForm.getSerie());
		    if (!StringUtil.isEmpty(slipBarcoForm.getUltimafechAmttoInspeccion())){
			   try {
				slipBarcoDTO.setUltimaFechaMtto(UtileriasWeb.getFechaFromString(slipBarcoForm.getUltimafechAmttoInspeccion()));
			} catch (ParseException e) {
		  		e.printStackTrace();
			}
		   }else{
			   slipBarcoDTO.setUltimaFechaMtto(null);
		   }
		}
	  
 	}
	
	
	private void poblarSlipObraCivilDTO(SlipObraCivilDTO slipObraCivilDTO,SlipObraCivilForm slipObraCivilForm ){
		BigDecimal idToSlip = new BigDecimal(slipObraCivilForm.getIdToSlip());
		if (idToSlip != null){
			slipObraCivilDTO.setIdToSlip(idToSlip);
		} 
	 	if(slipObraCivilForm != null){
	 		   slipObraCivilDTO.setAguasCercanas(slipObraCivilForm.getRioLagoMarEtcMasCercano());
	  		   slipObraCivilDTO.setBeneficiario(slipObraCivilForm.getBeneficiarioPreferente());
		  	   slipObraCivilDTO.setClausulaTerceros(slipObraCivilForm.getAnadirClausulaConsiderandoTercerosBienesPersonal());
		       slipObraCivilDTO.setCondicionesMetereologicas(slipObraCivilForm.getCondicionesMetereologicas());
		    //
		   if (!StringUtil.isEmpty(slipObraCivilForm.getCumpleRegulacionesSobreEstructurasResistentesaTerremotos())){
			   slipObraCivilDTO.setCumpleRegulacionesTerremotos(slipObraCivilForm.getCumpleRegulacionesSobreEstructurasResistentesaTerremotos());
		   }
	 		   slipObraCivilDTO.setDireccionContratista(slipObraCivilForm.getDireccionContratista());
	 	   //
		   if (!StringUtil.isEmpty(slipObraCivilForm.getEdificiosyEstructurasdeTercerosquePuedanSerAfectados())){
			   slipObraCivilDTO.setEdificiosTerceros(slipObraCivilForm.getEdificiosyEstructurasdeTercerosquePuedanSerAfectados());
		   }
		   //
		   if (!StringUtil.isEmpty(slipObraCivilForm.getExistenciaDeEstructurasAdyacentes())){
			   slipObraCivilDTO.setExistenEstructurasAdyacentes(slipObraCivilForm.getExistenciaDeEstructurasAdyacentes());
		   }
		   //
		   if (!StringUtil.isEmpty(slipObraCivilForm.getFechaTerminacionPeriodoMantenimiento())){
			   try {
				slipObraCivilDTO.setFechaTerminacionMtto(UtileriasWeb.getFechaFromString(slipObraCivilForm.getFechaTerminacionPeriodoMantenimiento()));
			} catch (ParseException e) {
	 			e.printStackTrace();
			}
		   }
	 		   slipObraCivilDTO.setIngenieroConsultor(slipObraCivilForm.getIngenieroConsultor());
	  		   slipObraCivilDTO.setMaquinaria(slipObraCivilForm.getMaquinariaEquiposFijosMoviles());
		    //
		   if (!StringUtil.isEmpty(slipObraCivilForm.getMetodosymaterialesdeconstruccion())){
			   slipObraCivilDTO.setMetodosYMaterialesConstruccion(slipObraCivilForm.getMetodosymaterialesdeconstruccion());
		   }
 			   slipObraCivilDTO.setNombreContratista(slipObraCivilForm.getNombreContratista());
 			   slipObraCivilDTO.setSumaAseguradas(slipObraCivilForm.getSumasAseguradasSublimitesAdyacentes());
 			   slipObraCivilDTO.setTituloContrato(slipObraCivilForm.getTitulodelContrato());
 		   //
		   if (!StringUtil.isEmpty(slipObraCivilForm.getTrabajosPorContrato())){
			   slipObraCivilDTO.setTrabajosPorContrato(slipObraCivilForm.getTrabajosPorContrato());
		   }
 			   
		   if (!StringUtil.isEmpty(slipObraCivilForm.getDireccionSubcontratista())){
			   slipObraCivilDTO.setDireccionSubContratista(slipObraCivilForm.getDireccionSubcontratista());
		   }
		   
		   if (!StringUtil.isEmpty(slipObraCivilForm.getNombreSubcontratista())){
			   slipObraCivilDTO.setNombreSubContratista(slipObraCivilForm.getNombreSubcontratista());
		   }		   
		   
		   if (!StringUtil.isEmpty(slipObraCivilForm.getTrabajosSubcontratista())){
			   slipObraCivilDTO.setTrabajoSubContratistas(slipObraCivilForm.getTrabajosSubcontratista());
		   }		   
		   
		   if (!StringUtil.isEmpty(slipObraCivilForm.getValorPorContrato())){
			   slipObraCivilDTO.setValorPorContrato(slipObraCivilForm.getValorPorContrato());
		   }		   

		   if (!StringUtil.isEmpty(slipObraCivilForm.getExperienciadelcontratista())){
			   slipObraCivilDTO.setExperienciaContratista(slipObraCivilForm.getExperienciadelcontratista());
		   }		   
 			   
 		}
		
	}
	
	private void poblarForm(SlipForm slipForm,SlipDTO slipDTO, HttpServletRequest request,SlipSoporteDTO slipSoporteDTO){
		if (slipDTO.getIdToSlip() != null)
			slipForm.setIdToSlip(slipDTO.getIdToSlip().toString());
		if (slipDTO.getIdToCotizacion() != null)
			slipForm.setIdToCotizacion(slipDTO.getIdToCotizacion().toString());
		if (slipDTO.getIdTcSubRamo() != null)
			slipForm.setIdTcSubRamo(slipDTO.getIdTcSubRamo().toString());
		if (slipDTO.getNumeroInciso() != null)
			slipForm.setNumeroInciso(slipDTO.getNumeroInciso().toString());
		if (slipDTO.getIdToSeccion() != null)
			slipForm.setIdToSeccion(slipDTO.getIdToSeccion().toString());
		if (slipDTO.getNumeroSubInciso() != null)
			slipForm.setNumeroSubInciso(slipDTO.getNumeroSubInciso().toString());
		if (slipDTO.getTipoDistribucion() != null)
			slipForm.setTipoDistribucion(slipDTO.getTipoDistribucion().toString());
		if (slipDTO.getEstatus() != null)
			slipForm.setEstatus(slipDTO.getEstatus().toString());
		if (slipDTO.getEstatusCotizacion() != null)
			slipForm.setEstatusCotizacion(slipDTO.getEstatusCotizacion().toString());
		slipForm.setSiniestrabilidad(slipDTO.getSiniestrabilidad());
		slipForm.setInformacionAdicional(slipDTO.getInformacionAdicional());
		slipForm.setTipoSlip(slipDTO.getTipoSlip());
		//Datos de la cotizacion
		slipForm.setCotizacionForm(new CotizacionForm());
//		CotizacionDTO cotizacionDTO = new CotizacionDTO();
//		try {
//			cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(slipDTO.getIdToCotizacion());
//			cotizacionDTO = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtenerDatosForaneos(cotizacionDTO);
//			
//		} catch (SystemException e) {
//			e.printStackTrace();
//		}
		poblarCotizacionForm(slipForm,slipSoporteDTO,slipSoporteDTO.getCotizacionDTO());
	}
	 
	private void poblarCotizacionForm(SlipForm slipForm,SlipSoporteDTO slipSoporteDTO,CotizacionDTO cotizacionDTO){
	    CotizacionForm cotizacionForm = new CotizacionForm();
	    if (slipSoporteDTO.getCotizacionDTO() != null && slipSoporteDTO.getCotizacionDTO().getIdToCotizacion() != null){
	    	String idToCotizacionFormateada = "COT" + UtileriasWeb.llenarIzquierda(slipSoporteDTO.getCotizacionDTO().getIdToCotizacion().toString(), "0", 8);
	    	cotizacionForm.setIdToCotizacionFormateada(idToCotizacionFormateada);
	    }    
	    if (slipSoporteDTO.getClienteDTO() != null){
	    	if(slipForm.getCliente() == null)
	    		slipForm.setCliente(new ClienteForm());
	    	ClienteAction.poblarClienteForm(slipSoporteDTO.getClienteDTO(), slipForm.getCliente());
	    	slipForm.setNombreAsegurado(cotizacionDTO.getNombreAsegurado());
	    }
	    //obtener fechas de vigencia 
	    Format formatter=new SimpleDateFormat("dd/MM/yyyy");
	    if ( cotizacionDTO.getFechaInicioVigencia() != null){
	    	cotizacionForm.setFechaInicioVigencia(formatter.format(cotizacionDTO.getFechaInicioVigencia()));
	    }
		if ( cotizacionDTO.getFechaFinVigencia() != null){
			cotizacionForm.setFechaFinVigencia(formatter.format(cotizacionDTO.getFechaFinVigencia()));
		}
  		slipForm.setCotizacionForm(cotizacionForm);
	}
	
	
	@SuppressWarnings("unused")
	private void poblarListaReaseguroCotizacion(SlipForm slipForm,SlipDTO slipDTO){
		try {
			switch(Integer.valueOf(slipForm.getTipoDistribucion())){
				case 1:
					this.generarListaReaseguroSlip(slipForm, slipDTO, null, null, false, false);
					break;
				case 2:
					this.generarListaReaseguroSlip(slipForm, slipDTO, generarReaseguroIncisoCotizacionId(slipDTO), null, true, false);
					break;
				case 3:
					this.generarListaReaseguroSlip(slipForm, slipDTO, generarReaseguroIncisoCotizacionId(slipDTO), generarReaseguroSubIncisoCotizacionId(slipDTO), false, true);
					break;
				case 4:
					List<ReaseguroCotizacionDTO> listaReaseguroCot = SlipDN.getInstancia().listarReaseguroCotizacionSlip(slipDTO);
					ReaseguroCotizacionForm reaseguroCotizacionForm = new ReaseguroCotizacionForm();
					//Recuperar el reaseguroCotizacion
					poblarReaseguroCotizacionForm(reaseguroCotizacionForm, (ReaseguroCotizacionDTO)listaReaseguroCot.get(0));
					//Se le asigna el ReaseguroCotizacion al SlipForm
					slipForm.setReaseguroCotizacion(reaseguroCotizacionForm);
					slipForm.setListaReaseguroIncisoCotizacion(null);
					break;
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	private ReaseguroSubIncisoCotizacionId generarReaseguroSubIncisoCotizacionId(SlipDTO slipDTO){
		ReaseguroSubIncisoCotizacionId idSubInciso = new ReaseguroSubIncisoCotizacionId();
		idSubInciso.setIdTcSubramo(slipDTO.getIdTcSubRamo());
		idSubInciso.setIdToCotizacion(slipDTO.getIdToCotizacion());
		idSubInciso.setIdToSeccion(slipDTO.getIdToSeccion());
		idSubInciso.setNumeroInciso(slipDTO.getNumeroInciso());
		idSubInciso.setNumeroSubinciso(slipDTO.getNumeroSubInciso());
		return idSubInciso;
	}
	
	private ReaseguroIncisoCotizacionId generarReaseguroIncisoCotizacionId(SlipDTO slipDTO){
		ReaseguroIncisoCotizacionId idInciso = new ReaseguroIncisoCotizacionId(); 
		idInciso = new ReaseguroIncisoCotizacionId(); 
		idInciso.setIdTcSubRamo(slipDTO.getIdTcSubRamo());
		idInciso.setIdToCotizacion(slipDTO.getIdToCotizacion());
		idInciso.setNumeroInciso(slipDTO.getNumeroInciso());
		return idInciso;
	}
	
	private void generarListaReaseguroSlip(SlipForm slipForm,SlipDTO slipDTO, ReaseguroIncisoCotizacionId idInciso, ReaseguroSubIncisoCotizacionId idSubInciso,boolean isIncisoEditable, boolean isSubIncisoEditable) throws ExcepcionDeAccesoADatos, SystemException{
		List<ReaseguroCotizacionDTO> listaReaseguroCot;
		List<ReaseguroIncisoCotizacionDTO> listaReaseguroIncisoCot;
		List<ReaseguroSubIncisoCotizacionDTO> listaSubIncisoReaseguroCot;
		List<ReaseguroIncisoCotizacionForm> listaReaseguroIncisoCotForm;
		List<ReaseguroSubIncisoCotizacionForm> listaSubIncisoReaseguroCotForm;
		ReaseguroCotizacionForm reaseguroCotizacionForm;
		ReaseguroIncisoCotizacionDTO reaseguroIncisoCotizacionDTO;
		ReaseguroIncisoCotizacionForm reaseguroIncisoCotizacionForm;
		ReaseguroSubIncisoCotizacionForm reaseguroSubIncisoForm;
		ReaseguroSubIncisoCotizacionDTO reaseguroSubIncisoDTO;
		//Se deben listar los datos del ReaseguroCotización, la lista de Incisos y todos sus subIncisos, sin permitir que sean editados
		listaReaseguroCot = SlipDN.getInstancia().listarReaseguroCotizacionSlip(slipDTO);
		reaseguroCotizacionForm = new ReaseguroCotizacionForm();
		//Recuperar el reaseguroCotizacion
		poblarReaseguroCotizacionForm(reaseguroCotizacionForm, (ReaseguroCotizacionDTO)listaReaseguroCot.get(0));
		//Se le asigna el ReaseguroCotizacion al SlipForm
		slipForm.setReaseguroCotizacion(reaseguroCotizacionForm);
		//Recuperar la lista de ReaseguroIncisoCot
		if (idInciso == null)
			listaReaseguroIncisoCot = SlipDN.getInstancia().listarReaseguroIncisoCotizacionSlip(slipDTO);
		else{
			reaseguroIncisoCotizacionDTO = ReaseguroIncisoCotizacionDN.getInstancia().getPorId(idInciso);
			listaReaseguroIncisoCot = new ArrayList<ReaseguroIncisoCotizacionDTO>();
			listaReaseguroIncisoCot.add(reaseguroIncisoCotizacionDTO);
		}
		listaReaseguroIncisoCotForm = new ArrayList<ReaseguroIncisoCotizacionForm>();
		//Iterar la lista para recuperar los subincisos
		for (ReaseguroIncisoCotizacionDTO reaseguroInciso : listaReaseguroIncisoCot){
			reaseguroIncisoCotizacionForm = new ReaseguroIncisoCotizacionForm();
			poblarReaseguroIncisoCotizacionForm(reaseguroIncisoCotizacionForm, reaseguroInciso);
			reaseguroIncisoCotizacionForm.setEditable(""+isIncisoEditable);
			//Por cada Inciso, se recupera la lista de subincisos
			if (idSubInciso == null)
				listaSubIncisoReaseguroCot = ReaseguroIncisoCotizacionDN.getInstancia().listarReaseguroSubIncisoCotizacion(reaseguroInciso);
			else{
				reaseguroSubIncisoDTO = ReaseguroSubIncisoCotizacionDN.getInstancia().getPorId(idSubInciso);
				listaSubIncisoReaseguroCot = new ArrayList<ReaseguroSubIncisoCotizacionDTO>();
				listaSubIncisoReaseguroCot.add(reaseguroSubIncisoDTO);
			}
			listaSubIncisoReaseguroCotForm = new ArrayList<ReaseguroSubIncisoCotizacionForm>();
			for(ReaseguroSubIncisoCotizacionDTO reaseguroSubInciso : listaSubIncisoReaseguroCot){
				reaseguroSubIncisoForm = new ReaseguroSubIncisoCotizacionForm();
				poblarReaseguroSubIncisoCotizacionForm(reaseguroSubIncisoForm, reaseguroSubInciso);
				reaseguroSubIncisoForm.setEditable(""+isSubIncisoEditable);
				listaSubIncisoReaseguroCotForm.add(reaseguroSubIncisoForm);
			}
			//Se le establece la lista de subincisos al inciso
			reaseguroIncisoCotizacionForm.setListaReaseguroSubIncisos(listaSubIncisoReaseguroCotForm);
			//Se agrega el inciso a la lista de incisos
			listaReaseguroIncisoCotForm.add(reaseguroIncisoCotizacionForm);
		}
		//Se le establece la lista de incisos al slipform
		slipForm.setListaReaseguroIncisoCotizacion(listaReaseguroIncisoCotForm);
		slipForm.setEditaIncisos(""+isIncisoEditable);
		slipForm.setEditaSubInisos(""+isSubIncisoEditable);
	}
	
	private void poblarDTO(SlipDTO slipDTO,SlipForm slipForm) throws SystemException{
		if (slipForm.getIdToSlip() != null && slipForm.getIdToSlip().trim().length() > 0)
			slipDTO.setIdToSlip(UtileriasWeb.regresaBigDecimal(slipForm.getIdToSlip()));
		if (slipForm.getIdToCotizacion() != null && slipForm.getIdToCotizacion().trim().length() > 0)
			slipDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(slipForm.getIdToCotizacion()));
		if (slipForm.getIdTcSubRamo() != null)
			slipDTO.setIdTcSubRamo(UtileriasWeb.regresaBigDecimal(slipForm.getIdTcSubRamo()));
		if (slipForm.getNumeroInciso() != null && slipForm.getNumeroInciso().trim().length() > 0)
			slipDTO.setNumeroInciso(UtileriasWeb.regresaBigDecimal(slipForm.getNumeroInciso()));
		if (slipForm.getIdToSeccion() != null && slipForm.getIdToSeccion().trim().length() > 0)
			slipDTO.setIdToSeccion(UtileriasWeb.regresaBigDecimal(slipForm.getIdToSeccion()));
		if (slipForm.getNumeroSubInciso() != null && slipForm.getNumeroSubInciso().trim().length() > 0)
			slipDTO.setNumeroSubInciso(UtileriasWeb.regresaBigDecimal(slipForm.getNumeroSubInciso()));
		if (!UtileriasWeb.esCadenaVacia(slipForm.getTipoDistribucion()))
			slipDTO.setTipoDistribucion(slipForm.getTipoDistribucion());
		if (!UtileriasWeb.esCadenaVacia(slipForm.getEstatus()))
			slipDTO.setEstatus(UtileriasWeb.regresaBigDecimal(slipForm.getEstatus()));
		if (!UtileriasWeb.esCadenaVacia(slipForm.getEstatusCotizacion()))
			slipDTO.setEstatusCotizacion(UtileriasWeb.regresaBigDecimal(slipForm.getEstatusCotizacion().toString()));
		if (!UtileriasWeb.esCadenaVacia(slipForm.getSiniestrabilidad()))
			slipDTO.setSiniestrabilidad(slipForm.getSiniestrabilidad());
		if (!UtileriasWeb.esCadenaVacia(slipForm.getInformacionAdicional()))
			slipDTO.setInformacionAdicional(slipForm.getInformacionAdicional());
		if (!UtileriasWeb.esCadenaVacia(slipForm.getTipoSlip()))
			slipDTO.setTipoSlip(slipForm.getTipoSlip());
	}
	
	private void poblarReaseguroCotizacionForm(ReaseguroCotizacionForm reaseguroForm, ReaseguroCotizacionDTO reaseguroDTO){
		if (reaseguroDTO != null && reaseguroForm != null){
			if (reaseguroDTO.getId()!=null){
				if (reaseguroDTO.getId().getIdTcSubRamo()!=null)
					reaseguroForm.setIdTcSubRamo(reaseguroDTO.getId().getIdTcSubRamo().toString());
				if (reaseguroDTO.getId().getIdToCotizacion()!=null)
					reaseguroForm.setIdToCotizacion(reaseguroDTO.getId().getIdToCotizacion().toString());
			}
			if (reaseguroDTO.getValorSumaAsegurada() != null)
				reaseguroForm.setValorSumaAsegurada(reaseguroDTO.getValorSumaAsegurada().toString());
			if (reaseguroDTO.getValorPrimaNeta() != null)
				reaseguroForm.setValorPrimaNeta(reaseguroDTO.getValorPrimaNeta().toString());
			if (reaseguroDTO.getClaveTipoOrigen() != null)
				reaseguroForm.setClaveTipoOrigen(reaseguroDTO.getClaveTipoOrigen().toString());
			if (reaseguroDTO.getClaveEstatus() != null)
				reaseguroForm.setClaveEstatus(reaseguroDTO.getClaveEstatus().toString());
		}
	}
	
	private void poblarReaseguroIncisoCotizacionForm(ReaseguroIncisoCotizacionForm reaseguroIncForm, ReaseguroIncisoCotizacionDTO reaseguroIncDTO){
		if (reaseguroIncDTO != null && reaseguroIncForm != null){
			if (reaseguroIncDTO.getId()!=null){
				if (reaseguroIncDTO.getId().getIdTcSubRamo()!=null)
					reaseguroIncForm.setIdTcSubRamo(reaseguroIncDTO.getId().getIdTcSubRamo().toString());
				if (reaseguroIncDTO.getId().getIdToCotizacion()!=null)
					reaseguroIncForm.setIdToCotizacion(reaseguroIncDTO.getId().getIdToCotizacion().toString());
				if (reaseguroIncDTO.getId().getNumeroInciso()!=null)
					reaseguroIncForm.setNumeroInciso(reaseguroIncDTO.getId().getNumeroInciso().toString());
			}
			if (reaseguroIncDTO.getValorSumaAsegurada() != null)
				reaseguroIncForm.setValorSumaAsegurada(reaseguroIncDTO.getValorSumaAsegurada().toString());
			if (reaseguroIncDTO.getValorPrimaNeta() != null)
				reaseguroIncForm.setValorPrimaNeta(reaseguroIncDTO.getValorPrimaNeta().toString());
			if (reaseguroIncDTO.getClaveTipoOrigen() != null)
				reaseguroIncForm.setClaveTipoOrigen(reaseguroIncDTO.getClaveTipoOrigen().toString());
			if (reaseguroIncDTO.getClaveEstatus() != null)
				reaseguroIncForm.setClaveEstatus(reaseguroIncDTO.getClaveEstatus().toString());
		}
	}
	
	private void poblarReaseguroSubIncisoCotizacionForm(ReaseguroSubIncisoCotizacionForm reaseguroSubIncForm, ReaseguroSubIncisoCotizacionDTO reaseguroSubIncDTO){
		if (reaseguroSubIncDTO != null && reaseguroSubIncForm != null){
			if (reaseguroSubIncDTO.getId()!=null){
				if (reaseguroSubIncDTO.getId().getIdTcSubramo()!=null)
					reaseguroSubIncForm.setIdTcSubRamo(reaseguroSubIncDTO.getId().getIdTcSubramo().toString());
				if (reaseguroSubIncDTO.getId().getIdToCotizacion()!=null)
					reaseguroSubIncForm.setIdToCotizacion(reaseguroSubIncDTO.getId().getIdToCotizacion().toString());
				if (reaseguroSubIncDTO.getId().getNumeroInciso()!=null)
					reaseguroSubIncForm.setNumeroInciso(reaseguroSubIncDTO.getId().getNumeroInciso().toString());
				if (reaseguroSubIncDTO.getId().getNumeroSubinciso()!=null)
					reaseguroSubIncForm.setNumeroSubInciso(reaseguroSubIncDTO.getId().getNumeroSubinciso().toString());
			}
			if (reaseguroSubIncDTO.getValorSumaAsegurada() != null)
				reaseguroSubIncForm.setValorSumaAsegurada(reaseguroSubIncDTO.getValorSumaAsegurada().toString());
			if (reaseguroSubIncDTO.getValorPrimaNeta() != null)
				reaseguroSubIncForm.setValorPrimaNeta(reaseguroSubIncDTO.getValorPrimaNeta().toString());
			if (reaseguroSubIncDTO.getClaveTipoOrigen() != null)
				reaseguroSubIncForm.setClaveTipoOrigen(reaseguroSubIncDTO.getClaveTipoOrigen().toString());
			if (reaseguroSubIncDTO.getClaveEstatus() != null)
				reaseguroSubIncForm.setClaveEstatus(reaseguroSubIncDTO.getClaveEstatus().toString());
		}
	}
	public ActionForward validarTipoSlip(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
		 	String idToSlip = (String) request.getSession().getAttribute("idToSlip");
	 	 	SlipDTO slipDTO = SlipDN.getInstancia().getPorId(new BigDecimal(idToSlip));
  		    if(slipDTO.getTipoSlip().equals("2")){	
  		    	reglaNavegacion = "barco";
  		    }
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	public void editarSlipIncisoGrid(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		try {
			MidasJsonBase json = new MidasJsonBase();
			String idToSlip = request.getParameter("idToSlip");
			//String slipGeneral = request.getParameter("slipGeneral");
			//SlipSoporteDTO slipSoporteDTO = SlipSoporteDN.getInstancia().obtenerSlipSoporte(new BigDecimal(idToSlip));
			
			SlipSoporteDTO slipSoporteDTO = (SlipSoporteDTO)request.getSession().getAttribute("slipSoporte");
			
			if(slipSoporteDTO != null){
				String path = crearPathSlipSoporte(slipSoporteDTO);
				List<DetalleSoporteDTO> detalleSoporteDTO = slipSoporteDTO.getListaDetalle();
				if(detalleSoporteDTO != null){
					for(int i = 0; i < detalleSoporteDTO.size(); i++){
						MidasJsonRow row = new MidasJsonRow();
						DetalleSoporteDTO dsDTO = detalleSoporteDTO.get(i);
						row.setId(dsDTO.getIdentificador());
						row.setDatos(dsDTO.getIdentificador()+"",
									 dsDTO.getDescripcion()+"",
									 MidasJsonRow.generarLineaImagenDataGrid("/MidasWeb/img/details.gif", "Ver Detalle", "document.location.href='/MidasWeb/contratofacultativo/slip/mostrarEditar"+path+"?inciso="+dsDTO.getIdentificador()+"&idToSlip="+idToSlip+"&slipGeneral=1"+"';", null));
									 //MidasJsonRow.generarLineaImagenDataGrid("/MidasWeb/img/details.gif", "Ver Detalle", "sendRequest(slipForm,'/MidasWeb/contratofacultativo/slip/mostrarEditar"+path+"?inciso="+dsDTO.getIdentificador()+"&idToSlip="+idToSlip+"&slipGeneral=1"+"','configuracion_detalle','mostrarGeneralGrids()');", null));
						
						/*if(dsDTO.getNumeroInciso() != null)
							row.setDatos(dsDTO.getNumeroInciso()+"", dsDTO.getDescripcion(),MidasJsonRow.generarLineaImagenDataGrid("/MidasWeb/img/details.gif", "Ver Detalle", "sendRequest(slipForm,'/MidasWeb/contratofacultativo/slip/mostrarEditar"+path+"?id="+dsDTO.getIdentificador()+"','configuracion_detalle','mostrarGeneralGrids()');", null));
						else if(dsDTO.getNumeroSubInciso() != null)
							row.setDatos(dsDTO.getNumeroSubInciso()+"", dsDTO.getDescripcion(),MidasJsonRow.generarLineaImagenDataGrid("/MidasWeb/img/details.gif", "Ver Detalle", "sendRequest(slipForm,'/MidasWeb/contratofacultativo/slip/mostrarEditar"+path+"?id="+dsDTO.getIdentificador()+"','configuracion_detalle','mostrarGeneralGrids()');", null));
							*/
						json.addRow(row);
					}
				}
				
			}
			
			response.setContentType("text/json");
			PrintWriter pw;
			pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (IOException exc) {
			LogDeMidasWeb.log(exc.getMessage(), Level.FINEST, exc);
		} catch (ExcepcionDeAccesoADatos exc) {
			LogDeMidasWeb.log(exc.getMessage(), Level.FINEST, exc);
		} 
	}
	
	public String crearPathSlipSoporte(SlipSoporteDTO ssDTO){
		String aux = "";
		if(ssDTO.getSlipAviacion() != null)
			aux = "SlipAviacion.do";
		else if(ssDTO.getSlipBarcos() != null)
			aux = "SlipBarco.do";
		else if(ssDTO.getSlipEquipoConstratista() != null)
			aux = "SlipEquipoContratista.do";
		else if(ssDTO.getSlipIncendioSoporte() != null)
			aux = "SlipIncendio.do";
		else if(ssDTO.getSlipObraCivilSoporte() != null)
			aux = "SlipObraCivil.do";
		else if(ssDTO.getSlipRCConstructoresSoporte() != null)
			aux = "SlipRCConstructores.do";
		else if(ssDTO.getSlipRCFuncionarioSoporte() != null)
			aux = "SlipRCFuncionarios.do";
		else if(ssDTO.getSlipTransportesSoporte() != null)
			aux = "SlipTransportesDeCarga.do";
		else if(ssDTO.getSlipGeneralSoporte() != null)
			aux = "SlipGeneral.do";
		else if(ssDTO.getSlipPrimerRiesgo() != null) // Primer Riesgo
			aux = "SlipGeneral.do";		
		return aux;
	}
	
	private BigDecimal obtenerSumaAsegurada(List<CoberturaSoporteDTO> coberturas, String desc){
		BigDecimal sumaAsegurada = BigDecimal.ZERO;
		
		if(coberturas != null){
			for(CoberturaSoporteDTO co : coberturas){
				if(co.getDescripcionCobertura().indexOf(desc) >= 0){
					sumaAsegurada = co.getSumaAsegurada();
				} 
			}
		}
		
		return sumaAsegurada;
	}	
}
