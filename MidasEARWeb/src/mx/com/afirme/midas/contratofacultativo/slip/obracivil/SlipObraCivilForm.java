/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.obracivil;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.UtileriasWeb;

/**
 * @author jmartinez
 *
 */
public class SlipObraCivilForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2374753367806815190L;
    private String idToSlip;
	private String numeroInciso;
	private String direccionInciso;
	private String dumaAsegurada;
	private String tipoObraCaracteristicasObra; 
	private String titulodelContrato;
	private String metodosymaterialesdeconstruccion; 
	private String ingenieroConsultor;
	private String cumpleRegulacionesSobreEstructurasResistentesaTerremotos;
	private String nombreContratista;
	private String tienePolizaSeparadadeRC;
	private String direccionContratista;
	private String existenciaDeEstructurasAdyacentes;
	private String nombreSubcontratista;
	private String fechaTerminacionPeriodoMantenimiento;
	private String direccionSubcontratista;
	private String edificiosyEstructurasdeTercerosquePuedanSerAfectados;
	private String experienciadelcontratista;
	private String rioLagoMarEtcMasCercano;
	private String trabajosSubcontratista;
	private String sumasAseguradasSublimitesAdyacentes;
	private String condicionesMetereologicas;
	private String anadirClausulaConsiderandoTercerosBienesPersonal;
	private String trabajosPorContrato;
	private String valorPorContrato; 
	private String maquinariaEquiposFijosMoviles;
	private String beneficiarioPreferente;
	private String coberturas;
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the direccionInciso
	 */
	public String getDireccionInciso() {
		return direccionInciso;
	}
	/**
	 * @param direccionInciso the direccionInciso to set
	 */
	public void setDireccionInciso(String direccionInciso) {
		this.direccionInciso = direccionInciso;
	}
	/**
	 * @return the dumaAsegurada
	 */
	public String getDumaAsegurada() {
		return dumaAsegurada;
	}
	/**
	 * @param dumaAsegurada the dumaAsegurada to set
	 */
	public void setDumaAsegurada(String dumaAsegurada) {
		this.dumaAsegurada = dumaAsegurada;
	}
	/**
	 * @return the tipoObraCaracteristicasObra
	 */
	public String getTipoObraCaracteristicasObra() {
		return tipoObraCaracteristicasObra;
	}
	/**
	 * @param tipoObraCaracteristicasObra the tipoObraCaracteristicasObra to set
	 */
	public void setTipoObraCaracteristicasObra(String tipoObraCaracteristicasObra) {
		this.tipoObraCaracteristicasObra = tipoObraCaracteristicasObra;
	}
	/**
	 * @return the titulodelContrato
	 */
	public String getTitulodelContrato() {
		return titulodelContrato;
	}
	/**
	 * @param titulodelContrato the titulodelContrato to set
	 */
	public void setTitulodelContrato(String titulodelContrato) {
		this.titulodelContrato = titulodelContrato;
	}
	/**
	 * @return the metodosymaterialesdeconstruccion
	 */
	public String getMetodosymaterialesdeconstruccion() {
		return metodosymaterialesdeconstruccion;
	}
	/**
	 * @param metodosymaterialesdeconstruccion the metodosymaterialesdeconstruccion to set
	 */
	public void setMetodosymaterialesdeconstruccion(
			String metodosymaterialesdeconstruccion) {
		this.metodosymaterialesdeconstruccion = metodosymaterialesdeconstruccion;
	}
	/**
	 * @return the ingenieroConsultor
	 */
	public String getIngenieroConsultor() {
		return ingenieroConsultor;
	}
	/**
	 * @param ingenieroConsultor the ingenieroConsultor to set
	 */
	public void setIngenieroConsultor(String ingenieroConsultor) {
		this.ingenieroConsultor = ingenieroConsultor;
	}
	/**
	 * @return the cumpleRegulacionesSobreEstructurasResistentesaTerremotos
	 */
	public String getCumpleRegulacionesSobreEstructurasResistentesaTerremotos() {
		return cumpleRegulacionesSobreEstructurasResistentesaTerremotos;
	}
	/**
	 * @param cumpleRegulacionesSobreEstructurasResistentesaTerremotos the cumpleRegulacionesSobreEstructurasResistentesaTerremotos to set
	 */
	public void setCumpleRegulacionesSobreEstructurasResistentesaTerremotos(
			String cumpleRegulacionesSobreEstructurasResistentesaTerremotos) {
		this.cumpleRegulacionesSobreEstructurasResistentesaTerremotos = cumpleRegulacionesSobreEstructurasResistentesaTerremotos;
	}
	/**
	 * @return the nombreContratista
	 */
	public String getNombreContratista() {
		return nombreContratista;
	}
	/**
	 * @param nombreContratista the nombreContratista to set
	 */
	public void setNombreContratista(String nombreContratista) {
		this.nombreContratista = nombreContratista;
	}
	/**
	 * @return the tienePolizaSeparadadeRC
	 */
	public String getTienePolizaSeparadadeRC() {
		return tienePolizaSeparadadeRC;
	}
	/**
	 * @param tienePolizaSeparadadeRC the tienePolizaSeparadadeRC to set
	 */
	public void setTienePolizaSeparadadeRC(String tienePolizaSeparadadeRC) {
		this.tienePolizaSeparadadeRC = tienePolizaSeparadadeRC;
	}
	/**
	 * @return the direccionContratista
	 */
	public String getDireccionContratista() {
		return direccionContratista;
	}
	/**
	 * @param direccionContratista the direccionContratista to set
	 */
	public void setDireccionContratista(String direccionContratista) {
		this.direccionContratista = direccionContratista;
	}
	/**
	 * @return the existenciaDeEstructurasAdyacentes
	 */
	public String getExistenciaDeEstructurasAdyacentes() {
		return existenciaDeEstructurasAdyacentes;
	}
	/**
	 * @param existenciaDeEstructurasAdyacentes the existenciaDeEstructurasAdyacentes to set
	 */
	public void setExistenciaDeEstructurasAdyacentes(
			String existenciaDeEstructurasAdyacentes) {
		this.existenciaDeEstructurasAdyacentes = existenciaDeEstructurasAdyacentes;
	}
	/**
	 * @return the nombreSubcontratista
	 */
	public String getNombreSubcontratista() {
		return nombreSubcontratista;
	}
	/**
	 * @param nombreSubcontratista the nombreSubcontratista to set
	 */
	public void setNombreSubcontratista(String nombreSubcontratista) {
		this.nombreSubcontratista = nombreSubcontratista;
	}
	/**
	 * @return the fechaTerminacionPeriodoMantenimiento
	 */
	public String getFechaTerminacionPeriodoMantenimiento() {
		return fechaTerminacionPeriodoMantenimiento;
	}
	/**
	 * @param fechaTerminacionPeriodoMantenimiento the fechaTerminacionPeriodoMantenimiento to set
	 */
	public void setFechaTerminacionPeriodoMantenimiento(
			String fechaTerminacionPeriodoMantenimiento) {
		this.fechaTerminacionPeriodoMantenimiento = fechaTerminacionPeriodoMantenimiento;
	}
	/**
	 * @return the direccionSubcontratista
	 */
	public String getDireccionSubcontratista() {
		return direccionSubcontratista;
	}
	/**
	 * @param direccionSubcontratista the direccionSubcontratista to set
	 */
	public void setDireccionSubcontratista(String direccionSubcontratista) {
		this.direccionSubcontratista = direccionSubcontratista;
	}
	/**
	 * @return the edificiosyEstructurasdeTercerosquePuedanSerAfectados
	 */
	public String getEdificiosyEstructurasdeTercerosquePuedanSerAfectados() {
		return edificiosyEstructurasdeTercerosquePuedanSerAfectados;
	}
	/**
	 * @param edificiosyEstructurasdeTercerosquePuedanSerAfectados the edificiosyEstructurasdeTercerosquePuedanSerAfectados to set
	 */
	public void setEdificiosyEstructurasdeTercerosquePuedanSerAfectados(
			String edificiosyEstructurasdeTercerosquePuedanSerAfectados) {
		this.edificiosyEstructurasdeTercerosquePuedanSerAfectados = edificiosyEstructurasdeTercerosquePuedanSerAfectados;
	}
	/**
	 * @return the experienciadelcontratista
	 */
	public String getExperienciadelcontratista() {
		return experienciadelcontratista;
	}
	/**
	 * @param experienciadelcontratista the experienciadelcontratista to set
	 */
	public void setExperienciadelcontratista(String experienciadelcontratista) {
		this.experienciadelcontratista = experienciadelcontratista;
	}
	/**
	 * @return the rioLagoMarEtcMasCercano
	 */
	public String getRioLagoMarEtcMasCercano() {
		return rioLagoMarEtcMasCercano;
	}
	/**
	 * @param rioLagoMarEtcMasCercano the rioLagoMarEtcMasCercano to set
	 */
	public void setRioLagoMarEtcMasCercano(String rioLagoMarEtcMasCercano) {
		this.rioLagoMarEtcMasCercano = rioLagoMarEtcMasCercano;
	}
	/**
	 * @return the trabajosSubcontratista
	 */
	public String getTrabajosSubcontratista() {
		return trabajosSubcontratista;
	}
	/**
	 * @param trabajosSubcontratista the trabajosSubcontratista to set
	 */
	public void setTrabajosSubcontratista(String trabajosSubcontratista) {
		this.trabajosSubcontratista = trabajosSubcontratista;
	}
	/**
	 * @return the sumasAseguradasSublimitesAdyacentes
	 */
	public String getSumasAseguradasSublimitesAdyacentes() {
		return sumasAseguradasSublimitesAdyacentes;
	}
	/**
	 * @param sumasAseguradasSublimitesAdyacentes the sumasAseguradasSublimitesAdyacentes to set
	 */
	public void setSumasAseguradasSublimitesAdyacentes(
			String sumasAseguradasSublimitesAdyacentes) {
		this.sumasAseguradasSublimitesAdyacentes = sumasAseguradasSublimitesAdyacentes;
	}
	/**
	 * @return the condicionesMetereologicas
	 */
	public String getCondicionesMetereologicas() {
		return condicionesMetereologicas;
	}
	/**
	 * @param condicionesMetereologicas the condicionesMetereologicas to set
	 */
	public void setCondicionesMetereologicas(String condicionesMetereologicas) {
		this.condicionesMetereologicas = condicionesMetereologicas;
	}
	/**
	 * @return the anadirClausulaConsiderandoTercerosBienesPersonal
	 */
	public String getAnadirClausulaConsiderandoTercerosBienesPersonal() {
		return anadirClausulaConsiderandoTercerosBienesPersonal;
	}
	/**
	 * @param anadirClausulaConsiderandoTercerosBienesPersonal the anadirClausulaConsiderandoTercerosBienesPersonal to set
	 */
	public void setAnadirClausulaConsiderandoTercerosBienesPersonal(
			String anadirClausulaConsiderandoTercerosBienesPersonal) {
		this.anadirClausulaConsiderandoTercerosBienesPersonal = anadirClausulaConsiderandoTercerosBienesPersonal;
	}
	/**
	 * @return the trabajosPorContrato
	 */
	public String getTrabajosPorContrato() {
		return trabajosPorContrato;
	}
	/**
	 * @param trabajosPorContrato the trabajosPorContrato to set
	 */
	public void setTrabajosPorContrato(String trabajosPorContrato) {
		this.trabajosPorContrato = trabajosPorContrato;
	}
	/**
	 * @return the valorPorContrato
	 */
	public String getValorPorContrato() {
		return valorPorContrato;
	}
	/**
	 * @param valorPorContrato the valorPorContrato to set
	 */
	public void setValorPorContrato(String valorPorContrato) {
		this.valorPorContrato = valorPorContrato;
	}
	/**
	 * @return the maquinariaEquiposFijosMoviles
	 */
	public String getMaquinariaEquiposFijosMoviles() {
		return maquinariaEquiposFijosMoviles;
	}
	/**
	 * @param maquinariaEquiposFijosMoviles the maquinariaEquiposFijosMoviles to set
	 */
	public void setMaquinariaEquiposFijosMoviles(
			String maquinariaEquiposFijosMoviles) {
		this.maquinariaEquiposFijosMoviles = maquinariaEquiposFijosMoviles;
	}
	/**
	 * @return the beneficiarioPreferente
	 */
	public String getBeneficiarioPreferente() {
		return beneficiarioPreferente;
	}
	/**
	 * @param beneficiarioPreferente the beneficiarioPreferente to set
	 */
	public void setBeneficiarioPreferente(String beneficiarioPreferente) {
		this.beneficiarioPreferente = beneficiarioPreferente;
	}
	/**
	 * @return the coberturas
	 */
	public String getCoberturas() {
		return coberturas;
	}
	/**
	 * @param coberturas the coberturas to set
	 */
	public void setCoberturas(String coberturas) {
		this.coberturas = coberturas;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	
	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
			if (UtileriasWeb.esCadenaVacia(edificiosyEstructurasdeTercerosquePuedanSerAfectados)
					 || UtileriasWeb.esCadenaVacia(this.existenciaDeEstructurasAdyacentes) 
					 || UtileriasWeb.esCadenaVacia(this.trabajosPorContrato)
					 || UtileriasWeb.esCadenaVacia(this.fechaTerminacionPeriodoMantenimiento)){
				request.setAttribute("coberturas", 1);
			}
 
	 	return errors;
	}
	
	
	
}
