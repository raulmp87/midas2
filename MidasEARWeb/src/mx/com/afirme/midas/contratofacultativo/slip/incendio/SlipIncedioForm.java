/**
 * 
 */
package mx.com.afirme.midas.contratofacultativo.slip.incendio;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class SlipIncedioForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1808386911962771792L;
   private String idToSlip;
	private String numeroInciso;
	private String sumaAseguradaTotal;
	private String girodelAsegurado;
	private String zonaTerremoto;
	private String direccionInciso;
	private String tipoConstruccion;
	private String sumaAseguradaInciso;
	private String proteccionContraIncendio;
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the sumaAseguradaTotal
	 */
	public String getSumaAseguradaTotal() {
		return sumaAseguradaTotal;
	}
	/**
	 * @param sumaAseguradaTotal the sumaAseguradaTotal to set
	 */
	public void setSumaAseguradaTotal(String sumaAseguradaTotal) {
		this.sumaAseguradaTotal = sumaAseguradaTotal;
	}
	/**
	 * @return the girodelAsegurado
	 */
	public String getGirodelAsegurado() {
		return girodelAsegurado;
	}
	/**
	 * @param girodelAsegurado the girodelAsegurado to set
	 */
	public void setGirodelAsegurado(String girodelAsegurado) {
		this.girodelAsegurado = girodelAsegurado;
	}
	/**
	 * @return the zonaTerremoto
	 */
	public String getZonaTerremoto() {
		return zonaTerremoto;
	}
	/**
	 * @param zonaTerremoto the zonaTerremoto to set
	 */
	public void setZonaTerremoto(String zonaTerremoto) {
		this.zonaTerremoto = zonaTerremoto;
	}
	/**
	 * @return the direccionInciso
	 */
	public String getDireccionInciso() {
		return direccionInciso;
	}
	/**
	 * @param direccionInciso the direccionInciso to set
	 */
	public void setDireccionInciso(String direccionInciso) {
		this.direccionInciso = direccionInciso;
	}
	/**
	 * @return the tipoConstruccion
	 */
	public String getTipoConstruccion() {
		return tipoConstruccion;
	}
	/**
	 * @param tipoConstruccion the tipoConstruccion to set
	 */
	public void setTipoConstruccion(String tipoConstruccion) {
		this.tipoConstruccion = tipoConstruccion;
	}
	/**
	 * @return the sumaAseguradaInciso
	 */
	public String getSumaAseguradaInciso() {
		return sumaAseguradaInciso;
	}
	/**
	 * @param sumaAseguradaInciso the sumaAseguradaInciso to set
	 */
	public void setSumaAseguradaInciso(String sumaAseguradaInciso) {
		this.sumaAseguradaInciso = sumaAseguradaInciso;
	}
	/**
	 * @return the proteccionContraIncendio
	 */
	public String getProteccionContraIncendio() {
		return proteccionContraIncendio;
	}
	/**
	 * @param proteccionContraIncendio the proteccionContraIncendio to set
	 */
	public void setProteccionContraIncendio(String proteccionContraIncendio) {
		this.proteccionContraIncendio = proteccionContraIncendio;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
		
}
