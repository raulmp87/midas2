package mx.com.afirme.midas.contratofacultativo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class DetalleContratoFacultativoDN {

	
	private static final DetalleContratoFacultativoDN INSTANCIA = new DetalleContratoFacultativoDN(); 
	
	  public static DetalleContratoFacultativoDN getInstancia(){
		     return DetalleContratoFacultativoDN.INSTANCIA;
	  }
	  
	  
	  public void agregar(DetalleContratoFacultativoDTO DetalleContratoFacultativoDTO) throws SystemException,
		           ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		  			DetalleContratoFacultativoSN DetalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		  			DetalleContratoFacultativoSN.agregar(DetalleContratoFacultativoDTO);
	  		}
	  
	  public void eliminar(DetalleContratoFacultativoDTO DetalleContratoFacultativoDTO) throws SystemException,
      ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
			DetalleContratoFacultativoSN DetalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
			DetalleContratoFacultativoSN.eliminar(DetalleContratoFacultativoDTO);
	}
	  
	  public DetalleContratoFacultativoDTO getCotizacionFacultativa(BigDecimal idTmDetalleContratoFacultativo) throws SystemException, ExcepcionDeAccesoADatos {
			return new DetalleContratoFacultativoSN().getPorId(idTmDetalleContratoFacultativo);
		}
	   
	  public List<DetalleContratoFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
			return new DetalleContratoFacultativoSN().buscarPorPropiedad(propiedad, valor);
		}
	  
	  public List<DetalleContratoFacultativoDTO> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		  DetalleContratoFacultativoSN  DetalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
			return DetalleContratoFacultativoSN.listarTodos();
		}
 
	  public DetalleContratoFacultativoDTO obtenerDetalleContratoFacultativo(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException,ExcepcionDeAccesoADatos {
		  DetalleContratoFacultativoSN detalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		  return detalleContratoFacultativoSN.obtenerDetalleContratoFacultativo(detalleContratoFacultativoDTO);
	  }
	  
	  public boolean validarDetalleParticipacionPorContrato(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException{
		  DetalleContratoFacultativoSN detalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		  return detalleContratoFacultativoSN.validarDetalleParticipacionPorContrato(detalleContratoFacultativoDTO);
	  }
	  
	  public boolean validarDetalleParticipacion(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException{
		  DetalleContratoFacultativoSN detalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		  return detalleContratoFacultativoSN.validarDetalleParticipacion(detalleContratoFacultativoDTO);
	  }
	
	  public List<DetalleContratoFacultativoDTO> aplicarConfiguracionATodasLasCoberturas(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException{
		  DetalleContratoFacultativoSN detalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		  return detalleContratoFacultativoSN.aplicarConfiguracionATodasLasCoberturas(detalleContratoFacultativoDTO);
	  }
	  
	  public void modificar(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException{
		  DetalleContratoFacultativoSN detalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		  detalleContratoFacultativoSN.modificar(detalleContratoFacultativoDTO);
	  }
	  
	  //TODO habilitar este método para que busque el detalle del contrato anterior correspondiente al detalle recibido.
	  public DetalleContratoFacultativoDTO obtenerDetalleContratoFacultativoAnterior(DetalleContratoFacultativoDTO detalleContratoFacultativoActual){
		  DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = null;
		  if(detalleContratoFacultativoActual != null && detalleContratoFacultativoActual.getContratoFacultativoDTO_1() != null){
			  
			  detalleContratoFacultativoDTO = detalleContratoFacultativoActual;
		  }
		  return detalleContratoFacultativoDTO; 
	  }
	  
	  public List<DetalleContratoFacultativoDTO> listarFiltrado(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException{
		  return new DetalleContratoFacultativoSN().listarFiltrado(detalleContratoFacultativoDTO);
	  }
	  
	  public List<DetalleContratoFacultativoDTO> listarDetalleContratoFiltrado(BigDecimal idTmContratoFacultativo,BigDecimal idTcSubramo,BigDecimal idToCobertura,
			  BigDecimal numeroInciso,BigDecimal numeroSubInciso,BigDecimal idToSeccion) throws SystemException{
		  ContratoFacultativoDTO contrato = new ContratoFacultativoDTO();
		  contrato.setIdTmContratoFacultativo(idTmContratoFacultativo);
		  DetalleContratoFacultativoDTO detalle = new DetalleContratoFacultativoDTO(null,contrato,null,idTcSubramo,idToCobertura);
		  detalle.setNumeroInciso(numeroInciso);
		  detalle.setNumeroSubInciso(numeroSubInciso);
		  detalle.setIdToSeccion(idToSeccion);
		  return new DetalleContratoFacultativoSN().listarFiltrado(detalle);
	  }
}
