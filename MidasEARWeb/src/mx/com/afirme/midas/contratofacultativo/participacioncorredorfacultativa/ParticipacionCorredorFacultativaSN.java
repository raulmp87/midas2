package mx.com.afirme.midas.contratofacultativo.participacioncorredorfacultativa;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ParticipacionCorredorFacultativaSN {

	private ParticipacionCorredorFacultativoFacadeRemote beanRemoto;

	public ParticipacionCorredorFacultativaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ParticipacionCorredorSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ParticipacionCorredorFacultativoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ParticipacionCorredorFacultativoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ParticipacionCorredorFacultativoDTO> participacionesCorredorFacultativo = beanRemoto.findAll();
		return participacionesCorredorFacultativo;

	} 
 	
	public List<ParticipacionCorredorFacultativoDTO> getPorPropiedad(String propiedad, Object valor)
	       throws ExcepcionDeAccesoADatos {
	  return beanRemoto.findByProperty(propiedad, valor);
}
	
	
	public void agregar(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
    try{
    	beanRemoto.save(participacionCorredorFacultativoDTO);
    } catch(Exception e){
	   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
    } 
   }
	

	public void borrar(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO)throws SystemException, ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(participacionCorredorFacultativoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ParticipacionCorredorFacultativoDTO getPorId(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(participacionCorredorFacultativoDTO.getIdTdParticapacionCorredorFac());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ParticipacionCorredorFacultativoDTO modificar(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(participacionCorredorFacultativoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
	
	
	
}
