package mx.com.afirme.midas.contratofacultativo.participacioncorredorfacultativa;

public class ParticipacionCorredorFacultativaForm {

	private String idTdParticipacionCorredorFac;
	private String idTdParticipacionFacultativo;
	private String idTcReaseguradorCorredor;
	private String porcentajeParticipacion;
	
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public String getIdTdParticipacionCorredorFac() {
		return idTdParticipacionCorredorFac;
	}
	public void setIdTdParticipacionCorredorFac(String idTdParticipacionCorredorFac) {
		this.idTdParticipacionCorredorFac = idTdParticipacionCorredorFac;
	}
	public String getIdTdParticipacionFacultativo() {
		return idTdParticipacionFacultativo;
	}
	public void setIdTdParticipacionFacultativo(String idTdParticipacionFacultativo) {
		this.idTdParticipacionFacultativo = idTdParticipacionFacultativo;
	}
	public String getIdTcReaseguradorCorredor() {
		return idTcReaseguradorCorredor;
	}
	public void setIdTcReaseguradorCorredor(String idTcReaseguradorCorredor) {
		this.idTcReaseguradorCorredor = idTcReaseguradorCorredor;
	}
	
}
