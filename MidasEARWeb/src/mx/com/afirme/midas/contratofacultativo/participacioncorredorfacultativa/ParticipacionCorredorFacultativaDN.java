package mx.com.afirme.midas.contratofacultativo.participacioncorredorfacultativa;

import java.util.List;

import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class ParticipacionCorredorFacultativaDN {
	
	
	private static final ParticipacionCorredorFacultativaDN INSTANCIA = new ParticipacionCorredorFacultativaDN();

	public static ParticipacionCorredorFacultativaDN getInstancia() {
		return ParticipacionCorredorFacultativaDN.INSTANCIA;
	}

	public List<ParticipacionCorredorFacultativoDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionCorredorFacultativaSN participacionCorredorFacultativaSN = new ParticipacionCorredorFacultativaSN();
		return participacionCorredorFacultativaSN.listarTodos();
	}
	
	public List<ParticipacionCorredorFacultativoDTO> getPorPropiedad(String propiedad, Object valor)
	  throws SystemException, ExcepcionDeAccesoADatos {
		ParticipacionCorredorFacultativaSN participacionCorredorFacultativaSN = new ParticipacionCorredorFacultativaSN();
		return participacionCorredorFacultativaSN.getPorPropiedad(propiedad, valor);
     }
	
	  public void agregar(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO) throws SystemException,
      ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		  ParticipacionCorredorFacultativaSN participacionCorredorFacultativaSN = new ParticipacionCorredorFacultativaSN();
		  participacionCorredorFacultativaSN.agregar(participacionCorredorFacultativoDTO);
	}
	  
	 public void borrar(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		 ParticipacionCorredorFacultativaSN participacionCorredorFacultativaSN = new ParticipacionCorredorFacultativaSN();
		 participacionCorredorFacultativaSN.borrar(participacionCorredorFacultativoDTO);
	 }
	 
	 public ParticipacionCorredorFacultativoDTO getPorId(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		 ParticipacionCorredorFacultativaSN participacionCorredorFacultativaSN = new ParticipacionCorredorFacultativaSN();
		 return participacionCorredorFacultativaSN.getPorId(participacionCorredorFacultativoDTO);
	 }
	 
	 public ParticipacionCorredorFacultativoDTO modificar(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		 ParticipacionCorredorFacultativaSN participacionCorredorFacultativaSN = new ParticipacionCorredorFacultativaSN();
		 return participacionCorredorFacultativaSN.modificar(participacionCorredorFacultativoDTO);
	 }
}
