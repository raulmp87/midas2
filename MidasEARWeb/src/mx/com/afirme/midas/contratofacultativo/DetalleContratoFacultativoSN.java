package mx.com.afirme.midas.contratofacultativo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetalleContratoFacultativoSN {

	private DetalleContratoFacultativoFacadeRemote beanRemoto;
	
	public DetalleContratoFacultativoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DetalleContratoFacultativoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public DetalleContratoFacultativoDTO getPorId(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToSlip);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<DetalleContratoFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<DetalleContratoFacultativoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<DetalleContratoFacultativoDTO> DetalleContratoFacultativos= beanRemoto.findAll();
		return DetalleContratoFacultativos;
	}
	
	public void agregar(DetalleContratoFacultativoDTO DetalleContratoFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
    try{
    	beanRemoto.save(DetalleContratoFacultativoDTO);
    } catch(Exception e){
	   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
    } 
  }
	

	public void eliminar(DetalleContratoFacultativoDTO DetalleContratoFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
    try{
    	beanRemoto.delete(DetalleContratoFacultativoDTO);
    } catch(Exception e){
	   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
    } 
  }

	public DetalleContratoFacultativoDTO obtenerDetalleContratoFacultativo(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
		try{
	    	return beanRemoto.findById(detalleContratoFacultativoDTO.getContratoFacultativoDTO());
	    } catch(Exception e){
		   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
	    }
	}
	
	public List<DetalleContratoFacultativoDTO> aplicarConfiguracionATodasLasCoberturas(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO){
		return beanRemoto.aplicarConfiguracionATodasLasCoberturas(detalleContratoFacultativoDTO);
	}
	 
	public boolean validarDetalleParticipacionPorContrato(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO)throws ExcepcionDeAccesoADatos {
		try{
			BigDecimal id = detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo();
	    	return beanRemoto.validarDetalleParticipacionPorContrato(id);
	    } catch(Exception e){
	    	e.printStackTrace();
		   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
	    }
	}
	public boolean validarDetalleParticipacion(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO)throws ExcepcionDeAccesoADatos {
		try{
			BigDecimal id = detalleContratoFacultativoDTO.getContratoFacultativoDTO();
	    	return beanRemoto.validarDetalleParticipacion(id);
	    } catch(Exception e){
	    	e.printStackTrace();
		   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
	    }
	}
	
	public DetalleContratoFacultativoDTO buscarPorIdNatural(BigDecimal idToCotizacion, BigDecimal idTcSubramo,
			BigDecimal idToCobertura, BigDecimal numeroInciso, BigDecimal numeroSubInciso, BigDecimal idToSeccion) {
		try {
			return beanRemoto.findByNaturalKey(idToCotizacion, idTcSubramo, 
					idToCobertura, numeroInciso, numeroSubInciso, idToSeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void modificar(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO){
		beanRemoto.update(detalleContratoFacultativoDTO);
	}
	
	public List<DetalleContratoFacultativoDTO> listarFiltrado(DetalleContratoFacultativoDTO detalleContratofacultativoDTO){
		return beanRemoto.listarFiltrado(detalleContratofacultativoDTO);
	}
}
