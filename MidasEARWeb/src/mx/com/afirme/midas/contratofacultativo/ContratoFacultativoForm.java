package mx.com.afirme.midas.contratofacultativo;


import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoForm;
import mx.com.afirme.midas.catalogos.subramo.SubRamoForm;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ContratoFacultativoForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6811580406219150445L;
    private String idTmContratoFacultativo;
    private String idToSlip;
    private String fechaInicial;
	private String fechaFinal;
	private String estatus;
	private String idFormaPago;
	private String notaCobejrtura;
	private String sumaAseguradaTotal;
	private String sumaAseguradaFacultada;
	private String requiereControlReclamos;
	private String porcentajeFacultativo;
 	private List<SlipDTO> listSLip;
	private SlipForm slipForm = new SlipForm();
	private RamoForm ramoForm = new RamoForm();
	private SubRamoForm subRamoForm = new SubRamoForm();
	private String sumaFacultada;
	private String idtcReaseguradorCorredor;
	private String tipoParticipante;
	private ContratoFacultativoForm contratoFacultativoAnterior;
	private String nombreAsegurado;
	private String primaCien;
	private String primaFac;
	private String porcentajeRetencion;
	private String porcentajeCuotaParte;
	private String porcentajePrimerExcedente;
	private String sumaAseguradaEndoso;
	private String montoPrimaAdicional;
	private String ajustadorNombrado;
	private String montoComisionTotal;
	private String montoTotalPrimaReaseguro;
	private String montoTotalPrimaNeta;
	private String esBonoPorNoSiniestro;
	
	public RamoForm getRamoForm() {
		return ramoForm;
	}
	public void setRamoForm(RamoForm ramoForm) {
		this.ramoForm = ramoForm;
	}
	public SubRamoForm getSubRamoForm() {
		return subRamoForm;
	}
	public void setSubRamoForm(SubRamoForm subRamoForm) {
		this.subRamoForm = subRamoForm;
	}
	
	
    public String getIdTmContratoFacultativo() {
		return idTmContratoFacultativo;
	}
	public void setIdTmContratoFacultativo(String idTmContratoFacultativo) {
		this.idTmContratoFacultativo = idTmContratoFacultativo;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getIdFormaPago() {
		return idFormaPago;
	}
	public void setIdFormaPago(String idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	public String getNotaCobejrtura() {
		return notaCobejrtura;
	}
	public void setNotaCobejrtura(String notaCobejrtura) {
		this.notaCobejrtura = notaCobejrtura;
	}
	public String getSumaAseguradaTotal() {
		return sumaAseguradaTotal;
	}
	public void setSumaAseguradaTotal(String sumaAseguradaTotal) {
		this.sumaAseguradaTotal = sumaAseguradaTotal;
	}
	public String getSumaAseguradaFacultada() {
		return sumaAseguradaFacultada;
	}
	public void setSumaAseguradaFacultada(String sumaAseguradaFacultada) {
		this.sumaAseguradaFacultada = sumaAseguradaFacultada;
	}
	public String getRequiereControlReclamos() {
		return requiereControlReclamos;
	}
	public void setRequiereControlReclamos(String requiereControlReclamos) {
		this.requiereControlReclamos = requiereControlReclamos;
	}
	public String getPorcentajeFacultativo() {
		return  porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(String porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
 	public List<SlipDTO> getListSLip() {
		return listSLip;
	}
	public void setListSLip(List<SlipDTO> listSLip) {
		this.listSLip = listSLip;
	}
	public SlipForm getSlipForm() {
		return slipForm;
	}
	public void setSlipForm(SlipForm slipForm) {
		this.slipForm = slipForm;
	}
	public String getSumaFacultada() {
		return sumaFacultada;
	}
	public void setSumaFacultada(String sumaFacultada) {
		this.sumaFacultada = sumaFacultada;
	}
	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}
	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}
	public String getTipoParticipante() {
		return tipoParticipante;
	}
	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}
	public ContratoFacultativoForm getContratoFacultativoAnterior() {
		return contratoFacultativoAnterior;
	}
	public void setContratoFacultativoAnterior(ContratoFacultativoForm contratoFacultativoAnterior) {
		this.contratoFacultativoAnterior = contratoFacultativoAnterior;
	}
	//setnombreAsegurado
	public String getnombreAsegurado() {
		return nombreAsegurado;
	}
	public void setnombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	
	public String getprimaCien() {
		return primaCien;
	}
	public void setprimaCien(String primaCien) {
		this.primaCien = primaCien;
	}
	
	public String getprimaFac() {
		return primaFac;
	}
	public void setprimaFac(String primaFac) {
		this.primaFac = primaFac;
	}
	
	public String getporcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setporcentajeRetencion(String porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	
	public String getporcentajeCuotaParte() {
		return porcentajeCuotaParte;
	}
	public void setporcentajeCuotaParte(String porcentajeCuotaParte) {
		this.porcentajeCuotaParte = porcentajeCuotaParte;
	}
	
	public String getporcentajePrimerExcedente() {
		return porcentajePrimerExcedente;
	}
	public void setporcentajePrimerExcedente(String porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}
	
	public String getSumaAseguradaEndoso() {
		return sumaAseguradaEndoso;
	}
	public void setSumaAseguradaEndoso(String sumaAseguradaEndoso) {
		this.sumaAseguradaEndoso = sumaAseguradaEndoso;
	}
	public String getMontoPrimaAdicional() {
		return montoPrimaAdicional;
	}
	public void setMontoPrimaAdicional(String montoPrimaAdicional) {
		this.montoPrimaAdicional = montoPrimaAdicional;
	}
	public String getAjustadorNombrado() {
		return ajustadorNombrado;
	}
	public void setAjustadorNombrado(String ajustadorNombrado) {
		this.ajustadorNombrado = ajustadorNombrado;
	}
	public String getMontoComisionTotal() {
		return montoComisionTotal;
	}
	public void setMontoComisionTotal(String montoComisionTotal) {
		this.montoComisionTotal = montoComisionTotal;
	}
	public String getMontoTotalPrimaReaseguro() {
		return montoTotalPrimaReaseguro;
	}
	public void setMontoTotalPrimaReaseguro(String montoTotalPrimaReaseguro) {
		this.montoTotalPrimaReaseguro = montoTotalPrimaReaseguro;
	}
	public String getMontoTotalPrimaNeta() {
		return montoTotalPrimaNeta;
	}
	public void setMontoTotalPrimaNeta(String montoTotalPrimaNeta) {
		this.montoTotalPrimaNeta = montoTotalPrimaNeta;
	}
	public String getEsBonoPorNoSiniestro() {
		return esBonoPorNoSiniestro;
	}
	public void setEsBonoPorNoSiniestro(String esBonoNoSiniestro) {
		this.esBonoPorNoSiniestro = esBonoNoSiniestro;
	}
}