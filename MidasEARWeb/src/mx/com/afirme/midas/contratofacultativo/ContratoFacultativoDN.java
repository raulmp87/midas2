package mx.com.afirme.midas.contratofacultativo;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoForm;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoForm;
import mx.com.afirme.midas.contratofacultativo.configuracionpagos.ConfiguracionPagosFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionfacultativa.ParticipacionFacultativaDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipForm;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroId;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroSN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class ContratoFacultativoDN {

	private static final ContratoFacultativoDN INSTANCIA = new ContratoFacultativoDN(); 
	private static String nombreUsuario;

	public static ContratoFacultativoDN getInstancia(String nombreUsuario){
		ContratoFacultativoDN.nombreUsuario = nombreUsuario;   
		return ContratoFacultativoDN.INSTANCIA;
	}


	public ContratoFacultativoDTO agregar(ContratoFacultativoDTO contratoFacultativoDTO) throws SystemException,
	ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.agregar(contratoFacultativoDTO);
	}

	public void eliminar(ContratoFacultativoDTO contratoFacultativoDTO) throws SystemException,
	ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		contratoFacultativoSN.eliminar(contratoFacultativoDTO);
	}


	public ContratoFacultativoDTO modificar(ContratoFacultativoDTO contratoFacultativoDTO) throws SystemException,
	ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.modificar(contratoFacultativoDTO);
	}

	public ContratoFacultativoDTO getCotizacionFacultativa(BigDecimal idTmContratoFacultativo) throws SystemException, ExcepcionDeAccesoADatos {
		return new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario).getPorId(idTmContratoFacultativo);
	}

	public List<ContratoFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
		return new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario).buscarPorPropiedad(propiedad, valor);
	}

	public List<ContratoFacultativoDTO> listarTodos() throws SystemException,ExcepcionDeAccesoADatos {
		ContratoFacultativoSN  contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.listarTodos();
	}

	public boolean autorizarContratoFacultativo(BigDecimal idTmContratoFacultativo)throws SystemException,ExcepcionDeAccesoADatos {
		boolean autorizado = false;
		ContratoFacultativoSN  contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		autorizado = contratoFacultativoSN.autorizarContratoFacultativo(idTmContratoFacultativo);

		// if (autorizado){
			//ContratoFacultativoDTO contrato = this.getCotizacionFacultativa(idTmContratoFacultativo);

		/*
		 * Ejecuta el thread para la creaci�n de los estados de cuenta del contrato facultativo autorizado
		 */
		//EstadoCuentaFacultativoGenerador generador = new EstadoCuentaFacultativoGenerador();
		//generador.setContratoFacultativoDTO(contrato);

		//Thread hiloGenerador = new Thread(generador);
		//hiloGenerador.start();

		//}

		return autorizado;
	}


	public boolean cancelarContratoFacultativo(BigDecimal idToSlip)throws SystemException,ExcepcionDeAccesoADatos {
		boolean autorizado = false;
		ContratoFacultativoSN  contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		autorizado = contratoFacultativoSN.cancelarContratoFacultativo(idToSlip);
		return autorizado;
	}


	@SuppressWarnings("unused")
	public String autorizarCotizacionFacultativa(BigDecimal idToSlip) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		String mensajeVeredicto = contratoFacultativoSN.autorizarCotizacionFacultativa(idToSlip); 
		if (mensajeVeredicto.equals("")){
			List<SlipDTO> slipDTOList = SlipDN.getInstancia().buscarPorPropiedad("idToSlip", idToSlip);
			for (SlipDTO slip : slipDTOList) {

				//				  if (!UtileriasWeb.esObjetoNulo(slip))
				//					  if (slip.getTipoDistribucion().equals("1")) 			// Cuando TipoDistribucion = Poliza
				//						  ReaseguroCotizacionDN.getInstancia().actualizaEstatus(slip.getIdToCotizacion(), slip.getIdTcSubRamo());
				//					  else
				//						  if (slip.getTipoDistribucion().equals("2")) 		// Cuando TipoDistribucion = Inciso
				//							  ReaseguroIncisoCotizacionDN.getInstancia().actualizaEstatus(slip.getIdToCotizacion(), slip.getIdTcSubRamo(),slip.getNumeroInciso());
				//						  
				//						  else
				//							  if (slip.getTipoDistribucion().equals("3")) 	// Cuando TipoDistribucion = SubInciso
				//								  ReaseguroSubIncisoCotizacionDN.getInstancia().actualizaEstatus(slip.getIdToCotizacion(),slip.getIdTcSubRamo(),slip.getNumeroInciso(),slip.getIdToSeccion(),slip.getNumeroSubInciso());
			}

		}

		return mensajeVeredicto;
	}

	public boolean validarParticipaciones(ContratoFacultativoDTO contratoFacultativoDTO) throws SystemException{
		boolean esValido = true;
		DetalleContratoFacultativoSN detalleContratoFacultativoSN = new DetalleContratoFacultativoSN();
		List<DetalleContratoFacultativoDTO> detallesContratoFacultativo = detalleContratoFacultativoSN.buscarPorPropiedad("contratoFacultativoDTO_1", contratoFacultativoDTO);
		for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detallesContratoFacultativo) {
			if (!detalleContratoFacultativoSN.validarDetalleParticipacion(detalleContratoFacultativoDTO)){//PorContrato(detalleContratoFacultativoDTO)){
				esValido = false;
				break;
			}
		}
		return esValido; 
	}

	public List<ParticipacionFacultativoDTO> obtenerParticipacionesFacultativasPorDetalleContratoFacultativo(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return ParticipacionFacultativaDN.getInstancia().buscarPorPropiedad("detalleContratoFacultativoDTO", detalleContratoFacultativoDTO);
	}

	public List<ConfiguracionPagosFacultativoDTO> findConfiguracionPagosFacultativoDTOByContratoFacultativo(BigDecimal idTmContratoFacultativo){
		try{
			ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
			return contratoFacultativoSN.findConfiguracionPagosFacultativoDTOByContratoFacultativo(idTmContratoFacultativo);
		} catch(Exception e){
			throw new ExcepcionDeAccesoADatos(this.getClass().getCanonicalName(), e);
		} 
	}

	public double obtenerPrimaPorReaseguradorYContratoFacultativo(ReaseguradorCorredorDTO reaseguradorCorredorDTO, ContratoFacultativoDTO contratoFacultativoDTO,BigDecimal porcentajeFacultativo) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.obtenerPrimaPorReaseguradorYContratoFacultativo(reaseguradorCorredorDTO, contratoFacultativoDTO,porcentajeFacultativo);
	}
	
	public void deleteSlipAndDetails(BigDecimal idToSlip) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		contratoFacultativoSN.deleteSlipAndDetails(idToSlip);
	}
	
	public void cambiarNotaCobertura(ContratoFacultativoDTO contratoFacultativoDTO) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		contratoFacultativoSN.cambiarNotaCobertura(contratoFacultativoDTO);
	}

	public int obtenerCantidadContratosPorCotizacion(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException{
		return new ContratoFacultativoSN(nombreUsuario).obtenerCantidadContratosPorCotizacion(idToCotizacion,null);
	}
	
	public int obtenerCantidadContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus,String nombreUsuario) throws SystemException{
		return new ContratoFacultativoSN(nombreUsuario).obtenerCantidadContratosPorCotizacion(idToCotizacion,estatus);
	}
	
	public List<ContratoFacultativoDTO> obtenerContratosPorCotizacion(BigDecimal idToCotizacion,String nombreUsuario) throws SystemException{
		return new ContratoFacultativoSN(nombreUsuario).obtenerContratosPorCotizacion(idToCotizacion,null);
	}
	
	public List<ContratoFacultativoDTO> obtenerContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus,String nombreUsuario) throws SystemException{
		return new ContratoFacultativoSN(nombreUsuario).obtenerContratosPorCotizacion(idToCotizacion,estatus);
	}
	
	public ContratoFacultativoDTO duplicarContratoFacultativo(BigDecimal idTmContratoFacultativo, BigDecimal idToSlip, int numeroEndoso) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.duplicarContratoFacultativo(idTmContratoFacultativo, idToSlip, numeroEndoso);
	}
	
//	public void notificacionEndoso(SoporteReaseguroDTO soporteNuevoEndoso, int tipoEndoso) throws SystemException, ExcepcionDeLogicaNegocio{
//		new ContratoFacultativoSN(nombreUsuario).notificacionEndoso(soporteNuevoEndoso, tipoEndoso);
//	}
	
	public ContratoFacultativoDTO procesarMovimientosCancelacionRehabilitacionCobertura(BigDecimal idTmContratoFacultativo,  int tipoMovimiento, int movimiento) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.procesarMovimientosCancelacionRehabilitacionCobertura(idTmContratoFacultativo, tipoMovimiento, movimiento);
	}
	
	public String poblarContratoFacultativoForm(ContratoFacultativoForm contratoFacultativoForm,SlipDTO slipDTO,ContratoFacultativoDTO contratoFacultativoDTO,int estatusCotizacionSlip,String nombreUsuario) throws SystemException, ExcepcionDeLogicaNegocio, ParseException{
		String reglaNavegacion = null;
		if(contratoFacultativoForm != null && slipDTO != null){
			limpiarSubFormularios(contratoFacultativoForm);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			if (estatusCotizacionSlip == 0){
				if(contratoFacultativoDTO == null){
					ContratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(),slipDTO,contratoFacultativoForm.getRamoForm(),contratoFacultativoForm.getSubRamoForm(),contratoFacultativoForm, nombreUsuario);					
					obtenerSumaAsegurada(contratoFacultativoForm, slipDTO);
					//No se quito de obtenerSumaAsegurada el set del porcentajeFacultativo ya que no
					//se sabe cual pudiera ser el impacto. Ademas la ubicacion de estos poblar no son la adecuada, mas sin embargo se
					//deja en este lugar debido al tiempo que tomaria en realizar un refactor.
					LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().obtenerLineaPorIdTmLineaSoporteReaseguroId(slipDTO.getIdTmLineaSoporteReaseguro());
					poblarContratosPorcentajes(lineaSoporteReaseguroDTO, contratoFacultativoForm);
					contratoFacultativoForm.setMontoPrimaAdicional(UtileriasWeb.formatoMoneda(0d));
				}else{
					ContratoFacultativoAction.poblarDetalleContratoFacultativoDTO(contratoFacultativoDTO,contratoFacultativoForm);
					contratoFacultativoForm.setIdToSlip(contratoFacultativoDTO.getSlipDTO().getIdToSlip().toString());
					ContratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(),slipDTO,contratoFacultativoForm.getRamoForm(),contratoFacultativoForm.getSubRamoForm(),contratoFacultativoForm, nombreUsuario);
					contratoFacultativoForm.setIdTmContratoFacultativo(contratoFacultativoDTO.getIdTmContratoFacultativo().toString());
					contratoFacultativoForm.setRequiereControlReclamos(contratoFacultativoDTO.getRequiereControlReclamos().toBigInteger().toString());
					contratoFacultativoForm.setFechaInicial(simpleDateFormat.format(contratoFacultativoDTO.getFechaInicial()));
					contratoFacultativoForm.setFechaFinal(simpleDateFormat.format(contratoFacultativoDTO.getFechaFinal()));
					obtenerSumaAsegurada(contratoFacultativoForm, slipDTO); 
				}
				reglaNavegacion = "exitoso";
			}else if(estatusCotizacionSlip == 1){
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			}else if(estatusCotizacionSlip == 2){
				ContratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(),slipDTO,contratoFacultativoForm.getRamoForm(),contratoFacultativoForm.getSubRamoForm(),contratoFacultativoForm, nombreUsuario);
				obtenerSumaAsegurada(contratoFacultativoForm, slipDTO);
				ContratoFacultativoAction.poblarDetalleContratoFacultativoDTO(contratoFacultativoDTO,contratoFacultativoForm);
				LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
				lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
				lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
				LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().getPorId(lineaSoporteReaseguroId);
				if (lineaSoporteReaseguroDTO != null && lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() == 0){
					contratoFacultativoForm.getSlipForm().setAutorizadoRetencion(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratofacultativo.slip..autorizadoRetencion"));
				}
				reglaNavegacion = "exitosoContrato";
			}else if(estatusCotizacionSlip == 3){
				ContratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(),slipDTO,contratoFacultativoForm.getRamoForm(),contratoFacultativoForm.getSubRamoForm(),contratoFacultativoForm, nombreUsuario);
				if(contratoFacultativoDTO != null){
					ContratoFacultativoAction.poblarDetalleContratoFacultativoDTO(contratoFacultativoDTO,contratoFacultativoForm);
					contratoFacultativoForm.setIdTmContratoFacultativo(contratoFacultativoDTO.getIdTmContratoFacultativo().toBigInteger().toString());
					contratoFacultativoForm.setRequiereControlReclamos(contratoFacultativoDTO.getRequiereControlReclamos().toBigInteger().toString());
				}
				obtenerSumaAsegurada(contratoFacultativoForm, slipDTO);
				reglaNavegacion = "exitosoSolicitarCancelado";
			}else if(estatusCotizacionSlip ==4){
				ContratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(),slipDTO,contratoFacultativoForm.getRamoForm(),contratoFacultativoForm.getSubRamoForm(),contratoFacultativoForm, nombreUsuario);
				if(contratoFacultativoDTO != null){
					ContratoFacultativoAction.poblarDetalleContratoFacultativoDTO(contratoFacultativoDTO,contratoFacultativoForm);
					contratoFacultativoForm.setIdTmContratoFacultativo(contratoFacultativoDTO.getIdTmContratoFacultativo().toBigInteger().toString());
				}else{
					throw new ExcepcionDeAccesoADatos("La cotizaci&oacute;n facultativa no existe");
				}
				obtenerSumaAsegurada(contratoFacultativoForm, slipDTO);
				reglaNavegacion = "exitosoCancelado";
			}
		}
		return reglaNavegacion;
	}
	
	public void poblarContratosPorcentajes(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO, 
			ContratoFacultativoForm contratoFacultativoForm) {
		if (lineaSoporteReaseguroDTO != null){
			if(lineaSoporteReaseguroDTO.getPorcentajeCuotaParte() != null){
				
				BigDecimal porcentajeCuotaParte= lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte() != null ?
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte() : BigDecimal.ZERO;
				contratoFacultativoForm.setporcentajeCuotaParte(porcentajeCuotaParte.toString());
			}
			
			if(lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente() != null){
				BigDecimal porcentajePrimerExcedente= lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente() != null ?
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente() : BigDecimal.ZERO;
				contratoFacultativoForm.setporcentajePrimerExcedente(porcentajePrimerExcedente.toString());
			}
			
			if(lineaSoporteReaseguroDTO.getPorcentajeRetencion() != null){
				BigDecimal porcentajeRetencion= lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion() != null ?
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion() : BigDecimal.ZERO;
				contratoFacultativoForm.setporcentajeRetencion(porcentajeRetencion.toString());
			}
			
			if(lineaSoporteReaseguroDTO.getPorcentajeFacultativo() != null){
				BigDecimal porcentajeFacultativo = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() != null ?
						lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() : BigDecimal.ZERO;
				contratoFacultativoForm.setPorcentajeFacultativo(porcentajeFacultativo.toString());
			}			
		}
		
	}
	
	public String poblarContratoFacultativoForm(ContratoFacultativoForm contratoFacultativoForm,int estatusCotizacionSlip,BigDecimal idTmContratoFacultativo,String nombreUsuario,boolean poblarFormularioAnidado) throws ExcepcionDeAccesoADatos, SystemException, ParseException, ExcepcionDeLogicaNegocio{
		String reglaNavegacion = null;
		if (idTmContratoFacultativo != null && contratoFacultativoForm != null){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
 			ContratoFacultativoDTO contratoFacultativoDTO = getCotizacionFacultativa(idTmContratoFacultativo);
 			ContratoFacultativoAction.poblarDetalleContratoFacultativoDTO(contratoFacultativoDTO,contratoFacultativoForm);
 			contratoFacultativoForm.setIdToSlip(contratoFacultativoDTO.getSlipDTO().getIdToSlip().toString());
// 			SlipDTO slipDTO = slipDN.getPorId(BigDecimal.valueOf(Integer.valueOf(contratoFacultativoForm.getIdToSlip()))); 
 			ContratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(), contratoFacultativoDTO.getSlipDTO(), contratoFacultativoForm.getRamoForm(), contratoFacultativoForm.getSubRamoForm(), contratoFacultativoForm, nombreUsuario);
 			contratoFacultativoForm.setIdTmContratoFacultativo(idTmContratoFacultativo.toString());
 			contratoFacultativoForm.setRequiereControlReclamos(contratoFacultativoDTO.getRequiereControlReclamos().toBigInteger().toString());
 			contratoFacultativoForm.setFechaInicial(simpleDateFormat.format(contratoFacultativoDTO.getFechaInicial()));
 			contratoFacultativoForm.setFechaFinal(simpleDateFormat.format(contratoFacultativoDTO.getFechaFinal()));
 			if(contratoFacultativoDTO.getEsBonoPorNoSiniestro() != null)
 				contratoFacultativoForm.setEsBonoPorNoSiniestro(contratoFacultativoDTO.getEsBonoPorNoSiniestro().toString());
 			else
 				contratoFacultativoForm.setEsBonoPorNoSiniestro("0"); 			
 			obtenerSumaAsegurada(contratoFacultativoForm, contratoFacultativoDTO.getSlipDTO());
 			if(poblarFormularioAnidado && contratoFacultativoDTO.getIdTmContratoFacultativoAnterior() != null){
 				contratoFacultativoForm.setContratoFacultativoAnterior(new ContratoFacultativoForm());
 				reglaNavegacion = poblarContratoFacultativoForm(contratoFacultativoForm.getContratoFacultativoAnterior(), estatusCotizacionSlip, contratoFacultativoDTO.getIdTmContratoFacultativoAnterior(), nombreUsuario, false);
 			} else{
 				contratoFacultativoForm.setContratoFacultativoAnterior(null);
	 			if(estatusCotizacionSlip == 0){
	 				reglaNavegacion = Sistema.EXITOSO;
	 			}else if(estatusCotizacionSlip == 1){
	 				reglaNavegacion = "exitosoAutorizado";
	 				contratoFacultativoForm.setEstatus(contratoFacultativoDTO.getEstatus().toString());
	 			}else if(estatusCotizacionSlip == 2){
	 				reglaNavegacion = "exitosoContrato";
	 			}else if(estatusCotizacionSlip == 3){
	 				reglaNavegacion = "exitosoSolicitarCancelado";
	 			}else if(estatusCotizacionSlip == 4){
	 				reglaNavegacion = "exitosoCancelado";
	 			}
 			}
 		}
		return reglaNavegacion;
	}
	
	private void limpiarSubFormularios(ContratoFacultativoForm contratoFacultativoForm){
		if(contratoFacultativoForm != null){
			contratoFacultativoForm.setSlipForm(new SlipForm());
			contratoFacultativoForm.setRamoForm(new RamoForm());
			contratoFacultativoForm.setSubRamoForm(new SubRamoForm());
		}
	}
	
	public void obtenerSumaAsegurada(ContratoFacultativoForm contratoFacultativoForm, SlipDTO slipDTO) throws SystemException, ExcepcionDeLogicaNegocio{
		if(slipDTO.getIdTmLineaSoporteReaseguro() != null ){
			SoporteReaseguroSN soporteReaseguroSN = new SoporteReaseguroSN();
			LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
			lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
			lineaSoporteReaseguroId.setIdToSoporteReaseguro(slipDTO.getIdToSoporteReaseguro());
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO =  soporteReaseguroSN.obtenerLineasPorId(lineaSoporteReaseguroId);
			if (lineaSoporteReaseguroDTO == null)
				throw new ExcepcionDeLogicaNegocio(this.getClass().getCanonicalName(), UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratofacultativo.slip.error.inesperado"));
			BigDecimal numero= lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() != null ? 
					lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo() : BigDecimal.ZERO;
			contratoFacultativoForm.setSumaAseguradaTotal(UtileriasWeb.formatoMoneda(lineaSoporteReaseguroDTO.getMontoSumaAsegurada()));    		   
   		  //contratoFacultativoForm.setPorcentajeFacultativo(lineaSoporteReaseguroDTO.getPorcentajeFacultativo().toString());
			contratoFacultativoForm.setPorcentajeFacultativo(numero.toString());
			Double sumaFacultada = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().doubleValue() * numero.doubleValue() / 100;
			NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
			contratoFacultativoForm.setSumaFacultada(numberFormat.format(sumaFacultada.doubleValue()));
			
			//Obtiene la suma asegurada del endoso.
			ContratoFacultativoDN contratoFacultativoDN = ContratoFacultativoDN.getInstancia("");
			BigDecimal idTmContratoFacultativoActual = 
				!UtileriasWeb.esCadenaVacia(contratoFacultativoForm.getIdTmContratoFacultativo()) ?
						new BigDecimal(contratoFacultativoForm.getIdTmContratoFacultativo()) : null;
			if (idTmContratoFacultativoActual != null) {
				ContratoFacultativoDTO contratoFacultativoActual = contratoFacultativoDN.getCotizacionFacultativa(idTmContratoFacultativoActual);

				BigDecimal idTmContratoFacultativoAnterior = contratoFacultativoActual.getIdTmContratoFacultativoAnterior();
				if (idTmContratoFacultativoAnterior != null) {
					ContratoFacultativoDTO contratoFacultativoAnterior = contratoFacultativoDN.getCotizacionFacultativa(idTmContratoFacultativoAnterior);

					BigDecimal sumaAseguradaContratoActual = contratoFacultativoActual.getSumaAseguradaTotal();
					BigDecimal sumaAseguradaContratoAnterior = contratoFacultativoAnterior.getSumaAseguradaTotal();
					BigDecimal sumaAseguradaEndoso = sumaAseguradaContratoActual.subtract(sumaAseguradaContratoAnterior);
					contratoFacultativoForm.setSumaAseguradaEndoso(UtileriasWeb.formatoMoneda(sumaAseguradaEndoso));
				} else {
					contratoFacultativoForm.setSumaAseguradaEndoso(UtileriasWeb.formatoMoneda(contratoFacultativoActual.getSumaAseguradaTotal()));
				}
			}
			else{
				//2010/10/15. Se agrega b�squeda de la l�nea anterior, para determinar la SA del endoso
				BigDecimal montoSAEndoso = null;
				if(lineaSoporteReaseguroDTO.getMontoSumaAseguradaEndoso() != null){
					montoSAEndoso = lineaSoporteReaseguroDTO.getMontoSumaAseguradaEndoso();
				}
				else{
					LineaSoporteReaseguroDTO lineaSRTMP = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteAnterior(lineaSoporteReaseguroDTO);
					BigDecimal montoSAAnterior = BigDecimal.ZERO;
					if(lineaSRTMP != null && lineaSRTMP.getMontoSumaAsegurada() != null){
						montoSAAnterior = lineaSRTMP.getMontoSumaAsegurada();
					}
					montoSAEndoso = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().subtract(montoSAAnterior);
					lineaSoporteReaseguroDTO.setMontoSumaAseguradaEndoso(montoSAEndoso);
					LineaSoporteReaseguroDN.getInstancia().actualizar(lineaSoporteReaseguroDTO);
				}
				contratoFacultativoForm.setSumaAseguradaEndoso(UtileriasWeb.formatoMoneda(montoSAEndoso));
				if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
					lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
				}
				BigDecimal primaSlip = new BigDecimal(0d);
				BigDecimal primaCobertura = null;
				for(LineaSoporteCoberturaDTO lineaCobertura : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs()){
					primaCobertura = lineaCobertura.getMontoPrimaSuscripcion();
					if(lineaCobertura.getMontoPrimaNoDevengada() != null)
						primaCobertura = primaCobertura.subtract(lineaCobertura.getMontoPrimaNoDevengada() );
					primaSlip = primaSlip.add( primaCobertura );
				}
				contratoFacultativoForm.setprimaCien(UtileriasWeb.formatoMoneda(primaSlip));
				String ceroString = UtileriasWeb.formatoMoneda(0d);
				contratoFacultativoForm.setprimaFac(ceroString);
				contratoFacultativoForm.setMontoComisionTotal(ceroString);
				contratoFacultativoForm.setMontoTotalPrimaReaseguro(ceroString);
				contratoFacultativoForm.setMontoTotalPrimaNeta(UtileriasWeb.formatoMoneda(primaSlip));
			}
			
		}else{
			throw new ExcepcionDeAccesoADatos("El Slip no tiene linea soporte");   
		}
	}
	
	public ContratoFacultativoDTO obtenerContratoFacultativo(BigDecimal idToSlip,int numeroEndoso) throws ExcepcionDeAccesoADatos, SystemException{
		ContratoFacultativoDTO contrato = null;
		List<ContratoFacultativoDTO> listaContratos = buscarPorPropiedad("slipDTO.idToSlip", idToSlip);
		if(listaContratos != null && !listaContratos.isEmpty()){
			contrato = listaContratos.get(0);
		}
		return contrato;
	}
	
	public ContratoFacultativoDTO obtenerContratoFacultativoPorIdToSlip(BigDecimal idToSlip)throws SystemException, ExcepcionDeAccesoADatos {
		ContratoFacultativoDTO contrato = null;
		List<ContratoFacultativoDTO> listaContratos = buscarPorPropiedad("slipDTO.idToSlip", idToSlip);
		if(listaContratos != null && !listaContratos.isEmpty()){
			contrato = listaContratos.get(0);
		}
		return contrato;
	}
	
	public DetalleContratoFacultativoDTO copiarParticipacionesFacultativoCoberturaEndosoAnterior(
			BigDecimal idTdContratoFacultativoActual) {
		try {
			ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
			return contratoFacultativoSN.copiarParticipacionesFacultativoCoberturaEndosoAnterior(idTdContratoFacultativoActual);
		} catch(SystemException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public BigDecimal getPrimaEndosoTotal(BigDecimal idTmContratoFacultativo) {
		try {
			ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
			return contratoFacultativoSN.getPrimaEndosoTotal(idTmContratoFacultativo);
		} catch(SystemException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public BigDecimal getPrimaNegociadaTotal(BigDecimal idTmContratoFacultativo) {
		try {
			ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
			return contratoFacultativoSN.getPrimaNegociadaTotal(idTmContratoFacultativo);
		} catch(SystemException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
     * Metodo usado para ajustar la prima adicional del contrato a partir de la prima adicional guardada en los detalles del mismo.
	 * @throws SystemException 
     */
	public ContratoFacultativoDTO ajustarPrimaAdicionalSegunDetalles(String nombreUsuario, BigDecimal idTmContratoFacultativo) throws SystemException {
		return new ContratoFacultativoSN(nombreUsuario).ajustarPrimaAdicionalSegunDetalles(idTmContratoFacultativo);
	}
	
	/**
	 * Metodo que consulta los ajustadores nombrados de los contratos facultativos registrados para una poliza en un endoso definido.
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return Lista de los nombres de ajustadores nombrados registrados en los contratos facultativos de la poliza.
	 */
	public List<String> obtenerAjustadorNombrado(BigDecimal idToPoliza,short numeroEndoso) throws SystemException{
		return new ContratoFacultativoSN(nombreUsuario).obtenerAjustadorNombrado(idToPoliza, numeroEndoso);
	}
	
	public BigDecimal getComisionTotal(BigDecimal idTmContratoFacultativo) throws SystemException{
		ContratoFacultativoSN contratoFacultativoSN = new ContratoFacultativoSN(ContratoFacultativoDN.nombreUsuario);
		return contratoFacultativoSN.getComisionTotal(idTmContratoFacultativo);
	}
}
