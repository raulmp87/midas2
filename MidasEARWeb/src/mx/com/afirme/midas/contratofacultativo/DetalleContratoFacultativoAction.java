package mx.com.afirme.midas.contratofacultativo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.contacto.ContactoDN;
import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDN;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorForm;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PlanPagosCoberturaDN;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PlanPagosCoberturaDTO;
import mx.com.afirme.midas.contratofacultativo.participacionCorredorFacultativo.ParticipacionCorredorFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacioncorredorfacultativa.ParticipacionCorredorFacultativaDN;
import mx.com.afirme.midas.contratofacultativo.participacioncorredorfacultativa.ParticipacionCorredorFacultativaForm;
import mx.com.afirme.midas.contratofacultativo.participacionfacultativa.ParticipacionFacultativaDN;
import mx.com.afirme.midas.contratofacultativo.participacionfacultativa.ParticipacionFacultativaForm;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.reaseguro.ReaseguroDetalleCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoDetalleCoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoDetalleCoberturaCotizacionDN;
import mx.com.afirme.midas.danios.ReaseguroDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.danios.ReaseguroSubIncisoDetalleCoberturaCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroId;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroSN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.TreeLoader;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DetalleContratoFacultativoAction extends MidasMappingDispatchAction {
	/**
	 * borrarParticipacionCorredorFacultativo
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void borrarParticipacionCorredorFacultativo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String idTdParticipacionCorredorFacultativo = request.getParameter("idTdParticipacionCorredorFacultativo");
		ParticipacionCorredorFacultativaDN participacionCorredorFacultativaDN = ParticipacionCorredorFacultativaDN.getInstancia();
		ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO = new ParticipacionCorredorFacultativoDTO();

		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		try{
			try {
				participacionCorredorFacultativoDTO.setIdTdParticapacionCorredorFac(new BigDecimal(idTdParticipacionCorredorFacultativo));
				participacionCorredorFacultativaDN.borrar(participacionCorredorFacultativoDTO);
				buffer.append("<id>"+1+"</id><description><![CDATA[La participaci\u00f3n fue eliminada correctamente.]]></description>");
				buffer.append("</item>");
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());
			}catch (SystemException e) {
				buffer.append("<id>"+0+"</id><description><![CDATA[No se pudo eliminar la participaci\u00f3n.]]></description>");
				buffer.append("</item>");
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
    //esta parte configura la covertura 
    public void configurarCobertura(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException {
		//int numHijos = 0;
		String itemId = "";
		if (request.getParameter("id") != null) {
			itemId = request.getParameter("id");
		}

		String contextoMenu = "";
		if (request.getParameter("menu") != null) {
			contextoMenu = request.getParameter("menu");
		}
		String id = "";
		if (request.getParameter("id") != null) {
			id = (String) request.getAttribute("id");
		}
		TreeLoader tree = new TreeLoader(contextoMenu, mapping, form, request,
				response);
		 try {
		     StringBuffer buffer = new StringBuffer();
		  	 DetalleContratoFacultativoDN  detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
		  	 ContratoFacultativoDN  contratoFacultativoDN = ContratoFacultativoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		  	 ContratoFacultativoDTO contratoFacultativoDTO = contratoFacultativoDN.getCotizacionFacultativa(BigDecimal.valueOf(Integer
					.valueOf(id)));
		     List<DetalleContratoFacultativoDTO> detalleContratoFacultativo = detalleContratoFacultativoDN.buscarPorPropiedad("contratoFacultativoDTO_1", contratoFacultativoDTO);
       	 	 Iterator<DetalleContratoFacultativoDTO> iteratorList = detalleContratoFacultativo.iterator();
       	 	 boolean esValido = false;
       	 	 Map<BigDecimal,CoberturaDTO> mapaCoberturas = new HashMap<BigDecimal,CoberturaDTO>();
       	 	 if (iteratorList.hasNext()){
			  	 while (iteratorList.hasNext()) { 
	       	 	  DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = iteratorList.next();
	       	      //numHijos = 0;
	       	      esValido = detalleContratoFacultativoDN.validarDetalleParticipacion(detalleContratoFacultativoDTO);
	       	      if(!mapaCoberturas.containsKey(detalleContratoFacultativoDTO.getIdToCobertura())){
	       	    	  mapaCoberturas.put(detalleContratoFacultativoDTO.getIdToCobertura(),CoberturaDN.getInstancia().getPorId(detalleContratoFacultativoDTO.getIdToCobertura()));
	       	      }
//	       	      CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(detalleContratoFacultativoDTO.getIdToCobertura());
//	       	      String tituloCobertura = coberturaDTO.getNombreComercial();
	       	      String tituloCobertura = (detalleContratoFacultativoDTO.getNumeroInciso()!=null?detalleContratoFacultativoDTO.getNumeroInciso()+" - ":"")+mapaCoberturas.get(detalleContratoFacultativoDTO.getIdToCobertura()).getNombreComercial();
	       	      if (esValido)
						buffer = tree.escribirItem(buffer, tituloCobertura, detalleContratoFacultativoDTO.getContratoFacultativoDTO()
									.toString(), id, 0);
	       	      else
						buffer = tree.escribirItemRojo(buffer,tituloCobertura, detalleContratoFacultativoDTO.getContratoFacultativoDTO()
								.toString(), id, 0);
			  	 }
       	 	 }else{
       	 		if (id!=null){
       	 			int idToSlip = -Integer.valueOf(id).intValue();
       	 			if(idToSlip>0){
       	 				List<DetalleContratoFacultativoDTO> detallesContratos = obtieneDetalleSinGuardar(new BigDecimal(idToSlip),
       	 						UtileriasWeb.obtieneNombreUsuario(request));
       	 				int i = 0;
       	 				for (DetalleContratoFacultativoDTO detalleContratoFacultativoDTO : detallesContratos) {
       	 					if (detalleContratoFacultativoDTO!=null){
       	 						buffer = tree.escribirItemRojo(buffer,"Contrato pendiente", "-"+String.valueOf(i), id, 0);
       	 					}
       	 				}
       	 			}
       	 		}
       	 	 }
			buffer = tree.xmlHeader(buffer, itemId);
 			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());

		} catch (SystemException e) {
			throw new SystemException(e);
		} catch (ExcepcionDeAccesoADatos e) {
			throw new SystemException(e);
		} catch (IOException ioException) {
			throw new SystemException("Unable to render producto List",
					ioException);
		} 
		
	}
    
      public boolean validarDetalleContratoFacultativo(String idTmContratoFacultativo){
      	 boolean existeContratoFacultativo = false;
       		DetalleContratoFacultativoDN  detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
      	 try{
       	 	 List<DetalleContratoFacultativoDTO> detalleContratoFacultativo = detalleContratoFacultativoDN.buscarPorPropiedad("contratoFacultativoDTO_1", BigDecimal.valueOf(Integer.valueOf(idTmContratoFacultativo)));
       	 	 Iterator<DetalleContratoFacultativoDTO> contratoList = detalleContratoFacultativo.iterator();
               if (contratoList.hasNext()){
              	 existeContratoFacultativo = true;
               }
             	} catch (ExcepcionDeAccesoADatos e) {
      			e.printStackTrace();
      		}catch (SystemException ex){
      			ex.printStackTrace();
      		}
       	return existeContratoFacultativo;
      }
      
      private List<DetalleContratoFacultativoDTO> obtieneDetalleSinGuardar(BigDecimal idToSlip, String nombreUsuario) throws SystemException{
    	  List<DetalleContratoFacultativoDTO> list = new ArrayList<DetalleContratoFacultativoDTO>();
			
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
			ContratoFacultativoDN contratoFacultativoDN = ContratoFacultativoDN.getInstancia(nombreUsuario);
			SlipDN slipDN = SlipDN.getInstancia();
			
			SlipDTO slipDTO = slipDN.getPorId(BigDecimal.valueOf(idToSlip.longValue()));	
			List<ContratoFacultativoDTO> contratoFacultativoListDTO = contratoFacultativoDN.buscarPorPropiedad("slipDTO", slipDTO); 
			ContratoFacultativoDTO contratoFacultativoDTO = (ContratoFacultativoDTO) contratoFacultativoListDTO.get(0);
			if (slipDTO.getTipoDistribucion().equals("1")){ // Tipo P�liza
				List<ReaseguroDetalleCoberturaCotizacionDTO> reaseguroDetalleCoberturaCotizacionDTOList = ReaseguroDetalleCoberturaCotizacionDN.getInstancia().obtenerDetalleCoberturasCotizacion(slipDTO.getIdToCotizacion(), slipDTO.getIdTcSubRamo());
				for (ReaseguroDetalleCoberturaCotizacionDTO reaseguroDetalleCoberturaCotizacionDTO : reaseguroDetalleCoberturaCotizacionDTOList) {
					detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
					
					detalleContratoFacultativoDTO.setIdToCotizacion(reaseguroDetalleCoberturaCotizacionDTO.getId().getIdToCotizacion());
					detalleContratoFacultativoDTO.setIdTcSubramo(reaseguroDetalleCoberturaCotizacionDTO.getId().getIdTcSubRamo());
					detalleContratoFacultativoDTO.setIdToCobertura(reaseguroDetalleCoberturaCotizacionDTO.getId().getIdToCobertura());
					detalleContratoFacultativoDTO.setSumaAsegurada(new BigDecimal(reaseguroDetalleCoberturaCotizacionDTO.getValorSumaAsegurada()));
					detalleContratoFacultativoDTO.setPrimaTotalCobertura(new BigDecimal(reaseguroDetalleCoberturaCotizacionDTO.getValorPrimaNeta()));
					
					//del SlipDTO
					detalleContratoFacultativoDTO.setTipoDistribucion(new BigDecimal(slipDTO.getTipoDistribucion()));
					detalleContratoFacultativoDTO.setDeducibleCobertura(BigDecimal.ZERO); // TODO revisar, no est� definido en dise�o
					detalleContratoFacultativoDTO.setPorcentajeComision(BigDecimal.ZERO);
					detalleContratoFacultativoDTO.setSumaFacultada(BigDecimal.ZERO);
					detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(BigDecimal.ZERO);
					
					list.add(detalleContratoFacultativoDTO);
					
				}
			}else{
				if (slipDTO.getTipoDistribucion().equals("2")){ // Tipo Inciso
			    	List<ReaseguroIncisoDetalleCoberturaCotizacionDTO> reaseguroIncisoDetalleCoberturaCotizacionDTOList = ReaseguroIncisoDetalleCoberturaCotizacionDN.getInstancia().obtenerDetalleIncisoCoberturasCotizacion(slipDTO.getIdToCotizacion(), slipDTO.getIdTcSubRamo(), slipDTO.getNumeroInciso());
			    	for (ReaseguroIncisoDetalleCoberturaCotizacionDTO reaseguroIncisoDetalleCoberturaCotizacionDTO : reaseguroIncisoDetalleCoberturaCotizacionDTOList) {
			    		detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
			    		
			    		detalleContratoFacultativoDTO.setContratoFacultativoDTO_1(contratoFacultativoDTO);
			  	    	detalleContratoFacultativoDTO.setIdToCotizacion(reaseguroIncisoDetalleCoberturaCotizacionDTO.getId().getIdToCotizacion());
			  	    	detalleContratoFacultativoDTO.setNumeroInciso(reaseguroIncisoDetalleCoberturaCotizacionDTO.getId().getNumeroInciso());
			    		detalleContratoFacultativoDTO.setIdTcSubramo(reaseguroIncisoDetalleCoberturaCotizacionDTO.getId().getIdTcSubRamo());
			    		detalleContratoFacultativoDTO.setIdToCobertura(reaseguroIncisoDetalleCoberturaCotizacionDTO.getId().getIdToCobertura());
			    		detalleContratoFacultativoDTO.setSumaAsegurada(new BigDecimal(reaseguroIncisoDetalleCoberturaCotizacionDTO.getValorSumaAsegurada()));
			    		detalleContratoFacultativoDTO.setPrimaTotalCobertura(new BigDecimal(reaseguroIncisoDetalleCoberturaCotizacionDTO.getValorPrimaNeta()));
			    		
			    		//del SlipDTO
			    		detalleContratoFacultativoDTO.setTipoDistribucion(new BigDecimal(slipDTO.getTipoDistribucion()));
			    		detalleContratoFacultativoDTO.setDeducibleCobertura(BigDecimal.ZERO); // TODO revisar, no est� definido en dise�o
			    		detalleContratoFacultativoDTO.setPorcentajeComision(BigDecimal.ZERO);
			    		detalleContratoFacultativoDTO.setSumaFacultada(BigDecimal.ZERO);
			    		detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(BigDecimal.ZERO);
			    		
			    		list.add(detalleContratoFacultativoDTO);
			    	}
			    }else{
			  	    if (slipDTO.getTipoDistribucion().equals("3")){ // Tipo Subinciso
			  	    	List<ReaseguroSubIncisoDetalleCoberturaCotizacionDTO> reaseguroSubIncisoDetalleCoberturaCotizacionDTOList = ReaseguroSubIncisoDetalleCoberturaCotizacionDN.getInstancia().obtenerDetalleSubIncisosCoberturasCotizacion(slipDTO.getIdToCotizacion(), slipDTO.getIdTcSubRamo(), slipDTO.getNumeroInciso(), slipDTO.getIdToSeccion(), slipDTO.getNumeroSubInciso());
			  	    	for (ReaseguroSubIncisoDetalleCoberturaCotizacionDTO reaseguroSubIncisoDetalleCoberturaCotizacionDTO : reaseguroSubIncisoDetalleCoberturaCotizacionDTOList) {
			  	    		detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
			  	    		
			  	    		detalleContratoFacultativoDTO.setContratoFacultativoDTO_1(contratoFacultativoDTO);
			   	  	    	detalleContratoFacultativoDTO.setIdToCotizacion(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId().getIdToCotizacion());
			   	  	    	detalleContratoFacultativoDTO.setNumeroInciso(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId().getNumeroInciso());
			   	  	    	detalleContratoFacultativoDTO.setIdToSeccion(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId().getIdToSeccion());
			   	  	    	detalleContratoFacultativoDTO.setNumeroSubInciso(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId().getNumeroSubInciso());
			   	    		detalleContratoFacultativoDTO.setIdTcSubramo(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId().getIdTcSubRamo());
			   	    		detalleContratoFacultativoDTO.setIdToCobertura(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getId().getIdToCobertura());
			   	    		detalleContratoFacultativoDTO.setSumaAsegurada(new BigDecimal(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getValorSumaAsegurada()));
			   	    		detalleContratoFacultativoDTO.setPrimaTotalCobertura(new BigDecimal(reaseguroSubIncisoDetalleCoberturaCotizacionDTO.getValorPrimaNeta()));
			  	    		
			  	    		//del SlipDTO
			   	    		detalleContratoFacultativoDTO.setTipoDistribucion(new BigDecimal(slipDTO.getTipoDistribucion()));
			   	    		detalleContratoFacultativoDTO.setDeducibleCobertura(BigDecimal.ZERO); // TODO revisar, no est� definido en dise�o
			   	    		detalleContratoFacultativoDTO.setPorcentajeComision(BigDecimal.ZERO);
				   	    	detalleContratoFacultativoDTO.setSumaFacultada(BigDecimal.ZERO);
			   	    		detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(BigDecimal.ZERO);
			    		
			   	    		list.add(detalleContratoFacultativoDTO);
			  	    	}
			  	    }
			    }
			}
			return list;
      }
      
      public ActionForward agregarDetalleCotizacionFacultativa(ActionMapping mapping, ActionForm form,
  			HttpServletRequest request, HttpServletResponse response) {
  		String reglaNavegacion = Sistema.EXITOSO;
  		ContratoFacultativoForm contratoFacultativoForm = (ContratoFacultativoForm) form;
  		DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
  		ContratoFacultativoDN contratoFacultativoDN = ContratoFacultativoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
  	 	SlipDN slipDN = SlipDN.getInstancia();
  	  	try {
	  	  	SlipDTO slipDTO = slipDN.getPorId(BigDecimal.valueOf(Integer.valueOf(request.getAttribute("idToSlip").toString())));	
	  	    List<ContratoFacultativoDTO> contratoFacultativoListDTO = contratoFacultativoDN.buscarPorPropiedad("slipDTO", slipDTO); 
	  		ContratoFacultativoDTO contratoFacultativoDTO = (ContratoFacultativoDTO) contratoFacultativoListDTO.get(0);
	  	    ContratoFacultativoAction contratoFacultativoAction = new ContratoFacultativoAction();
	  	    contratoFacultativoAction.poblarSlipForm(contratoFacultativoForm.getSlipForm(),slipDTO,contratoFacultativoForm.getRamoForm(),contratoFacultativoForm.getSubRamoForm(),contratoFacultativoForm, request);
	  	    contratoFacultativoDN.obtenerSumaAsegurada(contratoFacultativoForm, slipDTO);
	  	    poblarContratoFacultativoForm(contratoFacultativoDTO,contratoFacultativoForm,slipDTO.getIdToSlip().toString());
	   	  if (slipDTO.getIdTmLineaSoporteReaseguro() != null && slipDTO.getIdToSoporteReaseguro() != null){
	           SoporteReaseguroSN soporteReaseguroSN = new SoporteReaseguroSN(); 
//	           LineaSoporteCoberturaDTOId lineaSoporteCoberturaDTOId = new LineaSoporteCoberturaDTOId();
//	           lineaSoporteCoberturaDTOId.setIdTmLineaSoporteReaseguro(slipDTO.getIdTmLineaSoporteReaseguro());
//	           LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = new LineaSoporteCoberturaDTO();
//	           lineaSoporteCoberturaDTO.setId(lineaSoporteCoberturaDTOId); 
	           List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaList = soporteReaseguroSN.obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro", slipDTO.getIdTmLineaSoporteReaseguro()); 
	  	       for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteCoberturaList){
	  	    	   LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = new LineaSoporteReaseguroDTO();
	  	    	   LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
	  	    	   lineaSoporteReaseguroId.setIdTmLineaSoporteReaseguro(lineaSoporteCoberturaDTO.getId().getIdTmLineaSoporteReaseguro());
	  	    	   lineaSoporteReaseguroId.setIdToSoporteReaseguro(lineaSoporteCoberturaDTO.getId().getIdToSoporteReaseguro());
				   lineaSoporteReaseguroDTO = soporteReaseguroSN.obtenerLineasPorId(lineaSoporteReaseguroId);
	  	    	   detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
	  	    	   detalleContratoFacultativoDTO.setContratoFacultativoDTO_1(contratoFacultativoDTO);
   	  	    	   detalleContratoFacultativoDTO.setIdToCotizacion(lineaSoporteReaseguroDTO.getIdToCotizacion());
   	  	    	   detalleContratoFacultativoDTO.setNumeroInciso(slipDTO.getNumeroInciso());
   	  	    	   detalleContratoFacultativoDTO.setIdToSeccion(slipDTO.getIdToSeccion());
   	  	    	   detalleContratoFacultativoDTO.setNumeroSubInciso(slipDTO.getNumeroSubInciso());
   	    		   detalleContratoFacultativoDTO.setIdTcSubramo(slipDTO.getIdTcSubRamo());
   	    		   detalleContratoFacultativoDTO.setIdToCobertura(lineaSoporteCoberturaDTO.getIdToCobertura());
   	    		   detalleContratoFacultativoDTO.setSumaAsegurada(lineaSoporteReaseguroDTO.getMontoSumaAsegurada());
   	    		   detalleContratoFacultativoDTO.setPrimaTotalCobertura(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
	  	    	    //del SlipDTO
	   	    	   detalleContratoFacultativoDTO.setTipoDistribucion(new BigDecimal(slipDTO.getTipoDistribucion()));
	   	    	   detalleContratoFacultativoDTO.setDeducibleCobertura(lineaSoporteCoberturaDTO.getPorcentajeDeducible()); // TODO revisar, no est� definido en dise�o
	   	    	   detalleContratoFacultativoDTO.setPorcentajeComision(lineaSoporteCoberturaDTO.getPorcentajeCoaseguro());
		   	       detalleContratoFacultativoDTO.setSumaFacultada(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
   	    		   detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(lineaSoporteCoberturaDTO.getMontoPrimaFacultativo());
   	    	   	   detalleContratoFacultativoDTO.setPrimaFacultadaCobertura(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion());
   	    		   DetalleContratoFacultativoDN.getInstancia().agregar(detalleContratoFacultativoDTO);
	   	       }
	   	    }
	   	 	mensajeExitoAgregar(contratoFacultativoForm,"Se Agrego Detalle Contrato Facultativo Correctamente");
   	  	} catch (SystemException e) {
  			reglaNavegacion = Sistema.NO_DISPONIBLE;
  			mensajeExcepcion(contratoFacultativoForm, e);
  		} catch (ExcepcionDeAccesoADatos e) {
  			reglaNavegacion = Sistema.NO_EXITOSO;
  			mensajeExcepcion(contratoFacultativoForm, e);
  		} catch (ExcepcionDeLogicaNegocio e) {
  			reglaNavegacion = Sistema.NO_EXITOSO;
  			mensajeExcepcion(contratoFacultativoForm, e);
  		}
  		return mapping.findForward(reglaNavegacion);
  	}     
     protected void poblarContratoFacultativoForm(ContratoFacultativoDTO contratoFacultativoDTO,ContratoFacultativoForm contratoFacultativoForm,String idToSlip){
         if (contratoFacultativoDTO.getIdTmContratoFacultativo() != null){
        	 contratoFacultativoForm.setIdTmContratoFacultativo(contratoFacultativoDTO.getIdTmContratoFacultativo().toString());
         }
         if (idToSlip != null){
         	 contratoFacultativoForm.setIdToSlip(idToSlip);
         }
         if (contratoFacultativoDTO.getFechaFinal() != null){
        	 contratoFacultativoForm.setFechaFinal(UtileriasWeb.getFechaString(contratoFacultativoDTO.getFechaFinal()));
         }
         if (contratoFacultativoDTO.getFechaInicial() != null){
        	 contratoFacultativoForm.setFechaInicial(UtileriasWeb.getFechaString(contratoFacultativoDTO.getFechaInicial()));
         }
         if (contratoFacultativoDTO.getEstatus() != null){
        	 contratoFacultativoForm.setEstatus(contratoFacultativoDTO.getEstatus().toString());
         }
         if (contratoFacultativoDTO.getIdFormaDePago() != null){
        	 contratoFacultativoForm.setIdFormaPago(contratoFacultativoDTO.getIdFormaDePago().toString());
         }
         if (contratoFacultativoDTO.getNotaCobertura() != null){
        	 contratoFacultativoForm.setNotaCobejrtura(contratoFacultativoDTO.getNotaCobertura().toString());
         }
        
         if (contratoFacultativoDTO.getSumaAseguradaTotal() != null){
        	 contratoFacultativoForm.setSumaAseguradaTotal(contratoFacultativoDTO.getSumaAseguradaTotal().toString());
         }
         if (contratoFacultativoDTO.getSumaAseguradaFacultada() != null){
        	 contratoFacultativoForm.setSumaAseguradaFacultada(contratoFacultativoDTO.getSumaAseguradaFacultada().toString());
         }
         if (contratoFacultativoDTO.getRequiereControlReclamos()!= null){
        	 contratoFacultativoForm.setRequiereControlReclamos(contratoFacultativoDTO.getRequiereControlReclamos().toString());
         }
         
     }
     

     public ActionForward listarConfiguracionCobertura(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
  		String reglaNavegacion = Sistema.EXITOSO;
  		DetalleContratoFacultativoDN detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
  	  	DetalleContratoFacultativoForm detalleContratoFacultativoForm = (DetalleContratoFacultativoForm) form;
  	 	DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
  	   	String estatusCotizacionSlip = request.getSession().getAttribute("estatusCotizacionSlip").toString();
 	  	String idTdContratoFacultativo = null;
 	  	idTdContratoFacultativo =  request.getParameter("id");
 	  	String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
 	  	if (idTdContratoFacultativo != null){
 	  		request.setAttribute("idTdContratoFacultativo", idTdContratoFacultativo);
 	  	}
 	  	if (request.getAttribute("id") != null){
 	  		idTdContratoFacultativo = request.getAttribute("id").toString();
 	  	}
 	  	
  	    detalleContratoFacultativoForm.setIdTdContratoFacultativo(idTdContratoFacultativo);
  	    detalleContratoFacultativoDTO.setContratoFacultativoDTO(new BigDecimal(idTdContratoFacultativo));
  	    try{
  	    	Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
  	    	detalleContratoFacultativoDTO = detalleContratoFacultativoDN.obtenerDetalleContratoFacultativo(detalleContratoFacultativoDTO);
  	    	//si el detalle de contrato es de un endoso > 0, se redirecciona a la JSP con el accordion
  	    	poblarForm(detalleContratoFacultativoDTO, detalleContratoFacultativoForm,nombreUsuario,usuario);

  	    	//Esta logica es para saber si el textbox primaFacultadaCobertura estara modificable.
  	    	BigDecimal primaEndoso = detalleContratoFacultativoDTO.getPrimaTotalCobertura();
  	    	if (primaEndoso.compareTo(BigDecimal.ZERO) > 0) {
  	    		detalleContratoFacultativoForm.setPrimaFacultadaCobModificable(true);
  	    	}
  	    	
  	    	if(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativoAnterior() != null &&
  	    			detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getNumeroEndosoFacultativo() != null &&
  	    			detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getNumeroEndosoFacultativo() > 0 &&
  	    			detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativoAnterior().compareTo(BigDecimal.ZERO) > 0){
  	    		List<DetalleContratoFacultativoDTO> listaDetallesAnteriores = detalleContratoFacultativoDN.listarDetalleContratoFiltrado
  	    				(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativoAnterior(), detalleContratoFacultativoDTO.getIdTcSubramo(), 
  	    				detalleContratoFacultativoDTO.getIdToCobertura(), detalleContratoFacultativoDTO.getNumeroInciso()
  	    				, detalleContratoFacultativoDTO.getNumeroSubInciso(), detalleContratoFacultativoDTO.getIdToSeccion());
  	    		if(listaDetallesAnteriores != null && !listaDetallesAnteriores.isEmpty()){
  	    			if(listaDetallesAnteriores.size() == 1){
  	    				detalleContratoFacultativoForm.setDetalleContratoFacultativoAnterior(new DetalleContratoFacultativoForm());
  	    				poblarForm(listaDetallesAnteriores.get(0),detalleContratoFacultativoForm.getDetalleContratoFacultativoAnterior(),nombreUsuario,usuario);
  	    			}
  	    		}else{
  	    			detalleContratoFacultativoForm.setDetalleContratoFacultativoAnterior(null);
  	    		}
  	    	}
  	    	else{
  	    		detalleContratoFacultativoForm.setDetalleContratoFacultativoAnterior(null);
  	    	}
  	    }catch(SystemException e){
 			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
 		}catch (ExcepcionDeAccesoADatos e){
 			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
 		}catch (ExcepcionDeLogicaNegocio e){
 			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
 		}
// 		String endoso = (esDetalleContratoEndoso);
 	  	if (estatusCotizacionSlip.equals("0")){
 	  		if (detalleContratoFacultativoDTO.getEstatus() != null && 
 	  				detalleContratoFacultativoDTO.getEstatus().intValue() == DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA){
 	  			reglaNavegacion = "exitosoCancelacionPND";
 	  		}else{
 	  			reglaNavegacion = Sistema.EXITOSO;
 	  		}
 	  	}else if (estatusCotizacionSlip.equals("1")){
 	  		reglaNavegacion = "exitosoAutorizado";
 	  	}else if (estatusCotizacionSlip.equals("2")){
 	  		reglaNavegacion = "exitosoContrato"; 	
 	  	}else if (estatusCotizacionSlip.equals("3")){
 	  		reglaNavegacion = "exitosoSolicitarCancelado";
 	  	}else if (estatusCotizacionSlip.equals("4")){
 	  		reglaNavegacion = "exitosoCancelado";
 	  	}
 		
        	return mapping.findForward(reglaNavegacion);
  	}
     
     public void recargaParticipacionFacultativo(ActionMapping mapping, ActionForm form,
 			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
    	String idParticipacionFacultativo = "";
    	idParticipacionFacultativo = request.getParameter("id");
     	 if(idParticipacionFacultativo != null){
     	 	HttpSession session  = request.getSession();
     	 	session.setAttribute("idParticipacionFacultativo", idParticipacionFacultativo);
         }
//     	String json = "{rows:[{id:\"7\",data:[\"2\",\"RECARGO 1\",\"0.0\",\"0\",\"/MidasWeb/img/btn_guardar.gif^Pendiente^javascript:mostrarAgregarParticipacionCorredor();\"]}]}";

     	MidasJsonBase json = new MidasJsonBase();
     	
     	MidasJsonRow row = new MidasJsonRow();
		row.setId("7");
		row.setDatos(
				"2",
				"RECARGO 1",
				"0.0",
				"0",
				"/MidasWeb/img/btn_guardar.gif^Pendiente^javascript:mostrarAgregarParticipacionCorredor();"
		);
		json.addRow(row);
     	     	
     	System.out.println(json);
 		response.setContentType("text/json");
 		PrintWriter pw = response.getWriter();
 		pw.write(json.toString());
 		pw.flush();
 		pw.close();
 	}
    
    public void cargarParticipacionFacultativo(ActionMapping mapping, ActionForm form,
 			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
    		DetalleContratoFacultativoDN detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
	        ParticipacionFacultativaDN participacionfacultativaDN = ParticipacionFacultativaDN.getInstancia();
		    DetalleContratoFacultativoForm detallecontratoFacultativoForm = (DetalleContratoFacultativoForm) form;
	        String idTdContratoFacultativo = "";
	         if (request.getParameter("id") != null) {
	        	 idTdContratoFacultativo = (String) request.getParameter("id");
	        	 request.setAttribute("idTdContratoFacultativo", idTdContratoFacultativo);
				}
	         String esEndoso = request.getParameter("esEndoso");
	         boolean detalleEndoso = (esEndoso != null && esEndoso.equals("true"));
	         try
	           {
	        	DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
	        	detalleContratoFacultativoDTO.setContratoFacultativoDTO(BigDecimal.valueOf(Integer.valueOf(idTdContratoFacultativo)));
	        	detallecontratoFacultativoForm.setIdTdContratoFacultativo(idTdContratoFacultativo);
	        	detalleContratoFacultativoDTO = detalleContratoFacultativoDN.getCotizacionFacultativa(detalleContratoFacultativoDTO.getContratoFacultativoDTO());
    		    ParticipacionFacultativoDTO participacionFacultativoDTO = new ParticipacionFacultativoDTO();
    		    participacionFacultativoDTO.setDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
	        	List<ParticipacionFacultativoDTO> participacionesFacultativas =  participacionfacultativaDN.listarFiltrados(participacionFacultativoDTO);
	         	String estatusCotizacionSlip = request.getSession().getAttribute("estatusCotizacionSlip").toString();
	         	String json = getJsonParticipacionFacultativo(participacionesFacultativas,estatusCotizacionSlip,detalleEndoso);
                 
                   System.out.print(json);
	               response.setContentType("text/json");
				   PrintWriter pw = response.getWriter();
				   pw.write(json);
				   pw.flush();
				   pw.close();
	             } catch (ExcepcionDeAccesoADatos e) {
 	 			e.printStackTrace();
 	 		   } catch (SystemException e) {
 	 		    	e.printStackTrace();
 	 		  }
    	
    }

    private String getJsonParticipacionFacultativo(List<ParticipacionFacultativoDTO> participaciones,String estatus,boolean esEndoso) throws ExcepcionDeAccesoADatos, SystemException{
		MidasJsonBase json = new MidasJsonBase();
	 	CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();
	 	ContactoDN contactoDN = ContactoDN.getInstancia();
	 	ReaseguradorCorredorDTO reaseguradorCorredorDTO = null;
	 	if(participaciones != null && participaciones.size() > 0) {
			for(ParticipacionFacultativoDTO participacion : participaciones) {
			 	
				String nombreReaseguradorCorredor = "";
				String porcentajeParticipacion = "";
				String numCuentaPesos = "";
				String numCuentaDolares = "";
				String contacto = "";
				String comision = "";
				String iconoModificar = null;
				String iconoBorrar = null;
				String iconoAgregar = UtileriasWeb.generarLineaImagenDataGrid(null, "no aplica", null, null);// "/MidasWeb/img/add19.png^No Aplica^javascript:";
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredorDTO())){
				    reaseguradorCorredorDTO = participacion.getReaseguradorCorredorDTO();
				   if(!UtileriasWeb.esCadenaVacia(reaseguradorCorredorDTO.getNombre())){
					   nombreReaseguradorCorredor = reaseguradorCorredorDTO.getNombre();
				   }	   
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getPorcentajeParticipacion()) && !UtileriasWeb.esCadenaVacia(participacion.getPorcentajeParticipacion().toString())){
					porcentajeParticipacion = participacion.getPorcentajeParticipacion().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDTOByIdtccuentabancopesos()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDTOByIdtccuentabancopesos().getNumerocuenta())){
			 		CuentaBancoDTO cuentaBancoDTO = cuentaBancoDN.getPorId(participacion.getCuentaBancoDTOByIdtccuentabancopesos());
			 		numCuentaPesos = cuentaBancoDTO.getNumerocuenta().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDTOByIdtccuentabancodolares()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDTOByIdtccuentabancodolares().getNumerocuenta())){
					CuentaBancoDTO cuentaBancoDTO = cuentaBancoDN.getPorId(participacion.getCuentaBancoDTOByIdtccuentabancodolares());
					numCuentaDolares = cuentaBancoDTO.getNumerocuenta().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getContactoDTO())){
					 ContactoDTO contactoDTO = contactoDN.getPorId(participacion.getContactoDTO());
					 if(!UtileriasWeb.esCadenaVacia(contactoDTO.getNombre())){
						 contacto =  contactoDTO.getNombre(); 
					 }
		 		}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getComision()) && !UtileriasWeb.esObjetoNulo(participacion.getComision())) {
					comision = participacion.getComision() + "%";
				}
				int estatusDetalle = 0;
				if (participacion.getDetalleContratoFacultativoDTO().getEstatus() != null){
					estatusDetalle = participacion.getDetalleContratoFacultativoDTO().getEstatus().intValue();
				}
//				if(!estatus.equals("1") && !estatus.equals("2")) {
				if(estatus.equals("0") && estatusDetalle != DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA && !esEndoso) {
					iconoModificar = "/MidasWeb/img/Edit14.gif^Modificar^javascript: mostrarModificarParticipacionFacultativa("
						+participacion.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo() + ","
						+participacion.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO().toString()+","
						+participacion.getParticipacionFacultativoDTO().toString()+");^_self";
					
					iconoBorrar = "/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestParticipacionFacultativo(2,"
						+participacion.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo() + ","
						+participacion.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO().toString()+","
						+participacion.getParticipacionFacultativoDTO().toString()+");^_self";
					
			    }
				
				// si participacion.getReaseguradorCorredorDTO() == 0 entonces la participacion es de un CORREDOR
				// si participacion.getReaseguradorCorredorDTO() == 1 entonces la participacion es de un REASEGURADOR
				if (reaseguradorCorredorDTO.getTipo().equals("0") && !esEndoso) {
					iconoAgregar = "/MidasWeb/img/add18.png^Agregar mas Reaseguradores^javascript:mostrarGridParticipacionFacultativoCorredor("+participacion.getParticipacionFacultativoDTO()+");^_self";
				}
				else if (reaseguradorCorredorDTO.getTipo().equals("0") && esEndoso) {
					iconoAgregar = "/MidasWeb/img/b_ico_busq.gif^Ver Reaseguradores^javascript:mostrarGridParticipacionFacultativoCorredorEndosoAnterior("+participacion.getParticipacionFacultativoDTO()+");^_self";
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(participacion.getParticipacionFacultativoDTO().toString());
				if(!esEndoso)
					row.setDatos(nombreReaseguradorCorredor,porcentajeParticipacion,numCuentaPesos,numCuentaDolares,contacto,comision,iconoModificar,iconoBorrar,iconoAgregar);
				else
					row.setDatos(nombreReaseguradorCorredor,porcentajeParticipacion,numCuentaPesos,numCuentaDolares,contacto,comision,iconoAgregar);
				json.addRow(row);
					
			}
		}
	 	
		return json.toString();
	}


    public ActionForward mostrarParticipacionFacultativo(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
		    String id = request.getParameter("id");
		    String idTdContratoFacultativo = request.getParameter("idTdContratoFacultativo");
	        if (id != null && !UtileriasWeb.esCadenaVacia(id) && !id.equals("null") && !id.equals("undefined")) {
					ParticipacionFacultativaDN participacionFacultativaDN = ParticipacionFacultativaDN.getInstancia();
					ParticipacionFacultativoDTO participacionFacultativoDTO = new ParticipacionFacultativoDTO(); 
					participacionFacultativoDTO.setParticipacionFacultativoDTO(new BigDecimal(id));
					ParticipacionFacultativaForm participacionFacultativaForm = (ParticipacionFacultativaForm) form;
			        
					try {
						participacionFacultativoDTO = participacionFacultativaDN.getPorId(participacionFacultativoDTO);
						if (participacionFacultativoDTO != null){
							DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = participacionFacultativoDTO.getDetalleContratoFacultativoDTO();
							if (detalleContratoFacultativoDTO != null && detalleContratoFacultativoDTO.getContratoFacultativoDTO_1()!=null && detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo()!=null)
								participacionFacultativaForm.setIdTmContratoFacultativo(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toString());
							poblarParticipacionFacultativaForm(participacionFacultativoDTO, participacionFacultativaForm);
						}else{
							 DetalleContratoFacultativoDN detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
							 DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
							 detalleContratoFacultativoDTO.setContratoFacultativoDTO(new BigDecimal(idTdContratoFacultativo));
							 detalleContratoFacultativoDTO = detalleContratoFacultativoDN.obtenerDetalleContratoFacultativo(detalleContratoFacultativoDTO);
						     participacionFacultativaForm.setIdTdContratoFacultativo(detalleContratoFacultativoDTO.getContratoFacultativoDTO().toString());
						     participacionFacultativaForm.setIdTmContratoFacultativo(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toString());
						}
					} catch (ExcepcionDeAccesoADatos e) {
						UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
					} catch (SystemException e) {
						UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
					}
			}else {
		        ParticipacionFacultativaForm participacionFacultativaForm = (ParticipacionFacultativaForm) form;
		        DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
		        detalleContratoFacultativoDTO.setContratoFacultativoDTO(new BigDecimal(idTdContratoFacultativo));
		        try {
		        	detalleContratoFacultativoDTO = DetalleContratoFacultativoDN.getInstancia().obtenerDetalleContratoFacultativo(detalleContratoFacultativoDTO);
		        	participacionFacultativaForm.setIdTdContratoFacultativo(idTdContratoFacultativo);
		        	participacionFacultativaForm.setIdTmContratoFacultativo(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toBigInteger().toString());
				} catch (ExcepcionDeAccesoADatos e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				}
			}
	        return mapping.findForward(reglaNavegacion);
	}
    
    public void agregarParticipacionFacultativo(ActionMapping mapping, ActionForm form,
    		HttpServletRequest request, HttpServletResponse response) {
    	ParticipacionFacultativaForm participacionFacultativaForm= (ParticipacionFacultativaForm) form;
    	ParticipacionFacultativoDTO participacionFacultativoDTO = new ParticipacionFacultativoDTO();
    	ParticipacionFacultativaDN participacionFacultativoDN = ParticipacionFacultativaDN.getInstancia();
    	
    	try {
    		this.poblarParticipacionFacultativoDTO(participacionFacultativaForm , participacionFacultativoDTO);
    		participacionFacultativoDN.agregar(participacionFacultativoDTO);
    		HttpSession session = request.getSession();
    		session.setAttribute("detalleContratoFacultativoDTO", participacionFacultativoDTO.getDetalleContratoFacultativoDTO());
    		mensajeExitoAgregar(participacionFacultativaForm,"Se Agrego Detalle Contrato Facultativo Correctamente");
    		
    	} catch (SystemException e) {
    		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
    	} catch (ExcepcionDeAccesoADatos e) {
    		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
    	} catch (ExcepcionDeLogicaNegocio e) {
    		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
    	}

    } 
    
    public void validarDetalleParticipacionFacultativa(ActionMapping mapping, ActionForm form,
    		HttpServletRequest request, HttpServletResponse response){
    	HttpSession session = request.getSession();
    	String id = Sistema.EXITO;
    	String mensaje = "";
    	DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = (DetalleContratoFacultativoDTO) session.getAttribute("detalleContratoFacultativoDTO");
    	if (detalleContratoFacultativoDTO != null){
    		boolean esValido;
    		try {
    			esValido = DetalleContratoFacultativoDN.getInstancia().validarDetalleParticipacion(detalleContratoFacultativoDTO);
    			if (!esValido){
    				id = Sistema.ERROR;
    				mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratofacultativo.slip.participacion.novalida");
    			}
    			
    		} catch (SystemException e) {
    			e.printStackTrace();
    		}finally{
    			session.removeAttribute("detalleContratoFacultativoDTO");
    			UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
    		}
    	}
    	
    }
    
    public ActionForward modificarParticipacionFacultativo(ActionMapping mapping, ActionForm form,
    		HttpServletRequest request, HttpServletResponse response) {
    			String reglaNavegacion = Sistema.EXITOSO;
    			ParticipacionFacultativaForm participacionFacultativaForm= (ParticipacionFacultativaForm) form;
    		 	ParticipacionFacultativoDTO participacionFacultativoDTO = new ParticipacionFacultativoDTO();
    			ParticipacionFacultativaDN participacionFacultativoDN = ParticipacionFacultativaDN.getInstancia();
    			try {
	  	  	 		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdTdParticipacionFacultativo())){
    	  	  	 		participacionFacultativoDTO.setParticipacionFacultativoDTO(new BigDecimal(participacionFacultativaForm.getIdTdParticipacionFacultativo()));
    	  	  	 		participacionFacultativoDTO = participacionFacultativoDN.getPorId(participacionFacultativoDTO);
    	  	  	 		participacionFacultativaForm.setIdTdContratoFacultativo(null);
    	  	  	 		this.poblarParticipacionFacultativoDTO(participacionFacultativaForm , participacionFacultativoDTO);
    	  	  	 		participacionFacultativaForm.setIdTdContratoFacultativo(participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO().toString());
    	  	  	 		participacionFacultativoDN.modificar(participacionFacultativoDTO);
    	  	  	 		mensajeExitoAgregar(participacionFacultativaForm,"Se realizaron los cambios al Detalle Contrato Facultativo Correctamente");
	    	  	  	 	HttpSession session = request.getSession();
	    	    		session.setAttribute("detalleContratoFacultativoDTO", participacionFacultativoDTO.getDetalleContratoFacultativoDTO());
	  	  	 		}
    	   	  	} catch (SystemException e) {
    	  			reglaNavegacion = Sistema.NO_DISPONIBLE;
    	  			mensajeExcepcion(participacionFacultativaForm, e);
    	  		} catch (ExcepcionDeAccesoADatos e) {
    	  			reglaNavegacion = Sistema.NO_EXITOSO;
    	  			mensajeExcepcion(participacionFacultativaForm, e);
    	  		}
    	  		return mapping.findForward(reglaNavegacion);
    	} 
    
    private void poblarForm(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO, DetalleContratoFacultativoForm form,String nombreUsuario,Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		if (detalleContratoFacultativoDTO == null)return;
		form.setIdTmContratoFacultativo(detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toBigInteger().toString());
		if (detalleContratoFacultativoDTO.getContratoFacultativoDTO() != null)
			form.setIdTdContratoFacultativo(detalleContratoFacultativoDTO.getContratoFacultativoDTO().toString());
		if(detalleContratoFacultativoDTO.getIdToCobertura()!=null){
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(detalleContratoFacultativoDTO.getIdToCobertura());
			form.setIdToCobertura(detalleContratoFacultativoDTO.getIdToCobertura().toString());
			form.setDescripcionCobertura(coberturaDTO.getNombreComercial());
		}else
			form.setIdToCobertura("");
		if(detalleContratoFacultativoDTO.getIdToSeccion()!=null){
			SeccionDTO seccionDTO = SeccionDN.getInstancia().getPorId(detalleContratoFacultativoDTO.getIdToSeccion());
			form.setDescripcionSeccion(seccionDTO.getDescripcion());
		}else{
			form.setDescripcionSeccion("ND");
		}
		if (detalleContratoFacultativoDTO.getNumeroInciso() != null)
			form.setNumeroInciso(detalleContratoFacultativoDTO.getNumeroInciso().toString());
		else
			form.setNumeroInciso("");
		if (detalleContratoFacultativoDTO.getSumaAsegurada() != null)
			form.setSumaAsegurada(UtileriasWeb.formatoMoneda(detalleContratoFacultativoDTO.getSumaAsegurada()));
		if (detalleContratoFacultativoDTO.getPorcentajeComision() != null)
			form.setPorcentajeComision(detalleContratoFacultativoDTO.getPorcentajeComision().toString());
		if (detalleContratoFacultativoDTO.getDeducibleCobertura() != null)
			form.setDeducibleCobertura(detalleContratoFacultativoDTO.getDeducibleCobertura().toString());
		
		ContratoFacultativoForm contratoFacultativoForm = form.getContratoFacultativoForm();
		BigDecimal idTmContratoFacultativoActual = detalleContratoFacultativoDTO
				.getContratoFacultativoDTO_1().getIdTmContratoFacultativo();
		contratoFacultativoForm.setIdTmContratoFacultativo(idTmContratoFacultativoActual.toPlainString());
		ContratoFacultativoDN.getInstancia(nombreUsuario).obtenerSumaAsegurada(contratoFacultativoForm, detalleContratoFacultativoDTO.getContratoFacultativoDTO_1().getSlipDTO());
		form.setPorcentajeFacultativo(contratoFacultativoForm.getPorcentajeFacultativo());
		
		form.setSumaFacultada(contratoFacultativoForm.getSumaFacultada());
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia("").getPorId(detalleContratoFacultativoDTO.getIdToCotizacion());
		if (!UtileriasWeb.esObjetoNulo(cotizacionDTO)){
			MonedaDTO monedaDTO = MonedaDN.getInstancia().getPorId(cotizacionDTO.getIdMoneda().shortValue());
			if (!UtileriasWeb.esObjetoNulo(monedaDTO))
				form.setMoneda(monedaDTO.getDescripcion());
			else
				form.setMoneda("");
		}
		
		BigDecimal primaNoDevengada = detalleContratoFacultativoDTO.getMontoPrimaNoDevengada() != null ?
				detalleContratoFacultativoDTO.getMontoPrimaNoDevengada() : BigDecimal.ZERO;
		
		form.setPrimaNoDevengada(UtileriasWeb.formatoMoneda(primaNoDevengada));
		
		if (detalleContratoFacultativoDTO.getPrimaTotalCobertura() != null && 
				detalleContratoFacultativoDTO.getPrimaTotalCobertura().compareTo(BigDecimal.ZERO) != 0){
			BigDecimal primaNetaEndoso = detalleContratoFacultativoDTO.getPrimaTotalCobertura().subtract(primaNoDevengada);
			form.setPrimaTotalCobertura(UtileriasWeb.formatoMoneda(primaNetaEndoso));
		}
		else
			form.setPrimaTotalCobertura(UtileriasWeb.formatoMoneda(0d));
		
		if (detalleContratoFacultativoDTO.getPrimaFacultadaCobertura() != null &&
				detalleContratoFacultativoDTO.getPrimaFacultadaCobertura().compareTo(BigDecimal.ZERO) != 0){
			BigDecimal primaFacultadaEndoso = detalleContratoFacultativoDTO.getPrimaFacultadaCobertura().subtract(primaNoDevengada);
			form.setPrimaFacultadaCobertura(UtileriasWeb.formatoMoneda(primaFacultadaEndoso));
		}
		else
			form.setPrimaFacultadaCobertura(UtileriasWeb.formatoMoneda(0d));
		
		form.setMontoPrimaAdicional(UtileriasWeb.formatoMoneda(detalleContratoFacultativoDTO.getMontoPrimaAdicional() != null ?
				detalleContratoFacultativoDTO.getMontoPrimaAdicional() : BigDecimal.ZERO));
		
		//Datos del plan de pagos de la cobertura
		
		List<PlanPagosCoberturaDTO> listaPlanPagosCobertura = 
		PlanPagosCoberturaDN.getInstancia(nombreUsuario).buscarPorPropiedad("detalleContratoFacultativoDTO.contratoFacultativoDTO", 
																			detalleContratoFacultativoDTO.getContratoFacultativoDTO());
		if(listaPlanPagosCobertura.size()==1){
			Byte estatusAutorizacionPlanPagos = listaPlanPagosCobertura.get(0).getEstatusAutorizacionPlanPagos();
			form.setEstatusAutorizacionPlanPagos(estatusAutorizacionPlanPagos.toString());
		}
				
		form.setMostrarBotonAutorizarPlanPagos("0");		
		for (Rol rol : usuario.getRoles()) {
			if (rol.getDescripcion().equals(Sistema.ROL_DIRECTOR_REASEGURO)){
				form.setMostrarBotonAutorizarPlanPagos("1");
				break;
			}
		}			
    }
    
    	public void poblarParticipacionFacultativaForm(ParticipacionFacultativoDTO participacionFacultativoDTO, ParticipacionFacultativaForm form){
    		if (participacionFacultativoDTO == null)return;
    		
    		if (participacionFacultativoDTO.getParticipacionFacultativoDTO() != null) 
    			form.setIdTdParticipacionFacultativo(participacionFacultativoDTO.getParticipacionFacultativoDTO().toString());
    		
    		if (participacionFacultativoDTO.getDetalleContratoFacultativoDTO()!=null && participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO()!=null)
    			form.setIdTdContratoFacultativo(participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO().toString());
    		
    		if (participacionFacultativoDTO.getDetalleContratoFacultativoDTO()!=null 
    				&&	participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1()!=null
    				&& participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo()!=null)
    			form.setIdTmContratoFacultativo(participacionFacultativoDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toString());
    		
    		if (participacionFacultativoDTO.getReaseguradorCorredorDTO().getTipo()!=null)
    			form.setTipoParticipante(participacionFacultativoDTO.getReaseguradorCorredorDTO().getTipo());
    		
    		if(participacionFacultativoDTO.getReaseguradorCorredorDTO()!=null && participacionFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor()!=null)
    			form.setIdParticipante(participacionFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().toString());
    		
    		if (participacionFacultativoDTO.getCuentaBancoDTOByIdtccuentabancopesos()!=null)
    			form.setIdCuentaPesos(participacionFacultativoDTO.getCuentaBancoDTOByIdtccuentabancopesos().getIdtccuentabanco().toString());
    		
    		if (participacionFacultativoDTO.getCuentaBancoDTOByIdtccuentabancodolares()!=null)
    			form.setIdCuentaDolares(participacionFacultativoDTO.getCuentaBancoDTOByIdtccuentabancodolares().getIdtccuentabanco().toString());

    		if (participacionFacultativoDTO.getContactoDTO()!=null && participacionFacultativoDTO.getContactoDTO().getIdtccontacto()!=null)
    			form.setIdContacto(participacionFacultativoDTO.getContactoDTO().getIdtccontacto().toString());
    		
    		if (participacionFacultativoDTO.getPorcentajeParticipacion()!=null)
    			form.setPorcentajeParticipacion(participacionFacultativoDTO.getPorcentajeParticipacion().toString());
    		
    		if(participacionFacultativoDTO.getComision() != null)
    			form.setComision(participacionFacultativoDTO.getComision().toString());
    	}
    	
protected void poblarParticipacionFacultativoDTO(ParticipacionFacultativaForm participacionFacultativaForm, ParticipacionFacultativoDTO participacionFacultativoDTO ) throws ExcepcionDeAccesoADatos, SystemException{
        Double id;
		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdTdContratoFacultativo())) {
			DetalleContratoFacultativoDN detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
			id = Double.valueOf(participacionFacultativaForm.getIdTdContratoFacultativo());
			DetalleContratoFacultativoDTO detalleContratoFacultativoDTO = new DetalleContratoFacultativoDTO();
			detalleContratoFacultativoDTO.setContratoFacultativoDTO(BigDecimal.valueOf(id));
			detalleContratoFacultativoDTO = detalleContratoFacultativoDN.getCotizacionFacultativa(BigDecimal.valueOf(id));
			participacionFacultativoDTO.setDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
		}

		if (!StringUtil.isEmpty(participacionFacultativaForm.getPorcentaceRetencion())){
			id = Double.valueOf(participacionFacultativaForm.getPorcentaceRetencion());
			participacionFacultativoDTO.setPorcentajeRetencion(id);
		}
		
		if (!StringUtil.isEmpty(participacionFacultativaForm.getPorcentajeParticipacion())){
			BigDecimal porcentajeParticipacion = new BigDecimal(participacionFacultativaForm.getPorcentajeParticipacion(),new MathContext(12,RoundingMode.HALF_UP));
//			id = Double.valueOf(participacionFacultativaForm.getPorcentajeParticipacion());
			participacionFacultativoDTO.setPorcentajeParticipacion(porcentajeParticipacion);
		}
		if (!StringUtil.isEmpty(participacionFacultativaForm.getPrimerMaretencionl())){
			id = Double.valueOf(participacionFacultativaForm.getPrimerMaretencionl());
			participacionFacultativoDTO.setPrirmaretencion(id);
		}
		if (!StringUtil.isEmpty(participacionFacultativaForm.getComision())){
			BigDecimal porcentajeComision = new BigDecimal(participacionFacultativaForm.getComision(),new MathContext(12,RoundingMode.HALF_UP));
//			id = Double.valueOf(participacionFacultativaForm.getComision());
			participacionFacultativoDTO.setComision(porcentajeComision);
		}
	 	
		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdParticipante())) {
			ReaseguradorCorredorDN  reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
			id = Double.valueOf(participacionFacultativaForm.getIdParticipante());
		    ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		    reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(BigDecimal.valueOf(id));
		    reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
		 	participacionFacultativoDTO.setReaseguradorCorredorDTO(reaseguradorCorredorDTO);
		}
		
		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdCuentaPesos())) {
			id = Double.valueOf(participacionFacultativaForm.getIdCuentaPesos());
			CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();
			cuentaBancoDTO.setIdtccuentabanco(BigDecimal.valueOf(id));
			cuentaBancoDTO = cuentaBancoDN.getPorId(cuentaBancoDTO);
		 	participacionFacultativoDTO.setCuentaBancoDTOByIdtccuentabancopesos(cuentaBancoDTO);
		}
		
		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdCuentaDolares())) {
			id = Double.valueOf(participacionFacultativaForm.getIdCuentaDolares());
			CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();
			cuentaBancoDTO.setIdtccuentabanco(BigDecimal.valueOf(id));
			cuentaBancoDTO = cuentaBancoDN.getPorId(cuentaBancoDTO);
		 	participacionFacultativoDTO.setCuentaBancoDTOByIdtccuentabancodolares(cuentaBancoDTO);
		 }
 		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdContacto())) {
			id = Double.valueOf(participacionFacultativaForm.getIdContacto());
			ContactoDN contactoDN = ContactoDN.getInstancia(); 
		  	ContactoDTO contactoDTO = new ContactoDTO();
		  	contactoDTO.setIdtccontacto(BigDecimal.valueOf(id));
		  	contactoDTO = contactoDN.getPorId(contactoDTO);
		   participacionFacultativoDTO.setContactoDTO(contactoDTO);
		}
 		if (!StringUtil.isEmpty(participacionFacultativaForm.getTipoParticipante())) {
	 		participacionFacultativoDTO.setTipo(participacionFacultativaForm.getTipoParticipante());
		}
 		participacionFacultativoDTO.setPrirmaretencion(Double.valueOf(0));
 		participacionFacultativoDTO.setPorcentajeRetencion(Double.valueOf(0));
 		if (!UtileriasWeb.esCadenaVacia(participacionFacultativaForm.getComision())){
 			BigDecimal porcentajeComision = new BigDecimal(participacionFacultativaForm.getComision(),new MathContext(12,RoundingMode.HALF_UP));
 			participacionFacultativoDTO.setComision(porcentajeComision);
 		}
 		else{
 			participacionFacultativoDTO.setComision(BigDecimal.ZERO);
 		}
 			
    }	
    
      public ActionForward mostrarParticipacionFacultativoCorredor(ActionMapping mapping, ActionForm form,
    		    HttpServletRequest request, HttpServletResponse response) {
    			String reglaNavegacion = Sistema.EXITOSO;
    			 ParticipacionFacultativaDN participacionFacultativoDN = ParticipacionFacultativaDN.getInstancia();
    			 ReaseguradorCorredorDN reaseguradorCorredorDN= ReaseguradorCorredorDN.getInstancia();
    		 	 String id ="";
    		 	 HttpSession session= request.getSession(true);
    			 String idParticipacionFacultativo =(String) session.getAttribute("idParticipacionFacultativo");
    		     try {
					ParticipacionFacultativoDTO  participacionFacultativaDTO =  participacionFacultativoDN.getParticipacionFacultativa(BigDecimal.valueOf((Integer.valueOf(idParticipacionFacultativo))));
				    ReaseguradorCorredorDTO reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(participacionFacultativaDTO.getReaseguradorCorredorDTO());
				    ParticipacionFacultativaForm participacionFacultativaForm = (ParticipacionFacultativaForm) form;
	    	        participacionFacultativaForm.setIdTdParticipacionFacultativo(idParticipacionFacultativo);
	    	        ReaseguradorCorredorForm reaseguradorCorredorForm = new ReaseguradorCorredorForm();
	    	        reaseguradorCorredorForm.setCnfs(reaseguradorCorredorDTO.getCnfs());
	    	        reaseguradorCorredorForm.setIdTcReaseguradorCorredor(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().toString());
	    	        reaseguradorCorredorForm.setNombre(reaseguradorCorredorDTO.getNombre());
	    	        reaseguradorCorredorForm.setNombreCorto(reaseguradorCorredorDTO.getNombrecorto());
	    	        reaseguradorCorredorForm.setIdTcReaseguradorCorredor(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().toString());
	    	        participacionFacultativaForm.setReaseguradorCorredorForm(reaseguradorCorredorForm);
	    	        participacionFacultativaForm.setIdParticipante(reaseguradorCorredorDTO.getIdtcreaseguradorcorredor().toString());
	    	        participacionFacultativaForm.setPorcentajeParticipacion(participacionFacultativaDTO.getPorcentajeParticipacion().toString());
	    	        participacionFacultativaForm.setIdTmContratoFacultativo(participacionFacultativaDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo().toString());
	    	        participacionFacultativaForm.setIdTdContratoFacultativo(participacionFacultativaDTO.getDetalleContratoFacultativoDTO().getContratoFacultativoDTO().toString());
	    	        if (request.getParameter("id") != null && !request.getParameter("id").equals("null")){
	    	        	id = request.getParameter("id");
	    	    		ParticipacionCorredorFacultativaDN participacionCorredorFacultativaDN = ParticipacionCorredorFacultativaDN.getInstancia();
	  					ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO = new ParticipacionCorredorFacultativoDTO(); 
	  					participacionCorredorFacultativoDTO.setIdTdParticapacionCorredorFac(new BigDecimal(id));
	  					participacionCorredorFacultativoDTO = participacionCorredorFacultativaDN.getPorId(participacionCorredorFacultativoDTO);
	  			 		poblarParticipacionCorredorFacultativaForm(participacionCorredorFacultativoDTO, participacionFacultativaForm);
	  		          }
	    	        participacionFacultativaForm.setTotalReasegurador(this.validarPorcentajeParticipacionCorredor(participacionFacultativaForm).toString());	        
    	        } catch (NumberFormatException e) {
		 			e.printStackTrace();
				} catch (ExcepcionDeAccesoADatos e) {
		 			e.printStackTrace();
				} catch (SystemException e) {
		 			e.printStackTrace();
				}
    	         
    	        return mapping.findForward(reglaNavegacion);
    	}
	  
	        public void poblarParticipacionCorredorFacultativaForm(ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO, ParticipacionFacultativaForm form) throws ExcepcionDeAccesoADatos, SystemException{
  		if (participacionCorredorFacultativoDTO == null)return;
  	
  		if (participacionCorredorFacultativoDTO.getIdTdParticapacionCorredorFac() != null) {
  			ParticipacionCorredorFacultativaForm participacionCorredorFacultativaForm = new ParticipacionCorredorFacultativaForm(); 
  			participacionCorredorFacultativaForm.setIdTdParticipacionCorredorFac(participacionCorredorFacultativoDTO.getIdTdParticapacionCorredorFac().toString());
  			form.setParticipacionCorredorFacultativaForm(participacionCorredorFacultativaForm);
  		}
  		
  		
  		if (participacionCorredorFacultativoDTO.getParticipacionFacultativoDTO() != null) 
  			form.setIdTdParticipacionFacultativo(participacionCorredorFacultativoDTO.getParticipacionFacultativoDTO().getParticipacionFacultativoDTO().toString());
  	 	
  		if(participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO()!=null && participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor()!=null){
  			form.setIdParticipante(participacionCorredorFacultativoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor().toString()); 
  		}	
  	 	 
    		
  		if (participacionCorredorFacultativoDTO.getPorcentajeParticipacion()!=null)
  			form.setPorcentajeParticipacion(participacionCorredorFacultativoDTO.getPorcentajeParticipacion().toString());
  		
  		
  	}
  	

      public ActionForward modificarParticipacionFacultativoCorredor(ActionMapping mapping, ActionForm form,
      		HttpServletRequest request, HttpServletResponse response) {
      			String reglaNavegacion = Sistema.EXITOSO;
      			ParticipacionFacultativaForm participacionFacultativaForm= (ParticipacionFacultativaForm) form;
      		 	ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO = new ParticipacionCorredorFacultativoDTO();
      		 	ParticipacionCorredorFacultativaDN participacionCorredorFacultativoDN = ParticipacionCorredorFacultativaDN.getInstancia();
      		 	try {
	  	  	 		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdTdParticipacionFacultativo())){
	  	  	 		    participacionCorredorFacultativoDTO.setIdTdParticapacionCorredorFac(BigDecimal.valueOf(Integer.valueOf((participacionFacultativaForm.getParticipacionCorredorFacultativaForm().getIdTdParticipacionCorredorFac()))));
    	  	  	 		participacionCorredorFacultativoDTO = participacionCorredorFacultativoDN.getPorId(participacionCorredorFacultativoDTO);
    	  	  	 		this.poblarParticipacionFacultativoCorredorDTO(participacionFacultativaForm , participacionCorredorFacultativoDTO,"Modificar");
    	  	  	 		//participacionFacultativaForm.setIdTdContratoFacultativo(participacionCorredorFacultativoDTO.getParticipacionFacultativoDTO().getParticipacionFacultativoDTO().toString());
    	  	  	 	    participacionCorredorFacultativoDN.modificar(participacionCorredorFacultativoDTO);
    	  	  	 		mensajeExitoAgregar(participacionFacultativaForm,"Se realizaron los cambios al Detalle Contrato Facultativo Correctamente");
	  	  	 		}
      	   	  	} catch (SystemException e) {
      	  			reglaNavegacion = Sistema.NO_DISPONIBLE;
      	  			mensajeExcepcion(participacionFacultativaForm, e);
      	  		} catch (ExcepcionDeAccesoADatos e) {
      	  			reglaNavegacion = Sistema.NO_EXITOSO;
      	  			mensajeExcepcion(participacionFacultativaForm, e);
      	  		} 
      	  		return mapping.findForward(reglaNavegacion);
      	} 
      
      public ActionForward agregarParticipacionFacultativoCorredor(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	  String reglaNavegacion = Sistema.EXITOSO;
    	  ParticipacionFacultativaForm participacionFacultativaForm= (ParticipacionFacultativaForm) form;
    	  ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO = new ParticipacionCorredorFacultativoDTO();
    	  ParticipacionCorredorFacultativaDN ParticipacionCorredorFacultativoDN = ParticipacionCorredorFacultativaDN.getInstancia();
    	  try {
    		  this.poblarParticipacionFacultativoCorredorDTO(participacionFacultativaForm , participacionCorredorFacultativoDTO,"Agregar");
    		  ParticipacionCorredorFacultativoDN.agregar(participacionCorredorFacultativoDTO);
    		  mensajeExitoAgregar(participacionFacultativaForm,"Se Agrego Detalle Contrato Facultativo Correctamente");
    	  } catch (SystemException e) {
    		  reglaNavegacion = Sistema.NO_DISPONIBLE;
    		  mensajeExcepcion(participacionFacultativaForm, e);
    	  } catch (ExcepcionDeAccesoADatos e) {
    		  reglaNavegacion = Sistema.NO_EXITOSO;
    		  mensajeExcepcion(participacionFacultativaForm, e);
    	  } catch (ExcepcionDeLogicaNegocio e) {
    		  reglaNavegacion = Sistema.NO_EXITOSO;
    		  mensajeExcepcion(participacionFacultativaForm, e);
    	  } 
    	  return mapping.findForward(reglaNavegacion);
      }    
     
      protected BigDecimal validarPorcentajeParticipacionCorredor(ParticipacionFacultativaForm participacionFacultativaForm) throws ExcepcionDeAccesoADatos, SystemException{
//    	   String mensajeValida = "0";// 1 = el porcentaje es menor que la suma de los reaseguradores
    	                              //2= el porcentaje es mayor que la suma de los reaseguradore
    	                              //3= el porcentaje es igual que la suma de los reaseguradore
    	  String idParticipacionFac;
    	  BigDecimal porcentajeFacultativo = BigDecimal.ZERO;
    	  BigDecimal sumaRestante = BigDecimal.ZERO;
    	  
    	  BigDecimal sumaReaseguroCorredor = BigDecimal.ZERO;
    	  //if(participacionFacultativaForm.getPorcentajeParticipacion() != null){
    	//	  SumaReaseguroCorredor = Double.valueOf(participacionFacultativaForm.getPorcentajeParticipacion());  
    	 // }
    	   
    	  //System.out.println("la suma es: "+SumaReaseguroCorredor);
    	  ParticipacionFacultativoDTO participacionFacultativoDTO = new ParticipacionFacultativoDTO();
    	   if(participacionFacultativaForm.getIdTdParticipacionFacultativo()!= null){
    		   idParticipacionFac = participacionFacultativaForm.getIdTdParticipacionFacultativo();
    		   ParticipacionFacultativaDN participacionFacultativaDN = ParticipacionFacultativaDN.getInstancia();
    		   participacionFacultativoDTO.setParticipacionFacultativoDTO(BigDecimal.valueOf(Integer.valueOf(idParticipacionFac)));
    		   participacionFacultativoDTO = participacionFacultativaDN.getPorId(participacionFacultativoDTO);
    		   porcentajeFacultativo = participacionFacultativoDTO.getPorcentajeParticipacion();
    		   ParticipacionCorredorFacultativaDN participacionCorredorFacultativaDN = ParticipacionCorredorFacultativaDN.getInstancia();
       	       List<ParticipacionCorredorFacultativoDTO> participacionCorredorFacultativoDTOList = participacionCorredorFacultativaDN.getPorPropiedad("participacionFacultativoDTO.participacionFacultativoDTO", participacionFacultativoDTO.getParticipacionFacultativoDTO());
       	       Iterator<ParticipacionCorredorFacultativoDTO> iteratorList = participacionCorredorFacultativoDTOList.iterator();   	   
       	     while(iteratorList.hasNext()){
       		     ParticipacionCorredorFacultativoDTO corredorFacultativo = iteratorList.next();
       		     sumaReaseguroCorredor = sumaReaseguroCorredor.add(corredorFacultativo.getPorcentajeParticipacion());
       	      }
    	   }
      	  sumaRestante = porcentajeFacultativo.subtract(sumaReaseguroCorredor);
     	  return sumaRestante;
      }
      
      protected void poblarParticipacionFacultativoCorredorDTO(ParticipacionFacultativaForm participacionFacultativaForm, ParticipacionCorredorFacultativoDTO participacionCorredorFacultativoDTO,String tipo) throws ExcepcionDeAccesoADatos, SystemException{
          Double id;
        if(tipo == "Modificar"){ 
          if (!StringUtil.isEmpty(participacionFacultativaForm.getParticipacionCorredorFacultativaForm().getIdTdParticipacionCorredorFac())) {
    			id = Double.valueOf(participacionFacultativaForm.getParticipacionCorredorFacultativaForm().getIdTdParticipacionCorredorFac());
    		  	participacionCorredorFacultativoDTO.setIdTdParticapacionCorredorFac(BigDecimal.valueOf(id));
    		}
        }   
  		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdTdParticipacionFacultativo())) {
  			ParticipacionFacultativaDN participacionFacultativoDN = ParticipacionFacultativaDN.getInstancia();
  			id = Double.valueOf(participacionFacultativaForm.getIdTdParticipacionFacultativo());
  			ParticipacionFacultativoDTO participacionFacultativoDTO  = new ParticipacionFacultativoDTO ();
  			participacionFacultativoDTO.setParticipacionFacultativoDTO(BigDecimal.valueOf(id));
  			participacionFacultativoDTO = participacionFacultativoDN.getParticipacionFacultativa(participacionFacultativoDTO.getParticipacionFacultativoDTO());
  		 	participacionCorredorFacultativoDTO.setParticipacionFacultativoDTO(participacionFacultativoDTO);
  		}
   		if (!StringUtil.isEmpty(participacionFacultativaForm.getIdParticipante())) {
   			ReaseguradorCorredorDN  reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
			id = Double.valueOf(participacionFacultativaForm.getIdParticipante());
		    ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
		    reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(BigDecimal.valueOf(id));
		    reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
		    participacionCorredorFacultativoDTO.setReaseguradorCorredorDTO(reaseguradorCorredorDTO);
 		}
  		if (!StringUtil.isEmpty(participacionFacultativaForm.getPorcentajeParticipacion())){
//			id = Double.valueOf(participacionFacultativaForm.getPorcentajeParticipacion());
//			participacionCorredorFacultativoDTO.setPorcentajeParticipacion(new BigDecimal(id));
			participacionCorredorFacultativoDTO.setPorcentajeParticipacion(UtileriasWeb.regresaBigDecimal(participacionFacultativaForm.getPorcentajeParticipacion()));
  		}
  	 }
      public void cargarParticipacionFacultativoCorredor(ActionMapping mapping, ActionForm form,
    			HttpServletRequest request, HttpServletResponse response) throws IOException, SystemException, ExcepcionDeAccesoADatos {
    				String idParticipacionFacultativo = "";
    				idParticipacionFacultativo = request.getParameter("id");
    				String esEndoso = request.getParameter("esEndoso");
    				boolean detalleEndoso = (esEndoso != null && esEndoso.equals("true"));
    				if(idParticipacionFacultativo != null){
    				 	HttpSession session  = request.getSession();
    				 	session.setAttribute("idParticipacionFacultativo", idParticipacionFacultativo);
    				 }
    				 String estatusCotizacionSlip = request.getSession().getAttribute("estatusCotizacionSlip").toString();
    				 ParticipacionCorredorFacultativaDN ParticipacionCorredorFacultativoDN = ParticipacionCorredorFacultativaDN.getInstancia();
    				 List<ParticipacionCorredorFacultativoDTO> participacionesFacultativasCorredor = ParticipacionCorredorFacultativoDN.getPorPropiedad("participacionFacultativoDTO.participacionFacultativoDTO",BigDecimal.valueOf(Integer.valueOf(idParticipacionFacultativo)));
    				 String json = getJsonParticipacionFacultativoCorredor(participacionesFacultativasCorredor,estatusCotizacionSlip,detalleEndoso);	
    		         
    				 System.out.print(json);
    				 response.setContentType("text/json");
    				 PrintWriter pw = response.getWriter();
    				 pw.write(json);
    				 pw.flush();
    				 pw.close();
    		}
    		      
    		  
    		private String getJsonParticipacionFacultativoCorredor(List<ParticipacionCorredorFacultativoDTO> participaciones,String estatus,boolean esEndoso) throws ExcepcionDeAccesoADatos, SystemException{
    			MidasJsonBase json = new MidasJsonBase();
    			ReaseguradorCorredorDN  reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
    			ReaseguradorCorredorDTO reaseguradorCorredorDTO = null;
    			if(participaciones != null && participaciones.size() > 0) {
    				for(ParticipacionCorredorFacultativoDTO participacion : participaciones) {

    					String nombreReaseguradorCorredor = "";
    					String nombreCortoReaseguradorCorredor = "";
    					String cnFs = "";
    					String porcentajeParticipacion = "";
    					String iconoModificar = null;
    					String iconoBorrar = null;

    					if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredorDTO())){
    						reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(participacion.getReaseguradorCorredorDTO());
    						if(!UtileriasWeb.esCadenaVacia(reaseguradorCorredorDTO.getNombre())) {
    							nombreReaseguradorCorredor = reaseguradorCorredorDTO.getNombre();
    						}	   
    					}

    					if (!UtileriasWeb.esCadenaVacia(reaseguradorCorredorDTO.getNombrecorto())){
    						nombreCortoReaseguradorCorredor = reaseguradorCorredorDTO.getNombrecorto();
    					}

    					if (!UtileriasWeb.esCadenaVacia(reaseguradorCorredorDTO.getCnfs())){
    						cnFs = reaseguradorCorredorDTO.getCnfs();
    					}

    					if (!UtileriasWeb.esObjetoNulo(participacion.getPorcentajeParticipacion()) && !UtileriasWeb.esObjetoNulo(participacion.getPorcentajeParticipacion())) {
    						porcentajeParticipacion = participacion.getPorcentajeParticipacion() + "%";
    					}

    					int estatusDetalle = 0;
    					if (participacion.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getEstatus() != null){
    						estatusDetalle = participacion.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getEstatus().intValue();
    					}
    					
//    					if (!estatus.equals("1") && !estatus.equals("2")) {
    					if (estatus.equals("0")  && estatusDetalle != DetalleContratoFacultativoDTO.ESTATUS_COBERTURA_CANCELADA && !esEndoso){
    						iconoModificar = "/MidasWeb/img/Edit14.gif^Modificar^javascript: mostrarModificarParticipacionFacultativa("+participacion.getParticipacionFacultativoDTO().getParticipacionFacultativoDTO().toString()+","+0+","+participacion.getIdTdParticapacionCorredorFac().toString()+");^_self";
    						iconoBorrar = "/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestParticipacionCorredorFacultativo(2,"+ participacion.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getContratoFacultativoDTO_1().getIdTmContratoFacultativo() +","+ participacion.getParticipacionFacultativoDTO().getDetalleContratoFacultativoDTO().getContratoFacultativoDTO() +","+participacion.getParticipacionFacultativoDTO().getParticipacionFacultativoDTO().toString()+","+participacion.getIdTdParticapacionCorredorFac().toString()+");^_self"; 
    					}

    					MidasJsonRow row = new MidasJsonRow();
    					row.setId(participacion.getIdTdParticapacionCorredorFac().toString());
    					if(!esEndoso)
    						row.setDatos(nombreReaseguradorCorredor,nombreCortoReaseguradorCorredor,cnFs,porcentajeParticipacion,iconoModificar,iconoBorrar);
    					else
    						row.setDatos(nombreReaseguradorCorredor,nombreCortoReaseguradorCorredor,cnFs,porcentajeParticipacion);
    					json.addRow(row);

    				}
    			}

    			return json.toString();
    		}
    		
	public ActionForward copiarParticipacionesCoberturaEndosoAnterior(
			ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			ContratoFacultativoDN contratoFacultativoDN =
				ContratoFacultativoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			BigDecimal id = new BigDecimal(request.getParameter("id"));
			contratoFacultativoDN
					.copiarParticipacionesFacultativoCoberturaEndosoAnterior(id);
			String llave = "contratofacultativo.configuracion.cobertura.mantenerParticipantes.exito";
			UtileriasWeb.imprimeMensajeXML(request, response, Sistema.EXITO, llave);
		}
		catch (RuntimeException ex) {
			LogDeMidasWeb.log("No se pudieron crear las participaciones.", 
					Level.SEVERE, ex);
			UtileriasWeb.imprimeMensajeXML(request, response, 
					Sistema.ERROR, "mensaje.excepcion.sistema.nodisponible");
		}
		return null;
	}
}
