package mx.com.afirme.midas.contratofacultativo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ConfigurarPorcentajesFacultativoForm extends MidasBaseForm {

	private static final long serialVersionUID = -6058807279476135392L;
	private String idToSlip;
	private String porcentajeRetencion;
	private String porcentajeCuotaParte;
	private String porcentajePrimerExcedente;
	private String porcentajeFacultativo;
	private String idTmContratoFacultativo;
	
	/* Getters & Setters */
	public String getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public String getIdToSlip() {
		return idToSlip;
	}
	public void setIdToSlip(String idToSlip) {
		this.idToSlip = idToSlip;
	}
	public void setPorcentajeRetencion(String porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public String getPorcentajeCuotaParte() {
		return porcentajeCuotaParte;
	}
	public void setPorcentajeCuotaParte(String porcentajeCuotaParte) {
		this.porcentajeCuotaParte = porcentajeCuotaParte;
	}
	public String getPorcentajePrimerExcedente() {
		return porcentajePrimerExcedente;
	}
	public void setPorcentajePrimerExcedente(String porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}
	public String getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(String porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public String getIdTmContratoFacultativo() {
		return idTmContratoFacultativo;
	}
	public void setIdTmContratoFacultativo(String idTmContratoFacultativo) {
		this.idTmContratoFacultativo = idTmContratoFacultativo;
	}  
	
}
