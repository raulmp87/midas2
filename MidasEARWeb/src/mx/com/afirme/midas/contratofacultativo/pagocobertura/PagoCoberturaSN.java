package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PagoCoberturaSN {
	private PagoCoberturaFacadeRemote beanRemoto;
	
	public PagoCoberturaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PagoCoberturaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto PagoCoberturaFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public void eliminar(PagoCoberturaDTO pagoCobertura){
		try{
			beanRemoto.delete(pagoCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al eliminar el pago de la cobertura facultada.");
		}
	}
	
	public List<PagoCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valorPropiedad){
		try{
			return beanRemoto.findByProperty(nombrePropiedad, valorPropiedad);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al buscar por propiedad el pago de la cobertura facultada.");
		}
	}
}
