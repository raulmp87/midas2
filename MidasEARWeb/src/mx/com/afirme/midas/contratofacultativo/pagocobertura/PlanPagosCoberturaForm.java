package mx.com.afirme.midas.contratofacultativo.pagocobertura;

public class PlanPagosCoberturaForm {
	private String numeroExhibiciones;
	private String estatusAutorizacionPlanPagos;
	
	public void setNumeroExhibiciones(String numeroExhibiciones) {
		this.numeroExhibiciones = numeroExhibiciones;
	}
	
	public String getNumeroExhibiciones() {
		return numeroExhibiciones;
	}
	
	public void setEstatusAutorizacionPlanPagos(
			String estatusAutorizacionPlanPagos) {
		this.estatusAutorizacionPlanPagos = estatusAutorizacionPlanPagos;
	}
	
	public String getEstatusAutorizacionPlanPagos() {
		return estatusAutorizacionPlanPagos;
	}
}
