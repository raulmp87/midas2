package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class PlanPagosCoberturaSN {
private PlanPagosCoberturaFacadeRemote beanRemoto;
	
	public PlanPagosCoberturaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PlanPagosCoberturaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto PlanPagosCoberturaFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public PlanPagosCoberturaDTO agregar(PlanPagosCoberturaDTO planPagosCobertura){
		try{
			return beanRemoto.save(planPagosCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al agregar el plan de pagos de la cobertura facultada.");
		}
	}
	
	public PlanPagosCoberturaDTO modificar(PlanPagosCoberturaDTO planPagosCobertura){
		try{
			return beanRemoto.update(planPagosCobertura);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al modificar el plan de pagos de la cobertura facultada.");
		}
	}
	
	public List<PlanPagosCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valorPropiedad){
		try{
			return beanRemoto.findByProperty(nombrePropiedad, valorPropiedad);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al buscar por propiedad el plan de pagos de la cobertura facultada.");
		}
	}
	
	public void modificarPagosCobertura(PlanPagosCoberturaDTO planPagosCobertura, 
			List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos,
			boolean omitirAutorizacion, String usuarioModificacion){
		try{
			beanRemoto.modificarPagosCobertura(planPagosCobertura.getIdToPlanPagosCobertura(), pagos, omitirAutorizacion, usuarioModificacion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al modificar los pagos de la cobertura facultada.");
		}
	}
	
	public void distribuirPagosPorReasegurador(BigDecimal idTdContratoFacultativo)throws ExcepcionDeLogicaNegocio{
		try{
			beanRemoto.distribuirPagosPorReasegurador(idTdContratoFacultativo);
		} catch (Exception e) {
			throw new ExcepcionDeLogicaNegocio("PlanPagosCoberturaSN",e.getMessage());
		}
	}
	
	public List<PagoCoberturaDecoradoDTO> listarExhibicionesAgrupadasPorCobertura(String idsEstadoCuenta){
		try{
			return beanRemoto.listarExhibicionesAgrupadasPorCobertura(idsEstadoCuenta);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al listar exhibiciones agrupadas por cobertura (idsEstadoCuenta = {"+idsEstadoCuenta+"})");
		}		
	}
	
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesCoberturasReaseguradores(String idsCoberturasReaseguradores){
		try{
			return beanRemoto.listarExhibicionesCoberturasReaseguradores(idsCoberturasReaseguradores);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al listar exhibiciones por cobertura-reasegurador (idsCoberturasReaseguradores = {"+idsCoberturasReaseguradores+"})");
		}		
	}
	
	public PagoCoberturaReaseguradorDTO obtenerExhibicionPorId(PagoCoberturaReaseguradorId id){
		try{
			return beanRemoto.obtenerExhibicionPorId(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtener exhibiciones por id " +
											  "(id = [idToPlanPagosCobertura="+id.getIdToPlanPagosCobertura()+", " +
											  		 "numeroExhibicion="+id.getNumeroExhibicion()+", " +
											  		 "idReasegurador="+id.getIdReasegurador()+"])");
		}
	}
	
	public PagoCoberturaReaseguradorDTO actualizarExhibicion(PagoCoberturaReaseguradorDTO exhibicion){
		try{
			return beanRemoto.actualizarExhibicion(exhibicion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al actualizar exhibicion " +
											  "(id = [idToPlanPagosCobertura="+exhibicion.getId().getIdToPlanPagosCobertura()+", " +
											  		 "numeroExhibicion="+exhibicion.getId().getNumeroExhibicion()+", " +
											  		 "idReasegurador="+exhibicion.getId().getIdReasegurador()+"])");
		}
	}
	
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesDecoradas(String idsExhibiciones){
		try{
			return beanRemoto.listarExhibicionesDecoradas(idsExhibiciones);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al listar exhibiciones decoradas (idsExhibiciones = {"+idsExhibiciones+"})");
		}
	}
}
