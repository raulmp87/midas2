package mx.com.afirme.midas.contratofacultativo.pagocobertura;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDN;
import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class PlanPagosCoberturaDN {
	private static PlanPagosCoberturaDN INSTANCIA = new PlanPagosCoberturaDN();
	private static String nombreUsuario;
	
	public static PlanPagosCoberturaDN getInstancia(String nombreUsuario){
		PlanPagosCoberturaDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
	}		
	
	public List<PagoCoberturaDTO> consultaPagosCobertura(BigDecimal idTdContratoFacultativo) throws ExcepcionDeAccesoADatos, SystemException{		
		DetalleContratoFacultativoDN detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
		DetalleContratoFacultativoDTO detalleContratoFacultativo = 
			detalleContratoFacultativoDN.getCotizacionFacultativa(idTdContratoFacultativo);
				
		List<PlanPagosCoberturaDTO> listaPlanPagosCobertura = 
		this.buscarPorPropiedad("detalleContratoFacultativoDTO.contratoFacultativoDTO", idTdContratoFacultativo);
				
		if(listaPlanPagosCobertura == null || listaPlanPagosCobertura.isEmpty()){
		//1) No existe el plan de pagos
			//a) Se genera el plan de pagos
			this.crearPlanPagosCobertura(detalleContratoFacultativo);			
		}else{
		//2) Existe el plan de pagos
			Byte estatusAutorizacion = listaPlanPagosCobertura.get(0).getEstatusAutorizacionPlanPagos();
			if(estatusAutorizacion==null || estatusAutorizacion==Sistema.AUTORIZACION_NO_REQUERIDA){
			//2.1) No requiere autorizaci�n
				//a) Se regenera el plan de pagos
				//this.eliminaPagosCobertura(listaPlanPagosCobertura.get(0));
				listaPlanPagosCobertura.get(0).setDetalleContratoFacultativoDTO(detalleContratoFacultativo);
				this.crearPagosCobertura(listaPlanPagosCobertura.get(0));
			}
			//2.2) Requiere autorizaci�n / Autorizado / Rechazado
				//a) No se modifica el plan de pagos
		}
		
		//3) Se consulta el plan de pagos
		listaPlanPagosCobertura = 
			this.buscarPorPropiedad("detalleContratoFacultativoDTO.contratoFacultativoDTO", idTdContratoFacultativo);
						
		//4) Se regresa el plan de pagos
		return buscarPagosCoberturaPorPlanPagos(listaPlanPagosCobertura.get(0));		
	}
	
	//Crea el plan de pagos, de acuerdo a las reglas para generar recibos de SEYCOS.
	private void crearPlanPagosCobertura(DetalleContratoFacultativoDTO detalleContratoFacultativo) throws SystemException{		
		//Crear el registro de ToPlanPagosCobertura
		PlanPagosCoberturaDTO planPagosCobertura = new PlanPagosCoberturaDTO();
		planPagosCobertura.setDetalleContratoFacultativoDTO(detalleContratoFacultativo);
		planPagosCobertura.setEstatusAutorizacionPlanPagos(Byte.valueOf(Sistema.AUTORIZACION_NO_REQUERIDA+""));				
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		planPagosCobertura = planPagosCoberturaSN.agregar(planPagosCobertura);
		this.crearPagosCobertura(planPagosCobertura);
	}
	
	//Crea los pagos para un plan de pago existente.
	private void crearPagosCobertura(PlanPagosCoberturaDTO planPagosCobertura) throws SystemException{
		//Crear los registros de TmPagoCobertura
		List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos = 
			this.generaPlanPagosCobertura(planPagosCobertura.getDetalleContratoFacultativoDTO());										
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();			
		planPagosCoberturaSN.modificarPagosCobertura(planPagosCobertura, pagos, true, PlanPagosCoberturaDN.nombreUsuario);
	}	
	
	//Elimina los pagos de un plan de pagos existente.
//	private void eliminaPagosCobertura(PlanPagosCoberturaDTO planPagosCobertura) throws SystemException{
//		List<PagoCoberturaDTO> pagosCobertura = this.buscarPagosCoberturaPorPlanPagos(planPagosCobertura);
//		PagoCoberturaSN pagoCoberturaSN = new PagoCoberturaSN();
//		for(PagoCoberturaDTO pagoCobertura : pagosCobertura)
//			pagoCoberturaSN.eliminar(pagoCobertura);
//	}
	
	//Busca los pagos correspondientes a un plan de pagos
	private List<PagoCoberturaDTO> buscarPagosCoberturaPorPlanPagos(PlanPagosCoberturaDTO planPagosCobertura) throws SystemException{
		PagoCoberturaSN pagoCoberturaSN = new PagoCoberturaSN();		
		return pagoCoberturaSN.buscarPorPropiedad("id.idToPlanPagosCobertura", planPagosCobertura.getIdToPlanPagosCobertura());
	}
	
	//Genera el plan de pagos para un detalle de contrato facultativo de acuerdo a las reglas de generaci�n de recibos de SEYCOS
	private List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> 
	generaPlanPagosCobertura(DetalleContratoFacultativoDTO detalleContratoFacultativo) 
	throws SystemException{
		ContratoFacultativoDTO contratoFacultativo = detalleContratoFacultativo.getContratoFacultativoDTO_1();
		
		LineaSoporteReaseguroDN lineaSoporteReaseguroDN = LineaSoporteReaseguroDN.getInstancia();
		List<LineaSoporteReaseguroDTO> lineasSoporteReaseguro = 
			lineaSoporteReaseguroDN.getPorPropiedad("contratoFacultativoDTO.idTmContratoFacultativo", 
											     	 contratoFacultativo.getIdTmContratoFacultativo());
		LineaSoporteReaseguroDTO lineaSoporteReaseguro = lineasSoporteReaseguro.get(0);
		
		BigDecimal idToCotizacion = lineaSoporteReaseguro.getIdToCotizacion();
		
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacion = cotizacionDN.getPorId(idToCotizacion);
											
		Date fechaInicioVigencia = cotizacion.getFechaInicioVigencia();
		Date fechaFinVigencia = cotizacion.getFechaFinVigencia();
		int idFormaPago = cotizacion.getIdFormaPago().intValue();
	    Double montoAPagar = calcularMontoAPagarCobertura(detalleContratoFacultativo, lineaSoporteReaseguro);     
			    	    	   	    	    
	    Date fechaInicialBase = (Date)fechaInicioVigencia.clone();
	    Date fechaFinalBase = (Date)fechaFinVigencia.clone();
	    int idFormaPagoBase = idFormaPago;
	    
	    BigDecimal idToPolizaEndosada = cotizacion.getSolicitudDTO().getIdToPolizaEndosada();
	    if(idToPolizaEndosada != null && idToPolizaEndosada.compareTo(BigDecimal.ZERO)!=0){
	    // Si se trata de un endoso
	    	//Se obtienen las fechas de vigencia originales de la p�liza
	    	EndosoId id = new EndosoId(idToPolizaEndosada,Short.valueOf((short)0));	    	
	    	EndosoDN endosoDN = EndosoDN.getInstancia(nombreUsuario);
	    	EndosoDTO endosoCero = endosoDN.getPorId(id);	    		    	
	    	fechaInicialBase = endosoCero.getFechaInicioVigencia();
	    	fechaFinalBase = endosoCero.getFechaFinVigencia();	   
	    	
	    	//Se obtiene la forma actual de la p�liza, considerando los endosos de cambio de forma de pago
	    	PolizaDN polizaDN = PolizaDN.getInstancia();
	    	idFormaPagoBase = polizaDN.obtenerFormaPagoActual(idToPolizaEndosada);
	    }
	    	    	    
	    PagoCoberturaDN pagoCoberturaDN = PagoCoberturaDN.getInstancia(nombreUsuario);
	    List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos = 
	    	pagoCoberturaDN.calcularPagos(fechaInicialBase, fechaFinalBase, idFormaPagoBase,
										  fechaInicioVigencia, fechaFinVigencia, idFormaPago, 
										  montoAPagar);
	    
	    return pagos;
	}
	
	public List<PagoCoberturaDTO> regenerarPlanPagosCobertura(BigDecimal idTdContratoFacultativo, Short numeroExhibiciones) 
	throws ExcepcionDeAccesoADatos, SystemException{				
		DetalleContratoFacultativoDN detalleContratoFacultativoDN = DetalleContratoFacultativoDN.getInstancia();
		DetalleContratoFacultativoDTO detalleContratoFacultativo = 
			detalleContratoFacultativoDN.getCotizacionFacultativa(idTdContratoFacultativo);	
		
		ContratoFacultativoDTO contratoFacultativo = detalleContratoFacultativo.getContratoFacultativoDTO_1();
		
		LineaSoporteReaseguroDN lineaSoporteReaseguroDN = LineaSoporteReaseguroDN.getInstancia();
		List<LineaSoporteReaseguroDTO> lineasSoporteReaseguro = 
			lineaSoporteReaseguroDN.getPorPropiedad("contratoFacultativoDTO.idTmContratoFacultativo", 
											     	 contratoFacultativo.getIdTmContratoFacultativo());
		LineaSoporteReaseguroDTO lineaSoporteReaseguro = lineasSoporteReaseguro.get(0);
		
		BigDecimal idToCotizacion = lineaSoporteReaseguro.getIdToCotizacion();
		
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		CotizacionDTO cotizacion = cotizacionDN.getPorId(idToCotizacion);
		
		//Se obtienen fechas de inicio y fin de vigencia del contrato/cotizaci�n y el monto a pagar por exhibici�n
		Date fechaInicioVigencia = cotizacion.getFechaInicioVigencia();
		Date fechaFinVigencia = cotizacion.getFechaFinVigencia();
		Double montoAPagar = calcularMontoAPagarCobertura(detalleContratoFacultativo, lineaSoporteReaseguro);
		Double montoPorExhibicion = montoAPagar / numeroExhibiciones;
		//Se redondea (trunca) el monto por exhibici�n a dos decimales
		int montoPorExhibicionTemp = (int)(montoPorExhibicion * 100);		
		montoPorExhibicion = montoPorExhibicionTemp / 100.0;
		
		//Se obtienen las fechas de pagos de cada exhibici�n
		//Se genera la lista de PagoCoberturaDTO's con las exhibiciones
		int diasVigencia = (int)UtileriasWeb.obtenerDiasEntreFechas(fechaInicioVigencia, fechaFinVigencia);
		int diasPorExhibicion = diasVigencia/numeroExhibiciones;				
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fechaInicioVigencia);
		
		List<PlanPagosCoberturaDTO> listaPlanPagosCobertura = 
			this.buscarPorPropiedad("detalleContratoFacultativoDTO.contratoFacultativoDTO", idTdContratoFacultativo);
		
		PlanPagosCoberturaDTO planPagosCobertura = listaPlanPagosCobertura.get(0);
		
		List<PagoCoberturaDTO> pagosCobertura = new ArrayList<PagoCoberturaDTO>();
		
		for(int numeroExhibicion=1; numeroExhibicion <= numeroExhibiciones; numeroExhibicion++){			
			PagoCoberturaId id = new PagoCoberturaId();
			id.setNumeroExhibicion((short)numeroExhibicion);
			id.setIdToPlanPagosCobertura(planPagosCobertura.getIdToPlanPagosCobertura());
			PagoCoberturaDTO pagoCobertura = new PagoCoberturaDTO();
			pagoCobertura.setId(id);
			pagoCobertura.setMontoPagoPrimaNeta(montoPorExhibicion);
			pagoCobertura.setPlanPagosCobertura(planPagosCobertura);
			
			Date fechaPago = new Date(); 
				 fechaPago = (Date)cal.getTime().clone();						
			
			pagoCobertura.setFechaPago(fechaPago);
			
			pagosCobertura.add(pagoCobertura);
			
			cal.add(Calendar.DATE, diasPorExhibicion);	
		}
		
		
		//Se calcula la diferencia por el redondeo
		Double diferencia = montoAPagar - (montoPorExhibicion * numeroExhibiciones);		
		
		//La diferencia del redondeo se compensa en el primer pago
		if(pagosCobertura.get(0)!=null)
			pagosCobertura.get(0).setMontoPagoPrimaNeta(montoPorExhibicion+diferencia);
		
		return pagosCobertura;				
	}
	
	public void guardarPagosCobertura(BigDecimal idTdContratoFacultativo, String pagosCoberturaStr) throws SystemException, ExcepcionDeLogicaNegocio{
		List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos = 
			new ArrayList<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO>();
		
		String[] pagosStr = pagosCoberturaStr.split("\\|");
		Calendar cal = Calendar.getInstance();		
		for(String pagoStr : pagosStr){
			String[] datosPago = pagoStr.split("_");
			String numeroExhibicionStr = datosPago[0];
			String fechaPagoStr = datosPago[1];
			String montoPagoStr = datosPago[2];
			String[] datosFecha = fechaPagoStr.split("/");
			String dia = datosFecha[0];
			String mes = datosFecha[1];
			String anio = datosFecha[2];
			cal.set(Integer.valueOf(anio),Integer.valueOf(mes)-1,Integer.valueOf(dia));
			Date fechaPago = cal.getTime();
			
			mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO pagoDTO = 
				new mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO();
			pagoDTO.setNumeroExhibicion(Short.valueOf(numeroExhibicionStr));			
			pagoDTO.setFechaInicioPago(fechaPago);
			pagoDTO.setMontoPago(Double.valueOf(montoPagoStr));
			pagos.add(pagoDTO);
		}
		
		DetalleContratoFacultativoDTO detalleContratoFacultativo = 
			DetalleContratoFacultativoDN.getInstancia().getCotizacionFacultativa(idTdContratoFacultativo);
		
		validarMontoAPagarCobertura(detalleContratoFacultativo, pagos);
		
		List<PlanPagosCoberturaDTO> listaPlanPagosCobertura = 
			this.buscarPorPropiedad("detalleContratoFacultativoDTO.contratoFacultativoDTO", idTdContratoFacultativo);			
		PlanPagosCoberturaDTO planPagosCobertura = listaPlanPagosCobertura.get(0);
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		planPagosCoberturaSN.modificarPagosCobertura(planPagosCobertura, pagos, false, PlanPagosCoberturaDN.nombreUsuario);
	}
	
	public PlanPagosCoberturaDTO autorizarPlanPagosCobertura(PlanPagosCoberturaDTO planPagosCobertura) throws SystemException{
		planPagosCobertura.setEstatusAutorizacionPlanPagos((byte)Sistema.AUTORIZADA);
		planPagosCobertura.setUsuarioAutorizacionPlanPagos(nombreUsuario);
		planPagosCobertura.setFechaAutorizacionPlanPagos(new Date());
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.modificar(planPagosCobertura);		
	}
	
	public void validarMontoAPagarCobertura(DetalleContratoFacultativoDTO detalleContratoFacultativo,
			List<mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO> pagos) 
	throws SystemException, ExcepcionDeLogicaNegocio{							
		ContratoFacultativoDTO contratoFacultativo = detalleContratoFacultativo.getContratoFacultativoDTO_1();
		
		LineaSoporteReaseguroDN lineaSoporteReaseguroDN = LineaSoporteReaseguroDN.getInstancia();
		List<LineaSoporteReaseguroDTO> lineasSoporteReaseguro = 
			lineaSoporteReaseguroDN.getPorPropiedad("contratoFacultativoDTO.idTmContratoFacultativo", 
											     	 contratoFacultativo.getIdTmContratoFacultativo());
		LineaSoporteReaseguroDTO lineaSoporteReaseguro = lineasSoporteReaseguro.get(0);
		
		double montoCorrectoAPagar = calcularMontoAPagarCobertura(detalleContratoFacultativo, lineaSoporteReaseguro);
		double montoAPagar = 0;
		Date fechaAnterior = null;
		boolean planPagosCorrecto = true;
		StringBuilder mensaje = new  StringBuilder("Plan de pagos incorrecto:<br />").append("<ul>");
		String mensajeError = "";
		for(mx.com.afirme.midas.interfaz.contratofacultativo.pagocobertura.PagoCoberturaDTO pago : pagos){
			montoAPagar += pago.getMontoPago();
			
			if(fechaAnterior == null){
				fechaAnterior = pago.getFechaInicioPago();
			}else{
				if(!pago.getFechaInicioPago().after(fechaAnterior)){
					//Las fechas est�n desordenadas
					if(planPagosCorrecto){
						planPagosCorrecto = false;
						mensaje.append("<li>Las fechas de las exhibiciones deben estar en orden cronol�gico</li>");
									    
					}					
				}
			}
		}
		mensajeError = mensaje.toString();
		//Se redondea a 4 decimales porque en ocasiones sal�an diferencias de 0.00000000001, sin ning�n motivo aparente.
		montoAPagar = Math.round(montoAPagar * 10000)/10000.00;
		montoCorrectoAPagar = Math.round(montoCorrectoAPagar * 10000)/10000.00;
		
		if(montoAPagar != montoCorrectoAPagar){
			planPagosCorrecto = false;
			NumberFormat fMonto = new DecimalFormat("'$'#,##0.00");
			mensajeError += "<li>El monto total a pagar debe ser igual a "+fMonto.format(montoCorrectoAPagar)+"</li>";						
		}
		
		mensajeError += "</ul>";
		
		if(!planPagosCorrecto)
			throw new ExcepcionDeLogicaNegocio("PlanPagosCoberturaDN", mensajeError);
	}
	
	public double calcularMontoAPagarCobertura(DetalleContratoFacultativoDTO detalleContratoFacultativo, 
											   LineaSoporteReaseguroDTO lineaSoporteReaseguro) throws SystemException{		
		Double porcentajeFacultativo = lineaSoporteReaseguro.getDisPrimaPorcentajeFacultativo() != null ?
									   lineaSoporteReaseguro.getDisPrimaPorcentajeFacultativo().doubleValue() : 0D;

		Double primaCobertura = detalleContratoFacultativo.getPrimaFacultadaCobertura() != null ?
								detalleContratoFacultativo.getPrimaFacultadaCobertura().doubleValue() : 0D;

		Double primaNoDevengada = detalleContratoFacultativo.getMontoPrimaNoDevengada() != null ?
								  detalleContratoFacultativo.getMontoPrimaNoDevengada().doubleValue() : 0D;
								  
		//Double primaAdicional = detalleContratoFacultativo.getMontoPrimaAdicional() != null ?
		//						  detalleContratoFacultativo.getMontoPrimaAdicional().doubleValue() : 0D;

		NumberFormat fMonto = new DecimalFormat("##0.00");
		//Se deja de considerar la prima adicional, ya que el campo TdContratoFacultativo.primaFacultadaCobertura no la incluye.
		//return Double.valueOf(fMonto.format((primaCobertura - primaNoDevengada - primaAdicional) * porcentajeFacultativo/100D));
		return Double.valueOf(fMonto.format((primaCobertura - primaNoDevengada) * porcentajeFacultativo/100D));
	}
	
	public void distribuirPagosPorReasegurador(BigDecimal idTdContratoFacultativo) throws SystemException, ExcepcionDeLogicaNegocio{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		planPagosCoberturaSN.distribuirPagosPorReasegurador(idTdContratoFacultativo);
	}
	
	public List<PlanPagosCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valorPropiedad) throws SystemException{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.buscarPorPropiedad(nombrePropiedad, valorPropiedad);
	}
	
	public List<PagoCoberturaDecoradoDTO> listarExhibicionesAgrupadasPorCobertura(String idsEstadoCuenta) throws SystemException{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.listarExhibicionesAgrupadasPorCobertura(idsEstadoCuenta);
	}
	
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesCoberturasReaseguradores(String idsCoberturasReaseguradores) throws SystemException{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.listarExhibicionesCoberturasReaseguradores(idsCoberturasReaseguradores);										
	}
	
	public PagoCoberturaReaseguradorDTO obtenerExhibicionPorId(PagoCoberturaReaseguradorId id) throws SystemException{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.obtenerExhibicionPorId(id);
	}
	
	public PagoCoberturaReaseguradorDTO actualizarExhibicion(PagoCoberturaReaseguradorDTO exhibicion) throws SystemException{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.actualizarExhibicion(exhibicion);
	}
	
	public List<PagoCoberturaReaseguradorDecoradoDTO> listarExhibicionesDecoradas(String idsExhibiciones) throws SystemException{
		PlanPagosCoberturaSN planPagosCoberturaSN = new PlanPagosCoberturaSN();
		return planPagosCoberturaSN.listarExhibicionesDecoradas(idsExhibiciones);		
	}
}