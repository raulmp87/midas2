package mx.com.afirme.midas.contratofacultativo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.configuracionpagos.ConfiguracionPagosFacultativoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContratoFacultativoSN {

	private ContratoFacultativoFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public ContratoFacultativoSN(String nombreUsuario) throws SystemException {
		try {
			this.nombreUsuario = nombreUsuario;
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ContratoFacultativoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public ContratoFacultativoDTO getPorId(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToSlip);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ContratoFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ContratoFacultativoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ContratoFacultativoDTO> contratoFacultativos= beanRemoto.findAll();
		return contratoFacultativos;
	}
	
	public ContratoFacultativoDTO agregar(ContratoFacultativoDTO contratoFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
    try{
    	contratoFacultativoDTO.setNombreUsuarioLog(this.nombreUsuario);
    	return beanRemoto.save(contratoFacultativoDTO);
    } catch(Exception e){
	   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
    } 
  }
	public void eliminar(ContratoFacultativoDTO contratoFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
    try{
    	contratoFacultativoDTO.setNombreUsuarioLog(this.nombreUsuario);
    	beanRemoto.delete(contratoFacultativoDTO);
    } catch(Exception e){
	   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
    } 
  }
	
	
	
	
	public boolean autorizarContratoFacultativo(BigDecimal idTmContratoFacultativo){
		try {
			return beanRemoto.autorizarContratoFacultativo(idTmContratoFacultativo);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean cancelarContratoFacultativo(BigDecimal idToSlip){
		try {
			return beanRemoto.cancelarContratoFacultativo(idToSlip);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
	public String autorizarCotizacionFacultativa(BigDecimal id){
		try {
			return beanRemoto.autorizarCotizacionFacultativa(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public ContratoFacultativoDTO modificar(ContratoFacultativoDTO contratoFacultativoDTO)
			throws ExcepcionDeAccesoADatos {
	    try{
	    	contratoFacultativoDTO.setNombreUsuarioLog(this.nombreUsuario);
	    	return beanRemoto.update(contratoFacultativoDTO);
	    } catch(Exception e){
		   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
	    } 
	}
	
	public List<ContratoFacultativoDTO> buscarFiltrado(ContratoFacultativoDTO contratoFacultativoDTO) throws ExcepcionDeAccesoADatos {
		try{
			contratoFacultativoDTO.setNombreUsuarioLog(this.nombreUsuario);
			return beanRemoto.buscarFiltrado(contratoFacultativoDTO);
		} catch(Exception e){
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		} 
	}
	
	public List<ConfiguracionPagosFacultativoDTO> findConfiguracionPagosFacultativoDTOByContratoFacultativo(BigDecimal idTmContratoFacultativo){
		try{
			return beanRemoto.findConfiguracionPagosFacultativoDTOByContratoFacultativo(idTmContratoFacultativo);
		} catch(Exception e){
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		} 
	}
	
	public double obtenerPrimaPorReaseguradorYContratoFacultativo(ReaseguradorCorredorDTO reaseguradorCorredorDTO, ContratoFacultativoDTO contratoFacultativoDTO,BigDecimal porcentajeFacultativo) throws SystemException{
		return beanRemoto.obtenerPrimaPorReaseguradorYContratoFacultativo(reaseguradorCorredorDTO, contratoFacultativoDTO,porcentajeFacultativo);
	}
	
	public void deleteSlipAndDetails(BigDecimal idToSlip){
		beanRemoto.deleteSlipAndDetails(idToSlip);
	}

	public void cambiarNotaCobertura(ContratoFacultativoDTO contratoFacultativoDTO){
		beanRemoto.modificarNotaCobertura(contratoFacultativoDTO);
	}
	
	public int obtenerCantidadContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus){
		return beanRemoto.obtenerCantidadContratosPorCotizacion(idToCotizacion,estatus);
	}
	
	public List<ContratoFacultativoDTO> obtenerContratosPorCotizacion(BigDecimal idToCotizacion,BigDecimal estatus){
		return beanRemoto.obtenerContratosPorCotizacion(idToCotizacion,estatus);
	}
	
//	/**
//	 * Funci�n para notificar de un nuevo endoso de p�liza a los contratos facultativos, esta funci�n genera un nuevo {@link SlipDTO} y eval�a si
//	 * es necesario realizar un endoso al contrato facultativo vigente por cada {@link LineaSoporteReaseguroDTO} relacionado al {@link SoporteReaseguroDTO}
//	 * proporcionado como par�metro
//	 * @param soporteNuevoEndoso
//	 * @param tipoEndoso
//	 * @throws SystemException
//	 * @throws ExcepcionDeLogicaNegocio
//	 */
//	public void notificacionEndoso(SoporteReaseguroDTO soporteNuevoEndoso, int tipoEndoso) throws SystemException, ExcepcionDeLogicaNegocio{
//		LineaSoporteReaseguroSN lineaSoporteReaseguroSN = new LineaSoporteReaseguroSN();
//		SlipDN slipDN = new SlipDN();
//		List<Object[]> slipDTO_contratoFacultativoSoportaLineaList = new ArrayList<Object[]>();
//		SlipDTO slipDTO;
//		ContratoFacultativoDTO contratoFacultativoDTO;
//		Object[] slipDTO_contratoFacultativoSoportaLinea;
//		if (soporteNuevoEndoso.getNumeroEndoso() == 0){
//			throw new ExcepcionDeLogicaNegocio(this.getClass().getName(), "Ha ocurrido un error, el soporte proporcionado no debe pertenecer a un " +
//					"endoso cero, SoporteReaseguroDTO con id: " + soporteNuevoEndoso.getIdToSoporteReaseguro().toBigInteger().toString());
//		}
//		
//		List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOList = lineaSoporteReaseguroSN.getPorPropiedad("id.idToSoporteReaseguro", soporteNuevoEndoso.getIdToSoporteReaseguro());
//		for (LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO : lineaSoporteReaseguroDTOList) {
//			/**
//			 * TODO Es necesario un campo nuevo en LineaSoporteReaseguroDTO que indique si la linea est� cancelada o 
//			 * es de aumento/disminuci�n o modificaci�n
//			 */
//			slipDTO = slipDN.obtenerSlipLineaSoporte(lineaSoporteReaseguroDTO);
//			if (slipDTO == null){
//				slipDTO_contratoFacultativoSoportaLinea = new Object[2];
//				slipDTO = slipDN.llenarSlip(lineaSoporteReaseguroDTO);
//				slipDTO.setNumeroEndoso(soporteNuevoEndoso.getNumeroEndoso());
//				slipDTO.setTipoEndoso(tipoEndoso);
//				slipDTO_contratoFacultativoSoportaLinea[0] = slipDTO;
//				contratoFacultativoDTO = lineaSoporteReaseguroDTO.getContratoFacultativoDTO();
//				
//				SoporteReaseguro soporteReaseguro = new SoporteReaseguro(lineaSoporteReaseguroDTO.getIdToCotizacion());
//				boolean contratoFacultativoSoportaLinea = soporteReaseguro.contratoFacultativoSoportaLinea(contratoFacultativoDTO.getSlipDTO().getIdTmLineaSoporteReaseguro(), contratoFacultativoDTO.getSumaAseguradaFacultada());
//  			  
//				slipDTO_contratoFacultativoSoportaLinea[1] = contratoFacultativoSoportaLinea;
//				
//				slipDTO_contratoFacultativoSoportaLineaList.add(slipDTO_contratoFacultativoSoportaLinea);
//			}else{
//				throw new ExcepcionDeLogicaNegocio(this.getClass().getName(), "Ha ocurrido una inconsistencia, se ha encontrado un SlipDTO que ya ha sido relacionado al nuevo soporte");
//			}
//		}
//		beanRemoto.notificacionEndoso(soporteNuevoEndoso, slipDTO_contratoFacultativoSoportaLineaList, tipoEndoso);
//		
//	}
	
	public ContratoFacultativoDTO duplicarContratoFacultativo(BigDecimal idTmContratoFacultativo, BigDecimal idToSlip, int numeroEndoso){
		return beanRemoto.duplicarContratoFacultativo(idTmContratoFacultativo, idToSlip, numeroEndoso);
	}
	

	public ContratoFacultativoDTO procesarMovimientosCancelacionRehabilitacionCobertura(BigDecimal idTmContratoFacultativo, int tipoMovimiento, int movimiento){
		try
		{
		   return beanRemoto.procesarMovimientosCancelacionRehabilitacionCobertura(idTmContratoFacultativo, tipoMovimiento, movimiento);
		} catch(Exception e){
		   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
		} 				
	}
	
	public DetalleContratoFacultativoDTO copiarParticipacionesFacultativoCoberturaEndosoAnterior(
			BigDecimal idTdContratoFacultativoActual) {
		return beanRemoto
				.copiarParticipacionesFacultativoCoberturaEndosoAnterior(idTdContratoFacultativoActual);
	}
	
	public BigDecimal getPrimaEndosoTotal(BigDecimal idTmContratoFacultativo) {
		return beanRemoto.getPrimaEndosoTotal(idTmContratoFacultativo);
	}
	
	public BigDecimal getPrimaNegociadaTotal(BigDecimal idTmContratoFacultativo) {
		return beanRemoto.getPrimaNegociadaTotal(idTmContratoFacultativo);
	}
	
	/**
     * Metodo usado para ajustar la prima adicional del contrato a partir de la prima adicional guardada en los detalles del mismo.
     */
	public ContratoFacultativoDTO ajustarPrimaAdicionalSegunDetalles(BigDecimal idTmContratoFacultativo) {
		return beanRemoto.ajustarPrimaAdicionalSegunDetalles(idTmContratoFacultativo);
	}
	
	public List<String> obtenerAjustadorNombrado(BigDecimal idToPoliza,short numeroEndoso){
		return beanRemoto.obtenerAjustadorNombrado(idToPoliza, numeroEndoso);
	}
	
	public BigDecimal getComisionTotal(BigDecimal idTmContratoFacultativo) {
		return beanRemoto.getComisionTotal(idTmContratoFacultativo);
	}
}
