package mx.com.afirme.midas.contratofacultativo;

import mx.com.afirme.midas.contratofacultativo.participacionfacultativa.ParticipacionFacultativaForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class DetalleContratoFacultativoForm extends MidasBaseForm{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7063921806200336707L;
	private String idTdContratoFacultativo;
	private String idTmContratoFacultativo;
	private String idToCotizacion;
	private String idTcSubRamo;
	private String idToCobertura;
	private String descripcionCobertura;
	private String descripcionSeccion;
	private String numeroInciso;
	private String idToSeccion;
	private String numeroSubInciso;
	private String tipoDistribucion;
	private String sumaAsegurada;
    private String porcentajeComision;
    private String deducibleCobertura;
    private String primaTotalCobertura;
    private String primaFacultadaCobertura;
    private ContratoFacultativoForm contratoFacultativoForm = new ContratoFacultativoForm();
	private ParticipacionFacultativaForm participacionFacultativaForm = new ParticipacionFacultativaForm();
	private String aplicarATodas;
	private String moneda;
	private String porcentajeFacultativo;
	private String sumaFacultada;
	private String primaNoDevengada;
	private boolean primaFacultadaCobModificable;
	private DetalleContratoFacultativoForm detalleContratoFacultativoAnterior;
	private String montoPrimaAdicional;
	private String numeroExhibiciones;
	private String estatusAutorizacionPlanPagos;
	private String planPagosStr;
	private String mostrarBotonAutorizarPlanPagos;
	
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	
	public ParticipacionFacultativaForm getParticipacionFacultativaForm() {
		return participacionFacultativaForm;
	}
	public void setParticipacionFacultativaForm(
			ParticipacionFacultativaForm participacionFacultativaForm) {
		this.participacionFacultativaForm = participacionFacultativaForm;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	public String getIdTdContratoFacultativo() {
		return idTdContratoFacultativo;
	}
	public void setIdTdContratoFacultativo(String idTdContratoFacultativo) {
		this.idTdContratoFacultativo = idTdContratoFacultativo;
	}
	public String getIdTmContratoFacultativo() {
		return idTmContratoFacultativo;
	}
	public void setIdTmContratoFacultativo(String idTmContratoFacultativo) {
		this.idTmContratoFacultativo = idTmContratoFacultativo;
	}
	public String getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getIdToCobertura() {
		return idToCobertura;
	}
	public void setIdToCobertura(String idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	public String getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getIdToSeccion() {
		return idToSeccion;
	}
	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	public String getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(String numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public String getTipoDistribucion() {
		return tipoDistribucion;
	}
	public void setTipoDistribucion(String tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}
	public String getPorcentajeComision() {
		return porcentajeComision;
	}
	public void setPorcentajeComision(String porcentajeComision) {
		this.porcentajeComision = porcentajeComision;
	}
	public String getDeducibleCobertura() {
		return deducibleCobertura;
	}
	public void setDeducibleCobertura(String deducibleCobertura) {
		this.deducibleCobertura = deducibleCobertura;
	}
	public String getPrimaTotalCobertura() {
		return primaTotalCobertura;
	}
	public void setPrimaTotalCobertura(String primaTotalCobertura) {
		this.primaTotalCobertura = primaTotalCobertura;
	}
	public String getPrimaFacultadaCobertura() {
		return primaFacultadaCobertura;
	}
	public void setPrimaFacultadaCobertura(String primaFacultadaCobertura) {
		this.primaFacultadaCobertura = primaFacultadaCobertura;
	}
	public ContratoFacultativoForm getContratoFacultativoForm() {
		return contratoFacultativoForm;
	}
	public void setContratoFacultativoForm(
			ContratoFacultativoForm contratoFacultativoForm) {
		this.contratoFacultativoForm = contratoFacultativoForm;
	}
	public String getAplicarATodas() {
		return aplicarATodas;
	}
	public void setAplicarATodas(String aplicarATodas) {
		this.aplicarATodas = aplicarATodas;
	}
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(String porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public String getSumaFacultada() {
		return sumaFacultada;
	}
	public void setSumaFacultada(String sumaFacultada) {
		this.sumaFacultada = sumaFacultada;
	}
	public String getPrimaNoDevengada() {
		return primaNoDevengada;
	}
	public void setPrimaNoDevengada(String primaNoDevengada) {
		this.primaNoDevengada = primaNoDevengada;
	}
	public void setPrimaFacultadaCobModificable(boolean primaFacultadaCobModificable) {
		this.primaFacultadaCobModificable = primaFacultadaCobModificable;
	}
	public boolean isPrimaFacultadaCobModificable() {
		return primaFacultadaCobModificable;
	}
	public DetalleContratoFacultativoForm getDetalleContratoFacultativoAnterior() {
		if (detalleContratoFacultativoAnterior == null) {
			this.detalleContratoFacultativoAnterior = new DetalleContratoFacultativoForm();
		}
		return detalleContratoFacultativoAnterior;
	}
	public void setDetalleContratoFacultativoAnterior(DetalleContratoFacultativoForm detalleContratoFacultativoAnterior) {
		this.detalleContratoFacultativoAnterior = detalleContratoFacultativoAnterior;
	}
	public String getMontoPrimaAdicional() {
		return montoPrimaAdicional;
	}
	public void setMontoPrimaAdicional(String montoPrimaAdicional) {
		this.montoPrimaAdicional = montoPrimaAdicional;
	}
	public String getNumeroExhibiciones() {
		return numeroExhibiciones;
	}
	public void setNumeroExhibiciones(String numeroExhibiciones) {
		this.numeroExhibiciones = numeroExhibiciones;
	}
	public String getEstatusAutorizacionPlanPagos() {
		return estatusAutorizacionPlanPagos;
	}
	public void setEstatusAutorizacionPlanPagos(String estatusAutorizacionPlanPagos) {
		this.estatusAutorizacionPlanPagos = estatusAutorizacionPlanPagos;
	}
	public String getPlanPagosStr() {
		return planPagosStr;
	}
	public void setPlanPagosStr(String planPagosStr) {
		this.planPagosStr = planPagosStr;
	}
	public String getMostrarBotonAutorizarPlanPagos() {
		return mostrarBotonAutorizarPlanPagos;
	}
	public void setMostrarBotonAutorizarPlanPagos(
			String mostrarBotonAutorizarPlanPagos) {
		this.mostrarBotonAutorizarPlanPagos = mostrarBotonAutorizarPlanPagos;
	}
}

