package mx.com.afirme.midas.contratofacultativo.participacionfacultativa;


import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;


public class ParticipacionFacultativaDN {
	

	private static final ParticipacionFacultativaDN INSTANCIA = new ParticipacionFacultativaDN(); 
	
	  public static ParticipacionFacultativaDN getInstancia(){
		     return ParticipacionFacultativaDN.INSTANCIA;
	  }
	  
	  public List<ParticipacionFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor)throws SystemException, ExcepcionDeAccesoADatos {
			return new ParticipacionFacultativaSN().buscarPorPropiedad(propiedad, valor);
		}

	  public void agregar(ParticipacionFacultativoDTO participacionFacultativoDTO) throws SystemException,
      ExcepcionDeAccesoADatos, ExcepcionDeLogicaNegocio {
		  ParticipacionFacultativaSN participacionFacultativaSN = new ParticipacionFacultativaSN();
		  participacionFacultativaSN.agregar(participacionFacultativoDTO);
	}
	  
	  public List<ParticipacionFacultativoDTO> listarFiltrados(ParticipacionFacultativoDTO participacionFacultativoDTO)
		 throws SystemException, ExcepcionDeAccesoADatos {
		   ParticipacionFacultativaSN participacionFacultativoSN=  new ParticipacionFacultativaSN();
	    return participacionFacultativoSN.listarFiltrados(participacionFacultativoDTO);
     }
	 
	 public ParticipacionFacultativoDTO getParticipacionFacultativa(BigDecimal participacionFacultativoDTO) throws SystemException, ExcepcionDeAccesoADatos {
			return new ParticipacionFacultativaSN().getPorId(participacionFacultativoDTO);
	 }
	 
	 public ParticipacionFacultativoDTO getPorId(ParticipacionFacultativoDTO participacionFacultativoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		 ParticipacionFacultativaSN participacionFacultativoSN =  new ParticipacionFacultativaSN();
		 return participacionFacultativoSN.getPorId(participacionFacultativoDTO.getParticipacionFacultativoDTO());
	 }	 
	 
	 public ParticipacionFacultativoDTO modificar(ParticipacionFacultativoDTO participacionFacultativoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		 ParticipacionFacultativaSN participacionFacultativoSN =  new ParticipacionFacultativaSN();
		 return participacionFacultativoSN.modificar(participacionFacultativoDTO);
	 }	 
	 
 public void borrar(ParticipacionFacultativoDTO participacionFacultativoDTO)  throws SystemException, ExcepcionDeAccesoADatos {
		 ParticipacionFacultativaSN participacionFacultativoSN =  new ParticipacionFacultativaSN();
		 participacionFacultativoSN.borrar(participacionFacultativoDTO);
	 }
}
