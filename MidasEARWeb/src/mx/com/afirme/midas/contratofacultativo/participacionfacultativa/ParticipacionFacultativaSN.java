package mx.com.afirme.midas.contratofacultativo.participacionfacultativa;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionFacultativoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ParticipacionFacultativaSN {
    private ParticipacionFacultativoFacadeRemote beanRemoto;
	
	public ParticipacionFacultativaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto  = serviceLocator.getEJB(ParticipacionFacultativoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto SlipAnexoFacadeRemote instanciado", Level.FINEST, null);
	}

	public List<ParticipacionFacultativoDTO> buscarPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad, valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public void agregar(ParticipacionFacultativoDTO participacionFacultativoDTO)
	throws ExcepcionDeAccesoADatos {
    try{
    	beanRemoto.save(participacionFacultativoDTO);
    } catch(Exception e){
	   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
    } 
   }
	
	public ParticipacionFacultativoDTO getPorId(BigDecimal participacionFacultativoDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(participacionFacultativoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	
	public List<ParticipacionFacultativoDTO> listarFiltrados(ParticipacionFacultativoDTO participacionFacultativoDTO) throws ExcepcionDeAccesoADatos {
		List<ParticipacionFacultativoDTO> participacionesCorredor = beanRemoto.listarFiltrado(participacionFacultativoDTO);
		return participacionesCorredor;
	}

	public void borrar(ParticipacionFacultativoDTO participacionFacultativoDTO)throws SystemException, ExcepcionDeAccesoADatos{
		try {
			beanRemoto.delete(participacionFacultativoDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
	
	public ParticipacionFacultativoDTO modificar(ParticipacionFacultativoDTO participacionFacultativoDTO)
			throws ExcepcionDeAccesoADatos {
	    try{
	    	return beanRemoto.update(participacionFacultativoDTO);
	    } catch(Exception e){
		   throw new ExcepcionDeAccesoADatos(beanRemoto.getClass().getCanonicalName(), e);
	    } 
   }
	
}
