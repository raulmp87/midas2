/**
 * 
 */
package mx.com.afirme.midas.contratos.lineaparticipacion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class LineaParticipacionSN {
	private LineaParticipacionFacadeRemote beanRemoto;

	public LineaParticipacionSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(LineaParticipacionFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(LineaParticipacionDTO lineaParticipacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(lineaParticipacionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(LineaParticipacionDTO lineaParticipacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(lineaParticipacionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(LineaParticipacionDTO lineaParticipacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(lineaParticipacionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<LineaParticipacionDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public LineaParticipacionDTO getPorId(LineaParticipacionDTO lineaParticipacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(lineaParticipacionDTO.getId());
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}	
	
	public List<LineaParticipacionDTO> getPorPropiedad(String propertyName,
			Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
