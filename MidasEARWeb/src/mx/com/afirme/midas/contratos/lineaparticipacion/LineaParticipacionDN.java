package mx.com.afirme.midas.contratos.lineaparticipacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class LineaParticipacionDN {
	private static final LineaParticipacionDN INSTANCIA = new LineaParticipacionDN();

	public static LineaParticipacionDN getInstancia() {
		return LineaParticipacionDN.INSTANCIA;
	}

	public List<LineaParticipacionDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		LineaParticipacionSN lineaParticipacionSN = new LineaParticipacionSN();
		return lineaParticipacionSN.listarTodos();
	}	

	public void agregar(LineaParticipacionDTO lineaParticipacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		
		lineaParticipacionDTO.setLinea(null);
		LineaParticipacionSN lineaParticipacionSN = new LineaParticipacionSN();
		lineaParticipacionSN.agregar(lineaParticipacionDTO);
	}

	public void modificar(LineaParticipacionDTO lineaParticipacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		LineaParticipacionSN lineaParticipacionSN = new LineaParticipacionSN();
		lineaParticipacionSN.modificar(lineaParticipacionDTO);
	}

	public LineaParticipacionDTO getPorId(LineaParticipacionDTO lineaParticipacionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		LineaParticipacionSN lineaParticipacionSN = new LineaParticipacionSN();
		return lineaParticipacionSN.getPorId(lineaParticipacionDTO);
	}

	public void borrar(LineaParticipacionDTO lineaParticipacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		LineaParticipacionSN lineaParticipacionSN = new LineaParticipacionSN();
		lineaParticipacionSN.borrar(lineaParticipacionDTO);
	}
	
	public List<LineaParticipacionDTO> getPorPropiedad(String propertyName,
			Object value) throws SystemException,
			ExcepcionDeAccesoADatos {
		LineaParticipacionSN lineaParticipacionSN = new LineaParticipacionSN();
		return lineaParticipacionSN.getPorPropiedad(propertyName, value);
	}
}
