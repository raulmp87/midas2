package mx.com.afirme.midas.contratos.soporte;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SoporteContratosReaseguroDN {

	
	private static final SoporteContratosReaseguroDN INSTANCIA = new SoporteContratosReaseguroDN();
	
	public static SoporteContratosReaseguroDN getInstancia() throws SystemException, ExcepcionDeAccesoADatos{
		return INSTANCIA;
	}	
	
	public ValidacionFacultativoDTO obtenerTipoDistribucion(ValidacionFacultativoDTO datosValidacion) throws SystemException, ExcepcionDeAccesoADatos{
		return null;
	}
	
	public ValidacionFacultativoDTO validarSoporteFacultativo(ValidacionFacultativoDTO datosValidacion) throws SystemException, ExcepcionDeAccesoADatos{
		return null;
	}
	
	/**
	 * Por definir firma del servicio
	 * @param datosValidacion
	 */
	public ValidacionFacultativoDTO solicitarFacultativo(ValidacionFacultativoDTO datosValidacion){
		return null;
	}	
	
}
