package mx.com.afirme.midas.contratos.soporte;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class SoporteContratosReaseguroSN {
	
	@SuppressWarnings("unused")
	private SoporteContratosReaseguroServiciosRemote beanRemoto;
	
	public SoporteContratosReaseguroSN() throws SystemException {
		try{
			LogDeMidasWeb.log("Entrando en LineaSN - Constructor", Level.INFO,null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SoporteContratosReaseguroServiciosRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public ValidacionFacultativoDTO obtenerTipoDistribucion(ValidacionFacultativoDTO datosValidacion){
		return null;
	}
	
	public ValidacionFacultativoDTO validarSoporteFacultativo(ValidacionFacultativoDTO datosValidacion){
		return null;
	}
	
	/**
	 * Por definir firma del servicio
	 * @param datosValidacion
	 */
	public ValidacionFacultativoDTO solicitarFacultativo(ValidacionFacultativoDTO datosValidacion){
		return null;
	}


}
