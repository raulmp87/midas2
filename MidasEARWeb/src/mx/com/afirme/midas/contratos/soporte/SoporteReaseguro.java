package mx.com.afirme.midas.contratos.soporte;

import java.util.List;
import java.util.Set;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

@Deprecated
public class SoporteReaseguro {
	

	private ValidacionFacultativoDTO validacionFacultativo;

	public SoporteReaseguro() {
		validacionFacultativo = new ValidacionFacultativoDTO();
	}	
	
	/**
	 * 
	 * @param subRamo
	 */
	public void agregarSubRamo(SubRamoDTO subRamo){
		validacionFacultativo.agregarSubRamo(subRamo);
	}

	/**
	 * 
	 * @param subRamo
	 * @param tipoDistribucion
	 */
	public void asignarTipoDistribucion(SubRamoDTO subRamo, Integer tipoDistribucion){
		validacionFacultativo.asignarTipoDistribucion(subRamo, tipoDistribucion);
	}

	/**
	 * 
	 * @param subRamo
	 */
	public int obtenerTipoDistribucion(SubRamoDTO subRamo){
		return validacionFacultativo.obtenerTipoDistribucion(subRamo);
	}

	public Set<SubRamoDTO> obtenerSubRamos(){
		return validacionFacultativo.obtenerSubRamos();
	}

	/**
	 * 
	 * @param subRamo
	 * @param cumulo
	 */
	public void agregarCumulo(SubRamoDTO subRamo, CumuloDTO cumulo){
		validacionFacultativo.agregarCumulo(subRamo, cumulo);
	}

	/**
	 * 
	 * @param subRamo
	 */
	public CumuloDTO obtenerCumulo(SubRamoDTO subRamo){
		return validacionFacultativo.obtenerCumulo(subRamo);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<CumuloDTO> obtenerCumulos(){
		return validacionFacultativo.obtenerCumulos();
	}
	
	public boolean esFacultativoRequerido(SubRamoDTO subRamo){
		return validacionFacultativo.esFacultativoRequerido(subRamo);
	}
	
	
	public void verificarTipoDistribucion() throws SystemException, ExcepcionDeAccesoADatos{
		validacionFacultativo = 
			SoporteContratosReaseguroDN.getInstancia().obtenerTipoDistribucion(validacionFacultativo);
	}
	
	public void validarSoporteFacultativo() throws SystemException, ExcepcionDeAccesoADatos{
		validacionFacultativo = 
			SoporteContratosReaseguroDN.getInstancia().validarSoporteFacultativo(validacionFacultativo);
	}	
	/**
	 * Por definir firma del servicio
	 * @param datosValidacion
	 */
	public void solicitarFacultativo(SubRamoDTO subRamo) throws SystemException, ExcepcionDeAccesoADatos{
		validacionFacultativo = 
			SoporteContratosReaseguroDN.getInstancia().solicitarFacultativo(validacionFacultativo);
	}		
	
}
