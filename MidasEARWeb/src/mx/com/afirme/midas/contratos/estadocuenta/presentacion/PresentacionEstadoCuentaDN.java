package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

public class PresentacionEstadoCuentaDN {

	private static final PresentacionEstadoCuentaDN instancia = new PresentacionEstadoCuentaDN();
	
	public static PresentacionEstadoCuentaDN getInstancia() {
		return instancia;
	}

	private PresentacionEstadoCuentaDN(){
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO) throws SystemException{
		return obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO.getIdEstadoCuenta(), calculaClaveFormatoDefault(estadoCuentaDTO));
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(BigDecimal idToEstadoCuenta) throws SystemException{
		short claveFormato = obtenerClaveFormatoEstadoCuenta(4);
		return obtenerEstadoCuentaConSaldosCalculados(idToEstadoCuenta, claveFormato);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaContratosProporcionales(BigDecimal idToEstadoCuenta) throws SystemException{
		EstadoCuentaDTO estadoCuentaDTO = EstadoCuentaDN.getINSTANCIA().getPorId(idToEstadoCuenta);
		
		return obtenerEstadoCuentaConSaldosCalculados(idToEstadoCuenta, calculaClaveFormatoDefault(estadoCuentaDTO));
	}

	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(BigDecimal idToPoliza,Integer mes,Integer anio,boolean separarConceptosPorReasegurador) throws SystemException{
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza,null,null,null, mes, anio, separarConceptosPorReasegurador, obtenerClaveFormatoEstadoCuenta(2));
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaSaldoTecnicoPorReasegurador(BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,Integer mes,Integer anio) throws SystemException{
		short claveFormato = obtenerClaveFormatoEstadoCuenta(1);
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaCombinadoConSaldosCalculados(null,idTcReasegurador,idMoneda,idTcSubRamo, mes, anio, false, claveFormato);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaSaldoPolizaPorReasegurador(BigDecimal idTcReasegurador,BigDecimal idToPoliza,BigDecimal idMoneda,BigDecimal idTcSubRamo,Integer mes,Integer anio) throws SystemException{
		short claveFormato = obtenerClaveFormatoEstadoCuenta(2);
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza,idTcReasegurador,idMoneda,idTcSubRamo, mes, anio, false, claveFormato);
	}
	
	public String obtenerCadenaJsonEstadoCuentaFacultativoPorPoliza(BigDecimal idToPoliza) throws SystemException{
		short claveFormato = obtenerClaveFormatoEstadoCuenta(2);
		List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoList = obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza,claveFormato);
		return generarJsonEstadosCuentaFacultativosPorPoliza(idToPoliza,estadoCuentaDecoradoList);
	}
	
	public String obtenerCadenaJsonEstadoCuentaFacultativoPorSiniestro(List<ReporteSiniestroDTO> listaReporteSiniestros) throws SystemException{
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		MidasJsonRow jsonRow;
		if(listaReporteSiniestros != null && !listaReporteSiniestros.isEmpty()){
			boolean objetoSeleccionado = true;
			String seleccionado = "1";
			for(ReporteSiniestroDTO reporteSiniestroTMP : listaReporteSiniestros){
				jsonRow = new MidasJsonRow();
				jsonRow.setId(reporteSiniestroTMP.getIdToReporteSiniestro().toBigInteger().toString());
				PolizaSoporteDanosDTO polizaSoporteDanosDTO = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(reporteSiniestroTMP.getNumeroPoliza(), reporteSiniestroTMP.getFechaSiniestro());
				jsonRow.setDatos(seleccionado,reporteSiniestroTMP.getNumeroReporte(),
						UtileriasWeb.getFechaString(reporteSiniestroTMP.getFechaHoraReporte()),
						polizaSoporteDanosDTO.getNumeroPoliza(),
						(reporteSiniestroTMP.getNombreAsegurado()!=null?reporteSiniestroTMP.getNombreAsegurado():""),
						UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaEmision()));
				midasJsonBase.addRow(jsonRow);
				if(objetoSeleccionado){
					seleccionado = "0";
					objetoSeleccionado = false;
				}
			}
		}
		return midasJsonBase.toString();
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,short claveFormatoEstadoCuenta) throws SystemException{
		List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoList = new PresentacionEstadoCuentaSN().obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza, claveFormatoEstadoCuenta);
		return estadoCuentaDecoradoList;
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal[] idTcSubRamo,
			BigDecimal[] idTmContratoFacultativo,Integer mes,Integer anio,
			boolean separarConceptosPorReasegurador) throws SystemException{
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaCombinadoConSaldosCalculados(
				idToPoliza, idTcSubRamo,idTmContratoFacultativo,mes,anio,
				separarConceptosPorReasegurador, obtenerClaveFormatoEstadoCuenta(2));
	}
	
	private String generarJsonEstadosCuentaFacultativosPorPoliza(BigDecimal idToPoliza,List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoList){
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		MidasJsonRow jsonRow;
		for(EstadoCuentaDecoradoDTO estadoCuenta : estadoCuentaDecoradoList){
			jsonRow = new MidasJsonRow();
			jsonRow.setId(idToPoliza.toBigInteger().toString()+"_"+estadoCuenta.getSubRamoDTO().getIdTcSubRamo().toString()+
					"_"+estadoCuenta.getEstadoCuentaDTO().getContratoFacultativoDTO().getIdTmContratoFacultativo().toString());
			int tipoDistribucion = 0;
			try{
				tipoDistribucion = estadoCuenta.getLineaDTO().getTipoDistribucion().intValue();
			}catch(Exception e){}
			String tipoDistribucionString = "No disponible";
			String numeroInciso = "N/A",numeroSubInciso="N/A";
			if(tipoDistribucion != 0 && tipoDistribucion == Sistema.TIPO_CUMULO_POLIZA){
				tipoDistribucionString = "Por p�liza";
			}else if(tipoDistribucion != 0 && tipoDistribucion == Sistema.TIPO_CUMULO_INCISO){
				tipoDistribucionString = "Por inciso";
				numeroInciso = obtenerDescripcionIncisoSubInciso(estadoCuenta.getEstadoCuentaDTO().getContratoFacultativoDTO().getSlipDTO().getNumeroInciso());
				if(numeroInciso == null)
					numeroInciso = "PR/LUC";
			}else if(tipoDistribucion != 0 && tipoDistribucion == Sistema.TIPO_CUMULO_SUBINCISO){
				tipoDistribucionString = "Por inciso";
				numeroInciso = obtenerDescripcionIncisoSubInciso(estadoCuenta.getEstadoCuentaDTO().getContratoFacultativoDTO().getSlipDTO().getNumeroInciso());
				if(numeroInciso == null)
					numeroInciso = "PR/LUC";
				numeroSubInciso = obtenerDescripcionIncisoSubInciso(estadoCuenta.getEstadoCuentaDTO().getContratoFacultativoDTO().getSlipDTO().getNumeroSubInciso());
				if(numeroSubInciso == null)
					numeroSubInciso = "PR/LUC";
			}
			
//			estadoCuenta.getEstadoCuentaDTO().getContratoFacultativoDTO().getLineaSoporteReaseguroDTOs()
			jsonRow.setDatos("0",estadoCuenta.getSubRamoDTO().getDescripcionSubRamo(),tipoDistribucionString,numeroInciso,numeroSubInciso,
					currencyFormatter.format(estadoCuenta.getSaldoTecnico().getSaldoAcumulado()));
			midasJsonBase.addRow(jsonRow);
		}
		return midasJsonBase.toString();
	}
	
	private String obtenerDescripcionIncisoSubInciso(BigDecimal numero){
		String descripcion = null;
		if(numero != null && numero.compareTo(BigDecimal.ZERO) > 0)
			descripcion = numero.toBigInteger().toString();
		return descripcion;
	}
	
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(BigDecimal idToEstadoCuenta,short claveFormatoEstadoCuenta) throws SystemException{
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaConSaldosCalculados(idToEstadoCuenta, claveFormatoEstadoCuenta);
	}
	
	private Short calculaClaveFormatoDefault(EstadoCuentaDTO estadoCuentaDTO){
		Short claveFormatoEstadoCuenta = Sistema.CLAVE_FORMATO_DEFAULT_AUTOMATICO;
		if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			claveFormatoEstadoCuenta = Sistema.CLAVE_FORMATO_DEFAULT_FACULTATIVO;
		}
		return claveFormatoEstadoCuenta;
	}
	
	public String getJsonDetalleAcumulador(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO){
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		List<SaldoConceptoDTO> saldoConceptoDTOList = estadoCuentaDecoradoDTO.getSaldosAcumulados();
		MidasJsonRow midasJsonRow;
		int idRow = -1;
		if (estadoCuentaDecoradoDTO.getSaldoTecnico() != null)
			saldoConceptoDTOList.add(estadoCuentaDecoradoDTO.getSaldoTecnico());
		
		if (saldoConceptoDTOList != null){
			StringBuilder concepto = new StringBuilder("");
			StringBuilder saldo = new StringBuilder("");
			for (SaldoConceptoDTO saldoConceptoDTO : saldoConceptoDTOList) {
				idRow++;
				midasJsonRow = new MidasJsonRow();
				midasJsonRow.setId(String.valueOf(idRow));
				String fechaTrimestre = "";
				String debe;
				String haber;
				concepto.delete(0,concepto.length());
				saldo.delete(0,saldo.length());				
				concepto.append(saldoConceptoDTO.getDescripcionConcepto());
				if (concepto.toString().startsWith("Saldo Anterior")/*idRow==0*/){
					fechaTrimestre = saldoConceptoDTO.getFechaTrimestre() != null ? saldoConceptoDTO.getFechaTrimestre() : ""; // estadoCuentaDecoradoDTO.getDescripcionSuscripcion(); //TODO falta definir de donde saldr� este dato
				}else
					fechaTrimestre = "";
				
				if (concepto.toString().startsWith("Comisiones")){
					debe = saldoConceptoDTO.getDebe() != null ? currencyFormatter.format(saldoConceptoDTO.getDebe()) : "";
					haber = "";
				}else {
					if (saldoConceptoDTO.getDebe() == null)
						debe = "";
					else{
						if (concepto.toString().startsWith("Saldo Anterior") && saldoConceptoDTO.getDebe().compareTo(new BigDecimal("0")) == 0)
							debe = "";
						else{
							debe = currencyFormatter.format(saldoConceptoDTO.getDebe());
						}
					}
					if (saldoConceptoDTO.getHaber() == null)
						haber = "";
					else{
						if (concepto.toString().startsWith("Saldo Anterior") && saldoConceptoDTO.getHaber().compareTo(new BigDecimal("0")) == 0 && !(saldoConceptoDTO.getDebe().compareTo(new BigDecimal("0"))==0)){
							haber = "";
						}
						haber =  currencyFormatter.format(saldoConceptoDTO.getHaber());
					}
				}
				
				saldo.append(currencyFormatter.format(saldoConceptoDTO.getSaldoAcumulado().doubleValue()));
				if ((idRow+1) == saldoConceptoDTOList.size() || concepto.toString().startsWith("Saldo Anterior")/*idRow == 0*/){
					concepto.insert(0,"<b>").append("</b>");
					saldo.insert(0,"<b>").append("</b>");
				}
				midasJsonRow.setDatos(fechaTrimestre, concepto.toString(), debe, haber, saldo.toString());
				midasJsonBase.addRow(midasJsonRow);
			}
		}
		return midasJsonBase.toString();
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorSiniestro(BigDecimal idToReporteSiniestro,BigDecimal idMoneda,BigDecimal[] idTcSubRamos,Integer mes,Integer anio,boolean separarConceptosPorReasegurador) throws SystemException{
		Short claveFormatoEstadoCuenta = obtenerClaveFormatoEstadoCuenta(3);
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaPorSiniestro(idToReporteSiniestro, idMoneda,idTcSubRamos,mes,anio,separarConceptosPorReasegurador,claveFormatoEstadoCuenta);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaSaldoSiniestroPorReasegurador(BigDecimal idTcReasegurador,BigDecimal idToReporteSiniestro,BigDecimal idMoneda,BigDecimal idTcSubRamo,Integer mes,Integer anio) throws SystemException{
		Short claveFormatoEstadoCuenta = obtenerClaveFormatoEstadoCuenta(3);
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaPorReasegurador(idTcReasegurador,idMoneda,idToReporteSiniestro,idTcSubRamo,mes,anio,claveFormatoEstadoCuenta);
	}
	
	/**
	 * Consulta el archivo de recurso de mensajes en b�squeda de la clave de formato del estado de cuenta para los siguientes casos:
	 * tipo 1: Formato por default, mostrando CxP y CxC para los facultativos y los conceptos convencionales para autom�ticos.
	 * tipo 2: Formato para facultativos, mostrando saldos por p�liza.
	 * tipo 3: Formato para facultativos, mostrando saldos por siniestro.
	 * @param tipo
	 * @return
	 */
	private Short obtenerClaveFormatoEstadoCuenta(int tipo){
		String claveBusqueda = "";
		switch(tipo){
			case 1:
				claveBusqueda = "reaseguro.estadosdecuenta.contratofacultativo.claveFormatoPorDefault";
				break;
			case 2:
				claveBusqueda = "reaseguro.estadosdecuenta.contratofacultativo.claveFormatoPorPoliza";
				break;
			case 3:
				claveBusqueda = "reaseguro.estadosdecuenta.contratofacultativo.claveFormatoPorSiniestro";
				break;
			case 4:
				claveBusqueda = "reaseguro.estadosdecuenta.contratosproporcionales.claveFormatoPorDefault";
				break;
		}
		String claveFormato = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, claveBusqueda);
		Short claveFormatoEstadoCuenta = null;
		try{
			claveFormatoEstadoCuenta = Short.parseShort(claveFormato);
		}catch(Exception e){
			claveFormatoEstadoCuenta = Sistema.CLAVE_FORMATO_DEFAULT_FACULTATIVO;
		}
		return claveFormatoEstadoCuenta;
	}
	
	/**
	 * Consulta un estado de cuenta, correspondiente a una suscripci�n, y los estados de cuenta de suscripciones anteriores, adem�s consulta el estado 
	 * de cuenta de los contratos cuota parte � primer excedente de a�os contractuales anteriores y combina los saldos en un estado de cuenta �nico.
	 * @param idToEstadoCuenta
	 * @return
	 * @throws SystemException 
	 */
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaHistoricoCombinado(BigDecimal idToEstadoCuenta) throws SystemException{
		short claveFormato = obtenerClaveFormatoEstadoCuenta(4);
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaHistoricoCombinado(idToEstadoCuenta,claveFormato);
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador, BigDecimal idMoneda, int ejercicio, int suscripcion) throws SystemException{
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaFacultativoPorSuscripcion(idTcReasegurador, idMoneda, ejercicio, suscripcion);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,int ejercicio, int suscripcion) throws SystemException{
		return new PresentacionEstadoCuentaSN().obtenerEstadoCuentaFacultativoPorSuscripcion(idTcReasegurador, idMoneda, idTcSubRamo, ejercicio, suscripcion);
	}
}
