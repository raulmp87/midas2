package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

class ReporteEstadoCuentaSN {

	private ReporteEstadoCuentaServiciosRemote beanRemoto;
	
	public ReporteEstadoCuentaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		try{
		beanRemoto = serviceLocator.getEJB(ReporteEstadoCuentaServiciosRemote.class);
		}
		catch(SystemException e){
			LogDeMidasWeb.log("Error al buscar bean remoto: "+ReporteEstadoCuentaServiciosRemote.class, Level.SEVERE, e);
			throw e;
		}
	}
	
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorPagar(BigDecimal idToPoliza,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos){
		return beanRemoto.obtenerMovimientosReaseguroCuentasPorPagar(idToPoliza, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,fechaInicio,fechaFin, incluirSoloFacultativo,incluirSoloAutomaticos);
	}

	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorCobrar(BigDecimal idToReporteSiniestro,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos){
		return beanRemoto.obtenerMovimientosReaseguroCuentasPorCobrar(idToReporteSiniestro, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,fechaInicio,fechaFin, incluirSoloFacultativo,incluirSoloAutomaticos);
	}
	
	public ReporteSaldoTrimestralDTO obtenerSaldosPorReasegurador(BigDecimal idTcReasegurador,Integer[] idTcTipoReaseguro,Integer ejercicio,Integer suscripcion,
			boolean incluirEjerciciosAnteriores,BigDecimal idTcMoneda,boolean excluirSaldoCorredor){
		return beanRemoto.obtenerSaldosPorReasegurador(idTcReasegurador, idTcTipoReaseguro, ejercicio, suscripcion, incluirEjerciciosAnteriores, idTcMoneda, excluirSaldoCorredor);
	}
	
	public ReporteReaseguradorNegociosFacultativosDTO obtenerReporteMovimientosFacultativosTrimestre(BigDecimal idTcReasegurador,Integer ejercicio,Integer suscripcion,boolean excluirSaldoCorredor){
		return beanRemoto.obtenerReporteMovimientosFacultativosTrimestre(idTcReasegurador, ejercicio, suscripcion, excluirSaldoCorredor);
	}
}
