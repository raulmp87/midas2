package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ReporteEstadoCuentaDN {

	private static final ReporteEstadoCuentaDN instancia = new ReporteEstadoCuentaDN();
	
	public static ReporteEstadoCuentaDN getInstancia() {
		return instancia;
	}

	private ReporteEstadoCuentaDN(){
	}
	
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorPagar(BigDecimal idToPoliza,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,boolean incluirSoloFacultativo) throws SystemException{
		return new ReporteEstadoCuentaSN().obtenerMovimientosReaseguroCuentasPorPagar(idToPoliza, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,null,null, incluirSoloFacultativo,false);
	}
	
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorCobrar(BigDecimal idToReporteSiniestro,
			BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal[] idTcSubRamo,Integer mes,Integer anio,boolean incluirSoloFacultativo) throws SystemException{
		return new ReporteEstadoCuentaSN().obtenerMovimientosReaseguroCuentasPorCobrar(idToReporteSiniestro, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,null,null, incluirSoloFacultativo,false);
	}
	
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorPagar(BigDecimal idMoneda,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos) throws SystemException{
		return new ReporteEstadoCuentaSN().obtenerMovimientosReaseguroCuentasPorPagar(null, null, idMoneda, null, null, null,fechaInicio,fechaFin, incluirSoloFacultativo,incluirSoloAutomaticos);
	}
	
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosReaseguroCuentasPorCobrar(BigDecimal idMoneda,Date fechaInicio,Date fechaFin,boolean incluirSoloFacultativo,boolean incluirSoloAutomaticos) throws SystemException{
		return new ReporteEstadoCuentaSN().obtenerMovimientosReaseguroCuentasPorCobrar(null, null, idMoneda, null, null, null,fechaInicio,fechaFin, incluirSoloFacultativo,incluirSoloAutomaticos);
	}
	
	public ReporteSaldoTrimestralDTO obtenerSaldosPorReasegurador(BigDecimal idTcReasegurador,Integer[] idTcTipoReaseguro,Integer ejercicio,Integer suscripcion,
			boolean incluirEjerciciosAnteriores,BigDecimal idTcMoneda,boolean excluirSaldoCorredor) throws SystemException{
		return new ReporteEstadoCuentaSN().obtenerSaldosPorReasegurador(idTcReasegurador, idTcTipoReaseguro, ejercicio, suscripcion, incluirEjerciciosAnteriores, idTcMoneda, excluirSaldoCorredor);
	}
	
	public ReporteReaseguradorNegociosFacultativosDTO obtenerReporteMovimientosFacultativosTrimestre(BigDecimal idTcReasegurador,Integer ejercicio,Integer suscripcion,boolean excluirSaldoCorredor) throws SystemException{
		return new ReporteEstadoCuentaSN().obtenerReporteMovimientosFacultativosTrimestre(idTcReasegurador, ejercicio, suscripcion, excluirSaldoCorredor);
	}
}
