package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

class PresentacionEstadoCuentaSN {
	private PresentacionEstadoCuentaServiciosRemote beanRemoto;
	
	public PresentacionEstadoCuentaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		try{
		beanRemoto = serviceLocator.getEJB(PresentacionEstadoCuentaServiciosRemote.class);
		}
		catch(SystemException e){
			LogDeMidasWeb.log("Error al buscar bean remoto: "+PresentacionEstadoCuentaServiciosRemote.class, Level.SEVERE, e);
			throw e;
		}
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(BigDecimal idToEstadoCuenta,short claveFormatoEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaConSaldosCalculados(idToEstadoCuenta, claveFormatoEstadoCuenta);
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,short claveFormatoEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza, claveFormatoEstadoCuenta);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal[] idTcSubRamo,BigDecimal[] idTmContratoFacultativo,Integer mes,Integer anio,
			boolean separarConceptosPorReasegurador,short claveFormatoEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza, idTcSubRamo,idTmContratoFacultativo,mes,anio,separarConceptosPorReasegurador, claveFormatoEstadoCuenta);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorSiniestro(BigDecimal idToReporteSiniestro,BigDecimal idMoneda,BigDecimal[] idTcSubRamos,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,Short claveFormatoEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaPorSiniestro(idToReporteSiniestro, idMoneda,idTcSubRamos,mes,anio,separarConceptosPorReasegurador,claveFormatoEstadoCuenta);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaCombinadoConSaldosCalculados(BigDecimal idToPoliza,BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,Integer mes,Integer anio,boolean separarConceptosPorReasegurador,Short claveFormatoEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaCombinadoConSaldosCalculados(idToPoliza,idTcReasegurador,idMoneda,idTcSubRamo,false,null,true, mes, anio, separarConceptosPorReasegurador, claveFormatoEstadoCuenta);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorReasegurador(BigDecimal idTcReasegurador,BigDecimal idMoneda, BigDecimal idToSiniestro,BigDecimal idTcSubRamo,Integer mes,Integer anio,Short claveFormatoEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaPorReasegurador(idTcReasegurador, idMoneda, idToSiniestro, mes, anio, idTcSubRamo, claveFormatoEstadoCuenta);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaHistoricoCombinado(BigDecimal idToEstadoCuenta,short claveFormato){
		return beanRemoto.obtenerEstadoCuentaHistoricoCombinado(idToEstadoCuenta,claveFormato);
	}

	public List<EstadoCuentaDecoradoDTO> obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,int ejercicio, int suscripcion){
		return beanRemoto.obtenerEstadoCuentaFacultativoPorSuscripcion(idTcReasegurador, idMoneda, ejercicio, suscripcion);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaFacultativoPorSuscripcion(BigDecimal idTcReasegurador,BigDecimal idMoneda,BigDecimal idTcSubRamo,int ejercicio, int suscripcion){
		return beanRemoto.obtenerEstadoCuentaFacultativoPorSuscripcion(idTcReasegurador, idMoneda, idTcSubRamo, ejercicio, suscripcion);
	}
}
