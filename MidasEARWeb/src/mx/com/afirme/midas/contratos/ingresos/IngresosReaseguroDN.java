package mx.com.afirme.midas.contratos.ingresos;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IngresosReaseguroDN {
	private static final IngresosReaseguroDN INSTANCIA = new IngresosReaseguroDN();

	public static IngresosReaseguroDN getInstancia() {
		return IngresosReaseguroDN.INSTANCIA;
	}
	
	public List<IngresoReaseguroDTO> listarIngresos() throws SystemException, ExcepcionDeAccesoADatos {
		return new IngresosReaseguroSN().listarIngresos();
	}
	
	public void agregarIngresos(IngresoReaseguroDTO ir) throws SystemException, ExcepcionDeAccesoADatos {
		new IngresosReaseguroSN().agregarIngreso(ir);
	}
	
	public void borrarIngresos(IngresoReaseguroDTO ir) throws SystemException, ExcepcionDeAccesoADatos {
		new IngresosReaseguroSN().borrarIngreso(ir);
	}
	
	public IngresoReaseguroDTO modificarIngresos(IngresoReaseguroDTO ir) throws SystemException, ExcepcionDeAccesoADatos {
		return new IngresosReaseguroSN().modificarIngreso(ir);
	}

	public IngresoReaseguroDTO obtenerIngresoPorId(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos {
		return new IngresosReaseguroSN().obtenerIngresoPorId(id);
	}

}