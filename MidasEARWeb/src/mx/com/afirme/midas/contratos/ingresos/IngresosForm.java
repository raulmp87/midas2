
package mx.com.afirme.midas.contratos.ingresos;

import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class IngresosForm extends MidasBaseForm{
	private static final long serialVersionUID = 1L;
	private String montoIngreso;
	private String moneda;
	private String referencia;
	private String fechaIngreso;
	private String numeroCuenta;
	private String idIngresoReaseguro;
	private String notificarReaseguro;
	private String notificarSiniestro;
	private List<MonedaDTO> monedaDTOList;
	
	public String getIdIngresoReaseguro(){
		return idIngresoReaseguro;
	}
	public void setIdIngresoReaseguro(String idIngresoReaseguro){
		this.idIngresoReaseguro = idIngresoReaseguro;
	}
	public String getMontoIngreso() {
		return montoIngreso;
	}
	public void setMontoIngreso(String montoIngreso) {
		this.montoIngreso = montoIngreso;
	}	
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}	
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}	
	public String getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}	
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getNotificarReaseguro() {
		return notificarReaseguro;
	}
	public void setNotificarReaseguro(String notificarReaseguro) {
		this.notificarReaseguro = notificarReaseguro;
	}
	public String getNotificarSiniestro() {
		return notificarSiniestro;
	}
	public void setNotificarSiniestro(String notificarSiniestro) {
		this.notificarSiniestro = notificarSiniestro;
	}
	public List<MonedaDTO> getMonedaDTOList() {
		return monedaDTOList;
	}
	public void setMonedaDTOList(List<MonedaDTO> monedaDTOList) {
		this.monedaDTOList = monedaDTOList;
	}	
	
}
