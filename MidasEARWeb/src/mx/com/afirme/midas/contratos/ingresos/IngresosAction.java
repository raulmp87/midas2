package mx.com.afirme.midas.contratos.ingresos;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.contratos.ingreso.ConceptoIngresoReaseguroDTO;
import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class IngresosAction extends MidasMappingDispatchAction{

	public ActionForward registrarIngresosReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		List<MonedaDTO> monedaDTOList;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {
			String idIngresoReaseguro = request.getParameter("idIngresoReaseguro");
			monedaDTOList = MonedaDN.getInstancia().listarTodos();
			IngresosForm ingresosForm = (IngresosForm) form;
			IngresoReaseguroDTO ingresoReaseguroDTO;
			if (idIngresoReaseguro != null && !idIngresoReaseguro.equals("undefined") && !idIngresoReaseguro.equals("")){
				ingresoReaseguroDTO = IngresosReaseguroDN.getInstancia().obtenerIngresoPorId(new BigDecimal(idIngresoReaseguro));
				ingresosForm.setIdIngresoReaseguro(ingresoReaseguroDTO.getIdIngresoReaseguro().toBigInteger().toString());
				ingresosForm.setMoneda(String.valueOf(ingresoReaseguroDTO.getIdMoneda()));
			}else{
				ingresosForm.setMoneda("");
				ingresosForm.setFechaIngreso(simpleDateFormat.format(new Date()));
			}
			ingresosForm.setMonedaDTOList(monedaDTOList);
			ingresosForm.setNotificarReaseguro("1");
			ingresosForm.setNotificarSiniestro("1");
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	public ActionForward mostrarRegistrarIngresosReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	public void listarIngresosReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try{
			NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
			numberFormat.setMaximumFractionDigits(4);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			List<IngresoReaseguroDTO> ingresoReaseguroDTO = new ArrayList<IngresoReaseguroDTO>();
			ingresoReaseguroDTO = IngresosReaseguroDN.getInstancia().listarIngresos();
			MidasJsonBase json = new MidasJsonBase();
			for(int i = 0; i < ingresoReaseguroDTO.size(); i++){
				MidasJsonRow row = new MidasJsonRow();
				IngresoReaseguroDTO ingresoReaseguroTemp = ingresoReaseguroDTO.get(i);
				String id = ingresoReaseguroTemp.getIdIngresoReaseguro().toBigInteger().toString();
				row.setId(id);
				row.setDatos(id,"<b>" + numberFormat.format(ingresoReaseguroTemp.getMonto()) + "</b>",ingresoReaseguroTemp.getMonto().toString(),
						(ingresoReaseguroTemp.getIdMoneda() == 840? "D&oacute;lares":"Pesos"),
						ingresoReaseguroTemp.getReferencia(),
						ingresoReaseguroTemp.getNumeroCuenta().toBigInteger().toString(),
						//UtileriasWeb.getFechaStringConNombreMes(ingresoReaseguroTemp.getFechaIngreso()),
						simpleDateFormat.format(ingresoReaseguroTemp.getFechaIngreso()),
						simpleDateFormat.format(ingresoReaseguroTemp.getFechaIngreso()),
						"0");
				json.addRow(row);
			}
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	public void agregarIngresosReaseguro(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		DecimalFormat formatoDec = new DecimalFormat("0.0000");
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
		numberFormat.setMaximumFractionDigits(4);
		IngresoReaseguroDTO ingresoReaseguroDTO = new IngresoReaseguroDTO();
		IngresosForm ingresosForm = (IngresosForm) form;		 
		try{
			Double monto = new Double(ingresosForm.getMontoIngreso());
			String notificarReaseguro = ingresosForm.getNotificarReaseguro();
			String notificarSiniestro = ingresosForm.getNotificarSiniestro();
			ingresoReaseguroDTO.setMonto(new Double(formatoDec.format(monto)));
			ingresoReaseguroDTO.setIdMoneda(new Integer(ingresosForm.getMoneda()).intValue());
			ingresoReaseguroDTO.setReferencia(ingresosForm.getReferencia());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			ingresoReaseguroDTO.setFechaIngreso(sdf.parse((ingresosForm.getFechaIngreso())));
			ingresoReaseguroDTO.setNumeroCuenta(new BigDecimal(ingresosForm.getNumeroCuenta()));
			ConceptoIngresoReaseguroDTO con = new ConceptoIngresoReaseguroDTO();
			con.setIdConcepto(Byte.parseByte("18"));
			ingresoReaseguroDTO.setConceptoIngresoReaseguro(con);
		
			IngresosReaseguroDN.getInstancia().agregarIngresos(ingresoReaseguroDTO);
			List<String> destinatarios = new ArrayList<String>();
			String[] destinatariosStr;
			String listaDestinatarios = "";
			String[] nombre_mail;

			String titulo = "Nuevo Ingreso generado el " + UtileriasWeb.getFechaHoraString(new Date()) + "hrs";
			String contenido;
			String moneda = "";

			if (notificarReaseguro != null && notificarReaseguro.equals("1")){
				listaDestinatarios = "Emmanuel Ramirez Lango;eramirezl@afirme.com.mx,Gabriela Virginia Anguiano Carrillo;gabriela.anguiano@afirme.com,Adriana Concepcion Cantu Cantu;acantuc@afirme.com.mx," +
				"Diana Mart�nez Villarreal;diana.martinez@afirme.com,Julieta Soledad Villarreal Cant�;julieta.villarreal@afirme.com," +
				"Maria Ang�lica Mart�nez Olvera;ajimenezo@afirme.com.mx,Maria Ang�lica Mart�nez Olvera;maria.martinez@afirme.com" +
				"Omar Alejandro Cavazos Gonz�lez;omar.cavazos@afirme.com,Myriam Trevi�o Carranza;myriam.trevino@afirme.com";
			}

			if ( notificarSiniestro != null && notificarSiniestro.equals("1")){
				if (listaDestinatarios.length() > 0)
					listaDestinatarios = listaDestinatarios + ",";

				listaDestinatarios = listaDestinatarios + "Walter Carlos Ehrenstein Adame;walter.ehrenstein@afirme.com,Luis Roberto Trevi�o P�rez;ltrevinot@afirme.com.mx";
			}

			if (!UtileriasWeb.esCadenaVacia(listaDestinatarios)){
				String nombreUsuario = "";
				Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_MIDAS);
				if (usuario == null){
					usuario = new Usuario();
					usuario.setNombre("An�nimo");
					usuario.setApellidoPaterno("");
					usuario.setApellidoMaterno("");
				}
				nombreUsuario = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				destinatariosStr = listaDestinatarios.split(",");
				for (String destinatario : destinatariosStr){
					nombre_mail = destinatario.split(";");
					destinatarios = new ArrayList<String>();
					destinatarios.add(nombre_mail[1]);

					if (ingresoReaseguroDTO.getIdMoneda() == 484)
						moneda = "Pesos";
					if (ingresoReaseguroDTO.getIdMoneda() == 840)
						moneda = "D&oacute;lares";

					contenido = "Hola <b>" + nombre_mail[0] + "</b>" + "." + 
					"<br />Por medio de la presente se informa que el usuario " + nombreUsuario + " ha registrado un Ingreso con los siguientes datos:" +
					"<br />Monto: <b>" + numberFormat.format(ingresoReaseguroDTO.getMonto()) + "</b>" +
					"<br />Moneda: <b>" + moneda + "</b>" +
					"<br />Referencia: <b>" + ingresoReaseguroDTO.getReferencia() + "</b>" +
					"<br />Fecha del ingreso: <b>" + UtileriasWeb.getFechaStringConNombreMes(ingresoReaseguroDTO.getFechaIngreso()) + "</b>" +
					"<br />N&uacute;mero de Cuenta: <b>" + ingresoReaseguroDTO.getNumeroCuenta().toBigInteger().toString() +  "</b>" +
					"<br />Saludos cordiales." +
					"<br /><i>Notificaci&oacute;n autom&aacute;tica del sistema de Reaseguro.</i>";
					MailAction.enviaCorreo(destinatarios, titulo, contenido);
				}
			}
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ParseException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}

	public void borrarIngresosReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		IngresoReaseguroDTO ingresoReaseguroDTO = new IngresoReaseguroDTO();
		ingresoReaseguroDTO.setIdIngresoReaseguro(new BigDecimal(request.getParameter("idIngresoReaseguro")));
		try {
			IngresosReaseguroDN.getInstancia().borrarIngresos(ingresoReaseguroDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}

	public void modificarIngresosReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try {
			
			NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
			numberFormat.setMaximumFractionDigits(4);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			IngresosForm ingresosForm = (IngresosForm) form;
			String notificarReaseguro = ingresosForm.getNotificarReaseguro();
			String notificarSiniestro = ingresosForm.getNotificarSiniestro();
			IngresoReaseguroDTO ingresoReaseguroDTO = IngresosReaseguroDN.getInstancia().obtenerIngresoPorId(new BigDecimal(ingresosForm.getIdIngresoReaseguro()));
			
			ingresoReaseguroDTO.setReferencia(ingresosForm.getReferencia());
			ingresoReaseguroDTO.setFechaIngreso(simpleDateFormat.parse((ingresosForm.getFechaIngreso())));
			ingresoReaseguroDTO.setNumeroCuenta(new BigDecimal(ingresosForm.getNumeroCuenta()));
			
			IngresosReaseguroDN.getInstancia().modificarIngresos(ingresoReaseguroDTO);
			
			List<String> destinatarios = new ArrayList<String>();
			String[] destinatariosStr;
			String listaDestinatarios = "";
			String[] nombre_mail;

			String titulo = "Ingreso modificado el " + UtileriasWeb.getFechaHoraString(new Date()) + "hrs";
			String contenido;
			String moneda = "";

			if (notificarReaseguro != null && notificarReaseguro.equals("1")){
				listaDestinatarios = "Emmanuel Ramirez Lango;eramirezl@afirme.com.mx,Gabriela Virginia Anguiano Carrillo;gabriela.anguiano@afirme.com,Adriana Concepcion Cantu Cantu;acantuc@afirme.com.mx," +
				"Diana Mart�nez Villarreal;diana.martinez@afirme.com,Julieta Soledad Villarreal Cant�;julieta.villarreal@afirme.com," +
				"Maria Ang�lica Mart�nez Olvera;ajimenezo@afirme.com.mx,Maria Ang�lica Mart�nez Olvera;maria.martinez@afirme.com" +
				"Omar Alejandro Cavazos Gonz�lez;omar.cavazos@afirme.com,Myriam Trevi�o Carranza;myriam.trevino@afirme.com";
			}

			if ( notificarSiniestro != null && notificarSiniestro.equals("1")){
				if (listaDestinatarios.length() > 0)
					listaDestinatarios = listaDestinatarios + ",";

				listaDestinatarios = listaDestinatarios + "Walter Carlos Ehrenstein Adame;walter.ehrenstein@afirme.com,Luis Roberto Trevi�o P�rez;ltrevinot@afirme.com.mx";
			}

			if (!UtileriasWeb.esCadenaVacia(listaDestinatarios)){
				String nombreUsuario = "";
				Usuario usuario = (Usuario) request.getSession().getAttribute(Sistema.USUARIO_MIDAS);
				if (usuario == null){
					usuario = new Usuario();
					usuario.setNombre("An�nimo");
					usuario.setApellidoPaterno("");
					usuario.setApellidoMaterno("");
				}
				nombreUsuario = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				destinatariosStr = listaDestinatarios.split(",");
				for (String destinatario : destinatariosStr){
					nombre_mail = destinatario.split(";");
					destinatarios = new ArrayList<String>();
					destinatarios.add(nombre_mail[1]);

					if (ingresoReaseguroDTO.getIdMoneda() == 484)
						moneda = "Pesos";
					if (ingresoReaseguroDTO.getIdMoneda() == 840)
						moneda = "D&oacute;lares";

					contenido = "Hola <b>" + nombre_mail[0] + "</b>" + "." + 
					"<br />Por medio de la presente se informa que el usuario " + nombreUsuario + " modific&oacute; un Ingreso con los siguientes datos:" +
					"<br />Monto: <b>" + numberFormat.format(ingresoReaseguroDTO.getMonto()) + "</b>" +
					"<br />Moneda: <b>" + moneda + "</b>" +
					"<br />Referencia: <b>" + ingresoReaseguroDTO.getReferencia() + "</b>" +
					"<br />Fecha del ingreso: <b>" + UtileriasWeb.getFechaStringConNombreMes(ingresoReaseguroDTO.getFechaIngreso()) + "</b>" +
					"<br />N&uacute;mero de Cuenta: <b>" + ingresoReaseguroDTO.getNumeroCuenta().toBigInteger().toString() +  "</b>" +
					"<br />Saludos cordiales." +
					"<br /><i>Notificaci&oacute;n autom&aacute;tica del sistema de Reaseguro.</i>";
					MailAction.enviaCorreo(destinatarios, titulo, contenido);
				}
			}
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ParseException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
}