
package mx.com.afirme.midas.contratos.ingresos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroDTO;
import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IngresosReaseguroSN {
	private IngresoReaseguroFacadeRemote beanRemoto;
	
	public IngresosReaseguroSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(IngresoReaseguroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto RangoSumaAseguradaFacadeRemote instanciado", Level.FINEST, null);
	}
	
	public void agregarIngreso(IngresoReaseguroDTO ir) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(ir);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IngresoReaseguroDTO> listarIngresos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void borrarIngreso(IngresoReaseguroDTO ir) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(ir);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public IngresoReaseguroDTO modificarIngreso(IngresoReaseguroDTO ir) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(ir);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public IngresoReaseguroDTO obtenerIngresoPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
}