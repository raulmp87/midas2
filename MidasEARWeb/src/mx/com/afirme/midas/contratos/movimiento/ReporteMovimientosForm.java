package mx.com.afirme.midas.contratos.movimiento;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteMovimientosForm extends MidasBaseForm{
	private static final long serialVersionUID = 1L;
	private String movimientos;
	private String moneda;
	private String diaI;
	private String mesI;
	private String anoI;
	private String diaF;
	private String mesF;
	private String anoF;
	private String periodo;
	private String tipoCambio;
	
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}	
	
	public String getPeriodo() {
		return periodo;
	}
	public void setperiodo(String periodo) {
		this.periodo = periodo;
	}	
	public String getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(String movimientos) {
		this.movimientos = movimientos;
	}	
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}	
	public String getDiaI() {
		return diaI;
	}
	public void setDiaI(String diaI) {
		this.diaI = diaI;
	}	
	public String getMesI() {
		return mesI;
	}
	public void setMesI(String mesI) {
		this.mesI = mesI;
	}	
	public String getAnoI() {
		return anoI;
	}
	public void setAnoI(String anoI) {
		this.anoI = anoI;
	}	
	public String getDiaF() {
		return diaF;
	}
	public void setDiaF(String diaF) {
		this.diaF = diaF;
	}	
	public String getMesF() {
		return mesF;
	}
	public void setMesF(String mesF) {
		this.mesF = mesF;
	}	
	public String getAnoF() {
		return anoF;
	}
	public void setAnoF(String anoF) {
		this.anoF = anoF;
	}	
}
