package mx.com.afirme.midas.contratos.cartera;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.rangosumaasegurada.RangoSumaAseguradaDTO;
import mx.com.afirme.midas.catalogos.rangosumaasegurada.RangoSumaAseguradaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReportePerfilCarteraSN {
	private RangoSumaAseguradaFacadeRemote beanRemoto;
	
	public ReportePerfilCarteraSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RangoSumaAseguradaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto RangoSumaAseguradaFacadeRemote instanciado", Level.FINEST, null);
	}

	public List<RangoSumaAseguradaDTO> listarRangos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void agregarRango(RangoSumaAseguradaDTO rsa) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(rsa);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public RangoSumaAseguradaDTO actualizarRango(RangoSumaAseguradaDTO rsa) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(rsa);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public void borrarRango(RangoSumaAseguradaDTO rsa) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(rsa);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
