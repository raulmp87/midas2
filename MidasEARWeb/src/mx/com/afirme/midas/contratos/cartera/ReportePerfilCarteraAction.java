package mx.com.afirme.midas.contratos.cartera;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.rangosumaasegurada.RangoSumaAseguradaDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.reaseguro.reportes.perfilcartera.ReportePerfilCartera;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportePerfilCarteraAction extends MidasMappingDispatchAction{
	
	public ActionForward reportePerfilCartera(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		ReportePerfilCarteraForm reportePerfilCarteraForm = (ReportePerfilCarteraForm) form;
		try{
			Double tipoCambio  =  TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorDia(new Date(), Short.parseShort("1"));
			reportePerfilCarteraForm.setTipoCambio(tipoCambio.toString());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		reportePerfilCarteraForm.setFechaInicial(UtileriasWeb.getFechaString(new Date()));
		reportePerfilCarteraForm.setFechaFinal(UtileriasWeb.getFechaString(new Date()));
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void rPC(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception{
		List<RangoSumaAseguradaDTO> rangoSumaAseguradaDTO = new ArrayList<RangoSumaAseguradaDTO>();
		rangoSumaAseguradaDTO = ReportePerfilCarteraDN.getInstancia().listarRangos();
		MidasJsonBase json = new MidasJsonBase();
		int total = rangoSumaAseguradaDTO.size();
		long rangoAnt = 0;
		String id = "";
		for(int i = 0; i < total-1; i++){	
			for(int j = i; j < total; j++){
				if(rangoSumaAseguradaDTO.get(i).getIdTcRangoSumaAsegurada() > rangoSumaAseguradaDTO.get(j).getIdTcRangoSumaAsegurada()){
					RangoSumaAseguradaDTO temp = rangoSumaAseguradaDTO.get(i);
					rangoSumaAseguradaDTO.set(i, rangoSumaAseguradaDTO.get(j));
					rangoSumaAseguradaDTO.set(j, temp);
				}
			}
		}
		RangoSumaAseguradaDTO temp = null;
		if(total>0){
			for(int i = 0; i< total; i++){
				MidasJsonRow row = new MidasJsonRow();
				temp = rangoSumaAseguradaDTO.get(i);
				id = temp.getIdTcRangoSumaAsegurada() + "";
				row.setId(id);
				row.setDatos(id,
						temp.getPosicion()+"",
						rangoAnt+"",
						Math.round(temp.getRango())+"",
						"Seleccionar");
				json.addRow(row);
				rangoAnt = Math.round(temp.getRango()) + 1;
			}
			MidasJsonRow row = new MidasJsonRow();
			id = (Integer.parseInt(id)+1) +"";
			row.setId(id);
			row.setDatos(id, 
					(temp.getPosicion()+1)+"",
					rangoAnt+"",
					"Nuevo Rango",
					"Seleccionar");
			json.addRow(row);
		}else{
			MidasJsonRow row = new MidasJsonRow();
			id = "1";
			row.setId(id);
			row.setDatos(id, 
					"1",
					"0",
			"Nuevo Rango",
			"Seleccionar");
			json.addRow(row);
		}
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json.toString());
		pw.flush();
		pw.close();
	}
	
	public void agregarRangoSumaAsegurada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception{
		RangoSumaAseguradaDTO nueva = new RangoSumaAseguradaDTO();
		nueva.setIdTcRangoSumaAsegurada(Integer.parseInt(request.getParameter("idTcRangoSumaAsegurada")));
		nueva.setPosicion(Integer.parseInt(request.getParameter("posicion")));
		nueva.setRango(Double.parseDouble(request.getParameter("rango")));
		ReportePerfilCarteraDN.getInstancia().agregarRango(nueva);
	}
	
	public void borrarRangoSumaAsegurada(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception{
		List<RangoSumaAseguradaDTO> rangoSumaAseguradaDTO = new ArrayList<RangoSumaAseguradaDTO>();
		rangoSumaAseguradaDTO = ReportePerfilCarteraDN.getInstancia().listarRangos();
		int total = rangoSumaAseguradaDTO.size();
		for(int i = 0; i < total-1; i++){	
			for(int j = i; j < total; j++){
				if(rangoSumaAseguradaDTO.get(i).getIdTcRangoSumaAsegurada() > rangoSumaAseguradaDTO.get(j).getIdTcRangoSumaAsegurada()){
					RangoSumaAseguradaDTO temp = rangoSumaAseguradaDTO.get(i);
					rangoSumaAseguradaDTO.set(i, rangoSumaAseguradaDTO.get(j));
					rangoSumaAseguradaDTO.set(j, temp);
				}
					
			}
		}
		int borrar = Integer.parseInt(request.getParameter("idTcRangoSumaAsegurada")) - 1;
		RangoSumaAseguradaDTO temp;
		RangoSumaAseguradaDTO temp2 = rangoSumaAseguradaDTO.get(borrar);
		for(int i = borrar; i<total-1; i++){
			temp = temp2;
			temp2= rangoSumaAseguradaDTO.get(i+1);
			temp.setRango(temp2.getRango());
			temp = ReportePerfilCarteraDN.getInstancia().actualizarRango(temp);
		}
		ReportePerfilCarteraDN.getInstancia().borrarRango(temp2);
	}
	

	public ActionForward rptPerfilCartera(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		
		try {
			double tipoCambio = Double.valueOf(request.getParameter("tipoCambio"));
			
			int tipoReporte = 1;
			try{
				tipoReporte = Integer.valueOf(request.getParameter("tipoReportePC"));
			}catch(Exception e){
			}
			
			ReportePerfilCartera reporte = new ReportePerfilCartera(
					UtileriasWeb.getFechaFromString(fechaInicialStr),
					UtileriasWeb.getFechaFromString(fechaFinalStr),
					tipoCambio,tipoReporte);
			
			byte[] byteArrayReport = reporte.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
			
			this.writeBytes(response, byteArrayReport, Sistema.TIPO_XLS, "ReportePerfilCartera_Del_"+fechaInicialStr+"_Al_"+fechaFinalStr);

			return null;
		} catch(Exception e){
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>Reporte PerfilCartera. Parametros: fechaInicio: "+fechaInicialStr+" , " +
					" fechaFin: "+fechaFinalStr+", tipoCambio: "+request.getParameter("tipoCambio")+"</h3>"+
					"<h3>"+e+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}	
	
	
}