package mx.com.afirme.midas.contratos.cartera;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReportePerfilCarteraForm extends MidasBaseForm{
	private static final long serialVersionUID = 1L;
	private String movimientos;
	private String moneda;
	private String fechaInicial;
	private String fechaFinal;
	private String rango;
	private String generarPerfil;
	private String tipoCambio;
	
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}	
	
	public String getGenerarPerfil() {
		return generarPerfil;
	}
	public void setGenerarPerfil(String generarPerfil) {
		this.generarPerfil = generarPerfil;
	}	

	public String getRango() {
		return rango;
	}
	public void setRango(String rango) {
		this.rango = rango;
	}	
	
	public String getMovimientos() {
		return movimientos;
	}
	public void setMovimientos(String movimientos) {
		this.movimientos = movimientos;
	}	
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}	
	
}
