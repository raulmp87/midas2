package mx.com.afirme.midas.contratos.cartera;

import java.util.List;

import mx.com.afirme.midas.catalogos.rangosumaasegurada.RangoSumaAseguradaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReportePerfilCarteraDN {
	private static final ReportePerfilCarteraDN INSTANCIA = new ReportePerfilCarteraDN();

	public static ReportePerfilCarteraDN getInstancia() {
		return ReportePerfilCarteraDN.INSTANCIA;
	}
	
	public List<RangoSumaAseguradaDTO> listarRangos() throws SystemException, ExcepcionDeAccesoADatos {
		return new ReportePerfilCarteraSN().listarRangos();
	}
	
	public void agregarRango(RangoSumaAseguradaDTO rsa) throws SystemException, ExcepcionDeAccesoADatos {
		new ReportePerfilCarteraSN().agregarRango(rsa);
	}
	
	public RangoSumaAseguradaDTO actualizarRango(RangoSumaAseguradaDTO rsa) throws SystemException, ExcepcionDeAccesoADatos {
		return new ReportePerfilCarteraSN().actualizarRango(rsa);
	}
	
	public void borrarRango(RangoSumaAseguradaDTO rsa) throws SystemException{
		new ReportePerfilCarteraSN().borrarRango(rsa);
	}
}
