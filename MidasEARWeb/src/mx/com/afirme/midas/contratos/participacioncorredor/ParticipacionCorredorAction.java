package mx.com.afirme.midas.contratos.participacioncorredor;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;

public class ParticipacionCorredorAction extends MidasMappingDispatchAction {		

	
	/**
	 * Method listarReaseguradores
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarReaseguradores(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;
		try {			
			participacionCorredorForm.setIdTcReaseguradorCorredor(null);
			participacionCorredorForm.setPorcentajeParticipacion(null);
			ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
			String idParticipacion = request.getParameter("id");
			participacionCorredorForm.setIdTdParticipacion(idParticipacion);
			ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
			ParticipacionDTO participacionDTO = participacionDN.getPorId(new BigDecimal(idParticipacion));
			this.poblarForm(participacionDTO, participacionCorredorForm);			
			this.poblarDTO(participacionCorredorForm, participacionCorredorDTO);
			
			participacionCorredorForm.setPorcentajeParticipacionRestante(calcularPorcentajeRestante(form));
			
			ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
			List<ParticipacionCorredorDTO> participacionesCorredor = participacionCorredorDN.listarFiltrados(participacionCorredorDTO);
			request.setAttribute("participacionesCorredor", participacionesCorredor);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method cargarReaseguradores
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void cargarReaseguradores(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String idTdParticipacion = request.getParameter("idTdParticipacion");
		ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;
		
		if (!UtileriasWeb.esObjetoNulo(idTdParticipacion) && !UtileriasWeb.esCadenaVacia(idTdParticipacion))
			idTdParticipacion = participacionCorredorForm.getIdTdParticipacion();
		
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
		
		try {
			ParticipacionDTO participacionDTO = participacionDN.getPorId(new BigDecimal(idTdParticipacion));
			//ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
			//ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			//participacionCorredorDTO.setParticipacion(participacionDTO);
			//participacionCorredorDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
			List<ParticipacionCorredorDTO> participaciones = participacionCorredorDN.getPorPropiedad("participacion", participacionDTO);
			
			String json = this.getJson(participaciones);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);		
		}
		
	}
	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			
			ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;
			try {
				participacionCorredorForm.setPorcentajeParticipacionRestante(calcularPorcentajeRestante(form));
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);	
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);	
			}
			
			return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {			
			ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;			
			ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
			ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia(); 
			ActionForward forward;
			String reglaNavegacion = Sistema.EXITOSO;
			try {
				this.poblarDTO(participacionCorredorForm, participacionCorredorDTO);
				participacionCorredorForm.setPorcentajeParticipacionRestante(calcularPorcentajeRestante(form));
				participacionCorredorDN.agregar(participacionCorredorDTO);				
				forward = new ActionForward(mapping.findForward(reglaNavegacion));		
				String path = forward.getPath();
				forward.setPath(path + "?id=" + participacionCorredorForm.getIdTdParticipacion());			
			} catch (ExcepcionDeAccesoADatos e) {			
				forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			} catch (SystemException e) {			
				forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			}
			
			return forward;
	}
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {		
		ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;			
		ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();		
		ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();		
		ActionForward forward;
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			this.poblarDTO(participacionCorredorForm, participacionCorredorDTO);
			participacionCorredorForm.setPorcentajeParticipacionRestante(calcularPorcentajeRestante(form));
			participacionCorredorDN.modificar(participacionCorredorDTO);
			forward = new ActionForward(mapping.findForward(reglaNavegacion));		
			String path = forward.getPath();
			forward.setPath(path + "?id=" + participacionCorredorForm.getIdTdParticipacion());
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(reglaNavegacion));
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(reglaNavegacion));
		}

		return forward;

	}

	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;
		ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();				
		ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
		ActionForward forward;
		try {
			//this.poblarDTO(participacionCorredorForm, participacionCorredorDTO);
			participacionCorredorDTO = participacionCorredorDN.getPorId(new BigDecimal(participacionCorredorForm.getIdTdParticipacionCorredor()));
			participacionCorredorDN.borrar(participacionCorredorDTO);
			forward = new ActionForward(mapping.findForward(Sistema.EXITOSO));		
			String path = forward.getPath();
			forward.setPath(path + "?id=" + participacionCorredorForm.getIdTdParticipacion());
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
		}

		return forward;

	}
	
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;
		ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
		String idParam = request.getParameter("id");
		BigDecimal id = new BigDecimal(idParam);
		ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
		try {
			participacionCorredorDTO = participacionCorredorDN.getPorId(id);
			this.poblarForm(participacionCorredorDTO, participacionCorredorForm);
			double porcentajeParticipacionRestante = Double.parseDouble(
					participacionCorredorForm.getPorcentajeParticipacionRestante());
			porcentajeParticipacionRestante += participacionCorredorDTO.getPorcentajeParticipacion(); //Sumarle el porcentaje del reasegurador actual.
			participacionCorredorForm.setPorcentajeParticipacionRestante(
					String.valueOf(porcentajeParticipacionRestante));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
						
	/**
	 * Method poblarDTO
	 * 
	 * @param ParticipacionCorredorForm
	 * @param ParticipacionDTO
	 */
	private void poblarDTO(ParticipacionCorredorForm participacionCorredorForm, 
	ParticipacionCorredorDTO participacionCorredorDTO) 
	throws ExcepcionDeAccesoADatos, SystemException{
		if (!StringUtil.isEmpty(participacionCorredorForm.getIdTdParticipacionCorredor())) {
			String id = participacionCorredorForm.getIdTdParticipacionCorredor();
			participacionCorredorDTO.setIdTdParticipacionCorredor(new BigDecimal(id));
		}
		
		if (!StringUtil.isEmpty(participacionCorredorForm.getIdTdParticipacion())) {			
			String id = participacionCorredorForm.getIdTdParticipacion();
			ParticipacionDN participacionDN = ParticipacionDN.getInstancia();			
			ParticipacionDTO participacionDTO;
			participacionDTO = participacionDN.getPorId(new BigDecimal(id));					
			participacionCorredorDTO.setParticipacion(participacionDTO);
		}else{
			participacionCorredorDTO.setParticipacion(new ParticipacionDTO());
		}				
					
		if (!StringUtil.isEmpty(participacionCorredorForm.getIdTcReaseguradorCorredor())) {			
			String id = participacionCorredorForm.getIdTcReaseguradorCorredor();			
			ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(id));
			reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			participacionCorredorDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
		}else{
			participacionCorredorDTO.setReaseguradorCorredor(new ReaseguradorCorredorDTO());
		}
		
		if (!StringUtil.isEmpty(participacionCorredorForm.getPorcentajeParticipacion())) {
			String porcentaje = participacionCorredorForm.getPorcentajeParticipacion();
			participacionCorredorDTO.setPorcentajeParticipacion(Double.valueOf(porcentaje));
		}
	}	
	

	/**
	 * Method poblarForm
	 * 
	 * @param ParticipacionCorredorDTO
	 * @param ParticipacionCorredorForm
	 * @throws ParseException 
	 */	
	private void poblarForm(ParticipacionCorredorDTO participacionCorredorDTO, 
	ParticipacionCorredorForm participacionCorredorForm){
		if(participacionCorredorDTO.getIdTdParticipacionCorredor()!=null){
			String id = participacionCorredorDTO.getIdTdParticipacionCorredor().toString();
			participacionCorredorForm.setIdTdParticipacionCorredor(id);		
		}
				
		if(participacionCorredorDTO.getParticipacion()!=null){
			String id = participacionCorredorDTO.getParticipacion().getIdTdParticipacion().toString();
			participacionCorredorForm.setIdTdParticipacion(id);
		}
						
		if(participacionCorredorDTO.getReaseguradorCorredor()!=null){
			String id = participacionCorredorDTO.getReaseguradorCorredor().getIdtcreaseguradorcorredor().toString();
			participacionCorredorForm.setIdTcReaseguradorCorredor(id);						
		}
		
		if(participacionCorredorDTO.getPorcentajeParticipacion()!=null){
			Double porcentaje = participacionCorredorDTO.getPorcentajeParticipacion();
			String porcentajeFormato = MessageFormat.format("{0,number,#.##}", porcentaje);						
			participacionCorredorForm.setPorcentajeParticipacion(porcentajeFormato);
		}
		
								
	}
	
	/**
	 * Method poblarForm
	 * 
	 * @param ParticipacionDTO
	 * @param ParticipacionCorredorForm
	 * @throws ParseException 
	 */	
	private void poblarForm(ParticipacionDTO participacionDTO, 
	ParticipacionCorredorForm participacionCorredorForm){					
		if(participacionDTO.getContratoCuotaParte()!=null)
			participacionCorredorForm.setIdTmContratoCuotaParte(participacionDTO.getContratoCuotaParte().getIdTmContratoCuotaParte().toString());		
		
		if(participacionDTO.getIdTdParticipacion()!=null)
			participacionCorredorForm.setIdTdParticipacion(participacionDTO.getIdTdParticipacion().toString());		
		
		if(participacionDTO.getReaseguradorCorredor()!=null)		
			participacionCorredorForm.setNombreCorredor(participacionDTO.getReaseguradorCorredor().getNombre());
		
		if(!UtileriasWeb.esObjetoNulo(participacionDTO.getPorcentajeParticipacion()))
			participacionCorredorForm.setPorcentajeParticipacionRestante(participacionDTO.getPorcentajeParticipacion().toString());
		else
			participacionCorredorForm.setPorcentajeParticipacionRestante("-1");
		
		
	}
	
	private String getJson(List<ParticipacionCorredorDTO> participaciones) 
	throws ExcepcionDeAccesoADatos, SystemException{
		
//		String json = "{rows:[";
//		if(participaciones != null && participaciones.size() > 0) {
//			for(ParticipacionCorredorDTO participacionCorredor : participaciones) {
//				
//				json += "{id:" + participacionCorredor.getIdTdParticipacionCorredor() + ",data:[";				
//				json += "\"" + participacionCorredor.getReaseguradorCorredor().getNombre() + "\",";				
//				json += "\"" + participacionCorredor.getReaseguradorCorredor().getNombrecorto() + "\",";
//				json += "\"" + participacionCorredor.getReaseguradorCorredor().getCnfs() + "\",";
//				json += "\"" + participacionCorredor.getPorcentajeParticipacion() + "\",";
//				json += "\"/MidasWeb/img/Edit14.gif^Modificar^javascript: void(0); sendRequestParticipanteCorredor(1,"+participacionCorredor.getIdTdParticipacionCorredor()+");^_self"+ "\",";
//				json += "\"/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestParticipanteCorredor(2,"+participacionCorredor.getIdTdParticipacionCorredor()+");^_self"+ "\",";
//				
//				json += "]},";								
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(participaciones != null && participaciones.size() > 0) {
			for(ParticipacionCorredorDTO participacionCorredor : participaciones) {
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(participacionCorredor.getIdTdParticipacionCorredor().toString());
				row.setDatos(
						participacionCorredor.getReaseguradorCorredor().getNombre(),
						participacionCorredor.getReaseguradorCorredor().getNombrecorto(),
						participacionCorredor.getReaseguradorCorredor().getCnfs(),
						participacionCorredor.getPorcentajeParticipacion().toString(),
						"/MidasWeb/img/Edit14.gif^Modificar^javascript: void(0); sendRequestParticipanteCorredor(1,"+participacionCorredor.getIdTdParticipacionCorredor()+");^_self",
						"/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestParticipanteCorredor(2,"+participacionCorredor.getIdTdParticipacionCorredor()+");^_self"
				
				);
				json.addRow(row);
											
			}
		}
		return json.toString();
	}
	
	/**
	 * Method calcularPorcentajeRestante
	 * 
	 * @param ActionForm
	 * @param String reglaNavegacion
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */	
	private String calcularPorcentajeRestante(ActionForm form) throws ExcepcionDeAccesoADatos, SystemException{
		ParticipacionCorredorForm participacionCorredorForm = (ParticipacionCorredorForm) form;
		
		ParticipacionDTO participacionDTO = new ParticipacionDTO();
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		
		Double porcentajeParticipacionRestante = new Double(0);
		Double participacionReaseguradores;
		participacionDTO = participacionDN.getPorId(new BigDecimal(participacionCorredorForm.getIdTdParticipacion()));
		porcentajeParticipacionRestante = participacionDTO.getPorcentajeParticipacion();
		participacionReaseguradores = new Double(0);
		
		ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
		ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
		participacionCorredorDTO.setParticipacion(participacionDTO);
		participacionCorredorDTO.setReaseguradorCorredor(new ReaseguradorCorredorDTO());
		List<ParticipacionCorredorDTO> participacionCorredorDTOList = participacionCorredorDN.listarFiltrados(participacionCorredorDTO);
		
		for (ParticipacionCorredorDTO participacionCorredorDTO2 : participacionCorredorDTOList)
			participacionReaseguradores = participacionReaseguradores + participacionCorredorDTO2.getPorcentajeParticipacion();
		
		porcentajeParticipacionRestante = new Double(porcentajeParticipacionRestante.doubleValue() - participacionReaseguradores.doubleValue());
		
		if (porcentajeParticipacionRestante < 0)
			porcentajeParticipacionRestante = new Double(0);
		
		return porcentajeParticipacionRestante.toString();
	}
	
}

