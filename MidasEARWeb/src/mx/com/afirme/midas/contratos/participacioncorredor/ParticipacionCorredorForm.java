package mx.com.afirme.midas.contratos.participacioncorredor;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ParticipacionCorredorForm extends MidasBaseForm {

	private static final long serialVersionUID = -2902937149615793221L;
	
	
	private String idTdParticipacionCorredor;
	private String idTdParticipacion;
	private String idTcReaseguradorCorredor;
	private String porcentajeParticipacion;	
	private String nombreCorredor;
	private String porcentajeParticipacionRestante;
	private String reglaNavegacion;
	
	/* Propiedades del formulario registrarContratoCuotaParte */
	private String idTmContratoCuotaParte;
	private String folioContratoCuotaParte;
	private String porcentajeDeRetencion;
	private String porcentajeDeCesion;
	private String idTcMoneda;
	private String formaPago;
	private String fechaInicial;
	private String fechaFinal;
	private String estatus;
	/* Fin Propiedades del formulario registrarContratoCuotaParte */
	
	/* Propiedades del formulario registrarContratoPrimerExcedente */
	private String idTmContratoPrimerExcedente;
	private String numeroPlenos;
    private String montoPleno;
    private String folioContratoPrimerExcedente;
	/* Propiedades del formulario registrarContratoPrimerExcedente */
	
	
	/**
	 * @return the idTdParticipacionCorredor
	 */
	public String getIdTdParticipacionCorredor() {
		return idTdParticipacionCorredor;
	}
	/**
	 * @param idTdParticipacionCorredor the idTdParticipacionCorredor to set
	 */
	public void setIdTdParticipacionCorredor(String idTdParticipacionCorredor) {
		this.idTdParticipacionCorredor = idTdParticipacionCorredor;
	}
	/**
	 * @return the idTdParticipacion
	 */
	public String getIdTdParticipacion() {
		return idTdParticipacion;
	}
	/**
	 * @param idTdParticipacion the idTdParticipacion to set
	 */
	public void setIdTdParticipacion(String idTdParticipacion) {
		this.idTdParticipacion = idTdParticipacion;
	}
	/**
	 * @return the idTcReaseguradorCorredor
	 */
	public String getIdTcReaseguradorCorredor() {
		return idTcReaseguradorCorredor;
	}
	/**
	 * @param idTcReaseguradorCorredor the idTcReaseguradorCorredor to set
	 */
	public void setIdTcReaseguradorCorredor(String idTcReaseguradorCorredor) {
		this.idTcReaseguradorCorredor = idTcReaseguradorCorredor;
	}
	/**
	 * @return the porcentajeParticipacion
	 */
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	/**
	 * @param porcentajeParticipacion the porcentajeParticipacion to set
	 */
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	/**
	 * @return the idTmContratoCuotaParte
	 */
	public String getIdTmContratoCuotaParte() {
		return idTmContratoCuotaParte;
	}
	/**
	 * @param idTmContratoCuotaParte the idTmContratoCuotaParte to set
	 */
	public void setIdTmContratoCuotaParte(String idTmContratoCuotaParte) {
		this.idTmContratoCuotaParte = idTmContratoCuotaParte;
	}
	/**
	 * @return the nombreCorredor
	 */
	public String getNombreCorredor() {
		return nombreCorredor;
	}
	/**
	 * @param nombreCorredor the nombreCorredor to set
	 */
	public void setNombreCorredor(String nombreCorredor) {
		this.nombreCorredor = nombreCorredor;
	}
	public String getFolioContratoCuotaParte() {
		return folioContratoCuotaParte;
	}
	public void setFolioContratoCuotaParte(String folioContratoCuotaParte) {
		this.folioContratoCuotaParte = folioContratoCuotaParte;
	}
	public String getPorcentajeDeRetencion() {
		return porcentajeDeRetencion;
	}
	public void setPorcentajeDeRetencion(String porcentajeDeRetencion) {
		this.porcentajeDeRetencion = porcentajeDeRetencion;
	}
	public String getPorcentajeDeCesion() {
		return porcentajeDeCesion;
	}
	public void setPorcentajeDeCesion(String porcentajeDeCesion) {
		this.porcentajeDeCesion = porcentajeDeCesion;
	}
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getIdTmContratoPrimerExcedente() {
		return idTmContratoPrimerExcedente;
	}
	public void setIdTmContratoPrimerExcedente(String idTmContratoPrimerExcedente) {
		this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
	}
	public String getNumeroPlenos() {
		return numeroPlenos;
	}
	public void setNumeroPlenos(String numeroPlenos) {
		this.numeroPlenos = numeroPlenos;
	}
	public String getMontoPleno() {
		return montoPleno;
	}
	public void setMontoPleno(String montoPleno) {
		this.montoPleno = montoPleno;
	}
	public String getFolioContratoPrimerExcedente() {
		return folioContratoPrimerExcedente;
	}
	public void setFolioContratoPrimerExcedente(String folioContratoPrimerExcedente) {
		this.folioContratoPrimerExcedente = folioContratoPrimerExcedente;
	}
	public String getPorcentajeParticipacionRestante() {
		return porcentajeParticipacionRestante;
	}
	public void setPorcentajeParticipacionRestante(
			String porcentajeParticipacionRestante) {
		this.porcentajeParticipacionRestante = porcentajeParticipacionRestante;
	}
	public String getReglaNavegacion() {
		return reglaNavegacion;
	}
	public void setReglaNavegacion(String reglaNavegacion) {
		this.reglaNavegacion = reglaNavegacion;
	}
	
}
