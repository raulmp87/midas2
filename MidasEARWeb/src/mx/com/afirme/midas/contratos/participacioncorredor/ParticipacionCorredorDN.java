package mx.com.afirme.midas.contratos.participacioncorredor;

import java.util.List;
import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ParticipacionCorredorDN {
	private static final ParticipacionCorredorDN INSTANCIA = new ParticipacionCorredorDN();

	public static ParticipacionCorredorDN getInstancia() {
		return ParticipacionCorredorDN.INSTANCIA;
	}

	public List<ParticipacionCorredorDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
		return participacionCorredorSN.listarTodos();
	}

	public List<ParticipacionCorredorDTO> listarFiltrados(ParticipacionCorredorDTO participacionCorredorDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
		return participacionCorredorSN.listarFiltrados(participacionCorredorDTO);
	}

	public void agregar(ParticipacionCorredorDTO participacionCorredorDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
		participacionCorredorSN.agregar(participacionCorredorDTO);
	}

	public void modificar(ParticipacionCorredorDTO participacionCorredorDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
		participacionCorredorSN.modificar(participacionCorredorDTO);
	}

	public ParticipacionCorredorDTO getPorId(BigDecimal id)
			throws SystemException, ExcepcionDeAccesoADatos {
		ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
		return participacionCorredorSN.getPorId(id);
	}

	public void borrar(ParticipacionCorredorDTO participacionCorredorDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
		participacionCorredorSN.borrar(participacionCorredorDTO);
	}
	
	public List<ParticipacionCorredorDTO> getPorPropiedad(String propiedad, Object valor)
		throws SystemException, ExcepcionDeAccesoADatos {
			ParticipacionCorredorSN participacionCorredorSN = new ParticipacionCorredorSN();
			return participacionCorredorSN.getPorPropiedad(propiedad, valor);
	}
}
