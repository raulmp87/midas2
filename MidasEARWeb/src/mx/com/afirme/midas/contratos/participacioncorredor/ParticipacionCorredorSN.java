/**
 * 
 */
package mx.com.afirme.midas.contratos.participacioncorredor;

import java.util.List;
import java.util.logging.Level;
import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ParticipacionCorredorSN {
	private ParticipacionCorredorFacadeRemote beanRemoto;

	public ParticipacionCorredorSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ParticipacionCorredorSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ParticipacionCorredorFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ParticipacionCorredorDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ParticipacionCorredorDTO> participacionesCorredor = beanRemoto.findAll();
		return participacionesCorredor;

	}
	public List<ParticipacionCorredorDTO> listarFiltrados(ParticipacionCorredorDTO participacionCorredorDTO) throws ExcepcionDeAccesoADatos {
		List<ParticipacionCorredorDTO> participacionesCorredor = beanRemoto.listarFiltrado(participacionCorredorDTO);
		return participacionesCorredor;
	}

	public void agregar(ParticipacionCorredorDTO participacionCorredorDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(participacionCorredorDTO);
	}

	public void modificar(ParticipacionCorredorDTO participacionCorredorDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(participacionCorredorDTO);
	}

	public ParticipacionCorredorDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ParticipacionCorredorDTO participacionCorredorDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(participacionCorredorDTO);
	}
	
	public List<ParticipacionCorredorDTO> getPorPropiedad(String propiedad, Object valor)
		throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByProperty(propiedad, valor);
	}
}