package mx.com.afirme.midas.contratos.contratoprimerexcedente;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContratoPrimerExcedenteDN {
	private static final ContratoPrimerExcedenteDN INSTANCIA = new ContratoPrimerExcedenteDN();
	private static String nombreUsuario;
	
	public static ContratoPrimerExcedenteDN getInstancia(String nombreUsuario) {
		ContratoPrimerExcedenteDN.nombreUsuario = nombreUsuario;
		return ContratoPrimerExcedenteDN.INSTANCIA;
	}
		
	public ContratoPrimerExcedenteDTO crearContrato(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO)throws SystemException,
		ExcepcionDeAccesoADatos {
			ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario);
			return contratoPrimerExcedenteSN.crearContrato(contratoPrimerExcedenteDTO);
	}
	
	public ContratoPrimerExcedenteDTO guardarContrato(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) throws SystemException,
		ExcepcionDeAccesoADatos {
		ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario);
			return contratoPrimerExcedenteSN.guardarContrato(contratoPrimerExcedenteDTO);
	}	
		
	public List<ContratoPrimerExcedenteDTO> listarTodos() 
		throws SystemException,ExcepcionDeAccesoADatos{	
			ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario);		
			return contratoPrimerExcedenteSN.listarTodos();
	}
	
	public ContratoPrimerExcedenteDTO getPorId(BigDecimal id)
	throws SystemException, ExcepcionDeAccesoADatos {
		ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario);
		return contratoPrimerExcedenteSN.getPorId(id);
	}
	
	public List<ContratoPrimerExcedenteDTO> listarFiltrado(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) 
		throws SystemException,ExcepcionDeAccesoADatos{
		ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario);		
		return contratoPrimerExcedenteSN.listarFiltrado(contratoPrimerExcedenteDTO);
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String idLineas, Date fInicial, Date fFinal) 
	throws SystemException, ExcepcionDeAccesoADatos{
		ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario);
		return contratoPrimerExcedenteSN.actualizaFechasLineasPorIdLineas(idLineas,fInicial,fFinal);
	}
	
	public ContratoPrimerExcedenteDTO obtenerContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO) throws SystemException{
		return new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario).obtenerContratoPrimerExcedenteEjercicioAnterior(estadoCuentaDTO);
	}
	
	public List<ContratoPrimerExcedenteDTO> obtenerListaContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO) throws SystemException{
		return new ContratoPrimerExcedenteSN(ContratoPrimerExcedenteDN.nombreUsuario).obtenerListaContratoPrimerExcedenteEjercicioAnterior(estadoCuentaDTO);
	}
}