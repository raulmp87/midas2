package mx.com.afirme.midas.contratos.contratoprimerexcedente;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ContratoPrimerExcedenteForm extends MidasBaseForm {

	private static final long serialVersionUID = -2902937149615793221L;

	private String idTmContratoPrimerExcedente;
    private String folioContratoPrimerExcedente;
    private String fechaInicial;
    private String fechaFinal;
    private String horaInicial;
    private String horaFinal;
    private String minutoInicial;
    private String minutoFinal;    
    private String idTcRamo;
    private String idTcSubRamo;
    private String seleccionado;
    private String idTmContratoCuotaParte;
    private String participacionesCP;
    private String participacionesPE;
	private String limiteMaximo;
    private String numeroPlenos;
    private String montoPleno;
    private String idTcMoneda;
    private String usuarioAutorizo;
    private String fechaAutorizacion;
    private String modoDistribucion;
    private String maximo;
    private String porcentajeDeCesion;
    private String folioContratoCuotaParte;
    private String porcentajeRetencion;
    private String formaPago;
    private String reglaNavegacion;
    
    private String estatus;
    
	public String getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(String porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public String getFolioContratoCuotaParte() {
		return folioContratoCuotaParte;
	}
	public void setFolioContratoCuotaParte(String folioContratoCuotaParte) {
		this.folioContratoCuotaParte = folioContratoCuotaParte;
	}
	public String getPorcentajeDeCesion() {
		return porcentajeDeCesion;
	}
	public void setPorcentajeDeCesion(String porcentajeDeCesion) {
		this.porcentajeDeCesion = porcentajeDeCesion;
	}
	public String getMaximo() {
		return maximo;
	}
	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}
	public String getModoDistribucion() {
		return modoDistribucion;
	}
	public void setModoDistribucion(String modoDistribucion) {
		this.modoDistribucion = modoDistribucion;
	}
	public String getParticipacionesCP() {
		return participacionesCP;
	}
	public void setParticipacionesCP(String participacionesCP) {
		this.participacionesCP = participacionesCP;
	}
	public String getMinutoInicial() {
		return minutoInicial;
	}
	public void setMinutoInicial(String minutoInicial) {
		this.minutoInicial = minutoInicial;
	}
	public String getMinutoFinal() {
		return minutoFinal;
	}
	public void setMinutoFinal(String minutoFinal) {
		this.minutoFinal = minutoFinal;
	}
    
    public String getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(String seleccionado) {
		this.seleccionado = seleccionado;
	}
	public String getHoraInicial() {
		return horaInicial;
	}
	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}
	public String getHoraFinal() {
		return horaFinal;
	}
	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}

    /**
	 * @return the idTmContratoPrimerExcedente
	 */
	public String getIdTmContratoPrimerExcedente() {
		return idTmContratoPrimerExcedente;
	}
	/**
	 * @param idTmContratoPrimerExcedente the idTmContratoPrimerExcedente to set
	 */
	public void setIdTmContratoPrimerExcedente(String idTmContratoPrimerExcedente) {
		this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
	}
	/**
	 * @return the folioContrato
	 */
	public String getFolioContratoPrimerExcedente() {
		return folioContratoPrimerExcedente;
	}
	/**
	 * @param folioContrato the folioContrato to set
	 */
	public void setFolioContratoPrimerExcedente(String folioContratoPrimerExcedente) {
		this.folioContratoPrimerExcedente = folioContratoPrimerExcedente;
	}
	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the limiteMaximo
	 */
	public String getLimiteMaximo() {
		return limiteMaximo;
	}
	/**
	 * @param limiteMaximo the limiteMaximo to set
	 */
	public void setLimiteMaximo(String limiteMaximo) {
		this.limiteMaximo = limiteMaximo;
	}
	/**
	 * @return the numeroPlenos
	 */
	public String getNumeroPlenos() {
		return numeroPlenos;
	}
	/**
	 * @param numeroPlenos the numeroPlenos to set
	 */
	public void setNumeroPlenos(String numeroPlenos) {
		this.numeroPlenos = numeroPlenos;
	}
	/**
	 * @return the montoPleno
	 */
	public String getMontoPleno() {
		return montoPleno;
	}
	/**
	 * @param montoPleno the montoPleno to set
	 */
	public void setMontoPleno(String montoPleno) {
		this.montoPleno = montoPleno;
	}
	/**
	 * @return the idTcMoneda
	 */
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	/**
	 * @param idTcMoneda the idTcMoneda to set
	 */
	public void setIdMoneda(String idMoneda) {
		this.idTcMoneda = idMoneda;
	}
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	/**
	 * @return the usuarioAutorizo
	 */
	public String getUsuarioAutorizo() {
		return usuarioAutorizo;
	}
	/**
	 * @param usuarioAutorizo the usuarioAutorizo to set
	 */
	public void setUsuarioAutorizo(String usuarioAutorizo) {
		this.usuarioAutorizo = usuarioAutorizo;
	}
	/**
	 * @return the fechaAutorizacion
	 */
	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	/**
	 * @param fechaAutorizacion the fechaAutorizacion to set
	 */
	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	
	public String getIdTmContratoCuotaParte() {
		return idTmContratoCuotaParte;
	}
	
	public void setIdTmContratoCuotaParte(String idTmContratoCuotaParte) {
		this.idTmContratoCuotaParte = idTmContratoCuotaParte;
	}
	
	public String getIdTcRamo() {
		return idTcRamo;
	}
	
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	
	public String getParticipacionesPE() {
		return participacionesPE;
	}
	public void setParticipacionesPE(String participacionesPE) {
		this.participacionesPE = participacionesPE;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getReglaNavegacion() {
		return reglaNavegacion;
	}
	public void setReglaNavegacion(String reglaNavegacion) {
		this.reglaNavegacion = reglaNavegacion;
	}
}
