package mx.com.afirme.midas.contratos.contratoprimerexcedente;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.contratos.linea.LineaDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContratoPrimerExcedenteAction extends MidasMappingDispatchAction {

	
	/**
	 * Method autorizarContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void autorizarContrato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		LineaDN lineaDN = LineaDN.getInstancia();
		String idTmContratoPrimerExcedente = new String();
		
		ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();
		idTmContratoPrimerExcedente = request.getParameter("idTmContratoPrimerExcedente");

		try {
			contratoPrimerExcedenteDTO = contratoPrimerExcedenteDN.getPorId(new BigDecimal(idTmContratoPrimerExcedente));		
			Set<String> errores = lineaDN.validarAutorizarPE(
					contratoPrimerExcedenteDTO);
			if (errores.size() == 0){ //Sin errores
				contratoPrimerExcedenteDTO.setEstatus(new BigDecimal(1));
				contratoPrimerExcedenteDTO = contratoPrimerExcedenteDN.guardarContrato(contratoPrimerExcedenteDTO);
			}
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<item>");
			if (errores.size() == 0) {
				buffer.append("<id>1</id><description><![CDATA[Se autoriz\u00f3 el contrato.]]></description>");
			}
			else {
				Map<String,String> erroresMensajes = UtileriasWeb.generarMensajes(
						errores, getResources(request)); //Generar los mensajes
				String mensajes = generarStringMensaje(erroresMensajes);
				buffer.append("<id>0</id><description><![CDATA[El contrato no fue autorizado por las siguientes causas:\n" 
						+ mensajes + "]]></description>");
			}
			
			buffer.append("</item>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}

	/**
	 * Method mostrarInicio
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarInicio(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
	
			String reglaNavegacion = Sistema.EXITOSO;
			final String idTcMonedaDolares = "840"; 
			
			ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
			contratoPrimerExcedenteForm.setFolioContratoPrimerExcedente("Sin asignar");
			contratoPrimerExcedenteForm.setIdMoneda(idTcMonedaDolares); //Siempre en dolares
			return mapping.findForward(reglaNavegacion);

	}
	
	/**
	 * Method crearContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward crearContrato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
			throws SystemException{
			
		String reglaNavegacion = Sistema.EXITOSO;
		final String idTcMonedaDolares = "840"; 
		ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
		ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();		
		ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			contratoPrimerExcedenteForm.setIdTcMoneda(idTcMonedaDolares); //Siempre en dolares.
			this.poblarDTO(contratoPrimerExcedenteForm, contratoPrimerExcedenteDTO);
			
			contratoPrimerExcedenteDTO.setFolioContrato("1E0000"); //El dato es obligatorio, se inicializa de esta manera y una vez guardado el contrato se genera su consecutivo
			
			contratoPrimerExcedenteDTO.setEstatus(new BigDecimal(0));
			contratoPrimerExcedenteDTO = contratoPrimerExcedenteDN.crearContrato(contratoPrimerExcedenteDTO);
			String folio = this.darFormatoFolio(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente());
			contratoPrimerExcedenteDTO.setFolioContrato(folio);
			contratoPrimerExcedenteDTO.setEstatus(new BigDecimal(0));
			contratoPrimerExcedenteDN.guardarContrato(contratoPrimerExcedenteDTO);
			
			contratoPrimerExcedenteForm.setIdTmContratoPrimerExcedente(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().toString());
			contratoPrimerExcedenteForm.setFolioContratoPrimerExcedente(folio);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ParseException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);					
	}	
	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			String idContrato = request.getParameter("idContratoPE");
			String reglaNavegacionFromRequest = request.getParameter("reglaNavegacion");
			String formularioOrigen = request.getParameter("formularioOrigen");
			String ventanaAccionada =  request.getParameter("ventanaAccionada");
			request.setAttribute("formularioOrigen", formularioOrigen);
			request.setAttribute("ventanaAccionada", ventanaAccionada);
			
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
			if(idContrato != null){
				try{
					ContratoPrimerExcedenteDTO contratoDTO;
					contratoDTO = contratoPrimerExcedenteDN.getPorId(new BigDecimal(idContrato));
					this.poblarForm(contratoDTO, contratoPrimerExcedenteForm);
					contratoPrimerExcedenteForm.setReglaNavegacion(reglaNavegacionFromRequest);
					this.listarParticipaciones(request, contratoDTO);					
				}catch(ExcepcionDeAccesoADatos e){
					reglaNavegacion = Sistema.NO_EXITOSO;
				}catch(SystemException e){
					reglaNavegacion = Sistema.NO_DISPONIBLE;
				}catch(ParseException e){
					reglaNavegacion = Sistema.NO_EXITOSO;
				}
			}
			return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method guardarContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward guardarContrato(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {

			String reglaNavegacion;	
			ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
			if (UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteForm.getReglaNavegacion()) || contratoPrimerExcedenteForm.getReglaNavegacion().equals("0"))
				reglaNavegacion = Sistema.EXITOSO;
			else
				reglaNavegacion = "regresarAgregarLinea";
			
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));	
			try {
				this.poblarDTO(contratoPrimerExcedenteForm, contratoPrimerExcedenteDTO);
				
				contratoPrimerExcedenteDTO = contratoPrimerExcedenteDN.guardarContrato(contratoPrimerExcedenteDTO);
				
				request.setAttribute("contratoPE", contratoPrimerExcedenteDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch(ParseException pe){
				contratoPrimerExcedenteForm.setMensaje("Formato de fecha incorrecto.");
				reglaNavegacion = Sistema.NO_EXITOSO;			
			}
		
			return mapping.findForward(reglaNavegacion);
	}
		
	/**
	 * listar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {

			String reglaNavegacion = Sistema.EXITOSO;
			try {
				ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				List<ContratoPrimerExcedenteDTO> contratoPrimerExcedentes = contratoPrimerExcedenteDN.listarTodos();
				request.setAttribute("contratoPrimerExcedentes", contratoPrimerExcedentes);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
	
			return mapping.findForward(reglaNavegacion);
	}		
	
	/**
	 * listarFiltrado
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
			ContratoPrimerExcedenteDTO  contratoPrimerExcedenteDTO  = new ContratoPrimerExcedenteDTO();
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN 	= ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			this.poblarDTO(contratoPrimerExcedenteForm, contratoPrimerExcedenteDTO);
			request.setAttribute("contratoPrimerExcedentes", contratoPrimerExcedenteDN.listarFiltrado(contratoPrimerExcedenteDTO));
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			reglaNavegacion = Sistema.NO_EXITOSO;			
		}
		
		return mapping.findForward(reglaNavegacion);	
	}
	
	/**
	 * listarFiltradoEnGrid
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void listarFiltradoEnGrid(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
										
		String fechaInicial = request.getParameter("fi");
		String fechaFinal = request.getParameter("ff");
		try{												
			ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
			contratoPrimerExcedenteForm.setFechaInicial(fechaInicial);
			contratoPrimerExcedenteForm.setFechaFinal(fechaFinal);
			ContratoPrimerExcedenteDTO  contratoPrimerExcedenteDTO  = new ContratoPrimerExcedenteDTO();
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			this.poblarDTO(contratoPrimerExcedenteForm, contratoPrimerExcedenteDTO);
			List<ContratoPrimerExcedenteDTO> contratos = contratoPrimerExcedenteDN.listarFiltrado(contratoPrimerExcedenteDTO);
			String json = this.getJson(contratos);		
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){			
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);	
		}					
	}
	
	/**
	 * seleccionar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward seleccionar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;		
		ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;		
		
		String idSeleccionado = contratoPrimerExcedenteForm.getSeleccionado();
		if(idSeleccionado!=null && !idSeleccionado.equals("")){
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		
			try{
				ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO =  contratoPrimerExcedenteDN.getPorId(new BigDecimal(idSeleccionado));
				request.setAttribute("contratoPE", contratoPrimerExcedenteDTO);
			}catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.EXCEPCION_ACCESO_DATOS;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}catch (SystemException e){
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
	}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * mostrarListar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarListar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;		
		ContratoPrimerExcedenteForm contratoPrimerExcedenteForm = (ContratoPrimerExcedenteForm) form;
		
		if(request.getParameter("fi")!=null && request.getParameter("ff")!=null){
			String fechaInicial = request.getParameter("fi");
			String fechaFinal = request.getParameter("ff");
			contratoPrimerExcedenteForm.setFechaInicial(fechaInicial);
			contratoPrimerExcedenteForm.setFechaFinal(fechaFinal);
		}
		
		if(request.getParameter("idTcRamo")!=null)
			contratoPrimerExcedenteForm.setIdTcRamo(request.getParameter("idTcRamo"));
		if (request.getParameter("idTcSubRamo")!=null)
			contratoPrimerExcedenteForm.setIdTcSubRamo(request.getParameter("idTcSubRamo"));
			
		contratoPrimerExcedenteForm.setParticipacionesCP(request.getParameter("participacionesCP"));
		
		if(request.getParameter("porcentajeDeCesion")!= null)
			contratoPrimerExcedenteForm.setPorcentajeDeCesion(request.getParameter("porcentajeDeCesion"));
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param ContratoPrimerExcedenteForm
	 * @param ContratoPrimerExcedenteDTO
	 * @throws ParseException 
	 * @throws SystemException 
	 */
	private void poblarDTO(ContratoPrimerExcedenteForm contratoPrimerExcedenteForm, 
		ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) throws ParseException, SystemException{					
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getFechaFinal())) {			
			String fechaHora = contratoPrimerExcedenteForm.getFechaFinal();
			Date fechaFinal = UtileriasWeb.getFechaHoraFromString(fechaHora);
			contratoPrimerExcedenteDTO.setFechaFinal(fechaFinal);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getFechaInicial())) {			
			String fechaHora = contratoPrimerExcedenteForm.getFechaInicial();
			Date fechaInicial = UtileriasWeb.getFechaHoraFromString(fechaHora);
			contratoPrimerExcedenteDTO.setFechaInicial(fechaInicial);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getFolioContratoPrimerExcedente())) {
			String folioContrato = contratoPrimerExcedenteForm.getFolioContratoPrimerExcedente();
			contratoPrimerExcedenteDTO.setFolioContrato(folioContrato);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getIdTcMoneda())) {
			BigDecimal idMoneda = new BigDecimal(contratoPrimerExcedenteForm.getIdTcMoneda());
			contratoPrimerExcedenteDTO.setIdMoneda(idMoneda);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getIdTmContratoPrimerExcedente())) {
			BigDecimal idTmContratoPrimerExcedente = new BigDecimal(contratoPrimerExcedenteForm.getIdTmContratoPrimerExcedente());
			contratoPrimerExcedenteDTO.setIdTmContratoPrimerExcedente(idTmContratoPrimerExcedente);
		}
		
			double numeroPlenosLM;
			double montoPlenoLM;
			if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getNumeroPlenos()))
				numeroPlenosLM = new Double(contratoPrimerExcedenteForm.getNumeroPlenos());
			else 
				numeroPlenosLM = 0;
			
			if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getMontoPleno()))
				montoPlenoLM = new Double(contratoPrimerExcedenteForm.getMontoPleno());
			else
				montoPlenoLM = 0;
			BigDecimal limiteMaximo = new BigDecimal(numeroPlenosLM * montoPlenoLM);
			contratoPrimerExcedenteDTO.setLimiteMaximo(limiteMaximo);
		
			if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getMontoPleno())) {
			Double montoPleno = Double.valueOf(contratoPrimerExcedenteForm.getMontoPleno());
			contratoPrimerExcedenteDTO.setMontoPleno(montoPleno);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getNumeroPlenos())) {
			BigDecimal numeroPlenos = new BigDecimal(contratoPrimerExcedenteForm.getNumeroPlenos());
			contratoPrimerExcedenteDTO.setNumeroPlenos(numeroPlenos);
		}		
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getFechaAutorizacion())) {	
			String fecha = contratoPrimerExcedenteForm.getFechaAutorizacion();
			Date fechaAutorizacion = UtileriasWeb.getFechaFromString(fecha);
			contratoPrimerExcedenteDTO.setFechaAutorizacion(fechaAutorizacion);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getUsuarioAutorizo())) {	
			BigDecimal usuario = new BigDecimal(contratoPrimerExcedenteForm.getUsuarioAutorizo());			
			contratoPrimerExcedenteDTO.setUsuarioAutorizo(usuario);
		}
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getFormaPago()))
			contratoPrimerExcedenteDTO.setFormaPago(UtileriasWeb.regresaBigDecimal(contratoPrimerExcedenteForm.getFormaPago()));
		
		if (!StringUtil.isEmpty(contratoPrimerExcedenteForm.getEstatus()))
			contratoPrimerExcedenteDTO.setEstatus(UtileriasWeb.regresaBigDecimal(contratoPrimerExcedenteForm.getEstatus()));
		else
			contratoPrimerExcedenteDTO.setEstatus(UtileriasWeb.regresaBigDecimal("0"));
		
	}
	
	
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContratoPrimerExcedenteDTO
	 * @param ContratoPrimerExcedenteForm
	 * @throws ParseException 
	 */	
	
	private void poblarForm(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO, 
	ContratoPrimerExcedenteForm contratoPrimerExcedenteForm) throws ParseException{
				
		if(contratoPrimerExcedenteDTO.getFechaFinal()!=null){
			String fecha = UtileriasWeb.getFechaHoraString(contratoPrimerExcedenteDTO.getFechaFinal());						
			contratoPrimerExcedenteForm.setFechaFinal(fecha);					
		}
		
		if(contratoPrimerExcedenteDTO.getFechaInicial()!=null){
			String fecha = UtileriasWeb.getFechaHoraString(contratoPrimerExcedenteDTO.getFechaInicial());		
			contratoPrimerExcedenteForm.setFechaInicial(fecha);			
		}
					
		if(contratoPrimerExcedenteDTO.getFolioContrato()!=null)
			contratoPrimerExcedenteForm.setFolioContratoPrimerExcedente(contratoPrimerExcedenteDTO.getFolioContrato().toString());				
		
		if(contratoPrimerExcedenteDTO.getFechaAutorizacion()!=null){
			String fecha = UtileriasWeb.getFechaString(contratoPrimerExcedenteDTO.getFechaAutorizacion());						
			contratoPrimerExcedenteForm.setFechaAutorizacion(fecha);						
		}			
		
		if(contratoPrimerExcedenteDTO.getIdMoneda()!=null)
			contratoPrimerExcedenteForm.setIdMoneda(contratoPrimerExcedenteDTO.getIdMoneda().toString());
		
		if(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()!=null)
			contratoPrimerExcedenteForm.setIdTmContratoPrimerExcedente(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().toString());								
		
		if(contratoPrimerExcedenteDTO.getLimiteMaximo()!=null){
			Double limiteMaximo;
			if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getMontoPleno()) && !UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getNumeroPlenos())){
				limiteMaximo = new Double(contratoPrimerExcedenteDTO.getMontoPleno().doubleValue() * contratoPrimerExcedenteDTO.getNumeroPlenos().doubleValue());
				String limiteMaximoFormato = MessageFormat.format("{0,number,#.##}", limiteMaximo);
				contratoPrimerExcedenteForm.setLimiteMaximo(limiteMaximoFormato);
			}else
				contratoPrimerExcedenteForm.setLimiteMaximo("0.00");
		}
		
		if(contratoPrimerExcedenteDTO.getMontoPleno()!=null){
			Double monto = contratoPrimerExcedenteDTO.getMontoPleno();
			String montoFormato = MessageFormat.format("{0,number,#.##}", monto);
			contratoPrimerExcedenteForm.setMontoPleno(montoFormato);
		}
		
		if(contratoPrimerExcedenteDTO.getNumeroPlenos()!=null){
			BigDecimal numPlenos = contratoPrimerExcedenteDTO.getNumeroPlenos();			
			contratoPrimerExcedenteForm.setNumeroPlenos(numPlenos.intValue()+"");
		}
		
		if(contratoPrimerExcedenteDTO.getUsuarioAutorizo()!=null){
			BigDecimal usuario = contratoPrimerExcedenteDTO.getUsuarioAutorizo();	
			contratoPrimerExcedenteForm.setUsuarioAutorizo(usuario.toString());
		}
		
		if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getFormaPago())){
			BigDecimal formaPago = contratoPrimerExcedenteDTO.getFormaPago();
			contratoPrimerExcedenteForm.setFormaPago(formaPago.toBigInteger().toString());
		}
	}
	
	private String darFormatoFolio(BigDecimal id){
		String numeracion = UtileriasWeb.llenarIzquierda(id.toString(), "0", 4);		
		return 	"1E".concat(numeracion);	
	}
	
	private void listarParticipaciones(
			HttpServletRequest request, ContratoPrimerExcedenteDTO contratoDTO)
			throws ExcepcionDeAccesoADatos, SystemException{
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		ParticipacionDTO participacionDTO = new ParticipacionDTO();		
		participacionDTO.setContratoPrimerExcedente(contratoDTO);
		List<ParticipacionDTO> participaciones = participacionDN.listarFiltrados(participacionDTO);
		request.setAttribute("participaciones", participaciones);
	}
	
	private String getJson(List<ContratoPrimerExcedenteDTO> contratos) 
	throws ExcepcionDeAccesoADatos, SystemException{
		MonedaDN monedaDN = MonedaDN.getInstancia();		
//		String json = "{rows:[";
//		if(contratos != null && contratos.size() > 0) {
//			for(ContratoPrimerExcedenteDTO contrato : contratos) {
//				
//				String moneda = "";
//				try{
//					moneda = monedaDN.getPorId(contrato.getIdMoneda().shortValue()).getDescripcion();
//				}catch(NullPointerException e){}
//				
//				json += "{id:" + contrato.getIdTmContratoPrimerExcedente() + ",data:[";
//				json += "0,\"";
//				json += contrato.getFolioContrato() + "\",";				
//				json += contrato.getNumeroPlenos() + ",";
//				json += contrato.getMontoPleno() + ",\"";
//				json += moneda +"\",";
//				json += "0,";
//				if (contrato.getEstatus() == null || contrato.getEstatus().toString().equals("0")){
//					json += "\"No Autorizado\",";
//					json += "\"/MidasWeb/img/Edit14.gif^Modificar^javascript: sendRequestContrato(3," + contrato.getIdTmContratoPrimerExcedente() + ",contratoPrimerExcedenteForm);^_self\""+ ",";
//				}else
//					if (contrato.getEstatus().toString().equals("1")){
//						json += "\"Autorizado\",";
//						json += "\"/MidasWeb/img/Edit14_disabled.gif^No habilitado para modificar, desautorizar primero^javascript: void(0);^_self\""+ ",";
//					}
//				json += "\"/MidasWeb/img/details.gif^Detalle^javascript: sendRequestContrato(4," + contrato.getIdTmContratoPrimerExcedente() + ",contratoPrimerExcedenteForm);^_self\""+"]},";								
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		
		MidasJsonBase json = new MidasJsonBase();
		if(contratos != null && contratos.size() > 0) {
			for(ContratoPrimerExcedenteDTO contrato : contratos) {
				
				String moneda = "";
				String estatusDesc = null;
				String iconoEstatus = null;
				
				if (contrato.getEstatus() == null || contrato.getEstatus().toString().equals("0")){
					estatusDesc = "No Autorizado";
					iconoEstatus = "/MidasWeb/img/Edit14.gif^Modificar^javascript: sendRequestContrato(3," + contrato.getIdTmContratoPrimerExcedente() + ",contratoPrimerExcedenteForm);^_self"; 
				} else if (contrato.getEstatus().toString().equals("1")){
					estatusDesc = "Autorizado";
					iconoEstatus = "/MidasWeb/img/Edit14_disabled.gif^No habilitado para modificar, desautorizar primero^javascript: void(0);^_self"; 
				}
								
				try{
					moneda = monedaDN.getPorId(contrato.getIdMoneda().shortValue()).getDescripcion();
				}catch(NullPointerException e){}
				
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(contrato.getIdTmContratoPrimerExcedente().toString());
				row.setDatos(
						"0",
						contrato.getFolioContrato(),
						contrato.getNumeroPlenos().toString(),
						contrato.getMontoPleno().toString(),
						moneda,
						"0",
						estatusDesc,
						iconoEstatus,
						"/MidasWeb/img/details.gif^Detalle^javascript: sendRequestContrato(4," + contrato.getIdTmContratoPrimerExcedente() + ",contratoPrimerExcedenteForm);^_self"
						
				);
				json.addRow(row);
				
			}
		}
		
		return json.toString();
	}
	
	private String generarStringMensaje(Map<String, String> erroresMensajes) {
		StringBuffer mensajeTotal = new StringBuffer();
		for (String key : erroresMensajes.keySet()) {
			String mensaje = erroresMensajes.get(key);
			mensajeTotal.append(mensaje + "\n");
		}
		return mensajeTotal.toString();
	}
	
}