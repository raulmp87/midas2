package mx.com.afirme.midas.contratos.contratoprimerexcedente;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContratoPrimerExcedenteSN {

	private ContratoPrimerExcedenteFacadeRemote beanRemoto;
	private String nombreUsuario;
		
	public ContratoPrimerExcedenteSN(String nombreUsuario) throws SystemException {
		this.nombreUsuario = nombreUsuario;
		LogDeMidasWeb.log("Entrando en ContratoPrimerExcedenteSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ContratoPrimerExcedenteFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public ContratoPrimerExcedenteDTO crearContrato(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO)
	throws ExcepcionDeAccesoADatos {
		contratoPrimerExcedenteDTO.setNombreUsuarioLog(this.nombreUsuario);
		if(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()==null){
			return beanRemoto.save(contratoPrimerExcedenteDTO);				
		}
		else{
			return beanRemoto.update(contratoPrimerExcedenteDTO);
		}
	}
	
	public ContratoPrimerExcedenteDTO guardarContrato(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO)
		throws ExcepcionDeAccesoADatos {
			contratoPrimerExcedenteDTO.setNombreUsuarioLog(this.nombreUsuario);
			return beanRemoto.update(contratoPrimerExcedenteDTO);
	}	
	
	public List<ContratoPrimerExcedenteDTO> listarTodos() 
		throws ExcepcionDeAccesoADatos {	
			List<ContratoPrimerExcedenteDTO> contratoPrimerExcedentes = beanRemoto.findAll();
			return contratoPrimerExcedentes;
	}
	
	public ContratoPrimerExcedenteDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}
	
	public List<ContratoPrimerExcedenteDTO> listarFiltrado(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) 
	throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(contratoPrimerExcedenteDTO);			
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ContratoPrimerExcedenteDTO> buscarPorFechaInicialFinal(Date fechaInicial,Date fechaFinal)
	throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.buscarPorFechaInicialFinal(fechaInicial,fechaFinal);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String ids,Date fInicial,Date fFinal)
		throws ExcepcionDeAccesoADatos{
		try{			
			return beanRemoto.actualizaFechasLineasPorIdLineas(ids, fInicial, fFinal);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	ContratoPrimerExcedenteDTO obtenerContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		return beanRemoto.obtenerContratoPrimerExcedenteEjercicioAnterior(estadoCuentaDTO);
	}
	
	List<ContratoPrimerExcedenteDTO> obtenerListaContratoPrimerExcedenteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		return beanRemoto.obtenerListaContratoPrimerExcedenteEjercicioAnterior(estadoCuentaDTO);
	}
}
