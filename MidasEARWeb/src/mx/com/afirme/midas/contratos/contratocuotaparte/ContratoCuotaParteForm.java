package mx.com.afirme.midas.contratos.contratocuotaparte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ContratoCuotaParteForm extends MidasBaseForm {

	private static final long serialVersionUID = -2902937149615793221L;

	
	//Registrar contrato cuota parte 
	private String idTmContratoCuotaParte;
    private String fechaInicial;
    private String horaInicial;
    private String minutoInicial;
    private String fechaFinal;
    private String horaFinal;
    private String minutoFinal;
    private String porcentajeDeRetencion;
    private String porcentajeDeCesion;
    private String idTcMoneda;
    private String folioContratoCuotaParte;
    private String folioContratoPrimerExcedente;
	private String estatus;        
    private String formaPago;
    private String fechaAutorizacion;
    private String usuarioAutorizo;
    
    private String seleccionado;
    private String idTcRamo;
    private String idTcSubRamo;
    private String idTmContratoPrimerExcedente;
    private String participacionesPE;
    private String participacionesCP;
	private String modoDistribucion;
    private String maximo;
    private String numeroPlenos;
    private String montoPleno;
    private String reglaNavegacion;
               
	
	public String getMontoPleno() {
		return montoPleno;
	}
	public void setMontoPleno(String montoPleno) {
		this.montoPleno = montoPleno;
	}
	public String getNumeroPlenos() {
		return numeroPlenos;
	}
	public void setNumeroPlenos(String numeroPlenos) {
		this.numeroPlenos = numeroPlenos;
	}
	public String getMaximo() {
		return maximo;
	}
	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}
	public String getModoDistribucion() {
		return modoDistribucion;
	}
	public void setModoDistribucion(String modoDistribucion) {
		this.modoDistribucion = modoDistribucion;
	}
	public String getParticipacionesPE() {
		return participacionesPE;
	}
	public void setParticipacionesPE(String participacionesPE) {
		this.participacionesPE = participacionesPE;
	}
	/**
	 * @return the idTmContratoCuotaParte
	 */
	public String getIdTmContratoCuotaParte() {
		return idTmContratoCuotaParte;
	}
	/**
	 * @param idTmContratoCuotaParte the idTmContratoCuotaParte to set
	 */
	public void setIdTmContratoCuotaParte(String idTmContratoCuotaParte) {
		this.idTmContratoCuotaParte = idTmContratoCuotaParte;
	}
	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the horaInicial
	 */
	public String getHoraInicial() {
		return horaInicial;
	}
	/**
	 * @param horaInicial the horaInicial to set
	 */
	public void setHoraInicial(String horaInicial) {
		this.horaInicial = horaInicial;
	}
	/**
	 * @return the minutoInicial
	 */
	public String getMinutoInicial() {
		return minutoInicial;
	}
	/**
	 * @param minutoInicial the minutoInicial to set
	 */
	public void setMinutoInicial(String minutoInicial) {
		this.minutoInicial = minutoInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the horaFinal
	 */
	public String getHoraFinal() {
		return horaFinal;
	}
	/**
	 * @param horaFinal the horaFinal to set
	 */
	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}
	/**
	 * @return the minutoFinal
	 */
	public String getMinutoFinal() {
		return minutoFinal;
	}
	/**
	 * @param minutoFinal the minutoFinal to set
	 */
	public void setMinutoFinal(String minutoFinal) {
		this.minutoFinal = minutoFinal;
	}
	/**
	 * @return the porcentajeRetencion
	 */
	public String getPorcentajeDeRetencion() {
		return porcentajeDeRetencion;
	}
	/**
	 * @param porcentajeRetencion the porcentajeRetencion to set
	 */
	public void setPorcentajeDeRetencion(String porcentajeDeRetencion) {
		this.porcentajeDeRetencion = porcentajeDeRetencion;
	}
	/**
	 * @return the porcentajeCesion
	 */
	public String getPorcentajeDeCesion() {
		return porcentajeDeCesion;
	}
	/**
	 * @param porcentajeCesion the porcentajeCesion to set
	 */
	public void setPorcentajeDeCesion(String porcentajeDeCesion) {
		this.porcentajeDeCesion = porcentajeDeCesion;
	}
	/**
	 * @return the idTcMoneda
	 */
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	/**
	 * @param idTcMoneda the idTcMoneda to set
	 */
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	/**
	 * @return the folioContrato
	 */
	public String getFolioContratoCuotaParte() {
		return folioContratoCuotaParte;
	}
	/**
	 * @param folioContratoCuotaParte the folioContrato to set
	 */
	public void setFolioContratoCuotaParte(String folioContratoCuotaParte) {
		this.folioContratoCuotaParte = folioContratoCuotaParte;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}	
	/**
	 * @return the formaPago
	 */
	public String getFormaPago() {
		return formaPago;
	}
	/**
	 * @param formaPago the formaPago to set
	 */
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	/**
	 * @return the fechaAutorizacion
	 */
	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	/**
	 * @param fechaAutorizacion the fechaAutorizacion to set
	 */
	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	/**
	 * @return the usuarioAutorizo
	 */
	public String getUsuarioAutorizo() {
		return usuarioAutorizo;
	}
	/**
	 * @param usuarioAutorizo the usuarioAutorizo to set
	 */
	public void setUsuarioAutorizo(String usuarioAutorizo) {
		this.usuarioAutorizo = usuarioAutorizo;
	}
	/**
	 * @return the seleccionado
	 */
	public String getSeleccionado() {
		return seleccionado;
	}
	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(String seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the idTcRamo
	 */
	public String getIdTcRamo() {
		return idTcRamo;
	}
	/**
	 * @param idTcRamo the idTcRamo to set
	 */
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	
	/**
	 * @return the idTcSubRamo
	 */
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	/**
	 * @param idTcSubRamo the idTcSubRamo to set
	 */
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}	
	
	public String getIdTmContratoPrimerExcedente() {
		return idTmContratoPrimerExcedente;
	}
	public void setIdTmContratoPrimerExcedente(String idTmContratoPrimerExcedente) {
		this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
	}
	
	public String getParticipacionesCP() {
		return participacionesCP;
	}
	public void setParticipacionesCP(String participacionesCP) {
		this.participacionesCP = participacionesCP;
	}
	
	public String getFolioContratoPrimerExcedente() {
			return folioContratoPrimerExcedente;
	}
	public void setFolioContratoPrimerExcedente(String folioContratoPrimerExcedente) {
		this.folioContratoPrimerExcedente = folioContratoPrimerExcedente;
	}
	public String getReglaNavegacion() {
		return reglaNavegacion;
	}
	public void setReglaNavegacion(String reglaNavegacion) {
		this.reglaNavegacion = reglaNavegacion;
	}
	
}