package mx.com.afirme.midas.contratos.contratocuotaparte;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.contratos.linea.LineaDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContratoCuotaParteAction extends MidasMappingDispatchAction {
	
	/**
	 * Method autorizarContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void autorizarContrato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		LineaDN lineaDN = LineaDN.getInstancia();
		String idTmContratoCuotaParte = new String();
		ContratoCuotaParteDN contratoCuotaParteDN  = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
		idTmContratoCuotaParte = request.getParameter("idTmContratoCuotaParte");

		try {
			contratoCuotaParteDTO = contratoCuotaParteDN.getPorId(new BigDecimal(idTmContratoCuotaParte));		
			Set<String> errores = lineaDN.validarAutorizarCP(contratoCuotaParteDTO);
			if (errores.size() == 0){
				contratoCuotaParteDTO.setEstatus(new BigDecimal(1));
				contratoCuotaParteDTO = contratoCuotaParteDN.guardarContrato(contratoCuotaParteDTO);
			}
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<item>");			
			if (errores.size() == 0) {
				buffer.append("<id>1</id><description><![CDATA[Se autoriz\u00f3 el contrato.]]></description>");
			}
			else {
				Map<String,String> erroresMensajes = UtileriasWeb.generarMensajes(
						errores, getResources(request)); //Generar los mensajes
				String mensajes = generarStringMensaje(erroresMensajes);
				buffer.append("<id>0</id><description><![CDATA[El contrato no fue autorizado por las siguientes causas:\n" 
						+ mensajes + "]]></description>");
			}
			buffer.append("</item>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	/**
	 * Method cancelarContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward cancelarContrato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;
		ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
		try {
			contratoCuotaParteDTO.setIdTmContratoCuotaParte(new BigDecimal(contratoCuotaParteForm.getIdTmContratoCuotaParte()));
			contratoCuotaParteDTO = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request))
			.cancelarContratoCuotaParte(contratoCuotaParteDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method mostrarInicio
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarInicio(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
			final String idTcMonedaDolares = "840"; 
			String reglaNavegacion = Sistema.EXITOSO;
			ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;			
			contratoCuotaParteForm.setFolioContratoCuotaParte("Sin asignar");
			contratoCuotaParteForm.setIdTcMoneda(idTcMonedaDolares);
			
			return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method crearContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward crearContrato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
			throws SystemException{
			
		String reglaNavegacion = Sistema.EXITOSO;
		final String idTcMonedaDolares = "840"; 
		ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;
		ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
		ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			contratoCuotaParteForm.setIdTcMoneda(idTcMonedaDolares); //Poner siempre el valor en dolares.
			this.poblarDTO(contratoCuotaParteForm, contratoCuotaParteDTO);
			
			contratoCuotaParteDTO.setEstatus(new BigDecimal(0));
			contratoCuotaParteDTO.setFolioContrato("CP0000"); //El dato es obligatorio, se inicializa de esta manera y una vez guardado el contrato se genera su consecutivo
			contratoCuotaParteDTO = contratoCuotaParteDN.crearContrato(contratoCuotaParteDTO);
			String folio = this.darFormatoFolio(contratoCuotaParteDTO.getIdTmContratoCuotaParte());
			contratoCuotaParteDTO.setFolioContrato(folio);
			
			contratoCuotaParteDN.guardarContrato(contratoCuotaParteDTO);
			
			contratoCuotaParteForm.setIdTmContratoCuotaParte(contratoCuotaParteDTO.getIdTmContratoCuotaParte().toString());
			contratoCuotaParteForm.setFolioContratoCuotaParte(contratoCuotaParteDTO.getFolioContrato());
			contratoCuotaParteForm.setEstatus("0");
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ParseException e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);					
	}	
	
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;
			String idContrato = request.getParameter("idContratoCP");
			String reglaNavegacionFromRequest = request.getParameter("reglaNavegacion");
			String formularioOrigen = request.getParameter("formularioOrigen");
			String ventanaAccionada =  request.getParameter("ventanaAccionada");
			request.setAttribute("formularioOrigen", formularioOrigen);
			request.setAttribute("ventanaAccionada", ventanaAccionada);
			
			ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;
			ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			if(idContrato != null){
				try{
					ContratoCuotaParteDTO contratoDTO;
					contratoDTO = contratoCuotaParteDN.getPorId(new BigDecimal(idContrato));
					this.poblarForm(contratoDTO, contratoCuotaParteForm);
					contratoCuotaParteForm.setReglaNavegacion(reglaNavegacionFromRequest);
					this.listarParticipaciones(request, contratoDTO);					
				}catch(ExcepcionDeAccesoADatos edaad){
					reglaNavegacion = Sistema.NO_EXITOSO;
				}catch(SystemException e){
					reglaNavegacion = Sistema.NO_DISPONIBLE;
				}catch(ParseException e){
					reglaNavegacion = Sistema.NO_EXITOSO;
				}
			}
			return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Method guardarContrato
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void guardarContrato(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {

			ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;	
			
			ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
			ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			request.setAttribute("contratoCP", contratoCuotaParteDTO);
			try {
				this.poblarDTO(contratoCuotaParteForm, contratoCuotaParteDTO);
				contratoCuotaParteDTO = contratoCuotaParteDN.guardarContrato(contratoCuotaParteDTO);
												
				UtileriasWeb.imprimeMensajeXML("30", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.exito.guardar"), response);
				
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				UtileriasWeb.imprimeMensajeXML("10", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.guardar"), response);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				UtileriasWeb.imprimeMensajeXML("10", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.guardar"), response);
			} catch(ParseException pe){
				contratoCuotaParteForm.setMensaje("Formato de fecha incorrecto");
				UtileriasWeb.mandaMensajeExcepcionRegistrado(pe.getMessage(), request);
				UtileriasWeb.imprimeMensajeXML("10", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.guardar"), response);
			}
		
	}
	
	public ActionForward regresarRegistrarContratoCuotaParte(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion;	
		
		ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;	
		if (UtileriasWeb.esObjetoNulo(contratoCuotaParteForm.getReglaNavegacion()) || contratoCuotaParteForm.getReglaNavegacion().equals("0"))
			reglaNavegacion = Sistema.EXITOSO;
		else
			reglaNavegacion = "regresarAgregarLinea";
		
		return mapping.findForward(reglaNavegacion);
	}
		
	/**
	 * listar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {

			String reglaNavegacion = Sistema.EXITOSO;
			try {
				ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				List<ContratoCuotaParteDTO> contratoCuotaPartes = contratoCuotaParteDN.listarTodos();
				request.setAttribute("contratoCuotaPartes", contratoCuotaPartes);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
			}
	
			return mapping.findForward(reglaNavegacion);
	}		
	
	/**
	 * mostrarListar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarListar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;		
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * listarFiltrado
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
										
		String fechaInicial = request.getParameter("fi");
		String fechaFinal = request.getParameter("ff");
		
		try{												
			ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;
			contratoCuotaParteForm.setFechaInicial(fechaInicial);
			contratoCuotaParteForm.setFechaFinal(fechaFinal);
			ContratoCuotaParteDTO  contratoCuotaParteDTO  = new ContratoCuotaParteDTO();
			ContratoCuotaParteDN contratoCuotaParteDN 	= ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			this.poblarDTO(contratoCuotaParteForm, contratoCuotaParteDTO);			
			List<ContratoCuotaParteDTO> contratos = contratoCuotaParteDN.listarFiltrado(contratoCuotaParteDTO);
			String json = this.getJson(contratos);			
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){			
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
						
		}catch(IOException e){
						
		}					
	}
	
	/**
	 * seleccionar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward seleccionar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;		
		ContratoCuotaParteForm contratoCuotaParteForm = (ContratoCuotaParteForm) form;
		String idSeleccionado = contratoCuotaParteForm.getSeleccionado();
		if(idSeleccionado!=null && !idSeleccionado.equals("")){
			ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			try{
				ContratoCuotaParteDTO contratoCuotaParteDTO = 
					contratoCuotaParteDN.getPorId(new BigDecimal(idSeleccionado));								
				request.setAttribute("contratoCP", contratoCuotaParteDTO);
			}catch(ExcepcionDeAccesoADatos e){
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}catch(SystemException e){
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}			
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param ContratoCuotaParteForm
	 * @param ContratoCuotaParteDTO
	 * @throws ParseException 
	 */
	private void poblarDTO(ContratoCuotaParteForm contratoCuotaParteForm, 
		ContratoCuotaParteDTO contratoCuotaParteDTO) throws ParseException{					
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getEstatus())) {
			String estatus = contratoCuotaParteForm.getEstatus();
			contratoCuotaParteDTO.setEstatus(new BigDecimal(estatus));
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getFechaFinal())) {			
			String fechaHora = contratoCuotaParteForm.getFechaFinal();
			Date fechaFinal = UtileriasWeb.getFechaHoraFromString(fechaHora);
			contratoCuotaParteDTO.setFechaFinal(fechaFinal);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getFechaInicial())) {			
			String fechaHora = contratoCuotaParteForm.getFechaInicial();
			Date fechaInicial = UtileriasWeb.getFechaHoraFromString(fechaHora);
			contratoCuotaParteDTO.setFechaInicial(fechaInicial);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getFolioContratoCuotaParte())) {
			String folioContrato = contratoCuotaParteForm.getFolioContratoCuotaParte();
			contratoCuotaParteDTO.setFolioContrato(folioContrato);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getIdTcMoneda())) {
			BigDecimal idTcMoneda = new BigDecimal(contratoCuotaParteForm.getIdTcMoneda());
			contratoCuotaParteDTO.setIdTcMoneda(idTcMoneda);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getIdTmContratoCuotaParte())) {
			BigDecimal idTmContratoCuotaParte = new BigDecimal(contratoCuotaParteForm.getIdTmContratoCuotaParte());
			contratoCuotaParteDTO.setIdTmContratoCuotaParte(idTmContratoCuotaParte);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getPorcentajeDeCesion())) {
			Double porcentajeCesion = Double.valueOf(contratoCuotaParteForm.getPorcentajeDeCesion());
			contratoCuotaParteDTO.setPorcentajeCesion(porcentajeCesion);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getPorcentajeDeRetencion())) {
			Double porcentajeRetencion = Double.valueOf(contratoCuotaParteForm.getPorcentajeDeRetencion());
			contratoCuotaParteDTO.setPorcentajeRetencion(porcentajeRetencion);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getFormaPago())) {
			BigDecimal formaPago = new BigDecimal(contratoCuotaParteForm.getFormaPago());
			contratoCuotaParteDTO.setFormaPago(formaPago);
		}		
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getFechaAutorizacion())) {						
			String fecha = contratoCuotaParteForm.getFechaAutorizacion();
			Date fechaAutorizacion = UtileriasWeb.getFechaFromString(fecha);
			contratoCuotaParteDTO.setFechaAutorizacion(fechaAutorizacion);
		}
		
		if (!StringUtil.isEmpty(contratoCuotaParteForm.getUsuarioAutorizo())) {						
			BigDecimal usuario = new BigDecimal(contratoCuotaParteForm.getUsuarioAutorizo());			
			contratoCuotaParteDTO.setUsuarioAutorizo(usuario);
		}
		
	}
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContratoCuotaParteDTO
	 * @param ContratoCuotaParteForm
	 * @throws ParseException 
	 */	
	
	private void poblarForm(ContratoCuotaParteDTO contratoCuotaParteDTO, 
	ContratoCuotaParteForm contratoCuotaParteForm) throws ParseException{
		if(contratoCuotaParteDTO.getEstatus()!=null)
			contratoCuotaParteForm.setEstatus(contratoCuotaParteDTO.getEstatus().toString());		
		
		if(contratoCuotaParteDTO.getFechaFinal()!=null){
			String fecha = UtileriasWeb.getFechaHoraString(contratoCuotaParteDTO.getFechaFinal());						
			contratoCuotaParteForm.setFechaFinal(fecha);					
		}
		
		if(contratoCuotaParteDTO.getFechaInicial()!=null){
			String fecha = UtileriasWeb.getFechaHoraString(contratoCuotaParteDTO.getFechaInicial());		
			contratoCuotaParteForm.setFechaInicial(fecha);			
		}
					
		if(contratoCuotaParteDTO.getFolioContrato()!=null)
			contratoCuotaParteForm.setFolioContratoCuotaParte(contratoCuotaParteDTO.getFolioContrato().toString());
		
		if(contratoCuotaParteDTO.getFormaPago()!=null)
			contratoCuotaParteForm.setFormaPago(contratoCuotaParteDTO.getFormaPago().toString());
		
		if(contratoCuotaParteDTO.getFechaAutorizacion()!=null){
			String fecha = UtileriasWeb.getFechaString(contratoCuotaParteDTO.getFechaAutorizacion());						
			contratoCuotaParteForm.setFechaAutorizacion(fecha);						
		}			
		
		if(contratoCuotaParteDTO.getIdTcMoneda()!=null)
			contratoCuotaParteForm.setIdTcMoneda(contratoCuotaParteDTO.getIdTcMoneda().toBigInteger().toString());
		
		if(contratoCuotaParteDTO.getIdTmContratoCuotaParte()!=null)
			contratoCuotaParteForm.setIdTmContratoCuotaParte(contratoCuotaParteDTO.getIdTmContratoCuotaParte().toBigInteger().toString());
		
		if(contratoCuotaParteDTO.getPorcentajeCesion()!=null){
			Double porcentaje = contratoCuotaParteDTO.getPorcentajeCesion();
			String porcentajeFormato = MessageFormat.format("{0,number,#.##}", porcentaje);
			contratoCuotaParteForm.setPorcentajeDeCesion(porcentajeFormato);
		}
		
		if(contratoCuotaParteDTO.getPorcentajeRetencion()!=null){
			Double porcentaje = contratoCuotaParteDTO.getPorcentajeRetencion();
			String porcentajeFormato = MessageFormat.format("{0,number,#.##}", porcentaje);
			contratoCuotaParteForm.setPorcentajeDeRetencion(porcentajeFormato);
		}
		
		if(contratoCuotaParteDTO.getUsuarioAutorizo()!=null)
			contratoCuotaParteForm.setUsuarioAutorizo(contratoCuotaParteDTO.getUsuarioAutorizo().toString());
	}
	
	private String darFormatoFolio(BigDecimal id){
		String numeracion = UtileriasWeb.llenarIzquierda(id.toString(), "0", 4);		
		return 	"CP".concat(numeracion);	
	}
	
	private void listarParticipaciones(
			HttpServletRequest request, ContratoCuotaParteDTO contratoDTO)
			throws ExcepcionDeAccesoADatos, SystemException{
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		ParticipacionDTO participacionDTO = new ParticipacionDTO();		
		participacionDTO.setContratoCuotaParte(contratoDTO);
		List<ParticipacionDTO> participaciones = participacionDN.listarFiltrados(participacionDTO);
		request.setAttribute("participaciones", participaciones);
	}
	
	private String getJson(List<ContratoCuotaParteDTO> contratos) 
	throws ExcepcionDeAccesoADatos, SystemException{
		MonedaDN monedaDN = MonedaDN.getInstancia();		
//		String json = "{rows:[";
//		if(contratos != null && contratos.size() > 0) {
//			for(ContratoCuotaParteDTO contrato : contratos) {
//				
//				String moneda = "";
//				try{
//					moneda = monedaDN.getPorId(contrato.getIdTcMoneda().shortValue()).getDescripcion();
//				}catch(NullPointerException e){}
//				
//				json += "{id:" + contrato.getIdTmContratoCuotaParte() + ",data:[";
//				json += "0,\"";
//				json += contrato.getFolioContrato() + "\",";				
//				json += contrato.getPorcentajeCesion() + ",";
//				json += contrato.getPorcentajeRetencion() + ",\"";
//				json += moneda +"\",";
//				if (contrato.getEstatus() == null || contrato.getEstatus().toString().equals("0")){
//					json += "\"No Autorizado\",";
//					json += "\"/MidasWeb/img/Edit14.gif^Modificar^javascript:sendRequestContrato(1,"+contrato.getIdTmContratoCuotaParte()+",contratoCuotaParteForm);^_self\""+ ","; 
//				}else
//					if (contrato.getEstatus().toString().equals("1")){
//						json += "\"Autorizado\",";
//						json += "\"/MidasWeb/img/Edit14_disabled.gif^No habilitado para modificar, desautorizar primero^javascript: void(0);^_self\""+ ","; 
//					}
//				
//				json += "\"/MidasWeb/img/details.gif^Detalle^javascript:sendRequestContrato(2,"+contrato.getIdTmContratoCuotaParte()+",contratoCuotaParteForm);^_self\""+"]},";
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		
		MidasJsonBase json = new MidasJsonBase();
		if(contratos != null && contratos.size() > 0) {
			for(ContratoCuotaParteDTO contrato : contratos) {
				
				String moneda = "";
				String estatusDesc = null;
				String iconoEstatus = null;
				
				if (contrato.getEstatus() == null || contrato.getEstatus().toString().equals("0")){
					estatusDesc = "No Autorizado";
					iconoEstatus = "/MidasWeb/img/Edit14.gif^Modificar^javascript:sendRequestContrato(1,"+contrato.getIdTmContratoCuotaParte()+",contratoCuotaParteForm);^_self"; 
				} else if (contrato.getEstatus().toString().equals("1")){
					estatusDesc = "Autorizado";
					iconoEstatus = "/MidasWeb/img/Edit14_disabled.gif^No habilitado para modificar, desautorizar primero^javascript: void(0);^_self"; 
				}
				
				try{
					moneda = monedaDN.getPorId(contrato.getIdTcMoneda().shortValue()).getDescripcion();
				}catch(NullPointerException e){}
				
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(contrato.getIdTmContratoCuotaParte().toString());
				row.setDatos(
						"0",
						contrato.getFolioContrato(),
						contrato.getPorcentajeCesion().toString(),
						contrato.getPorcentajeRetencion().toString(),
						moneda,
						estatusDesc,
						iconoEstatus,
						"/MidasWeb/img/details.gif^Detalle^javascript:sendRequestContrato(2,"+contrato.getIdTmContratoCuotaParte()+",contratoCuotaParteForm);^_self"
						
				);
				json.addRow(row);
			}
		}
		
		return json.toString();
	}
	
	private String generarStringMensaje(Map<String, String> erroresMensajes) {
		StringBuffer mensajeTotal = new StringBuffer();
		for (String key : erroresMensajes.keySet()) {
			String mensaje = erroresMensajes.get(key);
			mensajeTotal.append(mensaje + "\n");
		}
		return mensajeTotal.toString();
	}
}

