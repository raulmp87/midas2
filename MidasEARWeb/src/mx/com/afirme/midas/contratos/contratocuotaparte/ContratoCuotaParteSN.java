package mx.com.afirme.midas.contratos.contratocuotaparte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContratoCuotaParteSN {

	private ContratoCuotaParteFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public ContratoCuotaParteSN(String nombreUsuario) throws SystemException {
		this.nombreUsuario = nombreUsuario;
		LogDeMidasWeb.log("Entrando en ContratoCuotaParteSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ContratoCuotaParteFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public ContratoCuotaParteDTO crearContrato(ContratoCuotaParteDTO contratoCuotaParteDTO)
	throws ExcepcionDeAccesoADatos {
		contratoCuotaParteDTO.setNombreUsuarioLog(this.nombreUsuario);
		if(contratoCuotaParteDTO.getIdTmContratoCuotaParte()==null){
			return beanRemoto.save(contratoCuotaParteDTO);				
		}
		else{
			return beanRemoto.update(contratoCuotaParteDTO);
		}
	}
	
	public ContratoCuotaParteDTO guardarContrato(ContratoCuotaParteDTO contratoCuotaParteDTO)
		throws ExcepcionDeAccesoADatos {
		contratoCuotaParteDTO.setNombreUsuarioLog(this.nombreUsuario);	
		return beanRemoto.update(contratoCuotaParteDTO);
	}	
	
	public List<ContratoCuotaParteDTO> listarTodos() 
		throws ExcepcionDeAccesoADatos {	
			List<ContratoCuotaParteDTO> contratoCuotaPartes = beanRemoto.findAll();
			return contratoCuotaPartes;
	}
	
	public ContratoCuotaParteDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}
	
	public List<ContratoCuotaParteDTO> listarFiltrado(ContratoCuotaParteDTO contratoCuotaParteDTO) 
	throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltrado(contratoCuotaParteDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ContratoCuotaParteDTO> buscarPorFechaInicialFinal(Date fechaInicial,Date fechaFinal)
	throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.buscarPorFechaInicialFinal(fechaInicial,fechaFinal);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String ids,Date fInicial,Date fFinal)
	throws ExcepcionDeAccesoADatos{
		try{			
			return beanRemoto.actualizaFechasLineasPorIdLineas(ids, fInicial, fFinal);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ContratoCuotaParteDTO cancelarContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParteDTO){
		return beanRemoto.cancelarContratoCuotaParte(contratoCuotaParteDTO);
	}
	
	ContratoCuotaParteDTO obtenerContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		return beanRemoto.obtenerContratoCuotaParteEjercicioAnterior(estadoCuentaDTO);
	}
	
	List<ContratoCuotaParteDTO> obtenerListaContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO){
		return beanRemoto.obtenerListaContratoCuotaParteEjercicioAnterior(estadoCuentaDTO);
	}
}
