package mx.com.afirme.midas.contratos.contratocuotaparte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContratoCuotaParteDN {
	private static final ContratoCuotaParteDN INSTANCIA = new ContratoCuotaParteDN();
	private static String nombreUsuario;
	
	public static ContratoCuotaParteDN getInstancia(String nombreUsuario) {
		ContratoCuotaParteDN.nombreUsuario = nombreUsuario;
		return ContratoCuotaParteDN.INSTANCIA;
	}
		
	public ContratoCuotaParteDTO crearContrato(ContratoCuotaParteDTO contratoCuotaParteDTO)throws SystemException,
		ExcepcionDeAccesoADatos {
			ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario);
			return contratoCuotaParteSN.crearContrato(contratoCuotaParteDTO);
	}
	
	public ContratoCuotaParteDTO guardarContrato(ContratoCuotaParteDTO contratoCuotaParteDTO) throws SystemException,
		ExcepcionDeAccesoADatos {
		
		ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario); 
			return contratoCuotaParteSN.guardarContrato(contratoCuotaParteDTO);
	}	
		
	public List<ContratoCuotaParteDTO> listarTodos() 
		throws SystemException,ExcepcionDeAccesoADatos{
	
			ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario);		
			return contratoCuotaParteSN.listarTodos();
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String idLineas, Date fInicial, Date fFinal) 
	throws SystemException, ExcepcionDeAccesoADatos{

		ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario);
		return contratoCuotaParteSN.actualizaFechasLineasPorIdLineas(idLineas,fInicial,fFinal);
	}
	public ContratoCuotaParteDTO getPorId(BigDecimal id)
	throws SystemException, ExcepcionDeAccesoADatos {
		ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario);
		return contratoCuotaParteSN.getPorId(id);
	}
	
	public List<ContratoCuotaParteDTO> listarFiltrado(ContratoCuotaParteDTO contratoCuotaParteDTO) 
	throws SystemException, ExcepcionDeAccesoADatos{
		ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario);
		return contratoCuotaParteSN.listarFiltrado(contratoCuotaParteDTO);
	}
	
	public ContratoCuotaParteDTO cancelarContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParteDTO) throws SystemException{
		ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario);
		return contratoCuotaParteSN.cancelarContratoCuotaParte(contratoCuotaParteDTO);
	}
	
	public ContratoCuotaParteDTO obtenerContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO) throws SystemException{
		return new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario).obtenerContratoCuotaParteEjercicioAnterior(estadoCuentaDTO);
	}
	
	public List<ContratoCuotaParteDTO> obtenerListaContratoCuotaParteEjercicioAnterior(EstadoCuentaDTO estadoCuentaDTO) throws SystemException{
		return new ContratoCuotaParteSN(ContratoCuotaParteDN.nombreUsuario).obtenerListaContratoCuotaParteEjercicioAnterior(estadoCuentaDTO);
	}
}
