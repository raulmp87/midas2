package mx.com.afirme.midas.contratos.participacion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ParticipacionDN {
	private static final ParticipacionDN INSTANCIA = new ParticipacionDN();

	public static ParticipacionDN getInstancia() {
		return ParticipacionDN.INSTANCIA;
	}

	public List<ParticipacionDTO> listarTodos() throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		return participacionSN.listarTodos();
	}

	public List<ParticipacionDTO> listarFiltrados(ParticipacionDTO participacionDTO)
			throws SystemException, ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		return participacionSN.listarFiltrados(participacionDTO);
	}

	public void agregar(ParticipacionDTO participacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		participacionSN.agregar(participacionDTO);
	}

	public void modificar(ParticipacionDTO participacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		participacionSN.modificar(participacionDTO);
	}

	public ParticipacionDTO getPorId(BigDecimal id)
			throws SystemException, ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		return participacionSN.getPorId(id);
	}

	public void borrar(ParticipacionDTO participacionDTO) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		participacionSN.borrar(participacionDTO);		
	}
	
	public List<ParticipacionDTO>  getPorPropiedad(String propertyName,Object value) throws SystemException,
			ExcepcionDeAccesoADatos {
		ParticipacionSN participacionSN = new ParticipacionSN();
		return participacionSN.getPorPropiedad(propertyName, value);
	}
	
	public List<ParticipacionDTO>  buscarPorLineaCCP(ContratoCuotaParteDTO contratoCuotaParteDTO,LineaDTO lineaDTO) throws SystemException,
		ExcepcionDeAccesoADatos {
			ParticipacionSN participacionSN = new ParticipacionSN();
				return participacionSN.buscarPorLineaCCP(contratoCuotaParteDTO, lineaDTO);
	}
	
	public List<ParticipacionDTO>  buscarPorLineaCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,LineaDTO lineaDTO) throws SystemException,
		ExcepcionDeAccesoADatos {
			ParticipacionSN participacionSN = new ParticipacionSN();
				return participacionSN.buscarPorLineaCPE(contratoPrimerExcedenteDTO, lineaDTO);
	}
	
	public List<ParticipacionDTO> listarPorContratoCuotaParte(ContratoCuotaParteDTO contratoCuotaParteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return this.getPorPropiedad("contratoCuotaParte.idTmContratoCuotaParte", contratoCuotaParteDTO.getIdTmContratoCuotaParte());
	}
	
	public List<ParticipacionDTO> listarPorContratoPrimerExcedente(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return this.getPorPropiedad("contratoPrimerExcedente.idTmContratoPrimerExcedente", contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente());
	}
	
	public double calcularTotalParticipacionPrimerExcedente(BigDecimal idContrato) throws SystemException {
		ParticipacionSN participacionSN = new ParticipacionSN();
		List<ParticipacionDTO> participaciones = participacionSN.getPorPropiedad(
				"contratoPrimerExcedente.idTmContratoPrimerExcedente", idContrato);
		return calcularTotalParticipacion(participaciones);
	}
	
	public double calcularTotalParticipacionCuotaParte(BigDecimal idContrato) throws SystemException {
		ParticipacionSN participacionSN = new ParticipacionSN();
		List<ParticipacionDTO> participaciones = participacionSN.getPorPropiedad(
				"contratoCuotaParte.idTmContratoCuotaParte", idContrato);
		return calcularTotalParticipacion(participaciones);
	}

	private double calcularTotalParticipacion(
			List<ParticipacionDTO> participaciones) {
		double sumaTotalParticipacion = 0;
		for (ParticipacionDTO participacionDTO : participaciones) {
			sumaTotalParticipacion += participacionDTO.getPorcentajeParticipacion();
		}
		return sumaTotalParticipacion;
	}
}
