package mx.com.afirme.midas.contratos.participacion;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDN;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDN;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDN;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.contacto.ContactoDN;
import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;

public class ParticipacionAction extends MidasMappingDispatchAction {		
	
	/**
	 * Method mostrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
			String reglaNavegacion = Sistema.EXITOSO;	
			String idContrato = request.getParameter("idContratoCP");
			BigDecimal idContratoDecimal;
			ParticipacionForm participacionForm = (ParticipacionForm) form;
			ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
			String sumaTotalParticipacion = null;
			
			try {
				if (!UtileriasWeb.esCadenaVacia(idContrato)){
					idContratoDecimal = new BigDecimal(idContrato);
					sumaTotalParticipacion = String.valueOf(
							participacionDN.calcularTotalParticipacionCuotaParte(
									idContratoDecimal));
					participacionForm.setIdTmContratoCuotaParte(idContrato);
					participacionForm.setIdTmContratoPrimerExcedente(null);
					participacionForm.setSumaTotalParticipacion(sumaTotalParticipacion);
				}else{
					idContrato = request.getParameter("idContratoPE");
					idContratoDecimal = new BigDecimal(idContrato);
					if (!UtileriasWeb.esCadenaVacia(idContrato)){
						sumaTotalParticipacion = String.valueOf(
								participacionDN.calcularTotalParticipacionPrimerExcedente(
										idContratoDecimal));
						participacionForm.setIdTmContratoPrimerExcedente(idContrato);
						participacionForm.setIdTmContratoCuotaParte(null);
						participacionForm.setSumaTotalParticipacion(sumaTotalParticipacion);
					}
				}
			}
			catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				mensajeExcepcion(participacionForm, e);

			}
			
			return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {			
			ParticipacionForm participacionForm = (ParticipacionForm) form;			
			ParticipacionDTO participacionDTO = new ParticipacionDTO();
			ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
			String reglaNavegacion= Sistema.NO_EXITOSO;
			String id = "0";
			ActionForward forward;
			try {
				this.poblarDTO(participacionForm, participacionDTO, request);
				if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoCuotaParte()) && !UtileriasWeb.esObjetoNulo(participacionDTO.getContratoCuotaParte().getIdTmContratoCuotaParte())){
					reglaNavegacion = "exitosoCP";
					id = "?idContratoCP=" + participacionForm.getIdTmContratoCuotaParte();
					participacionDTO.setContratoPrimerExcedente(null);
				}else{
					if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoPrimerExcedente()) && !UtileriasWeb.esObjetoNulo(participacionDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente())){
						reglaNavegacion = "exitosoPE";
						id = "?idContratoPE=" + participacionForm.getIdTmContratoPrimerExcedente();
						participacionDTO.setContratoCuotaParte(null);
					}
				}
				participacionDTO.setParticipacionCorredores(null);
				
				if (participacionDTO.getContacto() != null && participacionDTO.getContacto().getIdtccontacto() == null)
					participacionDTO.setContacto(null);
				
				participacionDN.agregar(participacionDTO);				
				forward = new ActionForward(mapping.findForward(reglaNavegacion));
				String path;
				path = forward.getPath();
				forward.setPath(path + id);
			} catch (ExcepcionDeAccesoADatos e) {				
				forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
			} catch (SystemException e) {				
				forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
			}
			
			return forward;
	}
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {		
		ParticipacionForm participacionForm = (ParticipacionForm) form;			
		ParticipacionDTO participacionDTO = new ParticipacionDTO();		
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();		
		ActionForward forward;
		try {
			this.poblarDTO(participacionForm, participacionDTO, request);
			participacionDN.modificar(participacionDTO);
			String reglaNavegacion="noExitoso";
			String id = "0";
			if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoCuotaParte())){
				reglaNavegacion = "exitosoCP";
				id = "?idContratoCP=" + participacionForm.getIdTmContratoCuotaParte();
			}else{
				if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoPrimerExcedente())){
					reglaNavegacion = "exitosoPE";
					id = "?idContratoPE=" + participacionForm.getIdTmContratoPrimerExcedente();
				}
			}
				
			forward = new ActionForward(mapping.findForward(reglaNavegacion));		
			String path = forward.getPath();
			forward.setPath(path + id);
		} catch (SystemException e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_DISPONIBLE));
		} catch (ExcepcionDeAccesoADatos e) {
			forward = new ActionForward(mapping.findForward(Sistema.NO_EXITOSO));
		}

		return forward;

	}

	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String estatusBorrar = "borrado";
		ParticipacionForm participacionForm = (ParticipacionForm) form;
		ParticipacionDTO participacionDTO = new ParticipacionDTO();				
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		@SuppressWarnings("unused")
		String reglaNavegacion="noExitoso";
		try {
			this.poblarDTO(participacionForm, participacionDTO, request);
			participacionDN.borrar(participacionDTO);
			if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoCuotaParte())){
				reglaNavegacion = "exitosoCP";
			}else{
				if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoPrimerExcedente())){
					reglaNavegacion = "exitosoPE";
				}
			}
				
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoCuotaParte())){
				estatusBorrar = "noBorrado";
				reglaNavegacion = "noExitosoCP";
			}else{
				if (!UtileriasWeb.esObjetoNulo(participacionDTO.getContratoPrimerExcedente())){
					estatusBorrar = "noBorrado";
					reglaNavegacion = "noExitosoPE";
				}
			}
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<response>");
		buffer.append("<item>");
		buffer.append("<estatus>"+estatusBorrar+"</estatus>");
		buffer.append("</item>");
		buffer.append("</response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
		}
	}
	
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		ParticipacionForm participacionForm = (ParticipacionForm) form;
		ParticipacionDTO participacionDTO = new ParticipacionDTO();
		String idParam = request.getParameter("id");
		BigDecimal id = BigDecimal.valueOf(Double.valueOf(idParam));
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		try {
			participacionDTO = participacionDN.getPorId(id);
			this.poblarForm(participacionDTO, participacionForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}

		return mapping.findForward(reglaNavegacion);
	}
						
	/**
	 * Method poblarDTO
	 * 
	 * @param ParticipacionForm
	 * @param ParticipacionDTO
	 */
	private void poblarDTO(ParticipacionForm participacionForm, 
	ParticipacionDTO participacionDTO, HttpServletRequest request) 
	throws ExcepcionDeAccesoADatos, SystemException{								
		if (!StringUtil.isEmpty(participacionForm.getIdTdParticipacion())) {			
			String id = participacionForm.getIdTdParticipacion();			
			participacionDTO.setIdTdParticipacion(new BigDecimal(id));
		}
		
		if (!StringUtil.isEmpty(participacionForm.getIdTmContratoCuotaParte())) {			
			String id = participacionForm.getIdTmContratoCuotaParte();			
			ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();			
			contratoCuotaParteDTO = contratoCuotaParteDN.getPorId(new BigDecimal(id));
			participacionDTO.setContratoCuotaParte(contratoCuotaParteDTO);
			participacionDTO.setContratoPrimerExcedente(null);
		
		} else if (!StringUtil.isEmpty(participacionForm.getIdTmContratoPrimerExcedente())) {			
			String id = participacionForm.getIdTmContratoPrimerExcedente();			
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();			
			contratoPrimerExcedenteDTO = contratoPrimerExcedenteDN.getPorId(new BigDecimal(id));
			participacionDTO.setContratoPrimerExcedente(contratoPrimerExcedenteDTO);
			participacionDTO.setContratoCuotaParte(null);
		}
		
		if (!StringUtil.isEmpty(participacionForm.getTipoParticipante())) {			
			String tipo = participacionForm.getTipoParticipante();			
			participacionDTO.setTipo(new BigDecimal(tipo));
		}
		
		if (!StringUtil.isEmpty(participacionForm.getIdParticipante())) {			
			String id = participacionForm.getIdParticipante();			
			ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(id));
			reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			participacionDTO.setReaseguradorCorredor(reaseguradorCorredorDTO);
		}
		
		if (!StringUtil.isEmpty(participacionForm.getIdCuentaPesos())) {
			BigDecimal id = new BigDecimal(participacionForm.getIdCuentaPesos());			
			CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();			
			cuentaBancoDTO = cuentaBancoDN.getPorId(id);
			participacionDTO.setCuentaBancoPesos(cuentaBancoDTO);
		}
		
		if (!StringUtil.isEmpty(participacionForm.getIdCuentaDolares())) {
			BigDecimal id = new BigDecimal(participacionForm.getIdCuentaDolares());			
			CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();
			CuentaBancoDTO cuentaBancoDTO = new CuentaBancoDTO();			
			cuentaBancoDTO = cuentaBancoDN.getPorId(id);
			participacionDTO.setCuentaBancoDolares(cuentaBancoDTO);
		}
		
		if (!StringUtil.isEmpty(participacionForm.getIdContacto())) {
			BigDecimal id = new BigDecimal(participacionForm.getIdContacto());			
			ContactoDN contactoDN = ContactoDN.getInstancia();
			ContactoDTO contactoDTO = new ContactoDTO();			
			contactoDTO = contactoDN.getPorId(id);
			participacionDTO.setContacto(contactoDTO);
		}else
			participacionDTO.setContacto(null);
		
		if (!StringUtil.isEmpty(participacionForm.getPorcentajeParticipacion())) {
			Double porcentaje = Double.valueOf(participacionForm.getPorcentajeParticipacion());										
			participacionDTO.setPorcentajeParticipacion(porcentaje);
		}						
	}	
	
	/**
	 * Method poblarForm
	 * 
	 * @param ContratoCuotaParteDTO
	 * @param ParticipacionForm
	 * @throws ParseException 
	 */	
	private void poblarForm(ParticipacionDTO participacionDTO, 
	ParticipacionForm participacionForm) throws SystemException{
		String sumaTotalParticipacion = null;
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		double sumaTotalParticipacionDbl = 0;
		double sumaTotalParticipacionMenosActual;

		if(participacionDTO.getContacto()!=null)
			participacionForm.setIdContacto(participacionDTO.getContacto().getId().toString());
		
		if(participacionDTO.getContratoCuotaParte()!=null && !UtileriasWeb.esObjetoNulo(participacionDTO.getContratoCuotaParte().getIdTmContratoCuotaParte())){
			participacionForm.setIdTmContratoCuotaParte(participacionDTO.getContratoCuotaParte().getIdTmContratoCuotaParte().toString());
			participacionForm.setIdTmContratoPrimerExcedente(null);
			sumaTotalParticipacionDbl = participacionDN.calcularTotalParticipacionCuotaParte(
							participacionDTO.getContratoCuotaParte().getIdTmContratoCuotaParte());
		}
		else
			if(participacionDTO.getContratoPrimerExcedente()!=null && !UtileriasWeb.esObjetoNulo(participacionDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente())){
				participacionForm.setIdTmContratoPrimerExcedente(participacionDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente().toString());
				participacionForm.setIdTmContratoCuotaParte(null);
				sumaTotalParticipacionDbl = participacionDN.calcularTotalParticipacionPrimerExcedente(
								participacionDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente());
			}
		
		if(participacionDTO.getCuentaBancoDolares()!=null)
			participacionForm.setIdCuentaDolares(participacionDTO.getCuentaBancoDolares().getId().toString());
		
		if(participacionDTO.getCuentaBancoPesos()!=null)
			participacionForm.setIdCuentaPesos(participacionDTO.getCuentaBancoPesos().getId().toString());
		
		if(participacionDTO.getIdTdParticipacion()!=null)
			participacionForm.setIdTdParticipacion(participacionDTO.getIdTdParticipacion().toString());
		
		if(participacionDTO.getPorcentajeParticipacion()!=null){
			Double porcentaje = participacionDTO.getPorcentajeParticipacion();
			String porcentajeFormato = MessageFormat.format("{0,number,#.##}", porcentaje);
			participacionForm.setPorcentajeParticipacion(porcentajeFormato);
			sumaTotalParticipacionMenosActual = sumaTotalParticipacionDbl - porcentaje; //Restar el porcentaje actual al porcentaje total.
			sumaTotalParticipacion = String.valueOf(sumaTotalParticipacionMenosActual);
			participacionForm.setSumaTotalParticipacion(sumaTotalParticipacion);
			
		}
		
		if(participacionDTO.getReaseguradorCorredor()!=null)
			participacionForm.setIdParticipante(participacionDTO.getReaseguradorCorredor().getId().toString());
		
		if(participacionDTO.getTipo()!=null)
			participacionForm.setTipoParticipante(participacionDTO.getTipo().toString());						
	}
	
}

