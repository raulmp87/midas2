/**
 * 
 */
package mx.com.afirme.midas.contratos.participacion;

import java.util.List;
import java.util.logging.Level;
import java.math.BigDecimal;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class ParticipacionSN {
	private ParticipacionFacadeRemote beanRemoto;

	public ParticipacionSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ParticipacionSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ParticipacionFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<ParticipacionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		List<ParticipacionDTO> participaciones = beanRemoto.findAll();
		return participaciones;

	}
	public List<ParticipacionDTO> listarFiltrados(ParticipacionDTO participacionDTO) throws ExcepcionDeAccesoADatos {
		List<ParticipacionDTO> participaciones = beanRemoto.listarFiltrado(participacionDTO);
		return participaciones;
	}

	public void agregar(ParticipacionDTO participacionDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.save(participacionDTO);
	}

	public void modificar(ParticipacionDTO participacionDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.update(participacionDTO);
	}

	public ParticipacionDTO getPorId(BigDecimal id) throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);
	}

	public void borrar(ParticipacionDTO participacionDTO)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(participacionDTO);
	}
	
	public List<ParticipacionDTO>  getPorPropiedad(String propertyName,Object value) 
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByProperty(propertyName, value);
	}
	
	public List<ParticipacionDTO>  buscarPorLineaCCP(ContratoCuotaParteDTO contratoCuotaParteDTO,LineaDTO lineaDTO) 
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.buscarPorLineaCCP(contratoCuotaParteDTO, lineaDTO);
	}
	
	public List<ParticipacionDTO>  buscarPorLineaCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,LineaDTO lineaDTO) 
		throws ExcepcionDeAccesoADatos {
	return beanRemoto.buscarPorLineaCPE(contratoPrimerExcedenteDTO, lineaDTO);
	}
}

