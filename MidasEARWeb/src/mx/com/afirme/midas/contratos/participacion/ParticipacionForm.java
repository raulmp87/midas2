package mx.com.afirme.midas.contratos.participacion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ParticipacionForm extends MidasBaseForm {

	private static final long serialVersionUID = -2902937149615793221L;
		
    //Registrar participacion
	private String idTdParticipacion;
    private String idTmContratoCuotaParte;
    private String idTmContratoPrimerExcedente;
	private String tipoParticipante;
    private String idParticipante;
    private String idCuentaPesos;
    private String idCuentaDolares;
    private String idContacto;
    private String porcentajeParticipacion;
    private String comision;
    private String fechaInicial;
    private String fechaFinal;
    private String nombreCorredor;
    
    private String porcentajeDeRetencion;
    private String porcentajeDeCesion;
    private String idTcMoneda;
    private String formaPago;
    private String folioContratoCuotaParte;
    
    private String estatus;
    private String numeroPlenos;
    private String montoPleno;
    private String folioContratoPrimerExcedente;
    private String reglaNavegacion;
    private String sumaTotalParticipacion;
    
    public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the idTdParticipacion
	 */
	public String getIdTdParticipacion() {
		return idTdParticipacion;
	}
	/**
	 * @param idTdParticipacion the idTdParticipacion to set
	 */
	public void setIdTdParticipacion(String idTdParticipacion) {
		this.idTdParticipacion = idTdParticipacion;
	}    
	/**
	 * @return the idTmContratoCuotaParte
	 */
	public String getIdTmContratoCuotaParte() {
		return idTmContratoCuotaParte;
	}
	/**
	 * @param idTmContratoCuotaParte the idTmContratoCuotaParte to set
	 */
	public void setIdTmContratoCuotaParte(String idTmContratoCuotaParte) {
		this.idTmContratoCuotaParte = idTmContratoCuotaParte;
	}
	/**
	 * @return the tipoParticipante
	 */
	public String getTipoParticipante() {
		return tipoParticipante;
	}
	/**
	 * @param tipoParticipante the tipoParticipante to set
	 */
	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}
	/**
	 * @return the idParticipante
	 */
	public String getIdParticipante() {
		return idParticipante;
	}
	/**
	 * @param idParticipante the idParticipante to set
	 */
	public void setIdParticipante(String idParticipante) {
		this.idParticipante = idParticipante;
	}
	/**
	 * @return the idCuentaPesos
	 */
	public String getIdCuentaPesos() {
		return idCuentaPesos;
	}
	/**
	 * @param idCuentaPesos the idCuentaPesos to set
	 */
	public void setIdCuentaPesos(String idCuentaPesos) {
		this.idCuentaPesos = idCuentaPesos;
	}
	/**
	 * @return the idCuentaDolares
	 */
	public String getIdCuentaDolares() {
		return idCuentaDolares;
	}
	/**
	 * @param idCuentaDolares the idCuentaDolares to set
	 */
	public void setIdCuentaDolares(String idCuentaDolares) {
		this.idCuentaDolares = idCuentaDolares;
	}
	/**
	 * @return the idContacto
	 */
	public String getIdContacto() {
		return idContacto;
	}
	/**
	 * @param idContacto the idContacto to set
	 */
	public void setIdContacto(String idContacto) {
		this.idContacto = idContacto;
	}
	/**
	 * @return the porcentajeParticipacion
	 */
	public String getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	/**
	 * @param porcentajeParticipacion the porcentajeParticipacion to set
	 */
	public void setPorcentajeParticipacion(String porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	/**
	 * @return the comision
	 */
	public String getComision() {
		return comision;
	}
	/**
	 * @param comision the comision to set
	 */
	public void setComision(String comision) {
		this.comision = comision;
	}
	
	public String getPorcentajeDeCesion() {
		return porcentajeDeCesion;
	}
	public void setPorcentajeDeCesion(String porcentajeDeCesion) {
		this.porcentajeDeCesion = porcentajeDeCesion;
	}
	
	public String getPorcentajeDeRetencion() {
		return porcentajeDeRetencion;
	}
	public void setPorcentajeDeRetencion(String porcentajeDeRetencion) {
		this.porcentajeDeRetencion = porcentajeDeRetencion;
	}
	
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	
	public String getFolioContratoCuotaParte() {
		return folioContratoCuotaParte;
	}
	public void setFolioContratoCuotaParte(String folioContratoCuotaParte) {
		this.folioContratoCuotaParte = folioContratoCuotaParte;
	}
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	 public String getIdTmContratoPrimerExcedente() {
			return idTmContratoPrimerExcedente;
	}
	public void setIdTmContratoPrimerExcedente(String idTmContratoPrimerExcedente) {
		this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
	}
	public String getNombreCorredor() {
		return nombreCorredor;
	}
	public void setNombreCorredor(String nombreCorredor) {
		this.nombreCorredor = nombreCorredor;
	}
	public String getNumeroPlenos() {
		return numeroPlenos;
	}
	public void setNumeroPlenos(String numeroPlenos) {
		this.numeroPlenos = numeroPlenos;
	}
	public String getMontoPleno() {
		return montoPleno;
	}
	public void setMontoPleno(String montoPleno) {
		this.montoPleno = montoPleno;
	}
	public String getFolioContratoPrimerExcedente() {
		return folioContratoPrimerExcedente;
	}
	public void setFolioContratoPrimerExcedente(String folioContratoPrimerExcedente) {
		this.folioContratoPrimerExcedente = folioContratoPrimerExcedente;
	}
	public String getReglaNavegacion() {
		return reglaNavegacion;
	}
	public void setReglaNavegacion(String reglaNavegacion) {
		this.reglaNavegacion = reglaNavegacion;
	}
	public void setSumaTotalParticipacion(String sumaTotalParticipacion) {
		this.sumaTotalParticipacion = sumaTotalParticipacion;
	}
	public String getSumaTotalParticipacion() {
		return sumaTotalParticipacion;
	}
	
}
