package mx.com.afirme.midas.contratos.linea;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteSN;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteSN;
import mx.com.afirme.midas.sistema.SystemException;

public class ArbolLinea {
	private Date fechaInicial=null;
	private Date fechaFinal=null;
	private HashMap<String, LineaDTO> Lineas = null;
	private HashMap<String, ContratoCuotaParteDTO> CCPs = null;
	private HashMap<String, ContratoPrimerExcedenteDTO> CPEs = null;
	private HashMap<String, LineaDTO> idlineas = null;
	private HashMap<String, ContratoCuotaParteDTO> idCCP = null;
	private HashMap<String, ContratoPrimerExcedenteDTO> idCPE = null;
	private String nombreUsuario = null;
	
	//Nuevas fechas
	private void setFechaInicial(Date fechaInicial){
		this.fechaInicial = fechaInicial;
	}
	
	private void setFechaFinal(Date fechaFinal){
		this.fechaFinal = fechaFinal;
	}
	
	public ArbolLinea(Date fechaInicial, Date fechaFinal, String nombreUsuario){
		setFechaInicial(fechaInicial);
		setFechaFinal(fechaFinal);
		this.nombreUsuario = nombreUsuario;
		Lineas = new HashMap<String, LineaDTO>();
		CCPs = new HashMap<String, ContratoCuotaParteDTO>();
		CPEs = new HashMap<String, ContratoPrimerExcedenteDTO>();
		idlineas = new HashMap<String, LineaDTO>();
		idCCP = new HashMap<String, ContratoCuotaParteDTO>();
		idCPE = new HashMap<String, ContratoPrimerExcedenteDTO>();
	}
	public List<String> getIdLineas(){
		List<String> list = new ArrayList<String>();
		Entry<String, LineaDTO> entrada = null;
		Iterator<Entry<String, LineaDTO>> it = Lineas.entrySet().iterator();
		
		while (it.hasNext()){
			entrada = (Entry<String, LineaDTO>)it.next();
			list.add((String)entrada.getKey());
		}
		return list;
	}
	
	private boolean validasFechas(){
		if (fechaInicial==null || fechaFinal==null)return false;
		if (fechaFinal.compareTo(fechaInicial)==0 || fechaFinal.after(fechaInicial))return true;
		return false;
	}
	
	public void recorreArbol(ConfiguracionLineaDTO configuracionLineaDTO, String nombreUsuario) 
			throws SystemException{
		if (!validasFechas())return ; 
			LineaDTO lineaDTO = new LineaDTO();
			lineaDTO.setIdTmLinea(configuracionLineaDTO.getIdTmLinea());
			lineaDTO.setFechaInicial(configuracionLineaDTO.getFechaInicial());
			lineaDTO.setFechaFinal(configuracionLineaDTO.getFechaFinal());
			llenaHashs(lineaDTO, nombreUsuario);
			lineaDTO = encuentraLineaDTO(lineaDTO);
			expandeLinea(lineaDTO);			
	}
	
	private void llenaHashs(LineaDTO lineaDTO, String nombreUsuario) throws SystemException{
		List<LineaDTO> listLineaDTO = new ArrayList<LineaDTO>();
		List<ContratoCuotaParteDTO> listCCP = new ArrayList<ContratoCuotaParteDTO>();
		List<ContratoPrimerExcedenteDTO> listCPE = new ArrayList<ContratoPrimerExcedenteDTO>();
		
		try{
			LineaSN lineaSN = new LineaSN();
			ContratoCuotaParteSN contratoCuotaParteSN = new ContratoCuotaParteSN(nombreUsuario);
			ContratoPrimerExcedenteSN contratoPrimerExcedenteSN = new ContratoPrimerExcedenteSN(this.nombreUsuario);
			listLineaDTO = lineaSN.buscarPorFechaInicialFinal(this.fechaInicial,this.fechaFinal);
			listCCP = contratoCuotaParteSN.buscarPorFechaInicialFinal(this.fechaInicial,this.fechaFinal);
			listCPE = contratoPrimerExcedenteSN.buscarPorFechaInicialFinal(this.fechaInicial,this.fechaFinal);
		
			for (LineaDTO linDTO : listLineaDTO) {
				this.Lineas.put(String.valueOf(linDTO.getIdTmLinea().longValue()), linDTO);
			}
			for (ContratoCuotaParteDTO cuotaParteDTO : listCCP) {
				this.CCPs.put(String.valueOf(cuotaParteDTO.getIdTmContratoCuotaParte().longValue()), cuotaParteDTO);
			}
			for (ContratoPrimerExcedenteDTO primerExcedenteDTO: listCPE){
				this.CPEs.put(String.valueOf(primerExcedenteDTO.getIdTmContratoPrimerExcedente().longValue()), primerExcedenteDTO);
			}
		}catch(SystemException e){
			throw e;
		}
	}
	
	private List<LineaDTO> encuentraLineasPorCCP(ContratoCuotaParteDTO contratoCuotaParteDTO){
		List<LineaDTO> lineasDTO = new ArrayList<LineaDTO>();
		LineaDTO linea = new LineaDTO();
		ContratoCuotaParteDTO ccp = null;
		Entry<String, LineaDTO> entrada;
		
		Iterator<Entry<String, LineaDTO>> it = Lineas.entrySet().iterator();
		
		while (it.hasNext()){
			entrada = (Entry<String, LineaDTO>)it.next();
			linea = (LineaDTO)entrada.getValue();
			ccp = linea.getContratoCuotaParte();
			if (ccp!=null){
				if (contratoCuotaParteDTO.getIdTmContratoCuotaParte().longValue() == ccp.getIdTmContratoCuotaParte().longValue()){
					lineasDTO.add(linea);
				}
			}
		}
				
		return lineasDTO;
	}
	
	private List<LineaDTO> encuentraLineasPorCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO){
		List<LineaDTO> lineasDTO = new ArrayList<LineaDTO>();
		LineaDTO linea = new LineaDTO();
		ContratoPrimerExcedenteDTO cpe = null;
		Entry<String, LineaDTO> entrada;
		
		Iterator<Entry<String, LineaDTO>> it = Lineas.entrySet().iterator();
		
		while (it.hasNext()){
			entrada = (Entry<String, LineaDTO>)it.next();
			linea = (LineaDTO)entrada.getValue();
			cpe = linea.getContratoPrimerExcedente();
			if(cpe!=null){
				if (contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().longValue() == cpe.getIdTmContratoPrimerExcedente().longValue()){
					lineasDTO.add(linea);
				}
			}
		}
				
		return lineasDTO;
	}	
	
	private ContratoCuotaParteDTO encuentraCCP(ContratoCuotaParteDTO contratoCuotaParteDTO){
		ContratoCuotaParteDTO cuotaParteDTO= null;
		if (contratoCuotaParteDTO!=null){
			cuotaParteDTO = CCPs.get(contratoCuotaParteDTO.getIdTmContratoCuotaParte().toString());
		}
		return cuotaParteDTO; 
	}
	
	private LineaDTO encuentraLineaDTO(LineaDTO lineaDTO){
		LineaDTO lin = null;
		if (lineaDTO!=null){
			lin = Lineas.get(lineaDTO.getIdTmLinea().toString());
		}
		return lin; 
	}
	
	private ContratoPrimerExcedenteDTO encuentraCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO){
		ContratoPrimerExcedenteDTO primerExcedenteDTO = null;
		if (contratoPrimerExcedenteDTO!=null){
			primerExcedenteDTO = CPEs.get(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().toString());
		}
		return primerExcedenteDTO; 
	}
	
	//Falta buscar la linea en el hash
	private void expandeLinea(LineaDTO lineaDTO){
		lineaDTO = encuentraLineaDTO(lineaDTO);
		ContratoCuotaParteDTO contratoCuotaParteDTO;
		ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO;
		if ((lineaDTO)!=null){
			if (!idlineas.containsKey(lineaDTO.getIdTmLinea().toString())){
				idlineas.put(String.valueOf(lineaDTO.getIdTmLinea().longValue()), lineaDTO);
				lineaDTO.setFechaInicial(fechaInicial);
				lineaDTO.setFechaFinal(fechaFinal);
				
				contratoPrimerExcedenteDTO = encuentraCPE(lineaDTO.getContratoPrimerExcedente());
				contratoCuotaParteDTO = encuentraCCP(lineaDTO.getContratoCuotaParte());
				expandeCCP(contratoCuotaParteDTO);
				expandeCPE(contratoPrimerExcedenteDTO);
			}
		}
	}//expandeLinea
	
	//Falta buscar la linea en el hash
	private void expandeCCP(ContratoCuotaParteDTO contratoCuotaParteDTO){
		if (contratoCuotaParteDTO!=null){
			if (!idCCP.containsKey(contratoCuotaParteDTO.getIdTmContratoCuotaParte().toString())){
				idCCP.put(String.valueOf(contratoCuotaParteDTO.getIdTmContratoCuotaParte().longValue()),contratoCuotaParteDTO);
				contratoCuotaParteDTO.setFechaInicial(fechaInicial);
				contratoCuotaParteDTO.setFechaFinal(fechaFinal);
				List<LineaDTO> list = encuentraLineasPorCCP(contratoCuotaParteDTO);
				if (list!=null){
					for (LineaDTO lineaDTO : list) {
						expandeLinea(lineaDTO);
					}
				}
			}
		}
	}//expandeCCP
	
	//Falta buscar el CPE en el hash
	private void expandeCPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO){
		
		if (contratoPrimerExcedenteDTO!=null){
			if (!idCPE.containsKey(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().toString())){
				idCPE.put(String.valueOf(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().longValue()), contratoPrimerExcedenteDTO);
				contratoPrimerExcedenteDTO.setFechaInicial(fechaInicial);
				contratoPrimerExcedenteDTO.setFechaFinal(fechaFinal);
				List<LineaDTO> list = encuentraLineasPorCPE(contratoPrimerExcedenteDTO);
				for (LineaDTO lineaDTO : list) {
					expandeLinea(lineaDTO);
				}
			}
		}
	}//expandeCCP
}
