package mx.com.afirme.midas.contratos.linea;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Date;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConfiguracionLineaDN {

	private static final ConfiguracionLineaDN INSTANCIA = new ConfiguracionLineaDN();
	private ConfiguracionLineaSN configuracionLineaSN;
	
	public static ConfiguracionLineaDN getInstancia() {
		
		return ConfiguracionLineaDN.INSTANCIA;
	}

	public List<ConfiguracionLineaDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos, SystemException{
	
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.listarTodos();
	}

	public List<ConfiguracionLineaDTO> listarLineaFiltrado(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.listarLineaFiltrado(configuracionLineaDTO);
	}
	
	public List<ConfiguracionLineaDTO> listarFiltrado(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.listarFiltrado(configuracionLineaDTO);
	}

	public List<ConfiguracionLineaDTO> listarFiltradoVigencia(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{

		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.listarFiltradoVigencia(configuracionLineaDTO);
	}
	
	public List<ConfiguracionLineaDTO> listarFiltradoContrato(ConfiguracionLineaDTO configuracionLineaDTO) 
		throws SystemException, ExcepcionDeAccesoADatos{
	
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.listarFiltradoContrato(configuracionLineaDTO);
	}
	
	public ConfiguracionLineaDTO getPorId(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.getPorId(configuracionLineaDTO);
	}
	
	public List<ConfiguracionLineaDTO> getCLineasPorContratosDeCLinea(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.getCLineasPorContratosDeCLinea(configuracionLineaDTO);
	}
	
	public List<ConfiguracionLineaDTO> extenderVigenciaLinea(String idLineas, Date fInicial,Date fFinal)
			throws SystemException, ExcepcionDeAccesoADatos{
		
		LineaDTO lineaDTO = new LineaDTO();
		List<ConfiguracionLineaDTO> configuracionDTOList = new ArrayList<ConfiguracionLineaDTO>();		
		configuracionLineaSN = new ConfiguracionLineaSN();
		LineaSN lineaSN = new LineaSN();
		boolean traslapanFechas = false;
		
		//configuracionLineaDTO = configuracionLineaSN.getPorId(configuracionLineaDTO);
		List<String> idsLinea = new ArrayList<String>();
		String lineas = new String();
		String ccp = new String ();
		StringBuilder contratoCuotaParte = new StringBuilder("");
		String cpe = new String();
		StringBuilder contratoPrimerExcedente = new StringBuilder("");
		StringTokenizer tokenizer = new StringTokenizer(idLineas,",");
		String token = new String();
		boolean tieneCCP = false;
		boolean tieneCPE = false;
		lineas = obtenerIdLineas(tokenizer);
		while (tokenizer.hasMoreTokens()){
			token = (String)tokenizer.nextToken();
			idsLinea.add(token);
				
		}
		configuracionDTOList = configuracionLineaSN.getConfiguracionLineaPorIDLineas(idsLinea);
		if (configuracionDTOList!=null && configuracionDTOList.size()>0){
			for (ConfiguracionLineaDTO conLineaDTO : configuracionDTOList) {
				if (conLineaDTO.getIdTmContratoCuotaParte()!=null){
					contratoCuotaParte.append(conLineaDTO.getIdTmContratoCuotaParte().toString()).append(",");
					tieneCCP = true;
				}
				if (conLineaDTO.getIdTmContratoPrimerExcedente()!=null){
					contratoPrimerExcedente.append(conLineaDTO.getIdTmContratoPrimerExcedente().toString()).append(",");
					tieneCPE = true;
				}
			}
			ccp = contratoCuotaParte.toString();
			cpe = contratoPrimerExcedente.toString();
		}
		if (tieneCCP){
			ccp = ccp.substring(0, ccp.length()-1);
		}else{
			ccp = null;
		}
		
		if (tieneCPE){
			cpe = cpe.substring(0, cpe.length()-1);
		}else{
			cpe = null;
		}
		
		for (String idTmLineaS : idsLinea) {
			lineaDTO = new LineaDTO();
			lineaDTO.setIdTmLinea(new BigDecimal(idTmLineaS));
			lineaDTO = lineaSN.getPorId(lineaDTO);
			lineaDTO.setFechaInicial(fInicial);
			lineaDTO.setFechaFinal(fFinal);
			if (!lineaSN.validaEjercicio(lineaDTO)){
				traslapanFechas = true;
				break;
			}
		}
		
		if (!traslapanFechas)
			lineaSN.actualizaFechasLineasPorIdLineas(lineas, ccp, cpe,fInicial, fFinal);
		return configuracionDTOList;
	}
	

	public String obtenerIdLineas(StringTokenizer tokenizer) {
		String token = new String();
		StringBuilder lineas = new StringBuilder("");
		while (tokenizer.hasMoreTokens()){
			token = (String)tokenizer.nextToken();
			lineas.append(token);
				if (tokenizer.hasMoreTokens())
					lineas.append(",");
		}
		return lineas.toString();
	}
	
	public List<ConfiguracionLineaDTO> getConfiguracionLineaDelArbolLineas(ConfiguracionLineaDTO configuracionLineaDTO, String nombreUsuario)
		throws SystemException, ExcepcionDeAccesoADatos{		
		List<ConfiguracionLineaDTO> list = new ArrayList<ConfiguracionLineaDTO>();
		Date fActual = new Date();
		if (configuracionLineaDTO!=null && configuracionLineaDTO.getIdTmLinea()!=null &&configuracionLineaDTO.getEstatus().intValue()==1){
			if (configuracionLineaDTO.getFechaInicial()!=null && configuracionLineaDTO.getFechaFinal()!=null){
				ArbolLinea arbolLinea = new ArbolLinea(configuracionLineaDTO.getFechaInicial(),configuracionLineaDTO.getFechaFinal(), nombreUsuario);
				arbolLinea.recorreArbol(configuracionLineaDTO, nombreUsuario);
				list = this.configuracionLineaSN.getConfiguracionLineaPorIDLineas(arbolLinea.getIdLineas());
				//verificar validacion con diseniador.
				if ( (configuracionLineaDTO.getFechaInicial().compareTo(fActual)==0 || configuracionLineaDTO.getFechaInicial().before(fActual))&& 
					(configuracionLineaDTO.getFechaFinal().compareTo(fActual)==0 || configuracionLineaDTO.getFechaFinal().after(fActual))){
				}
			}
		}
		return list;		
	}

	//Resibe una cadena de ids separada por comas
	public List<SubRamoDTO> getSubRamosRestantes(String ids, BigDecimal idTcRamo)throws SystemException, ExcepcionDeAccesoADatos{
		configuracionLineaSN = new ConfiguracionLineaSN();
		return configuracionLineaSN.getSubRamosRestantes(ids,idTcRamo);
	}
}
