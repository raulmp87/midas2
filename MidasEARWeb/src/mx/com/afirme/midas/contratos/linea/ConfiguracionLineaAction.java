package mx.com.afirme.midas.contratos.linea;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDN;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDN;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.reaseguro.reportes.ReportesReaseguro;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirPrimaDN;
import net.sf.jasperreports.engine.JRException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ConfiguracionLineaAction extends MidasMappingDispatchAction{
	
	/**
	 * listar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/***
	 * mostrarContratosPorLinea
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public void mostrarContratosPorLinea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
			String idTmLinea = request.getParameter("idTmLinea");
			ConfiguracionLineaDTO configuracionLineaDTO = new ConfiguracionLineaDTO();
			List<ConfiguracionLineaDTO> listaConfiguracionLineaDTO = new ArrayList<ConfiguracionLineaDTO>();
			configuracionLineaDTO.setIdTmLinea(new BigDecimal(idTmLinea.toCharArray()));
			ConfiguracionLineaDN configuracionLineaDN = ConfiguracionLineaDN.getInstancia();
			
			configuracionLineaDTO = configuracionLineaDN.getPorId(configuracionLineaDTO);
			listaConfiguracionLineaDTO = configuracionLineaDN.getConfiguracionLineaDelArbolLineas(configuracionLineaDTO, UtileriasWeb.obtieneNombreUsuario(request));
			
			String json = this.getJson(listaConfiguracionLineaDTO,"ExtenderVigencia");
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	/***
	 * mostrarExtenderVigenciaLinea
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public ActionForward mostrarExtenderVigenciaLinea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm)form;
		String idTmLinea = request.getParameter("idTmLinea");
	
		try{
			ConfiguracionLineaDTO configuracionLineaDTO = new ConfiguracionLineaDTO();
			ConfiguracionLineaDN configuracionLineaDN = ConfiguracionLineaDN.getInstancia();
			
			configuracionLineaForm.setIdTmLinea(idTmLinea);
			configuracionLineaDTO.setIdTmLinea(new BigDecimal(idTmLinea.toCharArray()));
			configuracionLineaDTO = configuracionLineaDN.getPorId(configuracionLineaDTO);
				
			this.poblarForm(configuracionLineaDTO, configuracionLineaForm, request);
			
			request.setAttribute("fechaInicial", UtileriasWeb.getFechaString(configuracionLineaDTO.getFechaInicial()));
			request.setAttribute("horaInicial", UtileriasWeb.getHoraString(configuracionLineaDTO.getFechaInicial()));
			request.setAttribute("fechaFinal", UtileriasWeb.getFechaString(configuracionLineaDTO.getFechaFinal()));
			request.setAttribute("horaFinal", UtileriasWeb.getHoraString(configuracionLineaDTO.getFechaFinal()));
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/***
	 * extenderVigenciaLinea
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public void extenderVigenciaLinea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		ConfiguracionLineaDN configuracionLineaDN = ConfiguracionLineaDN.getInstancia();
		String idTmLineas = request.getParameter("idTmLinea");
		String fInicial = request.getParameter("fi");
		String fFinal = request.getParameter("ff");
		String id = "10";
		String mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratos.linea.vigencia.extender.error");
		Date hoy = new Date();
		try{
			Date fechaFinal = UtileriasWeb.getFechaHoraFromString(fFinal);
			if (fechaFinal.getTime() > hoy.getTime()){
				configuracionLineaDN.extenderVigenciaLinea(idTmLineas, UtileriasWeb.getFechaHoraFromString(fInicial), 
						UtileriasWeb.getFechaHoraFromString(fFinal));
				id = "30";
				mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratos.linea.vigencia.extender.exito");
			}else{
				mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "contratos.linea.vigencia.extender.error.restriccionfecha");
			}
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}finally{
			UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
		}
		
	}
	
	/***
	 * mostrarInicial
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarInicial(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession sesion = request.getSession();		
		sesion.removeAttribute("configuracionLineaForm");
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
		configuracionLineaForm.setParticipacionesCP("");
		configuracionLineaForm.setParticipacionesPE("");
		configuracionLineaForm.setIdTmLinea(null); // Necesario para no confundir la linea nueva con una ya existente
			try{
				configuracionLineaForm.setFechaInicial(configuracionLineaForm.getFechaInicial());
				configuracionLineaForm.setFechaFinal(configuracionLineaForm.getFechaFinal());
			}catch(Exception e){}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/***
	 * mostrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		HttpSession sesion = request.getSession();
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) sesion.getAttribute("configuracionLineaForm");
		if (UtileriasWeb.esObjetoNulo(configuracionLineaForm))
			configuracionLineaForm = new ConfiguracionLineaForm();
		ConfiguracionLineaForm lineaForm = (ConfiguracionLineaForm) form;
		ContratoCuotaParteDTO contratoCuotaParteDTO = (ContratoCuotaParteDTO) request.getAttribute("contratoCP");
		ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = (ContratoPrimerExcedenteDTO) request.getAttribute("contratoPE");
		
		this.poblarForm(contratoCuotaParteDTO, contratoPrimerExcedenteDTO, configuracionLineaForm);
		copiarForm(configuracionLineaForm, lineaForm);

		return mapping.findForward(reglaNavegacion);
		
	}
	
	/***
	 * mostrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarLinea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		String idTmLinea = request.getParameter("idTmLinea");
		String formularioOrigen = request.getParameter("formularioOrigen");
		try{
			ConfiguracionLineaDTO configuracionLineaDTO = new ConfiguracionLineaDTO();
			ConfiguracionLineaDN configuracionLineaDN = ConfiguracionLineaDN.getInstancia();
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			configuracionLineaDTO.setIdTmLinea(new BigDecimal(idTmLinea.toCharArray()));
			configuracionLineaDTO = configuracionLineaDN.getPorId(configuracionLineaDTO);
			this.poblarForm(configuracionLineaDTO, configuracionLineaForm, request);
			request.setAttribute("configuracionLineaDTO", configuracionLineaDTO);
			request.setAttribute("formularioOrigen", formularioOrigen);
			
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * listarFiltrado
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void listarLineaContratoFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		try{
			String fechaInicial = request.getParameter("fi");
			String fechaFinal = request.getParameter("ff");
			String estatus = request.getParameter("estatus");
			String idTcRamo = request.getParameter("idTcRamo");
			
			String fechaActual = new String();
			Date fActual = new Date();
			Date fInicial = new Date();
			Date fFinal = new Date();			
			boolean mostrarTodos = (mostrarTodosPorFecha(fechaInicial, fechaFinal)==2);
			List<ConfiguracionLineaDTO> lineas = null;
			
			if (!mostrarTodos){
				fechaActual = UtileriasWeb.getFechaHoraString(new Date());
				fActual = UtileriasWeb.getFechaHoraFromString(fechaActual);			
				fInicial = UtileriasWeb.getFechaHoraFromString(fechaInicial);
				fFinal = UtileriasWeb.getFechaHoraFromString(fechaFinal);
			}
			if(mostrarTodos ||
				( (fInicial.after(fActual) || fInicial.getTime() == fActual.getTime()) &&
				(fFinal.after(fInicial) || fInicial.getTime() == fFinal.getTime())) ){
				
				ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
				configuracionLineaForm.setFechaInicial(fechaInicial);
				configuracionLineaForm.setFechaFinal(fechaFinal);
				configuracionLineaForm.setEstatus(estatus);
				configuracionLineaForm.setIdTcRamo(idTcRamo);
				
				ConfiguracionLineaDTO  configuracionLineaDTO  = new ConfiguracionLineaDTO();
				ConfiguracionLineaDN configuracionLineaDN 	= ConfiguracionLineaDN.getInstancia();
				this.poblarDTO(configuracionLineaForm, configuracionLineaDTO);			
				lineas = configuracionLineaDN.listarFiltradoContrato(configuracionLineaDTO);
			}
			
			String json = this.getJson(lineas,"Contrato");
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	
	public void listarLineaNegociacionFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		try{
			HashMap<String,String> idsSubRamosMap= new HashMap<String, String>();
			String fechaInicial = request.getParameter("fi");
			String fechaFinal = request.getParameter("ff");
			String estatus = request.getParameter("estatus");
			String idTcRamo = request.getParameter("idTcRamo");
			String idsRamos = new String();
			BigDecimal idRamo = null;
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			configuracionLineaForm.setFechaInicial(fechaInicial);
			configuracionLineaForm.setFechaFinal(fechaFinal);
			configuracionLineaForm.setEstatus(estatus);
			configuracionLineaForm.setIdTcRamo(idTcRamo);
			if (!UtileriasWeb.esCadenaVacia(idTcRamo))idRamo = new BigDecimal(idTcRamo);
			
			ConfiguracionLineaDTO  configuracionLineaDTO  = new ConfiguracionLineaDTO();
			ConfiguracionLineaDN configuracionLineaDN 	= ConfiguracionLineaDN.getInstancia();
			this.poblarDTO(configuracionLineaForm, configuracionLineaDTO);			
			List<ConfiguracionLineaDTO> lineas = configuracionLineaDN.listarLineaFiltrado(configuracionLineaDTO);
			
			for (ConfiguracionLineaDTO configuracionLinea : lineas) {
				idsSubRamosMap.put(configuracionLinea.getIdTcSubRamo().toString(), configuracionLinea.getIdTcSubRamo().toString());
			}
			configuracionLineaForm.setFechaInicial(fechaInicial);
			configuracionLineaForm.setFechaFinal(fechaFinal);
			this.poblarDTO(configuracionLineaForm, configuracionLineaDTO);
			List<ConfiguracionLineaDTO> lineasVigentes = configuracionLineaDN.listarFiltradoVigencia(configuracionLineaDTO);
			for (ConfiguracionLineaDTO configuracionLinea : lineasVigentes) {
				idsSubRamosMap.put(configuracionLinea.getIdTcSubRamo().toString(), configuracionLinea.getIdTcSubRamo().toString());
			}
			Iterator<Entry<String, String>> it = idsSubRamosMap.entrySet().iterator();
			idsRamos = obtenerIdsRamos(it);
			List<SubRamoDTO> subramos = configuracionLineaDN.getSubRamosRestantes(idsRamos, idRamo);
			
			String json = this.getJson(lineas,"Negociacion");
			json = this.agregaLineasNegociacionParaRamosRestantes(json, subramos);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}	
	}
	
	public String obtenerIdsRamos(Iterator<Entry<String, String>> it) {
		StringBuilder idsRamos = new StringBuilder();
		Entry<String, String> entrada = null;
		while (it.hasNext()){
			entrada = (Entry<String, String>)it.next();
			idsRamos.append((((String)entrada.getKey()) + ","));
		}
		 idsRamos.append("0");
		return idsRamos.toString();
	}

	public void listarLineaVigenciaFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try{
			String fechaInicial = request.getParameter("fi");
			String fechaFinal = request.getParameter("ff");
			String estatus = request.getParameter("estatus");
			String idTcRamo = request.getParameter("idTcRamo");
			
			String fechaActual = new String();
			Date fActual = new Date();
			Date fInicial = new Date();
			Date fFinal = new Date();
			boolean mostrarTodos = (mostrarTodosPorFecha(fechaInicial, fechaFinal)==2);
			List<ConfiguracionLineaDTO> lineas = null;
			
			if (!mostrarTodos){
				fechaActual = UtileriasWeb.getFechaHoraString(new Date());
				fActual = UtileriasWeb.getFechaHoraFromString(fechaActual);			
				fInicial = UtileriasWeb.getFechaHoraFromString(fechaInicial);
				fFinal = UtileriasWeb.getFechaHoraFromString(fechaFinal);
			}
			if(mostrarTodos ||
				( (fActual.getTime() == fInicial.getTime() || fInicial.before(fActual)) && 
				(fFinal.after(fInicial) || fInicial.getTime() == fFinal.getTime())) ){
				ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
				configuracionLineaForm.setFechaInicial(fechaInicial);
				configuracionLineaForm.setFechaFinal(fechaFinal);
				configuracionLineaForm.setIdTcRamo(idTcRamo);
				configuracionLineaForm.setEstatus(estatus);
				
				ConfiguracionLineaDTO  configuracionLineaDTO  = new ConfiguracionLineaDTO();
				ConfiguracionLineaDN configuracionLineaDN 	= ConfiguracionLineaDN.getInstancia();
				this.poblarDTO(configuracionLineaForm, configuracionLineaDTO);			
				lineas = configuracionLineaDN.listarFiltradoVigencia(configuracionLineaDTO);
			}
			
			String json = this.getJson(lineas,"Vigencia");
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}	
	}
	
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession sesion = request.getSession();		
		sesion.removeAttribute("configuracionLineaForm");
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
		ConfiguracionLineaDTO  configuracionLineaDTO  = new ConfiguracionLineaDTO();
		ConfiguracionLineaDN   configuracionLineaDN 	= ConfiguracionLineaDN.getInstancia();

		try {
			//Configura el form con los datos de la linea
			String idParam = request.getParameter("id");
			configuracionLineaDTO.setIdTmLinea(new BigDecimal(idParam));
			configuracionLineaDTO = configuracionLineaDN.getPorId(configuracionLineaDTO);
			this.poblarForm(configuracionLineaDTO, configuracionLineaForm, request);
			LineaDTO lineaDTO = new LineaDTO();
			lineaDTO.setIdTmLinea(new BigDecimal(idParam));
			configuracionLineaForm.setModoDistribucion(LineaDN.getInstancia().getPorId(lineaDTO).getTipoDistribucion().toString());
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	public void desasociarContratoCPLinea(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		String id = "10";
		String mensaje = "El contrato no pudo desasociarse correctamente";
		try {
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			LineaDTO lineaDTO;
			if (configuracionLineaForm.getIdTmLinea().length() > 0){
				lineaDTO = new LineaDTO();
				lineaDTO.setIdTmLinea(new BigDecimal(configuracionLineaForm.getIdTmLinea()));
				lineaDTO = LineaDN.getInstancia().getPorId(lineaDTO);
				if (!UtileriasWeb.esObjetoNulo(lineaDTO)){
					LineaDN.getInstancia().desasociarContratoCPLinea(lineaDTO);
				}
				id = "30";
				mensaje = "El contrato se desasoci&oacute; correctamente";
			}else{
				id = "30";
				mensaje = "El contrato se desasoci&oacute; correctamente";
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} finally{
			UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
		}
	}
	
	public void desasociarContratoPELinea(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		String id = "10";
		String mensaje = "El contrato no pudo desasociarse correctamente";
		try {
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			LineaDTO lineaDTO;
			if (configuracionLineaForm.getIdTmLinea().length() > 0){
				lineaDTO = new LineaDTO();
				lineaDTO.setIdTmLinea(new BigDecimal(configuracionLineaForm.getIdTmLinea()));
				lineaDTO = LineaDN.getInstancia().getPorId(lineaDTO);
				if (!UtileriasWeb.esObjetoNulo(lineaDTO)){
					LineaDN.getInstancia().desasociarContratoPELinea(lineaDTO);
				}
				id = "30";
				mensaje = "El contrato se desasoci&oacute; correctamente";
			}else{
				id = "30";
				mensaje = "El contrato se desasoci&oacute; correctamente";
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} finally{
			UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
		}
	}
	
	/**
	 * guardarSesion
	 * @param configuracionLineaDTO
	 * @param configuracionLineaForm
	 * @throws SystemException
	 */
	public ActionForward guardarSesion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse respone){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
		HttpSession sesion = request.getSession(); //configuracionLineaForm.setFolioContratoCuotaParte("65756");
		sesion.setAttribute("configuracionLineaForm", configuracionLineaForm);

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * poblarForm
	 * @param configuracionLineaDTO
	 * @param configuracionLineaForm
	 * @throws SystemException
	 */
	private void poblarForm(ConfiguracionLineaDTO configuracionLineaDTO, ConfiguracionLineaForm configuracionLineaForm, HttpServletRequest request) throws SystemException{
		
		if (configuracionLineaDTO.getIdTmLinea()!=null)
			configuracionLineaForm.setIdTmLinea(configuracionLineaDTO.getIdTmLinea().toString());
		else
			configuracionLineaForm.setIdTmLinea(null);
			
		if(configuracionLineaDTO.getCapacidadMaximaLinea()!= null)
			configuracionLineaForm.setCapacidadMaximaLinea(configuracionLineaDTO.getCapacidadMaximaLinea().toString());
		
		if(configuracionLineaDTO.getCesionCuotaParte()!= null)
			configuracionLineaForm.setCesionCuotaParte(configuracionLineaDTO.getCesionCuotaParte().toString());
		
		if(configuracionLineaDTO.getCesionPrimerExcedente()!= null)
			configuracionLineaForm.setCesionPrimerExcedente(configuracionLineaDTO.getCesionPrimerExcedente().toString());
		
		if(configuracionLineaDTO.getContratoCuotaParte()!= null){
			configuracionLineaForm.setContratoCuotaParte(configuracionLineaDTO.getContratoCuotaParte());
			configuracionLineaForm.setFolioContratoCuotaParte(configuracionLineaDTO.getContratoCuotaParte());
			if (configuracionLineaDTO.getIdTmContratoCuotaParte() != null){
				ContratoCuotaParteDTO contratoCuotaParteDTO = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(configuracionLineaDTO.getIdTmContratoCuotaParte());
				configuracionLineaForm.setEstatusContratoCuotaParte(contratoCuotaParteDTO.getEstatus().toBigInteger().toString());
			}else{
				configuracionLineaForm.setEstatusContratoCuotaParte("0");
			}
		}
		
		if(configuracionLineaDTO.getContratoPrimerExcedente()!= null){
			configuracionLineaForm.setContratoPrimerExcedente(configuracionLineaDTO.getContratoPrimerExcedente());
			configuracionLineaForm.setFolioContratoPrimerExcedente(configuracionLineaDTO.getContratoPrimerExcedente());
			if (configuracionLineaDTO.getIdTmContratoPrimerExcedente() != null){
				ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).getPorId(configuracionLineaDTO.getIdTmContratoPrimerExcedente());
				configuracionLineaForm.setEstatusContratoPrimerExcedente(contratoPrimerExcedenteDTO.getEstatus().toBigInteger().toString());
			}else{
				configuracionLineaForm.setEstatusContratoPrimerExcedente("0");
			}
		}
		
		if(configuracionLineaDTO.getEstatus()!= null)
			configuracionLineaForm.setEstatus(configuracionLineaDTO.getEstatus().toString());
		
		if(configuracionLineaDTO.getFechaFinal()!= null){
			Date fecha = configuracionLineaDTO.getFechaFinal();
			String fechaStr = UtileriasWeb.getFechaHoraString(fecha);
			configuracionLineaForm.setFechaFinal(fechaStr);
		}
		
		if(configuracionLineaDTO.getFechaInicial()!= null){
			Date fecha = configuracionLineaDTO.getFechaInicial();
			String fechaStr = UtileriasWeb.getFechaHoraString(fecha);
			configuracionLineaForm.setFechaInicial(fechaStr);
		}
				
		if(configuracionLineaDTO.getIdTcRamo()!= null){
			configuracionLineaForm.setIdTcRamo(configuracionLineaDTO.getIdTcRamo().toString());
		}
		
		if(configuracionLineaDTO.getIdTcSubRamo()!= null)
			configuracionLineaForm.setIdTcSubRamo(configuracionLineaDTO.getIdTcSubRamo().toString());
		
		if(configuracionLineaDTO.getIdTmContratoCuotaParte()!= null)
			configuracionLineaForm.setIdTmContratoCuotaParte(configuracionLineaDTO.getIdTmContratoCuotaParte().toString());
		
		if(configuracionLineaDTO.getIdTmContratoPrimerExcedente()!= null)
			configuracionLineaForm.setIdTmContratoPrimerExcedente(configuracionLineaDTO.getIdTmContratoPrimerExcedente().toString());
		
		if(configuracionLineaDTO.getIdTmLinea()!= null)
			configuracionLineaForm.setIdTmLinea(configuracionLineaDTO.getIdTmLinea().toString());
		
		if(configuracionLineaDTO.getMaximo()!= null)
			configuracionLineaForm.setMaximo(new BigDecimal(configuracionLineaDTO.getMaximo()) + "");
		
		if(configuracionLineaDTO.getModoDistribucion()!= null)
			configuracionLineaForm.setModoDistribucion(configuracionLineaDTO.getModoDistribucion().toString());
		
		if(configuracionLineaDTO.getMontoDelPleno()!= null)
			configuracionLineaForm.setMontoPleno(configuracionLineaDTO.getMontoDelPleno().toString());
		
		if(configuracionLineaDTO.getNumeroDePlenos()!= null)
			configuracionLineaForm.setNumeroPlenos(configuracionLineaDTO.getNumeroDePlenos().toString());
		
		if(configuracionLineaDTO.getPorcentajeDeCesion()!= null)
			configuracionLineaForm.setPorcentajeDeCesion(configuracionLineaDTO.getPorcentajeDeCesion().toString());
		
		if(configuracionLineaDTO.getRamo()!= null)
			configuracionLineaForm.setRamo(configuracionLineaDTO.getRamo().toString());
		
		if(configuracionLineaDTO.getSubRamo()!= null)
			configuracionLineaForm.setSubRamo(configuracionLineaDTO.getSubRamo().toString());
		
		if(configuracionLineaDTO.getTotalCedido()!= null)
			configuracionLineaForm.setTotalCedido(configuracionLineaDTO.getTotalCedido().toString());
		
		if(configuracionLineaDTO.getTotalRetencion()!= null)
			configuracionLineaForm.setTotalRetencion(configuracionLineaDTO.getTotalRetencion().toString());
	}
	
	/**
	 * poblarForm
	 * @param configuracionLineaDTO
	 * @param configuracionLineaForm
	 * @throws SystemException
	 */
	private void poblarForm(ContratoCuotaParteDTO contratoCuotaParteDTO, ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,
			ConfiguracionLineaForm configuracionLineaForm){
		
		if (UtileriasWeb.esObjetoNulo(configuracionLineaForm))
			configuracionLineaForm = new ConfiguracionLineaForm();
		
		if(contratoCuotaParteDTO!=null){
			if(contratoCuotaParteDTO.getFolioContrato()!=null)
				configuracionLineaForm.setFolioContratoCuotaParte(contratoCuotaParteDTO.getFolioContrato().toString());
			
			if(contratoCuotaParteDTO.getIdTmContratoCuotaParte()!=null)
				configuracionLineaForm.setIdTmContratoCuotaParte(
						contratoCuotaParteDTO.getIdTmContratoCuotaParte().toString());
			
			if(contratoCuotaParteDTO.getPorcentajeCesion()!=null){				
				String porcentajeCesion = contratoCuotaParteDTO.getPorcentajeCesion().toString();				
				configuracionLineaForm.setPorcentajeDeCesion(porcentajeCesion);				
			}
			
			if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getMaximo()) && 
					contratoCuotaParteDTO.getPorcentajeCesion()!=null){
				double maximo = Double.valueOf(configuracionLineaForm.getMaximo());
				double porcentajeCesion = contratoCuotaParteDTO.getPorcentajeCesion();
				double cesionCuotaParte = (porcentajeCesion/100)*maximo;
				double totalRetencion = maximo - cesionCuotaParte;
				configuracionLineaForm.setCesionCuotaParte(
						MessageFormat.format("{0,number,#.##}", cesionCuotaParte));
				configuracionLineaForm.setTotalRetencion(
						MessageFormat.format("{0,number,#.##}", totalRetencion));
				
			}
			configuracionLineaForm.setEstatusContratoCuotaParte(contratoCuotaParteDTO.getEstatus().toBigInteger().toString());
		}else{
			if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getMaximo()) && !UtileriasWeb.esCadenaVacia(configuracionLineaForm.getPorcentajeDeCesion())){
				configuracionLineaForm.setMaximo(configuracionLineaForm.getMaximo().replaceAll(",", ""));
				if (configuracionLineaForm.getMaximo().contains("$"))
					configuracionLineaForm.setMaximo(configuracionLineaForm.getMaximo().substring(1, configuracionLineaForm.getMaximo().length()));
				
				double maximo = Double.valueOf(configuracionLineaForm.getMaximo());
				double porcentajeCesion = new Double(configuracionLineaForm.getPorcentajeDeCesion()).doubleValue();
				double cesionCuotaParte = (porcentajeCesion/100)*maximo;
				double totalRetencion = maximo - cesionCuotaParte;
				configuracionLineaForm.setCesionCuotaParte(
						MessageFormat.format("{0,number,#.##}", cesionCuotaParte));
				configuracionLineaForm.setTotalRetencion(
						MessageFormat.format("{0,number,#.##}", totalRetencion));
				
			}
		}
		
		if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO)){
			
			if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()))
				configuracionLineaForm.setIdTmContratoPrimerExcedente(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente().toBigInteger().toString());
			else
				configuracionLineaForm.setIdTmContratoPrimerExcedente(null);
			
			if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getFolioContrato()))
				configuracionLineaForm.setFolioContratoPrimerExcedente(contratoPrimerExcedenteDTO.getFolioContrato());
			//else
				//configuracionLineaForm.setFolioContratoPrimerExcedente(null);
			
			if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getMontoPleno()))
				configuracionLineaForm.setMontoPleno(contratoPrimerExcedenteDTO.getMontoPleno().toString());
			else
				configuracionLineaForm.setMontoPleno(null);
			
			if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getNumeroPlenos()))
				configuracionLineaForm.setNumeroPlenos(contratoPrimerExcedenteDTO.getNumeroPlenos().toString());
			else
				configuracionLineaForm.setNumeroPlenos(null);
			
			double cesionPrimerExcedente = 0;
			if (!UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getMontoPleno()) && !UtileriasWeb.esObjetoNulo(contratoPrimerExcedenteDTO.getNumeroPlenos()))
				cesionPrimerExcedente = contratoPrimerExcedenteDTO.getMontoPleno().doubleValue() * contratoPrimerExcedenteDTO.getNumeroPlenos().doubleValue();
			
			configuracionLineaForm.setCesionPrimerExcedente(String.valueOf(cesionPrimerExcedente));
			
			double capacidadMaximaLinea = 0;
			double maximo = 0;
			if (configuracionLineaForm.getMaximo() != null && configuracionLineaForm.getMaximo().length() > 0)
				maximo = new Double(configuracionLineaForm.getMaximo()).doubleValue();
			
			capacidadMaximaLinea = maximo + cesionPrimerExcedente;
			configuracionLineaForm.setCapacidadMaximaLinea(String.valueOf(capacidadMaximaLinea));
			
			double porcentajeDeCesion = 0; 
			if (!UtileriasWeb.esObjetoNulo(configuracionLineaForm.getPorcentajeDeCesion()) && configuracionLineaForm.getPorcentajeDeCesion().length() > 0)
				porcentajeDeCesion = new Double(configuracionLineaForm.getPorcentajeDeCesion()).doubleValue()/100;
			
			double totalRetencion = maximo - (porcentajeDeCesion * maximo);			
			configuracionLineaForm.setTotalRetencion(String.valueOf(totalRetencion));
			
			double totalCedido = capacidadMaximaLinea - totalRetencion;
			configuracionLineaForm.setTotalCedido(String.valueOf(totalCedido));
			
			configuracionLineaForm.setEstatusContratoPrimerExcedente(contratoPrimerExcedenteDTO.getEstatus().toBigInteger().toString());
		}
						
	}


	/**
	 * poblarDTO
	 * @param configuracionLineaForm
	 * @param configuracionLineaDTO
	 * @throws SystemException
	 */
	private void poblarDTO(ConfiguracionLineaForm configuracionLineaForm, ConfiguracionLineaDTO configuracionLineaDTO) 
	throws SystemException, ParseException{
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTmLinea()))
			configuracionLineaDTO.setIdTmLinea(new BigDecimal(configuracionLineaForm.getIdTmLinea()));
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getEstatus()))
			configuracionLineaDTO.setEstatus(new BigDecimal(configuracionLineaForm.getEstatus()));
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaInicial())){
			String fechaStr = configuracionLineaForm.getFechaInicial();
			Date fecha = UtileriasWeb.getFechaHoraFromString(fechaStr);
			configuracionLineaDTO.setFechaInicial(fecha);
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaFinal())){
			String fechaStr = configuracionLineaForm.getFechaFinal();
			Date fecha = UtileriasWeb.getFechaHoraFromString(fechaStr);
			configuracionLineaDTO.setFechaFinal(fecha);
		}
					
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTcRamo()))
			configuracionLineaDTO.setIdTcRamo(new BigDecimal(configuracionLineaForm.getIdTcRamo()));
	}
	
		
	/**
	 * copiarForm
	 * @param configuracionLineaForm
	 * @param configuracionLineaForm
	 * @throws SystemException
	 */
	private void copiarForm(ConfiguracionLineaForm configuracionLineaForm1, ConfiguracionLineaForm configuracionLineaForm2){
		configuracionLineaForm2.setAutorizar(configuracionLineaForm1.getAutorizar());
		configuracionLineaForm2.setCapacidadMaximaLinea(configuracionLineaForm1.getCapacidadMaximaLinea());
		configuracionLineaForm2.setCesionCuotaParte(configuracionLineaForm1.getCesionCuotaParte());
		configuracionLineaForm2.setCesionPrimerExcedente(configuracionLineaForm1.getCesionPrimerExcedente());
		configuracionLineaForm2.setContratoCuotaParte(configuracionLineaForm1.getContratoCuotaParte());
		configuracionLineaForm2.setContratoPrimerExcedente(configuracionLineaForm1.getContratoPrimerExcedente());
		configuracionLineaForm2.setEstatus(configuracionLineaForm1.getEstatus());
		configuracionLineaForm2.setFechaAutorizacion(configuracionLineaForm1.getFechaAutorizacion());
		configuracionLineaForm2.setFechaFinal(configuracionLineaForm1.getFechaFinal());
		configuracionLineaForm2.setFechaInicial(configuracionLineaForm1.getFechaInicial());
		configuracionLineaForm2.setFolioContratoCuotaParte(configuracionLineaForm1.getFolioContratoCuotaParte());
		configuracionLineaForm2.setIdTcRamo(configuracionLineaForm1.getIdTcRamo());
		configuracionLineaForm2.setIdTcSubRamo(configuracionLineaForm1.getIdTcSubRamo());
		configuracionLineaForm2.setIdTmContratoCuotaParte(configuracionLineaForm1.getIdTmContratoCuotaParte());
		configuracionLineaForm2.setIdTmContratoPrimerExcedente(configuracionLineaForm1.getIdTmContratoPrimerExcedente());
		configuracionLineaForm2.setIdTmLinea(configuracionLineaForm1.getIdTmLinea());
		configuracionLineaForm2.setMaximo(configuracionLineaForm1.getMaximo());
		configuracionLineaForm2.setModoDistribucion(configuracionLineaForm1.getModoDistribucion());
		configuracionLineaForm2.setMontoPleno(configuracionLineaForm1.getMontoPleno());
		configuracionLineaForm2.setNumeroPlenos(configuracionLineaForm1.getNumeroPlenos());
		configuracionLineaForm2.setPorcentajeDeCesion(configuracionLineaForm1.getPorcentajeDeCesion());
		configuracionLineaForm2.setRamo(configuracionLineaForm1.getRamo());
		configuracionLineaForm2.setSubRamo(configuracionLineaForm1.getSubRamo());
		configuracionLineaForm2.setTotalCedido(configuracionLineaForm1.getTotalCedido());
		configuracionLineaForm2.setTotalRetencion(configuracionLineaForm1.getTotalRetencion());
		configuracionLineaForm2.setUsuarioAutorizo(configuracionLineaForm1.getUsuarioAutorizo());
		configuracionLineaForm2.setParticipacionesCP(configuracionLineaForm1.getParticipacionesCP());
		configuracionLineaForm2.setParticipacionesPE(configuracionLineaForm1.getParticipacionesPE());
		// Guarda informaci�n referente a contrato primer excedente
		configuracionLineaForm2.setMontoPleno(configuracionLineaForm1.getMontoPleno());
		configuracionLineaForm2.setNumeroPlenos(configuracionLineaForm1.getNumeroPlenos());
		configuracionLineaForm2.setFolioContratoPrimerExcedente(configuracionLineaForm1.getFolioContratoPrimerExcedente());
		configuracionLineaForm2.setEstatusContratoCuotaParte(configuracionLineaForm1.getEstatusContratoCuotaParte());
		configuracionLineaForm2.setEstatusContratoPrimerExcedente(configuracionLineaForm1.getEstatusContratoPrimerExcedente());
	}
	
	public String agregaLineasNegociacionParaRamosRestantes(String json, List<SubRamoDTO> subramos){
		StringBuilder newJson = new StringBuilder();
		StringBuilder newRow = new StringBuilder();
		SubRamoDTO subRamoDTO = new SubRamoDTO();
		newJson.append(json.substring(0, json.length() -2));
		for (int i=0;i<subramos.size();i++){
			newRow = new StringBuilder();
			subRamoDTO = subramos.get(i);
			if (!(json.equals("{rows:[]}") && i==0))
				newRow.append(",");
			newRow.append("{id:-").append(String.valueOf(i+1)).append(",data:[") ;
			newRow.append("0,,");
			newRow.append("\"").append(subRamoDTO.getRamoDTO().getDescripcion()).append("\",\"").append(subRamoDTO.getDescripcionSubRamo()).append("\"");
			newRow .append(",\"\",\"\",\"\",\"\",\"\",\"\",\"\"");
			newRow.append(",\"\",\"\",\"\",\"\",\"\",\"\"");//campos vacios
			newRow.append(",\"/MidasWeb/img/blank.gif\"");
			newRow.append(",\"/MidasWeb/img/blank.gif\"");
			newRow.append(",\"/MidasWeb/img/blank.gif\"");
			newRow.append(",\"/MidasWeb/img/blank.gif\"");
			newRow.append(",").append(subRamoDTO.getRamoDTO().getIdTcRamo().toString()).append(",").append(subRamoDTO.getIdTcSubRamo().toString());//2 campos ocultos;
			newRow.append("]}");
			newJson.append(newRow.toString());
		}
		
		newJson.append("]}");		
		return newJson.toString();
	}
	
	//Si se modifican la cantidad de registros se debe de modificar la funcion agregaLineasNegociacionParaRamosRestantes
	//ya que trabaja con 2 columnas ocultas
	private String getJson(List<ConfiguracionLineaDTO> lineas, String nombreFormulario) 
	throws ExcepcionDeAccesoADatos, SystemException{
		
		int idFormulario = 0;
		if (nombreFormulario.equalsIgnoreCase("Contrato"))idFormulario = 1;
		else if(nombreFormulario.equalsIgnoreCase("Negociacion"))idFormulario=2;
		else if(nombreFormulario.equalsIgnoreCase("Vigencia"))idFormulario=3;
		else if(nombreFormulario.equalsIgnoreCase("ExtenderVigencia"))idFormulario=4;
		
		MidasJsonBase json = new MidasJsonBase();
		if(lineas != null && lineas.size() > 0) {
			for(ConfiguracionLineaDTO linea : lineas) {						
				
				String iconoContratoCuotaParte = "/MidasWeb/img/ico_Doc_ok.gif^ConsultarCP^javascript: sendRequestMostrarDetalleContratos(7,"+linea.getIdTmContratoCuotaParte()+",configuracionLineaForm,"+linea.getIdTcRamo().toString()+","+idFormulario+");^_self";
				String iconoContratoPrimerExcedente = "/MidasWeb/img/ico_Doc_ok.gif^ConsultarPE^javascript: sendRequestMostrarDetalleContratos(8,"+linea.getIdTmContratoPrimerExcedente()+",configuracionLineaForm,"+linea.getIdTcRamo().toString()+","+idFormulario+");^_self";
				
				if (linea.getIdTmContratoCuotaParte()==null) {
					iconoContratoCuotaParte = "/MidasWeb/img/ico_Doc_ok.gif^ConsultarCP^javascript: sendRequestContrato(-7,"+linea.getIdTmContratoCuotaParte()+",configuracionLineaForm);^_self";
				}
				if (linea.getIdTmContratoPrimerExcedente()==null) {
					iconoContratoPrimerExcedente = "/MidasWeb/img/ico_Doc_ok.gif^ConsultarPE^javascript: sendRequestContrato(-8,"+linea.getIdTmLinea()+","+idFormulario+");^_self";
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(linea.getIdTmLinea().toString());
				row.setDatos(
						"0",
						UtileriasWeb.getFechaHoraString(linea.getFechaInicial()) + " al " + UtileriasWeb.getFechaHoraString(linea.getFechaFinal()),
						linea.getRamo(),
						linea.getSubRamo(),
						linea.getModoDistribucion(),
						(linea.getContratoCuotaParte().equals("0")? "":linea.getContratoCuotaParte()),
						linea.getPorcentajeDeCesion() + "%",
						NumberFormat.getCurrencyInstance().format(linea.getCesionCuotaParte()),
						NumberFormat.getCurrencyInstance().format(linea.getMaximo()),
						(linea.getContratoPrimerExcedente().equals("0")? "":linea.getContratoPrimerExcedente()),
						NumberFormat.getCurrencyInstance().format(linea.getMontoDelPleno()),
						linea.getNumeroDePlenos().toString(),
						NumberFormat.getCurrencyInstance().format(linea.getCesionPrimerExcedente()),
						"<b>" + NumberFormat.getCurrencyInstance().format(linea.getCapacidadMaximaLinea()) + "</b>",
						NumberFormat.getCurrencyInstance().format(linea.getTotalRetencion()),
						NumberFormat.getCurrencyInstance().format(linea.getTotalCedido()),
						"/MidasWeb/img/details.gif^Consultar^javascript: sendRequestLineaContrato(1,"+linea.getIdTmLinea()+","+idFormulario+");^_self",
						"/MidasWeb/img/Edit14.gif^Modificar^javascript: sendRequestLineaContrato(2,"+linea.getIdTmLinea()+","+idFormulario+");^_self",
						"/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestLineaContrato(3,"+linea.getIdTmLinea()+","+idFormulario+");^_self",
						iconoContratoCuotaParte,
						iconoContratoPrimerExcedente
				);
				json.addRow(row);
			}
		}
		
		return json.toString();
	}	
	
	/***
	 * listarTodos
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		ConfiguracionLineaDN configuracionLineaDN = ConfiguracionLineaDN.getInstancia();
		List<ConfiguracionLineaDTO> configuracionLineas = configuracionLineaDN.listarTodos();
		request.setAttribute("configuracionLineas", configuracionLineas);
	}
	
	private int mostrarTodosPorFecha(String fechaInicial,String fechaFinal){
		int resultado = 0;
			if (fechaInicial!=null && fechaInicial.trim().length()==0){
				resultado+=1;
			}
			if (fechaFinal!=null && fechaFinal.trim().length()==0){
				resultado+=1;
			}
		return resultado;
	}
	
	public void rptLineasContratos(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = request.getParameter("tipoReporte");
		String estatus = request.getParameter("estatus");
		
		Calendar calendario = Calendar.getInstance();
		//calendario.set(1900, 01, 01); // 01/01/1900
		
		Integer intEstatus = Integer.valueOf(estatus);
		
		parametros.put("pLogoUrl", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.imagenes"));
		parametros.put("pSubReportDir", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.linea"));
		parametros.put("pEstatus", intEstatus);
		parametros.put("pFecha", calendario.getTime());
		ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
		if (tipoReporte.equalsIgnoreCase("XLS"))
			tipoReporte = Sistema.TIPO_XLS;
		else
			tipoReporte = Sistema.TIPO_PDF;
		try {
			reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.rptLineasContratos"), parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
		} catch (Exception e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}

	public ActionForward distribucionMasiva(ActionMapping mapping, 
			ActionForm form,HttpServletRequest request, HttpServletResponse response){
		TemporizadorDistribuirPrimaDN temporizadorDistribuirPrimaDN = TemporizadorDistribuirPrimaDN.getInstancia();
		temporizadorDistribuirPrimaDN.iniciar();
		return null;
	}	
	
}
