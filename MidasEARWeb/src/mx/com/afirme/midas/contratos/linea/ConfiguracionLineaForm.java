package mx.com.afirme.midas.contratos.linea;


import org.apache.struts.validator.ValidatorForm;

public class ConfiguracionLineaForm  extends ValidatorForm{
	
	private static final long serialVersionUID = 3642907366618335740L;
	
	private String idTmLinea;
    private String fechaInicial;
    private String fechaFinal;
    private String ramo;
    private String subRamo;
    private String modoDistribucion;
    private String contratoCuotaParte;
    private String porcentajeDeCesion;
    private String cesionCuotaParte;
    private String maximo;
    private String contratoPrimerExcedente;
    private String montoPleno;
    private String numeroPlenos;
    private String cesionPrimerExcedente;
    private String capacidadMaximaLinea;
    private String totalRetencion;
    private String totalCedido;
    private String idTcRamo;
    private String idTcSubRamo;
    private String idTmContratoCuotaParte;
    private String idTmContratoPrimerExcedente;
    private String estatus;
    
    private String usuarioAutorizo;
    private String fechaAutorizacion;
    
    private String folioContratoCuotaParte;
    private String estatusContratoCuotaParte;
    private String folioContratoPrimerExcedente;
    private String estatusContratoPrimerExcedente;
    
    private String autorizar;
        
    private String participacionesCP;
    private String participacionesPE;
    
    private String seleccionado;
    private String mensajeAutorizar;
    private String idtcReaseguradorCorredor;
	private String tipoParticipante;
    
	public String getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(String seleccionado) {
		this.seleccionado = seleccionado;
	}
	/**
	 * @return the idTmLinea
	 */
	public String getIdTmLinea() {
		return idTmLinea;
	}
	/**
	 * @param idTmLinea the idTmLinea to set
	 */
	public void setIdTmLinea(String idTmLinea) {
		this.idTmLinea = idTmLinea;
	}
	/**
	 * @return the fechaInicial
	 */
	public String getFechaInicial() {
		return fechaInicial;
	}
	/**
	 * @param fechaInicial the fechaInicial to set
	 */
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	/**
	 * @return the fechaFinal
	 */
	public String getFechaFinal() {
		return fechaFinal;
	}
	/**
	 * @param fechaFinal the fechaFinal to set
	 */
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	/**
	 * @return the ramo
	 */
	public String getRamo() {
		return ramo;
	}
	/**
	 * @param ramo the ramo to set
	 */
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	/**
	 * @return the subRamo
	 */
	public String getSubRamo() {
		return subRamo;
	}
	/**
	 * @param subRamo the subRamo to set
	 */
	public void setSubRamo(String subRamo) {
		this.subRamo = subRamo;
	}
	/**
	 * @return the modoDistribucion
	 */
	public String getModoDistribucion() {
		return modoDistribucion;
	}
	/**
	 * @param modoDistribucion the modoDistribucion to set
	 */
	public void setModoDistribucion(String modoDistribucion) {
		this.modoDistribucion = modoDistribucion;
	}
	/**
	 * @return the contratoCuotaParte
	 */
	public String getContratoCuotaParte() {
		return contratoCuotaParte;
	}
	/**
	 * @param contratoCuotaParte the contratoCuotaParte to set
	 */
	public void setContratoCuotaParte(String contratoCuotaParte) {
		this.contratoCuotaParte = contratoCuotaParte;
	}
	/**
	 * @return the porcentajeDeCesion
	 */
	public String getPorcentajeDeCesion() {
		return porcentajeDeCesion;
	}
	/**
	 * @param porcentajeDeCesion the porcentajeDeCesion to set
	 */
	public void setPorcentajeDeCesion(String porcentajeDeCesion) {
		this.porcentajeDeCesion = porcentajeDeCesion;
	}
	/**
	 * @return the cesionCuotaParte
	 */
	public String getCesionCuotaParte() {
		return cesionCuotaParte;
	}
	/**
	 * @param cesionCuotaParte the cesionCuotaParte to set
	 */
	public void setCesionCuotaParte(String cesionCuotaParte) {
		this.cesionCuotaParte = cesionCuotaParte;
	}
	/**
	 * @return the maximo
	 */
	public String getMaximo() {
		return maximo;
	}
	/**
	 * @param maximo the maximo to set
	 */
	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}
	/**
	 * @return the contratoPrimerExcedente
	 */
	public String getContratoPrimerExcedente() {
		return contratoPrimerExcedente;
	}
	/**
	 * @param contratoPrimerExcedente the contratoPrimerExcedente to set
	 */
	public void setContratoPrimerExcedente(String contratoPrimerExcedente) {
		this.contratoPrimerExcedente = contratoPrimerExcedente;
	}
	/**
	 * @return the montoDelPleno
	 */
	public String getMontoPleno() {
		return montoPleno;
	}
	/**
	 * @param montoDelPleno the montoDelPleno to set
	 */
	public void setMontoPleno(String montoDelPleno) {
		this.montoPleno = montoDelPleno;
	}
	/**
	 * @return the numeroDePlenos
	 */
	public String getNumeroPlenos() {
		return numeroPlenos;
	}
	/**
	 * @param numeroDePlenos the numeroDePlenos to set
	 */
	public void setNumeroPlenos(String numeroDePlenos) {
		this.numeroPlenos = numeroDePlenos;
	}
	/**
	 * @return the cesionPrimerExcedente
	 */
	public String getCesionPrimerExcedente() {
		return cesionPrimerExcedente;
	}
	/**
	 * @param cesionPrimerExcedente the cesionPrimerExcedente to set
	 */
	public void setCesionPrimerExcedente(String cesionPrimerExcedente) {
		this.cesionPrimerExcedente = cesionPrimerExcedente;
	}
	/**
	 * @return the capacidadMaximaLinea
	 */
	public String getCapacidadMaximaLinea() {
		return capacidadMaximaLinea;
	}
	/**
	 * @param capacidadMaximaLinea the capacidadMaximaLinea to set
	 */
	public void setCapacidadMaximaLinea(String capacidadMaximaLinea) {
		this.capacidadMaximaLinea = capacidadMaximaLinea;
	}
	/**
	 * @return the totalRetencion
	 */
	public String getTotalRetencion() {
		return totalRetencion;
	}
	/**
	 * @param totalRetencion the totalRetencion to set
	 */
	public void setTotalRetencion(String totalRetencion) {
		this.totalRetencion = totalRetencion;
	}
	/**
	 * @return the totalCedido
	 */
	public String getTotalCedido() {
		return totalCedido;
	}
	/**
	 * @param totalCedido the totalCedido to set
	 */
	public void setTotalCedido(String totalCedido) {
		this.totalCedido = totalCedido;
	}
	/**
	 * @return the idTcRamo
	 */
	public String getIdTcRamo() {
		return idTcRamo;
	}
	/**
	 * @param idTcRamo the idTcRamo to set
	 */
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	/**
	 * @return the idTcSubRamo
	 */
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	/**
	 * @param idTcSubRamo the idTcSubRamo to set
	 */
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	/**
	 * @return the idTmContratoCuotaParte
	 */
	public String getIdTmContratoCuotaParte() {
		return idTmContratoCuotaParte;
	}
	/**
	 * @param idTmContratoCuotaParte the idTmContratoCuotaParte to set
	 */
	public void setIdTmContratoCuotaParte(String idTmContratoCuotaParte) {
		this.idTmContratoCuotaParte = idTmContratoCuotaParte;
	}
	/**
	 * @return the idTmContratoPrimerExcedente
	 */
	public String getIdTmContratoPrimerExcedente() {
		return idTmContratoPrimerExcedente;
	}
	/**
	 * @param idTmContratoPrimerExcedente the idTmContratoPrimerExcedente to set
	 */
	public void setIdTmContratoPrimerExcedente(String idTmContratoPrimerExcedente) {
		this.idTmContratoPrimerExcedente = idTmContratoPrimerExcedente;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the usuarioAutorizo
	 */
	public String getUsuarioAutorizo() {
		return usuarioAutorizo;
	}
	/**
	 * @param usuarioAutorizo the usuarioAutorizo to set
	 */
	public void setUsuarioAutorizo(String usuarioAutorizo) {
		this.usuarioAutorizo = usuarioAutorizo;
	}
	/**
	 * @return the fechaAutorizacion
	 */
	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}
	/**
	 * @param fechaAutorizacion the fechaAutorizacion to set
	 */
	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}
	/**
	 * @return the folioContratoCuotaParte
	 */
	public String getFolioContratoCuotaParte() {
		return folioContratoCuotaParte;
	}
	/**
	 * @param folioContratoCuotaParte the folioContratoCuotaParte to set
	 */
	public void setFolioContratoCuotaParte(String folioContratoCuotaParte) {
		this.folioContratoCuotaParte = folioContratoCuotaParte;
	}
	/**
	 * @return the folioContratoPrimerExcedente
	 */
	public String getFolioContratoPrimerExcedente() {
		return folioContratoPrimerExcedente;
	}
	/**
	 * @param folioContratoPrimerExcedente the folioContratoPrimerExcedente to set
	 */
	public void setFolioContratoPrimerExcedente(String folioContratoPrimerExcedente) {
		this.folioContratoPrimerExcedente = folioContratoPrimerExcedente;
	}
	/**
	 * @return the autorizar
	 */
	public String getAutorizar() {
		return autorizar;
	}
	/**
	 * @param autorizar the autorizar to set
	 */
	public void setAutorizar(String autorizar) {
		this.autorizar = autorizar;
	}		
	/**
	 * @return the participacionesCP
	 */
	public String getParticipacionesCP() {
		return participacionesCP;
	}
	/**
	 * @param participacionesCP the participacionesCP to set
	 */
	public void setParticipacionesCP(String participacionesCP) {
		this.participacionesCP = participacionesCP;
	}
	/**
	 * @return the participacionesPE
	 */
	public String getParticipacionesPE() {
		return participacionesPE;
	}
	/**
	 * @param participacionesPE the participacionesPE to set
	 */
	public void setParticipacionesPE(String participacionesPE) {
		this.participacionesPE = participacionesPE;
	}
	public String getMensajeAutorizar() {
		return mensajeAutorizar;
	}
	public void setMensajeAutorizar(String mensajeAutorizar) {
		this.mensajeAutorizar = mensajeAutorizar;
	}
	public String getEstatusContratoCuotaParte() {
		return estatusContratoCuotaParte;
	}
	public void setEstatusContratoCuotaParte(String estatusContratoCuotaParte) {
		this.estatusContratoCuotaParte = estatusContratoCuotaParte;
	}
	public String getEstatusContratoPrimerExcedente() {
		return estatusContratoPrimerExcedente;
	}
	public void setEstatusContratoPrimerExcedente(
			String estatusContratoPrimerExcedente) {
		this.estatusContratoPrimerExcedente = estatusContratoPrimerExcedente;
	}
	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}
	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}
	public String getTipoParticipante() {
		return tipoParticipante;
	}
	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}		
	
}
