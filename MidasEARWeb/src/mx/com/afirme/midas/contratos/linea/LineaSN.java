package mx.com.afirme.midas.contratos.linea;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class LineaSN {
	
	private LineaFacadeRemote beanRemoto;
	
	public LineaSN()
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en LineaSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(LineaFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}

	public List<LineaDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public LineaDTO agregar(LineaDTO lineaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		if (lineaDTO.getContratoCuotaParte() != null && lineaDTO.getContratoCuotaParte().getIdTmContratoCuotaParte() == null)
			lineaDTO.setContratoCuotaParte(null);
		
		if (lineaDTO.getContratoPrimerExcedente() != null && lineaDTO.getContratoPrimerExcedente().getIdTmContratoPrimerExcedente() == null)
			lineaDTO.setContratoPrimerExcedente(null);
		
		if (lineaDTO.getLineaParticipaciones() != null && lineaDTO.getLineaParticipaciones().size() == 0)
			lineaDTO.setLineaParticipaciones(null);
		
		try{
			return beanRemoto.save(lineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(LineaDTO lineaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			beanRemoto.delete(lineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public LineaDTO modificar(LineaDTO lineaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.update(lineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public LineaDTO getPorId(LineaDTO lineaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findById(lineaDTO.getIdTmLinea());
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<LineaDTO> buscarPorFechaInicialFinal(Date fechaInicial,Date fechaFinal)
	throws ExcepcionDeAccesoADatos{
		try{			
			return beanRemoto.buscarPorFechaInicialFinal(fechaInicial,fechaFinal);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String ids,String idsCCP,String idsCPE,Date fInicial,Date fFinal)
		throws ExcepcionDeAccesoADatos{
		try{			
			return beanRemoto.actualizaFechasLineasPorIdLineas(ids, idsCCP, idsCPE, fInicial, fFinal);
		}catch(Exception e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public LineaDTO duplicarLinea(LineaDTO lineaDTO,List<ParticipacionDTO> pCCP,List<ParticipacionDTO> pCPE)
		throws ExcepcionDeAccesoADatos{
		try{					
			return beanRemoto.duplicarLinea(lineaDTO, pCCP, pCPE);
		}catch(Exception e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public boolean validaEjercicio(LineaDTO lineaDTO)throws ExcepcionDeAccesoADatos{
		try{					
			return beanRemoto.validaEjercicio(lineaDTO);
		}catch(Exception e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<EjercicioDTO> obtenerEjercicios(){
		
		return beanRemoto.obtenerEjercicios();
	}
	
	public List<SuscripcionDTO> obtenerSuscripcionesPorLinea(BigDecimal idTcSubRamo,int ejercicio ,int tipoReaseguro){
		
		return beanRemoto.obtenerSuscripcionesPorLinea(idTcSubRamo, ejercicio, tipoReaseguro);
	}
	
	public void desasociarContratoCPLinea(LineaDTO lineaDTO){
		beanRemoto.desasociarContratoCPLinea(lineaDTO);
	}
	
	public void desasociarContratoPELinea(LineaDTO lineaDTO){
		beanRemoto.desasociarContratoPELinea(lineaDTO);
	}
	
	public int obtenerEjercicioActual(){
		return beanRemoto.obtenerEjercicioActual();
	}
	
	public BigDecimal obtenerTipoDistribucion(BigDecimal idToPoliza, BigDecimal idToCobertura){
		return beanRemoto.obtenerTipoDistribucion(idToPoliza, idToCobertura);
	}
}
