package mx.com.afirme.midas.contratos.linea;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConfiguracionLineaSN {
	
	private ConfiguracionLineaFacadeRemote beanRemoto;
	
	public ConfiguracionLineaSN()
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en ConfiguracionLineaSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ConfiguracionLineaFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}

	public List<ConfiguracionLineaDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionLineaDTO> listarLineaFiltrado(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarLineaFiltrado(configuracionLineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionLineaDTO> listarFiltrado(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.listarFiltrado(configuracionLineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ConfiguracionLineaDTO> listarFiltradoVigencia(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltradoVigencia(configuracionLineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionLineaDTO> listarFiltradoContrato(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarFiltradoContrato(configuracionLineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ConfiguracionLineaDTO getPorId(ConfiguracionLineaDTO configuracionLineaDTO) 
			throws ExcepcionDeAccesoADatos{
		
		try{
			return beanRemoto.findById(configuracionLineaDTO.getIdTmLinea());
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConfiguracionLineaDTO> getCLineasPorContratosDeCLinea(ConfiguracionLineaDTO configuracionLineaDTO)
			throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.listarLineaFiltrado(configuracionLineaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public List<ConfiguracionLineaDTO> getConfiguracionLineaPorIDLineas(List<String> idLineas)
			throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getConfiguracionLineaPorIDLineas(idLineas);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}	
	}
	
	//Resibe una cadena de ids separada por comas
	public List<SubRamoDTO> getSubRamosRestantes(String ids, BigDecimal idTcRamo)throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getSubRamosRestantes(ids,idTcRamo);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}	
	}
}
