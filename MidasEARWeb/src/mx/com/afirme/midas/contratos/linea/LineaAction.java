package mx.com.afirme.midas.contratos.linea;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDN;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDN;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDN;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionId;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDN;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.reaseguro.reportes.ReportesReaseguro;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import net.sf.jasperreports.engine.JRException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LineaAction extends MidasMappingDispatchAction{
	
	/***
	 * listarTodos
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	private void listarTodos(HttpServletRequest request) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		ConfiguracionLineaDN configuracionLineaDN = ConfiguracionLineaDN.getInstancia();
		List<ConfiguracionLineaDTO> configuracionLineas = configuracionLineaDN.listarTodos();
		request.setAttribute("configuracionLineas", configuracionLineas);
	}		
	
	
	/**
	 * listarLinea
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listarLinea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
	
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
		configuracionLineaForm.setFechaInicial("");
		configuracionLineaForm.setFechaFinal("");
		request.setAttribute("horaInicial", "00:00");
		request.setAttribute("horaFinal", "00:00");
	
		return mapping.findForward(reglaNavegacion);
	}
	
		/**
	 * agregar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String estatusGuardado="guardado"; //Estatus de la linea al guardarla, puede ser guardado autorizado, guardado No Autorizado, No guardado

		try{
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			// Estatus de 'modificar'
			configuracionLineaForm.setEstatus("0");

			LineaDTO  lineaDTO  = new LineaDTO();
			
			LineaDN lineaDN = LineaDN.getInstancia();
			
			if (configuracionLineaForm.getIdTmLinea() != null && !UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTmLinea())){
				lineaDTO.setIdTmLinea(new BigDecimal(configuracionLineaForm.getIdTmLinea()));
				lineaDTO = LineaDN.getInstancia().getPorId(lineaDTO);
			}
			this.poblarDTO(configuracionLineaForm, lineaDTO, request);
			
//			Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
//			mx.com.afirme.midas.interfaz.linea.LineaDN lineaInterfazDN = 
//				mx.com.afirme.midas.interfaz.linea.LineaDN.getInstancia(usuario.getNombreUsuario());
			
			this.agregarParticipacionesCPyPE(configuracionLineaForm,lineaDTO);
			lineaDTO = lineaDN.agregar(lineaDTO);
			
			if (lineaDTO == null)
				estatusGuardado = "noGuardado";
			else{
				if (configuracionLineaForm.getAutorizar() == null)
					configuracionLineaForm.setAutorizar("0");
				else
					configuracionLineaForm.setAutorizar("1");
				
				if (configuracionLineaForm.getAutorizar().equals("1"))
					if (lineaDN.autorizaLinea(lineaDTO)){
						lineaDTO.setEstatus(new BigDecimal(1));
						lineaDTO.setFechaAutorizacion(new Date());
						//lineaDTO.setUsuarioAutorizo(usuarioAutorizo); Pendiente de resolver, se espera que se defina el manejo de usuarios
						lineaDN.agregar(lineaDTO);
						
						configuracionLineaForm.setFechaAutorizacion(new Date().toString());
						configuracionLineaForm.setMensajeAutorizar("1");
						estatusGuardado="guardadoAutorizado";
					}else{
						configuracionLineaForm.setAutorizar("0");
						configuracionLineaForm.setMensajeAutorizar("0");
						lineaDTO.setEstatus(new BigDecimal(0));
						lineaDTO.setFechaAutorizacion(null);
						lineaDTO.setUsuarioAutorizo(null);
						lineaDN.agregar(lineaDTO);
						estatusGuardado="guardadoNoAutorizado";
					}
			}
				StringBuffer buffer = new StringBuffer();
				buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
				buffer.append("<response>");
				buffer.append("<item>");
				buffer.append("<estatus>"+estatusGuardado+"</estatus>");
				buffer.append("</item>");
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());
				//lineaDN.agregar(lineaDTO);		
				this.listarTodos(request);
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			estatusGuardado="noGuardado";
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);			
		}catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
	}
	
	public void borrarLineaById(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
			LineaDTO  lineaDTO  = new LineaDTO();
			LineaDN lineaDN	    = LineaDN.getInstancia();
			String idLinea = request.getParameter("idTmLinea");
			lineaDTO.setIdTmLinea(new BigDecimal(idLinea.toCharArray()));
			lineaDTO = lineaDN.getPorId(lineaDTO);
			lineaDN.borrar(lineaDTO);
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<item>");			
			buffer.append("<id>"+idLinea+"</id><description><![CDATA[La linea se elimin\u00f3 correctamente.]]></description>");			
			buffer.append("</item>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
			return;
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ExcepcionDeAccesoADatos e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}	
	}
	/**
	 * borrar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			LineaDTO  lineaDTO  = new LineaDTO();
			LineaDN lineaDN	    = LineaDN.getInstancia();
			this.poblarDTO(configuracionLineaForm, lineaDTO, request);
			lineaDTO = lineaDN.getPorId(lineaDTO);
			lineaDN.borrar(lineaDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			reglaNavegacion = Sistema.NO_EXITOSO;			
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * modificar
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try{
			ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
			LineaDTO  lineaDTO  = new LineaDTO();
			this.poblarDTO(configuracionLineaForm, lineaDTO, request);
			LineaDN lineaDN = LineaDN.getInstancia();
			lineaDN.modificar(lineaDTO);
			this.listarTodos(request);
		}catch(SystemException e){
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}catch(ExcepcionDeAccesoADatos e){
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			reglaNavegacion = Sistema.NO_EXITOSO;			
		}
		
		return mapping.findForward(reglaNavegacion);
		
	}
	
	/**
	 * autorizarLineas
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void autorizarLineas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		ConfiguracionLineaForm configuracionLineaForm = (ConfiguracionLineaForm) form;
		try{
			String idLineas = configuracionLineaForm.getIdTmLinea();
			LineaDN lineaDN = LineaDN.getInstancia();
			Usuario usuario = getUsuarioMidas(request);
			String autorizarCompleto = lineaDN.autorizarLineas(idLineas, usuario.getId());
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<item>");		
			String[] aux = autorizarCompleto.split("-");
			if(!autorizarCompleto.equals("true")&& autorizarCompleto != null){
				if(aux[2].equals("ccp")){
					buffer.append("<id>"+aux[1]+"</id><description><![CDATA[No Se Pudo Autorizar Ya Que La Linea Del Ramo "+ aux[4] + " Subramo " + aux[3] +" Tiene El Contrato CuotaParte Sin Autorizar.]]></description>");			
				}else if(aux[2].equals("cpe")) {
					buffer.append("<id>"+aux[1]+"</id><description><![CDATA[No Se Pudo Autorizar Ya Que La Linea Del Ramo "+ aux[4] + " Subramo " + aux[3] +" Tiene El Contrato PrimerExcedente Sin Autorizar.]]></description>");			
				}else{
					buffer.append("<id>"+aux[1]+"</id><description><![CDATA[No Se Pudo Autorizar Ya Que La Linea Del Ramo "+ aux[4] + " Subramo " + aux[3] +" Tiene El Contrato CuotaParte y PrimerExcedente Sin Autorizar.]]></description>");			
				}
			}else{
				buffer.append("<id>"+aux[1]+"</id><description><![CDATA[Se Autoriz\u00f3 Correctamente.]]></description>");			
			}
			buffer.append("</item>");
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		}catch(Exception e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	/**
	 * autorizarLineas
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public void renovarLinea(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
			String fechaInicial = request.getParameter("fechaInicial");
			String fechaFinal = request.getParameter("fechaFinal");
			String estatus = request.getParameter("estatus");
			String idTcRamo = request.getParameter("idTcRamo");
			
			try{
				LineaDN lineaDN = LineaDN.getInstancia();
				lineaDN.renovarLineas(fechaInicial,fechaFinal,estatus,idTcRamo);
			}catch (Exception e){
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
	}
	
	public void desautorizarLinea(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String idTcLinea = request.getParameter("idTcLinea");
		try{
			LineaDN lineaDN = LineaDN.getInstancia();
			lineaDN.desautorizarLinea(idTcLinea);
		}catch(ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	/**
	 * obtenerParticipacionesDelString
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */	
	public List<ParticipacionDTO> obtenerParticipacionesDelString(String cadenaGrid, HttpServletRequest request){
		String participacionesStr = cadenaGrid;
		String [] participaciones = participacionesStr.split("/");
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		List<ParticipacionDTO> participacionesList = new ArrayList<ParticipacionDTO>();
		ParticipacionDTO participacionDTO;
		for(String participacionStr : participaciones) {
			String[] datos = participacionStr.split("_");
			String idParticipacion = datos[0];
			String comision = datos[1];
			participacionDTO = new ParticipacionDTO();
			try {
				participacionDTO = participacionDN.getPorId(new BigDecimal(idParticipacion));
				
				if (UtileriasWeb.esObjetoNulo(comision) || comision.length() == 0)
					comision = "0";
				
				participacionDTO.setPorcentajeParticipacion(new Double(comision)); //El campo porcentaje participacion es usado solamente para conserva el porcentaje de la comision cuyo destino es LineaParticipacionDTO.comision
				participacionesList.add(participacionDTO);
			
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		
		
		return participacionesList;
	}
	
	/**
	 * obtenerComisionesDelString
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */	
	public List<LineaParticipacionDTO> obtenerComisionesDelString(String cadenaGrid, HttpServletRequest request){
		String participacionesStr = cadenaGrid;
		String [] participaciones = participacionesStr.split("/");
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		List<LineaParticipacionDTO> lineaParticipacionDTOList = new ArrayList<LineaParticipacionDTO>();
		ParticipacionDTO participacionDTO;
		LineaParticipacionDTO lineaParticipacionDTO;
		for(String participacionStr : participaciones) {
			String[] datos = participacionStr.split("_");
			String idParticipacion = datos[0];
			String comision = datos[1];
			String comisionCombinacion = datos[2];
			participacionDTO = new ParticipacionDTO();
			try {
				participacionDTO = participacionDN.getPorId(new BigDecimal(idParticipacion));
				
				if (UtileriasWeb.esObjetoNulo(comision) || comision.length() == 0)
					comision = "0";
				
				//participacionDTO.setPorcentajeParticipacion(new Double(comision)); //El campo porcentaje participacion es usado solamente para conserva el porcentaje de la comision cuyo destino es LineaParticipacionDTO.comision
				lineaParticipacionDTO = new LineaParticipacionDTO();
				lineaParticipacionDTO.setComision(new Double(comision));
				lineaParticipacionDTO.setComisionEnCombinacion(new Double(comisionCombinacion));
				lineaParticipacionDTO.setParticipacion(participacionDTO);
				lineaParticipacionDTOList.add(lineaParticipacionDTO);		
			
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		
		
		return lineaParticipacionDTOList;
	}
	
	/**
	 * ParticipacionesCP
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */	
	public void ParticipacionesCP(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
							
		String idParticipacion = request.getParameter("gr_id");
		String comision = request.getParameter("comision");
		
		if(idParticipacion!=null && comision!=null){
			//try{
				
				
				
				
			//}catch(SystemException e){
				
			//}catch(ExcepcionDeAccesoADatos e){			
			//	UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			//}				
		}
				
							
	}
	
	/**
	 * cargarParticipacionesPE
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */	
	public void cargarParticipacionesPE(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String json="{rows:[]}";
		response.setContentType("text/json");
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
		} catch (IOException e1) {
		}
		String id = request.getParameter("idContratoPE");
		String idTmLinea = request.getParameter("idTmLinea");
		int formatoGridADesplegar;
		
		if (request.getParameter("formatoGridADesplegar") != null)
			formatoGridADesplegar = new Integer(request.getParameter("formatoGridADesplegar")).intValue();
		else
			formatoGridADesplegar = 0;
		
		if (id != null && id.length() > 0){
			try{
				ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();
				contratoPrimerExcedenteDTO = contratoPrimerExcedenteDN.getPorId(new BigDecimal(id));
				
				ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
				List<ParticipacionDTO> participaciones = participacionDN.listarPorContratoPrimerExcedente(contratoPrimerExcedenteDTO);
				LineaParticipacionDTO lineaParticipacionDTO;
				List<LineaParticipacionDTO> lineaParticipacionDTOList = new ArrayList<LineaParticipacionDTO>();
			if (participaciones.size() > 0 && idTmLinea != null && !UtileriasWeb.esCadenaVacia(idTmLinea) && id != null){
					for (ParticipacionDTO participacionDTO2 : participaciones) {
						lineaParticipacionDTO = new LineaParticipacionDTO();
						LineaParticipacionId lineaParticipacionId = new LineaParticipacionId();
						lineaParticipacionId.setIdTdParticipacion(participacionDTO2.getIdTdParticipacion());
						lineaParticipacionId.setIdTmLinea(new BigDecimal(idTmLinea));
						lineaParticipacionDTO.setId(lineaParticipacionId);
						lineaParticipacionDTO = LineaParticipacionDN.getInstancia().getPorId(lineaParticipacionDTO);
						
						if (lineaParticipacionDTO != null){
							participacionDTO2.setPorcentajeParticipacion(lineaParticipacionDTO.getComision());
							if (formatoGridADesplegar == 0){
								lineaParticipacionDTO.setParticipacion(participacionDTO2);
								lineaParticipacionDTOList.add(lineaParticipacionDTO);
							}
						}else
							if (formatoGridADesplegar == 0){
								lineaParticipacionDTO = new LineaParticipacionDTO();
								lineaParticipacionDTO.setComision(new Double(0));
								lineaParticipacionDTO.setComisionEnCombinacion(new Double(0));
								lineaParticipacionDTOList.add(lineaParticipacionDTO);
								lineaParticipacionDTO.setParticipacion(participacionDTO2);
								participacionDTO2.setPorcentajeParticipacion(new Double(0));
							}
				}
			}else{
				if (formatoGridADesplegar == 0){
					for (ParticipacionDTO participacionDTO2 : participaciones) {
						participacionDTO2.setPorcentajeParticipacion(new Double(0));
						lineaParticipacionDTO = new LineaParticipacionDTO();
						lineaParticipacionDTO.setComision(new Double(0));
						lineaParticipacionDTO.setComisionEnCombinacion(new Double(0));
						lineaParticipacionDTO.setParticipacion(participacionDTO2);
						lineaParticipacionDTOList.add(lineaParticipacionDTO);
					}
				}
			}
				
			switch(formatoGridADesplegar){
				case 0:			
					json = this.getJson(lineaParticipacionDTOList, false);
					break;
				case 1:
					json = this.getJsonParaRegistrarContrato(participaciones);
					break;
				case 2:
					json = this.getJsonParaDesplegarContrato(participaciones);
			}		
			
			}catch(SystemException e){
				
			}catch(ExcepcionDeAccesoADatos e){			
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}else{ 
			String cadenaGrid = request.getParameter("participacionesPE");
			if (cadenaGrid !=null && cadenaGrid.length() > 0){
				try {
					switch(formatoGridADesplegar){
					case 0:
							json = this.getJson(obtenerComisionesDelString(cadenaGrid, request), true);
							break;
					case 1:
							json = this.getJsonParaRegistrarContrato(obtenerParticipacionesDelString(cadenaGrid, request));
							break;
					case 2:
							json = this.getJsonParaDesplegarContrato(obtenerParticipacionesDelString(cadenaGrid, request));
						
					}
					
		
				} catch (ExcepcionDeAccesoADatos e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				}

			}
		}
		pw.write(json);
		pw.flush();
		pw.close();
	}
	
	/**
	 * cargarParticipacionesCP
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */	
	public void cargarParticipacionesCP(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String json="{rows:[]}";
		response.setContentType("text/json");
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
		} catch (IOException e1) {
		}
		String id = request.getParameter("idContratoCP");
		String idTmLinea = request.getParameter("idTmLinea");
		
		int formatoGridADesplegar;
		
		if (request.getParameter("formatoGridADesplegar") != null)
			formatoGridADesplegar = new Integer(request.getParameter("formatoGridADesplegar")).intValue();
		else
			formatoGridADesplegar = 0;
		
		if (id != null && id.length() > 0){
			try{
				ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
				contratoCuotaParteDTO = contratoCuotaParteDN.getPorId(new BigDecimal(id));
				LineaParticipacionDTO lineaParticipacionDTO;
				List<LineaParticipacionDTO> lineaParticipacionDTOList = new ArrayList<LineaParticipacionDTO>();
				
				ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
				List<ParticipacionDTO> participaciones = participacionDN.listarPorContratoCuotaParte(contratoCuotaParteDTO);	
				json="{rows:[]}";
				
				if (participaciones.size() > 0 && idTmLinea != null && !UtileriasWeb.esCadenaVacia(idTmLinea) && id != null){
					for (ParticipacionDTO participacionDTO2 : participaciones) {
						lineaParticipacionDTO = new LineaParticipacionDTO();
						LineaParticipacionId lineaParticipacionId = new LineaParticipacionId();
						lineaParticipacionId.setIdTdParticipacion(participacionDTO2.getIdTdParticipacion());
						lineaParticipacionId.setIdTmLinea(new BigDecimal(idTmLinea));
						lineaParticipacionDTO.setId(lineaParticipacionId);
						lineaParticipacionDTO = LineaParticipacionDN.getInstancia().getPorId(lineaParticipacionDTO);
						if (lineaParticipacionDTO != null){
							participacionDTO2.setPorcentajeParticipacion(lineaParticipacionDTO.getComision());
							lineaParticipacionDTO.setParticipacion(participacionDTO2);
							lineaParticipacionDTOList.add(lineaParticipacionDTO);
						}else
							if (formatoGridADesplegar == 0){ 
								//participacionDTO2.setPorcentajeParticipacion(new Double(0));
								lineaParticipacionDTO = new LineaParticipacionDTO();
								lineaParticipacionDTO.setComision(new Double(0));
								lineaParticipacionDTO.setComisionEnCombinacion(new Double(0));
								lineaParticipacionDTO.setParticipacion(participacionDTO2);
								lineaParticipacionDTOList.add(lineaParticipacionDTO);
							}
					}
				}else{
					if (formatoGridADesplegar == 0) 
					for (ParticipacionDTO participacionDTO2 : participaciones) {
						participacionDTO2.setPorcentajeParticipacion(new Double(0));
						lineaParticipacionDTO = new LineaParticipacionDTO();
						lineaParticipacionDTO.setComision(new Double(0));
						lineaParticipacionDTO.setComisionEnCombinacion(new Double(0));
						lineaParticipacionDTO.setParticipacion(participacionDTO2);
						lineaParticipacionDTOList.add(lineaParticipacionDTO);
					}
				}
				
				switch(formatoGridADesplegar){
					case 0:			
						json = this.getJson(lineaParticipacionDTOList, false);
						break;
					case 1:
						json = this.getJsonParaRegistrarContrato(participaciones);
						break;
					case 2:
						json = this.getJsonParaDesplegarContrato(participaciones);
				}
				
			}catch(SystemException e){
				
			}catch(ExcepcionDeAccesoADatos e){			
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}else{ 
			String cadenaGrid = request.getParameter("participacionesCP");
			if (cadenaGrid !=null && cadenaGrid.length() > 0){				
				json="{rows:[]}";
				try {
					switch(formatoGridADesplegar){
					case 0:
							json = this.getJson(obtenerComisionesDelString(cadenaGrid, request), true);
							break;
					case 1:
							json = this.getJsonParaRegistrarContrato(obtenerParticipacionesDelString(cadenaGrid, request));
							break;
					case 2:
							json = this.getJsonParaDesplegarContrato(obtenerParticipacionesDelString(cadenaGrid, request));
						
					}		
				} catch (ExcepcionDeAccesoADatos e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				} catch (SystemException e) {
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				}
			}
		}
		pw.write(json);
		pw.flush();
		pw.close();
		
	}
	
	/**
	 * ParticipacionesPE
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */	
	public void ParticipacionesPE(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
							
		String idParticipacion = request.getParameter("gr_id");
		String comision = request.getParameter("comision");
		
		if(idParticipacion!=null && comision!=null){
			ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
			ParticipacionDTO participacionDTO = new ParticipacionDTO();
			participacionDTO.setIdTdParticipacion(new BigDecimal(idParticipacion));
			try {
				
				participacionDTO = participacionDN.getPorId(new BigDecimal(idParticipacion));
				
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
							
	}
	
			
	/**
	 * poblarDTO
	 * @param configuracionLineaForm
	 * @param lineaDTO
	 * @throws SystemException
	 */
	private void poblarDTO(ConfiguracionLineaForm configuracionLineaForm,
			LineaDTO lineaDTO, HttpServletRequest request) throws SystemException, ExcepcionDeAccesoADatos,
			ParseException{
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTmLinea()))
			lineaDTO.setIdTmLinea(new BigDecimal(configuracionLineaForm.getIdTmLinea()));
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTmContratoCuotaParte())){
			ContratoCuotaParteDN contratoCuotaParteDN = ContratoCuotaParteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			BigDecimal id = new BigDecimal(configuracionLineaForm.getIdTmContratoCuotaParte());			
			lineaDTO.setContratoCuotaParte(contratoCuotaParteDN.getPorId(id));
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTmContratoPrimerExcedente())){
			ContratoPrimerExcedenteDN contratoPrimerExcedenteDN = ContratoPrimerExcedenteDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));			
			BigDecimal id = new BigDecimal(configuracionLineaForm.getIdTmContratoPrimerExcedente());			
			lineaDTO.setContratoPrimerExcedente(contratoPrimerExcedenteDN.getPorId(id));
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getEstatus())){			
			BigDecimal estatus = new BigDecimal(configuracionLineaForm.getEstatus());			
			lineaDTO.setEstatus(estatus);
		}				
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaInicial())){
			String fechaStr = configuracionLineaForm.getFechaInicial();
			Date fecha = UtileriasWeb.getFechaHoraFromString(fechaStr);
			lineaDTO.setFechaInicial(fecha);
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaFinal())){
			String fechaStr = configuracionLineaForm.getFechaFinal();
			Date fecha = UtileriasWeb.getFechaHoraFromString(fechaStr);
			lineaDTO.setFechaFinal(fecha);
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaAutorizacion())){
			String fechaStr = configuracionLineaForm.getFechaAutorizacion();
			Date fecha = UtileriasWeb.getFechaFromString(fechaStr);
			lineaDTO.setFechaAutorizacion(fecha);
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getMaximo()))						
			lineaDTO.setMaximo(new Double(configuracionLineaForm.getMaximo()));
		
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTcSubRamo())){
			String id = configuracionLineaForm.getIdTcSubRamo();
			SubRamoDTO subRamoDTO = new SubRamoDTO();
			subRamoDTO.setIdTcSubRamo(new BigDecimal(id));
			SubRamoDN subRamoDN = SubRamoDN.getInstancia();
			lineaDTO.setSubRamo(subRamoDN.getSubRamoPorId(subRamoDTO));
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getModoDistribucion())){
			String id = configuracionLineaForm.getModoDistribucion();
			lineaDTO.setTipoDistribucion(new BigDecimal(id));
		}

		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getUsuarioAutorizo())){
			String id = configuracionLineaForm.getUsuarioAutorizo();
			lineaDTO.setUsuarioAutorizo(new BigDecimal(id));
		}
	}
	
	
	private String getJson(List<LineaParticipacionDTO> participaciones, boolean esDelTemporal) 
	throws ExcepcionDeAccesoADatos, SystemException{

		MidasJsonBase json = new MidasJsonBase();
		if(participaciones != null && participaciones.size() > 0) {
			for(LineaParticipacionDTO lineaParticipacionDTO : participaciones) {
				
				String porcentajeComision = "0.0";
				String porcentajeComisionCombinacion = "0.0";
				
				if (!UtileriasWeb.esObjetoNulo(lineaParticipacionDTO.getComision())){
					porcentajeComision =  lineaParticipacionDTO.getComision().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(lineaParticipacionDTO.getComisionEnCombinacion())){
					porcentajeComisionCombinacion = lineaParticipacionDTO.getComisionEnCombinacion().toString();
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(lineaParticipacionDTO.getParticipacion().getIdTdParticipacion().toString());
				row.setDatos(lineaParticipacionDTO.getParticipacion().getReaseguradorCorredor().getNombre(), porcentajeComision, porcentajeComisionCombinacion);
				json.addRow(row);
			}
		}
		return json.toString();
	}
	
	private String getJsonParaRegistrarContrato(List<ParticipacionDTO> participaciones) throws ExcepcionDeAccesoADatos, SystemException{
		
		MidasJsonBase json = new MidasJsonBase();
		if(participaciones != null && participaciones.size() > 0) {
			for(ParticipacionDTO participacion : participaciones) {
					
				String nombreReaseguradorCorredor = "";
				String cnFs = "";
				String numCuentaPesos = "";
				String numCuentaDolares = "";
				String contacto = "";
				String porcentajeParticipacion = "";
				String iconoModificar = null;
				String iconoBorrar = null;
				String iconoAgregar = null;
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredor()) && !UtileriasWeb.esCadenaVacia(participacion.getReaseguradorCorredor().getNombre())) {
					nombreReaseguradorCorredor = participacion.getReaseguradorCorredor().getNombre();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredor()) && !UtileriasWeb.esCadenaVacia(participacion.getReaseguradorCorredor().getCnfs())) {
					cnFs = participacion.getReaseguradorCorredor().getCnfs();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoPesos()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoPesos().getNumerocuenta())) {
					numCuentaPesos = participacion.getCuentaBancoPesos().getNumerocuenta().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDolares()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDolares().getNumerocuenta())) {
					numCuentaDolares = participacion.getCuentaBancoDolares().getNumerocuenta().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getContacto()) && !UtileriasWeb.esCadenaVacia(participacion.getContacto().getNombre())) {
					contacto = participacion.getContacto().getNombre();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getPorcentajeParticipacion())) {
					porcentajeParticipacion = participacion.getPorcentajeParticipacion() + "%";
				}
				
				String funcion01 = "0";
				String funcion02 = "0";
				if (!UtileriasWeb.esObjetoNulo(participacion.getContratoCuotaParte())){
					funcion01 = "1";
					funcion02 = "2";
				}else if (!UtileriasWeb.esObjetoNulo(participacion.getContratoPrimerExcedente())){
					funcion01 = "4";
					funcion02 = "5";
				}
				
				iconoModificar = "/MidasWeb/img/Edit14.gif^Modificar^javascript: sendRequestParticipantesContrato("+funcion01+","+participacion.getIdTdParticipacion()+");^_self";
				iconoBorrar = "/MidasWeb/img/delete14.gif^Borrar^javascript: sendRequestParticipantesContrato("+funcion02+","+participacion.getIdTdParticipacion()+");^_self";
				
				// si participacion.getReaseguradorCorredor() == 0 entonces la participacion es de un CORREDOR
				// si participacion.getReaseguradorCorredor() == 1 entonces la participacion es de un REASEGURADOR
				if (participacion.getTipo().equals(new BigDecimal(0))){
					ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
					ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
					participacionCorredorDTO.setParticipacion(participacion);
					participacionCorredorDTO.setReaseguradorCorredor(new ReaseguradorCorredorDTO());
					List<ParticipacionCorredorDTO> participacionCorredorDTOlist = participacionCorredorDN.listarFiltrados(participacionCorredorDTO);
					funcion01 = "0";
					if (!UtileriasWeb.esObjetoNulo(participacion.getContratoCuotaParte()))
						funcion01 = "3";
					else if (!UtileriasWeb.esObjetoNulo(participacion.getContratoPrimerExcedente()))
							funcion01 ="6";
					
					if (participacionCorredorDTOlist != null && participacionCorredorDTOlist.size() > 0)
						iconoAgregar = "/MidasWeb/img/add18.png^Agregar mas Reaseguradores^javascript:sendRequestParticipantesContrato("+funcion01+","+participacion.getIdTdParticipacion()+");^_self";
					else
						iconoAgregar = "/MidasWeb/img/add17.png^Agregar un Reasegurador^javascript:sendRequestParticipantesContrato("+funcion01+","+participacion.getIdTdParticipacion()+");^_self";
				}else if (participacion.getTipo().equals(new BigDecimal(1))) {
						iconoAgregar = "/MidasWeb/img/add19.png^No Aplica^javascript: void(0);^_self";
				}
				
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(participacion.getIdTdParticipacion().toString());
				row.setDatos(
						nombreReaseguradorCorredor,
						cnFs,
						numCuentaPesos,
						numCuentaDolares,
						contacto,
						porcentajeParticipacion,
						iconoModificar,
						iconoBorrar,
						iconoAgregar
				);
				json.addRow(row);
									
			}
		} 
		return json.toString();
	}
	
private String getJsonParaDesplegarContrato(List<ParticipacionDTO> participaciones) throws ExcepcionDeAccesoADatos, SystemException{
		
//		String json = "{rows:[";
//		if(participaciones != null && participaciones.size() > 0) {
//			for(ParticipacionDTO participacion : participaciones) {
//				
//				json += "{id:" + participacion.getIdTdParticipacion() + ",data:[";
//				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredor()) && !UtileriasWeb.esCadenaVacia(participacion.getReaseguradorCorredor().getNombre()))
//					json += "\"" + participacion.getReaseguradorCorredor().getNombre() + "\",";
//				else
//					json += "\"\",";
//				
//				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredor()) && !UtileriasWeb.esCadenaVacia(participacion.getReaseguradorCorredor().getCnfs()))
//				json += "\"" + participacion.getReaseguradorCorredor().getCnfs() + "\",";
//				else
//					json += "\"\",";
//				
//				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoPesos()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoPesos().getNumerocuenta()))
//					json += "\"" + participacion.getCuentaBancoPesos().getNumerocuenta() + "\",";
//				else
//					json += "\"\",";
//				
//				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDolares()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDolares().getNumerocuenta()))
//					json += "\"" + participacion.getCuentaBancoDolares().getNumerocuenta() + "\",";
//				else
//					json += "\"\",";
//				
//				if (!UtileriasWeb.esObjetoNulo(participacion.getContacto()) && !UtileriasWeb.esCadenaVacia(participacion.getContacto().getNombre()))
//					json += "\""  + participacion.getContacto().getNombre() + "\",";
//				else 
//					json += "\"\",";
//				
//				if (!UtileriasWeb.esObjetoNulo(participacion.getContacto()) && !UtileriasWeb.esObjetoNulo(participacion.getPorcentajeParticipacion()))
//					json += "\"" + participacion.getPorcentajeParticipacion() + "%\",";
//				else 
//					json += "\"\",";
//				
//				json += "]},";						
//			}
//			json = json.substring(0, json.length() -1) + "]}";
//		} else {
//			json += "]}";
//		}
		MidasJsonBase json = new MidasJsonBase();
		if(participaciones != null && participaciones.size() > 0) {
			for(ParticipacionDTO participacion : participaciones) {
				
				String nombreReaseguradorCorredor = "";
				String cnFs = "";
				String numCuentaPesos = "";
				String numCuentaDolares = "";
				String contacto = "";
				String porcentajeParticipacion = "";
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredor()) && !UtileriasWeb.esCadenaVacia(participacion.getReaseguradorCorredor().getNombre())) {
					nombreReaseguradorCorredor = participacion.getReaseguradorCorredor().getNombre();
				}
								
				if (!UtileriasWeb.esObjetoNulo(participacion.getReaseguradorCorredor()) && !UtileriasWeb.esCadenaVacia(participacion.getReaseguradorCorredor().getCnfs())) {
					cnFs = participacion.getReaseguradorCorredor().getCnfs();
				}
								
				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoPesos()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoPesos().getNumerocuenta())) {
					numCuentaPesos = participacion.getCuentaBancoPesos().getNumerocuenta().toString();
				}
								
				if (!UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDolares()) && !UtileriasWeb.esObjetoNulo(participacion.getCuentaBancoDolares().getNumerocuenta())) {
					numCuentaDolares = participacion.getCuentaBancoDolares().getNumerocuenta().toString();
				}
				
				if (!UtileriasWeb.esObjetoNulo(participacion.getContacto()) && !UtileriasWeb.esCadenaVacia(participacion.getContacto().getNombre())) {
					contacto = participacion.getContacto().getNombre();
				}
								
				if (!UtileriasWeb.esObjetoNulo(participacion.getPorcentajeParticipacion())) {
					porcentajeParticipacion = participacion.getPorcentajeParticipacion() + "%";
				}
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(participacion.getIdTdParticipacion().toString());
				row.setDatos(
						nombreReaseguradorCorredor,
						cnFs,
						numCuentaPesos,
						numCuentaDolares,
						contacto,
						porcentajeParticipacion
				);
				json.addRow(row);
									
			}
		}
		return json.toString();
	}
	
	private void agregarParticipacionesCPyPE(
			ConfiguracionLineaForm configuracionLineaForm, 
			LineaDTO lineaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ArrayList<LineaParticipacionDTO> lineaParticipaciones = new ArrayList<LineaParticipacionDTO>();
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		
		LineaParticipacionDTO lineaParticipacion = new LineaParticipacionDTO();
		if (configuracionLineaForm.getParticipacionesCP().length() > 0){
			String participacionesStr = configuracionLineaForm.getParticipacionesCP();
			String [] participaciones = participacionesStr.split("/");
			for(String participacionStr : participaciones) {
				String[] datos = participacionStr.split("_");
				String idParticipacion = datos[0];
				String comision = datos[1];
				String comisionCombinacion = datos[2];
				
				ParticipacionDTO participacionDTO;
				LineaParticipacionId id = new LineaParticipacionId();
				participacionDTO = participacionDN.getPorId(new BigDecimal(idParticipacion));	
				lineaParticipacion = new LineaParticipacionDTO();
				lineaParticipacion.setParticipacion(participacionDTO);
				lineaParticipacion.setLinea(lineaDTO);
				id.setIdTdParticipacion(new BigDecimal(idParticipacion));
				//id.setIdTmLinea(lineaDTO.getIdTmLinea());
				
				lineaParticipacion.setId(id);
				lineaParticipacion.setComision(new Double(comision));
				lineaParticipacion.setComisionEnCombinacion(new Double(comisionCombinacion));
				lineaParticipaciones.add(lineaParticipacion);
			}
			lineaDTO.setLineaParticipaciones(lineaParticipaciones);
		}
		
		if (configuracionLineaForm.getParticipacionesPE().length() > 0){
			String participacionesStr = configuracionLineaForm.getParticipacionesPE();
			String [] participaciones = participacionesStr.split("/");
			for(String participacionStr : participaciones) {
				String[] datos = participacionStr.split("_");
				String idParticipacion = datos[0];
				String comision = datos[1];
				String comisionCombinacion = datos[2];
							
				ParticipacionDTO participacionDTO = participacionDN.getPorId(new BigDecimal(idParticipacion));			
				lineaParticipacion = new LineaParticipacionDTO();
				lineaParticipacion.setParticipacion(participacionDTO);
				lineaParticipacion.setLinea(lineaDTO);
				
				LineaParticipacionId id = new LineaParticipacionId();
				id.setIdTdParticipacion(new BigDecimal(idParticipacion));
				lineaParticipacion.setId(id);
				lineaParticipacion.setComision(Double.valueOf(comision));
				lineaParticipacion.setComisionEnCombinacion(new Double(comisionCombinacion));
				lineaParticipaciones.add(lineaParticipacion);
			}
		}
		lineaDTO.setLineaParticipaciones(lineaParticipaciones);
	}
	
	public void rptEstadisticaFiscal(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String fechaInicial = request.getParameter("fechaInicial");
		String fechaFinal = request.getParameter("fechaFinal");
		String idReaseguradorCorredorStr = request.getParameter("idReaseguradorCorredor");
		String tipoReporte = Sistema.TIPO_XLS;
		Integer idReaseguradorCorredor;
		try {
			if (UtileriasWeb.esCadenaVacia(idReaseguradorCorredorStr) || idReaseguradorCorredorStr.equalsIgnoreCase("undefined"))
				idReaseguradorCorredor = null;
			else
				idReaseguradorCorredor = new Integer(idReaseguradorCorredorStr);

			parametros.put("pIdReasegurador", idReaseguradorCorredor);
			parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicial));
			parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinal));
			ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
			reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.contratos.rptEstadisticaFiscal"), parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
		} catch (ParseException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
	}
	
}