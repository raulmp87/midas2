package mx.com.afirme.midas.contratos.linea;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDN;
import mx.com.afirme.midas.contratos.lineaparticipacion.LineaParticipacionDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDN;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDN;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class LineaDN {

	private static final LineaDN INSTANCIA = new LineaDN();
	private LineaSN lineaSN;
	
	public static LineaDN getInstancia() {
		
		return LineaDN.INSTANCIA;
	}

	public List<LineaDTO> listarTodos() 
			throws ExcepcionDeAccesoADatos, SystemException{
	
		lineaSN = new LineaSN();
		return lineaSN.listarTodos();
	}
	
	private LineaDTO obtenLineaLlenaPorId(String idLinea)
		throws SystemException, ExcepcionDeAccesoADatos{
		ConfiguracionLineaDTO configuracionLineaDTO = new ConfiguracionLineaDTO();
		LineaDTO lineaDTO = new LineaDTO();				
		List<LineaParticipacionDTO> participaciones = null;		
		lineaSN = new LineaSN();				
		LineaParticipacionDN lineaParticipacionDN = new LineaParticipacionDN();		
		lineaDTO.setIdTmLinea(new BigDecimal(idLinea.toCharArray()));
		configuracionLineaDTO.setIdTmLinea(lineaDTO.getIdTmLinea());		
		lineaDTO = lineaSN.getPorId(lineaDTO);		
		participaciones = ((List<LineaParticipacionDTO>)(lineaParticipacionDN.getPorPropiedad("id.idTmLinea", lineaDTO.getIdTmLinea())));
		lineaDTO.setLineaParticipaciones(participaciones);
		return lineaDTO;
	}
	
	public void desautorizarLinea(String idLinea)
			throws SystemException, ExcepcionDeAccesoADatos{
		LineaDTO lineaDTO = obtenLineaLlenaPorId(idLinea);
		lineaDTO.setEstatus(new BigDecimal(0));
		lineaSN.modificar(lineaDTO);
	}
	
	public Set<String> validarAutorizarCP(ContratoCuotaParteDTO contratoCuotaParteDTO)throws SystemException{
		Set<String> errores = new HashSet<String>();

		if (contratoCuotaParteDTO == null || 
				contratoCuotaParteDTO.getIdTmContratoCuotaParte() == null) {
			errores.add("error.contrato.cuotaParte.vacio");
		}
		
		double sumaParticipaciones = 0;
		double ppp=0;
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		
		if (Math.ceil(contratoCuotaParteDTO.getPorcentajeCesion().doubleValue()
				 + contratoCuotaParteDTO.getPorcentajeRetencion().doubleValue()) != 100){
			errores.add("error.contrato.cuotaParte.cesionRetencion.noEsIgualACien");
		}
		
		sumaParticipaciones = 0;
		ppp=0;
		List<ParticipacionDTO> listParticipaciones = null;;
		listParticipaciones = participacionDN.getPorPropiedad("contratoCuotaParte", 
				contratoCuotaParteDTO);//(contratoCuotaParteDTO, lineaDTO);//((List<ParticipacionDTO>)(participacionDN.getPorPropiedad("contratoCuotaParte", contratoCuotaParteDTO)));;
		contratoCuotaParteDTO.setParticipaciones(listParticipaciones);
		if (listParticipaciones != null && listParticipaciones.size()>0){
			Set<String> erroresParticipacion;
			for (ParticipacionDTO participacionDTO : listParticipaciones){
				ppp = participacionDTO.getPorcentajeParticipacion().doubleValue(); 
				erroresParticipacion = validarParticipacion(participacionDTO);
				if (erroresParticipacion.size() > 0){
					agregarErrores(errores, erroresParticipacion);
				}				
				sumaParticipaciones += ppp;
			}
		}
		if (Math.ceil(sumaParticipaciones) != 100) {
			errores.add("error.participaciones.sumaNoIgualACien");
		}
		
		return errores;
	}
	
	public Set<String> validarAutorizarPE(ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO)throws SystemException{
		Set<String> errores = new HashSet<String>();
		if (contratoPrimerExcedenteDTO==null || 
				contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente()==null) {
			errores.add("error.contrato.primerExcedente.vacio");
		}
		double sumaParticipaciones = 0;
		double ppp=0;
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
	
		sumaParticipaciones = 0;
		ppp = 0;
		List<ParticipacionDTO> listParticipaciones = contratoPrimerExcedenteDTO.getParticipacionDTOList();
		listParticipaciones = participacionDN.getPorPropiedad("contratoPrimerExcedente", contratoPrimerExcedenteDTO);//buscarPorLineaCPE(contratoPrimerExcedenteDTO, lineaDTO);//((List<ParticipacionDTO>)(participacionDN.getPorPropiedad("contratoPrimerExcedente", contratoPrimerExcedenteDTO)));;
		contratoPrimerExcedenteDTO.setParticipacionDTOList(listParticipaciones);
		if (listParticipaciones != null && listParticipaciones.size()>0){
			Set<String> erroresParticipacion;
			for (ParticipacionDTO participacionDTO : listParticipaciones){								
				ppp = participacionDTO.getPorcentajeParticipacion().doubleValue();
				erroresParticipacion = validarParticipacion(participacionDTO);
				if (erroresParticipacion.size() > 0){
					agregarErrores(errores, erroresParticipacion);
				}
				sumaParticipaciones += ppp;
			}
		}else{
			errores.add("error.participaciones.vacia");
		}
		
		if (Math.ceil(sumaParticipaciones) != 100) {
			errores.add("error.participaciones.sumaNoIgualACien");
		}		
		return errores;
	}
	
	@SuppressWarnings("unused")	
	public boolean autorizaLinea(LineaDTO lineaDTO)throws SystemException{
		if (lineaDTO == null || lineaDTO.getIdTmLinea()==null) return false;
		double sumaComisiones = 0;
		boolean esValidoParaAutorizar = true;	
		
		
		//if (esValidoParaAutorizar){
			/*
			 * Ejecuta el thread para la creaci�n de los estados de cuenta de la l�nea autorizada
			 */
			//EstadoCuentaGenerador generador = new EstadoCuentaGenerador();
			//generador.setLineaDTO(lineaDTO);
			
			//Thread hiloGenerador = new Thread(generador);
			//hiloGenerador.start();
			
			//TODO verificar si se validaran las comisiones de la linea
			/*
			LineaParticipacionSN lineaParticipacionDN = new LineaParticipacionSN();
			comisiones = lineaParticipacionDN.getPorPropiedad("linea", lineaDTO);
			for (LineaParticipacionDTO lineaParticipacionDTO : comisiones) {
				sumaComisiones += lineaParticipacionDTO.getComision().doubleValue();
			}
			if (sumaComisiones!=100 && (contratoCuotaParteDTO!=null || contratoPrimerExcedenteDTO!=null))
				esValidoParaAutorizar = false;
				*/
		/*}else{
			esValidoParaAutorizar = false;
		}*/
		
			
		return esValidoParaAutorizar;
	}
	
	public String autorizarLineas(String idLineas, Integer idUsuario)
		throws SystemException, ExcepcionDeAccesoADatos{
		String seActualizaronTodos = "true";
		StringTokenizer tokens = new StringTokenizer(idLineas,",");
		StringTokenizer tokensAux = new StringTokenizer(idLineas,",");
		String idLinea=null;	
		
		LineaDTO lineaDTO = new LineaDTO();		
		
		lineaSN = new LineaSN();	
		while(tokensAux.hasMoreTokens()){
			idLinea = (String)tokens.nextToken();
			lineaDTO = obtenLineaLlenaPorId(idLinea);
			ContratoCuotaParteDTO contratoCuotaParteDTO = new ContratoCuotaParteDTO();
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO = new ContratoPrimerExcedenteDTO();
			contratoCuotaParteDTO = lineaDTO.getContratoCuotaParte();
			contratoPrimerExcedenteDTO = lineaDTO.getContratoPrimerExcedente();
			SubRamoDTO subramoDTO = lineaDTO.getSubRamo();
			RamoDTO ramoDTO = subramoDTO.getRamoDTO();
			String retorno = "";
			if (contratoPrimerExcedenteDTO!=null){
				if (contratoPrimerExcedenteDTO.getEstatus().intValue() != 1)
					retorno = "error-" + idLinea + "-cpe-" +subramoDTO.getDescripcionSubRamo() + "-" + ramoDTO.getDescripcion();
			}
		
			if (contratoCuotaParteDTO!=null){
				if (contratoCuotaParteDTO.getEstatus().intValue() != 1){
					if(retorno.equals(""))
						retorno = "error-" + idLinea + "-ccp-" +subramoDTO.getDescripcionSubRamo() + "-" + ramoDTO.getDescripcion();
					else
						retorno = "error-" + idLinea + "-cpecp-" +subramoDTO.getDescripcionSubRamo() + "-" + ramoDTO.getDescripcion();
				}
			}
			if(!retorno.equals(""))
				return retorno;
		}
		while(tokens.hasMoreTokens()){
			idLinea = (String)tokens.nextToken();
			lineaDTO = obtenLineaLlenaPorId(idLinea);
			if (lineaDTO==null || lineaDTO.getIdTmLinea()==null)continue;
			lineaDTO.setEstatus(new BigDecimal(1));
			lineaDTO.setFechaAutorizacion(new Date());
			lineaDTO.setUsuarioAutorizo(new BigDecimal(idUsuario));
			//TODO falta mandar correo
			lineaSN.modificar(lineaDTO);
		}
		return seActualizaronTodos;
	}
	
	private Set<String> validarParticipacion(ParticipacionDTO participacion)
		throws SystemException, ExcepcionDeAccesoADatos{
		Set<String> errores = new HashSet<String>();
		double porcentajeParticipacion = 0;
		double porcentajeCorredor = 0;
	
		List<ParticipacionCorredorDTO> corredores = new ArrayList<ParticipacionCorredorDTO>();
		ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
		
		porcentajeParticipacion = participacion.getPorcentajeParticipacion();
		
		if (participacion.getTipo().intValue() == 0){
				corredores = participacionCorredorDN.getPorPropiedad("participacion", 
						participacion);
				porcentajeCorredor = 0;
				if (corredores==null || corredores.size()<1){
					errores.add("error.corredores.sinReaseguradores");
				}else{
					for (ParticipacionCorredorDTO reasegurador : corredores) {
						porcentajeCorredor += reasegurador.getPorcentajeParticipacion();
					}
					
					if (porcentajeCorredor != porcentajeParticipacion){ 
						errores.add("error.corredores.sumaTotalParticipacion");
					}
				}				
		}
		return errores;

	}
	
	public void renovarLineas(String fechaInicial,String fechaFinal,String estatus, String idTcRamo)
		throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		ConfiguracionLineaForm configuracionLineaForm = new ConfiguracionLineaForm();
		configuracionLineaForm.setFechaInicial(fechaInicial);
		configuracionLineaForm.setFechaFinal(fechaFinal);
		configuracionLineaForm.setEstatus(estatus);
		configuracionLineaForm.setIdTcRamo(idTcRamo);
		
		ConfiguracionLineaDTO  configuracionLineaDTO  = new ConfiguracionLineaDTO();
		ConfiguracionLineaDN configuracionLineaDN 	= ConfiguracionLineaDN.getInstancia();
	
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTmLinea()))
			configuracionLineaDTO.setIdTmLinea(new BigDecimal(configuracionLineaForm.getIdTmLinea()));
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getEstatus()))
			configuracionLineaDTO.setEstatus(new BigDecimal(configuracionLineaForm.getEstatus()));
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaInicial())){
			String fechaStr = configuracionLineaForm.getFechaInicial();
			Date fecha = UtileriasWeb.getFechaHoraFromString(fechaStr);
			configuracionLineaDTO.setFechaInicial(fecha);
		}
		
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getFechaFinal())){
			String fechaStr = configuracionLineaForm.getFechaFinal();
			Date fecha = UtileriasWeb.getFechaHoraFromString(fechaStr);
			configuracionLineaDTO.setFechaFinal(fecha);
		}
					
		if(!UtileriasWeb.esCadenaVacia(configuracionLineaForm.getIdTcRamo()))
			configuracionLineaDTO.setIdTcRamo(new BigDecimal(configuracionLineaForm.getIdTcRamo()));
		
		List<ConfiguracionLineaDTO> lineas = null;
		lineas = configuracionLineaDN.listarLineaFiltrado(configuracionLineaDTO);
		for (ConfiguracionLineaDTO configuracionLineaSeleccionada : lineas) {
			renovarLinea(configuracionLineaSeleccionada.getIdTmLinea().toString());
		}
	}
	
	public boolean validaEjercicio(LineaDTO lineaDTO)
		throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		LineaSN lineaSN = new LineaSN();
		return lineaSN.validaEjercicio(lineaDTO);
	}
	
	public void renovarLinea(String idLinea) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		LineaDTO lineaDTO = obtenLineaLlenaPorId(idLinea);
		List<ParticipacionDTO> pCCP = new ArrayList<ParticipacionDTO>();
		List<ParticipacionDTO> pCPE = new ArrayList<ParticipacionDTO>();
		ParticipacionDN participacionDN = ParticipacionDN.getInstancia();
		
		pCCP = participacionDN.buscarPorLineaCCP(lineaDTO.getContratoCuotaParte(), lineaDTO); 
		pCPE = participacionDN.buscarPorLineaCPE(lineaDTO.getContratoPrimerExcedente(), lineaDTO);
		
		Calendar cFecha= Calendar.getInstance();
		cFecha.setTime(lineaDTO.getFechaInicial());
		cFecha.add(Calendar.YEAR, 1);
		lineaDTO.setFechaInicial(cFecha.getTime());
		cFecha.setTime(lineaDTO.getFechaFinal());
		cFecha.add(Calendar.YEAR, 1);
		lineaDTO.setFechaFinal(cFecha.getTime());
		lineaDTO.setEstatus(new BigDecimal(0));
		
		lineaSN.duplicarLinea(lineaDTO,pCCP,pCPE);		
	}
	
	public LineaDTO agregar(LineaDTO lineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		List<LineaParticipacionDTO> lineaParticipaciones = new ArrayList<LineaParticipacionDTO>();
		if (!UtileriasWeb.esObjetoNulo(lineaDTO.getLineaParticipaciones()) && lineaDTO.getLineaParticipaciones().size() > 0)
			 lineaParticipaciones = lineaDTO.getLineaParticipaciones();
		
		lineaDTO.setLineaParticipaciones(null);				
		lineaSN = new LineaSN();
		
		LineaParticipacionDN lineaParticipacionDN = LineaParticipacionDN.getInstancia();
		
		//linea nueva
		if (lineaDTO.getIdTmLinea()==null && !lineaSN.validaEjercicio(lineaDTO))
			return null;
		
		//modificando linea
		if (lineaDTO.getIdTmLinea()!=null){
			LineaSN lineaSN = new LineaSN();
			LineaDTO lineaBD = new LineaDTO();
			lineaBD.setIdTmLinea(new BigDecimal(lineaDTO.getIdTmLinea().longValue()));
			lineaBD = lineaSN.getPorId(lineaDTO);
			if (lineaBD!=null && lineaBD.getSubRamo()!=null && lineaDTO.getSubRamo()!=null 
				&& lineaBD.getSubRamo().getIdTcSubRamo().intValue()!=lineaDTO.getSubRamo().getIdTcSubRamo().intValue()){
				if(!lineaSN.validaEjercicio(lineaDTO))return null;
			}
		}
		
		if (lineaDTO.getIdTmLinea() != null){
			lineaDTO = lineaSN.modificar(lineaDTO);
			for(LineaParticipacionDTO lineaParticipacion : lineaParticipaciones){
				lineaParticipacion.getId().setIdTmLinea(lineaDTO.getIdTmLinea());	
				lineaParticipacionDN.modificar(lineaParticipacion);
			}
		}
		else{
			lineaDTO = lineaSN.agregar(lineaDTO);
			for(LineaParticipacionDTO lineaParticipacion : lineaParticipaciones){
				lineaParticipacion.getId().setIdTmLinea(lineaDTO.getIdTmLinea());	
				lineaParticipacionDN.agregar(lineaParticipacion);
			}
		}
		
		return lineaDTO;
	}

	public void borrar(LineaDTO lineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		lineaSN = new LineaSN();
		lineaSN.borrar(lineaDTO);
	}

	public void modificar(LineaDTO lineaDTO)
	        throws SystemException, ExcepcionDeAccesoADatos{

		lineaSN = new LineaSN();
		lineaSN.modificar(lineaDTO);
		
	}

	public LineaDTO getPorId(LineaDTO lineaDTO) 
			throws SystemException, ExcepcionDeAccesoADatos{
		
		lineaSN = new LineaSN();
		return lineaSN.getPorId(lineaDTO);
	}
	
	public boolean actualizaFechasLineasPorIdLineas(String ids,String idsCCP,String idsCPE,Date fInicial,Date fFinal)
	throws SystemException, ExcepcionDeAccesoADatos{

		lineaSN = new LineaSN();
		return lineaSN.actualizaFechasLineasPorIdLineas(ids, idsCCP, idsCPE, fInicial, fFinal);
	}

	
	public LineaDTO obtenerLineaPorSubRamo(BigDecimal idTcSubRamo, int ejercicio){
		return null;
	}
	

	
	public List<EjercicioDTO> obtenerEjercicios() throws SystemException{
		LineaSN lineaSN = new LineaSN();
		return lineaSN.obtenerEjercicios();
	}
	
	public List<SuscripcionDTO> obtenerSuscripcionesPorLinea(BigDecimal idTcSubRamo,int ejercicio ,int tipoReaseguro) throws SystemException{
		LineaSN lineaSN = new LineaSN();
		return lineaSN.obtenerSuscripcionesPorLinea(idTcSubRamo, ejercicio, tipoReaseguro);
	}
	
	private Set<String> agregarErrores(Set<String> original, Set<String> elOtroSet) {
		for (String errorKey : elOtroSet) {
			original.add(errorKey);
		}
		return original;
	}
	
	public void desasociarContratoCPLinea(LineaDTO lineaDTO) throws SystemException{
		LineaSN lineaSN = new LineaSN();
		lineaSN.desasociarContratoCPLinea(lineaDTO);
	}
	
	public void desasociarContratoPELinea(LineaDTO lineaDTO) throws SystemException{
		LineaSN lineaSN = new LineaSN();
		lineaSN.desasociarContratoPELinea(lineaDTO);
	}
	
	public int obtenerEjercicioActual() throws SystemException{
		LineaSN lineaSN = new LineaSN();
		return lineaSN.obtenerEjercicioActual();
	}

	public BigDecimal obtenerTipoDistribucion(BigDecimal idToPoliza, BigDecimal idToCobertura) throws SystemException{
		BigDecimal tipoDistribucion = null;
		if(idToPoliza != null && idToCobertura != null)
			tipoDistribucion = new LineaSN().obtenerTipoDistribucion(idToPoliza, idToCobertura);
		return tipoDistribucion;
	}
}
