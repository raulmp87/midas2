package mx.com.afirme.midas.endoso.agrupacion;

import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AgrupacionEndosoSN {
	private AgrupacionEndosoFacadeRemote beanRemoto;

	public AgrupacionEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(AgrupacionEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(AgrupacionEndosoDTO agrupacionEndosoDTO) {
		try {
			beanRemoto.save(agrupacionEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
	public AgrupacionEndosoDTO findById(AgrupacionEndosoId agrupacionEndosoId){
	    try {
		return beanRemoto.findById(agrupacionEndosoId);
	} catch (EJBTransactionRolledbackException e) {
		throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
	}
	}
	
}
