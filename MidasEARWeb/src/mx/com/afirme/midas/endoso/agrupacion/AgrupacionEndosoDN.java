package mx.com.afirme.midas.endoso.agrupacion;

import mx.com.afirme.midas.sistema.SystemException;


public class AgrupacionEndosoDN {
    private static final AgrupacionEndosoDN INSTANCIA = new AgrupacionEndosoDN();
	
	public static AgrupacionEndosoDN getInstancia() {
		return INSTANCIA;
	}
	
    public void agregar(AgrupacionEndosoDTO agrupacionEndosoDTO) throws SystemException{
	new AgrupacionEndosoSN().agregar(agrupacionEndosoDTO);
    }
    
    public AgrupacionEndosoDTO obtenerPorID(AgrupacionEndosoId agrupacionEndosoId)throws SystemException{
	return   new AgrupacionEndosoSN().findById(agrupacionEndosoId);
    }
}
