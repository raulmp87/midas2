package mx.com.afirme.midas.endoso.cancelacion;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoCancelableSN {

	private EndosoCancelableFacadeRemote beanRemoto;

	public EndosoCancelableSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(EndosoCancelableFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public boolean notificaCancelacion(EndosoCancelableDTO endoso)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.notificaCancelacion(endoso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}

	}

	public List<EndosoCancelableDTO> findByProperty(String propertyName,
			final Object value) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoCancelableDTO update(
			EndosoCancelableDTO endosoCancelable)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(endosoCancelable);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

}