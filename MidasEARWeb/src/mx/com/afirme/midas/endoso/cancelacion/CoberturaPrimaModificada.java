package mx.com.afirme.midas.endoso.cancelacion;

import java.math.BigDecimal;

public class CoberturaPrimaModificada {
	
	private BigDecimal idToCotizacion;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private Double primaNeta;
	private BigDecimal numeroInciso;
	
	public CoberturaPrimaModificada() {
		
	}
	
	
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	
	
	
	
}
