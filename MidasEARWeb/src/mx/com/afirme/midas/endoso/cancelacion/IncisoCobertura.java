package mx.com.afirme.midas.endoso.cancelacion;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;

public class IncisoCobertura {

	private BigDecimal incisoId;
	private BigDecimal coberturaId;
	
	public IncisoCobertura() {
		
	}
	
	public IncisoCobertura(MovimientoCotizacionEndosoDTO movimiento) {
		
		incisoId = movimiento.getNumeroInciso();
		coberturaId = movimiento.getIdToCobertura();
		
	}
	
	public BigDecimal getIncisoId() {
		return incisoId;
	}



	public void setIncisoId(BigDecimal incisoId) {
		this.incisoId = incisoId;
	}



	public BigDecimal getCoberturaId() {
		return coberturaId;
	}



	public void setCoberturaId(BigDecimal coberturaId) {
		this.coberturaId = coberturaId;
	}


	public boolean equals(Object o) {
		
		  if(!(o instanceof IncisoCobertura)) {
			  return false;
		  }
		
		  IncisoCobertura incisoCobertura = (IncisoCobertura)o;
		  return incisoId.intValue() == incisoCobertura.getIncisoId().intValue()
		  && coberturaId.intValue() == incisoCobertura.getCoberturaId().intValue();
	}

	
}
