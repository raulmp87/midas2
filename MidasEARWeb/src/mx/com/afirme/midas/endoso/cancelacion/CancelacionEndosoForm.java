package mx.com.afirme.midas.endoso.cancelacion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CancelacionEndosoForm extends MidasBaseForm {
	
	private static final long serialVersionUID = -5139779464513982519L;
	
	private String idPoliza;

	public String getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}

	
	
}
