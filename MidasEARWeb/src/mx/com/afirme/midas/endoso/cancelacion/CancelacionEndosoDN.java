package mx.com.afirme.midas.endoso.cancelacion;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.transaction.Status;
import javax.transaction.UserTransaction;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.calculo.CalculoCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.facultativo.CotizacionSoporteDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDN;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDN;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoDN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionEndosoDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.tareas.endoso.TareaRehabilitacionEndoso;
import mx.com.afirme.midas.wsCliente.emision.EmisionDN;

import org.quartz.SchedulerException;
import org.quartz.ee.jta.UserTransactionHelper;

public class CancelacionEndosoDN {

	private static final CancelacionEndosoDN INSTANCIA = new CancelacionEndosoDN();
	
	private static UserTransaction utJob = null;
	
	private Short excluyeEndosoPagado = new Short("1");
	
	public static CancelacionEndosoDN getInstancia() {
		return CancelacionEndosoDN.INSTANCIA;
	}
	
	public static CancelacionEndosoDN getInstancia2(UserTransaction utJob) {
		if (utJob != null) {
			CancelacionEndosoDN.utJob = utJob;
		}
		return CancelacionEndosoDN.INSTANCIA;
	}

	
	public void cancelarEndoso (BigDecimal idToPoliza, Short numeroEndoso, Date fechaInicioVigencia, String nombreUsuario) throws SystemException {
		
		Short grupo;
		EndosoDTO ultimoEndoso;
		Short numeroEndosoCE;
		
		//Se valida que si el numero de endoso viene negativo, significa que es una cancelaci�n de endoso llamada por
		//una cancelaci�n de p�liza, por lo que debe permitir posteriormente cancelar el endoso aunque ya est� pagado.
		//En caso contrario, no se bebe permitircancelar un endoso pagado.
		if (numeroEndoso < 0) {
			excluyeEndosoPagado = new Short("-1");
			
			//Se restituye el numero de endoso original para su proceso de cancelaci�n
			numeroEndoso =  Short.parseShort((numeroEndoso.intValue() * -1 + ""));
		} else {
			excluyeEndosoPagado = new Short("1");
		}
		
		
		//Se valida que la poliza continue vigente antes de cancelar el endoso (Esto por si se esta ejecutando desde el job)
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		
		if (polizaDTO.getClaveEstatus().compareTo((short)0) == 0) { 
			if (nombreUsuario.trim().equals(Sistema.USUARIO_SISTEMA)) {
				return;	
			} else {
				throw new RuntimeException("La poliza no es vigente");
			}
		}
		
		
		
		//Obtiene una lista con los endosos que tienen que ser cancelados en cascada
		List<Short> endososACancelar = obtieneEndososACancelar(idToPoliza, numeroEndoso);
		
		grupo = endososACancelar.get(0);
		
		//Se invierten los elementos para ir cancelando desde el ultimo hasta el primer endoso
		Collections.reverse(endososACancelar);
		
		for (Short numeroEndosoACancelar : endososACancelar) {
			
			EndosoDTO endosoACancelar = EndosoDN.getInstancia("").getPorId(new EndosoId(idToPoliza, numeroEndosoACancelar));
			
			//Si el endoso aun no ha sido cancelado
			if (endosoACancelar.getCancela() == null) {
				//Se obtiene el ultimo endoso
				ultimoEndoso = EndosoDN.getInstancia("").getUltimoEndoso(idToPoliza);
				
				//Se crea y emite el endoso CE
				try {
					numeroEndosoCE = creaEndosoCancelacion(endosoACancelar, ultimoEndoso, fechaInicioVigencia, nombreUsuario, false, grupo);
					if (numeroEndosoCE == null) {
						throw new RuntimeException("Error al crear el endoso de cancelacion");
					}
				} catch (IllegalStateException e) {
					e.printStackTrace();
					throw new RuntimeException("Error al crear el endoso de cancelacion (IllegalStateException)");
				} catch (SecurityException e) {
					e.printStackTrace();
					throw new RuntimeException("Error al crear el endoso de cancelacion (SecurityException)");
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					throw new RuntimeException("Error al crear el endoso de cancelacion (ClassNotFoundException)");
				} catch (javax.transaction.SystemException e) {
					e.printStackTrace();
					throw new RuntimeException("Error al crear el endoso de cancelacion (javax.transaction.SystemException)");
				} 
								
				//El endoso a Cancelar se marca como cancelado usando el numero de Endoso CE
				endosoACancelar.setCancela(numeroEndosoCE);
				
			}
			endosoACancelar.setGrupo(grupo);
			
			//Se guardan los cambios en endosoACancelar 
			EndosoDN.getInstancia(nombreUsuario).actualizar(endosoACancelar);
			
		}
		
		
	}
	
	
	public List<EndosoDTO> listarEndososCancelables(BigDecimal idToPoliza)
			throws SystemException {
		return new CancelacionEndosoSN().listarEndososCancelables(idToPoliza);
	}
	
	public List<EndosoDTO> listarEndososCancelablesPoliza(BigDecimal idToPoliza)
			throws SystemException {
		return new CancelacionEndosoSN().listarEndososCancelablesPoliza(idToPoliza);
	}
	
	/**
	 * Obtiene los endosos posteriores al endoso a cancelar hasta el ultimo endoso
	 * @param idToPoliza Id de la Poliza
	 * @param numeroEndoso Numero de endoso a cancelar
	 * @return Un listado con los endosos posteriores al endoso a cancelar hasta el ultimo endoso
	 * @throws SystemException
	 */
	public List<EndosoDTO> listarPosiblesEndososCancelables(BigDecimal idToPoliza, Short numeroEndoso)
			throws SystemException {
		return new CancelacionEndosoSN().listarPosiblesEndososCancelables(idToPoliza, numeroEndoso);
	}
	
	
	public Short creaEndosoRehabilitacion(EndosoDTO endosoARehabilitar, EndosoDTO ultimoEndoso, Date fechaInicioVigencia,
			String nombreUsuario, Short grupo) 
		throws SystemException, ClassNotFoundException, IllegalStateException, SecurityException, javax.transaction.SystemException {
		return creaEndosoCancelacion(endosoARehabilitar, ultimoEndoso, fechaInicioVigencia, nombreUsuario, true, grupo);
	}
	
	/**
	 * Obtiene una lista con los numeros de los endosos que tienen que ser cancelados por los movimientos asociados a ellos
	 * @param idToPoliza Id de la poliza del endoso a cancelar
	 * @param numeroEndoso Numero de endoso a cancelar
	 * @return Lista con los numeros de los endosos que tienen que ser cancelados
	 * @throws SystemException
	 */
	public List<Short> obtieneEndososACancelar(BigDecimal idToPoliza, Short numeroEndoso) throws SystemException {
		
		try {
			EndosoDTO primerEndosoEval;
			List<Short> endososACancelar = new ArrayList<Short>();
			List <IncisoCobertura> incisosCoberturas = new ArrayList<IncisoCobertura>();
			List <IncisoCobertura> incisosCoberturasProspectos = new ArrayList<IncisoCobertura>();
			List<EndosoDTO> endososAEvaluar = listarPosiblesEndososCancelables (idToPoliza, numeroEndoso);
			List<MovimientoCotizacionEndosoDTO> movimientosEvaluar;
			IncisoCobertura incisoCoberturaMovimiento;
			boolean endosoCancelable = false;
			
			primerEndosoEval = EndosoDN.getInstancia("").getPorId(new EndosoId(idToPoliza, numeroEndoso));
			
			endososACancelar.add(primerEndosoEval.getId().getNumeroEndoso());
			
			if (endososAEvaluar != null && endososAEvaluar.size() > 0) {
				
				//Se obtienen los movimientos del endoso primerEndosoEval
				movimientosEvaluar = MovimientoCotizacionEndosoDN.getInstancia("")
					.obtieneMovimientosAEvaluar(primerEndosoEval.getIdToCotizacion());
										
				//Se llena incisosCoberturas con los distintos IncisoCobertura de los movimientos de primerEndosoEval
				for (MovimientoCotizacionEndosoDTO movimientoEvaluar : movimientosEvaluar) {
					incisoCoberturaMovimiento = new IncisoCobertura(movimientoEvaluar);
					if (!incisosCoberturas.contains(incisoCoberturaMovimiento) && 
							movimientoEvaluar.getClaveTipoMovimiento().shortValue() != Sistema.TIPO_MOV_MODIFICACION_PRIMA_NETA &&
							movimientoEvaluar.getClaveTipoMovimiento().shortValue() != Sistema.TIPO_MOV_MODIFICACION_COBERTURA) {
						incisosCoberturas.add(new IncisoCobertura(movimientoEvaluar));
					}
				}
				
				for (EndosoDTO endosoEval : endososAEvaluar) {
					//Se obtienen los movimientos del endoso endosoEval
					movimientosEvaluar = MovimientoCotizacionEndosoDN.getInstancia("")
						.obtieneMovimientosAEvaluar(endosoEval.getIdToCotizacion());
					
					for (MovimientoCotizacionEndosoDTO movimientoEvaluar : movimientosEvaluar) {
						incisoCoberturaMovimiento = new IncisoCobertura(movimientoEvaluar);
						if (!incisosCoberturas.contains(incisoCoberturaMovimiento)) {
							//Si el movimiento es de tipo alta de cobertura se cancelaran en cascada los endosos 
							//relacionados a ese inciso/cobertura
							if (movimientoEvaluar.getClaveTipoMovimiento().shortValue() == Sistema.TIPO_MOV_ALTA_COBERTURA) {
								incisosCoberturasProspectos.add(new IncisoCobertura(movimientoEvaluar));
							}
						} else {
							endosoCancelable = true;
						}
					}
					
					if (endosoCancelable) {
						for (IncisoCobertura extra : incisosCoberturasProspectos) {
							if (!incisosCoberturas.contains(extra)) {
								incisosCoberturas.add(extra);
							}
						}
						endosoCancelable = false;
						endososACancelar.add(endosoEval.getId().getNumeroEndoso());
					}
					incisosCoberturasProspectos.clear();
				}
			}
			
			return endososACancelar;
		
		} catch (SystemException ex) {
			ex.printStackTrace();
			throw ex;
		}
		
	}
	
	
	public Short creaEndosoCancelacion(EndosoDTO endosoACancelar, EndosoDTO ultimoEndoso, Date fechaInicioVigencia,
			String nombreUsuario, boolean esRehabilitacion, Short grupo) 
		throws SystemException, ClassNotFoundException, IllegalStateException, SecurityException, javax.transaction.SystemException {
			
//		-Emite el endosoCE
//		-Se obtiene el endosoCE
		EndosoDTO endosoCE = emiteEndosoCancelacion (endosoACancelar, ultimoEndoso, fechaInicioVigencia, nombreUsuario, 
				esRehabilitacion, grupo);
		
		if (endosoCE != null) {
		
//			-Se busca si existen movimientos que regeneren recibo o tengan valor en prima neta
			boolean notificarInterfaz = Boolean.FALSE;
			MovimientoCotizacionEndosoDTO movimientoEndosoDTO = new MovimientoCotizacionEndosoDTO();
			movimientoEndosoDTO.setIdToCotizacion(endosoCE.getIdToCotizacion());
			List<MovimientoCotizacionEndosoDTO> movimientosEndoso = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario)
				.listarFiltrado(movimientoEndosoDTO);
			
			for(MovimientoCotizacionEndosoDTO movimiento: movimientosEndoso){
				if(movimiento.getValorDiferenciaPrimaNeta().intValue() != 0 || 
					(movimiento.getClaveRegeneraRecibos() != null 
							&& movimiento.getClaveRegeneraRecibos().intValue() == Sistema.SI_REGENERA_RECIBOS)){
					notificarInterfaz = true;
					break;
				}
			}
			
//			-Se notifica a Seycos de la emision del endosoCE siempre y cuando el endoso sus movimientos hayan generado recibos 
//			o tengan valor en prima neta 		
			if (notificarInterfaz) {
				notificaEmision(endosoCE, nombreUsuario);
			}
			
//			-Se insertan estadisticas de la emision del endoso
			try{
				CotizacionDTO cotizacionEndoso = CotizacionDN.getInstancia(nombreUsuario).getPorId(endosoCE.getIdToCotizacion());
				EstadisticasEstadoDN.getInstancia().insertarEstadisticasAutomaticas(
						cotizacionEndoso.getSolicitudDTO().getIdToSolicitud(), cotizacionEndoso.getIdToCotizacion(),
						endosoCE.getId().getIdToPoliza(), BigDecimal.valueOf(endosoCE.getId().getNumeroEndoso().doubleValue()));
			}catch (Exception e){
				LogDeMidasWeb.log("creaEndosoCancelacion : Excepcion en EstadisticasEstadoDN.insertarEstadisticasAutomaticas con id de Poliza = " 
						+ endosoCE.getId().getIdToPoliza() + "Endoso Numero= " + endosoCE.getId().getNumeroEndoso(), Level.WARNING, e);
			}	
			
//			-Se notifica el endoso a reaseguro
			notificaEndosoAReaseguro (endosoCE, nombreUsuario, esRehabilitacion);
			
			
			
//			Regresa el numero de endoso de EndosoCE
			return endosoCE.getId().getNumeroEndoso();
		}
		return null;

////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////Cancelacion de Poliza//////////////////////////////////
		
//		-Se obtiene el endoso a cancelar (Viene de los endosos a cancelar que entrega Seycos por falta de pago (automatica)
// 			o seleccionada por el usuario (cancelacion manual)).
//		-Se obtiene el ultimo endoso de la poliza a la cual pertenece el endoso a cancelar
//		-Valida que el ultimo endoso de la poliza exista
//		-Valida que el numero de endoso del ultimo endoso de la poliza sea 0 (significa que es la poliza original)
//		-Valida que el ultimo endoso de la poliza no sea de cancelacion 
//			(significaria que la poliza ya fue cancelada y no se puede cancelar una poliza cancelada)
//		NOTA: Para el caso de emision de endosos de cancelacion de Poliza, revisar el orden de las validaciones, al parecer hay algunas de mas.
//		-Se obtiene la cotizacion de la poliza (endoso 0) a cancelar
//		-Se establece el porcentaje de Pago Fraccionado en la cotizacion
//		-Se calcula el IVA para la cotizacion (depende de donde viva el contratante)
//		-Revisa si existen recibos pagados para la poliza a cancelar
//		-Emite el endoso de cancelacion (*Ver detalles)
//		-Se obtiene el endoso de cancelacion (el ultimo endoso de la poliza)
//		-Se actualiza el estatus de la poliza a Cancelada
//		-Se generan movimientos de cancelacion (movimientos de baja de coberturas)
//		-Se notifica a Seycos de la emision del endoso de cancelacion
//		-Se insertan estadisticas de la emision del endoso
//		-Se notifica el endoso a reaseguro
//
//
//		* Emite el endoso de cancelacion
//		-Se crea una nueva solicictud de endoso de cancelacion a partir de la solicitud de la cotizacion de la poliza (endoso 0) a cancelar
//		-Se crea una nueva cotizacion de endoso de cancelacion a partir de la cotizacion de la poliza (endoso 0) a cancelar
//		-Se copian todas las relaciones (incisos, subincisos, secciones, coberturas, recargos, aumentos... etc) 
//			de la cotizacion de la poliza a cancelar a la nueva cotizacion de endoso de cancelacion
//		-Vuelve a validar que el ultimo endoso no sea de cancelacion
//		-Se calcula el numero para el nuevo endoso
//		-Se obtiene el tipo de endoso que ser� (cancelacion) a partir de la solicitud de endoso
//		-Se agrega el nuevo endoso a la Base de Datos
//		-Se calculan los dias por devengar
//		-Se establece el factor de aplicacion (dias por devengar / 365)
//		-Se crea toda la estructura del endoso y se calculan las primas, comisiones y riesgos del endoso
//		-Si es cancelacion y se cancelo en base a recibos, se actualiza el Factor Ajuste 
//			(Prima a Cancelar en base a Recibos / Prima a Cancelar en base a D�as No Devengados)
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
	}

	private EndosoDTO emiteEndosoCancelacion (EndosoDTO endosoACancelar, EndosoDTO ultimoEndoso, Date fechaInicioVigencia,
			String nombreUsuario, boolean esRehabilitacion, Short grupo) 
		throws SystemException, IllegalStateException, SecurityException, javax.transaction.SystemException {
		
		UserTransaction ut;
		try {
			
			if (CancelacionEndosoDN.utJob != null) {
				ut = CancelacionEndosoDN.utJob;
			} else {
				ut = UserTransactionHelper.lookupUserTransaction();
			}
			
			ut.setTransactionTimeout(Sistema.TIEMPO_LIMITE_TX_EMISION_CE_RE);
		} catch (SchedulerException e1) {
			e1.printStackTrace();
			throw new SystemException("fallo al crear la transaccion");
		}
		
		try {
			
			ut.begin();
			
			boolean existeReciboPagado = false;
			EndosoIDTO endosoIDTO = new EndosoIDTO();
			List<Object> objetosARecalcular = new ArrayList<Object>();
			Object objetoARecalcular;
			MovimientoCotizacionEndosoDTO movimientoInverso;
			MovimientoCotizacionEndosoDN movimientoCotizacionEndosoDN = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario) ;
			
			Integer tipoEndoso = (esRehabilitacion?Integer.parseInt(Sistema.TIPO_ENDOSO_RE + ""):excluyeEndosoPagado.intValue() * Sistema.TIPO_ENDOSO_CE);
			//(esRehabilitacion?Integer.parseInt(Sistema.TIPO_ENDOSO_RE + ""):Integer.parseInt(Sistema.TIPO_ENDOSO_CE + ""));
			
			
	//		*emision endosoCE
			EndosoDTO endosoAnterior = null;
			if (esRehabilitacion) {
				endosoAnterior = endosoACancelar;
			} else {
				endosoAnterior = obtieneEndosoAnterior(endosoACancelar.getId());
			}
			
			CotizacionDTO cotizacionEndosoAnterior = CotizacionDN.getInstancia(nombreUsuario).getPorId(endosoAnterior.getIdToCotizacion());
				
	//		-Se crea una nueva solicitud de endosoCE a partir de la solicitud de la cotizacion de ultimoEndoso
	//		-Se crea una nueva cotizacion de endosoCE a partir de la cotizacion de ultimoEndoso
	//		-Se copian todas las relaciones (incisos, subincisos, secciones, coberturas, recargos, aumentos... etc) de ultimoEndoso 
	//			a la nueva cotizacion de endosoCE
			CotizacionDTO cotizacionEndosoCE = copiaCotizacion(ultimoEndoso,(short)Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO, nombreUsuario);
						
	//		-Se obtienen los movimientos de endosoACancelar
			MovimientoCotizacionEndosoDTO filtro = new MovimientoCotizacionEndosoDTO();
			filtro.setIdToCotizacion(endosoACancelar.getIdToCotizacion());
			List<MovimientoCotizacionEndosoDTO> movimientosEndosoACancelar = movimientoCotizacionEndosoDN.listarFiltrado(filtro);
	
	//		-Se obtiene un arreglo con los inversos de los movimientos de endosoACancelar
	//		-Se aplican los inversos de los movimientos a la cotizacion de endosoCE
			for (MovimientoCotizacionEndosoDTO movimientoEndosoACancelar : movimientosEndosoACancelar) {
				//Si es rehabilitacion no se invierten los movimientos
				if (esRehabilitacion) {//copiaMovimiento(movimiento, Sistema.MSG_MOV_PREFIJO_CANCELADO)
					movimientoInverso = movimientoCotizacionEndosoDN.copiaMovimiento(movimientoEndosoACancelar, 
							Sistema.MSG_MOV_PREFIJO_REHABILITADO);
				} else {
					movimientoInverso = movimientoCotizacionEndosoDN.obtieneMovimientoInverso(movimientoEndosoACancelar);
				}
								
				objetoARecalcular = movimientoCotizacionEndosoDN.aplicaMovimientoACotizacion(movimientoInverso, cotizacionEndosoCE, 
						cotizacionEndosoAnterior, nombreUsuario);
				if (objetoARecalcular != null) {
					objetosARecalcular.add(objetoARecalcular);
				}
			}
			
	//		-Se establece el porcentaje de Pago Fraccionado en la cotizacion del EndosoCE
			FormaPagoIDTO formaPago = FormaPagoDN.getInstancia(nombreUsuario).getPorId(cotizacionEndosoCE.getIdFormaPago().intValue(),cotizacionEndosoCE.getIdMoneda().shortValue());
			if(formaPago != null){
				cotizacionEndosoCE.setPorcentajePagoFraccionado(formaPago.getPorcentajeRecargoPagoFraccionado().doubleValue());
			}else{
				cotizacionEndosoCE.setPorcentajePagoFraccionado(0D);
			}
	
	//		-Se aplican en negativo los derechos y el Valor de Recargo Pago Fraccionado proporcionados por el usuario si asi fue el caso
			CotizacionDTO cotizacionEndosoACancelar = CotizacionDN.getInstancia(nombreUsuario).getPorId(endosoACancelar.getIdToCotizacion());
			
			cotizacionEndosoCE.setClaveDerechosUsuario(cotizacionEndosoACancelar.getClaveDerechosUsuario());
			cotizacionEndosoCE.setClaveRecargoPagoFraccionadoUsuario(cotizacionEndosoACancelar.getClaveRecargoPagoFraccionadoUsuario());
			cotizacionEndosoCE.setPorcentajeRecargoPagoFraccionadoUsuario(cotizacionEndosoACancelar.getPorcentajeRecargoPagoFraccionadoUsuario());
			
			if (cotizacionEndosoACancelar.getValorDerechosUsuario() != null && !esRehabilitacion) {
				cotizacionEndosoCE.setValorDerechosUsuario(cotizacionEndosoACancelar.getValorDerechosUsuario() * -1D);
			} else {
				cotizacionEndosoCE.setValorDerechosUsuario(cotizacionEndosoACancelar.getValorDerechosUsuario());
			}
			
			if (cotizacionEndosoACancelar.getValorRecargoPagoFraccionadoUsuario() != null && !esRehabilitacion) {
				cotizacionEndosoCE.setValorRecargoPagoFraccionadoUsuario(cotizacionEndosoACancelar.getValorRecargoPagoFraccionadoUsuario() * -1D);
			} else {
				cotizacionEndosoCE.setValorRecargoPagoFraccionadoUsuario(cotizacionEndosoACancelar.getValorRecargoPagoFraccionadoUsuario());
			}
			
			
	//		-Se calcula el IVA para la cotizacion del EndosoCE (depende de donde viva el contratante)
			Double ivaCotizacion =CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionEndosoCE.getIdToCotizacion(), nombreUsuario);	
			if(ivaCotizacion==null){
			    throw new RuntimeException("No se pudo encontrar el valor para el IVA");
			}
			
			
			/**
			 * BLOQUE
			 * 
			 * Original:
			 * 
			 * se consultan los recibos del endoso 0 (la poliza original)
			 * 
			 * si hay recibos emitidos y/o pagados se continua con la emision (ya que existe una fecha de inicio de vigencia)
			 * si es un endoso de cancelacion
			 * 	  Se valida ultimo endoso para ver si es cancelable
			 *    Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del ultimo recibo pagado o el 1 emitido
			 * 
			 * 
			 * CE:
			 * 
			 * se consultan los recibos del endoso a cancelar (el endoso original)
			 *  
			 *(si hay recibos emitidos y/o pagados se continua con la emision (ya que existe una fecha de inicio de vigencia))
			 * Esto se omite ya que tanto para CE como para RE se hacen sobre endosos ya emitidos
			 * 
			 * se valida ultimo endoso para ver si es cancelable (aplica tambien para rehabilitacion)
			 * se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del ultimo recibo pagado o el 1 emitido
			 * 
			 */
	
				
	//			-Revisa si existen recibos pagados para el endoso a cancelar
				List<ReciboDTO> recibosEndosoACancelar = ReciboDN.getInstancia(nombreUsuario).consultaRecibos(endosoACancelar.getId().getIdToPoliza(),
						endosoACancelar.getId().getNumeroEndoso());
				
				for(ReciboDTO reciboDTO: recibosEndosoACancelar){
					if (reciboDTO.getSituacion().trim().equals(Sistema.RECIBO_PAGADO)){
						existeReciboPagado = true;
					}
				}
				
	//			Se actualiza la fecha de inicio de vigencia con la establecida por MIDAS (por el usuario en caso de cancelacion manual
	//			y por SEYCOS en caso de autom�tica)
				cotizacionEndosoCE.setFechaInicioVigencia(fechaInicioVigencia);
				
//				Se actualiza la fecha de fin de vigencia con la fecha de fin de vigencia del endoso a cancelar
//				Este cambio invalida la cancelacion de movimiento de fin de vigencia, se deber� de entender 
//				que no existen cambios de fin de vigencia en la poliza, solo endosos con diferentes fechas de fin de vigencia
//				TODO: Comentar esta linea si se decide que si debe haber movimientos de cambio de fin de vigencia en la poliza
				cotizacionEndosoCE.setFechaFinVigencia(endosoACancelar.getFechaFinVigencia());
				
				
	//			Se actualiza el motivo del endoso para que Seycos posteriormente pueda distinguir si la cancelaci�n fu� a petici�n 
	//			� autom�tica
				if (nombreUsuario.trim().equals(Sistema.USUARIO_SISTEMA)) {
					cotizacionEndosoCE.setClaveMotivoEndoso(Short.parseShort(Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO + ""));
				} else {
					cotizacionEndosoCE.setClaveMotivoEndoso(Short.parseShort(Sistema.MOTIVO_A_PETICION_DEL_ASEGURADO + ""));
				}
				
				
				if (nombreUsuario.trim().equals(Sistema.USUARIO_SISTEMA) 
						&& esRehabilitacion) {
					EndosoRehabilitableDTO endosoRehabilitable = null;
					EndosoRehabilitableId id = new EndosoRehabilitableId();
					id.setIdToPoliza(endosoACancelar.getId().getIdToPoliza());
					id.setNumeroEndoso(endosoACancelar.getId().getNumeroEndoso());
					endosoRehabilitable = EndosoRehabilitableDN.getInstancia().encuentraPorIdRefrescado(id);
					
					endosoIDTO = new TareaRehabilitacionEndoso().convierteObjetoEndoso(endosoRehabilitable);
					endosoIDTO.setEsValido(Short.valueOf("1"));
					endosoIDTO.setCalculo(Sistema.TIPO_CALCULO_SEYCOS);
					
				} else {			
	//			Se valida ultimo endoso para ver si es cancelable (aplica tambien para rehabilitacion)
	//			-Se valida que el endoso sea cancelable en Seycos (esto tambien devuelve el monto de la prima por la cual se 
	//			 va a cancelar y establece si el calculo que se realizara sera TIPO_CALCULO_SEYCOS o TIPO_CALCULO_MIDAS)
				endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(
							endosoACancelar.getId().getIdToPoliza().toString() + "|"
									+ endosoACancelar.getId().getNumeroEndoso(), nombreUsuario,
									cotizacionEndosoCE.getFechaInicioVigencia(), tipoEndoso);
				}
				
	//			Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del ultimo recibo pagado o el 1 emitido
				cotizacionEndosoCE.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
				
				if (endosoIDTO.getEsValido() == null || endosoIDTO.getEsValido().intValue() == 0) {
									
					LogDeMidasWeb.log("El endoso " + endosoACancelar.getId().getIdToPoliza().toString() + "|"
							+ endosoACancelar.getId().getNumeroEndoso() + " no podra ser cancelado: " 
							+ endosoIDTO.getMotivoNoCancelable(), Level.WARNING, null);
					throw new Exception("Excepcion al obtener validacion de cancelacion " 
							+ endosoACancelar.getId().getIdToPoliza().toString() + "|"
							+ endosoACancelar.getId().getNumeroEndoso() + ": "
							+ endosoIDTO.getMotivoNoCancelable());
				}
				
				if (endosoIDTO.getCalculo() == null) {
//					endosoIDTO.setCalculo(Sistema.TIPO_CALCULO_MIDAS);//TODO: Quitar cuando terminen las pruebas sin soporte de Seycos
					LogDeMidasWeb.log("El endoso " + endosoACancelar.getId().getIdToPoliza().toString() + "|"
							+ endosoACancelar.getId().getNumeroEndoso() 
							+ " no obtuvo el tipo de calculo a efectuar sobre la prima...", Level.WARNING, null);
					throw new Exception("Excepcion al obtener validacion de cancelacion "
							+ endosoACancelar.getId().getIdToPoliza().toString() + "|"
							+ endosoACancelar.getId().getNumeroEndoso() + 
							": no obtuvo el tipo de calculo a efectuar sobre la prima");
				}
				
			/**
			 * FIN BLOQUE	
			 */
			
		
			
	//		-Se guardan los cambios a la cotizacion de endosoCE
			CotizacionDN.getInstancia(nombreUsuario).modificar(cotizacionEndosoCE);	
			
//			Se hace commit despues de actualizar la cotizacion del endosoCE (esto debido a que se necesitan hacer unos calculos sobre la
//					cotizacion usando unos stored procedures que necesitan que la informacion ya este planchada en la BD)
			ut.commit();
			
			//Se realizan los calculos de actualizacion sobre cotizacionEndosoCE
			for (Object objetoEstructuraCotizacionEndoso : objetosARecalcular) {
				aplicaCalculosCotizacion(objetoEstructuraCotizacionEndoso, nombreUsuario);
			}
			
			
			
	//		Se validan los cumulos de reaseguro, en caso de que haya cumulos que requieren facultativo se notificar� a reaseguro 
			validaReaseguroFacultativoEndosoCE(endosoACancelar.getId().getIdToPoliza(),
					ultimoEndoso.getId().getNumeroEndoso().intValue(),
					endosoACancelar.getId().getNumeroEndoso().intValue(),
					cotizacionEndosoCE.getIdToCotizacion(), nombreUsuario);
			
			ut.begin();
	//		-Se agrega el tipo de endoso CE (7) al campo clavetipoendoso
	//		-Se agrega numeroendosoACancelar al campo cancela
	//		-Se calcula el numero para el nuevo endoso
	//		numeroUltimoEndoso = ultimoEndoso.getId().getNumeroEndoso();
	//		numeroEndosoCE = Short.parseShort((numeroUltimoEndoso + 1) + "");
	//		-Se agrega el nuevo endoso a la Base de Datos
	//		-Se calculan los dias por devengar		
	//		-Se establece el factor de aplicacion (dias por devengar / 365) 
	//		-Se crea toda la estructura del endoso y se calculan las primas, comisiones y riesgos del endoso
	//		-Si se cancelo en base a recibos en base a si TIPO_CALCULO_SEYCOS o TIPO_CALCULO_MIDAS, se actualizan el Factor Ajuste 
	//			(Prima a Cancelar en base a Recibos / Prima a Cancelar en base a D�as No Devengados) y el factor de Aplicacion
									
			EndosoDTO endosoCE = creaEndosoCE(cotizacionEndosoCE, endosoACancelar, ultimoEndoso, 
					ivaCotizacion, existeReciboPagado, recibosEndosoACancelar, endosoIDTO, esRehabilitacion, grupo);
			
			ut.commit();
			
			return endosoCE;
		} catch(Exception e) {
			e.printStackTrace();
			if (ut.getStatus() != Status.STATUS_NO_TRANSACTION) {
				ut.rollback();
			}
			throw new RuntimeException("Excepcion en la transaccion..."+(e.getMessage() != null ? e.getMessage() : ""));
		}
	}
	
	
	private void notificaEmision(EndosoDTO endosoDTO, String nombreUsuario) {
		List<ReciboDTO> reciboDTOList = null;
		List<EndosoIDTO> listaEndosoTmp = null;
		EndosoIDTO endosoIDTO = null;
		String llaveFiscal = null;
		String fileName = null;
		
		try {
			
			LogDeMidasWeb.log("notificaEmision : Ejecutando EndosoIDN.emiteEndoso con id de Poliza|NoEndoso = " + 
					endosoDTO.getId().getIdToPoliza().toString()+ "|" +endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);
			
			listaEndosoTmp = EndosoIDN.getInstancia().emiteEndoso(endosoDTO.getId().getIdToPoliza().toString()+ "|" 
					+ endosoDTO.getId().getNumeroEndoso(), nombreUsuario);
			
			if (listaEndosoTmp != null && listaEndosoTmp.size() > 0) {
				LogDeMidasWeb.log("notificaEmision : EndosoIDN.emiteEndoso con id de Poliza|NoEndoso " + 
						endosoDTO.getId().getIdToPoliza().toString()+ "|" + endosoDTO.getId().getNumeroEndoso() 
						+ " ejecutado.", Level.INFO, null);
				endosoIDTO =  listaEndosoTmp.get(0);							
			}
		} catch (Exception e) {
			LogDeMidasWeb.log("notificaEmision : Excepcion en EndosoIDN.emiteEndoso con id de Poliza = " + 
					endosoDTO.getId().getIdToPoliza() + " Endoso Numero= "+ endosoDTO.getId().getNumeroEndoso(), Level.WARNING, e);
			LogDeMidasWeb.log("notificaEmision : Ejecutando ReciboDN.consultaRecibos con id de Poliza = " + 
					endosoDTO.getId().getIdToPoliza()  + " Endoso Numero= "+ endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);

			LogDeMidasWeb.log("notificaEmision : consultando recibos de endoso " + 
					endosoDTO.getId().getIdToPoliza()  + " Endoso Numero= " + endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);
			try {
				reciboDTOList = ReciboDN.getInstancia(nombreUsuario)
					.consultaRecibos(endosoDTO.getId().getIdToPoliza(),endosoDTO.getId().getNumeroEndoso());
				
				LogDeMidasWeb.log("notificaEmision : " + reciboDTOList.size() + " recibo(s) obtenido(s).", Level.INFO, null);
			} catch (Exception e1) {
				LogDeMidasWeb.log("notificaEmision : error al consultar los recibos...", Level.INFO, null);
				LogDeMidasWeb.log("notificaEmision : segundo intento...", Level.INFO, null);
				try {
					reciboDTOList = ReciboDN.getInstancia(nombreUsuario)
						.consultaRecibos(endosoDTO.getId().getIdToPoliza(),endosoDTO.getId().getNumeroEndoso());
					
					LogDeMidasWeb.log("notificaEmision (segundo intento): " + reciboDTOList.size() + " recibo(s) obtenido(s).", Level.INFO, null);
				} catch (Exception e2) {
					LogDeMidasWeb.log("notificaEmision (segundo intento) : error al consultar los recibos...", Level.INFO, null);
				}
		
			}
							
		}
				
		try {
			
			if (endosoIDTO != null && endosoIDTO.getLlaveFiscal() != null) {
				llaveFiscal = endosoIDTO.getLlaveFiscal();
			} else if (reciboDTOList != null && reciboDTOList.size() > 0) {
				llaveFiscal = reciboDTOList.get(0).getLlaveFiscal();
			}
			
			LogDeMidasWeb.log("notificaEmision : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);
			
			try {
				EmisionDN.getInstancia().generarRecibos();
			} catch (Exception e) {
				LogDeMidasWeb.log("notificaEmision : error al generar los recibos", Level.INFO, null);
			}
			LogDeMidasWeb.log("notificaEmision : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
			
			if (endosoDTO != null && llaveFiscal != null) {
				endosoDTO.setLlaveFiscal(llaveFiscal);
				EndosoDN.getInstancia(nombreUsuario).actualizar(endosoDTO);
			}	
			
		} catch (Exception e) {
			LogDeMidasWeb.log("notificaEmision : error al generar los recibos", Level.INFO, null);
		}
		LogDeMidasWeb.log("notificaEmision : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);

		
		byte[] archivoPDF = null;

		if(llaveFiscal != null){
			try{
				archivoPDF = EmisionDN.getInstancia().obtieneFactura(llaveFiscal);
			}catch (Exception e){
				LogDeMidasWeb.log("notificaEmision: error al obtener la factura con llave fiscal:"+ llaveFiscal, Level.INFO, null);
			}
		}
		
		try {
			if (archivoPDF != null) {
				if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1)
					fileName = Sistema.UPLOAD_FOLDER;
				else
					fileName = Sistema.LINUX_UPLOAD_FOLDER;
				fileName = fileName + "Recibo_Poliza" + endosoDTO.getId().getIdToPoliza().toBigInteger() +"_Endoso_" 
					+ endosoDTO.getId().getNumeroEndoso() + ".pdf";
				FileOutputStream fos = new FileOutputStream(fileName);
				fos.write(archivoPDF);
				fos.flush();                                    
				fos.close();
				LogDeMidasWeb.log("Se generaron los recibos del endoso :"+ endosoDTO.getId().getIdToPoliza().toBigInteger() 
						+ "-"+ endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);						
			} else {
				LogDeMidasWeb.log("NO se generaron los recibos del endoso :"+ endosoDTO.getId().getIdToPoliza().toBigInteger() 
						+ "-"+ endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			LogDeMidasWeb.log("NO se generaron los recibos del endoso :"+ endosoDTO.getId().getIdToPoliza().toBigInteger() 
					+ "-"+ endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);
		}
	}
	
	private void notificaEndosoAReaseguro(EndosoDTO endosoEmitido, String nombreUsuario, boolean esRehabilitacion) {
		String modulo = "cancelacion";
		
		if (esRehabilitacion) {
			modulo = "rehabilitacion";
		}
			
		try {
			
			SoporteReaseguroCotizacionEndosoDN soporteEndoso = new SoporteReaseguroCotizacionEndosoDN (endosoEmitido.getId().getIdToPoliza(), 
					endosoEmitido.getId().getNumeroEndoso().intValue() -1, endosoEmitido.getIdToCotizacion(), false);
			
			soporteEndoso.notificarEmision(
					endosoEmitido.getId().getIdToPoliza(), 
					endosoEmitido.getId().getNumeroEndoso().intValue(), 
					endosoEmitido.getFechaCreacion());
			
		} catch (Exception e) {
			
			String descError = "Ocurri� un error al realizar la notificaci�n de emisi�n a reaseguro. idToPoliza: " 
				+ endosoEmitido.getId().getIdToPoliza() + ", numeroEndoso: " + endosoEmitido.getId().getNumeroEndoso();
			
			UtileriasWeb.registraLogInteraccionReaseguro("notificaEndosoAReaseguro", 
					Sistema.MODULO_REASEGURO, "SoporteReaseguro", "notificarEmision", 
					nombreUsuario, 
					"idToCotizacion = " + endosoEmitido.getIdToCotizacion() 
					+ " idToPoliza = "+ endosoEmitido.getId().getIdToPoliza() + " noEndoso = "+ endosoEmitido.getId().getNumeroEndoso() 
					+ " fechaCreacionEndoso = " + endosoEmitido.getFechaCreacion(), 
					"void()", e, descError, "error al notificar la emision de un endoso de " + modulo + " de endoso automatica",
					"CotizacionDN", Sistema.MODULO_DANIOS);
		}
	}
	
	private void validaReaseguroFacultativoEndosoCE(
			BigDecimal idToPoliza, Integer ultimoEndoso,
			Integer numeroEndosoCancelado,
			BigDecimal idToCotizacion, String nombreUsuario) throws SystemException {
		
			CotizacionSoporteDN cotizacionSoporteDN = new CotizacionSoporteDN();
			//implementar obtencion del USUARIO
//			Usuario usuario = new Usuario();
//			usuario.setNombreUsuario(nombreUsuario);
			cotizacionSoporteDN.validarReaseguroFacultativoEndosoV2(idToCotizacion, nombreUsuario, null, null,false);
			
			SoporteReaseguroCotizacionEndosoDN soporteReaseguroEndoso = new SoporteReaseguroCotizacionEndosoDN(
					idToPoliza, ultimoEndoso, idToCotizacion, false);
			
			soporteReaseguroEndoso.copiarPorcentajesDistribucionEndosoCancelado(idToPoliza,numeroEndosoCancelado);
			
//			SoporteReaseguroCotizacionDN soporteReaseguro = new SoporteReaseguroCotizacionDN(idToCotizacion,false);
//			
//			soporteReaseguro.validarCancelacionEndoso();
		
	}
	
	
	private void aplicaCalculosCotizacion(Object objetoEstructuraCotizacionEndoso, String nombreUsuario) 
		throws ExcepcionDeAccesoADatos, SystemException {
		
		if(objetoEstructuraCotizacionEndoso instanceof CoberturaCotizacionDTO) {
			CoberturaCotizacionDTO coberturaCotizacion = (CoberturaCotizacionDTO) objetoEstructuraCotizacionEndoso;
			CalculoCotizacionDN.getInstancia().calcularCobertura(coberturaCotizacion, nombreUsuario, false);
		} else if(objetoEstructuraCotizacionEndoso instanceof CoberturaPrimaModificada) {
			CoberturaPrimaModificada coberturaPrimaMod = (CoberturaPrimaModificada) objetoEstructuraCotizacionEndoso;
			
			CotizacionDN.getInstancia(nombreUsuario).igualarPrimas(
					coberturaPrimaMod.getIdToCotizacion(),
					coberturaPrimaMod.getIdToSeccion(), 
					coberturaPrimaMod.getIdToCobertura(), 
					coberturaPrimaMod.getPrimaNeta(), 
					coberturaPrimaMod.getNumeroInciso(),
					nombreUsuario,
					true);
			
		} else if(objetoEstructuraCotizacionEndoso instanceof SubIncisoCotizacionDTO) {
			SubIncisoCotizacionDTO subIncisoCotizacion = (SubIncisoCotizacionDTO) objetoEstructuraCotizacionEndoso;
			CalculoCotizacionDN.getInstancia().calcularSubInciso(subIncisoCotizacion, nombreUsuario);
		} else if (objetoEstructuraCotizacionEndoso instanceof RiesgoCotizacionDTO) {
			RiesgoCotizacionDTO riesgoCotizacion = (RiesgoCotizacionDTO) objetoEstructuraCotizacionEndoso;
			CalculoCotizacionDN.getInstancia().calcularRiesgo(riesgoCotizacion, nombreUsuario);
		}
	}
	
	
	private CotizacionDTO copiaCotizacion(EndosoDTO ultimoEndoso, short motivo, String usuarioCreacion) 
	throws SystemException {
		return new CancelacionEndosoSN().copiaCotizacion(ultimoEndoso, motivo, usuarioCreacion);
	}
	
	
	private EndosoDTO creaEndosoCE(CotizacionDTO cotizacionEndosoCE, EndosoDTO endosoACancelar, EndosoDTO ultimoEndoso, 
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, EndosoIDTO endosoIDTO, boolean esRehabilitacion, 
			Short grupo) 
		throws SystemException {
		return new CancelacionEndosoSN().creaEndosoCE(cotizacionEndosoCE, endosoACancelar, ultimoEndoso, ivaCotizacion, 
				existeReciboPagado, recibos, endosoIDTO, esRehabilitacion, grupo);
	}
	
	private EndosoDTO obtieneEndosoAnterior (EndosoId id) throws SystemException {
		return new CancelacionEndosoSN().obtieneEndosoAnterior(id);
	}
	
	
	
	
}
