package mx.com.afirme.midas.endoso.cancelacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class CancelacionEndosoSN {

	private CancelacionEndosoFacadeRemote beanRemoto;
	
	public CancelacionEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CancelacionEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<EndosoDTO> listarEndososCancelables(BigDecimal idToPoliza) {
		
		return beanRemoto.listarEndososCancelables(idToPoliza);
		
	}
	
	public List<EndosoDTO> listarEndososCancelablesPoliza(BigDecimal idToPoliza) {
		return beanRemoto.listarEndososCancelablesPoliza(idToPoliza);
	}
	
	public List<EndosoDTO> listarPosiblesEndososCancelables(BigDecimal idToPoliza, Short numeroEndoso) {
		
		return beanRemoto.listarPosiblesEndososCancelables(idToPoliza, numeroEndoso);
		
	}
	
	public CotizacionDTO copiaCotizacion(EndosoDTO ultimoEndoso, short motivo, String usuarioCreacion) {
		return beanRemoto.copiaCotizacion(ultimoEndoso, motivo,usuarioCreacion);
	}
	
	
	public EndosoDTO creaEndosoCE(CotizacionDTO cotizacionEndosoCE, EndosoDTO endosoACancelar, EndosoDTO ultimoEndoso, 
			Double ivaCotizacion, boolean existeReciboPagado,  List<ReciboDTO> recibos, EndosoIDTO endosoIDTO, boolean esRehabilitacion, 
			Short grupo) {
		return beanRemoto.creaEndosoCE(cotizacionEndosoCE, endosoACancelar, ultimoEndoso, ivaCotizacion, existeReciboPagado, recibos,
				endosoIDTO, esRehabilitacion, grupo);
	}
	
	public EndosoDTO obtieneEndosoAnterior (EndosoId id) {
		return beanRemoto.obtieneEndosoAnterior(id);
	}
	
	public void copiaIncisoConRelaciones (CotizacionDTO cotizacionOrigen, CotizacionDTO cotizacionDestino, BigDecimal numeroInciso) {
		beanRemoto.copiaIncisoConRelaciones(cotizacionOrigen, cotizacionDestino, numeroInciso);
	} 
	
}
