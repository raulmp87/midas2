package mx.com.afirme.midas.endoso.cancelacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoCancelableDN {

	private static final EndosoCancelableDN INSTANCIA = new EndosoCancelableDN();

	public static EndosoCancelableDN getInstancia() {
		return EndosoCancelableDN.INSTANCIA;
	}

	public boolean notificaCancelacion(EndosoCancelableDTO endoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoCancelableSN().notificaCancelacion(endoso);
	}

	public List<EndosoCancelableDTO> listaRegistrosSinProcesar()
			throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoCancelableSN().findByProperty("estatusRegistro",
				EndosoCancelableDTO.ESTATUS_REGISTRO_SIN_PROCESAR);
	}

	public EndosoCancelableDTO encuentraPorIdRefrescado(
			EndosoCancelableId id) throws ExcepcionDeAccesoADatos,
			SystemException {
		List<EndosoCancelableDTO> endosoCancelableList = new EndosoCancelableSN()
				.findByProperty("id", id);

		if (endosoCancelableList != null
				&& !endosoCancelableList.isEmpty()) {
			return endosoCancelableList.get(0);
		}

		return null;
	}

	public EndosoCancelableDTO actualiza(
			EndosoCancelableDTO endosoCancelable)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoCancelableSN().update(endosoCancelable);
	}
}
