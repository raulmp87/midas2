package mx.com.afirme.midas.endoso.cancelacion;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CancelacionEndosoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			CancelacionEndosoForm cancelacionEndosoForm = (CancelacionEndosoForm) form;
			
			String idPoliza = cancelacionEndosoForm.getIdPoliza();
			
			if(idPoliza == null) {
				idPoliza = request.getParameter("idToPoliza");
			}
				
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
			
			List<EndosoDTO> listaEndososPoliza = CancelacionEndosoDN.getInstancia().listarEndososCancelables(idToPoliza);
			if (listaEndososPoliza == null) {
				listaEndososPoliza = new ArrayList<EndosoDTO>();
			}
			cancelacionEndosoForm.setIdPoliza(idPoliza);
			request.setAttribute("listaEndososCancelables", listaEndososPoliza);
		
		} catch (Exception e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);
				
		
	}
	
	public void cancelarEndoso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String tipoMensaje = Sistema.EXITO;
		String mensajeError = "";
		try {
			
			String idPoliza = request.getParameter("idToPoliza");
			String numeroEndosoStr = request.getParameter("numeroEndoso");
			String sFechaIniVigencia = request.getParameter("fechaIniVigencia");
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaIniVigencia = null;
			try {
				if(UtileriasWeb.esCadenaVacia(sFechaIniVigencia))
					throw new RuntimeException("Introduzca la fecha de Inicio de Vigencia");
				fechaIniVigencia = df.parse(sFechaIniVigencia);
				if (fechaIniVigencia == null) {
					throw new RuntimeException("Introduzca una fecha de Inicio de Vigencia Valida");
				}
			} catch (ParseException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
			Short numeroEndoso = UtileriasWeb.regresaShort(numeroEndosoStr);
			
			//Cancelar el endoso
			CancelacionEndosoDN.getInstancia().cancelarEndoso(idToPoliza, numeroEndoso, fechaIniVigencia, 
					UtileriasWeb.obtieneNombreUsuario(request));			
			
		
		} catch (SystemException e) {
			tipoMensaje = Sistema.INFORMACION;
		} catch (RuntimeException e) {
			e.printStackTrace();
			tipoMensaje = Sistema.ERROR;
			if(e.getMessage() != null)
				mensajeError = e.getMessage();
		} finally {
			//Mensaje de respuesta al usuario
			UtileriasWeb.imprimeMensajeXML(tipoMensaje, obtieneMensajeRespuesta(tipoMensaje,mensajeError), response);
		}
	}
	
	public void validarEndosoCancelable(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String tipoMensaje = Sistema.EXITO;
		String mensaje = "";
		try {
			
			String idPoliza = request.getParameter("idToPoliza");
			String numeroEndosoStr = request.getParameter("numeroEndoso");
			
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
			Short numeroEndoso = UtileriasWeb.regresaShort(numeroEndosoStr);
			
			//Cancelar el endoso
			List<Short> listaCancelables = CancelacionEndosoDN.getInstancia().obtieneEndososACancelar(idToPoliza, numeroEndoso);
			
			if(listaCancelables.size() == 1){
				mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.confirmar",numeroEndosoStr);
			}
			else{
				mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.confirmar.cancelacionencascada",
						numeroEndosoStr,listaCancelables.toString());
			}
			
		
		} catch (SystemException e) {
			tipoMensaje = Sistema.INFORMACION;
		} catch (RuntimeException e) {
			e.printStackTrace();
			tipoMensaje = Sistema.ERROR;
			if(e.getMessage() != null)
				mensaje = e.getMessage();
		} finally {
			//Mensaje de respuesta al usuario
			UtileriasWeb.imprimeMensajeXML(tipoMensaje, mensaje, response);
		}
	}
	
	
	private String obtieneMensajeRespuesta (String tipoMensaje,String mensajeError) {
		String mensaje = "";
		if (tipoMensaje.equals(Sistema.EXITO)) {
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.mensaje.exito");
		} else if (tipoMensaje.equals(Sistema.INFORMACION)) {
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.sistema.nodisponible");
		} else {
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.rollback");
			if(mensajeError != null)
				mensaje += "Detalle error: "+mensajeError;
		}
		
		return mensaje;
	}
	
}
