package mx.com.afirme.midas.endoso.cobertura;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaEndosoSN {
	private CoberturaEndosoFacadeRemote beanRemoto;

	public CoberturaEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CoberturaEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(CoberturaEndosoDTO coberturaEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(coberturaEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CoberturaEndosoDTO modificar(CoberturaEndosoDTO coberturaEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(coberturaEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CoberturaEndosoDTO getPorId(CoberturaEndosoId coberturaEndosoId) {
		try {
			return beanRemoto.findById(coberturaEndosoId);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CoberturaEndosoDTO> obtenerCoberturasEndosoPrimerRiesgoLUC(
			BigDecimal idToPoliza, Short numeroEndoso) {
		try {
			return beanRemoto.obtenerCoberturasPrimerRiesgoLUC(idToPoliza,
					numeroEndoso);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CoberturaEndosoDTO> listarFiltrado(CoberturaEndosoId id) {
		try {
			return beanRemoto.listarFiltrado(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
