package mx.com.afirme.midas.endoso.cobertura.subinciso;

import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubIncisoCoberturaEndosoSN {
	private SubIncisoCoberturaEndosoFacadeRemote beanRemoto;

	public SubIncisoCoberturaEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(SubIncisoCoberturaEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(SubIncisoCoberturaEndosoDTO subIncisoCoberturaEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(subIncisoCoberturaEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
