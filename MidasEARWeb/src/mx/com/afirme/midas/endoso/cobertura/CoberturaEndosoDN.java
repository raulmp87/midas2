package mx.com.afirme.midas.endoso.cobertura;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaEndosoDN {
	private static final CoberturaEndosoDN INSTANCIA = new CoberturaEndosoDN();

	public static CoberturaEndosoDN getInstancia() {
		return INSTANCIA;
	}

	public void agregar(CoberturaEndosoDTO coberturaEndosoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new CoberturaEndosoSN().agregar(coberturaEndosoDTO);
	}

	public CoberturaEndosoDTO obtenerPorID(CoberturaEndosoId coberturaEndosoId)
			throws SystemException {
		return new CoberturaEndosoSN().getPorId(coberturaEndosoId);
	}

	public List<CoberturaEndosoDTO> obtenerCoberturasPrimerRiesgoLUC(
			BigDecimal idToPoliza, Short numeroEndoso) throws SystemException {
		return new CoberturaEndosoSN().obtenerCoberturasEndosoPrimerRiesgoLUC(
				idToPoliza, numeroEndoso);
	}

	public List<CoberturaEndosoDTO> listarFiltrado(CoberturaEndosoId id)
			throws SystemException {
		return new CoberturaEndosoSN().listarFiltrado(id);
	}

}
