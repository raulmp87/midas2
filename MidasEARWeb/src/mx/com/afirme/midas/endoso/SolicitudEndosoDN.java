package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;


public class SolicitudEndosoDN {
	private static final SolicitudEndosoDN INSTANCIA = new SolicitudEndosoDN();

	private static String nombreUsuario;
	
	public static SolicitudEndosoDN getInstancia(String nombreUsuario) {
		SolicitudEndosoDN.nombreUsuario = nombreUsuario;
		return SolicitudEndosoDN.INSTANCIA;
	}

	
	

	public SolicitudDTO getPorId(BigDecimal id) throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		return solicitudEndosoSN.getPorId(id);
	}

	public List<SolicitudDTO> listarTodos() throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		return solicitudEndosoSN.listarTodos();
	}

	public SolicitudDTO agregar(SolicitudDTO solicitudEndosoDTO) throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		return solicitudEndosoSN.agregar(solicitudEndosoDTO);
	}

	public SolicitudDTO actualizar(SolicitudDTO solicitudEndosoDTO) throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		return solicitudEndosoSN.actualizar(solicitudEndosoDTO);
	}

	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO.getIdToControlArchivo());
	}

	public List<SolicitudDTO> listarFiltrado(SolicitudDTO solicitudEndosoDTO, Usuario usuario) throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		return solicitudEndosoSN.listarFiltrado(solicitudEndosoDTO, usuario);
	}

	public void borrar(SolicitudDTO solicitudDTO) throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		solicitudEndosoSN.borrar(solicitudDTO);
	}

	public List<SolicitudDTO> listarPorUsuario(Usuario usuario) throws SystemException {
		SolicitudEndosoSN solicitudEndosoSN = new SolicitudEndosoSN();
		return solicitudEndosoSN.listarPorUsuario(usuario);
	}
	
	public List<SolicitudDTO> listarPorEstatus(Short claveEstatus, Usuario usuario) throws SystemException{
		return new SolicitudEndosoSN().listarPorEstatus(claveEstatus, usuario);
	}
	
	public CotizacionDTO generarCotizacionASolicitud(BigDecimal idSolicitud, String nombreUsuarioCotizacion) throws SystemException {
		
		SolicitudDTO solicitudEndosoDTO;
		try {
			
			EndosoDTO endosoDTO = null;
			
			solicitudEndosoDTO = getPorId(idSolicitud);
			
			//Obtener la p�liza de la solicitud de endoso
			BigDecimal idToPolizaEndosada = solicitudEndosoDTO.getIdToPolizaEndosada();
			
			//Si el tipo de solicitud es el de una rehabilitaci�n
			if (solicitudEndosoDTO.getClaveTipoEndoso().intValue() == 2) {
				//Se obtiene el pen�ltimo endoso de la p�liza endosada
				endosoDTO = EndosoDN.getInstancia(SolicitudEndosoDN.nombreUsuario).getPenultimoEndoso(idToPolizaEndosada);
			} else {
				//Se obtiene el �ltimo endoso de la p�liza endosada
				endosoDTO = EndosoDN.getInstancia(SolicitudEndosoDN.nombreUsuario).getUltimoEndoso(idToPolizaEndosada);
			}
			
			//Se obtiene el id de la cotizaci�n del �ltimo/pen�ltimo endoso segun el caso
			BigDecimal idToCotizacionEndoso = endosoDTO.getIdToCotizacion();
			
			//Se genera una nueva cotizaci�n, COPIANDO la cotizaci�n con 
			//id = idToCotizacionEndoso, de manera similar a como se hace en la implementaci�n del caso de uso 
			//Copiar cotizaci�n, sin embargo, en este caso se copian tambi�n los datos generales de la cotizaci�n 
			//y no se crea una nueva solicitud (la cotizaci�n se asocia a su solicitud de endoso).
						
			CotizacionDTO cotizacionNueva = CotizacionDN.getInstancia(SolicitudEndosoDN.nombreUsuario).
				generarCotizacionASolicitud(idToCotizacionEndoso, solicitudEndosoDTO, nombreUsuarioCotizacion);
			
			return cotizacionNueva;
			
			
		} catch (SystemException e) {
			throw e;
		}
	
}

	
	
}
