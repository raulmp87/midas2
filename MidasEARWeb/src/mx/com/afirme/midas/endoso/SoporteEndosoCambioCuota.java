package mx.com.afirme.midas.endoso;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;

public class SoporteEndosoCambioCuota implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal idToCotizacion;
	private BigDecimal idToCotizacionAnterior;
	private String descripcionSeccion;
	private String descripcionCobertura;
	private List<CoberturaCotizacionDTO> coberturasInvolucradas;

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacionAnterior() {
		return idToCotizacionAnterior;
	}

	public void setIdToCotizacionAnterior(BigDecimal idToCotizacionAnterior) {
		this.idToCotizacionAnterior = idToCotizacionAnterior;
	}

	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}

	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}

	public List<CoberturaCotizacionDTO> getCoberturasInvolucradas() {
		return coberturasInvolucradas;
	}

	public void setCoberturasInvolucradas(
			List<CoberturaCotizacionDTO> coberturasInvolucradas) {
		this.coberturasInvolucradas = coberturasInvolucradas;
	}

}
