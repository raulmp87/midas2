package mx.com.afirme.midas.endoso.riesgo;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoEndosoSN {
	private RiesgoEndosoFacadeRemote beanRemoto;

	public RiesgoEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RiesgoEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(RiesgoEndosoDTO riesgoEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(riesgoEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RiesgoEndosoDTO modificar(RiesgoEndosoDTO riesgoEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(riesgoEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RiesgoEndosoDTO> listarFiltrado(RiesgoEndosoId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.listarFiltrado(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RiesgoEndosoDTO getPorID(RiesgoEndosoId riesgoEndosoId)
			throws ExcepcionDeAccesoADatos {

		return beanRemoto.findById(riesgoEndosoId);

	}
}
