package mx.com.afirme.midas.endoso.riesgo.subinciso;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubIncisoRiesgoEndosoSN {
	private SubIncisoRiesgoEndosoFacadeRemote beanRemoto;

	public SubIncisoRiesgoEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(SubIncisoRiesgoEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(SubIncisoRiesgoEndosoDTO subIncisoRiesgoEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(subIncisoRiesgoEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	
	public List<SubIncisoRiesgoEndosoDTO> listarFiltrado(SubIncisoRiesgoEndosoDTO subIncisoRiesgoEndosoDTO){
	    
		return beanRemoto.listarFiltrado(subIncisoRiesgoEndosoDTO);
	
	}
	

}
