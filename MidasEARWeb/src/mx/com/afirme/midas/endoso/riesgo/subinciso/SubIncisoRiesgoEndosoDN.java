package mx.com.afirme.midas.endoso.riesgo.subinciso;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class SubIncisoRiesgoEndosoDN {
       private static final SubIncisoRiesgoEndosoDN INSTANCIA = new SubIncisoRiesgoEndosoDN();
	
	public static SubIncisoRiesgoEndosoDN getInstancia() {
		return INSTANCIA;
	}
	
	
	
	public void agregar(SubIncisoRiesgoEndosoDTO subIncisoRiesgoEndosoDTO)
			throws ExcepcionDeAccesoADatos {
		
	   new SubIncisoRiesgoEndosoDN().agregar(subIncisoRiesgoEndosoDTO);
		
	}
	

	public List<SubIncisoRiesgoEndosoDTO> listarFiltrado(SubIncisoRiesgoEndosoDTO subIncisoRiesgoEndosoDTO) 
	throws SystemException{
	    
		return new SubIncisoRiesgoEndosoSN().listarFiltrado(subIncisoRiesgoEndosoDTO);
	
	}


}
