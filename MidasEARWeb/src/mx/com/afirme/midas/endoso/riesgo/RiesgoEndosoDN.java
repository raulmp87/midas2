package mx.com.afirme.midas.endoso.riesgo;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoEndosoDN {
	private static final RiesgoEndosoDN INSTANCIA = new RiesgoEndosoDN();

	public static RiesgoEndosoDN getInstancia() {
		return INSTANCIA;
	}

	public void agregar(RiesgoEndosoDTO riesgoEndosoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		new RiesgoEndosoSN().agregar(riesgoEndosoDTO);
	}

	public RiesgoEndosoDTO getPorID(RiesgoEndosoId riesgoEndosoId)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new RiesgoEndosoSN().getPorID(riesgoEndosoId);
	}

	public List<RiesgoEndosoDTO> listarFiltrado(RiesgoEndosoId id)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new RiesgoEndosoSN().listarFiltrado(id);
	}

	public RiesgoEndosoDTO modificar(RiesgoEndosoDTO riesgoEndosoDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new RiesgoEndosoSN().modificar(riesgoEndosoDTO);
	}

}
