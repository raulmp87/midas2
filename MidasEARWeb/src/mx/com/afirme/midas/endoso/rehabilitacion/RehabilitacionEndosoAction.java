package mx.com.afirme.midas.endoso.rehabilitacion;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RehabilitacionEndosoAction extends MidasMappingDispatchAction {

	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			RehabilitacionEndosoForm rehabilitacionEndosoForm = (RehabilitacionEndosoForm) form;
			
			String idPoliza = rehabilitacionEndosoForm.getIdPoliza();
			
			if(idPoliza == null) {
				idPoliza = request.getParameter("idToPoliza");
			}
				
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
			
			List<EndosoDTO> listaEndososPoliza = RehabilitacionEndosoDN.getInstancia().listarEndososRehabilitables(idToPoliza);
			if (listaEndososPoliza == null) {
				listaEndososPoliza = new ArrayList<EndosoDTO>();
			}
			rehabilitacionEndosoForm.setIdPoliza(idPoliza);
			request.setAttribute("listaEndososRehabilitables", listaEndososPoliza);
		
		} catch (Exception e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);
				
		
	}
	
	public void rehabilitarEndoso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String tipoMensaje = Sistema.EXITO;
		try {
			
			String idPoliza = request.getParameter("idToPoliza");
			String numeroEndosoStr = request.getParameter("numeroEndoso");
			String sFechaIniVigencia = request.getParameter("fechaIniVigencia");
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaIniVigencia = null;
			try {
				fechaIniVigencia = df.parse(sFechaIniVigencia);
				if (fechaIniVigencia == null) {
					throw new RuntimeException();
				}
			} catch (ParseException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
			
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
			Short numeroEndoso = UtileriasWeb.regresaShort(numeroEndosoStr);
			
			//Rehabilitar el endoso
			RehabilitacionEndosoDN.getInstancia().rehabilitarEndoso(idToPoliza, numeroEndoso, fechaIniVigencia, 
					UtileriasWeb.obtieneNombreUsuario(request));			
			
		
		} catch (SystemException e) {
			tipoMensaje = Sistema.INFORMACION;
		} catch (RuntimeException e) {
			tipoMensaje = Sistema.ERROR;
		} finally {
			//Mensaje de respuesta al usuario
			UtileriasWeb.imprimeMensajeXML(tipoMensaje, obtieneMensajeRespuesta(tipoMensaje), response);
		}
	}
	
	private String obtieneMensajeRespuesta (String tipoMensaje) {
		String mensaje = "";
		if (tipoMensaje.equals(Sistema.EXITO)) {
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.rehabilitacion.mensaje.exito");
		} else if (tipoMensaje.equals(Sistema.INFORMACION)) {
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.sistema.nodisponible");
		} else {
			mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.rollback");
		}
		
		return mensaje;
	}
	
}
