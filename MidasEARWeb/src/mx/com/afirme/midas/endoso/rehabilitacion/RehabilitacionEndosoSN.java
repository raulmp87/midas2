package mx.com.afirme.midas.endoso.rehabilitacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class RehabilitacionEndosoSN {

private RehabilitacionEndosoFacadeRemote beanRemoto;
	
	public RehabilitacionEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(RehabilitacionEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<EndosoDTO> listarEndososRehabilitables(BigDecimal idToPoliza) {
		
		return beanRemoto.listarEndososRehabilitables(idToPoliza);
		
	}
	
}
