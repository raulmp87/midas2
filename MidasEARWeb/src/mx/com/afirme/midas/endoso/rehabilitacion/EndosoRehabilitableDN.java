package mx.com.afirme.midas.endoso.rehabilitacion;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoRehabilitableDN {

	private static final EndosoRehabilitableDN INSTANCIA = new EndosoRehabilitableDN();

	public static EndosoRehabilitableDN getInstancia() {
		return EndosoRehabilitableDN.INSTANCIA;
	}

	public boolean notificaRehabilitacion(EndosoRehabilitableDTO endoso)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoRehabilitableSN().notificaRehabilitacion(endoso);
	}

	public List<EndosoRehabilitableDTO> listaRegistrosSinProcesar()
			throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoRehabilitableSN().findByProperty("estatusRegistro",
				EndosoRehabilitableDTO.ESTATUS_REGISTRO_SIN_PROCESAR);
	}

	public EndosoRehabilitableDTO encuentraPorIdRefrescado(
			EndosoRehabilitableId id) throws ExcepcionDeAccesoADatos,
			SystemException {
		List<EndosoRehabilitableDTO> endosoRehabilitableList = new EndosoRehabilitableSN()
				.findByProperty("id", id);

		if (endosoRehabilitableList != null
				&& !endosoRehabilitableList.isEmpty()) {
			return endosoRehabilitableList.get(0);
		}

		return null;
	}

	public EndosoRehabilitableDTO actualiza(
			EndosoRehabilitableDTO endosoRehabilitable)
			throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoRehabilitableSN().update(endosoRehabilitable);
	}
}
