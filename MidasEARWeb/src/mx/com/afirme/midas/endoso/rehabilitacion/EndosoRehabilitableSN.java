package mx.com.afirme.midas.endoso.rehabilitacion;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoRehabilitableSN {

	private EndosoRehabilitableFacadeRemote beanRemoto;

	public EndosoRehabilitableSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(EndosoRehabilitableFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public boolean notificaRehabilitacion(EndosoRehabilitableDTO endoso)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.notificaRehabilitacion(endoso);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}

	}

	public List<EndosoRehabilitableDTO> findByProperty(String propertyName,
			final Object value) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoRehabilitableDTO update(
			EndosoRehabilitableDTO endosoRehabilitable)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(endosoRehabilitable);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

}