package mx.com.afirme.midas.endoso.rehabilitacion;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class RehabilitacionEndosoForm extends MidasBaseForm {
	
		
	private static final long serialVersionUID = -3494931859169614825L;
	
	private String idPoliza;

	public String getIdPoliza() {
		return idPoliza;
	}

	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}

	
	
}
