package mx.com.afirme.midas.endoso.rehabilitacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class RehabilitacionEndosoDN {

	private static final RehabilitacionEndosoDN INSTANCIA = new RehabilitacionEndosoDN();

	public static RehabilitacionEndosoDN getInstancia() {
		return RehabilitacionEndosoDN.INSTANCIA;
	}

	public List<EndosoDTO> listarEndososRehabilitables(BigDecimal idToPoliza)
			throws SystemException {
		return new RehabilitacionEndosoSN().listarEndososRehabilitables(idToPoliza);
	}
	
	
	public void rehabilitarEndoso (BigDecimal idToPoliza, Short numeroEndoso, Date fechaInicioVigencia, String nombreUsuario) throws SystemException {
		
		EndosoDTO ultimoEndoso;
		Short numeroEndosoRE;
		
		//Se valida que la poliza continue vigente antes de rehabilitar el endoso (Esto por si se esta ejecutando desde el job)
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		
		if (polizaDTO.getClaveEstatus().compareTo((short)0) == 0) { 
//			if (nombreUsuario.trim().equals(Sistema.USUARIO_SISTEMA)) {
//				return;	
//			} else {
//				throw new RuntimeException("La poliza no es vigente");
//			}
			throw new RuntimeException("La poliza no es vigente");
		}
		
		
		
		EndosoDTO endosoARehabilitar = EndosoDN.getInstancia("").getPorId(new EndosoId(idToPoliza, numeroEndoso));
		
		//Se obtiene el ultimo endoso
		ultimoEndoso = EndosoDN.getInstancia("").getUltimoEndoso(idToPoliza);
		
		//Se crea y emite el endoso RE
		try {
			numeroEndosoRE = CancelacionEndosoDN.getInstancia().creaEndosoRehabilitacion(endosoARehabilitar, ultimoEndoso, fechaInicioVigencia,
					nombreUsuario, endosoARehabilitar.getGrupo());
			if (numeroEndosoRE == null) {
				throw new RuntimeException("Error al crear el endoso de rehabilitacion");
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RuntimeException("Error al crear el endoso de rehabilitacion (IllegalStateException)");
		} catch (SecurityException e) {
			e.printStackTrace();
			throw new RuntimeException("Error al crear el endoso de rehabilitacion (SecurityException)");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Error al crear el endoso de rehabilitacion (ClassNotFoundException)");
		} catch (javax.transaction.SystemException e) {
			e.printStackTrace();
			throw new RuntimeException("Error al crear el endoso de rehabilitacion (javax.transaction.SystemException)");
		} 
		//Se limpian los campos de grupo y cancela del endoso a rehabilitar			
		endosoARehabilitar.setCancela(null);
		endosoARehabilitar.setGrupo(null);
		
		//Se guardan los cambios en endosoACancelar 
		EndosoDN.getInstancia(nombreUsuario).actualizar(endosoARehabilitar);
			
		
		
		
	}
	
	
}
