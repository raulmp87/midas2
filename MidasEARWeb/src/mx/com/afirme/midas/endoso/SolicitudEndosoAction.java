package mx.com.afirme.midas.endoso;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionForm;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDN;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDTO;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudAction;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudForm;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

/**
 * Clase Para Manejar peticiones desde paginas relacionadas a solicitudes de
 * Endoso.
 * @author Cesar Morales
 *
 */
public class SolicitudEndosoAction extends MidasMappingDispatchAction{
	
	
	/**
	 * Constante de formato de fecha.
	 */
	private final static SimpleDateFormat formatoFecha= new SimpleDateFormat("dd/MM/yyyy");

	/**
	 * Carga los datos necesarios en <code>SolicitudEndosoForm</code> para presentar la pantalla de Agregar.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm) form;
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope
		(request, Sistema.USUARIO_ACCESO_MIDAS);
		solicitudEndosoForm.setTipoPersona("1");
		solicitudEndosoForm.setPresentaComboMotivo("0");
		solicitudEndosoForm.setFechaInicioVigenciaEndoso(formatoFecha.format(Calendar.getInstance().getTime()));
		try {
			this.poblarAgentes(solicitudEndosoForm, usuario);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Inserta una nueva solicitud con los datos  capturados por el Usuario.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion);
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		SolicitudEndosoForm solicitudEndosoForm = null;
		if(session.getAttribute("solicitudEndosoForm") != null) {
			solicitudEndosoForm = (SolicitudEndosoForm) session.getAttribute("solicitudEndosoForm");
			session.removeAttribute("solicitudEndosoForm");
		} else {
			solicitudEndosoForm = (SolicitudEndosoForm) form;
		}
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		SolicitudEndosoDN solicitudDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			poblarDTO(solicitudEndosoForm, solicitudDTO, request);
			solicitudDTO.setIdToPolizaAnterior(new BigDecimal(0));
			solicitudDTO.setClaveTipoSolicitud(Sistema.SOLICITUD_TIPO_ENDOSO);
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			solicitudDTO.setClaveEstatus((short)0);
			solicitudDTO.setFechaCreacion(new Date());
			solicitudDTO.setFechaModificacion(new Date());
			solicitudDTO.setEsRenovacion(Sistema.NO_ES_RENOVACION);
			solicitudDTO = solicitudDN.agregar(solicitudDTO);
			SolicitudDN.getInstancia().registraEstadisticas
			(solicitudDTO.getIdToSolicitud(), null, null, null, usuario.getNombreUsuario(), Sistema.SE_CREA_SOLICITUD);
			solicitudEndosoForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
			this.poblarForm(solicitudDTO, (SolicitudEndosoForm)form,request);
			request.setAttribute("documentos", new ArrayList<DocumentoDigitalSolicitudDTO>());
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			this.poblarAgentes((SolicitudEndosoForm)form, usuario);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
         /**
          * Carga un objeto <code>solicitudDTO</code> con datos de un <code>SolicitudEndosoForm</code>.
          * @param solicitudEndosoForm
          * @param solicitudDTO
          * @param request
          * @throws SystemException
          */
	private void poblarDTO(SolicitudEndosoForm solicitudEndosoForm, SolicitudDTO solicitudDTO, HttpServletRequest request) throws SystemException {
			solicitudDTO.setEsRenovacion(Sistema.NO_ES_RENOVACION);
			if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getEstatus())) {
                solicitudDTO.setClaveEstatus(Short.parseShort(solicitudEndosoForm.getEstatus()));
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getNumeroSolicitud())) {
                solicitudDTO.setIdToSolicitud(UtileriasWeb.regresaBigDecimal(solicitudEndosoForm.getNumeroSolicitud()));
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getTipoPersona())) {
                solicitudDTO.setClaveTipoPersona(Short.parseShort(solicitudEndosoForm.getTipoPersona()));
            }
            if(solicitudDTO.getClaveTipoPersona().shortValue() == 1) {
                if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getNombres())) {
                    solicitudDTO.setNombrePersona(solicitudEndosoForm.getNombres().trim().toUpperCase());
                }
                if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getApellidoPaterno())) {
                    solicitudDTO.setApellidoPaterno(solicitudEndosoForm.getApellidoPaterno().trim().toUpperCase());
                }
                if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getApellidoMaterno())) {
                    solicitudDTO.setApellidoMaterno(solicitudEndosoForm.getApellidoMaterno().trim().toUpperCase());
                }
            } else if(solicitudDTO.getClaveTipoPersona().shortValue() == 2) {
                if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getRazonSocial())) {
                    solicitudDTO.setNombrePersona(solicitudEndosoForm.getRazonSocial().trim().toUpperCase());
                }
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getTelefono())) {
                solicitudDTO.setTelefonoContacto(solicitudEndosoForm.getTelefono());
            }
           
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getIdAgente())) {
                BigDecimal idAgente = UtileriasWeb.regresaBigDecimal(solicitudEndosoForm.getIdAgente());
                solicitudDTO.setCodigoAgente(idAgente);
                
                AgenteDTO agenteDTO = new AgenteDTO();
                agenteDTO.setIdTcAgente(idAgente.intValue());
                
                Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
                String nombreUsuario = usuario != null ? usuario.getNombreUsuario() : "";
                
                AgenteDN agenteDN = AgenteDN.getInstancia();
                agenteDTO = agenteDN.verDetalleAgente(agenteDTO, nombreUsuario);
                
                solicitudDTO.setNombreAgente(agenteDTO.getNombre());
                solicitudDTO.setNombreOficinaAgente(agenteDTO.getNombreOficina());
                
                if(agenteDTO.getIdOficina()!=null){
                    BigDecimal idOficina=UtileriasWeb.regresaBigDecimal(agenteDTO.getIdOficina().trim());
                    solicitudDTO.setIdOficina(idOficina);
                    solicitudDTO.setNombreOficina(agenteDTO.getNombreGerencia());
                    solicitudDTO.setCodigoEjecutivo(idOficina);
                    solicitudDTO.setNombreOficinaAgente(agenteDTO.getNombrePromotoria());
                }
                solicitudDTO.setNombreEjecutivo(agenteDTO.getNombreOficina());
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getCorreos())) {
                solicitudDTO.setEmailContactos(solicitudEndosoForm.getCorreos());
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getClaveOpcionEmision())) {
                solicitudDTO.setClaveOpcionEmision(UtileriasWeb.regresaShort(solicitudEndosoForm.getClaveOpcionEmision()));
            } else {
                solicitudDTO.setClaveOpcionEmision((short)1);
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getIdUsuario())) {
                solicitudDTO.setCodigoUsuarioCreacion(solicitudEndosoForm.getIdUsuario());
            }
           
            //PROPIEDADES QUE IDENTIFICAN UN ENDOSO
            try{
            	Date fechaEndoso=null;
            	if(solicitudEndosoForm.getFechaInicioVigenciaEndoso()!=null){
    	        	fechaEndoso= formatoFecha.parse(solicitudEndosoForm.getFechaInicioVigenciaEndoso());
    		        Calendar calendar = Calendar.getInstance();
    	        	if(fechaEndoso!=null){
    		        	calendar.setTime(fechaEndoso);
    		        	solicitudDTO.setFechaInicioVigenciaEndoso(calendar.getTime());
    	        	}
            	}
            
            }catch (ParseException e) {
            	solicitudDTO.setFechaInicioVigenciaEndoso(null);
    		}
            if(solicitudEndosoForm.getIdPoliza()!= null){
            	solicitudDTO.setIdToPolizaEndosada(new BigDecimal(solicitudEndosoForm.getIdPoliza()));
            	PolizaDTO poliza= PolizaDN.getInstancia().getPorId(solicitudDTO.getIdToPolizaEndosada());
            	if(poliza!=null){
            		  solicitudDTO.setProductoDTO(poliza.getCotizacionDTO().getSolicitudDTO().getProductoDTO());
            	}
            	
            }
            solicitudDTO.setClaveTipoEndoso(new BigDecimal(solicitudEndosoForm.getTipoEndoso()));
            
            if(solicitudDTO.getClaveTipoEndoso()!=null && solicitudDTO.getClaveTipoEndoso().intValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
        	if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getClaveMotivoEndoso())){
        	    solicitudDTO.setClaveMotivoEndoso(Short.valueOf(solicitudEndosoForm.getClaveMotivoEndoso()));
        	}
            }
        
    }
	/**
	 * Obtiene el listado de docuementos de una solicitud.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion);
	 */
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			HttpSession session = request.getSession();
			Object idSolicitud = session.getAttribute("idSolicitud");
			// session.removeAttribute("idSolicitud");
			List<DocumentoDigitalSolicitudDTO> documentos = new ArrayList<DocumentoDigitalSolicitudDTO>();
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			
			if(idSolicitud != null){
				documentos = documentoDigitalSolicitudDN.
					listarDocumentosSolicitud(UtileriasWeb.regresaBigDecimal(idSolicitud.toString()));
			}
			
			request.setAttribute("documentos", documentos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga los datos necesarios en <code>SolicitudEndosoForm</code> para presentar la pantalla de Borrar.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion)
	 */
	public ActionForward mostrarBorrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm)form;
		try {
			String id = request.getParameter("id");
			SolicitudEndosoDN solicitudEndosoDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			SolicitudDTO solicitudDTO = solicitudEndosoDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			this.poblarForm(solicitudDTO, solicitudEndosoForm,request);

			HttpSession session = request.getSession();
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			
			this.listarDocumentos(mapping, solicitudEndosoForm, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga los datos de <code>SolicitudEndosoForm</code> de un objeto <code>SolicitudDTO</code>
	 * @param solicitudDTO De donde se obtienen los datos.
	 * @param solicitudEndosoForm Objeto a llenar.
	 * @param request
	 * @throws SystemException si al invocar el metodo 
	 * <code>verDetalleAgente()</code> de la clase <code>AgenteDN</code> se produce una exepcion.
	 */
	private void poblarForm(SolicitudDTO solicitudDTO,SolicitudEndosoForm solicitudEndosoForm,
		HttpServletRequest request) throws SystemException {
		
		if(solicitudDTO.getClaveEstatus() != null) {
			solicitudEndosoForm.setEstatus(solicitudDTO.getClaveEstatus().toString());
		}
		if(solicitudDTO.getIdToSolicitud() != null) {
			solicitudEndosoForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
		}
		if(solicitudDTO.getClaveTipoPersona() != null) {
			solicitudEndosoForm.setTipoPersona(solicitudDTO.getClaveTipoPersona().toString());
		}
		if(solicitudEndosoForm.getTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_FISICA)) {
			String nombre = "";
			if(solicitudDTO.getNombrePersona() != null) {
				solicitudEndosoForm.setNombres(solicitudDTO.getNombrePersona());
				nombre += solicitudDTO.getNombrePersona() + " ";
			}
			if(solicitudDTO.getApellidoPaterno() != null) {
				solicitudEndosoForm.setApellidoPaterno(solicitudDTO.getApellidoPaterno());
				nombre += solicitudDTO.getApellidoPaterno() + " ";
			}
			if(solicitudDTO.getApellidoMaterno() != null) {
				solicitudEndosoForm.setApellidoMaterno(solicitudDTO.getApellidoMaterno());
				nombre += solicitudDTO.getApellidoMaterno() + " ";
			}
			solicitudEndosoForm.setNombreSolicitante(nombre);
		} else if(solicitudEndosoForm.getTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_MORAL)) {
			if(solicitudDTO.getNombrePersona() != null) {
				solicitudEndosoForm.setRazonSocial(solicitudDTO.getNombrePersona());
				solicitudEndosoForm.setNombreSolicitante(solicitudDTO.getNombrePersona());
			}
		}
		if(solicitudDTO.getTelefonoContacto() != null) {
			solicitudEndosoForm.setTelefono(solicitudDTO.getTelefonoContacto());
		}
		if(solicitudDTO.getProductoDTO() != null) {
			if(solicitudDTO.getProductoDTO().getIdToProducto() != null) {
				solicitudEndosoForm.setIdProducto(solicitudDTO.getProductoDTO().getIdToProducto().toString());
			}
			if(solicitudDTO.getProductoDTO().getNombreComercial() != null) {
				solicitudEndosoForm.setNombreComercialProducto(solicitudDTO.getProductoDTO().getNombreComercial());
				solicitudEndosoForm.setDescProducto(solicitudDTO.getProductoDTO().getNombreComercial().toUpperCase());
			}
		}
		if(solicitudDTO.getCodigoAgente() != null) {
		    	solicitudEndosoForm.setCodigoAgente(solicitudDTO.getCodigoAgente().toBigInteger().toString());
			solicitudEndosoForm.setIdAgente(solicitudDTO.getCodigoAgente().toBigInteger().toString());
			AgenteDN agenteDN = AgenteDN.getInstancia();
			String nombreAgente = "No disponible";
			try{
			AgenteDTO agenteDTO = new AgenteDTO();
			agenteDTO.setIdTcAgente(Integer.valueOf(solicitudEndosoForm.getIdAgente()));
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			agenteDTO = agenteDN.verDetalleAgente(agenteDTO, usuario.getNombreUsuario());//getPorId(UtileriasWeb.regresaBigDecimal(solicitudEndosoForm.getIdAgente()));
			nombreAgente = agenteDTO.getNombre();
			}catch(Exception e){}
			solicitudEndosoForm.setNombreAgente(nombreAgente);
			
		}
		if(solicitudDTO.getEmailContactos() != null) {
			solicitudEndosoForm.setCorreos(solicitudDTO.getEmailContactos());
		}
		if(solicitudDTO.getClaveOpcionEmision() != null) {
			solicitudEndosoForm.setClaveOpcionEmision(solicitudDTO.getClaveOpcionEmision().toString());
		}
		if(solicitudDTO.getCodigoUsuarioCreacion() != null) {
			solicitudEndosoForm.setIdUsuario(solicitudDTO.getCodigoUsuarioCreacion());
		}
		if(solicitudDTO.getFechaCreacion() != null) {
			solicitudEndosoForm.setFechaSolicitud(solicitudDTO.getFechaCreacion());
		}
		
		
		if(solicitudDTO.getClaveTipoEndoso()!= null){
			solicitudEndosoForm.setTipoEndoso(solicitudDTO.getClaveTipoEndoso().toString());
			solicitudEndosoForm.setDescTipoEndoso(UtileriasWeb.getDescripcionCatalogoValorFijo(40,solicitudDTO.getClaveTipoEndoso().toString()));
		}
		
		if(solicitudDTO.getFechaInicioVigenciaEndoso()!=null){
			solicitudEndosoForm.setFechaInicioVigenciaEndoso(formatoFecha.format(solicitudDTO.getFechaInicioVigenciaEndoso()));
		}
		
		if(solicitudDTO.getIdToPolizaEndosada()!=null){
			PolizaDTO poliza= PolizaDN.getInstancia().getPorId(solicitudDTO.getIdToPolizaEndosada());
			if(poliza!=null){
				solicitudEndosoForm.setFechaInicioCotizacion(formatoFecha.format(poliza.getCotizacionDTO().getFechaInicioVigencia()));
				solicitudEndosoForm.setFechaFinCotizacion(formatoFecha.format(poliza.getCotizacionDTO().getFechaFinVigencia()));
				solicitudEndosoForm.setNumPoliza(UtileriasWeb.getNumeroPoliza(poliza));
				solicitudEndosoForm.setIdPoliza(poliza.getIdToPoliza().toString());
				solicitudEndosoForm.setCveEstatusPoliza(poliza.getClaveEstatus().toString());
			}
			
			
		}
		
		if(solicitudDTO.getClaveTipoEndoso()!=null && solicitudDTO.getClaveTipoEndoso().intValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION &&
			solicitudDTO.getClaveMotivoEndoso()!=null)  {
		    solicitudEndosoForm.setClaveMotivoEndoso(solicitudDTO.getClaveMotivoEndoso().toString());
		    solicitudEndosoForm.setPresentaComboMotivo("1");
		}else{ 
		    solicitudEndosoForm.setPresentaComboMotivo("0");
		    }
		
	}
	/**
	 * Elimina una solicitud de endoso , a partir de su <code>id</code> asi como sus dependencias.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id="";
		try {
		    	SolicitudAction solicitudAction = new SolicitudAction();
		    	HttpSession session = request.getSession();
			id = session.getAttribute("idSolicitud").toString();
			SolicitudEndosoDN solicitudDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			List<DocumentoDigitalSolicitudDTO> documentos = documentoDigitalSolicitudDN.listarDocumentosSolicitud(UtileriasWeb.regresaBigDecimal(id));
			for(DocumentoDigitalSolicitudDTO documento : documentos) {
				documentoDigitalSolicitudDN.borrar(documento);
			}
			SolicitudDTO solicitudDTO = new SolicitudDTO();
			solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			solicitudDN.borrar(solicitudDTO);
			return solicitudAction.listarFiltrado(mapping, new SolicitudForm(), request, response);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm) form;
		solicitudEndosoForm.setNumeroSolicitud(id);
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga los datos necesarios en el <code>SolicitudEndosoForm</code> para presentar 
	 * la pantalla de modificar.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm)form;
		try {
			String id = request.getParameter("id");
			SolicitudEndosoDN solicitudDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			HttpSession session = request.getSession();
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			session.removeAttribute("fechaCreacion");
			session.setAttribute("fechaCreacion", solicitudDTO.getFechaCreacion());
			this.listarDocumentos(mapping, solicitudEndosoForm, request, response);
			try {
				Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
				this.poblarAgentes(solicitudEndosoForm, usuario);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			this.poblarForm(solicitudDTO, solicitudEndosoForm,request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Actualiza los datos de una solicitud de endoso, con los datos que cambio o actualiz� el usuario.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion);
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm) form;
		SolicitudForm solicitudForm= new SolicitudForm();
		SolicitudDTO solicitudDTO;
		SolicitudEndosoDN solicitudDN = SolicitudEndosoDN.getInstancia("");
		SolicitudAction solicitudAction= new SolicitudAction();
		try {
		    	if(solicitudEndosoForm.getNumeroSolicitud() !=null){
        			solicitudDTO= solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(solicitudEndosoForm.getNumeroSolicitud()));
        			if(solicitudDTO!=null){
                			poblarDTOModificar(solicitudEndosoForm, solicitudDTO, request);
                			solicitudDTO.setClaveTipoSolicitud(Sistema.SOLICITUD_TIPO_ENDOSO); //modificar  0:Nueva, 1:Re-expedici�n, 2:Endoso
                			solicitudDTO.setFechaModificacion(new Date());
                			solicitudDTO = solicitudDN.actualizar(solicitudDTO);
                       			request.setAttribute("documentos", new ArrayList<DocumentoDigitalSolicitudDTO>());
                       			this.cargaSolicitudFormDeSolicitudEndosoForm(solicitudForm, solicitudEndosoForm);
                       			return solicitudAction.listarFiltrado(mapping, solicitudForm, request, response);
        			}else{
        			    UtileriasWeb.mandaMensajeExcepcionRegistrado
        			    ("No se pudo modificar, debido a errores,consulte al administrador", request);
        			}
			}else{
			    UtileriasWeb.mandaMensajeExcepcionRegistrado
			    ("No se pudo modificar, debido a errores,consulte al administrador", request);
			}
		    
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Actualiza el estatus de una solicitud de EN PROCESO a TERMINADA.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward terminar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
		    	SolicitudAction solicitudAction = new SolicitudAction();
		    	SolicitudForm solicitudForm= new SolicitudForm();
		    	String id = request.getParameter("id");
			SolicitudEndosoDN solicitudDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			solicitudDTO.setClaveEstatus((short)2);
			solicitudDN.actualizar(solicitudDTO);
			solicitudForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
       			return solicitudAction.listarFiltrado(mapping, solicitudForm, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Elimina un documento de una solicitud.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = documentoDigitalSolicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			controlArchivoDTO.setIdToControlArchivo(documentoDigitalSolicitudDTO.getIdToControlArchivo());
			ControlArchivoDN controlArchivoD = ControlArchivoDN.getInstancia();
			controlArchivoD.borrar(controlArchivoDTO);
			
			documentoDigitalSolicitudDN.borrar(documentoDigitalSolicitudDTO);
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga los datos necesarios en el <code>SolicitudEndosoForm</code> para presentar la pantalla de Detalle.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm)form;
		try {
			String id = request.getParameter("id");
			SolicitudEndosoDN solicitudDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			this.poblarForm(solicitudDTO, solicitudEndosoForm,request);
			HttpSession session = request.getSession();
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			
			this.listarDocumentos(mapping, solicitudEndosoForm, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward validarDatosSolicitud(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		final HttpSession session = request.getSession();
		final SolicitudEndosoForm forma = (SolicitudEndosoForm) form;
		session.removeAttribute("solicitudEndosoForm");
		session.setAttribute("solicitudEndosoForm", form);
		// RMP Valida que el agente este activo y con cedula vigente
		final SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		final Set<String> errores = solicitudDN.validarAgente(Integer.valueOf(forma.getIdAgente()), getUsuarioMidas(
				request).getNombreUsuario());
		final ActionMessages actionMessages = new ActionMessages();
		if (CollectionUtils.isEmpty(errores)) {
			forma.setMensaje(null);
			forma.setTipoMensaje(null);
		} else {
			actionMessages.add("idAgente", new ActionMessage("solicitud.validacion"));
			agregarMensajes(errores, Sistema.ERROR, forma, getResources(request).getMessage("solicitud.validacion"));
		}
		saveErrors(request, actionMessages);
		return mapping.findForward(Sistema.EXITOSO);
	}
	/**
	 * Carga una lista de agentes en el form de solicitud de endoso.
	 * @param solicitudEndosoForm
	 * @param usuario
	 * @throws SystemException
	 */
	private void poblarAgentes(SolicitudEndosoForm solicitudEndosoForm, Usuario usuario) 
		throws SystemException {
	       	AgenteDN agenteDN = AgenteDN.getInstancia();
		List<AgenteDTO> agentes = null;
		try{
			agentes = AgenteEspecialDN.getInstancia().poblarAgenteEspecial(usuario, solicitudEndosoForm);
			if (!solicitudEndosoForm.getBloqueoDatosAgente())
				agentes = agenteDN.getAgentesPorUsuario(usuario);
			    
		}catch(Exception e){
			e.printStackTrace();
		}
		if(agentes != null && !agentes.isEmpty()) {
			solicitudEndosoForm.setAgentes(agentes);
		} else {
			solicitudEndosoForm.setAgentes(new ArrayList<AgenteDTO>());
		}
	}
	
	
	/**
	 * Carga los datos necesarios en <code>SolicitudEndosoForm</code> para presentar la pantalla de resumen.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarResumen(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("id");
		SolicitudEndosoForm solicitudEndosoForm = (SolicitudEndosoForm)form;
		try {
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			if(solicitudDTO.getClaveTipoSolicitud() == Sistema.SOLICITUD_TIPO_ENDOSO){
				cotizacionDTO = SolicitudDN.getInstancia().getResumenSolicitudEndoso(solicitudDTO.getIdToSolicitud());				
			}else{
				cotizacionDTO= solicitudDN.getResumenSolicitud(UtileriasWeb.regresaBigDecimal(id));	
			}
			solicitudEndosoForm.setNumeroSolicitudFormateado(UtileriasWeb.llenarIzquierda(cotizacionDTO.getSolicitudDTO().getIdToSolicitud().toString(), "0", 8));
			solicitudEndosoForm.setDescripcionEstatus(UtileriasWeb.getDescripcionCatalogoValorFijo(29, cotizacionDTO.getSolicitudDTO().getClaveEstatus()));
			this.poblarForm(cotizacionDTO.getSolicitudDTO(), solicitudEndosoForm, request);
			solicitudEndosoForm.setNombreUsuarioAsignacionODT(cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			String estatusODT= "";
			try{
				if (cotizacionDTO != null){
					EstadisticasEstadoDN estadisticasEstadoDN = EstadisticasEstadoDN.getInstancia();
					List<EstadisticasEstadoDTO> estadisticas = estadisticasEstadoDN.buscarPorPropiedad("idBase2", cotizacionDTO.getIdToCotizacion());
					estatusODT =  UtileriasWeb.getDescripcionCatalogoValorFijo(30, cotizacionDTO.getClaveEstatus());
					solicitudEndosoForm.setNumeroOrdenTrabajoFormateado(UtileriasWeb.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toString(), "0", 8));
					solicitudEndosoForm.setFechaFinVigencia(cotizacionDTO.getFechaFinVigencia());
					solicitudEndosoForm.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
					solicitudEndosoForm.setNombreContratante(cotizacionDTO.getNombreContratante());
					solicitudEndosoForm.setNombreAsegurado(cotizacionDTO.getNombreAsegurado());
					solicitudEndosoForm.setTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getDescripcion());
					solicitudEndosoForm.setMoneda(MonedaDN.getInstancia().getPorId(cotizacionDTO.getIdMoneda().shortValue()).getDescripcion());
					for(EstadisticasEstadoDTO estadistica : estadisticas) {
						if(estadisticasEstadoDN.esTrancisionODT(estadistica)) {
							solicitudEndosoForm.setFechaODT(estadistica.getId().getFechaHora());
							break;
						}
					}
					for(EstadisticasEstadoDTO estadistica : estadisticas) {
						if(estadisticasEstadoDN.esTrancisionCOT(estadistica)) {
							solicitudEndosoForm.setFechaCOT(estadistica.getId().getFechaHora());
							break;
						}
					}
					solicitudEndosoForm.setNombreUsuarioAsignacionCOT(cotizacionDTO.getCodigoUsuarioCotizacion());
					
					if(cotizacionDTO.getClaveEstatus().shortValue() == Sistema.ESTATUS_COT_EMITIDA) {
						EndosoDN endosoDN = EndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
						List<EndosoDTO> endosos = endosoDN.buscarPorPropiedad("idToCotizacion", cotizacionDTO.getIdToCotizacion(), Boolean.TRUE);
						if(endosos != null && !endosos.isEmpty()) {
							EndosoDTO endoso = endosos.get(0);
							PolizaDN polizaDN = PolizaDN.getInstancia();
							PolizaDTO polizaDTO = polizaDN.getPorId(endoso.getId().getIdToPoliza());
							solicitudEndosoForm.setNumPoliza(UtileriasWeb.getNumeroPoliza(polizaDTO));
							solicitudEndosoForm.setNumeroEndoso(endoso.getId().getNumeroEndoso().toString());
							for(EstadisticasEstadoDTO estadistica : estadisticas) {
								if(estadisticasEstadoDN.esTrancisionEmision(estadistica)) {
									solicitudEndosoForm.setNombreUsuarioAsignacionEmision(estadistica.getValorVariable1());
									break;
								}
							}							
							solicitudEndosoForm.setFechaEmision(endoso.getFechaCreacion());
						}
					}
				}
			}catch (Exception e) {}
			solicitudEndosoForm.setDescripcionEstatusODT(estatusODT);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}	
	
	/**
	 * Carga un DTO de Solicitud de los datos del form, para efectos de modificacion.
	 * @param solicitudEndosoForm
	 * @param solicitudDTO
	 * @param request
	 * @throws SystemException si al invocar el metodo <code>regresaBigDecimal()</code> de 
	 * <code>UtileriasWeb</code> se genera una exepcion.
	 */
	private void poblarDTOModificar(SolicitudEndosoForm solicitudEndosoForm, 
		SolicitudDTO solicitudDTO, HttpServletRequest request) 
		throws SystemException {
	       
	    if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getEstatus())) {
	            solicitudDTO.setClaveEstatus(Short.parseShort(solicitudEndosoForm.getEstatus()));
	        }
	        if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getNumeroSolicitud())) {
	            solicitudDTO.setIdToSolicitud(UtileriasWeb.regresaBigDecimal(solicitudEndosoForm.getNumeroSolicitud()));
	        }
	        if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getTipoPersona())) {
	            solicitudDTO.setClaveTipoPersona(Short.parseShort(solicitudEndosoForm.getTipoPersona()));
	        }
	        if(solicitudDTO.getClaveTipoPersona().shortValue() == 1) {
	            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getNombres())) {
	                solicitudDTO.setNombrePersona(solicitudEndosoForm.getNombres());
	            }
	            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getApellidoPaterno())) {
	                solicitudDTO.setApellidoPaterno(solicitudEndosoForm.getApellidoPaterno());
	            }
	            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getApellidoMaterno())) {
	                solicitudDTO.setApellidoMaterno(solicitudEndosoForm.getApellidoMaterno());
	            }
	        } else if(solicitudDTO.getClaveTipoPersona().shortValue() == 2) {
	            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getRazonSocial())) {
	                solicitudDTO.setNombrePersona(solicitudEndosoForm.getRazonSocial());
	            }
	        }
	        if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getTelefono())) {
	            solicitudDTO.setTelefonoContacto(solicitudEndosoForm.getTelefono());
	        }
	       
	            
	       
	        if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getIdAgente())) {
	            BigDecimal idAgente = UtileriasWeb.regresaBigDecimal(solicitudEndosoForm.getIdAgente());
	            solicitudDTO.setCodigoAgente(idAgente);
	            
	            AgenteDTO agenteDTO = new AgenteDTO();
	            agenteDTO.setIdTcAgente(idAgente.intValue());
	            
	            Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
	            String nombreUsuario = usuario != null ? usuario.getNombreUsuario() : "";
	            
	            AgenteDN agenteDN = AgenteDN.getInstancia();
	            agenteDTO = agenteDN.verDetalleAgente(agenteDTO, nombreUsuario);
	            
	            solicitudDTO.setNombreAgente(agenteDTO.getNombre());
	            solicitudDTO.setNombreOficinaAgente(agenteDTO.getNombreOficina());
	            /*solicitudDTO.setIdOficina(idOficina);
	            solicitudDTO.setCodigoEjecutivo(codigoEjecutivo);
	            solicitudDTO.setNombreEjecutivo(agenteDTO.getNombreOficina());*/
	        }
	        if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getCorreos())) {
	            solicitudDTO.setEmailContactos(solicitudEndosoForm.getCorreos());
	        }
	        if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getClaveOpcionEmision())) {
	            solicitudDTO.setClaveOpcionEmision(UtileriasWeb.regresaShort(solicitudEndosoForm.getClaveOpcionEmision()));
	        } else {
	            solicitudDTO.setClaveOpcionEmision((short)1);
	        }
	        if(!UtileriasWeb.esObjetoNulo(UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_MIDAS))) {
	            Usuario usuario=(Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_MIDAS);
	            solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
	        }
	       
	        //PROPIEDADES QUE IDENTIFICAN UN ENDOSO
	        try{
	        	Date fechaEndoso=null;
	        	if(solicitudEndosoForm.getFechaInicioVigenciaEndoso()!=null){
		        	fechaEndoso= formatoFecha.parse(solicitudEndosoForm.getFechaInicioVigenciaEndoso());
			        Calendar calendar = Calendar.getInstance();
		        	if(fechaEndoso!=null){
			        	calendar.setTime(fechaEndoso);
			        	solicitudDTO.setFechaInicioVigenciaEndoso(calendar.getTime());
		        	}
	        	}
	        
	        }catch (ParseException e) {
	        	solicitudDTO.setFechaInicioVigenciaEndoso(null);
			}
	        if(solicitudEndosoForm.getIdPoliza()!= null){
	        	solicitudDTO.setIdToPolizaEndosada(new BigDecimal(solicitudEndosoForm.getIdPoliza()));
	        	PolizaDTO poliza= PolizaDN.getInstancia().getPorId(solicitudDTO.getIdToPolizaEndosada());
	        	if(poliza!=null){
	        		  solicitudDTO.setProductoDTO(poliza.getCotizacionDTO().getSolicitudDTO().getProductoDTO());
	        	}
	        	
	        }
	        solicitudDTO.setClaveTipoEndoso(new BigDecimal(solicitudEndosoForm.getTipoEndoso()));
	        
	        if(solicitudDTO.getClaveTipoEndoso()!=null && solicitudDTO.getClaveTipoEndoso().intValue()== Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
	            if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getClaveMotivoEndoso())){
	        	solicitudDTO.setClaveMotivoEndoso(Short.valueOf(solicitudEndosoForm.getClaveMotivoEndoso()));
	            }
	            
	        }
	      
	    }

	/**
	 * Carga ciertos datos de <code>SolicitudForm</code> de un objeto <code>SolicitudEndosoForm</code>
	 * para manejar el listar filtrado, en <code> SolicitudAction </code>.
	 * @param solicitudForm
	 * @param solicitudEndosoForm
	 */
	private void cargaSolicitudFormDeSolicitudEndosoForm(SolicitudForm solicitudForm, 
		SolicitudEndosoForm solicitudEndosoForm){
	    if(!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getEstatus())) {
		solicitudForm.setEstatus(solicitudEndosoForm.getEstatus());
        	}
        	if (!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getNombres())) {
        	    solicitudForm.setNombres(solicitudEndosoForm.getNombres());
        	}
        	if (!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getApellidoPaterno())) {
        	    solicitudForm.setApellidoPaterno(solicitudEndosoForm.getApellidoPaterno());
        	}
        	if (!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getApellidoMaterno())) {
        	    solicitudForm.setApellidoMaterno(solicitudEndosoForm.getApellidoMaterno());
        	}
        	if (!UtileriasWeb.esCadenaVacia(solicitudEndosoForm.getNumeroSolicitud())) {
        	    solicitudForm.setNumeroSolicitud(solicitudEndosoForm.getNumeroSolicitud());
        	}
	    
	}
	
	/**
	 * Carga los datos necesarios para cargar la pantalla de usuarios
	 * a los que se le puede asignarle una solicitud de un Endoso
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public ActionForward mostrarAsignar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) 
		throws ExcepcionDeAccesoADatos, SystemException {
		String reglaNavegacion = Sistema.EXITOSO;
		//UsuarioDTO usuarioDTO = new UsuarioDTO();
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		List<Usuario> usuarios = new ArrayList<Usuario>();
		String idToSolicitud = request.getParameter("idToSolicitud");
		if(!UtileriasWeb.esCadenaVacia(idToSolicitud) && !idToSolicitud.toLowerCase().startsWith("und")) {
			cotizacionForm.setIdToSolicitud(idToSolicitud);
			SolicitudDTO solicitudDTO= SolicitudEndosoDN.getInstancia("").getPorId(UtileriasWeb.regresaBigDecimal(idToSolicitud));
			if(solicitudDTO!=null&& solicitudDTO.getClaveTipoEndoso().intValue()==Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION){
				usuarios = UsuarioWSDN.getInstancia()
						.obtieneUsuariosSinRolesPorNombreRol(
								usuario.getNombreUsuario(),
								usuario.getIdSesionUsuario(),
								Sistema.ROL_SUSCRIPTOR_OT,
								Sistema.ROL_AGENTE_EXTERNO,
								Sistema.ROL_SUSCRIPTOR_EXT);
			}else{
				usuarios = UsuarioWSDN.getInstancia()
						.obtieneUsuariosSinRolesPorNombreRol(
								usuario.getNombreUsuario(),
								usuario.getIdSesionUsuario(),
								Sistema.ROL_SUSCRIPTOR_COT,
								Sistema.ROL_AGENTE_EXTERNO,
								Sistema.ROL_SUSCRIPTOR_EXT);
			}
		}
		
		
		cotizacionForm.setUsuarios(usuarios);
		return mapping.findForward(reglaNavegacion);
	}
	
	
	/**
	 * Asigna una  solicitud de endoso a un usuario seleccionado
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void asignar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		boolean error=false;
		CotizacionForm cotizacionForm = (CotizacionForm) form;
		CotizacionDTO cotizacionDTO = null;
		StringBuilder buffer = new StringBuilder();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item>");
		try {
			if(!UtileriasWeb.esCadenaVacia(cotizacionForm.getIdToSolicitud()) & !cotizacionForm.getIdToSolicitud().equalsIgnoreCase("undefined")) {
				SolicitudEndosoDN solicitudEndososDN = SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				SolicitudDTO solicitudDTO = solicitudEndososDN.getPorId(UtileriasWeb.regresaBigDecimal(cotizacionForm.getIdToSolicitud()));
				if(solicitudDTO!=null){
					cotizacionDTO= solicitudEndososDN.generarCotizacionASolicitud(solicitudDTO.getIdToSolicitud(),cotizacionForm.getCodigoUsuarioOrdenTrabajo());
					if(cotizacionDTO != null){
						if (solicitudDTO.getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION || 
								solicitudDTO.getClaveTipoEndoso().shortValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO) {
							SolicitudDN.getInstancia().registraEstadisticas(
									solicitudDTO.getIdToSolicitud(),
									cotizacionDTO.getIdToCotizacion(),
									null,
									null,
									cotizacionForm.getCodigoUsuarioOrdenTrabajo(),
									Sistema.SE_ASIGNA_SOL_A_ODT);
							buffer.append("<id>1</id><description><![CDATA[La solicitud se asign\u00f3 correctamente.<br>El numero de Orden de Trabajo  es: ODT-"
											+ UtileriasWeb.llenarIzquierda(
															cotizacionDTO.getIdToCotizacion().toBigInteger().toString(),"0", 8)
											+ "]]></description>");
						} else {
							SolicitudDN.getInstancia().registraEstadisticas(
									solicitudDTO.getIdToSolicitud(),
									cotizacionDTO.getIdToCotizacion(),
									null,
									null,
									cotizacionForm.getCodigoUsuarioOrdenTrabajo(),
									Sistema.SE_ASIGNA_SOL_A_COT);
							buffer.append("<id>1</id><description><![CDATA[La solicitud se asign\u00f3 correctamente.<br>El numero de cotizacion es: COT-"
											+ UtileriasWeb.llenarIzquierda(
															cotizacionDTO.getIdToCotizacion().toBigInteger().toString(),
															"0", 8)
											+ "]]></description>");
						}
						solicitudDTO.setClaveEstatus(Sistema.ESTATUS_SOL_ASIGNADA);
						solicitudEndososDN.actualizar(solicitudDTO);
						cotizacionForm.setIdToCotizacion(cotizacionDTO.getIdToCotizacion().toString());
						
					}
				
				}
			
			}
				
			
		} catch (ExcepcionDeAccesoADatos e) {
			error=true;
			e.printStackTrace();
		} catch (SystemException e) {
			error=true;
			e.printStackTrace();
		} catch (Exception e){error=true;
			e.printStackTrace();
		}
		if(error){
			buffer.append("<id>2</id><description><![CDATA[Ocurri\u00f3 un error al asignar la orden de trabajo.]]></description>");
		}
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {e.printStackTrace();}
	}
	
	
	
	//METODOS AGREGADOS PARA ASIGNAR UNA ORDEN DE TRABAJO CUANDO SEA UN ENDOSO DE MODIFICACION
	/**
	 * Action para presentar acciones , para autoasignado de una solicitud.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarAccionesSolicitudEndoso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}
	  /**
         * Guarda una solicitud, con los datos capturados por el Usuario en <code>SolicitudEndosoForm</code>
         * @param mapping
         * @param form
         * @param request
         * @param response
         * @return mapping.findForward(reglaNavegacion).
         */
	public ActionForward autoAsignarOrdenTrabajo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		SolicitudEndosoForm solicitudEndosoForm = null;
		if(session.getAttribute("solicitudEndosoForm") != null) {
		    	solicitudEndosoForm = (SolicitudEndosoForm) session.getAttribute("solicitudEndosoForm");
			session.removeAttribute("solicitudEndosoForm");
		} else {
		    solicitudEndosoForm = (SolicitudEndosoForm) form;
		}
		String accion = request.getParameter("accion");
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		try {
			poblarDTO(solicitudEndosoForm, solicitudDTO, request);
			solicitudDTO.setIdToPolizaAnterior(new BigDecimal(0));
			solicitudDTO.setClaveTipoSolicitud(Sistema.SOLICITUD_TIPO_ENDOSO);
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			if(accion.equals("1")) {
				solicitudDTO.setClaveEstatus(Sistema.ESTATUS_SOL_ASIGNADA);
				
			} else {
				solicitudDTO.setClaveEstatus(Sistema.ESTATUS_SOL_EN_PROCESO);
			}
			solicitudDTO.setFechaCreacion(new Date());
			solicitudDTO.setFechaModificacion(new Date());
			solicitudDTO = solicitudDN.agregar(solicitudDTO);
			solicitudDN.registraEstadisticas(solicitudDTO.getIdToSolicitud(), 
					null, null,null, usuario.getNombreUsuario(), Sistema.SE_CREA_SOLICITUD);
			
			if(accion.equals("1")) {
				CotizacionDTO cotizacionDTO =SolicitudEndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).
				generarCotizacionASolicitud(solicitudDTO.getIdToSolicitud(),UtileriasWeb.obtieneNombreUsuario(request));
				if(cotizacionDTO!=null){
        				SolicitudDN.getInstancia().registraEstadisticas(solicitudDTO.getIdToSolicitud(), 
        					cotizacionDTO.getIdToCotizacion(), null,null, usuario.getNombreUsuario(), Sistema.SE_ASIGNA_SOL_A_ODT);
        				request.setAttribute("id", cotizacionDTO.getIdToCotizacion().toString());
        				reglaNavegacion = Sistema.ALTERNO;
				}
			} else {
				solicitudEndosoForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
				this.poblarForm(solicitudDTO, (SolicitudEndosoForm)form,request);
				request.setAttribute("documentos", new ArrayList<DocumentoDigitalSolicitudDTO>());
				session.removeAttribute("idSolicitud");
				session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
				this.poblarAgentes((SolicitudEndosoForm)form, usuario);
			}
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
}
