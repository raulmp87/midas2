package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;


public class SolicitudEndosoSN {
	private SolicitudEndosoFacadeRemote beanRemoto;

	public SolicitudEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SolicitudEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public SolicitudDTO agregar(SolicitudDTO SolicitudEndosoDTO) {
		try {
			return beanRemoto.save(SolicitudEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public SolicitudDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<SolicitudDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SolicitudDTO> listarFiltrado(SolicitudDTO SolicitudEndosoDTO, Usuario usuario) {
		try {
			return beanRemoto.findByEstatus(SolicitudEndosoDTO.getClaveEstatus(), usuario.getNombreUsuario());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public SolicitudDTO actualizar(SolicitudDTO SolicitudEndosoDTO) {
		try {
			return beanRemoto.update(SolicitudEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SolicitudDTO SolicitudEndosoDTO) {
		try {
			beanRemoto.delete(SolicitudEndosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SolicitudDTO> listarPorUsuario(Usuario usuario) {
		try {
			return beanRemoto.findByUserId(usuario.getNombreUsuario());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SolicitudDTO> listarPorEstatus(Short claveEstatus, Usuario usuario){
		try {
			return beanRemoto.findByEstatus(claveEstatus, usuario.getId().toString());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

}
