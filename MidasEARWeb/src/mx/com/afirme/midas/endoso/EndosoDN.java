package mx.com.afirme.midas.endoso;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.AgrupacionCotDTO;
import mx.com.afirme.midas.cotizacion.primerriesgoluc.PrimerRiesgoLUCDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDN;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDN;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDN;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoSN;
import mx.com.afirme.midas.endoso.estructura.EstructuraEndosoDN;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoDN;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoDTO;
import mx.com.afirme.midas.endoso.riesgo.RiesgoEndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoDN;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo.SoporteReaseguroCotizacionDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.emision.EmisionDN;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

public class EndosoDN {
	private static final EndosoDN INSTANCIA = new EndosoDN();
	private static String nombreUsuario;
	private double valorPrimaNetaEndoso;
	private double valorRecargoPagoFraccionadoEndoso;
	private double valorDerechosEndoso;
	private double valorBonificacionComisionEndoso;
	private double valorIVAEndoso;
	private double valorPrimaTotalEndoso;
	private double valorBonificacionComisionRPFEndoso;
	private double valorComisionEndoso;
	private double valorComisionRPFEndoso;
	private boolean existeReciboPagado;
	
	public static EndosoDN getInstancia(String nombreUsuario) {
		EndosoDN.nombreUsuario = nombreUsuario;
		return EndosoDN.INSTANCIA;
	}

	public void agregar (EndosoDTO endosoDTO)  throws ExcepcionDeAccesoADatos, SystemException {
		EndosoSN endosoSN;
		try {
			endosoSN = new EndosoSN();
			endosoSN.agregar(endosoDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}
	
	public EndosoDTO actualizar (EndosoDTO endosoDTO)  throws ExcepcionDeAccesoADatos, SystemException {
		EndosoSN endosoSN;
		try {
			endosoSN = new EndosoSN();
			return endosoSN.actualizar(endosoDTO);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}
	
	public EndosoDTO getPorId(EndosoId id) throws ExcepcionDeAccesoADatos, SystemException {
		EndosoSN endosoSN;
		try {
			endosoSN = new EndosoSN();
			return endosoSN.getPorId(id);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}

	public List<EndosoDTO> listarCotizaciones() throws ExcepcionDeAccesoADatos,
			SystemException {
		return null;
	}

	public EndosoDTO getUltimoEndoso(BigDecimal idToPoliza) throws SystemException {
		EndosoSN endosoSN = new EndosoSN();
		return endosoSN.getUltimoEndoso(idToPoliza);
	}
	
	public EndosoDTO getPenultimoEndoso(BigDecimal idToPoliza) throws SystemException {
		EndosoSN endosoSN = new EndosoSN();
		return endosoSN.getPenultimoEndoso(idToPoliza);
	}
	
	public Map<String, String> emitirEndosoSP(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos,SystemException {
		//EndosoSN endosoSN = new EndosoSN();
		Map<String, String> mensaje = new HashMap<String, String>();
		String fileName=null;
		//Emision
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		
		BigDecimal pctPagoFraccionado = new BigDecimal(0);
		//Obtiene % por forma de Pago		
		if(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario()==null || cotizacionDTO.getValorRecargoPagoFraccionadoUsuario() == null){		
			pctPagoFraccionado = BigDecimal.valueOf(CotizacionDN.getInstancia(null).obtenerPorcentajeRecargoPagoFraccionadoCotizacion(cotizacionDTO));						
			CotizacionDN.getInstancia(null).modificarRecargoPagoFraccionado(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_RPF_SISTEMA, pctPagoFraccionado.doubleValue(), false, false);			
		}else{
			pctPagoFraccionado = BigDecimal.valueOf(cotizacionDTO.getPorcentajeRecargoPagoFraccionadoUsuario());
		}
		
		cotizacionDTO.setPorcentajePagoFraccionado(pctPagoFraccionado.doubleValue());
				
		Double ivaCotizacion =CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), nombreUsuario);		

		//Se genera la estructura del endoso
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(polizaDTO.getIdToPoliza());

		if(endosoDTO.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CANCELACION){
			mensaje.put("mensaje", "</br>Error al generar el Endoso.</br> La poliza ya esta cancelada");
			mensaje.put("icono", "10");//Error				
			return mensaje;
		}
		int numeroEndosoSigiente = endosoDTO.getId().getNumeroEndoso() + 1;
		boolean estructuraLista = false;
		try{
			estructuraLista = EstructuraEndosoDN.getInstancia().generaEstructuraEndoso(
					cotizacionDTO.getIdToCotizacion(), polizaDTO.getIdToPoliza(),
					numeroEndosoSigiente, (short) 2,
					endosoDTO.getIdToCotizacion(), 
					nombreUsuario);
		}catch (Exception e) {
			LogDeMidasWeb.log("EndosoDN.emitirEndoso : Excepcion en EstructuraEndosoDN.generaEstructuraEndoso con poliza|numeroendoso = " + 
					endosoDTO.getId().getIdToPoliza().toString()+ "|" +numeroEndosoSigiente, Level.INFO, null);
		}
		if(estructuraLista){
			//Se emite el endoso
			//mensaje = endosoSN.emitirEndoso(cotizacionDTO,ivaCotizacion);
			mensaje = emitirEndoso(cotizacionDTO,ivaCotizacion, polizaDTO,endosoDTO);
			EndosoDTO endosoEmitido = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(polizaDTO.getIdToPoliza());
			
			if(mensaje.get("icono").equals("30")){ //Exito de Emision
				cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_EMITIDA);
				cotizacionDTO.setPorcentajeIva(ivaCotizacion);
				cotizacionDN.modificar(cotizacionDTO);
				//se actualiza el estatus de la poliza si fue un endoso de cancelacion
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
					polizaDTO.setClaveEstatus(Sistema.ESTATUS_CANCELADA);
					PolizaDN.getInstancia().actualizar(polizaDTO);
				}
				
				//Se le agrega al primer endoso la llave fiscal de los recibos
				
				String numeroPoliza =mensaje.get("mensaje")+"<b>"+UtileriasWeb.getNumeroPoliza(polizaDTO)+"</b>";
				mensaje.put("mensaje", numeroPoliza);
				
				String numeroEndoso = mensaje.get("mensaje") + "</br> El n\u00famero de endoso es: " + mensaje.get("noendoso");
				mensaje.put("mensaje", numeroEndoso);

				mensaje.put("fechacreacion",UtileriasWeb.getFechaString(endosoEmitido.getFechaCreacion()));
				mensaje.put("idtocotizacion", cotizacionDTO.getIdToCotizacion().toString());
				mensaje.put("idtopoliza", endosoEmitido.getId().getIdToPoliza().toString());
				mensaje.put("noendoso", endosoEmitido.getId().getNumeroEndoso().toString());
				//Se busca si existen movimientos que regeneren recibo o tengan valor en prima neta
				boolean notificarInterfaz = Boolean.FALSE;
				MovimientoCotizacionEndosoDTO movimientoEndosoDTO = new MovimientoCotizacionEndosoDTO();
				movimientoEndosoDTO.setIdToCotizacion(endosoEmitido.getIdToCotizacion());
				List<MovimientoCotizacionEndosoDTO> movimientosEndoso = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).listarFiltrado(movimientoEndosoDTO);
				
				for(MovimientoCotizacionEndosoDTO movimiento: movimientosEndoso){
					if(movimiento.getValorDiferenciaPrimaNeta().intValue() != 0 || 
						(movimiento.getClaveRegeneraRecibos() != null && movimiento.getClaveRegeneraRecibos().intValue() == Sistema.SI_REGENERA_RECIBOS)){
						notificarInterfaz = true;
						break;
					}
				}
				
				if(notificarInterfaz){
					EndosoIDTO endosoIDTO = null;
					List<ReciboDTO> reciboDTOList = null;
					try{
						
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando EndosoIDN.emiteEndoso con id de Poliza|NoEndoso = " + 
								endosoEmitido.getId().getIdToPoliza().toString()+ "|" +endosoEmitido.getId().getNumeroEndoso(), Level.INFO, null);

						endosoIDTO = EndosoIDN.getInstancia().emiteEndoso(endosoEmitido.getId().getIdToPoliza().toString()+ "|" +endosoEmitido.getId().getNumeroEndoso(), nombreUsuario).get(0);
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : EndosoIDN.emiteEndoso con id de Poliza|NoEndoso " + 
									endosoEmitido.getId().getIdToPoliza().toString()+ "|" +endosoEmitido.getId().getNumeroEndoso()+ " ejecutado.", Level.INFO, null);
						
					}catch (Exception e){
						LogDeMidasWeb.log("PolizaDN.emitirEndoso : Excepcion en EndosoIDN.emiteEndoso con id de Poliza = " + 
								polizaDTO.getIdToPoliza() + "Endoso Numero= "+endosoEmitido.getId().getNumeroEndoso(), Level.WARNING, e);
						LogDeMidasWeb.log("PolizaDN.emitirEndoso : Ejecutando ReciboDN.consultaRecibos con id de Poliza = " + 
								polizaDTO.getIdToPoliza()  + "Endoso Numero= "+endosoEmitido.getId().getNumeroEndoso(), Level.INFO, null);

						reciboDTOList = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(endosoEmitido.getId().getIdToPoliza(),endosoEmitido.getId().getNumeroEndoso());

						endosoIDTO = null;
						LogDeMidasWeb.log("PolizaDN.emitirEndoso: ReciboDN.consultaRecibos con id de Poliza = " + 
								polizaDTO.getIdToPoliza()  + "Endoso Numero= "+endosoEmitido.getId().getNumeroEndoso(), Level.INFO, null);				
					}
						
					if (endosoIDTO != null && endosoIDTO.getLlaveFiscal() != null) {
						try{
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);						
							EmisionDN.getInstancia().generarRecibos();
							if (endosoEmitido != null) {
								endosoEmitido.setLlaveFiscal(endosoIDTO.getLlaveFiscal());
								EndosoDN.getInstancia(nombreUsuario).actualizar(endosoEmitido);
							}						
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
						}
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);

					}else if (reciboDTOList != null && reciboDTOList.size() > 0) {
						
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : " + reciboDTOList.size() + " recibo(s) obtenido(s).", Level.INFO, null);
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);
						try{
							EmisionDN.getInstancia().generarRecibos();
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
						}
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
					
						if (endosoEmitido != null){
							endosoEmitido.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
							EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoEmitido);
						}

					}else{
						LogDeMidasWeb.log("EndosoDN.consultaRecibos : consultando recibos de endoso...", Level.INFO, null);
						try{
							reciboDTOList = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(endosoEmitido.getId().getIdToPoliza(),endosoEmitido.getId().getNumeroEndoso());
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.consultaRecibos : error al consultar los recibos...", Level.INFO, null);
						}
						
						if(reciboDTOList != null && reciboDTOList.size() > 0){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : " + reciboDTOList.size() + " recibo(s) obtenido(s).", Level.INFO, null);
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);					
							try{
								EmisionDN.getInstancia().generarRecibos();
							}catch (Exception e){
								LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
							}
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
							
							if (endosoEmitido != null && reciboDTOList!= null && reciboDTOList.size() > 0 ){
								endosoEmitido.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
								EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoEmitido);
							}								
						}
					}
					
					byte[] archivoPDF = null;
					if(endosoIDTO != null && endosoIDTO.getLlaveFiscal() != null){
						try{
							archivoPDF = EmisionDN.getInstancia().obtieneFactura(endosoIDTO.getLlaveFiscal());
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.obtieneFactura: error al obtener la factura con llave fiscal:"+endosoIDTO.getLlaveFiscal(), Level.INFO, null);
						}
					}else if(reciboDTOList != null && reciboDTOList.size() > 0){
						try{
							archivoPDF = EmisionDN.getInstancia().obtieneFactura(reciboDTOList.get(0).getLlaveFiscal());
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.obtieneFactura: error al obtener la factura con llave fiscal:"+reciboDTOList.get(0).getLlaveFiscal(), Level.INFO, null);
						}						
					}
							
					try {
						if (archivoPDF != null) {
							if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1)
								fileName = Sistema.UPLOAD_FOLDER;
							else
								fileName = Sistema.LINUX_UPLOAD_FOLDER;
							fileName = fileName + "Recibo_Poliza" + polizaDTO.getIdToPoliza().toBigInteger() +"_Endoso_"+ endosoEmitido.getId().getNumeroEndoso() + ".pdf";
							FileOutputStream fos = new FileOutputStream(fileName);
							fos.write(archivoPDF);
							fos.flush();                                    
							fos.close();
							
							String recibosGenerados =mensaje.get("mensaje")+"</br>Se generaron y guardaron correctamente los recibos de la P\u00f3liza";
							mensaje.put("mensaje", recibosGenerados);							
						}else {
							String recibosNoGenerados =mensaje.get("mensaje")+"</br>NO Se generaron los recibos de la P\u00f3liza";
							mensaje.put("mensaje", recibosNoGenerados);
						}
						
					} catch (IOException e) {
						e.printStackTrace();
						String recibosNoGenerados =mensaje.get("mensaje")+"</br>NO Se generaron los recibos de la P\u00f3liza";
						mensaje.put("mensaje", recibosNoGenerados);
					}			
					
				}				
			}else{ //Roll Back de la estructura del endoso //Error al usuario ocurrio un error en la transaccion
				try{
					EstructuraEndosoDN.getInstancia().deshaceEstructuraEndoso(polizaDTO.getIdToPoliza(), numeroEndosoSigiente, nombreUsuario);
				}catch (Exception e) {
					LogDeMidasWeb.log("EndosoDN.emitirEndoso : Excepcion en EstructuraEndosoDN.deshaceEstructuraEndoso con poliza|numeroendoso = " + 
							endosoDTO.getId().getIdToPoliza().toString()+ "|" +numeroEndosoSigiente, Level.INFO, null);						
				}
			}
			
		}else{//Roll Back de la estructura del endoso//Error al usuario no c genero la estructura del endoso
			try{
				EstructuraEndosoDN.getInstancia().deshaceEstructuraEndoso(polizaDTO.getIdToPoliza(), numeroEndosoSigiente, nombreUsuario);
			}catch (Exception e) {
				LogDeMidasWeb.log("EndosoDN.emitirEndoso : Excepcion en EstructuraEndosoDN.deshaceEstructuraEndoso con poliza|numeroendoso = " + 
						endosoDTO.getId().getIdToPoliza().toString()+ "|" +numeroEndosoSigiente, Level.INFO, null);						
			}
			mensaje.put("mensaje", "</br>Error al generar el Endoso.");
			mensaje.put("icono", "10");//Error	
		}
		return mensaje;
	}

	
	public Map<String, String> emitirEndoso(CotizacionDTO cotizacionDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		EndosoSN endosoSN = new EndosoSN();
		String fileName=null;
		Map<String, String> mensaje = new HashMap<String, String>();
		//Emision
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(nombreUsuario);
		
		//Obtiene % por forma de Pago
		FormaPagoIDTO formaPago = FormaPagoDN.getInstancia(nombreUsuario).getPorId(cotizacionDTO.getIdFormaPago().intValue(),cotizacionDTO.getIdMoneda().shortValue());
		if(formaPago != null){
			cotizacionDTO.setPorcentajePagoFraccionado(formaPago.getPorcentajeRecargoPagoFraccionado().doubleValue());
		}else{
			cotizacionDTO.setPorcentajePagoFraccionado(0D);
		}
		Double ivaCotizacion =CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionDTO.getIdToCotizacion(), nombreUsuario);
		
		//Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del ultimo recibo pagado o el 1 emitido
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		List<ReciboDTO> recibosPolizaOriginal = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(polizaDTO.getIdToPoliza(),(short)0);
		ReciboDTO ultimoReciboEmitido= null;
		existeReciboPagado = false;
		for(ReciboDTO reciboDTO:recibosPolizaOriginal){
			if (reciboDTO.getSituacion().trim().equals(Sistema.RECIBO_EMITIDO)){// se valida el primer recibo no pagado emitido
				if (ultimoReciboEmitido == null)
					ultimoReciboEmitido = reciboDTO;
			}else if (reciboDTO.getSituacion().trim().equals(Sistema.RECIBO_PAGADO)){
				existeReciboPagado = true;
			}
		}
		if(ultimoReciboEmitido != null || existeReciboPagado || cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
			//Se consulta permiteCancelar
			EndosoIDTO endosoIDTO = new EndosoIDTO();
			EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario)
					.getUltimoEndoso(polizaDTO.getIdToPoliza());
			if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION) {
				if(ultimoEndoso.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CANCELACION){
					//No se puede emitir
					mensaje.put("icono", "10");//Error
					mensaje.put("mensaje", "Error al generar la P\u00f3liza. </br> La poliza que se desea cancelar ya esta cancelada.");	
					return mensaje;
				}	
				Integer tipoEndoso = UtileriasWeb.calculaTipoEndoso(cotizacionDTO);
				endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(
						ultimoEndoso.getId().getIdToPoliza().toString() + "|"
								+ ultimoEndoso.getId().getNumeroEndoso(), nombreUsuario,
						cotizacionDTO.getFechaInicioVigencia(), tipoEndoso);
				cotizacionDTO.setFechaInicioVigencia(endosoIDTO.getFechaInicioVigencia());
				cotizacionDN.modificar(cotizacionDTO);
			}
			//Se emite el endoso
			mensaje = endosoSN.emitirEndoso(cotizacionDTO,ivaCotizacion, existeReciboPagado, recibosPolizaOriginal, endosoIDTO);
	
			if(mensaje.get("icono").equals("30")){ //Exito de Emision
				
				cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_EMITIDA);
				cotizacionDTO.setPorcentajeIva(ivaCotizacion);
				cotizacionDN.modificar(cotizacionDTO);
				//se actualiza el estatus de la poliza si fue un endoso de cancelacion
				if (cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION){
					polizaDTO.setClaveEstatus(Sistema.ESTATUS_CANCELADA);
					PolizaDN.getInstancia().actualizar(polizaDTO);
				}
				
				//Se le agrega al primer endoso la llave fiscal de los recibos
				EndosoId id = new EndosoId(polizaDTO.getIdToPoliza(), Short.valueOf(mensaje.get("noendoso")));
				EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).getPorId(id);
				
				String numeroPoliza =mensaje.get("mensaje")+"<b>"+UtileriasWeb.getNumeroPoliza(polizaDTO)+"</b>";
				mensaje.put("mensaje", numeroPoliza);
				
				String numeroEndoso = mensaje.get("mensaje") + "</br> El n\u00famero de endoso es: " + mensaje.get("noendoso");
				mensaje.put("mensaje", numeroEndoso);
	
				mensaje.put("fechacreacion",UtileriasWeb.getFechaString(endosoDTO.getFechaCreacion()));
				mensaje.put("idtocotizacion", cotizacionDTO.getIdToCotizacion().toString());
				mensaje.put("idtopoliza", endosoDTO.getId().getIdToPoliza().toString());
				mensaje.put("noendoso", endosoDTO.getId().getNumeroEndoso().toString());
				//Se busca si existen movimientos que regeneren recibo o tengan valor en prima neta
				boolean notificarInterfaz = Boolean.FALSE;
				MovimientoCotizacionEndosoDTO movimientoEndosoDTO = new MovimientoCotizacionEndosoDTO();
				movimientoEndosoDTO.setIdToCotizacion(endosoDTO.getIdToCotizacion());
				List<MovimientoCotizacionEndosoDTO> movimientosEndoso = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).listarFiltrado(movimientoEndosoDTO);
				
				for(MovimientoCotizacionEndosoDTO movimiento: movimientosEndoso){
					if(movimiento.getValorDiferenciaPrimaNeta().intValue() != 0 || 
						(movimiento.getClaveRegeneraRecibos() != null && movimiento.getClaveRegeneraRecibos().intValue() == Sistema.SI_REGENERA_RECIBOS)){
						notificarInterfaz = true;
						break;
					}
				}
				
				if(notificarInterfaz){
					List<ReciboDTO> reciboDTOList = null;
					try{
						
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando EndosoIDN.emiteEndoso con id de Poliza|NoEndoso = " + 
								endosoDTO.getId().getIdToPoliza().toString()+ "|" +endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);
						List<EndosoIDTO> listaEndosoTmp = null;
						listaEndosoTmp = EndosoIDN.getInstancia().emiteEndoso(endosoDTO.getId().getIdToPoliza().toString()+ "|" +endosoDTO.getId().getNumeroEndoso(), nombreUsuario);
						if(listaEndosoTmp != null && listaEndosoTmp.size()>0){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : EndosoIDN.emiteEndoso con id de Poliza|NoEndoso " + 
									endosoDTO.getId().getIdToPoliza().toString()+ "|" +endosoDTO.getId().getNumeroEndoso()+ " ejecutado.", Level.INFO, null);
							endosoIDTO =  listaEndosoTmp.get(0);							
						}
					}catch (Exception e){
						LogDeMidasWeb.log("PolizaDN.emitirEndoso : Excepcion en EndosoIDN.emiteEndoso con id de Poliza = " + 
								polizaDTO.getIdToPoliza() + "Endoso Numero= "+endosoDTO.getId().getNumeroEndoso(), Level.WARNING, e);
						LogDeMidasWeb.log("PolizaDN.emitirEndoso : Ejecutando ReciboDN.consultaRecibos con id de Poliza = " + 
								polizaDTO.getIdToPoliza()  + "Endoso Numero= "+endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);
	
						LogDeMidasWeb.log("EndosoDN.consultaRecibos : consultando recibos de endoso...", Level.INFO, null);
						try{
							reciboDTOList = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(endosoDTO.getId().getIdToPoliza(),endosoDTO.getId().getNumeroEndoso());
						}catch (Exception e1){
							LogDeMidasWeb.log("EndosoDN.consultaRecibos : error al consultar los recibos...", Level.INFO, null);
						}
						endosoIDTO = null;
						LogDeMidasWeb.log("PolizaDN.emitirEndoso: ReciboDN.consultaRecibos con id de Poliza = " + 
								polizaDTO.getIdToPoliza()  + "Endoso Numero= "+endosoDTO.getId().getNumeroEndoso(), Level.INFO, null);				
					}
						
					if (endosoIDTO != null && endosoIDTO.getLlaveFiscal() != null) {
						try{
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);
							
							try{
								EmisionDN.getInstancia().generarRecibos();
							}catch (Exception e){
								LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
							}
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
							if (endosoDTO != null) {
								endosoDTO.setLlaveFiscal(endosoIDTO.getLlaveFiscal());
								EndosoDN.getInstancia(nombreUsuario).actualizar(endosoDTO);
							}						
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
						}
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
	
					}else if (reciboDTOList != null && reciboDTOList.size() > 0) {
						
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : " + reciboDTOList.size() + " recibo(s) obtenido(s).", Level.INFO, null);
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);
						try{
							EmisionDN.getInstancia().generarRecibos();
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
						}
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
					
						if (endosoDTO != null){
							endosoDTO.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
							EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoDTO);
						}
	
					}else{
						LogDeMidasWeb.log("EndosoDN.consultaRecibos : consultando recibos de endoso...", Level.INFO, null);
						try{
							reciboDTOList = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(endosoDTO.getId().getIdToPoliza(),endosoDTO.getId().getNumeroEndoso());
						}catch (Exception e1){
							LogDeMidasWeb.log("EndosoDN.consultaRecibos : error al consultar los recibos...", Level.INFO, null);
						}
		
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : " + reciboDTOList.size() + " recibo(s) obtenido(s).", Level.INFO, null);
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Ejecutando Metodo WS EmisionDN.generarRecibos...", Level.INFO, null);					
						try{
							EmisionDN.getInstancia().generarRecibos();
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.emitirEndoso : error al generar los recibos", Level.INFO, null);
						}
						LogDeMidasWeb.log("EndosoDN.emitirEndoso : Metodo WS EmisionDN.generarRecibos ejecutado.", Level.INFO, null);
						
						if (endosoDTO != null && reciboDTOList != null && reciboDTOList.size() > 0){
							endosoDTO.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
							EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoDTO);
						}					
					}
					
					byte[] archivoPDF = null;
	
					if(endosoIDTO != null && endosoIDTO.getLlaveFiscal() != null){
						try{
							archivoPDF = EmisionDN.getInstancia().obtieneFactura(endosoIDTO.getLlaveFiscal());
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.obtieneFactura: error al obtener la factura con llave fiscal:"+endosoIDTO.getLlaveFiscal(), Level.INFO, null);
						}
					}else if(reciboDTOList != null && reciboDTOList.size() > 0){
						try{
							archivoPDF = EmisionDN.getInstancia().obtieneFactura(reciboDTOList.get(0).getLlaveFiscal());
						}catch (Exception e){
							LogDeMidasWeb.log("EndosoDN.obtieneFactura: error al obtener la factura con llave fiscal:"+reciboDTOList.get(0).getLlaveFiscal(), Level.INFO, null);
						}						
					}				
					try {
						if (archivoPDF != null) {
							if (System.getProperty("os.name").toLowerCase().indexOf("windows")!=-1)
								fileName = Sistema.UPLOAD_FOLDER;
							else
								fileName = Sistema.LINUX_UPLOAD_FOLDER;
							fileName = fileName + "Recibo_Poliza" + polizaDTO.getIdToPoliza().toBigInteger() +"_Endoso_"+ endosoDTO.getId().getNumeroEndoso() + ".pdf";
							FileOutputStream fos = new FileOutputStream(fileName);
							fos.write(archivoPDF);
							fos.flush();                                    
							fos.close();
							
							String recibosGenerados =mensaje.get("mensaje")+"</br>Se generaron y guardaron correctamente los recibos de la P\u00f3liza";
							mensaje.put("mensaje", recibosGenerados);							
						}else {
							String recibosNoGenerados =mensaje.get("mensaje")+"</br>NO Se generaron los recibos de la P\u00f3liza";
							mensaje.put("mensaje", recibosNoGenerados);
						}
						
					} catch (IOException e) {
						e.printStackTrace();
						String recibosNoGenerados =mensaje.get("mensaje")+"</br>NO Se generaron los recibos de la P\u00f3liza";
						mensaje.put("mensaje", recibosNoGenerados);
					}			
					
				}
			}
		}else{
			//No se puede emitir
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la P\u00f3liza. </br> No se pudo determinar la fecha de inicio de vigencia del Endoso.");		
		}
		return mensaje;

	}
	public BigDecimal obtenerCantidadEndososPoliza (BigDecimal idToPoliza)  throws ExcepcionDeAccesoADatos, SystemException {
		try {
			return new EndosoSN().obtenerCantidadEndososPoliza(idToPoliza);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}
	
	public List<EndosoDTO> listarEndososPorPoliza(BigDecimal idToPoliza,
			boolean ordenAscendente) throws SystemException {
		return new EndosoSN().buscarPorPropiedad("id.idToPoliza", idToPoliza,ordenAscendente);
	}

	public void emiteEndosoCancelacionAutomatica(EndosoIDTO cacelable) throws ExcepcionDeAccesoADatos, SystemException{
		Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(Sistema.USUARIO_SISTEMA);

		if (usuario == null){
			throw new SystemException("El endoso " + cacelable.getIdPoliza() + "|" + cacelable.getNumeroEndoso() + " no se puede cancelar debido a que no se pudo obtener el usuario: "+ Sistema.USUARIO_SISTEMA);
		}
		
		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cacelable.getIdPoliza());
	
		if(ultimoEndoso.getClaveTipoEndoso().shortValue() == Sistema.TIPO_ENDOSO_CANCELACION){
			EndosoIDN.getInstancia().emiteEndoso(
					ultimoEndoso.getId().getIdToPoliza().toString() + "|"+ ultimoEndoso.getId().getNumeroEndoso(),nombreUsuario);
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Termina ReciboDN.emiteRecibosPoliza con id de Poliza|NoEndoso "
									+ ultimoEndoso.getId().getIdToPoliza().toString()
									+ "|" + ultimoEndoso.getId().getNumeroEndoso()
									+ " ejecutado.", Level.INFO, null);		
			throw new SystemException("La poliza que se desea cancelar ya esta cancelada");
		}
		
		if (cacelable.getNumeroEndoso().intValue() > 0 
				&& cacelable.getNumeroEndoso().intValue() != this.buscarNumeroEndosoUltimoCFP(cacelable.getIdPoliza()).intValue()){
			throw new SystemException("Se desea cancelar un endoso, la notificacion fue enviada a los destinatarios correspondientes");
		}
		EndosoDTO endosoCancelable = ultimoEndoso;
		if (endosoCancelable == null){
			throw new SystemException("El endoso " + cacelable.getIdPoliza() + "|" + cacelable.getNumeroEndoso()+ " no se localizo en MIDAS");
		}
		
		CotizacionDTO cotizacionOrigen = CotizacionDN.getInstancia(nombreUsuario).getPorId(endosoCancelable.getIdToCotizacion());
		//Obtiene % por forma de Pago
		FormaPagoIDTO formaPago = FormaPagoDN.getInstancia(nombreUsuario).getPorId(cotizacionOrigen.getIdFormaPago().intValue(),cotizacionOrigen.getIdMoneda().shortValue());
		if(formaPago != null){
			cotizacionOrigen.setPorcentajePagoFraccionado(formaPago.getPorcentajeRecargoPagoFraccionado().doubleValue());
		}else{
			cotizacionOrigen.setPorcentajePagoFraccionado(0D);
		}		
		Double ivaCotizacion =CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionOrigen.getIdToCotizacion(), nombreUsuario);	
		if(ivaCotizacion==null){
		    throw new SystemException("No se pudo encontrar el valor para el IVA");
		}
		EndosoDTO endosoCancelado;
		EndosoSN endosoSN = new EndosoSN();
		
		//Se actualiza la fecha de inicio de vigencia a la fecha fin de vigencia del ultimo recibo pagado o el 1 emitido
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(cacelable.getIdPoliza());
		List<ReciboDTO> recibosPolizaOriginal = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(polizaDTO.getIdToPoliza(),(short)0);
//		ReciboDTO ultimoReciboEmitido= null;
		existeReciboPagado = false;
		for(ReciboDTO reciboDTO:recibosPolizaOriginal){
			if (reciboDTO.getSituacion().trim().equals(Sistema.RECIBO_PAGADO)){
				existeReciboPagado = true;
			}
		}
//		if(UtileriasWeb.sonFechasIguales(ultimoReciboEmitido.getFechaIncioVigencia(), cacelable.getFechaInicioVigencia())){
//			throw new SystemException("La fecha inicio de vigencia no corresponde con la fecha inicion de vigencia del primer recibo emitido");
//		}
		try{
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Ejecutando la emision del endoso de cancelacion de la idpoliza: "+cacelable.getIdPoliza(), Level.INFO, null);			
			endosoSN.emiteEndosoCancelacionAutomatica(cotizacionOrigen, nombreUsuario, ivaCotizacion,existeReciboPagado, recibosPolizaOriginal, cacelable);
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Termina la emision del endoso de cancelacion de la idpoliza: "+cacelable.getIdPoliza(), Level.INFO, null);
			endosoCancelado = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(cacelable.getIdPoliza());
			polizaDTO = endosoCancelado.getPolizaDTO();
			polizaDTO.setClaveEstatus(Sistema.ESTATUS_CANCELADA);
			PolizaDN.getInstancia().actualizar(polizaDTO);
		}catch (Exception e){
			 throw new SystemException("Error al emitir el endoso de cancelacion: "+ e.getMessage());
		}
		//Genera Movimientos de cancelacion
		MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).calcularDiferenciasDeEndosoDeCancelacion(endosoCancelado.getIdToCotizacion());
		try{
			
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica: Ejecutando ReciboEndosoDN.emiteRecibosEndoso con id de Poliza|NoEndoso = " + 
					endosoCancelado.getId().getIdToPoliza().toString()+ "|" +endosoCancelado.getId().getNumeroEndoso(), Level.INFO, null);

			EndosoIDN.getInstancia().emiteEndoso(endosoCancelado.getId().getIdToPoliza().toString()+ "|" +endosoCancelado.getId().getNumeroEndoso(), nombreUsuario);
				LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Termina ReciboDN.emiteRecibosPoliza con id de Poliza|NoEndoso " + 
						endosoCancelado.getId().getIdToPoliza().toString()+ "|" +endosoCancelado.getId().getNumeroEndoso()+ " ejecutado.", Level.INFO, null);
			//TODO Actualizar el endoso cancelaco con la informacion de "referencia"
		}catch (Exception e){
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Excepcion en ReciboDN.emiteRecibosPoliza con id de Poliza = " + 
					polizaDTO.getIdToPoliza() + "Endoso Numero= "+endosoCancelado.getId().getNumeroEndoso(), Level.WARNING, e);
		}
		//Se insertan estadisticas
		try{
			CotizacionDTO cotizacionEndoso = CotizacionDN.getInstancia(
					nombreUsuario).getPorId(
					endosoCancelado.getIdToCotizacion());
			EstadisticasEstadoDN.getInstancia()
					.insertarEstadisticasAutomaticas(
							cotizacionEndoso.getSolicitudDTO()
									.getIdToSolicitud(),
							cotizacionEndoso.getIdToCotizacion(),
							cacelable.getIdPoliza(), BigDecimal.valueOf(endosoCancelado.getId().getNumeroEndoso().doubleValue()));
		}catch (Exception e){
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Excepcion en EstadisticasEstadoDN.insertarEstadisticasAutomaticas con id de Poliza = " + 
					polizaDTO.getIdToPoliza() + "Endoso Numero= "+endosoCancelado.getId().getNumeroEndoso(), Level.WARNING, e);
		}	
		//Se notifica el endoso
		List<String> errores = new ArrayList<String>();		
		try{
			SoporteReaseguroCotizacionDN soporteReaseguroCotizacionDN = new SoporteReaseguroCotizacionDN(endosoCancelable.getIdToCotizacion());
			soporteReaseguroCotizacionDN.notificarEndosoCancelacion(endosoCancelado.getId().getIdToPoliza(), endosoCancelado.getId().getNumeroEndoso(), endosoCancelado.getIdToCotizacion(), endosoCancelado.getFechaCreacion(), endosoCancelado.getFechaInicioVigencia(), endosoCancelado.getFactorAplicacion(), errores, usuario,null,null);
			
		}catch (Exception e) {
			String descError = "";
			if(errores != null)
				descError = obtenerDescError(errores);
			UtileriasWeb.registraLogInteraccionReaseguro("EndosoDN.emiteEndosoCancelacionAutomatica", 
					Sistema.MODULO_REASEGURO, "SoporteReaseguro", "notificarEmision", 
					nombreUsuario, 
					"idToCotizacion = "+ endosoCancelado.getIdToCotizacion() + " idToPoliza = "+ cacelable.getIdPoliza()+ " noEndoso = "+ endosoCancelado.getId().getNumeroEndoso() +" fechaCreacionEndoso = "+endosoCancelado.getFechaCreacion(), 
					"void()", e, descError, "error al notificar la emision de un endoso de cancelacion automatica", "CotizacionDN", Sistema.MODULO_DANIOS);
		}
	}
	
	public String obtenerDescError(List<String> errores) {
		StringBuilder descError = new StringBuilder("");
		for(String error : errores)
			descError.append(error ).append("\n");
		return descError.toString();
	}

	public void emiteEndosoCancelacionAutomatica(){
		try {
			List<EndosoIDTO> cancelables = EndosoIDN.getInstancia().obtieneListaCancelables(new Date(), nombreUsuario);
			for (EndosoIDTO cancelable: cancelables){
				EndosoId id = new EndosoId(cancelable.getIdPoliza(), cancelable.getNumeroEndoso());
				EndosoDTO endosoDTO = EndosoDN.getInstancia(nombreUsuario).getPorId(id);
				if (endosoDTO != null){
					try{
						emiteEndosoCancelacionAutomatica(cancelable);
					}catch(Exception e){
						LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Error al emitir el endoso numero:"+ cancelable.getNumeroEndoso()+" de la poliza:"+cancelable.getIdPoliza(), Level.INFO, e);
					}
					
				}
			}
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Error al obtener la lista de endosos cancelables:", Level.INFO, e);
		} catch (SystemException e) {
			LogDeMidasWeb.log("EndosoDN.emiteEndosoCancelacionAutomatica : Error al obtener la lista de endosos cancelables:", Level.INFO, e);
		}
	}

	public void emiteEndosoRehabilitacion(EndosoRehabilitableDTO endosoaProcesar) throws ExcepcionDeAccesoADatos, SystemException{
		Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(Sistema.USUARIO_SISTEMA);
		
		if (usuario == null){
			throw new SystemException("El endoso " + endosoaProcesar.getId().getIdToPoliza() + "|" + endosoaProcesar.getId().getNumeroEndoso() + " no se puede rehabilitar debido a que no se pudo obtener el usuario: "+ Sistema.USUARIO_SISTEMA);
		}
		EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(endosoaProcesar.getId().getIdToPoliza());
		
		if(ultimoEndoso.getClaveTipoEndoso().shortValue() != Sistema.TIPO_ENDOSO_CANCELACION){
			throw new SystemException("El endoso " + endosoaProcesar.getId().getIdToPoliza() + "|" + endosoaProcesar.getId().getNumeroEndoso() + " no se puede rehabilitar debido a su estatus");
		}

		EndosoDTO endosoRehabilitable = ultimoEndoso;
		if (endosoRehabilitable == null){
			throw new SystemException("El endoso " + endosoaProcesar.getId().getIdToPoliza() + "|" + endosoaProcesar.getId().getNumeroEndoso() + " no se localizo en MIDAS");
		}
		
		CotizacionDTO cotizacionOrigen = CotizacionDN.getInstancia(nombreUsuario).getPorId(endosoRehabilitable.getIdToCotizacion());
		//Obtiene % por forma de Pago
		FormaPagoIDTO formaPago = FormaPagoDN.getInstancia(nombreUsuario).getPorId(cotizacionOrigen.getIdFormaPago().intValue(),cotizacionOrigen.getIdMoneda().shortValue());
		if(formaPago != null){
			cotizacionOrigen.setPorcentajePagoFraccionado(formaPago.getPorcentajeRecargoPagoFraccionado().doubleValue());
		}else{
			cotizacionOrigen.setPorcentajePagoFraccionado(0D);
		}		
		Double ivaCotizacion =CodigoPostalIVADN.getInstancia().getIVAPorIdCotizacion(cotizacionOrigen.getIdToCotizacion(), nombreUsuario);	
		if(ivaCotizacion==null){
		    throw new SystemException("No se pudo encontrar el valor para el IVA");
		}
		
		EndosoSN endosoSN = new EndosoSN();
		EndosoDTO endosoRehabilitado;
		PolizaDTO polizaDTO;
		try{
			LogDeMidasWeb.log("EndosoDN.emiteEndosoRehabilitacion : Ejecutando la emision del endoso de rehabilitacion de la idpoliza: "+endosoRehabilitable.getId().getIdToPoliza(), Level.INFO, null);
			//TODO implementar la firma del metodo correctamente desde la intefaz del temporizador
			//para que envie un objeto de tipo EndosoIDTO
			endosoSN.emiteEndosoRehabilitacion(cotizacionOrigen, nombreUsuario,ivaCotizacion, endosoaProcesar);
			LogDeMidasWeb.log("EndosoDN.emiteEndosoRehabilitacion : Termina la emision del endoso de rehabilitacion de la idpoliza: "+endosoRehabilitable.getId().getIdToPoliza(), Level.INFO, null);
			endosoRehabilitado = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(endosoRehabilitable.getId().getIdToPoliza());
			polizaDTO = endosoRehabilitado.getPolizaDTO();
			polizaDTO.setClaveEstatus(Sistema.ESTATUS_VIGENTE);
			PolizaDN.getInstancia().actualizar(polizaDTO);		
		}catch (Exception e) {
			 throw new SystemException("Error al emitir el endoso de rehabilitacion: "+ e.getMessage());
		}
		//Genera Movimientos de rehabilitacion
		MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).calcularDiferenciasDeEndosoDeRehabilitacion(endosoRehabilitado.getIdToCotizacion());		
		try{
			
			LogDeMidasWeb.log("EndosoDN.emiteEndosoRehabilitacion: Ejecutando ReciboEndosoDN.emiteRecibosEndoso con id de Poliza|NoEndoso = " + 
					endosoRehabilitado.getId().getIdToPoliza().toString()+ "|" +endosoRehabilitado.getId().getNumeroEndoso(), Level.INFO, null);

			EndosoIDN.getInstancia().emiteEndoso(endosoRehabilitado.getId().getIdToPoliza().toString()+ "|" +endosoRehabilitado.getId().getNumeroEndoso(), nombreUsuario);
				LogDeMidasWeb.log("EndosoDN.emiteEndosoRehabilitacion: Termina ReciboDN.emiteRecibosPoliza con id de Poliza|NoEndoso " + 
						endosoRehabilitado.getId().getIdToPoliza().toString()+ "|" +endosoRehabilitado.getId().getNumeroEndoso()+ " ejecutado.", Level.INFO, null);
		}catch (Exception e){
			LogDeMidasWeb.log("EndosoDN.emiteEndosoRehabilitacion : Excepcion en ReciboDN.emiteRecibosPoliza con id de Poliza = " + 
					polizaDTO.getIdToPoliza() + "Endoso Numero= "+endosoRehabilitado.getId().getNumeroEndoso(), Level.WARNING, e);
		}			
		//Se insertan estadisticas
		try{
			CotizacionDTO cotizacionEndoso = CotizacionDN.getInstancia(
					nombreUsuario).getPorId(
							endosoRehabilitado.getIdToCotizacion());
			EstadisticasEstadoDN.getInstancia()
					.insertarEstadisticasAutomaticas(
							cotizacionEndoso.getSolicitudDTO()
									.getIdToSolicitud(),
							cotizacionEndoso.getIdToCotizacion(),
							endosoRehabilitado.getId().getIdToPoliza(), BigDecimal.valueOf(endosoRehabilitado.getId().getNumeroEndoso().doubleValue()));
		}catch (Exception e){
			LogDeMidasWeb.log("EndosoDN.emiteEndosoRehabilitacion : Excepcion en EstadisticasEstadoDN.insertarEstadisticasAutomaticas con id de Poliza = " + 
					polizaDTO.getIdToPoliza() + "Endoso Numero= "+endosoRehabilitado.getId().getNumeroEndoso(), Level.WARNING, e);
		}	
		//Se notifica el endoso
		List<String> errores = new ArrayList<String>();		
		try{
		
			SoporteReaseguroCotizacionDN soporteReaseguroCotizacionDN = new SoporteReaseguroCotizacionDN(endosoRehabilitable.getIdToCotizacion());
			soporteReaseguroCotizacionDN.notificarEndosoRehabilitacion(endosoRehabilitado.getId().getIdToPoliza(), endosoRehabilitado.getId().getNumeroEndoso(), endosoRehabilitado.getIdToCotizacion(), endosoRehabilitado.getFechaCreacion(), endosoRehabilitado.getFechaInicioVigencia(), endosoRehabilitado.getFactorAplicacion(), errores, usuario,null,null);
			
		}catch (Exception e) {
			String descError = "";
			if(errores != null)
				descError = obtenerDescError(errores);
			UtileriasWeb.registraLogInteraccionReaseguro("EndosoDN.emiteEndosoRehabilitacion", 
					Sistema.MODULO_REASEGURO, "SoporteReaseguro", "notificarEmision", 
					nombreUsuario, 
					"idToCotizacion = "+ endosoRehabilitado.getIdToCotizacion() + " idToPoliza = "+ endosoRehabilitado.getId().getIdToPoliza()+ " noEndoso = "+ endosoRehabilitado.getId().getNumeroEndoso() +" fechaCreacionEndoso = "+endosoRehabilitado.getFechaCreacion(), 
					"void()", e, descError, "error al notificar la emision de un endoso de rehabilitacion", "CotizacionDN", Sistema.MODULO_DANIOS);
		}
	}

	/**
	 * Obtiene las diferencias en cotizaciones de endoso
	 * @param idToCotizacion Id de la cotizacion
	 * @param nombreUsuario Nombre del usuario logueado
	 * @return Listado con las diferencias en cotizaciones de endoso
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public List<DiferenciaCotizacionEndosoDTO> obtenerDiferenciasCotizacionEndoso(
			BigDecimal idToCotizacion, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException {
		return new EndosoSN().obtenerDiferenciasCotizacionEndoso(idToCotizacion, nombreUsuario);
	}
	private Map<String, String> emitirEndoso(CotizacionDTO cotizacionDTO,
			Double ivaCotizacion, PolizaDTO polizaDTO, EndosoDTO endosoAnterior){
		Map<String, String> mensaje = new HashMap<String, String>();
		EndosoDTO endosoEmitido = null;
		try{
			endosoEmitido = getUltimoEndoso(polizaDTO.getIdToPoliza());
			short tipoEndoso = obtieneClaveTipoEndoso (cotizacionDTO.getIdToCotizacion(), 
					cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue());	
					
			endosoEmitido.setCotizacionDTO(cotizacionDTO);
			endosoEmitido.setClaveTipoEndoso(tipoEndoso);		
			calcularRiesgos(endosoEmitido.getCotizacionDTO(), endosoEmitido, endosoEmitido.getPolizaDTO(),ivaCotizacion, endosoAnterior);
			acumulaCoberturas(endosoEmitido);
			acumulaEndoso(endosoEmitido);
			actualizar(endosoEmitido);
			mensaje.put("icono", "30");//Exito
			mensaje.put("mensaje", "El endoso se emitio correctamente.</br> El n\u00famero de  P\u00f3liza es: ");	
			mensaje.put("idpoliza", endosoEmitido.getId().getIdToPoliza().toString());
			mensaje.put("noendoso", endosoEmitido.getId().getNumeroEndoso().toString());			
			return mensaje;
		} catch (Exception e) {	
			try{
				EstructuraEndosoDN.getInstancia().deshaceEstructuraEndoso(polizaDTO.getIdToPoliza(), endosoEmitido.getId().getNumeroEndoso(), nombreUsuario);
			}catch (Exception ex) {
				LogDeMidasWeb.log("EndosoDN.emitirEndoso : Excepcion en EstructuraEndosoDN.deshaceEstructuraEndoso con poliza|numeroendoso = " + 
						endosoEmitido.getId().getIdToPoliza().toString()+ "|" +endosoEmitido.getId().getNumeroEndoso(), Level.INFO, null);						
			}
			String recibosNoGenerados =mensaje.put("mensaje", "</br>Error al generar el Endoso.");
			mensaje.put("mensaje", recibosNoGenerados);		
			LogDeMidasWeb.log("emitirEndoso failed", Level.SEVERE, e);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar el Endoso.");
			return mensaje;
		}
	}
	private Short obtieneClaveTipoEndoso(BigDecimal idCotizacion, Short claveTipoEndosoSolicitud) throws ExcepcionDeAccesoADatos, SystemException {
		Short tipoEndoso = null;
		
		if (claveTipoEndosoSolicitud.intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION) {
			tipoEndoso = Sistema.TIPO_ENDOSO_CANCELACION;
		} else if (claveTipoEndosoSolicitud.intValue() == Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION) {
			tipoEndoso = Sistema.TIPO_ENDOSO_REHABILITACION;
		} else if (claveTipoEndosoSolicitud.intValue() == Sistema.SOLICITUD_ENDOSO_DE_DECLARACION) {
			tipoEndoso = Sistema.TIPO_ENDOSO_AUMENTO;
		} else if (claveTipoEndosoSolicitud.intValue() == Sistema.SOLICITUD_ENDOSO_DE_MODIFICACION) {
			
			BigDecimal diferenciaPrimaNeta = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario).obtenerDiferenciaPrimaNeta(idCotizacion);
			if (diferenciaPrimaNeta == null || diferenciaPrimaNeta.intValue() == 0){
				tipoEndoso = Sistema.TIPO_ENDOSO_CAMBIO_DATOS;
			}else if (diferenciaPrimaNeta.intValue() > 0) {
				tipoEndoso = Sistema.TIPO_ENDOSO_AUMENTO;
			} else if (diferenciaPrimaNeta.intValue() < 0) {
				tipoEndoso = Sistema.TIPO_ENDOSO_DISMINUCION;
			} 
		} else {
			tipoEndoso = Sistema.TIPO_ENDOSO_CAMBIO_DATOS;
		}
		
		return tipoEndoso;
		
	}
	protected void calcularRiesgos(CotizacionDTO cotizacionDTO, EndosoDTO ultimoEndoso, PolizaDTO polizaBase,Double ivaCotizacion, EndosoDTO endosoAnterior) throws ExcepcionDeAccesoADatos, SystemException{
		int INCENDIO = 1;
		valorPrimaNetaEndoso= 0D;
		valorRecargoPagoFraccionadoEndoso= 0D;
		valorDerechosEndoso = 0D;
		valorBonificacionComisionEndoso = 0D;
		valorIVAEndoso = 0D;
		valorPrimaTotalEndoso= 0D;
		valorBonificacionComisionRPFEndoso = 0D;
		valorComisionEndoso = 0D;
		valorComisionRPFEndoso = 0D;		
		//Dias por devengar
 
		double diasPorDevengar = UtileriasWeb.obtenerDiasEntreFechas(cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia());
		
		double diasPorDevengarBase = UtileriasWeb.obtenerDiasEntreFechas(polizaBase.getCotizacionDTO().getFechaInicioVigencia(), polizaBase.getCotizacionDTO().getFechaFinVigencia());
		
		if(cotizacionDTO.getTipoPolizaDTO().getProductoDTO() != null && 
				cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getClaveAjusteVigencia().intValue() == 0){
			diasPorDevengar = 365d;
		}							
		
		Double primaNetaCotizacion = CotizacionDN.getInstancia(nombreUsuario).getPrimaNetaCotizacion(cotizacionDTO.getIdToCotizacion());
		Double primaNetaCotizacionRespaldo = primaNetaCotizacion;
		Double primaNetaCotizacionBase = CotizacionDN.getInstancia(nombreUsuario).getPrimaNetaCotizacion(endosoAnterior.getIdToCotizacion());
		primaNetaCotizacion = (primaNetaCotizacion - primaNetaCotizacionBase) * (diasPorDevengar / 365D );
		
		if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_CANCELACION
				&& diasPorDevengar == diasPorDevengarBase){
			primaNetaCotizacion = primaNetaCotizacionBase * -1D;
		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()==Sistema.SOLICITUD_ENDOSO_DE_REHABILITACION){
			primaNetaCotizacion = primaNetaCotizacionBase * (diasPorDevengar / 365D );
		}else if(cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso().shortValue()== Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO){
			primaNetaCotizacion = primaNetaCotizacionRespaldo;	
		}
		
		//Se obtienen los gastos de expedicion
		Double derechos = 0D;
		//Se obtienen los gastos de expedicion		
		if(cotizacionDTO.getValorDerechosUsuario()==null){
			derechos = CotizacionDN.getInstancia(null).calcularDerechosCotizacion(cotizacionDTO,false);
			CotizacionDN.getInstancia(null).modificarDerechos(cotizacionDTO.getIdToCotizacion(), Sistema.CLAVE_DERECHOS_SISTEMA, derechos.doubleValue(), false, false);
		}else{
			derechos = cotizacionDTO.getValorDerechosUsuario();
		}			

		AgrupacionCotDTO agrupacion = PrimerRiesgoLUCDN.getInstancia().buscarPorCotizacion(cotizacionDTO.getIdToCotizacion(),Sistema.TIPO_PRIMER_RIESGO);
		
		if(ivaCotizacion==null){
		    throw new RuntimeException("No se pudo encontrar el valor para el IVA");
		}
		//List<ParametroGeneralDTO> parametroIva = parametroGeneral.findByProperty("id.codigoParametroGeneral", BigDecimal.valueOf(20020D));
		List<ParametroGeneralDTO> parametrosLimiteSA = ParametroGeneralDN.getINSTANCIA().getPorPropiedad("id.codigoParametroGeneral", BigDecimal.valueOf(30010D));
		Double limiteSA = Double.valueOf(parametrosLimiteSA.get(0).getValor());
		Double tipoCambioVal = cotizacionDTO.getTipoCambio();		
		

		Double primaNetaRiesgoBase = 0D;

		RiesgoEndosoId id0 = new RiesgoEndosoId();
		id0.setIdToPoliza(polizaBase.getIdToPoliza());
		id0.setNumeroEndoso(ultimoEndoso.getId().getNumeroEndoso());

		List<RiesgoEndosoDTO> riesgoEndosoDTOs = RiesgoEndosoDN.getInstancia().listarFiltrado(id0);
		boolean riesgoEliminado = false;				
		for(RiesgoEndosoDTO riesgoEndosoDTO: riesgoEndosoDTOs){
			
			primaNetaRiesgoBase = 0D;
			
			RiesgoCotizacionDTO riesgoBase = new RiesgoCotizacionDTO();
			RiesgoCotizacionId id1 = new RiesgoCotizacionId();
			id1.setIdToCotizacion(endosoAnterior.getIdToCotizacion());
			id1.setNumeroInciso(riesgoEndosoDTO.getId().getNumeroInciso());
			id1.setIdToSeccion(riesgoEndosoDTO.getId().getIdToSeccion());
			id1.setIdToCobertura(riesgoEndosoDTO.getId().getIdToCobertura());
			id1.setIdToRiesgo(riesgoEndosoDTO.getId().getIdToRiesgo());
			
			riesgoBase.setId(id1);
			
			riesgoBase = RiesgoCotizacionDN.getInstancia().getPorId(id1);
			id1.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			
			RiesgoCotizacionDTO riesgo = new RiesgoCotizacionDTO();
			riesgo = RiesgoCotizacionDN.getInstancia().getPorId(id1);
			if (riesgo == null){
				riesgo = riesgoBase;
				riesgoEliminado = true;
			}else{
				riesgoEliminado = false;
			}
							
			if (riesgoBase != null)
				primaNetaRiesgoBase = riesgoBase.getValorPrimaNeta();
			
			ComisionCotizacionId id = new ComisionCotizacionId();
			id.setIdTcSubramo(riesgo.getIdTcSubramo());
			id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			
			ComisionCotizacionDTO comision = null;
			Double sumaAseguradaIncendio = 0D;
			if(riesgo.getIdTcSubramo().intValue() == INCENDIO){
				limiteSA = limiteSA/tipoCambioVal;
				// Se obtiene la SA de Incendio por Inciso
				sumaAseguradaIncendio = CoberturaCotizacionDN.getInstancia()
						.obtenerSACoberturasBasicasIncendioPorCotizacion(
								cotizacionDTO.getIdToCotizacion());					
				if(agrupacion != null){	
					if (riesgo.getCoberturaCotizacionDTO().getNumeroAgrupacion() == agrupacion.getId().getNumeroAgrupacion()) {
						id.setTipoPorcentajeComision(Sistema.TIPO_PRR);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					}
					
				}else{
					if(sumaAseguradaIncendio < limiteSA){
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					}else if(sumaAseguradaIncendio >= limiteSA){
						id.setTipoPorcentajeComision(Sistema.TIPO_RCI);
					}else{
						id.setTipoPorcentajeComision(Sistema.TIPO_RO);
					}
				}
			}else{
				id.setTipoPorcentajeComision(Sistema.TIPO_RO);
			}
			comision = ComisionCotizacionDN.getInstancia().getPorId(id);
			if(ultimoEndoso.getClaveTipoEndoso() == Sistema.TIPO_ENDOSO_CANCELACION) {
				riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D ) * -1D));
			}else if(ultimoEndoso.getClaveTipoEndoso() == Sistema.TIPO_ENDOSO_REHABILITACION){
				riesgoEndosoDTO.setValorPrimaNeta(riesgo.getValorPrimaNeta()* (diasPorDevengar / 365D ));
			}else{
				if(riesgoEliminado){
					riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D ) * -1D));
				}else{
					boolean riesgoEndosoAnteriorContratado = false;
					if(riesgoBase != null){
						riesgoEndosoAnteriorContratado = riesgoBase
							.getCoberturaCotizacionDTO().getSeccionCotizacionDTO()
							.getClaveContrato().shortValue() == Sistema.CONTRATADO
							&& riesgoBase.getCoberturaCotizacionDTO()
									.getClaveContrato().shortValue() == Sistema.CONTRATADO
							&& riesgoBase.getClaveContrato().shortValue() == Sistema.CONTRATADO;						
					}
		
					boolean riesgoEndosoActualContratado = riesgo
							.getCoberturaCotizacionDTO().getSeccionCotizacionDTO()
							.getClaveContrato().shortValue() == Sistema.CONTRATADO
							&& riesgo.getCoberturaCotizacionDTO().getClaveContrato()
									.shortValue() == Sistema.CONTRATADO
							&& riesgo.getClaveContrato().shortValue() == Sistema.CONTRATADO;			

					if(!riesgoEndosoActualContratado && riesgoEndosoAnteriorContratado){// Baja de riesgo 
						riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D ) * -1D));
					}else if (riesgoEndosoActualContratado && !riesgoEndosoAnteriorContratado ){ //Alta de riesgo
						riesgoEndosoDTO.setValorPrimaNeta(((riesgo.getValorPrimaNeta()) * (diasPorDevengar / 365D )));
					}else{
						riesgoEndosoDTO.setValorPrimaNeta((riesgo.getValorPrimaNeta() - primaNetaRiesgoBase) * (diasPorDevengar / 365D ));
					}
				
				}
			}
			
			riesgoEndosoDTO.setPorcentajeComision(comision.getPorcentajeComisionCotizacion().doubleValue());
			//valorComision = primaNeta del riesgo * porcentajeComision/100
			riesgoEndosoDTO.setValorComision(riesgoEndosoDTO.getValorPrimaNeta()*riesgoEndosoDTO.getPorcentajeComision()/100);
			//valorRecargoPagoFrac = valorPrimaNeta * PorcentajeRPF/100
			riesgoEndosoDTO.setValorRecargoPagoFrac(riesgoEndosoDTO.getValorPrimaNeta() * cotizacionDTO.getPorcentajePagoFraccionado() / 100);		
			//valorComisionRecPagoFrac = valorRecargoPagoFrac * porcentajeComision/100
			riesgoEndosoDTO.setValorComisionRecPagoFrac(riesgoEndosoDTO.getValorRecargoPagoFrac() * riesgoEndosoDTO.getPorcentajeComision()/100);
			//valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro / 100
			riesgoEndosoDTO.setValorCoaseguro(riesgoEndosoDTO.getValorPrimaNeta() * riesgo.getPorcentajeCoaseguro() / 100);
			//valorDeducible = valorPrimaNeta * porcentajeDeducible / 100
			riesgoEndosoDTO.setValorDeducible(riesgoEndosoDTO.getValorPrimaNeta() * riesgo.getPorcentajeDeducible() / 100);
			//valorCuota = valorPrimaNeta / valorSumaAsegurada
			if(riesgo.getValorSumaAsegurada() == 0){
				riesgoEndosoDTO.setValorCuota(0D);
			}else{
				riesgoEndosoDTO.setValorCuota(riesgoEndosoDTO.getValorPrimaNeta()/riesgo.getValorSumaAsegurada());
			}
			//derechos = derechos poliza * valorPrimaNetaCobertura / primaNetaCotizacion 
			if(derechos == 0D){
				riesgoEndosoDTO.setValorDerechos(derechos);
			}else{
				riesgoEndosoDTO.setValorDerechos(derechos * (riesgoEndosoDTO.getValorPrimaNeta()/ primaNetaCotizacion));
	
			}
			//valorBonifComision =  getValorComision * porcentajeBonifComision/100
			riesgoEndosoDTO.setValorBonifComision(riesgoEndosoDTO.getValorComision() * cotizacionDTO.getPorcentajebonifcomision()/100);
			//valorBonifComRecPagoFrac = ValorComisionRecPagoFrac * porcentajeBonifComision/100
			riesgoEndosoDTO.setValorBonifComRecPagoFrac(riesgoEndosoDTO.getValorComisionRecPagoFrac() * cotizacionDTO.getPorcentajebonifcomision()/100);
			//valorComisionFinal = valorComision - valorBonifComision
			riesgoEndosoDTO.setValorComisionFinal(riesgoEndosoDTO.getValorComision() - riesgoEndosoDTO.getValorBonifComision());
			//valorComFinalRecPagoFrac = valorComisionRecPagoFrac - valorBonifComRecPagoFac
			riesgoEndosoDTO.setValorComFinalRecPago(riesgoEndosoDTO.getValorComisionRecPagoFrac() - riesgoEndosoDTO.getValorBonifComRecPagoFrac());
			//valorIVA = (primaNeta del riesgo + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac) * porcentaje de IVA
			Double iva = (riesgoEndosoDTO.getValorPrimaNeta() + riesgoEndosoDTO.getValorRecargoPagoFrac() +riesgoEndosoDTO.getValorDerechos() 
						- riesgoEndosoDTO.getValorBonifComision() -riesgoEndosoDTO.getValorBonifComRecPagoFrac())* ivaCotizacion/100D; 
			riesgoEndosoDTO.setValorIVA(iva);
			//valorPrimaTotal = primaNeta del riesgo + valorRecargoPagoFrac + valorDerechos - valorBonfComision - valorBonifComRecPagoFrac + IVA
			Double primaTotal = riesgoEndosoDTO.getValorPrimaNeta() + riesgoEndosoDTO.getValorRecargoPagoFrac() +riesgoEndosoDTO.getValorDerechos()
								- riesgoEndosoDTO.getValorBonifComision() -riesgoEndosoDTO.getValorBonifComRecPagoFrac();
			riesgoEndosoDTO.setValorPrimaTotal(primaTotal + riesgoEndosoDTO.getValorIVA());			

			//Acumula a Nivel Endoso
			valorPrimaNetaEndoso += riesgoEndosoDTO.getValorPrimaNeta();
			valorRecargoPagoFraccionadoEndoso += riesgoEndosoDTO.getValorRecargoPagoFrac();
			valorDerechosEndoso += riesgoEndosoDTO.getValorDerechos();
			valorBonificacionComisionEndoso += riesgoEndosoDTO.getValorBonifComision();
			valorIVAEndoso += riesgoEndosoDTO.getValorIVA();
			valorPrimaTotalEndoso += riesgoEndosoDTO.getValorPrimaTotal();
			valorBonificacionComisionRPFEndoso += riesgoEndosoDTO.getValorBonifComRecPagoFrac();
			valorComisionEndoso += riesgoEndosoDTO.getValorComision();
			valorComisionRPFEndoso += riesgoEndosoDTO.getValorComisionRecPagoFrac();
			
			RiesgoEndosoDN.getInstancia().modificar(riesgoEndosoDTO);
		}
		
	}
	
	protected void acumulaCoberturas(EndosoDTO ultimoEndoso) throws ExcepcionDeAccesoADatos, SystemException{
		CoberturaEndosoId id = new CoberturaEndosoId();
		id.setIdToPoliza(ultimoEndoso.getId().getIdToPoliza());
		id.setNumeroEndoso(ultimoEndoso.getId().getNumeroEndoso());
		
		List<CoberturaEndosoDTO> coberturas = CoberturaEndosoDN.getInstancia().listarFiltrado(id);
		
		double valorRPF;
		double valorDerechos;
		double valorBonificacionComis;
		double valorBonificacionComisRFP;
		double valorIVA;
		double valorPrimaNetaTotal;
		double valorPrimaNeta;
		double porcentajeComision;
		double valorComision;
		double valorComisionFinal;
		double valorComisionRPF;
		double valorComisionFinalRPF;	
		
		for(CoberturaEndosoDTO cobertura: coberturas){
			valorRPF = 0D;
			valorDerechos = 0D;
			valorBonificacionComis = 0D;	
			valorBonificacionComisRFP = 0D;
			valorIVA = 0D;
			valorPrimaNetaTotal = 0d;
			valorPrimaNeta = 0d;
			porcentajeComision = 0D;
			valorComision = 0D;
			valorComisionFinal = 0D;
			valorComisionRPF = 0D;
			valorComisionFinalRPF = 0D;
			
			CoberturaCotizacionId id2 = new CoberturaCotizacionId();
			id2.setIdToCotizacion(ultimoEndoso.getIdToCotizacion());
			id2.setIdToCobertura(cobertura.getId().getIdToCobertura());
			id2.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id2.setNumeroInciso(cobertura.getId().getNumeroInciso());
			
			CoberturaCotizacionDTO coberturaCotizacionDTO = new CoberturaCotizacionDTO();
			coberturaCotizacionDTO.setId(id2);
			
			coberturaCotizacionDTO = CoberturaCotizacionDN.getInstancia().getPorId(coberturaCotizacionDTO);
			
			RiesgoEndosoId id3  = new RiesgoEndosoId();
			id3.setIdToPoliza(cobertura.getId().getIdToPoliza());
			id3.setNumeroEndoso(cobertura.getId().getNumeroEndoso());
			id3.setNumeroInciso(cobertura.getId().getNumeroInciso());
			id3.setIdToSeccion(cobertura.getId().getIdToSeccion());
			id3.setIdToCobertura(cobertura.getId().getIdToCobertura());
			
			List<RiesgoEndosoDTO> riesgos = RiesgoEndosoDN.getInstancia().listarFiltrado(id3);
			
			for(RiesgoEndosoDTO riesgo : riesgos){
				valorRPF += riesgo.getValorRecargoPagoFrac();
				valorDerechos += riesgo.getValorDerechos();
				valorBonificacionComis += riesgo.getValorBonifComision();
				valorBonificacionComisRFP += riesgo.getValorBonifComRecPagoFrac();
				valorIVA += riesgo.getValorIVA();
				valorPrimaNetaTotal += riesgo.getValorPrimaTotal();
				porcentajeComision = riesgo.getPorcentajeComision();
				valorComision += riesgo.getValorComision();
				valorComisionFinal += riesgo.getValorComisionFinal();
				valorComisionRPF += riesgo.getValorComisionRecPagoFrac();
				valorComisionFinalRPF += riesgo.getValorComFinalRecPago();
				valorPrimaNeta += riesgo.getValorPrimaNeta();
			}
			// valorCoaseguro = valorPrimaNeta * porcentajeCoaseguro /100	
			if(coberturaCotizacionDTO != null){
				cobertura.setValorCoaseguro(cobertura.getValorPrimaNeta() * coberturaCotizacionDTO.getPorcentajeCoaseguro() / 100D);
			// valorDeducible = valorPrimaNeta * porcentajeDeducible /100
				cobertura.setValorDeducible(cobertura.getValorPrimaNeta() * coberturaCotizacionDTO.getPorcentajeDeducible() / 100D);
			}
			cobertura.setValorRecargoPagoFrac(valorRPF);
			cobertura.setValorBonifComision(valorBonificacionComis);
			cobertura.setValorBonifComRecPagoFrac(valorBonificacionComisRFP);
			cobertura.setValorIVA(valorIVA);
			cobertura.setValorDerechos(valorDerechos);
			cobertura.setValorPrimaTotal(valorPrimaNetaTotal);
			cobertura.setPorcentajeComision(porcentajeComision);
			cobertura.setValorComision(valorComision);
			cobertura.setValorComisionFinal(valorComisionFinal);
			cobertura.setValorComisionRecPagoFrac(valorComisionRPF);
			cobertura.setValorComisionFinalRecPagoFrac(valorComisionFinalRPF);
			cobertura.setValorPrimaNeta(valorPrimaNeta);
			new CoberturaEndosoSN().modificar(cobertura);
		}
		
	}
	protected void acumulaEndoso(EndosoDTO ultimoEndoso){
		ultimoEndoso.setValorPrimaNeta(valorPrimaNetaEndoso);
		ultimoEndoso.setValorRecargoPagoFrac(valorRecargoPagoFraccionadoEndoso);
		ultimoEndoso.setValorDerechos(valorDerechosEndoso);
		ultimoEndoso.setValorBonifComision(valorBonificacionComisionEndoso);
		ultimoEndoso.setValorIVA(valorIVAEndoso);
		ultimoEndoso.setValorPrimaTotal(valorPrimaTotalEndoso);
		ultimoEndoso.setValorBonifComisionRPF(valorBonificacionComisionRPFEndoso);
		ultimoEndoso.setValorComision(valorComisionEndoso);
		ultimoEndoso.setValorComisionRPF(valorComisionRPFEndoso);
	}

	public List<EndosoDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor, boolean ordenAscendente) throws SystemException {
		return new EndosoSN().buscarPorPropiedad(nombrePropiedad, valor, ordenAscendente);
	}
	
	public EndosoDTO buscarPorCotizacion(BigDecimal idToCotizacion) throws SystemException {
		return new EndosoSN().buscarPorCotizacion(idToCotizacion);
	}
	
	public EndosoDTO obtenerEndoso(BigDecimal idToPoliza,Short numeroEndoso) throws SystemException {
		EndosoSN endosoSN = new EndosoSN();
		return endosoSN.obtenerEndoso(idToPoliza, numeroEndoso);
	}
	
	//Verifica si el primer recibo del endoso fue pagado
	public boolean primerReciboPagado(EndosoDTO endoso)throws SystemException{
		List<ReciboDTO> recibos = ReciboDN.getInstancia(null).consultaRecibos(endoso.getId().getIdToPoliza());
		
		for(ReciboDTO recibo : recibos){
			if(recibo.getNumeroEndoso().equals(endoso.getId().getNumeroEndoso().toString()) &&
			   recibo.getSituacion().trim().equals(Sistema.RECIBO_PAGADO))
				return true;
		}
		return false;
	}
	
	public SoporteEndosoCambioCuota getDatosCambioCuotaEndoso(
			BigDecimal idToCotizacion, BigDecimal idToSeccion,
			BigDecimal idToCobertura, Usuario usuario) throws SystemException {
		SoporteEndosoCambioCuota soporteEndosoCambioCuota = new SoporteEndosoCambioCuota();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(usuario.getNombreUsuario()).getPorId(idToCotizacion);
		CoberturaCotizacionId id = new CoberturaCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setIdToSeccion(idToSeccion);
		id.setIdToCobertura(idToCobertura);
		
		List<CoberturaCotizacionDTO> coberturasInvolucradas = CoberturaCotizacionDN.getInstancia().listarPorIdFiltrado(id);
		
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(cotizacionDTO.getSolicitudDTO().getIdToPolizaEndosada());
		EndosoDTO endosoDTO = EndosoDN.getInstancia(usuario.getNombreUsuario()).getUltimoEndoso(polizaDTO.getIdToPoliza());

		List<CoberturaCotizacionDTO> coberturasInvolucradasFinal = new ArrayList<CoberturaCotizacionDTO>();
		
		for(CoberturaCotizacionDTO coberturaInvolucrada: coberturasInvolucradas){
			CoberturaCotizacionDTO coberturaTmp = new CoberturaCotizacionDTO();
			CoberturaCotizacionId idTmp = new CoberturaCotizacionId();
			idTmp.setIdToCotizacion(endosoDTO.getIdToCotizacion());
			idTmp.setIdToCobertura(coberturaInvolucrada.getId().getIdToCobertura());
			idTmp.setIdToSeccion(coberturaInvolucrada.getId().getIdToSeccion());
			idTmp.setNumeroInciso(coberturaInvolucrada.getId().getNumeroInciso());
			coberturaTmp.setId(idTmp);
			coberturaTmp = CoberturaCotizacionDN.getInstancia().getPorId(coberturaTmp);
			if(coberturaTmp != null){
				coberturaInvolucrada.setValorCuotaOriginal(coberturaTmp.getValorCuota());
				if(coberturaInvolucrada.getValorCuota().doubleValue() != coberturaTmp.getValorCuota().doubleValue()){
					coberturasInvolucradasFinal.add(coberturaInvolucrada);
				}				
			}
			
		}
		if(!coberturasInvolucradasFinal.isEmpty()){
			CoberturaCotizacionDTO coberturaTmp = coberturasInvolucradasFinal.get(0);
			
			CoberturaDTO coberturaDTO = CoberturaDN.getInstancia().getPorId(coberturaTmp.getId().getIdToCobertura());
			SeccionDTO seccionDTO = SeccionDN.getInstancia().getPorId(coberturaTmp.getId().getIdToSeccion());
			
			soporteEndosoCambioCuota.setDescripcionCobertura(coberturaDTO.getNombreComercial());
			soporteEndosoCambioCuota.setDescripcionSeccion(seccionDTO.getNombreComercial());
			soporteEndosoCambioCuota.setIdToCotizacion(idToCotizacion);
			soporteEndosoCambioCuota.setIdToCotizacionAnterior(endosoDTO.getIdToCotizacion());
			soporteEndosoCambioCuota.setCoberturasInvolucradas(coberturasInvolucradasFinal);
			
		}
		
		return soporteEndosoCambioCuota;
	}
	
	public Short buscarNumeroEndosoUltimoCFP(BigDecimal idPoliza) throws SystemException {
		return new EndosoSN().buscarNumeroEndosoUltimoCFP(idPoliza);
	}
	
	
}
