package mx.com.afirme.midas.endoso;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.rehabilitacion.EndosoRehabilitableDTO;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EndosoSN {
	private EndosoFacadeRemote beanRemoto;

	public EndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(EndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(EndosoDTO endosoDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(endosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoDTO getPorId(EndosoId id) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoDTO actualizar(EndosoDTO endosoDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(endosoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<EndosoDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoDTO getUltimoEndoso(BigDecimal idToPoliza) {
		try {
			return beanRemoto.getUltimoEndoso(idToPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoDTO getPenultimoEndoso(BigDecimal idToPoliza) {
		try {
			return beanRemoto.getPenultimoEndoso(idToPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public Map<String, String> emitirEndoso(CotizacionDTO cotizacionDTO,
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, 
			EndosoIDTO endosoIDTO) {
		try {
			return beanRemoto.emitirEndoso(cotizacionDTO, ivaCotizacion,
					existeReciboPagado, recibos,  endosoIDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public BigDecimal obtenerCantidadEndososPoliza(BigDecimal idToPoliza) {
		try {
			return beanRemoto.obtenerCantidadEndososPoliza(idToPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<EndosoDTO> buscarPorPropiedad(String nombrePropiedad,
			Object valor, boolean ordenAscendente) {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor,
					ordenAscendente);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void emiteEndosoCancelacionAutomatica(
			CotizacionDTO cotizacionOrigen, String usuarioCreacion,
			Double ivaCotizacion, boolean existeReciboPagado, List<ReciboDTO> recibos, 
			EndosoIDTO endosoIDTO) {
		try {
			beanRemoto.emiteEndosoCancelacionAutomatica(cotizacionOrigen,
					usuarioCreacion, ivaCotizacion, existeReciboPagado, recibos, 
					endosoIDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void emiteEndosoRehabilitacion(CotizacionDTO cotizacionOrigen,
			String usuarioCreacion, Double ivaCotizacion,
			EndosoRehabilitableDTO endosoaProcesar) {
		try {
			beanRemoto.emiteEndosoRehabilitacion(cotizacionOrigen,
					usuarioCreacion, ivaCotizacion, endosoaProcesar);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DiferenciaCotizacionEndosoDTO> obtenerDiferenciasCotizacionEndoso(
			BigDecimal idToCotizacion, String nombreUsuario)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerDiferenciasCotizacionEndoso(
					idToCotizacion, nombreUsuario);
		} catch (SQLException e) {

			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Generales.spDAN_DiferenciaCotizacion");
			sb.append("|");
			sb.append("pIdToCotizacion" + "=" + idToCotizacion + ",");

			UtileriasWeb.enviaCorreoExcepcion(
					"Obtener diferencias de cotizacion endoso con id de cotizacion: "
							+ idToCotizacion + " en "
							+ Sistema.AMBIENTE_SISTEMA, sb.toString());
			return null;
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public EndosoDTO buscarPorCotizacion(BigDecimal idToCotizacion) {
		try {
			return beanRemoto.buscarPorCotizacion(idToCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public EndosoDTO obtenerEndoso(BigDecimal idToPoliza,Short numeroEndoso) {
		try {
			return beanRemoto.obtenerEndoso(idToPoliza, numeroEndoso);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public Short buscarNumeroEndosoUltimoCFP(BigDecimal idPoliza) {
		return beanRemoto.buscarNumeroEndosoUltimoCFP(idPoliza);
	}
}
