package mx.com.afirme.midas.endoso.estructura;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class EstructuraEndosoDN {

	private static final EstructuraEndosoDN INSTANCIA = new EstructuraEndosoDN();
	
	public static EstructuraEndosoDN getInstancia() {
		return EstructuraEndosoDN.INSTANCIA;
	}
	
	/**
	 * Deshace la generacion de la estructura de un endoso a partir de una poliza o endoso anterior
	 * @param idToPoliza Id de la poliza
	 * @param numeroEndoso Numero del endoso de la poliza
	 * @param nombreUsuario Nombre del usuario logueado
	 * @return true si se deshizo correctamente la estructura del endoso
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public boolean deshaceEstructuraEndoso(BigDecimal idToPoliza, int numeroEndoso, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException {
		return new EstructuraEndosoSN().deshaceEstructuraEndoso(idToPoliza, numeroEndoso, nombreUsuario);
	}
	
	/**
	 * Genera la estructura de un endoso a partir de una poliza o endoso anterior
	 * @param idToCotizacion Id de la cotizacion
	 * @param idToPoliza Id de la poliza
	 * @param numeroEndoso Numero del nuevo endoso de la poliza
	 * @param tipoEndoso Tipo de endoso
	 * @param idToCotizacionAnterior Id de la cotizacion anterior
	 * @param nombreUsuario Nombre del usuario logueado
	 * @return true si la estructura se genero correctamente
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public boolean generaEstructuraEndoso(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, int numeroEndoso, Short tipoEndoso,
			BigDecimal idToCotizacionAnterior, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException {
		return new EstructuraEndosoSN().generaEstructuraEndoso(idToCotizacion, idToPoliza, numeroEndoso, tipoEndoso, idToCotizacionAnterior, nombreUsuario);
	}
	
}
