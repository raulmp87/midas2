package mx.com.afirme.midas.endoso.estructura;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstructuraEndosoSN {

	private EstructuraEndosoFacadeRemote beanRemoto;

	public EstructuraEndosoSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(EstructuraEndosoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public boolean deshaceEstructuraEndoso(BigDecimal idToPoliza, int numeroEndoso, String nombreUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.deshaceEstructuraEndoso(idToPoliza, numeroEndoso, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Generales.spDAN_BorraEstructuraEndoso");
			sb.append("|");
			sb.append("pIdToPoliza" + "=" + idToPoliza + ",");
			sb.append("pNumeroEndoso" + "=" + numeroEndoso + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Deshacer la generacion de la estructura de un endoso con pIdToPoliza: " + idToPoliza + 
					", pNumeroEndoso: " + numeroEndoso +
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public boolean generaEstructuraEndoso(BigDecimal idToCotizacion,
			BigDecimal idToPoliza, int numeroEndoso, Short tipoEndoso,
			BigDecimal idToCotizacionAnterior, String nombreUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.generaEstructuraEndoso(idToCotizacion, idToPoliza, numeroEndoso, tipoEndoso, idToCotizacionAnterior, nombreUsuario);
		} catch (SQLException e) {
			
			StringBuffer sb = new StringBuffer();
			sb.append("pkgDAN_Generales.spDAN_GeneraEstructuraEndoso");
			sb.append("|");
			sb.append("pIdToCotizacion" + "=" + idToCotizacion + ",");
			sb.append("pIdToPoliza" + "=" + idToPoliza + ",");
			sb.append("pNumeroEndoso" + "=" + numeroEndoso + ",");
			sb.append("pClaveTipoEndoso" + "=" + tipoEndoso + ",");
			sb.append("pIdToCotizacionAnterior" + "=" + idToCotizacionAnterior + ",");
			
			UtileriasWeb.enviaCorreoExcepcion("Generar de la estructura de un endoso con pIdToCotizacion: " + idToCotizacion + 
					", pIdToPoliza: " + idToPoliza +
					" en " + Sistema.AMBIENTE_SISTEMA, sb.toString());
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
}
