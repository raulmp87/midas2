package mx.com.afirme.midas.poliza;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PolizaSN {
	private PolizaFacadeRemote beanRemoto;

	public PolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public PolizaDTO agregar(PolizaDTO polizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(polizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public PolizaDTO actualizar(PolizaDTO polizaDTO)
	throws ExcepcionDeAccesoADatos {
	try {
		return beanRemoto.update(polizaDTO);
	} catch (EJBTransactionRolledbackException e) {
		throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
	}
}

	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarFiltrado(polizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public Long obtenerTotalFiltrado(PolizaDTO polizaDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalFiltrado(polizaDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos("Error al obtenerTotalFiltrado de las polizas");
		}
		
	}

	public Map<String, String> emitirPoliza(BigDecimal idToCotizacion,
			PolizaDTO polizaDTO,Double ivaCotizacion) {
		try {
			return beanRemoto.emitirPoliza(idToCotizacion, polizaDTO,ivaCotizacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<PolizaDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public PolizaDTO getPorId(BigDecimal idToPoliza){
		try {
			return beanRemoto.findById(idToPoliza);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<PolizaDTO> listarPorEstatus(short cveEstatus) {
		try {
			return beanRemoto.selectPolizasByEstatus(cveEstatus);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<PolizaDTO> listarPorNumPolizaFormato(String numPolizaFormato) {
		try {
			return beanRemoto.selectPolizasByDatosNumPolizaFormato(numPolizaFormato);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public PolizaDTO buscaPrimerRegistro(){
		try {
			return beanRemoto.buscaPrimerRegistro();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<PolizaDTO> buscarPolizasPorPropiedad(String nombreProp ,Object value){
	    try {
		    return beanRemoto.findByProperty(nombreProp, value);
	  } catch (Exception e) {
		    throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
	  }
	}
	
	public List<PolizaDTO> buscarFacultativo() throws ExcepcionDeAccesoADatos {
		try {
			 return beanRemoto.selectPolizasFacultativo();
		} catch (EJBTransactionRolledbackException e) {
			  throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
    }
}
