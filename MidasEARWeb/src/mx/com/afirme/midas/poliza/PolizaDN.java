package mx.com.afirme.midas.poliza;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.emision.EmisionDN;

import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class PolizaDN {
	private static final PolizaDN INSTANCIA = new PolizaDN();

	public static PolizaDN getInstancia() {
		return PolizaDN.INSTANCIA;
	}

	public Map<String, String> emitirPoliza(BigDecimal idToCotizacion,
			String nombreUsuario) throws ExcepcionDeAccesoADatos,
			SystemException {
		Map<String, String> mensaje = new HashMap<String, String>();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia(nombreUsuario).getPorId(idToCotizacion);
		if(cotizacionDTO != null){
			List<CoberturaCotizacionDTO> coberturasContratadas = CoberturaCotizacionDN
					.getInstancia().listarCoberturasContratadas(
							cotizacionDTO.getIdToCotizacion());
			ResumenCotizacionDN.getInstancia().calcularTotalesResumenCotizacion(cotizacionDTO, coberturasContratadas);
			cotizacionDTO.setPorcentajeIva(cotizacionDTO.getFactorIVA());
			CotizacionDN.getInstancia(nombreUsuario).modificar(cotizacionDTO);
			if (cotizacionDTO.getDiasPorDevengar() != null &&
				cotizacionDTO.getDerechosPoliza() != null &&
				cotizacionDTO.getPorcentajePagoFraccionado() != null){
				mensaje = mx.com.afirme.midas.interfaz.poliza.PolizaDN.getInstancia(nombreUsuario).emitePoliza(cotizacionDTO);
				if(mensaje.get("icono").equals("30")){ //Exito de Emision
					PolizaDTO polizaEmitida = new PolizaSN().getPorId(UtileriasWeb.regresaBigDecimal(mensaje.get("idpoliza")));
					if (polizaEmitida.getCotizacionDTO().getSolicitudDTO()
							.getEsRenovacion() != null
							&& polizaEmitida.getCotizacionDTO().getSolicitudDTO()
									.getEsRenovacion().intValue() == 1){
						RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN.getInstancia();
						renovacionPolizaDN.setDetalleNotificacion(UtileriasWeb
								.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"poliza.renovacion.detalle.movimiento.emision",
										polizaEmitida
												.getNumeroPolizaFormateada(),
										nombreUsuario));
						renovacionPolizaDN.setTipoMovimiento(Sistema.TIPO_POLIZA_RENOVADA);
						RenovacionPolizaDTO renovacion = renovacionPolizaDN.buscarRenovacionPolizaPorCotizacion(cotizacionDTO.getIdToCotizacion());
						if(renovacion != null)
							renovacionPolizaDN.agregarDetalleSeguimiento(renovacion.getPolizaDTO());
					}
					String numeroPoliza =mensaje.get("mensaje")+"<b>"+UtileriasWeb.getNumeroPoliza(polizaEmitida)+"</b>";
					mensaje.put("mensaje", numeroPoliza);
					mensaje.put("idtopoliza", polizaEmitida.getIdToPoliza().toString());
					mensaje.put("noendoso", String.valueOf(Sistema.POLIZA_NUEVA));
					mensaje.put("fechacreacion",UtileriasWeb.getFechaString(polizaEmitida.getFechaCreacion()));
					mensaje.put("idtocotizacion", cotizacionDTO.getIdToCotizacion().toString());
					String notificaReaseguro =mensaje.get("mensaje")+"</br>Se notifico correctamente a Reaseguro";
					mensaje.put("mensaje", notificaReaseguro);					
					cotizacionDTO.setClaveEstatus(Sistema.ESTATUS_COT_EMITIDA);
					cotizacionDTO.setPorcentajeIva(cotizacionDTO.getFactorIVA());
					CotizacionDN.getInstancia(nombreUsuario).modificar(cotizacionDTO);
					
					if(generarRecibosEndoso(polizaEmitida, (short)0)){
						String recibosGenerados =mensaje.get("mensaje")+"</br>Se generaron correctamente los recibos de la P\u00f3liza";
						mensaje.put("mensaje", recibosGenerados);							
					}else{
						String recibosNoGenerados =mensaje.get("mensaje")+"</br>NO Se generaron los recibos de la P\u00f3liza";
						mensaje.put("mensaje", recibosNoGenerados);						
					}
				}
			}else{
				mensaje.put("icono", "10");//Error
				mensaje.put("mensaje", "Error al generar la P\u00f3liza: Los datos necesarios para la emisi\u00f3n no fueron obtenidos exitosamente");							
			}
		}else{
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la P\u00f3liza: No se encontro Cotizaci\u00f3n");			
		}
		return mensaje;
	}
	/**
	 * Busca las polizas dado un filtro en el <code>polizaDTO</code>
	 * @param polizaDTO
	 * @return
	 * @throws SystemException
	 */
	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO, Usuario usuario)
			throws SystemException {
		String claveSolicitudesFiltradas = "midas.danios.poliza.busqueda.limitada";	
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveSolicitudesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveSolicitudesFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;					
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)		
			polizaDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());

		//Se agrega filtro de Codigo de Agente Especial en caso que aplique
		AgenteEspecialDN.getInstancia().agregaFiltroCodigoAgente(usuario, polizaDTO);
		
		return new PolizaSN().buscarFiltrado(polizaDTO);
	}
	
	public Long obtenerTotalFiltrado(PolizaDTO polizaDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException {
		String claveSolicitudesFiltradas = "midas.danios.poliza.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveSolicitudesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveSolicitudesFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;					
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)		
			polizaDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());

		//Se agrega filtro de Codigo de Agente Especial en caso que aplique
		AgenteEspecialDN.getInstancia().agregaFiltroCodigoAgente(usuario, polizaDTO);
		
		return new PolizaSN().obtenerTotalFiltrado(polizaDTO);
	}
	/**
	 * Valida si la poliza con un numero de poliza  con formato ingresado,
	 * existe en la BD.
	 * @param numeroDePolizaConFormato
	 * @return
	 * @throws SystemException
	 */
	public boolean existePoliza(String numeroDePolizaConFormato,Usuario usuario)
			throws SystemException {
		boolean existePoliza = false;

		String[] numeroDePolizaArray = numeroDePolizaConFormato.split("-");
		String codigoProducto = numeroDePolizaArray[0].substring(0, 2);
		String codigoTipoPoliza = numeroDePolizaArray[0].substring(2, 4);
		Integer numeroPoliza = new Integer(numeroDePolizaArray[1]);
		Integer numeroRenovacion = new Integer(numeroDePolizaArray[2]);

		PolizaDN polizaDN = PolizaDN.getInstancia();

		PolizaDTO polizaDTO = new PolizaDTO();
		polizaDTO.setCodigoProducto(codigoProducto);
		polizaDTO.setCodigoTipoPoliza(codigoTipoPoliza);
		polizaDTO.setNumeroPoliza(numeroPoliza);
		polizaDTO.setNumeroRenovacion(numeroRenovacion);

		List<PolizaDTO> listaPoliza = polizaDN.buscarFiltrado(polizaDTO,usuario);

		if (listaPoliza != null && listaPoliza.size() == 1) {
			existePoliza = true;
		}

		return existePoliza;
	}

	/**
	 * Genera los recibos de la p�liza recibida, con el n�mero de endoso recibido.
	 * El nombre del archivo queda configurado como 'Recibo'+Id de la poliza+numeroEndoso y la extensi�n PDF.
	 * @param polizaDTO, la p�liza cuyos recibos se desean imprimir.
	 * @param Integer numeroEndoso, el endoso cuyos recibos se desean imprimir.
	 * @return true, si la operaci�n es exitosa, false de lo contrario.
	 */
	public boolean generarRecibosEndoso(PolizaDTO polizaDTO, Short numeroEndoso){
		boolean result = true;
		boolean emisionGeneroExcepcion = false;
		if (numeroEndoso == null)
			numeroEndoso = new Short((short)0);
		EndosoId endosoId = new EndosoId();
		endosoId.setIdToPoliza(polizaDTO.getIdToPoliza());
		endosoId.setNumeroEndoso(numeroEndoso);
		EndosoDTO endosoDTO = null;
		try{
			endosoDTO = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).getPorId(endosoId);
			if (endosoDTO == null)
				throw new Exception();
			//Si no se han generado los recibos, se invoca el procedimiento para generarlos.
			List<ReciboDTO> reciboDTOList = null;
			EndosoIDTO endosoIDTO = null;
			if (UtileriasWeb.esCadenaVacia(endosoDTO.getLlaveFiscal())){
				try{
					
					LogDeMidasWeb.log("PolizaDN.generarRecibosEndoso : Ejecutando ReciboDN.emiteRecibosPoliza con id de Poliza = " + 
							polizaDTO.getIdToPoliza() + "Endoso Numero= "+numeroEndoso, Level.INFO, null);
					if (numeroEndoso > 0){
						endosoIDTO = EndosoIDN.getInstancia().emiteEndoso(endosoDTO.getId().getIdToPoliza().toString()+ "|" +endosoDTO.getId().getNumeroEndoso(), polizaDTO.getCodigoUsuarioCreacion()).get(0);
					}else{
						reciboDTOList = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).emiteRecibosPoliza(polizaDTO.getIdToPoliza());
					}

					LogDeMidasWeb.log("PolizaDN.generarRecibosEndoso : ReciboDN.emiteRecibosPoliza con id de Poliza " + 
							polizaDTO.getIdToPoliza() + "Endoso Numero= "+numeroEndoso+ " ejecutado.", Level.INFO, null);
				
				}catch (Exception e){
					emisionGeneroExcepcion = true;
					LogDeMidasWeb.log("PolizaDN.generarRecibosEndoso : Excepcion en ReciboDN.emiteRecibosPoliza con id de Poliza = " + 
							polizaDTO.getIdToPoliza() + "Endoso Numero= "+numeroEndoso, Level.WARNING, e);
					
					LogDeMidasWeb.log("PolizaDN.generarRecibosEndoso : Ejecutando ReciboDN.consultaRecibos con id de Poliza = " + 
							polizaDTO.getIdToPoliza()  + "Endoso Numero= "+numeroEndoso, Level.INFO, null);
					//FIXME Implementar busqueda de recibos por poliza-numeroEndoso
					reciboDTOList = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(polizaDTO.getIdToPoliza(), numeroEndoso);				

					LogDeMidasWeb.log("PolizaDN.emitirPoliza : ReciboDN.consultaRecibos con id de Poliza = " + 
							polizaDTO.getIdToPoliza()  + "Endoso Numero= "+numeroEndoso, Level.INFO, null);						
					
				}
				if(emisionGeneroExcepcion){
					if (reciboDTOList != null && !reciboDTOList.isEmpty()) {
						EmisionDN.getInstancia().generarRecibos();
						//Se le agrega al endoso la llave fiscal de los recibos
						endosoDTO.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
						endosoDTO = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoDTO);
					}					
				}else{
					if(numeroEndoso > 0){
						if (endosoIDTO != null){
							EmisionDN.getInstancia().generarRecibos();
							if (endosoDTO != null) {
								endosoDTO.setLlaveFiscal(endosoIDTO.getLlaveFiscal());
								EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoDTO);
							}						
						}
					}else{
						if (reciboDTOList != null && !reciboDTOList.isEmpty()) {
							EmisionDN.getInstancia().generarRecibos();
							//Se le agrega al endoso la llave fiscal de los recibos
							endosoDTO.setLlaveFiscal(reciboDTOList.get(0).getLlaveFiscal());
							endosoDTO = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).actualizar(endosoDTO);
						}	
					}					
				}
			}
			
	
			byte[] archivoPDF = EmisionDN.getInstancia().obtieneFactura(endosoDTO.getLlaveFiscal());
			// Cesar Ayma solicita que no se guarden los recibos 20100810
			
			if(archivoPDF != null && archivoPDF.length > 0){
				//Validaci�n del documento
	            try {
	            	PdfReader reader = new PdfReader(archivoPDF);
//					@SuppressWarnings("unused")
					PdfStamper stamper = new PdfStamper(reader, new ByteArrayOutputStream());
					stamper.getAcroFields();
					stamper.getMoreInfo();
					stamper.getSignatureAppearance();
					stamper.getWriter();
					result = true;
				} catch (Exception e) {
					//Si se genera un DocumentException, el archivo est� corrupto, se debe borrar e importar nuevamente del WebService.
					
					result = false;
				}
			}
			else{
				
				result = false;
			}

		} catch(SystemException e){
		} catch(FileNotFoundException e){
		} catch(IOException e){
		} catch(Exception e){
		}
		return result;
	}
	/**
	 * Devuelve todas las polizas existentes.
	 * @return
	 * @throws SystemException
	 */
	public List<PolizaDTO> listarTodos() throws SystemException {
		return new PolizaSN().listarTodos();
	}
	/**
	 * Busca los datos de una poliza por su id.
	 * @param idToPoliza
	 * @return
	 * @throws SystemException
	 */
	public PolizaDTO getPorId(BigDecimal idToPoliza) throws SystemException {
		return new PolizaSN().getPorId(idToPoliza);
	}
	/**
	 * Busca todas las polizas que esten en un estatus dado.
	 * @param cveEstatus
	 * @return
	 * @throws SystemException
	 */
	public List<PolizaDTO> listarPorEstatus(short cveEstatus) throws SystemException {
		return new PolizaSN().listarPorEstatus(cveEstatus);
	}
	/**
	 * Method listarPorNumPolizaFormato
	 * @param numPolizaFormato
	 * @return
	 * @throws SystemException
	 */
	public List<PolizaDTO> listarPorNumPolizaFormato(String numPolizaFormato) throws SystemException {
		return new PolizaSN().listarPorNumPolizaFormato(numPolizaFormato);
	}
	/**
	 * Method obtenerNombreRecibo
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public String obtenerNombreRecibo(BigDecimal idToPoliza,Short numeroEndoso){
		String nombreYRuta = null;
		if (idToPoliza != null && numeroEndoso != null){
			String rutaRecibo;
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1)
				rutaRecibo = Sistema.UPLOAD_FOLDER;
			else
				rutaRecibo = Sistema.LINUX_UPLOAD_FOLDER; 
			String nombreRecibo = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.poliza.recibo.nombreArchivo", idToPoliza.toBigInteger().toString(),numeroEndoso);
			if (nombreRecibo != null)
				nombreYRuta = rutaRecibo+nombreRecibo;
		}
		return nombreYRuta;
	}
	/**
	 * Method buscaPrimerRegistro().
	 * @return
	 * @throws SystemException
	 */
	public PolizaDTO buscaPrimerRegistro() throws SystemException {
		return new PolizaSN().buscaPrimerRegistro();
	}
	/**
	 * Method actualizar().
	 * @param polizaDTO
	 * @return
	 * @throws SystemException
	 */
	public PolizaDTO actualizar(PolizaDTO polizaDTO)throws SystemException {
		 return new PolizaSN().actualizar(polizaDTO);
	}
	
	public List<PolizaDTO> buscarPolizasPorPropiedad(String nombreProp ,Object value) throws SystemException{
	    return new PolizaSN().buscarPolizasPorPropiedad(nombreProp, value);
	}
	
	public boolean existeReciboPagado(BigDecimal idToPoliza)throws SystemException{	
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		List<ReciboDTO> recibosPolizaOriginal = ReciboDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).consultaRecibos(polizaDTO.getIdToPoliza(),(short)0);
		ReciboDTO ultimoReciboEmitido= null;
		boolean existeReciboPagado = false;
		for(ReciboDTO reciboDTO:recibosPolizaOriginal){
			if (reciboDTO.getSituacion().trim().equals(Sistema.RECIBO_EMITIDO)){// se valida el primer recibo no pagado emitido
				if (ultimoReciboEmitido == null)
					ultimoReciboEmitido = reciboDTO;
			}else if (reciboDTO.getSituacion().trim().equals(Sistema.RECIBO_PAGADO)){
				existeReciboPagado = true;
			}
		}
		return existeReciboPagado;
	}
	
	//Actualiza en cotizacion los montos de RFP y bonificaci�n de comisi�n de RFP , colocando los acumulados de todos los endosos 
	//de una p�liza particular, prorrateados de acuerdo a los d�as por devengar dados como par�metro. 
	//(Se toman en cuenta todos los endosos a partir del �ltimo endoso de cambio de forma de pago emitido).
	public void setMontosRFPAcumulados(BigDecimal idToPoliza,double diasPorDevengar, CotizacionDTO cotizacion)throws SystemException{
		List<EndosoDTO> endosos = EndosoDN.getInstancia(null).listarEndososPorPoliza(idToPoliza, false);		
		
		double montoRPF = 0;
		double montoComisionRFP = 0;
		double montoBonificacionComisionRFP = 0;
				
		//suma los montos de todos los endosos a partir del �ltimo endoso de cambio de forma de pago emitido
		for(EndosoDTO endoso : endosos){			
			//Se obtiene el factor de vigencia
			double diasEndoso = UtileriasWeb.obtenerDiasEntreFechas(endoso.getFechaInicioVigencia(), 
																	endoso.getFechaFinVigencia());			
			double factor = diasPorDevengar/diasEndoso;			
			factor = factor > 1 ? 1 : factor; //es necesario igualar el factor a 1 para no regresar m�s RPF de los emitidos
			
			montoRPF += endoso.getValorRecargoPagoFrac() * factor;
			montoComisionRFP += endoso.getValorBonifComisionRPF() * factor;
			montoBonificacionComisionRFP += endoso.getValorBonifComisionRPF() * factor;
			
			if(endoso.getClaveTipoEndoso().compareTo(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0)
				break;
		}
		
		cotizacion.setMontoRecargoPagoFraccionado(montoRPF);
		cotizacion.setMontoComisionPagoFraccionado(montoComisionRFP);
		cotizacion.setMontoBonificacionComisionPagoFraccionado(montoBonificacionComisionRFP);
		
	}
	
	//Obtiene la forma de pago actual de la p�liza, considerando todos los endosos de cambio de forma de pago
	public int obtenerFormaPagoActual(BigDecimal idToPoliza) throws SystemException{
		int idFormaPago = 0;
		List<EndosoDTO> endosos = EndosoDN.getInstancia(null).listarEndososPorPoliza(idToPoliza, false);
		for(EndosoDTO endoso : endosos){
			if(endoso.getId().getNumeroEndoso() == 0 || 
			   endoso.getClaveTipoEndoso().compareTo(Sistema.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO) == 0){
			   BigDecimal idToCotizacion = endoso.getIdToCotizacion();
			   CotizacionDTO cotizacion = CotizacionDN.getInstancia(null).getPorId(idToCotizacion);
			   idFormaPago = cotizacion.getIdFormaPago().intValue();
			   break;
			}
		}
		return idFormaPago;		
	}
}
