package mx.com.afirme.midas.poliza.folio;

import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class FolioPolizaSN {
	private FolioPolizaFacadeRemote beanRemoto;

	public FolioPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(FolioPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(FolioPolizaDTO folioPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(folioPolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public Integer getSiguienteNumeroPoliza(String codigoProducto,
			String codigoTipoPoliza) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.getSiguienteNumeroPoliza(codigoProducto,
					codigoTipoPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
