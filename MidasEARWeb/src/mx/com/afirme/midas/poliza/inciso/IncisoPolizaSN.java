package mx.com.afirme.midas.poliza.inciso;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IncisoPolizaSN {
	private IncisoPolizaFacadeRemote beanRemoto;

	public IncisoPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(IncisoPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public IncisoPolizaDTO agregar(IncisoPolizaDTO incisoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(incisoPolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<IncisoPolizaDTO> agregar(List<IncisoPolizaDTO> incisos)
			throws ExcepcionDeAccesoADatos {
		List<IncisoPolizaDTO> incisosPersistidos = new ArrayList<IncisoPolizaDTO>();
		try {
			for (IncisoPolizaDTO inciso : incisos) {
				incisosPersistidos.add(beanRemoto.save(inciso));
			}
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return incisosPersistidos;
	}
}
