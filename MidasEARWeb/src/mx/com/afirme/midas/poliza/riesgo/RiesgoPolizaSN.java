package mx.com.afirme.midas.poliza.riesgo;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RiesgoPolizaSN {
	private RiesgoPolizaFacadeRemote beanRemoto;

	public RiesgoPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(RiesgoPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public RiesgoPolizaDTO agregar(RiesgoPolizaDTO riesgoPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(riesgoPolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RiesgoPolizaDTO> agregar(List<RiesgoPolizaDTO> riesgos)
			throws ExcepcionDeAccesoADatos {
		try {
			List<RiesgoPolizaDTO> riesgosPersistidos = new ArrayList<RiesgoPolizaDTO>();
			for (RiesgoPolizaDTO riesgo : riesgos) {
				riesgosPersistidos.add(beanRemoto.save(riesgo));
			}
			return riesgosPersistidos;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RiesgoPolizaDTO getPorId(RiesgoPolizaId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
