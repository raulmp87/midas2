package mx.com.afirme.midas.poliza.seccion;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SeccionPolizaSN {
	private SeccionPolizaFacadeRemote beanRemoto;

	public SeccionPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SeccionPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<SeccionPolizaDTO> agregar(List<SeccionPolizaDTO> secciones)
			throws ExcepcionDeAccesoADatos {
		List<SeccionPolizaDTO> seccionesPersistidas = new ArrayList<SeccionPolizaDTO>();
		try {
			for (SeccionPolizaDTO seccion : secciones) {
				seccionesPersistidas.add(beanRemoto.save(seccion));
			}
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return seccionesPersistidas;
	}
}
