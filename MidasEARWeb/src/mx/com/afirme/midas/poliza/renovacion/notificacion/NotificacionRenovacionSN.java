package mx.com.afirme.midas.poliza.renovacion.notificacion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class NotificacionRenovacionSN {
	private NotificacionRenovacionFacadeRemote beanRemoto;

	public NotificacionRenovacionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(NotificacionRenovacionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<NotificacionRenovacionDTO> buscarNotificacionRenovacionPropiedad(
			String nombrePropiedad, Object valor)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void agregar(NotificacionRenovacionDTO notificacionRenovacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(notificacionRenovacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(NotificacionRenovacionDTO notificacionRenovacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(notificacionRenovacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public NotificacionRenovacionDTO actualizar(
			NotificacionRenovacionDTO notificacionRenovacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(notificacionRenovacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
