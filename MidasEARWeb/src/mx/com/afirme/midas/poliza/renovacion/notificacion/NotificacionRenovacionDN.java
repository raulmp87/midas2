package mx.com.afirme.midas.poliza.renovacion.notificacion;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class NotificacionRenovacionDN {
	private static final NotificacionRenovacionDN INSTANCIA = new NotificacionRenovacionDN();

	public static NotificacionRenovacionDN getInstancia() {
		return NotificacionRenovacionDN.INSTANCIA;
	}

	public void agregarNotificacionRenovacion(
			NotificacionRenovacionDTO notificacionRenovacionDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		try {
			new NotificacionRenovacionSN().agregar(notificacionRenovacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
