package mx.com.afirme.midas.poliza.renovacion;

import java.util.List;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class RenovacionPolizaForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Filtros de busqueda
	private String numeroPoliza;
	private String nombreAsegurado;
	private String fechaInicioVigencia;
	private String fechaFinVigencia;
	private String estatus;
	private String idProducto;
	private String idSuscriptor;
	private String polizasListadas = "0";
	private String idDestinatarioCorreo;
	private String destinatarioCorreo;
	private String idToCotizacion;
	private RenovacionPolizaDTO detalleRenovacion;
	private String esFacultativa;
	private String fechaAsignacionDesde;
	private String fechaAsignacionHasta;
	private String esReporte="false";
	private String adjuntarCotizacion="0";

	// Polizas Renovables
	private List<PolizaDTO> polizasRenovables;
	// Seguimiento de Polizas Renovables
	private List<RenovacionPolizaDTO> seguimientoRenovaciones;

	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	public List<PolizaDTO> getPolizasRenovables() {
		return polizasRenovables;
	}

	public void setPolizasRenovables(List<PolizaDTO> polizasRenovables) {
		this.polizasRenovables = polizasRenovables;
	}

	public String getIdSuscriptor() {
		return idSuscriptor;
	}

	public void setIdSuscriptor(String idSuscriptor) {
		this.idSuscriptor = idSuscriptor;
	}

	public String getPolizasListadas() {
		return polizasListadas;
	}

	public void setPolizasListadas(String polizasListadas) {
		this.polizasListadas = polizasListadas;
	}

	public String getIdDestinatarioCorreo() {
		return idDestinatarioCorreo;
	}

	public void setIdDestinatarioCorreo(String idDestinatarioCorreo) {
		this.idDestinatarioCorreo = idDestinatarioCorreo;
	}

	public String getDestinatarioCorreo() {
		return destinatarioCorreo;
	}

	public void setDestinatarioCorreo(String destinatarioCorreo) {
		this.destinatarioCorreo = destinatarioCorreo;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public RenovacionPolizaDTO getDetalleRenovacion() {
		return detalleRenovacion;
	}

	public void setDetalleRenovacion(RenovacionPolizaDTO detalleRenovacion) {
		this.detalleRenovacion = detalleRenovacion;
	}

	public List<RenovacionPolizaDTO> getSeguimientoRenovaciones() {
		return seguimientoRenovaciones;
	}

	public void setSeguimientoRenovaciones(
			List<RenovacionPolizaDTO> seguimientoRenovaciones) {
		this.seguimientoRenovaciones = seguimientoRenovaciones;
	}

	public String getEsFacultativa() {
		return esFacultativa;
	}

	public void setEsFacultativa(String esFacultativa) {
		this.esFacultativa = esFacultativa;
	}

	public String getFechaAsignacionDesde() {
		return fechaAsignacionDesde;
	}

	public void setFechaAsignacionDesde(String fechaAsignacionDesde) {
		this.fechaAsignacionDesde = fechaAsignacionDesde;
	}

	public String getFechaAsignacionHasta() {
		return fechaAsignacionHasta;
	}

	public void setFechaAsignacionHasta(String fechaAsignacionHasta) {
		this.fechaAsignacionHasta = fechaAsignacionHasta;
	}

	public String getEsReporte() {
		return esReporte;
	}

	public void setEsReporte(String esReporte) {
		this.esReporte = esReporte;
	}

	public String getAdjuntarCotizacion() {
		return adjuntarCotizacion;
	}

	public void setAdjuntarCotizacion(String adjuntarCotizacion) {
		this.adjuntarCotizacion = adjuntarCotizacion;
	}

}
