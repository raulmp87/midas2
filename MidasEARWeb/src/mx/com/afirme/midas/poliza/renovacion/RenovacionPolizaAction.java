package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RenovacionPolizaAction extends MidasMappingDispatchAction {

	/**
	 *Busca todas las polizas existentes renovables en la BD y retorna a la
	 * pagina de listar, si no ocurre ningun problema.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarRenovables(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			RenovacionPolizaForm renovacionPolizaForm = (RenovacionPolizaForm) form;
			PolizaDTO polizaDTO = new PolizaDTO();
			
			poblarDTO(renovacionPolizaForm, polizaDTO);
			List<PolizaDTO> polizas = RenovacionPolizaDN.getInstancia()
					.buscarFiltrado(polizaDTO, Boolean.FALSE, Boolean.valueOf(renovacionPolizaForm.getEsReporte()));

			this.setNombreProductoAsegurado(polizas, usuario.getNombreUsuario(),PolizaDTO.class);
			List<PolizaDTO> polizasFinal = new ArrayList<PolizaDTO>();
			if(polizaDTO.getEsFacultativa().equalsIgnoreCase("0")){
				for(PolizaDTO poliza:polizas){					
					if(poliza.getEsFacultativa().equalsIgnoreCase("NO")){
						polizasFinal.add(poliza);
					}
				}
			}else{
				polizasFinal.addAll(polizas);
			}
			renovacionPolizaForm.setPolizasRenovables(polizasFinal);
			if (polizasFinal != null && polizasFinal.size() > 0)
				renovacionPolizaForm.setPolizasListadas(String.valueOf(polizasFinal
						.size()));
			request.setAttribute("polizas", polizasFinal);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	public ActionForward listarSeguimientoRenovaciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			RenovacionPolizaForm renovacionPolizaForm = (RenovacionPolizaForm) form;
			RenovacionPolizaDTO renovacionPolizaDTO= new RenovacionPolizaDTO();
			poblarRenovacionDTO(renovacionPolizaForm, renovacionPolizaDTO);
			List<RenovacionPolizaDTO> renovaciones = RenovacionPolizaDN.getInstancia()
					.buscarFiltrado(renovacionPolizaDTO, Boolean.FALSE);
			this.setNombreProductoAsegurado(renovaciones, usuario.getNombreUsuario(), RenovacionPolizaDTO.class);
			renovacionPolizaForm.setSeguimientoRenovaciones(renovaciones);
			if (renovaciones != null && renovaciones.size() > 0)
				renovacionPolizaForm.setPolizasListadas(String.valueOf(renovaciones
						.size()));
			request.setAttribute("polizas", renovaciones);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}	

	/**
	 *Asigna las polizas seleccionadas al suscriptor elegido retorna a la
	 * pagina de listar, si no ocurre ningun problema.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public ActionForward enviarCorreo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String mensaje = "";
		RenovacionPolizaForm renovacionPolizaForm = (RenovacionPolizaForm) form;
		try {
			String polizas = request.getParameter("polizas").trim();
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);
			polizas = polizas.substring(0, polizas.lastIndexOf(","));
			String[] polizasSplit = null;
			if (polizas.contains(",")) {
				polizasSplit = polizas.split(",");
			} else {
				polizasSplit = new String[1];
				polizasSplit[0] = polizas;
			}

			RenovacionPolizaDTO renovacionPolizaDTO =new RenovacionPolizaDTO();
			poblarRenovacionDTO(renovacionPolizaForm, renovacionPolizaDTO);
			
			Map<String, String> mesajesError = new HashMap<String, String>();
			boolean adjuntaCotizacion = renovacionPolizaForm.getAdjuntarCotizacion().equals("on")?true:false; 
			if (!RenovacionPolizaDN.getInstancia()
					.enviaNotificacionCorreoElectronico(polizasSplit,
							usuario.getNombreUsuario(),
							renovacionPolizaForm.getIdDestinatarioCorreo(),
							renovacionPolizaForm.getDestinatarioCorreo(),
							mesajesError, adjuntaCotizacion)) {
				if (mesajesError != null && mesajesError.size() > 0) {
				mensaje = obtenerMensaje(mesajesError);
					agregarMensaje(mensaje, Sistema.ERROR, renovacionPolizaForm);
				}
			} else {
				// exito en el proceso de renovacion
				mensaje += "Los correos se enviaron satisfactoriamente";
				agregarMensaje(mensaje, Sistema.EXITO, renovacionPolizaForm);
			}
			List<RenovacionPolizaDTO> renovaciones = RenovacionPolizaDN.getInstancia().buscarFiltrado(renovacionPolizaDTO, Boolean.FALSE);
			
			this.setNombreProductoAsegurado(renovaciones, usuario.getNombreUsuario(), RenovacionPolizaDTO.class);
			renovacionPolizaForm.setSeguimientoRenovaciones(renovaciones);
			if (renovaciones != null && renovaciones.size() > 0)
				renovacionPolizaForm.setPolizasListadas(String
						.valueOf(renovaciones.size()));
			request.setAttribute("polizas", renovaciones);
		} catch (SystemException e) {
			mensaje += "Ocurri&oacute; un error al enviar los correos electr&oacute;nicos";
			agregarMensaje(mensaje, Sistema.ERROR, renovacionPolizaForm);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ExcepcionDeAccesoADatos e) {
			mensaje += "Ocurri&oacute; un error al enviar los correos electr&oacute;nicos";
			agregarMensaje(mensaje, Sistema.ERROR, renovacionPolizaForm);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	public String obtenerMensaje(Map<String, String> mesajesError) {
	StringBuilder	mensaje = new StringBuilder("");
		Iterator<?> it = mesajesError.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> pairs = (Map.Entry<String, String>) it
					.next();
			mensaje.append("P�liza: " ).append(pairs.getKey() ).append(" Error: ").append(pairs.getValue() ).append(" </br>");
		}
		return mensaje.toString();
	}
	/**
	 *Asigna las polizas seleccionadas al suscriptor elegido retorna a la
	 * pagina de listar, si no ocurre ningun problema.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	@SuppressWarnings("unchecked")
	public ActionForward asignarRenovaciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String mensaje = "";
		RenovacionPolizaForm renovacionPolizaForm = (RenovacionPolizaForm) form;
		try {
			String polizas = request.getParameter("polizas").trim();
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);
			polizas = polizas.substring(0, polizas.lastIndexOf(","));
			String[] polizasSplit = null;
			if (polizas.contains(",")) {
				polizasSplit = polizas.split(",");
			} else {
				polizasSplit = new String[1];
				polizasSplit[0] = polizas;
			}
			PolizaDTO polizaDTO = new PolizaDTO();
			poblarDTO(renovacionPolizaForm, polizaDTO);
			Map<String, String> mesajesError = new HashMap<String, String>();
			if (!RenovacionPolizaDN.getInstancia().procesaRenovacionPoliza(
					polizasSplit, usuario.getNombreUsuario(),
					renovacionPolizaForm.getIdSuscriptor(), mesajesError)) {
				if (mesajesError != null && mesajesError.size() > 0) {
					mensaje = obtenerMensaje(mesajesError);
					agregarMensaje(mensaje, Sistema.ERROR, renovacionPolizaForm);
				}
			} else {
				// exito en el proceso de renovacion
				mensaje += "Termino satisfactorimente el proceso de asignaci&oacute;n";
				agregarMensaje(mensaje, Sistema.EXITO, renovacionPolizaForm);
			}
			List<PolizaDTO> polizasDTO = RenovacionPolizaDN.getInstancia()
					.buscarFiltrado(polizaDTO, Boolean.FALSE, Boolean.FALSE);
			this.setNombreProductoAsegurado(polizasDTO, usuario.getNombreUsuario(), PolizaDTO.class);
			renovacionPolizaForm.setPolizasRenovables(polizasDTO);
			if (polizasDTO != null && polizasDTO.size() > 0)
				renovacionPolizaForm.setPolizasListadas(String
						.valueOf(polizasDTO.size()));
		} catch (SystemException e) {
			mensaje += "Ocurri&oacute; un error al procesar la asignaci&oacute;n de polizas";
			agregarMensaje(mensaje, Sistema.ERROR, renovacionPolizaForm);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ExcepcionDeAccesoADatos e) {
			mensaje += "Ocurri&oacute; un error al procesar la asignaci&oacute;n de polizas";
			agregarMensaje(mensaje, Sistema.ERROR, renovacionPolizaForm);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	@SuppressWarnings("unchecked")
	private void setNombreProductoAsegurado(List<?> polizas,
			String nombreUsuario, Class<?> classe) throws ExcepcionDeAccesoADatos,
			SystemException {
		if(classe.getCanonicalName().equals(PolizaDTO.class.getCanonicalName())){
			for (PolizaDTO poliza : (List<PolizaDTO>)polizas) {
				RenovacionPolizaDN.getInstancia().setNombreAseguradoProducto(poliza, nombreUsuario);
			}			
		}else{
			for (RenovacionPolizaDTO renovacion : (List<RenovacionPolizaDTO>)polizas) {
				RenovacionPolizaDN.getInstancia().setNombreAseguradoProducto(renovacion.getPolizaDTO(), nombreUsuario);
			}			
		}
	}


	/**
	 * Metodo que muestra la pantalla de renovacion de polizas
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarRenovables(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarRenovacionDTO(RenovacionPolizaForm renovacionPolizaForm,
			RenovacionPolizaDTO renovacionPolizaDTO) throws SystemException {
		
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm.getIdToCotizacion())){
			renovacionPolizaDTO.setCotizacionDTO(new CotizacionDTO());
			renovacionPolizaDTO.getCotizacionDTO().setIdToCotizacion(
					UtileriasWeb.regresaBigDecimal(renovacionPolizaForm
							.getIdToCotizacion()));		
		}		
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm.getNumeroPoliza())){
			renovacionPolizaDTO.setPolizaDTO(new PolizaDTO());
			renovacionPolizaDTO.getPolizaDTO()
					.setNumeroPoliza(
							Integer.valueOf(renovacionPolizaForm
									.getNumeroPoliza()));			
		}
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm.getNombreAsegurado())){
			if(renovacionPolizaDTO.getCotizacionDTO() == null)
				renovacionPolizaDTO.setCotizacionDTO(new CotizacionDTO());
			renovacionPolizaDTO.getCotizacionDTO().setNombreAsegurado(renovacionPolizaForm.getNombreAsegurado());
		}		
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm
				.getNombreAsegurado())) {
			if(renovacionPolizaDTO.getPolizaDTO() == null)
				renovacionPolizaDTO.setPolizaDTO(new PolizaDTO());
			if(renovacionPolizaDTO.getPolizaDTO().getCotizacionDTO() == null)
				renovacionPolizaDTO.setCotizacionDTO(new CotizacionDTO());			
			renovacionPolizaDTO.getCotizacionDTO().setNombreAsegurado(
					renovacionPolizaForm.getNombreAsegurado());
		}
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm
				.getFechaInicioVigencia())
				&& !UtileriasWeb.esCadenaVacia(renovacionPolizaForm
						.getFechaFinVigencia())) {
			String fechaInicial[] = renovacionPolizaForm
					.getFechaInicioVigencia().split("/");
			String fechaFinal[] = renovacionPolizaForm.getFechaFinVigencia()
					.split("/");
			GregorianCalendar gcInicial = new GregorianCalendar(Integer
					.valueOf(fechaInicial[2]),
					Integer.valueOf(fechaInicial[1]) - 1, Integer
							.valueOf(fechaInicial[0]));
			GregorianCalendar gcFinal = new GregorianCalendar(Integer
					.valueOf(fechaFinal[2]),
					Integer.valueOf(fechaFinal[1]) - 1, Integer
							.valueOf(fechaFinal[0]));
			if(renovacionPolizaDTO.getPolizaDTO() == null)
				renovacionPolizaDTO.setPolizaDTO(new PolizaDTO());
			if(renovacionPolizaDTO.getCotizacionDTO() == null)
				renovacionPolizaDTO.setCotizacionDTO(new CotizacionDTO());				
			
			renovacionPolizaDTO.getCotizacionDTO()
					.setFechaInicioVigencia(gcInicial.getTime());
			renovacionPolizaDTO.getCotizacionDTO()
					.setFechaFinVigencia(gcFinal.getTime());

		}
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm
				.getFechaAsignacionDesde())
				&& !UtileriasWeb.esCadenaVacia(renovacionPolizaForm
						.getFechaAsignacionHasta())) {
			String fechaInicial[] = renovacionPolizaForm
					.getFechaAsignacionDesde().split("/");
			String fechaFinal[] = renovacionPolizaForm.getFechaAsignacionHasta()
					.split("/");
			GregorianCalendar gcInicial = new GregorianCalendar(Integer
					.valueOf(fechaInicial[2]),
					Integer.valueOf(fechaInicial[1]) - 1, Integer
							.valueOf(fechaInicial[0]));
			GregorianCalendar gcFinal = new GregorianCalendar(Integer
					.valueOf(fechaFinal[2]),
					Integer.valueOf(fechaFinal[1]) - 1, Integer
							.valueOf(fechaFinal[0]));
			if(renovacionPolizaDTO == null)
				renovacionPolizaDTO = new RenovacionPolizaDTO();			
			
			renovacionPolizaDTO.setFechaCreacionDesde(gcInicial.getTime());
			renovacionPolizaDTO.setFechaCreacionHasta(gcFinal.getTime());

		}
		
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm.getIdProducto())) {
			ProductoDTO productoDTO = new ProductoDTO();
			productoDTO.setIdToProducto(UtileriasWeb
					.regresaBigDecimal(renovacionPolizaForm.getIdProducto()));
			productoDTO = ProductoDN.getInstancia().getPorId(productoDTO);
			if(renovacionPolizaDTO.getPolizaDTO() == null)
				renovacionPolizaDTO.setPolizaDTO(new PolizaDTO());
			renovacionPolizaDTO.getPolizaDTO().setCodigoProducto(productoDTO.getCodigo());
		}

	}	
	private void poblarDTO(RenovacionPolizaForm renovacionPolizaForm,
			PolizaDTO polizaDTO) throws SystemException {
		
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm.getNumeroPoliza()))
			polizaDTO.setNumeroPoliza(Integer.valueOf(renovacionPolizaForm
					.getNumeroPoliza()));
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm
				.getNombreAsegurado())) {
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setNombreAsegurado(
					renovacionPolizaForm.getNombreAsegurado());
		}
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm
				.getFechaInicioVigencia())
				&& !UtileriasWeb.esCadenaVacia(renovacionPolizaForm
						.getFechaFinVigencia())) {
			String fechaInicial[] = renovacionPolizaForm
					.getFechaInicioVigencia().split("/");
			String fechaFinal[] = renovacionPolizaForm.getFechaFinVigencia()
					.split("/");
			GregorianCalendar gcInicial = new GregorianCalendar(Integer
					.valueOf(fechaInicial[2]),
					Integer.valueOf(fechaInicial[1]) - 1, Integer
							.valueOf(fechaInicial[0]));
			GregorianCalendar gcFinal = new GregorianCalendar(Integer
					.valueOf(fechaFinal[2]),
					Integer.valueOf(fechaFinal[1]) - 1, Integer
							.valueOf(fechaFinal[0]));
			if (polizaDTO.getCotizacionDTO() != null) {
				polizaDTO.getCotizacionDTO().setFechaInicioVigencia(
						gcInicial.getTime());
				polizaDTO.getCotizacionDTO().setFechaFinVigencia(
						gcFinal.getTime());
			} else {
				polizaDTO.setCotizacionDTO(new CotizacionDTO());
				polizaDTO.getCotizacionDTO().setFechaInicioVigencia(
						gcInicial.getTime());
				polizaDTO.getCotizacionDTO().setFechaFinVigencia(
						gcFinal.getTime());
			}
		}
		if (!UtileriasWeb.esCadenaVacia(renovacionPolizaForm.getIdProducto())) {
			ProductoDTO productoDTO = new ProductoDTO();
			productoDTO.setIdToProducto(UtileriasWeb
					.regresaBigDecimal(renovacionPolizaForm.getIdProducto()));
			ProductoDN productoDN = ProductoDN.getInstancia();
			productoDTO = productoDN.getPorId(productoDTO);
			polizaDTO.setCodigoProducto(productoDTO.getCodigo());
		}
		polizaDTO.setEsFacultativa(UtileriasWeb.calculaValorEnteroDelCheck(renovacionPolizaForm.getEsFacultativa()));
		polizaDTO.setClaveEstatus(Sistema.ESTATUS_VIGENTE);
	}

	/**
	 *Busca todas las polizas en proceso de renovacion en la BD y retorna a la
	 * pagina de listar, si no ocurre ningun problema.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarRenovaciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);			
			RenovacionPolizaForm renovacionPolizaForm = (RenovacionPolizaForm) form;
			RenovacionPolizaDTO renovacionPolizaDTO = new RenovacionPolizaDTO();
			poblarRenovacionDTO(renovacionPolizaForm, renovacionPolizaDTO);
			List<RenovacionPolizaDTO> polizas = RenovacionPolizaDN.getInstancia()
					.buscarFiltrado(renovacionPolizaDTO, Boolean.FALSE);
			this.setNombreProductoAsegurado(polizas, usuario.getNombreUsuario(), RenovacionPolizaDTO.class);
			renovacionPolizaForm.setSeguimientoRenovaciones(polizas);
			if (polizas != null && polizas.size() > 0)
				renovacionPolizaForm.setPolizasListadas(String.valueOf(polizas
						.size()));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method mostrar la bitacora de Seguimiento
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarBitacoraSeguimiento(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
					request, Sistema.USUARIO_ACCESO_MIDAS);				
			RenovacionPolizaForm renovacionPolizaForm = (RenovacionPolizaForm) form;
			BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(request
					.getParameter("idToPoliza"));

			PolizaDTO polizaDTO = new PolizaDTO();
			polizaDTO.setIdToPoliza(idToPoliza);

			List<PolizaDTO> polizas = RenovacionPolizaDN.getInstancia()
					.buscarFiltrado(polizaDTO, Boolean.TRUE, Boolean.FALSE);
			if (polizas != null && polizas.size() > 0) {
				this.setNombreProductoAsegurado(polizas, usuario.getNombreUsuario(), PolizaDTO.class);
				polizaDTO = polizas.get(0);
				
				RenovacionPolizaDTO detalleRenovacion = RenovacionPolizaDN
						.getInstancia().buscarDetalleRenovacionPoliza(
								polizaDTO.getIdToPoliza());
				Collections.sort(detalleRenovacion.getSeguimientoRenovacion());
				detalleRenovacion.setPolizaDTO(polizaDTO);
				renovacionPolizaForm.setDetalleRenovacion(detalleRenovacion);
			}

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);

	}
}
