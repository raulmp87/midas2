package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class RenovacionPolizaSN {
	private RenovacionPolizaFacadeRemote beanRemoto;

	public RenovacionPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(RenovacionPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public Map<String, String> procesaRenovacionPoliza(BigDecimal idToPoliza,
			String usuarioCreacion, String usuarioAsigacion)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.procesaRenovacionPoliza(idToPoliza,
					usuarioCreacion, usuarioAsigacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO,
			boolean esRenovacion, boolean esReporte) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarFiltrado(polizaDTO, esRenovacion, esReporte);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void agregarDetalleSeguimiento(BigDecimal idToPoliza,
			SeguimientoRenovacionDTO seguimientoRenovacionDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.agregarDetalleSeguimiento(idToPoliza,
					seguimientoRenovacionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public RenovacionPolizaDTO buscarDetalleRenovacionPoliza(
			BigDecimal idToPoliza) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarDetalleRenovacionPoliza(idToPoliza);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RenovacionPolizaDTO> buscarRenovacionPolizaPorPropiedad(
			String nombrePropiedad, Object valor)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(nombrePropiedad, valor);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RenovacionPolizaDTO> buscarFiltrado(
			RenovacionPolizaDTO renovacionPolizaDTO, boolean esNotificacion)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.buscarFiltrado(renovacionPolizaDTO,
					esNotificacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
