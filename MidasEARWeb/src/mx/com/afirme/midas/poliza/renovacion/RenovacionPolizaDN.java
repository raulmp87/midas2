package mx.com.afirme.midas.poliza.renovacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.MailAction;

public class RenovacionPolizaDN {
	private static final RenovacionPolizaDN INSTANCIA = new RenovacionPolizaDN();
	private static final String ERROR_AL_COPIAR_COTIZACION = "Error al generar la copia de la cotizaci&oacute;n.";
	private static final String ERROR_AL_PARSEAR_POLIZA = "Error al obtener la p&oacute;liza a renovar";
	private String asuntoCorreo;
	private String destinatarioNombre;
	private String bodyCorreo;
	private String detalleNotificacion;
	private String diasVencimiento;
	private BigDecimal tipoMovimiento;

	public static RenovacionPolizaDN getInstancia() {
		return RenovacionPolizaDN.INSTANCIA;
	}

	/**
	 * Se procesa el arreglo de polizas se notifica por
	 * correo electronico al destinatario seleccionado
	 * 
	 * @param polizas
	 *            : Arreglo de polizas
	 * @param tipoDestinario
	 *            : De acuerdo al catalogo de valores fijos 51 los tipos son 10.
	 *            Ejecutivo de Ventas, 20. Agente, 30. Cliente, 40. Otro
	 * @param diasVencimiento
	 *            : Dias de vencimiento de la poliza
	 * @return boolean: true si se completo con exito la transaccion, false si
	 *         ocurrio un error en la transaccion,
	 * @throws SystemException
	 */
	public boolean enviaNotificacionCorreoElectronico(PolizaDTO polizaDTO,
			String usuarioCreacion, String tipoDestinario, String diasVencimiento, boolean adjuntaCotizacion)
			throws SystemException {
		boolean exito = Boolean.TRUE;
		Map<String, String>mesajesError = new HashMap<String, String>();
		if (polizaDTO != null) {
			this.setTipoMovimiento(Sistema.TIPO_ENVIO_ALERTAMIENTO);
			this.setDiasVencimiento(diasVencimiento);
			exito = this.enviaNotificacionCorreoElectronico(polizaDTO,
					usuarioCreacion, tipoDestinario, null, mesajesError, Boolean.TRUE, adjuntaCotizacion);
		}
		return exito;
	}
	/**
	 * Se procesa el arreglo de polizas separadas por pipe | se notifica por
	 * correo electronico al destinatario seleccionado
	 * 
	 * @param polizasSplit
	 *            : Arreglo de polizas separados por pipe |
	 * @param tipoDestinario
	 *            : De acuerdo al catalogo de valores fijos 51 los tipos son 10.
	 *            Ejecutivo de Ventas, 20. Agente, 30. Cliente, 40. Otro
	 * @param destinatarioCorreoElectronico
	 *            : Campo requerido siempre y cuando el tipo de destinatario sea
	 *            40. Otro.
	 * @return boolean: true si se completo con exito la transaccion, false si
	 *         ocurrio un error en la transaccion, y se llena el mapa de
	 *         mensajes de error
	 * @throws SystemException
	 */
	public boolean enviaNotificacionCorreoElectronico(String[] polizasSplit,
			String usuarioCreacion, String tipoDestinario,
			String destinatarioCorreoElectronico,
			Map<String, String> mesajesError, boolean adjuntaCotizacion) throws SystemException {
		boolean exito = Boolean.TRUE;
		if (mesajesError == null)
			mesajesError = new HashMap<String, String>();

		if (polizasSplit != null && polizasSplit.length > 0) {
			Double diasVence = 0D;
			for (String poliza : polizasSplit) {
				BigDecimal idToPoliza = null;
				this.setTipoMovimiento(Sistema.TIPO_ENVIO_EMAIL);
				try {
					idToPoliza = UtileriasWeb.regresaBigDecimal(poliza);
				} catch (SystemException e) {
					exito = Boolean.FALSE;
				}
				PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(
						idToPoliza);
				diasVence = UtileriasWeb.obtenerDiasEntreFechas(
						new Date(), polizaDTO.getCotizacionDTO()
						.getFechaFinVigencia());
				this.setDiasVencimiento(diasVence.toString());
				this.setNombreAseguradoProducto(polizaDTO, usuarioCreacion);
				exito = this.enviaNotificacionCorreoElectronico(polizaDTO,
						usuarioCreacion, tipoDestinario,
						destinatarioCorreoElectronico, mesajesError, Boolean.FALSE, adjuntaCotizacion);
			}
		}
		return exito;
	}

	
	private boolean enviaNotificacionCorreoElectronico(PolizaDTO polizaDTO,
			String usuarioCreacion, String tipoDestinario,
			String destinatarioCorreoElectronico,
			Map<String, String> mesajesError, boolean esNotificacion, boolean adjuntaCotizacion) {
		boolean exito = Boolean.TRUE;
		if (polizaDTO != null) {
			List<String> destinatarios = new ArrayList<String>();
			this.setAsuntoCorreo(UtileriasWeb.getMensajeRecurso(
					Sistema.ARCHIVO_RECURSOS,
					"poliza.renovacion.mail.asunto", polizaDTO
							.getNumeroPolizaFormateada()));
			destinatarios.clear();
			RenovacionPolizaDTO detalleRenovacion = null;
			try {
				detalleRenovacion = this.buscarDetalleRenovacionPoliza(polizaDTO.getIdToPoliza());
			} catch (Exception e1) {}			
			if (tipoDestinario.equals(Sistema.DESTINATARIO_CORREO_OTRO)) {
				this.setDestinatarioNombre("Cliente");				
				this.setBodyCorreo(UtileriasWeb.getMensajeRecurso(
						Sistema.ARCHIVO_RECURSOS,
						"poliza.renovacion.mail.body",
						polizaDTO.getNumeroPolizaFormateada(),
						detalleRenovacion != null?detalleRenovacion.getPolizaDTO().getNombreAsegurado():polizaDTO.getNombreAsegurado(),
						this.getDiasVencimiento(),
						detalleRenovacion != null? detalleRenovacion.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud().toString(): "N/D"));
				this
						.setDetalleNotificacion(UtileriasWeb
								.getMensajeRecurso(
										Sistema.ARCHIVO_RECURSOS,
										"poliza.renovacion.detalle.movimiento.email",
										destinatarioCorreoElectronico,
										"Cliente",
										"Ingres� otro correo electr�nico no registrado en MIDAS"));
				destinatarios.add(destinatarioCorreoElectronico);
			} else {
				String destinatario = null;
				try {
					destinatario = this.asignarDestinatarios(
							tipoDestinario, polizaDTO);
				} catch (SystemException e) {
				}
				if (destinatario != null) {
					destinatarios.add(destinatario);
				} else {
					exito = Boolean.FALSE;
				}
			}
			if (destinatarios != null && destinatarios.size() > 0) {
				if (!esNotificacion && adjuntaCotizacion) {
					CotizacionDTO cotizacionDTO = null;
					if (detalleRenovacion != null) {
						cotizacionDTO = detalleRenovacion.getCotizacionDTO();
					} else {
						cotizacionDTO = this
								.getCotizacionDeRenovacion(polizaDTO
										.getIdToPoliza());
					}
					if (cotizacionDTO != null) {
						List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
						String nombreAdjunto =UtileriasWeb.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"poliza.renovacion.mail.nombre.archivoAdjunto", polizaDTO
										.getNumeroPolizaFormateada());
						
						adjuntos.add(CotizacionDN.getInstancia(usuarioCreacion)
								.obtenerCotizacionEnPDF(
										cotizacionDTO.getIdToCotizacion(),
										nombreAdjunto));

						MailAction.enviaCorreoConTemplate(destinatarios, this
								.getAsuntoCorreo(), this.getBodyCorreo(), this
								.getDestinatarioNombre(), adjuntos);

					} else {
						MailAction.enviaCorreoConTemplate(destinatarios, this
								.getAsuntoCorreo(), this.getBodyCorreo(), this
								.getDestinatarioNombre(), null);
					}
				}else{
					MailAction.enviaCorreoConTemplate(destinatarios, this
							.getAsuntoCorreo(), this.getBodyCorreo(), this
							.getDestinatarioNombre(),null);					
				}
				try {
					this.agregarDetalleSeguimiento(polizaDTO);
				} catch (SystemException e) {
					mesajesError
							.put(
									polizaDTO
											.getNumeroPolizaFormateada(),
									"Se envi� el correo electr�nico, pero no se pudo guardar el movimiento en la bitacora.");
				}
			} else {
				mesajesError
						.put(polizaDTO.getNumeroPolizaFormateada(),
								"Error al obtener el correo electr�nico especificado");
			}
		}
		return exito;
	}
	public CotizacionDTO getCotizacionDeRenovacion(BigDecimal idToPoliza){
		CotizacionDTO cotizacionDTO = null;
		RenovacionPolizaDTO renovacionPolizaDTO = new RenovacionPolizaDTO();
		renovacionPolizaDTO.setPolizaDTO(new PolizaDTO());
		renovacionPolizaDTO.getPolizaDTO().setIdToPoliza(idToPoliza);		
		List<RenovacionPolizaDTO> renovacion;
		try {
			renovacion = RenovacionPolizaDN.getInstancia().buscarFiltrado(renovacionPolizaDTO, Boolean.FALSE);
			if(renovacion != null && renovacion.size() > 0)
				cotizacionDTO = renovacion.get(0).getCotizacionDTO();			
		} catch (SystemException e) {}
		
		return cotizacionDTO;
			
	}
	public boolean esPrimeraNotificacion(BigDecimal idToPoliza){
		boolean esPrimeraNotificacion =Boolean.TRUE;	
		try {
			RenovacionPolizaDTO renovacionPolizaDTO = RenovacionPolizaDN.getInstancia().buscarDetalleRenovacionPoliza(idToPoliza);
			if(renovacionPolizaDTO != null ){
				for(SeguimientoRenovacionDTO seguimiento: renovacionPolizaDTO.getSeguimientoRenovacion()){
					if (seguimiento.getTipoMovimiento().intValue() == Sistema.TIPO_ENVIO_EMAIL.intValue()){
						esPrimeraNotificacion = Boolean.FALSE;
						break;
					}
				}
			}
		} catch (SystemException e) {
			esPrimeraNotificacion =Boolean.FALSE;	
		}
		return esPrimeraNotificacion;
	}
	public void agregarDetalleSeguimiento(PolizaDTO polizaDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		if (polizaDTO != null) {
			SeguimientoRenovacionDTO seguimientoRenovacionDTO = new SeguimientoRenovacionDTO();
			seguimientoRenovacionDTO.setDescripcionMovimiento(this
						.getDetalleNotificacion());				
			seguimientoRenovacionDTO.setFechaMovimiento(new Date());
			seguimientoRenovacionDTO
					.setTipoMovimiento(this.getTipoMovimiento());			
			new RenovacionPolizaSN().agregarDetalleSeguimiento(polizaDTO
					.getIdToPoliza(), seguimientoRenovacionDTO);
		}
	}

	private String asignarDestinatarios(String tipoDestinario,
			PolizaDTO polizaDTO) throws SystemException {
		String destinatario = null;
		RenovacionPolizaDTO detalleRenovacion = null;
		try {
			detalleRenovacion = this.buscarDetalleRenovacionPoliza(polizaDTO.getIdToPoliza());
		} catch (Exception e1) {}	
		if (tipoDestinario.equals(Sistema.DESTINATARIO_CORREO_EJECUTIVO_VENTAS)
				|| tipoDestinario.equals(Sistema.DESTINATARIO_CORREO_AGENTE)) {
			AgenteDTO agenteDTO = new AgenteDTO();
			agenteDTO.setIdTcAgente(polizaDTO.getCotizacionDTO()
					.getSolicitudDTO().getCodigoAgente().intValue());
			AgenteDN agenteDN = AgenteDN.getInstancia();
			agenteDTO = agenteDN.verDetalleAgente(agenteDTO, polizaDTO
					.getCodigoUsuarioCreacion());				
			if (tipoDestinario.equals(Sistema.DESTINATARIO_CORREO_AGENTE)) {
				if (agenteDTO != null && agenteDTO.getEmail() != null) {
					destinatario = agenteDTO.getEmail();					
					this.setDestinatarioNombre(agenteDTO.getNombre());
					this.setBodyCorreo(UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,
							"poliza.renovacion.mail.body",
							polizaDTO.getNumeroPolizaFormateada(),
							detalleRenovacion != null?detalleRenovacion.getPolizaDTO().getNombreAsegurado():polizaDTO.getNombreAsegurado(),
							this.getDiasVencimiento(),
							detalleRenovacion != null? detalleRenovacion.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud().toString(): "N/D"));					
					this.setDetalleNotificacion(UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,
							"poliza.renovacion.detalle.movimiento.email",
							destinatario, "Agente", agenteDTO.getNombre()));
				}
			} else {
				if (agenteDTO != null && agenteDTO.getEmailOficina() != null) {
					destinatario = agenteDTO.getEmailOficina();
					this.setDestinatarioNombre(agenteDTO.getNombreOficina());
					this.setBodyCorreo(UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,
							"poliza.renovacion.mail.body",
							polizaDTO.getNumeroPolizaFormateada(),
							detalleRenovacion != null?detalleRenovacion.getPolizaDTO().getNombreAsegurado():polizaDTO.getNombreAsegurado(),
							this.getDiasVencimiento(),
							detalleRenovacion != null? detalleRenovacion.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud().toString(): "N/D"));					
					this.setDetalleNotificacion(UtileriasWeb.getMensajeRecurso(
							Sistema.ARCHIVO_RECURSOS,
							"poliza.renovacion.detalle.movimiento.email",
							destinatario, "Ejecutivo de Ventas", agenteDTO
									.getNombreOficina()));
				}
			}
		} else if (tipoDestinario.equals(Sistema.DESTINATARIO_CORREO_CLIENTE)) {
			ClienteDTO asegurado = null;
			if(polizaDTO.getCotizacionDTO().getIdDomicilioAsegurado() != null && polizaDTO.getCotizacionDTO().getIdDomicilioAsegurado().intValue() > 0){
				asegurado = new ClienteDTO();
				asegurado.setIdCliente(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado());
				asegurado.setIdDomicilio(polizaDTO.getCotizacionDTO().getIdDomicilioAsegurado());
				asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, polizaDTO.getCotizacionDTO().getCodigoUsuarioCotizacion());
			}else{
				asegurado = ClienteDN.getInstancia().verDetalleCliente(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado(), polizaDTO.getCotizacionDTO().getCodigoUsuarioCotizacion());
			}				

			if (asegurado != null && asegurado.getEmail() != null) {
				destinatario = asegurado.getEmail();
				this.setDestinatarioNombre(asegurado.getNombre()+ " " + asegurado.getApellidoPaterno());
				this.setBodyCorreo(UtileriasWeb.getMensajeRecurso(
						Sistema.ARCHIVO_RECURSOS,
						"poliza.renovacion.mail.body",
						polizaDTO.getNumeroPolizaFormateada(),
						detalleRenovacion != null?detalleRenovacion.getPolizaDTO().getNombreAsegurado():polizaDTO.getNombreAsegurado(),
						this.getDiasVencimiento(),
						detalleRenovacion != null? detalleRenovacion.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud().toString(): "N/D"));
				this.setDetalleNotificacion(UtileriasWeb.getMensajeRecurso(
						Sistema.ARCHIVO_RECURSOS,
						"poliza.renovacion.detalle.movimiento.email",
						destinatario, "Cliente", asegurado.getNombre() + " "
								+ (asegurado.getClaveTipoPersona().intValue() == 1 ? asegurado.getApellidoPaterno(): "")));
			}
		}
		return destinatario;
	}

	/**
	 * Se procesa el arreglo de polizas separadas por pipe | se generan las
	 * copias de cotizacion y se asignan a el usuarioAsignacion.
	 * 
	 * @param polizasSplit
	 *            : Arreglo de polizas separados por pipe |
	 * @param usuarioAsigacion
	 *            : Nombre de usuario al que se leasignaran las polizas
	 * @param mesajesError
	 *            : Mapa con la llave que corresponde al numero de poliza y el
	 *            valor que corresponde al mensaje generado por el proceso.
	 * @return boolean: true si se completo con exito la transaccion, false si
	 *         ocurrio un error en la transaccion, y se llena el mapa de
	 *         mensajes de error
	 * @throws SystemException
	 */
	public boolean procesaRenovacionPoliza(String[] polizasSplit,
			String usuarioCreacion, String usuarioAsigacion,
			Map<String, String> mesajesError) throws SystemException {
		boolean exito = Boolean.TRUE;
		if (mesajesError == null)
			mesajesError = new HashMap<String, String>();

		if (polizasSplit != null && polizasSplit.length > 0) {
			RenovacionPolizaSN renovacionPolizaSN = new RenovacionPolizaSN();
			for (String poliza : polizasSplit) {
				BigDecimal idToPoliza = null;
				try {
					idToPoliza = UtileriasWeb.regresaBigDecimal(poliza);
				} catch (SystemException e) {
					mesajesError.put("N/D", RenovacionPolizaDN.ERROR_AL_PARSEAR_POLIZA);
					exito = Boolean.FALSE;
				}
				Map<String, String> estatusProceso = renovacionPolizaSN
						.procesaRenovacionPoliza(idToPoliza, usuarioCreacion,
								usuarioAsigacion);
				if (estatusProceso != null && estatusProceso.size() > 0) {
					String exitoso = estatusProceso.get("icono");
					if (!exitoso.equals(Sistema.EXITO)) {
						mesajesError.put(estatusProceso.get("numeroPoliza"),
								RenovacionPolizaDN.ERROR_AL_COPIAR_COTIZACION);
						exito = Boolean.FALSE;
					}
				}
			}
		}
		return exito;
	}

	public List<PolizaDTO> buscarFiltrado(PolizaDTO polizaDTO,
			boolean esRenovacion, boolean esReporte) throws SystemException {
		try {
			return new RenovacionPolizaSN().buscarFiltrado(polizaDTO,
					esRenovacion, esReporte);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RenovacionPolizaDTO> buscarFiltrado(
			RenovacionPolizaDTO renovacionPolizaDTO, boolean esNotificacion)
			throws SystemException {
		try {
			return new RenovacionPolizaSN().buscarFiltrado(renovacionPolizaDTO,
					esNotificacion);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	public RenovacionPolizaDTO buscarDetalleRenovacionPoliza(
			BigDecimal idToPoliza) throws ExcepcionDeAccesoADatos,
			SystemException {
		try {
			RenovacionPolizaDTO renovacion = new RenovacionPolizaSN()
			.buscarDetalleRenovacionPoliza(idToPoliza);
			if(renovacion == null)
				return renovacion;
			this.setNombreAseguradoProducto(renovacion.getPolizaDTO(), renovacion.getCodigoUsuarioCreacion());
			return renovacion;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<RenovacionPolizaDTO> buscarRenovacionPolizaPorPropiedad(
			String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos,
			SystemException {
		try {
			return new RenovacionPolizaSN()
					.buscarRenovacionPolizaPorPropiedad(nombrePropiedad, valor);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}	
	
	public RenovacionPolizaDTO buscarRenovacionPolizaPorCotizacion(
			BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos,
			SystemException {
		try {
			RenovacionPolizaDTO renovacionPolizaDTO = null;
			List<RenovacionPolizaDTO> renovaciones = new RenovacionPolizaSN()
					.buscarRenovacionPolizaPorPropiedad(
							"cotizacionDTO.idToCotizacion", idToCotizacion);
			if (renovaciones != null && renovaciones.size() > 0)
				renovacionPolizaDTO = renovaciones.get(0);
			return renovacionPolizaDTO;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}

	}
	public boolean tieneSieniestros(BigDecimal idToPoliza, String nombreUsuario){
		boolean tieneSiniestros = false;
		List<ReporteSiniestroDTO> reportes;
		try {
			reportes = ReporteSiniestroDN.getInstancia(nombreUsuario).listarFiltradoPorNumPoliza(idToPoliza.toString());
			if(reportes!= null && reportes.size()>0)
				tieneSiniestros = true;
		} catch (ExcepcionDeAccesoADatos e) {
		} catch (SystemException e) {
		}
		return tieneSiniestros;
	}
	public void setNombreAseguradoProducto(PolizaDTO poliza, String nombreUsuario){
		ClienteDTO asegurado = null;
		String cliente = "No disponible";
		ProductoDTO productoDTO = null;		
		CotizacionDTO cotizacion = poliza.getCotizacionDTO();
		if (cotizacion.getIdToPersonaAsegurado() != null) {
			try {
				if(cotizacion.getIdDomicilioAsegurado() != null && cotizacion.getIdDomicilioAsegurado().intValue() > 0){
					asegurado = new ClienteDTO();
					asegurado.setIdCliente(cotizacion.getIdToPersonaAsegurado());
					asegurado.setIdDomicilio(cotizacion.getIdDomicilioAsegurado());
					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, cotizacion.getCodigoUsuarioCotizacion());
				}else{
					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacion.getIdToPersonaAsegurado(), cotizacion.getCodigoUsuarioCotizacion());
				}					
			} catch (Exception exc) {
			}
			if (asegurado != null) {
				if (asegurado.getClaveTipoPersona() == 1)
					cliente = asegurado.getNombre() + " "
							+ asegurado.getApellidoPaterno() + " "
							+ asegurado.getApellidoMaterno();
				else
					cliente = asegurado.getNombre();
			}
		}
		cotizacion.setDiasPorDevengar(UtileriasWeb.obtenerDiasEntreFechas(
				cotizacion.getFechaInicioVigencia(), cotizacion
						.getFechaFinVigencia()));
		poliza.setNombreAsegurado(cliente);
		ProductoDN productoDN = ProductoDN.getInstancia();
		try {
			productoDTO = productoDN.encontrarPorSolicitud(poliza
					.getIdToSolicitud());
		} catch (Exception exc ) {}
		if (productoDTO != null) {
			poliza.setNombreProducto(productoDTO.getNombreComercial());
		}		
		try{
			EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario)
					.getUltimoEndoso(poliza.getIdToPoliza());
			cotizacion.setPrimaNetaCotizacion(CotizacionDN.getInstancia(
					nombreUsuario).getPrimaNetaCotizacion(
					ultimoEndoso.getIdToCotizacion()));
			int contratos = SlipDN.getInstancia().obtenerCantidadSlipsPorCotizacion(ultimoEndoso.getIdToCotizacion());
			if(contratos > 0){
				System.out.println(poliza.getNumeroPolizaFormateada());
			}
			poliza.setEsFacultativa(contratos > 0 ? "SI":"NO");
		} catch (Exception exc) {
		}
		
	}
	
	public String getBodyCorreo() {
		return bodyCorreo;
	}

	public void setBodyCorreo(String bodyCorreo) {
		this.bodyCorreo = bodyCorreo;
	}

	public String getAsuntoCorreo() {
		return asuntoCorreo;
	}

	public void setAsuntoCorreo(String asuntoCorreo) {
		this.asuntoCorreo = asuntoCorreo;
	}

	public String getDetalleNotificacion() {
		return detalleNotificacion;
	}

	public void setDetalleNotificacion(String detalleNotificacion) {
		this.detalleNotificacion = detalleNotificacion;
	}

	public BigDecimal getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(BigDecimal tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getDestinatarioNombre() {
		return destinatarioNombre;
	}

	public void setDestinatarioNombre(String destinatarioNombre) {
		this.destinatarioNombre = destinatarioNombre;
	}

	public String getDiasVencimiento() {
		return diasVencimiento;
	}

	public void setDiasVencimiento(String diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}
}
