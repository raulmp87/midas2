/**
 * Clase tipo wrapper para implementar la Cancelaci�n de P�liza usando Cancelaci�n de Endosos (CE)
 */

package mx.com.afirme.midas.poliza.cancelacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Status;
import javax.transaction.UserTransaction;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDN;
import mx.com.afirme.midas.cotizacion.endoso.movimiento.MovimientoCotizacionEndosoDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoDN;
import mx.com.afirme.midas.endoso.cancelacion.CancelacionEndosoSN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

import org.quartz.SchedulerException;
import org.quartz.ee.jta.UserTransactionHelper;

public class CancelacionPolizaDN {

private static final CancelacionPolizaDN INSTANCIA = new CancelacionPolizaDN();
	
	private final int CANCELACION_FALLIDA = 0;
	private final int CANCELACION_EXITOSA = 1;

	private EndosoIDTO endosoCeroCancelable;
	private String nombreUsuario;
	private Map<String, String> mensaje = null;
	private Date fechaInicioVigencia;
	private BigDecimal idCotizacionCancelacion;
	
	public static CancelacionPolizaDN getInstancia() {
		return CancelacionPolizaDN.INSTANCIA;
	}
	
	/**
	 * Ejecuta la cancelaci�n manual de la p�liza y sus endosos cancelables
	 * @param cotizacionCancelacion Cotizaci�n original del endoso de cancelaci�n de la p�liza
	 * @param nombreUsuario Clave del usuario que realiza la cancelaci�n
	 * @return Un mensaje con el resultado del proceso de cancelaci�n
	 */
	public Map<String, String> cancelacionManualPolizaEndoso(CotizacionDTO cotizacionCancelacion, String nombreUsuario) {

			mensaje  = null;
			this.nombreUsuario = nombreUsuario;
			
//			Se guarda la fecha de inicio de vigencia de la cotizacion original de la cancelaci�n de la p�liza
			this.fechaInicioVigencia = cotizacionCancelacion.getFechaInicioVigencia();
			
//			Se guarda el id de la cotizacion original de la cancelaci�n de la p�liza
			this.idCotizacionCancelacion = cotizacionCancelacion.getIdToCotizacion();
			
//			Se llama al metodo de cancelacion de poliza con una bandera indicando que es cancelaci�n manual
			int resultado = cancelacionPolizaEndosos(cotizacionCancelacion.getSolicitudDTO().getIdToPolizaEndosada(), false, cotizacionCancelacion.getClaveMotivoEndoso());
			
//			Si el metodo regreso el estatus de fallido:
			if (resultado == CANCELACION_FALLIDA) {
//				Se obtiene el mensaje de error y lo regresa
				if (mensaje == null) {
					mensaje  = new HashMap<String, String>();
					mensaje.put("icono", "10");//Error
					mensaje.put("mensaje", "Error al cancelar la P\u00f3liza. </br>");
				}
			}
		
		return mensaje;
	}
	
	
	
	
	/**
	 * Ejecuta la cancelaci�n autom�tica de la p�liza y sus endosos cancelables
	 * @param endosoCeroCancelable El endoso 0 (p�liza) a cancelar
	 * @throws Exception Si la cancelaci�n de p�liza o la de alguno de sus endosos falla
	 */
	public void cancelacionAutomaticaPolizaEndoso(EndosoIDTO endosoCeroCancelable) throws Exception {
				
		this.nombreUsuario = Sistema.USUARIO_SISTEMA;
		this.endosoCeroCancelable = obtieneEndosoParaCancelacionPoliza(endosoCeroCancelable);
		
//		Se guarda la fecha de inicio de vigencia del endoso cancelable		
		this.fechaInicioVigencia = this.endosoCeroCancelable.getFechaInicioVigencia();
		
//		Se llama al metodo de cancelacion de poliza con una bandera indicando que es cancelaci�n autom�tica
		int resultado = cancelacionPolizaEndosos(this.endosoCeroCancelable.getIdPoliza(), true, (short)Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO);
		
//		Si el metodo regreso el estatus de fallido:
		if (resultado == CANCELACION_FALLIDA) {
//			Se manda una excepcion de que la cancelaci�n de p�liza fall�	
			throw new Exception("La cancelacion de la poliza con endosos fallo...");
		}		
		
	}
	
	
	
	
	/**
	 * Realiza la cancelaci�n de la p�liza y de todos sus endosos cancelables
	 * @param idPoliza Id de la p�liza a cancelar junto a sus endosos
	 * @param esCancelacionAutomatica Indica si la cancelaci�n es invocada mediente el proceso de cancelaci�n aut�matica o es
	 * cancelaci�n manual (Solicitada por el usuario)
	 * @return Un n�mero que indica si la cancelaci�n fu� exitosa (CANCELACION_EXITOSA) o si fall� (CANCELACION_FALLIDA)
	 */
	@SuppressWarnings("finally")
	private int cancelacionPolizaEndosos (BigDecimal idPoliza, boolean esCancelacionAutomatica, short motivo) {
		
		int respuesta = CANCELACION_FALLIDA;
		Short numeroEndoso;
		try {
			
//		Se obtiene una lista con todos los endosos cancelables de la poliza (ver logica CE)
			List<EndosoDTO> listaEndososPoliza = CancelacionEndosoDN.getInstancia().listarEndososCancelablesPoliza(idPoliza);
		
//		Se cancela cada uno de los endosos de la lista
			for (EndosoDTO endoso : listaEndososPoliza) {
				
				//Se convierte a negativo para indicarle a la cancelaci�n de endoso que se trata de una cancelaci�n a partir de una p�liza
				numeroEndoso = Short.parseShort(endoso.getId().getNumeroEndoso().intValue() * -1 + "");
				
				CancelacionEndosoDN.getInstancia().cancelarEndoso(endoso.getId().getIdToPoliza(), 
						numeroEndoso, this.fechaInicioVigencia, 
						nombreUsuario);
				
			}

			
//			Si se trata del endoso 0 (poliza)
//			Si se trata de una cancelaci�n autom�tica			
			if (esCancelacionAutomatica) {
//				Se llama al metodo de cancelacion de poliza autom�tica
				EndosoDN.getInstancia(nombreUsuario).emiteEndosoCancelacionAutomatica(endosoCeroCancelable);
			} else {
				UserTransaction ut;
				EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(idPoliza);
				
				try {
					ut = UserTransactionHelper.lookupUserTransaction();
					ut.setTransactionTimeout(Sistema.TIEMPO_LIMITE_TX_EMISION_CE_RE);
				} catch (SchedulerException e1) {
					e1.printStackTrace();
					throw new SystemException("fallo al crear la transaccion");
				}
				
				try {
					ut.begin();
					
//					Se obtiene una copia de la cotizacion del ultimo endoso emitido (El que qued� similar a la cotizaci�n original de la p�liza)
					CotizacionDTO cotizacionCancelacionSinEndosos = new CancelacionEndosoSN().copiaCotizacion(ultimoEndoso, motivo, nombreUsuario);
					
//					Se establece en la cotizacion del ultimo endoso el tipo de cancelacion de poliza 
//					(para que al emitir se entienda que es una cancelacion de poliza)
					cotizacionCancelacionSinEndosos.getSolicitudDTO().setClaveTipoEndoso(new BigDecimal(Sistema.SOLICITUD_ENDOSO_DE_CANCELACION));
									
//					Se actualiza la fecha de inicio de vigencia con la original capturada por el usuario
					cotizacionCancelacionSinEndosos.setFechaInicioVigencia(this.fechaInicioVigencia);
					
//					Se copian los movimientos de la cotizacion original a la cotizacion del ultimo endoso emitido
					MovimientoCotizacionEndosoDTO movimiento;
					MovimientoCotizacionEndosoDN movCotDN = MovimientoCotizacionEndosoDN.getInstancia(nombreUsuario);
					MovimientoCotizacionEndosoDTO filtro = new MovimientoCotizacionEndosoDTO();
					filtro.setIdToCotizacion(this.idCotizacionCancelacion);
					
					List<MovimientoCotizacionEndosoDTO> movimientosCotOriginal = movCotDN.listarFiltrado(filtro);
					
					for (MovimientoCotizacionEndosoDTO movimientoCotOriginal : movimientosCotOriginal) {
						movimiento = movCotDN.copiaMovimiento(movimientoCotOriginal, "");
						movimiento.setIdToCotizacion(cotizacionCancelacionSinEndosos.getIdToCotizacion());
						movCotDN.agregar(movimiento);
					}
					
//					Se hace commit sobre la copia exitosa de la cotizacion
					ut.commit();
					
					
//					Se llama al metodo de cancelacion de poliza manual
					mensaje = EndosoDN.getInstancia(nombreUsuario).emitirEndoso(cotizacionCancelacionSinEndosos);

					
				} catch(Exception e) {
					e.printStackTrace();
					if (ut.getStatus() != Status.STATUS_NO_TRANSACTION) {
						ut.rollback();
					}
					throw new Exception("Excepcion en la transaccion para copiar la cotizacion de cancelacion de poliza...");
				}
			}
			
			respuesta = CANCELACION_EXITOSA;
			
		} catch (Exception e) {
			e.printStackTrace();
			respuesta = CANCELACION_FALLIDA;
		} finally {
			return respuesta;
		}
		
	}

	/**
	 * Obtiene el endoso para ser cancelado usando la cancelaci�n de p�liza
	 * @param endosoOriginal El endoso de la p�liza original (Endoso 0)
	 * @return El ultimo endoso de CFP si existe alguno o el endoso original en caso contrario
	 * @throws SystemException 
	 */
	private EndosoIDTO obtieneEndosoParaCancelacionPoliza(EndosoIDTO endosoOriginal) throws SystemException {
		
		Short numeroEndoso = EndosoDN.getInstancia(nombreUsuario).buscarNumeroEndosoUltimoCFP(endosoOriginal.getIdPoliza());
			
		//S�lo se cambia el n�mero de endoso, que es lo �nico que 
		//necesita la cancelaci�n autom�tica de p�liza de este objeto
		endosoOriginal.setNumeroEndoso(numeroEndoso);
		
		return endosoOriginal;
		
		
		
	}
	
	
}
