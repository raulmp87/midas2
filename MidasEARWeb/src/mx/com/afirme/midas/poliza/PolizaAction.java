package mx.com.afirme.midas.poliza;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDN;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDN;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * MyEclipse Struts Creation date: 16-11-2009
 * 
 */
public class PolizaAction extends MidasMappingDispatchAction {
    
    
    	private static final short SIN_ESTATUS_DEFINIDO=2;
    	private static final String SIN_ESTATUS_DEFINIDO_STR="2";
	/**
	 *Busca todas las polizas existentes en la BD y retorna a la pagina de listar,
	 *si no ocurre ningun problema.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		return listarPolizaFiltrado(mapping, form, request, response);
	}
	
	/**
	 * Busca todas las polizas de la BD, y las setea en el <code>request</code>.
	 * @param request
	 * @throws SystemException si falla al llamar al metodo <code>listarTodos()</code> de <code>polizaDN</code>
	 */
	private void listarTodos(HttpServletRequest request) throws SystemException {
		PolizaDN polizaDN = PolizaDN.getInstancia();
		List<PolizaDTO> polizas = polizaDN.listarTodos();
		request.setAttribute("polizas", polizas);
	}
	
	/**
	 * Buscar las polizas a paratir de un filtro, dado por el usuario y 
	 * retorna a la pagina listar, si no ocurren errores.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward listarPolizaFiltrado(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			PolizaForm polizaForm = (PolizaForm) form;
			PolizaDTO polizaDTO = new PolizaDTO();
			poblarDTO(polizaForm, polizaDTO);
			
			Long totalRegistros = PolizaDN.getInstancia().obtenerTotalFiltrado(polizaDTO,usuario);
			
			polizaForm.setTotalRegistros(totalRegistros.toString()); 
			
			return listarFiltradoPaginado(mapping, form, request, response);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
		
	}
	
	public ActionForward listarFiltradoPaginado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			Usuario usuario = getUsuarioMidas(request);
			if (!listaEnCache(form, request)) {
				PolizaForm polizaForm = (PolizaForm) form;
				PolizaDTO polizaDTO = new PolizaDTO();
				poblarDTO(polizaForm, polizaDTO);
				
				polizaDTO.setPrimerRegistroACargar(polizaForm.getPrimerRegistroACargar());
				polizaDTO.setNumeroMaximoRegistrosACargar(polizaForm.getNumeroMaximoRegistrosACargar());
				polizaForm.setListaPaginada(PolizaDN.getInstancia().buscarFiltrado(polizaDTO,usuario), request);
			
			} 
	
			request.setAttribute("polizas", obtieneListaAMostrar(form, request));
		
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
		
	}
	
	private void poblarDTO(PolizaForm polizaForm, PolizaDTO polizaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(polizaForm.getNumeroPoliza()))
			polizaDTO.setNumeroPoliza(Integer.valueOf(polizaForm.getNumeroPoliza()));
		if (!UtileriasWeb.esCadenaVacia(polizaForm.getNombreAsegurado())){
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setNombreAsegurado(polizaForm.getNombreAsegurado());
		}
		if (!UtileriasWeb.esCadenaVacia(polizaForm.getFechaCreacion())) {
			String fecha[] = polizaForm.getFechaCreacion().split("/");
			GregorianCalendar gc = new GregorianCalendar(Integer.valueOf(fecha[2]), Integer.valueOf(fecha[1])-1 ,Integer.valueOf(fecha[0]));
			polizaDTO.setFechaCreacion(gc.getTime());
		}
	}
	/**
	 * Si en el <code>request</code> viene una clave de estatus valida
	 * las busca por este ultimo, sino devuelve todas las polizas
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listarPorEstatus(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		short estatusPoliza = SIN_ESTATUS_DEFINIDO;
		try {
			String par = request.getParameter("cveEstatus");
			if (par != null && !par.equals(SIN_ESTATUS_DEFINIDO_STR)) {
				estatusPoliza = Short.valueOf(par.trim());
			}
			if (estatusPoliza != SIN_ESTATUS_DEFINIDO) {
				listarPorEstatus(request, estatusPoliza);
			} else {
				listarTodos(request);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
					request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Busca las polizas filtrando por <code>cveEstatus</code>.
	 * @param request
	 * @param cveEstatus
	 * @throws SystemException si al invocar el metodo <code>listarPorEstatus()</code> de la clase 
	 * <code>PolizaDN</code> ocurre un error.
	 */
	private void listarPorEstatus(HttpServletRequest request, short cveEstatus) throws SystemException {
		PolizaDN polizaDN = PolizaDN.getInstancia();
		List<PolizaDTO> polizas = polizaDN.listarPorEstatus(cveEstatus);
		request.removeAttribute("polizas");
		request.setAttribute("polizas", polizas);
	}
	
	/**
	 * Dado un numero de poliza formateado, busca los datos en la tabla de
	 * polizas.Metodo invocado por ajax.
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 */
	public void buscarPolizaPorAjax(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String numPoliza = request.getParameter("numPol");
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		StringBuffer buffer = new StringBuffer();
		PolizaDTO polizaDTO = null;
		PolizaDN polizaDN = PolizaDN.getInstancia();
		List<PolizaDTO> polizas = null;
		String cveEstatusStr = request.getParameter("cveEstatus");
		String esSolicitudEndoso = request.getParameter("esSolicitudEndoso");
		String esRenovacion = request.getParameter("esRenovacion");
		try {
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response>");
			if (!UtileriasWeb.esCadenaVacia(numPoliza)) {
				if (cveEstatusStr == null || cveEstatusStr.equals(SIN_ESTATUS_DEFINIDO_STR)) {
					// El numero de poliza lo busca en todas
					polizas = polizaDN.listarPorNumPolizaFormato(numPoliza);
					if (polizas != null && !polizas.isEmpty()) {
						polizaDTO = polizas.get(0);
					}
				} else {
					// La busca dentro de las polizas, vigentes o canceladas
					// segun el estatus
					polizas = polizaDN.listarPorEstatus(Short.valueOf(cveEstatusStr));
					if (polizas != null && !polizas.isEmpty()) {
						String[] numeroDePolizaArray = numPoliza.split("-");
						String codigoProducto = numeroDePolizaArray[0].substring(0, 2);
						String codigoTipoPoliza = numeroDePolizaArray[0].substring(2, 4);
						int numeroPoliza = Integer.valueOf(numeroDePolizaArray[1]);
						int numeroRenovacion = Integer.valueOf(numeroDePolizaArray[2]);
						for (PolizaDTO poliza : polizas) {
							if (poliza.getCodigoProducto().equals(codigoProducto)
									&& poliza.getCodigoTipoPoliza().equals(codigoTipoPoliza)
									&& poliza.getNumeroPoliza() == numeroPoliza
									&& poliza.getNumeroRenovacion() == numeroRenovacion) {
								polizaDTO = poliza;
							}
						}
					}
				}
			}
			if (polizaDTO != null) {
				boolean permiteCopia = true;
				Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
				
				if (AgenteEspecialDN.getInstancia().esPolizaDeOtroAgente(usuario, polizaDTO)) {
					buffer.append("<item>");
					buffer.append("<mensaje>" +
							"Favor de acudir a Seguros Afirme, S.A. a solicitar este tr�mite ya  " +
							"que el n�mero de p�liza proporcionado pertenece a otro agente." +
							"</mensaje>");
					buffer.append("</item>");
					permiteCopia = false;	
				} else if(esSolicitudEndoso != null && esSolicitudEndoso.equals("false")) {
					if(esRenovacion != null && esRenovacion.equals("1")){//Renovacion de Poliza
						if(polizaDTO.getClaveEstatus().shortValue() == Sistema.ESTATUS_CANCELADA){
							buffer.append("<item>");
							buffer.append("<mensaje>" +
									"No se puede crear la solicitud de renovaci�n a partir de esta p�liza, " +
									"ya que la p�liza se encuentra cancelada." +
									"</mensaje>");
							buffer.append("</item>");
							permiteCopia = false;						
						}
					}else{
						IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
						BigDecimal ultimoInciso = incisoCotizacionDN.obtenerNumeroIncisoSiguiente(polizaDTO.getCotizacionDTO().getIdToCotizacion()).subtract(BigDecimal.ONE);
						int incisos = incisoCotizacionDN.obtenerCantidadIncisosPorCotizacion(polizaDTO.getCotizacionDTO().getIdToCotizacion());
						if(incisos != ultimoInciso.intValue()) {
							buffer.append("<item>");
							buffer.append("<mensaje>" +
									"No se puede crear la solicitud a partir de esta p�liza, " +
									"ya que los n�meros de sus incisos son discontinuos." +
									"</mensaje>");
							buffer.append("</item>");
							permiteCopia = false;
						}						
					}
				}
				if(permiteCopia) {
					String fechaInicioPoliza = "1";
					String fechaFinPoliza = "1";
					BigDecimal codigoAgente = BigDecimal.ZERO;
					CotizacionDTO cotizacionDTO = null;
					EndosoId id = new EndosoId();
					id.setIdToPoliza(polizaDTO.getIdToPoliza());
					id.setNumeroEndoso((short) 0);
					EndosoDTO endoso = EndosoDN.getInstancia(usuario.getNombreUsuario()).getPorId(id);
					if (endoso != null) {
						Calendar cal = Calendar.getInstance();
						cal.setTime(endoso.getFechaInicioVigencia());
						fechaInicioPoliza = formatoFecha.format(cal.getTime());
						cal.setTime(endoso.getFechaFinVigencia());
						fechaFinPoliza = formatoFecha.format(cal.getTime());
						cotizacionDTO = CotizacionDN.getInstancia("").getPorId(endoso.getIdToCotizacion());
						if (cotizacionDTO != null) {
							codigoAgente = cotizacionDTO.getSolicitudDTO().getCodigoAgente();
						}
					}
					buffer.append("<item>");
					buffer.append("<idPol>" + polizaDTO.getIdToPoliza() + "</idPol>");
					buffer.append("<descProd>"
							+ polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getNombreComercial()
							+ "</descProd>");
					buffer.append("<idToProducto>"
							+ polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto().intValue()
							+ "</idToProducto>");
					buffer.append("<fecha1>" + fechaInicioPoliza + "</fecha1>");
					buffer.append("<fecha2>" + fechaFinPoliza + "</fecha2>");
					buffer.append("<estatus>" + polizaDTO.getClaveEstatus().toString() + "</estatus>");
					buffer.append("<codAgente>" + codigoAgente + "</codAgente>");
					buffer.append("</item>");
				}
			}
			buffer.append("</response>");
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
		}
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Devuelve todos los endosos que presenta una poliza
	 * a partir de un id de poliza.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listarEndososPorPoliza(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idPolizaString = request.getParameter("idToPoliza");
		String accion = request.getParameter("accion");
		PolizaForm polizaForm = (PolizaForm) form;
		if (!UtileriasWeb.esCadenaVacia(idPolizaString)){
			try {
				BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPolizaString);
				List<EndosoDTO> listaEndososPoliza = EndosoDN.getInstancia("").listarEndososPorPoliza(idToPoliza, Boolean.TRUE);
				if (listaEndososPoliza == null)
					listaEndososPoliza = new ArrayList<EndosoDTO>();
				polizaForm.setListaEndosos(listaEndososPoliza);
				polizaForm.setNumeroPoliza(idPolizaString);
				polizaForm.setAccion(accion);
			} catch (SystemException e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (ExcepcionDeAccesoADatos e) {
				reglaNavegacion = Sistema.NO_EXITOSO;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void listarPolizasJson(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, SystemException {
		short estatusPoliza = SIN_ESTATUS_DEFINIDO;
		String claveEstatus = request.getParameter("claveEstatus");
		String esSolicitudPoliza = request.getParameter("esSolicitudPoliza");
		if (claveEstatus != null && !claveEstatus.equals(SIN_ESTATUS_DEFINIDO_STR)) {
			estatusPoliza = Short.parseShort(claveEstatus.trim());
		}
		List<PolizaDTO> polizas = new ArrayList<PolizaDTO>();
		if (estatusPoliza != SIN_ESTATUS_DEFINIDO) {
			polizas = PolizaDN.getInstancia().listarPorEstatus(estatusPoliza);
		} else {
			polizas = PolizaDN.getInstancia().listarTodos();
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		MidasJsonBase json = new MidasJsonBase();
		for(PolizaDTO poliza : polizas) {
			String permiteCopia = "true";
			if(esSolicitudPoliza != null && esSolicitudPoliza.equals("true")) {
				IncisoCotizacionDN incisoCotizacionDN = IncisoCotizacionDN.getInstancia();
				BigDecimal ultimoInciso = incisoCotizacionDN.obtenerNumeroIncisoSiguiente(poliza.getCotizacionDTO().getIdToCotizacion()).subtract(BigDecimal.ONE);
				int incisos = incisoCotizacionDN.obtenerCantidadIncisosPorCotizacion(poliza.getCotizacionDTO().getIdToCotizacion());
				if(incisos != ultimoInciso.intValue()) {
					permiteCopia = "false";
				}
			}
			MidasJsonRow row = new MidasJsonRow();
			row.setId(poliza.getIdToPoliza().toString());
			row.setDatos(UtileriasWeb.getNumeroPoliza(poliza), 
					this.getNombreAsegurado(poliza, request), 
					dateFormat.format(poliza.getFechaCreacion()), 
					this.getClaveEstatus(poliza), 
					this.generaListaValores(poliza), 
					poliza.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto().toBigInteger().toString(),
					permiteCopia);
			json.addRow(row);
		}
		response.setContentType("text/xml");
		PrintWriter pw = response.getWriter();
		pw.write(json.toXMLString());
		pw.flush();
		pw.close();
	}

	private String generaListaValores(PolizaDTO polizaDTO) throws SystemException {
		String lista = "";
		lista += polizaDTO.getIdToPoliza().toString() + "|";
		lista += UtileriasWeb.getNumeroPoliza(polizaDTO) + "|";
		lista += polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getNombreComercial().toUpperCase() + "|";
		
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaInicioPoliza = "1";
		String fechaFinPoliza = "1";
		EndosoId id = new EndosoId(polizaDTO.getIdToPoliza(), BigDecimal.ZERO.shortValue());
		EndosoDTO endoso = new EndosoDTO();
		endoso = EndosoDN.getInstancia(polizaDTO.getCodigoUsuarioCreacion()).getPorId(id);
		BigDecimal codigoAgente = BigDecimal.ZERO;
		CotizacionDTO cotizacionDTO = polizaDTO.getCotizacionDTO();
		if (endoso != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(endoso.getFechaInicioVigencia());
			fechaInicioPoliza = formatoFecha.format(cal.getTime());
			cal.setTime(endoso.getFechaFinVigencia());
			fechaFinPoliza = formatoFecha.format(cal.getTime());
			if (cotizacionDTO != null) {
				codigoAgente = cotizacionDTO.getSolicitudDTO().getCodigoAgente();
			}
		}

		lista += fechaInicioPoliza + "|";
		lista += fechaFinPoliza + "|";
		lista += polizaDTO.getClaveEstatus().toString() + "|";
		lista += codigoAgente;
		
		return lista;
	}

	private String getClaveEstatus(PolizaDTO polizaDTO) {
		String estatus = "";
		if (polizaDTO.getClaveEstatus().compareTo((short) 0) == 0) {
			estatus = "CANCELADA";
		} else {
			estatus = "VIGENTE";
		}
		return estatus;
	}

	private String getNombreAsegurado(PolizaDTO polizaDTO, HttpServletRequest request) {
		String cliente = "NO DISPONIBLE";
		CotizacionDTO cotizacion = polizaDTO.getCotizacionDTO();
		if(cotizacion != null && !UtileriasWeb.esCadenaVacia(cotizacion.getNombreAsegurado())) {
			cliente = cotizacion.getNombreAsegurado();
		}
		return cliente;
	}

	public ActionForward mostrarPolizasWindow(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String esSolicitudPoliza = request.getParameter("esSolicitudPoliza");
		if(esSolicitudPoliza != null && esSolicitudPoliza.equals("true")) {
			request.setAttribute("esSolicitudPoliza", esSolicitudPoliza);
		}
		return mapping.findForward(reglaNavegacion);
	}

	public void validaParaCancelacion(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) {
		StringBuffer buffer = new StringBuffer();
		try{
        		String idToPoliza=request.getParameter("idPoliza");
        		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response>");
        		if(idToPoliza!=null){
                		EndosoDTO ultimoEndoso = EndosoDN.getInstancia("").getUltimoEndoso(UtileriasWeb.regresaBigDecimal(idToPoliza.trim()));
                		if (ultimoEndoso != null) {
                		    EndosoIDTO endosoIDTO = EndosoIDN.getInstancia().validaEsCancelable(idToPoliza.trim()+"|0", UtileriasWeb.obtieneNombreUsuario(request),new Date(),Integer.valueOf(Sistema.TIPO_ENDOSO_CANCELACION)); 
                		    if(endosoIDTO!=null){
                			 buffer.append("<item>");
                			 buffer.append("<permiteCancelacion>" +endosoIDTO.getEsValido().toString()+ "</permiteCancelacion>");
                			 buffer.append("<descripcion>" + endosoIDTO.getMotivoNoCancelable() + "</descripcion>");
                			 buffer.append("</item>");
                		    }
                		}
        		}
        		buffer.append("</response>");
        	} catch (Throwable e) {
        		e.printStackTrace();
        		buffer.append("</response>");
        	} 
        	response.setContentType("text/xml");
        	response.setHeader("Cache-Control", "no-cache");
        	response.setContentLength(buffer.length());
        	try {
        		response.getWriter().write(buffer.toString());
        	} catch (IOException e) {
        		e.printStackTrace();
        	}
        	
	}
	
	
}