package mx.com.afirme.midas.poliza.cobertura;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaPolizaDN {
    private static final CoberturaPolizaDN INSTANCIA = new CoberturaPolizaDN();
	

	public static CoberturaPolizaDN getInstancia() {
		return INSTANCIA;
	}

	
	public void agregar(CoberturaPolizaDTO coberturaPolizaDTO)
	throws ExcepcionDeAccesoADatos, SystemException {
	new CoberturaPolizaSN().agregar(coberturaPolizaDTO);
	}
	
	
	public CoberturaPolizaDTO obtenerPorID(CoberturaPolizaId coberturaPolizaId) throws ExcepcionDeAccesoADatos, SystemException{
	    return new CoberturaPolizaSN().getPorId(coberturaPolizaId);
	}
	
	

}
