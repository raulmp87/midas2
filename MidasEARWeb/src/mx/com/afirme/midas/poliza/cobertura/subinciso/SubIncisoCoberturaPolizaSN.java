package mx.com.afirme.midas.poliza.cobertura.subinciso;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubIncisoCoberturaPolizaSN {
	private SubIncisoCoberturaPolizaFacadeRemote beanRemoto;

	public SubIncisoCoberturaPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(SubIncisoCoberturaPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(SubIncisoCoberturaPolizaDTO subIncisoCoberturaPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(subIncisoCoberturaPolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SubIncisoCoberturaPolizaDTO> buscarPorPropiedad(
			String propertyName, Object value) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propertyName, value);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
