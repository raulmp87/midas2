package mx.com.afirme.midas.poliza.cobertura;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoberturaPolizaSN {
	private CoberturaPolizaFacadeRemote beanRemoto;

	public CoberturaPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator
					.getEJB(CoberturaPolizaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public CoberturaPolizaDTO agregar(CoberturaPolizaDTO coberturaPolizaDTO)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(coberturaPolizaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<CoberturaPolizaDTO> agregar(List<CoberturaPolizaDTO> coberturas)
			throws ExcepcionDeAccesoADatos {
		try {
			List<CoberturaPolizaDTO> coberturasPersistidas = new ArrayList<CoberturaPolizaDTO>();
			for (CoberturaPolizaDTO cobertura : coberturas) {
				coberturasPersistidas.add(beanRemoto.save(cobertura));
			}
			return coberturasPersistidas;
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public CoberturaPolizaDTO getPorId(CoberturaPolizaId id)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
