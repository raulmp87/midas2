package mx.com.afirme.midas.poliza;

import java.util.List;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class PolizaForm extends MidasBaseForm{
	private static final long serialVersionUID = -1855775241380994793L;
	private String numeroPoliza;
	private String nombreAsegurado;
	private String fechaCreacion;
	private List<EndosoDTO> listaEndosos;
	private String accion;

	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public List<EndosoDTO> getListaEndosos() {
		return listaEndosos;
	}
	public void setListaEndosos(List<EndosoDTO> listaEndosos) {
		this.listaEndosos = listaEndosos;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getAccion() {
		return accion;
	}
}
