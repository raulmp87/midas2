package mx.com.afirme.midas.wsCliente.curp;

import java.rmi.RemoteException;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.commons.lang.StringUtils;

import com.afirme.validation.curp.CurpAnalyzerWSDLStub;
import com.afirme.validation.curp.ValidateCurp;
import com.afirme.validation.curp.ValidateCurpE;
import com.afirme.validation.curp.ValidateCurpResponseE;

public class ValidaCURPSN {
	private CurpAnalyzerWSDLStub stub;

	public ValidaCURPSN() throws SystemException {
		try {
			stub = new CurpAnalyzerWSDLStub(Sistema.END_POINT_WS_VALIDADOR_CURP);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE, e);
		}
	}

	public boolean esValido(ValidateCurp curp) throws ExcepcionDeAccesoADatos {
		curp = this.valida(curp);
		return curp.isValid();
	}

	public ValidateCurp valida(ValidateCurp curp)
			throws ExcepcionDeAccesoADatos {
		ValidateCurpE operacionE = new ValidateCurpE();
		ValidateCurpResponseE response = null;
		operacionE.setValidateCurp(curp);
		try {
			response = stub.validateCurp(operacionE);
			if (StringUtils.contains(response.getValidateCurpResponse()
					.getOut().getValidationMessage(), "Error"))
				curp.setValid(false);
			else
				curp.setValid(true);
			curp.setMessageValidation(response.getValidateCurpResponse()
					.getOut().getValidationMessage());
		} catch (RemoteException e) {
			throw new ExcepcionDeAccesoADatos(stub.getClass()
					.getCanonicalName(), e);
		}
		return curp;
	}

}
