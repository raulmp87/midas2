package mx.com.afirme.midas.wsCliente.curp;

import java.rmi.RemoteException;

import com.afirme.validation.curp.CurpAnalyzerWSDLStub;
import com.afirme.validation.curp.ValidateCurp;
import com.afirme.validation.curp.ValidateCurpE;
import com.afirme.validation.curp.ValidateCurpResponseE;

public class TestWS {

	public static void main(java.lang.String args[]){
        try{
        	String endPoint = "http://172.20.77.104:9087/WSCurp/CurpAnalyzerWSDL";
            CurpAnalyzerWSDLStub stub =new CurpAnalyzerWSDLStub(endPoint);
            

            doInOnly(stub);
//            twoWayOneParameterEcho(stub);
//            noParameters(stub);
//            multipleParameters(stub);

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("\n\n\n");
        }
    }
	public static void doInOnly(CurpAnalyzerWSDLStub stub) throws RemoteException{
		ValidateCurp operacion = new ValidateCurp();
		ValidateCurpE operacionE = new ValidateCurpE();
		ValidateCurpResponseE response = null;
		
		operacion.setBirthDate("03/10/1981");
		operacion.setBornState("DISTRITO FEDERAL");
		operacion.setCapturedCurp("CACJ811003HDFNVR09");
		operacion.setPersonFirstLastName("CANO");
		operacion.setPersonGender("MASCULINO");
		operacion.setPersonName("JORGE ALVERTO");
		operacion.setPersonSecondLastName("CUEVAZ");
		
		operacionE.setValidateCurp(operacion);
		response = stub.validateCurp(operacionE);
		//System.out.println(response.localValidateCurpResponse.getOut().getValidationMessage());
		System.out.println(response.getValidateCurpResponse().getOut().getValidationMessage());
	}
}
