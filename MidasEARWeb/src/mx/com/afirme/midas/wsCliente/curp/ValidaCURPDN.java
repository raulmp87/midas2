package mx.com.afirme.midas.wsCliente.curp;

import java.text.Format;
import java.text.SimpleDateFormat;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import com.afirme.validation.curp.ValidateCurp;

public class ValidaCURPDN {
	private static final ValidaCURPDN INSTANCIA = new ValidaCURPDN();

	public static ValidaCURPDN getInstancia() {
		return ValidaCURPDN.INSTANCIA;
	}

	public ValidateCurp valida(ValidateCurp curp)
			throws ExcepcionDeAccesoADatos, SystemException {
		ValidaCURPSN validaCURPSN;
		try {
			validaCURPSN = new ValidaCURPSN();
			return validaCURPSN.valida(curp);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}

	public boolean esValido(ValidateCurp curp) throws ExcepcionDeAccesoADatos,
			SystemException {
		ValidaCURPSN validaCURPSN;
		try {
			validaCURPSN = new ValidaCURPSN();
			return validaCURPSN.esValido(curp);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}

	public String valida(String nombre, String apellidoPaterno,
			String apellidoMaterno, String FechaNacimiento,
			String estadoNacimiento, String sexo, String curp)
			throws ExcepcionDeAccesoADatos, SystemException {
		try {
			ValidateCurp curpVO = new ValidateCurp();

			curpVO.setBirthDate(FechaNacimiento);
			curpVO.setBornState(estadoNacimiento);
			curpVO.setCapturedCurp(curp);
			curpVO.setPersonFirstLastName(apellidoPaterno);
			curpVO.setPersonGender(sexo);
			curpVO.setPersonName(nombre);
			curpVO.setPersonSecondLastName(apellidoMaterno);

			curpVO = this.valida(curpVO);

			return curpVO.getMessageValidation();
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} 
	}
	public boolean esValido(ClienteDTO cliente) throws SystemException{
		try {
			ValidateCurp curpVO = new ValidateCurp();
			Format 	formatter = new SimpleDateFormat("dd/MM/yyyy");
			if (cliente.getFechaNacimiento() != null)
				curpVO.setBirthDate(formatter.format(cliente.getFechaNacimiento()));
			
			curpVO.setBornState(cliente.getEstadoNacimiento());
			curpVO.setCapturedCurp(cliente.getCodigoCURP());
			curpVO.setPersonFirstLastName(cliente.getApellidoPaterno());
			curpVO.setPersonSecondLastName(cliente.getApellidoMaterno());
			curpVO.setPersonGender(cliente.getSexo());
			if(cliente.getClaveTipoPersona().equals(Sistema.CLAVE_TIPO_PERSONA_FISICA))
				curpVO.setPersonName(cliente.getNombre());
			else
				curpVO.setPersonName(cliente.getNombreRepresentante());

			return this.esValido(curpVO);
			
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} 		
	}
}
