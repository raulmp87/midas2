package mx.com.afirme.midas.wsCliente.emision;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EmisionDN {

	private static final EmisionDN INSTANCIA = new EmisionDN();
	
	public static EmisionDN getInstancia() {
		return EmisionDN.INSTANCIA;
	}
	
	/**
	 * Genera los recibos en el servidor. Se recomienda ejecutar despues de la emisi�n. (WFactura_TRANSPORTE)
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public void generarRecibos() throws ExcepcionDeAccesoADatos, SystemException {
		EmisionSN emisionSN;
		try {
			emisionSN = new EmisionSN();
			emisionSN.generarRecibos();
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}
	
	/**
	 * Devuelve un arreglo de Bytes con el archivo PDF de la factura de emisi�n previamente generada por el transporte.
	 * @param llaveFiscal Llave fiscal del recibo generada en la emisi�n
	 * @return Arreglo de Bytes con el archivo PDF de la factura de emisi�n previamente generada por el transporte.
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 */
	public byte[] obtieneFactura (String llaveFiscal)  throws ExcepcionDeAccesoADatos, SystemException {
		EmisionSN emisionSN;
		try {
			emisionSN = new EmisionSN();
			return emisionSN.obtieneFactura(llaveFiscal);
		} catch (ExcepcionDeAccesoADatos e) {
			throw e;
		} catch (SystemException e) {
			throw e;
		}
	}
	
	
}
