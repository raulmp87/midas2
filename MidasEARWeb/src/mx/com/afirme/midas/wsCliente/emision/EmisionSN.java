package mx.com.afirme.midas.wsCliente.emision;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;

public class EmisionSN {

	private PrintReport printReportService;
	
	public EmisionSN() throws SystemException {
		try {
			PrintReportServiceLocator locator = new PrintReportServiceLocator(Sistema.URL_WS_IMPRESION, Sistema.PUERTO_WS_IMPRESION);						
			printReportService = locator.getPrintReport();
		
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public void generarRecibos() throws ExcepcionDeAccesoADatos {
		try {
			printReportService.generateDigitalBill();
		}  catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(printReportService.getClass().getCanonicalName(), e);
		}
	}
	
	public byte[] obtieneFactura (String llaveFiscal) throws ExcepcionDeAccesoADatos {
		try {
			return printReportService.getDigitalBill(llaveFiscal);
		}  catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(printReportService.getClass().getCanonicalName(), e);
		}
	}
	
}
