package mx.com.afirme.midas.wsCliente.seguridad;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.seguridad.filler.DummyFiller;

import com.asm.dto.PageConsentDTO;
import com.asm.dto.UserDTO;

/**
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.seguridad.UsuarioService}
 */
@Deprecated
public class UsuarioWSDN {

	private int idAccesoUsuario;
	private static final UsuarioWSDN INSTANCIA = new UsuarioWSDN();
	private static final boolean ASM_ATIVO = Sistema.ASM_ACTIVO;

	public static UsuarioWSDN getInstancia() {
		return UsuarioWSDN.INSTANCIA;
	}

	public List<Usuario> obtieneUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, String idSesionUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		try {
			if (UsuarioWSDN.ASM_ATIVO) {
				idAccesoUsuario = Integer.parseInt(idSesionUsuario);
				List<UserDTO> usuariosASM = new UsuarioWSSN()
						.obtieneUsuariosSinRolesPorNombreRol(nombreRol,
								Sistema.USUARIO_SISTEMA, idAccesoUsuario);
				return new MidasASMTranslator()
						.obtieneListaUsuariosMidasSinRoles(usuariosASM);
			} else {
				DummyFiller df = new DummyFiller();
				return df.obtieneUsuariosPorRol(nombreRol);
			}

		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

	public List<Usuario> obtieneUsuariosSinRolesPorNombreRol(
			String nombreUsuario, String idSesionUsuario, String... nombresRol)
			throws ExcepcionDeAccesoADatos, SystemException {
		try {
			if (UsuarioWSDN.ASM_ATIVO) {
				idAccesoUsuario = Integer.parseInt(idSesionUsuario);
				List<Usuario> usuarios = new ArrayList<Usuario>();
				for (String nombreRol : nombresRol) {
					List<UserDTO> usuariosTMP = new UsuarioWSSN()
							.obtieneUsuariosSinRolesPorNombreRol(nombreRol,
									Sistema.USUARIO_SISTEMA, idAccesoUsuario);
					usuarios.addAll(new MidasASMTranslator()
							.obtieneListaUsuariosMidasSinRoles(usuariosTMP));
				}

				return usuarios;
			} else {
				DummyFiller df = new DummyFiller();
				List<Usuario> usuarios = new ArrayList<Usuario>();
				for (String nombreRol : nombresRol) {
					usuarios.addAll(df.obtieneUsuariosPorRol(nombreRol));
				}
				return usuarios;
			}

		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

	public Usuario obtieneUsuarioRegistrado(String nombreUsuario,
			String idSesionUsuario) throws ExcepcionDeAccesoADatos,
			SystemException {
		try {
			if (UsuarioWSDN.ASM_ATIVO) {
				idAccesoUsuario = Integer.parseInt(idSesionUsuario);
				UserDTO usuarioASM = new UsuarioWSSN()
						.obtieneUsuarioRegistrado(nombreUsuario,
								idAccesoUsuario);
				return new MidasASMTranslator().obtieneUsuarioMidas(usuarioASM, null);
			} else {
				DummyFiller df = new DummyFiller();
				return df.obtieneUsuarioMidas(nombreUsuario);
			}
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

	public boolean LogOutUsuario(String nombreUsuario, String idSesionUsuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		try {
			if (UsuarioWSDN.ASM_ATIVO) {
				idAccesoUsuario = Integer.parseInt(idSesionUsuario);
				return new UsuarioWSSN().logOutUsuario(nombreUsuario,
						idAccesoUsuario);
			} else {
				return true;
			}

		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

	public List<PageConsentDTO> obtieneListaConsentimientos(int idRole) {
		try {
			return new UsuarioWSSN().obtieneListaConsentimientos(idRole);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

	public Usuario obtieneUsuario(int idUsuario) {
		try {
			if (UsuarioWSDN.ASM_ATIVO) {
				UserDTO usuarioASM = new UsuarioWSSN()
						.obtieneUsuario(idUsuario);
				return new MidasASMTranslator().obtieneUsuarioMidas(usuarioASM, null);
			} else {
				DummyFiller df = new DummyFiller();
				return df.obtenerUsuarioPorId(idUsuario);
			}
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

	public Usuario obtieneUsuario(String nombreUsuario) {
		try {
			if (UsuarioWSDN.ASM_ATIVO) {
				UserDTO usuarioASM = new UsuarioWSSN()
						.obtieneUsuario(nombreUsuario);
				return new MidasASMTranslator().obtieneUsuarioMidas(usuarioASM, null);
			} else {
				DummyFiller df = new DummyFiller();
				return df.obtieneUsuarioPorNombreUsuario(nombreUsuario);
			}
		} catch (Exception e) {

			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
					.getCanonicalName(), e);
		}
	}

//	public int getUserAccessId() {
//		try {
//			String username = "829PETL ";
//			String userIPAddress = "172.22.2.2";
//			String jSessionId = "LTzWqdKthGQ7rAyvSG_B06i";
//			int applicationId = 5;
//			return new UsuarioWSSN().getUserAccessId(username, userIPAddress,
//					jSessionId, applicationId);
//		} catch (Exception e) {
//
//			throw new ExcepcionDeAccesoADatos(UsuarioWSDN.class
//					.getCanonicalName(), e);
//		}
//	}
}
