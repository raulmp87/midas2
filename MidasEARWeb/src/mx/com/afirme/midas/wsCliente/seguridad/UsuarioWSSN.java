package mx.com.afirme.midas.wsCliente.seguridad;

import java.util.List;

import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.MidasASMFacadeRemote;

import com.asm.dto.PageConsentDTO;
import com.asm.dto.UserDTO;

/**
 * @deprecated desde Midas 2.0. Utilizar {@link mx.com.afirme.midas2.service.seguridad.UsuarioService}
 */
@Deprecated
public class UsuarioWSSN {

	private MidasASMFacadeRemote beanRemoto;

	public UsuarioWSSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MidasASMFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}

	public void bloqueaAcceso(int idUsuario) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.bloqueaAcceso(idUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public List<UserDTO> obtieneUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, int idAccesoUsuario)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuariosSinRolesPorNombreRol(nombreRol,
					nombreUsuario, idAccesoUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public List<UserDTO> obtieneUsuariosSinRolesPorIdRol(int idRol,
			String nombreUsuario, int idAccesoUsuario)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuariosSinRolesPorIdRol(idRol,
					nombreUsuario, idAccesoUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public UserDTO obtieneUsuarioSinRolesPorNombreUsuario(String nombreUsuario,
			int idAccesoUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuarioSinRolesPorNombreUsuario(
					nombreUsuario, idAccesoUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public UserDTO obtieneUsuarioSinRolesPorIdUsuario(int idUsuario,
			int idAccesoUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuarioSinRolesPorIdUsuario(idUsuario,
					idAccesoUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public UserDTO obtieneUsuarioRegistrado(String nombreUsuario,
			int idAccesoUsuario) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuarioRegistrado(nombreUsuario,
					idAccesoUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public boolean logOutUsuario(String nombreUsuario, int idAccesoUsuario)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.logOutUsuario(nombreUsuario, idAccesoUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public List<PageConsentDTO> obtieneListaConsentimientos(int idRole)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneListaConsentimientos(idRole);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(this.getClass()
					.getCanonicalName(), e);
		}
	}

	public UserDTO obtieneUsuario(String nombreUsuario)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuario(nombreUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

	public UserDTO obtieneUsuario(int idUsuario)
			throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtieneUsuario(idUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
					.getCanonicalName(), e);
		}
	}

//	public int getUserAccessId(String username, String userIPAddress,
//			String jSessionId, int applicationId) {
//		try {
//			return beanRemoto.getUserAccessId(username, userIPAddress,
//					jSessionId, applicationId);
//		} catch (Exception e) {
//			throw new ExcepcionDeAccesoADatos(beanRemoto.getClass()
//					.getCanonicalName(), e);
//		}
//	}
	
}
