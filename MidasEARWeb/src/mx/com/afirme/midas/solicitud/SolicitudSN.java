package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

/**
 * @author Fernando Alonzo
 * @since 01 de Septiembre del 2009
 * 
 */
public class SolicitudSN {
	private SolicitudFacadeRemote beanRemoto;

	public SolicitudSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SolicitudFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public SolicitudDTO agregar(SolicitudDTO solicitudDTO) {
		try {
			return beanRemoto.save(solicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public SolicitudDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<SolicitudDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SolicitudDTO> listarFiltrado(SolicitudDTO solicitudDTO) {
		try {
			return beanRemoto.listarFiltrado(solicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public SolicitudDTO actualizar(SolicitudDTO solicitudDTO) {
		try {
			return beanRemoto.update(solicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(SolicitudDTO solicitudDTO) {
		try {
			beanRemoto.delete(solicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<SolicitudDTO> listarPorUsuario(Usuario usuario) {
		try {
			return beanRemoto.findByUserId(usuario.getNombreUsuario());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SolicitudDTO> listarPorEstatus(Short claveEstatus, Usuario usuario){
		try {
			return beanRemoto.findByEstatus(claveEstatus, usuario.getNombreUsuario());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public Long obtenerTotalFiltrado(SolicitudDTO solicitudDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.obtenerTotalFiltrado(solicitudDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		
	}
		
}
