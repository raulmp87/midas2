package mx.com.afirme.midas.solicitud.documento;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class DocumentoDigitalSolicitudDN {
	private static final DocumentoDigitalSolicitudDN INSTANCIA = new DocumentoDigitalSolicitudDN();

	public static DocumentoDigitalSolicitudDN getInstancia() {
		return DocumentoDigitalSolicitudDN.INSTANCIA;
	}

	public DocumentoDigitalSolicitudDTO getPorId(BigDecimal id) throws SystemException {
		DocumentoDigitalSolicitudSN solicitudSN = new DocumentoDigitalSolicitudSN();
		return solicitudSN.getPorId(id);
	}

	public List<DocumentoDigitalSolicitudDTO> listarTodos() throws SystemException {
		DocumentoDigitalSolicitudSN solicitudSN = new DocumentoDigitalSolicitudSN();
		return solicitudSN.listarTodos();
	}

	public void agregar(DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalSolicitudSN solicitudSN = new DocumentoDigitalSolicitudSN();
		solicitudSN.agregar(documentoDigitalSolicitudDTO);
	}

	public List<DocumentoDigitalSolicitudDTO> listarDocumentosSolicitud(
			BigDecimal idToSolicitud) throws SystemException {
		DocumentoDigitalSolicitudSN solicitudSN = new DocumentoDigitalSolicitudSN();
		return solicitudSN.listarDocumentosSolicitud(idToSolicitud);
	}

	public void borrar(DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) throws SystemException {
		DocumentoDigitalSolicitudSN solicitudSN = new DocumentoDigitalSolicitudSN();
		solicitudSN.borrar(documentoDigitalSolicitudDTO);
	}
	
	public List<DocumentoDigitalSolicitudDTO> listarDocumentosDigitalesPorSolicitud(BigDecimal idToSolicitud) throws SystemException{
		DocumentoDigitalSolicitudSN documentoDigitalSolicitudSN = new DocumentoDigitalSolicitudSN();
		return documentoDigitalSolicitudSN.listarDocumentosSolicitud(idToSolicitud);
	}
}
