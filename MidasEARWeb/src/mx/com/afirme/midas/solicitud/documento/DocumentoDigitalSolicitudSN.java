package mx.com.afirme.midas.solicitud.documento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author Fernando Alonzo
 * @since 01 de Septiembre del 2009
 * 
 */
public class DocumentoDigitalSolicitudSN {
	private DocumentoDigitalSolicitudFacadeRemote beanRemoto;

	public DocumentoDigitalSolicitudSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoDigitalSolicitudFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) {
		try {
			beanRemoto.save(documentoDigitalSolicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DocumentoDigitalSolicitudDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DocumentoDigitalSolicitudDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DocumentoDigitalSolicitudDTO> listarDocumentosSolicitud(
			BigDecimal idToSolicitud) {
		try {
			return beanRemoto.findByProperty("solicitudDTO.idToSolicitud", idToSolicitud);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) {
		try {
			beanRemoto.delete(documentoDigitalSolicitudDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
