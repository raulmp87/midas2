package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.endoso.CotizacionEndosoDN;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDN;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDTO;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoId;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.commons.lang.time.DateUtils;

public class SolicitudDN {
	private static final SolicitudDN INSTANCIA = new SolicitudDN();

	public static SolicitudDN getInstancia() {
		return SolicitudDN.INSTANCIA;
	}

	public CotizacionDTO getResumenSolicitud(BigDecimal id) throws SystemException{
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		SolicitudDTO solicitudDTO = solicitudDN.getPorId(id);		
		
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setSolicitudDTO(solicitudDTO);
		
		CotizacionDN cotizacionDN = CotizacionDN.getInstancia(solicitudDTO.getCodigoUsuarioCreacion());
		List<CotizacionDTO> cotizaciones = cotizacionDN.buscarPorPropiedad("solicitudDTO.idToSolicitud", solicitudDTO.getIdToSolicitud());
		
		if (cotizaciones.size() > 0)
			cotizacionDTO = cotizaciones.get(0);

		return cotizacionDTO;
	}
	public CotizacionDTO getResumenSolicitudEndoso(BigDecimal id) throws SystemException{
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		SolicitudDTO solicitudDTO = solicitudDN.getPorId(id);		
		
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setSolicitudDTO(solicitudDTO);

		CotizacionEndosoDN cotizacionDN = CotizacionEndosoDN.getInstancia(solicitudDTO.getCodigoUsuarioCreacion());
		List<CotizacionDTO> cotizaciones = cotizacionDN.listarCotizacionFiltrado(cotizacionDTO, null);
		
		if (cotizaciones.size() > 0)
			cotizacionDTO = cotizaciones.get(0);

		return cotizacionDTO;
	}	
	public SolicitudDTO getPorId(BigDecimal id) throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		return solicitudSN.getPorId(id);
	}

	public List<SolicitudDTO> listarTodos() throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		return solicitudSN.listarTodos();
	}

	public SolicitudDTO agregar(SolicitudDTO solicitudDTO) throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		return solicitudSN.agregar(solicitudDTO);
	}

	public SolicitudDTO actualizar(SolicitudDTO solicitudDTO) throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		return solicitudSN.actualizar(solicitudDTO);
	}

	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO.getIdToControlArchivo());
	}

	public List<SolicitudDTO> listarFiltrado(SolicitudDTO solicitudDTO, Usuario usuario) throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		String claveSolicitudesFiltradas = "midas.danios.solicitud.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveSolicitudesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveSolicitudesFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;			
	
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)
			solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		
		//Se agrega filtro de Codigo de Agente Especial en caso que aplique
		AgenteEspecialDN.getInstancia().agregaFiltroCodigoAgente(usuario, solicitudDTO);
		
		return solicitudSN.listarFiltrado(solicitudDTO);		
	}

	public void borrar(SolicitudDTO solicitudDTO) throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		solicitudSN.borrar(solicitudDTO);
	}

	public List<SolicitudDTO> listarPorUsuario(Usuario usuario) throws SystemException {
		SolicitudSN solicitudSN = new SolicitudSN();
		return solicitudSN.listarPorUsuario(usuario);
	}
	
	public List<SolicitudDTO> listarPorEstatus(Short claveEstatus, Usuario usuario) throws SystemException{
		return new SolicitudSN().listarPorEstatus(claveEstatus, usuario);
	}
	
	public Long obtenerTotalFiltrado(SolicitudDTO solicitudDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException{
		String claveSolicitudesFiltradas = "midas.danios.solicitud.busqueda.limitada";
		boolean aplicaFiltrado = true;
		if (usuario != null
				&& usuario.contieneAtributo(claveSolicitudesFiltradas)){
			AtributoUsuario atributoUsuario = usuario
			.obtenerAtributo(claveSolicitudesFiltradas);
			if(!atributoUsuario.isActivo())
				aplicaFiltrado = false;			
	
		}else{
			aplicaFiltrado = false;
		}
		if(aplicaFiltrado)
			solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		
		//Se agrega filtro de Codigo de Agente Especial en caso que aplique
		AgenteEspecialDN.getInstancia().agregaFiltroCodigoAgente(usuario, solicitudDTO);
		
		return new SolicitudSN().obtenerTotalFiltrado(solicitudDTO);
	}	

	public  void registraEstadisticas(BigDecimal idToSolicitud,BigDecimal idToCotizacion,
		BigDecimal idToPoliza, BigDecimal numeroEndoso,String codigoUsuario,int tipoAccion) throws SystemException{
	    EstadisticasEstadoDTO estadisticasEstadoDTO;
	    EstadisticasEstadoId estadisticasEstadoId;
	    switch (tipoAccion) {
	    case Sistema.SE_CREA_SOLICITUD:
		if(idToSolicitud!=null){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
		}
		break;
	    case Sistema.SE_ASIGNA_SOL_A_ODT:
		if(idToSolicitud!=null && idToCotizacion!=null ){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("20"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
		}
		break;
	    case Sistema.SE_ASIGNA_ODT_A_COT:
		if(idToSolicitud!=null && idToCotizacion!=null ){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("30"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
		}
		break;
	    case Sistema.SE_ASIGNA_COT_A_EMISION:
		if(idToSolicitud!=null && idToCotizacion!=null ){
		    estadisticasEstadoDTO=EstadisticasEstadoDN.getInstancia().
    			buscarParaActualizarPorRetornoEstatus(idToSolicitud,idToCotizacion);
		    if(estadisticasEstadoDTO==null){
			estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("40"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
		    }else{
			estadisticasEstadoDTO.getId().setFechaHora(Calendar.getInstance().getTime());
			estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
			EstadisticasEstadoDN.getInstancia().modificar(estadisticasEstadoDTO);
		    }
		}
		break;
	    case Sistema.SE_EMITE_COTIZACION:
		if(idToSolicitud!=null && idToCotizacion!=null && idToPoliza!=null 
			&& numeroEndoso!=null ){
        		estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("50"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(idToPoliza);
        		estadisticasEstadoDTO.setIdBase4(numeroEndoso);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
		}
		break;
		//para el caso de algunos endosos, y para cuando se Crea una solicitud a partir de Los Datos de una Poliza
	    case Sistema.SE_ASIGNA_SOL_A_COT:
		if(idToSolicitud!=null && idToCotizacion!=null ){
		    	estadisticasEstadoDTO= new EstadisticasEstadoDTO();
        		estadisticasEstadoId= new EstadisticasEstadoId();
        		estadisticasEstadoId.setIdCodigoEstadistica(new BigDecimal("10"));
        		estadisticasEstadoId.setIdSubCodigoEstadistica(new BigDecimal("20"));
        		estadisticasEstadoId.setFechaHora(Calendar.getInstance().getTime());
        		estadisticasEstadoDTO.setId(estadisticasEstadoId);
        		estadisticasEstadoDTO.setIdBase1(idToSolicitud);
        		estadisticasEstadoDTO.setIdBase2(idToCotizacion);
        		estadisticasEstadoDTO.setIdBase3(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setIdBase4(BigDecimal.ZERO);
        		estadisticasEstadoDTO.setValorVariable1(codigoUsuario);
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
        		estadisticasEstadoDTO.getId().setIdSubCodigoEstadistica(new BigDecimal("30"));        		
        		EstadisticasEstadoDN.getInstancia().agregar(estadisticasEstadoDTO);
		}
		break;
	     default:
		break;
	    }
	}
	
	/**
	 * Valida que el agente con id <code>idAgente<code>
	 * este activo y tenga cedula vigente
	 */
	public Set<String> validarAgente(Integer idAgente, String nombreUsuario) {
		final Set<String> result = new HashSet<String>();
		final AgenteDTO agente = new AgenteDTO();
		agente.setIdTcAgente(idAgente);
		try {
			final AgenteDN agenteDN = AgenteDN.getInstancia();
			final AgenteDTO agenteBD = agenteDN.verDetalleAgente(agente, nombreUsuario);
			final Date diaActual = DateUtils.truncate(new Date(), Calendar.DATE);
			// Valida que el agente est� activo
			if (!"A".equals(agenteBD.getSituacionAgente())) {
				result.add(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "solicitud.agenteInactivo"));
			}
			// Valida que tenga una cedula permitida - Se comenta la validaci�n para un mayor analisis de la misma
			/*if (!ArrayUtils.contains(Sistema.CEDULAS_DE_AGENTES_PERMITIDOS, StringUtils.trim(agenteBD.getTipoCedula()))) {
				result.add(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "solicitud.agenteCedulaInvalida",
						agenteBD.getTipoCedula()));
			}*/
			// Valida que la cedula del agente est� activa
			if (agenteBD.getFechaVencimientoCedula() == null
					|| agenteBD.getFechaVencimientoCedula().compareTo(diaActual) < 0) {
				result.add(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "solicitud.agenteCedulaVencida"));
			}
		} catch (SystemException e) {
			LogDeMidasWeb.log("Al obtener el detalle del agente con id " + idAgente, Level.SEVERE, e);
		}
		return result;
	}
}
