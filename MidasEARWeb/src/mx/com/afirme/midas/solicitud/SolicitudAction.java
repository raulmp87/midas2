package mx.com.afirme.midas.solicitud;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoSN;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDN;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionSN;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionSN;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDN;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.agente.AgenteDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDTO;
import mx.com.afirme.midas.poliza.renovacion.SeguimientoRenovacionDTO;
import mx.com.afirme.midas.producto.ProductoDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.vault.VaultAction;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts2.ServletActionContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * MyEclipse Struts Creation date: 09-01-2009
 * 
 */
public class SolicitudAction extends MidasMappingDispatchAction {
	/**
	 * Carga los elementos necesarios en 
	 * <code>SolicitudForm</code> para presentar la pagina de resumen.
	 * @param mapping 
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarResumen(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id = request.getParameter("id");
		SolicitudForm solicitudForm = (SolicitudForm)form;
		try {
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			CotizacionDTO cotizacionDTO = new CotizacionDTO();
			if(solicitudDTO.getClaveTipoSolicitud() == Sistema.SOLICITUD_TIPO_ENDOSO){
				cotizacionDTO = SolicitudDN.getInstancia().getResumenSolicitudEndoso(solicitudDTO.getIdToSolicitud());				
			}else{
				cotizacionDTO= solicitudDN.getResumenSolicitud(UtileriasWeb.regresaBigDecimal(id));	
			}
			solicitudForm.setNumeroSolicitudFormateado(UtileriasWeb.llenarIzquierda(cotizacionDTO.getSolicitudDTO().getIdToSolicitud().toString(), "0", 8));
			solicitudForm.setDescripcionEstatus(UtileriasWeb.getDescripcionCatalogoValorFijo(29, cotizacionDTO.getSolicitudDTO().getClaveEstatus()));
			this.poblarForm(cotizacionDTO.getSolicitudDTO(), solicitudForm, request);
			solicitudForm.setNombreUsuarioAsignacionODT(cotizacionDTO.getCodigoUsuarioOrdenTrabajo());
			String estatusODT= "";
			try{
				if (cotizacionDTO != null){
					EstadisticasEstadoDN estadisticasEstadoDN = EstadisticasEstadoDN.getInstancia();
					List<EstadisticasEstadoDTO> estadisticas = estadisticasEstadoDN.buscarPorPropiedad("idBase2", cotizacionDTO.getIdToCotizacion());
					estatusODT =  UtileriasWeb.getDescripcionCatalogoValorFijo(30, cotizacionDTO.getClaveEstatus());
					solicitudForm.setNumeroOrdenTrabajoFormateado(UtileriasWeb.llenarIzquierda(cotizacionDTO.getIdToCotizacion().toString(), "0", 8));
					solicitudForm.setFechaFinVigencia(cotizacionDTO.getFechaFinVigencia());
					solicitudForm.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
					solicitudForm.setNombreContratante(cotizacionDTO.getNombreContratante());
					solicitudForm.setNombreAsegurado(cotizacionDTO.getNombreAsegurado());
					if(cotizacionDTO.getTipoPolizaDTO() != null) {
						solicitudForm.setTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getDescripcion());
					}
					if(cotizacionDTO.getIdMoneda() != null) {
						solicitudForm.setMoneda(MonedaDN.getInstancia().getPorId(cotizacionDTO.getIdMoneda().shortValue()).getDescripcion());
					}
					for(EstadisticasEstadoDTO estadistica : estadisticas) {
						if(estadisticasEstadoDN.esTrancisionODT(estadistica)) {
							solicitudForm.setFechaODT(estadistica.getId().getFechaHora());
							break;
						}
					}
					for(EstadisticasEstadoDTO estadistica : estadisticas) {
						if(estadisticasEstadoDN.esTrancisionCOT(estadistica)) {
							solicitudForm.setFechaCOT(estadistica.getId().getFechaHora());
							break;
						}
					}
					solicitudForm.setNombreUsuarioAsignacionCOT(cotizacionDTO.getCodigoUsuarioCotizacion());
					
					if(cotizacionDTO.getClaveEstatus().shortValue() == Sistema.ESTATUS_COT_EMITIDA) {
						EndosoDN endosoDN = EndosoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
						List<EndosoDTO> endosos = endosoDN.buscarPorPropiedad("idToCotizacion", cotizacionDTO.getIdToCotizacion(), Boolean.TRUE);
						if(endosos != null && !endosos.isEmpty()) {
							EndosoDTO endoso = endosos.get(0);
							PolizaDN polizaDN = PolizaDN.getInstancia();
							PolizaDTO polizaDTO = polizaDN.getPorId(endoso.getId().getIdToPoliza());
							solicitudForm.setNumPoliza(UtileriasWeb.getNumeroPoliza(polizaDTO));
							solicitudForm.setNumeroEndoso(endoso.getId().getNumeroEndoso().toString());
							solicitudForm.setNombreUsuarioAsignacionEmision(cotizacionDTO.getCodigoUsuarioEmision());
							solicitudForm.setFechaEmision(endoso.getFechaCreacion());
						}
					}
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			solicitudForm.setDescripcionEstatusODT(estatusODT);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}	
	/**
	 *  Devuelve las solicitudes existentes por filtros de busqueda,aplicando el paginado.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
	    	return listarFiltrado(mapping, form, request, response);
	}


	/**
	 * Devuelve las solicitudes encontradas para los filtro(s)
	 * capturado(s) en <code>SolicitudForm</code>, con  el Paginado correspondiente.
	 * @param mapping 
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listarFiltrado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
		    SolicitudForm solicitudForm = (SolicitudForm)form;
		    SolicitudDTO solicitudDTO = new SolicitudDTO();
		    poblarDTO(solicitudForm, solicitudDTO);
		    Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		    //Se comenta a peticion de Cesar Ayma, que no vaya el filtro por usuario
	       	   /* Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
	       	    if (usuario != null){
	       		solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
	       	    }*/
		    
	       	    Long totalRegistros = SolicitudDN.getInstancia().obtenerTotalFiltrado(solicitudDTO, usuario);
	       	    solicitudForm.setTotalRegistros(totalRegistros.toString()); 
		    return listarFiltradoPaginado(mapping, form, request, response);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Obtiene  y pagina las solicitudes existentes,dado un filtro en <code>SolicitudForm</code>.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listarFiltradoPaginado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			if (!listaEnCache(form, request)) {
				SolicitudForm solicitudForm = (SolicitudForm)form;
				SolicitudDTO solicitudDTO = new SolicitudDTO();
				poblarDTO(solicitudForm, solicitudDTO);
				solicitudDTO.setPrimerRegistroACargar(solicitudForm.getPrimerRegistroACargar());
				solicitudDTO.setNumeroMaximoRegistrosACargar(solicitudForm.getNumeroMaximoRegistrosACargar());
				solicitudForm.setListaPaginada(SolicitudDN.getInstancia().listarFiltrado(solicitudDTO,usuario), request);
			
			} 
			
			request.setAttribute("solicitudes", obtieneListaAMostrar(form, request));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga los datos de SolicitudDTO a partir de los datos de SolicitudForm, para efectos de busqueda(filtros).
	 * @param solicitudForm
	 * @param solicitudDTO
	 * @throws SystemException si al invocar <code>UtileriasWeb.regresaBigDecimal</code> se levantara la excepcion
	 * @throws ExcepcionDeAccesoADatos
	 */
	private void poblarDTO(SolicitudForm solicitudForm, SolicitudDTO solicitudDTO) throws SystemException, ExcepcionDeAccesoADatos {
		
		if(!UtileriasWeb.esCadenaVacia(solicitudForm.getEstatus())) {
			solicitudDTO.setClaveEstatus(UtileriasWeb.regresaShort(solicitudForm.getEstatus()));
		}
		if (!UtileriasWeb.esCadenaVacia(solicitudForm.getNombres())) {
			solicitudDTO.setNombrePersona(solicitudForm.getNombres());
		}
		if (!UtileriasWeb.esCadenaVacia(solicitudForm.getApellidoPaterno())) {
			solicitudDTO.setApellidoPaterno(solicitudForm.getApellidoPaterno());
		}
		if (!UtileriasWeb.esCadenaVacia(solicitudForm.getApellidoMaterno())) {
			solicitudDTO.setApellidoMaterno(solicitudForm.getApellidoMaterno());
		}
		if (!UtileriasWeb.esCadenaVacia(solicitudForm.getNumeroSolicitud())) {
			solicitudDTO.setIdToSolicitud(UtileriasWeb.regresaBigDecimal(solicitudForm.getNumeroSolicitud()));
		}
		
	}
	/**
	 * Carga los elementos necesarios en <code>SolicitudForm</code> para presentar la pagina de Agregar.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudForm solicitudForm = (SolicitudForm) form;
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		//String accionDo = request.getContextPath()+request.getAttribute("org.apache.struts.globals.ORIGINAL_URI_KEY").toString();
		//String rutaPagina = mapping.findForward(reglaNavegacion).getPath();

		//String pagina = rutaPagina.substring(rutaPagina.lastIndexOf("/") + 1);
		
		//Si tiene permiso para autoasignar, se establece el atributo "permisoautoasignacion" en true 

		//String permisoPorValidar = "AA";
		
		/*if(Sistema.SEGURIDAD_ACTIVADA && Sistema.PERMISOS_SEGURIDAD_ACTIVADOS){
		for (Rol rol : usuario.getRoles())
			for(PaginaPermiso pp : rol.getPaginasPermisos()) {
				if (pp.getPagina().getNombre().equals(pagina) && pp.getPagina().getNombreAccionDo().equals(accionDo)) {
					for (Permiso permiso : pp.getPermisos()) {
						if (permiso.getCodigo().equalsIgnoreCase(permisoPorValidar)) {
							solicitudForm.setPermitirAutoAsignacion(Boolean.TRUE);
							solicitudForm.setTipoPersona("1");
							try {
								this.poblarAgentes(solicitudForm, usuario);
							} catch (SystemException e) {
								e.printStackTrace();
							}
							return mapping.findForward(reglaNavegacion);
						}
					}
 				}
			}
		
			solicitudForm.setTipoPersona("1");
        		try {
        			this.poblarAgentes(solicitudForm, usuario);
        		} catch (SystemException e) {
        			e.printStackTrace();
        		}
		}else{
		       //CODIGO AGREGADO CUANDO NO HAYA SEGURIDAD
		        solicitudForm.setPermitirAutoAsignacion(Boolean.TRUE);
			solicitudForm.setTipoPersona("1");
			try {
				this.poblarAgentes(solicitudForm, usuario);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			return mapping.findForward(reglaNavegacion);
		}*/
		try {
			this.poblarAgentes(solicitudForm, usuario);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		solicitudForm.setTipoPersona("1");
		solicitudForm.setEsRenovacion("0");
		
		SistemaContext sistemaContext = getSistemaContext(request);
		if(sistemaContext.getSeguridadActivada()) {
			for (Rol rol : usuario.getRoles()) {
				if(rol.getDescripcion().equals(sistemaContext.getRolSuscriptorOt()) 
						|| rol.getDescripcion().equals(sistemaContext.getRolAsignadorSol()) 
						|| rol.getDescripcion().equals(sistemaContext.getRolSupervisorSuscriptor())
						|| rol.getDescripcion().equals(sistemaContext.getRolDirectorTecnico())
						|| rol.getDescripcion().equals(sistemaContext.getRolAgenteExterno())
						|| rol.getDescripcion().equals(sistemaContext.getRolSuscriptorExt())) {
					solicitudForm.setPermitirAutoAsignacion(Boolean.TRUE);
				}
			}
		}
		return mapping.findForward(reglaNavegacion);
	}
        /**
         * Guarda una solicitud, con los datos capturados por el Usuario en <code>SolicitudForm</code>
         * @param mapping
         * @param form
         * @param request
         * @param response
         * @return mapping.findForward(reglaNavegacion).
         */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		SolicitudForm solicitudForm = null;
		if(session.getAttribute("solicitudForm") != null) {
			solicitudForm = (SolicitudForm) session.getAttribute("solicitudForm");
			session.removeAttribute("solicitudForm");
		} else {
			solicitudForm = (SolicitudForm) form;
		}
		String accion = request.getParameter("accion");
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		try {
			poblarDTO(solicitudForm, solicitudDTO, request);
			
			solicitudDTO.setClaveTipoSolicitud((short)0);
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			if(accion.equals("1")) {
				solicitudDTO.setClaveEstatus((short)1);
				
			} else {
				solicitudDTO.setClaveEstatus((short)0);
			}
			solicitudDTO.setFechaCreacion(new Date());
			solicitudDTO.setFechaModificacion(new Date());
			
			if (solicitudDTO.getIdToSolicitud() != null)
				solicitudDTO = solicitudDN.actualizar(solicitudDTO);
			else
				solicitudDTO = solicitudDN.agregar(solicitudDTO);
			solicitudDN.registraEstadisticas(solicitudDTO.getIdToSolicitud(), 
					null, null,null, usuario.getNombreUsuario(), Sistema.SE_CREA_SOLICITUD);
			
			if(accion.equals("1")) {
				reglaNavegacion = Sistema.ALTERNO;
				CotizacionDTO cotizacionDTO = SolicitudAction.crearOrdenTrabajo(solicitudDTO, usuario);
				cotizacionDTO.setClaveEstatus((short)1);
				cotizacionDTO.setTipoCambio((double)1);
				CotizacionDN cotizacionDN = CotizacionDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				
				RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN.getInstancia();
				RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN.buscarDetalleRenovacionPoliza(solicitudDTO.getIdToPolizaAnterior());
				if(renovacionPolizaDTO != null){
					renovacionPolizaDN.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_ODT_PROCESO);
					renovacionPolizaDN.setDetalleNotificacion("Order de Trabajo en Proceso, Usuario: " + UtileriasWeb.obtieneNombreUsuario(request));
					renovacionPolizaDN.agregarDetalleSeguimiento(renovacionPolizaDTO.getPolizaDTO());
					cotizacionDN.modificar(cotizacionDTO);
				}else{
					cotizacionDTO = cotizacionDN.agregar(cotizacionDTO);	
				}
				
				solicitudDN.registraEstadisticas(solicitudDTO.getIdToSolicitud(), 
					cotizacionDTO.getIdToCotizacion(), null,null, usuario.getNombreUsuario(), Sistema.SE_ASIGNA_SOL_A_ODT);
				request.setAttribute("id", cotizacionDTO.getIdToCotizacion().toString());
				
				SubRamoSN subRamoSN = new SubRamoSN();
				List<SubRamoDTO> subRamos = subRamoSN.getSubRamosInCotizacion(cotizacionDTO.getIdToCotizacion());
				for(SubRamoDTO subRamoDTO : subRamos) {
					//Si es incendio, se generan tres registros para comisi�n
					if (subRamoDTO.getCodigoSubRamo().compareTo(new BigDecimal(1))==0){
						for(int i=1;i<=3;i++){
							ComisionCotizacionId id = new ComisionCotizacionId();
							id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
							id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
							id.setTipoPorcentajeComision((short)i);
							
							ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
							comisionCotizacionDTO.setId(id);
							comisionCotizacionDTO.setCotizacionDTO(cotizacionDTO);
							comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
							comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
							comisionCotizacionDTO.setClaveAutComision((short)0);
							if (i == 1){
								comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
								comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
							}
							else if (i == 2){
								comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRCI()));
								comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRCI()));
							}
							else{
								comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionPRR()));
								comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionPRR()));
							}
							ComisionCotizacionSN comisionCotizacionSN = new ComisionCotizacionSN();
							comisionCotizacionSN.agregar(comisionCotizacionDTO);
						}
					}
					//Si no es incendio, s�lo se genera un registro de comisiones usando el campo porcentajeMaximoComisionRO
					else{
						ComisionCotizacionId id = new ComisionCotizacionId();
						id.setIdTcSubramo(subRamoDTO.getIdTcSubRamo());
						id.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
						id.setTipoPorcentajeComision((short)1);
						
						ComisionCotizacionDTO comisionCotizacionDTO = new ComisionCotizacionDTO();
						comisionCotizacionDTO.setId(id);
						comisionCotizacionDTO.setCotizacionDTO(cotizacionDTO);
						comisionCotizacionDTO.setSubRamoDTO(subRamoDTO);
						comisionCotizacionDTO.setValorComisionCotizacion(new BigDecimal(0));
						comisionCotizacionDTO.setClaveAutComision((short)0);
						comisionCotizacionDTO.setPorcentajeComisionCotizacion(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
						comisionCotizacionDTO.setPorcentajeComisionDefault(new BigDecimal(subRamoDTO.getPorcentajeMaximoComisionRO()));
						
						ComisionCotizacionSN comisionCotizacionSN = new ComisionCotizacionSN();
						comisionCotizacionSN.agregar(comisionCotizacionDTO);
					}
				}
			} else {
				solicitudForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
				this.poblarForm(solicitudDTO, (SolicitudForm)form,request);
				request.setAttribute("documentos", new ArrayList<DocumentoDigitalSolicitudDTO>());
				session.removeAttribute("idSolicitud");
				session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			}
			this.poblarAgentes((SolicitudForm)form, usuario);
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
        /**
         * Crea un objeto <code>CotizacionDTO</code> con los datos de una <code>SolicitudDTO</code> y valores default.
         * @param solicitudDTO
         * @param usuario
         * @return
         * @throws ExcepcionDeAccesoADatos
         * @throws SystemException
         */
	public static CotizacionDTO crearOrdenTrabajo(SolicitudDTO solicitudDTO, Usuario usuario) throws ExcepcionDeAccesoADatos, SystemException {
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setSolicitudDTO(solicitudDTO);
		cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		cotizacionDTO.setFechaCreacion(new Date());
		cotizacionDTO.setFechaModificacion(new Date());
		cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		cotizacionDTO.setClaveEstatus((short)0);
		cotizacionDTO.setIdMedioPago(UtileriasWeb.regresaBigDecimal("2"));
		return cotizacionDTO;
	}
        /**
         * Carga un objeto <code>SolicitudDTO</code> con los datos de <code>SolicitudForm</code>, 
         * para efectos de guardado o modificaci�n.
         * @param solicitudForm objeto donde se obtendran los datos.
         * @param solicitudDTO objeto a ser llenado.
         * @param request 
         * @throws SystemException si al invocar <code>UtileriasWeb.regresaBigDecimal()</code>, se produce una excepcion.
         */
	private void poblarDTO(SolicitudForm solicitudForm, SolicitudDTO solicitudDTO, HttpServletRequest request) throws SystemException {

	        if(!UtileriasWeb.esCadenaVacia(solicitudForm.getEsRenovacion())) {
	            solicitudDTO.setEsRenovacion(Short.parseShort(solicitudForm.getEsRenovacion()));
	            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getIdPoliza()))
	            	solicitudDTO.setIdToPolizaAnterior(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdPoliza()));
	        }else{
	        	solicitudDTO.setEsRenovacion(Short.parseShort("0"));
	        	solicitudDTO.setIdToPolizaAnterior(new BigDecimal(0));
	        }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getEstatus())) {
                solicitudDTO.setClaveEstatus(Short.parseShort(solicitudForm.getEstatus()));
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getNumeroSolicitud())) {
                solicitudDTO.setIdToSolicitud(UtileriasWeb.regresaBigDecimal(solicitudForm.getNumeroSolicitud()));
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getTipoPersona())) {
                solicitudDTO.setClaveTipoPersona(Short.parseShort(solicitudForm.getTipoPersona()));
            }
            if(solicitudDTO.getClaveTipoPersona().shortValue() == 1) {
                if(!UtileriasWeb.esCadenaVacia(solicitudForm.getNombres())) {
                    solicitudDTO.setNombrePersona(solicitudForm.getNombres());
                }
                if(!UtileriasWeb.esCadenaVacia(solicitudForm.getApellidoPaterno())) {
                    solicitudDTO.setApellidoPaterno(solicitudForm.getApellidoPaterno());
                }
                if(!UtileriasWeb.esCadenaVacia(solicitudForm.getApellidoMaterno())) {
                    solicitudDTO.setApellidoMaterno(solicitudForm.getApellidoMaterno());
                }
            } else if(solicitudDTO.getClaveTipoPersona().shortValue() == 2) {
                if(!UtileriasWeb.esCadenaVacia(solicitudForm.getRazonSocial())) {
                    solicitudDTO.setNombrePersona(solicitudForm.getRazonSocial());
                }
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getTelefono())) {
                solicitudDTO.setTelefonoContacto(solicitudForm.getTelefono());
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getIdProducto())) {
                ProductoDTO productoDTO = new ProductoDTO();
                productoDTO.setIdToProducto(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdProducto()));
                ProductoDN productoDN = ProductoDN.getInstancia();
                productoDTO = productoDN.getPorId(productoDTO);
                solicitudDTO.setProductoDTO(productoDTO);
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getIdAgente())) {
                BigDecimal idAgente = UtileriasWeb.regresaBigDecimal(solicitudForm.getIdAgente());
                solicitudDTO.setCodigoAgente(idAgente);
                
                AgenteDTO agenteDTO = new AgenteDTO();
                agenteDTO.setIdTcAgente(idAgente.intValue());
                
                Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
                String nombreUsuario = usuario != null ? usuario.getNombreUsuario() : "";
                
                AgenteDN agenteDN = AgenteDN.getInstancia();
                agenteDTO = agenteDN.verDetalleAgente(agenteDTO, nombreUsuario);
                
                solicitudDTO.setNombreAgente(agenteDTO.getNombre());
                if(agenteDTO.getIdOficina()!=null){
                    solicitudDTO.setIdOficina(UtileriasWeb.regresaBigDecimal(agenteDTO.getIdGerencia()));
                    solicitudDTO.setNombreOficina(agenteDTO.getNombreGerencia());                    
                    solicitudDTO.setCodigoEjecutivo(UtileriasWeb.regresaBigDecimal(agenteDTO.getIdOficina()));
                    solicitudDTO.setNombreEjecutivo(agenteDTO.getNombreOficina());
                    solicitudDTO.setNombreOficinaAgente(agenteDTO.getNombrePromotoria());
                }
                
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getCorreos())) {
                solicitudDTO.setEmailContactos(solicitudForm.getCorreos());
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getClaveOpcionEmision())) {
                solicitudDTO.setClaveOpcionEmision(UtileriasWeb.regresaShort(solicitudForm.getClaveOpcionEmision()));
            } else {
                solicitudDTO.setClaveOpcionEmision((short)1);
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getIdUsuario())) {
                solicitudDTO.setCodigoUsuarioCreacion(solicitudForm.getIdUsuario());
            }
            if(!UtileriasWeb.esCadenaVacia(solicitudForm.getIdPoliza())) {
            	solicitudDTO.setIdToPolizaAnterior(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdPoliza()));
            } else {
            	solicitudDTO.setIdToPolizaAnterior(BigDecimal.ZERO);
            	solicitudDTO.setEsRenovacion(Short.parseShort("0"));
            }
	}
	/**
	 * Busca los documentos cargados de una solicitud, para ser presentados en una pagina.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			HttpSession session = request.getSession();
			Object idSolicitud = session.getAttribute("idSolicitud");
			session.removeAttribute("idSolicitud");			
			List<DocumentoDigitalSolicitudDTO> documentos = new ArrayList<DocumentoDigitalSolicitudDTO>();
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			if (idSolicitud==null){
				String idPadre = request.getParameter("idPadre");
				String descripcionPadre = request.getParameter("descripcionPadre");
				if(!UtileriasWeb.esCadenaVacia(idPadre) && !UtileriasWeb.esCadenaVacia(descripcionPadre)){
					if(descripcionPadre.equals("cotizacion")){
						CotizacionSN cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
						CotizacionDTO cotizacionDTO = new CotizacionDTO();
						cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(idPadre));
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						documentos = documentoDigitalSolicitudDN.listarDocumentosSolicitud(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
						reglaNavegacion = "exitosoOT";
					}
				}
			}else{
				session.setAttribute("idSolicitud", idSolicitud);
				documentos = documentoDigitalSolicitudDN.listarDocumentosSolicitud(UtileriasWeb.regresaBigDecimal(idSolicitud.toString()));
			}
			request.setAttribute("documentos", documentos);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
        /**
         * Carga los elementos necesarios del <code>SolicitudForm</code> para la pagina de borrar.
         * @param mapping
         * @param form
         * @param request
         * @param response
         * @return mapping.findForward(reglaNavegacion).
         */
	public ActionForward mostrarBorrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudForm solicitudForm = (SolicitudForm)form;
		try {
			String id = request.getParameter("id");
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			this.poblarForm(solicitudDTO, solicitudForm,request);

			HttpSession session = request.getSession();
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			
			this.listarDocumentos(mapping, solicitudForm, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga un objeto <code>SolicitudDTO</code> a partir de los datos de <code>SolicitudForm</code>
	 * @param solicitudDTO objeto de donde se obtendran los datos
	 * @param solicitudForm objeto que sera llenado.
	 * @param request
	 * @throws SystemException si al invocar <code> agenteDN.verDetalleAgente</code> se produce una exepcion.
	 */
	private void poblarForm(SolicitudDTO solicitudDTO,
			SolicitudForm solicitudForm,HttpServletRequest request) throws SystemException {
		if(solicitudDTO.getEsRenovacion() != null) {
			solicitudForm.setEsRenovacion(solicitudDTO.getEsRenovacion().toString());
		}
		if(solicitudDTO.getClaveEstatus() != null) {
			solicitudForm.setEstatus(solicitudDTO.getClaveEstatus().toString());
		}
		if(solicitudDTO.getIdToSolicitud() != null) {
			solicitudForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
		}
		if(solicitudDTO.getClaveTipoPersona() != null) {
			solicitudForm.setTipoPersona(solicitudDTO.getClaveTipoPersona().toString());
		}
		if(solicitudForm.getTipoPersona().equals("1")) {
			String nombre = "";
			if(solicitudDTO.getNombrePersona() != null) {
				solicitudForm.setNombres(solicitudDTO.getNombrePersona());
				nombre += solicitudDTO.getNombrePersona() + " ";
			}
			if(solicitudDTO.getApellidoPaterno() != null) {
				solicitudForm.setApellidoPaterno(solicitudDTO.getApellidoPaterno());
				nombre += solicitudDTO.getApellidoPaterno() + " ";
			}
			if(solicitudDTO.getApellidoMaterno() != null) {
				solicitudForm.setApellidoMaterno(solicitudDTO.getApellidoMaterno());
				nombre += solicitudDTO.getApellidoMaterno() + " ";
			}
			solicitudForm.setNombreSolicitante(nombre);
		} else if(solicitudForm.getTipoPersona().equals("2")) {
			if(solicitudDTO.getNombrePersona() != null) {
				solicitudForm.setRazonSocial(solicitudDTO.getNombrePersona());
				solicitudForm.setNombreSolicitante(solicitudDTO.getNombrePersona());
			}
		}
		if(solicitudDTO.getTelefonoContacto() != null) {
			solicitudForm.setTelefono(solicitudDTO.getTelefonoContacto());
		}
		if(solicitudDTO.getProductoDTO() != null) {
			if(solicitudDTO.getProductoDTO().getIdToProducto() != null) {
				solicitudForm.setIdProducto(solicitudDTO.getProductoDTO().getIdToProducto().toString());
			}
			if(solicitudDTO.getProductoDTO().getNombreComercial() != null) {
				solicitudForm.setNombreComercialProducto(solicitudDTO.getProductoDTO().getNombreComercial());
			}
		}
		if(solicitudDTO.getCodigoAgente() != null) {
			solicitudForm.setIdAgente(solicitudDTO.getCodigoAgente().toBigInteger().toString());
			AgenteDN agenteDN = AgenteDN.getInstancia();
			String nombreAgente = "No disponible";
			try{
			AgenteDTO agenteDTO = new AgenteDTO();
			agenteDTO.setIdTcAgente(Integer.valueOf(solicitudForm.getIdAgente()));
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			agenteDTO = agenteDN.verDetalleAgente(agenteDTO, usuario.getNombreUsuario());//getPorId(UtileriasWeb.regresaBigDecimal(solicitudForm.getIdAgente()));
			nombreAgente = agenteDTO.getNombre();
			}catch(Exception e){}
			solicitudForm.setNombreAgente(nombreAgente);
		}
		if(solicitudDTO.getEmailContactos() != null) {
			solicitudForm.setCorreos(solicitudDTO.getEmailContactos());
		}
		if(solicitudDTO.getClaveOpcionEmision() != null) {
			solicitudForm.setClaveOpcionEmision(solicitudDTO.getClaveOpcionEmision().toString());
		}
		if(solicitudDTO.getCodigoUsuarioCreacion() != null) {
			solicitudForm.setIdUsuario(solicitudDTO.getCodigoUsuarioCreacion());
		}
		if(solicitudDTO.getFechaCreacion() != null) {
			solicitudForm.setFechaSolicitud(solicitudDTO.getFechaCreacion());
		}
		if(solicitudDTO.getCodigoAgente() != null) {
			solicitudForm.setCodigoAgente(solicitudDTO.getCodigoAgente().toString());
		}
		if(solicitudDTO.getIdToPolizaAnterior() != null && solicitudDTO.getIdToPolizaAnterior().intValue() != 0) {
			solicitudForm.setIdPoliza(solicitudDTO.getIdToPolizaAnterior().toString());
		}
	}
	/**
	 * Elimina una solicitud, a partir de su <code>id</code>, asi como tambien sus dependencias
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String id="";
		try {
			HttpSession session = request.getSession();
			id = session.getAttribute("idSolicitud").toString();
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			List<DocumentoDigitalSolicitudDTO> documentos = documentoDigitalSolicitudDN.listarDocumentosSolicitud(UtileriasWeb.regresaBigDecimal(id));
			for(DocumentoDigitalSolicitudDTO documento : documentos) {
				documentoDigitalSolicitudDN.borrar(documento);
			}
			
			SolicitudDTO solicitudDTO = new SolicitudDTO();
			solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			solicitudDN.borrar(solicitudDTO);
			
			return listarFiltrado(mapping, new SolicitudForm(), request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		SolicitudForm solicitudForm = (SolicitudForm) form;
		solicitudForm.setNumeroSolicitud(id);
		return mapping.findForward(reglaNavegacion);
	}
	  /**
          * Carga los datos necesarios en <code>SolicitudForm</code> para presentar la pagina de modificar.
          * @param mapping
          * @param form
          * @param request
          * @param response
          * @return mapping.findForward(reglaNavegacion).
          */
	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudForm solicitudForm = (SolicitudForm)form;
		try {
			String id = request.getParameter("id");
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			
			HttpSession session = request.getSession();
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			session.removeAttribute("fechaCreacion");
			session.setAttribute("fechaCreacion", solicitudDTO.getFechaCreacion());
			this.listarDocumentos(mapping, solicitudForm, request, response);
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			try {
				this.poblarAgentes(solicitudForm, usuario);
			} catch (SystemException e) {
				e.printStackTrace();
			}
			if(solicitudDTO.getClaveEstatus().intValue() == Sistema.ESTATUS_SOL_PENDIENTE_AUTORIZACION){
				solicitudDTO.setClaveEstatus(Sistema.ESTATUS_SOL_EN_PROCESO);
				solicitudDN.actualizar(solicitudDTO);
				
				RenovacionPolizaDN renovacionPolizaDN = RenovacionPolizaDN.getInstancia();
				RenovacionPolizaDTO renovacionPolizaDTO = renovacionPolizaDN.buscarDetalleRenovacionPoliza(solicitudDTO.getIdToPolizaAnterior());
				if(renovacionPolizaDTO != null){
					
					renovacionPolizaDN.setTipoMovimiento(SeguimientoRenovacionDTO.TIPO_SOLICITUD_EN_PROCESO);
					renovacionPolizaDN.setDetalleNotificacion("Solicitud en proceso Usuario: " + usuario.getNombreUsuario());
					renovacionPolizaDN.agregarDetalleSeguimiento(renovacionPolizaDTO.getPolizaDTO());
				}
			}
			this.poblarForm(solicitudDTO, solicitudForm,request);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
     /**
      * Actualiza una solicitud con los datos que cambio o agreg� el usuario.
      * @param mapping
      * @param form
      * @param request
      * @param response
      * @return mapping.findForward(reglaNavegacion).
      */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudForm solicitudForm = (SolicitudForm) form;
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		try {
			HttpSession session = request.getSession();
			poblarDTO(solicitudForm, solicitudDTO, request);

			solicitudDTO.setClaveTipoSolicitud((short)0); //modificar  0:Nueva, 1:Re-expedici�n, 2:Endoso

			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			solicitudDTO.setFechaModificacion(new Date());
			solicitudDTO.setFechaCreacion((Date)session.getAttribute("fechaCreacion"));
			session.removeAttribute("fechaCreacion");

			solicitudDTO = solicitudDN.actualizar(solicitudDTO);

			request.setAttribute("documentos", new ArrayList<DocumentoDigitalSolicitudDTO>());
			//this.listarTodos(request);
			return listarFiltrado(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Actualiza el estatus de una solicitud, de en proceso a terminada.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward terminar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			solicitudDTO.setClaveEstatus((short)2);
			solicitudDN.actualizar(solicitudDTO);
			return listarFiltrado(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Elimina un documento de una solicitud, por el <code>id</code> de la solicitud y el 
	 * <code>documentoDigitalSolicitudDTO.getIdToControlArchivo()</code>
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
			DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = documentoDigitalSolicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			controlArchivoDTO.setIdToControlArchivo(documentoDigitalSolicitudDTO.getIdToControlArchivo());
			ControlArchivoDN controlArchivoD = ControlArchivoDN.getInstancia();
			controlArchivoD.borrar(controlArchivoDTO);
			
			documentoDigitalSolicitudDN.borrar(documentoDigitalSolicitudDTO);
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Carga los elementos necesarion en <code>SolicitudForm</code> para presentar la pagina de Detalle.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SolicitudForm solicitudForm = (SolicitudForm)form;
		try {
			String id = request.getParameter("id");
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			this.poblarForm(solicitudDTO, solicitudForm,request);

			HttpSession session = request.getSession();
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));
			
			this.listarDocumentos(mapping, solicitudForm, request, response);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Metodo creado , para que se efectue la validacion, por el struts config y
	 * sea remplazado el objeto <code>SolicitudForm</code> de la sesion por el
	 * del <code>request</code> .
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward validarDatosSolicitud(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		final HttpSession session = request.getSession();
		final SolicitudForm forma = (SolicitudForm) form;
		session.removeAttribute("solicitudForm");
		session.setAttribute("solicitudForm", form);
		// RMP Valida que el agente este activo y con cedula vigente
		final SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		final Set<String> errores = solicitudDN.validarAgente(Integer.valueOf(forma.getIdAgente()), getUsuarioMidas(
				request).getNombreUsuario());
		final ActionMessages actionMessages = new ActionMessages();
		if (CollectionUtils.isEmpty(errores)) {
			forma.setMensaje(null);
			forma.setTipoMensaje(null);
		} else {
			actionMessages.add("idAgente", new ActionMessage("solicitud.validacion"));
			agregarMensajes(errores, Sistema.ERROR, forma, getResources(request).getMessage("solicitud.validacion"));
		}
		saveErrors(request, actionMessages);
		return mapping.findForward(Sistema.EXITOSO);
	}
	/**
	 * Action para presentar acciones , para autoasignado de una solicitud.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward mostrarAcciones(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Obtiene el listado de agentes y los setea en <code> SolicitudForm</code>.
	 * @param solicitudForm
	 * @param usuario
	 * @throws SystemException si se levanta una exepcion al invocar <code>agenteDN.getAgentesPorUsuario</code>.
	 */
	private void poblarAgentes(SolicitudForm solicitudForm, Usuario usuario)
			throws SystemException {
		final AgenteDN agenteDN = AgenteDN.getInstancia();
		List<AgenteDTO> agentes = null;
		try {
			agentes = AgenteEspecialDN.getInstancia().poblarAgenteEspecial(
					usuario, solicitudForm);
			if (!solicitudForm.getBloqueoDatosAgente()) {
				agentes = agenteDN.getAgentesPorUsuario(usuario);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (agentes == null || agentes.isEmpty()) {
			solicitudForm.setAgentes(new ArrayList<AgenteDTO>());
		} else {
			solicitudForm.setAgentes(agentes);
		}
	}
	
	/*public ActionForward crearOrdenTrabajo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			String id = request.getParameter("id");
			SolicitudDN solicitudDN = SolicitudDN.getInstancia();
			SolicitudDTO solicitudDTO = solicitudDN.getPorId(UtileriasWeb.regresaBigDecimal(id));

			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			CotizacionDTO cotizacionDTO = this.crearOrdenTrabajo(solicitudDTO, usuario);
			CotizacionDN cotizacionDN = CotizacionDN.getInstancia();
			cotizacionDTO = cotizacionDN.agregar(cotizacionDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}*/


	/**
	 * Carga datos de la solicitud a partir de una poliza existente.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward agregarApartirPoliza(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		HttpSession session = request.getSession();
		SolicitudForm solicitudForm = null;
		if(session.getAttribute("solicitudForm") != null) {
			solicitudForm = (SolicitudForm) session.getAttribute("solicitudForm");
			session.removeAttribute("solicitudForm");
		} else {
			solicitudForm = (SolicitudForm) form;
		}
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		try {
			solicitudForm.setIdProducto(null);
			poblarDTO(solicitudForm, solicitudDTO, request);

			BigDecimal idToPoliza = solicitudDTO.getIdToPolizaAnterior();
			solicitudDTO.setClaveTipoSolicitud((short)0);
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			solicitudDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			solicitudDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			solicitudDTO.setClaveEstatus((short)0);
			solicitudDTO.setFechaCreacion(new Date());
			solicitudDTO.setFechaModificacion(new Date());

			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
			solicitudDTO.setProductoDTO(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO());
			List<DocumentoDigitalSolicitudDTO> documentos = DocumentoDigitalSolicitudDN.getInstancia().listarDocumentosSolicitud(polizaDTO.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud());
			for(DocumentoDigitalSolicitudDTO documento : documentos) {
				ControlArchivoDTO controlArchivo = new ControlArchivoDTO();
				controlArchivo.setClaveTipo(documento.getControlArchivo().getClaveTipo());
				controlArchivo.setNombreArchivoOriginal(documento.getControlArchivo().getNombreArchivoOriginal());
				controlArchivo = ControlArchivoDN.getInstancia().agregar(controlArchivo);

				VaultAction.copyFile(documento.getControlArchivo(), controlArchivo);
				documento.setIdToDocumentoDigitalSolicitud(null);
				documento.setSolicitudDTO(solicitudDTO);
			}
			solicitudDTO.setDocumentoDigitalSolicitudDTOs(documentos);
			solicitudDTO = solicitudDN.agregar(solicitudDTO);
			solicitudDN.registraEstadisticas(solicitudDTO.getIdToSolicitud(), 
				null, null,null, usuario.getNombreUsuario(), Sistema.SE_CREA_SOLICITUD);
			solicitudForm.setNumeroSolicitud(solicitudDTO.getIdToSolicitud().toString());
			this.poblarForm(solicitudDTO, (SolicitudForm)form,request);
			request.setAttribute("documentos", solicitudDTO.getDocumentoDigitalSolicitudDTOs());
			session.removeAttribute("idSolicitud");
			session.setAttribute("idSolicitud", new Integer(solicitudDTO.getIdToSolicitud().intValue()));

			this.poblarAgentes((SolicitudForm)form, usuario);

		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void  modificarNotaCobertura(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String idContratoFacultativo = request.getParameter("id");
		String nota = request.getParameter("nota");
		String ajustadorNombrado = request.getParameter("ajustadorNombrado");
				
		ContratoFacultativoDTO contratoFacultativoDTO = new ContratoFacultativoDTO();
		ContratoFacultativoDN contratoFacultativoDN = ContratoFacultativoDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		
		try{			
			contratoFacultativoDTO.setIdTmContratoFacultativo(new BigDecimal(idContratoFacultativo));
			contratoFacultativoDTO.setNotaCobertura(nota);
			contratoFacultativoDTO.setAjustadorNombrado(ajustadorNombrado);
			
			
			if (idContratoFacultativo!=null){	
				contratoFacultativoDN.cambiarNotaCobertura(contratoFacultativoDTO);
			}
			
		}catch(SystemException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);		
		}
	}
	
	private SistemaContext getSistemaContext(HttpServletRequest request) {
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());		
		return webApplicationContext.getBean(SistemaContext.class);
	}
	
}