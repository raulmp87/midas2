package mx.com.afirme.midas.solicitud;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

/**
 * @author Fernando Alonzo
 */
public class SolicitudForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String estatus;
	private String numeroSolicitud;
	private String tipoPersona;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String telefono;
	private String correos;
	private String claveOpcionEmision;
	private String razonSocial;
	private String idProducto;
	private String nombreComercialProducto;
	private Date fechaSolicitud;
	private String idAgente;
	private String nombreAgente;
	private String idUsuario;
	private String accion;
	private Boolean permitirAutoAsignacion;
	private Boolean bloqueoDatosAgente;
	private List<AgenteDTO> agentes;
	//Datos de resumen de solicitud
	private String nombreSolicitante;
	private String numeroSolicitudFormateado;
	private String descripcionEstatus;
	
	private String numeroOrdenTrabajoFormateado;
	private String nombreUsuarioAsignacionODT;
	private String descripcionEstatusODT;
	private String tipoPoliza;
	private String oficina;
	private Date fechaInicioVigencia;
	private Date fechaFinVigencia;
	private String nombreContratante;
	private String nombreAsegurado;
	private String moneda;
	private Date fechaCreacion;
	private String nombreUsuarioAsignacionCOT;
	private String codigoAgente;
	
	private String numPoliza;
	private String idPoliza;
	private String apartirPoliza;
	private Date fechaEmision;
	private String numeroEndoso;
	private String nombreUsuarioAsignacionEmision;
	private Date fechaODT;
	private Date fechaCOT;
	private String esRenovacion;
	
	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getNombreSolicitante() {
		this.nombreSolicitante = "";
		nombreSolicitante += this.nombres!=null?this.nombres:"";
		nombreSolicitante += " ";
		nombreSolicitante += this.apellidoPaterno!=null?this.apellidoPaterno:"";
		nombreSolicitante += " ";
		nombreSolicitante += this.apellidoMaterno!=null?this.apellidoMaterno:"";
		
		return nombreSolicitante + "."; 
	}
	
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNombres() {
		return nombres;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setCorreos(String correos) {
		this.correos = correos;
	}

	public String getCorreos() {
		return correos;
	}

	public void setClaveOpcionEmision(String claveOpcionEmision) {
		this.claveOpcionEmision = claveOpcionEmision;
	}

	public String getClaveOpcionEmision() {
		return claveOpcionEmision;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setIdAgente(String idAgente) {
		this.idAgente = idAgente;
	}

	public String getIdAgente() {
		return idAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getAccion() {
		return accion;
	}

	public void setPermitirAutoAsignacion(Boolean permitirAutoAsignacion) {
		this.permitirAutoAsignacion = permitirAutoAsignacion;
	}

	public Boolean getPermitirAutoAsignacion() {
		return permitirAutoAsignacion;
	}

	public Boolean getBloqueoDatosAgente() {
		return bloqueoDatosAgente;
	}

	public void setBloqueoDatosAgente(Boolean bloqueoDatosAgente) {
		this.bloqueoDatosAgente = bloqueoDatosAgente;
	}

	public void setAgentes(List<AgenteDTO> agentes) {
		this.agentes = agentes;
	}

	public List<AgenteDTO> getAgentes() {
		return agentes;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		if(request.getParameter("idToProducto") != null) {
			this.idProducto = request.getParameter("idToProducto");
		}
		ActionErrors errors = super.validate(mapping, request);
		if (errors == null) {
			errors = new ActionErrors();
		}
		if(this.agentes == null) {
		       	Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			mx.com.afirme.midas.interfaz.agente.AgenteDN agenteDN = mx.com.afirme.midas.interfaz.agente.AgenteDN.getInstancia();
			List<AgenteDTO> agentes;
			try {
			    agentes = agenteDN.getAgentesPorUsuario(usuario);
				if(agentes != null && !agentes.isEmpty()) {
					this.agentes = agentes;
				} else {
					this.agentes = new ArrayList<AgenteDTO>();
				}
			} catch (SystemException e) {
				e.printStackTrace();
				this.agentes = new ArrayList<AgenteDTO>();
			}
			
		}
		return errors;
	}

	public String getNumeroSolicitudFormateado() {
		return numeroSolicitudFormateado;
	}

	public void setNumeroSolicitudFormateado(String numeroSolicitudFormateado) {
		this.numeroSolicitudFormateado = numeroSolicitudFormateado;
	}

	public String getDescripcionEstatus() {
		return descripcionEstatus;
	}

	public void setDescripcionEstatus(String descripcionEstatus) {
		this.descripcionEstatus = descripcionEstatus;
	}

	public String getNumeroOrdenTrabajoFormateado() {
		return numeroOrdenTrabajoFormateado;
	}

	public void setNumeroOrdenTrabajoFormateado(String numeroOrdenTrabajoFormateado) {
		this.numeroOrdenTrabajoFormateado = numeroOrdenTrabajoFormateado;
	}

	public String getNombreUsuarioAsignacionODT() {
		return nombreUsuarioAsignacionODT;
	}

	public void setNombreUsuarioAsignacionODT(String nombreUsuarioAsignacionODT) {
		this.nombreUsuarioAsignacionODT = nombreUsuarioAsignacionODT;
	}

	public String getDescripcionEstatusODT() {
		return descripcionEstatusODT;
	}

	public void setDescripcionEstatusODT(String descripcionEstatusODT) {
		this.descripcionEstatusODT = descripcionEstatusODT;
	}

	public String getTipoPoliza() {
		return tipoPoliza;
	}

	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombreUsuarioAsignacionCOT() {
		return nombreUsuarioAsignacionCOT;
	}

	public void setNombreUsuarioAsignacionCOT(String nombreUsuarioAsignacionCOT) {
		this.nombreUsuarioAsignacionCOT = nombreUsuarioAsignacionCOT;
	}

	public void setCodigoAgente(String codigoAgente) {
		this.codigoAgente = codigoAgente;
	}

	public String getCodigoAgente() {
		return codigoAgente;
	}

	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}

	public String getNumPoliza() {
		return numPoliza;
	}

	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}

	public String getIdPoliza() {
		return idPoliza;
	}

	public void setApartirPoliza(String apartirPoliza) {
		this.apartirPoliza = apartirPoliza;
	}

	public String getApartirPoliza() {
		return apartirPoliza;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public String getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNombreUsuarioAsignacionEmision(
			String nombreUsuarioAsignacionEmision) {
		this.nombreUsuarioAsignacionEmision = nombreUsuarioAsignacionEmision;
	}

	public String getNombreUsuarioAsignacionEmision() {
		return nombreUsuarioAsignacionEmision;
	}

	public void setFechaODT(Date fechaODT) {
		this.fechaODT = fechaODT;
	}

	public Date getFechaODT() {
		return fechaODT;
	}

	public void setFechaCOT(Date fechaCOT) {
		this.fechaCOT = fechaCOT;
	}

	public Date getFechaCOT() {
		return fechaCOT;
	}

	public String getEsRenovacion() {
		return esRenovacion;
	}

	public void setEsRenovacion(String esRenovacion) {
		this.esRenovacion = esRenovacion;
	}
}