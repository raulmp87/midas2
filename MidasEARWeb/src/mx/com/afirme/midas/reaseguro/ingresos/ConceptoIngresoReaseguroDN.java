package mx.com.afirme.midas.reaseguro.ingresos;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratos.ingreso.ConceptoIngresoReaseguroDTO;
import mx.com.afirme.midas.sistema.SystemException;

public class ConceptoIngresoReaseguroDN {
	private static final ConceptoIngresoReaseguroDN INSTANCIA = new ConceptoIngresoReaseguroDN();
	
	public static ConceptoIngresoReaseguroDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public List<ConceptoIngresoReaseguroDTO> obtenerConceptosIngresoReaseguro() throws SystemException{
		ConceptoIngresoReaseguroSN conceptoIngresoReaseguroSN = new ConceptoIngresoReaseguroSN();
		return conceptoIngresoReaseguroSN.obtenerConceptosIngresoReaseguro();
	}
	
	public ConceptoIngresoReaseguroDTO getPorId(BigDecimal idConcepto) throws SystemException{
		ConceptoIngresoReaseguroSN conceptoIngresoReaseguroSN = new ConceptoIngresoReaseguroSN();
		return conceptoIngresoReaseguroSN.getPorId(idConcepto);
	}


}
