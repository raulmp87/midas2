package mx.com.afirme.midas.reaseguro.ingresos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroDTO;
import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class IngresoReaseguroSN {
	IngresoReaseguroFacadeRemote beanRemoto;
	
	public IngresoReaseguroSN(){
		LogDeMidasWeb.log("Entrando en IngresoReaseguroSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		try {
			beanRemoto = serviceLocator.getEJB(IngresoReaseguroFacadeRemote.class);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<IngresoReaseguroDTO> obtenerIngresosPendientes(){
		return beanRemoto.obtenerIngresosPendientes();	
	}
	
	public IngresoReaseguroDTO getPorId(BigDecimal id){
		return beanRemoto.findById(id);
	}
	
	public boolean mismoReaseguradorCorredor(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta){
		return beanRemoto.mismoReaseguradorCorredor(listaEstadosCuenta);
	}
	
	public void relacionarIngreso(IngresoReaseguroDTO ingresoReaseguro, List<EstadoCuentaDecoradoDTO> lista){
		beanRemoto.relacionarIngreso(ingresoReaseguro, lista);
	}

}
