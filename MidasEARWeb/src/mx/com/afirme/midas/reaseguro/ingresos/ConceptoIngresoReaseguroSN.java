package mx.com.afirme.midas.reaseguro.ingresos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.ingreso.ConceptoIngresoReaseguroDTO;
import mx.com.afirme.midas.contratos.ingreso.ConceptoIngresoReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class ConceptoIngresoReaseguroSN {
	private ConceptoIngresoReaseguroFacadeRemote beanRemoto;
	
	public ConceptoIngresoReaseguroSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ConceptoIngresoReaseguroSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ConceptoIngresoReaseguroFacadeRemote.class);
		LogDeMidasWeb.log("Bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<ConceptoIngresoReaseguroDTO> obtenerConceptosIngresoReaseguro(){
		return beanRemoto.findAll();
	}
	
	public ConceptoIngresoReaseguroDTO getPorId(BigDecimal idBd){
		Byte id = idBd.byteValue();
		return beanRemoto.findById(id);
	}
	
}
