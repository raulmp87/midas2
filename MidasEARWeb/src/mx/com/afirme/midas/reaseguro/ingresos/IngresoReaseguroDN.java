package mx.com.afirme.midas.reaseguro.ingresos;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.ingreso.IngresoReaseguroDTO;
import mx.com.afirme.midas.contratos.ingresos.IngresosReaseguroDN;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.reaseguro.movimiento.MovimientoReaseguroSN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class IngresoReaseguroDN {
	private static final IngresoReaseguroDN INSTANCIA = new IngresoReaseguroDN();
	
	public static final int NACIONAL = 484;
	public static final int DOLARES = 840;

	public static IngresoReaseguroDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public List<IngresoReaseguroDTO> obtenerIngresosPendientes(){
		IngresoReaseguroSN ingresoReaseguroSN = new IngresoReaseguroSN();
		return ingresoReaseguroSN.obtenerIngresosPendientes();
	}
	
	public IngresoReaseguroDTO getPorId(BigDecimal id){
		IngresoReaseguroSN ingresoReaseguroSN = new IngresoReaseguroSN();
		return ingresoReaseguroSN.getPorId(id);
	}
	
	public void validarRelacionIngresoEdosCta(String[] idsEdosCta, String[] idIngreso, String mensaje) throws SystemException, ExcepcionDeLogicaNegocio{
		List<EstadoCuentaDecoradoDTO> listaEstadosCuenta = new ArrayList<EstadoCuentaDecoradoDTO>();
		
		if (idIngreso.length > 1){
			throw new ExcepcionDeLogicaNegocio(this.getClass().getCanonicalName(), 
					UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.error.validarrelacioningresos.ingresoNoSeleccionado"));
		}else{
			listaEstadosCuenta = EstadoCuentaDN.getINSTANCIA().obtenerEstadosCuentaDecoradoPorIds(idsEdosCta);
			
			if (!verificarReaseguradores(listaEstadosCuenta)){
				throw new ExcepcionDeLogicaNegocio(this.getClass().getCanonicalName(), 
						UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.error.validarrelacioningresos.reaseguradoresDiferentes"));
			}
			
		}
		
	}
	
	public boolean tipoCambioNecesario(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta, IngresoReaseguroDTO ingresoReaseguroDTO){
		boolean resultado = false;
		for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : listaEstadosCuenta) {
			if (estadoCuentaDecoradoDTO.getIdMoneda() != ingresoReaseguroDTO.getIdMoneda()){
				resultado = true;
			}
		}
		return resultado;
	}
	
	public boolean verificarReaseguradores(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta){
		IngresoReaseguroSN ingresoReaseguroSN = new IngresoReaseguroSN();
		return ingresoReaseguroSN.mismoReaseguradorCorredor(listaEstadosCuenta);
	}
	
	public void relacionarIngreso(IngresoReaseguroDTO ingresoReaseguro, List<EstadoCuentaDecoradoDTO> lista, double sumaOtrosMontos, String nombreUsuario) throws RuntimeException{
		DecimalFormat formatoDec = new DecimalFormat("0.0000");
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
		numberFormat.setMaximumFractionDigits(4);
		double suma = new Double(formatoDec.format(sumaOtrosMontos));
		double factorTipoCambio;
		boolean banderaTC = false;
		String sumaStr;
		String montoIngresoStr;
		if (ingresoReaseguro.getMonto().doubleValue() > 0){
				for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : lista) {
					if (estadoCuentaDecoradoDTO.getMontoIngreso().doubleValue() > 0){
						factorTipoCambio = 1;
						if (estadoCuentaDecoradoDTO.getIdMoneda() != ingresoReaseguro.getIdMoneda()){
							// La regla de negocio indica que el Tipo de cambio siempre ser� proporcionado como Pesos por D�lar
							factorTipoCambio = ingresoReaseguro.getTipoCambio().doubleValue();
							if (ingresoReaseguro.getIdMoneda() == NACIONAL){ // Cuando el factor es de D�lares a Pesos
								banderaTC = false; //Determina que el Tipo de Cambio va a ser aplicado como producto (multiplicaci�n)
							}else{
								if (ingresoReaseguro.getIdMoneda() == DOLARES){ // Cuando el factor es de Pesos a D�lares
									if (ingresoReaseguro.getTipoCambio().equals(new Double("0")))
										throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.relacionaringreso.error.tipocambiocero"));
									else{
										banderaTC = true; //Determina que el Tipo de Cambio va a ser aplicado como dividendo (divisi�n)
									}
								}
							}
						}
						if (banderaTC)
							suma = new Double(formatoDec.format(suma)).doubleValue() + new Double(formatoDec.format(estadoCuentaDecoradoDTO.getMontoIngreso().doubleValue())).doubleValue() / new Double(formatoDec.format(factorTipoCambio)).doubleValue();
						else
							suma = new Double(formatoDec.format(suma)).doubleValue() + new Double(formatoDec.format(estadoCuentaDecoradoDTO.getMontoIngreso().doubleValue())).doubleValue() * new Double(formatoDec.format(factorTipoCambio)).doubleValue();
					}else{
						throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.relacionaringreso.error.montocero"));
					}
			}
			sumaStr = formatoDec.format(suma);
			montoIngresoStr = formatoDec.format(ingresoReaseguro.getMonto().doubleValue());
			if (!sumaStr.equals(montoIngresoStr) ){
				suma = ingresoReaseguro.getMonto().doubleValue() - suma;
				String montoFaltante = "";
				if (suma > 0)
					montoFaltante = "<br/>Falta por cubrir la cantidad de " + numberFormat.format(suma);
				else
					montoFaltante = "<br/>Se ha excedido el monto de ingreso por la cantidad de " + numberFormat.format(suma*-1);
				
				if (ingresoReaseguro.getIdMoneda() == NACIONAL)
					montoFaltante = montoFaltante + " MXN";
				if (ingresoReaseguro.getIdMoneda() == DOLARES)
					montoFaltante = montoFaltante + " USD";
				
				throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.relacionaringreso.error.validarmonto") + montoFaltante);
			}

			IngresoReaseguroSN ingresoReaseguroSN = new IngresoReaseguroSN();
			try{
				String identificadorContable = "";
				//String idIngresoRea = "";
				
				if (lista.size() > 0){
					ingresoReaseguroSN.relacionarIngreso(ingresoReaseguro, lista);
					//idIngresoRea = ingresoReaseguro.getIdIngresoReaseguro().toString();					
					try {
						List<ContabilidadSiniestroDTO> movimientos = 
							getMovimientosReservaPorContabilizar(ingresoReaseguro, lista, nombreUsuario);
						identificadorContable = getIdentificadorContable(movimientos.get(0));
						
						List<AsientoContableDTO> asientosContables = null;
						asientosContables = contabilizar(identificadorContable, nombreUsuario);
						if(asientosContables==null){
							throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.errorContabilizado"));
						}
						if(asientosContables.size()>0){
							ingresoReaseguro.setContabiliza(1);
						}else{
							ingresoReaseguro.setContabiliza(0);
						}
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				}else{
					ingresoReaseguro.setEstatus(1); //Este es el caso cuando no hay Edos Cuenta, solamente hay 'Otros Conceptos', el IngresoReaseguro no se persiste, solamente retorna el estatus en 1
					try {
						IngresosReaseguroDN.getInstancia().modificarIngresos(ingresoReaseguro);
					} catch (SystemException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}catch(RuntimeException e){
				throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.errorInesperado"));
			}

		}else{
			throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "reaseguro.ingreso.administrar.ingresos.relacionaringreso.error.montoingresocero"));
		}
	}
	
	private List<AsientoContableDTO> contabilizar(String idToReporteSiniestro, String usuario) throws SystemException{
		List<AsientoContableDTO> asientosContables = null;
		
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		asientosContables =	movimientoReaseguroSN.contabilizaMovimientos(idToReporteSiniestro, Sistema.REASEGURO_IN, usuario);
		/*asientosContables = asientoContableDN.contabilizaMovimientos(idToReporteSiniestro, Sistema.REASEGURO_IN);
		MovimientoReaseguroDN movimientoReaseguroDN = MovimientoReaseguroDN.getInstancia();*/		
		return asientosContables;
	}
	
	private List<ContabilidadSiniestroDTO> getMovimientosReservaPorContabilizar(IngresoReaseguroDTO ingresoReaseguro, List<EstadoCuentaDecoradoDTO> lista, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		List<ContabilidadSiniestroDTO> coberturasPorContabilizar = new ArrayList<ContabilidadSiniestroDTO>();
		
		BigDecimal numeroPoliza = lista.get(0).getIdPoliza(); //reservaDTO.getReporteSiniestroDTO().getNumeroPoliza();	
		String numeroReporte = ingresoReaseguro.getIdIngresoReaseguro().toString(); //reservaDTO.getReporteSiniestroDTO().getNumeroReporte();
		if (numeroPoliza == null){
			numeroPoliza = new BigDecimal("0") ;
		}
		BigDecimal idMoneda;	
		PolizaSoporteDanosDTO poliza = getPolizaPorNumeroPoliza(numeroPoliza);		
		if (lista.get(0).getIdPoliza() == null ){
			idMoneda = new BigDecimal("1") ;
		}else{
			idMoneda = poliza.getIdMoneda();	
		}
			
		String fechaAplico = UtileriasWeb.getFechaString(Calendar.getInstance().getTime());
		String[] fecha = fechaAplico.split("/");
		
		Double tipoCambio = ingresoReaseguro.getTipoCambio(); //getTipoCambioActual(usuario, (short)idMoneda.intValue());
		
		for(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : lista){
			//SoporteContabilidadSiniestroDTO sc = coberturasReserva.get(key); 
			
			//if(Math.round(sc.getEstimacion().doubleValue()) != 0){				
				ContabilidadSiniestroDTO contabilidad = new ContabilidadSiniestroDTO();
				ContabilidadSiniestroId contabilidadId = new ContabilidadSiniestroId();
				String conceptoPoliza = 
					getConceptoPoliza(numeroReporte, numeroPoliza.toString());
				
				contabilidadId.setIdToReporteSiniestro(ingresoReaseguro.getIdIngresoReaseguro());
				contabilidadId.setIdTcMovimientoSiniestro(estadoCuentaDecoradoDTO.getIdEstadoCuenta());
				contabilidadId.setIdRegistro(estadoCuentaDecoradoDTO.getIdEstadoCuenta());			
				contabilidadId.setIdSubr(estadoCuentaDecoradoDTO.getSubRamoDTO().getIdTcSubRamo());
				contabilidadId.setCveConceptoMov(Sistema.REASEGURO_IN);
				
				contabilidad.setId(contabilidadId);
				contabilidad.setIdRamo(estadoCuentaDecoradoDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo());
				//contabilidad.setConceptoPol("Siniestro" + numeroReporte); 
				contabilidad.setConceptoPol(conceptoPoliza);
				contabilidad.setIdMoneda(idMoneda); 
				contabilidad.setTipoCambio(tipoCambio); 
				
				//contabilidad.setCveConceptoMov(conceptoPoliza);
				contabilidad.setAuxiliar(new Integer(1000)); // constante
				contabilidad.setCCosto("1.101"); // constante
				contabilidad.setTipoPersona(" "); // este no se pasa de acuerdo a la especificacion original
				contabilidad.setImpNeto(ingresoReaseguro.getMonto());
				contabilidad.setImpBonificacion(new Double(0)); // constante
				contabilidad.setImpDescuento(new Double(0)); // constante
				contabilidad.setImpDerechos(new Double(0)); // constante
				contabilidad.setImpRecargos(new Double(0));// constante
				contabilidad.setImpIva(new Double(0));// constante
				contabilidad.setImpComision(new Double(0)); // constante
				contabilidad.setImpGastos(new Double(0)); // constante
				contabilidad.setImpOtrosConc(new Double(0)); //constante
				contabilidad.setImpOtrosImp(new Double(0)); // constante
				contabilidad.setImpIvaRet(new Double(0));
				contabilidad.setImpIsrRet(new Double(0));
				String conceptoMov = "Movimiento "+ fecha[2] +"/"+numeroReporte+"/"+estadoCuentaDecoradoDTO.getSubRamoDTO().getRamoDTO().getIdTcRamo()+"/"+estadoCuentaDecoradoDTO.getIdEstadoCuenta()+"/"+Sistema.REASEGURO_IN;
				contabilidad.setConceptoMov(conceptoMov);
				contabilidad.setUsuario(nombreUsuario); 
				BigDecimal agente;
				if(lista.get(0).getIdPoliza() == null){
					agente = new BigDecimal("0");
				}else{
					agente = poliza.getCodigoAgente();
				}
				contabilidad.setIdAgente(agente);				
				coberturasPorContabilizar.add(contabilidad);				
				LogDeMidasWeb.log(contabilidad.toString(), Level.INFO, null);
			
		}
		
		return coberturasPorContabilizar;		
	}
	
	private PolizaSoporteDanosDTO getPolizaPorNumeroPoliza(BigDecimal numeroPoliza) throws ExcepcionDeAccesoADatos, SystemException{
		PolizaSoporteDanosDTO poliza = null;
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		poliza = soporteDanosDN.getDatosGeneralesPoliza(numeroPoliza);
		
		return poliza;
	}
	
	private String getConceptoPoliza(String numeroReporte, String numeroPoliza){
		StringBuilder sb = new StringBuilder();
		sb.append("Reaseguro ");
		sb.append(numeroReporte);
		sb.append("/P-");
		sb.append(numeroPoliza);
		
		return sb.toString();		
	}
	
	private String getIdentificadorContable(ContabilidadSiniestroDTO contabilidadSiniestroDTO){
		ContabilidadSiniestroId id = contabilidadSiniestroDTO.getId();
		
		StringBuilder sb = new StringBuilder();
		sb.append(id.getIdToReporteSiniestro().toString());
		sb.append("|");
		sb.append(id.getIdTcMovimientoSiniestro().toString());
		sb.append("|");
		sb.append(id.getIdRegistro());
		sb.append("|");
		sb.append(id.getCveConceptoMov());
		
		return sb.toString();
	}

}
