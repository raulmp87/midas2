package mx.com.afirme.midas.reaseguro.ingresos;

import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.contratos.ingreso.ConceptoIngresoReaseguroDTO;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.interfaz.ingreso.referencia.ReferenciaIngresoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class AdministrarIngresosForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9046622742444483940L;
	private String idTcRamo;
	private String idTcSubRamo;
	private String idEjercicio;
	private String idTcMoneda;
	private String tipoReaseguro;
	private String idRetencion;
	private String idtcReaseguradorCorredor;
	private String idSuscripcion;
	private String idConcepto;
	private String tipoCambio;
	private String idIngreso;
	private String tipoCambioNecesario;
	private String idReferenciaExterna;
	
	// Listas
	private List<MonedaDTO> monedaDTOList;
	private List<TipoReaseguroDTO> tipoReaseguroDTOList;
	private List<EjercicioDTO> ejercicioDTOList;
	private List<ConceptoIngresoReaseguroDTO> conceptoIngresoReaseguroDTOList;
	private List<ReferenciaIngresoDTO> listaReferenciasIngreso;
 
	public String getIdTcRamo() {
		return idTcRamo;
	}

	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}

	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	public String getIdEjercicio() {
		return idEjercicio;
	}

	public void setIdEjercicio(String idEjercicio) {
		this.idEjercicio = idEjercicio;
	}

	public String getIdTcMoneda() {
		return idTcMoneda;
	}

	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}

	public List<MonedaDTO> getMonedaDTOList() {
		return monedaDTOList;
	}

	public void setMonedaDTOList(List<MonedaDTO> monedaDTOList) {
		this.monedaDTOList = monedaDTOList;
	}

	public List<TipoReaseguroDTO> getTipoReaseguroDTOList() {
		return tipoReaseguroDTOList;
	}

	public void setTipoReaseguroDTOList(List<TipoReaseguroDTO> tipoReaseguroDTOList) {
		this.tipoReaseguroDTOList = tipoReaseguroDTOList;
	}

	public String getTipoReaseguro() {
		return tipoReaseguro;
	}

	public void setTipoReaseguro(String tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}

	public String getIdRetencion() {
		return idRetencion;
	}

	public void setIdRetencion(String idRetencion) {
		this.idRetencion = idRetencion;
	}

	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}

	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}

	public String getIdSuscripcion() {
		return idSuscripcion;
	}

	public void setIdSuscripcion(String idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}

	public List<EjercicioDTO> getEjercicioDTOList() {
		return ejercicioDTOList;
	}

	public void setEjercicioDTOList(List<EjercicioDTO> ejercicioDTOList) {
		this.ejercicioDTOList = ejercicioDTOList;
	}

	public String getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(String idConcepto) {
		this.idConcepto = idConcepto;
	}

	public List<ConceptoIngresoReaseguroDTO> getConceptoIngresoReaseguroDTOList() {
		return conceptoIngresoReaseguroDTOList;
	}

	public void setConceptoIngresoReaseguroDTOList(
			List<ConceptoIngresoReaseguroDTO> conceptoIngresoReaseguroDTOList) {
		this.conceptoIngresoReaseguroDTOList = conceptoIngresoReaseguroDTOList;
	}

	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getIdIngreso() {
		return idIngreso;
	}

	public void setIdIngreso(String idIngreso) {
		this.idIngreso = idIngreso;
	}

	public String getTipoCambioNecesario() {
		return tipoCambioNecesario;
	}

	public void setTipoCambioNecesario(String tipoCambioNecesario) {
		this.tipoCambioNecesario = tipoCambioNecesario;
	}

	public List<ReferenciaIngresoDTO> getListaReferenciasIngreso() {
		return listaReferenciasIngreso;
	}

	public void setListaReferenciasIngreso(List<ReferenciaIngresoDTO> listaReferenciasIngreso) {
		this.listaReferenciasIngreso = listaReferenciasIngreso;
	}

	public String getIdReferenciaExterna() {
		return idReferenciaExterna;
	}

	public void setIdReferenciaExterna(String idReferenciaExterna) {
		this.idReferenciaExterna = idReferenciaExterna;
	}
	
}
