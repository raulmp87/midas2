package mx.com.afirme.midas.reaseguro.distribucion;

/**
 * @author asalinas
 */

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirPolizaDN;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirSiniestroSN;

public class DistribucionReaseguroDN {

	private static final DistribucionReaseguroDN INSTANCIA = new DistribucionReaseguroDN();
	
	public static DistribucionReaseguroDN getInstancia() {
		return INSTANCIA;
	}
	
	public static final String EMITIDO_Y_DISTRIBUIDO_NOTA = "Midas: Emitido y Distribuido";

	/**
	 * Este Metodo distribuye las Cuentas por Pagar que se generen relacionados
	 * a un Siniestro convirtiendolas en Cuentas por Cobrar a Reaseguro al distribuir. <br>
	 * Valores esperados<br>
	 * 
	 * @param idToPoliza
	 *            Es el identificador de la P�liza a la que se le relaciona el
	 *            movimiento de siniestro.
	 * @param numeroEndoso
	 *            Es el identificador del Endoso al que se le relaciona el
	 *            movimiento de siniestro.
	 * @param idTcSubrramo
	 *            Es el identificador del subRamo de la Cobertura.
	 * @param idToCobertura
	 *            Es el identificador al que pertenece la Cobertura.
	 * @param idToReporteSiniestro
	 *            Es el identificador del Siniestro
	 * @param conceptoMovimiento
	 *            Es la clave del conceptop del movimiento: <br>
	 *            Conceptos de Siniestros<br>
	 *            Indemnizaci�n 5<br>
	 *            Cancelaci�n de Indemnizaci�n 6<br>
	 *            Deducible 7<br>
	 *            Cancelaci�n de Decucible 8<br>
	 *            Gastos de Ajuste 9<br>
	 *            Salvamento 10<br>
	 *            Cancelar Salvamento 11<br>
	 *            Recuperaci�n Legal 12<br>
	 *            Cancelar Recuperaci�n Legal 14<br>
	 *            Cancelaci�n de Gasto de Ajuste <br>
	 *            Reserva Inicial <br>
	 *            Ajuste de m�s en Reserva <br>
	 *            Ajuste de menos en Reserva <br>
	 * 
	 *<br>
	 * @param fechaRegistroMovimiento
	 *            Es la fecha en la que se realiza el registro del Movimiento
	 * @param tipoMovimiento
	 *            Es el tipo de movimiento que se va a distribuir desde la
	 *            perspectiva de Siniestros Tipo de Movimiento<br>
	 *            Cuenta por Pagar 1<br>
	 *            Cuenta por Cobrar 2<br>
	 * @param idMovimientoSiniestro
	 *            Este es el identificador del movimiento de Siniestro que se
	 *            registra por cada distribucion en movimientos de reaseguro
	 *            como referencia.
	 * @param montoSiniestro
	 */
	public void distribuirMontoSiniestro(
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,	BigDecimal idToReporteSiniestro,String nombreUsuario) throws SystemException {
		
		DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		
		String descripcionParametros = "idToPoliza:"+idToPoliza+",numeroEndoso:"+numeroEndoso+"," +
			"idToSeccion:"+idToSeccion+",idToCobertura:"+idToCobertura+",numeroInciso:"+numeroInciso+",numeroSubInciso:"+numeroSubInciso+"," +
			"conceptoMovimiento:"+conceptoMovimiento+",idToMoneda:"+idToMoneda+"," +
			"fechaMovimiento:"+formatoFecha.format(fechaMovimiento)+",idMovimientoSiniestro:"+idMovimientoSiniestro+"," +
			"montoMovimiento:"+montoMovimiento+",tipoMovimiento:"+tipoMovimiento+"," +
			"idToReporteSiniestro:"+idToReporteSiniestro;
		LogDeMidasWeb.log("DistribucionReaseguroDN.distribuirMontoSiniestro. Se reciben los par�metros de distribuci�n: "+descripcionParametros, Level.SEVERE, null);
		
		BigDecimal idToDistribucionMovSiniestro = null;
		Map<String,String> mapaRespuesta = null;
		try{
			LogDeMidasWeb.log("Se intentar� generar registro DistribucionMovSiniestroDTO.", Level.SEVERE, null);
			DistribucionMovSiniestroSN distribucionMovSiniestroSN = new DistribucionMovSiniestroSN();
			
			mapaRespuesta = distribucionMovSiniestroSN.registrarMovimientoDistribucionSiniestro(idToPoliza, numeroEndoso, idToSeccion, idToCobertura, numeroInciso, numeroSubInciso, 
					conceptoMovimiento, idToMoneda, fechaMovimiento, idMovimientoSiniestro, montoMovimiento, tipoMovimiento, idToReporteSiniestro);
			
//			distribucionMovSiniestroDTO = distribucionMovSiniestroSN.registrarMovimiento(idToPoliza, numeroEndoso,
//					idToSeccion, idToCobertura, numeroInciso, numeroSubInciso, conceptoMovimiento, 
//					idToMoneda, fechaMovimiento, idMovimientoSiniestro, montoMovimiento, tipoMovimiento, idToReporteSiniestro);
		}catch(Exception e){
			LogDeMidasWeb.log("Ocurri� un error al generar el registro DistribucionMovSiniestroDTO. Se generar� Log para procesamiento posterior.  Parametros recibidos: "+descripcionParametros, Level.SEVERE, e);
		}
		
		boolean programarTimer;
		
		if(mapaRespuesta != null && mapaRespuesta.containsKey("claveRespuesta") && mapaRespuesta.get("claveRespuesta").equals(Sistema.EXITO)){
			//Se procede con la programaci�n del timer.
			programarTimer = true;
		}
		else{
			//Generar log para procesamiento posterior.
			generarLogErrorRegistroMovimientoSiniestro(descripcionParametros, mapaRespuesta, nombreUsuario);
			programarTimer = false;
		}
		
		if(programarTimer){
			String idToDistribucionMovSiniestroStr = mapaRespuesta.get("idToDistribucionMovSiniestro");
			if(!UtileriasWeb.esCadenaVacia(idToDistribucionMovSiniestroStr)){
				LogDeMidasWeb.log("Se gener� exitosamente el registro DistribucionMovSiniestroDTO. idToDistribucionMovSiniestro: "+idToDistribucionMovSiniestroStr+"Se intentar� programar el EJBTimer para distribuirlo.", Level.SEVERE, null);
				
				try{
					idToDistribucionMovSiniestro = UtileriasWeb.regresaBigDecimal(idToDistribucionMovSiniestroStr);
					if(idToDistribucionMovSiniestro != null){
						new TemporizadorDistribuirSiniestroSN().iniciar(
								idToPoliza, numeroEndoso, idToSeccion, idToCobertura, 
								numeroInciso, numeroSubInciso, conceptoMovimiento, 
								idToMoneda, fechaMovimiento, idMovimientoSiniestro, 
								montoMovimiento, tipoMovimiento, idToReporteSiniestro, 
								idToDistribucionMovSiniestro);
					}
				}catch(Exception e){
					//Ocurri� un error al programar el timer de dsitribuci�n, generar log para su distribuci�n posterior.
					descripcionParametros += "idDistMovSin:"+idToDistribucionMovSiniestroStr;
					generarLogErrorIniciarTimerDistribucionSiniestro(descripcionParametros, idToDistribucionMovSiniestro, nombreUsuario);
				}
			}
		}
			
	}

	private void generarLogErrorRegistroMovimientoSiniestro(String descripcionParametros,Map<String,String> mapaRespuesta,String nombreUsuario){
		String metodoCapturaError = "DistribucionReaseguroDN.distribuirMontoSiniestro(...)"; 
		Short moduloOrigenError = (short)2;//reaseguro 
		String objetoOrigenError = this.toString();
		String metodoOrigenError = metodoCapturaError; 
		String paramMetodoOrigenError = descripcionParametros;//CAMPO EN EL CUAL SE GUARDAN LOS DATOS DEL SINIESTRO.
		String retornoMetodoorigenError = TemporizadorDistribuirPolizaDN.MENSAJE_ERROR_REGISTRO_DISTRIBUCION_MOV_SINIESTRO;//CLAVE PARA DETECTAR MOVIMIENTOS DE SINIESTROS QUE NO FUERON REGISTRADOS
		String descripcionError = mapaRespuesta.get("mensaje");
		String comentariosAdicionales = "";
		String objetoCapturaError = this.toString();
		Short moduloCapturaError = (short)2;//Reaseguro
		LogDeMidasWeb.log("Ocurri� un error al registrar un objeto DostribucionMovSiniestroDTO, parametros: "+descripcionParametros+". DETALLE: "+descripcionError, Level.SEVERE, null);
		UtileriasWeb.registraLogInteraccionReaseguro(metodoCapturaError, moduloOrigenError, objetoOrigenError, metodoOrigenError, 
				nombreUsuario, paramMetodoOrigenError, retornoMetodoorigenError, null, descripcionError, comentariosAdicionales, objetoCapturaError, moduloCapturaError);
		
	}
	
	private void generarLogErrorIniciarTimerDistribucionSiniestro(String descripcionParametros,BigDecimal idToDistribucionMovSiniestro,String nombreUsuario){
		String metodoCapturaError = "DistribucionReaseguroDN.distribuirMontoSiniestro(...)"; 
		Short moduloOrigenError = (short)2;//reaseguro 
		String objetoOrigenError = this.toString();
		String metodoOrigenError = metodoCapturaError; 
		String paramMetodoOrigenError = descripcionParametros;
		String retornoMetodoorigenError = TemporizadorDistribuirPolizaDN.MENSAJE_ERROR_INICIAR_TIMER_DISTRIBUCION_SINIESTRO;//CLAVE PARA DETECTAR MOVIMIENTOS DE SINIESTROS QUE NO FUERON DISTRIBUIDOS
		String descripcionError = "Se registr� el objeto DistribucionMovSiniestroDTO.idToDistribucionMovSiniestro: "+idToDistribucionMovSiniestro+", pero fall� la programaci�n del timer de distribuci�n.";
		String comentariosAdicionales = "";
		String objetoCapturaError = this.toString();
		Short moduloCapturaError = (short)2;//Reaseguro
		LogDeMidasWeb.log(descripcionError+" Parametros: "+descripcionParametros, Level.SEVERE, null);
		UtileriasWeb.registraLogInteraccionReaseguro(metodoCapturaError, moduloOrigenError, objetoOrigenError, metodoOrigenError, 
				nombreUsuario, paramMetodoOrigenError, retornoMetodoorigenError, null, descripcionError, comentariosAdicionales, objetoCapturaError, moduloCapturaError);
		
	}
	
	public LineaSoporteReaseguroDTO getPorcentajeDistribucion(BigDecimal idToPoliza, Integer numeroEndoso,BigDecimal idToSeccion, BigDecimal idToCobertura,Integer numeroInciso, Integer numeroSubInciso) throws SystemException {
		LineaSoporteReaseguroDTO lineaEncontrada = 
			new DistribucionReaseguroServiciosSN().getPorcentajeDistribucion(idToPoliza, numeroEndoso,idToSeccion, idToCobertura, numeroInciso, numeroSubInciso);
		return lineaEncontrada;
	}
}
