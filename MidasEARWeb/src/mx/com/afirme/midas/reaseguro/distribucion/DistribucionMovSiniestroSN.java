package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class DistribucionMovSiniestroSN {

	DistribucionMovSiniestroFacadeRemote beanRemoto;

	public DistribucionMovSiniestroSN() throws SystemException {
		try{
			LogDeMidasWeb.log("Entrando en DistribucionReaseguroSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DistribucionMovSiniestroFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	@Deprecated
	public DistribucionMovSiniestroDTO registrarMovimiento(BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal idToMoneda, 
			Date fechaMovimiento, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro) {
		return beanRemoto.registrarMovimiento(idToPoliza, numeroEndoso, idToSeccion, 
				idToCobertura, numeroInciso, numeroSubInciso, 
				conceptoMovimiento, idToMoneda, fechaMovimiento, 
				idMovimientoSiniestro, montoMovimiento, tipoMovimiento, 
				idToReporteSiniestro);
	}
	
	public Map<String, String> registrarMovimientoDistribucionSiniestro(BigDecimal idToPoliza,
			Integer numeroEndoso, BigDecimal idToSeccion, BigDecimal idToCobertura, Integer numeroInciso,
			Integer numeroSubInciso, Integer conceptoMovimiento, BigDecimal idToMoneda, Date fechaMovimiento,
			BigDecimal idMovimientoSiniestro, BigDecimal montoMovimiento, Integer tipoMovimiento, BigDecimal idToReporteSiniestro) {
		return beanRemoto.registrarMovimientoDistribucionSiniestro(
				idToPoliza, numeroEndoso, idToSeccion, idToCobertura, numeroInciso, numeroSubInciso, 
				conceptoMovimiento, idToMoneda, fechaMovimiento, idMovimientoSiniestro, montoMovimiento, tipoMovimiento, idToReporteSiniestro);
	}
	
	public List<DistribucionMovSiniestroDTO> listarFiltrado(DistribucionMovSiniestroDTO distribucionMovSiniestroDTO){
		return beanRemoto.listarFiltrado(distribucionMovSiniestroDTO);
	}
	
	public List<DistribucionMovSiniestroDTO> obtenerMovimientosNoDistribuidos(BigDecimal idMovimientoSiniestro, BigDecimal idToReporteSiniestro,int idConceptoDetalle) {
		return beanRemoto.obtenerMovimientosNoDistribuidos(idMovimientoSiniestro, idToReporteSiniestro, idConceptoDetalle);
	}
}
