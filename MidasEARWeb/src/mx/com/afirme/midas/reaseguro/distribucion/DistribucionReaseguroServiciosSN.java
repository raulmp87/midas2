package mx.com.afirme.midas.reaseguro.distribucion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class DistribucionReaseguroServiciosSN {
	
	DistribucionReaseguroServicios beanRemoto;

	public DistribucionReaseguroServiciosSN() throws SystemException {
		try{
			LogDeMidasWeb.log("Entrando en DistribucionReaseguroSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DistribucionReaseguroServicios.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
//	public void distribuirPoliza(SoporteReaseguroDTO soporteReaseguroDTO) {
//		beanRemoto.distribuirPoliza(soporteReaseguroDTO);
//	}
	
//	public void distribuirPoliza(BigDecimal idToPoliza, Integer numeroEndoso) {
//		beanRemoto.distribuirPoliza(idToPoliza, numeroEndoso);
//	}

	public void distribuirPrimas() {
		beanRemoto.distribuirPrimas();
	}
	
	public void distribuirMontoSiniestro(
			BigDecimal idToPoliza, Integer numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,
			Integer numeroInciso, Integer numeroSubInciso,
			Integer conceptoMovimiento,	BigDecimal IdToMoneda, 
			Date fechaMovimietno, BigDecimal idMovimientoSiniestro,
			BigDecimal montoMovimiento, Integer tipoMovimiento,
			BigDecimal idToReporteSiniestro,
			BigDecimal idToDistribucionMovSiniestro) {
		beanRemoto.distribuirMontoSiniestro(idToPoliza, 
				numeroEndoso, idToSeccion, idToCobertura, numeroInciso, 
				numeroSubInciso, conceptoMovimiento, IdToMoneda, fechaMovimietno, 
				idMovimientoSiniestro, montoMovimiento, tipoMovimiento, 
				idToReporteSiniestro, idToDistribucionMovSiniestro);
	}
	
	public LineaSoporteReaseguroDTO getPorcentajeDistribucion(BigDecimal idToPoliza, Integer numeroEndoso,BigDecimal idToSeccion, BigDecimal idToCobertura,Integer numeroInciso, Integer numeroSubInciso) {
		return beanRemoto.getPorcentajeDistribucion(idToPoliza, numeroEndoso, idToSeccion, idToCobertura, numeroInciso, numeroSubInciso);
	}

}
