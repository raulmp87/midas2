package mx.com.afirme.midas.reaseguro.distribucion;

import mx.com.afirme.midas.sistema.SystemException;


public class DistribucionReaseguroServiciosDN {
	private static final DistribucionReaseguroServiciosDN INSTANCIA = new DistribucionReaseguroServiciosDN();

	public static DistribucionReaseguroServiciosDN getInstancia() {
		return DistribucionReaseguroServiciosDN.INSTANCIA;
	}
	
//	public void distribuirPoliza(SoporteReaseguroDTO soporteReaseguroDTO) throws SystemException {
//		new DistribucionReaseguroServiciosSN().distribuirPoliza(soporteReaseguroDTO);
//		
//		/*
//		 * Contabilizar
//		 */
//		
//	}
	
//	public void distribuirPoliza(BigDecimal idToPoliza, Integer numeroEndoso) throws SystemException {
//		new DistribucionReaseguroServiciosSN().distribuirPoliza(idToPoliza, numeroEndoso);
//		
//		/*
//		 * Contabilizar
//		 */
//	}

	public void distribuirPrimas() throws SystemException{
		new DistribucionReaseguroServiciosSN().distribuirPrimas();
	}
}
