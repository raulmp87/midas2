package mx.com.afirme.midas.reaseguro.acumulador;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorFacadeRemote;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.reaseguro.movimiento.MovimientoReaseguroSN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class AcumuladorSN {
	private AcumuladorFacadeRemote beanRemoto;

	public AcumuladorSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en AcumuladorSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(AcumuladorFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<AcumuladorDTO> listarTodos(){
		return beanRemoto.findAll();
	}
	
	public void regenerarAcumuladoresReaseguro(String mesPorProcesar, String anioPorProcesar,List<BigDecimal> idToMovimientosPorCalcular,Boolean restarCantidad) throws SystemException {
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		final int INCREMENTO_IDS = 100;
		int limiteInferior = 0;
		int limiteSuperior = -1;
		int maximoMovimientos = 0;
		int numeroCiclos = 0;
		List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
		StringBuilder movimientosNoProcesados = new StringBuilder("");
		
		boolean procesarRangoFechas = false;
		Integer mes = null, anio = null;
		
		procesarRangoFechas = (!UtileriasWeb.esCadenaVacia(mesPorProcesar) && !UtileriasWeb.esCadenaVacia(anioPorProcesar));
		if(procesarRangoFechas){
			try{
				mes = Integer.valueOf(mesPorProcesar);
				anio = Integer.valueOf(anioPorProcesar);
				procesarRangoFechas = mes > 0 && mes < 12;
				if (procesarRangoFechas)
					procesarRangoFechas = anio > 2008;
			}catch(Exception e){
				procesarRangoFechas = false;
				mes = anio = null;
			}
		}

		boolean procesarListaIdToMovimientos = (!procesarRangoFechas && idToMovimientosPorCalcular != null && !idToMovimientosPorCalcular.isEmpty() && restarCantidad != null);
		
		if(procesarRangoFechas){
			maximoMovimientos = movimientoReaseguroSN.getMaxId(mes,anio);
		}else if(procesarListaIdToMovimientos){
			maximoMovimientos = idToMovimientosPorCalcular.size();
		}else{
			maximoMovimientos = movimientoReaseguroSN.getMaxId(null,null);
		}
		numeroCiclos = maximoMovimientos / INCREMENTO_IDS + 1;
		
		LogDeMidasWeb.log("Iniciando regeneraci�n de Acumuladores Reaseguro. Movimientos por acumular: "+maximoMovimientos+(procesarRangoFechas ? (" del mes "+mes+"/"+anio):""), Level.INFO, null);
		try{
			if(!procesarListaIdToMovimientos)
				beanRemoto.reestablecerAcumuladores(mes,anio);

			for (int i = 0; i < numeroCiclos; i++){
				Runtime.getRuntime().gc();
				limiteInferior = limiteSuperior + 1;
				limiteSuperior = limiteSuperior + INCREMENTO_IDS;
				if(!procesarListaIdToMovimientos){
					movimientoReaseguroDTOList = movimientoReaseguroSN.listarPorRangoIds(mes,anio,limiteInferior, limiteSuperior);
					movimientosNoProcesados.append(beanRemoto.procesarLoteMovimientosARegenerar(movimientoReaseguroDTOList,null));	
				}
				else
				{
					movimientoReaseguroDTOList = movimientoReaseguroSN.listarPorRangoIds(idToMovimientosPorCalcular,limiteInferior, limiteSuperior);
					movimientosNoProcesados.append(beanRemoto.procesarLoteMovimientosARegenerar(movimientoReaseguroDTOList,restarCantidad));

				}
				LogDeMidasWeb.log("Entrando a procesarLoteMovimientosARegenerar del idToMovimientoReaseguro "+limiteInferior+" al "+limiteSuperior+" de "+maximoMovimientos+". Faltan "+(numeroCiclos-i)+" iteraciones", Level.INFO, null);
			}
			System.out.println("\n\nFin de la regeneraci�n de Acumuladores Reaseguro.\n");
			if (movimientosNoProcesados.length() > 0){
				movimientosNoProcesados.toString().replaceFirst(",", "");
				System.out.println("\n\nLos movimientos que no han podido procesarse son los siguientes: \n" + movimientosNoProcesados + "\n\n");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Lista todos los movimientos de reaseguro correspondientes a un siniestro que tengan conceptos no acumulables y los resta de los acumuladores. 
	 * @throws SystemException
	 */
	public void deshacerMovimientosSiniestrosNoAcumulables() throws SystemException {
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		final int INCREMENTO_IDS = 100;
		int limiteInferior = 0;
		int limiteSuperior = -1;
		int cantidadMovimientos = 0;
		int numeroCiclos = 0;
		List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
		StringBuilder movimientosNoProcesados = new StringBuilder("");

		cantidadMovimientos = movimientoReaseguroSN.obtenerCantidadMovimientosSiniestrosNoAcumulables();
		numeroCiclos = cantidadMovimientos / INCREMENTO_IDS + 1;
		
		LogDeMidasWeb.log("Iniciando cancelaci�n de movimientos de siniestro no acumulables. Movimientos por procesar: "+cantidadMovimientos, Level.INFO, null);
		try{
			for (int i = 0; i < numeroCiclos; i++){
				Runtime.getRuntime().gc();
				limiteInferior = limiteSuperior + 1;
				limiteSuperior = limiteSuperior + INCREMENTO_IDS;
				movimientoReaseguroDTOList = movimientoReaseguroSN.listarMovimientosSiniestrosNoAcumulables(limiteInferior, limiteSuperior);
				LogDeMidasWeb.log("Entrando a cancelarLoteMovimientosSiniestrosNoAcumulables del registro "+limiteInferior+" al "+limiteSuperior+" de "+cantidadMovimientos+". Faltan "+(numeroCiclos-i)+" iteraciones", Level.INFO, null);
				movimientosNoProcesados.append(beanRemoto.procesarLoteMovimientosARegenerar(movimientoReaseguroDTOList,Boolean.FALSE));
			}
			LogDeMidasWeb.log("Fin de la cancelaci�n de movimientos de siniestros no Acumulables.",Level.INFO,null);
			if (movimientosNoProcesados.length() > 0){
				movimientosNoProcesados.toString().replaceFirst(",", "");
				LogDeMidasWeb.log("Los movimientos que no han podido procesarse son los siguientes: \n" + movimientosNoProcesados,Level.WARNING,null);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Lista todos los movimientos de reaseguro correspondientes a un siniestro que tengan conceptos no acumulables y los resta de los acumuladores. 
	 * @throws SystemException
	 */
	public void deshacerMovimientosSiniestrosConFechaIncorrecta() throws SystemException {
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		final int INCREMENTO_IDS = 100;
		int limiteInferior = 0;
		int limiteSuperior = -1;
		int cantidadMovimientos = 0;
		int numeroCiclos = 0;
		List<MovimientoReaseguroDTO> movimientoReaseguroDTOList;
		StringBuilder movimientosNoProcesados = new StringBuilder("");

		cantidadMovimientos = movimientoReaseguroSN.obtenerCantidadMovimientosSiniestrosConFechaIncorrecta();
		numeroCiclos = cantidadMovimientos / INCREMENTO_IDS + 1;
		
		LogDeMidasWeb.log("Iniciando cancelaci�n de movimientos de siniestro con fecha de registro incorrecta. Movimientos por procesar: "+cantidadMovimientos, Level.INFO, null);
		try{
			for (int i = 0; i < numeroCiclos; i++){
				Runtime.getRuntime().gc();
				limiteInferior = limiteSuperior + 1;
				limiteSuperior = limiteSuperior + INCREMENTO_IDS;
				movimientoReaseguroDTOList = movimientoReaseguroSN.listarMovimientosSiniestrosConFechaIncorrecta(limiteInferior, limiteSuperior);
				LogDeMidasWeb.log("Entrando a cancelarLoteMovimientosSiniestrosConFechaIncorrecta del registro "+limiteInferior+" al "+limiteSuperior+" de "+cantidadMovimientos+". Faltan "+(numeroCiclos-i)+" iteraciones", Level.INFO, null);
				movimientosNoProcesados.append(beanRemoto.procesarLoteMovimientosARegenerar(movimientoReaseguroDTOList,Boolean.TRUE/*Restar los movimientos*/));
			}
			LogDeMidasWeb.log("Fin de la cancelaci�n de movimientos de siniestros con fecha incorrecta.",Level.INFO,null);
			if (movimientosNoProcesados.length() > 0){
				movimientosNoProcesados.toString().replaceFirst(",", "");
				LogDeMidasWeb.log("Los movimientos que no han podido procesarse son los siguientes: \n" + movimientosNoProcesados,Level.WARNING,null);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public List<BigDecimal> obtenerIdsAcumuladores(Integer mes,Integer anio){
		return beanRemoto.obtenerIdsAcumuladores(mes, anio);
	}
}
