package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ContratosCOBAction extends MidasMappingDispatchAction {
	
	public static final String EXITOSO = "exitoso";
	public static final String NO_EXITOSO = "noExitoso";
	public static final String NO_DISPONIBLE = "noDisponible";

	public ActionForward mostrarContratosCOB(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		
		try {
			request.setAttribute("negocio", request.getParameter("claveNegocio"));
			
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al setar parametro... ", Level.SEVERE, e);
		}
		
		return mapping.findForward(EXITOSO);
	}
	
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			
			List<ContratosCoberturaDTO> contratos = new ArrayList<ContratosCoberturaDTO>();
			ContratosCoberturaDN contratosCoberturaDN = ContratosCoberturaDN.getInstancia();
						
			contratos = contratosCoberturaDN.listarTodos();
					
			request.setAttribute("documentos", contratos);
		} catch (SystemException e) {
			reglaNavegacion = NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Elimina contrato por el <code>id</code> de la solicitud
	 * @param mapping
	 * @param form
	 * @param request
	 * @param responsesolicitud/borrarDocumento
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			String id = request.getParameter("id");
			
			ContratosCoberturaDN contratosCoberturaDN = ContratosCoberturaDN.getInstancia();
			ContratosCoberturaDTO contratoDTO = contratosCoberturaDN.getPorId(UtileriasWeb.regresaBigDecimal(id));
			
			contratosCoberturaDN.borrar(contratoDTO);
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		ContratosCOBForm contratosForm = (ContratosCOBForm) form;
		ContratosCoberturaDTO contratosDTO = new ContratosCoberturaDTO();
		this.llenarDTO(contratosForm, contratosDTO, request);
		ContratosCoberturaDN contratoDN = ContratosCoberturaDN.getInstancia();
		try {
			contratoDN.agregar(contratosDTO);
			//contratosForm.setMensajeUsuario(null,"guardar");
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			reglaNavegacion = NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param ContactoForm
	 * @param ContactoDTO
	 */
	private void llenarDTO(ContratosCOBForm contratoForm, ContratosCoberturaDTO contratoDTO, HttpServletRequest request) {
		SimpleDateFormat formato = new SimpleDateFormat("dd/mm/yyyy");
		//String fechaInicio = contratoForm.getFechaInicio();
		String fechaInicio = request.getParameter("fecha_corte");
		
		try {
			Date date = formato.parse(fechaInicio);
			if (!StringUtil.isEmpty(contratoForm.getFechaInicio())) {
				contratoDTO.setFechaCorte(date);
			}
		} catch (ParseException e) {
			LogDeMidasWeb.log("Error al parsear la fecha... ", Level.SEVERE, e);
		}
		
		if (!StringUtil.isEmpty(request.getParameter("ramo"))){
			contratoDTO.setRamo(request.getParameter("ramo"));
		}
	
				
		if (!StringUtil.isEmpty(request.getParameter("claveEsquemas"))){
			contratoDTO.setCveEsquema(request.getParameter("claveEsquemas"));
		}
		
		if (!StringUtil.isEmpty(String.valueOf(request.getParameter("monto")))) {
			contratoDTO.setMonto(new BigDecimal(request.getParameter("monto")));
		}
		
		if (!StringUtil.isEmpty(request.getParameter("calificacion"))){ 
			contratoDTO.setCalificacion(request.getParameter("calificacion"));
		}
	}
}
