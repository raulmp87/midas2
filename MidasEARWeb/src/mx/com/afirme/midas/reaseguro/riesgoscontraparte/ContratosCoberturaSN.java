package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Paulo dos Santos
 * @since 16 de Febrero del 2015
 * 
 */
public class ContratosCoberturaSN {
	private ContratosCoberturaFacadeRemote beanRemoto;
	public static final String NO_DISPONIBLE = "noDisponible";
	public static final String EXCEPCION_OBTENER_DTO = "excepcionAlObtenerDTO";

	public ContratosCoberturaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ContratosCoberturaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(ContratosCoberturaDTO contratoDTO) {
		try {
			beanRemoto.save(contratoDTO);
		} catch (EJBTransactionRolledbackException e) {
			LogDeMidasWeb.log("Beam remoto error... ", Level.SEVERE, e);
			
		}
	}

	public ContratosCoberturaDTO getPorId(BigDecimal id) {
		try {
			return beanRemoto.findById(id);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ContratosCoberturaDTO> listarTodos() {
		try {
			return beanRemoto.findAll();
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ContratosCoberturaDTO> listarContratos(
			BigDecimal idCveNegocio) {
		try {
			return beanRemoto.findByProperty("cveNegocio", idCveNegocio.intValue());
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(ContratosCoberturaDTO contratoDTO) {
		try {
			beanRemoto.delete(contratoDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void update(ContratosCoberturaDTO contratoDTO) {
		try {
			beanRemoto.update(contratoDTO);
		} catch (EJBTransactionRolledbackException e) {
			LogDeMidasWeb.log("Beam remoto error... ", Level.SEVERE, e);
		}
	}
}
