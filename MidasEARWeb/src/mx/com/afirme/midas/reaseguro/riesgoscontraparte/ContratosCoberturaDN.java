package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoSN;

public class ContratosCoberturaDN {
	private static final ContratosCoberturaDN INSTANCIA = new ContratosCoberturaDN();

	public static ContratosCoberturaDN getInstancia() {
		return ContratosCoberturaDN.INSTANCIA;
	}

	public ContratosCoberturaDTO getPorId(BigDecimal id) throws SystemException {
		ContratosCoberturaSN solicitudSN = new ContratosCoberturaSN();
		return solicitudSN.getPorId(id);
	}

	public List<ContratosCoberturaDTO> listarTodos() throws SystemException {
		ContratosCoberturaSN solicitudSN = new ContratosCoberturaSN();
		return solicitudSN.listarTodos();
	}

	public void agregar(ContratosCoberturaDTO documentoDigitalSolicitudDTO) throws SystemException {
		ContratosCoberturaSN solicitudSN = new ContratosCoberturaSN();
		solicitudSN.agregar(documentoDigitalSolicitudDTO);
	}
	
	public void update(ContratosCoberturaDTO documentoDigitalSolicitudDTO) throws SystemException {
		ContratosCoberturaSN solicitudSN = new ContratosCoberturaSN();
		solicitudSN.update(documentoDigitalSolicitudDTO);
	}

	public List<ContratosCoberturaDTO> listarDocumentosSolicitud(
			BigDecimal idToSolicitud) throws SystemException {
		ContratosCoberturaSN solicitudSN = new ContratosCoberturaSN();
		return solicitudSN.listarTodos();
	}

	public void borrar(ContratosCoberturaDTO contratoDTO) throws SystemException {
		ContratosCoberturaSN solicitudSN = new ContratosCoberturaSN();
		solicitudSN.borrar(contratoDTO);
	}
	
	public List<ContratosCoberturaDTO> listarDocumentosDigitalesPorCveNegocio(BigDecimal idNegocio) throws SystemException{
		ContratosCoberturaSN documentoDigitalSolicitudSN = new ContratosCoberturaSN();
		return documentoDigitalSolicitudSN.listarContratos(idNegocio);
	}
	
	public ControlArchivoDTO obtenerControlArchivoDTO(
			ControlArchivoDTO controlArchivoDTO) throws SystemException {
		ControlArchivoSN controlArchivoSN = new ControlArchivoSN();
		return controlArchivoSN.getPorId(controlArchivoDTO.getIdToControlArchivo());
	}
}
