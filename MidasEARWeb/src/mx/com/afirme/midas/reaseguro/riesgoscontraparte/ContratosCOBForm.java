package mx.com.afirme.midas.reaseguro.riesgoscontraparte;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ContratosCOBForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String fechaInicio;
	private String id_ramo;
	private String claveEsquemas;
	private BigDecimal monto;	
	private String calificacion;
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}
	
	public String getId_ramo() {
		return id_ramo;
	}

	public void setId_ramo(String id_ramo) {
		this.id_ramo = id_ramo;
	}
	
	public String getClaveEsquemas() {
		return claveEsquemas;
	}

	public void setClaveEsquemas(String claveEsquemas) {
		this.claveEsquemas = claveEsquemas;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	
	
	public String getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(String calificacion) {
		this.calificacion = calificacion;
	}

	@Override
	public String toString(){
		String cadena = "ReporteRCSForm: ";
		cadena += "fechaInicio = "+fechaInicio;
		cadena += "idTcRamo = "+id_ramo;
		cadena += "monto = "+monto;
		return cadena;
	}
}