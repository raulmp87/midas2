package mx.com.afirme.midas.reaseguro.estadoscuenta;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;

public class EstadoCuentaTipoReaseguroForm  extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9079376611226938405L;
	
	private String idEstadoCuenta;
	private String suscripcion;
	private String idTcRamo;
	private String idTcSubRamo;
	private String nombreReasegurador;
	private String direccionReasegurador;
	private String idTcMoneda;
	private String fechaInicial;
	private String fechaFinal;
	private String observaciones;
	private String formaPago;
	private String rfc;
	
	private String idEjercicio;
	private String tipoReaseguro;
	private String idRetencion;
	private String idtcReaseguradorCorredor;
	private String idSuscripcion;
	private String descripcionSubRamo;
	private String descripcionMoneda;
	
	private String mostrarEstadoCuentaConEjerciciosAnteriores;
	
	// Listas
	private List<MonedaDTO> monedaDTOList;
	private List<TipoReaseguroDTO> tipoReaseguroDTOList;
	private List<EjercicioDTO> ejercicioDTOList;
	
	private String aplicaRegistroObservaciones;
	
	//Getters & Setters
	public String getIdEstadoCuenta() {
		return idEstadoCuenta;
	}
	public void setIdEstadoCuenta(String idEstadoCuenta) {
		this.idEstadoCuenta = idEstadoCuenta;
	}
	public List<MonedaDTO> getMonedaDTOList() {
		return monedaDTOList;
	}
	public void setMonedaDTOList(List<MonedaDTO> monedaDTOList) {
		this.monedaDTOList = monedaDTOList;
	}
	public List<TipoReaseguroDTO> getTipoReaseguroDTOList() {
		return tipoReaseguroDTOList;
	}
	public void setTipoReaseguroDTOList(List<TipoReaseguroDTO> tipoReaseguroDTOList) {
		this.tipoReaseguroDTOList = tipoReaseguroDTOList;
	}
	public String getIdEjercicio() {
		return idEjercicio;
	}
	public void setIdEjercicio(String idEjercicio) {
		this.idEjercicio = idEjercicio;
	}
	public String getTipoReaseguro() {
		return tipoReaseguro;
	}
	public void setTipoReaseguro(String tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}
	public String getIdRetencion() {
		return idRetencion;
	}
	public void setIdRetencion(String idRetencion) {
		this.idRetencion = idRetencion;
	}
	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}
	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}
	public String getIdSuscripcion() {
		return idSuscripcion;
	}
	public void setIdSuscripcion(String idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}
	public String getSuscripcion() {
		return suscripcion;
	}
	public void setSuscripcion(String suscripcion) {
		this.suscripcion = suscripcion;
	}
	public String getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	public String getDireccionReasegurador() {
		return direccionReasegurador;
	}
	public void setDireccionReasegurador(String direccionReasegurador) {
		this.direccionReasegurador = direccionReasegurador;
	}
	/*****************/
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	/****************/
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public List<EjercicioDTO> getEjercicioDTOList() {
		return ejercicioDTOList;
	}
	public void setEjercicioDTOList(List<EjercicioDTO> ejercicioDTOList) {
		this.ejercicioDTOList = ejercicioDTOList;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public String getMostrarEstadoCuentaConEjerciciosAnteriores() {
		return mostrarEstadoCuentaConEjerciciosAnteriores;
	}
	public void setMostrarEstadoCuentaConEjerciciosAnteriores(String mostrarEstadoCuentaConEjerciciosAnteriores) {
		this.mostrarEstadoCuentaConEjerciciosAnteriores = mostrarEstadoCuentaConEjerciciosAnteriores;
	}
	public String getAplicaRegistroObservaciones() {
		return aplicaRegistroObservaciones;
	}
	public void setAplicaRegistroObservaciones(String aplicaRegistroObservaciones) {
		this.aplicaRegistroObservaciones = aplicaRegistroObservaciones;
	}
}
