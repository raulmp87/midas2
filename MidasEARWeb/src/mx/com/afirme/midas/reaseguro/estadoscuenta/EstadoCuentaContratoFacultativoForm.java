package mx.com.afirme.midas.reaseguro.estadoscuenta;

import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorForm;
import mx.com.afirme.midas.siniestro.reportes.CaratulaSiniestroForm;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class EstadoCuentaContratoFacultativoForm  extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9079376611226938405L;
	private String idEstadoCuenta;
	private String poliza;
	private String endoso;
	private String nomAsegurado;
	private String notaCobertura;
	private String suscripcion;
	private String idTcRamo;
	private String nombreReasegurador;
	private String direccionReasegurador;
	private String idTcMoneda;
	private String contrato;
	private String fechaInicial;
	private String fechaFinal;
	private String observaciones;
	private String descripcionSubRamo;
	private String descripcionMoneda;
	private String formaPago;
	@SuppressWarnings("unused")
	private String Rfc;
	
	private String busquedaNomAsegurado;
	private String clavePolizaSubramo;
	
	private CaratulaSiniestroForm caratulaSiniestroForm;
	
	private String hasta;
	private String separarConceptosPorAcumulador;
	private String idPoliza;
	
	private String idReporteSiniestro;
	private String claveSiniestroSubRamo;
	
	private List<MonedaDTO> monedaDTOList;
	private String idtcReaseguradorCorredor;
	private ReaseguradorCorredorForm reaseguradorForm;
	private String idTcSubRamo;
	private String tipoConsulta;
	private String corteAl;
	private String numeroReporteSiniestro;
	private String numeroPoliza;
	private String fechaReporteSiniestro;
	
	// Getters & Setters
	
	public String getIdEstadoCuenta() {
		return idEstadoCuenta;
	}
	public void setIdEstadoCuenta(String idEstadoCuenta) {
		this.idEstadoCuenta = idEstadoCuenta;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getEndoso() {
		return endoso;
	}
	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}
	public String getNomAsegurado() {
		return nomAsegurado;
	}
	public void setNomAsegurado(String nomAsegurado) {
		this.nomAsegurado = nomAsegurado;
	}
	public String getNotaCobertura() {
		return notaCobertura;
	}
	public void setNotaCobertura(String notaCobertura) {
		this.notaCobertura = notaCobertura;
	}
	public String getSuscripcion() {
		return suscripcion;
	}
	public void setSuscripcion(String suscripcion) {
		this.suscripcion = suscripcion;
	}
	public String getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public String getNombreReasegurador() {
		return nombreReasegurador;
	}
	public void setNombreReasegurador(String nombreReasegurador) {
		this.nombreReasegurador = nombreReasegurador;
	}
	/**
	public String getRfc() {
		return Rfc;
	}
	public void setRfc(String Rfc) {
		this.Rfc = Rfc;
	}
	/***/
	public String getDireccionReasegurador() {
		return direccionReasegurador;
	}
	public void setDireccionReasegurador(String direccionReasegurador) {
		this.direccionReasegurador = direccionReasegurador;
	}
	public String getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}
	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}
	public String getBusquedaNomAsegurado() {
		return busquedaNomAsegurado;
	}
	public void setBusquedaNomAsegurado(String busquedaNomAsegurado) {
		this.busquedaNomAsegurado = busquedaNomAsegurado;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getClavePolizaSubramo() {
		return clavePolizaSubramo;
	}
	public void setClavePolizaSubramo(String clavePolizaSubramo) {
		this.clavePolizaSubramo = clavePolizaSubramo;
	}
	public CaratulaSiniestroForm getCaratulaSiniestroForm() {
		if(caratulaSiniestroForm == null)
			caratulaSiniestroForm = new CaratulaSiniestroForm();
		return caratulaSiniestroForm;
	}
	public void setCaratulaSiniestroForm(CaratulaSiniestroForm caratulaSiniestroForm) {
		this.caratulaSiniestroForm = caratulaSiniestroForm;
	}
	public String getHasta() {
		return hasta;
	}
	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	public String getSepararConceptosPorAcumulador() {
		return separarConceptosPorAcumulador;
	}
	public void setSepararConceptosPorAcumulador(String separarConceptosPorAcumulador) {
		this.separarConceptosPorAcumulador = separarConceptosPorAcumulador;
	}
	public String getIdPoliza() {
		return idPoliza;
	}
	public void setIdPoliza(String idPoliza) {
		this.idPoliza = idPoliza;
	}
	public String getIdReporteSiniestro() {
		return idReporteSiniestro;
	}
	public void setIdReporteSiniestro(String idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}
	public String getClaveSiniestroSubRamo() {
		return claveSiniestroSubRamo;
	}
	public void setClaveSiniestroSubRamo(String claveSiniestroSubRamo) {
		this.claveSiniestroSubRamo = claveSiniestroSubRamo;
	}
	public List<MonedaDTO> getMonedaDTOList() {
		return monedaDTOList;
	}
	public void setMonedaDTOList(List<MonedaDTO> monedaDTOList) {
		this.monedaDTOList = monedaDTOList;
	}
	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}
	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}
	public ReaseguradorCorredorForm getReaseguradorForm() {
		return reaseguradorForm;
	}
	public void setReaseguradorForm(ReaseguradorCorredorForm reaseguradorForm) {
		this.reaseguradorForm = reaseguradorForm;
	}
	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}
	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	public String getCorteAl() {
		return corteAl;
	}
	public void setCorteAl(String corteAl) {
		this.corteAl = corteAl;
	}
	public String getNumeroReporteSiniestro() {
		return numeroReporteSiniestro;
	}
	public void setNumeroReporteSiniestro(String numeroReporteSiniestro) {
		this.numeroReporteSiniestro = numeroReporteSiniestro;
	}
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	public String getFechaReporteSiniestro() {
		return fechaReporteSiniestro;
	}
	public void setFechaReporteSiniestro(String fechaReporteSiniestro) {
		this.fechaReporteSiniestro = fechaReporteSiniestro;
	}
}