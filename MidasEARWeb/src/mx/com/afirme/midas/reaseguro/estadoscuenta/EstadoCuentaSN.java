package mx.com.afirme.midas.reaseguro.estadoscuenta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDN;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFacadeRemote;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFiltroDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoEgresoDTO;
import mx.com.afirme.midas.contratos.linea.LineaDN;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class EstadoCuentaSN {
	private EstadoCuentaFacadeRemote beanRemoto;

	public EstadoCuentaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en EstadoCuentaSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(EstadoCuentaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<EstadoCuentaDTO> obtenerEstadosCuenta(EstadoCuentaFiltroDTO  filtro){
		
		return beanRemoto.obtenerEstadosCuenta(filtro);
	}

	public EstadoCuentaDTO getPorId(BigDecimal id){
		return beanRemoto.findById(id);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaDetalle(BigDecimal idEstadoCuenta, List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> acumuladorDTOList){
		return beanRemoto.obtenerEstadoCuentaDetalle(idEstadoCuenta,listaConceptosPorMoneda,acumuladorDTOList);
	}
	
	public EstadoCuentaDTO modificar(EstadoCuentaDTO estadoCuentaDTO){
		return beanRemoto.update(estadoCuentaDTO);
	}
	
	public List<PolizaSoporteDanosDTO> filtrarPolizasFacultativas(List<PolizaSoporteDanosDTO> polizas){
		List<PolizaSoporteDanosDTO> polizasFacultativas = new ArrayList<PolizaSoporteDanosDTO>();
		polizasFacultativas = beanRemoto.filtrarPolizasFacultativas(polizas);
		
		return polizasFacultativas;
	}
	
	public List<EstadoCuentaDTO> buscarEstadosCuentaFacultativos(PolizaSoporteDanosDTO poliza){
		return beanRemoto.buscarEstadosCuentaFacultativos(poliza);
	}
	
	public void crearEstadosCuentaLinea(LineaDTO lineaDTO){
		beanRemoto.crearEstadosCuentaLinea(lineaDTO);
	}
	
	public void crearEstadosCuentaFacultativo(ContratoFacultativoDTO contrato){
		beanRemoto.crearEstadosCuentaFacultativo(contrato);
	}
	
	public List<LineaDTO> obtenerLineasAutorizadas(Date fecha){
		return beanRemoto.obtenerLineasAutorizadas(fecha);
	}
	
	public List<EstadoCuentaDTO> obtenerSerieEstadosCuenta(EstadoCuentaDTO estadoCuenta){
		return beanRemoto.obtenerSerieEstadosCuenta(estadoCuenta);
	}
	
	public CuentaBancoDTO obtenerCuentaBanco(EstadoCuentaDecoradoDTO estadoCuenta,Integer idMoneda){
		return beanRemoto.obtenerCuentaBanco(estadoCuenta,idMoneda);
	}
	
	@SuppressWarnings("deprecation")
	public void generarEdosCta(){
		List<ContratoFacultativoDTO> contratoFacultativoList = beanRemoto.obtenerContratoAutorizadasFacultativo(new Date("01/01/2007"));
		if(contratoFacultativoList != null){
			System.out.println("Comienza la creacion de estados de cuenta " + new Date());
			for(ContratoFacultativoDTO contratoFacultativoDTO : contratoFacultativoList){
				beanRemoto.crearEstadosCuentaFacultativo(contratoFacultativoDTO);
				contratoFacultativoDTO.setEstadosCuentaCreados(1);
				try {
					ContratoFacultativoDN.getInstancia("").modificar(contratoFacultativoDTO);
				} catch (ExcepcionDeAccesoADatos e) {
					e.printStackTrace();
				} catch (SystemException e) {
					e.printStackTrace();
				} catch (ExcepcionDeLogicaNegocio e) {
					e.printStackTrace();
				}
			}
			System.out.println("Se crearon estados de cuenta facultativo " + new Date());
		}else{
			System.out.println("No hay contratos vigentes facultativos para crear estados de cuenta");
		}
		
		List<LineaDTO> contratosAutomaticos = beanRemoto.obtenerLineasAutorizadas(new Date("01/01/2007"));
		for (LineaDTO lineaDTO : contratosAutomaticos) {
			beanRemoto.crearEstadosCuentaLinea(lineaDTO);
			lineaDTO.setEstadosCuentaCreados(1);
			try {
				LineaDN.getInstancia().modificar(lineaDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public List<ConceptoAcumuladorDTO> obtenerConceptosMovimientos(BigDecimal idMoneda){
		return beanRemoto.obtenerConceptosMovimientos(idMoneda, null);
	}
	
	public List<AcumuladorDTO> obtenerAcumuladoresPorIdEstadoCta(BigDecimal idEstadoCuenta){
		return beanRemoto.obtenerAcumuladoresPorIdEstadoCta(idEstadoCuenta);
	}
	
	public SaldoEgresoDTO obtenerEgresoPorIdEstadoCta(BigDecimal idEstadoCuenta){
		return beanRemoto.obtenerEgresoPorIdEstadoCta(idEstadoCuenta);
	}
	
	public EstadoCuentaDTO obtenerEstadoCuentaHistorico(BigDecimal idEstadoCuenta){
		return beanRemoto.obtenerEstadoCuentaHistorico(idEstadoCuenta);
	}
	
	public List<SaldoConceptoDTO> obtenerSaldosIndividuales(EstadoCuentaDTO estadoCuentaDTO,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> listaAcumuladoresEdoCta,boolean consultaAcumuladores){
		return beanRemoto.obtenerSaldosIndividuales(estadoCuentaDTO, listaConceptosPorMoneda, listaAcumuladoresEdoCta, consultaAcumuladores);
	}
	
	SaldoConceptoDTO obtenerSaldosAcumulados(List<SaldoConceptoDTO> listaSaldosConceptos, SaldoConceptoDTO saldoAnterior){
		return beanRemoto.obtenerSaldosAcumulados(listaSaldosConceptos, saldoAnterior);
	}
	
	public List<BigDecimal> filtrarPolizasFacultativasPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda){
		return beanRemoto.filtrarPolizasFacultativasPorReaseguradorMoneda(idTcReasegurador, idMoneda);
	}
	
	public List<BigDecimal> filtrarSiniestrosFacultativosPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda){
		return beanRemoto.filtrarSiniestrosFacultativosPorReaseguradorMoneda(idTcReasegurador, idMoneda);
	}
	
	public List<EstadoCuentaDTO> obtenerEstadosCuentaPorIdsTipoReaseguro(String[] idsEdosCta, int[] tiposReaseguro){
		return beanRemoto.obtenerEstadosCuentaPorIdsTipoReaseguro(idsEdosCta, tiposReaseguro);
	}
}
