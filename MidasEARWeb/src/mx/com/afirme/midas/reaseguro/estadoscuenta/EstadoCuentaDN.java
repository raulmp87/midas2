package mx.com.afirme.midas.reaseguro.estadoscuenta;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDN;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDN;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDN;
import mx.com.afirme.midas.contratos.contratoprimerexcedente.ContratoPrimerExcedenteDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.ConceptoAcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFiltroDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoEgresoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.PresentacionEstadoCuentaDN;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.ConceptoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.danios.soporte.EndosoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstadoCuentaDN {
	private static final EstadoCuentaDN INSTANCIA = new EstadoCuentaDN();
	
	public static EstadoCuentaDN getINSTANCIA() {
		return INSTANCIA;
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadosCuenta(EstadoCuentaFiltroDTO  filtro,boolean agruparFacultativos) throws SystemException{
		List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList = new ArrayList<EstadoCuentaDecoradoDTO>();
		
		if(filtro.getTipoReaseguro()!=null && filtro.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO && agruparFacultativos){
			if(filtro.getIdTcSubRamo() != null){
				EstadoCuentaDecoradoDTO estadoCuentaSubRamo = PresentacionEstadoCuentaDN.getInstancia().
					obtenerEstadoCuentaFacultativoPorSuscripcion(
							filtro.getIdtcreaseguradorcorredor(), new BigDecimal(filtro.getIdMoneda()),filtro.getIdTcSubRamo(), filtro.getEjercicio(), filtro.getSuscripcion());
				if(estadoCuentaSubRamo != null){
					estadoCuentaDecoradoDTOList.add(estadoCuentaSubRamo);
				}
			}
			else{
				estadoCuentaDecoradoDTOList = PresentacionEstadoCuentaDN.getInstancia().
						obtenerEstadoCuentaFacultativoPorSuscripcion(
								filtro.getIdtcreaseguradorcorredor(), new BigDecimal(filtro.getIdMoneda()), filtro.getEjercicio(), filtro.getSuscripcion());
			}
		}
		else{
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
			List<EstadoCuentaDTO> estadoCuentaDTOList = new ArrayList<EstadoCuentaDTO>();
			EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
			estadoCuentaDTOList = estadoCuentaSN.obtenerEstadosCuenta(filtro);
			for (EstadoCuentaDTO estadoCuentaDTO : estadoCuentaDTOList) {
				estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
				estadoCuentaDecoradoDTOList.add(estadoCuentaDecoradoDTO);
			}
		}
		
		return estadoCuentaDecoradoDTOList;
	}
	
	public List<EstadoCuentaDTO> obtenerEstadosCuentaCheque(EstadoCuentaFiltroDTO  filtro) throws SystemException{
		List<EstadoCuentaDTO> estadoCuentaDecoradoDTOList = new ArrayList<EstadoCuentaDTO>();
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
		List<EstadoCuentaDTO> estadoCuentaDTOList = new ArrayList<EstadoCuentaDTO>();
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		estadoCuentaDTOList = estadoCuentaSN.obtenerEstadosCuenta(filtro);
		for (EstadoCuentaDTO estadoCuentaDTO : estadoCuentaDTOList) {
			estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
			estadoCuentaDecoradoDTOList.add(estadoCuentaDecoradoDTO);
		}
		
		return estadoCuentaDecoradoDTOList;
	}
	
	public EstadoCuentaDTO getPorId(BigDecimal id) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		
		return estadoCuentaSN.getPorId(id);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaDetalle(BigDecimal idEstadoCuenta,List<ConceptoAcumuladorDTO> listaConceptosPorMoneda,List<AcumuladorDTO> acumuladorDTOList) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = estadoCuentaSN.obtenerEstadoCuentaDetalle(idEstadoCuenta,listaConceptosPorMoneda,acumuladorDTOList);
		PolizaSoporteDanosDTO poliza = new PolizaSoporteDanosDTO();
		if (estadoCuentaDecoradoDTO != null && estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			poliza = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(estadoCuentaDecoradoDTO.getIdPoliza());
			if (poliza != null){
				//throw new ExcepcionDeLogicaNegocio(this.getClass().getCanonicalName(), "No hay p�liza relacionada al estado de cuenta con id: " + estadoCuentaDecoradoDTO.getIdEstadoCuenta());
				estadoCuentaDecoradoDTO.setPolizaSoporteDanosDTO(poliza);
				EndosoSoporteDanosDTO endosoSoporteDanosDTO = SoporteDanosDN.getInstancia().getEndosoPorId(poliza.getIdToPoliza(), new BigDecimal(estadoCuentaDecoradoDTO.getNumeroEndoso()));
				estadoCuentaDecoradoDTO.setEndosoSoporteDanosDTO(endosoSoporteDanosDTO);
			}
		}
		return estadoCuentaDecoradoDTO;
	}
	
	public EstadoCuentaDTO modificar(EstadoCuentaDTO estadoCuentaDTO) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		return estadoCuentaSN.modificar(estadoCuentaDTO);
	}
	
	public List<PolizaSoporteDanosDTO>  listarPolizasFacultativas(List<PolizaSoporteDanosDTO> polizaSoporteDanosList) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		List<PolizaSoporteDanosDTO> polizasFacultativas = estadoCuentaSN.filtrarPolizasFacultativas(polizaSoporteDanosList);
		
		return polizasFacultativas;
	}
	
	public List<EstadoCuentaDecoradoDTO> buscarEstadosCuentaFacultativos(PolizaSoporteDanosDTO poliza) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		List<EstadoCuentaDTO> estadoCuentaDTOList =  estadoCuentaSN.buscarEstadosCuentaFacultativos(poliza);
		List<EstadoCuentaDecoradoDTO> estadoCuentaList = new ArrayList<EstadoCuentaDecoradoDTO>();
	 	for (EstadoCuentaDTO estadoCuentaDTO : estadoCuentaDTOList) {
				 EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
				 estadoCuentaDecoradoDTO.setEndosoSoporteDanosDTO(SoporteDanosDN.getInstancia().getEndosoPorId(estadoCuentaDTO.getIdPoliza(), BigDecimal.valueOf(estadoCuentaDTO.getNumeroEndoso())));
				 estadoCuentaDecoradoDTO.setPolizaSoporteDanosDTO(poliza);
	 			 estadoCuentaList.add(estadoCuentaDecoradoDTO);
			 }
		return estadoCuentaList;
	}
	
	public void crearEstadosCuentaLinea(LineaDTO lineaDTO) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		estadoCuentaSN.crearEstadosCuentaLinea(lineaDTO);
	}
	
	public void crearEstadosCuentaFacultativo(ContratoFacultativoDTO contrato) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		estadoCuentaSN.crearEstadosCuentaFacultativo(contrato);
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadosCuenta(EstadoCuentaFiltroDTO filtro, PolizaSoporteDanosDTO polizaSoporteDanosDTO) throws SystemException{
		List<EstadoCuentaDecoradoDTO> listaEstadosCuentaDecorados = obtenerEstadosCuenta(filtro,false);
		List<EstadoCuentaDecoradoDTO> listaEstadosCuentaDecoradosFiltrados = new ArrayList<EstadoCuentaDecoradoDTO>();
		if (polizaSoporteDanosDTO != null && polizaSoporteDanosDTO.getIdToPoliza() != null){
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : listaEstadosCuentaDecorados) {
				if (polizaSoporteDanosDTO.getIdToPoliza().equals(estadoCuentaDecoradoDTO.getIdPoliza())){
					listaEstadosCuentaDecoradosFiltrados.add(estadoCuentaDecoradoDTO);
				}
			}
		}else{
			listaEstadosCuentaDecoradosFiltrados = listaEstadosCuentaDecorados;
		}
		
		return listaEstadosCuentaDecoradosFiltrados;
	}
		
	public List<EstadoCuentaDTO> obtenerEstadosCuentaPorIdsTipoReaseguro(String[] idsEdosCta, int[] tiposReaseguro) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		return estadoCuentaSN.obtenerEstadosCuentaPorIdsTipoReaseguro(idsEdosCta, tiposReaseguro);
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerEstadosCuentaDecoradoPorIdsTipoReaseguro(String[] idsEdosCta,  int[] tiposReaseguro) throws SystemException{		
		List<EstadoCuentaDTO> estadosCuenta = this.obtenerEstadosCuentaPorIdsTipoReaseguro(idsEdosCta, tiposReaseguro);
		List<EstadoCuentaDecoradoDTO> estadosCuentaDecorados = new ArrayList<EstadoCuentaDecoradoDTO>();
				
		for (EstadoCuentaDTO estadoCuenta : estadosCuenta){			
			estadosCuentaDecorados.add(new EstadoCuentaDecoradoDTO(estadoCuenta));
		}
		return estadosCuentaDecorados;
	}
	
	/**
	 * 
	 * @param idsEdosCta, String[] con los ids de los estados de cuenta a recuperar
	 * @return
	 * @throws SystemException
	 */
	public List<EstadoCuentaDecoradoDTO> obtenerEstadosCuentaDecoradoPorIds(String[] idsEdosCta) throws SystemException{				
		List<EstadoCuentaDecoradoDTO> listaEstadosCuenta = new ArrayList<EstadoCuentaDecoradoDTO>();
		EstadoCuentaDTO estadoCuentaDTO;
		for (int i = 0; i < idsEdosCta.length; i++){
			estadoCuentaDTO = new EstadoCuentaDTO();
			estadoCuentaDTO = EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(idsEdosCta[i]));
			listaEstadosCuenta.add(new EstadoCuentaDecoradoDTO(estadoCuentaDTO));
		}
		return listaEstadosCuenta;
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuenta(BigDecimal id) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		PolizaSoporteDanosDTO poliza;
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaSN.getPorId(id));
		if (estadoCuentaDecoradoDTO != null && estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			poliza = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(estadoCuentaDecoradoDTO.getIdPoliza());
			estadoCuentaDecoradoDTO.setPolizaSoporteDanosDTO(poliza);
			EndosoSoporteDanosDTO endosoSoporteDanosDTO = SoporteDanosDN.getInstancia().getEndosoPorId(poliza.getIdToPoliza(), new BigDecimal(estadoCuentaDecoradoDTO.getNumeroEndoso()));
			estadoCuentaDecoradoDTO.setEndosoSoporteDanosDTO(endosoSoporteDanosDTO);		
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(poliza.getIdToPoliza());
			estadoCuentaDecoradoDTO.setPolizaDTO(polizaDTO);
			
		}
		return estadoCuentaDecoradoDTO;
	}
		 
	
	public List<EstadoCuentaDTO> obtenerSerieEstadosCuenta(EstadoCuentaDTO estadoCuenta) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		return estadoCuentaSN.obtenerSerieEstadosCuenta(estadoCuenta);
	}
	
	public CuentaBancoDTO obtenerCuentaBanco(EstadoCuentaDecoradoDTO estadoCuenta,Integer idMoneda) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		
		return estadoCuentaSN.obtenerCuentaBanco(estadoCuenta, idMoneda);
	}
	
	public List<ConceptoAcumuladorDTO> obtenerConceptosMovimientos(BigDecimal idMoneda) throws SystemException{
		return new EstadoCuentaSN().obtenerConceptosMovimientos(idMoneda);
	}
	
	public List<AcumuladorDTO> obtenerAcumuladoresPorIdEstadoCta(BigDecimal idEstadoCuenta) throws SystemException{
		return new EstadoCuentaSN().obtenerAcumuladoresPorIdEstadoCta(idEstadoCuenta);
	}
	
	public SaldoEgresoDTO obtenerEgresoPorIdEstadoCta(BigDecimal idEstadoCuenta) throws SystemException{
		return new EstadoCuentaSN().obtenerEgresoPorIdEstadoCta(idEstadoCuenta);
	}

	/**
	 * Consulta un estado de cuenta, correspondiente a una suscripci�n, y los estados de cuenta de suscripciones anteriores, adem�s consulta el estado 
	 * de cuenta de los contratos cuota parte � primer excedente de a�os contractuales anteriores y combina los saldos en un estado de cuenta �nico.
	 * @param idToEstadoCuenta
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	@Deprecated
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaHistoricoCombinado(BigDecimal idToEstadoCuenta,String nombreUsuario) throws SystemException{
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		//Buscar el estado de cuenta con detalle hist�rico
		EstadoCuentaDTO estadoCuentaDTO = estadoCuentaSN.obtenerEstadoCuentaHistorico(idToEstadoCuenta);
		List<EstadoCuentaDTO> listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
		
		if(estadoCuentaDTO != null){
			listaEstadoCuentaContratosAnteriores = consultarEstadosCuentaContratosAnteriores(estadoCuentaDTO,nombreUsuario);
		}
		
		/*
		 * Esta lista de conceptos se cambiar� por una lista de configuraciones de conceptos (nuevas tablas)
		 */
		List<ConceptoAcumuladorDTO> listaConceptosAcumulador = obtenerConceptosMovimientos(new BigDecimal(estadoCuentaDTO.getIdMoneda()));
		listaEstadoCuentaContratosAnteriores.add(0, estadoCuentaDTO);
		
		List<EstadoCuentaDecoradoDTO> listaEstadoCuentaEjerciciosAnteriores = new ArrayList<EstadoCuentaDecoradoDTO>();
		for(EstadoCuentaDTO estadoCuentaTMP : listaEstadoCuentaContratosAnteriores){
			listaEstadoCuentaEjerciciosAnteriores.add(obtenerEstadoCuentaConSaldosCalculados(estadoCuentaTMP, listaConceptosAcumulador,estadoCuentaSN));
		}
		
		//Se tienen los n saldos totales de cada suscripcion, se procede a unirlos por concepto
		List<SaldoConceptoDTO> listaSaldosCombinados = obtenerSaldosAgrupadosPorEjercicio(listaEstadoCuentaEjerciciosAnteriores, listaConceptosAcumulador);
		
		listaEstadoCuentaEjerciciosAnteriores.get(0).setSaldosAcumulados(listaSaldosCombinados);
		listaEstadoCuentaEjerciciosAnteriores.get(0).setSaldoTecnico(this.obtenerSaldosAcumulados(listaSaldosCombinados,null));
		
		return listaEstadoCuentaEjerciciosAnteriores.get(0);
	}
	
	private List<SaldoConceptoDTO> obtenerSaldosAgrupadosPorEjercicio(List<EstadoCuentaDecoradoDTO> listaEstadoCuentaEjerciciosAnteriores,List<ConceptoAcumuladorDTO> listaConceptosAcumulador){
		List<SaldoConceptoDTO> listaSaldosCombinados = new ArrayList<SaldoConceptoDTO>();
		String descripcionPeriodo = null;
		
		ConceptoAcumuladorDTO conceptoPrimaAnterior = new ConceptoAcumuladorDTO();
		conceptoPrimaAnterior.setConceptoMovimientoDTO(new ConceptoMovimientoDTO());
		conceptoPrimaAnterior.getConceptoMovimientoDTO().setDescripcion("Saldo Anterior");
		listaConceptosAcumulador.add(0, conceptoPrimaAnterior);
		//iterar los diferentes conceptos
		for(ConceptoAcumuladorDTO conceptoTMP : listaConceptosAcumulador){
			SaldoConceptoDTO saldoConceptoEnCurso = null;
			//cada concepto se buscar� en los diferentes estados de cuenta 
			for(EstadoCuentaDecoradoDTO estadoCuentaEnCurso : listaEstadoCuentaEjerciciosAnteriores){
				descripcionPeriodo = " ("+estadoCuentaEnCurso.getEjercicio()+")";
				for(SaldoConceptoDTO saldoConceptoTMP : estadoCuentaEnCurso.getSaldosAcumulados()){
					if(saldoConceptoTMP.getDescripcionConcepto().compareTo(conceptoTMP.getConceptoMovimientoDTO().getDescripcion()) == 0){
						saldoConceptoEnCurso = saldoConceptoTMP;
						break;
					}
				}
				if(saldoConceptoEnCurso != null){
					//TODO si el concepto no trae saldo (en ceros), no agregarlo a la lista
					saldoConceptoEnCurso.setDescripcionConcepto(saldoConceptoEnCurso.getDescripcionConcepto()+descripcionPeriodo);
					listaSaldosCombinados.add(saldoConceptoEnCurso);
					saldoConceptoEnCurso = null;
				}
			}
		}
		return listaSaldosCombinados;
	}
	
	/**
	 * 
	 * @param estadoCuentaDTO
	 * @param nombreUsuario
	 * @return
	 * @throws SystemException
	 */
	private List<EstadoCuentaDTO> consultarEstadosCuentaContratosAnteriores(EstadoCuentaDTO estadoCuentaDTO,String nombreUsuario) throws SystemException{
		List<EstadoCuentaDTO> listaEstadoCuentaContratosAnteriores = null;
		CalculoSuscripcionesDTO calculoSuscripciones = null;
		
		//Calcular una fecha entre el rango, para utilizarla como refrencia al buscar suscripciones de otros periodos.
		calculoSuscripciones = new CalculoSuscripcionesDTO();
		SuscripcionDTO suscripcionPeriodoActual = calculoSuscripciones.getSuscripcion(new EstadoCuentaDecoradoDTO(estadoCuentaDTO), estadoCuentaDTO.getSuscripcion());
		Calendar fechaEntrePeriodoEstadoCuenta = Calendar.getInstance();
		fechaEntrePeriodoEstadoCuenta.setTime(suscripcionPeriodoActual.getFechaInicial());
		fechaEntrePeriodoEstadoCuenta.add(Calendar.DATE, 5);
		
		//Obtener los contratos CP o 1E anteriores del mismo reasegurador
		if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE){
			List<ContratoCuotaParteDTO> listaContratosCPAnteriores = ContratoCuotaParteDN.getInstancia(nombreUsuario).obtenerListaContratoCuotaParteEjercicioAnterior(estadoCuentaDTO);
			if(listaContratosCPAnteriores != null && !listaContratosCPAnteriores.isEmpty()){
				listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
				for(ContratoCuotaParteDTO contratoCPEnCurso : listaContratosCPAnteriores){
					//Identificar el anio del contrato, en base  a su anio, determinar su suscripcion correspondiente a la del estado de cuenta del ejercicio actual
					Calendar fechaInicioContrato = Calendar.getInstance();
					fechaInicioContrato.setTime(contratoCPEnCurso.getFechaInicial());
					int anioContrato = fechaInicioContrato.get(Calendar.YEAR);
					calculoSuscripciones = new CalculoSuscripcionesDTO(anioContrato,contratoCPEnCurso.getFechaInicial(),
							estadoCuentaDTO.getContratoCuotaParteDTO().getFechaFinal(),contratoCPEnCurso.getFormaPago());
					
					SuscripcionDTO suscripcionPeriodoAnterior = calculoSuscripciones.obtenerPeriodo(fechaEntrePeriodoEstadoCuenta.getTime());
					int suscripcion = suscripcionPeriodoAnterior.getIdSuscripcion();
					
					EstadoCuentaDTO estadoCuentaPeriodoEnCursoDTO = null;
					estadoCuentaPeriodoEnCursoDTO = obtenerEstadoCuentaHistorico(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor(),
							estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo(), contratoCPEnCurso, null,null, estadoCuentaDTO.getIdMoneda(), anioContrato, suscripcion);
					
					if(estadoCuentaPeriodoEnCursoDTO != null){
						listaEstadoCuentaContratosAnteriores.add(estadoCuentaPeriodoEnCursoDTO);
					}
				}
			}
		}else if(estadoCuentaDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE){
			List<ContratoPrimerExcedenteDTO> listaContratos1EAnteriores = ContratoPrimerExcedenteDN.getInstancia(nombreUsuario).obtenerListaContratoPrimerExcedenteEjercicioAnterior(estadoCuentaDTO);
			if(listaContratos1EAnteriores != null && !listaContratos1EAnteriores.isEmpty()){
				listaEstadoCuentaContratosAnteriores = new ArrayList<EstadoCuentaDTO>();
				for(ContratoPrimerExcedenteDTO contrato1EEnCurso : listaContratos1EAnteriores){
					//Identificar el anio del contrato, en base  a su anio, determinar su suscripcion correspondiente a la del estado de cuenta del ejercicio actual
					Calendar fechaInicioContrato = Calendar.getInstance();
					fechaInicioContrato.setTime(contrato1EEnCurso.getFechaInicial());
					int anioContrato = fechaInicioContrato.get(Calendar.YEAR);
					calculoSuscripciones = new CalculoSuscripcionesDTO(anioContrato,contrato1EEnCurso.getFechaInicial(),
							estadoCuentaDTO.getContratoPrimerExcedenteDTO().getFechaFinal(),contrato1EEnCurso.getFormaPago());
					
					SuscripcionDTO suscripcionPeriodoAnterior = calculoSuscripciones.obtenerPeriodo(fechaEntrePeriodoEstadoCuenta.getTime());
					int suscripcion = suscripcionPeriodoAnterior.getIdSuscripcion();
					
					EstadoCuentaDTO estadoCuentaPeriodoEnCursoDTO = null;
					estadoCuentaPeriodoEnCursoDTO = obtenerEstadoCuentaHistorico(estadoCuentaDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor(),
							estadoCuentaDTO.getLineaDTO().getSubRamo().getIdTcSubRamo(), null, contrato1EEnCurso,null, estadoCuentaDTO.getIdMoneda(), anioContrato, suscripcion);
					
					if(estadoCuentaPeriodoEnCursoDTO != null){
						listaEstadoCuentaContratosAnteriores.add(estadoCuentaPeriodoEnCursoDTO);
					}
				}
			}
		}
		
		return listaEstadoCuentaContratosAnteriores;
	}
	
	public EstadoCuentaDTO obtenerEstadoCuentaHistorico(BigDecimal idTcReasegurador,BigDecimal idTcSubRamo,ContratoCuotaParteDTO contratoCuotaParteDTO, 
			ContratoPrimerExcedenteDTO contratoPrimerExcedenteDTO,Integer tipoReaseguro,int idMoneda,int ejercicio,int suscripcion) throws SystemException{
//		boolean consultaValida = true;
		EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
		EstadoCuentaFiltroDTO filtroEstadoCtaEnCurso = new EstadoCuentaFiltroDTO();
		filtroEstadoCtaEnCurso.setEjercicio(ejercicio);
		filtroEstadoCtaEnCurso.setIdMoneda(idMoneda);
		filtroEstadoCtaEnCurso.setIdtcreaseguradorcorredor(idTcReasegurador);
		filtroEstadoCtaEnCurso.setIdTcSubRamo(idTcSubRamo);
		//TODO falta settear el corredor al filtro
		
		int tipoReaseguroLocal = 0;
		if(contratoCuotaParteDTO != null && contratoCuotaParteDTO.getIdTmContratoCuotaParte() != null){
			tipoReaseguroLocal = TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE;
			filtroEstadoCtaEnCurso.setIdTmContratoCuotaParte(contratoCuotaParteDTO.getIdTmContratoCuotaParte());
		}
		else if(contratoPrimerExcedenteDTO != null && contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente() != null){
			tipoReaseguroLocal = TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE;
			filtroEstadoCtaEnCurso.setIdTmContratoPrimerExcedente(contratoPrimerExcedenteDTO.getIdTmContratoPrimerExcedente());
		}
		else if(tipoReaseguro != null && tipoReaseguro != TipoReaseguroDTO.TIPO_RETENCION && tipoReaseguro != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
			tipoReaseguroLocal = tipoReaseguro;
		}
		else{
			throw new SystemException("No se recibi� un contrato cuota parte ni primer excedente ni especificaci�n v�lida del tipo de reaseguro.");
		}
		
		filtroEstadoCtaEnCurso.setTipoReaseguro(tipoReaseguroLocal);
		
		List<EstadoCuentaDTO> listaEstadosCuentaPeriodoEnCurso = null;
		EstadoCuentaDTO estadoCuentaPeriodoEnCursoDTO = null;
		EstadoCuentaDTO estadoCuentaTMP = null;
		
		boolean estadoCuentaEncontrado = false;
		
//		if(consultaValida){
			while(suscripcion >= 1){//Buscar el mismo estado de cuenta para la suscripcion anterior
				filtroEstadoCtaEnCurso.setSuscripcion(suscripcion);
				listaEstadosCuentaPeriodoEnCurso = estadoCuentaSN.obtenerEstadosCuenta(filtroEstadoCtaEnCurso);
				//Si encuentra el estado de cuenta, se busca su historial y termina la b�squeda.
				if(listaEstadosCuentaPeriodoEnCurso != null && !listaEstadosCuentaPeriodoEnCurso.isEmpty()){
					estadoCuentaTMP = listaEstadosCuentaPeriodoEnCurso.get(0);
					estadoCuentaTMP = estadoCuentaSN.obtenerEstadoCuentaHistorico(estadoCuentaTMP.getIdEstadoCuenta());
					if(estadoCuentaPeriodoEnCursoDTO != null){
						obtenerUltimoEstadoCuenta(estadoCuentaPeriodoEnCursoDTO).setEstadoCuentaSuscripcionAnterior(estadoCuentaTMP);
					}else{
						estadoCuentaPeriodoEnCursoDTO = estadoCuentaTMP;
					}
					estadoCuentaEncontrado = true;
					break;
				}
				else{//Si no lo encuentra, se simula uno y se continua la b�squeda hacia atr�s
					estadoCuentaTMP = duplicarEstadoCuenta(null, contratoCuotaParteDTO, contratoPrimerExcedenteDTO, ejercicio, suscripcion);
					if(estadoCuentaPeriodoEnCursoDTO == null){
						estadoCuentaPeriodoEnCursoDTO = estadoCuentaTMP;
					}
					else{
						obtenerUltimoEstadoCuenta(estadoCuentaPeriodoEnCursoDTO).setEstadoCuentaSuscripcionAnterior(estadoCuentaTMP);
					}
					suscripcion = suscripcion -1;
				}
			}
//		}
		
		if(estadoCuentaEncontrado){//Si se encontr� un estado de cuenta, los datos del reasegurador se pasan al primer estado de cuenta de la suscripci�n consultada originalmente
			EstadoCuentaDTO estadoCuentaReal = obtenerPrimerEstadoCuentaConContratoYReasegurador(estadoCuentaPeriodoEnCursoDTO);
			if(estadoCuentaReal != null){
				copiarDatosEstadoCuenta(estadoCuentaReal, estadoCuentaPeriodoEnCursoDTO);
			}
		}
		else{//Si no se encontr� estado de cuenta, no hay nada que mostrar
			estadoCuentaPeriodoEnCursoDTO = null;
		}
		
		return estadoCuentaPeriodoEnCursoDTO;
	}
	
	private EstadoCuentaDTO obtenerUltimoEstadoCuenta(EstadoCuentaDTO estadoCuentaDTO){
		if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() != null)
			return obtenerUltimoEstadoCuenta(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior());
		else
			return estadoCuentaDTO;
	}
	
	private EstadoCuentaDTO obtenerPrimerEstadoCuentaConContratoYReasegurador(EstadoCuentaDTO estadoCuentaDTO){
		//Si el estado de cuenta tiene id tipo reaseguro, contrato y reasegurador o corredor
		if (estadoCuentaDTO.getIdEstadoCuenta() != null && 
				estadoCuentaDTO.getIdEstadoCuenta().compareTo(BigDecimal.ZERO) > 0){
			return estadoCuentaDTO;
		}
		else if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() != null)
			return obtenerUltimoEstadoCuenta(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior());
		else
			return null;
	}
	
	private void copiarDatosEstadoCuenta(EstadoCuentaDTO estadoCuentaOrigen,EstadoCuentaDTO estadoCuentaDestino){
		estadoCuentaDestino.setContratoCuotaParteDTO(estadoCuentaOrigen.getContratoCuotaParteDTO());
		estadoCuentaDestino.setContratoFacultativoDTO(estadoCuentaOrigen.getContratoFacultativoDTO());
		estadoCuentaDestino.setContratoPrimerExcedenteDTO(estadoCuentaOrigen.getContratoPrimerExcedenteDTO());
		estadoCuentaDestino.setCorredorDTO(estadoCuentaOrigen.getCorredorDTO());
		estadoCuentaDestino.setEjercicio(estadoCuentaOrigen.getEjercicio());
		estadoCuentaDestino.setIdEstadoCuenta(BigDecimal.ZERO);
		estadoCuentaDestino.setIdMoneda(estadoCuentaOrigen.getIdMoneda());
		estadoCuentaDestino.setLineaDTO(estadoCuentaOrigen.getLineaDTO());
		estadoCuentaDestino.setReaseguradorCorredorDTO(estadoCuentaOrigen.getReaseguradorCorredorDTO());
		estadoCuentaDestino.setSubRamoDTO(estadoCuentaOrigen.getSubRamoDTO());
		estadoCuentaDestino.setTipoReaseguro(estadoCuentaOrigen.getTipoReaseguro());
	}
	
	public SaldoConceptoDTO obtenerSaldosAcumulados(List<SaldoConceptoDTO> listaSaldosConceptos, SaldoConceptoDTO saldoAnterior){
		BigDecimal saldoAcumulado = new BigDecimal(0);
		SaldoConceptoDTO ultimoSaldoConcepto;
		SaldoConceptoDTO saldoConceptoDTO = new SaldoConceptoDTO();
		boolean sumaSaldoAnterior = false;
		if(saldoAnterior != null){
			listaSaldosConceptos.add(0,saldoAnterior);
			sumaSaldoAnterior = true;
		}
		if (listaSaldosConceptos != null && listaSaldosConceptos.size() > 0){
			for (SaldoConceptoDTO saldoConceptoDTO2 : listaSaldosConceptos) {
				if(saldoConceptoDTO2.getDescripcionConcepto().startsWith("Saldo Anterior") || sumaSaldoAnterior){
					saldoAcumulado = saldoAcumulado.add(saldoConceptoDTO2.getSaldoAcumulado());
					sumaSaldoAnterior = false;
				}
				else{
					saldoAcumulado = saldoAcumulado.add(saldoConceptoDTO2.getSaldo());
				}
				saldoConceptoDTO2.setSaldoAcumulado(saldoAcumulado);
			}
			ultimoSaldoConcepto = listaSaldosConceptos.get(listaSaldosConceptos.size() - 1);
			saldoConceptoDTO.setDescripcionConcepto("Saldo T�cnico");
			saldoConceptoDTO.setSaldo(ultimoSaldoConcepto.getSaldo());
			saldoConceptoDTO.setSaldoAcumulado(ultimoSaldoConcepto.getSaldoAcumulado());
		}
		
		return saldoConceptoDTO;
	}
	
	private EstadoCuentaDecoradoDTO obtenerEstadoCuentaConSaldosCalculados(EstadoCuentaDTO estadoCuentaDTO,List<ConceptoAcumuladorDTO> listaConceptosAcumulador,EstadoCuentaSN estadoCuentaSN){
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoSaldoAnterior = null;
		if(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior() != null){
			estadoCuentaDecoradoSaldoAnterior = obtenerEstadoCuentaConSaldosCalculados(estadoCuentaDTO.getEstadoCuentaSuscripcionAnterior(), listaConceptosAcumulador,estadoCuentaSN);
		}
		else{
			List<SaldoConceptoDTO> listaSaldosConceptos = estadoCuentaSN.obtenerSaldosIndividuales(estadoCuentaDTO,listaConceptosAcumulador,estadoCuentaDTO.getAcumuladorDTOs(),false);
			
			//Una vez que se tienen los saldos por concepto, se acumulan para calcular el saldo t�cnico
			SaldoConceptoDTO saldoTecnico = this.obtenerSaldosAcumulados(listaSaldosConceptos, null);
			
			//Se regresa el estado de cuenta para acumularlo hacia arriba
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO =  new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
			estadoCuentaDecoradoDTO.setSaldosAcumulados(listaSaldosConceptos);
			estadoCuentaDecoradoDTO.setSaldoTecnico(saldoTecnico);
			saldoTecnico.setFechaTrimestre(estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
			return estadoCuentaDecoradoDTO;
		}
		SaldoConceptoDTO saldoTecnicoAnterior = estadoCuentaDecoradoSaldoAnterior.getSaldoTecnico();
		List<SaldoConceptoDTO> listaSaldosConceptos = estadoCuentaSN.obtenerSaldosIndividuales(estadoCuentaDTO,listaConceptosAcumulador,estadoCuentaDTO.getAcumuladorDTOs(),false);
		saldoTecnicoAnterior.setDescripcionConcepto("Saldo Anterior");
		if(saldoTecnicoAnterior.getSaldoAcumulado() != null && saldoTecnicoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) > 0){
			saldoTecnicoAnterior.setHaber(saldoTecnicoAnterior.getSaldoAcumulado());
		}else if (saldoTecnicoAnterior.getSaldoAcumulado() != null && saldoTecnicoAnterior.getSaldoAcumulado().compareTo(BigDecimal.ZERO) < 0){
			saldoTecnicoAnterior.setDebe(saldoTecnicoAnterior.getSaldoAcumulado().abs());
		}
		listaSaldosConceptos.add(0,saldoTecnicoAnterior);
		//Una vez que se tienen los saldos por concepto, se acumulan para calcular el saldo t�cnico
		SaldoConceptoDTO saldoTecnico = this.obtenerSaldosAcumulados(listaSaldosConceptos, null);
		
		//Se regresa el estado de cuenta para acumularlo hacia arriba
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO =  new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
		estadoCuentaDecoradoDTO.setSaldosAcumulados(listaSaldosConceptos);
		estadoCuentaDecoradoDTO.setSaldoTecnico(saldoTecnico);
		saldoTecnico.setFechaTrimestre(estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
		return estadoCuentaDecoradoDTO;
	}
	
	private EstadoCuentaDTO duplicarEstadoCuenta(EstadoCuentaDTO estadoCuentaOriginal,ContratoCuotaParteDTO contratoCP,ContratoPrimerExcedenteDTO contrato1E,int anioContrato,int suscripcion){
		EstadoCuentaDTO estadoCuentaDTO = null;
		if(estadoCuentaOriginal != null){
			estadoCuentaDTO = estadoCuentaOriginal.duplicate();
			estadoCuentaDTO.setContratoCuotaParteDTO(contratoCP);
			estadoCuentaDTO.setContratoPrimerExcedenteDTO(contrato1E);
			estadoCuentaDTO.setAcumuladorDTOs(new ArrayList<AcumuladorDTO>());
			estadoCuentaDTO.setEjercicio(anioContrato);
			estadoCuentaDTO.setSuscripcion(suscripcion);
			estadoCuentaDTO.setIdEstadoCuenta(null);
			estadoCuentaDTO.setEstadoCuentaSuscripcionAnterior(null);
		}
		else{
			estadoCuentaDTO = new EstadoCuentaDTO();
			estadoCuentaDTO.setAcumuladorDTOs(new ArrayList<AcumuladorDTO>());
			if(contratoCP != null){
				estadoCuentaDTO.setContratoCuotaParteDTO(contratoCP);
				estadoCuentaDTO.setIdMoneda(contratoCP.getIdTcMoneda().intValue());
				estadoCuentaDTO.setTipoReaseguro(TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);
			}
			else if(contrato1E != null){
				estadoCuentaDTO.setContratoPrimerExcedenteDTO(contrato1E);
				estadoCuentaDTO.setIdMoneda(contrato1E.getIdMoneda().intValue());
				estadoCuentaDTO.setTipoReaseguro(TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);
			}
			estadoCuentaDTO.setEjercicio(anioContrato);
			estadoCuentaDTO.setSuscripcion(suscripcion);
		}
		return estadoCuentaDTO;
	}
	
	public String obtenerDireccionReasegurador(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO){
		String direccionReasegurador = null;
		String pais = "";
		String estado = "";
		String ciudad = "";
		try{
			if (estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO() != null){
				if (!UtileriasWeb.esCadenaVacia(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getEstado())){
					CiudadDTO ciudadDTO = CodigoPostalDN.getInstancia().getCiudadPorId(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getCiudad());
					if (ciudadDTO != null){
						ciudad = ciudadDTO.getDescription();
					}
				}if (!UtileriasWeb.esCadenaVacia(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getEstado())){
					EstadoDTO estadoDTO = CodigoPostalDN.getInstancia().getEstadoPorId(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getEstado());
					if (estadoDTO != null){
						estado = estadoDTO.getDescription();
					}
				}
				if (!UtileriasWeb.esCadenaVacia(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getPais())){
					PaisDTO paisDTO = new PaisDTO();
					paisDTO.setCountryId(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getPais());
					paisDTO = PaisTipoDestinoTransporteDN.getInstancia().getPaisPorId(paisDTO);
					pais = paisDTO.getDescription();
				}
				direccionReasegurador = estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getUbicacion() + ", " + ciudad +
				", " + estado + ", " + pais;
			}
		}catch(Exception e){
			direccionReasegurador = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.datoNoDisponible");
		}
		return direccionReasegurador;
	}
	
	public String obtenerDescripcionMoneda(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO){
		String descripcionMoneda = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.datoNoDisponible");
		try{
			if (estadoCuentaDecoradoDTO.getIdMoneda() != 0){
				Integer idMoneda = estadoCuentaDecoradoDTO.getIdMoneda();
				MonedaDTO monedaDTO = MonedaDN.getInstancia().getPorId(idMoneda.shortValue());
				if (monedaDTO != null)
					descripcionMoneda = monedaDTO.getDescripcion();
			}
		}catch(Exception e){}
		return descripcionMoneda;
	}
	
	public List<PolizaSoporteDanosDTO> listarPolizasFacultativasPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda) throws SystemException{
		PolizaSoporteDanosDTO polizaTMP = null;
		List<PolizaSoporteDanosDTO> listaPolizasEncontradas = new ArrayList<PolizaSoporteDanosDTO>();
		List<BigDecimal> listaIdToPoliza = new EstadoCuentaSN().filtrarPolizasFacultativasPorReaseguradorMoneda(idTcReasegurador, idMoneda);
		for(BigDecimal idToPoliza : listaIdToPoliza){
			if(idToPoliza != null){
				polizaTMP = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(idToPoliza);
				listaPolizasEncontradas.add(polizaTMP);
			}
		}
		return listaPolizasEncontradas;
	}
	
	public List<ReporteSiniestroDTO> listarSiniestrosFacultativosPorReaseguradorMoneda(BigDecimal idTcReasegurador,BigDecimal idMoneda,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		List<ReporteSiniestroDTO> reporteSiniestros = new ArrayList<ReporteSiniestroDTO>();
		ReporteSiniestroDTO reporteSiniestroTMP = null;
		List<BigDecimal> listaIdReporteSiniestro = new EstadoCuentaSN().filtrarSiniestrosFacultativosPorReaseguradorMoneda(idTcReasegurador, idMoneda);
		if(listaIdReporteSiniestro != null){
			for(BigDecimal idReporteSiniestro : listaIdReporteSiniestro){
				if(idReporteSiniestro != null){
					reporteSiniestroTMP = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idReporteSiniestro);
					if(reporteSiniestroTMP != null){
						reporteSiniestros.add(reporteSiniestroTMP);
					}
				}
			}
		}
		return reporteSiniestros;
	}		
}