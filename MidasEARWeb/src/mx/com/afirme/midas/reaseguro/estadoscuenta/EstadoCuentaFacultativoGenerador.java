package mx.com.afirme.midas.reaseguro.estadoscuenta;

import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;

public class EstadoCuentaFacultativoGenerador implements Runnable{
	
	private ContratoFacultativoDTO contratoFacultativoDTO;

	public void run() {
		try{
			EstadoCuentaDN.getINSTANCIA().crearEstadosCuentaFacultativo(contratoFacultativoDTO);
			
		} catch (Exception exc){
			LogDeMidasWeb.log("Excepci�n en Proceso Generador de Estados de Cuenta al Autorizar Contrato Facultativo: "
					+ exc.getMessage(), Level.ALL, exc);			
		}

		
	}

	public ContratoFacultativoDTO getContratoFacultativoDTO() {
		return contratoFacultativoDTO;
	}

	public void setContratoFacultativoDTO(
			ContratoFacultativoDTO contratoFacultativoDTO) {
		this.contratoFacultativoDTO = contratoFacultativoDTO;
	}

}
