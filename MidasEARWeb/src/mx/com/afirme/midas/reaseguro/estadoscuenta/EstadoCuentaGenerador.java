package mx.com.afirme.midas.reaseguro.estadoscuenta;



import java.util.logging.Level;


import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;


public class EstadoCuentaGenerador implements Runnable{
	
	private LineaDTO lineaDTO;

	public void run() {
		try {
			EstadoCuentaDN.getINSTANCIA().crearEstadosCuentaLinea(lineaDTO);
		}
		catch (Exception exc){
			LogDeMidasWeb.log("Excepci�n en Proceso Generador de Estados de Cuenta al Autorizar L�nea: "
					+ exc.getMessage(), Level.ALL, exc);
		}
	}

	public LineaDTO getLineaDTO() {
		return lineaDTO;
	}

	public void setLineaDTO(LineaDTO lineaDTO) {
		this.lineaDTO = lineaDTO;
	}

}
