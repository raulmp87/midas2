package mx.com.afirme.midas.reaseguro.estadoscuenta.facultativo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorAction;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorForm;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.PresentacionEstadoCuentaDN;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaContratoFacultativoForm;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadosCuentaAction;
import mx.com.afirme.midas.reaseguro.movimiento.MovimientoReaseguroDN;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo.ReporteMovimientosCuentaPorPagarEstadoCuentaFacultativo;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.reportes.CaratulaSiniestroForm;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EstadoCuentaFacultativoAction extends MidasMappingDispatchAction{

	/**
	 * 
	 */
	public ActionForward mostrarDetalleEstadoCuentaFacultativoPorPoliza(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm = (EstadoCuentaContratoFacultativoForm) form;
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
		EstadoCuentaDTO estadoCuentaDTO;
		String idPolizaSubRamo = request.getParameter("idPolizaSubRamo");
		String idPoliza = request.getParameter("idPoliza");
		String fechaHasta = request.getParameter("fechaHasta");
		String separarPorReasegurador = request.getParameter("separarPorReasegurador");
		try {
			BigDecimal idToPoliza = null;
			BigDecimal idTcSubRamo = null;
			String descripcionSubRamo = "";
			StringBuilder descripcion = new StringBuilder("");
			List<BigDecimal> listaIdTcSubRamo = new ArrayList<BigDecimal>();
			if (!UtileriasWeb.esCadenaVacia(idPolizaSubRamo) && !idPolizaSubRamo.equals("undefined")){
				String[] idArray = idPolizaSubRamo.split(",");
				for(int i=0;i<idArray.length;i++){
					String[] idAnidado = idArray[i].split("_");
					if(i==0){
						idToPoliza = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
					}
					idTcSubRamo = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
					if(!listaIdTcSubRamo.contains(idTcSubRamo))
						listaIdTcSubRamo.add(idTcSubRamo);
				}
				for(BigDecimal idSubRamoTMP : listaIdTcSubRamo){
					SubRamoDTO subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(idSubRamoTMP);
					if(subRamoDTO != null){
						descripcion.append(subRamoDTO.getDescripcionSubRamo() ).append(", ");
					}
				}
				descripcionSubRamo = descripcion.toString();
				if(descripcionSubRamo.length() > 0)
					descripcionSubRamo = descripcionSubRamo.substring(0, descripcionSubRamo.length()-2);
			}
			else if(!UtileriasWeb.esCadenaVacia(idPoliza)){
				idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
			}
			SoporteDanosDN soporteDanosDN =  SoporteDanosDN.getInstancia();
			estadoCuentaDTO = new EstadoCuentaDTO();
			estadoCuentaDTO.setSubRamoDTO(new SubRamoDTO());
			estadoCuentaDTO.getSubRamoDTO().setDescripcionSubRamo(descripcionSubRamo);
			estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
		 	PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			polizaSoporteDanosDTO.setIdToPoliza(idToPoliza);
			List<PolizaSoporteDanosDTO> polizaSoporteDanosList = soporteDanosDN.buscarPolizasFiltrado(polizaSoporteDanosDTO);
			estadoCuentaDecoradoDTO.setEndosoSoporteDanosDTO(SoporteDanosDN.getInstancia().getUltimoEndoso(idToPoliza));
			estadoCuentaDecoradoDTO.setPolizaSoporteDanosDTO(polizaSoporteDanosList.get(0));
	 		EstadosCuentaAction.poblarEstadoCuentaContratoFacultativoFormConEstadoCuentaDecoradoDTO(estadoCuentaDecoradoDTO, estadoCuentaContratoFacultativoForm);
	 		estadoCuentaContratoFacultativoForm.setDescripcionMoneda(polizaSoporteDanosList.get(0).getDescripcionMoneda());
	 		estadoCuentaContratoFacultativoForm.setClavePolizaSubramo(idPolizaSubRamo);
	 		estadoCuentaContratoFacultativoForm.setEndoso(""+estadoCuentaDecoradoDTO.getEndosoSoporteDanosDTO().getNumeroEndoso());
	 		estadoCuentaContratoFacultativoForm.setHasta(fechaHasta);
	 		estadoCuentaContratoFacultativoForm.setIdPoliza(idPoliza);
	 		estadoCuentaContratoFacultativoForm.setSepararConceptosPorAcumulador(separarPorReasegurador);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
 		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Action para consultar los estados de cuenta por subramo de una p�liza facultativa. Escribe el resultado de la consulta en formato json en el response.
	 */
	public void mostrarSiniestros(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String json = "{rows:[]}";
		try{
			String numeroPoliza = request.getParameter("numPoliza");
			String nombreAsegurado = request.getParameter("nomAsegurado");
			String numeroReporteSiniestro = request.getParameter("numSiniestro");
			String fechaSiniestro = request.getParameter("fechaSiniestro");
			
			String idTcReasegurador = request.getParameter("idTcReasegurador");
			String idMoneda = request.getParameter("idMoneda");
			
			boolean busquedaPorReasegurador = true;
			
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			if (UtileriasWeb.esCadenaVacia(nombreAsegurado) || nombreAsegurado.equals("undefined") || nombreAsegurado.equals("null")){
				nombreAsegurado = "";
			}
			if (!UtileriasWeb.esCadenaVacia(numeroPoliza)){
				reporteSiniestroDTO.setNumeroPolizaCliente(numeroPoliza);
				busquedaPorReasegurador = false;
			}
			if (!UtileriasWeb.esCadenaVacia(fechaSiniestro)){
				reporteSiniestroDTO.setFechaHoraReporte(UtileriasWeb.getFechaFromString(fechaSiniestro));
				busquedaPorReasegurador = false;
			}
			if (!UtileriasWeb.esCadenaVacia(numeroReporteSiniestro)){
				reporteSiniestroDTO.setNumeroReporte(numeroReporteSiniestro);
				busquedaPorReasegurador = false;
			}
			
			List<ReporteSiniestroDTO> reporteSiniestros = null;
			
			if(!busquedaPorReasegurador){
				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				reporteSiniestros = reporteSiniestroDN.listarFiltrado(reporteSiniestroDTO, nombreAsegurado);
			}
			else if (!UtileriasWeb.esCadenaVacia(idTcReasegurador) &&!UtileriasWeb.esCadenaVacia(idMoneda)){
				reporteSiniestros = EstadoCuentaDN.getINSTANCIA().
						listarSiniestrosFacultativosPorReaseguradorMoneda(UtileriasWeb.regresaBigDecimal(idTcReasegurador), UtileriasWeb.regresaBigDecimal(idMoneda), "TMP");
			}
			
			json = PresentacionEstadoCuentaDN.getInstancia().obtenerCadenaJsonEstadoCuentaFacultativoPorSiniestro(reporteSiniestros);
			
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		finally{
			try{
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			}
			catch(IOException e){
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	/**
	 * Action para consultar los subramos afectados en un siniestro. Escribe el resultado de la consulta en formato json en el response.
	 */
	public void mostrarSubRamosAfectadosPorSiniestro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try{
			String idReporteSiniestro = request.getParameter("idReporteSiniestro");
			
			if (!UtileriasWeb.esCadenaVacia(idReporteSiniestro)){
				
//				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
//				ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(idReporteSiniestro));
				
				String json = "{rows:[{}]}";
				
				List<SubRamoDTO> listaSubRamosAfectados = MovimientoReaseguroDN.getInstancia().obtenerSubRamosFacultativosAfectadosPorSiniestro(UtileriasWeb.regresaBigDecimal(idReporteSiniestro));
				
				if(listaSubRamosAfectados != null){
					MidasJsonBase jsonBase = new MidasJsonBase();
					MidasJsonRow row;
					for(SubRamoDTO subramoDTO : listaSubRamosAfectados){
						row = new MidasJsonRow();
						row.setId(idReporteSiniestro+"_"+subramoDTO.getIdTcSubRamo().toBigInteger().toString());
						row.setDatos("0",subramoDTO.getDescripcionSubRamo());
						jsonBase.getRows().add(row);
					}
					json = jsonBase.toString();
				}
				
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			}
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	/**
	 * Action que consulta la informaci�n de un siniestro
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarDetalleEstadoCuentaFacultativoPorSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm = (EstadoCuentaContratoFacultativoForm) form;
		//Este par�metro indica que se realiza la consulta por todo el siniestro, sin filtro por subramo
		String idReporteSiniestro = request.getParameter("idReporteSiniestro");
		//Este par�metro indica que se realiza la consulta por siniestro y subramos. 
		//Se recibe en formato idReporteSiniestro+"_"+idSubramo1+","+idReporteSiniestro+"_idSubramo2+","+...
		String idSiniestroSubRamo = request.getParameter("idSiniestroSubRamo");
		String fechaHasta = request.getParameter("fechaHasta");
		String separarPorReasegurador = request.getParameter("separarPorReasegurador");
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		BigDecimal idToReporteSiniestro = null;
		List<SubRamoDTO> listaSubRamosIncluidos = new ArrayList<SubRamoDTO>();
		try {
			if (!UtileriasWeb.esCadenaVacia(idReporteSiniestro) && 
					!idReporteSiniestro.equals("undefined") && !idReporteSiniestro.equals("null")){
				idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(idReporteSiniestro);
				listaSubRamosIncluidos = MovimientoReaseguroDN.getInstancia().obtenerSubRamosFacultativosAfectadosPorSiniestro(idToReporteSiniestro);
			}
			else if(!UtileriasWeb.esCadenaVacia(idSiniestroSubRamo) && 
					!idSiniestroSubRamo.equals("undefined") && !idSiniestroSubRamo.equals("null")){
				String[] idArray = idSiniestroSubRamo.split(",");
				if(idArray.length > 0){
					for(int i=0;i<idArray.length;i++){
						String[] idAnidado = idArray[i].split("_");
						if(i==0){
							idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
						}
						listaSubRamosIncluidos.add(SubRamoDN.getInstancia().getSubRamoPorId(UtileriasWeb.regresaBigDecimal(idAnidado[1])));
					}
				}
			}
			if(!poblarFormEstadoCuentaPorSiniestro(idToReporteSiniestro, nombreUsuario,idSiniestroSubRamo, estadoCuentaContratoFacultativoForm,listaSubRamosIncluidos)){
				reglaNavegacion = "noEncontrado";
			}
			estadoCuentaContratoFacultativoForm.setHasta(fechaHasta);
			estadoCuentaContratoFacultativoForm.setSepararConceptosPorAcumulador(separarPorReasegurador);
			
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (Exception e) {
			LogDeMidasWeb.log("Error en EstadoCuentaFacultativoAction.mostrarDetalleEstadoCuentaFacultativoPorSiniestro(...)", Level.SEVERE, e);
			reglaNavegacion = Sistema.NO_EXITOSO;
 		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Action que consulta la informaci�n de un reasegurador y la p�liza o siniestro cuyo estado de cuenta facultativo solicita el usuario
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarDetalleEstadoCuentaFacultativoPorReasegurador(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm = (EstadoCuentaContratoFacultativoForm) form;
		//Lectura de par�metros
		//Este par�metro indica el tipo de consulta que se realiza
		String tipoConsulta = request.getParameter("tipoConsulta");
		String idReasegurador = request.getParameter("idReasegurador");
		String idMoneda = request.getParameter("idMoneda");
		String idSubRamo = request.getParameter("idSubRamo");
		String hasta = request.getParameter("hasta");
		String idSiniestro = request.getParameter("idSiniestro");
		String idPoliza = request.getParameter("idPoliza");
		//Finaliza lectura de par�metros
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		try {
			if (validaParametro(tipoConsulta) && validaParametro(idReasegurador)){
				BigDecimal idTcReaseguradorCorredor = UtileriasWeb.regresaBigDecimal(idReasegurador);
				ReaseguradorCorredorDTO reaseguradorDTO = ReaseguradorCorredorDN.getInstancia().obtenerReaseguradorPorId(idTcReaseguradorCorredor);
				if(reaseguradorDTO != null){
					estadoCuentaContratoFacultativoForm.setReaseguradorForm(new ReaseguradorCorredorForm());
					ReaseguradorCorredorAction.poblarForm(estadoCuentaContratoFacultativoForm.getReaseguradorForm(), reaseguradorDTO);
					estadoCuentaContratoFacultativoForm.setIdTcMoneda(null);
					if(tipoConsulta.equalsIgnoreCase("saldoPorSiniestro") && validaParametro(idSiniestro)){//Consulta de saldo incluyendo s�lo un siniestro
						BigDecimal idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(idSiniestro);
//						ReporteSiniestroDTO reporteSiniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idToReporteSiniestro);
						poblarFormEstadoCuentaPorSiniestro(idToReporteSiniestro, nombreUsuario, null, estadoCuentaContratoFacultativoForm, null);
					}
					else if(tipoConsulta.equalsIgnoreCase("saldoPorPoliza") && validaParametro(idPoliza)){//Consulta de saldo incluyendo s�lo una p�liza
						BigDecimal idToPoliza = UtileriasWeb.regresaBigDecimal(idPoliza);
						EstadoCuentaDTO estadoCuentaDTO = new EstadoCuentaDTO();
						estadoCuentaDTO.setSubRamoDTO(new SubRamoDTO());
						estadoCuentaDTO.getSubRamoDTO().setDescripcionSubRamo("");
						EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
					 	PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
						polizaSoporteDanosDTO.setIdToPoliza(idToPoliza);
						List<PolizaSoporteDanosDTO> polizaSoporteDanosList = SoporteDanosDN.getInstancia().buscarPolizasFiltrado(polizaSoporteDanosDTO);
						estadoCuentaDecoradoDTO.setEndosoSoporteDanosDTO(SoporteDanosDN.getInstancia().getUltimoEndoso(idToPoliza));
						estadoCuentaDecoradoDTO.setPolizaSoporteDanosDTO(polizaSoporteDanosList.get(0));
				 		EstadosCuentaAction.poblarEstadoCuentaContratoFacultativoFormConEstadoCuentaDecoradoDTO(estadoCuentaDecoradoDTO, estadoCuentaContratoFacultativoForm);
				 		estadoCuentaContratoFacultativoForm.setDescripcionMoneda(polizaSoporteDanosList.get(0).getDescripcionMoneda());
				 		estadoCuentaContratoFacultativoForm.setIdTcMoneda(polizaSoporteDanosList.get(0).getIdMoneda().toBigInteger().toString());
				 		estadoCuentaContratoFacultativoForm.setIdPoliza(idPoliza);
				 		estadoCuentaContratoFacultativoForm.setNumeroPoliza(polizaSoporteDanosList.get(0).getNumeroPoliza());
					}
					if(validaParametro(idSubRamo)){
						SubRamoDTO subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(UtileriasWeb.regresaBigDecimal(idSubRamo));
						if(subRamoDTO != null){
							estadoCuentaContratoFacultativoForm.setDescripcionSubRamo(subRamoDTO.getDescripcionSubRamo());
						}
					}
					if(UtileriasWeb.esCadenaVacia(estadoCuentaContratoFacultativoForm.getIdTcMoneda())){
						estadoCuentaContratoFacultativoForm.setIdTcMoneda(idMoneda);
						if(UtileriasWeb.esCadenaVacia(estadoCuentaContratoFacultativoForm.getDescripcionMoneda())){
							int idm = Integer.valueOf(idMoneda).intValue();
							if(idm == Sistema.MONEDA_DOLARES)
								estadoCuentaContratoFacultativoForm.setDescripcionMoneda("D�lares");
							else
								estadoCuentaContratoFacultativoForm.setDescripcionMoneda("Pesos");
						}
					}
					if(!UtileriasWeb.esCadenaVacia(hasta)){
						estadoCuentaContratoFacultativoForm.setCorteAl(hasta);
					}else{
						estadoCuentaContratoFacultativoForm.setCorteAl("Todo");
					}
					if(UtileriasWeb.esCadenaVacia(estadoCuentaContratoFacultativoForm.getNumeroPoliza())){
						estadoCuentaContratoFacultativoForm.setNumeroPoliza("TODAS");
					}
					if(UtileriasWeb.esCadenaVacia(estadoCuentaContratoFacultativoForm.getNumeroReporteSiniestro())){
						estadoCuentaContratoFacultativoForm.setNumeroReporteSiniestro("TODAS");
					}
					estadoCuentaContratoFacultativoForm.setIdTcSubRamo(idSubRamo);
					estadoCuentaContratoFacultativoForm.setHasta(hasta);
					estadoCuentaContratoFacultativoForm.setTipoConsulta(tipoConsulta);
				}
			}
			
		}catch (Exception e) {
			LogDeMidasWeb.log("Error en EstadoCuentaFacultativoAction.mostrarDetalleEstadoCuentaFacultativoPorReasegurador(...)", Level.SEVERE, e);
			reglaNavegacion = Sistema.NO_EXITOSO;
 		}
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward obtenerReporteMovimientosEstadoCuenta(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
//			String idEstadoCuenta = request.getParameter("idEstadoCuenta");
			String mostrarEjerciciosAnteriores = request.getParameter("mostrarEjerciciosAnteriores");
			String idToPolizaStr = request.getParameter("idPoliza");
			String idPolizaSubRamo = request.getParameter("idPolizaSubRamo");
			
			String idReporteSiniestro = request.getParameter("idReporteSiniestro");
			String idSiniestroSubRamo = request.getParameter("idSiniestroSubRamo");
			
			String separarPorReaseguradorStr = request.getParameter("separarPorReasegurador");
			String hasta = request.getParameter("hasta");
			String tipoConsulta = request.getParameter("tipoConsulta");
			String idReasegurador = request.getParameter("idReasegurador");
			
			String idMoneda = request.getParameter("idMoneda");
			String idSubRamo = request.getParameter("idSubRamo");
			String idSiniestro = request.getParameter("idSiniestro");
			
			String tipoReporte = request.getParameter("tipoReporte");
			if(UtileriasWeb.esCadenaVacia(tipoReporte))
				tipoReporte = Sistema.TIPO_XLS;
			
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			
			ReporteMovimientosCuentaPorPagarEstadoCuentaFacultativo reporteMovtosCxP= new ReporteMovimientosCuentaPorPagarEstadoCuentaFacultativo();
			reporteMovtosCxP.setTipoReporte(tipoReporte);
			reporteMovtosCxP.establecerParametroConsulta("mostrarEjerciciosAnteriores", mostrarEjerciciosAnteriores);
			reporteMovtosCxP.establecerParametroConsulta("idPoliza", idToPolizaStr);
			reporteMovtosCxP.establecerParametroConsulta("idPolizaSubRamo", idPolizaSubRamo);
			reporteMovtosCxP.establecerParametroConsulta("idReporteSiniestro", idReporteSiniestro);
			reporteMovtosCxP.establecerParametroConsulta("idSiniestroSubRamo", idSiniestroSubRamo);
			reporteMovtosCxP.establecerParametroConsulta("separarPorReasegurador", separarPorReaseguradorStr);
			reporteMovtosCxP.establecerParametroConsulta("hasta", hasta);
			reporteMovtosCxP.establecerParametroConsulta("tipoConsulta", tipoConsulta);
			reporteMovtosCxP.establecerParametroConsulta("idReasegurador", idReasegurador);
			reporteMovtosCxP.establecerParametroConsulta("idMoneda",idMoneda);
			reporteMovtosCxP.establecerParametroConsulta("idSubRamo",idSubRamo);
			reporteMovtosCxP.establecerParametroConsulta("idSiniestro",idSiniestro);
			
			byte[] byteArray = null;
			try {
				byteArray = reporteMovtosCxP.obtenerReporte(nombreUsuario);
				this.writeBytes(response, byteArray, tipoReporte, reporteMovtosCxP.getTituloEstadoCuenta().replaceAll(" ", "_"));
			} catch (SystemException e) {
				LogDeMidasWeb.log("EstadoCuentaFacultativoAction.obtenerReporteMovimientosEstadoCuenta(...) Ocurri� un error al imprimir un reporte de movimientos de estado de cuenta facultativo.", Level.SEVERE, e);
			}
			
			return null;
		}catch(Exception e){
			LogDeMidasWeb.log("Error al imprimir reporte de estado de cuenta facultativo", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br>"+e.getCause()+"<br>"+e.getCause());
			return mapping.findForward("errorImpresion");
		}
	}
	
	private boolean validaParametro(String parametro){
		return (!UtileriasWeb.esCadenaVacia(parametro) && !parametro.equals("undefined") && !parametro.equals("null"));
	}
	private boolean poblarFormEstadoCuentaPorSiniestro(BigDecimal idtoReporteSiniestro,String nombreUsuario,
			String idSiniestroSubRamo,EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm,
			List<SubRamoDTO> listaSubRamosIncluidos) throws ExcepcionDeAccesoADatos, SystemException{
		ReporteSiniestroDTO reporteSiniestroDTO = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idtoReporteSiniestro);
		boolean resultado = false;
		if(reporteSiniestroDTO != null){
			PolizaSoporteDanosDTO polizaSoporteDanosDTO = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getFechaSiniestro());
			estadoCuentaContratoFacultativoForm.setCaratulaSiniestroForm(new CaratulaSiniestroForm());
			CaratulaSiniestroForm caratulaSiniestroForm = estadoCuentaContratoFacultativoForm.getCaratulaSiniestroForm();
			poblarDatosSiniestroPolizaForm(reporteSiniestroDTO, caratulaSiniestroForm,polizaSoporteDanosDTO);
			caratulaSiniestroForm.setFechaEmisionPoliza(removerHorasCampoFecha(caratulaSiniestroForm.getFechaEmisionPoliza()));
			caratulaSiniestroForm.setFechaTerminacionVigenciaPoliza(removerHorasCampoFecha(caratulaSiniestroForm.getFechaTerminacionVigenciaPoliza()));
			caratulaSiniestroForm.setFechaInicioVigenciaPoliza(removerHorasCampoFecha(caratulaSiniestroForm.getFechaInicioVigenciaPoliza()));
			caratulaSiniestroForm.setFechaHoraReporte(caratulaSiniestroForm.getFechaHoraReporte());
			resultado = true;
		
			String descripcionSubRamos = obtenerDescripcionSubRamosDTO(listaSubRamosIncluidos);
			estadoCuentaContratoFacultativoForm.setBusquedaNomAsegurado(descripcionSubRamos);
			estadoCuentaContratoFacultativoForm.setIdReporteSiniestro(idtoReporteSiniestro.toBigInteger().toString());
			estadoCuentaContratoFacultativoForm.setClaveSiniestroSubRamo(idSiniestroSubRamo);
			BigDecimal idMoneda = polizaSoporteDanosDTO.getIdMoneda();
			estadoCuentaContratoFacultativoForm.setIdTcMoneda((idMoneda != null)?idMoneda.toBigInteger().toString():null);
			estadoCuentaContratoFacultativoForm.setDescripcionMoneda(polizaSoporteDanosDTO.getDescripcionMoneda());
			estadoCuentaContratoFacultativoForm.setNumeroReporteSiniestro(reporteSiniestroDTO.getNumeroReporte());
		}
		return resultado;
	}
	
	public String obtenerDescripcionSubRamosDTO(
			List<SubRamoDTO> listaSubRamosIncluidos) {
		String descripcionSubRamos = "";
		StringBuilder descripcion =  new StringBuilder("");
		boolean removerUltimaComma = false;
		if(listaSubRamosIncluidos != null)
			for(SubRamoDTO subRamoDTO : listaSubRamosIncluidos){
				descripcion.append(subRamoDTO.getDescripcionSubRamo() ).append(", ");
				removerUltimaComma = true;
			}
		descripcionSubRamos = descripcion.toString();
		if(removerUltimaComma){
			descripcionSubRamos = descripcionSubRamos.substring(0,descripcionSubRamos.length()-2);
		}
		return descripcionSubRamos;
	}

	private String removerHorasCampoFecha(String fechaConHora){
		String fecha = "";
		if(!UtileriasWeb.esCadenaVacia(fechaConHora)){
			if(fechaConHora.length() > 10)
				fecha = fechaConHora.substring(0, 10);
		}
		return fecha;
	}
	
	private void poblarDatosSiniestroPolizaForm(ReporteSiniestroDTO reporteSiniestroDTO,CaratulaSiniestroForm caratulaSiniestroForm,PolizaSoporteDanosDTO polizaSoporteDanosDTO){
		caratulaSiniestroForm.setNumeroReporte(reporteSiniestroDTO.getNumeroReporte());
		caratulaSiniestroForm.setIdReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro().toBigInteger().toString());
		AjustadorDTO ajustadorDTO = reporteSiniestroDTO.getAjustador();
		if (ajustadorDTO!=null && ajustadorDTO.getIdTcAjustador()!=null){
			caratulaSiniestroForm.setIdAjustadorReporte(ajustadorDTO.getIdTcAjustador().toString());
			caratulaSiniestroForm.setNombreAjustadorReporte(ajustadorDTO.getNombre());
		}
		caratulaSiniestroForm.setFechaHoraReporte(UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaHoraReporte()));
		caratulaSiniestroForm.setFechaOcurrioReporte(UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaSiniestro()));
		caratulaSiniestroForm.setDescripcionSiniestro(reporteSiniestroDTO.getDescripcionSiniestro());
		caratulaSiniestroForm.setCalleSiniestro(reporteSiniestroDTO.getCalle());
		caratulaSiniestroForm.setCodigoPostalSiniestro(reporteSiniestroDTO.getCodigoPostal().toString());
		if (reporteSiniestroDTO.getColonia() !=null){
			ColoniaDTO coloniaDTO = CodigoPostalDN.getInstancia().getColoniaPorId(reporteSiniestroDTO.getColonia());
			if (coloniaDTO != null){
				caratulaSiniestroForm.setColoniaSiniestro(coloniaDTO.getColonyName());
			}
		}
		if (reporteSiniestroDTO.getIdCiudad()!=null){
			CiudadDTO ciudadDTO = CodigoPostalDN.getInstancia().getCiudadPorId(reporteSiniestroDTO.getIdCiudad());
			if (ciudadDTO!=null){
				caratulaSiniestroForm.setMunicipioSiniestro(ciudadDTO.getDescription());
			}
		}
		if (reporteSiniestroDTO.getIdEstado()!=null){
			EstadoDTO estadoDTO = CodigoPostalDN.getInstancia().getEstadoPorId(reporteSiniestroDTO.getIdEstado());
			if (estadoDTO!=null){
				caratulaSiniestroForm.setEstadoSiniestro(estadoDTO.getDescription());
			}
		}
		
		try{
			if (polizaSoporteDanosDTO!=null){
				//Se llenan los datos de la poliza para la caratula
				caratulaSiniestroForm.setTipoMonedaPoliza(polizaSoporteDanosDTO.getDescripcionMoneda());
				caratulaSiniestroForm.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
				caratulaSiniestroForm.setClaveOficinaPoliza(polizaSoporteDanosDTO.getNombreOficina());
				caratulaSiniestroForm.setEndosoPoliza(String.valueOf(polizaSoporteDanosDTO.getNumeroUltimoEndoso()));
				caratulaSiniestroForm.setClaveProductoPoliza(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
				caratulaSiniestroForm.setIncisoPoliza("");
				caratulaSiniestroForm.setClaveSubRamoPoliza(""); //falta agregar a la poliza
				caratulaSiniestroForm.setDescripcionSubRamoPoliza(polizaSoporteDanosDTO.getDescripcionTipoPoliza());
				caratulaSiniestroForm.setFechaEmisionPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaEmision()));
				caratulaSiniestroForm.setFechaInicioVigenciaPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaInicioVigencia()));
				caratulaSiniestroForm.setFechaTerminacionVigenciaPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaFinVigencia()));
				if (polizaSoporteDanosDTO.getIdAsegurado()!=null)
					caratulaSiniestroForm.setIdAseguradoPoliza(polizaSoporteDanosDTO.getIdAsegurado().toString());
				caratulaSiniestroForm.setNombreAseguradoPoliza(polizaSoporteDanosDTO.getNombreAsegurado());
				caratulaSiniestroForm.setCallePoliza(polizaSoporteDanosDTO.getCalleAsegurado());
				caratulaSiniestroForm.setCodigoPostalPoliza(polizaSoporteDanosDTO.getCpAsegurado());
				
				if (!UtileriasWeb.esCadenaVacia(polizaSoporteDanosDTO.getCpAsegurado())){
					ColoniaDTO coloniaDTO = CodigoPostalDN.getInstancia().getColoniaPorId(polizaSoporteDanosDTO.getCpAsegurado().trim() + "-" +polizaSoporteDanosDTO.getIdColoniaAsegurado().toString());
					if (coloniaDTO!=null&&coloniaDTO.getId()!=null)
						caratulaSiniestroForm.setColoniaPoliza(coloniaDTO.getDescription());
					CiudadDTO ciudadDTOPoliza = CodigoPostalDN.getInstancia().getCiudadPorId(coloniaDTO.getCityId());
					if (ciudadDTOPoliza!=null&&ciudadDTOPoliza.getId()!=null)
						caratulaSiniestroForm.setMunicipioPoliza(ciudadDTOPoliza.getDescription());
					
					EstadoDTO estadoDTOPoliza = CodigoPostalDN.getInstancia().getEstadoPorId(ciudadDTOPoliza.getStateId());
					if (estadoDTOPoliza!=null&&estadoDTOPoliza.getId()!=null)
						caratulaSiniestroForm.setEstadoPoliza(estadoDTOPoliza.getDescription());
				}
				
				if (polizaSoporteDanosDTO.getCodigoAgente()!=null)
					caratulaSiniestroForm.setIdAgentePoliza(polizaSoporteDanosDTO.getCodigoAgente().toString());
				
				caratulaSiniestroForm.setNombreAgentePoliza(polizaSoporteDanosDTO.getNombreAgente());
				caratulaSiniestroForm.setFormaPagoPoliza(polizaSoporteDanosDTO.getDescripcionFormaPago());
			}
		}
		catch(Exception e){}
	}
}
