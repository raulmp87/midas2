package mx.com.afirme.midas.reaseguro.estadoscuenta;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDN;
import mx.com.afirme.midas.catalogos.tiporeaseguro.TipoReaseguroDN;
import mx.com.afirme.midas.contratos.egreso.PagoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.AcumuladorDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaFiltroDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.PresentacionEstadoCuentaDN;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.SaldoAgrupacionDTO;
import mx.com.afirme.midas.contratos.linea.CalculoSuscripcionesDTO;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.contratos.linea.LineaDN;
import mx.com.afirme.midas.contratos.linea.SuscripcionDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.egresos.EgresoReaseguroDN;
import mx.com.afirme.midas.reaseguro.movimiento.MovimientoReaseguroDN;
import mx.com.afirme.midas.reaseguro.reportes.ReportesReaseguro;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo.ReporteEstadoCuentaFacultativo;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import net.sf.jasperreports.engine.JRException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.lowagie.text.DocumentException;

public class EstadosCuentaAction  extends MidasMappingDispatchAction{
	
	/***
	 * mostrarPrincipalEstadosCuenta
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
		public ActionForward mostrarPrincipalEstadosCuenta(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
			String reglaNavegacion = Sistema.EXITOSO;
			
// C�digo para pruebas de creaci�n de estados de cuenta facultativo
//			try {
//				EstadoCuentaSN estadoCuentaSN = new EstadoCuentaSN();
//				estadoCuentaSN.generarEdosCta();
//			} catch (SystemException e) {
//				e.printStackTrace();
//			}	
			
			return mapping.findForward(reglaNavegacion);
		}
		
		/**
		 * 
		 * @param mapping
		 * @param form
		 * @param request
		 * @param response
		 */
		public void deshacerMovimientosPorPoliza(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){
//			try {
//				MovimientoReaseguroDN.getInstancia().acumularMovimientosTerminacionSiniestros(new BigDecimal(187));
//			} catch (SystemException e) {
//				e.printStackTrace();
//			}
			//TODO Funci�n deshabilitada por el momento
			PolizaSoporteDanosDTO polizaSoporteDanosDTO;
			String idToPoliza = request.getParameter("idToPoliza");
			try {
				if (!UtileriasWeb.esCadenaVacia(idToPoliza)){
					polizaSoporteDanosDTO = SoporteDanosDN.getInstancia().getDetallePoliza(new BigDecimal(idToPoliza));
					MovimientoReaseguroDN.getInstancia().deshacerMovimientosPorPoliza(polizaSoporteDanosDTO);
				}else{
					throw new ExcepcionDeAccesoADatos("Debe indicar un idToPoliza");
				}
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
		public void regenerarAcumuladoresReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
			String mesPorProcesar = request.getParameter("mes");
			String anioPorProcesar = request.getParameter("anio");
			String codigoProceso = request.getParameter("codigoProceso");
			String idToMovimientos= request.getParameter("idMovimiento");
			String acumularCantidad = request.getParameter("acumularMovimiento");
			String acumularSiniestros = request.getParameter("acumularSiniestros");
			try { 
				Integer codigo = null;
				boolean procesarMovimientos = true;
				if(!UtileriasWeb.esCadenaVacia(codigoProceso)){
					try{
						codigo = Integer.valueOf(codigoProceso);
					}catch(Exception e){
						LogDeMidasWeb.log("EstadosDeCuentaAction.regenerarAcumuladoresReaseguro: se recibi� un codigo de proceso inv�lido: "+codigoProceso+
								", se omite el rec�lculo de acumuladores.", Level.INFO, null);
						procesarMovimientos = false;
					}
				}
				if(acumularSiniestros != null && acumularSiniestros.compareTo("true") == 0)
					MovimientoReaseguroDN.getInstancia().acumularMovimientosTerminacionSiniestros();
				if (procesarMovimientos)
					MovimientoReaseguroDN.getInstancia().regenerarAcumuladoresReaseguro(mesPorProcesar,anioPorProcesar,codigo,idToMovimientos,acumularCantidad);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
	
	
	/***
	 * listarEstadoDeCuentaTipoDeReaseguro
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
		public ActionForward listarEstadoCuentaTipoReaseguro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		//String ejercicioActual = "";
		Calendar calendar = Calendar.getInstance();
		EstadoCuentaTipoReaseguroForm estadoCuentaTipoReaseguroForm = (EstadoCuentaTipoReaseguroForm) form;
		try {
			List<MonedaDTO> monedaDTOList = MonedaDN.getInstancia().listarTodos();
			estadoCuentaTipoReaseguroForm.setMonedaDTOList(monedaDTOList);
			List<TipoReaseguroDTO> tipoReaseguroDTOList = TipoReaseguroDN.getINSTANCIA().listarTodos();
			for (TipoReaseguroDTO tipoReaseguroDTO : tipoReaseguroDTOList) {
				tipoReaseguroDTO.setDescripcion(tipoReaseguroDTO.getDescripcion().toUpperCase());
			}
			calendar.setTime(new Date());
			//ejercicioActual = String.valueOf(calendar.get(Calendar.YEAR));
			estadoCuentaTipoReaseguroForm.setIdEjercicio(String.valueOf(LineaDN.getInstancia().obtenerEjercicioActual()));
			estadoCuentaTipoReaseguroForm.setTipoReaseguroDTOList(tipoReaseguroDTOList);
			List<EjercicioDTO> ejercicioDTOList = LineaDN.getInstancia().obtenerEjercicios();
//			estadoCuentaTipoReaseguroForm.setMostrarEstadoCuentaConEjerciciosAnteriores("on");
			estadoCuentaTipoReaseguroForm.setEjercicioDTOList(ejercicioDTOList);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion =  Sistema.NO_DISPONIBLE;
		}

		return mapping.findForward(reglaNavegacion);
	}
		
	public void guardarObservaciones(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
			
		EstadoCuentaTipoReaseguroForm estadoCuentaTipoReaseguroForm = new EstadoCuentaTipoReaseguroForm();
		EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm = new EstadoCuentaContratoFacultativoForm();
		String idEstadoCuenta = "";
		String observaciones = "";
		String id = "10";
		String mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.guardar");
		if (form.getClass().isInstance(estadoCuentaTipoReaseguroForm)){
			estadoCuentaTipoReaseguroForm = (EstadoCuentaTipoReaseguroForm) form;
			idEstadoCuenta = estadoCuentaTipoReaseguroForm.getIdEstadoCuenta();
			observaciones = estadoCuentaTipoReaseguroForm.getObservaciones();
		}else if (form.getClass().isInstance(estadoCuentaContratoFacultativoForm)){
			estadoCuentaContratoFacultativoForm = (EstadoCuentaContratoFacultativoForm) form;
			idEstadoCuenta = estadoCuentaContratoFacultativoForm.getIdEstadoCuenta();
			observaciones = estadoCuentaContratoFacultativoForm.getObservaciones();
		}
			
		 
		if (idEstadoCuenta.length() > 0){
			try {
				EstadoCuentaDTO estadoCuentaDTO = EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(idEstadoCuenta));
				estadoCuentaDTO.setObservaciones(observaciones);
				EstadoCuentaDN.getINSTANCIA().modificar(estadoCuentaDTO);
				id = "30";
				mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.exito.guardar");
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (NumberFormatException e) {
				id="10";
				mensaje = "No disponible el registro de observaciones en estados de cuenta facultativos.";
			}
			finally{
				UtileriasWeb.imprimeMensajeXML(id, mensaje, response);
			}
		}
		
	}
		
	public ActionForward listarEstadoCuentaContratoFacultativo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		String tipo = request.getParameter("tipo");
		if(!UtileriasWeb.esCadenaVacia(tipo)){
			if(tipo.equalsIgnoreCase("siniestro")){
				reglaNavegacion = "exitosoSiniestro";
			}
			if(tipo.equalsIgnoreCase("poliza")){
				reglaNavegacion = "exitosoPoliza";
			}
			if(tipo.equalsIgnoreCase("reasegurador")){
				EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm = (EstadoCuentaContratoFacultativoForm) form;
				List<MonedaDTO> monedaDTOList;
				try {
					monedaDTOList = MonedaDN.getInstancia().listarTodos();
					estadoCuentaContratoFacultativoForm.setMonedaDTOList(monedaDTOList);
				} catch (ExcepcionDeAccesoADatos e) {
				} catch (SystemException e) {
				}
				reglaNavegacion = "exitosoReasegurador";
			}
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarDetalleEstadoCuentaTipoReaseguro(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		EstadoCuentaDTO estadoCuentaDTO = new EstadoCuentaDTO();
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		EstadoCuentaTipoReaseguroForm estadoCuentaTipoReaseguroForm = (EstadoCuentaTipoReaseguroForm) form;
		String idEstadoCuenta = request.getParameter("idEstadoCuenta");
//		String mostrarEjerciciosAnteriores = request.getParameter("mostrarEjerciciosAnteriores");
		boolean consultaPorIdEstadoCuenta = true;
		if (!UtileriasWeb.esCadenaVacia(idEstadoCuenta) && !idEstadoCuenta.equals("undefined")){
			try {
				try{
					estadoCuentaDTO = EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(idEstadoCuenta));
					estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
				}
				catch(Exception e){
					String[] ids = idEstadoCuenta.split("_");
					BigDecimal idTcSubRamo = UtileriasWeb.regresaBigDecimal(ids[0]);
					BigDecimal idTcReasegurador = UtileriasWeb.regresaBigDecimal(ids[1]);
//					Integer tipoReaseguro = new Integer(ids[2]);
					Integer ejercicio = new Integer(ids[3]);
					Integer suscripcion = new Integer(ids[4]);
					BigDecimal idMoneda = UtileriasWeb.regresaBigDecimal(ids[5]);
					consultaPorIdEstadoCuenta = false;
					
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaFacultativoPorSuscripcion(idTcReasegurador,idMoneda,idTcSubRamo,ejercicio,suscripcion);
				}
				this.poblarEstadoCuentaTipoReaseguroFormConEstadoCuentaDecoradoDTO(estadoCuentaDecoradoDTO, estadoCuentaTipoReaseguroForm,consultaPorIdEstadoCuenta,idEstadoCuenta);
				estadoCuentaTipoReaseguroForm.setMostrarEstadoCuentaConEjerciciosAnteriores("on");
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_EXITOSO;
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				reglaNavegacion = Sistema.NO_DISPONIBLE;
			}
		}
	
		return mapping.findForward(reglaNavegacion);
	}
	
	private void poblarEstadoCuentaTipoReaseguroFormConEstadoCuentaDecoradoDTO(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO, 
			EstadoCuentaTipoReaseguroForm estadoCuentaTipoReaseguroForm, boolean consultaPorIdEstadoCuenta,
			String idEstadoCuenta) throws ExcepcionDeAccesoADatos, SystemException{
		if(consultaPorIdEstadoCuenta){
			estadoCuentaTipoReaseguroForm.setIdEstadoCuenta(estadoCuentaDecoradoDTO.getIdEstadoCuenta().toBigInteger().toString());
			estadoCuentaTipoReaseguroForm.setNombreReasegurador(estadoCuentaDecoradoDTO.getNombreLargoReasegurador());
			String direccionReasegurador = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.datoNoDisponible");
			String RFC = "";
			direccionReasegurador = EstadoCuentaDN.getINSTANCIA().obtenerDireccionReasegurador(estadoCuentaDecoradoDTO);
			RFC=estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getRfc();
			estadoCuentaTipoReaseguroForm.setDireccionReasegurador(direccionReasegurador);
			estadoCuentaTipoReaseguroForm.setRfc(RFC);
			
			estadoCuentaTipoReaseguroForm.setTipoReaseguro(estadoCuentaDecoradoDTO.getDescripcionTipoReaseguro());
			estadoCuentaTipoReaseguroForm.setFechaInicial(UtileriasWeb.getFechaString(estadoCuentaDecoradoDTO.getFechaInicialPeriodo()));
			estadoCuentaTipoReaseguroForm.setFechaFinal(UtileriasWeb.getFechaString(estadoCuentaDecoradoDTO.getFechaFinalPeriodo()));
			estadoCuentaTipoReaseguroForm.setSuscripcion(estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
			estadoCuentaTipoReaseguroForm.setDescripcionSubRamo(estadoCuentaDecoradoDTO.getSubRamoDTO().getDescripcionSubRamo());
			String descripcionMoneda = EstadoCuentaDN.getINSTANCIA().obtenerDescripcionMoneda(estadoCuentaDecoradoDTO);
			estadoCuentaTipoReaseguroForm.setDescripcionMoneda(descripcionMoneda);
			
			estadoCuentaTipoReaseguroForm.setObservaciones(estadoCuentaDecoradoDTO.getObservaciones());
			estadoCuentaTipoReaseguroForm.setFormaPago(estadoCuentaDecoradoDTO.getDescripcionFormaPago());
			estadoCuentaTipoReaseguroForm.setAplicaRegistroObservaciones("true");
		}
		else{
			estadoCuentaTipoReaseguroForm.setIdEstadoCuenta(idEstadoCuenta);
			estadoCuentaTipoReaseguroForm.setNombreReasegurador(estadoCuentaDecoradoDTO.getNombreLargoReasegurador());
			String direccionReasegurador = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.datoNoDisponible");
			String RFC = "";
			direccionReasegurador = EstadoCuentaDN.getINSTANCIA().obtenerDireccionReasegurador(estadoCuentaDecoradoDTO);
			RFC=estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getRfc();
			estadoCuentaTipoReaseguroForm.setDireccionReasegurador(direccionReasegurador);
			estadoCuentaTipoReaseguroForm.setRfc(RFC);
			
			estadoCuentaTipoReaseguroForm.setTipoReaseguro(estadoCuentaDecoradoDTO.getDescripcionTipoReaseguro());
			estadoCuentaTipoReaseguroForm.setFechaInicial(UtileriasWeb.getFechaString(estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getFechaInicial()));
			estadoCuentaTipoReaseguroForm.setFechaFinal(UtileriasWeb.getFechaString(estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getFechaFinal()));
			estadoCuentaTipoReaseguroForm.setSuscripcion(estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getDescripcion());
			estadoCuentaTipoReaseguroForm.setDescripcionSubRamo(estadoCuentaDecoradoDTO.getSubRamoDTO().getDescripcionSubRamo());
			String descripcionMoneda = EstadoCuentaDN.getINSTANCIA().obtenerDescripcionMoneda(estadoCuentaDecoradoDTO);
			estadoCuentaTipoReaseguroForm.setDescripcionMoneda(descripcionMoneda);
			
			estadoCuentaTipoReaseguroForm.setObservaciones("");
			estadoCuentaTipoReaseguroForm.setFormaPago("");
			estadoCuentaTipoReaseguroForm.setAplicaRegistroObservaciones("false");
		}
	}
	
	public ActionForward mostrarDetalleEstadoCuentaContratoFacultativo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm = (EstadoCuentaContratoFacultativoForm) form;
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
		EstadoCuentaDTO estadoCuentaDTO;
		String idEstadoCuenta = request.getParameter("idEstadoCuenta");
		try {
			if (!UtileriasWeb.esObjetoNulo(idEstadoCuenta) && !UtileriasWeb.esCadenaVacia(idEstadoCuenta) && !idEstadoCuenta.equals("undefined")){
				SoporteDanosDN soporteDanosDN =  SoporteDanosDN.getInstancia();
				estadoCuentaDTO = EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(idEstadoCuenta));
				estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
			 	PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO(); 
				polizaSoporteDanosDTO.setIdToPoliza(estadoCuentaDTO.getIdPoliza());
				List<PolizaSoporteDanosDTO> polizaSoporteDanosList = soporteDanosDN.buscarPolizasFiltrado(polizaSoporteDanosDTO);
 				estadoCuentaDecoradoDTO.setEndosoSoporteDanosDTO(SoporteDanosDN.getInstancia().getEndosoPorId(estadoCuentaDTO.getIdPoliza(), BigDecimal.valueOf(estadoCuentaDTO.getNumeroEndoso())));
				estadoCuentaDecoradoDTO.setPolizaSoporteDanosDTO(polizaSoporteDanosList.get(0));
		 		poblarEstadoCuentaContratoFacultativoFormConEstadoCuentaDecoradoDTO(estadoCuentaDecoradoDTO, estadoCuentaContratoFacultativoForm);
			}
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
 		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public static void poblarEstadoCuentaContratoFacultativoFormConEstadoCuentaDecoradoDTO(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO, EstadoCuentaContratoFacultativoForm estadoCuentaContratoFacultativoForm) throws ExcepcionDeAccesoADatos, SystemException{
		
		if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getPolizaSoporteDanosDTO()) && !UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getPolizaSoporteDanosDTO().getNumeroPoliza()))
			estadoCuentaContratoFacultativoForm.setPoliza(estadoCuentaDecoradoDTO.getPolizaSoporteDanosDTO().getNumeroPoliza().toString());
		else
			estadoCuentaContratoFacultativoForm.setPoliza("");
		
		if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getNumeroEndoso()))
			estadoCuentaContratoFacultativoForm.setEndoso(Integer.valueOf(estadoCuentaDecoradoDTO.getEndosoSoporteDanosDTO().getNumeroEndoso()).toString());
		else
			estadoCuentaContratoFacultativoForm.setEndoso("");
		
		if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getPolizaSoporteDanosDTO()))
			estadoCuentaContratoFacultativoForm.setNomAsegurado(estadoCuentaDecoradoDTO.getPolizaSoporteDanosDTO().getNombreAsegurado());
		else
			estadoCuentaContratoFacultativoForm.setNomAsegurado("");
		
		if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getContratoFacultativoDTO()))
			estadoCuentaContratoFacultativoForm.setNotaCobertura(estadoCuentaDecoradoDTO.getContratoFacultativoDTO().getNotaCobertura());
		else
			estadoCuentaContratoFacultativoForm.setNotaCobertura("");
		
		if(estadoCuentaDecoradoDTO.getIdEstadoCuenta() != null){
			estadoCuentaContratoFacultativoForm.setIdEstadoCuenta(estadoCuentaDecoradoDTO.getIdEstadoCuenta().toBigInteger().toString());
		}else{
			estadoCuentaContratoFacultativoForm.setIdEstadoCuenta("");
		}
		
		if(estadoCuentaDecoradoDTO.getNombreReasegurador() != null){
			estadoCuentaContratoFacultativoForm.setNombreReasegurador(estadoCuentaDecoradoDTO.getNombreReasegurador());
		}else{
			estadoCuentaContratoFacultativoForm.setNombreReasegurador("");
		}
		
		String direccionReasegurador = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.datoNoDisponible");
		if (estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO() != null){
			String estado = "";
			String pais = "";
			String ciudad = "";
			if (!UtileriasWeb.esCadenaVacia(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getEstado())){
				CiudadDTO ciudadDTO = CodigoPostalDN.getInstancia().getCiudadPorId(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getCiudad());
				if(ciudadDTO != null)
					ciudad = ciudadDTO.getDescription();
			}
			if (!UtileriasWeb.esCadenaVacia(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getEstado())){
				EstadoDTO estadoDTO = CodigoPostalDN.getInstancia().getEstadoPorId(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getEstado());
				if(estadoDTO != null)
					estado = estadoDTO.getDescription();
			}
			if (!UtileriasWeb.esCadenaVacia(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getPais())){
				PaisDTO paisDTO = new PaisDTO();
				paisDTO.setCountryId(estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getPais());
				paisDTO = PaisTipoDestinoTransporteDN.getInstancia().getPaisPorId(paisDTO);
				if(paisDTO != null)
					pais = paisDTO.getDescription();
			}
			direccionReasegurador = estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getUbicacion() + ", " + (!ciudad.equals("") ? ciudad +
			", " : "") + (!estado.equals("") ? estado + ", " : "") + pais;
		}
		estadoCuentaContratoFacultativoForm.setDireccionReasegurador(direccionReasegurador);
		
		try{
			estadoCuentaContratoFacultativoForm.setDescripcionSubRamo(estadoCuentaDecoradoDTO.getSubRamoDTO().getDescripcionSubRamo());
			estadoCuentaContratoFacultativoForm.setFechaInicial(UtileriasWeb.getFechaString(estadoCuentaDecoradoDTO.getFechaInicialPeriodo()));
			estadoCuentaContratoFacultativoForm.setFechaFinal(UtileriasWeb.getFechaString(estadoCuentaDecoradoDTO.getFechaFinalPeriodo()));
			estadoCuentaContratoFacultativoForm.setSuscripcion(estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
			
			String descripcionMoneda = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.error.datoNoDisponible");
			if (estadoCuentaDecoradoDTO.getIdMoneda() != 0){
				Integer idMoneda = estadoCuentaDecoradoDTO.getIdMoneda();
				MonedaDTO monedaDTO = MonedaDN.getInstancia().getPorId(idMoneda.shortValue());
				if (monedaDTO != null)
					descripcionMoneda = monedaDTO.getDescripcion();
			}
			estadoCuentaContratoFacultativoForm.setDescripcionMoneda(descripcionMoneda);
			
			estadoCuentaContratoFacultativoForm.setObservaciones(estadoCuentaDecoradoDTO.getObservaciones());
			estadoCuentaContratoFacultativoForm.setFormaPago(estadoCuentaDecoradoDTO.getDescripcionFormaPago().toString());
		}catch(Exception e){
		}
	}
	
	public void mostrarPolizas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
	    SoporteDanosDN soporteDanosDN =  SoporteDanosDN.getInstancia();
	    String json = "{rows:[]}";
 		try{
			String nombreAsegurado = request.getParameter("nomAsegurado");
			String numPoliza = request.getParameter("numPoliza");
			String idTcReasegurador = request.getParameter("idTcReasegurador");
			String idMoneda = request.getParameter("idMoneda");
			
			PolizaSoporteDanosDTO polizaSoporteDanosDTO = null;
			List<PolizaSoporteDanosDTO> polizaSoporteDanosDTOListFiltradoFac = null;
			List<PolizaSoporteDanosDTO> polizaSoporteDanosList = null;
			
			boolean consultarPorReasegurador = false;
			
			if(!nombreAsegurado.equalsIgnoreCase("undefined") && !numPoliza.equalsIgnoreCase("undefined")){			
				if(nombreAsegurado.length() > 0 || numPoliza.length() > 0){	
					polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
					if (!nombreAsegurado.equalsIgnoreCase("undefined") && !nombreAsegurado.equalsIgnoreCase("null") 
				 			&& !numPoliza.equalsIgnoreCase("undefined") && !numPoliza.equalsIgnoreCase("null")){
						
						if (numPoliza == null || numPoliza.length() == 0 || numPoliza.equals("undefined"))
							polizaSoporteDanosDTO.setNombreAsegurado(nombreAsegurado);
						else
							polizaSoporteDanosDTO.setNumeroPoliza(numPoliza);
						
						polizaSoporteDanosList = soporteDanosDN.buscarPolizasFiltrado(polizaSoporteDanosDTO);
						polizaSoporteDanosDTOListFiltradoFac = new ArrayList<PolizaSoporteDanosDTO>();
						if (polizaSoporteDanosList != null && polizaSoporteDanosList.size() > 0){
							polizaSoporteDanosDTOListFiltradoFac = EstadoCuentaDN.getINSTANCIA().listarPolizasFacultativas(polizaSoporteDanosList);
						}
					}else{
						consultarPorReasegurador = true;
					}
				}
				else	consultarPorReasegurador = true;
			}
			else consultarPorReasegurador = true;
			
			if(consultarPorReasegurador && 
					!UtileriasWeb.esCadenaVacia(idTcReasegurador) && !UtileriasWeb.esCadenaVacia(idMoneda)){
				polizaSoporteDanosDTOListFiltradoFac = EstadoCuentaDN.getINSTANCIA().
					listarPolizasFacultativasPorReaseguradorMoneda(UtileriasWeb.regresaBigDecimal(idTcReasegurador), UtileriasWeb.regresaBigDecimal(idMoneda));
			}
			
			
			json = this.getJsonPolizas(polizaSoporteDanosDTOListFiltradoFac);
			
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ParseException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} finally{
			try{
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			}catch(IOException e){
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} 
		}
	}
	
	public void mostrarDetallePoliza(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
			//SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
			String idToPoliza = request.getParameter("numeroPoliza");
			PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			String json = "";
			List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList = new ArrayList<EstadoCuentaDecoradoDTO>();
			if (!UtileriasWeb.esCadenaVacia(idToPoliza) && !idToPoliza.equals("undefined") && !idToPoliza.equals("null")){
				polizaSoporteDanosDTO = SoporteDanosDN.getInstancia().getDetallePoliza(new BigDecimal(idToPoliza));
				estadoCuentaDecoradoDTOList = EstadoCuentaDN.getINSTANCIA().buscarEstadosCuentaFacultativos(polizaSoporteDanosDTO);
			}
			json = this.getJsonDetallePoliza(estadoCuentaDecoradoDTOList, polizaSoporteDanosDTO);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	public void mostrarDetallePagos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
		String idEstadoCuenta = request.getParameter("idEstadoCuenta");
		BigDecimal idEstadoCuentaBigDecimal = new BigDecimal(idEstadoCuenta);
		List<AcumuladorDTO> acumuladorDTOList = EstadoCuentaDN.getINSTANCIA().obtenerAcumuladoresPorIdEstadoCta(idEstadoCuentaBigDecimal);
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaDetalle(idEstadoCuentaBigDecimal,null,acumuladorDTOList);
	 	String json = this.getJsonDetallePagos(estadoCuentaDecoradoDTO);	
		if (json.length() == 0)
   			json="{rows:[{}]}";
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		pw.write(json);
		pw.flush();
		pw.close();
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
 			e.printStackTrace();
		} catch (SystemException e) {
	 		e.printStackTrace();
		} catch (ParseException e) {
	 		e.printStackTrace();
		}
	}
	
	public void mostrarDetalleAcumulador(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ParseException{
		try{
			String idEstadoCuenta = request.getParameter("idEstadoCuenta");
			String mostrarEjerciciosAnteriores = request.getParameter("mostrarEjerciciosAnteriores");
			String idToPolizaStr = request.getParameter("idPoliza");
			String idPolizaSubRamo = request.getParameter("idPolizaSubRamo");
			
			String idReporteSiniestro = request.getParameter("idReporteSiniestro");
			String idSiniestroSubRamo = request.getParameter("idSiniestroSubRamo");
			
			String separarPorReaseguradorStr = request.getParameter("separarPorReasegurador");
			String hasta = request.getParameter("hasta");
			String tipoConsulta = request.getParameter("tipoConsulta");
			String idReasegurador = request.getParameter("idReasegurador");
			
			String idMoneda = request.getParameter("idMoneda");
			String idSubRamo = request.getParameter("idSubRamo");
			String idSiniestro = request.getParameter("idSiniestro");
			
			String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
			
			ReporteEstadoCuentaFacultativo reporteEstadoCuentaFacultativo = new ReporteEstadoCuentaFacultativo();
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idEstadoCuenta", idEstadoCuenta);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("mostrarEjerciciosAnteriores", mostrarEjerciciosAnteriores);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idPoliza", idToPolizaStr);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idPolizaSubRamo", idPolizaSubRamo);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idReporteSiniestro", idReporteSiniestro);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idSiniestroSubRamo", idSiniestroSubRamo);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("separarPorReasegurador", separarPorReaseguradorStr);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("hasta", hasta);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("tipoConsulta", tipoConsulta);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idReasegurador", idReasegurador);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idMoneda",idMoneda);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idSubRamo",idSubRamo);
			reporteEstadoCuentaFacultativo.establecerParametroConsulta("idSiniestro",idSiniestro);
			
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = reporteEstadoCuentaFacultativo.obtenerEstadoCuenta(nombreUsuario);
			
			String json = estadoCuentaDecoradoDTO != null ? this.getJsonDetalleAcumulador(estadoCuentaDecoradoDTO) : "{rows:[{}]}";
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} /*catch (ParseException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}*/
	}
	
	public String getJsonDetallePagos(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO){
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		try {
			List<PagoDTO> pagosList = EgresoReaseguroDN.getINSTANCIA().obtenerPagosContrato(estadoCuentaDecoradoDTO,null);
			int id = 1;
			for (PagoDTO pagoDTO : pagosList) {
			 	String numeroPago = pagoDTO.getNumeroPago();
				String fechaPago = UtileriasWeb.getFechaString(pagoDTO.getFechaPago());
				String monto =  pagoDTO.getMonto().toString();
				String estatus = pagoDTO.getEstatus();
  				MidasJsonRow midasJsonRow = new MidasJsonRow();
				midasJsonRow.setId(""+id);id++;
				midasJsonRow.setDatos(numeroPago, monto, fechaPago, estatus);
				midasJsonBase.addRow(midasJsonRow);
	 		}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return midasJsonBase.toString(); 	
	
	}
	
	public String getJsonDetalleAcumulador(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO){
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		List<SaldoConceptoDTO> saldoConceptoDTOList = estadoCuentaDecoradoDTO.getSaldosAcumulados();
		MidasJsonRow midasJsonRow;
		int idRow = -1;
		if (estadoCuentaDecoradoDTO.getSaldoTecnico() != null && saldoConceptoDTOList != null)
			saldoConceptoDTOList.add(estadoCuentaDecoradoDTO.getSaldoTecnico());
		
		if (saldoConceptoDTOList != null){
			StringBuilder concepto = new StringBuilder();
			StringBuilder saldo = new StringBuilder();
			for (SaldoConceptoDTO saldoConceptoDTO : saldoConceptoDTOList) {
				idRow++;
				midasJsonRow = new MidasJsonRow();
				midasJsonRow.setId(String.valueOf(idRow));
				String fechaTrimestre = "";
				String debe;
				String haber;
				concepto.delete(0,concepto.length());
				saldo.delete(0,saldo.length());
				concepto.append(saldoConceptoDTO.getDescripcionConcepto());
				if(saldoConceptoDTO instanceof SaldoAgrupacionDTO){
					debe = currencyFormatter.format(((SaldoAgrupacionDTO)saldoConceptoDTO).getTotalDebe());
					haber = currencyFormatter.format(((SaldoAgrupacionDTO)saldoConceptoDTO).getTotalHaber());
					saldo.append(currencyFormatter.format(((SaldoAgrupacionDTO)saldoConceptoDTO).getTotalSaldo()));
				}
				else{
					if (concepto.toString().startsWith("Saldo Anterior")/*idRow==0*/){
						fechaTrimestre = saldoConceptoDTO.getFechaTrimestre() != null ? saldoConceptoDTO.getFechaTrimestre() : ""; // estadoCuentaDecoradoDTO.getDescripcionSuscripcion(); //TODO falta definir de donde saldr� este dato
					}else
						fechaTrimestre = "";
					
//					if (concepto.startsWith("Comisiones")){
//						debe = saldoConceptoDTO.getDebe() != null ? currencyFormatter.format(saldoConceptoDTO.getDebe()) : "";
//						haber = "";
//					}else {
						if (saldoConceptoDTO.getDebe() == null)
							debe = "";
						else{
							if (concepto.toString().startsWith("Saldo Anterior") && saldoConceptoDTO.getDebe().compareTo(new BigDecimal("0")) == 0)
								debe = "";
							else{
								debe = currencyFormatter.format(saldoConceptoDTO.getDebe());
							}
						}
						if (saldoConceptoDTO.getHaber() == null)
							haber = "";
						else{
							if (concepto.toString().startsWith("Saldo Anterior") && saldoConceptoDTO.getHaber().compareTo(new BigDecimal("0")) == 0 && !(saldoConceptoDTO.getDebe().compareTo(new BigDecimal("0"))==0)){
								haber = "";
							}
							haber =  currencyFormatter.format(saldoConceptoDTO.getHaber());
						}
//					}
					
					if(saldoConceptoDTO.isMostrarSaldoAcumulado())
						saldo.append(currencyFormatter.format(saldoConceptoDTO.getSaldoAcumulado().doubleValue()));
					else
						saldo.append(currencyFormatter.format(saldoConceptoDTO.getSaldo().doubleValue()));
					if ((idRow+1) == saldoConceptoDTOList.size() || concepto.toString().startsWith("Saldo Anterior") || saldoConceptoDTO.isResaltarConcepto()){
						concepto.insert(0, "<b>").append("</b>");
						saldo.insert(0,"<b>").append("</b>");
					}
					if(saldoConceptoDTO.isOcultarSaldos()){
						debe =  haber = "";
						saldo.delete(0,saldo.length());
						
					}
				}
				midasJsonRow.setDatos(fechaTrimestre, concepto.toString(), debe, haber, saldo.toString());
				midasJsonBase.addRow(midasJsonRow);
			}
		}
		return midasJsonBase.toString();
	}
	
	public String getJsonPolizas(List<PolizaSoporteDanosDTO> polizaSoporteDanosList){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		MidasJsonRow midasJsonRow;
		String nomAsegurador;
		String numeroPoliza;
		String fechaEmision;
		String vigencia;
		boolean objetoSeleccionado = true;
		String seleccionado = "1";
		if (polizaSoporteDanosList != null){
			for (PolizaSoporteDanosDTO polizaSoporteDanosDTO : polizaSoporteDanosList) {
				midasJsonRow = new MidasJsonRow();
				if(polizaSoporteDanosDTO != null && polizaSoporteDanosDTO.getIdToPoliza() != null){
					midasJsonRow.setId(polizaSoporteDanosDTO.getIdToPoliza().toString());
					if(polizaSoporteDanosDTO.getNombreAsegurado() == null){
						nomAsegurador = "";	
					}else{
						nomAsegurador = polizaSoporteDanosDTO.getNombreAsegurado(); 
					}
				 	numeroPoliza = polizaSoporteDanosDTO.getNumeroPoliza().toString();
					fechaEmision = simpleDateFormat.format(polizaSoporteDanosDTO.getFechaEmision());
					vigencia = "Del "+ simpleDateFormat.format(polizaSoporteDanosDTO.getFechaFinVigencia()) +
								" al "+simpleDateFormat.format(polizaSoporteDanosDTO.getFechaInicioVigencia());
					midasJsonRow.setDatos(seleccionado, numeroPoliza,nomAsegurador, fechaEmision, vigencia);
					midasJsonBase.addRow(midasJsonRow);
					if(objetoSeleccionado){
						seleccionado = "0";
						objetoSeleccionado = false;
					}
				}
			}
		}
		return midasJsonBase.toString();
	}
	
	public String getJsonDetallePoliza(List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList, PolizaSoporteDanosDTO polizaSoporteDanosDTO) throws ExcepcionDeAccesoADatos, SystemException{
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		MidasJsonRow midasJsonRow;
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		currency.setMaximumFractionDigits(4);
		//MonedaDTO monedaDTO;
		String poliza;
		String endoso;
		String notaDeCobertura;
		String subRamo;
		String tipoReaseguro;
		String suscripcion;
		String numSuscripcion;
		String formaDePago;
		String reasegurador;
		String corredor;
		String moneda;
		String saldoTecnico;
		CalculoSuscripcionesDTO calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		SuscripcionDTO suscripcionDTO;
		
		if (estadoCuentaDecoradoDTOList != null){
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : estadoCuentaDecoradoDTOList) {
				midasJsonRow = new MidasJsonRow();
				midasJsonRow.setId(estadoCuentaDecoradoDTO.getIdEstadoCuenta().toBigInteger().toString());
				if (!UtileriasWeb.esObjetoNulo(polizaSoporteDanosDTO))
					poliza = polizaSoporteDanosDTO.getNumeroPoliza(); 
				else
					poliza = "";
				if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getNumeroEndoso()))
					endoso = Integer.valueOf(estadoCuentaDecoradoDTO.getEndosoSoporteDanosDTO().getNumeroEndoso()).toString();
				else
					endoso = "";
				
				if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getContratoFacultativoDTO()))
					if (estadoCuentaDecoradoDTO.getContratoFacultativoDTO().getNotaCobertura() != null){
						notaDeCobertura = estadoCuentaDecoradoDTO.getContratoFacultativoDTO().getNotaCobertura();
					}else{
						notaDeCobertura = "&nbsp;";
					}	
				else
					notaDeCobertura = "";
				
				if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getSubRamoDTO()))
					subRamo = estadoCuentaDecoradoDTO.getSubRamoDTO().getDescripcionSubRamo();
				else
					subRamo = "";
				
				tipoReaseguro = estadoCuentaDecoradoDTO.getDescripcionTipoReaseguro();
				suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(estadoCuentaDecoradoDTO, null);
				suscripcion = suscripcionDTO.getDescripcion();
				numSuscripcion = String.valueOf(suscripcionDTO.getIdSuscripcion());
				formaDePago =  estadoCuentaDecoradoDTO.getDescripcionFormaPago();
				reasegurador = estadoCuentaDecoradoDTO.getNombreReasegurador();
				corredor = estadoCuentaDecoradoDTO.getNombreCorredor();
				if (!UtileriasWeb.esObjetoNulo(estadoCuentaDecoradoDTO.getIdMoneda())){
					moneda = estadoCuentaDecoradoDTO.getDescripcionMoneda();
				}else
					moneda = "";
				
				try {
					List<AcumuladorDTO> acumuladorDTOList = EstadoCuentaDN.getINSTANCIA().obtenerAcumuladoresPorIdEstadoCta(estadoCuentaDecoradoDTO.getIdEstadoCuenta());
					estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaDetalle(estadoCuentaDecoradoDTO.getIdEstadoCuenta(),null,acumuladorDTOList);
					if (estadoCuentaDecoradoDTO != null && estadoCuentaDecoradoDTO.getSaldoTecnico() != null){
						saldoTecnico = currency.format(estadoCuentaDecoradoDTO.getSaldoTecnico().getSaldoAcumulado().doubleValue());
					}else
						saldoTecnico = "";
				} catch (ParseException e) {
					e.printStackTrace();
					saldoTecnico = "";
				}
				
				midasJsonRow.setDatos("0", poliza, endoso, notaDeCobertura, subRamo, tipoReaseguro, suscripcion, numSuscripcion, formaDePago, reasegurador, corredor, moneda, saldoTecnico);
				midasJsonBase.addRow(midasJsonRow);
			}
		}
		return midasJsonBase.toString();
	}
	
	public void llenaComboSuscripcion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws SystemException {
		
		String idEjercicioStr = request.getParameter("idEjercicio");
		String idTcSubRamoStr = request.getParameter("idTcSubRamo");
		String tipoReaseguroStr = request.getParameter("tipoReaseguro");
		
		if (!UtileriasWeb.esObjetoNulo(idEjercicioStr) && !UtileriasWeb.esCadenaVacia(idEjercicioStr) 
				&& !UtileriasWeb.esObjetoNulo(idTcSubRamoStr) && !UtileriasWeb.esCadenaVacia(idTcSubRamoStr)
				&& !UtileriasWeb.esObjetoNulo(tipoReaseguroStr) && !UtileriasWeb.esCadenaVacia(tipoReaseguroStr)){
		}
		Integer idEjercicio = new Integer(idEjercicioStr);
		BigDecimal idTcSubRamo = new BigDecimal(idTcSubRamoStr);
		int tipoReaseguro = new Integer(tipoReaseguroStr).intValue();
		List<SuscripcionDTO> suscripcionDTOList = LineaDN.getInstancia().obtenerSuscripcionesPorLinea(idTcSubRamo, idEjercicio, tipoReaseguro);
		UtileriasWeb.imprimeMensajeXML(suscripcionDTOList, "idSuscripcion", "descripcion", response);
	}
	
	public void obtenerEstadosCuenta(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		
		EstadoCuentaTipoReaseguroForm estadoCuentaTipoReaseguroForm = (EstadoCuentaTipoReaseguroForm) form;
		EstadoCuentaFiltroDTO estadoCuentaFiltroDTO = new EstadoCuentaFiltroDTO();
		List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList = new ArrayList<EstadoCuentaDecoradoDTO>();
		HttpSession session = request.getSession();
		session.removeAttribute("estadoCuentaDecoradoDTOList");
		if (!UtileriasWeb.esObjetoNulo(estadoCuentaTipoReaseguroForm.getIdEjercicio()) && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdEjercicio())){
			estadoCuentaFiltroDTO = this.poblarEstadoCuentaFiltroDTO(estadoCuentaTipoReaseguroForm);
			try {
				estadoCuentaDecoradoDTOList = EstadoCuentaDN.getINSTANCIA().obtenerEstadosCuenta(estadoCuentaFiltroDTO,true);
				session.setAttribute("estadoCuentaDecoradoDTOList", estadoCuentaDecoradoDTOList);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	/***
	 * mostrarEstadosCuentaTipoReaseguroGrid
	 * @param request
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	@SuppressWarnings("unchecked")
	public void mostrarEstadosCuentaTipoReaseguroGrid(ActionMapping mapping, ActionForm form,
				HttpServletRequest request, HttpServletResponse response){

		HttpSession session = request.getSession();
		try{
			List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList = (List<EstadoCuentaDecoradoDTO>) session.getAttribute("estadoCuentaDecoradoDTOList");
			String json = this.getJsonEstadoCuentaTipoReaseguroGrid(estadoCuentaDecoradoDTOList);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		}catch (SystemException e) {
			e.printStackTrace();
		}finally{
			session.removeAttribute("estadoCuentaDecoradoDTOList");
		}
	}

	public String getJsonEstadoCuentaTipoReaseguroGrid(List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList) throws ExcepcionDeAccesoADatos, SystemException{
		MidasJsonBase midasJsonBase = new MidasJsonBase();
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		currency.setMaximumFractionDigits(4);
		String saldoTecnico;
		String id;
		String subRamo;
		String tipoReaseguro;
		String suscripcion;
		String numeroSuscripcion;
		String reasegurador;
		String corredor;
		Integer idMoneda;
		MonedaDTO monedaDTO;
		String moneda = "";
		SuscripcionDTO suscripcionDTO;
		CalculoSuscripcionesDTO calculoSuscripcionesDTO = new CalculoSuscripcionesDTO();
		Map<Integer,MonedaDTO> mapaMonedas = new HashMap<Integer, MonedaDTO>();
//		Map<BigDecimal,List<ConceptoAcumuladorDTO>> mapaConceptosPorMoneda = new HashMap<BigDecimal, List<ConceptoAcumuladorDTO>>();
		if (estadoCuentaDecoradoDTOList != null){
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : estadoCuentaDecoradoDTOList) {
				subRamo = estadoCuentaDecoradoDTO.getSubRamoDTO().getDescripcionSubRamo();
				tipoReaseguro = estadoCuentaDecoradoDTO.getDescripcionTipoReaseguro();
				try{
					suscripcionDTO = calculoSuscripcionesDTO.getSuscripcion(estadoCuentaDecoradoDTO, null);
				}catch(Exception e){
					suscripcionDTO = estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada();
				}
				if(suscripcionDTO != null){
					suscripcion = suscripcionDTO.getDescripcion();
					numeroSuscripcion = String.valueOf(suscripcionDTO.getIdSuscripcion());
				}
				else{
					suscripcion = "No disponible";
					numeroSuscripcion = "No disponible";
				}
				reasegurador = estadoCuentaDecoradoDTO.getNombreReasegurador();
				corredor = estadoCuentaDecoradoDTO.getNombreCorredor();
				
				idMoneda = new Integer(estadoCuentaDecoradoDTO.getIdMoneda());
				if(!mapaMonedas.containsKey(idMoneda)){
					try {
						mapaMonedas.put(idMoneda, MonedaDN.getInstancia().getPorId(idMoneda.shortValue()));
					} catch (ExcepcionDeAccesoADatos e) {
						LogDeMidasWeb.log("EstadosCuentaAction.getJsonEstadoCuentaTipoReaseguroGrid. Ocurri� un error al consultar la moneda con ID: "+idMoneda, Level.SEVERE, e);
					} catch (SystemException e) {
						LogDeMidasWeb.log("EstadosCuentaAction.getJsonEstadoCuentaTipoReaseguroGrid. Ocurri� un error al consultar la moneda con ID: "+idMoneda, Level.SEVERE, e);
					}
				}
				
				monedaDTO = mapaMonedas.get(idMoneda);
				moneda = monedaDTO != null ? monedaDTO.getDescripcion() : "No disponible";				
				
//				BigDecimal idMonedaBigDecimal = new BigDecimal(idMoneda);
//				List<ConceptoAcumuladorDTO> listaConceptosPorMoneda = mapaConceptosPorMoneda.get(idMonedaBigDecimal);
				
				saldoTecnico = "";
//				try {
//					if (listaConceptosPorMoneda == null){
//						listaConceptosPorMoneda = EstadoCuentaDN.getINSTANCIA().obtenerConceptosMovimientos(idMonedaBigDecimal);
//						mapaConceptosPorMoneda.put(idMonedaBigDecimal, listaConceptosPorMoneda);
//					}
//					List<AcumuladorDTO> acumuladorDTOList = EstadoCuentaDN.getINSTANCIA().obtenerAcumuladoresPorIdEstadoCta(estadoCuentaDecoradoDTO.getIdEstadoCuenta());
//					estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaDetalle(estadoCuentaDecoradoDTO.getIdEstadoCuenta(),listaConceptosPorMoneda,acumuladorDTOList);
				if(estadoCuentaDecoradoDTO.getTipoReaseguro() != TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					id = estadoCuentaDecoradoDTO.getIdEstadoCuenta().toBigInteger().toString();
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaHistoricoCombinado(estadoCuentaDecoradoDTO.getIdEstadoCuenta());
					if (estadoCuentaDecoradoDTO != null && estadoCuentaDecoradoDTO.getSaldoTecnico() != null)
						saldoTecnico = currency.format(estadoCuentaDecoradoDTO.getSaldoTecnico().getSaldoAcumulado().doubleValue());
				}
				else{
					id = estadoCuentaDecoradoDTO.getSubRamoDTO().getIdTcSubRamo()+"_"+
						estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor()+"_"+
						estadoCuentaDecoradoDTO.getTipoReaseguro()+"_"+
						estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getEjercicio()+"_"+
						estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getIdSuscripcion()+"_"+
						estadoCuentaDecoradoDTO.getIdMoneda();
					saldoTecnico = currency.format(estadoCuentaDecoradoDTO.getSaldoTecnico().getSaldoAcumulado().doubleValue());
				}
					
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
				
				MidasJsonRow midasJsonRow = new MidasJsonRow();
				midasJsonRow.setId(id);
				midasJsonRow.setDatos("0",subRamo, tipoReaseguro, suscripcion, numeroSuscripcion, reasegurador, corredor, moneda, saldoTecnico);
				midasJsonBase.addRow(midasJsonRow);
			}
		}
		
		return midasJsonBase.toString();
	}
	
	private EstadoCuentaFiltroDTO poblarEstadoCuentaFiltroDTO(EstadoCuentaTipoReaseguroForm estadoCuentaTipoReaseguroForm){
		EstadoCuentaFiltroDTO estadoCuentaFiltroDTO = new EstadoCuentaFiltroDTO();
		
		if (estadoCuentaTipoReaseguroForm != null){
			if (estadoCuentaTipoReaseguroForm.getIdEjercicio() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdEjercicio()))
				estadoCuentaFiltroDTO.setEjercicio(new Integer(estadoCuentaTipoReaseguroForm.getIdEjercicio()).intValue());
			
			if (estadoCuentaTipoReaseguroForm.getIdTcMoneda() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdTcMoneda()))
				estadoCuentaFiltroDTO.setIdMoneda(new Integer(estadoCuentaTipoReaseguroForm.getIdTcMoneda()).intValue());
			
			if (estadoCuentaTipoReaseguroForm.getIdTcRamo() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdTcRamo()))
				estadoCuentaFiltroDTO.setIdTcRamo(new BigDecimal(estadoCuentaTipoReaseguroForm.getIdTcRamo()));
			
			if (estadoCuentaTipoReaseguroForm.getIdTcSubRamo() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdTcSubRamo()))
				estadoCuentaFiltroDTO.setIdTcSubRamo(new BigDecimal(estadoCuentaTipoReaseguroForm.getIdTcSubRamo()));
			
			if (estadoCuentaTipoReaseguroForm.getIdSuscripcion() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdSuscripcion()))
				estadoCuentaFiltroDTO.setSuscripcion(new Integer(estadoCuentaTipoReaseguroForm.getIdSuscripcion()).intValue());
			
			if (estadoCuentaTipoReaseguroForm.getTipoReaseguro() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getTipoReaseguro()))
				estadoCuentaFiltroDTO.setTipoReaseguro(new Integer(estadoCuentaTipoReaseguroForm.getTipoReaseguro()).intValue());
				
			if (estadoCuentaTipoReaseguroForm.getIdtcReaseguradorCorredor() != null && !UtileriasWeb.esCadenaVacia(estadoCuentaTipoReaseguroForm.getIdtcReaseguradorCorredor()))
				estadoCuentaFiltroDTO.setIdtcreaseguradorcorredor(new BigDecimal(estadoCuentaTipoReaseguroForm.getIdtcReaseguradorCorredor()));
			
		}
		
		return estadoCuentaFiltroDTO;
	}
	
	public ActionForward rptEstadoCuenta(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		String idEstadoCuenta = request.getParameter("idEstadoCuenta");
//		String mostrarEjerciciosAnteriores = request.getParameter("mostrarEjerciciosAnteriores");
		String tipoReporte = request.getParameter("tipoReporte");
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		byte[] byteArray = null;
		try {
//			boolean ejerciciosAnteriores = false;
			if(tipoReporte == null)
				tipoReporte = Sistema.TIPO_PDF;
//			if(mostrarEjerciciosAnteriores != null && mostrarEjerciciosAnteriores.compareTo("true") == 0)
//				ejerciciosAnteriores = true;
			if(idEstadoCuenta != null){
				ReporteEstadoCuentaFacultativo reporteEstadoCuenta = new ReporteEstadoCuentaFacultativo(tipoReporte);
				reporteEstadoCuenta.establecerParametroConsulta("idEstadoCuenta", idEstadoCuenta);
				reporteEstadoCuenta.setImprimirFormatoAutomaticos(true);
//				ReporteEstadoCuenta reporteEstadoCuenta = new ReporteEstadoCuenta(ejerciciosAnteriores, tipoReporte, new BigDecimal(idEstadoCuenta));
//				byteArray = reporteEstadoCuenta.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request), false);
				byteArray = reporteEstadoCuenta.obtenerReporte(nombreUsuario);
			}
			if(byteArray != null){
				this.writeBytes(response, byteArray, tipoReporte, "Estado_Cuenta_"+idEstadoCuenta);
			}
			return null;
		}catch(Exception e){
			LogDeMidasWeb.log("Error al imprimir estado de cuenta: "+idEstadoCuenta, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>id Estado Cuenta"+idEstadoCuenta+" , tipo de reporte: "+tipoReporte+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	public ActionForward rptEstadoCuentaMasivo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		String idEstadoCuenta = request.getParameter("idsEstadosCuenta");
//		String mostrarEjerciciosAnteriores = request.getParameter("mostrarEjerciciosAnteriores");
		String tipoReporte = Sistema.TIPO_PDF;
		byte[] byteArray = null;
		try {
//			boolean ejerciciosAnteriores = true;//false; 14/12/2010. Siempre se deben mostrar los saldos del ejercicio anterior
//			if(tipoReporte == null)
//				tipoReporte = Sistema.TIPO_PDF;
//			if(mostrarEjerciciosAnteriores != null && mostrarEjerciciosAnteriores.compareTo("true") == 0)
//				ejerciciosAnteriores = true;
			if(idEstadoCuenta != null){
//				String[] idToEstadoCuentaStringArray = idEstadoCuenta.split(",");
//				BigDecimal [] idToEstadoCuentaArray = new BigDecimal[idToEstadoCuentaStringArray.length];
//				for(int i=0;i<idToEstadoCuentaStringArray.length;i++){
//					idToEstadoCuentaArray[i]=new BigDecimal(idToEstadoCuentaStringArray[i]);
//				}
//				ReporteEstadoCuenta reporteEstadoCuenta = new ReporteEstadoCuenta(ejerciciosAnteriores,Sistema.TIPO_PDF, idToEstadoCuentaArray);
//				byteArray = reporteEstadoCuenta.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request), false);
				
				ReporteEstadoCuentaFacultativo reporteEstadoCuentaF = new ReporteEstadoCuentaFacultativo(tipoReporte);
				reporteEstadoCuentaF.establecerParametroConsulta("idEstadoCuentaMasivo", idEstadoCuenta);
				reporteEstadoCuentaF.establecerParametroConsulta("impresionMasivaAutomaticos", "true");
				reporteEstadoCuentaF.setImprimirFormatoAutomaticos(true);
				
				byteArray = reporteEstadoCuentaF.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
			}
			if(byteArray != null){
				this.writeBytes(response, byteArray, tipoReporte, "Estado_Cuenta_"+idEstadoCuenta);
			}
			return null;
		}catch(Exception e){
			LogDeMidasWeb.log("Error al imprimir estado de cuenta: "+idEstadoCuenta, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>id Estado Cuenta"+idEstadoCuenta+" , tipo de reporte: "+tipoReporte+"</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	public void rptEstadoCuentaFacultativo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String idEstadoCuenta = request.getParameter("idEstadoCuenta");
		String suscripcion = request.getParameter("suscripcion");
		String fechaInicial = request.getParameter("fechaInicial");
	  	String fechaFinal = request.getParameter("fechaFinal");
	  	try {
	  		if(!UtileriasWeb.esCadenaVacia(idEstadoCuenta) && 
	  				!UtileriasWeb.esCadenaVacia(suscripcion) &&
	  				!UtileriasWeb.esCadenaVacia(fechaInicial) &&
	  				!UtileriasWeb.esCadenaVacia(fechaFinal)){
	  			//Generar el reporte usando query's
	  			parametros.put("pIdEdoCuenta", Integer.valueOf(idEstadoCuenta));
				parametros.put("pSuscripcion", suscripcion);
			 	parametros.put("pfInicialPeriodo", UtileriasWeb.getFechaFromString(fechaInicial));
			 	parametros.put("pfFinalPeriodo", UtileriasWeb.getFechaFromString(fechaFinal));
				parametros.put("pImpresion", 0);
				parametros.put("pLogoUrl", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.imagenes"));
	 	  		ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.rptEstadoCuentaFacultativo"), parametros, Sistema.TIPO_PDF, Sistema.RPT_COMO_ADJUNTO, response);
	  		}
	  		//Generar el reporte usando objetos
	  		else{
	  			String tipoReporte = request.getParameter("tipoReporte");
	  			String mostrarEjerciciosAnteriores = request.getParameter("mostrarEjerciciosAnteriores");
				String idToPolizaStr = request.getParameter("idPoliza");
				String idPolizaSubRamo = request.getParameter("idPolizaSubRamo");
				
				String idReporteSiniestro = request.getParameter("idReporteSiniestro");
				String idSiniestroSubRamo = request.getParameter("idSiniestroSubRamo");
				
				String separarPorReaseguradorStr = request.getParameter("separarPorReasegurador");
				String hasta = request.getParameter("hasta");
				String tipoConsulta = request.getParameter("tipoConsulta");
				String idReasegurador = request.getParameter("idReasegurador");
				
				String idMoneda = request.getParameter("idMoneda");
				String idSubRamo = request.getParameter("idSubRamo");
				String idSiniestro = request.getParameter("idSiniestro");
				
//				String idPoliza = request.getParameter("idPoliza");
				
				String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
				
				if(UtileriasWeb.esCadenaVacia(tipoReporte))
					tipoReporte = Sistema.TIPO_PDF;
				
				ReporteEstadoCuentaFacultativo reporteEstadoCuentaFacultativo = new ReporteEstadoCuentaFacultativo(tipoReporte);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idEstadoCuenta", idEstadoCuenta);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("mostrarEjerciciosAnteriores", mostrarEjerciciosAnteriores);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idPoliza", idToPolizaStr);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idPolizaSubRamo", idPolizaSubRamo);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idReporteSiniestro", idReporteSiniestro);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idSiniestroSubRamo", idSiniestroSubRamo);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("separarPorReasegurador", separarPorReaseguradorStr);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("hasta", hasta);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("tipoConsulta", tipoConsulta);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idReasegurador", idReasegurador);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idMoneda",idMoneda);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idSubRamo",idSubRamo);
				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idSiniestro",idSiniestro);
//				reporteEstadoCuentaFacultativo.establecerParametroConsulta("clavePolizaSubRamo",clavePolizaSubRamo);
//				reporteEstadoCuentaFacultativo.establecerParametroConsulta("idPoliza",idPoliza);
				
				byte[] byteArray = null;
				try {
					byteArray = reporteEstadoCuentaFacultativo.obtenerReporte(nombreUsuario);
					this.writeBytes(response, byteArray, tipoReporte, reporteEstadoCuentaFacultativo.getTituloEstadoCuenta());
				} catch (SystemException e) {
					LogDeMidasWeb.log("EstadosCuentaAction.rptEstadoCuentaFacultativo(...) Ocurri� un error al imprimir un estado de cuenta.", Level.SEVERE, e);
				}
	  		}
		} catch (IOException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		 } catch (ParseException e) {
		 	e.printStackTrace();
		 	UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
	}
	
	public void rptEstadoCuentaFacultativoMasivo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String idsEstadosCuenta = request.getParameter("idsEstadosCuenta");
		List<byte[]> listaReportesByte = new ArrayList<byte[]>();
	  	 try {
	  		String[] arrayIdsEstadosCuenta = idsEstadosCuenta.split(",");
	  		String[] id_numSuscripcion;
	  		ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
	  		for (int i = 0; i < arrayIdsEstadosCuenta.length; i++){
	  			id_numSuscripcion = arrayIdsEstadosCuenta[i].split("_");
	  			EstadoCuentaDTO estadoCuentaDTO;
				estadoCuentaDTO = EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(id_numSuscripcion[0]));
				EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = new EstadoCuentaDecoradoDTO(estadoCuentaDTO);
				parametros.put("pIdEdoCuenta", Integer.valueOf(estadoCuentaDTO.getIdEstadoCuenta().toBigInteger().toString()));
				parametros.put("pSuscripcion", id_numSuscripcion[1]);
			 	parametros.put("pfInicialPeriodo", estadoCuentaDecoradoDTO.getFechaInicialPeriodo());
			 	parametros.put("pfFinalPeriodo", estadoCuentaDecoradoDTO.getFechaFinalPeriodo());
				parametros.put("pImpresion", 0);
				parametros.put("pLogoUrl", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.imagenes"));
				
				byte[] reporteBytes = null; 
				try{
					reporteBytes = reportesReaseguro.generarReportePLSQLAndReturnByteArray(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.rptEstadoCuentaFacultativo"), parametros, Sistema.TIPO_PDF, Sistema.RPT_COMO_ADJUNTO, response);
				}catch (Exception e) {
					e.printStackTrace();
					UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
				}
				if (reporteBytes != null)
					listaReportesByte.add(reporteBytes);
	  		}
	  		if (listaReportesByte.size() > 0){
	  			reportesReaseguro.concatenarReportes(listaReportesByte, UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.rptEstadoCuentaFacultativo"), response);
	  		}
		} catch (IOException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Action para consultar los estados de cuenta por subramo de una p�liza facultativa. Escribe el resultado de la consulta en formato json en el response.
	 */
	public void mostrarEstadosCuentaPorPoliza(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response){
		try{
			String idToPoliza = request.getParameter("idToPoliza");
//			PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			String json = "{rows:[]}";
//			List<EstadoCuentaDecoradoDTO> estadoCuentaDecoradoDTOList = new ArrayList<EstadoCuentaDecoradoDTO>();
			if (!UtileriasWeb.esCadenaVacia(idToPoliza) && !idToPoliza.equals("undefined") && !idToPoliza.equals("null")){
				json = PresentacionEstadoCuentaDN.getInstancia().obtenerCadenaJsonEstadoCuentaFacultativoPorPoliza(UtileriasWeb.regresaBigDecimal(idToPoliza));
			}
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
		}catch(IOException e){
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
}