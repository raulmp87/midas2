package mx.com.afirme.midas.reaseguro.movimiento;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;


import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroServiciosRemote;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MovimientoReaseguroSN {

	private MovimientoReaseguroServiciosRemote beanRemoto;
	
	public MovimientoReaseguroSN()
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en MovimientoReaseguroSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MovimientoReaseguroServiciosRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento) throws ExcepcionDeAccesoADatos{
		return beanRemoto.registrarMovimiento(movimiento,Boolean.FALSE);
		
	}
	
	/**
	 * Registra un conjunto de movimientos reaseguro en una sola transacción.
	 * @param movimientos
	 */
	public void registrarMovimientos(List<MovimientoReaseguroDTO> movimientos) throws ExcepcionDeAccesoADatos{
		beanRemoto.registrarMovimientos(movimientos);
	}
	
	public void deshacerMovimientosPorPoliza(PolizaSoporteDanosDTO polizaSoporteDanosDTO){
		beanRemoto.deshacerMovimientosPorPoliza(polizaSoporteDanosDTO);
	}
	
	public int getMaxId(Integer mes,Integer anio){
		return beanRemoto.getMaxId(mes,anio);
	}
	
	public int obtenerCantidadMovimientosSiniestrosNoAcumulables(){
		return beanRemoto.obtenerCantidadMovimientosSiniestrosNoAcumulables();
	}
	
	public int obtenerCantidadMovimientosSiniestrosConFechaIncorrecta(){
		return beanRemoto.obtenerCantidadMovimientosSiniestrosConFechaIncorrecta();
	}
	
	public List<MovimientoReaseguroDTO> listarPorRangoIds(Integer mes,Integer anio,int limiteInferior, int limiteSuperior) {
		return beanRemoto.listarPorRangoIds(mes,anio,limiteInferior, limiteSuperior);
	}
	
	public List<MovimientoReaseguroDTO> listarPorRangoIds(List<BigDecimal> listaIdToMovimientos,int limiteInferior, int limiteSuperior) {
		return beanRemoto.listarPorRangoIds(listaIdToMovimientos,limiteInferior, limiteSuperior);
	}
	
	public List<MovimientoReaseguroDTO> listarMovimientosSiniestrosNoAcumulables(Integer limiteInferior, Integer limiteSuperior) {
		return beanRemoto.listarMovimientosSiniestrosNoAcumulables(limiteInferior, limiteSuperior);
	}
	
	public List<MovimientoReaseguroDTO> listarMovimientosSiniestrosConFechaIncorrecta(Integer limiteInferior, Integer limiteSuperior) {
		return beanRemoto.listarMovimientosSiniestrosConFechaIncorrecta(limiteInferior, limiteSuperior);
	}
	
	public void acumularMovimientosTerminacionSiniestros(BigDecimal idToReporteSiniestro){
		beanRemoto.acumularMovimientosTerminacionSiniestros(idToReporteSiniestro);
	}
	
	public List<AsientoContableDTO>  contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable, String usuario){
		return beanRemoto.contabilizaMovimientosReaIn(idObjetoContable, claveTransaccionContable, usuario);
	}
	
	public boolean recalcularAcumulador(BigDecimal idToAcumulador){
		return beanRemoto.recalcularAcumulador(idToAcumulador);
	}
	
	public List<BigDecimal> obtenerIdReporteSiniestrosTerminados(){
		return beanRemoto.obtenerIdReporteSiniestrosTerminados();
	}
	
	public List<SubRamoDTO> obtenerSubRamosAfectadosPorSiniestro(BigDecimal idToReporteSiniestro,boolean incluirSoloSubRamosFacultados){
		return beanRemoto.obtenerSubRamosAfectadosPorSiniestro(idToReporteSiniestro,incluirSoloSubRamosFacultados);
	}
}
