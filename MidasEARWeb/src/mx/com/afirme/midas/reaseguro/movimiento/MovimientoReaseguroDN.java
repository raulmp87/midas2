package mx.com.afirme.midas.reaseguro.movimiento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.contratos.movimiento.MovimientoReaseguroDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.reaseguro.acumulador.AcumuladorSN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;


public class MovimientoReaseguroDN {

	private static final MovimientoReaseguroDN INSTANCIA = new MovimientoReaseguroDN();
	
	public static MovimientoReaseguroDN getInstancia() {
		return INSTANCIA;
	}	
	
	public MovimientoReaseguroDTO registrarMovimiento(MovimientoReaseguroDTO movimiento) throws ExcepcionDeAccesoADatos, SystemException,ExcepcionDeLogicaNegocio{
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		MovimientoReaseguroDTO movimientoReaseguroDTO = null;
		try{
			movimientoReaseguroDTO = movimientoReaseguroSN.registrarMovimiento(movimiento);	
		}catch(RuntimeException ex){
			throw new ExcepcionDeLogicaNegocio("MovimientoReaseguroDN", ex);
		}
		return movimientoReaseguroDTO;	 
	}
	
	/**
	 * Registra un conjunto de movimientos reaseguro en una sola transacci�n.
	 * @param movimientos
	 */
	public void registrarMovimientos(List<MovimientoReaseguroDTO> movimientos) throws ExcepcionDeAccesoADatos, SystemException,ExcepcionDeLogicaNegocio{
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		
		try{
			movimientoReaseguroSN.registrarMovimientos(movimientos);	
		}catch(RuntimeException ex){
			throw new ExcepcionDeLogicaNegocio("MovimientoReaseguroDN", ex);
		}		
	}
	
	public void deshacerMovimientosPorPoliza(PolizaSoporteDanosDTO polizaSoporteDanosDTO) throws SystemException{
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		movimientoReaseguroSN.deshacerMovimientosPorPoliza(polizaSoporteDanosDTO);
	}
	
	public void regenerarAcumuladoresReaseguro(String mesPorProcesar, String anioPorProcesar,Integer codigoProceso,String idToMovimientos, String acumularCantidad) throws SystemException{
		AcumuladorSN acumuladorSN = new AcumuladorSN();
		if(codigoProceso != null && codigoProceso == 2){
			acumuladorSN.deshacerMovimientosSiniestrosNoAcumulables();
		}else if(codigoProceso != null && codigoProceso == 3){
			acumuladorSN.deshacerMovimientosSiniestrosConFechaIncorrecta();
		}else if(codigoProceso != null && codigoProceso == 4){
			boolean restarCantidad = false;
			if(!UtileriasWeb.esCadenaVacia(idToMovimientos) && !UtileriasWeb.esCadenaVacia(acumularCantidad) && !UtileriasWeb.esCadenaVacia(acumularCantidad)){
				List<BigDecimal> idToMovimientosList = new ArrayList<BigDecimal>();
				String[] idToMovimientos_Str = idToMovimientos.split(",");
				for(int i=0; i < idToMovimientos_Str.length; i++){
					try{
						idToMovimientosList.add(new BigDecimal(idToMovimientos_Str[i]));
					}catch(Exception e){
						LogDeMidasWeb.log("Se recibi� un idToMovimiento inv�lido: "+idToMovimientos_Str[i]+", el cual ser� ignorado por el proceso de rec�lculo.", Level.INFO, null);
					}
				}
				restarCantidad = !Boolean.valueOf(acumularCantidad).booleanValue();
				acumuladorSN.regenerarAcumuladoresReaseguro(mesPorProcesar,anioPorProcesar,idToMovimientosList,restarCantidad);
			}
		}else{
			boolean procesarRangoFechas = false;
			Integer mes = null, anio = null;
			procesarRangoFechas = (!UtileriasWeb.esCadenaVacia(mesPorProcesar) && !UtileriasWeb.esCadenaVacia(anioPorProcesar));
			if(procesarRangoFechas){
				try{
					mes = Integer.valueOf(mesPorProcesar);
					anio = Integer.valueOf(anioPorProcesar);
					procesarRangoFechas = mes > 0 && mes < 12;
					if (procesarRangoFechas)
						procesarRangoFechas = anio > 2008;
				}catch(Exception e){
					procesarRangoFechas = false;
					mes = anio = null;
				}
			}
			regenerarAcumuladores(mes,anio);
		}
	}
	
	public void acumularMovimientosTerminacionSiniestros(BigDecimal idToReporteSiniestro) throws SystemException{
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		movimientoReaseguroSN.acumularMovimientosTerminacionSiniestros(idToReporteSiniestro);
	}
	
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable, String usuario) throws SystemException {
		MovimientoReaseguroSN movimientoReaseguroSN = new MovimientoReaseguroSN();
		return movimientoReaseguroSN.contabilizaMovimientos(idObjetoContable, claveTransaccionContable, usuario);		
	}
	
	public void acumularMovimientosTerminacionSiniestros() throws SystemException{
		List<BigDecimal> listaIdsReporteSiniestroPendientesAcumular= new MovimientoReaseguroSN().obtenerIdReporteSiniestrosTerminados();
		LogDeMidasWeb.log("Acumulando movimientos de terminaci�n de siniestros. Cantidad de siniestros terminados encontrados: "+(listaIdsReporteSiniestroPendientesAcumular != null ? listaIdsReporteSiniestroPendientesAcumular.size() : "0"), Level.INFO, null);
		if(listaIdsReporteSiniestroPendientesAcumular != null && !listaIdsReporteSiniestroPendientesAcumular.isEmpty()){
			for(BigDecimal idToReporteSiniestro : listaIdsReporteSiniestroPendientesAcumular){
				if(idToReporteSiniestro.compareTo(new BigDecimal(481)) == 0){
					System.out.print("Entra");
				}
				acumularMovimientosTerminacionSiniestros(idToReporteSiniestro);
			}
		}
		LogDeMidasWeb.log("Termina el procesamiento de movimientos de terminaci�n de siniestros. Se recalcular�n los acumuladores.", Level.INFO, null);
	}
	
	public void regenerarAcumuladores(Integer mesPorProcesar,Integer anioPorProcesar) throws SystemException{
		List<BigDecimal> listaIdsAcumuladoresPorRegenerar = new AcumuladorSN().obtenerIdsAcumuladores(mesPorProcesar, anioPorProcesar);
		StringBuilder idsSinRegenerarConcat = new StringBuilder("");
		String idsSinRegenerar = "";
		boolean errorAlRegenerar = false;
		if(listaIdsAcumuladoresPorRegenerar != null && !listaIdsAcumuladoresPorRegenerar.isEmpty()){
			for (BigDecimal idToAcumulador : listaIdsAcumuladoresPorRegenerar){
				if(!recalcularAcumulador(idToAcumulador)){
					idsSinRegenerarConcat.append(idToAcumulador).append(",");
					errorAlRegenerar = true;
				}
			}
			if(errorAlRegenerar){
				idsSinRegenerar = idsSinRegenerarConcat.substring(0,idsSinRegenerarConcat.length()-1);
				LogDeMidasWeb.log("Los siguientes acumuladores no se pudieron regenerar: "+idsSinRegenerar, Level.SEVERE, null);
			}
			else{
				LogDeMidasWeb.log("Finaliza exitosamente el rec�lculo de "+listaIdsAcumuladoresPorRegenerar.size()+" acumuladores "+
						((mesPorProcesar != null && anioPorProcesar != null)?"mes: "+mesPorProcesar+", anio: "+anioPorProcesar : ""), Level.INFO, null);
			}
		}
		else{
			LogDeMidasWeb.log("La consulta de acumuladores no arroj� resultados, no se pudo procesar ning�n Acumulador.", Level.WARNING, null);
		}
	}
	public boolean recalcularAcumulador(BigDecimal idToAcumulador) throws SystemException{
		return new MovimientoReaseguroSN().recalcularAcumulador(idToAcumulador);
	}
	
	public List<SubRamoDTO> obtenerSubRamosAfectadosPorSiniestro(BigDecimal idToReporteSiniestro) throws SystemException{
		return new MovimientoReaseguroSN().obtenerSubRamosAfectadosPorSiniestro(idToReporteSiniestro,false);
	}
	
	public List<SubRamoDTO> obtenerSubRamosFacultativosAfectadosPorSiniestro(BigDecimal idToReporteSiniestro) throws SystemException{
		return new MovimientoReaseguroSN().obtenerSubRamosAfectadosPorSiniestro(idToReporteSiniestro,true);
	}
}
