package mx.com.afirme.midas.reaseguro.movimiento;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoMovimientoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class TipoMovimientoSN {

	private TipoMovimientoFacadeRemote beanRemoto;
	
	public TipoMovimientoSN()
			throws SystemException{
		
		try{
			LogDeMidasWeb.log("Entrando en TipoMovimientoSN - Constructor", Level.INFO,
					null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(TipoMovimientoFacadeRemote.class);
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}catch (Exception e) {
				throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<TipoMovimientoDTO> listarTodos(){
		return beanRemoto.findAll();
	}
}
