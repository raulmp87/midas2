package mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro;

import java.util.ArrayList;

import mx.com.afirme.midas.reaseguro.reportes.saldos.ReporteMovimientosSaldoBase;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author Jose Luis Arellano
 * @since 19/11/2010
 */
public class ReporteSaldosCuentasPorCobrar extends ReporteMovimientosSaldoBase{
	
	public ReporteSaldosCuentasPorCobrar(){
		super.setListaPlantillas(new ArrayList<byte[]>());
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		return super.obtenerReporte(claveUsuario);
	}
	
}
