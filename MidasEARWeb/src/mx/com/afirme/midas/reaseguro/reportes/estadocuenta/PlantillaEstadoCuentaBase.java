package mx.com.afirme.midas.reaseguro.reportes.estadocuenta;

import java.util.List;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.generica.DatoIncisoCotizacionForm;

public abstract class PlantillaEstadoCuentaBase extends MidasPlantillaBase{
	protected List<DatoIncisoCotizacionForm> listaDatosEstadoCuenta;
	protected String nombrePlantillaSubReporteDatosEstadoCuenta = "";
	private String tituloReporte;
	
	public List<DatoIncisoCotizacionForm> getListaDatosEstadoCuenta() {
		return listaDatosEstadoCuenta;
	}

	public void setListaDatosEstadoCuenta(List<DatoIncisoCotizacionForm> listaDatosEstadoCuenta) {
		this.listaDatosEstadoCuenta = listaDatosEstadoCuenta;
	}
	
	public String getTituloReporte() {
		return tituloReporte;
	}
	
	public void setTituloReporte(String tituloReporte) {
		if(getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put("TITULO_ESTADO_CUENTA", tituloReporte);
		}
		this.tituloReporte = tituloReporte;
	}
}
