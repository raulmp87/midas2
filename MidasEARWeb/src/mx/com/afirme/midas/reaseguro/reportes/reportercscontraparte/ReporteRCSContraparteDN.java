package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ReporteRCSContraparteDN {

	private static final ReporteRCSContraparteDN INSTANCIA = new ReporteRCSContraparteDN();
	
	public static ReporteRCSContraparteDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<ReporteRCSContraparteDTO> obtenerReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario)
			throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();
		
		return reporteReaseguroSN.obtenerReporte(fechaCorte, tipoCambio, nombreUsuario);
	}
	
	public void procesarReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario)
	throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();
		
		reporteReaseguroSN.procesarReporte(fechaCorte, tipoCambio, nombreUsuario);
}
	
	public List<ReporteRCSContraparteDTO> listarTodos(String fechaCorte) throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();
		return reporteReaseguroSN.listarTodos(fechaCorte);
	}
	
	public ReporteRCSContraparteDTO getPorId(Integer id) throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();
		return reporteReaseguroSN.getPorId(id);
	}
	
	public void borrar(ReporteRCSContraparteDTO reporteDTO) throws SystemException {
		ReporteRCSContraparteSN solicitudSN = new ReporteRCSContraparteSN();
		solicitudSN.borrar(reporteDTO);
	}
	
	public List<CargaContraParteDTO> listarCargas(String fechaCorte) throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();
		return reporteReaseguroSN.listarCargas(fechaCorte);
	}
	
	public void actualizarEstatusCarga(int idCarga, int estatusCarga) throws SystemException {
		ReporteRCSContraparteSN solicitudSN = new ReporteRCSContraparteSN();
		solicitudSN.actualizarEstatusCarga(idCarga, estatusCarga);
	}
	
	public int obtenerRegistros(Date fechaCorte)
	throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();

		return reporteReaseguroSN.obtenerRegistros(fechaCorte);
	}
	
	public List<String> obtenerArchivos(Date fechaFinal)
	throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();

	return reporteReaseguroSN.obtenerArchivos(fechaFinal);
	}
	
	public List<Object[]> obtenerDesgloce(Date fechaFinal, BigDecimal tipoCambio)
	throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();

		return reporteReaseguroSN.obtenerDesgloce(fechaFinal, tipoCambio);
	}
	
	public List<Object[]> obtenerIntegracion(Date fechaFinal, BigDecimal tipoCambio, String concepto)
	throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();

		return reporteReaseguroSN.obtenerIntegracion(fechaFinal, tipoCambio, concepto);
	}
	
	public List<Object[]> obtenerIntegracionNRS(Date fechaFinal, BigDecimal tipoCambio)
	throws SystemException {
		ReporteRCSContraparteSN reporteReaseguroSN = new ReporteRCSContraparteSN();

		return reporteReaseguroSN.obtenerIntegracionNRS(fechaFinal, tipoCambio);
	}
}
