package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle;

import java.io.File;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador.MovimientoPrimaReaseguradorDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasXMLCreator;
import mx.com.afirme.midas.sistema.MidasXMLHandler;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class MovimientoPrimaReaseguradorDetalleDN {
	private static final MovimientoPrimaReaseguradorDetalleDN INSTANCIA = new MovimientoPrimaReaseguradorDetalleDN();
	private static final String PREFIJO_ARCHIVO_XML = "Movimientos_Prima_Por_Reasegurador_Detalle_";
	private static final String PREFIJO_ARCHIVO_XLS = "ReporteMovimientosPrimaPorReaseguradorDetalle_Del_";
	public static final int CON_RETENCION = 1;
	public static final int SIN_RETENCION = 0;
	
	private MovimientoPrimaReaseguradorDetalleDN(){
	}
	
	public static MovimientoPrimaReaseguradorDetalleDN getInstancia(){
		return INSTANCIA;
	}
	
	public List<MovimientoPrimaReaseguradorDetalleDTO> obtenerMovimientosPrimaDetalladosPorReasegurador(MovimientoPrimaReaseguradorDetalleDTO entity,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		return new MovimientoPrimaReaseguradorDetalleSN().obtenerMovimientosPrimaDetalladosPorReasegurador(entity, nombreUsuario);
	}
	
	public boolean generarDocumentoXMLMovimientosPorReaseguradorDetalle(Date fecha,boolean sobreEscribirArchivo,String nombreUsuario,int conRetencion){
		boolean result = true;
		if (fecha == null)
			fecha = new Date();
		String nombreDocumento = obtenerNombreDocumentoXML(fecha,conRetencion);
		LogDeMidasWeb.log("Generando archivo XML de movimientos detallados de prima por reasegurador: "+nombreDocumento, Level.INFO, null);
		File file = new File(nombreDocumento);
		if(file.exists()){
			LogDeMidasWeb.log("Se encontr� el archivo XML: "+nombreDocumento, Level.INFO, null);
			if(sobreEscribirArchivo){
				if ( file.delete() ){
					LogDeMidasWeb.log("Se elimin� el archivo XML :"+nombreDocumento+". Se generar� nuevamente.", Level.INFO, null);
					
				}
				else
					LogDeMidasWeb.log("No se pudo eliminar el archivo XML :"+nombreDocumento, Level.INFO, null);
			}
			else
				return result;
		}
		//Obtener los movimientos correspondientes a la fecha recibida;
		MovimientoPrimaReaseguradorDetalleDTO movimientoPrimaReasegurador = new MovimientoPrimaReaseguradorDetalleDTO();
//		movimientoPrimaReasegurador.setFechaInicioVigencia(fecha);
		Calendar calendarInicio = Calendar.getInstance();
		calendarInicio.setTime(fecha);
		calendarInicio.set(Calendar.HOUR, 0);
		calendarInicio.set(Calendar.MINUTE, 0);
		calendarInicio.set(Calendar.SECOND, 0);
		movimientoPrimaReasegurador.setFechaInicioVigencia(calendarInicio.getTime());
		Calendar calendarFin = Calendar.getInstance();
		calendarFin.setTime(fecha);
		calendarInicio.set(Calendar.HOUR, 23);
		calendarInicio.set(Calendar.MINUTE, 59);
		calendarInicio.set(Calendar.SECOND, 59);
		movimientoPrimaReasegurador.setFechaFinVigencia(calendarFin.getTime());
		movimientoPrimaReasegurador.setRetencion(conRetencion);
		List<MovimientoPrimaReaseguradorDetalleDTO> listaMovimientos;
		Runtime runtime = Runtime.getRuntime();
		try {
			listaMovimientos = obtenerMovimientosPrimaDetalladosPorReasegurador(movimientoPrimaReasegurador, nombreUsuario);
			LogDeMidasWeb.log("Cantidad de movimientos detallados de prima por reasegurador encontrados para la fecha: "+fecha.toString()+": "+listaMovimientos.size(), Level.INFO, null);
			String[] listaAtributos = {"codigoSubRamo","folioPoliza","numeroEndoso","tipoEndoso","identificador","fechaInicioVigencia",
					"fechaFinVigencia","numeroInciso","numeroSubInciso","porcentajeRetencion",
					"porcentajeCuotaParte","porcentajePrimerExcedente","porcentajeFacultativo","montoPrimaNeta",
					"folioContrato","tipoReaseguro","nombreCuentaDe","porcentajeParticipacion",
					"porcentajeComision","corredor","porcentajeParticipacionCorredor","porcentajeComisionCorredor",
					"ambito","montoPrimas","montoComisiones","descripcionMoneda",
					"fechaMovto","nombreSucursal","idAsegurado","nombreAsegurado",
					"monedaReporte","folioPolizaReporte","montoSumaAsegurada","montoSumaAseguradaReaseguro",
					"montoSumaAseguradaDistribuida","fechaEmisionPoliza","seccion","cobertura", 
					"registroCNSF","porcentajePleno","nombreOficina","claveGerencia",
					"gerencia","claveOficina","claveSupervisoria","supervisoria",
					"idTipoReaseguro","folioPolizaAnterior","montoPrimaRetenida","montoPrimaCuotaParte",
					"montoPrimaPrimerExcedente","montoPrimaFacultativo","montoPrimasRetenidas"};
			MidasXMLCreator xmlCreator = new MidasXMLCreator(listaAtributos);
			List<Object> listaObjetos = new ArrayList<Object>();
			listaObjetos.addAll(listaMovimientos);
			result = xmlCreator.generarDocumentoXML(listaObjetos, "MovimientosDetalladosPrimaPorReasegurador", "MovimientoDetallado", nombreDocumento);
			listaMovimientos = null;
			listaObjetos = null;
			runtime.gc();
			if(result)
				LogDeMidasWeb.log("Se gener� el archivo XML: "+nombreDocumento, Level.INFO, null);
			else
				LogDeMidasWeb.log("No se pudo generar el archivo XML: "+nombreDocumento, Level.SEVERE, null);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XML: "+nombreDocumento, Level.SEVERE, e);
			result = false;
		} catch (SystemException e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XML: "+nombreDocumento, Level.SEVERE, e);
			result = false;
		} catch(Exception e){
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XML: "+nombreDocumento, Level.SEVERE, e);
			result = false;
		}
		return result;
	}
	
	public boolean generarDocumentoXLSMovimientosPorReaseguradorDet(Date fechaInicial,Date fechaFinal,int conRetencion,boolean sobreEscribirArchivo,List<Date> fechasPorSobreEscribir,String nombreUsuario) throws SystemException{
		LogDeMidasWeb.log("Generando reporte XLS de movimientos detallados de prima por reasegurador. Rango de fechas: del "+fechaInicial.toString()+" al "+fechaFinal.toString(), Level.INFO, null);
		if (fechaFinal.before(fechaInicial)){
			LogDeMidasWeb.log("Error al generar reporte XLS de movimientos detallados de prima por reasegurador. Rango de fechas: del "+fechaInicial.toString()+" al "+fechaFinal.toString()+ " NO V�LIDO", Level.SEVERE, null);
			return false;
		}
		Runtime runtime = Runtime.getRuntime();
		boolean resultado = true;
		String nombreArchivo = obtenerNombreDocumentoXLS(fechaInicial, fechaFinal,conRetencion);
		Calendar calendarFechaFinal = Calendar.getInstance();
		calendarFechaFinal.setTime(fechaFinal);
		File file = new File(nombreArchivo);
		if(file.exists()){
			LogDeMidasWeb.log("Se encontr� el archivo "+nombreArchivo, Level.INFO, null);
			if(sobreEscribirArchivo){
				if(!(fechasPorSobreEscribir != null && !fechasPorSobreEscribir.isEmpty() )){//Sobreescribor subreportes por d�a, indicados en fechasPorSobreEscribir
					return resultado;
				}
				else{	
					LogDeMidasWeb.log("Se sobreescribir� el archivo "+nombreArchivo, Level.INFO, null);
					try{
						new File(nombreArchivo).delete();
						LogDeMidasWeb.log("Se elimin� correctamente el archivo "+nombreArchivo, Level.INFO, null);
						//eliminar todos los archivos XLS del mes
						Format formatter = new SimpleDateFormat("MM_yyyy");
						MovimientoPrimaReaseguradorDN.eliminarArchivos(UtileriasWeb.obtenerRutaDocumentosXLSReportes(), 
								PREFIJO_ARCHIVO_XLS, (formatter.format(fechaFinal)+((conRetencion == SIN_RETENCION) ? "SR" : "CR"))+".xls");
					}catch(Exception e){}
				}
			}
			else	return resultado;
		}
		calendarFechaFinal.add(Calendar.DATE, 1);
		Calendar calendarEntreRango = Calendar.getInstance();
		calendarEntreRango.setTime(fechaInicial);
		int reportesSobreescritosPorFecha = 0;
		do{
			boolean sobreEscribirArchivoFechaActual = false;
			if(sobreEscribirArchivo){
				if(reportesSobreescritosPorFecha < fechasPorSobreEscribir.size()){
					for(int i=0;i<fechasPorSobreEscribir.size();i++){
						if(fechasPorSobreEscribir.get(i) != null){
							Calendar calendarTMP = Calendar.getInstance();
							calendarTMP.setTime(fechasPorSobreEscribir.get(i));
							if(calendarTMP.get(Calendar.DATE) == calendarEntreRango.get(Calendar.DATE) &&
									calendarTMP.get(Calendar.MONTH) == calendarEntreRango.get(Calendar.MONTH) &&
										calendarTMP.get(Calendar.YEAR) == calendarEntreRango.get(Calendar.YEAR)){
								sobreEscribirArchivoFechaActual = true;
								reportesSobreescritosPorFecha ++;
							}
						}
					}
				}
			}
			if(generarDocumentoXMLMovimientosPorReaseguradorDetalle(calendarEntreRango.getTime(), sobreEscribirArchivoFechaActual, nombreArchivo,conRetencion)){
				calendarEntreRango.add(Calendar.DATE, 1);
			}
			else
				return false;
		}while(calendarEntreRango.get(Calendar.DATE) != calendarFechaFinal.get(Calendar.DATE) || calendarEntreRango.get(Calendar.MONTH) != calendarFechaFinal.get(Calendar.MONTH) || calendarEntreRango.get(Calendar.YEAR) != calendarFechaFinal.get(Calendar.YEAR));
		//Se generaron todos los documentos XML necesarios para obtener el archivo XML.
		calendarEntreRango = Calendar.getInstance();
		calendarEntreRango.setTime(fechaInicial);
		String[] listaAtributos = {"codigoSubRamo","folioPoliza","numeroEndoso","tipoEndoso","identificador","fechaEmisionPoliza","fechaInicioVigencia",
				"fechaFinVigencia","numeroInciso","numeroSubInciso","seccion","cobertura","porcentajePleno","porcentajeRetencion",
				"porcentajeCuotaParte","porcentajePrimerExcedente","porcentajeFacultativo","montoPrimaNeta",
				"montoPrimaRetenida","montoPrimaCuotaParte","montoPrimaPrimerExcedente","montoPrimaFacultativo",
				"folioContrato","tipoReaseguro","nombreCuentaDe","registroCNSF","porcentajeParticipacion",
				"porcentajeComision","corredor","porcentajeParticipacionCorredor","porcentajeComisionCorredor",
				"ambito","montoSumaAsegurada","montoSumaAseguradaReaseguro","montoSumaAseguradaDistribuida","montoPrimas",
				"montoPrimasRetenidas","montoComisiones","descripcionMoneda","fechaMovto","claveGerencia","gerencia",
				"claveOficina","nombreOficina","claveSupervisoria","supervisoria","idAsegurado","nombreAsegurado","folioPolizaAnterior"
				};
		String[] nombreColumnas= {"SUBRAMO","POLIZA","ENDOSO","TIPO ENDOSO","IDENTIFICADOR","FECHA EMISION POLIZA","VIGENCIA DESDE",
				"VIGENCIA HASTA","INCISO","SUBINCISO", "SECCION","COBERTURA","% PLENO","% RETENCION",
				"% CUOTA PARTE","% 1ER EXCEDENTE","% FACULTATIVO","PRIMA NETA",
				"PRIMA RETENIDA","PRIMA CUOTA PARTE","PRIMA A 1ER EXCEDENTE","PRIMA FACULTADA",
				"CODIGO DEL CONTRATO","TIPO REASEGURO","NOMBRE A CUENTA DE","CNSF","% PARTICIPACION A CUENTA DE",
				"% COMISION A CUENTA DE","NOMBRE CORREDOR","% PARTICIPACION DEL CORREDOR","% COMISION DEL CORREDOR",
				"LOCAL O EXTRANJERO","SUMA ASEGURADA CUMULO","SUMA ASEGURADA REASEGURO","SUMA ASEGURADA DISTRIBUIDA","PRIMA CEDIDA",
				"PRIMA RETENIDA","COMISIONES","MONEDA","FECHA DEL MOVIMIENTO","CLAVE GERENCIA","GERENCIA",
				"CLAVE OFICINA","OFICINA","CLAVE PROMOTORIA","PROMOTORIA","ID ASEGURADO","NOMBRE ASEGURADO","POLIZA SEYCOS"
				};
		
		MidasXLSCreator creadorDocumentoXLS = new MidasXLSCreator(nombreColumnas, listaAtributos,MidasXLSCreator.FORMATO_XLS);
		creadorDocumentoXLS.iniciarProcesamientoArchivoXLS("MovimientosPorReaseguradorDet");
//		String[] formatoCeldas= {null,null,"#0",null,null,null,
//				null,"#0","#0", null,null,"##0.0000","##0.0000",
//				"##0.0000","##0.0000","##0.0000","#,##0.0000",
//				"#,##0.0000","#,##0.0000","#,##0.0000","#,##0.0000",
//				null,null,null,null,"##0.0000",
//				"##0.0000",null,"##0.0000","##0.0000",
//				null,"#,##0.0000","#,##0.0000","#,##0.0000","#,##0.0000",
//				"#,##0.0000","#,##0.0000",null,null,null,null,
//				null,null,null,null,"#####0",null,null
//				};
//		creadorDocumentoXLS.setListaFormatoCeldas(formatoCeldas);
		do{
			String nombreDocumento = obtenerNombreDocumentoXML(calendarEntreRango.getTime(),conRetencion);
		

			if(! new File(nombreDocumento).exists()){
				if(!generarDocumentoXMLMovimientosPorReaseguradorDetalle(calendarEntreRango.getTime(), false, nombreUsuario,conRetencion))
					return false;
			}
			XMLReader xmlParser = new SAXParser();
			List<Object> listaRegistros = new ArrayList<Object>();
			xmlParser.setContentHandler(new MidasXMLHandler("MovimientoDetallado", listaRegistros, listaAtributos, MovimientoPrimaReaseguradorDetalleDTO.class.getCanonicalName()));
			try {
				xmlParser.parse(nombreDocumento);
			} catch (IOException e) {
				LogDeMidasWeb.log("Ocurri� un error al leer el archivo '"+nombreDocumento+"'. No se pudo generar el reporte '"+nombreArchivo+"'", Level.SEVERE, e);
				return false;
			} catch (SAXException e) {
				LogDeMidasWeb.log("Ocurri� un error al Parsear el archivo '"+nombreDocumento+"'. No se pudo generar el reporte '"+nombreArchivo+"'", Level.SEVERE, e);
				return false;
			}
			if(listaRegistros != null && !listaRegistros.isEmpty()){
				creadorDocumentoXLS.insertarFilasArchivoXLS(listaRegistros);
				listaRegistros = null;
				runtime.gc();
			}
			calendarEntreRango.add(Calendar.DATE, 1);
		}while (calendarEntreRango.get(Calendar.DATE) != calendarFechaFinal.get(Calendar.DATE) || calendarEntreRango.get(Calendar.MONTH) != calendarFechaFinal.get(Calendar.MONTH) || calendarEntreRango.get(Calendar.YEAR) != calendarFechaFinal.get(Calendar.YEAR));
		try {
			creadorDocumentoXLS.finalizarProcesamientoArchivoXLS(nombreArchivo);
			runtime.gc();
			LogDeMidasWeb.log("Se gener� el archivo '"+nombreArchivo+"'.", Level.INFO, null);
		} catch (IOException e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XLS: "+nombreArchivo, Level.INFO, null);
			resultado = false;
		}
		return resultado;
	}

	public void generarDocXMLMovtosPorReaseguradorDetDiaAnterior(String nombreUsuario){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		if(! generarDocumentoXMLMovimientosPorReaseguradorDetalle(calendar.getTime(), true, nombreUsuario, CON_RETENCION)){//Con retencion
		}
		if(!generarDocumentoXMLMovimientosPorReaseguradorDetalle(calendar.getTime(), true, nombreUsuario, 0)){//sin retencion
		}
		/*
		 * 30/07/2010. Jos� Luis Arellano. Por petici�n de Euler, se debe generar el REAS de todo el mes, no s�lo del d�a anterior.
		 */
		if(Sistema.REGENERAR_REAS_MENSUAL_JOB){
			Format formatter = new SimpleDateFormat("MM_yyyy");
			MovimientoPrimaReaseguradorDN.eliminarArchivos(UtileriasWeb.obtenerRutaDocumentosXLSReportes(), PREFIJO_ARCHIVO_XLS, formatter.format(calendar.getTime())+".xls");
			Calendar calendarHoy = Calendar.getInstance();
			calendar.add(Calendar.DATE, -1);
			while(calendarHoy.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)){
				generarDocumentoXMLMovimientosPorReaseguradorDetalle(calendar.getTime(), true, nombreUsuario, CON_RETENCION);//Con retencion
				generarDocumentoXMLMovimientosPorReaseguradorDetalle(calendar.getTime(), true, nombreUsuario, SIN_RETENCION);//sin retencion
				calendar.add(Calendar.DATE, -1);
			}
		}
	}
	
	public String obtenerNombreDocumentoXLS(Date fechaInicial,Date fechaFinal,int conRetencion){
		Format formatter = new SimpleDateFormat("dd_MM_yyyy");
		return UtileriasWeb.obtenerRutaDocumentosXLSReportes()+PREFIJO_ARCHIVO_XLS+formatter.format(fechaInicial)+
		"_Al_"+formatter.format(fechaFinal)+((conRetencion == SIN_RETENCION) ? "SR" : "CR")	+".xls";
	}
	
	public String obtenerNombreDocumentoXML(Date fecha,int conRetencion){
		Format formatter = new SimpleDateFormat("dd_MM_yyyy");
		return UtileriasWeb.obtenerRutaDocumentosXMLReportes()+PREFIJO_ARCHIVO_XML+formatter.format(fecha)+
		((conRetencion == SIN_RETENCION) ? "SR" : "CR")	+".xml";
	}
}