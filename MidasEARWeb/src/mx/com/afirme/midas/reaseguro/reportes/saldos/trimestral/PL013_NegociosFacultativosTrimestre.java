package mx.com.afirme.midas.reaseguro.reportes.saldos.trimestral;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteEstadoCuentaDN;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteReaseguradorNegociosFacultativosDTO;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteSaldoPorNegocioFacultativoDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL013_NegociosFacultativosTrimestre extends MidasPlantillaBase {

	private BigDecimal idTcReasegurador;
	private Integer ejercicio;
	private Integer suscripcion;
	private boolean excluirSaldoCorredor = true;
	
	public PL013_NegociosFacultativosTrimestre(){
		inicializarDatosPlantilla();
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteNegociosFacultativos();
		return getByteArrayReport();
	}
	
	private void generarReporteNegociosFacultativos() throws SystemException{
		try {
			ReporteReaseguradorNegociosFacultativosDTO resumenFacultativosTrimestre = ReporteEstadoCuentaDN.getInstancia().obtenerReporteMovimientosFacultativosTrimestre(idTcReasegurador, ejercicio, suscripcion, excluirSaldoCorredor);
			if(resumenFacultativosTrimestre != null){
				
				getParametrosVariablesReporte().put("NOMBRE_REASEGURADOR", resumenFacultativosTrimestre.getNombreReasegurador());
				getParametrosVariablesReporte().put("TITULO_REPORTE", "NEGOCIOS CON REASEGURO FACULTATIVO "+resumenFacultativosTrimestre.getSuscripcionDTO().getDescripcion());
				
				if(resumenFacultativosTrimestre.getListaSaldosNegociosFacultativos() != null && 
						!resumenFacultativosTrimestre.getListaSaldosNegociosFacultativos().isEmpty()){
					getListaRegistrosContenido().clear();
					for(ReporteSaldoPorNegocioFacultativoDTO resumenNegocio : resumenFacultativosTrimestre.getListaSaldosNegociosFacultativos()){
						getListaRegistrosContenido().add(resumenNegocio);
					}
				}
				
				finalizarReporte();
			}
		} catch (SystemException e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el reporte trimestral de negocios facultativos por reasegurador.", Level.SEVERE, e);
			throw e;
		}
	}

	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.resumenTrimestralNegociosFacultadosPorReasegurador"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldoTrimestralPorReasegurador.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
	}

	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}

	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSuscripcion() {
		return suscripcion;
	}

	public void setSuscripcion(Integer suscripcion) {
		this.suscripcion = suscripcion;
	}

	public boolean isExcluirSaldoCorredor() {
		return excluirSaldoCorredor;
	}

	public void setExcluirSaldoCorredor(boolean excluirSaldoCorredor) {
		this.excluirSaldoCorredor = excluirSaldoCorredor;
	}
}
