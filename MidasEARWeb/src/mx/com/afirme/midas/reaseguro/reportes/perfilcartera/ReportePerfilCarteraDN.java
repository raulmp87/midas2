package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ReportePerfilCarteraDN {
	
	private static final ReportePerfilCarteraDN INSTANCIA = new ReportePerfilCarteraDN();
	
	private ReportePerfilCarteraDN(){	
	}
	
	public static ReportePerfilCarteraDN getInstancia(){
		return INSTANCIA;
	}
	
	public List<RegistroPerfilCarteraDTO> obtenerRegistrosPerfilCartera(
			Date fechaInicial, Date fechaFinal, Double tipoCambio,Integer tipoReporte, String nombreUsuario) throws SystemException, Exception{
		return new ReportePerfilCarteraSN().obtenerRegistrosPerfilCartera(fechaInicial, fechaFinal, tipoCambio,tipoReporte, nombreUsuario);
	}
	
	public List<AgrupacionSubRamoPerfilCarteraDTO> obtenerRegistrosAgrupadosPerfilCartera(
			Date fechaInicial, Date fechaFinal, Double tipoCambio,Integer tipoReporte, String nombreUsuario) throws SystemException, Exception{
		return new ReportePerfilCarteraSN().obtenerRegistrosAgrupadosPerfilCartera(fechaInicial, fechaFinal, tipoCambio, tipoReporte, nombreUsuario);
	}
}
