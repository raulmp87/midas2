package mx.com.afirme.midas.reaseguro.reportes.egresos;

import java.math.BigDecimal;
import java.util.ArrayList;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * 
 * @author jose luis arellano
 *
 */
public class ReporteSoporteOrdenPago extends MidasReporteBase {
	private BigDecimal idEgresoReaseguro;
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		PL014_SoporteOrdenPago plantillaSoporteOrdenPago = new PL014_SoporteOrdenPago();
		
		plantillaSoporteOrdenPago.setIdEgresoReaseguro(idEgresoReaseguro);
		setListaPlantillas(new ArrayList<byte[]>());
		
		getListaPlantillas().add(plantillaSoporteOrdenPago.obtenerReporte(claveUsuario));
		
		return super.obtenerReporte(claveUsuario, false);
	}

	public BigDecimal getIdEgresoReaseguro() {
		return idEgresoReaseguro;
	}

	public void setIdEgresoReaseguro(BigDecimal idEgresoReaseguro) {
		this.idEgresoReaseguro = idEgresoReaseguro;
	}
}
