package mx.com.afirme.midas.reaseguro.reportes.saldos;

import java.util.Date;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;

/**
 * @author Jose Luis Arellano
 *
 */
public abstract class PlantillaMovimientosSaldoBase extends MidasPlantillaBase {

	protected Date fechaInicial;
	protected Date fechaFinal;
	protected Double idMoneda;
	protected boolean soloFacultativos;
	protected boolean soloAutomaticos;
	protected boolean incluirRetencion;
	
	public PlantillaMovimientosSaldoBase(){
		this.incluirRetencion = false;
	}

	protected boolean parametrosReporteValidos(){
		boolean valido = false;
		valido = (fechaInicial != null && fechaFinal != null &&
				fechaInicial.before(fechaFinal) && 
				idMoneda != null && 
				(idMoneda.intValue() == Sistema.MONEDA_PESOS || idMoneda.intValue() == Sistema.MONEDA_DOLARES) &&
				(soloFacultativos || soloAutomaticos));
		return valido;
	}

	protected void poblarParametrosComunes(){
		String descripcionMoneda = "";
		if(idMoneda.intValue() == Sistema.MONEDA_DOLARES)
			descripcionMoneda = "DOLARES US";
		else if (idMoneda.intValue() == Sistema.MONEDA_PESOS)
			descripcionMoneda = "PESOS";
		
		String descripcionReporte = "";
		
		if(soloFacultativos)
			descripcionReporte = "Saldo por p�lizas incluyendo s�lo la parte facultativa.";
		else if(soloAutomaticos){
			descripcionReporte = "Saldo por p�lizas incluyendo s�lo contratos autom�ticos.";
		}
		
		getParametrosVariablesReporte().put("FECHA_INICIAL", fechaInicial);
		getParametrosVariablesReporte().put("FECHA_FINAL", fechaFinal);
		getParametrosVariablesReporte().put("DESCRIPCION_MONEDA", descripcionMoneda);
		getParametrosVariablesReporte().put("DESCRIPCION_REPORTE", descripcionReporte);
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public Double getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Double idMoneda) {
		this.idMoneda = idMoneda;
	}
	public boolean isSoloFacultativos() {
		return soloFacultativos;
	}
	public void setSoloFacultativos(boolean soloFacultativos) {
		this.soloFacultativos = soloFacultativos;
	}
	public boolean isSoloAutomaticos() {
		return soloAutomaticos;
	}
	public void setSoloAutomaticos(boolean soloAutomaticos) {
		this.soloAutomaticos = soloAutomaticos;
	}
	public boolean isIncluirRetencion() {
		return incluirRetencion;
	}
	public void setIncluirRetencion(boolean incluirRetencion) {
		this.incluirRetencion = incluirRetencion;
	}
}
