package mx.com.afirme.midas.reaseguro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteMovimientoSiniestroConReaseguroForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	private String parametrosReporte;
	private String numeroSiniestro;
	private String movimiento;
	private String nombre;
	private String idtcReaseguradorCorredor;
	
	public String getParametrosReporte() {
		return parametrosReporte;
	}
	public void setParametrosReporte(String parametrosReporte) {
		this.parametrosReporte = parametrosReporte;
	}	
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}
	public String getMovimiento() {
		return movimiento;
	}
	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}
	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}
}
