package mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.subramo.SubRamoDN;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.presentacion.PresentacionEstadoCuentaDN;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteEstadoCuentaDN;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteReaseguradorEstadoCuentaDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.danios.reportes.cotizacion.generica.DatoIncisoCotizacionForm;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.PlantillaEstadoCuentaBase;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author Jose Luis Arellano
 */
public abstract class ReporteEstadoCuentaBase extends MidasReporteBase{

	protected HashMap<String, String> parametrosConfiguracion;
	protected EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
	protected List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecoradoDTO;
	protected String tituloEstadoCuenta;
	protected String tipoReporte;
	
	public ReporteEstadoCuentaBase(){
		parametrosConfiguracion = new HashMap<String, String>();
	}
	
	protected PlantillaEstadoCuentaBase obtenerPlantillaEstadoCuentaFacultativo(){
		String idToPolizaStr = parametrosConfiguracion.get("idPoliza");
		String idPolizaSubRamo = parametrosConfiguracion.get("idPolizaSubRamo");
		
		String idReporteSiniestro = parametrosConfiguracion.get("idReporteSiniestro");
		String idSiniestroSubRamo = parametrosConfiguracion.get("idSiniestroSubRamo");
		
		String hasta = parametrosConfiguracion.get("hasta");
		String tipoConsulta = parametrosConfiguracion.get("tipoConsulta");
		String idReasegurador = parametrosConfiguracion.get("idReasegurador");
		
		String idMoneda = parametrosConfiguracion.get("idMoneda");
		String idSubRamo = parametrosConfiguracion.get("idSubRamo");
		String idSiniestro = parametrosConfiguracion.get("idSiniestro");
		String idPoliza = parametrosConfiguracion.get("idPoliza");
		
		BigDecimal idTcReasegurador = obtenerBigDecimal(idReasegurador);
		BigDecimal idMonedaBigDecimal = obtenerBigDecimal(idMoneda);
		BigDecimal idSubRamoBigDecimal = obtenerBigDecimal(idSubRamo);
		BigDecimal idToPoliza = obtenerBigDecimal(idPoliza);
		BigDecimal idSiniestroBigDecimal = obtenerBigDecimal(idSiniestro);
		
		if(idSiniestroBigDecimal == null)
			idSiniestroBigDecimal = obtenerBigDecimal(idReporteSiniestro);
		
		if(idToPoliza == null)
			idToPoliza = obtenerBigDecimal(idToPolizaStr);
		
		return obtenerPlantillaEstadoCuentaFacultativo(idTcReasegurador, idPolizaSubRamo, idSiniestroSubRamo, 
				tipoConsulta, hasta, idMonedaBigDecimal, idSubRamoBigDecimal, idSiniestroBigDecimal, idToPoliza);
	}
	
	public List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosEstadoCuentaFacultativo(boolean incluirSoloFacultativo,String nombreUsuario){
		String idToPolizaStr = parametrosConfiguracion.get("idPoliza");
		String idPolizaSubRamo = parametrosConfiguracion.get("idPolizaSubRamo");
		
		String idReporteSiniestro = parametrosConfiguracion.get("idReporteSiniestro");
		String idSiniestroSubRamo = parametrosConfiguracion.get("idSiniestroSubRamo");
		
		String hasta = parametrosConfiguracion.get("hasta");
		String tipoConsulta = parametrosConfiguracion.get("tipoConsulta");
		String idReasegurador = parametrosConfiguracion.get("idReasegurador");
		
		String idMoneda = parametrosConfiguracion.get("idMoneda");
		String idSubRamo = parametrosConfiguracion.get("idSubRamo");
		String idSiniestro = parametrosConfiguracion.get("idSiniestro");
		String idPoliza = parametrosConfiguracion.get("idPoliza");
		
		BigDecimal idTcReasegurador = obtenerBigDecimal(idReasegurador);
		BigDecimal idMonedaBigDecimal = obtenerBigDecimal(idMoneda);
		BigDecimal idSubRamoBigDecimal = obtenerBigDecimal(idSubRamo);
		BigDecimal idToPoliza = obtenerBigDecimal(idPoliza);
		BigDecimal idSiniestroBigDecimal = obtenerBigDecimal(idSiniestro);
		
		if(idSiniestroBigDecimal == null)
			idSiniestroBigDecimal = obtenerBigDecimal(idReporteSiniestro);
		
		if(idToPoliza == null)
			idToPoliza = obtenerBigDecimal(idToPolizaStr);
		
		List<ReporteReaseguradorEstadoCuentaDTO> listaReaseguradores = new ArrayList<ReporteReaseguradorEstadoCuentaDTO>();
		try {
			listaReaseguradores = obtenerMovimientosEstadoCuentaFacultativo(idTcReasegurador, idPolizaSubRamo, idSiniestroSubRamo, tipoConsulta,
					hasta, idMonedaBigDecimal, idSubRamoBigDecimal, idSiniestroBigDecimal, idToPoliza, incluirSoloFacultativo, nombreUsuario);
			
		} catch (Exception e) {
			
		}
		
		return listaReaseguradores;
	}
	
	private List<ReporteReaseguradorEstadoCuentaDTO> obtenerMovimientosEstadoCuentaFacultativo(BigDecimal idTcReasegurador,
			String idPolizaSubRamo,String idSiniestroSubRamo,
			String tipoConsulta,String hasta,
			BigDecimal idMoneda,BigDecimal idSubRamo,
			BigDecimal idSiniestro,BigDecimal idToPoliza,boolean incluirSoloFacultativo,
			String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		
		List<ReporteReaseguradorEstadoCuentaDTO> listaReaseguradores = null;
		
		Integer mes=null,anio=null;
		Integer [] mesYAnio = obtenerMesAnio(hasta);
		if(mesYAnio != null){
			mes= mesYAnio[0];
			anio= mesYAnio[1];
		}
		
		/*
		 * Consulta de saldos por p�liza y subramo(s)
		 */
		if(!UtileriasWeb.esCadenaVacia(idPolizaSubRamo)){
			String[] idArray = idPolizaSubRamo.split(",");
			BigDecimal idToPolizaTMP = null;
			BigDecimal[] idTcSubRamo = new BigDecimal[idArray.length];
			for(int i=0;i<idArray.length;i++){
				String[] idAnidado = idArray[i].split("_");
				if(i==0){
					idToPolizaTMP = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
				}
				idTcSubRamo[i] = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
			}
			listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorPagar(idToPolizaTMP, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
		}
		/*
		 * Consulta de estado de cuenta por reasegurador
		 */
		else if(!UtileriasWeb.esCadenaVacia(tipoConsulta) && idTcReasegurador != null){
			BigDecimal []idTcSubRamo = null;
			if(idSubRamo != null){
				idTcSubRamo = new BigDecimal[1];
				idTcSubRamo[0] = idSubRamo;
			}
			if (tipoConsulta.equalsIgnoreCase("saldoTecnico")){
				if(idMoneda != null){
					//TODO no es cuenta por pagar ni cuenta por cobrar
					listaReaseguradores = null;
//					listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorPagar(null, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
				}
			}
			else if (tipoConsulta.equalsIgnoreCase("saldoTotalSiniestros")){
				if(idMoneda != null){
					listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorCobrar(null, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoTotalPolizas")){
				if(idMoneda != null){
					listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorPagar(null, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorSiniestro")){
				if(idSiniestro != null && idMoneda != null){//Parametros opcionales: idSubRamo, hasta
					listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorCobrar(idSiniestro, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorPoliza")){//Par�metros opcionales: idSubRamo
				if(idToPoliza != null && idMoneda != null){
					listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorPagar(idToPoliza, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
				}
			}
		}
		/*
		 * Consulta de saldos por p�liza incluyendo todos sus subramos.
		 */
		else if(idToPoliza != null){
			listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorPagar(idToPoliza, null, null, null, mes, anio,incluirSoloFacultativo);
		}
		/*
		 * Consulta de saldos por siniestro y subramo(s)
		 */
		else if(!UtileriasWeb.esCadenaVacia(idSiniestroSubRamo)){
			String[] idArray = idSiniestroSubRamo.split(",");
			BigDecimal idToReporteSiniestro = null;
			BigDecimal[] idSubRamosPorIncluir = new BigDecimal[idArray.length];
			if(idArray.length > 0){
				for(int i=0;i<idArray.length;i++){
					String[] idAnidado = idArray[i].split("_");
					if(i==0){
						idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
					}
					idSubRamosPorIncluir[i] = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
				}
			}
			ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idToReporteSiniestro);
			if(siniestro != null){
				listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorCobrar(idToReporteSiniestro, null, null, idSubRamosPorIncluir, mes, anio,incluirSoloFacultativo);
			}
		}
		/*
		 * Consulta de saldos por siniestro
		 */
		else if (idSiniestro != null){
			ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idSiniestro);
			if(siniestro != null){
				PolizaSoporteDanosDTO poliza = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(siniestro.getNumeroPoliza());
//				estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaPorSiniestro(siniestro.getIdToReporteSiniestro(),
//						poliza.getIdMoneda(), null, mes, anio, separarSaldosPorConcepto);
				listaReaseguradores = ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorCobrar(idSiniestro, null, poliza.getIdMoneda(), null, mes, anio,incluirSoloFacultativo);
			}
		}
		return listaReaseguradores;
	}
	
	private PlantillaEstadoCuentaBase obtenerPlantillaEstadoCuentaFacultativo(BigDecimal idTcReasegurador,
			String idPolizaSubRamo,String idSiniestroSubRamo,
			String tipoConsulta,String hasta,
			BigDecimal idMoneda,BigDecimal idSubRamo,
			BigDecimal idSiniestro,BigDecimal idToPoliza) {
		
		PlantillaEstadoCuentaBase plantilla = null;
		
		/*
		 * Consulta de saldos por p�liza y subramo(s)
		 */
		if(!UtileriasWeb.esCadenaVacia(idPolizaSubRamo)){
			plantilla = new PL004_MovimientosCuentaPorPagar(getMapaParametrosGeneralesPlantillas());
		}
		/*
		 * Consulta de estado de cuenta por reasegurador
		 */
		else if(!UtileriasWeb.esCadenaVacia(tipoConsulta) && idTcReasegurador != null){
			if (tipoConsulta.equalsIgnoreCase("saldoTecnico")){
				if(idMoneda != null){
					//TODO no es cuenta por pagar ni cuenta por cobrar
					plantilla = null;
				}
			}
			else if (tipoConsulta.equalsIgnoreCase("saldoTotalSiniestros")){
				if(idMoneda != null){
					plantilla = new PL005_MovimientosCuentaPorCobrar(getMapaParametrosGeneralesPlantillas());
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoTotalPolizas")){
				if(idMoneda != null){
					plantilla = new PL004_MovimientosCuentaPorPagar(getMapaParametrosGeneralesPlantillas());
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorSiniestro")){
				if(idSiniestro != null && idMoneda != null){//Parametros opcionales: idSubRamo, hasta
					plantilla = new PL005_MovimientosCuentaPorCobrar(getMapaParametrosGeneralesPlantillas());
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorPoliza")){//Par�metros opcionales: idSubRamo
				if(idToPoliza != null && idMoneda != null){
					plantilla = new PL004_MovimientosCuentaPorPagar(getMapaParametrosGeneralesPlantillas());
				}
			}
		}
		/*
		 * Consulta de saldos por p�liza incluyendo todos sus subramos.
		 */
		else if(idToPoliza != null){
			plantilla = new PL004_MovimientosCuentaPorPagar(getMapaParametrosGeneralesPlantillas());
		}
		/*
		 * Consulta de saldos por siniestro y subramo(s)
		 */
		else if(!UtileriasWeb.esCadenaVacia(idSiniestroSubRamo)){
				plantilla = new PL005_MovimientosCuentaPorCobrar(getMapaParametrosGeneralesPlantillas());
		}
		/*
		 * Consulta de saldos por siniestro
		 */
		else if (idSiniestro != null){
				plantilla = new PL005_MovimientosCuentaPorCobrar(getMapaParametrosGeneralesPlantillas());
		}
		return plantilla;
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuenta(String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		String idEstadoCuenta = parametrosConfiguracion.get("idEstadoCuenta");
		String mostrarEjerciciosAnteriores = parametrosConfiguracion.get("mostrarEjerciciosAnteriores");
		String idToPolizaStr = parametrosConfiguracion.get("idPoliza");
		String idPolizaSubRamo = parametrosConfiguracion.get("idPolizaSubRamo");
		
		String idReporteSiniestro = parametrosConfiguracion.get("idReporteSiniestro");
		String idSiniestroSubRamo = parametrosConfiguracion.get("idSiniestroSubRamo");
		
		String separarPorReaseguradorStr = parametrosConfiguracion.get("separarPorReasegurador");
		String hasta = parametrosConfiguracion.get("hasta");
		String tipoConsulta = parametrosConfiguracion.get("tipoConsulta");
		String idReasegurador = parametrosConfiguracion.get("idReasegurador");
		
		String idMoneda = parametrosConfiguracion.get("idMoneda");
		String idSubRamo = parametrosConfiguracion.get("idSubRamo");
		String idSiniestro = parametrosConfiguracion.get("idSiniestro");
		String idPoliza = parametrosConfiguracion.get("idPoliza");
		
		boolean separarSaldosPorConcepto = (!UtileriasWeb.esCadenaVacia(separarPorReaseguradorStr) && separarPorReaseguradorStr.equals("true"));
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		
//		BigDecimal idEstadoCuentaBigDecimal = obtenerBigDecimal(idEstadoCuenta);
		BigDecimal idTcReasegurador = obtenerBigDecimal(idReasegurador);
		BigDecimal idMonedaBigDecimal = obtenerBigDecimal(idMoneda);
		BigDecimal idSubRamoBigDecimal = obtenerBigDecimal(idSubRamo);
		BigDecimal idToPoliza = obtenerBigDecimal(idPoliza);
		BigDecimal idSiniestroBigDecimal = obtenerBigDecimal(idSiniestro);
		
		if(idSiniestroBigDecimal == null)
			idSiniestroBigDecimal = obtenerBigDecimal(idReporteSiniestro);
		
		if(idToPoliza == null)
			idToPoliza = obtenerBigDecimal(idToPolizaStr);
		
		Boolean mostrarEjerciciosAnterioresBoolean = null;
		if(!UtileriasWeb.esCadenaVacia(mostrarEjerciciosAnteriores)){
			try{
				mostrarEjerciciosAnterioresBoolean = Boolean.valueOf(mostrarEjerciciosAnteriores);
			}catch(Exception e){}
		}
		
		
		estadoCuentaDecoradoDTO = obtenerEstadoCuenta(idEstadoCuenta, idTcReasegurador, idPolizaSubRamo, idSiniestroSubRamo, 
				tipoConsulta, hasta, idMonedaBigDecimal, idSubRamoBigDecimal, idSiniestroBigDecimal, idToPoliza, separarSaldosPorConcepto, mostrarEjerciciosAnterioresBoolean, nombreUsuario);
		
		return estadoCuentaDecoradoDTO;
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuenta(String idEstadoCuenta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerEstadoCuenta(idEstadoCuenta, null, null, null, null, null, null, null, null, null, false, null, nombreUsuario);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorPolizaSubRamo(String idPolizaSubRamo,String hasta,boolean separarConceptosPorReasegurador,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerEstadoCuenta(null, null, idPolizaSubRamo, null, null, hasta, null, null, null, null, separarConceptosPorReasegurador, null, nombreUsuario);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorReasegurador(String tipoConsulta,BigDecimal idMoneda,BigDecimal idTcReasegurador,
			BigDecimal idToPoliza,BigDecimal idSiniestro,BigDecimal idTcSubRamo,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerEstadoCuenta(null, idTcReasegurador, null, null, tipoConsulta, hasta, idMoneda, idTcSubRamo, idSiniestro, idToPoliza, false, null, nombreUsuario);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorPoliza(BigDecimal idToPoliza,boolean separarConceptosPorReasegurador,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerEstadoCuenta(null, null, null, null, null, hasta, null, null, null, idToPoliza, separarConceptosPorReasegurador, null, nombreUsuario);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorSiniestroSubRamos(String idSiniestroSubRamos,boolean separarConceptosPorReasegurador,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerEstadoCuenta(null, null, null, idSiniestroSubRamos, null, hasta, null, null, null, null, separarConceptosPorReasegurador, null, nombreUsuario);
	}
	
	public EstadoCuentaDecoradoDTO obtenerEstadoCuentaPorSiniestro(BigDecimal idSiniestro,boolean separarConceptosPorReasegurador,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerEstadoCuenta(null, null, null, null, null, hasta, null, null, idSiniestro, null, separarConceptosPorReasegurador, null, nombreUsuario);
	}
	
	
	private EstadoCuentaDecoradoDTO obtenerEstadoCuenta(String idEstadoCuenta,BigDecimal idTcReasegurador,
			String idPolizaSubRamo,String idSiniestroSubRamo,
			String tipoConsulta,String hasta,
			BigDecimal idMoneda,BigDecimal idSubRamo,
			BigDecimal idSiniestro,BigDecimal idToPoliza,
			boolean separarSaldosPorConcepto,Boolean mostrarEjerciciosAnteriores,
			String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		Integer mes=null,anio=null;
		if(!UtileriasWeb.esCadenaVacia(hasta)){
			try{
				String[] datosHasta = hasta.split("/");
				mes=new Integer(datosHasta[0]);
				anio = new Integer(datosHasta[1]);
			}catch(Exception e){
				mes = anio = null;
			}
		}
		EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = null;
		if(idEstadoCuenta != null){
//			if(mostrarEjerciciosAnteriores != null && mostrarEjerciciosAnteriores){
//				estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaHistoricoCombinado(idEstadoCuenta, nombreUsuario);
			try{
				estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaHistoricoCombinado(UtileriasWeb.regresaBigDecimal(idEstadoCuenta));
			}
			catch(Exception e){
				String[] ids = idEstadoCuenta.split("_");
				BigDecimal idTcSubRamo = UtileriasWeb.regresaBigDecimal(ids[0]);
				BigDecimal idTcReasegurador1 = UtileriasWeb.regresaBigDecimal(ids[1]);
//				Integer tipoReaseguro = new Integer(ids[2]);
				Integer ejercicio = new Integer(ids[3]);
				Integer suscripcion = new Integer(ids[4]);
				BigDecimal idMoneda1 = UtileriasWeb.regresaBigDecimal(ids[5]);
				
				estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaFacultativoPorSuscripcion(idTcReasegurador1,idMoneda1,idTcSubRamo,ejercicio,suscripcion);
			}
//			}
//			else{
//				estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaConSaldosCalculados(idEstadoCuenta);
//			}
		}
		/*
		 * Consulta de saldos por p�liza y subramo(s)
		 */
		else if(!UtileriasWeb.esCadenaVacia(idPolizaSubRamo)){
			String[] idArray = idPolizaSubRamo.split(",");
			BigDecimal idToPolizaTMP = null;
			BigDecimal[] idTcSubRamo = new BigDecimal[idArray.length];
			BigDecimal[] idTmContratoFacultativo = new BigDecimal[idArray.length];
			for(int i=0;i<idArray.length;i++){
				String[] idAnidado = idArray[i].split("_");
				if(i==0){
					idToPolizaTMP = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
				}
				idTcSubRamo[i] = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
				idTmContratoFacultativo[i] = UtileriasWeb.regresaBigDecimal(idAnidado[2]);
			}
			estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().
									obtenerEstadoCuentaCombinadoConSaldosCalculados(
											idToPolizaTMP, idTcSubRamo,idTmContratoFacultativo,mes,anio,separarSaldosPorConcepto);
		}
		/*
		 * Consulta de estado de cuenta por reasegurador
		 */
		else if(!UtileriasWeb.esCadenaVacia(tipoConsulta) && idTcReasegurador != null){
			if (tipoConsulta.equalsIgnoreCase("saldoTecnico")){
				if(idMoneda != null){
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaSaldoTecnicoPorReasegurador(idTcReasegurador, idMoneda, idSubRamo, mes, anio);
				}
			}
			else if (tipoConsulta.equalsIgnoreCase("saldoTotalSiniestros")){
				if(idMoneda != null){
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaSaldoSiniestroPorReasegurador(idTcReasegurador, null, idMoneda, idSubRamo, mes, anio);
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoTotalPolizas")){
				if(idMoneda != null){
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaSaldoPolizaPorReasegurador(idTcReasegurador,null,idMoneda, idSubRamo, mes, anio);
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorSiniestro")){
				if(idSiniestro != null && idMoneda != null){//Parametros opcionales: idSubRamo, hasta
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaSaldoSiniestroPorReasegurador(idTcReasegurador, idSiniestro, idMoneda, idSubRamo, mes, anio);
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorPoliza")){//Par�metros opcionales: idSubRamo
				if(idToPoliza != null && idMoneda != null){
					estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaSaldoPolizaPorReasegurador(idTcReasegurador,idToPoliza,idMoneda, idSubRamo, mes, anio);
				}
			}
		}
		/*
		 * Consulta de saldos por p�liza incluyendo todos sus subramos.
		 */
		else if(idToPoliza != null){
			estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaConSaldosCalculados(idToPoliza, mes, anio, separarSaldosPorConcepto);
		}
		/*
		 * Consulta de saldos por siniestro y subramo(s)
		 */
		else if(!UtileriasWeb.esCadenaVacia(idSiniestroSubRamo)){
			String[] idArray = idSiniestroSubRamo.split(",");
			BigDecimal idToReporteSiniestro = null;
			BigDecimal[] idSubRamosPorIncluir = new BigDecimal[idArray.length];
			if(idArray.length > 0){
				for(int i=0;i<idArray.length;i++){
					String[] idAnidado = idArray[i].split("_");
					if(i==0){
						idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
					}
					idSubRamosPorIncluir[i] = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
				}
			}
			ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idToReporteSiniestro);
			if(siniestro != null){
				PolizaSoporteDanosDTO poliza = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(siniestro.getNumeroPoliza());
				estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaPorSiniestro(idToReporteSiniestro, poliza.getIdMoneda(),idSubRamosPorIncluir
						,mes,anio,separarSaldosPorConcepto);
			}
		}
		/*
		 * Consulta de saldos por siniestro
		 */
		else if (idSiniestro != null){
			ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idSiniestro);
			if(siniestro != null){
				PolizaSoporteDanosDTO poliza = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(siniestro.getNumeroPoliza());
				estadoCuentaDecoradoDTO = PresentacionEstadoCuentaDN.getInstancia().obtenerEstadoCuentaPorSiniestro(siniestro.getIdToReporteSiniestro(),
						poliza.getIdMoneda(), null, mes, anio, separarSaldosPorConcepto);
			}
		}
		return estadoCuentaDecoradoDTO;
	}
	
	public List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuenta(String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		String idToPolizaStr = parametrosConfiguracion.get("idPoliza");
		String idPolizaSubRamo = parametrosConfiguracion.get("idPolizaSubRamo");
		
		String idReporteSiniestro = parametrosConfiguracion.get("idReporteSiniestro");
		String idSiniestroSubRamo = parametrosConfiguracion.get("idSiniestroSubRamo");
		
		String hasta = parametrosConfiguracion.get("hasta");
		String tipoConsulta = parametrosConfiguracion.get("tipoConsulta");
		String idReasegurador = parametrosConfiguracion.get("idReasegurador");
		
		String idMoneda = parametrosConfiguracion.get("idMoneda");
		String idSubRamo = parametrosConfiguracion.get("idSubRamo");
		String idSiniestro = parametrosConfiguracion.get("idSiniestro");
		String idPoliza = parametrosConfiguracion.get("idPoliza");
		
		List<DatoIncisoCotizacionForm> listaDatosEstadoCuenta = null;
		
		BigDecimal idTcReasegurador = obtenerBigDecimal(idReasegurador);
		BigDecimal idTcMoneda = obtenerBigDecimal(idMoneda);
		BigDecimal idTcSubRamo = obtenerBigDecimal(idSubRamo);
		BigDecimal idToPoliza = obtenerBigDecimal(idPoliza);
		BigDecimal idToSiniestro = obtenerBigDecimal(idSiniestro);
		
		if(idToPoliza == null)
			idToPoliza = obtenerBigDecimal(idToPolizaStr);
		
		if(idToSiniestro == null)
			idToSiniestro = obtenerBigDecimal(idReporteSiniestro);
		
		listaDatosEstadoCuenta = obtenerDatosEstadoCuenta1(idTcReasegurador, idPolizaSubRamo, idSiniestroSubRamo, tipoConsulta, hasta, idTcMoneda, idTcSubRamo, idToSiniestro, idToPoliza, nombreUsuario);
		
		return listaDatosEstadoCuenta;
	}
	
	public List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuentaPorPolizaSubRamo(String idPolizaSubRamo,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerDatosEstadoCuenta1(null, idPolizaSubRamo, null, null, hasta, null, null, null, null, nombreUsuario);
	}
	
	public List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuentaPorReasegurador(String tipoConsulta,BigDecimal idMoneda,BigDecimal idTcReasegurador,
			BigDecimal idToPoliza,BigDecimal idSiniestro,BigDecimal idTcSubRamo,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerDatosEstadoCuenta1(idTcReasegurador, null, null, tipoConsulta, hasta, idMoneda, idTcSubRamo, idSiniestro, idToPoliza, nombreUsuario);
	}
	
	public List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuentaPorPoliza(BigDecimal idToPoliza,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerDatosEstadoCuenta1(null, null, null, null, hasta, null, null, null, idToPoliza, nombreUsuario);
	}
	
	public List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuentaPorSiniestroSubRamos(String idSiniestroSubRamos,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerDatosEstadoCuenta1(null, null, idSiniestroSubRamos, null, hasta, null, null, null, null, nombreUsuario);
	}
	
	public List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuentaPorSiniestro(BigDecimal idSiniestro,String hasta,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		return obtenerDatosEstadoCuenta1(null, null, null, null, hasta, null, null, idSiniestro, null, nombreUsuario);
	}
	
	protected List<DatoIncisoCotizacionForm> obtenerDatosEstadoCuenta1(BigDecimal idTcReasegurador,
			String idPolizaSubRamo,String idSiniestroSubRamo,String tipoConsulta,String hasta,
			BigDecimal idMoneda,BigDecimal idSubRamo,BigDecimal idSiniestro,BigDecimal idToPoliza,
			String nombreUsuario) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		List<DatoIncisoCotizacionForm> listaDatosEstadoCuenta = new ArrayList<DatoIncisoCotizacionForm>();
		/*
		 * Consulta de saldos por p�liza y subramo(s)
		 */
		if(!UtileriasWeb.esCadenaVacia(idPolizaSubRamo)){
			String[] idArray = idPolizaSubRamo.split(",");
			BigDecimal idToPolizaTMP = null;
			BigDecimal[] idTcSubRamo = new BigDecimal[idArray.length];
			
			for(int i=0;i<idArray.length;i++){
				String[] idAnidado = idArray[i].split("_");
				if(i==0){
					idToPolizaTMP = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
				}
				idTcSubRamo[i] = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
			}
			listaDatosEstadoCuenta = obtenerDatosIncisoConsultaPorPolizaSubramos(idToPolizaTMP, idTcSubRamo,true, nombreUsuario);
			tituloEstadoCuenta = "Estado de cuenta facultativo por p�liza";
		}
		/*
		 * Consulta de estado de cuenta por reasegurador
		 */
		else if(!UtileriasWeb.esCadenaVacia(tipoConsulta) && idTcReasegurador != null){
			if (tipoConsulta.equalsIgnoreCase("saldoTecnico")){
				if(idMoneda != null){
					listaDatosEstadoCuenta = obtenerEstadoCuentaSaldoTecnicoPorReasegurador(idTcReasegurador, idMoneda, idSubRamo);
					tituloEstadoCuenta = "Estado de cuenta facultativo por reasegurador (saldo t�cnico)";
				}
			}
			else if (tipoConsulta.equalsIgnoreCase("saldoTotalSiniestros")){
				if(idMoneda != null){
					listaDatosEstadoCuenta = obtenerEstadoCuentaSaldoTecnicoPorReasegurador(idTcReasegurador, idMoneda, idSubRamo);
					tituloEstadoCuenta = "Estado de cuenta facultativo por reasegurador (CxC)";
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoTotalPolizas")){
				if(idMoneda != null){
					listaDatosEstadoCuenta = obtenerEstadoCuentaSaldoTecnicoPorReasegurador(idTcReasegurador, idMoneda, idSubRamo);
					tituloEstadoCuenta = "Estado de cuenta facultativo por reasegurador (CxP)";
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorSiniestro")){
				if(idSiniestro != null && idMoneda != null){//Parametros opcionales: idSubRamo, hasta
					BigDecimal[] idTcSubRamos = null;
					if (idSubRamo != null){
						idTcSubRamos = new BigDecimal[1];
						idTcSubRamos[0] = idSubRamo;
					}
					
					listaDatosEstadoCuenta = obtenerEstadoCuentaSaldoTecnicoPorReasegurador(idTcReasegurador, idMoneda, idSubRamo);
					ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idSiniestro);
					if(siniestro != null){
						listaDatosEstadoCuenta.addAll( obtenerDatosConsultaPorSiniestro(siniestro,false, nombreUsuario) );
					}
					tituloEstadoCuenta = "Estado de cuenta facultativo por reasegurador y siniestro";
				}
			}
			else if(tipoConsulta.equalsIgnoreCase("saldoPorPoliza")){//Par�metros opcionales: idSubRamo
				if( idToPoliza != null && idMoneda != null){
					
					listaDatosEstadoCuenta = obtenerEstadoCuentaSaldoTecnicoPorReasegurador(idTcReasegurador, idMoneda, idSubRamo);
					listaDatosEstadoCuenta.addAll( obtenerDatosIncisoConsultaPorPolizaSubramos(idToPoliza, null,false, nombreUsuario) );
					
					tituloEstadoCuenta = "Estado de cuenta facultativo por reasegurador y p�liza";
				}
			}
		}
		/*
		 * Consulta de saldos por p�liza incluyendo todos sus subramos.
		 */
		else if(idToPoliza != null){
			listaDatosEstadoCuenta = obtenerDatosIncisoConsultaPorPolizaSubramos(idToPoliza, null,true, nombreUsuario);
			tituloEstadoCuenta = "Estado de cuenta facultativo por p�liza";
		}
		/*
		 * Consulta de saldos por siniestro y subramo(s)
		 */
		else if(!UtileriasWeb.esCadenaVacia(idSiniestroSubRamo)){
			String[] idArray = idSiniestroSubRamo.split(",");
			BigDecimal idToReporteSiniestro = null;
			BigDecimal[] idSubRamosPorIncluir = new BigDecimal[idArray.length];
			if(idArray.length > 0){
				for(int i=0;i<idArray.length;i++){
					String[] idAnidado = idArray[i].split("_");
					if(i==0){
						idToReporteSiniestro = UtileriasWeb.regresaBigDecimal(idAnidado[0]);
					}
					idSubRamosPorIncluir[i] = UtileriasWeb.regresaBigDecimal(idAnidado[1]);
				}
			}
			ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idToReporteSiniestro);
			if(siniestro != null){
				listaDatosEstadoCuenta = obtenerDatosConsultaPorSiniestro(siniestro,true, nombreUsuario);
				agregarDatoSubRamos(listaDatosEstadoCuenta, idSubRamosPorIncluir);
			}
			tituloEstadoCuenta = "Estado de cuenta facultativo por siniestro";
		}
		/*
		 * Consulta de saldos por siniestro
		 */
		else if (idSiniestro != null){
			ReporteSiniestroDTO siniestro = ReporteSiniestroDN.getInstancia(nombreUsuario).desplegarReporte(idSiniestro);
			if(siniestro != null){
				listaDatosEstadoCuenta = obtenerDatosConsultaPorSiniestro(siniestro, true, nombreUsuario);
			}
			tituloEstadoCuenta = "Estado de cuenta facultativo por siniestro";
		}
		//Se agrega dato "fecha de corte", que aplica para todas las consultas.
		if(listaDatosEstadoCuenta != null){
			agregarDatoFechaCorte(listaDatosEstadoCuenta, hasta);
		}
		
		super.mapaParametrosGeneralesPlantillas.put("TITULO_ESTADO_CUENTA", tituloEstadoCuenta);
		return listaDatosEstadoCuenta;
	}
	
	protected BigDecimal obtenerBigDecimal(String dato){
		BigDecimal valor = null;
		if(!UtileriasWeb.esCadenaVacia(dato)){
			try {
				valor = UtileriasWeb.regresaBigDecimal(dato);
			} catch (SystemException e) {
			}
		}
		return valor;
	}
	
	private void agregarDatoFechaCorte(List<DatoIncisoCotizacionForm> listaDatos,String hasta){
		Integer[] mesYAnio = obtenerMesAnio(hasta);
		
		if(mesYAnio != null){
			DatoIncisoCotizacionForm datoInciso = new DatoIncisoCotizacionForm();
			datoInciso.setEtiqueta("Corte al mes");
			datoInciso.setValor(hasta);
			listaDatos.add(datoInciso);
		}
	}
	
	protected Integer[] obtenerMesAnio(String hasta){
		Integer[] mesYAnio=new Integer[2];
		if(!UtileriasWeb.esCadenaVacia(hasta)){
			try{
				String[] datosHasta = hasta.split("/");
				mesYAnio[0] = new Integer(datosHasta[0]);
				mesYAnio[1] = new Integer(datosHasta[1]);
			}catch(Exception e){
				mesYAnio = null;
			}
		}
		else	mesYAnio = null;
		return mesYAnio;
	}
	
	private List<DatoIncisoCotizacionForm> obtenerEstadoCuentaSaldoTecnicoPorReasegurador(BigDecimal idTcReasegurador, BigDecimal idTcMoneda,BigDecimal idTcSubRamo){
		List<DatoIncisoCotizacionForm> datosEdoCtaPorSiniestro = new ArrayList<DatoIncisoCotizacionForm>();
		if(idTcReasegurador != null){
			ReaseguradorCorredorDTO reasegurador;
			try {
				reasegurador = ReaseguradorCorredorDN.getInstancia().obtenerReaseguradorPorId(idTcReasegurador);
				
				agregarDatosReasegurador(datosEdoCtaPorSiniestro, reasegurador);
				
				datosEdoCtaPorSiniestro.add(obtenerDatoTipoMoneda(idTcMoneda));
				
				if(idTcSubRamo != null){
					BigDecimal[] idSubRamoArray = new BigDecimal[1];
					idSubRamoArray[0] = idTcSubRamo;
					
					agregarDatoSubRamos(datosEdoCtaPorSiniestro, idSubRamoArray);
				}
			} catch (Exception e) {
				
			}
			
		}
		return datosEdoCtaPorSiniestro;
	}
	
	private void agregarDatosReasegurador(List<DatoIncisoCotizacionForm> listaDatos,ReaseguradorCorredorDTO reaseguradorDTO){
		if(reaseguradorDTO != null){
			DatoIncisoCotizacionForm datoInciso = new DatoIncisoCotizacionForm();
			datoInciso.setEtiqueta("Reasegurador/ Corredor");
			datoInciso.setValor(reaseguradorDTO.getNombre());
			listaDatos.add(datoInciso);
			
			datoInciso = new DatoIncisoCotizacionForm();
			datoInciso.setEtiqueta("Direcci�n");
			datoInciso.setValor(reaseguradorDTO.getUbicacion());
			listaDatos.add(datoInciso);
		}
	}
	
	private List<DatoIncisoCotizacionForm> obtenerDatosConsultaPorSiniestro(ReporteSiniestroDTO siniestro,boolean agregarDatoMoneda,String nombreUsuario){
		List<DatoIncisoCotizacionForm> datosEdoCtaPorSiniestro = new ArrayList<DatoIncisoCotizacionForm>();
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		if(siniestro != null){
			datosEdoCtaPorSiniestro.add(obtenerDatoNumeroSiniestro(siniestro));
			try {
				PolizaSoporteDanosDTO poliza = SoporteDanosDN.getInstancia().getDatosGeneralesPoliza(siniestro.getNumeroPoliza());
				
				datoEstadoCuenta = new DatoIncisoCotizacionForm();
				datoEstadoCuenta.setEtiqueta("N�mero p�liza");
				datoEstadoCuenta.setValor(poliza.getNumeroPoliza());
				datosEdoCtaPorSiniestro.add(datoEstadoCuenta);
				
				datosEdoCtaPorSiniestro.add(obtenerDatoNumeroEndoso(poliza.getNumeroUltimoEndoso()));
				
				datosEdoCtaPorSiniestro.add(obtenerDatoNombreAsegurado(poliza));
				
				if(agregarDatoMoneda)
					datosEdoCtaPorSiniestro.add(obtenerDatoTipoMoneda(poliza.getIdMoneda()));
				
			} catch (Exception e) {
			}
		}
		return datosEdoCtaPorSiniestro;
	}
	
	private List<DatoIncisoCotizacionForm> obtenerDatosIncisoConsultaPorPolizaSubramos(BigDecimal idToPoliza,BigDecimal[] idTcSubRamo,boolean agregarDatoMoneda,String nombreUsuario){
		List<DatoIncisoCotizacionForm> datosEdoCtaPorPolizaSubramos = new ArrayList<DatoIncisoCotizacionForm>();
		//N�mero de p�liza
		try {
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
			EndosoDTO ultimoEndoso = EndosoDN.getInstancia(nombreUsuario).getUltimoEndoso(idToPoliza);
			
			datosEdoCtaPorPolizaSubramos.add(obtenerDatoNumeroPoliza(polizaDTO));
			datosEdoCtaPorPolizaSubramos.add(obtenerDatoNumeroEndoso(ultimoEndoso));
			
			agregarDatosVigenciaPoliza(datosEdoCtaPorPolizaSubramos, polizaDTO);
			
			datosEdoCtaPorPolizaSubramos.add(obtenerDatoNombreAsegurado(polizaDTO));
			
			if(agregarDatoMoneda)
				datosEdoCtaPorPolizaSubramos.add(obtenerDatoTipoMoneda(polizaDTO.getCotizacionDTO().getIdMoneda()));
			
		} catch (SystemException e) {
		}
		
		if(idTcSubRamo != null){
			agregarDatoSubRamos(datosEdoCtaPorPolizaSubramos, idTcSubRamo);
		}
		
		return datosEdoCtaPorPolizaSubramos;
	}
	
	private void agregarDatoSubRamos(List<DatoIncisoCotizacionForm>listaDatos,BigDecimal[] idTcSubRamo){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		if(idTcSubRamo != null){
			String descripcionSubRamo = obtenerDescripcionSubRamos(idTcSubRamo);
			datoEstadoCuenta = new DatoIncisoCotizacionForm();
			datoEstadoCuenta.setEtiqueta("Sub Ramo");
			datoEstadoCuenta.setValor(descripcionSubRamo);
			
			listaDatos.add(datoEstadoCuenta);
		}
	}
	
	
	private DatoIncisoCotizacionForm obtenerDatoNumeroSiniestro(ReporteSiniestroDTO siniestro){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("N�mero reporte siniestro");
		datoEstadoCuenta.setValor(siniestro.getNumeroReporte());
		return datoEstadoCuenta;
	}
	
	private DatoIncisoCotizacionForm obtenerDatoTipoMoneda(BigDecimal idMoneda){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("Moneda");
		String descripcionMoneda = "Pesos";
		if(idMoneda.intValue() == Sistema.MONEDA_DOLARES)
			descripcionMoneda = "D�lares";
		datoEstadoCuenta.setValor(descripcionMoneda);
		return datoEstadoCuenta;
	}
	
	private DatoIncisoCotizacionForm obtenerDatoNumeroEndoso(EndosoDTO endosoDTO){
		return obtenerDatoNumeroEndoso(endosoDTO.getId().getNumeroEndoso().intValue());
	}
	
	private DatoIncisoCotizacionForm obtenerDatoNumeroEndoso(int numeroEndoso){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("N�mero endoso");
		datoEstadoCuenta.setValor(""+numeroEndoso);
		return datoEstadoCuenta;
	}
	
	private DatoIncisoCotizacionForm obtenerDatoNombreAsegurado(PolizaDTO polizaDTO){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("Nombre Asegurado");
		datoEstadoCuenta.setValor(polizaDTO.getCotizacionDTO().getNombreAsegurado());
		return datoEstadoCuenta;
	}
	
	private DatoIncisoCotizacionForm obtenerDatoNombreAsegurado(PolizaSoporteDanosDTO poliza){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("Nombre Asegurado");
		datoEstadoCuenta.setValor(poliza.getNombreAsegurado());
		return datoEstadoCuenta;
	}
	
	private void agregarDatosVigenciaPoliza(List<DatoIncisoCotizacionForm> listaDatos,PolizaDTO polizaDTO){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("Fecha inicio");
		datoEstadoCuenta.setValor(UtileriasWeb.getFechaString(polizaDTO.getCotizacionDTO().getFechaInicioVigencia()));
		listaDatos.add(datoEstadoCuenta);
		
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("Fecha fin");
		datoEstadoCuenta.setValor(UtileriasWeb.getFechaString(polizaDTO.getCotizacionDTO().getFechaFinVigencia()));
		listaDatos.add(datoEstadoCuenta);
		
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("Fecha emisi�n");
		datoEstadoCuenta.setValor(UtileriasWeb.getFechaString(polizaDTO.getFechaCreacion()));
		listaDatos.add(datoEstadoCuenta);
	}
	
	private DatoIncisoCotizacionForm obtenerDatoNumeroPoliza(PolizaDTO polizaDTO){
		DatoIncisoCotizacionForm datoEstadoCuenta = null;
		datoEstadoCuenta = new DatoIncisoCotizacionForm();
		datoEstadoCuenta.setEtiqueta("N�mero p�liza");
		datoEstadoCuenta.setValor(UtileriasWeb.getNumeroPoliza(polizaDTO));
		return datoEstadoCuenta;
	}
	
	public String obtenerDescripcionSubRamos(BigDecimal[] idTcSubRamo){
		List<BigDecimal> listaIdTcSubRamo = new ArrayList<BigDecimal>();
		StringBuilder descripcionSubRamo = new StringBuilder("");
		for(int i=0;i<idTcSubRamo.length;i++){
			if(!listaIdTcSubRamo.contains(idTcSubRamo[i]))
				listaIdTcSubRamo.add(idTcSubRamo[i]);
		}
		for(BigDecimal idSubRamoTMP : listaIdTcSubRamo){
			SubRamoDTO subRamoDTO = null;
			try {
				subRamoDTO = SubRamoDN.getInstancia().getSubRamoPorId(idSubRamoTMP);
			} catch (Exception e) {}
			if(subRamoDTO != null){
				descripcionSubRamo.append(subRamoDTO.getDescripcionSubRamo()).append(", ");
			}
		}
		if(descripcionSubRamo.length() > 0)
			return descripcionSubRamo.substring(0, descripcionSubRamo.length()-2);
		return descripcionSubRamo.toString();
	}

	public void establecerParametroConsulta(String clave,String valor){
		parametrosConfiguracion.put(clave, valor);
	}
	
	/*
	 * Getters y setters
	 */
	public HashMap<String, String> getParametrosConfiguracion() {
		return parametrosConfiguracion;
	}
	public void setParametrosConfiguracion(
			HashMap<String, String> parametrosConfiguracion) {
		this.parametrosConfiguracion = parametrosConfiguracion;
	}
	public EstadoCuentaDecoradoDTO getEstadoCuentaDecoradoDTO() {
		return estadoCuentaDecoradoDTO;
	}
	public void setEstadoCuentaDecoradoDTO(
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO) {
		this.estadoCuentaDecoradoDTO = estadoCuentaDecoradoDTO;
	}
	public String getTituloEstadoCuenta() {
		return tituloEstadoCuenta;
	}
	public void setTituloEstadoCuenta(String tituloEstadoCuenta) {
		this.tituloEstadoCuenta = tituloEstadoCuenta;
	}
	public String getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}
	public List<EstadoCuentaDecoradoDTO> getListaEstadoCuentaDecoradoDTO() {
		return listaEstadoCuentaDecoradoDTO;
	}
	public void setListaEstadoCuentaDecoradoDTO(List<EstadoCuentaDecoradoDTO> listaEstadoCuentaDecoradoDTO) {
		this.listaEstadoCuentaDecoradoDTO = listaEstadoCuentaDecoradoDTO;
	}
}
