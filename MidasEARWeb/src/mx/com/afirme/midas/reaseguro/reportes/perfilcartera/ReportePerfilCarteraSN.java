package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

class ReportePerfilCarteraSN {

	private ReportePerfilCarteraServicios beanRemoto;
	
	ReportePerfilCarteraSN() throws SystemException{
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReportePerfilCarteraServicios.class);
		} catch (Exception e) {
			LogDeMidasWeb.log("error al instanciar bean Remoto ReportePerfilCarteraServicios", Level.WARNING, e);
			throw new SystemException(e);
		}
	}
	
	public List<RegistroPerfilCarteraDTO> obtenerRegistrosPerfilCartera(
			Date fechaInicial, Date fechaFinal, Double tipoCambio,Integer tipoReporte, String nombreUsuario) throws Exception{
		return beanRemoto.obtenerRegistrosPerfilCartera(fechaInicial, fechaFinal, tipoCambio,tipoReporte, nombreUsuario);
	}
	
	public List<AgrupacionSubRamoPerfilCarteraDTO> obtenerRegistrosAgrupadosPerfilCartera(
			Date fechaInicial, Date fechaFinal, Double tipoCambio,Integer tipoReporte, String nombreUsuario) throws Exception{
		return beanRemoto.obtenerRegistrosAgrupadosPerfilCartera(fechaInicial, fechaFinal, tipoCambio,tipoReporte, nombreUsuario);
	}
	
}
