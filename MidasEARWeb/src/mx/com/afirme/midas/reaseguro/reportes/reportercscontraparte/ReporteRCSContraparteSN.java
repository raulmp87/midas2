package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJBTransactionRolledbackException;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte.CargaContraParteDTO;

public class ReporteRCSContraparteSN {

		private ReporteRCSContraparteFacadeRemote beanRemoto;
		public static final String NO_DISPONIBLE = "noDisponible";
		public static final String EXCEPCION_OBTENER_DTO = "excepcionAlObtenerDTO";
			
		
		public ReporteRCSContraparteSN() throws SystemException {
			try {
				ServiceLocator serviceLocator = ServiceLocator.getInstance();
				beanRemoto = serviceLocator.getEJB(ReporteRCSContraparteFacadeRemote.class);
				
				
			} catch (Exception e) {
				throw new SystemException(NO_DISPONIBLE);
			}
	
			LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
		}
	
		public List<ReporteRCSContraparteDTO> obtenerReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario) {
		try {
			return beanRemoto.obtenerReporte(fechaCorte, tipoCambio, nombreUsuario);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
		
		public void procesarReporte(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario) {
			try {
				beanRemoto.procesarReporte(fechaCorte, tipoCambio, nombreUsuario);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<ReporteRCSContraparteDTO> listarTodos(String fechaCorte) {
			try {
				return beanRemoto.findAll(fechaCorte);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public ReporteRCSContraparteDTO getPorId(Integer id) {
			try {
				return beanRemoto.findById(id);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(EXCEPCION_OBTENER_DTO);
			}
		}
		
		public void borrar(ReporteRCSContraparteDTO reporteDTO) {
			try {
				beanRemoto.delete(reporteDTO);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<CargaContraParteDTO> listarCargas(String fechaCorte) {
			try {
				return beanRemoto.findAllCarga(fechaCorte);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public void actualizarEstatusCarga(int idCarga, int estatusCarga) {
			try {
				beanRemoto.actualizarEstatusCarga(idCarga, estatusCarga);
			} catch (EJBTransactionRolledbackException e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public int obtenerRegistros(Date fechaCorte) {
			try {
				return beanRemoto.obtenerRegistros(fechaCorte);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<String> obtenerArchivos(Date fechaFinal) {
			try {
				return beanRemoto.obtenerArchivos(fechaFinal);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<Object[]> obtenerDesgloce(Date fechaFinal, BigDecimal tipoCambio) {
			try {
				return beanRemoto.obtenerDesgloce(fechaFinal, tipoCambio);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<Object[]> obtenerIntegracion(Date fechaFinal, BigDecimal tipoCambio, String concepto) {
			try {
				return beanRemoto.obtenerIntegracion(fechaFinal, tipoCambio, concepto);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
		
		public List<Object[]> obtenerIntegracionNRS(Date fechaFinal, BigDecimal tipoCambio) {
			try {
				return beanRemoto.obtenerIntegracionNRS(fechaFinal, tipoCambio);
			} catch (Exception e) {
				throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
			}
		}
}
