package mx.com.afirme.midas.reaseguro.reportes.slip;

import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipSoporteDTO;

public abstract class ReporteSlipBase extends MidasReporteBase{
	
	protected SlipDTO slipDTO;
	protected SlipSoporteDTO slipSoporteDTO;
	public SlipDTO getSlipDTO() {
		return slipDTO;
	}
	public void setSlipDTO(SlipDTO slipDTO) {
		this.slipDTO = slipDTO;
	}
	public SlipSoporteDTO getSlipSoporteDTO() {
		return slipSoporteDTO;
	}
	public void setSlipSoporteDTO(SlipSoporteDTO slipSoporteDTO) {
		this.slipSoporteDTO = slipSoporteDTO;
	}
}
