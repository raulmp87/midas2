package mx.com.afirme.midas.reaseguro.reportes.egresos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.contratos.egreso.OrdenPagoDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.reaseguro.egresos.OrdenPagoDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

/**
 * @author jose luis arellano
 */
public class ReporteOrdenPago extends MidasReporteBase {
	private BigDecimal idEgresoReaseguro;
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		try {
			if(idEgresoReaseguro != null){
				List<OrdenPagoDTO> ordenPagoDTOList = OrdenPagoDN.getINSTANCIA().obtenerOrdenesPago(idEgresoReaseguro);
				
				setListaPlantillas(new ArrayList<byte[]>());
				
				if(ordenPagoDTOList != null && !ordenPagoDTOList.isEmpty()){
					for(OrdenPagoDTO ordenPagoDTO : ordenPagoDTOList){
						PL015_OrdenPago plantillaOrdenPago = new PL015_OrdenPago(ordenPagoDTO);
						
						byte[] plantilla = plantillaOrdenPago.obtenerReporte(claveUsuario);
						
						if(plantilla != null && plantilla.length > 0){
							getListaPlantillas().add(plantilla);
						}
					}
				}
				
				return super.obtenerReporte(claveUsuario, false);
			}
			else{
				throw new SystemException("No se recibi� un id de egreso v�lido: "+idEgresoReaseguro);
			}
		} catch (ExcepcionDeLogicaNegocio e) {
			throw new SystemException(e);
		}
	}

	public BigDecimal getIdEgresoReaseguro() {
		return idEgresoReaseguro;
	}

	public void setIdEgresoReaseguro(BigDecimal idEgresoReaseguro) {
		this.idEgresoReaseguro = idEgresoReaseguro;
	}
}
