package mx.com.afirme.midas.reaseguro.reportes.estadocuenta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL001_ConceptosPorEstadoCuenta extends MidasPlantillaBase{
	private BigDecimal idToEstadoCuenta;
	private EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
	private boolean mostrarConceptosDeDistintosEjercicios;
	private boolean consultarRegistrosReporte = true;
	private boolean consultarEjerciciosAnteriores = false;;
	
	public PL001_ConceptosPorEstadoCuenta(BigDecimal idToEstadoCuenta){
		inicializaDatosPlantilla();
		this.idToEstadoCuenta = idToEstadoCuenta;
	}
	
	public PL001_ConceptosPorEstadoCuenta(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO,boolean consultarSaldo){
		inicializaDatosPlantilla();
		if(estadoCuentaDecoradoDTO != null){
			this.estadoCuentaDecoradoDTO = estadoCuentaDecoradoDTO;
			this.idToEstadoCuenta = estadoCuentaDecoradoDTO.getIdEstadoCuenta();
		}
		this.consultarRegistrosReporte = consultarSaldo;
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarPlantillaEstadoCuenta(claveUsuario);
		return getByteArrayReport();
	}
	
	private void generarPlantillaEstadoCuenta(String nombreUsuario){
//		if(idToEstadoCuenta != null){
			if(estadoCuentaDecoradoDTO == null){
				try {
					this.estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaDetalle(idToEstadoCuenta, null, null);
				} catch (Exception e) {
					LogDeMidasWeb.log("", Level.WARNING, e);
				}
			}
			if(consultarRegistrosReporte){
				try {
					if(consultarEjerciciosAnteriores)
						this.estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaHistoricoCombinado(idToEstadoCuenta, nombreUsuario);
					else
						this.estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaDetalle(idToEstadoCuenta, null, null);
				} catch (Exception e) {
					LogDeMidasWeb.log("", Level.WARNING, e);
				}
			}
			if(estadoCuentaDecoradoDTO != null){
				super.getParametrosVariablesReporte().put("ID_ESTADO_CUENTA", estadoCuentaDecoradoDTO.getIdEstadoCuenta());
				super.getParametrosVariablesReporte().put("NOMBRE_REASEGURADOR", estadoCuentaDecoradoDTO.getNombreLargoReasegurador());
				super.getParametrosVariablesReporte().put("DIRECCION_REASEGURADOR", EstadoCuentaDN.getINSTANCIA().obtenerDireccionReasegurador(estadoCuentaDecoradoDTO));
				super.getParametrosVariablesReporte().put("RFC_REASEGURADOR", estadoCuentaDecoradoDTO.getReaseguradorCorredorDTO().getRfc());
				try{
					super.getParametrosVariablesReporte().put("DESCRIPCION_SUSCRIPCION", estadoCuentaDecoradoDTO.getDescripcionSuscripcion());
					super.getParametrosVariablesReporte().put("FECHA_INICIO_PERIODO", estadoCuentaDecoradoDTO.getFechaInicial());
					super.getParametrosVariablesReporte().put("FECHA_FIN_PERIODO", estadoCuentaDecoradoDTO.getFechaFinal());
				}catch(Exception e){
					super.getParametrosVariablesReporte().put("DESCRIPCION_SUSCRIPCION", estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getDescripcion());
					super.getParametrosVariablesReporte().put("FECHA_INICIO_PERIODO", estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getFechaInicial());
					super.getParametrosVariablesReporte().put("FECHA_FIN_PERIODO", estadoCuentaDecoradoDTO.getSuscripcionDTOCalculada().getFechaFinal());
				}
				super.getParametrosVariablesReporte().put("DESCRIPCION_SUBRAMO", estadoCuentaDecoradoDTO.getSubRamoDTO().getDescripcionSubRamo());
				super.getParametrosVariablesReporte().put("DESCRIPCION_TIPO_REASEGURO", estadoCuentaDecoradoDTO.getDescripcionTipoReaseguro());
				super.getParametrosVariablesReporte().put("DESCRIPCION_MONEDA", EstadoCuentaDN.getINSTANCIA().obtenerDescripcionMoneda(estadoCuentaDecoradoDTO));
				super.getParametrosVariablesReporte().put("OBSERVACIONES", estadoCuentaDecoradoDTO.getObservaciones());
				
				//buscar el saldo t�cnico inicial
				SaldoConceptoDTO saldoTotalAnterior = null;
				SaldoConceptoDTO saldoTotalPeriodo = null;
				SaldoConceptoDTO saldoTecnicoPeriodo = null;
				
				for(SaldoConceptoDTO saldoTMP : estadoCuentaDecoradoDTO.getSaldosAcumulados()){
					//Se agrega el registro a la lista de la plantilla
					super.getListaRegistrosContenido().add(saldoTMP);
					if(saldoTMP.getDescripcionConcepto().indexOf("Saldo Anterior") != -1){//Si es un saldo anterior
						if(saldoTotalAnterior == null){
							saldoTotalAnterior = new SaldoConceptoDTO();
							saldoTotalAnterior.setDebe(saldoTMP.getDebe());
							saldoTotalAnterior.setHaber(saldoTMP.getHaber());
							saldoTotalAnterior.setSaldo(saldoTMP.getSaldo());
							saldoTotalAnterior.setSaldoAcumulado(saldoTMP.getSaldoAcumulado());
						}
						else{
							BigDecimal nuevoDebeAnterior = obtenerValorBigDecimal(saldoTotalAnterior.getDebe()).add(obtenerValorBigDecimal(saldoTMP.getDebe()));
							BigDecimal nuevoHaberAnterior = obtenerValorBigDecimal(saldoTotalAnterior.getHaber()).add(obtenerValorBigDecimal(saldoTMP.getHaber()));
							BigDecimal nuevoSaldoTecnicoAnterior = nuevoHaberAnterior.subtract(nuevoDebeAnterior.abs());
							saldoTotalAnterior.setDebe(nuevoDebeAnterior);
							saldoTotalAnterior.setHaber(nuevoHaberAnterior);
							saldoTotalAnterior.setSaldo(nuevoSaldoTecnicoAnterior);
						}
					}
					else{//Si no es saldo anterior, acumularlo como saldo del periodo.
						if(saldoTotalPeriodo == null){
							saldoTotalPeriodo = new SaldoConceptoDTO();
							saldoTotalPeriodo.setDebe(saldoTMP.getDebe());
							saldoTotalPeriodo.setHaber(saldoTMP.getHaber());
							saldoTotalPeriodo.setSaldo(saldoTMP.getSaldo());
							saldoTotalPeriodo.setSaldoAcumulado(saldoTMP.getSaldoAcumulado());
						}
						else{
							BigDecimal nuevoDebeAnterior = obtenerValorBigDecimal(saldoTotalPeriodo.getDebe()).add(obtenerValorBigDecimal(saldoTMP.getDebe()));
							BigDecimal nuevoHaberAnterior = obtenerValorBigDecimal(saldoTotalPeriodo.getHaber()).add(obtenerValorBigDecimal(saldoTMP.getHaber()));
							BigDecimal nuevoSaldoTecnicoAnterior = nuevoHaberAnterior.subtract(nuevoDebeAnterior.abs());
							saldoTotalPeriodo.setDebe(nuevoDebeAnterior);
							saldoTotalPeriodo.setHaber(nuevoHaberAnterior);
							saldoTotalPeriodo.setSaldo(nuevoSaldoTecnicoAnterior);
						}
					}
				}
				
				if(saldoTotalAnterior == null){
					saldoTotalAnterior = new SaldoConceptoDTO();
				}
				saldoTotalAnterior.setDebe(obtenerValorBigDecimal(saldoTotalAnterior.getDebe()));
				saldoTotalAnterior.setHaber(obtenerValorBigDecimal(saldoTotalAnterior.getHaber()));
				saldoTotalPeriodo.setDebe(obtenerValorBigDecimal(saldoTotalPeriodo.getDebe()));
				saldoTotalPeriodo.setHaber(obtenerValorBigDecimal(saldoTotalPeriodo.getHaber()));
				
				saldoTecnicoPeriodo = new SaldoConceptoDTO();
				saldoTecnicoPeriodo.setDebe(saldoTotalPeriodo.getDebe().abs().add(saldoTotalAnterior.getDebe().abs()));
				saldoTecnicoPeriodo.setHaber(saldoTotalPeriodo.getHaber().abs().add(saldoTotalAnterior.getHaber().abs()));
				saldoTecnicoPeriodo.setSaldo(saldoTecnicoPeriodo.getHaber().abs().subtract(saldoTecnicoPeriodo.getDebe()).abs());
				
				super.getParametrosVariablesReporte().put("TOTAL_DEBE_ANTERIOR", saldoTotalAnterior.getDebe());
				super.getParametrosVariablesReporte().put("TOTAL_HABER_ANTERIOR", saldoTotalAnterior.getHaber());
				super.getParametrosVariablesReporte().put("TOTAL_DEBE_PERIODO", saldoTotalPeriodo.getDebe());
				super.getParametrosVariablesReporte().put("TOTAL_HABER_PERIODO", saldoTotalPeriodo.getHaber());
				super.getParametrosVariablesReporte().put("TOTAL_DEBE_TECNICO", saldoTecnicoPeriodo.getDebe());
				super.getParametrosVariablesReporte().put("TOTAL_HABER_TECNICO", saldoTecnicoPeriodo.getHaber());
				super.getParametrosVariablesReporte().put("TOTAL_SALDO_TECNICO", saldoTecnicoPeriodo.getSaldo());
				
				if(super.getListaRegistrosContenido() == null || super.getListaRegistrosContenido().isEmpty()){
					setByteArrayReport(null);
					generarLogPlantillaSinDatosParaMostrar();
				}
				else{
					try {
						super.setByteArrayReport( generaReporte(super.getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
								getParametrosVariablesReporte(), getListaRegistrosContenido()));
					} catch (JRException e) {
						setByteArrayReport( null );
						generarLogErrorCompilacionPlantilla(e);
					}
				}
			}
//		}
	}
	
	public BigDecimal getIdToEstadoCuenta() {
		return idToEstadoCuenta;
	}

	public void setIdToEstadoCuenta(BigDecimal idToEstadoCuenta) {
		this.idToEstadoCuenta = idToEstadoCuenta;
	}

	public EstadoCuentaDecoradoDTO getEstadoCuentaDecoradoDTO() {
		return estadoCuentaDecoradoDTO;
	}

	public void setEstadoCuentaDecoradoDTO(EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO) {
		this.estadoCuentaDecoradoDTO = estadoCuentaDecoradoDTO;
	}

	public boolean isMostrarConceptosDeDistintosEjercicios() {
		return mostrarConceptosDeDistintosEjercicios;
	}

	public void setMostrarConceptosDeDistintosEjercicios(boolean mostrarConceptosDeDistintosEjercicios) {
		this.mostrarConceptosDeDistintosEjercicios = mostrarConceptosDeDistintosEjercicios;
	}

	public boolean isConsultarRegistrosReporte() {
		return consultarRegistrosReporte;
	}

	public void setConsultarRegistrosReporte(boolean consultarRegistrosReporte) {
		this.consultarRegistrosReporte = consultarRegistrosReporte;
	}
	
	public boolean isConsultarEjerciciosAnteriores() {
		return consultarEjerciciosAnteriores;
	}

	public void setConsultarEjerciciosAnteriores(
			boolean consultarEjerciciosAnteriores) {
		this.consultarEjerciciosAnteriores = consultarEjerciciosAnteriores;
	}

	private void inicializaDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldosPorConcepto"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reporte.estadoCuenta.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_PDF);
	}
	
	private BigDecimal obtenerValorBigDecimal(BigDecimal Valor){
		return Valor == null ? BigDecimal.ZERO : Valor;
	}
}
