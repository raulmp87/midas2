package mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.PlantillaEstadoCuentaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author Jose Luis Arellano
 *
 */
public class PL004_MovimientosCuentaPorPagar extends PlantillaEstadoCuentaBase{

	/*private BigDecimal idToPoliza;
	private BigDecimal idTcReasegurador;
	private BigDecimal idMoneda;
	private BigDecimal[] idTcSubRamo;
	private Integer mes;
	private Integer anio;
	private boolean incluirSoloFacultativo;
	*/
	
	public PL004_MovimientosCuentaPorPagar(Map<String,Object> mapaParametrosPlantilla){
		inicializaDatosPlantilla(mapaParametrosPlantilla);
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarPlantillaMovimientos(claveUsuario);
		return getByteArrayReport();
	}

	private void generarPlantillaMovimientos(String nombreUsuario) throws SystemException{
		
//		List<ReporteReaseguradorEstadoCuentaDTO> listaReaseguradores = 
//			ReporteEstadoCuentaDN.getInstancia().obtenerMovimientosReaseguroCuentasPorPagar(idToPoliza, idTcReasegurador, idMoneda, idTcSubRamo, mes, anio,incluirSoloFacultativo);
//		
//		if(listaReaseguradores != null){
//			for(ReporteReaseguradorEstadoCuentaDTO reporteReasegurador : listaReaseguradores){
//				if(reporteReasegurador.getListaMovimientosReaseguro() != null){
//					reporteReasegurador.setDataSourceSubReporte(new JRBeanCollectionDataSource(reporteReasegurador.getListaMovimientosReaseguro()));
//				}
//				getListaRegistrosContenido().add(reporteReasegurador);
//			}
//		}
		
		//Generar subreporte de datos del estado de cuenta
		JasperReport subReporteDatosEstadoCuenta= getJasperReport(getPaquetePlantilla()+nombrePlantillaSubReporteDatosEstadoCuenta);
		if(subReporteDatosEstadoCuenta == null)
			generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporteDatosEstadoCuenta, null);
		getParametrosVariablesReporte().put("SUBREPORTE_DATOS_ESTADO_CUENTA", subReporteDatosEstadoCuenta);
		getParametrosVariablesReporte().put("DATASOURCE_SUBREPORTE_DATOS_EDO_CTA", new JRBeanCollectionDataSource(listaDatosEstadoCuenta));
		
		//Generar subreporte de movimientos
		String nombrePlantillaSubreporteMovimientos = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.subReporteMovimientosEstadoCuentaCxP");
		JasperReport subReporteMovimientosEstadoCuenta= getJasperReport(getPaquetePlantilla()+nombrePlantillaSubreporteMovimientos);
		getParametrosVariablesReporte().put("SUBREPORTE_MOVIMIENTOS_ESTADO_CUENTA", subReporteMovimientosEstadoCuenta);
		
		if(super.getListaRegistrosContenido() == null || super.getListaRegistrosContenido().isEmpty()){
			setByteArrayReport(null);
			generarLogPlantillaSinDatosParaMostrar();
		}
		else{
			try {
				super.setByteArrayReport( generaReporte(super.getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}
	}
	
	private void inicializaDatosPlantilla(Map<String,Object> mapaParametrosPlantilla){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.movimientosSoporteCuentaPorPagar"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reporte.estadoCuenta.facultativo.paquete"));
		nombrePlantillaSubReporteDatosEstadoCuenta = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.subReporteDatosEstadoCuentaCxP");
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
		if(mapaParametrosPlantilla != null)
			super.getParametrosVariablesReporte().putAll(mapaParametrosPlantilla);
	}

}
