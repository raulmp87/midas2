package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MovimientoPrimaReaseguradorDetalleSN {
private MovimientoPrimaReaseguradorDetalleFacadeRemote beanRemoto;
	
	public MovimientoPrimaReaseguradorDetalleSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MovimientoPrimaReaseguradorDetalleFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto MovimientoPrimaReaseguradorDetalleFacadeRemote instanciado", Level.FINEST, null);
	}

	public List<MovimientoPrimaReaseguradorDetalleDTO> obtenerMovimientosPrimaDetalladosPorReasegurador(MovimientoPrimaReaseguradorDetalleDTO entity,String nombreUsuario) {
		try {
			return beanRemoto.obtenerMovimientosPrimaDetalladosPorReasegurador(entity, nombreUsuario);
		} catch (SQLException e) {
			StringBuffer sb = new StringBuffer();
			sb.append("MIDAS.PKGREA_REPORTES.spREA_RptMovtosPorReasDetalle");
			sb.append("|");
			sb.append("pFechaInicial"+ "=" + entity.getFechaInicioVigencia()+ ",");
			sb.append("pFechaFinal"+ "=" + entity.getFechaFinVigencia()+ ",");
			sb.append("pIdToPoliza"+ "=" + entity.getIdToPoliza()+ ",");
			sb.append("pIdMoneda"+ "=" + entity.getIdMoneda()+ ",");
			sb.append("pIdRamo"+ "=" + entity.getIdRamo()+ ",");
			sb.append("pIdSubRamo"+ "=" + entity.getIdSubRamo());
			sb.append("pRetencion"+ "=" + entity.getRetencion());
			
			String info = "Ocurri� un error al realizar la consulta de movimientos detallados de prima por reasegurador en " + Sistema.AMBIENTE_SISTEMA;
			LogDeMidasWeb.log(info+sb.toString(), Level.SEVERE, e);
			UtileriasWeb.enviaCorreoExcepcion(info, sb.toString());
			return null;
		} catch (Exception e) {
			StringBuffer sb = new StringBuffer();
			sb.append("MIDAS.PKGREA_REPORTES.spREA_RptMovtosPorReasDetalle");
			sb.append("|");
			sb.append("pFechaInicial"+ "=" + entity.getFechaInicioVigencia()+ ",");
			sb.append("pFechaFinal"+ "=" + entity.getFechaFinVigencia()+ ",");
			sb.append("pIdToPoliza"+ "=" + entity.getIdToPoliza()+ ",");
			sb.append("pIdMoneda"+ "=" + entity.getIdMoneda()+ ",");
			sb.append("pIdRamo"+ "=" + entity.getIdRamo()+ ",");
			sb.append("pIdSubRamo"+ "=" + entity.getIdSubRamo());
			sb.append("pRetencion"+ "=" + entity.getRetencion());
			
			String info = "Ocurri� un error al realizar la consulta de movimientos detallados de prima por reasegurador en " + Sistema.AMBIENTE_SISTEMA;
			LogDeMidasWeb.log(info+sb.toString(), Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
