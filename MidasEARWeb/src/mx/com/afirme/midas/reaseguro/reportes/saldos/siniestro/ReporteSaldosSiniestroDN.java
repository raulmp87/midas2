package mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;

public class ReporteSaldosSiniestroDN {
	private static final ReporteSaldosSiniestroDN INSTANCIA = new ReporteSaldosSiniestroDN();
	
	public static ReporteSaldosSiniestroDN getInstancia(){
		return INSTANCIA;
	}

	private ReporteSaldosSiniestroDN() {
	}
	
	public List<ReporteSaldosSiniestroDTO> consultarSaldosSiniestro(Date fechaInicial,Date fechaFinal,boolean soloFacultativos,boolean soloAutomaticos,boolean incluirRetencion,
    		Double idMoneda, final int...rowStartIdxAndCount) throws SystemException{
		return new ReporteSaldosSiniestroSN().consultarSaldosSiniestro(fechaInicial, fechaFinal, soloFacultativos, soloAutomaticos, incluirRetencion, idMoneda, rowStartIdxAndCount);
	}
}
