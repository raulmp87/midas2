package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasXMLCreator;
import mx.com.afirme.midas.sistema.MidasXMLHandler;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class MovimientoPrimaReaseguradorDN {
	private static final MovimientoPrimaReaseguradorDN INSTANCIA = new MovimientoPrimaReaseguradorDN();
	private static final String PREFIJO_ARCHIVO_XML = "Movimientos_Prima_Por_Reasegurador_";
	private static final String PREFIJO_ARCHIVO_XLS = "ReporteMovimientosPrimaPorReasegurador_Del_";
	
	public static MovimientoPrimaReaseguradorDN getInstancia(){
		return INSTANCIA;
	}

	private MovimientoPrimaReaseguradorDN(){
	}
	
	public List<MovimientoPrimaReaseguradorDTO> obtenerMovimientosPrimaPorReasegurador(MovimientoPrimaReaseguradorDTO entity,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		return new MovimientoPrimaReaseguradorSN().obtenerMovimientosPrimaPorReasegurador(entity, nombreUsuario);
	}
	
	public boolean generarDocumentoXMLMovimientosPorReasegurador(Date fecha,boolean sobreEscribirArchivo,String nombreUsuario){
		boolean result = true;
		if (fecha == null)
			fecha = new Date();
		String nombreDocumento = obtenerNombreDocumentoXML(fecha);
		LogDeMidasWeb.log("Generando archivo XML de movimientos de prima por reasegurador: "+nombreDocumento, Level.INFO, null);
		File file = new File(nombreDocumento);
		if(file.exists()){
			LogDeMidasWeb.log("Se encontr� el archivo XML: "+nombreDocumento, Level.INFO, null);
			if(sobreEscribirArchivo){
				if ( file.delete() )
					LogDeMidasWeb.log("Se elimin� el archivo XML :"+nombreDocumento+". Se generar� nuevamente.", Level.INFO, null);
				else
					LogDeMidasWeb.log("No se pudo eliminar el archivo XML :"+nombreDocumento, Level.INFO, null);
			}
			else
				return result;
		}
		//Obtener los movimientos correspondientes a la fecha recibida;
		MovimientoPrimaReaseguradorDTO movimientoPrimaReasegurador = new MovimientoPrimaReaseguradorDTO();
		Calendar calendarInicio = Calendar.getInstance();
		calendarInicio.setTime(fecha);
		calendarInicio.set(Calendar.HOUR, 0);
		calendarInicio.set(Calendar.MINUTE, 0);
		calendarInicio.set(Calendar.SECOND, 0);
		movimientoPrimaReasegurador.setFechaInicioVigencia(calendarInicio.getTime());
		Calendar calendarFin = Calendar.getInstance();
		calendarFin.setTime(fecha);
		calendarInicio.set(Calendar.HOUR, 23);
		calendarInicio.set(Calendar.MINUTE, 59);
		calendarInicio.set(Calendar.SECOND, 59);
		movimientoPrimaReasegurador.setFechaFinVigencia(calendarFin.getTime());
		List<MovimientoPrimaReaseguradorDTO> listaMovimientos;
		Runtime runtime = Runtime.getRuntime();
		try {
			listaMovimientos = obtenerMovimientosPrimaPorReasegurador(movimientoPrimaReasegurador, nombreUsuario);
			LogDeMidasWeb.log("Cantidad de movimientos de prima por reasegurador encontrados para la fecha: "+fecha.toString()+": "+listaMovimientos.size(), Level.INFO, null);
			String[] listaAtributos = {"codigoSubRamo","folioPoliza","numeroEndoso","identificador","fechaInicioVigencia","fechaFinVigencia","numeroInciso","numeroSubInciso",
					"porcentajeRetencion","porcentajeCuotaParte","porcentajePrimerExcedente","porcentajeFacultativo","montoPrimaNeta","folioContrato","tipoReaseguro",
					"nombreACuentaDe","porcentajeParticipacion","porcentajeComision","nombreCorredor","porcentajeParticipacionCorredor","porcentajeComisionCorredor",
					"descripcionAmbito","montoPrimaCedida","montoComisiones","descripcionMoneda","fechaMovimiento","descripcionSucursal","idAsegurado","nombreAsegurado",
					"descripcionMonedaReporte","folioPolizaReporte","montoSumaAsegurada","montoSumaAseguradaReaseguro","montoSumaAseguradaDistribuida","porcentajePleno",
					"descripcionOficina","idTipoReaseguro"};
			MidasXMLCreator xmlCreator = new MidasXMLCreator(listaAtributos);
			List<Object> listaObjetos = new ArrayList<Object>();
			listaObjetos.addAll(listaMovimientos);
			result = xmlCreator.generarDocumentoXML(listaObjetos, "MovimientosPrimaPorReasegurador", "Movimiento", nombreDocumento);
			listaMovimientos = null;
			listaObjetos = null;
			runtime.gc();
			if(result)
				LogDeMidasWeb.log("Se gener� el archivo XML: "+nombreDocumento, Level.INFO, null);
			else
				LogDeMidasWeb.log("No se pudo generar el archivo XML: "+nombreDocumento, Level.SEVERE, null);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XML: "+nombreDocumento, Level.SEVERE, e);
			result = false;
		} catch (SystemException e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XML: "+nombreDocumento, Level.SEVERE, e);
			result = false;
		} catch(Exception e){
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XML: "+nombreDocumento, Level.SEVERE, e);
			result = false;
		}
		return result;
	}
	
	public boolean generarDocumentoXLSMovimientosPorReasegurador(Date fechaInicial,Date fechaFinal,boolean sobreEscribirArchivo,List<Date> fechasPorSobreEscribir,String nombreUsuario) throws SystemException{
		LogDeMidasWeb.log("Generando reporte XLS de movimientos de prima por reasegurador. Rango de fechas: del "+fechaInicial.toString()+" al "+fechaFinal.toString(), Level.INFO, null);
		if (fechaFinal.before(fechaInicial)){
			LogDeMidasWeb.log("Error al generar reporte XLS de movimientos de prima por reasegurador. Rango de fechas: del "+fechaInicial.toString()+" al "+fechaFinal.toString()+ " NO V�LIDO", Level.SEVERE, null);
			return false;
		}
		Runtime runtime = Runtime.getRuntime();
		boolean resultado = true;
		String nombreArchivo = obtenerNombreDocumentoXLS(fechaInicial, fechaFinal);
		Calendar calendarFechaFinal = Calendar.getInstance();
		calendarFechaFinal.setTime(fechaFinal);
		File file = new File(nombreArchivo);
		if(file.exists()){
			LogDeMidasWeb.log("Se encontr� el archivo "+nombreArchivo, Level.INFO, null);
			if(sobreEscribirArchivo){
				if(!(fechasPorSobreEscribir != null && !fechasPorSobreEscribir.isEmpty() )){//Sobreescribir subreportes por d�a, indicados en fechasPorSobreEscribir
					return resultado;
				}
				else{	
					LogDeMidasWeb.log("Se sobreescribir� el archivo "+nombreArchivo, Level.INFO, null);
					try{
						new File(nombreArchivo).delete();
						LogDeMidasWeb.log("Se elimin� correctamente el archivo "+nombreArchivo, Level.INFO, null);
						//eliminar todos los archivos XLS del mes
						Format formatter = new SimpleDateFormat("MM_yyyy");
						eliminarArchivos(UtileriasWeb.obtenerRutaDocumentosXLSReportes(), PREFIJO_ARCHIVO_XLS, formatter.format(fechaFinal)+".xls");
					}catch(Exception e){}
				}
			}
			else	return resultado;
		}
		calendarFechaFinal.add(Calendar.DATE, 1);
		Calendar calendarEntreRango = Calendar.getInstance();
		calendarEntreRango.setTime(fechaInicial);
		int reportesSobreescritosPorFecha = 0;
		do{
			boolean sobreEscribirArchivoFechaActual = false;
			if(sobreEscribirArchivo){
				if(reportesSobreescritosPorFecha < fechasPorSobreEscribir.size()){
					for(int i=0;i<fechasPorSobreEscribir.size();i++){
						if(fechasPorSobreEscribir.get(i) != null){
							Calendar calendarTMP = Calendar.getInstance();
							calendarTMP.setTime(fechasPorSobreEscribir.get(i));
							if(calendarTMP.get(Calendar.DATE) == calendarEntreRango.get(Calendar.DATE) &&
									calendarTMP.get(Calendar.MONTH) == calendarEntreRango.get(Calendar.MONTH) &&
										calendarTMP.get(Calendar.YEAR) == calendarEntreRango.get(Calendar.YEAR)){
								sobreEscribirArchivoFechaActual = true;
								reportesSobreescritosPorFecha ++;
							}
						}
					}
				}
			}
			if(generarDocumentoXMLMovimientosPorReasegurador(calendarEntreRango.getTime(),sobreEscribirArchivoFechaActual,nombreUsuario)){
				calendarEntreRango.add(Calendar.DATE, 1);
			}
			else
				return false;
		}while(calendarEntreRango.get(Calendar.DATE) != calendarFechaFinal.get(Calendar.DATE) || calendarEntreRango.get(Calendar.MONTH) != calendarFechaFinal.get(Calendar.MONTH) || calendarEntreRango.get(Calendar.YEAR) != calendarFechaFinal.get(Calendar.YEAR));
		//Se generaron todos los documentos XML necesarios para obtener el archivo XML.
		calendarEntreRango = Calendar.getInstance();
		calendarEntreRango.setTime(fechaInicial);
		String[] listaAtributos = {"codigoSubRamo","folioPoliza","numeroEndoso","identificador","fechaInicioVigencia","fechaFinVigencia","numeroInciso","numeroSubInciso",
				"porcentajePleno","porcentajeRetencion","porcentajeCuotaParte","porcentajePrimerExcedente","porcentajeFacultativo","montoPrimaNeta","folioContrato","tipoReaseguro",
				"nombreACuentaDe","porcentajeParticipacion","porcentajeComision","nombreCorredor","porcentajeParticipacionCorredor","porcentajeComisionCorredor",
				"descripcionAmbito","montoPrimaCedida","montoComisiones","descripcionMoneda","fechaMovimiento","descripcionSucursal","idAsegurado","nombreAsegurado",
				"descripcionMonedaReporte","folioPolizaReporte","montoSumaAsegurada","montoSumaAseguradaReaseguro","montoSumaAseguradaDistribuida",
				"descripcionOficina","idTipoReaseguro"};
		String[] nombreColumnas= {"SUBRAMO","POLIZA","ENDOSO","IDENTIFICADOR","VIGENCIA DESDE","VIGENCIA HASTA","INCISO","SUBINICISO",
				"% PLENO","% RETENCION","% CUOTA PARTE","% 1ER EXCEDENTE","% FACULTATIVO","PRIMA NETA","CODIGO DEL CONTRATO","TIPO DE REASEGURO",
				"NOMBRE A CUENTA DE","% PARTICIPACION","% COMISION","NOMBRE CORREDOR","% PARTICIPACION DEL CORREDOR","% COMISION DEL CORREDOR",
				"LOCAL O EXTRANJERO","PRIMA CEDIDA","COMISIONES","MONEDA","FECHA DEL MOVIMIENTO","SUCURSAL","ID ASEGURADO","NOMBRE ASEGURADO",
				"descripcionMonedaReporte","folioPolizaReporte","montoSumaAsegurada","montoSumaAseguradaReaseguro","montoSumaAseguradaDistribuida",
				"SUCURSAL","idTipoReaseguro"};
		
		MidasXLSCreator creadorDocumentoXLS = new MidasXLSCreator(nombreColumnas, listaAtributos,MidasXLSCreator.FORMATO_XLS);
		creadorDocumentoXLS.iniciarProcesamientoArchivoXLS("MovimientosPorReasegurador");
		do{
			String nombreDocumento = obtenerNombreDocumentoXML(calendarEntreRango.getTime());
			if(! new File(nombreDocumento).exists()){
				if(!generarDocumentoXMLMovimientosPorReasegurador(calendarEntreRango.getTime(), false,nombreUsuario))
					return false;
			}
			XMLReader xmlParser = new SAXParser();
			List<Object> listaRegistros = new ArrayList<Object>();
			xmlParser.setContentHandler(new MidasXMLHandler("Movimiento", listaRegistros, listaAtributos, MovimientoPrimaReaseguradorDTO.class.getCanonicalName()));
			try {
				xmlParser.parse(nombreDocumento);
			} catch (IOException e) {
				LogDeMidasWeb.log("Ocurri� un error al leer el archivo '"+nombreDocumento+"'. No se pudo generar el reporte '"+nombreDocumento+"'", Level.SEVERE, e);
				return false;
			} catch (SAXException e) {
				LogDeMidasWeb.log("Ocurri� un error al Parsear el archivo '"+nombreDocumento+"'. No se pudo generar el reporte '"+nombreDocumento+"'", Level.SEVERE, e);
				return false;
			}
			if(listaRegistros != null && !listaRegistros.isEmpty()){
				creadorDocumentoXLS.insertarFilasArchivoXLS(listaRegistros);
				listaRegistros = null;
				runtime.gc();
			}
			calendarEntreRango.add(Calendar.DATE, 1);
		}while (calendarEntreRango.get(Calendar.DATE) != calendarFechaFinal.get(Calendar.DATE) || calendarEntreRango.get(Calendar.MONTH) != calendarFechaFinal.get(Calendar.MONTH) || calendarEntreRango.get(Calendar.YEAR) != calendarFechaFinal.get(Calendar.YEAR));
		try {
			creadorDocumentoXLS.finalizarProcesamientoArchivoXLS(nombreArchivo);
			runtime.gc();
			LogDeMidasWeb.log("Se gener� el archivo '"+nombreArchivo+"'.", Level.INFO, null);
		} catch (IOException e) {
			LogDeMidasWeb.log("Ocurri� un error al generar el archivo XLS: "+nombreArchivo, Level.INFO, null);
			resultado = false;
		}
		return resultado;
	}
	
	public void generarDocXMLMovtosPorReaseguradorDiaAnterior(String nombreUsuario){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		if(! generarDocumentoXMLMovimientosPorReasegurador(calendar.getTime(), true, nombreUsuario)){
			
		}
		/*
		 * 30/07/2010. Jos� Luis Arellano. Por petici�n de Euler, se debe generar el REAS de todo el mes, no s�lo del d�a anterior.
		 */
		Format formatter = new SimpleDateFormat("MM_yyyy");
		eliminarArchivos(UtileriasWeb.obtenerRutaDocumentosXLSReportes(), PREFIJO_ARCHIVO_XLS, formatter.format(calendar.getTime())+".xls");
		if(Sistema.REGENERAR_REAS_MENSUAL_JOB){
			Calendar calendarHoy = Calendar.getInstance();
			calendar.add(Calendar.DATE, -1);
			while(calendarHoy.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)){
				generarDocumentoXMLMovimientosPorReasegurador(calendar.getTime(), true, nombreUsuario);
				calendar.add(Calendar.DATE, -1);
			}
		}
	}
	
	public String obtenerNombreDocumentoXLS(Date fechaInicial,Date fechaFinal){
		Format formatter = new SimpleDateFormat("dd_MM_yyyy");
		return UtileriasWeb.obtenerRutaDocumentosXLSReportes()+PREFIJO_ARCHIVO_XLS+formatter.format(fechaInicial)+
		"_Al_"+formatter.format(fechaFinal)+".xls";
	}
	
	public String obtenerNombreDocumentoXML(Date fecha){
		Format formatter = new SimpleDateFormat("dd_MM_yyyy");
		return UtileriasWeb.obtenerRutaDocumentosXMLReportes()+PREFIJO_ARCHIVO_XML+formatter.format(fecha)+".xml";
	}
	
	public static void eliminarArchivos(String directorioArchivos,final String inicioNombreArchivosPorBorrar,final String finNombreArchivosPorBorrar){
		try{
			File directorioArchivosFile = new File(directorioArchivos);
			if(directorioArchivosFile.exists() && directorioArchivosFile.isDirectory()){
				File[] archivosPorBorrar = directorioArchivosFile.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return (name.startsWith(inicioNombreArchivosPorBorrar) && name.endsWith(finNombreArchivosPorBorrar));
					}
				});
				if(archivosPorBorrar != null && archivosPorBorrar.length > 0){
					for(int i=0; i<archivosPorBorrar.length; i++){
						archivosPorBorrar[i].delete();
						LogDeMidasWeb.log("Se elimin� correctamente el archivo \""+archivosPorBorrar[i].getName()+"\"", Level.INFO, null);
					}
				}
		}
		}catch(Exception e){}
		}
}