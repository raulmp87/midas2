package mx.com.afirme.midas.reaseguro.reportes.estadocuenta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteEstadoCuenta extends MidasReporteBase{
	private List<EstadoCuentaDecoradoDTO> listaEstadosCuentaReporte;
	private BigDecimal[] idToEstadoCuentaArray;
	boolean consultarEjerciciosAnteriores;
	private String tipoReporte;
	
	public ReporteEstadoCuenta(boolean consultarEjerciciosAnteriores,String tipoReporte,BigDecimal ... idToEstadoCuentaArray){
		this.idToEstadoCuentaArray = idToEstadoCuentaArray;
		this.consultarEjerciciosAnteriores = consultarEjerciciosAnteriores;
		this.tipoReporte = tipoReporte;
		listaEstadosCuentaReporte = new ArrayList<EstadoCuentaDecoradoDTO>();
	}
	
	public byte[] obtenerReporte(String claveUsuario,boolean agregaPiePagina) throws SystemException {
		super.setListaPlantillas(new ArrayList<byte[]>());
		if(idToEstadoCuentaArray != null){
			listaEstadosCuentaReporte = new ArrayList<EstadoCuentaDecoradoDTO>();
			for(BigDecimal idToEstadoCuenta : idToEstadoCuentaArray){
				EstadoCuentaDecoradoDTO estadoCuentaTMP = null;
				if(consultarEjerciciosAnteriores)
					estadoCuentaTMP = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaHistoricoCombinado(idToEstadoCuenta, claveUsuario);
				else{
					try{
						estadoCuentaTMP = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuentaDetalle(idToEstadoCuenta, null, null);
					}catch(Exception e){}
				}
				if(estadoCuentaTMP != null){
					listaEstadosCuentaReporte.add(estadoCuentaTMP);
				}
			}
			for(EstadoCuentaDecoradoDTO estadoCuentaTMP : listaEstadosCuentaReporte){
				PL001_ConceptosPorEstadoCuenta plantillaEdoCta = new PL001_ConceptosPorEstadoCuenta(estadoCuentaTMP, false);
				plantillaEdoCta.setTipoReporte(tipoReporte);
				byte[] plantilla = plantillaEdoCta.obtenerReporte(claveUsuario);
				if(plantilla != null){
					super.getListaPlantillas().add(plantilla);
				}
			}
		}
		return super.obtenerReporte(claveUsuario,false);
	}
	
}
