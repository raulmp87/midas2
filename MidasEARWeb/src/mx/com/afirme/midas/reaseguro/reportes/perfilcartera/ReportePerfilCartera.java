package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.util.Date;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.SystemException;

public class ReportePerfilCartera extends MidasReporteBase{

	private Date fechaInicio;
	private Date fechaFin;
	private Double tipoCambio;
	private Integer tipoReporte;
	
		public ReportePerfilCartera(Date fechaInicio, Date fechaFin,Double tipoCambio,Integer tipoReporte) {
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.tipoCambio = tipoCambio;
		this.tipoReporte = tipoReporte;
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		
		byte byteArray[] = null;
		
		PlantillaPerfilCartera plantilla = new PlantillaPerfilCartera(fechaInicio,fechaFin,tipoCambio,tipoReporte);
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		
		return byteArray;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
}
