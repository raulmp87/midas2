package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;

import java.util.ArrayList;

import mx.com.afirme.midas.reaseguro.reportes.saldos.ReporteMovimientosSaldoBase;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author Jose Luis Arellano
 * @since 19/11/2010
 */
public class ReporteSaldosCuentasPorPagar extends ReporteMovimientosSaldoBase{

	public ReporteSaldosCuentasPorPagar(){
		super.setListaPlantillas(new ArrayList<byte[]>());
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		return super.obtenerReporte(claveUsuario);
	}
	
}
