package mx.com.afirme.midas.reaseguro.reportes;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.jfree.util.Log;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRProperties;
import mx.com.afirme.midas.sistema.DataSourceLocator;
import mx.com.afirme.midas.sistema.MidasBaseReporte;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReportesReaseguro extends MidasBaseReporte {
	DataSource dataSource;
	private final String rutaPaqueteReportesReaseguro = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes");
	
	public ReportesReaseguro(){
		JRProperties.setProperty("net.sf.jasperreports.query.executer.factory.plsql", "org.jasperforge.jaspersoft.demo.PlSqlQueryExecuterFactory");
		try {
			dataSource = DataSourceLocator.getDataSource(Sistema.MIDAS_DATASOURCE);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param nombreReporte, El nombre asignado al reporte (nombre del archivo fuente de Jasper Reports sin la extensi�n .jrxml, los nombres deben 
	 * estar definidos en el archivo de properties 'RecursoDeMensajes' en el apartado '#Reaseguro constantes para reportes')
	 * @param parametros, HashMap con los par�metros espec�ficos del reporte
	 * @param formatoSalidaReporte, dos opciones, obtenidas de Sistema.TIPO_PDF y de Sistema.TIPO_XLS, dependiendo del tipo de archivo de salida requerido
	 * @param tipoSalidaReporte, dos opciones, obtenidas de Sistema.RPT_COMO_ADJUNTO y de Sistema.RPT_EN_NAVEGADOR dependiendo de si el archivo quiere ser ofrecido para descargar 'como adjunto' o se desea 'ver en el navegador'
	 * @param response, necesario para poder lograr el flujo de salida del reporte
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
	public void generarReportePLSQL(String nombreReporte, Map<String, Object> parametros, String formatoSalidaReporte, String tipoSalidaReporte, HttpServletResponse response){

		Connection connection = null;
		byte[] ficheroReporte = null; 
		JasperReport jasperReport = getJasperReport(rutaPaqueteReportesReaseguro+nombreReporte+".jrxml");
		JasperPrint jasperPrint;
		try {
			connection = dataSource.getConnection();
			ServletOutputStream out;
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, connection);
			response.setContentType(formatoSalidaReporte);
			if (formatoSalidaReporte.equals(Sistema.TIPO_PDF))
				ficheroReporte = prepararPDF(jasperPrint, response, nombreReporte, tipoSalidaReporte);
			else if (formatoSalidaReporte.equals(Sistema.TIPO_XLS))
				ficheroReporte = prepararXLS(jasperPrint, response, nombreReporte, tipoSalidaReporte);
			else if (formatoSalidaReporte.equals(Sistema.TIPO_CSV))
				ficheroReporte = prepararCSV(jasperPrint, response, nombreReporte, tipoSalidaReporte);
			else if (formatoSalidaReporte.equals(Sistema.TIPO_TXT))
				ficheroReporte = prepararTXT(jasperPrint, response, nombreReporte, tipoSalidaReporte);
			else throw new JRException("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");

			out = response.getOutputStream();
			out.write(ficheroReporte, 0, ficheroReporte.length);
			out.flush();
			out.close();
			
		} catch (JRException e) {
			System.out.println("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");
		} catch (IOException e) {
			System.out.println("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");
		} catch (SQLException e) {
			System.out.println("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");
		}finally{
		    DbUtils.closeQuietly(connection);
		}
		
	}
	
	public static String obtenerSufijoArchivoReporte(){
		String sufijo = "";
		String fecha = UtileriasWeb.getFechaString(new Date());
		String hora = UtileriasWeb.getHoraConSegundosString(new Date());
		String[] fechaArr = fecha.split("/");
		String[] horaArr = hora.split(":");
		String dia = fechaArr[0];
		String mes = fechaArr[1];
		String anio = fechaArr[2];
		String horas = horaArr[0];
		String mins = horaArr[1];
		String segs = horaArr[2];
		if (dia.length() == 1)
			dia = "0" + dia;
		if (mes.length() == 1)
			mes = "0" + mes;
		if (anio.length() == 1)
			anio = "0" + anio;
		if (horas.length() == 1)
			horas = "0" + horas;
		if (mins.length() == 1)
			mins = "0" + mins;
		
		sufijo = dia + mes + anio + "-" + horas + "_" + mins + "_" + segs + "hrs";
		
		return sufijo;
	}
	
	private byte[] prepararPDF(JasperPrint jasperPrint, HttpServletResponse response, String nombreArchivo, String tipoSalidaReporte) throws JRException{
		
		byte[] ficheroReporte = JasperExportManager.exportReportToPdf(jasperPrint);
		String[] nombreArchivoFormato = nombreArchivo.split("/");
		nombreArchivo = nombreArchivoFormato[nombreArchivoFormato.length-1];
		nombreArchivo = nombreArchivo.replaceAll(" ", "_");
		if(response != null){
			response.setHeader("Content-Disposition", tipoSalidaReporte+"; filename="+nombreArchivo+obtenerSufijoArchivoReporte()+".pdf");
			response.setContentLength(ficheroReporte.length);
			response.setHeader("Cache-Control", "max-age=30");
			response.setHeader("Pragma", "No-cache");
			response.setContentLength(ficheroReporte.length);
		}
		
		return ficheroReporte;
	}
	
	private byte[] prepararXLS(JasperPrint jasperPrint, HttpServletResponse response, String nombreArchivo, String tipoSalidaReporte) throws JRException{
		byte[] ficheroReporte;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRXlsExporter jrxlsExporter = new JRXlsExporter();
		jrxlsExporter.setParameter(JRExporterParameter.JASPER_PRINT,
				jasperPrint);
		jrxlsExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		jrxlsExporter.setParameter(
				JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
				Boolean.FALSE);
		jrxlsExporter.setParameter(
				JRXlsExporterParameter.IS_DETECT_CELL_TYPE,
				Boolean.TRUE);
		jrxlsExporter.setParameter(
				JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED,
				Boolean.TRUE);
		
		jrxlsExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "ISO-8859-1");
		
		jrxlsExporter.exportReport(); 
		
		String[] nombreArchivoFormato = nombreArchivo.split("/");
		nombreArchivo = nombreArchivoFormato[nombreArchivoFormato.length-1];
		nombreArchivo = nombreArchivo.replaceAll(" ", "_");
		ficheroReporte = outputStream.toByteArray();
		if(response != null){
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", tipoSalidaReporte+"; filename="+nombreArchivo+obtenerSufijoArchivoReporte()+".xls");
			response.setHeader("Cache-Control", "max-age=30");
			response.setHeader("Pragma", "No-cache");
			response.setContentLength(ficheroReporte.length);
		}
		
		return ficheroReporte;
	}
	
	private byte[] prepararCSV(JasperPrint jasperPrint, HttpServletResponse response, String nombreArchivo, String tipoSalidaReporte) throws JRException{
		byte[] ficheroReporte;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRCsvExporter jRCsvExporter = new JRCsvExporter();
		jRCsvExporter.setParameter(JRExporterParameter.JASPER_PRINT,
				jasperPrint);
		jRCsvExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		
		jRCsvExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "ISO-8859-1");
		
		jRCsvExporter.exportReport(); 
		
		String[] nombreArchivoFormato = nombreArchivo.split("/");
		nombreArchivo = nombreArchivoFormato[nombreArchivoFormato.length-1];
		nombreArchivo = nombreArchivo.replaceAll(" ", "_");
		ficheroReporte = outputStream.toByteArray();
		response.setContentType("text/csv");
		response.setHeader("Content-Disposition", tipoSalidaReporte+"; filename="+nombreArchivo+obtenerSufijoArchivoReporte()+".csv");
		response.setHeader("Cache-Control", "max-age=30");
		response.setHeader("Pragma", "No-cache");
		response.setContentLength(ficheroReporte.length);
		
		return ficheroReporte;
	}
	
	private byte[] prepararTXT(JasperPrint jasperPrint, HttpServletResponse response, String nombreArchivo, String tipoSalidaReporte) throws JRException{
		byte[] ficheroReporte;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JRTextExporter jRTextExporter = new JRTextExporter();
		jRTextExporter.setParameter(JRExporterParameter.JASPER_PRINT,
				jasperPrint);
		jRTextExporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				outputStream);
		jRTextExporter.setParameter(JRTextExporterParameter.CHARACTER_HEIGHT, new Float(7));
		jRTextExporter.setParameter(JRTextExporterParameter.CHARACTER_WIDTH, new Float(7));
		jRTextExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "ISO-8859-1");
		jRTextExporter.exportReport(); 
		
		String[] nombreArchivoFormato = nombreArchivo.split("/");
		nombreArchivo = nombreArchivoFormato[nombreArchivoFormato.length-1];
		nombreArchivo = nombreArchivo.replaceAll(" ", "_");
		ficheroReporte = outputStream.toByteArray();
		response.setContentType("text/plain");
		response.setHeader("Content-Disposition", "text/plain; filename="+nombreArchivo+obtenerSufijoArchivoReporte()+".txt");
		response.setHeader("Cache-Control", "max-age=30");
		response.setHeader("Pragma", "No-cache");
		response.setContentLength(ficheroReporte.length);
		
		return ficheroReporte;
	}
	
	public void generarReporteBean(String nombreReporte, JRDataSource jrDataSource, HashMap<String, Object> parametros, String formatoSalidaReporte, String tipoSalidaReporte, HttpServletResponse response) throws JRException, IOException{
		ServletOutputStream out;
		byte[] ficheroReporte = null;
		JasperReport jasperReport = getJasperReport(rutaPaqueteReportesReaseguro+nombreReporte+".jrxml");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, jrDataSource);
		response.setContentType(formatoSalidaReporte);
		if (formatoSalidaReporte.equals(Sistema.TIPO_PDF))
			ficheroReporte = prepararPDF(jasperPrint, response, nombreReporte, tipoSalidaReporte);
		else if (formatoSalidaReporte.equals(Sistema.TIPO_XLS))
			ficheroReporte = prepararXLS(jasperPrint, response, nombreReporte, tipoSalidaReporte);
		else throw new JRException("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");
		
		out = response.getOutputStream();
		out.write(ficheroReporte, 0, ficheroReporte.length);
		out.flush();
		out.close();
	}
	
	public byte[] generarReporteBeanAndReturnByteArray(String nombreReporte, JRDataSource jrDataSource, HashMap<String, Object> parametros, String formatoSalidaReporte, String tipoSalidaReporte, HttpServletResponse response) throws JRException, IOException{
		byte[] ficheroReporte = null;
		JasperReport jasperReport = getJasperReport(rutaPaqueteReportesReaseguro+nombreReporte+".jrxml");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, jrDataSource);
		if(response != null)
			response.setContentType(formatoSalidaReporte);
		if (formatoSalidaReporte.equals(Sistema.TIPO_PDF))
			ficheroReporte = prepararPDF(jasperPrint, response, nombreReporte, tipoSalidaReporte);
		else if (formatoSalidaReporte.equals(Sistema.TIPO_XLS))
			ficheroReporte = prepararXLS(jasperPrint, response, nombreReporte, tipoSalidaReporte);
		else throw new JRException("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");
		
		return ficheroReporte;
	}
	
	@SuppressWarnings("unchecked")
	public byte[] concatenarReportes(List<byte[]> listaReportesByte, String nombreArchivoFinal, HttpServletResponse response) throws DocumentException, IOException{
		ServletOutputStream out;
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		int pageOffset = 0;
		ArrayList master = new ArrayList();
		int f = 0;
		Document document = null;
		PdfCopy  writer = null;
		int rptsLenght = 0;
		for (byte[] rptEnTurno : listaReportesByte) {
			rptsLenght += rptEnTurno.length;
			PdfReader reader = new PdfReader(rptEnTurno);
			reader.consolidateNamedDestinations();
			int n = reader.getNumberOfPages();
			List bookmarks = SimpleBookmark.getBookmark(reader);
			if (bookmarks != null) {
				if (pageOffset != 0)
					SimpleBookmark.shiftPageNumbers
					(bookmarks, pageOffset, null);
				master.addAll(bookmarks);
			}
			pageOffset += n;
			if (f == 0) {                      
				document = new Document(reader.getPageSizeWithRotation(1));                  
				writer = new PdfCopy(document, byteArrayOutputStream);
				document.open();     
			}
			PdfImportedPage page;
			for (int i = 0; i < n;){
				++i;
				page = writer.getImportedPage(reader, i);
				writer.addPage(page);
			}
			PRAcroForm form = reader.getAcroForm();
			if (form != null)
				writer.copyAcroForm(reader);
			f++;
		}
		if (!master.isEmpty())
			writer.setOutlines(master);
		
		document.close();
		byte byteArray[] = byteArrayOutputStream.toByteArray();
		if(response != null){
			response.setContentLength(byteArray.length);
			out = response.getOutputStream();
			out.write(byteArray, 0, byteArray.length);
			out.flush();
			out.close();
		}
		return byteArray;
	}
	
	/**
	 * 
	 * @param nombreReporte, El nombre asignado al reporte (nombre del archivo fuente de Jasper Reports sin la extensi�n .jrxml, los nombres deben 
	 * estar definidos en el archivo de properties 'RecursoDeMensajes' en el apartado '#Reaseguro constantes para reportes')
	 * @param parametros, HashMap con los par�metros espec�ficos del reporte
	 * @param formatoSalidaReporte, dos opciones, obtenidas de Sistema.TIPO_PDF y de Sistema.TIPO_XLS, dependiendo del tipo de archivo de salida requerido
	 * @param tipoSalidaReporte, dos opciones, obtenidas de Sistema.RPT_COMO_ADJUNTO y de Sistema.RPT_EN_NAVEGADOR dependiendo de si el archivo quiere ser ofrecido para descargar 'como adjunto' o se desea 'ver en el navegador'
	 * @param response, necesario para poder lograr el flujo de salida del reporte
	 * @throws SQLException
	 * @throws JRException
	 * @throws IOException
	 */
	public byte[] generarReportePLSQLAndReturnByteArray(String nombreReporte, Map<String, Object> parametros, String formatoSalidaReporte, String tipoSalidaReporte, HttpServletResponse response){
		byte[] ficheroReporte = null;
		JasperReport jasperReport = getJasperReport(rutaPaqueteReportesReaseguro+nombreReporte+".jrxml");
		JasperPrint jasperPrint;
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, connection);
			response.setContentType(formatoSalidaReporte);
			if (formatoSalidaReporte.equals(Sistema.TIPO_PDF))
				ficheroReporte = prepararPDF(jasperPrint, response, nombreReporte, tipoSalidaReporte);
			else if (formatoSalidaReporte.equals(Sistema.TIPO_XLS))
				ficheroReporte = prepararXLS(jasperPrint, response, nombreReporte, tipoSalidaReporte);
			else throw new JRException("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)");

		} catch (JRException e) {
			Log.error("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)", e);
			return ficheroReporte;
		} catch (SQLException e) {
			Log.error("formatoSalidaReporte no definido en ReportesReaseguro.generarReportePLSQL(...)", e);
		}finally{
		    DbUtils.closeQuietly(connection);
		}
		
		return ficheroReporte;
	}
	
}
