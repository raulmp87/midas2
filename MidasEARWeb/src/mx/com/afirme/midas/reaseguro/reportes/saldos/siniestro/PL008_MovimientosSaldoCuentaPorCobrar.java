package mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.reaseguro.reportes.saldos.PlantillaMovimientosSaldoBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

/**
 * @author Jose Luis Arellano
 *
 */
public class PL008_MovimientosSaldoCuentaPorCobrar extends PlantillaMovimientosSaldoBase{

	public PL008_MovimientosSaldoCuentaPorCobrar(){
		inicializarDatosPlantilla();
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteMovimientosSaldo();
		return getByteArrayReport();
	}
	
	private void generarReporteMovimientosSaldo() throws SystemException{
		if(parametrosReporteValidos()){
			List<ReporteSaldosSiniestroDTO> listaMovimientos = ReporteSaldosSiniestroDN.getInstancia().consultarSaldosSiniestro(
					fechaInicial, fechaFinal, soloFacultativos, soloAutomaticos, incluirRetencion, idMoneda,null);
			
			BigDecimal totalCesionSiniestro = BigDecimal.ZERO;
			BigDecimal totalCobros = BigDecimal.ZERO;
			BigDecimal totalSaldoPorCobrar = BigDecimal.ZERO;
			
			setListaRegistrosContenido(new ArrayList<Object>());
			
			for(ReporteSaldosSiniestroDTO movimientoReporte : listaMovimientos){
				getListaRegistrosContenido().add(movimientoReporte);
				
				totalCesionSiniestro = totalCesionSiniestro.add(movimientoReporte.getMontoCesionSiniestro());
				totalCobros = totalCobros.add(movimientoReporte.getMontoCobros());
				totalSaldoPorCobrar = totalSaldoPorCobrar.add(movimientoReporte.getMontoSaldosPorCobrar());
			}
			
			poblarParametrosComunes();
			
			getParametrosVariablesReporte().put("TOTAL_CESION_SINIESTROS", totalCesionSiniestro);
			getParametrosVariablesReporte().put("TOTAL_COBROS", totalCobros);
			getParametrosVariablesReporte().put("TOTAL_SALDO_POR_COBRAR", totalSaldoPorCobrar);
			
			if(super.getListaRegistrosContenido().isEmpty()){
//				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
			}
//			else{
				try {
					super.setByteArrayReport( generaReporte(super.getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
							getParametrosVariablesReporte(), getListaRegistrosContenido()));
				} catch (JRException e) {
					setByteArrayReport( null );
					generarLogErrorCompilacionPlantilla(e);
				}
//			}
		}
	}

	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.movimientosSaldoCuentaPorCobrar"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.movimientosSaldoCuentaPorCobrar.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
	}
}
