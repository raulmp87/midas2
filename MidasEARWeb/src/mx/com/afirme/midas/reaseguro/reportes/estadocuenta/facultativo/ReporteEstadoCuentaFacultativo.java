package mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.PL001_ConceptosPorEstadoCuenta;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReporteEstadoCuentaFacultativo extends ReporteEstadoCuentaBase{
//	private List<EstadoCuentaDecoradoDTO> listaEstadosCuentaReporte;
	
	private boolean imprimirFormatoAutomaticos = false;
	
	public ReporteEstadoCuentaFacultativo(){
		super.setListaPlantillas(new ArrayList<byte[]>());
		super.setMapaParametrosGeneralesPlantillas(new HashMap<String, Object>());
	}
	
	public ReporteEstadoCuentaFacultativo(String tipoReporte){
		super.tipoReporte = tipoReporte;
		super.setMapaParametrosGeneralesPlantillas(new HashMap<String, Object>());
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		super.setListaPlantillas(new ArrayList<byte[]>());
		byte[] plantilla = null;
		String impresionMasivaString = parametrosConfiguracion.get("impresionMasivaAutomaticos");
		if(!UtileriasWeb.esCadenaVacia(impresionMasivaString) && impresionMasivaString.equals("true")){
			super.setListaEstadoCuentaDecoradoDTO(new ArrayList<EstadoCuentaDecoradoDTO>());
			String idsEstadocuentaMasivo = parametrosConfiguracion.get("idEstadoCuentaMasivo");
			String[] idToEstadoCuentaStringArray = idsEstadocuentaMasivo.split(",");
			for(int i=0;i<idToEstadoCuentaStringArray.length;i++){
				parametrosConfiguracion.put("idEstadoCuenta", idToEstadoCuentaStringArray[i]);
				try {
					estadoCuentaDecoradoDTO = this.obtenerEstadoCuenta(claveUsuario);
				} catch (Exception e) {
					LogDeMidasWeb.log("Ocurri� un error al consultar el estado de cuenta facultativo.", Level.SEVERE, e);
				}
				if(estadoCuentaDecoradoDTO != null){
					PL001_ConceptosPorEstadoCuenta plantillaEstadoCuentaAutomaticos = new PL001_ConceptosPorEstadoCuenta(estadoCuentaDecoradoDTO, false);
					plantillaEstadoCuentaAutomaticos.setTipoReporte(tipoReporte);
					plantilla = plantillaEstadoCuentaAutomaticos.obtenerReporte(claveUsuario);
					
					if(plantilla != null){
						super.getListaPlantillas().add(plantilla);
						plantilla = null;
					}
				}
			}
		}
		else{
			if(estadoCuentaDecoradoDTO == null){
				try {
					estadoCuentaDecoradoDTO = this.obtenerEstadoCuenta(claveUsuario);
				} catch (Exception e) {
					LogDeMidasWeb.log("Ocurri� un error al consultar el estado de cuenta facultativo.", Level.SEVERE, e);
				}
			}
			if(estadoCuentaDecoradoDTO != null){
				if(!imprimirFormatoAutomaticos){
					PL002_ConceptosPorEstadoCuentaFacultativo plantillaEstadoCuentaFacultativo = new PL002_ConceptosPorEstadoCuentaFacultativo(getMapaParametrosGeneralesPlantillas(),tipoReporte);
					plantillaEstadoCuentaFacultativo.setEstadoCuentaDecoradoDTO(estadoCuentaDecoradoDTO);
					try {
						plantillaEstadoCuentaFacultativo.setListaDatosEstadoCuenta(obtenerDatosEstadoCuenta(claveUsuario));
						plantillaEstadoCuentaFacultativo.setTituloEstadoCuenta(tituloEstadoCuenta);
					} catch (Exception e) {
						LogDeMidasWeb.log("ReporteEstadoCuentaFacultativo.obtenerReporte(...). Ocurri� un error al consultar los datos del estado de cuenta facultativo.", Level.SEVERE, e);
					}
					plantilla = plantillaEstadoCuentaFacultativo.obtenerReporte(claveUsuario);
				}
				else{
					PL001_ConceptosPorEstadoCuenta plantillaEstadoCuentaAutomaticos = new PL001_ConceptosPorEstadoCuenta(estadoCuentaDecoradoDTO, false);
					plantillaEstadoCuentaAutomaticos.setTipoReporte(tipoReporte);
					plantilla = plantillaEstadoCuentaAutomaticos.obtenerReporte(claveUsuario);
				}
				super.getListaPlantillas().add(plantilla);
			}
		}
		return super.obtenerReporte(claveUsuario,false);
	}

	public boolean isImprimirFormatoAutomaticos() {
		return imprimirFormatoAutomaticos;
	}

	public void setImprimirFormatoAutomaticos(boolean imprimirFormatoAutomaticos) {
		this.imprimirFormatoAutomaticos = imprimirFormatoAutomaticos;
	}
}
