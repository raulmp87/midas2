package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JasperReport;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PlantillaPerfilCartera extends MidasPlantillaBase{
	
	private Date fechaInicio;
	private Date fechaFin;
	private Double tipoCambio;
	private Integer tipoReporte;
	
	public PlantillaPerfilCartera(Date fechaInicio, Date fechaFin,Double tipoCambio,Integer tipoReporte) {
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.tipoCambio = tipoCambio;
		this.tipoReporte = tipoReporte;
		inicializarDatosPlantilla();
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporte(claveUsuario);
		return getByteArrayReport();
	}
	
	private void generarReporte(String claveUsuario) throws SystemException{
		if (this.fechaInicio != null && fechaFin != null && tipoCambio != null){
			
			poblarParametrosPlantilla(claveUsuario);
			
			try{
				
				List<AgrupacionSubRamoPerfilCarteraDTO> listaRegistros = ReportePerfilCarteraDN.getInstancia().
					obtenerRegistrosAgrupadosPerfilCartera(fechaInicio, fechaFin, tipoCambio, tipoReporte, claveUsuario);
			
				getListaRegistrosContenido().clear();
				
				if(listaRegistros != null && !listaRegistros.isEmpty()){
					for(AgrupacionSubRamoPerfilCarteraDTO registro : listaRegistros){
						getListaRegistrosContenido().add(registro);
					}
				}
				else{
					generarLogPlantillaSinDatosParaMostrar();
				}
				
				super.finalizarReporte();
			}
			catch(Exception e){
				throw new SystemException(e);
			}
		}
	}
	

	private void poblarParametrosPlantilla(String claveUsuario) {
		
		getParametrosVariablesReporte().put("FECHA_INICIO", fechaInicio);
		getParametrosVariablesReporte().put("FECHA_FIN", fechaFin);
		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
		getParametrosVariablesReporte().put("TIPO_CAMBIO", tipoCambio);
		
		String nombrePlantillaSubReporte = getPaquetePlantilla()+ "pl_RepPerfilCarteraSubReporteRangos.jrxml";
		JasperReport subReporteRangos = getJasperReport(nombrePlantillaSubReporte);
		if(subReporteRangos == null)
			generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
		getParametrosVariablesReporte().put("SUBREPORTE_RANGOS", subReporteRangos);
		
		getParametrosVariablesReporte().put("SUBTITULO_REPORTE", "P&oacute;lizas emitidas del <font color=\"RED\">"+
				obtenerDescripcionRango()+"</font> y vigentes al <font color=\"RED\">"+obtenerDescripcionFecha(fechaFin,true)+"</font>");
	}
	private static final String[] meses = {"enero","febrero","marzo","abril","mayo",
			"junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
	
	private String obtenerDescripcionRango(){
		
		
		Calendar calendarInicio = Calendar.getInstance();
		Calendar calendarFin = Calendar.getInstance();
		
		calendarInicio.setTime(fechaInicio);
		calendarFin.setTime(fechaFin);
		
		String descripcionRango = "";
		
		descripcionRango += obtenerDescripcionFecha(fechaInicio,
				(calendarInicio.get(Calendar.YEAR) != calendarFin.get(Calendar.YEAR)));
		
		descripcionRango += " al "+obtenerDescripcionFecha(fechaFin,true);
		
		
		return descripcionRango;
	}
	
	private String obtenerDescripcionFecha(Date fecha,boolean incluirAnio){
		Calendar calendarTMP = Calendar.getInstance();
		calendarTMP.setTime(fecha);
		
		String descripcionFecha = ""+ calendarTMP.get(Calendar.DATE)+" de "+
				meses[calendarTMP.get(Calendar.MONTH)];
		if(incluirAnio)
			descripcionFecha += " de "+calendarTMP.get(Calendar.YEAR);
		
		return descripcionFecha;
	}
	
	private void inicializarDatosPlantilla(){
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.reaseguro.reporte.plantillaPerfilCartera.paquete"));
		setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.reaseguro.reporte.plantillaPerfilCartera"));
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte( Sistema.TIPO_XLS );
	}
}
