package mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.SaldoConceptoDTO;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.PlantillaEstadoCuentaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class PL002_ConceptosPorEstadoCuentaFacultativo extends PlantillaEstadoCuentaBase{
	
	private String tituloEstadoCuenta = "";
	private EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO;
	

	public PL002_ConceptosPorEstadoCuentaFacultativo(Map<String,Object> mapaParametrosPlantilla){
		inicializaDatosPlantilla(mapaParametrosPlantilla);
	}
	
	public PL002_ConceptosPorEstadoCuentaFacultativo(Map<String,Object> mapaParametrosPlantilla,String tipoReporte){
		inicializaDatosPlantilla(mapaParametrosPlantilla);
		if(tipoReporte != null)
			setTipoReporte(tipoReporte);
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarPlantillaEstadoCuenta(claveUsuario);
		return getByteArrayReport();
	}
	
	private void generarPlantillaEstadoCuenta(String nombreUsuario){
		if(estadoCuentaDecoradoDTO != null){
			
			//buscar el saldo t�cnico inicial
			SaldoConceptoDTO saldoTotalAnterior = null;
			SaldoConceptoDTO saldoTotalPeriodo = null;
			SaldoConceptoDTO saldoTecnicoPeriodo = null;
			
			for(SaldoConceptoDTO saldoTMP : estadoCuentaDecoradoDTO.getSaldosAcumulados()){
				//Se agrega el registro a la lista de la plantilla
				super.getListaRegistrosContenido().add(saldoTMP);
				if(saldoTMP.getDescripcionConcepto().indexOf("Saldo Anterior") != -1){//Si es un saldo anterior
					if(saldoTotalAnterior == null){
						saldoTotalAnterior = new SaldoConceptoDTO();
						saldoTotalAnterior.setDebe(saldoTMP.getDebe());
						saldoTotalAnterior.setHaber(saldoTMP.getHaber());
						saldoTotalAnterior.setSaldo(saldoTMP.getSaldo());
						saldoTotalAnterior.setSaldoAcumulado(saldoTMP.getSaldoAcumulado());
					}
					else{
						BigDecimal nuevoDebeAnterior = obtenerValorBigDecimal(saldoTotalAnterior.getDebe()).add(obtenerValorBigDecimal(saldoTMP.getDebe()));
						BigDecimal nuevoHaberAnterior = obtenerValorBigDecimal(saldoTotalAnterior.getHaber()).add(obtenerValorBigDecimal(saldoTMP.getHaber()));
						BigDecimal nuevoSaldoTecnicoAnterior = nuevoHaberAnterior.subtract(nuevoDebeAnterior.abs());
						saldoTotalAnterior.setDebe(nuevoDebeAnterior);
						saldoTotalAnterior.setHaber(nuevoHaberAnterior);
						saldoTotalAnterior.setSaldo(nuevoSaldoTecnicoAnterior);
					}
				}
				else{//Si no es saldo anterior, acumularlo como saldo del periodo.
					if(saldoTotalPeriodo == null){
						saldoTotalPeriodo = new SaldoConceptoDTO();
						saldoTotalPeriodo.setDebe(saldoTMP.getDebe());
						saldoTotalPeriodo.setHaber(saldoTMP.getHaber());
						saldoTotalPeriodo.setSaldo(saldoTMP.getSaldo());
						saldoTotalPeriodo.setSaldoAcumulado(saldoTMP.getSaldoAcumulado());
					}
					else{
						BigDecimal nuevoDebeAnterior = obtenerValorBigDecimal(saldoTotalPeriodo.getDebe()).add(obtenerValorBigDecimal(saldoTMP.getDebe()));
						BigDecimal nuevoHaberAnterior = obtenerValorBigDecimal(saldoTotalPeriodo.getHaber()).add(obtenerValorBigDecimal(saldoTMP.getHaber()));
						BigDecimal nuevoSaldoTecnicoAnterior = nuevoHaberAnterior.subtract(nuevoDebeAnterior.abs());
						saldoTotalPeriodo.setDebe(nuevoDebeAnterior);
						saldoTotalPeriodo.setHaber(nuevoHaberAnterior);
						saldoTotalPeriodo.setSaldo(nuevoSaldoTecnicoAnterior);
					}
				}
			}
			
			if(saldoTotalAnterior == null){
				saldoTotalAnterior = new SaldoConceptoDTO();
			}
			saldoTotalAnterior.setDebe(obtenerValorBigDecimal(saldoTotalAnterior.getDebe()));
			saldoTotalAnterior.setHaber(obtenerValorBigDecimal(saldoTotalAnterior.getHaber()));
			saldoTotalPeriodo.setDebe(obtenerValorBigDecimal(saldoTotalPeriodo.getDebe()));
			saldoTotalPeriodo.setHaber(obtenerValorBigDecimal(saldoTotalPeriodo.getHaber()));
			
			saldoTecnicoPeriodo = new SaldoConceptoDTO();
			saldoTecnicoPeriodo.setDebe(saldoTotalPeriodo.getDebe().abs().add(saldoTotalAnterior.getDebe().abs()));
			saldoTecnicoPeriodo.setHaber(saldoTotalPeriodo.getHaber().abs().add(saldoTotalAnterior.getHaber().abs()));
			saldoTecnicoPeriodo.setSaldo(saldoTecnicoPeriodo.getHaber().abs().subtract(saldoTecnicoPeriodo.getDebe()).abs());
			
			super.getParametrosVariablesReporte().put("TOTAL_DEBE_ANTERIOR", saldoTotalAnterior.getDebe());
			super.getParametrosVariablesReporte().put("TOTAL_HABER_ANTERIOR", saldoTotalAnterior.getHaber());
			super.getParametrosVariablesReporte().put("TOTAL_DEBE_PERIODO", saldoTotalPeriodo.getDebe());
			super.getParametrosVariablesReporte().put("TOTAL_HABER_PERIODO", saldoTotalPeriodo.getHaber());
			super.getParametrosVariablesReporte().put("TOTAL_DEBE_TECNICO", saldoTecnicoPeriodo.getDebe());
			super.getParametrosVariablesReporte().put("TOTAL_HABER_TECNICO", saldoTecnicoPeriodo.getHaber());
			super.getParametrosVariablesReporte().put("TOTAL_SALDO_TECNICO", saldoTecnicoPeriodo.getSaldo());
			
			//Generar subreporte de datos del estado de cuenta
			JasperReport subReporteDatosEstadoCuenta= getJasperReport(getPaquetePlantilla()+nombrePlantillaSubReporteDatosEstadoCuenta);
			if(subReporteDatosEstadoCuenta == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporteDatosEstadoCuenta, null);
			getParametrosVariablesReporte().put("SUBREPORTE_DATOS_ESTADO_CUENTA", subReporteDatosEstadoCuenta);
			getParametrosVariablesReporte().put("DATASOURCE_SUBREPORTE_DATOS_EDO_CTA", new JRBeanCollectionDataSource(listaDatosEstadoCuenta));
			
			if(super.getListaRegistrosContenido() == null || super.getListaRegistrosContenido().isEmpty()){
				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
			}
			else{
				try {
					super.setByteArrayReport( generaReporte(super.getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
							getParametrosVariablesReporte(), getListaRegistrosContenido()));
				} catch (JRException e) {
					setByteArrayReport( null );
					generarLogErrorCompilacionPlantilla(e);
				}
			}
		}
	}
	
	private void inicializaDatosPlantilla(Map<String,Object> mapaParametrosPlantilla){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldosPorConceptoFacultativo"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reporte.estadoCuenta.facultativo.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_PDF);
		nombrePlantillaSubReporteDatosEstadoCuenta = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.subReporteDatosEstadoCuenta");
		if(mapaParametrosPlantilla != null)
			super.getParametrosVariablesReporte().putAll(mapaParametrosPlantilla);
	}
	
	private BigDecimal obtenerValorBigDecimal(BigDecimal Valor){
		return Valor == null ? BigDecimal.ZERO : Valor;
	}

	public EstadoCuentaDecoradoDTO getEstadoCuentaDecoradoDTO() {
		return estadoCuentaDecoradoDTO;
	}

	public void setEstadoCuentaDecoradoDTO(
			EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO) {
		this.estadoCuentaDecoradoDTO = estadoCuentaDecoradoDTO;
	}

	public String getTituloEstadoCuenta() {
		return tituloEstadoCuenta;
	}

	public void setTituloEstadoCuenta(String tituloEstadoCuenta) {
		this.tituloEstadoCuenta = tituloEstadoCuenta;
		if(getParametrosVariablesReporte() != null)
			getParametrosVariablesReporte().put("TITULO_ESTADO_CUENTA", tituloEstadoCuenta);
	}
}
