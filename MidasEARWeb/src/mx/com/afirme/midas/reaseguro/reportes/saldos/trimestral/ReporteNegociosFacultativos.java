package mx.com.afirme.midas.reaseguro.reportes.saldos.trimestral;

import java.math.BigDecimal;
import java.util.ArrayList;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteNegociosFacultativos extends MidasReporteBase {

	private BigDecimal idTcReasegurador;
	private Integer ejercicio;
	private Integer suscripcion;
	private boolean excluirSaldoCorredor = true;
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		PL013_NegociosFacultativosTrimestre plantillaFacultativos = new PL013_NegociosFacultativosTrimestre();
		plantillaFacultativos.setIdTcReasegurador(idTcReasegurador);
		plantillaFacultativos.setEjercicio(ejercicio);
		plantillaFacultativos.setSuscripcion(suscripcion);
		plantillaFacultativos.setExcluirSaldoCorredor(excluirSaldoCorredor);
		
		setListaPlantillas(new ArrayList<byte[]>());
		getListaPlantillas().add(plantillaFacultativos.obtenerReporte(claveUsuario));
		
		return super.obtenerReporte(claveUsuario, false);
	}

	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}

	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSuscripcion() {
		return suscripcion;
	}

	public void setSuscripcion(Integer suscripcion) {
		this.suscripcion = suscripcion;
	}

	public boolean isExcluirSaldoCorredor() {
		return excluirSaldoCorredor;
	}

	public void setExcluirSaldoCorredor(boolean excluirSaldoCorredor) {
		this.excluirSaldoCorredor = excluirSaldoCorredor;
	}
}
