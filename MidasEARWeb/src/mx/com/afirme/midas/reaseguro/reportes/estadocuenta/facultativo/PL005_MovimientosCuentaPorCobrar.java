package mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.PlantillaEstadoCuentaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

/**
 * @author Jose Luis Arellano
 *
 */
public class PL005_MovimientosCuentaPorCobrar extends PlantillaEstadoCuentaBase{

	private String tituloReporte;
	
	public PL005_MovimientosCuentaPorCobrar(Map<String,Object> mapaParametrosPlantilla){
		inicializaDatosPlantilla(mapaParametrosPlantilla);
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarPlantillaMovimientos(claveUsuario);
		return getByteArrayReport();
	}

	private void generarPlantillaMovimientos(String nombreUsuario) throws SystemException{
		
		//Generar subreporte de datos del estado de cuenta
		JasperReport subReporteDatosEstadoCuenta= getJasperReport(getPaquetePlantilla()+nombrePlantillaSubReporteDatosEstadoCuenta);
		if(subReporteDatosEstadoCuenta == null)
			generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporteDatosEstadoCuenta, null);
		getParametrosVariablesReporte().put("SUBREPORTE_DATOS_ESTADO_CUENTA", subReporteDatosEstadoCuenta);
		getParametrosVariablesReporte().put("DATASOURCE_SUBREPORTE_DATOS_EDO_CTA", new JRBeanCollectionDataSource(listaDatosEstadoCuenta));
		
		//Generar subreporte de movimientos
		String nombrePlantillaSubreporteMovimientos = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.subReporteMovimientosEstadoCuentaCxC");
		JasperReport subReporteMovimientosEstadoCuenta= getJasperReport(getPaquetePlantilla()+nombrePlantillaSubreporteMovimientos);
		getParametrosVariablesReporte().put("SUBREPORTE_MOVIMIENTOS_ESTADO_CUENTA", subReporteMovimientosEstadoCuenta);
		
		if(super.getListaRegistrosContenido() == null || super.getListaRegistrosContenido().isEmpty()){
			setByteArrayReport(null);
			generarLogPlantillaSinDatosParaMostrar();
		}
		else{
			try {
				super.setByteArrayReport( generaReporte(super.getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
						getParametrosVariablesReporte(), getListaRegistrosContenido()));
			} catch (JRException e) {
				setByteArrayReport( null );
				generarLogErrorCompilacionPlantilla(e);
			}
		}
	}
	
	private void inicializaDatosPlantilla(Map<String,Object> mapaParametrosPlantilla){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.movimientosSoporteCuentaPorCobrar"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reporte.estadoCuenta.facultativo.paquete"));
		nombrePlantillaSubReporteDatosEstadoCuenta = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.subReporteDatosEstadoCuentaCxP");
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
		if(mapaParametrosPlantilla != null)
			super.getParametrosVariablesReporte().putAll(mapaParametrosPlantilla);
	}

	//Getters y Setters
	public String getTituloReporte() {
		return tituloReporte;
	}
	public void setTituloReporte(String tituloReporte) {
		if(getParametrosVariablesReporte() != null){
			getParametrosVariablesReporte().put("TITULO_ESTADO_CUENTA", tituloReporte);
		}
		this.tituloReporte = tituloReporte;
	}
}
