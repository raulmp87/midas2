package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReporteRCSContraparteAction extends MidasMappingDispatchAction{
	public static final int BUFFERMAX = 1024;
	public static final int BUFFERMIN = 15;
	public static final String EXITOSO = "exitoso";
	public static final String USUARIO_ACCESO_MIDAS = "usuarioAccesoMIDAS";
	public static final String RIESGO_CXL = "CXL";
	public static final String RIESGO_IMPREC = "ImpRec";
	public static final String EXTENSION = ".txt";
	public static final String ERROR = "errorImpresion";
	public static final String ARCHIVOSCARGADOS = "SINIESTROS_PENDIENTES, POLIZAS_FACULTADAS, CONTRATOS_AUTOMATICOS, SISE, AJUSTES";
	public static final String INICIAR = "0";
	public static final int PROCESAR = 1;
	public static final int REPROCESAR = 2;
	private FileManagerService fileManagerService;
	public static final String TIPO_ZIP = "application/zip";
	
	public ActionForward mostrarRCSContraparte(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {
		
		return mapping.findForward(EXITOSO);
	}
	
	/**
	 * Genera informacion de los reportes Contraparte.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */	
	public void generarReporte(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		StringBuffer resultado = new StringBuffer();
		String fechaCorte = request.getParameter("fechaCorte");
		String fileName = getNombreArchivo(fechaCorte);
		BigDecimal tipoCambio = new BigDecimal(request.getParameter("tipoCambio"));
		int registros = 0;
		
		try {
			
			String nombreUsuario = obtieneNombreUsuario(request); 
			List<ReporteRCSContraparteDTO> lista = ReporteRCSContraparteDN.getInstancia().obtenerReporte(fechaCorte, tipoCambio, nombreUsuario);
			
			if(lista == null)
				throw new SystemException ("Ocurrio un error al recuperar la informacion para el reporte.");
			else if(lista.isEmpty())
			{
				ReporteRCSContraparteDTO reporteRCSContraparteDTO = new ReporteRCSContraparteDTO();
				reporteRCSContraparteDTO.setResultado("No se encontraron registros para los datos introducidos");
				lista.add(reporteRCSContraparteDTO);
			}
			
			for(ReporteRCSContraparteDTO item:lista){
				if(registros != lista.size()-1){
			        resultado.append(item.getResultado()+"\r\n");
				}else{
					resultado.append(item.getResultado());
				}
				registros++;
			}
			
			byte[] file = resultado.toString().getBytes();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] excelFile = obtenerDesgloce(fechaCorte, tipoCambio).toByteArray();
			byte[] integracionFile = obtenerIntegracion(fechaCorte, tipoCambio).toByteArray();
			byte[] integracionNRS = obtenerIntegracionNRS(fechaCorte, tipoCambio).toByteArray();
			ZipOutputStream zos = new ZipOutputStream(baos);
			
			ZipEntry entryTXT = new ZipEntry(fileName +".txt");
			entryTXT.setSize(file.length);
			zos.putNextEntry(entryTXT);
			zos.write(file);
			zos.closeEntry();
			
			ZipEntry desgloseEXCEL = new ZipEntry("Desgloce Contraparte" +".xls");
			desgloseEXCEL.setSize(excelFile.length);
			zos.putNextEntry(desgloseEXCEL);
			zos.write(excelFile);
			zos.closeEntry();
			
			ZipEntry integracionEXCEL = new ZipEntry("Integracion Contraparte" +".xls");
			integracionEXCEL.setSize(integracionFile.length);
			zos.putNextEntry(integracionEXCEL);
			zos.write(integracionFile);
			zos.closeEntry();
			
			ZipEntry integracionNRs = new ZipEntry("Integracion NRs" +".xls");
			integracionNRs.setSize(integracionNRS.length);
			zos.putNextEntry(integracionNRs);
			zos.write(integracionNRS);
			zos.closeEntry();
			
			zos.close();
			this.writeBytes(response, baos.toByteArray(), TIPO_ZIP,fileName+ ".zip");
			baos.close();
			
					
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			}		
		
	}
	
	/**
	 * Procesa informacion de los reportes ContraParte, Se crea Tread, debido a que 
	 * esos reportes tardan mucho tiempo en ser generados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward procesarReporteContraParte(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		Date fechaCorte = null;		
		String fechaFin = request.getParameter("fechaCorte");
		BigDecimal tipoCambio = new BigDecimal(request.getParameter("tipoCambio"));
		int accion = Integer.parseInt(request.getParameter("accion"));
		String forward = "success";		
		String fileName = getNombreArchivo(fechaFin);
		
		try {
			fechaCorte = UtileriasWeb.getFechaFromString(fechaFin);
		} catch (ParseException ex) {
			LogDeMidasWeb.log("Excepci&oacute;n obteniendo fecha del Reporte ContraParte: " + ex.getMessage(), Level.ALL, ex);			
		}
		
		try {
			
			String nombreUsuario = obtieneNombreUsuario(request); 
			int registros = ReporteRCSContraparteDN.getInstancia().obtenerRegistros(fechaCorte);
			
			if(registros == 0){
				// Ejecuta el thread para procesar la informacion del reporte.				
				ejecutarThread(fechaFin, tipoCambio, nombreUsuario);
				try {
					response.getWriter().write(INICIAR);
					
			  		}catch (IOException e) {	
			  		 throw new SystemException ("Ocurri&oacute; un error al recuperar la informaci&oacute;n del reporte Contraparte con fecha de corte: "+fechaFin+" ... ", e);
			  		}
			}else if(registros > 0){			
			    if(accion == PROCESAR){
					try {
						response.getWriter().write("Ya existe Informaci&oacute;n del Corte: "+ fechaFin+ " para el reporte Contraparte. \n");
						
				  		}catch (IOException e) {	
				  		 throw new SystemException ("Ocurri&oacute; un error al recuperar la informaci&oacute;n del reporte Contraparte con fecha de corte: "+fechaFin+" ... ", e);
				  		}
				}else if (accion == REPROCESAR){
					// Ejecuta el thread para reprocesar la informacion del reporte.
					ejecutarThread(fechaFin, tipoCambio, nombreUsuario);
					forward = EXITOSO;
				}
					
			  }
		   }catch (SystemException e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				mapping.findForward(ERROR);				
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Error obteniendo el archivo "+fileName+"...", Level.SEVERE, e);
				request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
				request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
				mapping.findForward(ERROR);
			}
				
		return mapping.findForward(forward);
	}
	
	/**
	 * Lista los reportes cargados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward listarDocumentos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			
			String fechaCorte = request.getParameter("fechaCorte");
			
			List<ReporteRCSContraparteDTO> contratos = new ArrayList<ReporteRCSContraparteDTO>();
			ReporteRCSContraparteDN reporteContraParteDN = ReporteRCSContraparteDN.getInstancia();
						
			contratos = reporteContraParteDN.listarTodos(fechaCorte);
					
			request.setAttribute("documentos", contratos);
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		} catch (ExcepcionDeAccesoADatos e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}	
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Elimina registro por el <code>id</code> del reporte
	 * @param mapping
	 * @param form
	 * @param request
	 * @param responsereporte/borrarDocumento
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward borrarDocumento(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			String id = request.getParameter("id");
			
			ReporteRCSContraparteDN reporteRCSContraparteDN = ReporteRCSContraparteDN.getInstancia();
			ReporteRCSContraparteDTO reporteRCSContraparteDTO = reporteRCSContraparteDN.getPorId(Integer.valueOf(id));
			
			reporteRCSContraparteDN.borrar(reporteRCSContraparteDTO);
			return this.listarDocumentos(mapping, form, request, response);
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error al borrar el registro ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Error al borrar el registro ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Lista los estatus de los reportes cargados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param responsereporte
	 * @return mapping.findForward(reglaNavegacion).
	 */
	
	public ActionForward listarCargas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = EXITOSO;
		try {
			
			String fechaCorte = request.getParameter("fechaFinal");
			
			List<CargaContraParteDTO> contratos = new ArrayList<CargaContraParteDTO>();
			ReporteRCSContraparteDN reporteRCSContraparteDN = ReporteRCSContraparteDN.getInstancia();
						
			contratos = reporteRCSContraparteDN.listarCargas(fechaCorte);
					
			request.setAttribute("cargas", contratos);
		} catch (SystemException e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		} catch (ExcepcionDeAccesoADatos e) {
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}	
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Descarga los reportes cargados.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward descargarReporte(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = EXITOSO;
		
		try {
			fileManagerService = ServiceLocator.getInstance().getEJB(
					FileManagerService.class);

		} catch (Exception e) {
			LogDeMidasWeb.log("Error en reporte Contraparte Action: ",Level.SEVERE, e);
			reglaNavegacion = ERROR;
		}	

		try {

			final String idControlArchivo = request.getParameter("idControlArchivo");
			final ControlArchivoDTO controlArchivoDTO = ControlArchivoDN
					.getInstancia().getPorId(new BigDecimal(idControlArchivo));
			final String fileName = fileManagerService.getFileName(controlArchivoDTO);
			byte[] byteArray = fileManagerService.downloadFile(fileName, idControlArchivo);
			response.setHeader("Content-Disposition", "attachment; filename="
					+ URLEncoder.encode(controlArchivoDTO.getNombreArchivoOriginal(), "ISO-8859-1"));
			response.setHeader("Cache-Control", "no-cache");
			response.setContentType("application/vnd.ms-excel");
			response.setContentLength(byteArray.length);
			response.setBufferSize(BUFFERMAX * BUFFERMIN);
			
			OutputStream output = response.getOutputStream(); 
			output.write(byteArray);
			output.flush();
			output.close();
			
			
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al descargar el archivo.", Level.SEVERE, e);
			reglaNavegacion = ERROR;
		} 
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Actualiza estatus de carga de los reportes cargados - Bloquea o Desbloquea carga.
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return mapping.findForward(reglaNavegacion).
	 */
	public ActionForward actualizarEstatusCarga(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		    String reglaNavegacion = EXITOSO;
		try {
			int id = Integer.valueOf(request.getParameter("id"));
			int estatusCarga = Integer.valueOf(request.getParameter("estatusCarga"));
			
			ReporteRCSContraparteDN reporteRCSContraparteDN = ReporteRCSContraparteDN.getInstancia();
			
			reporteRCSContraparteDN.actualizarEstatusCarga(id, estatusCarga);
			return this.listarCargas(mapping, form, request, response);
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error al actualizar el estatus de carga ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede actualizar la carga del.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Error al actualizar el estatus de carga ...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede actualizar la carga del reporte.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br><h3>"+e.getMessage()+"</h3>");
			reglaNavegacion = ERROR;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Ejecuta el Thread encargado de ejecutar el Stored de cada reporte.
	 * @param fechaIni
	 * @param fechaFin
	 * @param tipoCambio
	 * @param reporte
	 * @param nombreUsuario
	 */
	public void ejecutarThread(String fechaCorte, BigDecimal tipoCambio, String nombreUsuario) {
		try {
			ReporteContraParteGenerador reporteContraParte = new ReporteContraParteGenerador();
			reporteContraParte.setFechaCorte(fechaCorte);
			reporteContraParte.setTipoCambio(tipoCambio);
			reporteContraParte.setNombreUsuario(nombreUsuario);
			
			Thread hiloGenerador = new Thread(reporteContraParte);
			hiloGenerador.start();	
		}
		catch (Exception ex){
			LogDeMidasWeb.log("Excepci&oacute;n en Proceso Generador del Reporte ContraParte: "
					+ ex.getMessage(), Level.ALL, ex);
		}
	}
	
	
	/**
	 * Obtiene los archivos que necesitan ser cargados para el reporte a ser generado
	 * 
	 * @param fecha
	 *            Fecha de Corte
	 * @param tipoArchivo
	 *            El tipo de archivo a ser generardo
	 * @return Los archivos a cargar
	 */
	public ActionForward getArchivosPorCargar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws SystemException {
		
		Date fechaFinal = null;
		StringBuffer reporteCarga = new StringBuffer();
		int registros = 0;
		String forward = "success";
		String msn = "Archivos de carga para ese reporte:<br>";
		String cargado = "Archivo(s) cargado(s):<br>";
		
		List<String> lista = null;
		try {
			
			String fechaCorte = request.getParameter("fechaCorte");
					fechaFinal = UtileriasWeb.getFechaFromString(fechaCorte);
					lista = ReporteRCSContraparteDN.getInstancia().obtenerArchivos(fechaFinal);
				
				
				if(lista == null){
					throw new SystemException ("Ocurrio un error obteniendo los archivos de carga del Contraparte.");
				}else if(lista.isEmpty()){
					
					response.getWriter().write("<b>"+msn+"</b>"+ARCHIVOSCARGADOS+"<br>"+"<b>"+cargado+"</b>"+"<p style=\"color:red;display: inline;\">No hay archivo(s) cargado(s).</p>");				
				}		
			
						
			for(String reporte : lista){
				
					if(registros != lista.size()-1){
						reporteCarga.append(reporte+", ");
					}else{
						reporteCarga.append(reporte);
					}
					registros++;
			}
		
			if(reporteCarga.length()>0){
									
				response.getWriter().write("<b>"+msn+"</b>"+ARCHIVOSCARGADOS+"<br>"+"<b>"+cargado+"</b>"+"<p style=\"color:green;display: inline;\">"+reporteCarga+"</p>");			
			
		  }
			
		} catch (SystemException e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del Contraparte...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (IOException e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del Contraparte...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del Contraparte...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
		} catch (ParseException e) {
			LogDeMidasWeb.log("Error obteniendo los archivos de carga del Contraparte...", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede generar el reporte.");
			request.setAttribute("leyenda", "Notifique al administrador lo siguiente:<br>"+e.getMessage());
			forward = ERROR;
		}
		
		return mapping.findForward(forward);
	}
	
	
	
	/**
	 * Obtiene el nombre del usuario de Midas registrado
	 * 
	 * @param request
	 *            Request del servlet
	 * @return El nombre del usuario de Midas registrado
	 */
	public static String obtieneNombreUsuario(HttpServletRequest request) {

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, USUARIO_ACCESO_MIDAS);

		if (usuario != null) {
			return usuario.getNombreUsuario();
		}

		return "anonimo";
	}
	/**
	 * Obtiene el nombre del archivo a ser generado
	 * 
	 * @param texto
	 *            Estructura vigente para el nombre del archivo
	 * @param fecha
	 *            Fecha de Corte
	 * @param tipoArchivo
	 *            El tipo de archivo a ser generardo de acuerdo al riesgo
	 * @return El nombre del archivo
	 */
	public static String getNombreArchivo(String fecha) {
		String fechaModificada = fecha;
		String dia = fechaModificada.substring(0,2);
		String mes = fechaModificada.substring(3,5);
		String anio = fechaModificada.substring(6,10);
		String fechaCorte = anio+mes+dia;
		StringBuilder nomenclatura = new StringBuilder();
		nomenclatura.append("S0904_");
		nomenclatura.append(fechaCorte);
		nomenclatura.append("_ImpRec");
		nomenclatura.append(EXTENSION);
	
		
		return nomenclatura.toString();
	}
	
	/**
	 * Genera el reporte en formato excel.
	 * @param fechaCorte
	 * @return ByteArrayOutputStream
	 */	
	public ByteArrayOutputStream  obtenerDesgloce(String fechaFin, BigDecimal tipoCambio) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String[] tituloCabecera = {"CONCEPTO","RESERVA ORIGEN","RESERVA PESOS"};
		Date fechaFinal = null;
		List<Object[]> lista = null;
		try {
				fechaFinal = UtileriasWeb.getFechaFromString(fechaFin);					
				lista = ReporteRCSContraparteDN.getInstancia().obtenerDesgloce(fechaFinal, tipoCambio);				
			
			} catch (ParseException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n al convertir la fecha del Reporte: " + ex.getMessage(), Level.ALL, ex);			
			} catch (SystemException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n obteniendo fecha del Reporte: " + ex.getMessage(), Level.ALL, ex);			
			}
			
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet hoja = workbook.createSheet("REPORTE CONTRAPARTE");  
            HSSFRow cabecera = hoja.createRow((short)0);
	        	        
            Cell cell = null;
            HSSFRow hssfRow = null;
            
            setCobinarCeldas(workbook, cabecera, hoja, cell, tituloCabecera);
           
                    	
        	int rowIdx = 1;
        	
        	 for(Object[] reporte : lista){
 	        	hssfRow = hoja.createRow((short)rowIdx++);
 	        	
 	        	for (int celdas = 0; celdas < reporte.length; celdas++) 
   			    {
 	        		cell = hssfRow.createCell(celdas);
 	        		if(celdas == 0){ 	        		
 		        		cell.setCellValue(reporte[celdas].toString());
 	        		}else{
 	        			cell.setCellValue(Double.valueOf(reporte[celdas].toString())); 	        			
 	        		}
 	        		cell.setCellStyle(getEstiloCeldas(workbook));
 		        	hoja.autoSizeColumn(celdas, true);
   			    }
 	        }
        	 	    		
	        workbook.write(outputStream);
	        
		} catch ( Exception ex ) {
			LogDeMidasWeb.log("Excepci&oacute;n al generar platilla de carga del Reporte Contraparte: "
					+ ex.getMessage(), Level.ALL, ex);
        }finally {
		
		if (outputStream != null) {
			try {
				outputStream.flush();
		        outputStream.close();
				
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo plantilla RR6.", Level.SEVERE, e);				
			}
		}
		
	}
				
		return outputStream;
	}
	
	/**
	 * Genera el reporte con la integracion de los conceptos en formato excel.
	 * @param fechaCorte
	 * @return ByteArrayOutputStream
	 */	
	public ByteArrayOutputStream  obtenerIntegracion(String fechaFin, BigDecimal tipoCambio) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String[] tituloCabecera = {"SINIESTRO","REASEGURADOR","CNSF","MONEDA","RESERVA"};
		Date fechaFinal = null;
		List<Object[]> listaSise = null;
		List<Object[]> listaVida = null;
		List<Object[]> listaMidas = null;
					
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sise = workbook.createSheet("SISE");  
            HSSFRow cabeceraSise = sise.createRow((short)0);
            HSSFSheet vida = workbook.createSheet("VIDA");  
            HSSFRow cabeceraVida = vida.createRow((short)0);
            HSSFSheet midas = workbook.createSheet("MIDAS");  
            HSSFRow cabeceraMidas = midas.createRow((short)0);
            
            try {
				fechaFinal = UtileriasWeb.getFechaFromString(fechaFin);					
				listaSise = ReporteRCSContraparteDN.getInstancia().obtenerIntegracion(fechaFinal, tipoCambio, sise.getSheetName());
				listaVida = ReporteRCSContraparteDN.getInstancia().obtenerIntegracion(fechaFinal, tipoCambio, vida.getSheetName());
				listaMidas = ReporteRCSContraparteDN.getInstancia().obtenerIntegracion(fechaFinal, tipoCambio, midas.getSheetName());
			
			} catch (ParseException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n al convertir la fecha del Reporte: " + ex.getMessage(), Level.ALL, ex);			
			} catch (SystemException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n obteniendo fecha del Reporte: " + ex.getMessage(), Level.ALL, ex);			
			}
	        
            Cell cell = null;
                        
            setHojas(workbook, cabeceraSise, sise, cell, tituloCabecera, listaSise);
            setHojas(workbook, cabeceraVida, vida, cell, tituloCabecera, listaVida);
            setHojas(workbook, cabeceraMidas, midas, cell, tituloCabecera, listaMidas);
            
                    	
            workbook.write(outputStream);
	        
		} catch ( Exception ex ) {
			LogDeMidasWeb.log("Excepci&oacute;n al generar platilla de carga del Reporte Contraparte: "
					+ ex.getMessage(), Level.ALL, ex);
        }finally {
		
		if (outputStream != null) {
			try {
				outputStream.flush();
		        outputStream.close();
				
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo plantilla RR6.", Level.SEVERE, e);				
			}
		}
		
	}
				
		return outputStream;
	}
	
	
	/**
	 * Genera el reporte en formato excel.
	 * @param fechaCorte
	 * @return ByteArrayOutputStream
	 */	
	public ByteArrayOutputStream  obtenerIntegracionNRS(String fechaFin, BigDecimal tipoCambio) {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		String[] tituloCabecera = {"RGRE","REASEGURADOR","CONCEPTO","RESERVA"};
		Date fechaFinal = null;
		List<Object[]> lista = null;
		try {
				fechaFinal = UtileriasWeb.getFechaFromString(fechaFin);					
				lista = ReporteRCSContraparteDN.getInstancia().obtenerIntegracionNRS(fechaFinal, tipoCambio);				
			
			} catch (ParseException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n al convertir la fecha del Reporte: " + ex.getMessage(), Level.ALL, ex);			
			} catch (SystemException ex) {
				LogDeMidasWeb.log("Excepci&oacute;n obteniendo fecha del Reporte: " + ex.getMessage(), Level.ALL, ex);			
			}
			
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet hoja = workbook.createSheet("INTEGRACION NRs");  
            HSSFRow cabecera = hoja.createRow((short)0);
	        	        
            Cell cell = null;
            HSSFRow hssfRow = null;
            
            setCobinarCeldas(workbook, cabecera, hoja, cell, tituloCabecera);
           
                    	
        	int rowIdx = 1;
        	
        	 for(Object[] reporte : lista){
 	        	hssfRow = hoja.createRow((short)rowIdx++);
 	        	
 	        	for (int celdas = 0; celdas < reporte.length; celdas++) 
   			    {
 	        		cell = hssfRow.createCell(celdas);
 	        		if(celdas <= 2){ 	        		
 		        		cell.setCellValue(reporte[celdas].toString());
 	        		}else{
 	        			cell.setCellValue(Double.valueOf(reporte[celdas].toString())); 	        			
 	        		}
 	        		cell.setCellStyle(getEstiloCeldas(workbook));
 		        	hoja.autoSizeColumn(celdas, true);
   			    }
 	        }
        	 	    		
	        workbook.write(outputStream);
	        
		} catch ( Exception ex ) {
			LogDeMidasWeb.log("Excepci&oacute;n al generar platilla de carga del Reporte Contraparte: "
					+ ex.getMessage(), Level.ALL, ex);
        }finally {
		
		if (outputStream != null) {
			try {
				outputStream.flush();
		        outputStream.close();
				
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo plantilla RR6.", Level.SEVERE, e);				
			}
		}
		
	}
				
		return outputStream;
	}
	
	
	/**
	 * Genera el estilo de las celdas.
	 * @param workbook, celdas
	 * @return CellStyle
	 */
	public CellStyle getEstiloCeldas(HSSFWorkbook workbook){
		CellStyle cs = workbook.createCellStyle();
	    
		
		cs.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cs.setDataFormat((short)8);
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		
				    
		return cs;    	
	}
	
	
	
	/**
	 * Genera el estilo de la cabecera.
	 * @param fechaFin
	 * @return CellStyle
	 */
	public CellStyle getEstiloCabecera(HSSFWorkbook workbook){
		HSSFFont font = workbook.createFont();
	    font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    CellStyle cs = workbook.createCellStyle();
	    
	    cs.setFont(font);
	    cs.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
	    cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    cs.setBorderBottom(CellStyle.BORDER_THIN);
	    cs.setBorderTop(CellStyle.BORDER_THIN);
	    cs.setBorderRight(CellStyle.BORDER_THIN);
	    cs.setBorderLeft(CellStyle.BORDER_THIN);
	    cs.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
	    cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	    
		return cs;    	
	}
	
	/**
	 * Conbina las celdas de la cabecera.
	 * @param workbook
	 * @param cabecera
	 * @param hoja
	 * @param cell
	 * @param tituloCabecera
	 */	
	public void setCobinarCeldas(HSSFWorkbook workbook, HSSFRow cabecera, HSSFSheet hoja, Cell cell, String[] tituloCabecera){
		
		for(int i = 0; i < tituloCabecera.length; i++)
	    {
	    	cell = cabecera.createCell(i);
	    	cell.setCellValue(tituloCabecera[i]);
	    	cell.setCellStyle(getEstiloCabecera(workbook));
	    	hoja.autoSizeColumn(i);
	    }	
	}
	
/**
 * Introduce el titulo de la cabecera y llena la hoja.
 * @param workbook
 * @param cabecera
 * @param hoja
 * @param cell
 * @param tituloCabecera
 * @param lista
 */	
	public void setHojas(HSSFWorkbook workbook, HSSFRow cabecera, HSSFSheet hoja, Cell cell, String[] tituloCabecera, List<Object[]> lista){
		cell = null;
		for(int i = 0; i < tituloCabecera.length; i++)
		{
			cell = cabecera.createCell(i);
			cell.setCellValue(tituloCabecera[i]);
			cell.setCellStyle(getEstiloCabecera(workbook));
			hoja.autoSizeColumn(i);
		}

		HSSFRow hssfRow = null;
		Cell cuerpo = null;
		CellStyle cs = workbook.createCellStyle();
		
		int rowIdx = 1;

		cs.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		cs.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setBorderLeft(CellStyle.BORDER_THIN);

		for(Object[] reporte : lista){
			hssfRow = hoja.createRow((short)rowIdx++);

			for (int celdas = 0; celdas < reporte.length; celdas++) 
			{
				cuerpo = hssfRow.createCell(celdas);
				if(celdas != 4){ 	        		
					cuerpo.setCellValue(reporte[celdas].toString());
					cuerpo.setCellStyle(cs);
				}else{
					cuerpo.setCellValue(Double.valueOf(reporte[celdas].toString()));
					cuerpo.setCellStyle(cs);
				}

			}

		}
	}

}
