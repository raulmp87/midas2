package mx.com.afirme.midas.reaseguro.reportes.egresos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.reaseguro.egresos.EgresoReaseguroDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.springframework.util.CollectionUtils;

/**
 * 
 * @author jose luis arellano
 *
 */
public class PL014_SoporteOrdenPago extends MidasPlantillaBase {

	private BigDecimal idEgresoReaseguro;
	private List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones;
	private final EgresoReaseguroDN egresoReaseguroDN = EgresoReaseguroDN.getINSTANCIA();
	
	public PL014_SoporteOrdenPago() {
		inicializarDatosPlantilla();
	}
	
	@Override
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteDesglosePagos();
		return getByteArrayReport();
	}
	
	private void generarReporteDesglosePagos() throws SystemException {
		if (listaParticipaciones == null) {
			listaParticipaciones = egresoReaseguroDN.obtenerConfiguracionPagosEgresoContratoFacultativo(idEgresoReaseguro);
		}
		
		if (!CollectionUtils.isEmpty(listaParticipaciones)) {
			getListaRegistrosContenido().addAll(listaParticipaciones);
		}
		
		finalizarReporte();
	}

	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.soporteOrdenPago"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.soporteOrdenPago.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
	}
	
	public BigDecimal getIdEgresoReaseguro() {
		return idEgresoReaseguro;
	}

	public void setIdEgresoReaseguro(BigDecimal idEgresoReaseguro) {
		this.idEgresoReaseguro = idEgresoReaseguro;
	}

}