package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import java.math.BigDecimal;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;


public class ReporteContraParteGenerador implements Runnable{
	
		
	private String fechaCorte;
	private BigDecimal tipoCambio;
	private String nombreUsuario; 

	public void run() {
		try {
			LogDeMidasWeb.log("Inicia el proceso de ejecucion del reporte ContraParte...", Level.INFO, null);
			ReporteRCSContraparteDN reporteContraParteDN = ReporteRCSContraparteDN.getInstancia();
			reporteContraParteDN.procesarReporte(fechaCorte, tipoCambio, nombreUsuario);
			LogDeMidasWeb.log("Finaliza el proceso de ejecucion del reporte ContraParte...", Level.INFO, null);
		}
		catch (Exception ex){
			LogDeMidasWeb.log("Excepci&oacute;n en Proceso Generador del Reporte ContraParte: "
					+ ex.getMessage(), Level.ALL, ex);
		}
	}

	
	
	public String getFechaCorte() {
		return fechaCorte;
	}
	
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

}
