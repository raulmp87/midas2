package mx.com.afirme.midas.reaseguro.reportes.saldos.trimestral;

import java.math.BigDecimal;
import java.util.ArrayList;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteSaldosTrimestrales extends MidasReporteBase{

	private BigDecimal idTcReasegurador;
	private Integer ejercicio;
	private Integer suscripcion;
	private BigDecimal idTcMoneda;
	private boolean incluirCuotaParte;
	private boolean incluirPrimerExcedente;
	private boolean incluirFacultativo;
	private boolean incluirEjerciciosAnteriores;
	private boolean desglosarPorMes;
	private boolean desglosarPorConceptos;
	private boolean excluirSaldoCorredor = true;
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		PL009_SaldoTrimestral plantillaSaldoTrimestral = new PL009_SaldoTrimestral();
		plantillaSaldoTrimestral.setIdTcReasegurador(idTcReasegurador);
		plantillaSaldoTrimestral.setEjercicio(ejercicio);
		plantillaSaldoTrimestral.setSuscripcion(suscripcion);
		plantillaSaldoTrimestral.setIdTcMoneda(idTcMoneda);
		plantillaSaldoTrimestral.setIncluirCuotaParte(incluirCuotaParte);
		plantillaSaldoTrimestral.setIncluirPrimerExcedente(incluirPrimerExcedente);
		plantillaSaldoTrimestral.setIncluirFacultativo(incluirFacultativo);
		plantillaSaldoTrimestral.setIncluirEjerciciosAnteriores(incluirEjerciciosAnteriores);
		plantillaSaldoTrimestral.setDesglosarPorMes(desglosarPorMes);
		plantillaSaldoTrimestral.setDesglosarPorConceptos(desglosarPorConceptos);
		
		setListaPlantillas(new ArrayList<byte[]>());
		getListaPlantillas().add(plantillaSaldoTrimestral.obtenerReporte(claveUsuario));
		
		return super.obtenerReporte(claveUsuario, false);
	}
	
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public Integer getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}
	public Integer getSuscripcion() {
		return suscripcion;
	}
	public void setSuscripcion(Integer suscripcion) {
		this.suscripcion = suscripcion;
	}
	public BigDecimal getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(BigDecimal idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	public boolean isIncluirCuotaParte() {
		return incluirCuotaParte;
	}
	public void setIncluirCuotaParte(boolean incluirCuotaParte) {
		this.incluirCuotaParte = incluirCuotaParte;
	}
	public boolean isIncluirPrimerExcedente() {
		return incluirPrimerExcedente;
	}
	public void setIncluirPrimerExcedente(boolean incluirPrimerExcedente) {
		this.incluirPrimerExcedente = incluirPrimerExcedente;
	}
	public boolean isIncluirFacultativo() {
		return incluirFacultativo;
	}
	public void setIncluirFacultativo(boolean incluirFacultativo) {
		this.incluirFacultativo = incluirFacultativo;
	}
	public boolean isIncluirEjerciciosAnteriores() {
		return incluirEjerciciosAnteriores;
	}
	public void setIncluirEjerciciosAnteriores(boolean incluirEjerciciosAnteriores) {
		this.incluirEjerciciosAnteriores = incluirEjerciciosAnteriores;
	}
	public boolean isDesglosarPorMes() {
		return desglosarPorMes;
	}
	public void setDesglosarPorMes(boolean desglosarPorMes) {
		this.desglosarPorMes = desglosarPorMes;
	}
	public boolean isDesglosarPorConceptos() {
		return desglosarPorConceptos;
	}
	public void setDesglosarPorConceptos(boolean desglosarPorConceptos) {
		this.desglosarPorConceptos = desglosarPorConceptos;
	}
	public boolean isExcluirSaldoCorredor() {
		return excluirSaldoCorredor;
	}
	public void setExcluirSaldoCorredor(boolean excluirSaldoCorredor) {
		this.excluirSaldoCorredor = excluirSaldoCorredor;
	}
}
