package mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;



public class PrioridadSiniestroReaseguroSN {
	private PrioridadSiniestroReaseguroFacadeRemote beanRemoto;

	public PrioridadSiniestroReaseguroSN() throws SystemException {
		try {
			LogDeMidasWeb.log("Entrando en PrioridadSiniestroReaseguroFacadeRemote  - Constructor",
					Level.INFO, null);
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PrioridadSiniestroReaseguroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<PrioridadSiniestroReaseguroDTO> listarTodos() {
		LogDeMidasWeb.log("Ejecutando listarTodos ", Level.FINEST, null);
		return beanRemoto.findAll();
		
	}
	
	
	
	
}
