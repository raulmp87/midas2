package mx.com.afirme.midas.reaseguro.reportes.estadocuenta.facultativo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteReaseguradorEstadoCuentaDTO;
import mx.com.afirme.midas.danios.reportes.cotizacion.generica.DatoIncisoCotizacionForm;
import mx.com.afirme.midas.reaseguro.reportes.estadocuenta.PlantillaEstadoCuentaBase;
import mx.com.afirme.midas.sistema.SystemException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author Jose Luis Arellano
 *
 */
public class ReporteMovimientosCuentaPorPagarEstadoCuentaFacultativo extends ReporteEstadoCuentaBase{
	
	public ReporteMovimientosCuentaPorPagarEstadoCuentaFacultativo(){
		super.setMapaParametrosGeneralesPlantillas(new HashMap<String, Object>());
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		super.setListaPlantillas(new ArrayList<byte[]>());

		
		List<DatoIncisoCotizacionForm> listaDatosReporte = new ArrayList<DatoIncisoCotizacionForm>();
		try {
			listaDatosReporte = obtenerDatosEstadoCuenta(claveUsuario);
		} catch (Exception e) {
		}
		
		List<ReporteReaseguradorEstadoCuentaDTO> listaReaseguradores = obtenerMovimientosEstadoCuentaFacultativo(true, claveUsuario);
		
		List<Object> listaRegistros = new ArrayList<Object>();
		if(listaReaseguradores != null){
			for(ReporteReaseguradorEstadoCuentaDTO reporteReasegurador : listaReaseguradores){
				if(reporteReasegurador.getListaMovimientosReaseguro() != null){
					reporteReasegurador.setDataSourceSubReporte(new JRBeanCollectionDataSource(reporteReasegurador.getListaMovimientosReaseguro()));
				}
				listaRegistros.add(reporteReasegurador);
			}
		}
		
		PlantillaEstadoCuentaBase plantillaMovimientosEdoCta = obtenerPlantillaEstadoCuentaFacultativo();
		
		plantillaMovimientosEdoCta.setListaDatosEstadoCuenta(listaDatosReporte);
		plantillaMovimientosEdoCta.setListaRegistrosContenido(listaRegistros);
		plantillaMovimientosEdoCta.setTituloReporte(getTituloEstadoCuenta());
		super.getListaPlantillas().add(plantillaMovimientosEdoCta.obtenerReporte(claveUsuario));

		return super.obtenerReporte(claveUsuario,false);
	}
	
	
}
