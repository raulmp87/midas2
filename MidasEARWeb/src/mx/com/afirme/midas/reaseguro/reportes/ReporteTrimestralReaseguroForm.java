package mx.com.afirme.midas.reaseguro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteTrimestralReaseguroForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	private String moneda;
	private String tipoCambio;
	private String fechaInicial;
	private String fechaFinal;
	
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
}
