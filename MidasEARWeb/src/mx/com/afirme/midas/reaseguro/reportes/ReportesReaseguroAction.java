package mx.com.afirme.midas.reaseguro.reportes;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.eventocatastrofico.EventoCatastroficoDN;
import mx.com.afirme.midas.catalogos.eventocatastrofico.EventoCatastroficoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.tiporeaseguro.TipoReaseguroDN;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.reaseguro.reportes.saldos.ReporteMovimientosSaldoBase;
import mx.com.afirme.midas.reaseguro.reportes.saldos.poliza.ReporteSaldosCuentasPorPagar;
import mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro.ReporteSaldosCuentasPorCobrar;
import mx.com.afirme.midas.reaseguro.reportes.saldos.trimestral.ReporteNegociosFacultativos;
import mx.com.afirme.midas.reaseguro.reportes.saldos.trimestral.ReporteSaldosTrimestrales;
import mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad.PrioridadSiniestroReaseguroDN;
import mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad.PrioridadSiniestroReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import net.sf.jasperreports.engine.JRException;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReportesReaseguroAction extends MidasMappingDispatchAction {
	
	public ActionForward mostrarFiltroRptTrimestralReaseguro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReporteTrimestralReaseguroForm reporteTrimestralReaseguroForm = (ReporteTrimestralReaseguroForm) form;
		Double tipoCambio = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorDia(new Date(), Short.parseShort("1"));
		Date fechaHoy = new Date();
		reporteTrimestralReaseguroForm.setTipoCambio(tipoCambio.toString());
		reporteTrimestralReaseguroForm.setFechaInicial(UtileriasWeb.getFechaString(fechaHoy));
		reporteTrimestralReaseguroForm.setFechaFinal(UtileriasWeb.getFechaString(fechaHoy));
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void rptTrimestralReaseguro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String monedaStr = request.getParameter("moneda");
		String tipoCambioStr = request.getParameter("tipoCambio");
		String tipoReporteTrim = request.getParameter("tipoReporteTrim");
		String reporteALanzar;
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0 && monedaStr.length() > 0 && tipoCambioStr.length() > 0){
			Integer pIdMoneda = new Integer(monedaStr);
			Double tipoCambio = Double.valueOf(tipoCambioStr);
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pIdMoneda", pIdMoneda);
				parametros.put("pTipoCambio", tipoCambio);
				parametros.put("pLogoUrl", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.imagenes"));		

				//Default (tipoReporteTrim es 0)
				reporteALanzar = "midas.sistema.reaseguro.reportes.rptTrimestralREA_FOR";
				if (tipoReporteTrim.equals("1"))
					reporteALanzar = "midas.sistema.reaseguro.reportes.rptTrimestralREA_FDN";
				if (tipoReporteTrim.equals("2"))
					reporteALanzar = "midas.sistema.reaseguro.reportes.rptTrimestralREA_FDR";
				if (tipoReporteTrim.equals("3"))
					reporteALanzar = "midas.sistema.reaseguro.reportes.rptTrimestralREA_FRE";
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
				
				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,reporteALanzar), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public ActionForward mostrarRptSaldoReasegurador(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReportesReaseguroForm reportesForm = (ReportesReaseguroForm) form;
		reportesForm.setFechaInicial("");
		reportesForm.setFechaFinal("");
		reportesForm.setTipoContrato("");
		
		String llenarMonedas = request.getParameter("llenarMonedas");
		if(!UtileriasWeb.esCadenaVacia(llenarMonedas) && llenarMonedas.equalsIgnoreCase("true")){
			try {
				reportesForm.setListaMonedas(MonedaDN.getInstancia().listarTodos());
			} catch (Exception e) {
				LogDeMidasWeb.log("Error al consultar las monedas.", Level.SEVERE, e);
			}
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	public void rptSaldoReasegurador(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaCorteStr = request.getParameter("fechaCorte");
		String reporteALanzar;
		if (fechaCorteStr.length() > 0){
			
			try {
				parametros.put("pFechaCorte", UtileriasWeb.getFechaFromString(fechaCorteStr));
			
				//Default (tipoReporteTrim es 0)
				reporteALanzar = "midas.sistema.reaseguro.reportes.rptSaldoPorReasegurador";
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
				
				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,reporteALanzar), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public ActionForward mostrarFiltroRptSiniestrosEventoCatastrofico(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReporteSiniestrosEventoCatastroficoForm reporteSiniestrosEventoCatastroficoForm = (ReporteSiniestrosEventoCatastroficoForm) form;
		reporteSiniestrosEventoCatastroficoForm.setFechaCorte(UtileriasWeb.getFechaString(new Date()));
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void rptSiniestrosEventoCatastrofico(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String pFechaCorte = request.getParameter("pFechaCorte");
		String idEventoCatastroficoStr = request.getParameter("eventoCatastrofico");
		
		if (pFechaCorte.length() > 0 && idEventoCatastroficoStr.length() > 0){
			Integer idEventoCatastrofico = new Integer(idEventoCatastroficoStr);
			try {
				//pTipoCambio = TipoCambioDN.getInstancia(Sistema.USUARIO_MIDAS).obtieneTipoCambioPorDia(new Date(), Short.parseShort("1"));
				Date dFechaCorte;
				dFechaCorte = UtileriasWeb.getFechaFromString(pFechaCorte);
				Calendar calendarFinal = Calendar.getInstance();
				calendarFinal.setTime(dFechaCorte);
				calendarFinal.add(Calendar.MONTH, 1);
				int mesInicial = calendarFinal.get(Calendar.MONTH);
				int anioInicial = calendarFinal.get(Calendar.YEAR);
								
				Double pTipoCambio = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorMes(mesInicial, anioInicial, Short.parseShort("1"));

				parametros.put("pFechaCorte", UtileriasWeb.getFechaFromString(pFechaCorte));
				//parametros.put("pIdEventoCatastrofico", idEventoCatastrofico);
				parametros.put("pIdEventoCat", idEventoCatastrofico);				
				parametros.put("pTipoCambio", pTipoCambio);

				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSiniestrosEventoCat"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void cargaComboEventoCatastrofico(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		List<EventoCatastroficoDTO> eventoCatastroficoDTOList;
		try {
			eventoCatastroficoDTOList = EventoCatastroficoDN.getInstancia().listarTodos();
			UtileriasWeb.imprimeMensajeXML(eventoCatastroficoDTOList, "idTcEventoCatastrofico", "descripcionEvento", response);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
	}
	
	public ActionForward mostrarFiltroRptSiniestrosTentPlan(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
		reportesReaseguroForm.setFechaCorte(UtileriasWeb.getFechaString(new Date()));
		reportesReaseguroForm.setFechaInicial(UtileriasWeb.getFechaString(new Date()));
		reportesReaseguroForm.setFechaFinal(UtileriasWeb.getFechaString(new Date()));
		/*List<EjercicioDTO> ejercicioDTOList;
		try {
			ejercicioDTOList = LineaDN.getInstancia().obtenerEjercicios();
			reportesReaseguroForm.setEjercicioDTOList(ejercicioDTOList);
		} catch (SystemException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}*/
		List<PrioridadSiniestroReaseguroDTO> prioridadSiniestroReListaseguroDTOList;				
		try {
			prioridadSiniestroReListaseguroDTOList = PrioridadSiniestroReaseguroDN.getInstancia().listarTodos();
			reportesReaseguroForm.setPrioridadSiniestroReaseguroDTOList(prioridadSiniestroReListaseguroDTOList);
		} catch (SystemException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void rptSiniestrosTentPlan(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
		
		String pFechaCorte = request.getParameter("pFechaCorte");
		String pFechaInicialStr = request.getParameter("pFechaInicial");
		String pFechaFinalStr = request.getParameter("pFechaFinal");
		String idPrioridadSiniestro = request.getParameter("idPrioridadSiniestro"); //Se recibe el a�o inicial de la prioridad, ejemplo, se recibe el 09 para la prioridad 09-10
		String prioridad;
		if (!UtileriasWeb.esCadenaVacia(pFechaInicialStr) && !UtileriasWeb.esCadenaVacia(pFechaFinalStr)
				&& !UtileriasWeb.esCadenaVacia(pFechaCorte) && !UtileriasWeb.esCadenaVacia(idPrioridadSiniestro)){
			try {
				EjercicioDTO ejercicioDTO = new EjercicioDTO();
				ejercicioDTO.setAnio(new Integer(idPrioridadSiniestro));
				prioridad = reportesReaseguroForm.getIdPrioridadSiniestro();
				Double pTipoCambio = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorDia(new Date(), Short.parseShort("1"));
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(pFechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(pFechaFinalStr));
				parametros.put("pFechaCorte", UtileriasWeb.getFechaFromString(pFechaCorte));
				parametros.put("pTipoCambio", pTipoCambio);
				parametros.put("pPrioridad", new Integer(prioridad));
				//parametros.put("pPrioridad", prioridad);

				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSiniestrosTentPlan"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public ActionForward mostrarFiltroRptSiniestrosWorkingCover(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
		reportesReaseguroForm.setFechaCorte(UtileriasWeb.getFechaString(new Date()));
		reportesReaseguroForm.setFechaInicial(UtileriasWeb.getFechaString(new Date()));
		reportesReaseguroForm.setFechaFinal(UtileriasWeb.getFechaString(new Date()));
		List<PrioridadSiniestroReaseguroDTO> prioridadSiniestroReListaseguroDTOList;		
		try {
			prioridadSiniestroReListaseguroDTOList = PrioridadSiniestroReaseguroDN.getInstancia().listarTodos();
			reportesReaseguroForm.setPrioridadSiniestroReaseguroDTOList(prioridadSiniestroReListaseguroDTOList);
		} catch (SystemException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarFiltroRptSinReservaPendienteAcum(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
		reportesReaseguroForm.setFechaCorte(UtileriasWeb.getFechaString(new Date()));
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarFiltroRptSinReservaPendienteAcumDet(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
		reportesReaseguroForm.setFechaCorte(UtileriasWeb.getFechaString(new Date()));
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void rptSiniestrosWorkingCover(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
		String pFechaCorte = request.getParameter("pFechaCorte");
		String pFechaInicialStr = request.getParameter("pFechaInicial");
		String pFechaFinalStr = request.getParameter("pFechaFinal");
		String idPrioridadSiniestro = request.getParameter("idPrioridadSiniestro"); //Se recibe el a�o inicial de la prioridad, ejemplo, se recibe el 09 para la prioridad 09-10
		String prioridad;
		if (!UtileriasWeb.esCadenaVacia(pFechaInicialStr) && !UtileriasWeb.esCadenaVacia(pFechaFinalStr)
				&& !UtileriasWeb.esCadenaVacia(pFechaCorte) && !UtileriasWeb.esCadenaVacia(idPrioridadSiniestro)){
			try {
				//Double tipoCambio = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorDia(new Date(), Short.parseShort("1"));
				/*DateFormat formatter ; 
				formatter = new SimpleDateFormat("dd/MM/yyyy");
				Date dcorte ; 
	            dcorte = (Date)formatter.parse(pFechaCorte); 
	              
				Calendar toDay = Calendar.getInstance();		
				Integer mes = new Integer(toDay.get(dcorte.getMonth()));
				Integer anio = new Integer(toDay.get(dcorte.getYear()));*/
	            Date dFechaCorte;
	            dFechaCorte = UtileriasWeb.getFechaFromString(pFechaCorte);
	            Calendar calendarFinal = Calendar.getInstance();
				calendarFinal.setTime(dFechaCorte);
				calendarFinal.add(Calendar.MONTH, 1);
				int mesInicial = calendarFinal.get(Calendar.MONTH);
				int anioInicial = calendarFinal.get(Calendar.YEAR);
				
	            
				Double tipoCambio = TipoCambioDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request)).obtieneTipoCambioPorMes(mesInicial, anioInicial, Short.parseShort("1"));
				EjercicioDTO ejercicioDTO = new EjercicioDTO();
				ejercicioDTO.setAnio(new Integer(idPrioridadSiniestro));
				//prioridad = ejercicioDTO.getDescripcion();
				prioridad = reportesReaseguroForm.getIdPrioridadSiniestro();
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(pFechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(pFechaFinalStr));
				parametros.put("pFechaCorte", UtileriasWeb.getFechaFromString(pFechaCorte));
				parametros.put("pTipoCambio", tipoCambio);
				parametros.put("pPrioridad", new Integer(prioridad));

				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSiniestrosWorkingCover"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptDistribucion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String monedaStr = request.getParameter("moneda");
		
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0 && monedaStr.length() > 0){
			Integer pIdMoneda = new Integer(monedaStr);
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pIdMoneda", pIdMoneda);
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptDistribucion"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptEmision(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String monedaStr = request.getParameter("moneda");
		
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0 && monedaStr.length() > 0){
			Integer pIdMoneda = new Integer(monedaStr);
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pIdMoneda", pIdMoneda);
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptEmision"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptIngresosReaseguro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0){
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptIngresosReaseguro"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptMovtosPorContrato(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String monedaStr = request.getParameter("moneda");
		
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0 && monedaStr.length() > 0){
			Integer pIdMoneda = new Integer(monedaStr);
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pIdMoneda", pIdMoneda);
				parametros.put("pTipoMovto", -1);
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptMovtosPorContrato"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptSiniestrosPorReasegurador(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String monedaStr = request.getParameter("moneda");
		String idtcReaseguradorCorredorStr = request.getParameter("idtcReaseguradorCorredor");
		String tipoReporte = request.getParameter("tipoReporte");
		
		if (tipoReporte.equals("XLS"))
			tipoReporte = Sistema.TIPO_XLS;
		else
			tipoReporte = Sistema.TIPO_PDF;
		
		Integer pIdCorredor = null;
		Integer pIdReasegurador = null;
		if (!UtileriasWeb.esCadenaVacia(fechaInicialStr) && !UtileriasWeb.esCadenaVacia(fechaFinalStr) 
				&& !UtileriasWeb.esCadenaVacia(monedaStr)){  //&& !UtileriasWeb.esCadenaVacia(idtcReaseguradorCorredorStr)
			Integer pIdMoneda = new Integer(monedaStr);
			try {
				
				ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
				if (idtcReaseguradorCorredorStr.equals("0")){
					pIdReasegurador = new Integer(-1);
					pIdCorredor = new Integer(-1);
					idtcReaseguradorCorredorStr  = "-1";
				}else{
					reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(new BigDecimal(idtcReaseguradorCorredorStr));
					reaseguradorCorredorDTO = ReaseguradorCorredorDN.getInstancia().obtenerReaseguradorPorId(reaseguradorCorredorDTO);
					if (!UtileriasWeb.esObjetoNulo(reaseguradorCorredorDTO) && reaseguradorCorredorDTO.getTipo().equals("0"))
						pIdCorredor = new Integer(idtcReaseguradorCorredorStr);
					else
						pIdReasegurador = new Integer(idtcReaseguradorCorredorStr);
				}
				
				//if (reaseguradorCorredorDTO != null){
				
					parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
					parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
					parametros.put("pIdMoneda", pIdMoneda);
					parametros.put("pIdReasegurador", pIdReasegurador);
					parametros.put("pIdCorredor", pIdCorredor);

					ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

					reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSiniestrosPorReasegurador"), 
							parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				//}
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptSiniestrosConReaseguro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String pNoSiniestroStr = request.getParameter("pNoSiniestro");
		String tipoMovimiento = request.getParameter("pTipoMovimiento");
		String idtcReaseguradorCorredorStr = request.getParameter("pIdReasegurador");
		Integer pIdReasegurador = null;
		Integer pTipoMovimiento = null;
		Integer pNoSiniestro = null;
		if (!UtileriasWeb.esCadenaVacia(fechaInicialStr) && !UtileriasWeb.esCadenaVacia(fechaFinalStr)){
			
			if (!UtileriasWeb.esCadenaVacia(tipoMovimiento) && !tipoMovimiento.equals("undefined") && !tipoMovimiento.equals("null")){
				pTipoMovimiento= new Integer(tipoMovimiento);
			}
			if (!UtileriasWeb.esCadenaVacia(idtcReaseguradorCorredorStr) && !idtcReaseguradorCorredorStr.equals("undefined") && !idtcReaseguradorCorredorStr.equals("null")){
				pIdReasegurador= new Integer(idtcReaseguradorCorredorStr);
			}
			if (!UtileriasWeb.esCadenaVacia(pNoSiniestroStr) && !pNoSiniestroStr.equals("undefined") && !pNoSiniestroStr.equals("null")){
				pNoSiniestro= new Integer(pNoSiniestroStr);
			}
			try {
					
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pNoSiniestro", pNoSiniestro);
				parametros.put("pTipoMovimiento", pTipoMovimiento);
				parametros.put("pIdReasegurador", pIdReasegurador);

				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSiniestrosConReaseguro"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public void rptSinReservaPendiente(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0){
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pRamoContable", null); //TODO Pendiente definir de d�nde traer este par�metro en el filtro
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSinReservaPendiente"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}

	public void rptSinReservaPendienteAcum(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		String reporteALanzar = request.getParameter("reporteALanzar");
		String nombreReporte;
		String pFechaCorte = request.getParameter("fechacorte");
		
		
		if (reporteALanzar.equals("1"))
			nombreReporte = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSinReservaPendienteAcum");
		else
			nombreReporte = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptSinReservaPendienteAcumDet");
		
		try {
			parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(pFechaCorte));
			parametros.put("pSubReportDir", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.siniestrosconreaseguro"));
			
			ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

			reportesReaseguro.generarReportePLSQL(nombreReporte, parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);

		} catch (Exception e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}
	
	public ActionForward mostrarFiltroRptCumulos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
			ReportesReaseguroForm reportesReaseguroForm = (ReportesReaseguroForm) form;
			reportesReaseguroForm.setFechaInicial(UtileriasWeb.getFechaString(calendar.getTime()));
			reportesReaseguroForm.setFechaFinal(UtileriasWeb.getFechaString(new Date()));
			reportesReaseguroForm.setTipoReaseguroList(TipoReaseguroDN.getINSTANCIA().listarTodos());
			
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public void rptCumulos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		final String HURACAN = "0";
		final String SISMO = "1";
		String fechaInicialStr = request.getParameter("fechaInicial");
		String fechaFinalStr = request.getParameter("fechaFinal");
		String tipoDeducible = request.getParameter("tipoDeducible");
		String tipoContrato = request.getParameter("tipoContrato");
		String tipoCumulo = request.getParameter("tipoCumulo");
		
		
		if (fechaInicialStr.length() > 0 && fechaFinalStr.length() > 0 && tipoDeducible.length() > 0 && tipoContrato.length() > 0){
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(fechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(fechaFinalStr));
				parametros.put("pTipoDeducible", new Integer(tipoDeducible));
				parametros.put("pTipoContrato", new Integer(tipoContrato));
				
				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();
				if (tipoCumulo.equals(HURACAN))
					tipoCumulo = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptCumulosHuracan");
				else if (tipoCumulo.equals(SISMO))
					tipoCumulo = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptCumulosSismo");
				
				reportesReaseguro.generarReportePLSQL(tipoCumulo, 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}
	
	public ActionForward mostrarFiltroRptReservasRiesgo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		ReportesReaseguroForm reaseguroForm = (ReportesReaseguroForm) form;
		reaseguroForm.setFechaFinal(simpleDateFormat.format(calendar.getTime()));
		reaseguroForm.setFechaCorte(simpleDateFormat.format(calendar.getTime()));
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		reaseguroForm.setFechaInicial(simpleDateFormat.format(calendar.getTime()));
		
		
		
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public void rptReservasRiesgo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> parametros = new HashMap<String, Object>();
		String tipoReporte = Sistema.TIPO_XLS;
		
		String pFechaCorte = request.getParameter("pFechaCorte");
		String pFechaInicialStr = request.getParameter("pFechaInicial");
		String pFechaFinalStr = request.getParameter("pFechaFinal");
		if (!UtileriasWeb.esCadenaVacia(pFechaInicialStr) && !UtileriasWeb.esCadenaVacia(pFechaFinalStr)
				&& !UtileriasWeb.esCadenaVacia(pFechaCorte)){
			try {
				parametros.put("pFechaInicial", UtileriasWeb.getFechaFromString(pFechaInicialStr));
				parametros.put("pFechaFinal", UtileriasWeb.getFechaFromString(pFechaFinalStr));
				parametros.put("pFechaCorte", UtileriasWeb.getFechaFromString(pFechaCorte));

				ReportesReaseguro reportesReaseguro = new ReportesReaseguro();

				reportesReaseguro.generarReportePLSQL(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.rptReservasRiesgo"), 
						parametros, tipoReporte, Sistema.RPT_COMO_ADJUNTO, response);
				
			} catch (Exception e) {
				e.printStackTrace();
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
	}

	public ActionForward generarReporteSaldosPorPolizaSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
			String fechaInicial = request.getParameter("fechaInicial");
			String fechaFinal = request.getParameter("fechaFinal");
			String tipoReporte = request.getParameter("tipoReporte");//'poliza' o 'siniestro'
			String tipoContrato = request.getParameter("tipoContrato");//'todo', 'soloFacultativo' o 'soloAutomatico'
			String idMoneda = request.getParameter("idMoneda");
			
			if(!UtileriasWeb.esCadenaVacia(fechaInicial) && !UtileriasWeb.esCadenaVacia(fechaFinal) &&
					!UtileriasWeb.esCadenaVacia(tipoReporte) && !UtileriasWeb.esCadenaVacia(tipoContrato) &&
					!UtileriasWeb.esCadenaVacia(idMoneda)){
				byte[] byteArray = null;
				
				ReporteMovimientosSaldoBase reporteMovimientosSaldo = null;
				
				if(tipoReporte.equalsIgnoreCase("poliza")){
					reporteMovimientosSaldo = new ReporteSaldosCuentasPorPagar();
				}
				else if (tipoReporte.equalsIgnoreCase("siniestro")){
					reporteMovimientosSaldo = new ReporteSaldosCuentasPorCobrar();
				}else{
					throw new SystemException("Los par�metros introducidos son inv�lidos.");
				}
				reporteMovimientosSaldo.setIdMoneda(new Double(idMoneda));
				
				reporteMovimientosSaldo.setFechaInicial(UtileriasWeb.getFechaFromString(fechaInicial));
				reporteMovimientosSaldo.setFechaFinal(UtileriasWeb.getFechaFromString(fechaFinal));
				
				if(tipoContrato.equalsIgnoreCase("soloFacultativo")){
					reporteMovimientosSaldo.setSoloFacultativos(true);
					reporteMovimientosSaldo.setSoloAutomaticos(false);
				}
				else{
					reporteMovimientosSaldo.setSoloAutomaticos(true);
					reporteMovimientosSaldo.setSoloFacultativos(false);
				}
				
					byteArray = reporteMovimientosSaldo.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
					this.writeBytes(response, byteArray, tipoReporte, generarNombreReporte(fechaInicial,fechaFinal,tipoReporte));
			}
			
			return null;
		}catch(Exception e){
			LogDeMidasWeb.log("Error al imprimir reporte de estado de cuenta facultativo", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br>"+e.getLocalizedMessage()+"<br>Causa: "+e.getCause());
			return mapping.findForward("errorImpresion");
		}
	}
	
	public ActionForward generarReporteSaldosPorTrimestre(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
			String idTcReaseguradorString = request.getParameter("idTcReasegurador");
			String ejercicioString = request.getParameter("ejercicio");
			String suscripcionString = request.getParameter("suscripcion");
			String monedaString = request.getParameter("moneda");
			String incluirSaldosCuotaParteString = request.getParameter("incluirSaldosCuotaParte");
			String incluirSaldosPrimerExcedenteString = request.getParameter("incluirSaldosPrimerExcedente");
			String incluirSaldosFacultativoString = request.getParameter("incluirSaldosFacultativo");
			String incluirSaldosEjerciciosAnteriores = request.getParameter("incluirSaldosEjerciciosAnteriores");
			String desglosarPorMesString = request.getParameter("desglosarPorMes");
			String desglosarPorConceptoString = request.getParameter("desglosarPorConceptos");
			
			if(!UtileriasWeb.esCadenaVacia(idTcReaseguradorString) && !UtileriasWeb.esCadenaVacia(ejercicioString) &&
					!UtileriasWeb.esCadenaVacia(suscripcionString) && !UtileriasWeb.esCadenaVacia(monedaString) ){
				
				if((!UtileriasWeb.esCadenaVacia(incluirSaldosCuotaParteString) && incluirSaldosCuotaParteString.equalsIgnoreCase("true")) ||
						(!UtileriasWeb.esCadenaVacia(incluirSaldosPrimerExcedenteString) && incluirSaldosPrimerExcedenteString.equalsIgnoreCase("true")) ||
						(!UtileriasWeb.esCadenaVacia(incluirSaldosFacultativoString) && incluirSaldosFacultativoString.equalsIgnoreCase("true")) ){
					
					byte[] byteArray = null;
					
					ReporteSaldosTrimestrales reporteSaldosTrimestrales = new ReporteSaldosTrimestrales();
					
					reporteSaldosTrimestrales.setIdTcReasegurador(UtileriasWeb.regresaBigDecimal(idTcReaseguradorString));
					reporteSaldosTrimestrales.setEjercicio(Integer.valueOf(ejercicioString));
					reporteSaldosTrimestrales.setSuscripcion(Integer.valueOf(suscripcionString));
					reporteSaldosTrimestrales.setIdTcMoneda(UtileriasWeb.regresaBigDecimal(monedaString));
					reporteSaldosTrimestrales.setIncluirCuotaParte(Boolean.valueOf(incluirSaldosCuotaParteString));
					reporteSaldosTrimestrales.setIncluirPrimerExcedente(Boolean.valueOf(incluirSaldosPrimerExcedenteString));
					reporteSaldosTrimestrales.setIncluirFacultativo(Boolean.valueOf(incluirSaldosFacultativoString));
					reporteSaldosTrimestrales.setIncluirEjerciciosAnteriores(Boolean.valueOf(incluirSaldosEjerciciosAnteriores));
					reporteSaldosTrimestrales.setDesglosarPorMes(Boolean.valueOf(desglosarPorMesString));
					reporteSaldosTrimestrales.setDesglosarPorConceptos(Boolean.valueOf(desglosarPorConceptoString));
					
					byteArray = reporteSaldosTrimestrales.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
					
					this.writeBytes(response, byteArray, Sistema.TIPO_XLS, "reporteSaldosTrimestrales");
				}else{
					throw new SystemException("Debe seleccionar un tipo de contrato para mostrar el saldo.");
				}
			}
			
			return null;
		}catch(Exception e){
			LogDeMidasWeb.log("Error al imprimir reporte de estado de cuenta facultativo", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br>"+e.getLocalizedMessage()+"<br>Causa: "+e.getCause());
			return mapping.findForward("errorImpresion");
		}
	}
	
	public ActionForward generarReporteNegociosFacultativosPorTrimestre(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		try{
			String idTcReaseguradorString = request.getParameter("idTcReasegurador");
			String ejercicioString = request.getParameter("ejercicio");
			String suscripcionString = request.getParameter("suscripcion");
			
			if(!UtileriasWeb.esCadenaVacia(idTcReaseguradorString) && !UtileriasWeb.esCadenaVacia(ejercicioString) &&
					!UtileriasWeb.esCadenaVacia(suscripcionString) ){
				byte[] byteArray = null;
				
				ReporteNegociosFacultativos reporteFacultativos= new ReporteNegociosFacultativos();
				
				reporteFacultativos.setIdTcReasegurador(UtileriasWeb.regresaBigDecimal(idTcReaseguradorString));
				reporteFacultativos.setEjercicio(Integer.valueOf(ejercicioString));
				reporteFacultativos.setSuscripcion(Integer.valueOf(suscripcionString));
				
				byteArray = reporteFacultativos.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
				
				this.writeBytes(response, byteArray, Sistema.TIPO_XLS, "reporteNegociosFacultativos");
			}
			else{
				throw new SystemException("Debe introducir los criterios de b�squeda para mostrar el reporte.");
			}
			
			return null;
		}catch(Exception e){
			LogDeMidasWeb.log("Error al imprimir reporte de estado de cuenta facultativo", Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><br>"+e.getLocalizedMessage()+"<br>Causa: "+e.getCause());
			return mapping.findForward("errorImpresion");
		}
	}
	
	private String generarNombreReporte(String fechaInicio,String fechaFin,String tipoReporte){
		String nombre = "";
		if(tipoReporte.equalsIgnoreCase("poliza")){
			nombre += "ReporteSaldosCxP_Del_";
		}
		else{
			nombre += "ReporteSaldosCxC_Del_";
		}
		nombre += fechaInicio+ "_Al_" + fechaFin+".xls";
		return nombre;
	}
}
