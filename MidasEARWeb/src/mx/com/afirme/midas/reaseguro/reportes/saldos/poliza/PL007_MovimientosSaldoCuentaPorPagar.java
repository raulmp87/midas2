package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas.reaseguro.reportes.saldos.PlantillaMovimientosSaldoBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;


/**
 * @author Jose Luis Arellano
 *
 */
public class PL007_MovimientosSaldoCuentaPorPagar extends PlantillaMovimientosSaldoBase{

	public PL007_MovimientosSaldoCuentaPorPagar(){
		inicializarDatosPlantilla();
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteMovimientosSaldo();
		return getByteArrayReport();
	}
	
	private void generarReporteMovimientosSaldo() throws SystemException{
		if(parametrosReporteValidos()){
			List<ReporteSaldosPolizaDTO> listaMovimientos = ReporteSaldosPolizaDN.getInstancia().consultarSaldosPoliza(
					fechaInicial, fechaFinal, soloFacultativos, soloAutomaticos, incluirRetencion, idMoneda,null);
			
			BigDecimal totalPrimaMenosComision = BigDecimal.ZERO;
			BigDecimal totalIngresosPrimaNoDevengada = BigDecimal.ZERO;
			BigDecimal totalIngresosBonoPorNoSiniestro = BigDecimal.ZERO;
			BigDecimal totalPagos = BigDecimal.ZERO;
			BigDecimal totalImpuestos = BigDecimal.ZERO;
			BigDecimal totalSaldoAPagar = BigDecimal.ZERO;
			
			setListaRegistrosContenido(new ArrayList<Object>());
			
			for(ReporteSaldosPolizaDTO movimientoReporte : listaMovimientos){
				getListaRegistrosContenido().add(movimientoReporte);
				
				totalPrimaMenosComision = totalPrimaMenosComision.add(movimientoReporte.getTotalPrimaMenosComision());
				totalIngresosPrimaNoDevengada = totalIngresosPrimaNoDevengada.add(movimientoReporte.getTotalIngresosPrimaNoDevengada());
				totalIngresosBonoPorNoSiniestro = totalIngresosBonoPorNoSiniestro.add(movimientoReporte.getTotalIngresosBonoPorNoSiniestro());
				totalPagos = totalPagos.add(movimientoReporte.getTotalPagos());
				totalImpuestos = totalImpuestos.add(movimientoReporte.getTotalRetencionImpuestos());
				totalSaldoAPagar = totalSaldoAPagar.add(movimientoReporte.getSaldoTotal());
			}
			
			poblarParametrosComunes();
			
			getParametrosVariablesReporte().put("TOTAL_PRIMA_MENOS_COMISION", totalPrimaMenosComision);
			getParametrosVariablesReporte().put("TOTAL_INGRESOS_DEVOLUCION_PRIMA", totalIngresosPrimaNoDevengada);
			getParametrosVariablesReporte().put("TOTAL_INGRESOS_BONO_NO_SINIESTRO", totalIngresosBonoPorNoSiniestro);
			getParametrosVariablesReporte().put("TOTAL_PAGOS", totalPagos);
			getParametrosVariablesReporte().put("TOTAL_IMPUESTOS", totalImpuestos);
			getParametrosVariablesReporte().put("TOTAL_SALDO_PAGAR", totalSaldoAPagar);
			
			if(super.getListaRegistrosContenido().isEmpty()){
//				setByteArrayReport(null);
				generarLogPlantillaSinDatosParaMostrar();
			}
//			else{
				try {
					super.setByteArrayReport( generaReporte(super.getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
							getParametrosVariablesReporte(), getListaRegistrosContenido()));
				} catch (JRException e) {
					setByteArrayReport( null );
					generarLogErrorCompilacionPlantilla(e);
				}
//			}
		}
	}

	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.movimientosSaldoCuentaPorPagar"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.movimientosSaldoCuentaPorPagar.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
	}
}
