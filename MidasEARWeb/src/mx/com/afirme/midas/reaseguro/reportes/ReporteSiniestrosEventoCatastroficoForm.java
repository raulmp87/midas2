package mx.com.afirme.midas.reaseguro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteSiniestrosEventoCatastroficoForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	private String fechaCorte;
	
	public String getFechaCorte() {
		return fechaCorte;
	}
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
}
