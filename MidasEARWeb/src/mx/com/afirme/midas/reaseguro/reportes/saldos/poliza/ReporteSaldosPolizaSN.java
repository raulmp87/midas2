package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteSaldosPolizaSN {
	private ReporteSaldosPolizaFacadeRemote beanRemoto;
	
	ReporteSaldosPolizaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReporteSaldosPolizaFacadeRemote.class);
		} catch (Exception e) {
			LogDeMidasWeb.log("Error al instanciar bean Remoto ReporteSaldosPolizaFacadeRemote", Level.SEVERE, e);
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
	}
	
	public List<ReporteSaldosPolizaDTO> consultarSaldosPoliza(Date fechaInicial,Date fechaFinal,boolean soloFacultativos,boolean soloAutomaticos,boolean incluirRetencion,
    		Double idMoneda, final int...rowStartIdxAndCount){
		return beanRemoto.consultarSaldosPoliza(fechaInicial, fechaFinal, soloFacultativos, soloAutomaticos, incluirRetencion, idMoneda, rowStartIdxAndCount);
	}
}
