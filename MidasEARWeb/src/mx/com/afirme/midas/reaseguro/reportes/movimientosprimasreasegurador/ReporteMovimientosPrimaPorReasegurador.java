package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteMovimientosPrimaPorReasegurador extends MidasReporteBase{
	private MovimientoPrimaReaseguradorDTO movimientoPrimaReasegurador;
	private List<MovimientoPrimaReaseguradorDTO> listaMovimientos;
	
	public ReporteMovimientosPrimaPorReasegurador(MovimientoPrimaReaseguradorDTO movimientoPrimaReasegurador){
		this.movimientoPrimaReasegurador = movimientoPrimaReasegurador;
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		List<MovimientoPrimaReaseguradorDTO> listaMovimientos = null;
		try{
			listaMovimientos = MovimientoPrimaReaseguradorDN.getInstancia().obtenerMovimientosPrimaPorReasegurador(movimientoPrimaReasegurador, claveUsuario);
		}catch(SystemException e1){
			throw new SystemException("Ocurri� un error al recuperar la informaci�n para el reporte: "+e1.getCause());
		}
		if(listaMovimientos == null)
			throw new SystemException ("Ocurri� un error al recuperar la informaci�n para el reporte. Lista null.");
//		if(listaMovimientos.isEmpty())
//			throw new SystemException ("No se encontraron registros para los datos introducidos.");
		Map<String,Object> mapaParametros = new Hashtable<String, Object>();
		mapaParametros.put("pFechaInicial", movimientoPrimaReasegurador.getFechaInicioVigencia());
		mapaParametros.put("pFechaFinal", movimientoPrimaReasegurador.getFechaFinVigencia());
		mapaParametros.put("URL_LOGO_AFIRME",Sistema.LOGOTIPO_SEGUROS_AFIRME);
		PL1_Reas_MovimientosPrimaReasegurador plantilla = new PL1_Reas_MovimientosPrimaReasegurador(listaMovimientos,mapaParametros);
		byte byteArray[] = null;
		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		
		return byteArray;
	}

	public void generarReporteXML(String claveUsuario){
		if(listaMovimientos == null || listaMovimientos.isEmpty()){
			try{
				listaMovimientos = MovimientoPrimaReaseguradorDN.getInstancia().obtenerMovimientosPrimaPorReasegurador(movimientoPrimaReasegurador, claveUsuario);
			}catch(SystemException e1){
				LogDeMidasWeb.log("Ocurri� un error al recuperar la lista de movimientos por reasegurador del "+movimientoPrimaReasegurador.getFechaInicioVigencia()+" al "+movimientoPrimaReasegurador.getFechaFinVigencia(),Level.WARNING,e1);
				return;
			}
		}
		if(listaMovimientos == null){
			LogDeMidasWeb.log("Ocurri� un error al recuperar la lista de movimientos por reasegurador del "+movimientoPrimaReasegurador.getFechaInicioVigencia()+" al "+movimientoPrimaReasegurador.getFechaFinVigencia(),Level.WARNING,null);
			return;
		}
		
	}
	public MovimientoPrimaReaseguradorDTO getMovimientoPrimaReasegurador() {
		return movimientoPrimaReasegurador;
	}
	public void setMovimientoPrimaReasegurador(MovimientoPrimaReaseguradorDTO movimientoPrimaReasegurador) {
		this.movimientoPrimaReasegurador = movimientoPrimaReasegurador;
	}
}
