package mx.com.afirme.midas.reaseguro.reportes.egresos;

import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.contratos.egreso.OrdenPagoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JasperReport;

/**
 * @author Jose Luis Arellano
 *
 */
public class PL015_OrdenPago extends MidasPlantillaBase{

	private OrdenPagoDTO ordenPagoDTO;
	
	public PL015_OrdenPago(OrdenPagoDTO ordenPagoDTO){
		this.ordenPagoDTO = ordenPagoDTO;
		inicializarDatosPlantilla();
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarPlantilla();
		return getByteArrayReport();
	}

	private void generarPlantilla(){
		
		if (ordenPagoDTO != null && ordenPagoDTO.getEstadoCuenta() != null && ordenPagoDTO.getEstadoCuenta().getTipoReaseguro() != TipoReaseguroDTO.TIPO_RETENCION) {
			getListaRegistrosContenido().add(ordenPagoDTO);
			
			//Generar parametros para la plantilla
			String nombrePlantillaSubReporte = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.egresos.ordenpago.subReporteParticipacionFacultativa");
			JasperReport subReporteParticipacionFacultativa = getJasperReport(nombrePlantillaSubReporte);
			
			if(subReporteParticipacionFacultativa == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
			
			nombrePlantillaSubReporte = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"midas.sistema.reaseguro.reportes.egresos.ordenpago.subReporteParticipacionAutomaticos");
			JasperReport subReporteParticipacionAutomatico = getJasperReport(nombrePlantillaSubReporte);
			
			if(subReporteParticipacionAutomatico == null)
				generarLogErrorCompilacionPlantilla(nombrePlantillaSubReporte,null);
			
			getParametrosVariablesReporte().put("SUBREPORTE_PARTICIPACION_FACULTATIVO", subReporteParticipacionFacultativa);
			getParametrosVariablesReporte().put("SUBREPORTE_PARTICIPACION_AUTOMATICO", subReporteParticipacionAutomatico);
			getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
			
			finalizarReporte();
		}
		else{
			generarLogPlantillaSinDatosParaMostrar();
		}
		
	}
	
	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.egresos.ordenpago.plantilla"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.egresos.ordenpago.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_PDF);
	}
}
