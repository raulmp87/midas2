package mx.com.afirme.midas.reaseguro.reportes.saldos;

import java.util.ArrayList;
import java.util.Date;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.reaseguro.reportes.saldos.poliza.PL007_MovimientosSaldoCuentaPorPagar;
import mx.com.afirme.midas.reaseguro.reportes.saldos.poliza.ReporteSaldosCuentasPorPagar;
import mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro.PL008_MovimientosSaldoCuentaPorCobrar;
import mx.com.afirme.midas.reaseguro.reportes.saldos.siniestro.ReporteSaldosCuentasPorCobrar;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

/**
 * @author Jose Luis Arellano
 *
 */
public class ReporteMovimientosSaldoBase extends MidasReporteBase{
	protected Date fechaInicial;
	protected Date fechaFinal;
	protected Double idMoneda;
	protected boolean soloFacultativos;
	protected boolean soloAutomaticos;
	protected boolean incluirRetencion;
	
	public ReporteMovimientosSaldoBase(){
		this.incluirRetencion = false;
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		super.setListaPlantillas(new ArrayList<byte[]>());
		if(parametrosReporteValidos()){
			
			PlantillaMovimientosSaldoBase plantilla = null;
			if (this instanceof ReporteSaldosCuentasPorCobrar){
				plantilla = new PL008_MovimientosSaldoCuentaPorCobrar();
			}else if(this instanceof ReporteSaldosCuentasPorPagar){
				plantilla = new PL007_MovimientosSaldoCuentaPorPagar();
			}
			
			if(plantilla != null){
				plantilla.setFechaFinal(fechaFinal);
				plantilla.setFechaInicial(fechaInicial);
				plantilla.setIdMoneda(idMoneda);
				plantilla.setSoloAutomaticos(soloAutomaticos);
				plantilla.setSoloFacultativos(soloFacultativos);
				plantilla.setIncluirRetencion(incluirRetencion);
				
				byte[] byteArrayPlantilla = plantilla.obtenerReporte(claveUsuario);
				
				if(byteArrayPlantilla != null)
					getListaPlantillas().add(byteArrayPlantilla);
				else
					throw new SystemException("Ocurri� un error al crear la plantilla del reporte: "+this);
			}
			else
				throw new SystemException("No se ha implementado plantilla para el reporte de tipo: "+this);	
			
		}
		else
			throw new SystemException("Parametros de filtrado no v�lidos.");
		
		return super.obtenerReporte(claveUsuario,false);
	}

	protected boolean parametrosReporteValidos(){
		boolean valido = false;
		valido = (fechaInicial != null && fechaFinal != null &&
				fechaInicial.before(fechaFinal) && 
				idMoneda != null && 
				(idMoneda.intValue() == Sistema.MONEDA_PESOS || idMoneda.intValue() == Sistema.MONEDA_DOLARES) &&
				(soloFacultativos || soloAutomaticos));
		return valido;
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public Double getIdMoneda() {
		return idMoneda;
	}
	public void setIdMoneda(Double idMoneda) {
		this.idMoneda = idMoneda;
	}
	public boolean isSoloFacultativos() {
		return soloFacultativos;
	}
	public void setSoloFacultativos(boolean soloFacultativos) {
		this.soloFacultativos = soloFacultativos;
	}
	public boolean isSoloAutomaticos() {
		return soloAutomaticos;
	}
	public void setSoloAutomaticos(boolean soloAutomaticos) {
		this.soloAutomaticos = soloAutomaticos;
	}
	public boolean isIncluirRetencion() {
		return incluirRetencion;
	}
	public void setIncluirRetencion(boolean incluirRetencion) {
		this.incluirRetencion = incluirRetencion;
	}
}
