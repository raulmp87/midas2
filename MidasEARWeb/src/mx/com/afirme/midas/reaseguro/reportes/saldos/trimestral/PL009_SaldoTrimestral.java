package mx.com.afirme.midas.reaseguro.reportes.saldos.trimestral;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteEstadoCuentaDN;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteSaldoPorSubRamoDTO;
import mx.com.afirme.midas.contratos.estadocuenta.reportes.ReporteSaldoTrimestralDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PL009_SaldoTrimestral extends MidasPlantillaBase{

	private BigDecimal idTcReasegurador;
	private Integer ejercicio;
	private Integer suscripcion;
	private BigDecimal idTcMoneda;
	private boolean incluirCuotaParte;
	private boolean incluirPrimerExcedente;
	private boolean incluirFacultativo;
	private boolean incluirEjerciciosAnteriores;
	private boolean desglosarPorMes;
	private boolean desglosarPorConceptos;
	private boolean excluirSaldoCorredor = true;
	
	public PL009_SaldoTrimestral(){
		inicializarDatosPlantilla();
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporteSaldoTrimestral();
		return getByteArrayReport();
	}
	
	private void generarReporteSaldoTrimestral() throws SystemException{
		
		List<Integer> listaTipoReaseguro = new ArrayList<Integer>();
		
		if(incluirCuotaParte)
			listaTipoReaseguro.add(TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE);
		if(incluirPrimerExcedente)
			listaTipoReaseguro.add(TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE);
		if(incluirFacultativo)
			listaTipoReaseguro.add(TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO);

		//Especificación de la plantilla de acuerdo a la configuración recibida
		if((incluirCuotaParte || incluirPrimerExcedente) && incluirFacultativo && desglosarPorMes && desglosarPorConceptos){
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldoTrimestralPorReasegurador"));
		}
		else if((incluirCuotaParte || incluirPrimerExcedente) && !incluirFacultativo && desglosarPorMes && desglosarPorConceptos){
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldoTrimestralPorReasegurador.sinFacultativo"));
		}
		else if((incluirCuotaParte || incluirPrimerExcedente) && incluirFacultativo && !desglosarPorMes && !desglosarPorConceptos){
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.resumenSaldoTrimestralPorReasegurador"));
		}
		else if((incluirCuotaParte || incluirPrimerExcedente) && !incluirFacultativo && !desglosarPorMes && !desglosarPorConceptos){
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.resumenSaldoTrimestralPorReasegurador.sinFacultativo"));
		}
		else{//Se seleccionó una combinación no válida, se usará la plantilla 9 por default
			super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldoTrimestralPorReasegurador")); 
		}
		
		Integer[] idTcTipoReaseguroArray = listaTipoReaseguro.toArray(new Integer[listaTipoReaseguro.size()]);

		ReporteSaldoTrimestralDTO reporteSaldoTrimestral = ReporteEstadoCuentaDN.getInstancia().obtenerSaldosPorReasegurador(
				idTcReasegurador,idTcTipoReaseguroArray,ejercicio,suscripcion,incluirEjerciciosAnteriores,idTcMoneda,excluirSaldoCorredor);
		
		if(reporteSaldoTrimestral != null && reporteSaldoTrimestral.getListaSaldosSubRamo() != null){
			
			getParametrosVariablesReporte().put("NOMBRE_REASEGURADOR", reporteSaldoTrimestral.getNombreReasegurador());
			getParametrosVariablesReporte().put("DESCRIPCION_MONEDA", UtileriasWeb.obtenerDescripcionMoneda(idTcMoneda.intValue()));
			
			getParametrosVariablesReporte().put("TOTAL_SALDO_ANTERIOR", reporteSaldoTrimestral.getMontoTotalSaldoAnterior());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_MES1", reporteSaldoTrimestral.getMontoTotalPrimaMes1());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_MES2", reporteSaldoTrimestral.getMontoTotalPrimaMes2());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_MES3", reporteSaldoTrimestral.getMontoTotalPrimaMes3());
			getParametrosVariablesReporte().put("TOTAL_COMISION_MES1", reporteSaldoTrimestral.getMontoTotalComisionMes1());
			getParametrosVariablesReporte().put("TOTAL_COMISION_MES2", reporteSaldoTrimestral.getMontoTotalComisionMes2());
			getParametrosVariablesReporte().put("TOTAL_COMISION_MES3", reporteSaldoTrimestral.getMontoTotalComisionMes3());
			getParametrosVariablesReporte().put("TOTAL_SINIESTRO_MES1", reporteSaldoTrimestral.getMontoTotalSiniestroMes1());
			getParametrosVariablesReporte().put("TOTAL_SINIESTRO_MES2", reporteSaldoTrimestral.getMontoTotalSiniestroMes2());
			getParametrosVariablesReporte().put("TOTAL_SINIESTRO_MES3", reporteSaldoTrimestral.getMontoTotalSiniestroMes3());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_FACULTATIVO_MES1", reporteSaldoTrimestral.getMontoTotalPrimaFacultativoMes1());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_FACULTATIVO_MES2", reporteSaldoTrimestral.getMontoTotalPrimaFacultativoMes2());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_FACULTATIVO_MES3", reporteSaldoTrimestral.getMontoTotalPrimaFacultativoMes3());
			getParametrosVariablesReporte().put("TOTAL_COMISION_FACULTATIVO_MES1", reporteSaldoTrimestral.getMontoTotalComisionFacultativoMes1());
			getParametrosVariablesReporte().put("TOTAL_COMISION_FACULTATIVO_MES2", reporteSaldoTrimestral.getMontoTotalComisionFacultativoMes2());
			getParametrosVariablesReporte().put("TOTAL_COMISION_FACULTATIVO_MES3", reporteSaldoTrimestral.getMontoTotalComisionFacultativoMes3());
			getParametrosVariablesReporte().put("TOTAL_PRIMA_TRIMESTRE", reporteSaldoTrimestral.getMontoTotalPrimaTrimestre());
			getParametrosVariablesReporte().put("TOTAL_COMISION_TRIMESTRE", reporteSaldoTrimestral.getMontoTotalComisionTrimestre());
			getParametrosVariablesReporte().put("TOTAL_SINIESTRO_TRIMESTRE", reporteSaldoTrimestral.getMontoTotalSiniestroTrimestre());
			getParametrosVariablesReporte().put("TOTAL_SALDO_TECNICO_TRIMESTRE", reporteSaldoTrimestral.getMontoTotalSaldoTecnicoTrimestre());
			
			getParametrosVariablesReporte().put("TOTAL_SALDO_TECNICO_TRIMESTRE_FACULTATIVO", reporteSaldoTrimestral.getMontoTotalSaldoTecnicoFacultativoTrimestre());
			
			getParametrosVariablesReporte().put("TOTAL_SALDO_PROPORCIONAL_MAS_FACULTATIVO_TRIMESTRE", 
					reporteSaldoTrimestral.getMontoTotalSaldoTecnicoFacultativoTrimestre().add(reporteSaldoTrimestral.getMontoTotalSaldoTecnicoTrimestre()));
			
			getParametrosVariablesReporte().put("TOTAL_REMESA_SALDO", reporteSaldoTrimestral.getMontoTotalRemesaSaldo());
			getParametrosVariablesReporte().put("TOTAL_ARRASTRE_SALDO", reporteSaldoTrimestral.getMontoTotalArrastreSaldo());

			getParametrosVariablesReporte().put("DESCRIPCION_TRIMESTRE_ANTERIOR", "SALDO ANTERIOR "+ reporteSaldoTrimestral.obtenerDescripcionSuscripcionAnterior());
			
			if(reporteSaldoTrimestral.getSuscripcionDTO() != null){
				List<Date> mesesEnPeriodo = reporteSaldoTrimestral.getSuscripcionDTO().obtenerMesesIncluidos();
				if(mesesEnPeriodo != null && mesesEnPeriodo.size() == 3){
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(mesesEnPeriodo.get(0));
					getParametrosVariablesReporte().put("DESCRIPCION_MES1", obtenerDescripcionMes(mesesEnPeriodo.get(0)));
					getParametrosVariablesReporte().put("DESCRIPCION_MES2", obtenerDescripcionMes(mesesEnPeriodo.get(1)));
					getParametrosVariablesReporte().put("DESCRIPCION_MES3", obtenerDescripcionMes(mesesEnPeriodo.get(2)));
				}
				
				getParametrosVariablesReporte().put("DESCRIPCION_TOTAL_TRIMESTRE", "TOTAL "+reporteSaldoTrimestral.getSuscripcionDTO().getDescripcion());// total X trimm cttos +ejercicio
				getParametrosVariablesReporte().put("DESCRIPCION_TRIMESTRE", reporteSaldoTrimestral.getSuscripcionDTO().getDescripcion());
			}
			
			for(ReporteSaldoPorSubRamoDTO saldoPorSubRamoTMP : reporteSaldoTrimestral.getListaSaldosSubRamo()){
				getListaRegistrosContenido().add(saldoPorSubRamoTMP);
			}
		}
		
		finalizarReporte();
	}
	
	private void inicializarDatosPlantilla(){
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldoTrimestralPorReasegurador"));
		super.setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.plantilla.saldoTrimestralPorReasegurador.paquete"));
		super.setListaRegistrosContenido(new ArrayList<Object>());
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setTipoReporte(Sistema.TIPO_XLS);
	}
	
	public BigDecimal getIdTcReasegurador() {
		return idTcReasegurador;
	}
	public void setIdTcReasegurador(BigDecimal idTcReasegurador) {
		this.idTcReasegurador = idTcReasegurador;
	}
	public boolean isIncluirCuotaParte() {
		return incluirCuotaParte;
	}
	public void setIncluirCuotaParte(boolean incluirCuotaParte) {
		this.incluirCuotaParte = incluirCuotaParte;
	}
	public boolean isIncluirPrimerExcedente() {
		return incluirPrimerExcedente;
	}
	public void setIncluirPrimerExcedente(boolean incluirPrimerExcedente) {
		this.incluirPrimerExcedente = incluirPrimerExcedente;
	}
	public boolean isIncluirFacultativo() {
		return incluirFacultativo;
	}
	public void setIncluirFacultativo(boolean incluirFacultativo) {
		this.incluirFacultativo = incluirFacultativo;
	}
	public boolean isIncluirEjerciciosAnteriores() {
		return incluirEjerciciosAnteriores;
	}
	public void setIncluirEjerciciosAnteriores(boolean incluirEjerciciosAnteriores) {
		this.incluirEjerciciosAnteriores = incluirEjerciciosAnteriores;
	}
	public boolean isDesglosarPorMes() {
		return desglosarPorMes;
	}
	public void setDesglosarPorMes(boolean desglosarPorMes) {
		this.desglosarPorMes = desglosarPorMes;
	}
	public boolean isDesglosarPorConceptos() {
		return desglosarPorConceptos;
	}
	public void setDesglosarPorConceptos(boolean desglosarPorConceptos) {
		this.desglosarPorConceptos = desglosarPorConceptos;
	}
	public Integer getEjercicio() {
		return ejercicio;
	}
	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}
	public Integer getSuscripcion() {
		return suscripcion;
	}
	public void setSuscripcion(Integer suscripcion) {
		this.suscripcion = suscripcion;
	}
	public BigDecimal getIdTcMoneda() {
		return idTcMoneda;
	}
	public void setIdTcMoneda(BigDecimal idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	public boolean isExcluirSaldoCorredor() {
		return excluirSaldoCorredor;
	}
	public void setExcluirSaldoCorredor(boolean excluirSaldoCorredor) {
		this.excluirSaldoCorredor = excluirSaldoCorredor;
	}
}
