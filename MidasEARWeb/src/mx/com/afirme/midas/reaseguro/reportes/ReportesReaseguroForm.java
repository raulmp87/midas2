package mx.com.afirme.midas.reaseguro.reportes;

import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.reaseguro.reportes.siniestrosconreaseguro.prioridad.PrioridadSiniestroReaseguroDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReportesReaseguroForm extends MidasBaseForm{

	private static final long serialVersionUID = -1829844420008563330L;
	
	private String moneda;
	private String fechaCorte;
	private String fechaInicial;
	private String fechaFinal;
	private String idEjercicio;
	private String idPrioridadSiniestro;
	private String tipoContrato;
	private String tipoReporte;
	private List<EjercicioDTO> ejercicioDTOList;
	private List<TipoReaseguroDTO> tipoReaseguroList;
	private List<PrioridadSiniestroReaseguroDTO> prioridadSiniestroReaseguroDTOList;
	private String idSuscripcion;
	private List<MonedaDTO> listaMonedas;
	private String incluirSaldosCuotaParte;
	private String incluirSaldosPrimerExcedente;
	private String incluirSaldosFacultativo;
	private String incluirSaldosEjerciciosAnteriores;
	private String desglosarPorMes;
	private String desglosarPorConceptos;
	
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getIdEjercicio() {
		return idEjercicio;
	}

	public void setIdEjercicio(String idEjercicio) {
		this.idEjercicio = idEjercicio;
	}
	
	public String getIdPrioridadSiniestro() {
		return idPrioridadSiniestro;
	}

	public void setIdPrioridadSiniestro(String idPrioridadSiniestro) {
		this.idPrioridadSiniestro = idPrioridadSiniestro;
	}

	public List<EjercicioDTO> getEjercicioDTOList() {
		return ejercicioDTOList;
	}

	public void setEjercicioDTOList(List<EjercicioDTO> ejercicioDTOList) {
		this.ejercicioDTOList = ejercicioDTOList;
	}

	public List<TipoReaseguroDTO> getTipoReaseguroList() {
		return tipoReaseguroList;
	}

	public void setTipoReaseguroList(List<TipoReaseguroDTO> tipoReaseguroList) {
		this.tipoReaseguroList = tipoReaseguroList;
	}

	public String getTipoContrato() {
		return tipoContrato;
	}

	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	public List<PrioridadSiniestroReaseguroDTO> getPrioridadSiniestroReaseguroDTOList() {
		return prioridadSiniestroReaseguroDTOList;
	}

	public void setPrioridadSiniestroReaseguroDTOList(
			List<PrioridadSiniestroReaseguroDTO> prioridadSiniestroReaseguroDTOList) {
		this.prioridadSiniestroReaseguroDTOList = prioridadSiniestroReaseguroDTOList;
	}

	public String getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public String getIdSuscripcion() {
		return idSuscripcion;
	}

	public void setIdSuscripcion(String idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}

	public List<MonedaDTO> getListaMonedas() {
		return listaMonedas;
	}

	public void setListaMonedas(List<MonedaDTO> listaMonedas) {
		this.listaMonedas = listaMonedas;
	}

	public String getIncluirSaldosCuotaParte() {
		return incluirSaldosCuotaParte;
	}

	public void setIncluirSaldosCuotaParte(String incluirSaldosCuotaParte) {
		this.incluirSaldosCuotaParte = incluirSaldosCuotaParte;
	}

	public String getIncluirSaldosPrimerExcedente() {
		return incluirSaldosPrimerExcedente;
	}

	public void setIncluirSaldosPrimerExcedente(String incluirSaldosPrimerExcedente) {
		this.incluirSaldosPrimerExcedente = incluirSaldosPrimerExcedente;
	}

	public String getIncluirSaldosFacultativo() {
		return incluirSaldosFacultativo;
	}

	public void setIncluirSaldosFacultativo(String incluirSaldosFacultativo) {
		this.incluirSaldosFacultativo = incluirSaldosFacultativo;
	}

	public String getIncluirSaldosEjerciciosAnteriores() {
		return incluirSaldosEjerciciosAnteriores;
	}

	public void setIncluirSaldosEjerciciosAnteriores(
			String incluirSaldosEjerciciosAnteriores) {
		this.incluirSaldosEjerciciosAnteriores = incluirSaldosEjerciciosAnteriores;
	}

	public String getDesglosarPorMes() {
		return desglosarPorMes;
	}

	public void setDesglosarPorMes(String desglosarPorMes) {
		this.desglosarPorMes = desglosarPorMes;
	}

	public String getDesglosarPorConceptos() {
		return desglosarPorConceptos;
	}

	public void setDesglosarPorConceptos(String desglosarPorConceptos) {
		this.desglosarPorConceptos = desglosarPorConceptos;
	}
}