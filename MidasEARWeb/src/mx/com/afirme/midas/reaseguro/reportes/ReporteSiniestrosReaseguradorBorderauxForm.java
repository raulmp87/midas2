package mx.com.afirme.midas.reaseguro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteSiniestrosReaseguradorBorderauxForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	private String moneda;
	private String idTcReaseguradorCorredor;
	private String tipoParticipante;
	
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getIdTcReaseguradorCorredor() {
		return idTcReaseguradorCorredor;
	}
	public void setIdTcReaseguradorCorredor(String idTcReaseguradorCorredor) {
		this.idTcReaseguradorCorredor = idTcReaseguradorCorredor;
	}
	public String getTipoParticipante() {
		return tipoParticipante;
	}
	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}
}
