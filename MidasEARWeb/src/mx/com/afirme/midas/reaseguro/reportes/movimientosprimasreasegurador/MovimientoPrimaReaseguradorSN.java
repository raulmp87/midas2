package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador.MovimientoPrimaReaseguradorDTO;
import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador.MovimientoPrimaReaseguradorFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class MovimientoPrimaReaseguradorSN {
	private MovimientoPrimaReaseguradorFacadeRemote beanRemoto;
	
	public MovimientoPrimaReaseguradorSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(MovimientoPrimaReaseguradorFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto MovimientoPrimaReaseguradorFacadeRemote instanciado", Level.FINEST, null);
	}

	public List<MovimientoPrimaReaseguradorDTO> obtenerMovimientosPrimaPorReasegurador(MovimientoPrimaReaseguradorDTO entity,String nombreUsuario) {
		try {
			return beanRemoto.obtenerMovimientosPrimaPorReasegurador(entity, nombreUsuario);
		} catch (SQLException e) {
			StringBuffer sb = new StringBuffer();
			sb.append("MIDAS.PKGREA_REPORTES.spREA_RptMovtosPorReasegurador");
			sb.append("|");
			sb.append("pFechaInicial"+ "=" + entity.getFechaInicioVigencia()+ ",");
			sb.append("pFechaFinal"+ "=" + entity.getFechaFinVigencia()+ ",");
			sb.append("pIdToPoliza"+ "=" + entity.getIdToPoliza()+ ",");
			sb.append("pIdMoneda"+ "=" + entity.getIdMoneda()+ ",");
			sb.append("pIdRamo"+ "=" + entity.getIdRamo()+ ",");
			sb.append("pIdSubRamo"+ "=" + entity.getIdSubRamo());
			
			String info = "Ocurri� un error al realizar la consulta de movimientos de prima por reasegurador en " + Sistema.AMBIENTE_SISTEMA;
			LogDeMidasWeb.log(info+sb.toString(), Level.SEVERE, e);
			UtileriasWeb.enviaCorreoExcepcion(info, sb.toString());
			return null;
		} catch (Exception e) {
			StringBuffer sb = new StringBuffer();
			sb.append("MIDAS.PKGREA_REPORTES.spREA_RptMovtosPorReasegurador");
			sb.append("|");
			sb.append("pFechaInicial"+ "=" + entity.getFechaInicioVigencia()+ ",");
			sb.append("pFechaFinal"+ "=" + entity.getFechaFinVigencia()+ ",");
			sb.append("pIdToPoliza"+ "=" + entity.getIdToPoliza()+ ",");
			sb.append("pIdMoneda"+ "=" + entity.getIdMoneda()+ ",");
			sb.append("pIdRamo"+ "=" + entity.getIdRamo()+ ",");
			sb.append("pIdSubRamo"+ "=" + entity.getIdSubRamo());
			
			String info = "Ocurri� un error al realizar la consulta de movimientos de prima por reasegurador en " + Sistema.AMBIENTE_SISTEMA;
			LogDeMidasWeb.log(info+sb.toString(), Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
