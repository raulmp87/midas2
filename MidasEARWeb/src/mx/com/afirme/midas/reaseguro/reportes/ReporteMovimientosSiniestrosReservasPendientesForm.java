package mx.com.afirme.midas.reaseguro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteMovimientosSiniestrosReservasPendientesForm extends MidasBaseForm{
	private static final long serialVersionUID = 1L;
	private String idTcRamo;
	private String parametrosReporte;
	
	public String getIdTcRamo() {
		return idTcRamo;
	}
	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}
	public String getParametrosReporte() {
		return parametrosReporte;
	}
	public void setParametrosReporte(String parametrosReporte) {
		this.parametrosReporte = parametrosReporte;
	}	
}
