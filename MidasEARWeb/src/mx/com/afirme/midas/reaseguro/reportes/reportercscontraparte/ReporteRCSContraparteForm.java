package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteRCSContraparteForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private String fechaCorte;
	private String nomenclatura;
	
	public String getFechaCorte() {
		return fechaCorte;
	}
	
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
	public String getNomenclatura() {
		return nomenclatura;
	}

	public void setNomenclatura(String nomenclatura) {
		this.nomenclatura = nomenclatura;
	}
	
	@Override
	public String toString(){
		String cadena = "ReporteRCSForm: ";
		cadena += "fechaCorte = "+fechaCorte;		
		return cadena;
	}
}