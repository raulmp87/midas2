package mx.com.afirme.midas.reaseguro.reportes.saldos.poliza;

import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;


public class ReporteSaldosPolizaDN {
	private static final ReporteSaldosPolizaDN INSTANCIA = new ReporteSaldosPolizaDN();
	
	public static ReporteSaldosPolizaDN getInstancia(){
		return INSTANCIA;
	}

	private ReporteSaldosPolizaDN() {
	}
	
	public List<ReporteSaldosPolizaDTO> consultarSaldosPoliza(Date fechaInicial,Date fechaFinal,boolean soloFacultativos,boolean soloAutomaticos,boolean incluirRetencion,
    		Double idMoneda, final int...rowStartIdxAndCount) throws SystemException{
		return new ReporteSaldosPolizaSN().consultarSaldosPoliza(fechaInicial, fechaFinal, soloFacultativos, soloAutomaticos, incluirRetencion, idMoneda, rowStartIdxAndCount);
	}
}
