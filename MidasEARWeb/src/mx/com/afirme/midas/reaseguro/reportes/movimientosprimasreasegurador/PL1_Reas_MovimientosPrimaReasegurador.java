package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;

public class PL1_Reas_MovimientosPrimaReasegurador extends MidasPlantillaBase{
	List<MovimientoPrimaReaseguradorDTO> listaMovimientosPorReasegurador;
	
	public PL1_Reas_MovimientosPrimaReasegurador(List<MovimientoPrimaReaseguradorDTO> movimientosPrimaReaseguradorDTO,Map<String,Object> mapaParametrosGenerales){
		this.listaMovimientosPorReasegurador = movimientosPrimaReaseguradorDTO;
		setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.rptMovtosPorReasegurador.plantilla"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.reaseguro.reportes.rptMovtosPorReasegurador.paquete"));
		setTipoReporte(Sistema.TIPO_XLS);
		if(mapaParametrosGenerales != null){
			setParametrosVariablesReporte(new HashMap<String, Object>());
			getParametrosVariablesReporte().putAll(mapaParametrosGenerales);
		}
	}
	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		setListaRegistrosContenido(new ArrayList<Object>());
		if (listaMovimientosPorReasegurador != null && !listaMovimientosPorReasegurador.isEmpty()){
			getListaRegistrosContenido().addAll(listaMovimientosPorReasegurador);
			if(getParametrosVariablesReporte() == null){
				setParametrosVariablesReporte(new Hashtable<String,Object>());
				getParametrosVariablesReporte().put("pFechaInicial", listaMovimientosPorReasegurador.get(0).getFechaInicioVigencia());
				getParametrosVariablesReporte().put("pFechaFinal", listaMovimientosPorReasegurador.get(0).getFechaFinVigencia());
			}
		}
		try {
			setByteArrayReport( generaReporte(getTipoReporte(), getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			setByteArrayReport( null );
			generarLogErrorCompilacionPlantilla(e);
		}
		return getByteArrayReport();
	}

}
