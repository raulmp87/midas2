package mx.com.afirme.midas.reaseguro.egresos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorDecoradoDTO;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorId;
import mx.com.afirme.midas.contratofacultativo.pagocobertura.PlanPagosCoberturaDN;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoEstadoCuentaDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoEstadoCuentaId;
import mx.com.afirme.midas.contratos.egreso.EgresoReaseguroDTO;
import mx.com.afirme.midas.contratos.egreso.PagoDTO;
import mx.com.afirme.midas.contratos.egreso.PolizaRelacionadaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDecoradoDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDecoradoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDN;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.danios.soporte.EndosoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDN;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDN;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.reaseguro.ingresos.IngresoReaseguroDN;
import mx.com.afirme.midas.reaseguro.reportes.ReportesReaseguro;
import mx.com.afirme.midas.reaseguro.reportes.egresos.ReporteOrdenPago;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadOrdenPagoId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.MailAction;

public class EgresoReaseguroDN {
	private static final EgresoReaseguroDN INSTANCIA = new EgresoReaseguroDN();
	public static final int INVALIDO = 0;
	public static final int SOLO_AUTOMATICOS = 1;
	public static final int SOLO_FACULTATIVOS = 2;
	public static final int AUTOMATICOS_Y_FACULTATIVOS = 3;

	public static EgresoReaseguroDN getINSTANCIA() {
		return INSTANCIA;
	}

	public EgresoReaseguroDTO getPorId(BigDecimal id) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.getPorId(id);
	}

	public List<EgresoReaseguroDTO> obtenerEgresosPendientes() {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();

		return egresoReaseguroSN.obtenerEgresosPendientes();
	}
	
	public List<EgresoReaseguroDTO> obtenerEgresosPendientesCancel() {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.obtenerEgresosPendientesCancel();
	}

	public void validarRelacionEgresoEdosCta(String[] idsEdosCta, String mensaje)
			throws SystemException, ExcepcionDeLogicaNegocio {

		List<EstadoCuentaDecoradoDTO> listaEstadosCuenta = new ArrayList<EstadoCuentaDecoradoDTO>();

		listaEstadosCuenta = EstadoCuentaDN.getINSTANCIA().obtenerEstadosCuentaDecoradoPorIds(idsEdosCta);

		// try{
		// EgresoReaseguroDN.getINSTANCIA().verificarMonedaEdosCta(listaEstadosCuenta);
		// }catch (RuntimeException e){
		// throw new
		// RuntimeException("Los Estados de Cuenta Seleccionados no son de la misma moneda");
		// }
		if (!IngresoReaseguroDN.getINSTANCIA().verificarReaseguradores(listaEstadosCuenta)) {
			throw new ExcepcionDeLogicaNegocio(
					this.getClass().getCanonicalName(),UtileriasWeb
							.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,"reaseguro.egreso.administrar.egresos.error.validarrelacionegresos.reaseguradoresDiferentes"));
		}

	}

	// private void verificarMonedaEdosCta(List<EstadoCuentaDecoradoDTO>
	// listaEstadosCuenta) throws RuntimeException{
	// EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO =
	// listaEstadosCuenta.get(0);
	// for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO2 :
	// listaEstadosCuenta) {
	// if (estadoCuentaDecoradoDTO2.getIdMoneda() !=
	// estadoCuentaDecoradoDTO.getIdMoneda()){
	// throw new
	// RuntimeException("Los Estados de Cuenta Seleccionados no son de la misma moneda");
	// }
	// }
	// }

	/*
	public void borrar(EgresoReaseguroDTO egresoReaseguroDTO, String mensaje, BigDecimal idEstadoCuenta)
			throws SystemException {
		//SolicitudSN solicitudSN = new SolicitudSN();
		//solicitudSN.borrar(egresoReaseguroDTO);
		EgresoReaseguroSN egresoReaseguroDTO2 = new EgresoReaseguroSN();
		egresoReaseguroDTO2.borrarEgresoDTO(egresoReaseguroDTO, idEstadoCuenta);
	}
	*/
	public void borrarCheque(EgresoReaseguroDTO egresoReaseguroDTO, String mensaje)
	throws SystemException {
		//EgresoReaseguroSN egresoReaseguroDTO2 = new EgresoReaseguroSN();
		//egresoReaseguroDTO2.borrarEgresoCheque(egresoReaseguroDTO);
    }

	public boolean tipoCambioNecesario(
			List<EstadoCuentaDecoradoDTO> listaEstadosCuenta,
			EgresoReaseguroDTO egresoReaseguroDTO) {
		boolean resultado = false;
		for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : listaEstadosCuenta) {
			if (estadoCuentaDecoradoDTO.getIdMoneda() != egresoReaseguroDTO
					.getIdMoneda()) {
				resultado = true;
				break;
			}
		}
		return resultado;
	}
	
	public int obtenerTiposEstadosCuentaAMostrar(List<EstadoCuentaDecoradoDTO> listaEstadosCuenta) {		
		boolean automaticos = false;
		boolean facultativos = false;
		for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : listaEstadosCuenta) {
			if (estadoCuentaDecoradoDTO.getTipoReaseguro()==TipoReaseguroDTO.TIPO_CONTRATO_CUOTA_PARTE ||
				estadoCuentaDecoradoDTO.getTipoReaseguro()==TipoReaseguroDTO.TIPO_CONTRATO_PRIMER_EXCEDENTE) {
				automaticos = true;
			}else if (estadoCuentaDecoradoDTO.getTipoReaseguro()==TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
				facultativos = true;
			}
			if(automaticos && facultativos) {
				return AUTOMATICOS_Y_FACULTATIVOS;
			}
		}
		
		if(automaticos) {
			return SOLO_AUTOMATICOS;
		}
		
		if(facultativos) {
			return SOLO_FACULTATIVOS;
		}
				
		return INVALIDO;
	}
	
	private void validarEgresoTipoCambio(EgresoReaseguroDTO egresoReaseguro){
		if (egresoReaseguro.getIdMoneda() == 0) {
			throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, 
																	  "reaseguro.egreso.administrar.egresos.relacionaregreso.error.tipomoneda"));
		}
		
		if(egresoReaseguro.getTipoCambio() == 0) {
			throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, 
																	  "reaseguro.egreso.administrar.egresos.relacionaregreso.error.tipocambiocero"));
		}
		//Se agrega validacion de monto, no debe de ser nulo o =0.
		if(egresoReaseguro.getMonto()==null || egresoReaseguro.getMonto()==0){
			throw new RuntimeException(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, 
			  														  "reaseguro.egreso.administrar.egresos.relacionaregreso.error.montocero"));
		}
	}
	
	private boolean[] validarExhibicionEgreso(PagoCoberturaReaseguradorDTO exhibicion, HashMap<String, ReciboDTO> ultimoReciboPagadoEndosoMap, 
											HashMap<BigDecimal, Boolean> autorizacionEstadosCuentaMap, boolean validarAutorizacionPagosFacultativosPorEdoCta, String usuario) 
	throws SystemException, ParseException{				
		boolean estadoCuentaRequiereAutorizacion = false;
		boolean exhibicionRequiereAutorizacionFaltaPagoAsegurado = false;
		boolean exhibicionRequiereAutorizacionVencimiento = false;
		Date fechaActual = new Date();
				
		String idToPlanPagosCobertura = exhibicion.getId().getIdToPlanPagosCobertura().toString();
		String numeroExhibicion = exhibicion.getId().getNumeroExhibicion().toString();
		String idReasegurador = exhibicion.getId().getIdReasegurador().toString();
		String idExhibicion = "("+idToPlanPagosCobertura+","+numeroExhibicion+","+idReasegurador+")";
		List<PagoCoberturaReaseguradorDecoradoDTO> exhibiciones = 
			PlanPagosCoberturaDN.getInstancia(usuario).listarExhibicionesDecoradas(idExhibicion);
		PagoCoberturaReaseguradorDecoradoDTO exhibicionDecorada = exhibiciones.get(0);
		
		/* ********AUTORIZACI�N DE PAGOS FACULTATIVOS POR ESTADO DE CUENTA******** */
		/*Todos los pagos facultativos que se paguen por estados de cuenta deber�n de 
		  contar con una autorizaci�n previa para poder efectuarse.*/

		BigDecimal estadoCuentaKey = exhibicionDecorada.getIdToEstadoCuenta();
		if(validarAutorizacionPagosFacultativosPorEdoCta && !autorizacionEstadosCuentaMap.containsKey(estadoCuentaKey)){			
			EstadoCuentaDN estadoCuentaDN = EstadoCuentaDN.getINSTANCIA();
			EstadoCuentaDTO estadoCuenta = estadoCuentaDN.getPorId(exhibicionDecorada.getIdToEstadoCuenta());
			if(estadoCuenta.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() != Sistema.AUTORIZADA){
				if(estadoCuenta.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() == null ||
				   estadoCuenta.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() == Sistema.AUTORIZACION_NO_REQUERIDA){
					estadoCuenta.setEstatusAutorizacionPagoFacultativoPorEstadoCuenta(Sistema.AUTORIZACION_REQUERIDA);
					estadoCuenta.setUsuarioSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(usuario);
					estadoCuenta.setFechaSolicitudAutorizacionPagoFacultativoPorEstadoCuenta(fechaActual);		
					estadoCuentaDN.modificar(estadoCuenta);									
				}
				estadoCuentaRequiereAutorizacion = true;
				autorizacionEstadosCuentaMap.put(estadoCuentaKey, Boolean.TRUE);
			}else{
				autorizacionEstadosCuentaMap.put(estadoCuentaKey, Boolean.FALSE);
			}
		}
		
		/* ********AUTORIZACI�N DE PAGOS A REASEGURADORES SIN PAGO DE ASEGURADO******** */		
		/*Para poder realizar el pago de una exhibici�n a un reasegurador, es necesario que 
		  el asegurado haya pagado todos los recibos con fecha igual o anterior a la fecha de 
		  la exhibici�n. Si lo anterior, no se cumple, el pago al reasegurador no podr� ser efectuado, 
		  a menos que se cuente con una autorizaci�n por parte de un supervisor.*/	
		if(exhibicion.getEstatusAutorizacionFaltaPagoAsegurado() == null ||
		   exhibicion.getEstatusAutorizacionFaltaPagoAsegurado().shortValue() != Sistema.AUTORIZADA){						
			BigDecimal idToPoliza = new BigDecimal(exhibicionDecorada.getPoliza());
			Short numeroEndoso = new Short(exhibicionDecorada.getEndoso().toString());
			SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaPagoExhibicion = dateFmt.parse(exhibicionDecorada.getFechaPago());
			
			ReciboDTO ultimoReciboPagado = null;
			String reciboKey = idToPoliza.toString()+"|"+numeroEndoso.toString();
			if(!ultimoReciboPagadoEndosoMap.containsKey(reciboKey)){		
				List<ReciboDTO> recibos = ReciboDN.getInstancia(usuario).consultaRecibos(idToPoliza, numeroEndoso);
				recibos=(recibos!=null && !recibos.isEmpty())?recibos:new ArrayList<ReciboDTO>();
				for(int i=(recibos.size()-1);i>=0;i--){
					ReciboDTO recibo=recibos.get(i);
					//busca el ultimo recibo pagado
					if (recibo.getNumeroEndoso().equals(numeroEndoso.toString()) && 
						recibo.getSituacion().trim().equals(Sistema.RECIBO_PAGADO)){
						ultimoReciboPagado = recibo;					
						ultimoReciboPagadoEndosoMap.put(reciboKey, ultimoReciboPagado);					
						break;
					}
				}
				/*
				for(ReciboDTO recibo : recibos){			 						
					if (recibo.getNumeroEndoso().equals(numeroEndoso.toString()) && 
						recibo.getSituacion().trim().equals(Sistema.RECIBO_EMITIDO)){
						primerReciboNoPagado = recibo;					
						primerReciboNoPagadoEndosoMap.put(reciboKey, primerReciboNoPagado);					
						break;
					}
				}		
				*/
			}else{
				ultimoReciboPagado = ultimoReciboPagadoEndosoMap.get(reciboKey);
			}
			//Se evita un nullpointerException
			//Date fechaPagoPrimerReciboNoPagado = (primerReciboNoPagado!=null)?primerReciboNoPagado.getFechaIncioVigencia():null;
			//La fecha de fin de vigencia del ultimo recibo pagado es la fecha de inicio de vigencia del primer no pagado
			Date fechaFinUltimoReciboPagado = (ultimoReciboPagado!=null)?ultimoReciboPagado.getFechaFinVigencia():null;
			//TODO Cambiar por ultimo pagado
			if((ultimoReciboPagado==null)||(fechaFinUltimoReciboPagado==null) || (fechaPagoExhibicion.compareTo(fechaFinUltimoReciboPagado)>0)){
				//Si la fecha de la exhibicion es mayor o igual a la fecha de pago del primer recibo no pagado del endoso
				//la exhibicion requiere autorizaci�n por falta de pago del asegurado
				exhibicionRequiereAutorizacionFaltaPagoAsegurado = true;
				if(exhibicion.getEstatusAutorizacionFaltaPagoAsegurado() == null ||
				   exhibicion.getEstatusAutorizacionFaltaPagoAsegurado().shortValue() == Sistema.AUTORIZACION_NO_REQUERIDA){
					//Si no se ha solicitado antes la autorizaci�n, entonces se solicita.
					exhibicion.setEstatusAutorizacionFaltaPagoAsegurado((byte)Sistema.AUTORIZACION_REQUERIDA);
					exhibicion.setUsuarioSolicitudAutorizacionFaltaPagoAsegurado(usuario);
					exhibicion.setFechaSolicitudAutorizacionFaltaPagoAsegurado(fechaActual);
				}
			}else{
				//Si la fecha de la exhibicion es menor a la fecha de pago del primer recibo no pagado del endoso
				//la exhibicion no requiere autorizaci�n por falta de pago del asegurado
				exhibicion.setEstatusAutorizacionFaltaPagoAsegurado((byte)Sistema.AUTORIZACION_NO_REQUERIDA);
				exhibicion.setUsuarioSolicitudAutorizacionFaltaPagoAsegurado(null);
				exhibicion.setFechaSolicitudAutorizacionFaltaPagoAsegurado(null);
			}
		}
		
		/* ********AUTORIZACI�N DE PAGOS DE EXHIBICIONES VENCIDAS******** */		
		/*Una exhibici�n vencida es aqu�lla que no fue pagada en la fecha estipulada originalmente. 
		  El pago de una exhibici�n que lleva m�s de 45 d�as de vencimiento no puede ser efectuado 
		  a menos que se tenga la autorizaci�n de un supervisor.*/
		if(exhibicion.getEstatusAutorizacionExhibicionVencida() == null ||
		   exhibicion.getEstatusAutorizacionExhibicionVencida().shortValue() != Sistema.AUTORIZADA){
			Date fechaPagoExhibicion = exhibicion.getPagoCobertura().getFechaPago();
			double diasDiferencia = UtileriasWeb.obtenerDiasEntreFechas(fechaPagoExhibicion, fechaActual);
			if(diasDiferencia>45){
				//La exhibici�n lleva m�s de 45 d�as de vencimiento. Requiere autorizaci�n.
				exhibicionRequiereAutorizacionVencimiento = true;
				if(exhibicion.getEstatusAutorizacionExhibicionVencida() == null ||
				   exhibicion.getEstatusAutorizacionExhibicionVencida().shortValue() == Sistema.AUTORIZACION_NO_REQUERIDA){
					//Si no se ha solicitado antes la autorizaci�n, entonces se solicita.
					exhibicion.setEstatusAutorizacionExhibicionVencida((byte)Sistema.AUTORIZACION_REQUERIDA);
					exhibicion.setUsuarioSolicitudAutorizacionExhibicionVencida(usuario);
					exhibicion.setFechaSolicitudAutorizacionExhibicionVencida(fechaActual);
				}
			}else{
				//La exhibici�n lleva 45 d�as o menos de vencimiento. No requiere autorizaci�n.
				exhibicion.setEstatusAutorizacionExhibicionVencida((byte)Sistema.AUTORIZACION_NO_REQUERIDA);
				exhibicion.setUsuarioSolicitudAutorizacionExhibicionVencida(null);
				exhibicion.setFechaSolicitudAutorizacionExhibicionVencida(null);
			}
		}
		
		return new boolean[]{estadoCuentaRequiereAutorizacion, 
							 exhibicionRequiereAutorizacionFaltaPagoAsegurado, 
							 exhibicionRequiereAutorizacionVencimiento};
	}
	
	public EgresoReaseguroDTO agregarEgreso(String idsEstadosCuentaAutomaticosMontosExhibicionesFacultativos,  
			  EgresoReaseguroDTO egresoReaseguro, String usuario) throws SystemException, ParseException, ExcepcionDeLogicaNegocio {		
		
		this.validarEgresoTipoCambio(egresoReaseguro);
		
		BigDecimal montoTotalEgreso = new BigDecimal(0D);
		BigDecimal impuestoTotalEgreso = new BigDecimal(0D);
		
		List<EgresoEstadoCuentaDTO> listaEgresosEstadosCuenta = new ArrayList<EgresoEstadoCuentaDTO>();
		String[] idsEdoCtaAutMontoFac = idsEstadosCuentaAutomaticosMontosExhibicionesFacultativos.split("\\|");
		
		boolean validarAutorizacionPagosFacultativosPorEdoCta = false;
		
		//Se procesan los estados de cuenta autom�ticos		
		if(egresoReaseguro.getRegistrarEgresoEdoCtaAutomaticos() && idsEdoCtaAutMontoFac.length>0 && !idsEdoCtaAutMontoFac[0].equals("")){
			//Si existen estados de cuenta autom�ticos, entonces ser�a necesaria una autorizaci�n 
			//para pagar estados de cuenta facultativos en el mismo egreso
			validarAutorizacionPagosFacultativosPorEdoCta = true;
			String idsEdoCtaAutomaticosMontoStr = idsEdoCtaAutMontoFac[0];
			String[] idsEdoCtaAutomaticosMontoPorSuscripcion = idsEdoCtaAutomaticosMontoStr.split(",");
			for(String idsEdoCtaAutomaticosMontoPorSuscripcionStr : idsEdoCtaAutomaticosMontoPorSuscripcion){
				String[] idsEdoCtaAutomaticoMonto = idsEdoCtaAutomaticosMontoPorSuscripcionStr.split("\\$");
				for(String idEdoCtaAutomaticoMontoStr: idsEdoCtaAutomaticoMonto){
					String[] idEdoCtaAutomaticoMonto = idEdoCtaAutomaticoMontoStr.split("_"); 
					String idEstadoCuentaAutomatico = idEdoCtaAutomaticoMonto[0];						
					String saldoTecnico = idEdoCtaAutomaticoMonto[1];
					
					EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = 
						new EstadoCuentaDecoradoDTO(EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(idEstadoCuentaAutomatico)));																			
					
					EgresoEstadoCuentaDTO egresoEstadoCuentaDTO = new EgresoEstadoCuentaDTO();
					egresoEstadoCuentaDTO.setEstadoCuentaDTO(estadoCuentaDecoradoDTO.getEstadoCuentaDTO());	    
				    egresoEstadoCuentaDTO.setCuentaAfirme(egresoReaseguro.getCuentaAfirme());
				    
				    BigDecimal montoEgreso = new BigDecimal(saldoTecnico).multiply(new BigDecimal(egresoReaseguro.getFactorTipoCambio()));
				    egresoEstadoCuentaDTO.setMonto(montoEgreso);
				    egresoEstadoCuentaDTO.setImpuesto(new BigDecimal("0"));
				
				    EgresoEstadoCuentaId egresoEstadoCuentaId = new EgresoEstadoCuentaId();
				    egresoEstadoCuentaId.setIdEstadoCuenta(estadoCuentaDecoradoDTO.getIdEstadoCuenta());				
				    egresoEstadoCuentaDTO.setId(egresoEstadoCuentaId);
				    
				    listaEgresosEstadosCuenta.add(egresoEstadoCuentaDTO);
				    
				    montoTotalEgreso = montoTotalEgreso.add(egresoEstadoCuentaDTO.getMonto()).setScale(10, RoundingMode.HALF_DOWN);
				    impuestoTotalEgreso = impuestoTotalEgreso.add(egresoEstadoCuentaDTO.getImpuesto()).setScale(10, RoundingMode.HALF_DOWN);
				}
			}
		}				
						
		//Se procesan los estados de cuenta facultativos
		HashMap<String,EgresoEstadoCuentaDTO> egresoEstadosCuentaFacultativos = new HashMap<String, EgresoEstadoCuentaDTO>();
		HashMap<String, ReciboDTO> ultimoReciboPagadoEndosoMap = new HashMap<String, ReciboDTO>();
		HashMap<BigDecimal, Boolean> autorizacionEstadosCuentaMap = new HashMap<BigDecimal, Boolean>();
		List<PagoCoberturaReaseguradorDTO> exhibiciones = new ArrayList<PagoCoberturaReaseguradorDTO>();
		boolean estadosCuentaRequierenAutorizacion = false;
		boolean exhibicionesRequierenAutorizacionFaltaPagoAsegurado = false;
		boolean exhibicionesRequierenAutorizacionVencimiento = false;
		if(egresoReaseguro.getRegistrarEgresoEdoCtaFacultativos() && idsEdoCtaAutMontoFac.length>1 && !idsEdoCtaAutMontoFac[1].equals("")){							
			String idsExhibicionesFacultativosStr = idsEdoCtaAutMontoFac[1];			
			String[] idsExhibicionesFacultativos = idsExhibicionesFacultativosStr.split(",");
												
			for(String idExhibicionFacultativoStr : idsExhibicionesFacultativos){
				String[] idExhibicionFacultativo = idExhibicionFacultativoStr.split("-");
				String idToPlanPagosCobertura = idExhibicionFacultativo[0];
				String numeroExhibicion = idExhibicionFacultativo[1];
				String idReasegurador = idExhibicionFacultativo[2];
				String idEstadoCuentaFacultativo = idExhibicionFacultativo[3];
				
				PagoCoberturaReaseguradorId idExhibicion = new PagoCoberturaReaseguradorId(Long.valueOf(idToPlanPagosCobertura),
																						   Short.valueOf(numeroExhibicion),
																						   new BigDecimal(idReasegurador));
				PlanPagosCoberturaDN planPagosCoberturaDN = PlanPagosCoberturaDN.getInstancia(null);
				PagoCoberturaReaseguradorDTO exhibicion = planPagosCoberturaDN.obtenerExhibicionPorId(idExhibicion);
								
				BigDecimal montoExhibicion = new BigDecimal(exhibicion.getMontoPagoTotal()).multiply(
															new BigDecimal(egresoReaseguro.getFactorTipoCambio())).
															setScale(10, RoundingMode.HALF_DOWN);
			    BigDecimal impuestoExhibicion = exhibicion.getRetenerImpuesto() ?  
			    					  			new BigDecimal(exhibicion.getMontoImpuesto()*egresoReaseguro.getFactorTipoCambio()).
												setScale(10, RoundingMode.HALF_DOWN) : 
			    					  			new BigDecimal(0D);
				
				if(!egresoEstadosCuentaFacultativos.containsKey(idEstadoCuentaFacultativo)){								
					EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = 
						new EstadoCuentaDecoradoDTO(EstadoCuentaDN.getINSTANCIA().getPorId(new BigDecimal(idEstadoCuentaFacultativo)));
				
					EgresoEstadoCuentaDTO egresoEstadoCuentaDTO = new EgresoEstadoCuentaDTO();
					egresoEstadoCuentaDTO.setEstadoCuentaDTO(estadoCuentaDecoradoDTO.getEstadoCuentaDTO());
					egresoEstadoCuentaDTO.setCuentaAfirme(egresoReaseguro.getCuentaAfirme());
														    					  
				    egresoEstadoCuentaDTO.setMonto(montoExhibicion);
				    egresoEstadoCuentaDTO.setImpuesto(impuestoExhibicion); 
				
				    EgresoEstadoCuentaId egresoEstadoCuentaId = new EgresoEstadoCuentaId();
				    egresoEstadoCuentaId.setIdEstadoCuenta(estadoCuentaDecoradoDTO.getIdEstadoCuenta());				
				    egresoEstadoCuentaDTO.setId(egresoEstadoCuentaId);
				    
				    egresoEstadosCuentaFacultativos.put(idEstadoCuentaFacultativo, egresoEstadoCuentaDTO);				    
				}else{
					EgresoEstadoCuentaDTO egresoEstadoCuentaDTO = egresoEstadosCuentaFacultativos.get(idEstadoCuentaFacultativo);
					BigDecimal montoAcumulado = egresoEstadoCuentaDTO.getMonto().add(montoExhibicion).setScale(10, RoundingMode.HALF_DOWN);
					egresoEstadoCuentaDTO.setMonto(montoAcumulado);					
					BigDecimal impuestoAcumulado = egresoEstadoCuentaDTO.getImpuesto().add(impuestoExhibicion).setScale(10, RoundingMode.HALF_DOWN);
					egresoEstadoCuentaDTO.setImpuesto(impuestoAcumulado);
				}
				
				EgresoEstadoCuentaDTO egresoEstadoCuentaDTO = egresoEstadosCuentaFacultativos.get(idEstadoCuentaFacultativo);
				EstadoCuentaDTO estadoCuenta = egresoEstadoCuentaDTO.getEstadoCuentaDTO();
				exhibicion.setEstadoCuenta(estadoCuenta);
				boolean[] estatusAutorizaciones = this.validarExhibicionEgreso(exhibicion, ultimoReciboPagadoEndosoMap, autorizacionEstadosCuentaMap, 
																		validarAutorizacionPagosFacultativosPorEdoCta, usuario);
				
				boolean estadoCuentaRequiereAutorizacion = estatusAutorizaciones[0];
				boolean exhibicionRequiereAutorizacionFaltaPagoAsegurado = estatusAutorizaciones[1]; 
				boolean exhibicionRequiereAutorizacionVencimiento = estatusAutorizaciones[2];
								
				if(estadoCuentaRequiereAutorizacion){
					estadosCuentaRequierenAutorizacion = true;
				}
				
				if(exhibicionRequiereAutorizacionFaltaPagoAsegurado){
					exhibicionesRequierenAutorizacionFaltaPagoAsegurado = true;
				}
				
				if(exhibicionRequiereAutorizacionVencimiento){
					exhibicionesRequierenAutorizacionVencimiento = true;
				}
									
				exhibicion = planPagosCoberturaDN.actualizarExhibicion(exhibicion);
				exhibiciones.add(exhibicion);				
			}
			
			for(EgresoEstadoCuentaDTO egresoEstadoCuentaDTO : egresoEstadosCuentaFacultativos.values()){
				listaEgresosEstadosCuenta.add(egresoEstadoCuentaDTO);				
				montoTotalEgreso = montoTotalEgreso.add(egresoEstadoCuentaDTO.getMonto()).setScale(10, RoundingMode.HALF_DOWN);
				impuestoTotalEgreso = impuestoTotalEgreso.add(egresoEstadoCuentaDTO.getImpuesto()).setScale(10, RoundingMode.HALF_DOWN);				
			}
		}
		
		
		if(estadosCuentaRequierenAutorizacion || exhibicionesRequierenAutorizacionFaltaPagoAsegurado || exhibicionesRequierenAutorizacionVencimiento){
			String mensaje = "Se requiere autorizaci�n para programar el pago:<br />";
			mensaje += "<ul>";							
			if(estadosCuentaRequierenAutorizacion) {
				mensaje += "<li>Existen estados de cuenta facultativos a pagar por estado de cuenta.</li>";
			}
			
			if(exhibicionesRequierenAutorizacionFaltaPagoAsegurado) {
				mensaje += "<li>No existe pago por parte del asegurado para cubrir ciertas exhibiciones.</li>";
			}
			
			if(exhibicionesRequierenAutorizacionVencimiento) {
				mensaje += "<li>Algunas exhibiciones tienen m�s de 45 d�as de vencimiento</li>";
			}
						
			mensaje += "</ul><br />";
			mensaje += "Por favor, informar a un supervisor.";
			
			throw new ExcepcionDeLogicaNegocio("EgresoReaseguroDN", mensaje);
		}
			
		
		
		egresoReaseguro.setImpuesto(impuestoTotalEgreso.doubleValue());
				
		egresoReaseguro.setMonto(montoTotalEgreso.doubleValue());		
		
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		egresoReaseguro = egresoReaseguroSN.agregarEgreso(egresoReaseguro, listaEgresosEstadosCuenta, exhibiciones);
		
		
		ReaseguradorCorredorDTO reasegurador = listaEgresosEstadosCuenta.get(0).getEstadoCuentaDTO().getReaseguradorCorredorDTO();
		ReaseguradorCorredorDTO corredor = listaEgresosEstadosCuenta.get(0).getEstadoCuentaDTO().getCorredorDTO();		
		egresoReaseguro.setReasegurador(reasegurador);
		egresoReaseguro.setCorredor(corredor);
		
		return egresoReaseguro;
	}
	/*
	public void agregarEgreso2(EgresoReaseguroDTO egresoReaseguro,
			List<EstadoCuentaDecoradoDTO> lista) throws RuntimeException {
		String montoFaltante = "";
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
		numberFormat.setMaximumFractionDigits(2);
		DecimalFormat formatoDec = new DecimalFormat("0.0000");
		DecimalFormat formatoDecTres = new DecimalFormat("0.000");
		double factorTipoCambio = 1;
		boolean banderaTC = false;
		String sumaStr;
		String montoEgresoStr;
		if (egresoReaseguro.getMonto().doubleValue() > 0) {
			double suma = 0;
			for (EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO : lista) {
				if (estadoCuentaDecoradoDTO.getMontoEgreso().doubleValue() > 0) {
					factorTipoCambio = 1;
					if (estadoCuentaDecoradoDTO.getIdMoneda() != egresoReaseguro
							.getIdMoneda()) {
						// La regla de negocio indica que el Tipo de cambio
						// siempre ser� proporcionado como Pesos por D�lar
						factorTipoCambio = egresoReaseguro.getTipoCambio()
								.doubleValue();
						if (egresoReaseguro.getIdMoneda() == IngresoReaseguroDN.NACIONAL) { // Cuando el factor es de D�lares a Pesos
							banderaTC = false;
						} else {
							if (egresoReaseguro.getIdMoneda() == IngresoReaseguroDN.DOLARES) { // Cuando el factor es de Pesos a D�lares
								if (egresoReaseguro.getTipoCambio().equals(
										new Double("0")))
									throw new RuntimeException(
											UtileriasWeb
													.getMensajeRecurso(
															Sistema.ARCHIVO_RECURSOS,
															"reaseguro.egreso.administrar.egresos.relacionaregreso.error.tipocambiocero"));
								else
									banderaTC = true;
							}
						}
					}

					if (banderaTC)
						suma = new Double(formatoDec.format(suma)).doubleValue()
								+ new Double(formatoDec.format(estadoCuentaDecoradoDTO.getMontoEgreso().doubleValue())).doubleValue()
								/ new Double(formatoDec.format(factorTipoCambio)).doubleValue();
					else
						suma = new Double(formatoDec.format(suma)).doubleValue()
								+ new Double(formatoDec.format(estadoCuentaDecoradoDTO.getMontoEgreso().doubleValue())).doubleValue()
								* new Double(formatoDec.format(factorTipoCambio)).doubleValue();
				} else
					throw new RuntimeException(
							UtileriasWeb.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"reaseguro.egreso.administrar.egresos.relacionaregreso.error.montocero"));
			}

			sumaStr =  formatoDecTres.format(suma);
			montoEgresoStr =  formatoDecTres.format(egresoReaseguro.getMonto().doubleValue());
			if (!sumaStr.equals(montoEgresoStr)) {
				//suma = egresoReaseguro.getMonto().doubleValue()- suma;
				suma = new Double(montoEgresoStr).doubleValue() - new Double(sumaStr).doubleValue();
				if (suma > 0){
					double dvalor ;
				    dvalor = suma / factorTipoCambio;
					montoFaltante = "<br/>Falta por cubrir la cantidad de "
							+ formatoDec.format(dvalor) ;}
				else{
					double dvalor ;
				    dvalor = suma / factorTipoCambio;
					montoFaltante = "<br/>Se ha excedido el monto de egreso por la cantidad de "
							+ formatoDec.format(dvalor * -1);
				}

				if (egresoReaseguro.getIdMoneda() == IngresoReaseguroDN.NACIONAL)
					montoFaltante = montoFaltante + " MXN";
				if (egresoReaseguro.getIdMoneda() == IngresoReaseguroDN.DOLARES)
					montoFaltante = montoFaltante + " USD";

				throw new RuntimeException(
						UtileriasWeb
								.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
										"reaseguro.egreso.administrar.egresos.relacionaregreso.validarmonto.error")
								+ montoFaltante);
			}

			//EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
			//egresoReaseguroSN.agregarEgreso(egresoReaseguro, lista);
		} else {
			throw new RuntimeException(
					UtileriasWeb
							.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS,
									"reaseguro.egreso.administrar.egresos.relacionaregreso.error.montoegresocero"));
		}

	}
	*/

	public void confirmarPago(EgresoReaseguroDTO egreso)
			throws RuntimeException {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		egresoReaseguroSN.confirmarPago(egreso);
	}

	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgreso(BigDecimal idToEgresoReaseguro) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();

		return egresoReaseguroSN.obtenerEstadosCuentaEgreso(idToEgresoReaseguro);

	}

	public List<ParticipacionDecoradoDTO> obtenerParticipaciones(
			EstadoCuentaDecoradoDTO estadoCuenta) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();

		return egresoReaseguroSN.obtenerParticipaciones(estadoCuenta);
	}

	public List<ParticipacionCorredorDecoradoDTO> obtenerParticipaciones(
			ParticipacionDecoradoDTO participacion, BigDecimal idContratoFacultativo, BigDecimal idEgresoReaseguro) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.obtenerParticipaciones(participacion, idContratoFacultativo, idEgresoReaseguro);
	}

	public List<PagoDTO> obtenerPagosContrato(
			EstadoCuentaDecoradoDTO estadoCuenta, BigDecimal idEgresoReaseguro) throws SystemException {
		List<PagoDTO> listaPagos = new ArrayList<PagoDTO>();
		@SuppressWarnings("unused")
		ContratoFacultativoDTO contratoFacultativoDTO;
		List<EstadoCuentaDTO> listaEstadosCuenta;
		List<EgresoEstadoCuentaDTO> listaEgresosEstadoCuenta;
		PagoDTO pagoDTO;
		if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
			contratoFacultativoDTO = estadoCuenta.getContratoFacultativoDTO();

			listaEstadosCuenta = new ArrayList<EstadoCuentaDTO>();
			listaEstadosCuenta = EstadoCuentaDN.getINSTANCIA()
					.obtenerSerieEstadosCuenta(estadoCuenta);
			for (EstadoCuentaDTO estadoCuentaDTO : listaEstadosCuenta) {
				listaEgresosEstadoCuenta = new ArrayList<EgresoEstadoCuentaDTO>();
				listaEgresosEstadoCuenta = this
						.obtenerEgresosPorEstadoCuenta(estadoCuentaDTO, idEgresoReaseguro);
				Integer numeroPago = 1;
				for (EgresoEstadoCuentaDTO egresoEstadoCuentaDTO : listaEgresosEstadoCuenta) {
					if (egresoEstadoCuentaDTO.getidEstatus()==null){
						egresoEstadoCuentaDTO.setidEstatus(0);
					}
					pagoDTO = new PagoDTO(egresoEstadoCuentaDTO);
					pagoDTO.setNumeroPago(egresoEstadoCuentaDTO.getEgresoReaseguro().getReferencia());
					pagoDTO.setSuscripcion(numeroPago.toString());
					pagoDTO.setMonto(egresoEstadoCuentaDTO.getMonto());
					pagoDTO.setFechaPago(egresoEstadoCuentaDTO
							.getEgresoReaseguro().getFechaEgreso());					
					listaPagos.add(pagoDTO);
					numeroPago = numeroPago + 1;
				}
			}

		}

		return listaPagos;
	}

	private List<EgresoEstadoCuentaDTO> obtenerEgresosPorEstadoCuenta(
			EstadoCuentaDTO estadoCuenta, BigDecimal idEgresoReaseguro) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();

		return egresoReaseguroSN.obtenerEgresosPorEstadoCuenta(estadoCuenta, idEgresoReaseguro);
	}

	public PolizaRelacionadaDTO obtenerPolizaRelacionada(
			EstadoCuentaDecoradoDTO estadoCuenta)
			throws ExcepcionDeAccesoADatos, SystemException {
		EndosoSoporteDanosDTO endosoSoporteDanosDTO;
		PolizaRelacionadaDTO polizaRelacionadaDTO = null;

		if (estadoCuenta.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO) {
			endosoSoporteDanosDTO = SoporteDanosDN.getInstancia()
					.getEndosoPorId(estadoCuenta.getIdPoliza(),
							new BigDecimal(estadoCuenta.getNumeroEndoso()));
			polizaRelacionadaDTO = new PolizaRelacionadaDTO(
					endosoSoporteDanosDTO, estadoCuenta.getSubRamoDTO());
		}

		return polizaRelacionadaDTO;
	}
	
	//@SuppressWarnings("unused")
	/*
	private List<EgresoEstadoCuentaDTO> borrarEgresoDTO(
			EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.borrarEgresoDTO(egresoReaseguroId, idEstadoCuenta);
	}
	*/

	
	public ContabilidadOrdenPagoDTO generarAsientoOrdenDePago(EgresoReaseguroDTO egresoReaseguroDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException {		
		ContabilidadOrdenPagoId idAsientoContable = new ContabilidadOrdenPagoId();
		idAsientoContable.setIdToOrdenPago(egresoReaseguroDTO.getIdEgresoReaseguro());
		idAsientoContable.setIdToAutorizacionTecnica(BigDecimal.ZERO);
		ContabilidadOrdenPagoDTO asientoContable = new ContabilidadOrdenPagoDTO();
		asientoContable.setId(idAsientoContable);
		asientoContable.setPtransCont("REASEGURO-PAGO");
		asientoContable.setIdMoneda(new BigDecimal(egresoReaseguroDTO.getIdMoneda()));
		asientoContable.setTipoCambio(egresoReaseguroDTO.getTipoCambio());

		if(egresoReaseguroDTO.getCorredor() != null && 
		   egresoReaseguroDTO.getCorredor().getIdtcreaseguradorcorredor() != null && 
		   egresoReaseguroDTO.getCorredor().getIdtcreaseguradorcorredor().compareTo(BigDecimal.ZERO) > 0){
			asientoContable.setIdPrestadorServicio(egresoReaseguroDTO.getCorredor().getIdContable());
			asientoContable.setPBeneficiario(egresoReaseguroDTO.getCorredor().getNombre());				
		}
		else{
			asientoContable.setIdPrestadorServicio(egresoReaseguroDTO.getReasegurador().getIdContable());
			asientoContable.setPBeneficiario(egresoReaseguroDTO.getReasegurador().getNombre());				
		}

		asientoContable.setTipoPago("SC");
		asientoContable.setConceptoPol("PAGO DE REASEGURO MIDAS " + egresoReaseguroDTO.getIdEgresoReaseguro());		

		return asientoContable;
	}
	/*
	// Esta es una funcion nueva
	public List<ContabilidadOrdenPagoDTO> generarAsientosOrdenDePago2(EgresoReaseguroDTO egresoReaseguroDTO, BigDecimal idToOrdenPago,List<EstadoCuentaDTO> listaEstadosCuenta, String usuario) throws ExcepcionDeAccesoADatos, SystemException {
		List<ContabilidadOrdenPagoDTO> asientosOrdenDePago = new ArrayList<ContabilidadOrdenPagoDTO>();
		
		BigDecimal prestadorServicios = null;
		BigDecimal totalSumaAsegurada = new BigDecimal("0.00"); 
		Double tipoDeCambio = null;
		Double importeCero = new Double(0.00);

		for (EstadoCuentaDTO cobertura : listaEstadosCuenta) {
			
			EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
			List<EgresoEstadoCuentaDTO> listaEgresosEstadoCuenta;
			listaEgresosEstadoCuenta = new ArrayList<EgresoEstadoCuentaDTO>();
			listaEgresosEstadoCuenta = egresoReaseguroSN.obtenerEstadosCuentaEgreso(idToOrdenPago);
			totalSumaAsegurada = listaEgresosEstadoCuenta.get(0).getMonto();
						
			if (listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getTipoCambio() == null || listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getTipoCambio().intValue() == 0) {
				if(listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getIdMoneda() == Sistema.MONEDA_DOLARES)
					tipoDeCambio = TipoCambioDN.getInstancia(usuario).obtieneTipoCambioPorDia(new java.util.Date(), (short)Sistema.MONEDA_DOLARES);
				else
					tipoDeCambio = new Double(1);
			} else {
				tipoDeCambio = listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getTipoCambio();//TipoCambioDN.getInstancia(usuario).obtieneTipoCambioPorDia(new Date(), Short.parseShort("1"));
			}
				
			ContabilidadOrdenPagoId idAsientoContable = new ContabilidadOrdenPagoId();
			idAsientoContable.setIdToOrdenPago(idToOrdenPago);
			idAsientoContable.setIdSubr(cobertura.getSubRamoDTO().getCodigoSubRamo());

			ContabilidadOrdenPagoDTO asientoContable = new ContabilidadOrdenPagoDTO();
			asientoContable.setId(idAsientoContable);

			asientoContable.setIdRamo(cobertura.getSubRamoDTO().getRamoDTO().getIdTcRamo());
			
			asientoContable.setConceptoPol("PAGO DE REASEGURO MIDAS " + idToOrdenPago);
			asientoContable.setIdMoneda(new BigDecimal(egresoReaseguroDTO.getIdMoneda()));
			asientoContable.setTipoCambio(tipoDeCambio);
			asientoContable.setAuxiliar(new Integer(SoporteContabilidad.AUXULIAR)); 
			asientoContable.setCcosto(SoporteContabilidad.CENTRO_COSTOS); 
			asientoContable.setUsuario(usuario);
			asientoContable.setEstatus(new BigDecimal(0));
		
			asientoContable.setIdAgente(cobertura.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			asientoContable.setImpSalvamento(importeCero);
			asientoContable.setImpPrimasCob(importeCero);				
								
			if(prestadorServicios == null){
				prestadorServicios = new BigDecimal("-1");
			}				
													
			asientoContable.setImpDeducible(importeCero);
			asientoContable.setImpCoaseguro(importeCero);
			
			ReaseguradorCorredorDTO reaseguradorCorredorDTO = new ReaseguradorCorredorDTO();
			reaseguradorCorredorDTO.setIdtcreaseguradorcorredor(cobertura.getReaseguradorCorredorDTO().getIdtcreaseguradorcorredor());
			try {
				reaseguradorCorredorDTO = ReaseguradorCorredorDN.getInstancia().obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			} 	
					
			if( cobertura.getCorredorDTO() != null && cobertura.getCorredorDTO().getIdtcreaseguradorcorredor() != null && cobertura.getCorredorDTO().getIdtcreaseguradorcorredor().compareTo(BigDecimal.ZERO) > 0){
				asientoContable.setPBeneficiario(cobertura.getCorredorDTO().getNombre());
				asientoContable.setIdPrestadorServicio(cobertura.getCorredorDTO().getIdContable());
			}
			else{
				asientoContable.setPBeneficiario(cobertura.getReaseguradorCorredorDTO().getNombre());
				asientoContable.setIdPrestadorServicio(reaseguradorCorredorDTO.getIdContable());
			}
			asientoContable.setTipoPago("SC");
				
			asientoContable.setImpNeto(totalSumaAsegurada.doubleValue());
			asientoContable.setPtransCont("REASEGURO-PAGO"); 
			asientoContable.setCptoPago(cobertura.getIdEstadoCuenta().shortValue());				
					
			asientoContable.setImpIva(importeCero);
			asientoContable.setImpIvaRet(importeCero);
			asientoContable.setImpIsrRet(importeCero);
			asientoContable.setImpOtrosImpuestos(importeCero);																			
				
				
			asientosOrdenDePago.add(asientoContable);
		}

		return asientosOrdenDePago;
	}
	*/

	@SuppressWarnings("unused")
	private Object ultimoEgresoDTO() {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.ultimoEgresoDTO();
	}
	
	
	public void procesarOrdenesPendientes() throws Exception{
		String nombreUsuario = "SISTEMA";
		SolicitudChequeDN solicitudChequeDN = SolicitudChequeDN.getInstancia(null);		
	
		List<EgresoReaseguroDTO> listaEgresosReaseguro = this.obtenerEgresosPendientes();
						
		if (listaEgresosReaseguro != null) {
			for (EgresoReaseguroDTO egresoReaseguroDTO : listaEgresosReaseguro) {				
				LogDeMidasWeb.log("Consultando estatus actual de la solicitud de cheque " +
						"(idEgresoReaseguro=" + egresoReaseguroDTO.getIdEgresoReaseguro()+")",
						Level.INFO, null);
				
				List<EgresoEstadoCuentaDTO> listaEgresosEstadoCuenta = obtenerEstadosCuentaEgreso(egresoReaseguroDTO.getIdEgresoReaseguro());
				BigDecimal noCheque = listaEgresosEstadoCuenta.get(0).getIdCheque();
				
				SolicitudChequeDTO solicitudCheque = solicitudChequeDN.consultaEstatusSolicitudCheque(noCheque);
												
				if (solicitudCheque != null) {
					this.actualizaOrdenCheque(egresoReaseguroDTO, noCheque, solicitudCheque.getSituacionPago(),nombreUsuario);
					
					LogDeMidasWeb.log("Ejecucion exitosa en la Actualizacion de estatus la solicitud de cheque con id de Orden de pago " +
							          "(idToEgresoReaseguro = "+egresoReaseguroDTO.getIdEgresoReaseguro()+")",
							          Level.INFO, null);
				}
			}
		}		
	}
	
	
	public void procesarOrdenesCancel() throws Exception{
		//String nombreUsuario = "SISTEMA";
		SolicitudChequeDN solicitudChequeDN = SolicitudChequeDN.getInstancia(null);
		String estatusActual = null;
	
		List<EgresoReaseguroDTO> egresosPorCancelar = obtenerConfiguracionPagosEgresoContratoFacultativo();
						
		if (egresosPorCancelar != null) {			
			for (EgresoReaseguroDTO egresoReaseguroDTO : egresosPorCancelar) {				
				LogDeMidasWeb.log("Cancelando egreso (idToEgresoReaseguro = " + egresoReaseguroDTO.getIdEgresoReaseguro() +")", Level.INFO, null);
				
				List<EgresoEstadoCuentaDTO> listaEgresosEstadoCuenta = obtenerEstadosCuentaEgreso(egresoReaseguroDTO.getIdEgresoReaseguro());
				BigDecimal noCheque = listaEgresosEstadoCuenta.get(0).getIdCheque();
				
				estatusActual = solicitudChequeDN.cancelaSolicitudCheque(noCheque);
				
				if (estatusActual != null) {
					this.actualizaOrdenChequeCancel(egresoReaseguroDTO, noCheque, estatusActual);
					
					LogDeMidasWeb.log("Ejecucion exitosa de cancelaci�n de egreso " +
									  "(idToEgresoReaseguro = " + egresoReaseguroDTO.getIdEgresoReaseguro() +")",
									  Level.INFO, null);
				}
			}
		}		
	}
	
	
	
	public  List<EgresoEstadoCuentaDTO> obtenerEgresosEstadosCuentaPendientes()	throws ExcepcionDeAccesoADatos, SystemException {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.buscar();
	}
	
	public  List<EgresoEstadoCuentaDTO> findByPropertyCancel()	throws ExcepcionDeAccesoADatos, SystemException {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.buscarCancel();
	}
	
	private void actualizaOrdenCheque(EgresoReaseguroDTO egresoReaseguro, BigDecimal noCheque, String estatusSolicitud, String nombreUsuario) 
	throws ExcepcionDeLogicaNegocio{
		Short estatus = null;
		BigDecimal idEgresoReaseguro = egresoReaseguro.getIdEgresoReaseguro();
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();		
						
		if(estatusSolicitud != null && idEgresoReaseguro != null){			
			estatus = estatusSolicitud.equalsIgnoreCase("PROCESO") ? EgresoReaseguroDTO.ESTATUS_PENDIENTE :
				      estatusSolicitud.equalsIgnoreCase("TERMINADO") ? EgresoReaseguroDTO.ESTATUS_PAGADO : 
					  estatusSolicitud.equalsIgnoreCase("CANCELADO")? EgresoReaseguroDTO.ESTATUS_CANCELADO :
					  estatusSolicitud.equalsIgnoreCase("EN SOLICITUD MIZAR")? EgresoReaseguroDTO.ESTATUS_EN_SOLICITUD_MIZAR :
					  null;
		}
			
		if(estatus != null){
			if(egresoReaseguro.getEstatus() != estatus){
				//S�lo se actualiza el egreso si cambio de estatus.
				egresoReaseguroSN.actualiza(idEgresoReaseguro, new Integer(estatus), noCheque);
			}else{
				LogDeMidasWeb.log("Estatus de solicitud de cheque sin cambio: " + estatusSolicitud +" " +
			         "(idToEgresoReaseguro = "+egresoReaseguro.getIdEgresoReaseguro()+")", Level.INFO, null);
			}
		}else{
			LogDeMidasWeb.log("Estatus de solicitud desconocido: " + estatusSolicitud +" " +
					         "(idToEgresoReaseguro = "+egresoReaseguro.getIdEgresoReaseguro()+")", Level.SEVERE, null);
			throw new ExcepcionDeLogicaNegocio("EgresoReaseguroDN","El egreso tiene un estatus desconocido. Estatus actual: "+estatusSolicitud);
		}		
	}
	/*
	private void actualizaOrdenCheque2(BigDecimal idEgresoReaseguro, String estatusSolicitud, String nombreUsuario) throws Exception{
//		EgresoEstadoCuentaDTO egresoEstadoCuentaDTO;	
		BigDecimal egresoReaseguro, noCheque; 
		Integer estatus;		
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		List<EgresoEstadoCuentaDTO> listaEgresosEstadoCuenta;
		listaEgresosEstadoCuenta = new ArrayList<EgresoEstadoCuentaDTO>();
		listaEgresosEstadoCuenta = egresoReaseguroSN.obtenerEstadosCuentaEgreso(idEgresoReaseguro);
		noCheque = listaEgresosEstadoCuenta.get(0).getIdCheque();
		
		
		if(estatusSolicitud != null && idEgresoReaseguro != null){
			if(estatusSolicitud.equalsIgnoreCase("PROCESO")){
				LogDeMidasWeb.log("La solicitud esta en proceso : " + idEgresoReaseguro.toString(), Level.INFO, null);
			}else if(estatusSolicitud.equalsIgnoreCase("EN SOLICITUD MIZAR")){
				LogDeMidasWeb.log("La solicitud fue requerida a MIZAR : " + idEgresoReaseguro.toString(), Level.INFO, null);
			}else if(estatusSolicitud.equalsIgnoreCase("CANCELADO")){
				EgresoReaseguroSN egresoReaseguroSN2 = new EgresoReaseguroSN();		
				
				try{
					EgresoReaseguroDTO egresoReaseguroDTO = this.obtenerEgresosEstadoCuentaCheque(listaEgresosEstadoCuenta.get(0).getEstadoCuentaDTO().getIdEstadoCuenta() ,listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getIdEgresoReaseguro()); //(idOrdenPago);		
					//aqui cambiar el query
					if(egresoReaseguroDTO!= null){						
						estatus = OrdenDePagoDTO.ESTATUS_CANCELADA;
						egresoReaseguro = egresoReaseguroDTO.getIdEgresoReaseguro();
						
						egresoReaseguroSN2.actualiza(egresoReaseguro, estatus, noCheque);									
					}
				}catch(RuntimeException ex){
					LogDeMidasWeb.log("Error en actualizaOrdenCheque, actualizar estatus" + estatusSolicitud, Level.WARNING, ex);
				}				
			}else if(estatusSolicitud.equalsIgnoreCase("TERMINADO")){
				EgresoReaseguroSN egresoReaseguroSN2 = new EgresoReaseguroSN();			
				//EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();	
				try{
					EgresoReaseguroDTO egresoReaseguroDTO = this.obtenerEgresosEstadoCuentaCheque(listaEgresosEstadoCuenta.get(0).getEstadoCuentaDTO().getIdEstadoCuenta() ,listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getIdEgresoReaseguro());								
					if(egresoReaseguroDTO!= null){						
						estatus = OrdenDePagoDTO.ESTATUS_PAGADA;
						egresoReaseguro = egresoReaseguroDTO.getIdEgresoReaseguro();
						egresoReaseguroSN2.actualiza(egresoReaseguro, estatus, noCheque);	
						EgresoReaseguroDN.getINSTANCIA().confirmarPago(egresoReaseguroDTO);
					}
				}catch (Exception e) {
					LogDeMidasWeb.log(e.getMessage(), Level.WARNING, e);
					throw e;
				}								
			}else{
				LogDeMidasWeb.log("Estatus de solicitud desconocido : " + estatusSolicitud, Level.INFO, null);
			}
		}		
	}
	*/
	public void actualizaOrdenChequeCancel(EgresoReaseguroDTO egresoReaseguro, BigDecimal noCheque, String estatusSolicitud) 
	throws ExcepcionDeLogicaNegocio{
		Short estatus = null;
		BigDecimal idEgresoReaseguro = egresoReaseguro.getIdEgresoReaseguro();
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();		
						
		if(estatusSolicitud != null && idEgresoReaseguro != null){			
			estatus = estatusSolicitud.equalsIgnoreCase("PROCESO") ? EgresoReaseguroDTO.ESTATUS_PENDIENTE :
				      estatusSolicitud.equalsIgnoreCase("TERMINADO") ? EgresoReaseguroDTO.ESTATUS_PAGADO : 
					  estatusSolicitud.equalsIgnoreCase("CANCELADO")? EgresoReaseguroDTO.ESTATUS_CANCELADO :
					  estatusSolicitud.equalsIgnoreCase("EN SOLICITUD MIZAR")? EgresoReaseguroDTO.ESTATUS_EN_SOLICITUD_MIZAR :
					  null;
		}
			
		if(estatus != null && egresoReaseguro.getEstatus() != estatus && estatus.compareTo(EgresoReaseguroDTO.ESTATUS_CANCELADO)==0){
			egresoReaseguroSN.actualiza(idEgresoReaseguro, new Integer(estatus), noCheque);						
		}else{			
			LogDeMidasWeb.log("El egreso no pudo ser cancelado. Estatus actual: "+estatusSolicitud +
					         "(idToEgresoReaseguro = "+egresoReaseguro.getIdEgresoReaseguro()+")", Level.SEVERE, null);
			throw new ExcepcionDeLogicaNegocio("EgresoReaseguroDN", "El egreso no pudo ser cancelado. Estatus actual: "+estatusSolicitud);
		}	
	}
	/*
	public void actualizaOrdenChequeCancel2(BigDecimal idOrdenPago, String estatusSolicitud, String nombreUsuario) throws Exception{
//		EgresoEstadoCuentaDTO egresoEstadoCuentaDTO;	
		BigDecimal egresoReaseguro, noCheque; 
		Integer estatus;		
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		List<EgresoEstadoCuentaDTO> listaEgresosEstadoCuenta;
		listaEgresosEstadoCuenta = new ArrayList<EgresoEstadoCuentaDTO>();
		listaEgresosEstadoCuenta = egresoReaseguroSN.obtenerEstadosCuentaEgreso(idOrdenPago);
		noCheque = listaEgresosEstadoCuenta.get(0).getIdCheque();
		
		
		if(estatusSolicitud != null && idOrdenPago != null){
			if(estatusSolicitud.equalsIgnoreCase("PROCESO")){
				LogDeMidasWeb.log("La solicitud esta en proceso : " + idOrdenPago.toString(), Level.INFO, null);
			}else if(estatusSolicitud.equalsIgnoreCase("EN SOLICITUD MIZAR")){
				LogDeMidasWeb.log("La solicitud fue requerida a MIZAR : " + idOrdenPago.toString(), Level.INFO, null);
			}else if(estatusSolicitud.equalsIgnoreCase("CANCELADO")){
				EgresoReaseguroSN egresoReaseguroSN2 = new EgresoReaseguroSN();		
				
				try{
					EgresoReaseguroDTO egresoReaseguroDTO = this.obtenerEgresosEstadoCuentaCheque(listaEgresosEstadoCuenta.get(0).getEstadoCuentaDTO().getIdEstadoCuenta() ,listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getIdEgresoReaseguro());								
					if(egresoReaseguroDTO!= null){						
						estatus = 3;
						egresoReaseguro = egresoReaseguroDTO.getIdEgresoReaseguro();
						
						egresoReaseguroSN2.actualiza(egresoReaseguro, estatus, noCheque);									
					}
				}catch(RuntimeException ex){
					LogDeMidasWeb.log("Error en actualizaOrdenCheque, actualizar estatus" + estatusSolicitud, Level.WARNING, ex);
				}				
			}else if(estatusSolicitud.equalsIgnoreCase("TERMINADO")){
				EgresoReaseguroSN egresoReaseguroSN2 = new EgresoReaseguroSN();			
				//EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();	
				try{
					EgresoReaseguroDTO egresoReaseguroDTO = this.obtenerEgresosEstadoCuentaCheque(listaEgresosEstadoCuenta.get(0).getEstadoCuentaDTO().getIdEstadoCuenta() ,listaEgresosEstadoCuenta.get(0).getEgresoReaseguro().getIdEgresoReaseguro()); //this.getPorId(idOrdenPago);								
					if(egresoReaseguroDTO!= null){						
						estatus = 4;
						egresoReaseguro = egresoReaseguroDTO.getIdEgresoReaseguro();
						//egresoReaseguroSN2.borrarEgresoDTO(egresoReaseguroDTO, listaEgresosEstadoCuenta.get(0).getEstadoCuentaDTO().getIdEstadoCuenta());							
					}
				}catch (Exception e) {
					LogDeMidasWeb.log(e.getMessage(), Level.WARNING, e);
					throw e;
				}								
			}else{
				LogDeMidasWeb.log("Estatus de solicitud desconocido : " + estatusSolicitud, Level.INFO, null);
			}
		}		
	}
	*/
	public void actualizaChequeCancel(EgresoReaseguroDTO egresoReaseguro, BigDecimal noCheque) throws Exception{
		Short estatus = EgresoReaseguroDTO.ESTATUS_EN_PROCESO_DE_CANCELACION;
		BigDecimal idEgresoReaseguro = egresoReaseguro.getIdEgresoReaseguro();
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
											
		if(egresoReaseguro.getEstatus() != estatus){
			egresoReaseguroSN.actualiza(idEgresoReaseguro, new Integer(estatus), noCheque);						
		}
	}	
	
	public EgresoReaseguroDTO obtenerEgresosEstadoCuentaCheque(BigDecimal idEstadoCuenta, BigDecimal idEgresoReaseguro) {
		EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
		return egresoReaseguroSN.obtenerEgresosEstadoCuentaCheque(idEstadoCuenta, idEgresoReaseguro);
	}
	
	
	public List<EgresoEstadoCuentaDTO> buscarPorPropiedad(String propiedad, Object valor) throws Exception {
		try{
			EgresoReaseguroSN egresoReaseguroSN = new EgresoReaseguroSN();
			return egresoReaseguroSN.buscarPorPropiedad(propiedad, valor);
		}catch (Exception e) {
			LogDeMidasWeb.log(e.getMessage(), Level.WARNING, e);
			throw e;
		}			
	}

	public ParticipacionReaseguradorContratoFacultativoDTO obtenerParticipacionesEgresoContratoFacultativo(EstadoCuentaDecoradoDTO estadoCuenta){
		return new EgresoReaseguroSN().obtenerParticipacionesEgresoContratoFacultativo(estadoCuenta);
	}
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerConfiguracionPagosEgresoContratoFacultativo(BigDecimal idToEgresoReaseguro){
		return new EgresoReaseguroSN().obtenerConfiguracionPagosEgresoContratoFacultativo(idToEgresoReaseguro);
	}
	
	public void envioCorreoOrdenPago(BigDecimal idToEgresoReaseguro,List<String> listaDestinatarios,String nombreUsuario){
		if(idToEgresoReaseguro != null && listaDestinatarios != null && !listaDestinatarios.isEmpty()){
			String titulo = "Orden de Pago " + UtileriasWeb.getFechaHoraString(new Date()) + "hrs";
			String contenido = "Orden de pago generada el " + UtileriasWeb.getFechaStringConNombreMes(new Date()) + " a las " + UtileriasWeb.getHoraString(new Date()) + "hrs por " + nombreUsuario+ ". " +
			"<br /><i>Este mensaje es auto-generado por el sistema MIDAS v "+Sistema.VERSION_SISTEMA+", por favor no responda a este e-mail.</i>";
			    
			try {
				
				ReporteOrdenPago reporteOrdenPago = new ReporteOrdenPago();
				reporteOrdenPago.setIdEgresoReaseguro(idToEgresoReaseguro);
				
				byte[] reporte = reporteOrdenPago.obtenerReporte(nombreUsuario);
			    
			    if(reporte != null && reporte.length >0){
			    	List<byte[]> listaBytesReporte = new ArrayList<byte[]>();
			    	listaBytesReporte.add(reporte);
			    }
			
			    ByteArrayAttachment byteArrayAttachment = new ByteArrayAttachment("ordenPago" + ReportesReaseguro.obtenerSufijoArchivoReporte() + ".pdf", ByteArrayAttachment.TipoArchivo.PDF, reporte);
				List<ByteArrayAttachment> listaAdjuntos = new ArrayList<ByteArrayAttachment>();
				listaAdjuntos.add(byteArrayAttachment);
				
			    MailAction.enviaCorreo( listaDestinatarios, titulo, contenido,listaAdjuntos);
			    
			} catch (SystemException e) {
				LogDeMidasWeb.log("Error al intentar enviar correo con la orden de pago del egreso reaseguro. idToEgresoReaseguro:"+idToEgresoReaseguro, Level.SEVERE, e);
			}
		}
		else{
			LogDeMidasWeb.log("Se recibieron par�metros inv�lidos para env�o de orden de pago por correo. idToEgresoReaseguro: "+idToEgresoReaseguro+", listaDestinatarios:"+listaDestinatarios, Level.WARNING, null);
		}
	}
	
	public void envioCorreoOrdenPago(BigDecimal idToEgresoReaseguro,String destinatario,String nombreUsuario){
		if(idToEgresoReaseguro != null && destinatario != null && !UtileriasWeb.esCadenaVacia(destinatario)){
			List<String> listaDestinatarios = new ArrayList<String>();
			envioCorreoOrdenPago(idToEgresoReaseguro, listaDestinatarios,nombreUsuario);
		}
	}
	
	public void actualiza(BigDecimal idEgresoReaseguro, Integer estatus, BigDecimal idSolicitudCheque){
		new EgresoReaseguroSN().actualiza(idEgresoReaseguro, estatus, idSolicitudCheque);
	}
	
	public void eliminarEgreso(BigDecimal idEgresoReaseguro) {
		new EgresoReaseguroSN().eliminarEgreso(idEgresoReaseguro);
	}
	
	public List<EgresoReaseguroDTO> obtenerConfiguracionPagosEgresoContratoFacultativo(){
		return new EgresoReaseguroSN().obtenerEgresosCancelacionPendiente();
	}
	
	/**
	 * Consulta los pagos pendientes de porgramar previos a la fecha de corte recibida.
	 * @param fechaCorte la fecha de corte a la cual se consultar�n los pagos. Si se recibe null, se omite este par�metro en el filtrado.
	 * @return List<ParticipacionReaseguradorContratoFacultativoDTO>
	 */
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerPagosPendientes(Date fechaCorte){
		return new EgresoReaseguroSN().obtenerPagosPendientes(fechaCorte);
	}
	
	public void envioCorreoMonitoreoPagosPendientes(Date fechaCorteConsulta){
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones = null;
		
		String titulo = "Midas. Pagos pendientes por programar." + UtileriasWeb.getFechaHoraString(new Date()) + "hrs";
		String contenido = null;
		try{
			listaParticipaciones = obtenerPagosPendientes(fechaCorteConsulta);
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			if(listaParticipaciones != null && !listaParticipaciones.isEmpty()){
				contenido = "Se encontraron "+listaParticipaciones.size()+" pago(s) pendiente(s) por programar ";
				if(fechaCorteConsulta != null){
					contenido += "al d&iacute;a "+formato.format(fechaCorteConsulta)+". ";
				}else{
					contenido += ". ";
				}
				contenido += "<br>";
				contenido += "<table><tr>";
				contenido += "<th>P&oacute;liza</th>"+"<th>Endoso</th>"+"<th>Inciso</th>"+"<th>SubInciso</th>"+"<th>Seccion</th>"+"<th>Cobertura</th>"+"<th>Sub Ramo</th>"+
					"<th>Reasegurador</th>"+"<th>Corredor</th>"+"<th>No. Exhibici&oacute;n</th>"+"<th>Fecha de pago</th>"+"<th>Monto</th></tr>";
				contenido += obtenerContenido(listaParticipaciones);
				contenido += "</table><br>";
			}else{
				contenido = "No se encontraron pagos pendientes por programar. <br>";
			}
			contenido += "Gracias por su atenci&oacute;n, por favor no responda este mensaje.";
	
			List<String> destinatarios = new ArrayList<String>();
			if(!UtileriasWeb.esCadenaVacia(Sistema.DESTINATARIOS_MONITOREO_PAGOS_PENDIENTES_REASEGURO)){
				String[] dest = Sistema.DESTINATARIOS_MONITOREO_PAGOS_PENDIENTES_REASEGURO.split(",");
				for(int i=0;i<dest.length;i++){
					destinatarios.add(dest[i]);
				}
			}
			else{
				destinatarios.add(Sistema.EMAIL_ADMINISTRADOR_REASEGURO);
			}
			contenido = UtileriasWeb.codificarCadenaISO_8859_1(contenido);
			titulo = UtileriasWeb.codificarCadenaISO_8859_1(titulo);
			MailAction.enviaCorreo(destinatarios, titulo, contenido);
		}catch(Exception e){
			LogDeMidasWeb.log("Error al intentar enviar correo con el monitoreo de pagos pendientes por programar.", Level.SEVERE, e);
		}
	}
	
	public String obtenerContenido(
			List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipaciones) {
		StringBuilder contenido = new StringBuilder("");
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		for(ParticipacionReaseguradorContratoFacultativoDTO participacion : listaParticipaciones){
			contenido.append("<tr>");
			if(participacion.getNumeroPoliza() != null){
				contenido.append("<td>").append(participacion.getNumeroPoliza()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getNumeroEndoso() != null){
				contenido.append("<td>").append(participacion.getNumeroEndoso()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getNumeroInciso() != null){
				contenido.append("<td>").append(participacion.getNumeroInciso()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getNumeroSubInciso() != null){
				contenido.append("<td>").append(participacion.getNumeroSubInciso()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getDescripcionSeccion() != null){
				contenido.append("<td>").append(participacion.getDescripcionSeccion()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getDescripcionCobertura() != null){
				contenido.append("<td>").append(participacion.getDescripcionCobertura()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getSubRamoDTO() != null && participacion.getSubRamoDTO().getDescripcionSubRamo() != null){
				contenido.append("<td>").append(participacion.getSubRamoDTO().getDescripcionSubRamo()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getNombreReasegurador()!= null){
				contenido.append("<td>").append(participacion.getNombreReasegurador()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getNombreCorredor()!= null){
				contenido.append("<td>").append(participacion.getNombreCorredor()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getNumeroExhibicion() != null){
				contenido.append("<td>").append(participacion.getNumeroExhibicion()).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getFechaPago() != null){
				contenido.append("<td>").append(formato.format(participacion.getFechaPago())).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			if(participacion.getPrimaNetaReasegurador() != null){
				contenido.append("<td>").append(Sistema.FORMATO_MONEDA.format(participacion.getPrimaNetaReasegurador())).append("</td>");
			}else{
				contenido.append("<td></td>");
			}
			contenido.append("</tr>");
		}//Fin iteracion pagos pendientes
		return contenido.toString();
	}
	

	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerAutorizacionesPendientesExhibicionesVencidas(){
		return new EgresoReaseguroSN().obtenerAutorizacionesPagosPendientes(PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA);
	}
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerAutorizacionesPendientesExhibicionSinPagoAsegurado(){
		return new EgresoReaseguroSN().obtenerAutorizacionesPagosPendientes(PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO);
	}
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerAutorizacionesPagosPendientes(short tipoAutorizacion){
		return new EgresoReaseguroSN().obtenerAutorizacionesPagosPendientes(tipoAutorizacion);
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerAutorizacionesPendientesPagoFacultativo(){
		return new EgresoReaseguroSN().obtenerAutorizacionesPendientesPagoFacultativo();
	}
	
	public MidasJsonBase obtenerJsonAutorizacionesPagoFacultativoPorEstadoCuenta(){
		List<EstadoCuentaDecoradoDTO> listaAutorizacionesPendientes = obtenerAutorizacionesPendientesPagoFacultativo();
		MidasJsonBase jsonAutorizacionesPendientes= new MidasJsonBase();
		
		if(listaAutorizacionesPendientes != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			for(EstadoCuentaDecoradoDTO estadoCuentaDTO : listaAutorizacionesPendientes){
				MidasJsonRow reccord = new MidasJsonRow();
				reccord.setId(estadoCuentaDTO.getIdEstadoCuenta().toString());
				String folioPoliza = estadoCuentaDTO.getPolizaDTO().getFolioPoliza();
				String endoso = estadoCuentaDTO.getNumeroEndoso().toString();
				String vigencia = sdf.format(estadoCuentaDTO.getEndosoDTO().getFechaInicioVigencia())+" - "+
					sdf.format(estadoCuentaDTO.getEndosoDTO().getFechaFinVigencia());
				String reasegurador = estadoCuentaDTO.getReaseguradorCorredorDTO().getNombre();
				String corredor = (estadoCuentaDTO.getCorredorDTO() != null ? estadoCuentaDTO.getCorredorDTO().getNombre() : "");
				String subRamo = estadoCuentaDTO.getSubRamoDTO().getDescripcionSubRamo();
				String moneda = UtileriasWeb.obtenerDescripcionMoneda(estadoCuentaDTO.getIdMoneda());
				String estatus = "ND";
				if(estadoCuentaDTO.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta() != null){
					estatus = UtileriasWeb.obtenerDescripcionEstatus(
							estadoCuentaDTO.getEstatusAutorizacionPagoFacultativoPorEstadoCuenta().shortValue());
				}
				
				reccord.setDatos("0",folioPoliza,endoso,vigencia,reasegurador,corredor,subRamo,moneda,estatus);
				jsonAutorizacionesPendientes.addRow(reccord);
			}
		}
		return jsonAutorizacionesPendientes;
	}
	
	public MidasJsonBase obtenerJsonAutorizacionesExhibiciones(short tipoAutorizacion){
		List<ParticipacionReaseguradorContratoFacultativoDTO> listaAutorizacionesPendientes = obtenerAutorizacionesPagosPendientes(tipoAutorizacion);
		MidasJsonBase jsonAutorizacionesPendientes= new MidasJsonBase();
		
		if(listaAutorizacionesPendientes != null){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			for(ParticipacionReaseguradorContratoFacultativoDTO participacion : listaAutorizacionesPendientes){
				MidasJsonRow reccord = new MidasJsonRow();
				reccord.setId(participacion.getIdPagoCoberturaReasegurador());
				
				String estatus = "ND";
				if(tipoAutorizacion == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_SIN_PAGO_ASEGURADO){
					if(participacion.getEstatusAutorizacionFaltaPagoAsegurado() != null){
						estatus = UtileriasWeb.obtenerDescripcionEstatus(participacion.getEstatusAutorizacionFaltaPagoAsegurado().shortValue());
					}
				}
				else if(tipoAutorizacion == PagoCoberturaReaseguradorDTO.AUTORIZACION_PENDIENTE_EXHIBICION_VENCIDA){
					if(participacion.getEstatusAutorizacionExhibicionVencida() != null){
						estatus = UtileriasWeb.obtenerDescripcionEstatus(participacion.getEstatusAutorizacionExhibicionVencida().shortValue());
					}
				}
				
				reccord.setDatos("0",participacion.getNumeroPoliza(),""+participacion.getNumeroEndoso(),
						""+participacion.getNumeroInciso(),""+participacion.getNumeroSubInciso(),participacion.getDescripcionSeccion(),
						participacion.getDescripcionCobertura(),participacion.getNombreReasegurador(),
						(participacion.getNombreCorredor() != null ? participacion.getNombreCorredor() : ""),
						""+participacion.getNumeroExhibicion(),sdf.format(participacion.getFechaPago()),
						Sistema.FORMATO_MONEDA.format(participacion.getPrimaNetaReasegurador()),estatus);
				jsonAutorizacionesPendientes.addRow(reccord);
			}
		}
		return jsonAutorizacionesPendientes;
	}
	
	public boolean autorizarPagoCoberturaReasegurador(Long idToPlanPagosCobertura,Short numeroExhibicion,BigDecimal idReasegurador,
			boolean autorizacion,String claveUsuario,short tipoAutorizacionPendiente){
		return new EgresoReaseguroSN().autorizarPagoCoberturaReasegurador(idToPlanPagosCobertura, numeroExhibicion, idReasegurador, autorizacion, claveUsuario, tipoAutorizacionPendiente);
	}
	
	public boolean autorizarPagoFacultativoPorEstadoCuenta(BigDecimal idToEstadoCuenta,boolean autorizacion,String claveUsuario){
		return new EgresoReaseguroSN().autorizarPagoFacultativoPorEstadoCuenta(idToEstadoCuenta, autorizacion, claveUsuario);
	}
	
	public List<String> obtenerEmailsOrdenPago() throws SystemException{
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(new BigDecimal("5"));
		id.setCodigoParametroGeneral(new BigDecimal("50020"));
		ParametroGeneralDTO parametroGeneral = ParametroGeneralDN.getINSTANCIA().getPorId(id);
		String emailsStr = parametroGeneral.getValor().trim();
		String[] emailsArr = emailsStr.split(",");
		List<String> emails = new ArrayList<String>();
		for(String email : emailsArr){
			emails.add(email);
		}
		return emails;
	}
}