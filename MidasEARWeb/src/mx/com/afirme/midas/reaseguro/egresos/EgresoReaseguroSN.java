package mx.com.afirme.midas.reaseguro.egresos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.pagocobertura.PagoCoberturaReaseguradorDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoEstadoCuentaDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoReaseguroDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoReaseguroFacadeRemote;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDecoradoDTO;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDecoradoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class EgresoReaseguroSN {
	private EgresoReaseguroFacadeRemote beanRemoto;
	
	public EgresoReaseguroSN(){
		LogDeMidasWeb.log("Entrando en EgresoReaseguroSN - Constructor", Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		try {
			beanRemoto = serviceLocator.getEJB(EgresoReaseguroFacadeRemote.class);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public EgresoReaseguroDTO getPorId(BigDecimal id){
		return beanRemoto.findById(id);
	}
	
	public List<EgresoReaseguroDTO> obtenerEgresosPendientes(){
		
		return beanRemoto.obtenerEgresosPendientes();
	}
	
	public List<EgresoReaseguroDTO> obtenerEgresosPendientesCancel(){		
		return beanRemoto.obtenerEgresosPendientesCancel();
	}
	
	public EgresoReaseguroDTO agregarEgreso(EgresoReaseguroDTO egresoReaseguro, 
											List<EgresoEstadoCuentaDTO> egresoEstadosCuenta,
											List<PagoCoberturaReaseguradorDTO> exhibiciones){
		return beanRemoto.agregarEgreso(egresoReaseguro, egresoEstadosCuenta, exhibiciones);
	}
	
	public void confirmarPago(EgresoReaseguroDTO egreso)throws RuntimeException{
		beanRemoto.confirmarPago(egreso);
	}
	
	public List<EgresoEstadoCuentaDTO> obtenerEstadosCuentaEgreso(BigDecimal idToEgresoReaseguro){
		return beanRemoto.obtenerEstadosCuentaEgreso(idToEgresoReaseguro);
	}
	
	public List<ParticipacionDecoradoDTO> obtenerParticipaciones(EstadoCuentaDecoradoDTO estadoCuenta){
		return beanRemoto.obtenerParticipaciones(estadoCuenta);
	}
	
	public List<ParticipacionCorredorDecoradoDTO> obtenerParticipaciones(ParticipacionDecoradoDTO participacion, BigDecimal idContratoFacultativo, BigDecimal idEgresoReaseguro){
		return beanRemoto.obtenerParticipaciones(participacion, idContratoFacultativo, idEgresoReaseguro);
	}
	
	public List<EgresoEstadoCuentaDTO> obtenerEgresosPorEstadoCuenta(EstadoCuentaDTO estadoCuenta, BigDecimal idEgresoReaseguro){
		return beanRemoto.obtenerEgresosPorEstadoCuenta(estadoCuenta, idEgresoReaseguro);	
		
	}
	/*
	public List<EgresoEstadoCuentaDTO> borrarEgresoDTO(EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta){
		return beanRemoto.borrarEgresoDTO(egresoReaseguroId, idEstadoCuenta);		
	}
	
	public List<EgresoEstadoCuentaDTO> borrarEgresoCheque(EgresoReaseguroDTO egresoReaseguroId, BigDecimal idEstadoCuenta){
		return beanRemoto.borrarEgresoCheque(egresoReaseguroId, idEstadoCuenta);		
	}
	*/
	public List<EgresoEstadoCuentaDTO> eliminarEgresoDTOEstadoCuenta(EgresoReaseguroDTO egresoReaseguroId){
		return beanRemoto.eliminarEgresoDTOEstadoCuenta(egresoReaseguroId);
	}
	
	public void actualiza(BigDecimal egresoReaseguro, Integer estatus, BigDecimal noCheque){	
		beanRemoto.actualizaEgresoReaseguro(egresoReaseguro, estatus, noCheque);		
	}
	
	public Object ultimoEgresoDTO(){
		return beanRemoto.ultimoEgresoDTO();
	}
	
	public List<EgresoEstadoCuentaDTO> buscar(){
		return beanRemoto.obtenerEstadosCuentaEgresoEstatus();
	}
	
	public List<EgresoEstadoCuentaDTO> buscarCancel(){
		return beanRemoto.obtenerEstadosCuentaEgresoCancel();
	}
	
	public void actualizaBorrar(BigDecimal egresoReaseguro, Integer estatus, BigDecimal noCheque){	
		beanRemoto.actualizaEgresoReaseguroBorrar(egresoReaseguro, estatus, noCheque);		
	}
	
	public EgresoReaseguroDTO obtenerEgresosEstadoCuentaCheque(BigDecimal idEstadoCuenta, BigDecimal idEgresoReaseguro){
		return beanRemoto.obtenerEgresosEstadoCuentaCheque(idEstadoCuenta, idEgresoReaseguro);
	}
	
	public List<EgresoEstadoCuentaDTO> buscarPorPropiedad(String propiedad, Object valor) {
		return beanRemoto.findByProperty(propiedad, valor);		
	}
	
	public ParticipacionReaseguradorContratoFacultativoDTO obtenerParticipacionesEgresoContratoFacultativo(EstadoCuentaDecoradoDTO estadoCuenta){
		return beanRemoto.obtenerParticipacionesEgresoContratoFacultativo(estadoCuenta);
	}
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerConfiguracionPagosEgresoContratoFacultativo(BigDecimal idToEgresoReaseguro){
		return beanRemoto.obtenerConfiguracionPagosEgresoContratoFacultativo(idToEgresoReaseguro);
	}
	
	public void eliminarEgreso(BigDecimal idEgresoReaseguro) {
		beanRemoto.eliminarEgreso(idEgresoReaseguro);
	}
	
	public List<EgresoReaseguroDTO> obtenerEgresosCancelacionPendiente() {
		return beanRemoto.obtenerEgresosCancelacionPendiente();
	}
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerPagosPendientes(Date fechaCorte){
		return beanRemoto.obtenerPagosPendientes(fechaCorte);
	}
	
	public List<EstadoCuentaDecoradoDTO> obtenerAutorizacionesPendientesPagoFacultativo(){
		return beanRemoto.obtenerAutorizacionesPendientesPagoFacultativo();
	}
	
	public List<ParticipacionReaseguradorContratoFacultativoDTO> obtenerAutorizacionesPagosPendientes(short tipoAutorizacionPendiente){
		return beanRemoto.obtenerAutorizacionesPagosPendientes(tipoAutorizacionPendiente);
	}
	
	public boolean autorizarPagoFacultativoPorEstadoCuenta(BigDecimal idToEstadoCuenta,boolean autorizacion,String claveUsuario){
		return beanRemoto.autorizarPagoFacultativoPorEstadoCuenta(idToEstadoCuenta, autorizacion, claveUsuario);
	}
	
	public boolean autorizarPagoCoberturaReasegurador(Long idToPlanPagosCobertura,Short numeroExhibicion,BigDecimal idReasegurador,
			boolean autorizacion,String claveUsuario,short tipoAutorizacionPendiente){
		return beanRemoto.autorizarPagoCoberturaReasegurador(idToPlanPagosCobertura, numeroExhibicion, idReasegurador, autorizacion, claveUsuario, tipoAutorizacionPendiente);
	}
}
