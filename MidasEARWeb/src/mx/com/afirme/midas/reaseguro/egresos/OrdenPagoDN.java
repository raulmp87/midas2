package mx.com.afirme.midas.reaseguro.egresos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.contratofacultativo.participacionFacultativa.ParticipacionReaseguradorContratoFacultativoDTO;
import mx.com.afirme.midas.contratos.egreso.EgresoEstadoCuentaDTO;
import mx.com.afirme.midas.contratos.egreso.OrdenPagoDTO;
import mx.com.afirme.midas.contratos.egreso.PolizaRelacionadaDTO;
import mx.com.afirme.midas.contratos.estadocuenta.EstadoCuentaDecoradoDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.participacion.ParticipacionDecoradoDTO;
import mx.com.afirme.midas.reaseguro.estadoscuenta.EstadoCuentaDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class OrdenPagoDN {
	private static final OrdenPagoDN INSTANCIA = new OrdenPagoDN();

	private final EgresoReaseguroDN egresoReaseguroDN = EgresoReaseguroDN.getINSTANCIA();

	public static OrdenPagoDN getINSTANCIA() {
		return INSTANCIA;
	}

	public List<OrdenPagoDTO> obtenerOrdenesPago(BigDecimal idToEgresoReaseguro) throws SystemException, ExcepcionDeLogicaNegocio{
		final List<EgresoEstadoCuentaDTO> listaEgresoEstadosCuenta = egresoReaseguroDN.obtenerEstadosCuentaEgreso(idToEgresoReaseguro);
		final List<OrdenPagoDTO> listaOrdenesPago = new ArrayList<OrdenPagoDTO>();
		final List<ParticipacionDecoradoDTO> listaParticipacion = new ArrayList<ParticipacionDecoradoDTO>();
		final List<ParticipacionReaseguradorContratoFacultativoDTO> listaParticipacionFacultativo = new ArrayList<ParticipacionReaseguradorContratoFacultativoDTO>();
		OrdenPagoDTO ordenPagoDTO = null;
		for (EgresoEstadoCuentaDTO egresoEstadoCuentaDTO : listaEgresoEstadosCuenta) {

			//Valida que no sea reaseguro de retenci�n
			if (egresoEstadoCuentaDTO.getEstadoCuentaDTO().getTipoReaseguro() == TipoReaseguroDTO.TIPO_RETENCION) {
				throw new ExcepcionDeLogicaNegocio(this.getClass().toString(), UtileriasWeb.getMensajeRecurso(
						Sistema.ARCHIVO_RECURSOS, "midas.error.reporte.sindatos"));
			}
			
			final EstadoCuentaDecoradoDTO estadoCuentaDecoradoDTO = EstadoCuentaDN.getINSTANCIA().obtenerEstadoCuenta(
					egresoEstadoCuentaDTO.getId().getIdEstadoCuenta());
			
			if(estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
				final ParticipacionReaseguradorContratoFacultativoDTO participacion = 
					egresoReaseguroDN.obtenerParticipacionesEgresoContratoFacultativo(estadoCuentaDecoradoDTO);
				if(participacion != null){
					listaParticipacionFacultativo.add(participacion);
				}
			} else {
				final List<ParticipacionDecoradoDTO> listaParticipacionTMP = egresoReaseguroDN.obtenerParticipaciones(estadoCuentaDecoradoDTO);
				if (listaParticipacionTMP != null && !listaParticipacionTMP.isEmpty()) {
					listaParticipacion.addAll(listaParticipacionTMP);
				}
			}

			if(ordenPagoDTO == null){
				ordenPagoDTO = new OrdenPagoDTO();
				if (estadoCuentaDecoradoDTO.getTipoReaseguro() == TipoReaseguroDTO.TIPO_CONTRATO_FACULTATIVO){
					final PolizaRelacionadaDTO polizaRelacionada = egresoReaseguroDN.obtenerPolizaRelacionada(estadoCuentaDecoradoDTO);
					ordenPagoDTO.setPolizaRelacionadaDTO(polizaRelacionada);
				}
				CuentaBancoDTO cuentaBanco = EstadoCuentaDN.getINSTANCIA().obtenerCuentaBanco(estadoCuentaDecoradoDTO,egresoEstadoCuentaDTO.getEgresoReaseguro().getIdMoneda());
				ordenPagoDTO.setEstadoCuenta(estadoCuentaDecoradoDTO);
				ordenPagoDTO.setEgresoEstadoCuentaDTO(egresoEstadoCuentaDTO);
				ordenPagoDTO.setListaParticipacion(listaParticipacion);

				ordenPagoDTO.setCuentaBanco(cuentaBanco);
				ordenPagoDTO.setEgresoEstadoCuenta(egresoEstadoCuentaDTO);
				listaOrdenesPago.add(ordenPagoDTO);
				ordenPagoDTO.setListaParticipacionFacultativo(listaParticipacionFacultativo);
				
				ordenPagoDTO.setFechaEgreso(egresoEstadoCuentaDTO.getEgresoReaseguro().getFechaEgreso());
				ordenPagoDTO.setEstatus(egresoEstadoCuentaDTO.getEgresoReaseguro().getEstatus());
			}
			
		}
		return listaOrdenesPago;
	}
}