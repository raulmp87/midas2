package mx.com.afirme.midas.reaseguro.egresos;

import java.util.List;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.contratos.linea.EjercicioDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class AdministrarEgresosForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idEgreso;
	private String idTcRamo;
	private String idEjercicio;
	private String idTcMoneda;
	private String idTcMonedaEdosCta;
	private String tipoReaseguro;
	private String idRetencion;
	private String idtcReaseguradorCorredor;
	private String idSuscripcion;
	private String idTcSubRamo;
	private String monto;
	private String montoEgresoValor;
	private String referencia;
	private String fecha;
	private String descripcionMoneda;
	private String tipoCambioNecesario;
	private String tipoCambio;
	private String cuentaAfirme;
	private String mostrarGridEdosCuentaAutomaticos;
	private String mostrarGridEdosCuentaFacultativos;
	private String idToSeccion;
	private String idToCobertura;
	private String csvAutorizacionesPagosPendientes;
	private String tipoConsultaAutorizaciones;
		
	// Listas
	private List<MonedaDTO> monedaDTOList;
	private List<TipoReaseguroDTO> tipoReaseguroDTOList;
	private List<EjercicioDTO> ejercicioDTOList;	

	
	// Getters & Setters
	
	public String getIdTcRamo() {
		return idTcRamo;
	}

	public String getIdEgreso() {
		return idEgreso;
	}

	public void setIdEgreso(String idEgreso) {
		this.idEgreso = idEgreso;
	}

	public void setIdTcRamo(String idTcRamo) {
		this.idTcRamo = idTcRamo;
	}

	public String getIdEjercicio() {
		return idEjercicio;
	}

	public void setIdEjercicio(String idEjercicio) {
		this.idEjercicio = idEjercicio;
	}

	public String getIdTcMoneda() {
		return idTcMoneda;
	}

	public void setIdTcMoneda(String idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}
	
	public String getIdTcMonedaEdosCta() {
		return idTcMonedaEdosCta;
	}

	public void setIdTcMonedaEdosCta(String idTcMonedaEdosCta) {
		this.idTcMonedaEdosCta = idTcMonedaEdosCta;
	}

	public List<MonedaDTO> getMonedaDTOList() {
		return monedaDTOList;
	}

	public void setMonedaDTOList(List<MonedaDTO> monedaDTOList) {
		this.monedaDTOList = monedaDTOList;
	}

	public List<TipoReaseguroDTO> getTipoReaseguroDTOList() {
		return tipoReaseguroDTOList;
	}

	public void setTipoReaseguroDTOList(List<TipoReaseguroDTO> tipoReaseguroDTOList) {
		this.tipoReaseguroDTOList = tipoReaseguroDTOList;
	}

	public String getTipoReaseguro() {
		return tipoReaseguro;
	}

	public void setTipoReaseguro(String tipoReaseguro) {
		this.tipoReaseguro = tipoReaseguro;
	}

	public String getIdRetencion() {
		return idRetencion;
	}

	public void setIdRetencion(String idRetencion) {
		this.idRetencion = idRetencion;
	}

	public String getIdtcReaseguradorCorredor() {
		return idtcReaseguradorCorredor;
	}

	public void setIdtcReaseguradorCorredor(String idtcReaseguradorCorredor) {
		this.idtcReaseguradorCorredor = idtcReaseguradorCorredor;
	}

	public String getIdSuscripcion() {
		return idSuscripcion;
	}

	public void setIdSuscripcion(String idSuscripcion) {
		this.idSuscripcion = idSuscripcion;
	}
	
	public List<EjercicioDTO> getEjercicioDTOList() {
		return ejercicioDTOList;
	}

	public void setEjercicioDTOList(List<EjercicioDTO> ejercicioDTOList) {
		this.ejercicioDTOList = ejercicioDTOList;
	}

	public String getIdTcSubRamo() {
		return idTcSubRamo;
	}

	public void setIdTcSubRamo(String idTcSubRamo) {
		this.idTcSubRamo = idTcSubRamo;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getMontoEgresoValor() {
		return montoEgresoValor;
	}

	public void setMontoEgresoValor(String montoEgresoValor) {
		this.montoEgresoValor = montoEgresoValor;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDescripcionMoneda() {
		return descripcionMoneda;
	}

	public void setDescripcionMoneda(String descripcionMoneda) {
		this.descripcionMoneda = descripcionMoneda;
	}

	public String getTipoCambioNecesario() {
		return tipoCambioNecesario;
	}

	public void setTipoCambioNecesario(String tipoCambioNecesario) {
		this.tipoCambioNecesario = tipoCambioNecesario;
	}

	public String getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
	public String getCuentaAfirme() {
		return cuentaAfirme;
	}

	public void setCuentaAfirme(String cuentaAfirme) {
		this.cuentaAfirme = cuentaAfirme;
	}

	public String getMostrarGridEdosCuentaAutomaticos() {
		return mostrarGridEdosCuentaAutomaticos;
	}

	public void setMostrarGridEdosCuentaAutomaticos(
			String mostrarGridEdosCuentaAutomaticos) {
		this.mostrarGridEdosCuentaAutomaticos = mostrarGridEdosCuentaAutomaticos;
	}

	public String getMostrarGridEdosCuentaFacultativos() {
		return mostrarGridEdosCuentaFacultativos;
	}

	public void setMostrarGridEdosCuentaFacultativos(
			String mostrarGridEdosCuentaFacultativos) {
		this.mostrarGridEdosCuentaFacultativos = mostrarGridEdosCuentaFacultativos;
	}

	public String getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(String idToCobertura) {
		this.idToCobertura = idToCobertura;
	}
	
	public String getCsvAutorizacionesPagosPendientes() {
		return csvAutorizacionesPagosPendientes;
	}
	public void setCsvAutorizacionesPagosPendientes(String csvAutorizacionesPagosPendientes) {
		this.csvAutorizacionesPagosPendientes = csvAutorizacionesPagosPendientes;
	}
	public String getTipoConsultaAutorizaciones() {
		return tipoConsultaAutorizaciones;
	}
	public void setTipoConsultaAutorizaciones(String tipoConsultaAutorizaciones) {
		this.tipoConsultaAutorizaciones = tipoConsultaAutorizaciones;
	}
}