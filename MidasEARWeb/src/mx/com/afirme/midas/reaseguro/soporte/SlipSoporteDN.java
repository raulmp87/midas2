package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAnexoEquipoContratistaDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipAviacionDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipBarcoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipConstructoresDTOId;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioAnexoDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipFuncionarioDTOId;
import mx.com.afirme.midas.contratofacultativo.slip.SlipIncendioDTO;
import mx.com.afirme.midas.contratofacultativo.slip.SlipObraCivilDTO;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.SlipRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.constructor.anexo.SlipAnexoRCConstructoresDN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.SlipRCFuncionariosDN;
import mx.com.afirme.midas.contratofacultativo.slip.funcionario.anexo.SlipAnexoRCFuncionariosDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.reaseguro.soporte.slip.DetalleSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAnexoSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAviacionSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipBarcosSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipEquipoContratistaSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipGeneralSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipIncendioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipObraCivilSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipPrimerRiesgoSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCConstructoresSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipRCFuncionarioSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipSoporteDTO;
import mx.com.afirme.midas.reaseguro.soporte.slip.SlipTransportesSoporteDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SlipSoporteDN {
	
	
	
  	private static final SlipSoporteDN INSTANCIA = new SlipSoporteDN(); 
	   
	  public static SlipSoporteDN getInstancia(){
	 	  return SlipSoporteDN.INSTANCIA;
	  }

	  /**
		 * Obtiene un SlipSoporteDTO en base al identificador del Slip para obtener la informacion general
		 * @param idToSlip
		 * @param slipGeneral
	 	 * @return SlipSoporteDTO
	  	 * @throws SystemException
		 * @throws ExcepcionDeAccesoADatos
		 */
	  
	 
	public SlipSoporteDTO obtieneSlipGeneral(BigDecimal idToSlip) throws ExcepcionDeAccesoADatos, SystemException{
		SlipSoporteDTO slipSoporte = null;
		SlipDTO slip = SlipDN.getInstancia().getPorId(idToSlip);
		slipSoporte = new SlipSoporteDTO();
		slipSoporte.setTipoSlip(Integer.valueOf(slip.getTipoSlip()));
		List<SlipAnexoDTO> slipAnexoList =  SlipAnexoDN.getInstancia().listarAnexosSlip(idToSlip); 
		List<SlipAnexoSoporteDTO> slipAnexoSoporteList = new ArrayList<SlipAnexoSoporteDTO>();
		for(SlipAnexoDTO slipAnexoDTO :  slipAnexoList){
			SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
			slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoDTO.getIdToSlipDocumentoAnexo());
			slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoDTO.getIdToControlArchivo());
			ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoDTO.getIdToControlArchivo()); 
			slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
			slipAnexoSoporteList.add(slipAnexoSoporteDTO);
			slipSoporte.setAnexos(slipAnexoSoporteList);			
		}
		return slipSoporte;	 
    }
	  
	
	 /**
	 * Obtiene un SlipSoporteDTO en base al identificador del Slip requerido
	 * @param idToSlip
	 * @param slipGeneral
	 * @return SlipSoporteDTO
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public SlipSoporteDTO obtenerSlipSoporte(BigDecimal idToSlip) throws SystemException, ExcepcionDeAccesoADatos {
		SlipSoporteDTO slipSoporte = null;
		SlipDTO slip = SlipDN.getInstancia().getPorId(idToSlip);
		slipSoporte = new SlipSoporteDTO();
		slipSoporte.setIdSlip(idToSlip);
		slipSoporte.setTipoSlip(Integer.valueOf(slip.getTipoSlip()));
		if(slipSoporte.getCotizacionDTO() == null || slipSoporte.getCotizacionDTO().getIdToCotizacion() == null)
			slipSoporte.setCotizacionDTO(CotizacionDN.getInstancia("ADMIN").getPorId(slip.getIdToCotizacion()));
		
		/*
		 * Carga lista de anexos en caso que existan
		 */
		List<SlipAnexoDTO> listaAnexos = SlipAnexoDN.getInstancia().listarAnexosSlip(idToSlip);
		List<SlipAnexoSoporteDTO> slipAnexosSlipList = new ArrayList<SlipAnexoSoporteDTO>();
		if(listaAnexos != null){
			for(SlipAnexoDTO slipAnexoDTO :  listaAnexos){
				SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
				slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoDTO.getIdToSlipDocumentoAnexo());
				slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoDTO.getIdToControlArchivo());
				ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoDTO.getIdToControlArchivo());
				slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
				slipAnexosSlipList.add(slipAnexoSoporteDTO);
			} 
			slipSoporte.setAnexos(slipAnexosSlipList);				
		}

		if(slipSoporte.getTipoSlip() == SlipGeneralSoporteDTO.TIPO){
			SlipGeneralSoporteDTO  slipGeneralSoporteDTO = new SlipGeneralSoporteDTO();
			
			slipGeneralSoporteDTO.setNumeroInciso(slip.getNumeroInciso());
			slipGeneralSoporteDTO.setSubInciso(slip.getNumeroSubInciso());
			slipGeneralSoporteDTO.setIdToSeccion(slip.getIdToSeccion());
			
			slipSoporte.setSlipGeneralSoporte(slipGeneralSoporteDTO);
		}
		else if (slipSoporte.getTipoSlip() == SlipAviacionSoporteDTO.TIPO){
			SlipAviacionDTO slipAviacionDTO =  SlipDN.getInstancia().obtenerSlipAviacion(idToSlip);
			SlipAviacionSoporteDTO slipAviacionSoporteDTO = new SlipAviacionSoporteDTO();
			BigDecimal pagosPasageros = new BigDecimal(0);
			if (slipAviacionDTO != null){
				slipAviacionSoporteDTO.setHorasMarcaTipo(slipAviacionDTO.getHorasMarcaTipo());
				if(slipAviacionDTO.getPagosPasajeros() != null){
					pagosPasageros = new BigDecimal(slipAviacionDTO.getPagosPasajeros());
				} 
				slipAviacionSoporteDTO.setPagosVoluntariosPasajeros(pagosPasageros);
				slipAviacionSoporteDTO.setSubLimites(slipAviacionDTO.getSubLimites());
				
			}
			slipSoporte.setSlipAviacion(slipAviacionSoporteDTO);
		}
		else if (slipSoporte.getTipoSlip() == SlipBarcosSoporteDTO.TIPO){
			SlipBarcoDTO slipBarcoDTO =  SlipDN.getInstancia().obtenerSlipBarco(idToSlip);
			SlipBarcosSoporteDTO  slipBarcosSoporteDTO = new SlipBarcosSoporteDTO();
			if (slipBarcoDTO != null){
				slipBarcosSoporteDTO.setLugarConstruccion(slipBarcoDTO.getLugarConstruccion());
				slipBarcosSoporteDTO.setMatricula(slipBarcoDTO.getMatricula());
				slipBarcosSoporteDTO.setSerie(slipBarcoDTO.getSerie());
				slipBarcosSoporteDTO.setUltimaFechaMttoInspeccion(slipBarcoDTO.getUltimaFechaMtto());
			}
			slipSoporte.setSlipBarcos(slipBarcosSoporteDTO);
		}
		else if (slipSoporte.getTipoSlip() == SlipEquipoContratistaSoporteDTO.TIPO){
			SlipEquipoContratistaSoporteDTO slipEquipoContratistaSoporteDTO = new SlipEquipoContratistaSoporteDTO();
			slipSoporte.setSlipEquipoConstratista(slipEquipoContratistaSoporteDTO);

			List<SlipAnexoSoporteDTO> listaAnexosEquipoConstratistaList = new ArrayList<SlipAnexoSoporteDTO>();
			List<SlipAnexoEquipoContratistaDTO> slipAnexoEquipoContratistaList =  SlipAnexoDN.getInstancia().listarAnexosSlipEquipoContratista(idToSlip); 
			if(slipAnexoEquipoContratistaList != null){
				for(SlipAnexoEquipoContratistaDTO slipAnexoEquipoContratistaDTO :  slipAnexoEquipoContratistaList){
					SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
					slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoEquipoContratistaDTO.getIdToSlipDocumentoAnexo());
					slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoEquipoContratistaDTO.getIdToControlArchivo());
					ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoEquipoContratistaDTO.getIdToControlArchivo()); 
					slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
					listaAnexosEquipoConstratistaList.add(slipAnexoSoporteDTO);
				} 
			}
			slipSoporte.getSlipEquipoConstratista().setAnexos(listaAnexosEquipoConstratistaList);
		}
		else if (slipSoporte.getTipoSlip() == SlipIncendioSoporteDTO.TIPO){
			SlipIncendioDTO slipIncendioDTO = SlipDN.getInstancia().obtenerSlipIncendio(idToSlip);
			SlipIncendioSoporteDTO slipIncendioSoporteDTO = new SlipIncendioSoporteDTO();
			
			slipIncendioSoporteDTO.setNumeroInciso(slip.getNumeroInciso());
			
			if(slipIncendioDTO != null){
				slipIncendioSoporteDTO.setProteccionesContraIncendio(slipIncendioDTO.getProteccionesIndencios());
			}
			slipSoporte.setSlipIncendioSoporte(slipIncendioSoporteDTO);
		}
		else if (slipSoporte.getTipoSlip() == SlipObraCivilSoporteDTO.TIPO){
			SlipObraCivilDTO slipObraCivilDTO = SlipDN.getInstancia().obtenerSlipObraCivil(idToSlip);
			SlipObraCivilSoporteDTO slipObraCivilSoporteDTO = new SlipObraCivilSoporteDTO();
			BigDecimal sumaSegurada = new BigDecimal(0);
			if(slipObraCivilDTO != null){
				slipObraCivilSoporteDTO.setAguasCercanas(slipObraCivilDTO.getAguasCercanas());
				slipObraCivilSoporteDTO.setBeneficiarioPreferente(slipObraCivilDTO.getBeneficiario());
				slipObraCivilSoporteDTO.setClausulaTerceros(slipObraCivilDTO.getClausulaTerceros());
				slipObraCivilSoporteDTO.setCondicionesMetereologicas(slipObraCivilDTO.getCondicionesMetereologicas());
				slipObraCivilSoporteDTO.setCumpleRegulacionesTerremotos(slipObraCivilDTO.getCumpleRegulacionesTerremotos());
				slipObraCivilSoporteDTO.setDireccionContratista( slipObraCivilDTO.getDireccionContratista());
				slipObraCivilSoporteDTO.setEdificiosTerceros( slipObraCivilDTO.getEdificiosTerceros());
				slipObraCivilSoporteDTO.setExistenEstructurasAdyacentes(slipObraCivilDTO.getExistenEstructurasAdyacentes());
				slipObraCivilSoporteDTO.setExperienciaContratista( slipObraCivilDTO.getExperienciaContratista());
				slipObraCivilSoporteDTO.setFechaTerminacionPeriodoMantenimiento(UtileriasWeb.getFechaString(slipObraCivilDTO.getFechaTerminacionMtto()));
				slipObraCivilSoporteDTO.setIngenieroConsultor(slipObraCivilDTO.getIngenieroConsultor());
				slipObraCivilSoporteDTO.setMaquinaria(slipObraCivilDTO.getMaquinaria());
				slipObraCivilSoporteDTO.setMetodosMaterialesConstruccion(slipObraCivilDTO.getMetodosYMaterialesConstruccion());
				slipObraCivilSoporteDTO.setNombreContratista(slipObraCivilDTO.getNombreContratista());
				slipObraCivilSoporteDTO.setDireccionSubContratista(slipObraCivilDTO.getDireccionSubContratista());
				slipObraCivilSoporteDTO.setNombreSubContratista(slipObraCivilDTO.getNombreSubContratista());
				
				if(slipObraCivilSoporteDTO.getSumaTotalAsegurada() != null){
					sumaSegurada = slipObraCivilSoporteDTO.getSumaTotalAsegurada();
				} 
				slipObraCivilSoporteDTO.setSumaTotalAsegurada(sumaSegurada);  
				slipObraCivilSoporteDTO.setTituloContrato(slipObraCivilDTO.getTituloContrato());
				slipObraCivilSoporteDTO.setTrabajosPorContrato(slipObraCivilDTO.getTrabajosPorContrato());
				slipObraCivilSoporteDTO.setTrabajoSubContratistas(slipObraCivilDTO.getTrabajoSubContratistas());
				slipObraCivilSoporteDTO.setValorPorContrato(slipObraCivilDTO.getValorPorContrato());
			}
			slipSoporte.setSlipObraCivilSoporte(slipObraCivilSoporteDTO);
		}
		else if (slipSoporte.getTipoSlip() == SlipRCConstructoresSoporteDTO.TIPO){
			
			List<SlipConstructoresDTO> slipConstructoresDTO = SlipDN.getInstancia().obtenerListaSlipConstructores("id.idToSlip",idToSlip);
			for(int i = 0; i < slipConstructoresDTO.size()-1; i++){	
				for(int j = i+1; j < slipConstructoresDTO.size(); j++){
					if(slipConstructoresDTO.get(i).getId().getNumeroInciso().longValueExact()> slipConstructoresDTO.get(j).getId().getNumeroInciso().longValueExact()){
						SlipConstructoresDTO temp = slipConstructoresDTO.get(i);
						slipConstructoresDTO.set(i, slipConstructoresDTO.get(j));
						slipConstructoresDTO.set(j, temp);
					}
						
				}
			}
			if(slipConstructoresDTO != null){
				slipSoporte.setSlipRCConstructoresSoporte(new ArrayList<SlipRCConstructoresSoporteDTO>());
				
				for(int i = 0; i < slipConstructoresDTO.size(); i++){
					SlipRCConstructoresSoporteDTO slipRCConstructoresSoporteDTO = new SlipRCConstructoresSoporteDTO();
					if(slipConstructoresDTO.get(i) != null){
						slipRCConstructoresSoporteDTO.setColindantes(slipConstructoresDTO.get(i).getColindantes());
						slipRCConstructoresSoporteDTO.setExperienciaLaboral(slipConstructoresDTO.get(i).getExperienciaLaboral());
						slipRCConstructoresSoporteDTO.setParaQuienRealizaLaObra(slipConstructoresDTO.get(i).getParaQuienRealizaLaObra());
						slipRCConstructoresSoporteDTO.setSistemasPrevistos(slipConstructoresDTO.get(i).getSistemasPrevistos());
						slipRCConstructoresSoporteDTO.setSitioTrabajos(slipConstructoresDTO.get(i).getSitioTrabajos());
						slipRCConstructoresSoporteDTO.setTipoMaquinaria(Integer.valueOf(slipConstructoresDTO.get(i).getTipoMaquinaria()));
						slipRCConstructoresSoporteDTO.setValorEstimadoObra(slipConstructoresDTO.get(i).getValorEstimadoObra());
						slipRCConstructoresSoporteDTO.setNumeroInciso(slipConstructoresDTO.get(i).getId().getNumeroInciso());
					}

					//slipSoporte.setSlipRCConstructoresSoporte(slipRCConstructoresSoporteDTO);			
					slipSoporte.getSlipRCConstructoresSoporte().add(slipRCConstructoresSoporteDTO);


					List<SlipAnexoConstructoresDTO> listaAnexosConstructoresList = SlipAnexoRCConstructoresDN.getInstancia().buscarAnexos(slipSoporte.getIdSlip(),slipRCConstructoresSoporteDTO.getNumeroInciso());
					
					
					List<SlipAnexoSoporteDTO> slipAnexosSlipConstructoresList = new ArrayList<SlipAnexoSoporteDTO>();
					
					if(listaAnexosConstructoresList != null){
						for(SlipAnexoConstructoresDTO slipAnexoConstructoresDTO :  listaAnexosConstructoresList){
							SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
							slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
							slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
							ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoConstructoresDTO.getIdToControlArchivo()); 
							slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
							slipAnexosSlipConstructoresList.add(slipAnexoSoporteDTO);
						} 
						slipSoporte.getSlipRCConstructoresSoporte().get(i).setAnexos(slipAnexosSlipConstructoresList);						
					}
				}
			}
		}
		else if (slipSoporte.getTipoSlip() == SlipRCFuncionarioSoporteDTO.TIPO){
			List<SlipFuncionarioDTO> slipFuncionarioDTO = SlipDN.getInstancia().obtenerListaSlipFuncionario(idToSlip);
			if(slipFuncionarioDTO != null){
				for(int i = 0; i < slipFuncionarioDTO.size()-1; i++){
					for(int j = i+1; j < slipFuncionarioDTO.size(); j++){
						if(slipFuncionarioDTO.get(i).getId().getNumeroInciso().longValueExact()> slipFuncionarioDTO.get(j).getId().getNumeroInciso().longValueExact()){
							SlipFuncionarioDTO temp = slipFuncionarioDTO.get(i);
							slipFuncionarioDTO.set(i, slipFuncionarioDTO.get(j));
							slipFuncionarioDTO.set(j, temp);
						}
					}
				}
				for(int i = 0; i < slipFuncionarioDTO.size(); i++){
					SlipRCFuncionarioSoporteDTO slipRCFuncionarioSoporteDTO = new SlipRCFuncionarioSoporteDTO();
					if(slipFuncionarioDTO.get(i) != null){
						slipRCFuncionarioSoporteDTO.setAccionLegal( slipFuncionarioDTO.get(i).getAccionLegal());
						slipRCFuncionarioSoporteDTO.setComision(slipFuncionarioDTO.get(i).getComision());
						slipRCFuncionarioSoporteDTO.setCondicionesGenerales(slipFuncionarioDTO.get(i).getCondicionesGenerales());
						slipRCFuncionarioSoporteDTO.setCondicionesReaseguro(slipFuncionarioDTO.get(i).getCondicionesReaseguro());
						slipRCFuncionarioSoporteDTO.setGarantiaExpresa(slipFuncionarioDTO.get(i).getGarantiaExpresa());
						slipRCFuncionarioSoporteDTO.setFechaRetroactiva(slipFuncionarioDTO.get(i).getFechaRetroactiva());
						slipRCFuncionarioSoporteDTO.setImpuestos(slipFuncionarioDTO.get(i).getImpuestos());
						slipRCFuncionarioSoporteDTO.setInteres(slipFuncionarioDTO.get(i).getInteres());
						slipRCFuncionarioSoporteDTO.setLeyesJurisdiccion(slipFuncionarioDTO.get(i).getLeyesJurisdiccion( ));
						slipRCFuncionarioSoporteDTO.setLimiteTerritorial(slipFuncionarioDTO.get(i).getLimiteTerritorial( ));
						slipRCFuncionarioSoporteDTO.setPeriodoNotificaciones(slipFuncionarioDTO.get(i).getPeriodoNotificaciones( ));
						slipRCFuncionarioSoporteDTO.setRetencion(slipFuncionarioDTO.get(i).getRetencion( ));
						slipRCFuncionarioSoporteDTO.setSubjetividades(slipFuncionarioDTO.get(i).getSubjetividades());
						slipRCFuncionarioSoporteDTO.setTerminoPrimas(slipFuncionarioDTO.get(i).getTerminoPrimas());
						slipRCFuncionarioSoporteDTO.setNumeroInciso(slipFuncionarioDTO.get(i).getId().getNumeroInciso());
					}

					//slipSoporte.setSlipRCFuncionarioSoporte(slipRCFuncionarioSoporteDTO);
					if(slipSoporte.getSlipRCFuncionarioSoporte() == null)
						slipSoporte.setSlipRCFuncionarioSoporte(new ArrayList<SlipRCFuncionarioSoporteDTO>());
					slipSoporte.getSlipRCFuncionarioSoporte().add(slipRCFuncionarioSoporteDTO);

					List<SlipFuncionarioAnexoDTO> listaFuncionariosList =SlipAnexoRCFuncionariosDN.getInstancia().obtenerAnexosPorSlipInciso(idToSlip, slipRCFuncionarioSoporteDTO.getNumeroInciso());
					List<SlipAnexoSoporteDTO> slipAnexosSlipFuncionariosList = new ArrayList<SlipAnexoSoporteDTO>();

					for(SlipFuncionarioAnexoDTO slipFuncionarioAnexoDTO :  listaFuncionariosList){
						SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
						slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipFuncionarioAnexoDTO.getIdSlipDocumentoAnexo());
						slipAnexoSoporteDTO.setIdToControlArchivo(slipFuncionarioAnexoDTO.getIdControlArchivo());
						ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipFuncionarioAnexoDTO.getIdControlArchivo()); 
						slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
						slipAnexosSlipFuncionariosList.add(slipAnexoSoporteDTO);
					} 
					slipSoporte.getSlipRCFuncionarioSoporte().get(0).setAnexos(slipAnexosSlipFuncionariosList);
				}
			}
		}
		else if (slipSoporte.getTipoSlip() == SlipTransportesSoporteDTO.TIPO){
			SlipTransportesSoporteDTO slipTransportesSoporteDTO = new SlipTransportesSoporteDTO();
			
			slipTransportesSoporteDTO.setNumeroInciso(slip.getNumeroInciso());			
			
			
			slipSoporte.setSlipTransportesSoporte(slipTransportesSoporteDTO);
		}
		else if (slipSoporte.getTipoSlip() == SlipPrimerRiesgoSoporteDTO.TIPO){
			SlipPrimerRiesgoSoporteDTO slipPrimerRiesgoSoporteDTO = new SlipPrimerRiesgoSoporteDTO();
			slipSoporte.setSlipPrimerRiesgo(slipPrimerRiesgoSoporteDTO);
		}		
		
    	return slipSoporte;		
	}
	
	
	public void procesaRCConstructores(SlipSoporteDTO slipSoporte) throws SystemException{
		
		if(slipSoporte.getTipoSlip() == SlipRCConstructoresSoporteDTO.TIPO){
			List<SlipRCConstructoresSoporteDTO> lista = slipSoporte.getSlipRCConstructoresSoporte();
			BigDecimal idSlip = slipSoporte.getIdSlip();
			
			
			for(SlipRCConstructoresSoporteDTO rc : lista){
				BigDecimal numeroInciso = rc.getNumeroInciso();
				
				SlipConstructoresDTOId id = new SlipConstructoresDTOId();
				id.setIdToSlip(idSlip);
				id.setNumeroInciso(numeroInciso);
				
				SlipConstructoresDTO cons =  SlipRCConstructoresDN.getInstancia().getPorId(id);
				
				rc.setColindantes(cons.getColindantes());
				rc.setExperienciaLaboral(cons.getExperienciaLaboral());
				rc.setParaQuienRealizaLaObra(cons.getParaQuienRealizaLaObra());
				rc.setSistemasPrevistos(cons.getSistemasPrevistos());
				rc.setSitioTrabajos(cons.getSitioTrabajos());
				rc.setTipoMaquinaria(Integer.valueOf(cons.getTipoMaquinaria()));
				rc.setValorEstimadoObra(cons.getValorEstimadoObra());
				rc.setNumeroInciso(cons.getId().getNumeroInciso());
				
				List<SlipAnexoConstructoresDTO> listaAnexosConstructoresList 
										= SlipAnexoRCConstructoresDN.getInstancia().buscarAnexos(
														idSlip,
														numeroInciso);	
				 
				if(listaAnexosConstructoresList != null){
					List<SlipAnexoSoporteDTO> slipAnexosSlipConstructoresList = new ArrayList<SlipAnexoSoporteDTO>();
					for(SlipAnexoConstructoresDTO slipAnexoConstructoresDTO :  listaAnexosConstructoresList){
						SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
						slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
						slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
						ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoConstructoresDTO.getIdToControlArchivo()); 
						slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
						slipAnexosSlipConstructoresList.add(slipAnexoSoporteDTO);
					} 
					rc.setAnexos(slipAnexosSlipConstructoresList);						
				}			
				
				
			}			
		}
	}

	public void procesaRCFuncionarios(SlipSoporteDTO slipSoporte) throws SystemException{
		
		if(slipSoporte.getTipoSlip() == SlipRCFuncionarioSoporteDTO.TIPO){
			List<SlipRCFuncionarioSoporteDTO> lista = slipSoporte.getSlipRCFuncionarioSoporte();
			BigDecimal idSlip = slipSoporte.getIdSlip();
			
			
			for(SlipRCFuncionarioSoporteDTO rc : lista){
				BigDecimal numeroInciso = rc.getNumeroInciso();
				
				SlipFuncionarioDTOId id = new SlipFuncionarioDTOId();
				id.setIdToSlip(idSlip);
				id.setNumeroInciso(numeroInciso);
				
				//SlipConstructoresDTO cons =  SlipRCConstructoresDN.getInstancia().getPorId(id);
				
				SlipFuncionarioDTO func = SlipRCFuncionariosDN.getInstancia().getPorId(id);

				
				rc.setAccionLegal(func.getAccionLegal());
				rc.setComision(func.getComision());
				rc.setCondicionesGenerales(func.getCondicionesGenerales());
				rc.setCondicionesReaseguro(func.getCondicionesReaseguro());
				rc.setFechaRetroactiva(func.getFechaRetroactiva());
				rc.setGarantiaExpresa(func.getGarantiaExpresa());
				rc.setImpuestos(func.getImpuestos());
				rc.setInteres(func.getInteres());
				rc.setLeyesJurisdiccion(func.getLeyesJurisdiccion());
				rc.setLimiteTerritorial(func.getLimiteTerritorial());
				//rc.setNumeroInciso();
				
				rc.setPeriodoNotificaciones(func.getPeriodoNotificaciones());
				rc.setRetencion(func.getRetencion());
				rc.setSubjetividades(func.getSubjetividades());
				rc.setTerminoPrimas(func.getTerminoPrimas());

				
				List<SlipAnexoConstructoresDTO> listaAnexosConstructoresList 
										= SlipAnexoRCConstructoresDN.getInstancia().buscarAnexos(
														idSlip,
														numeroInciso);	
				 
				if(listaAnexosConstructoresList != null){
					List<SlipAnexoSoporteDTO> slipAnexosSlipConstructoresList = new ArrayList<SlipAnexoSoporteDTO>();
					for(SlipAnexoConstructoresDTO slipAnexoConstructoresDTO :  listaAnexosConstructoresList){
						SlipAnexoSoporteDTO slipAnexoSoporteDTO = new SlipAnexoSoporteDTO();
						slipAnexoSoporteDTO.setIdToSlipDocumentoAnexo(slipAnexoConstructoresDTO.getIdToSlipDocumentoAnexo());
						slipAnexoSoporteDTO.setIdToControlArchivo(slipAnexoConstructoresDTO.getIdToControlArchivo());
						ControlArchivoDTO controlArchivoDTO  = ControlArchivoDN.getInstancia().getPorId(slipAnexoConstructoresDTO.getIdToControlArchivo()); 
						slipAnexoSoporteDTO.setUrl(controlArchivoDTO.getNombreArchivoOriginal());
						slipAnexosSlipConstructoresList.add(slipAnexoSoporteDTO);
					} 
					rc.setAnexos(slipAnexosSlipConstructoresList);						
				}			
				
				
			}			
		}
	}
	
	
	public void crearListaDetalle(SlipSoporteDTO slipSoporte){
		/*
		 * Obtiene la lista de detalle
		 */
		List<DetalleSoporteDTO> listaDetalle = obtenerListaDetalle(slipSoporte);
		slipSoporte.setListaDetalle(listaDetalle);
	}
	
	/**
	 * Modifica un SlipSoporteDTO realizando las transformaciones a DTO Entities necesarias.
	 * @param slipSoporte SlipSoporteDTO
	 */
	public void modificarSlipSoporte(SlipSoporteDTO slipSoporte){
		/*
		 * TODO: Implementar
		 */
		
	}
	
	private List<DetalleSoporteDTO> obtenerListaDetalle(String identificador, String descripcion){
		DetalleSoporteDTO inciso = new DetalleSoporteDTO();

		
		if(identificador == null){
			inciso.setIdentificador("N/A");
		}else{
			inciso.setIdentificador(identificador);
		}
		
		if(descripcion == null){
			inciso.setDescripcion("N/A");
		}else{
			inciso.setDescripcion(descripcion);
		}
		
		List<DetalleSoporteDTO> listaIncisos = new ArrayList<DetalleSoporteDTO>();
		listaIncisos.add(inciso);		
	
		return listaIncisos;
	}
	
	private List<DetalleSoporteDTO> obtenerListaDetalle(List<SlipRCConstructoresSoporteDTO> listaIncisos){
		List<DetalleSoporteDTO> listaDetalle = null;
		
		if(listaIncisos != null){
			listaDetalle = new ArrayList<DetalleSoporteDTO>();
			for(SlipRCConstructoresSoporteDTO inciso : listaIncisos){
				DetalleSoporteDTO detalle = new DetalleSoporteDTO();
				
				if(inciso.getNumeroInciso() == null){
					detalle.setIdentificador("N/A");
				}else{
					detalle.setIdentificador(inciso.getNumeroInciso().toPlainString());
				}
				
				if(inciso.getDescripcionObra() == null){
					detalle.setDescripcion("N/A");
				}else{
					detalle.setDescripcion(inciso.getDescripcionObra());
				}				
				
				listaDetalle.add(detalle);
			}
			
		}
		
		return listaDetalle;
	}
	
	private List<DetalleSoporteDTO> obtenerListaDetalle(List<SlipRCFuncionarioSoporteDTO> listaIncisos,BigDecimal idToSlip){
		List<DetalleSoporteDTO> listaDetalle = null;
		
		if(listaIncisos != null){
			listaDetalle = new ArrayList<DetalleSoporteDTO>();
			for(SlipRCFuncionarioSoporteDTO inciso : listaIncisos){
				DetalleSoporteDTO detalle = new DetalleSoporteDTO();
				
				if(inciso.getNumeroInciso() == null){
					detalle.setIdentificador("N/A");
				}else{
					detalle.setIdentificador(inciso.getNumeroInciso().toPlainString());
				}
				
//				if(inciso.getDescripcionObra() == null){
					detalle.setDescripcion("N/A");
//				}else{
//					detalle.setDescripcion(inciso.getDescripcionObra());
//				}				
				
				listaDetalle.add(detalle);
			}
			
		}
		
		return listaDetalle;
	}
	
	private List<DetalleSoporteDTO> obtenerListaDetalle(SlipSoporteDTO slipSoporteDTO){
		List<DetalleSoporteDTO> listaDetalle = null;
		String desc = null;
		String id = null;
		switch(slipSoporteDTO.getTipoSlip()){
			case SlipIncendioSoporteDTO.TIPO:
				listaDetalle = obtenerListaDetalle(slipSoporteDTO.getSlipIncendioSoporte().getNumeroInciso().toPlainString(),slipSoporteDTO.getSlipIncendioSoporte().getDireccionRiesgo());
			break;
			case SlipTransportesSoporteDTO.TIPO:
				desc = slipSoporteDTO.getSlipTransportesSoporte().getBienesAsegurados();
				listaDetalle = obtenerListaDetalle(slipSoporteDTO.getSlipTransportesSoporte().getNumeroInciso().toPlainString(),desc);
				break;
			case SlipGeneralSoporteDTO.TIPO:
				if(slipSoporteDTO.getSlipGeneralSoporte().getSubInciso() != null){
					id = slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso()+ "."+ slipSoporteDTO.getSlipGeneralSoporte().getSubInciso();
					desc = slipSoporteDTO.getSlipGeneralSoporte().getDescripcionSeccion()+ "-"+ slipSoporteDTO.getSlipGeneralSoporte().getDescripcionSubInciso();				
				} else {
					id = slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso().toPlainString();		
					desc = slipSoporteDTO.getSlipGeneralSoporte().getNumeroInciso().toPlainString();
				}
				listaDetalle =obtenerListaDetalle(id,desc);
				break;
			case SlipObraCivilSoporteDTO.TIPO:
				listaDetalle = obtenerListaDetalle(slipSoporteDTO.getSlipObraCivilSoporte().getNumeroInciso().toPlainString(),
						slipSoporteDTO.getSlipObraCivilSoporte().getLocalizacionObra());
				break;
			case SlipEquipoContratistaSoporteDTO.TIPO:
				id = slipSoporteDTO.getSlipEquipoConstratista().getNumeroInciso()+ "."+ slipSoporteDTO.getSlipEquipoConstratista().getNumeroSubInciso();
				desc = slipSoporteDTO.getSlipEquipoConstratista().getDescripcionSeccion()+ "-"+ slipSoporteDTO.getSlipEquipoConstratista().getDescripcionSubInciso();
				listaDetalle = obtenerListaDetalle(id,desc);
				break;
			case SlipBarcosSoporteDTO.TIPO:
				id =  slipSoporteDTO.getSlipBarcos().getNumeroInciso().toPlainString();
				if(slipSoporteDTO.getSlipBarcos().getTipoEmbarcacion() != null && slipSoporteDTO.getSlipBarcos().getClasificacion() != null){
					desc = slipSoporteDTO.getSlipBarcos().getTipoEmbarcacion().trim()+ "-"+ slipSoporteDTO.getSlipBarcos().getClasificacion().trim()
							+ "-"
							+ slipSoporteDTO.getSlipBarcos().getAnioConstruccion();				
				} else {
					desc = "N/D";
				}
				listaDetalle = obtenerListaDetalle(id,desc);
				break;
			case SlipAviacionSoporteDTO.TIPO:
				id =  slipSoporteDTO.getSlipAviacion().getNumeroInciso().toPlainString();
				if(slipSoporteDTO.getSlipAviacion().getMarca() != null && slipSoporteDTO.getSlipAviacion().getModelo() != null
						&& slipSoporteDTO.getSlipAviacion().getMatricula() != null){
					desc = slipSoporteDTO.getSlipAviacion().getMarca().trim() + "-"+ slipSoporteDTO.getSlipAviacion().getModelo().trim()
							+ "-" + slipSoporteDTO.getSlipAviacion().getMatricula().trim() + "-"+ slipSoporteDTO.getSlipAviacion().getAnio();
				} else {
					desc = "N/D";
				}
				listaDetalle = obtenerListaDetalle(id,desc);
				break;
			case SlipRCConstructoresSoporteDTO.TIPO:
				listaDetalle = obtenerListaDetalle(slipSoporteDTO.getSlipRCConstructoresSoporte());
				break;			
			case SlipRCFuncionarioSoporteDTO.TIPO:
				listaDetalle = obtenerListaDetalle(slipSoporteDTO.getSlipRCFuncionarioSoporte(),slipSoporteDTO.getIdSlip());
				break;			
			case SlipPrimerRiesgoSoporteDTO.TIPO:
				//listaDetalle = obtenerListaDetalle(slipSoporteDTO.getSlipRCFuncionarioSoporte());
				id = slipSoporteDTO.getCotizacionDTO().getIdToCotizacion().toPlainString();
				listaDetalle = obtenerListaDetalle(id,SlipPrimerRiesgoSoporteDTO.DESCRIPCION);
				break;			
		}
		return listaDetalle;
	}

}
