package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class LineaSoporteReaseguroSN {
	private LineaSoporteReaseguroFacadeRemote beanRemoto;
	
	public LineaSoporteReaseguroSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(LineaSoporteReaseguroFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public LineaSoporteReaseguroDTO agregar(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		try{
			return beanRemoto.save(lineaSoporteReaseguroDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public LineaSoporteReaseguroDTO actualizar(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		try {
			return beanRemoto.update(lineaSoporteReaseguroDTO);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void actualizar(List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTOs) {
		try {
			beanRemoto.update(lineaSoporteReaseguroDTOs);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public LineaSoporteReaseguroDTO getPorId(LineaSoporteReaseguroId lineaSoporteReaseguroId){

		return beanRemoto.findById(lineaSoporteReaseguroId);
	}
	
	public List<LineaSoporteReaseguroDTO> getPorPropiedad(String propiedad, Object object){
		return beanRemoto.findByProperty(propiedad, object);
	}
	
	public void eliminarLineaSoporteReaseguro(LineaSoporteReaseguroDTO entity){
		beanRemoto.eliminarLineaSoporteReaseguro(entity);
	}
	
	public List<LineaSoporteReaseguroDTO> listarFiltrado(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
		return beanRemoto.listarFiltrado(lineaSoporteReaseguroDTO);
	}
	
	public void eliminarLineaSoporteReaseguro(BigDecimal idToSoporteReaseguro, Integer estatusFacultatuvo){
		beanRemoto.eliminarLineaSoporteReaseguro(idToSoporteReaseguro, estatusFacultatuvo);
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1, Integer estatus2, Boolean comparaEstatusIgual){
		return beanRemoto.listarLineaSoporteReaseguroPorEstatus(idToSoporteReaseguro, estatus1, estatus2, comparaEstatusIgual);
	}
	
	public int contarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus, Boolean comparaEstatusIgual){
		return beanRemoto.contarLineaSoporteReaseguroPorEstatus(idToSoporteReaseguro, estatus, comparaEstatusIgual);
	}
	
	public void actualizarEstatusLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal, Integer estatusNuevo,String notaDelSistema){
		beanRemoto.actualizarEstatusLineaSoporte(idToSoporteReaseguro, estatusOriginal, estatusNuevo, notaDelSistema);
	}
	
	public void eliminarLineaSoportePoridSoporteReaseguro(BigDecimal idToSoporteReaseguro, List<BigDecimal> listaIdTmLineaSoporteReaseguro, boolean verificarIdTmLineaIgual){
		beanRemoto.eliminarLineaSoportePoridSoporteReaseguro(idToSoporteReaseguro, listaIdTmLineaSoporteReaseguro, verificarIdTmLineaIgual);
	}
	
	public int contarLineaSoporteReaseguroPorPropiedad(BigDecimal idToSoporteReaseguro, String propiedad,Object valor,boolean comparaEstatusIgual) {
		return beanRemoto.contarLineaSoporteReaseguroPorPropiedad(idToSoporteReaseguro, propiedad, valor, comparaEstatusIgual);
	}
	
	public void actualizarEstatusFacultativoLineaSoporte(BigDecimal idToSoporteReaseguro,int estatusOriginal, int estatusNuevo){
		beanRemoto.actualizarEstatusFacultativoLineaSoporte(idToSoporteReaseguro, estatusOriginal, estatusNuevo);
	}
	
	public void registrarLineasSoporteReaseguro(List<LineaSoporteReaseguroDTO> listaLineasSoporte){
		beanRemoto.registrarLineasSoporteReaseguro(listaLineasSoporte);
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorSoporte(BigDecimal idToSoporteReaseguro){
		return beanRemoto.listarLineaSoporteReaseguroPorSoporte(idToSoporteReaseguro);
	}
}
