package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.ContratoFacultativoDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;
import mx.com.afirme.midas.reaseguro.soporte.validacion.DetalleCobertura;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class LineaSoporteReaseguroDN {
	/**
	 * Constantes para manejo de estatus facultativo de LineaSoporteReaseguroDTO:
	 */
	public static final int ESTATUS_SOPORTADO_POR_REASEGURO = 1;	
	public static final int ESTATUS_REQUIERE_FACULTATIVO = 2;
	public static final int ESTATUS_FACULTTIVO_SOLICITADO = 3;
	public static final int ESTATUS_CANCELACION_FAC_SOLICITADA = 4;
	public static final int ESTATUS_COTIZACION_FAC_AUTORIZADA = 5;
	//public static final int ESTATUS_COTIZACION_FAC_AUTORIZADA_POR_RETENCION = 6;
	public static final int ESTATUS_FACULTATIVO_INTEGRADO = 7;
	public static final int ESTATUS_LIBERADA = 8;
	public static final int ESTATUS_AUTORIZADA_EMISION = 9;
	public static final int ESTATUS_EMITIDA = 10;
	public static final int ESTATUS_LINEA_ENDOSO_FACULTATIVO = 12;//el 11 se us� para estatus cancelado

	//estatus de la linea
	public static final int ESTATUS_LINEA_COTIZANDO = 1;
	public static final int ESTATUS_LINEA_EMITIDO_SIN_DISTIBUIR = 2;
	public static final int ESTATUS_LINEA_EMITIDO_Y_DITRIBUIDO = 3;
	public static final int ESTATUS_LINEA_EMITIDO_INCONSISTENCIAS= 4;
	
	private static final LineaSoporteReaseguroDN INSTANCIA = new LineaSoporteReaseguroDN();

	private LineaSoporteReaseguroDN(){
	}
	
	public static LineaSoporteReaseguroDN getInstancia() {
		return INSTANCIA;
	}
	
	public LineaSoporteReaseguroDTO getPorId(LineaSoporteReaseguroId lineaSoporteReaseguroId) throws SystemException{
		LineaSoporteReaseguroSN lineaSoporteReaseguroSN = new LineaSoporteReaseguroSN();
		return lineaSoporteReaseguroSN.getPorId(lineaSoporteReaseguroId);
	}
	
	public LineaSoporteReaseguroDTO agregar(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws SystemException{
		return new LineaSoporteReaseguroSN().agregar(lineaSoporteReaseguroDTO);
	}
	
	public LineaSoporteReaseguroDTO actualizar(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws SystemException{
		LineaSoporteReaseguroSN lineaSoporteReaseguroSN = new LineaSoporteReaseguroSN();
		return lineaSoporteReaseguroSN.actualizar(lineaSoporteReaseguroDTO);
	}
	
	public List<LineaSoporteReaseguroDTO> getPorPropiedad(String propiedad, Object object) throws SystemException{
		LineaSoporteReaseguroSN lineaSoporteReaseguroSN = new LineaSoporteReaseguroSN();
		return lineaSoporteReaseguroSN.getPorPropiedad(propiedad, object);
	}

	public void defineSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,BigDecimal montoNuevoParaFacultarUSD, BigDecimal porcentajeNuevoParaFacultar) throws ExcepcionDeLogicaNegocio{
		int escala = 10;
		BigDecimal maximo  = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getMaximo());
		BigDecimal porcentajeCesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal plenosPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoPlenoPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal capacidadPrimerExcedente  = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal cien = new BigDecimal(100d).setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePleno = lineaSoporteReaseguroDTO.getPorcentajePleno().setScale(escala,RoundingMode.HALF_UP);

		BigDecimal porcentajeACesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeARetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeFacultativo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal diferienciaPorcentual = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal tipoCambio = new BigDecimal(lineaSoporteReaseguroDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP);
		
		// Paso 1 .- Obtiene datos de la Linea de Reaseguro
		if (lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte() != null){
			porcentajeCesionCP = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte().getPorcentajeCesion()).setScale(escala,RoundingMode.HALF_UP);;
			porcentajeRetencion = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte().getPorcentajeRetencion()).setScale(escala,RoundingMode.HALF_UP);;
		}else{
			porcentajeRetencion = cien;
		}

		if (lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente() != null ) {
			plenosPrimerExcedente = lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente().getNumeroPlenos().setScale(escala,RoundingMode.HALF_UP);
			montoPlenoPrimerExcedente = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente().getMontoPleno()).setScale(escala,RoundingMode.HALF_UP);
			capacidadPrimerExcedente = montoPlenoPrimerExcedente.multiply(plenosPrimerExcedente).setScale(escala, RoundingMode.HALF_UP);
		} 
		//Paso 1.- Redefine la capacidad de la linea basado en la estimaci�n del pleno de suscripci�n.
		maximo = (maximo.multiply(porcentajePleno)).divide(cien,escala,RoundingMode.HALF_UP);
		capacidadPrimerExcedente = (capacidadPrimerExcedente.multiply(porcentajePleno)).divide(cien,escala,RoundingMode.HALF_UP);

		BigDecimal capacidadMaximaLinea = maximo.add(capacidadPrimerExcedente).setScale(escala, RoundingMode.HALF_UP);

		//Paso 3.- Obtener participacion Facultativa
		BigDecimal montoParaFacultar = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
	    BigDecimal montoSumaAseguradaUSD = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().setScale(2,RoundingMode.HALF_UP);
	    if(lineaSoporteReaseguroDTO.getIdMoneda().intValue() != Sistema.MONEDA_DOLARES){
	    	montoSumaAseguradaUSD = montoSumaAseguradaUSD.divide(tipoCambio,RoundingMode.HALF_UP);
	    }
	    
		if( montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 ) {
			montoParaFacultar = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea).setScale(escala, RoundingMode.HALF_UP); 
			//this.setPorcentajeFacultativo((montoParaFacultar.divide(montoSumaAseguradaUSD,RoundingMode.HALF_DOWN)).multiply(cien).doubleValue());
			porcentajeFacultativo = (montoParaFacultar.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
		}

	    //Paso 3a.- Si se requiere facultar una suma � porcentaje en especifico
        if (montoNuevoParaFacultarUSD != null && montoNuevoParaFacultarUSD.compareTo(BigDecimal.ZERO) > 0){
        	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
        	if (montoNuevoParaFacultarUSD.compareTo(montoParaFacultar) >= 0){
        		//y menor o igual a la suma asegurada total
        		if (montoNuevoParaFacultarUSD.compareTo(montoSumaAseguradaUSD) <= 0){
        			montoParaFacultar = montoNuevoParaFacultarUSD;
        			porcentajeFacultativo = (montoNuevoParaFacultarUSD.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
        		}else{
        			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Mayor <br>a la Cotizada: <br>USD$" + montoSumaAseguradaUSD.setScale(2, RoundingMode.UP) + "<br> MX$" + montoSumaAseguradaUSD.multiply(tipoCambio));
        		}
        	}else{
        		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Menor <br>a la Originalmente Requerida <br>por superaci�n de contratos : <br>USD$" + montoParaFacultar.setScale(2, RoundingMode.UP) + "<br> MX$" + montoParaFacultar.multiply(tipoCambio));
        	}
        } else if (porcentajeNuevoParaFacultar != null && porcentajeNuevoParaFacultar.compareTo(BigDecimal.ZERO) > 0){
        	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
        	if (porcentajeNuevoParaFacultar.compareTo(porcentajeFacultativo) >= 0){
        		//y menor o igual al 100%  de la suma asegurada
        		if (porcentajeNuevoParaFacultar.compareTo(cien) <= 0){
        			montoParaFacultar = montoSumaAseguradaUSD.multiply(porcentajeNuevoParaFacultar).divide(cien,escala,RoundingMode.HALF_UP);
        			porcentajeFacultativo = porcentajeNuevoParaFacultar;		
        		}else{
        			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una porcentaje mayor "+ porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP) +"%  al " + cien +"%");
        		}
        	}else{
        		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","Este porcentaje "+porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP)+"% es menor <br> al necesario requerido por superaci�n de la Linea Reaseguro <br> De : " + porcentajeFacultativo.setScale(4, RoundingMode.UP) + "%");
        	}
        }
		
		// Paso 3b.- Ajuste de capacidades basado en lo que se faculta.
        if (montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 || porcentajeFacultativo.compareTo(BigDecimal.ZERO) > 0){
        	BigDecimal excedente = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea);
        	if ((montoParaFacultar.subtract(excedente)).compareTo(capacidadPrimerExcedente) >= 0){
        		maximo = maximo.subtract((montoParaFacultar.subtract(excedente)).subtract(capacidadPrimerExcedente)).setScale(escala, RoundingMode.HALF_UP);
        	}
        }
		
		// Paso 4.- Calcula la Suma Asegurada a Cuota Parte, Retenci�n y la de Primer Excedente
		BigDecimal montoParaMaximo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoParaPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
    	
		if (( montoSumaAseguradaUSD.subtract(maximo)).compareTo(BigDecimal.ZERO) > 0){
			montoParaMaximo = maximo;
			montoParaPrimerExcedente = montoSumaAseguradaUSD.subtract(maximo).subtract(montoParaFacultar).setScale(escala, RoundingMode.HALF_UP);
		} else {
			montoParaMaximo = montoSumaAseguradaUSD;
		}

		BigDecimal montoParaCuotaParte = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		// Paso 5.- Obtener participacion de Cuota Parte
		if ( porcentajeCesionCP.compareTo(BigDecimal.ZERO) > 0 && ( montoParaMaximo.compareTo(BigDecimal.ZERO) > 0 ) ) {
			montoParaCuotaParte = ((montoParaMaximo.multiply(porcentajeCesionCP)).divide(cien));
		//this.setPorcentajeCuotaParte((montoParaCuotaParte.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajeACesionCP = (montoParaCuotaParte.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
		} 

		BigDecimal montoParaRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		// Paso 6.- Obtener participacion de Retencion
		if ( porcentajeRetencion.compareTo(BigDecimal.ZERO) > 0 && ( montoParaMaximo.compareTo(BigDecimal.ZERO) > 0 ) ) {
			montoParaRetencion = (montoParaMaximo.multiply(porcentajeRetencion)).divide(cien,escala,RoundingMode.HALF_UP);
		//this.setPorcentajeRetencion((montoParaRetencion.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajeARetencion = (montoParaRetencion.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
		}
			// Paso 7.- Obtener participacion de Primer Excedente
		if (montoParaPrimerExcedente.compareTo(BigDecimal.ZERO) > 0){
			//this.setPorcentajePrimerExcedente((montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajePrimerExcedente = (montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
		}
		//Proceso para Corregir desviaciones sobre la distribucion al 100%
		porcentajeARetencion = porcentajeARetencion.setScale(escala,RoundingMode.HALF_UP);
		porcentajeACesionCP = porcentajeACesionCP.setScale(escala,RoundingMode.HALF_UP);
		porcentajePrimerExcedente = porcentajePrimerExcedente.setScale(escala,RoundingMode.HALF_UP);
		porcentajeFacultativo = porcentajeFacultativo.setScale(escala,RoundingMode.HALF_UP);
		
		if (!(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo).setScale(escala).compareTo(cien) == 0)){
			diferienciaPorcentual = cien.subtract(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo)).setScale(escala);
			if (porcentajeFacultativo.compareTo(BigDecimal.ZERO) > 0){
				porcentajeFacultativo = porcentajeFacultativo.add(diferienciaPorcentual);
			} else if (porcentajePrimerExcedente.compareTo(BigDecimal.ZERO) > 0) {
				porcentajePrimerExcedente = porcentajePrimerExcedente.add(diferienciaPorcentual);
			}
		}
		lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeARetencion);
		lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeACesionCP);
		lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentajePrimerExcedente);
		lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacultativo);
	}
	
	private void calcularPorcentajesLineaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO,BigDecimal montoNuevoParaFacultarUSD, BigDecimal porcentajeNuevoParaFacultar) throws ExcepcionDeLogicaNegocio{
		int escala = 12;
		BigDecimal maximo  = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getMaximo());
		BigDecimal porcentajeCesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal plenosPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoPlenoPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal capacidadPrimerExcedente  = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal cien = new BigDecimal(100d).setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePleno = lineaSoporteReaseguroDTO.getPorcentajePleno().setScale(escala,RoundingMode.HALF_UP);

		BigDecimal porcentajeACesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeARetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeFacultativo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal diferienciaPorcentual = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal tipoCambio = new BigDecimal(lineaSoporteReaseguroDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP);
		final BigDecimal CANTIDAD_INSIGNIFICANTE = new BigDecimal(0.009d);
		BigDecimal sumaAseguradaAcumulada = new BigDecimal(0d).setScale(escala,RoundingMode.HALF_UP);
		BigDecimal sumaAseguradaEndoso = BigDecimal.ZERO;
		
		/*
		 * Variables usadas para los casos de endosos
		 */
		BigDecimal sumaAseguradaEndosoAnterior = null;
		BigDecimal sumaAseguradaRetEndosoAnterior = null;
		BigDecimal sumaAseguradaCPEndosoAnterior = null;
		BigDecimal sumaAsegurada1EEndosoAnterior = null;
		BigDecimal sumaAseguradaFacEndosoAnterior = null;
		BigDecimal diferenciaSumaAseguradaUSD = null;
		BigDecimal diferenciaSumaAsegurada =null;
		
		// Paso 1 .- Obtiene datos de la Linea de Reaseguro
		if (lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte() != null){
			porcentajeCesionCP = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte().getPorcentajeCesion()).setScale(escala,RoundingMode.HALF_UP);;
			porcentajeRetencion = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte().getPorcentajeRetencion()).setScale(escala,RoundingMode.HALF_UP);;
		}else{
			porcentajeRetencion = cien;
		}

		if (lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente() != null ) {
			plenosPrimerExcedente = lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente().getNumeroPlenos().setScale(escala,RoundingMode.HALF_UP);
			montoPlenoPrimerExcedente = new BigDecimal(lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente().getMontoPleno()).setScale(escala,RoundingMode.HALF_UP);
			capacidadPrimerExcedente = montoPlenoPrimerExcedente.multiply(plenosPrimerExcedente).setScale(escala, RoundingMode.HALF_UP);
		} 
		//Paso 1.- Redefine la capacidad de la linea basado en la estimaci�n del pleno de suscripci�n.
		maximo = (maximo.multiply(porcentajePleno)).divide(cien,escala,RoundingMode.HALF_UP);
		capacidadPrimerExcedente = (capacidadPrimerExcedente.multiply(porcentajePleno)).divide(cien,escala,RoundingMode.HALF_UP);

		BigDecimal capacidadMaximaLinea = maximo.add(capacidadPrimerExcedente).setScale(escala, RoundingMode.HALF_UP);
		
		/*
		 * Obtener las capacidades disponibles basadas en las capacidades usadas en el endoso anterior
		 */
		if(lineaSoporteReaseguroAnteriorDTO != null){
			sumaAseguradaEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getMontoSumaAseguradaAcumulada();//obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada(), new BigDecimal (lineaSoporteReaseguroAnteriorDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP), lineaSoporteReaseguroAnteriorDTO.getIdMoneda().intValue());
			sumaAseguradaRetEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAseguradaCPEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAsegurada1EEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAseguradaFacEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			
			/**
			 * 12/07/2010. Jose Luis Arellano. Se agreg� validaci�n para establecer cantidades disponibles de SA insignificantes en cero
			 */
			maximo = maximo.subtract(sumaAseguradaRetEndosoAnterior).subtract(sumaAseguradaCPEndosoAnterior);
			if(maximo.compareTo(CANTIDAD_INSIGNIFICANTE) < 0)
				maximo = BigDecimal.ZERO;
			capacidadPrimerExcedente = capacidadPrimerExcedente.subtract(sumaAsegurada1EEndosoAnterior);
			if(capacidadPrimerExcedente.compareTo(CANTIDAD_INSIGNIFICANTE) < 0)
				capacidadPrimerExcedente = BigDecimal.ZERO;
			capacidadMaximaLinea = maximo.add(capacidadPrimerExcedente);
			
			diferenciaSumaAsegurada = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().subtract(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada());

			diferenciaSumaAseguradaUSD = obtenerSumaAseguradaEnDolares(diferenciaSumaAsegurada, tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue());
			sumaAseguradaAcumulada = sumaAseguradaAcumulada.add(sumaAseguradaEndosoAnterior).setScale(escala,RoundingMode.HALF_UP);
		}

		//Paso 3.- Obtener participacion Facultativa
		BigDecimal montoParaFacultar = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
	    BigDecimal montoSumaAseguradaUSD = diferenciaSumaAseguradaUSD != null ? diferenciaSumaAseguradaUSD : obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroDTO.getMontoSumaAsegurada(), tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue());

	    sumaAseguradaEndoso = diferenciaSumaAsegurada != null ? diferenciaSumaAsegurada : lineaSoporteReaseguroDTO.getMontoSumaAsegurada();

	    sumaAseguradaAcumulada = sumaAseguradaAcumulada.add(montoSumaAseguradaUSD).setScale(escala,RoundingMode.HALF_UP);
	    
		if( montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 ) {
			montoParaFacultar = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea).setScale(escala, RoundingMode.HALF_UP); 
			//this.setPorcentajeFacultativo((montoParaFacultar.divide(montoSumaAseguradaUSD,RoundingMode.HALF_DOWN)).multiply(cien).doubleValue());
			porcentajeFacultativo = (montoParaFacultar.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
		}

	    //Paso 3a.- Si se requiere facultar una suma � porcentaje en especifico
        if (montoNuevoParaFacultarUSD != null && montoNuevoParaFacultarUSD.compareTo(BigDecimal.ZERO) > 0){
        	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
        	if (montoNuevoParaFacultarUSD.compareTo(montoParaFacultar) >= 0){
        		//y menor o igual a la suma asegurada total
        		if (montoNuevoParaFacultarUSD.compareTo(montoSumaAseguradaUSD) <= 0){
        			montoParaFacultar = montoNuevoParaFacultarUSD;
        			porcentajeFacultativo = (montoNuevoParaFacultarUSD.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
        		}else{
        			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Mayor <br>a la Cotizada: <br>USD$" + montoSumaAseguradaUSD.setScale(2, RoundingMode.UP) + "<br> MX$" + montoSumaAseguradaUSD.multiply(tipoCambio));
        		}
        	}else{
        		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Menor <br>a la Originalmente Requerida <br>por superaci�n de contratos : <br>USD$" + montoParaFacultar.setScale(2, RoundingMode.UP) + "<br> MX$" + montoParaFacultar.multiply(tipoCambio));
        	}
        } else if (porcentajeNuevoParaFacultar != null && porcentajeNuevoParaFacultar.compareTo(BigDecimal.ZERO) > 0){
        	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
        	if (porcentajeNuevoParaFacultar.compareTo(porcentajeFacultativo) >= 0){
        		//y menor o igual al 100%  de la suma asegurada
        		if (porcentajeNuevoParaFacultar.compareTo(cien) <= 0){
        			montoParaFacultar = montoSumaAseguradaUSD.multiply(porcentajeNuevoParaFacultar).divide(cien,escala,RoundingMode.HALF_UP);
        			porcentajeFacultativo = porcentajeNuevoParaFacultar;		
        		}else{
        			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una porcentaje mayor "+ porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP) +"%  al " + cien +"%");
        		}
        	}else{
        		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","Este porcentaje "+porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP)+"% es menor <br> al necesario requerido por superaci�n de la Linea Reaseguro <br> De : " + porcentajeFacultativo.setScale(4, RoundingMode.UP) + "%");
        	}
        }
		
		// Paso 3b.- Ajuste de capacidades basado en lo que se faculta.
        if (montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 || porcentajeFacultativo.compareTo(BigDecimal.ZERO) > 0){
        	BigDecimal excedente = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea);
        	if ((montoParaFacultar.subtract(excedente)).compareTo(capacidadPrimerExcedente) >= 0){
        		maximo = maximo.subtract((montoParaFacultar.subtract(excedente)).subtract(capacidadPrimerExcedente)).setScale(escala, RoundingMode.HALF_UP);
        	}
        }
		
		// Paso 4.- Calcula la Suma Asegurada a Cuota Parte, Retenci�n y la de Primer Excedente
		BigDecimal montoParaMaximo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoParaPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
    	
		if (( montoSumaAseguradaUSD.subtract(maximo)).compareTo(BigDecimal.ZERO) > 0){
			montoParaMaximo = maximo;
			montoParaPrimerExcedente = montoSumaAseguradaUSD.subtract(maximo).subtract(montoParaFacultar).setScale(escala, RoundingMode.HALF_UP);
		} else {
			montoParaMaximo = montoSumaAseguradaUSD;
		}

		BigDecimal montoParaCuotaParte = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		// Paso 5.- Obtener participacion de Cuota Parte
		if ( porcentajeCesionCP.compareTo(BigDecimal.ZERO) > 0 && ( montoParaMaximo.compareTo(BigDecimal.ZERO) > 0 ) ) {
			montoParaCuotaParte = ((montoParaMaximo.multiply(porcentajeCesionCP)).divide(cien));
		//this.setPorcentajeCuotaParte((montoParaCuotaParte.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajeACesionCP = (montoParaCuotaParte.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
		} 

		BigDecimal montoParaRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		// Paso 6.- Obtener participacion de Retencion
		if ( porcentajeRetencion.compareTo(BigDecimal.ZERO) > 0 && ( montoParaMaximo.compareTo(BigDecimal.ZERO) > 0 ) ) {
			montoParaRetencion = (montoParaMaximo.multiply(porcentajeRetencion)).divide(cien,escala,RoundingMode.HALF_UP);
		//this.setPorcentajeRetencion((montoParaRetencion.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajeARetencion = (montoParaRetencion.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
		}
			// Paso 7.- Obtener participacion de Primer Excedente
		if (montoParaPrimerExcedente.compareTo(BigDecimal.ZERO) > 0){
			//this.setPorcentajePrimerExcedente((montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
			porcentajePrimerExcedente = (montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien).setScale(escala, RoundingMode.HALF_UP);
		}
		//Proceso para Corregir desviaciones sobre la distribucion al 100%
		porcentajeARetencion = porcentajeARetencion.setScale(escala,RoundingMode.HALF_UP);
		porcentajeACesionCP = porcentajeACesionCP.setScale(escala,RoundingMode.HALF_UP);
		porcentajePrimerExcedente = porcentajePrimerExcedente.setScale(escala,RoundingMode.HALF_UP);
		porcentajeFacultativo = porcentajeFacultativo.setScale(escala,RoundingMode.HALF_UP);
		
		if (!(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo).setScale(escala).compareTo(cien) == 0)){
			diferienciaPorcentual = cien.subtract(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo)).setScale(escala);
			if (porcentajeFacultativo.compareTo(BigDecimal.ZERO) > 0){
				porcentajeFacultativo = porcentajeFacultativo.add(diferienciaPorcentual);
			} else if (porcentajePrimerExcedente.compareTo(BigDecimal.ZERO) > 0) {
				porcentajePrimerExcedente = porcentajePrimerExcedente.add(diferienciaPorcentual);
			} else if(porcentajeACesionCP.compareTo(BigDecimal.ZERO) > 0){
				porcentajeACesionCP = porcentajeACesionCP.add(diferienciaPorcentual.multiply(porcentajeACesionCP).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				if(porcentajeARetencion.compareTo(BigDecimal.ZERO) > 0){
					porcentajeARetencion = porcentajeARetencion.add(diferienciaPorcentual.multiply(porcentajeARetencion).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				}
			} else 
				porcentajeARetencion = porcentajeARetencion.add(diferienciaPorcentual.multiply(porcentajeARetencion).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
		}
		
		/*
		 * los porcentajes y montos calculados pueden ser los del endoso de aumento
		 */
		if(lineaSoporteReaseguroAnteriorDTO != null){
			/*
			 * 01/11/2010 Se agreg� esta validaci�n debido a que hay casos en que da�os env�a una suma asegurada mayor por mil�simas, que son insignificantes,
			 * y esto causa que la suma asegurada en dolares sea 0 (por redondeo) y los porcentajes calculados se generan con valor de cero.
			 */
			if(diferenciaSumaAseguradaUSD.compareTo(CANTIDAD_INSIGNIFICANTE) > 0){
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(porcentajeARetencion);
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(porcentajeACesionCP);
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(porcentajePrimerExcedente);
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(porcentajeFacultativo);
				
				sumaAseguradaEndosoAnterior = obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada(), new BigDecimal (lineaSoporteReaseguroDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP), lineaSoporteReaseguroAnteriorDTO.getIdMoneda().intValue());
				sumaAseguradaRetEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				sumaAseguradaCPEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				sumaAsegurada1EEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				sumaAseguradaFacEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo().multiply(sumaAseguradaEndosoAnterior).divide(cien);
				
				//posteriormente, calcular los porcentajes totales asignados a Ret, CP, 1E y Fac
				BigDecimal sumaAseguradaRet = sumaAseguradaRetEndosoAnterior.add(montoParaRetencion);
				BigDecimal sumaAseguradaCP = sumaAseguradaCPEndosoAnterior.add(montoParaCuotaParte);
				BigDecimal sumaAsegurada1E = sumaAsegurada1EEndosoAnterior.add(montoParaPrimerExcedente);
				BigDecimal sumaAseguradaFac = sumaAseguradaFacEndosoAnterior.add(montoParaFacultar);
				
				
				BigDecimal montoTotalSumaAsegurada = obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroDTO.getMontoSumaAsegurada(), tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue()).setScale(escala);
				BigDecimal porcentajeRetSiniestros = sumaAseguradaRet.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentajeCPSiniestros = sumaAseguradaCP.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentaje1ESiniestros = sumaAsegurada1E.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentajeFacSiniestros = sumaAseguradaFac.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				
				if (!(porcentajeRetSiniestros.add(porcentajeCPSiniestros).add(porcentaje1ESiniestros).add(porcentajeFacSiniestros).setScale(escala).compareTo(cien) == 0)){
					diferienciaPorcentual = cien.subtract(porcentajeRetSiniestros.add(porcentajeCPSiniestros).add(porcentaje1ESiniestros).add(porcentajeFacSiniestros)).setScale(escala);
					if (porcentajeFacSiniestros.compareTo(BigDecimal.ZERO) > 0){
						porcentajeFacSiniestros = porcentajeFacSiniestros.add(diferienciaPorcentual);
					} else if (porcentaje1ESiniestros.compareTo(BigDecimal.ZERO) > 0) {
						porcentaje1ESiniestros = porcentaje1ESiniestros.add(diferienciaPorcentual);
					} else if(porcentajeCPSiniestros.compareTo(BigDecimal.ZERO) > 0){
						porcentajeCPSiniestros = porcentajeCPSiniestros.add(diferienciaPorcentual.multiply(porcentajeCPSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
						if(porcentajeRetSiniestros.compareTo(BigDecimal.ZERO) > 0){
							porcentajeRetSiniestros = porcentajeRetSiniestros.add(diferienciaPorcentual.multiply(porcentajeRetSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
						}
					} else 
						porcentajeRetSiniestros = porcentajeRetSiniestros.add(diferienciaPorcentual.multiply(porcentajeRetSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				}
				
				lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeRetSiniestros);
				lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeCPSiniestros);
				lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentaje1ESiniestros);
				lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacSiniestros);
			}
			else{
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajeRetencion());
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajeCuotaParte());
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajePrimerExcedente());
				lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getDisPrimaPorcentajeFacultativo());
				
				lineaSoporteReaseguroDTO.setPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion());
				lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte());
				lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente());
				lineaSoporteReaseguroDTO.setPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo());
			}
		}else{
		
			lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeARetencion);
			lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeACesionCP);
			lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentajePrimerExcedente);
			lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacultativo);
			
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(porcentajeARetencion);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(porcentajeACesionCP);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(porcentajePrimerExcedente);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(porcentajeFacultativo);
		}
		lineaSoporteReaseguroDTO.setMontoSumaAseguradaAcumulada(sumaAseguradaAcumulada);
		lineaSoporteReaseguroDTO.setMontoSumaAseguradaEndoso(sumaAseguradaEndoso);
	}
	
	public LineaSoporteReaseguroDTO asignarPorcentajesCancelacionEndoso(
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
			LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO,
			LineaSoporteReaseguroDTO lineaSoporteReaseguroOriginalDTO) throws SystemException{
		int escala = 12;
		BigDecimal cien = new BigDecimal(100d).setScale(escala,RoundingMode.HALF_UP);
		
		BigDecimal porcentajeACesionCP = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeARetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajePrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal porcentajeFacultativo = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal diferienciaPorcentual = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal tipoCambio = new BigDecimal(lineaSoporteReaseguroDTO.getTipoCambio()).setScale(escala,RoundingMode.HALF_UP);
		final BigDecimal CANTIDAD_INSIGNIFICANTE = new BigDecimal(0.009d);
		
		BigDecimal diferenciaSumaAsegurada =null;
		
		BigDecimal sumaAseguradaEndosoAnterior = null;
		BigDecimal sumaAseguradaRetEndosoAnterior = null;
		BigDecimal sumaAseguradaCPEndosoAnterior = null;
		BigDecimal sumaAsegurada1EEndosoAnterior = null;
		BigDecimal sumaAseguradaFacEndosoAnterior = null;
		
		BigDecimal montoParaRetencion = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoParaCuotaParte = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoParaPrimerExcedente = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		BigDecimal montoParaFacultar = BigDecimal.ZERO.setScale(escala,RoundingMode.HALF_UP);
		
		/*
		 * los porcentajes y montos calculados pueden ser los del endoso de aumento
		 */
		if(lineaSoporteReaseguroOriginalDTO != null && lineaSoporteReaseguroDTO != null && lineaSoporteReaseguroAnteriorDTO != null){
			
			porcentajeARetencion = lineaSoporteReaseguroOriginalDTO.getDisPrimaPorcentajeRetencion();
			porcentajeACesionCP = lineaSoporteReaseguroOriginalDTO.getDisPrimaPorcentajeCuotaParte();
			porcentajePrimerExcedente = lineaSoporteReaseguroOriginalDTO.getDisPrimaPorcentajePrimerExcedente();
			porcentajeFacultativo = lineaSoporteReaseguroOriginalDTO.getDisPrimaPorcentajeFacultativo();
			
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(porcentajeARetencion);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(porcentajeACesionCP);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(porcentajePrimerExcedente);
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(porcentajeFacultativo);
			
			diferenciaSumaAsegurada = obtenerSumaAseguradaEnDolares(
					lineaSoporteReaseguroDTO.getMontoSumaAsegurada().subtract(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada()),
					tipoCambio,lineaSoporteReaseguroDTO.getIdMoneda().intValue());
				
			if(diferenciaSumaAsegurada.abs().compareTo(CANTIDAD_INSIGNIFICANTE) <= 0){
				diferenciaSumaAsegurada = BigDecimal.ZERO;
			}
				
			//Calcular la SA distribuida en el endoso anterior
			montoParaRetencion = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion().multiply(diferenciaSumaAsegurada).divide(cien);
			montoParaCuotaParte = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte().multiply(diferenciaSumaAsegurada).divide(cien);
			montoParaPrimerExcedente = lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente().multiply(diferenciaSumaAsegurada).divide(cien);
			montoParaFacultar = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().multiply(diferenciaSumaAsegurada).divide(cien);
			
			//Calcular la SA distribuida en el endoso anterior
			sumaAseguradaEndosoAnterior = obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada(), 
					tipoCambio, lineaSoporteReaseguroOriginalDTO.getIdMoneda().intValue());
			sumaAseguradaRetEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAseguradaCPEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAsegurada1EEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			sumaAseguradaFacEndosoAnterior = lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo().multiply(sumaAseguradaEndosoAnterior).divide(cien);
			
			//posteriormente, calcular los porcentajes totales asignados a Ret, CP, 1E y Fac
			BigDecimal sumaAseguradaRet = sumaAseguradaRetEndosoAnterior.add(montoParaRetencion);
			BigDecimal sumaAseguradaCP = sumaAseguradaCPEndosoAnterior.add(montoParaCuotaParte);
			BigDecimal sumaAsegurada1E = sumaAsegurada1EEndosoAnterior.add(montoParaPrimerExcedente);
			BigDecimal sumaAseguradaFac = sumaAseguradaFacEndosoAnterior.add(montoParaFacultar);
			
			//Validar que no queden montos negativos
			if(sumaAseguradaRet.compareTo(BigDecimal.ZERO) < 0 || sumaAseguradaCP.compareTo(BigDecimal.ZERO) < 0 ||
					sumaAsegurada1E.compareTo(BigDecimal.ZERO) < 0 || sumaAseguradaFac.compareTo(BigDecimal.ZERO) < 0){
				throw new SystemException("No se pueden aplicar los porcentajes de distribuci�n para la cancelaci�n de endoso, el reparto quedar�a negativo.");
			}
			
			BigDecimal montoTotalSumaAsegurada = obtenerSumaAseguradaEnDolares(lineaSoporteReaseguroDTO.getMontoSumaAsegurada(), tipoCambio, lineaSoporteReaseguroDTO.getIdMoneda().intValue()).setScale(escala);
			
			BigDecimal porcentajeRetSiniestros = BigDecimal.ZERO;
			BigDecimal porcentajeCPSiniestros = BigDecimal.ZERO;
			BigDecimal porcentaje1ESiniestros = BigDecimal.ZERO;
			BigDecimal porcentajeFacSiniestros = BigDecimal.ZERO;
			
			if(montoTotalSumaAsegurada.compareTo(CANTIDAD_INSIGNIFICANTE) > 0){
				porcentajeRetSiniestros = sumaAseguradaRet.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				porcentajeCPSiniestros = sumaAseguradaCP.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				porcentaje1ESiniestros = sumaAsegurada1E.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				porcentajeFacSiniestros = sumaAseguradaFac.divide(montoTotalSumaAsegurada,escala,RoundingMode.HALF_UP).multiply(cien).setScale(escala,RoundingMode.HALF_UP);
				
				if (!(porcentajeRetSiniestros.add(porcentajeCPSiniestros).add(porcentaje1ESiniestros).add(porcentajeFacSiniestros).setScale(escala).compareTo(cien) == 0)){
					diferienciaPorcentual = cien.subtract(porcentajeRetSiniestros.add(porcentajeCPSiniestros).add(porcentaje1ESiniestros).add(porcentajeFacSiniestros)).setScale(escala);
					if (porcentajeFacSiniestros.compareTo(BigDecimal.ZERO) > 0){
						porcentajeFacSiniestros = porcentajeFacSiniestros.add(diferienciaPorcentual);
					} else if (porcentaje1ESiniestros.compareTo(BigDecimal.ZERO) > 0) {
						porcentaje1ESiniestros = porcentaje1ESiniestros.add(diferienciaPorcentual);
					} else if(porcentajeCPSiniestros.compareTo(BigDecimal.ZERO) > 0){
						porcentajeCPSiniestros = porcentajeCPSiniestros.add(diferienciaPorcentual.multiply(porcentajeCPSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
						if(porcentajeRetSiniestros.compareTo(BigDecimal.ZERO) > 0){
							porcentajeRetSiniestros = porcentajeRetSiniestros.add(diferienciaPorcentual.multiply(porcentajeRetSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
						}
					} else 
						porcentajeRetSiniestros = porcentajeRetSiniestros.add(diferienciaPorcentual.multiply(porcentajeRetSiniestros).divide(cien,escala,RoundingMode.HALF_UP)).setScale(escala,RoundingMode.HALF_UP);
				}
			}
			else{
				porcentajeRetSiniestros = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion();
				porcentajeCPSiniestros = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte();
				porcentaje1ESiniestros = lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente();
				porcentajeFacSiniestros = lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo();
			}
			
			lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeRetSiniestros);
			lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeCPSiniestros);
			lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentaje1ESiniestros);
			lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacSiniestros);
		}
		else{
			throw new RuntimeException("No se recibieron par�metros v�lidos: lineaSoporteReaseguroOriginalDTO="+lineaSoporteReaseguroOriginalDTO+
					", lineaSoporteReaseguroDTO="+lineaSoporteReaseguroDTO+", lineaSoporteReaseguroAnteriorDTO="+lineaSoporteReaseguroAnteriorDTO);
		}
		
		return lineaSoporteReaseguroDTO;
	}
	
	private BigDecimal obtenerSumaAseguradaEnDolares(BigDecimal montoSumaAsegurada,BigDecimal tipoCambio,int idMoneda){
		BigDecimal montoSumaAseguradaUSD = montoSumaAsegurada.setScale(4,RoundingMode.HALF_UP);
	    if(idMoneda != Sistema.MONEDA_DOLARES){
	    	if(tipoCambio == null){
	    		tipoCambio = new BigDecimal(TipoCambioDN.getInstancia("ADMIN").obtieneTipoCambioPorDia(new Date(), Integer.valueOf(Sistema.MONEDA_DOLARES).shortValue()));
	    	}
	    	montoSumaAseguradaUSD = montoSumaAseguradaUSD.divide(tipoCambio,RoundingMode.HALF_UP);
	    }
	    return montoSumaAseguradaUSD;
	}
	
	public int obtenerNumeroInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
		}
		int numeroInciso = 0;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			numeroInciso = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroInciso()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroInciso().intValue():0;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getNumeroInciso() != null && lineaCobertura.getNumeroInciso().intValue() != numeroInciso){
					numeroInciso = 0;
					break;
				}
			}
		}
		return numeroInciso;
	}
	
	public int obtenerNumeroSubInciso(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
		}
		int numeroSubInciso = 0;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			numeroSubInciso = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroSubInciso()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getNumeroSubInciso().intValue():0;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getNumeroSubInciso() != null && lineaCobertura.getNumeroSubInciso().intValue() != numeroSubInciso){
					numeroSubInciso = 0;
					break;
				}
			}
		}
		return numeroSubInciso;
	}
	
	public BigDecimal obtenerIdToSeccion(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty()){
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
		}
		BigDecimal idToSeccion = BigDecimal.ZERO;
		if(!lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty() ){
			idToSeccion = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getIdToSeccion()!=null?lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(0).getIdToSeccion():BigDecimal.ZERO;
			for(int i=1; i<lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size(); i++){
				LineaSoporteCoberturaDTO lineaCobertura = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().get(i);
				if(lineaCobertura.getIdToSeccion() != null && lineaCobertura.getIdToSeccion().compareTo(idToSeccion) != 0){
					idToSeccion = BigDecimal.ZERO;
					break;
				}
			}
		}
		return idToSeccion;
	}
	
	public LineaSoporteReaseguroDTO solicitarFacultarLinea(BigDecimal idTmLineaSoporteReaseguro) throws SystemException {
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = obtenerLineaPorIdTmLineaSoporteReaseguroId(idTmLineaSoporteReaseguro);
		if(lineaSoporteReaseguroDTO != null && lineaSoporteReaseguroDTO.getEstatusFacultativo() == ESTATUS_LINEA_ENDOSO_FACULTATIVO){
			//buscar las lineas del soporte anterior que tenian
			//buscar la linea que le corresponde a la linea recibida
			List<LineaSoporteReaseguroDTO> listaLineasconContratoAnterior = listarLineaSoporteReaseguroPorSoporte(lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getIdToSoporteReaseguroEndosoAnterior());
			lineaSoporteReaseguroDTO = copiarContratoFacultativo(lineaSoporteReaseguroDTO, listaLineasconContratoAnterior);
		}else if (lineaSoporteReaseguroDTO != null && lineaSoporteReaseguroDTO.getEstatusFacultativo().intValue() == ESTATUS_SOPORTADO_POR_REASEGURO
   	    		|| lineaSoporteReaseguroDTO.getEstatusFacultativo().intValue() == ESTATUS_REQUIERE_FACULTATIVO){
       		SlipDTO slipDTO = SlipDN.getInstancia().llenarSlip(lineaSoporteReaseguroDTO);
            SlipDN.getInstancia().agregar(slipDTO);
            lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_FACULTTIVO_SOLICITADO);
            lineaSoporteReaseguroDTO = LineaSoporteReaseguroDN.getInstancia().actualizar(lineaSoporteReaseguroDTO);
   	    }else{
   	    	throw new  SystemException("No existe la linea reaseguro");
       	}
		return lineaSoporteReaseguroDTO;
    }
	
	private LineaSoporteReaseguroDTO copiarContratoFacultativo(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
			List<LineaSoporteReaseguroDTO> listaLineasconContratoAnterior) throws ExcepcionDeAccesoADatos, SystemException{
		
		LineaSoporteReaseguroDTO lineaAnterior = obtenerLineaSoporteReaseguroPorCumulo(
				lineaSoporteReaseguroDTO.getLineaDTO(), this.obtenerNumeroInciso(lineaSoporteReaseguroDTO), 
				this.obtenerNumeroSubInciso(lineaSoporteReaseguroDTO), listaLineasconContratoAnterior);
		
		return copiarContratoFacultativo(lineaSoporteReaseguroDTO,lineaAnterior,null);
	}
	
	public LineaSoporteReaseguroDTO copiarContratoFacultativo(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,
			LineaSoporteReaseguroDTO lineaContratoAnterior,Integer numeroEndoso) throws ExcepcionDeAccesoADatos, SystemException{
		
		if(lineaContratoAnterior != null && lineaContratoAnterior.getContratoFacultativoDTO() != null){
			SlipDTO slipAnterior = lineaContratoAnterior.getContratoFacultativoDTO().getSlipDTO();
			SlipDTO slipNuevo = SlipDN.getInstancia().llenarSlip(lineaSoporteReaseguroDTO);
			if(numeroEndoso == null)
				slipNuevo.setNumeroEndoso(slipAnterior.getNumeroEndoso() != null ? slipAnterior.getNumeroEndoso() + 1 : 1);
			else{
				slipNuevo.setNumeroEndoso(numeroEndoso);
			}
			slipNuevo = SlipDN.getInstancia().agregar(slipNuevo);
			//el m�todo de duplicar actualiza el estatus de la linea
			ContratoFacultativoDN.getInstancia("tmp").duplicarContratoFacultativo(
					lineaContratoAnterior.getContratoFacultativoDTO().getIdTmContratoFacultativo(), slipNuevo.getIdToSlip(), slipNuevo.getNumeroEndoso());
            lineaSoporteReaseguroDTO = obtenerLineaPorIdTmLineaSoporteReaseguroId(
            		lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
		}
		return lineaSoporteReaseguroDTO;
	}
	
	public LineaSoporteReaseguroDTO cancelarSolicitudFacultativo(BigDecimal idLineaSoporteReaseguro) throws ExcepcionDeAccesoADatos, SystemException {
		LineaSoporteReaseguroDTO  lineaSoporteReaseguro =  LineaSoporteReaseguroDN.getInstancia().obtenerLineaPorIdTmLineaSoporteReaseguroId(idLineaSoporteReaseguro);
		if(lineaSoporteReaseguro != null && lineaSoporteReaseguro.getEstatusFacultativo() != null && 
				lineaSoporteReaseguro.getEstatusFacultativo().intValue() <= LineaSoporteReaseguroDN.ESTATUS_LIBERADA){
			SlipDTO slipDTO = SlipDN.getInstancia().obtenerSlipLineaSoporte(lineaSoporteReaseguro);
			if(slipDTO != null){
				slipDTO.setEstatusCotizacion(BigDecimal.valueOf(3d));
				SlipDN.getInstancia().modificar(slipDTO);
				lineaSoporteReaseguro.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_CANCELACION_FAC_SOLICITADA);
				lineaSoporteReaseguro = LineaSoporteReaseguroDN.getInstancia().actualizar(lineaSoporteReaseguro);
			}
			else{
				throw new SystemException("No se encontr� SLIP para la linea recibida.");
			}
		}
		else{
			throw new SystemException("No se puede cancelar el facultativo debido a que ya se cerr� el contrato.");
		}
		return lineaSoporteReaseguro;
	}
	
	public void actualizarEstatusLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal,Integer estatusNuevo,String notaDelSistema) throws SystemException{
		new LineaSoporteReaseguroSN().actualizarEstatusLineaSoporte(idToSoporteReaseguro, estatusOriginal, estatusNuevo, notaDelSistema);
	}
	
	public boolean aplicaControlReclamo(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws SystemException{
		boolean result = false;
		if (lineaSoporteReaseguroDTO.getId() != null){
			if(lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().isEmpty())
				lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
			for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs()){
				//if(lineaSoporteCoberturaDTO.getAplicaControlReclamo() == true){
				BigDecimal band = lineaSoporteCoberturaDTO.getLineaSoporteReaseguroDTO().getContratoFacultativoDTO()!=null?
				   lineaSoporteCoberturaDTO.getLineaSoporteReaseguroDTO().getContratoFacultativoDTO().getRequiereControlReclamos() : null;				
				if(band!=null &&  band.compareTo(BigDecimal.ONE) == 0 ){
					result = true;
					break;
				}
			}
		}
		return result;
	}
	
	public void eliminarLineaSoporteReaseguro(LineaSoporteReaseguroDTO entity) throws SystemException{
		new LineaSoporteReaseguroSN().eliminarLineaSoporteReaseguro(entity);
	}
	
	public void eliminarLineaSoportePoridSoporteReaseguro(BigDecimal idToSoporteReaseguro, List<BigDecimal> listaIdTmLineaSoporteReaseguro, boolean verificarIdTmLineaIgual) throws SystemException{
		new LineaSoporteReaseguroSN().eliminarLineaSoportePoridSoporteReaseguro(idToSoporteReaseguro, listaIdTmLineaSoporteReaseguro, verificarIdTmLineaIgual);
	}
	
	public LineaSoporteReaseguroDTO obtenerLineaPorIdTmLineaSoporteReaseguroId(BigDecimal idLineaSoporteReaseguro) throws SystemException{
		List<LineaSoporteReaseguroDTO> listaLineas = getPorPropiedad("id.idTmLineaSoporteReaseguro", idLineaSoporteReaseguro);
		LineaSoporteReaseguroDTO linea = null;
		if(listaLineas != null && !listaLineas.isEmpty())
			linea=listaLineas.get(0);
		return linea;
	}
	
	public List<LineaSoporteReaseguroDTO> listarFiltrado(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws SystemException{
		return new LineaSoporteReaseguroSN().listarFiltrado(lineaSoporteReaseguroDTO);
	}
	
	public void eliminarLineaSoporteReaseguro(BigDecimal idToSoporteReaseguro, Integer estatusFacultatuvo) throws SystemException{
		new LineaSoporteReaseguroSN().eliminarLineaSoporteReaseguro(idToSoporteReaseguro, estatusFacultatuvo);
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1, Integer estatus2, Boolean comparaEstatusIgual) throws SystemException{
		return new LineaSoporteReaseguroSN().listarLineaSoporteReaseguroPorEstatus(idToSoporteReaseguro, estatus1, estatus2, comparaEstatusIgual);
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus1) throws SystemException{
		return new LineaSoporteReaseguroSN().listarLineaSoporteReaseguroPorEstatus(idToSoporteReaseguro, estatus1, null, true);
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorEstatusDiferente(BigDecimal idToSoporteReaseguro, Integer estatus1) throws SystemException{
		return new LineaSoporteReaseguroSN().listarLineaSoporteReaseguroPorEstatus(idToSoporteReaseguro, estatus1, null, false);
	}
	
	public int contarLineaSoporteReaseguroPorEstatus(BigDecimal idToSoporteReaseguro, Integer estatus, Boolean comparaEstatusIgual) throws SystemException{
		return new LineaSoporteReaseguroSN().contarLineaSoporteReaseguroPorEstatus(idToSoporteReaseguro, estatus, comparaEstatusIgual);
	}
	
	public List<LineaSoporteReaseguroDTO> listarLineaSoporteReaseguroPorSoporte(BigDecimal idToSoporteReaseguro) throws SystemException{
		return new LineaSoporteReaseguroSN().listarLineaSoporteReaseguroPorSoporte(idToSoporteReaseguro);
	}
	
	/*
	 * Se agreg� TEMPORALMENTE el par�metro calcularLineaSoporte, para definir si se deben o no calcular los porcentajes
	 */
	public LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguro(CumuloPoliza cumuloLinea,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO,Double tipoCambio,BigDecimal idToSoporteReaseguro,Short numeroEndoso,boolean esSoporteEndoso,boolean calcularLineaSoporte) throws ExcepcionDeLogicaNegocio{
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = new LineaSoporteReaseguroDTO();
		if(idToSoporteReaseguro != null){
			lineaSoporteReaseguroDTO.setId(new LineaSoporteReaseguroId());
			lineaSoporteReaseguroDTO.getId().setIdToSoporteReaseguro(idToSoporteReaseguro);
		}
		lineaSoporteReaseguroDTO.setLineaDTO(cumuloLinea.getLineaDTO());
		lineaSoporteReaseguroDTO.setEsPrimerRiesgo(cumuloLinea.getAplicaPrimerRiesgo());
		lineaSoporteReaseguroDTO.setIdMoneda(cumuloLinea.getIdMoneda());
		lineaSoporteReaseguroDTO.setTipoCambio(tipoCambio);
		lineaSoporteReaseguroDTO.setFechaInicioVigencia(cumuloLinea.getFechaInicioVigencia());
		try{
			lineaSoporteReaseguroDTO.setNotaDelSistema("Cotizaci�n"+(esSoporteEndoso?" de endoso ":"")+" sobre Subramo: "+ cumuloLinea.getLineaDTO().getSubRamo().getDescription());
		}catch(Exception e){
			lineaSoporteReaseguroDTO.setNotaDelSistema("Cotizaci�n"+(esSoporteEndoso?" de endoso ":"")+" sobre Subramo: no disponible");
		}
		lineaSoporteReaseguroDTO.setMontoSumaAsegurada(new BigDecimal(cumuloLinea.getSumaAsegurada()));
		lineaSoporteReaseguroDTO.setPorcentajePleno(new BigDecimal(cumuloLinea.getPorcentajePleno()));
		lineaSoporteReaseguroDTO.setIdToCotizacion(cumuloLinea.getIdCotizacion());
		lineaSoporteReaseguroDTO.setEstatusLineaSoporte(1);
		lineaSoporteReaseguroDTO.setFechaModificacion(new Date());
		lineaSoporteReaseguroDTO.setFechaCreacion(new Date());
		lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setPorcentajeRetencion(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setPorcentajeFacultativo(BigDecimal.ZERO);
		lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_SOPORTADO_POR_REASEGURO);
		lineaSoporteReaseguroDTO.setNumeroEndoso(numeroEndoso);
		if(lineaSoporteReaseguroAnteriorDTO != null && lineaSoporteReaseguroAnteriorDTO.getPorcentajePleno() != null ){
			lineaSoporteReaseguroDTO.setPorcentajePleno(lineaSoporteReaseguroAnteriorDTO.getPorcentajePleno());
		}
		//10/01/2010 Se considera el nuevo campo "aplicaDistribucion" para identificar si se tiene prima por distribuir.
		lineaSoporteReaseguroDTO.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.NO_APLICA_DISTRIBUCION_PRIMA);
		if (cumuloLinea.getDetalleCoberturas() != null){
			for (DetalleCobertura detalleCobertura : cumuloLinea.getDetalleCoberturas()) {
				LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO = new LineaSoporteCoberturaDTO();
				if (detalleCobertura.getNumeroInciso() != null)
					lineaSoporteCoberturaDTO.setNumeroInciso(BigDecimal.valueOf(detalleCobertura.getNumeroInciso()));
				if(detalleCobertura.getNumeroSubinciso() != null)
					lineaSoporteCoberturaDTO.setNumeroSubInciso(BigDecimal.valueOf(detalleCobertura.getNumeroSubinciso())); 
				lineaSoporteCoberturaDTO.setIdToSeccion(detalleCobertura.getIdSeccion());
				lineaSoporteCoberturaDTO.setIdToCobertura(detalleCobertura.getIdCobertura());
				lineaSoporteCoberturaDTO.setPorcentajeCoaseguro(new BigDecimal(detalleCobertura.getPorcentajeCoaseguro()));
				lineaSoporteCoberturaDTO.setMontoPrimaSuscripcion(new BigDecimal(detalleCobertura.getMontoPrimaSuscripcion()));
				lineaSoporteCoberturaDTO.setMontoPrimaFacultativo(new BigDecimal(detalleCobertura.getMontoPrimaSuscripcion()));
				lineaSoporteCoberturaDTO.setMontoPrimaAdicional(BigDecimal.ZERO);
				lineaSoporteCoberturaDTO.setFechaCreacion(new Date());
				lineaSoporteCoberturaDTO.setFechaModificacion(new Date());
				if(esSoporteEndoso){
					lineaSoporteCoberturaDTO.setMontoPrimaNoDevengada(new BigDecimal(detalleCobertura.getMontoPrimaNoDevengada()));
				}
				if(detalleCobertura.getMonedaPrima() != null)
					lineaSoporteCoberturaDTO.setIdMoneda(BigDecimal.valueOf(detalleCobertura.getMonedaPrima().doubleValue()));
				else
					lineaSoporteCoberturaDTO.setIdMoneda(cumuloLinea.getIdMoneda());
				lineaSoporteCoberturaDTO.setPorcentajeDeducible(new BigDecimal(detalleCobertura.getPorcentajeDeducible()));
				lineaSoporteCoberturaDTO.setEsPrimerRiesgo(detalleCobertura.getEsPrimerRiesgo());
				try{
					lineaSoporteCoberturaDTO.setNotaDelSistema("Cobertura del Subramo: "+ lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getDescripcionSubRamo());
				}catch(Exception e){
					lineaSoporteCoberturaDTO.setNotaDelSistema("Cobertura del Subramo: no disponible");
				}
				lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().add(lineaSoporteCoberturaDTO);
				if ((detalleCobertura.getMontoPrimaSuscripcion().doubleValue() - 
						(detalleCobertura.getMontoPrimaNoDevengada()!= null ? detalleCobertura.getMontoPrimaNoDevengada().doubleValue() : 0)) != 0){
					lineaSoporteReaseguroDTO.setAplicaDistribucionPrima(LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA);
				}
			}
		}
		if(calcularLineaSoporte)
			calculaSoporteReaseguro(lineaSoporteReaseguroDTO,lineaSoporteReaseguroAnteriorDTO);
		else if (lineaSoporteReaseguroAnteriorDTO != null){
			lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte());
			lineaSoporteReaseguroDTO.setPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo());
			lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente());
			lineaSoporteReaseguroDTO.setPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion());
			
			/*
			 * 14/01/2011 Se implementa correcci�n al c�lculo de porcentajes. En los casos donde se mantiene se disminuye la SA, se deben
			 * aplicar los % globales de distribuci�n, y no los del c�lculo de distribuci�n de prima.
			 */
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeRetencion(lineaSoporteReaseguroAnteriorDTO.getPorcentajeRetencion());
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeCuotaParte(lineaSoporteReaseguroAnteriorDTO.getPorcentajeCuotaParte());
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajePrimerExcedente(lineaSoporteReaseguroAnteriorDTO.getPorcentajePrimerExcedente());
			lineaSoporteReaseguroDTO.setDisPrimaPorcentajeFacultativo(lineaSoporteReaseguroAnteriorDTO.getPorcentajeFacultativo());

			BigDecimal diferenciaSumaAsegurada = lineaSoporteReaseguroDTO.getMontoSumaAsegurada().subtract(lineaSoporteReaseguroAnteriorDTO.getMontoSumaAsegurada());
			
			//18/03/2011 Se agrega registro de la SA del endoso
			lineaSoporteReaseguroDTO.setMontoSumaAseguradaEndoso(diferenciaSumaAsegurada);
			diferenciaSumaAsegurada = obtenerSumaAseguradaEnDolares(diferenciaSumaAsegurada, new BigDecimal(tipoCambio), lineaSoporteReaseguroDTO.getIdMoneda().intValue());
			
			
			if (lineaSoporteReaseguroAnteriorDTO.getMontoSumaAseguradaAcumulada() == null) {
				lineaSoporteReaseguroAnteriorDTO.setMontoSumaAseguradaAcumulada(new BigDecimal("0"));
			}
			
			BigDecimal sumaAseguradaAcumulada = lineaSoporteReaseguroAnteriorDTO.getMontoSumaAseguradaAcumulada().add(diferenciaSumaAsegurada);
			lineaSoporteReaseguroDTO.setMontoSumaAseguradaAcumulada(sumaAseguradaAcumulada);
		}
		if(lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() > 0d)
			lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_REQUIERE_FACULTATIVO);
		if (lineaSoporteReaseguroAnteriorDTO != null){
			if(lineaSoporteReaseguroAnteriorDTO.getContratoFacultativoDTO() != null &&
					lineaSoporteReaseguroAnteriorDTO.getContratoFacultativoDTO().getIdTmContratoFacultativo() != null)
				lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_LINEA_ENDOSO_FACULTATIVO);
		}
		return lineaSoporteReaseguroDTO;
	}
	
	/**
	 * Para calcular el Soporte de Reaseguro primero tendra que validar si la linea tiene todos los datos necesarios para 
	 * ello.
	 * Condiciones:
	 * 1.- Debe tener una LineaDTO autorizada
	 * 2.- Debe tener Suma Asegurada mayor de 0
	 * 3.- Debe tener Porcentaje de Pleno mayor de %0 hasta 100%.
	 * @throws ExcepcionDeLogicaNegocio 
	 *  
	 */
	public void calculaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnteriorDTO) throws ExcepcionDeLogicaNegocio {
		if (lineaSoporteReaseguroDTO.getLineaDTO() != null && lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() == 1){
			if (lineaSoporteReaseguroDTO.getLineaDTO().getEstatus().intValue() == 1 &&
					lineaSoporteReaseguroDTO.getPorcentajePleno().doubleValue() >= 0.0 && 
					lineaSoporteReaseguroDTO.getMontoSumaAsegurada().compareTo(BigDecimal.ZERO) >= 0 &&
						(lineaSoporteReaseguroDTO.getEstatusFacultativo() == LineaSoporteReaseguroDN.ESTATUS_SOPORTADO_POR_REASEGURO 
							|| lineaSoporteReaseguroDTO.getEstatusFacultativo() == LineaSoporteReaseguroDN.ESTATUS_REQUIERE_FACULTATIVO) ){
//				defineSoporteReaseguro(lineaSoporteReaseguroDTO,null,null);
				calcularPorcentajesLineaSoporteReaseguro(lineaSoporteReaseguroDTO, lineaSoporteReaseguroAnteriorDTO, null, null);
			}
		}
	}
	
	/**
	 * Cuenta la cantidad de lineas que pertenecen al SoporteReaseguro cuyo ID se recibe y que cumplan o no con la propiedad recibida. 
	 * @param idToSoporteReaseguro el Id del SoporteReaseguro al que pertenecen las lineas
	 * @param propiedad. El campo que ser� validado.
	 * @param valor. El valor que se buscar� en el campo que se recibe como "propiedad"
	 * @param comparaEstatusIgual. Indica si la b�squeda ser� para registros que tengan el mismo valor(true) o un valor diferente (false)
	 * @return Cantidad de lineas del soporteReaseguro que cumplen con el criterio de b�squeda recibido
	 * @throws SystemException
	 */
	public int contarLineaSoporteReaseguroPorPropiedad(BigDecimal idToSoporteReaseguro, String propiedad,Object valor,boolean comparaEstatusIgual) throws SystemException {
		return new LineaSoporteReaseguroSN().contarLineaSoporteReaseguroPorPropiedad(idToSoporteReaseguro, propiedad, valor, comparaEstatusIgual);
	}
	
	/**
	 * Cuenta la cantidad de lineas que pertenecen al SoporteReaseguro cuyo ID se recibe y que tengan el porcentaje facultativo
	 * igual o distinto del monto recibido.
	 * @param idToSoporteReaseguro el Id del SoporteReaseguro al que pertenecen las lineas
	 * @param porcentaje. El monto de porcentaje que ser� validado.
	 * @param porcentajeIgual. Indica si se deben contar registros con el porcentaje recibido (true) o el porcentaje distinto al recibido (false)
	 * @return
	 * @throws SystemException
	 */
	public int contarLineaSoporteReaseguroPorPorcentajeFacultativo(BigDecimal idToSoporteReaseguro, double porcentaje,boolean porcentajeIgual) throws SystemException {
		return new LineaSoporteReaseguroSN().contarLineaSoporteReaseguroPorPropiedad(idToSoporteReaseguro, "porcentajeFacultativo",porcentaje, porcentajeIgual);
	}
	
	public void actualizarEstatusFacultativoLineaSoporte(BigDecimal idToSoporteReaseguro,Integer estatusOriginal,Integer estatusNuevo) throws SystemException{
		new LineaSoporteReaseguroSN().actualizarEstatusFacultativoLineaSoporte(idToSoporteReaseguro, estatusOriginal, estatusNuevo);
	}
	
	public boolean recalcularSoporteReaseguroPorPorcentajeFacultativo(LineaSoporteReaseguroDTO lineaSoporteReaseguro,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnterior, BigDecimal porcentajeNuevoParaFacultar,List<String> listaErrores) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		return recalcularSoporteReaseguro(lineaSoporteReaseguro,lineaSoporteReaseguroAnterior, porcentajeNuevoParaFacultar, true,listaErrores);
	}
	
	public boolean recalcularSoporteReaseguroPorMontoFacultativo(LineaSoporteReaseguroDTO lineaSoporteReaseguro,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnterior,BigDecimal montoNuevoParaFacultar,List<String> listaErrores) throws ExcepcionDeLogicaNegocio, SystemException {
		return recalcularSoporteReaseguro(lineaSoporteReaseguro,lineaSoporteReaseguroAnterior, montoNuevoParaFacultar, false,listaErrores);
	}
	
	private boolean recalcularSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguro,LineaSoporteReaseguroDTO lineaSoporteReaseguroAnterior,BigDecimal cantidad, boolean esPorcentaje,List<String> listaErrores) throws ExcepcionDeLogicaNegocio, SystemException{
		boolean resultado = false;
		if(lineaSoporteReaseguro.getSoporteReaseguroDTO().getNumeroEndoso().intValue() == 0){
			if (lineaSoporteReaseguro.getLineaDTO() != null && lineaSoporteReaseguro.getEstatusLineaSoporte() == ESTATUS_LINEA_COTIZANDO){
				if (lineaSoporteReaseguro.getLineaDTO().getEstatus().intValue() == 1 &&
						lineaSoporteReaseguro.getPorcentajePleno().doubleValue() >= 0d && 
						lineaSoporteReaseguro.getMontoSumaAsegurada().compareTo(BigDecimal.ZERO) >= 0 &&
						(lineaSoporteReaseguro.getEstatusFacultativo() == ESTATUS_FACULTTIVO_SOLICITADO) ){
					if(esPorcentaje){//Se recalcula por el porcentaje
						calcularPorcentajesLineaSoporteReaseguro(lineaSoporteReaseguro, lineaSoporteReaseguroAnterior, null, cantidad);
	//					defineSoporteReaseguro(lineaSoporteReaseguro,null, cantidad);
					}else{//Se recalcula por el monto facultativo
						BigDecimal montoNuevoParaFacultarUSD = cantidad;
						BigDecimal montoTipoCambio = BigDecimal.valueOf(lineaSoporteReaseguro.getTipoCambio()).setScale(2,RoundingMode.HALF_UP);
						montoNuevoParaFacultarUSD = obtenerSumaAseguradaEnDolares(cantidad, montoTipoCambio, lineaSoporteReaseguro.getIdMoneda().intValue());
	//					if (lineaSoporteReaseguro.getIdMoneda().intValue() != Sistema.MONEDA_DOLARES){
	//						montoNuevoParaFacultarUSD = cantidad.divide(montoTipoCambio,RoundingMode.HALF_UP);
	//					}
						calcularPorcentajesLineaSoporteReaseguro(lineaSoporteReaseguro, lineaSoporteReaseguroAnterior, montoNuevoParaFacultarUSD, null);
	//					defineSoporteReaseguro(lineaSoporteReaseguro,montoNuevoParaFacultarUSD, null);
					}
						
				}else{
					throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede definir un nuevo Porcentaje a Facultar ya que no se encuentra en Proceso de Negociaci�n ");
				}
			}
			BigDecimal []porcentajes = {lineaSoporteReaseguro.getPorcentajeRetencion(),lineaSoporteReaseguro.getPorcentajeCuotaParte(),lineaSoporteReaseguro.getPorcentajePrimerExcedente(),lineaSoporteReaseguro.getPorcentajeFacultativo()};
			
			BigDecimal diferenciaPorcentual1 = UtileriasWeb.obtenerDiferenciaPorcentual(porcentajes, 10);
			
	//		if (porcentajeRiesgoCubierto != 100d){
			if (diferenciaPorcentual1 != null && diferenciaPorcentual1.compareTo(BigDecimal.ZERO) != 0){
				LogDeMidasWeb.log("La Reparticion sobre Linea de Reaseguro y Contratos es Incorrecta:" +
						" %Ret["+ lineaSoporteReaseguro.getPorcentajeRetencion() +"] %CP["+ lineaSoporteReaseguro.getPorcentajeCuotaParte() +"] %1E["+ lineaSoporteReaseguro.getPorcentajePrimerExcedente() +"] %F["+ 
						lineaSoporteReaseguro.getPorcentajeFacultativo() +"] Porcentaje_Sin_Cubrir["+ diferenciaPorcentual1 +"]" +
								"Para la Linea:" + lineaSoporteReaseguro.getLineaDTO().getSubRamo().getDescription() +" TipoDistribucion:" + lineaSoporteReaseguro.getLineaDTO().getTipoDistribucion() , Level.SEVERE, null);
			}else if (diferenciaPorcentual1 != null && diferenciaPorcentual1.compareTo(BigDecimal.ZERO) == 0){
				this.actualizar(lineaSoporteReaseguro);
				resultado = true;
			}else{
				LogDeMidasWeb.log("La Reparticion sobre Linea de Reaseguro y Contratos es Incorrecta:" +
						" %Ret["+ lineaSoporteReaseguro.getPorcentajeRetencion() +"] %CP["+ lineaSoporteReaseguro.getPorcentajeCuotaParte() +"] %1E["+ lineaSoporteReaseguro.getPorcentajePrimerExcedente() +"] %F["+ 
						lineaSoporteReaseguro.getPorcentajeFacultativo() +"] Porcentaje_Sin_Cubrir["+ diferenciaPorcentual1 +"]" +
								"Para la Linea:" + lineaSoporteReaseguro.getLineaDTO().getSubRamo().getDescription() +" TipoDistribucion:" + lineaSoporteReaseguro.getLineaDTO().getTipoDistribucion() , Level.SEVERE, null);
			}
		}
		else{
			if(listaErrores != null){
				listaErrores.add("No se permite modificar porcentajes manualmente en endosos facultativos");
			}
		}
		return resultado;
	}
	
	public boolean integrarCotizacionFacultativa(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,List<LineaSoporteCoberturaDTO> listaLineaSoporteCoberturas,List<String> errores) throws ExcepcionDeAccesoADatos, SystemException{
		boolean resultado = false;
		if(lineaSoporteReaseguroDTO.getEstatusFacultativo().intValue() == ESTATUS_COTIZACION_FAC_AUTORIZADA /*|| lineaSoporteReaseguroDTO.getEstatusFacultativo().intValue() == ESTATUS_COTIZACION_FAC_AUTORIZADA_POR_RETENCION*/){
			//Se actualizan las coberturas de la linea
			LineaSoporteCoberturaDN.getInstancia().integrarCoberturasSoporteReaseguro(lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
			//Se actualiza el estatus de la linea
			lineaSoporteReaseguroDTO.setEstatusFacultativo(ESTATUS_FACULTATIVO_INTEGRADO);
			List<LineaSoporteCoberturaDTO> listaTMP = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs();
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(null);
			LineaSoporteReaseguroDN.getInstancia().actualizar(lineaSoporteReaseguroDTO);
			lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(listaTMP);
			resultado = true;
		}else{
			String error = "El estatus de la linea es diferente de "+ESTATUS_COTIZACION_FAC_AUTORIZADA+"(ESTATUS_COTIZACION_FAC_AUTORIZADA), no se puede integrar el facultativo.";
			if(errores != null)
				errores.add(error);
			LogDeMidasWeb.log("Se intent� integrar el facultativo de una lineaSoporteReaseguroDTO con estatus: "+lineaSoporteReaseguroDTO.getEstatusFacultativo().intValue()+". "+error, Level.WARNING, null);
		}
		return resultado;
	}
	
	public LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguroPorCumulo(LineaDTO lineaDTO,Integer numeroInciso,Integer numeroSubInciso,List<LineaSoporteReaseguroDTO> listaLineasPendientes) throws ExcepcionDeAccesoADatos, SystemException{
		LineaSoporteReaseguroDTO lineaSoporteTMP = null;
		if(listaLineasPendientes != null && !listaLineasPendientes.isEmpty()){
			int tipoDistribucion = lineaDTO.getTipoDistribucion().intValue();
			for (LineaSoporteReaseguroDTO linea : listaLineasPendientes) {
				if ( linea.getLineaDTO().getSubRamo().getIdTcSubRamo().compareTo(lineaDTO.getSubRamo().getIdTcSubRamo()) == 0) {
					if (tipoDistribucion == Sistema.TIPO_CUMULO_POLIZA){
						lineaSoporteTMP = linea;
						break;
					}else{
						int numeroIncisoLinea = LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(linea);
						if( numeroIncisoLinea == numeroInciso.intValue() ){
							if(tipoDistribucion == Sistema.TIPO_CUMULO_INCISO){
								lineaSoporteTMP = linea;
								break;
							} else{
								if (tipoDistribucion == 3 && LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(linea) == numeroSubInciso.intValue()){
								lineaSoporteTMP = linea;
								break;
								}
							}
						}
					}
				}
			}
		}
		return lineaSoporteTMP;
	}
	
	public LineaSoporteReaseguroDTO obtenerLineaSoporteAnterior(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws SystemException{
		LineaSoporteReaseguroDTO lineaAnterior = null;
		if(lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getIdToSoporteReaseguroEndosoAnterior() != null){
			List<LineaSoporteReaseguroDTO> listaLineasAnteriores = listarLineaSoporteReaseguroPorSoporte(
						lineaSoporteReaseguroDTO.getSoporteReaseguroDTO().getIdToSoporteReaseguroEndosoAnterior());
			lineaAnterior = obtenerLineaSoporteReaseguroPorCumulo(lineaSoporteReaseguroDTO.getLineaDTO(), 
						LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteReaseguroDTO), 
						LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporteReaseguroDTO), 
						listaLineasAnteriores);
		}
		return lineaAnterior;
	}
	
	public boolean validarPorcentajesDistribucion(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,List<String> listaErrores){
		boolean porcentajesValidos = true;
		if(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte() != null && 
				lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte().compareTo(BigDecimal.ZERO) != 0 && 
				lineaSoporteReaseguroDTO.getLineaDTO().getContratoCuotaParte() == null){
			porcentajesValidos = false;
			if(listaErrores != null)
				listaErrores.add("No se puede configurar porcentaje Cuota Parte debido a que no se tiene un contrato registrado para esta l�nea.");
		}
		if(porcentajesValidos && lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente() != null && 
				lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente().compareTo(BigDecimal.ZERO) != 0 && 
				lineaSoporteReaseguroDTO.getLineaDTO().getContratoPrimerExcedente() == null){
			porcentajesValidos = false;
			if(listaErrores != null)
				listaErrores.add("No se puede configurar porcentaje Primer Excedente debido a que no se tiene un contrato registrado para esta l�nea.");
		}
		return porcentajesValidos;
	}
	
	public void registrarLineasSoporteReaseguro(List<LineaSoporteReaseguroDTO> listaLineasSoporte) throws SystemException{
		new LineaSoporteReaseguroSN().registrarLineasSoporteReaseguro(listaLineasSoporte);
	}
}

