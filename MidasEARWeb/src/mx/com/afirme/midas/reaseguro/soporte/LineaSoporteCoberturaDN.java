package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class LineaSoporteCoberturaDN {
	private static final LineaSoporteCoberturaDN INSTANCIA = new LineaSoporteCoberturaDN();

	private LineaSoporteCoberturaDN(){
		
	}
	
	public static LineaSoporteCoberturaDN getInstancia() {
		return INSTANCIA;
	}
	
	public LineaSoporteCoberturaDTO agregar(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) throws SystemException {
		return new LineaSoporteCoberturaSN().agregar(lineaSoporteCoberturaDTO);
	}
	
	public void modificar(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) throws SystemException{
		LineaSoporteCoberturaSN lineaSoporteCoberturaSN = new LineaSoporteCoberturaSN();
		lineaSoporteCoberturaSN.modificar(lineaSoporteCoberturaDTO);
	}
	
	public LineaSoporteCoberturaDTO getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO) throws SystemException{
		LineaSoporteCoberturaSN lineaSoporteCoberturaSN = new LineaSoporteCoberturaSN();
		return lineaSoporteCoberturaSN.getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
	}
	
	public LineaSoporteCoberturaDTO getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO,List<LineaSoporteCoberturaDTO> listaLineasoporteCoberturaDTO) throws SystemException{
		LineaSoporteCoberturaDTO cobertura = null;
		if(listaLineasoporteCoberturaDTO != null && detalleContratoFacultativoDTO != null){
			BigDecimal idCoberturaContrato = detalleContratoFacultativoDTO.getIdToCobertura() != null ? detalleContratoFacultativoDTO.getIdToCobertura() : BigDecimal.ZERO ;
			BigDecimal idSeccionContrato = detalleContratoFacultativoDTO.getIdToSeccion() != null ? detalleContratoFacultativoDTO.getIdToSeccion() : BigDecimal.ZERO ;
			BigDecimal numeroIncisoContrato = detalleContratoFacultativoDTO.getNumeroInciso() != null ? detalleContratoFacultativoDTO.getNumeroInciso() : BigDecimal.ZERO ;
			BigDecimal numeroSubIncisoContrato = detalleContratoFacultativoDTO.getNumeroSubInciso() != null ? detalleContratoFacultativoDTO.getNumeroSubInciso() : BigDecimal.ZERO ;
			
			for(LineaSoporteCoberturaDTO lineaCobertura : listaLineasoporteCoberturaDTO){
				BigDecimal idCoberturaLinea = lineaCobertura.getIdToCobertura() != null ? lineaCobertura.getIdToCobertura() : BigDecimal.ZERO ;
				BigDecimal idSeccionLinea = lineaCobertura.getIdToSeccion() != null ? lineaCobertura.getIdToSeccion() : BigDecimal.ZERO ;
				BigDecimal numeroIncisoLinea = lineaCobertura.getNumeroInciso() != null ? lineaCobertura.getNumeroInciso() : BigDecimal.ZERO ;
				BigDecimal numeroSubIncisoLinea = lineaCobertura.getNumeroSubInciso() != null ? lineaCobertura.getNumeroSubInciso() : BigDecimal.ZERO ;
				
				if(idCoberturaContrato.compareTo(idCoberturaLinea) == 0 &&
						idSeccionContrato.compareTo(idSeccionLinea) == 0 &&
						numeroIncisoContrato.compareTo(numeroIncisoLinea) == 0 &&
						numeroSubIncisoContrato.compareTo(numeroSubIncisoLinea) == 0){
					cobertura = lineaCobertura;
					break;
				}
			}
		}
		return cobertura;
	}
	
	public List<LineaSoporteCoberturaDTO> getPorPropiedad(String propiedad, Object valor) throws SystemException{
		LineaSoporteCoberturaSN lineaSoporteCoberturaSN = new LineaSoporteCoberturaSN();
		return lineaSoporteCoberturaSN.getPorPropiedad(propiedad, valor);
	}
	
	public void eliminarLineasSoporteCoberturaPorLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro) throws SystemException{
		new LineaSoporteCoberturaSN().eliminarLineasSoporteCoberturaPorLineaSoporteReaseguro(idTmLineaSoporteReaseguro);
	}
	
	public List<LineaSoporteCoberturaDTO> listarLineaSoporteCoberturaPorLineaSoporteId(BigDecimal idTmLineaSoporteReaseguro) throws ExcepcionDeAccesoADatos, SystemException{
		return new SoporteReaseguroSN().obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro", idTmLineaSoporteReaseguro);
	}
	
	public void notificarEmisionCoberturas(BigDecimal idToSoporteReaseguro, BigDecimal idToPoliza) throws SystemException{
		new LineaSoporteCoberturaSN().notificarEmisionCoberturas(idToSoporteReaseguro, idToPoliza);
	}
	
	public Double getMontoPrimaContratos(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO){
		BigDecimal primaNoDevengada = lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() != null ? lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() : BigDecimal.ZERO;
		
		Double montoPrimaRetencion = (lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() - primaNoDevengada.doubleValue()) * 
									(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion()!=null ? lineaSoporteReaseguroDTO.getDisPrimaPorcentajeRetencion().doubleValue() : 0d) /100;
		Double montoPrimaCP = (lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() - primaNoDevengada.doubleValue()) * 
									(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte()!=null ? lineaSoporteReaseguroDTO.getDisPrimaPorcentajeCuotaParte().doubleValue() : 0) /100;
		Double montoPrima1E = (lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue()- primaNoDevengada.doubleValue())* 
									(lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente()!= null ? lineaSoporteReaseguroDTO.getDisPrimaPorcentajePrimerExcedente().doubleValue() : 0) /100;
		//Double montoPrimaFacultativa = lineaSoporteReaseguroCoberturaDTO.getMontoPrimaSuscripcion().doubleValue() * this.getPorcentajeFacultativo();
		Double montoPrimaContratos = montoPrimaRetencion + montoPrimaCP + montoPrima1E;
		
		return montoPrimaContratos;
	}
	
	public Double getMontoPrimaFacultada(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO,LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO){
		BigDecimal primaNoDevengada = lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() != null ? lineaSoporteCoberturaDTO.getMontoPrimaNoDevengada() : BigDecimal.ZERO;
		return (lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() - primaNoDevengada.doubleValue()) * 
				(lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo()!=null? lineaSoporteReaseguroDTO.getDisPrimaPorcentajeFacultativo().doubleValue() : 0) /100;
	}
	
	public void integrarCoberturasSoporteReaseguro (BigDecimal idTmLineaSoporteReaseguro) throws SystemException{
		new LineaSoporteCoberturaSN().integrarCoberturasSoporteReaseguro(idTmLineaSoporteReaseguro);
	}
}
