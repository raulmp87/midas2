package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.slip.SlipFacadeRemote;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Aram Salinas
 * @version 1.0
 * @created 01-nov-2009 12:21:00 PM
 */
public class SoporteReaseguroSN {
	
	private SoporteReaseguroFacadeRemote beanRemoto;	
	private LineaSoporteReaseguroFacadeRemote beanRemotoLinea;	
	private LineaSoporteCoberturaFacadeRemote beanRemotoLineaCobertura;	
	private SlipFacadeRemote beanRemotoSlip;

	public SoporteReaseguroSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SoporteReaseguroFacadeRemote.class);
			beanRemotoLinea = serviceLocator.getEJB(LineaSoporteReaseguroFacadeRemote.class);
			beanRemotoLineaCobertura = serviceLocator.getEJB(LineaSoporteCoberturaFacadeRemote.class);
			beanRemotoSlip = serviceLocator.getEJB(SlipFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}	

	public SoporteReaseguroSN(boolean instanciarBeanRemotoLinea,boolean instanciarBeanRemotoLineaCobertura,boolean instanciarBeanRemotoSlip) throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SoporteReaseguroFacadeRemote.class);
			if(instanciarBeanRemotoLinea)
				beanRemotoLinea = serviceLocator.getEJB(LineaSoporteReaseguroFacadeRemote.class);
			if(instanciarBeanRemotoLineaCobertura)
				beanRemotoLineaCobertura = serviceLocator.getEJB(LineaSoporteCoberturaFacadeRemote.class);
			if(instanciarBeanRemotoSlip)
				beanRemotoSlip = serviceLocator.getEJB(SlipFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
 	public void agregarLinea(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
 			beanRemotoLinea.save(lineaSoporteReaseguroDTO);
 	}
	
	
	public SoporteReaseguroDTO agregar(SoporteReaseguroDTO SoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
		return beanRemoto.save(SoporteReaseguroDTO);
 	}

	public void agregar(BigDecimal idToSoporteReaseguroDTO,List<LineaSoporteReaseguroDTO> lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
 			beanRemoto.save(idToSoporteReaseguroDTO,lineaSoporteReaseguroDTO);
 	}
 
	public void borrar(SoporteReaseguroDTO SoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
 			beanRemoto.delete(SoporteReaseguroDTO);
 	}	

	public void actualiza(SoporteReaseguroDTO SoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
 			beanRemoto.update(SoporteReaseguroDTO);
 	}

	public SoporteReaseguroDTO actualizar(SoporteReaseguroDTO SoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
		return beanRemoto.update(SoporteReaseguroDTO);
 	}
	
	public LineaDTO obtenerLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo,BigDecimal estatus) throws ExcepcionDeAccesoADatos {
 			return beanRemoto.getLineaDTO(fechaInicioVigencia, idSubramo,estatus);
 	}

	public LineaDTO obtenerLineaDTO(BigDecimal idTmLinea) throws ExcepcionDeAccesoADatos {
		return beanRemoto.getLineaDTO(idTmLinea);
 	}
	
	public SoporteReaseguroDTO getPorId(BigDecimal idToSoporteReaseguro) throws ExcepcionDeAccesoADatos {
 		return beanRemoto.findById(idToSoporteReaseguro);
 	}	

	public SoporteReaseguroDTO getPorIdCotizacion(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos {
 		return beanRemoto.getPorIdCotizacion(idToCotizacion);
 	}
	
	public List<SoporteReaseguroDTO> getPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos {
			return beanRemoto.findByProperty(propertyName, value);
 	}	
    //optener linea Por Id
	public LineaSoporteReaseguroDTO obtenerLineasPorId(LineaSoporteReaseguroId lineaSoporteReaseguroId) throws ExcepcionDeAccesoADatos {
	 	return beanRemotoLinea.findById(lineaSoporteReaseguroId);
	}
	
	///obtiene lineas soportadas 
  	
	public List<LineaSoporteReaseguroDTO> obtenerLineasPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos {
		return beanRemotoLinea.findByProperty(propertyName, value);
	}
	//borra linea  
	public void borrarLineaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
		beanRemotoLinea.delete(lineaSoporteReaseguroDTO);
	}
	//actualiza la linea 
	public void actualizaLineaSoporte(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) throws ExcepcionDeAccesoADatos {
		beanRemotoLinea.update(lineaSoporteReaseguroDTO);
	}
	
	//eliminar coberturas
	public void borrarCoberturas(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) throws ExcepcionDeAccesoADatos {
		beanRemotoLineaCobertura.delete(lineaSoporteCoberturaDTO);
	}	
	
	//agregar nuevas coberturas
	public void agregarLineaCobertura(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) throws ExcepcionDeAccesoADatos {
	 		beanRemotoLineaCobertura.save(lineaSoporteCoberturaDTO);
 	  }
	//busca todas las coberturas
	public List<LineaSoporteCoberturaDTO> obtenerCoberturasPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos {
		return beanRemotoLineaCobertura.findByProperty(propertyName, value);
	}
	//actualizar Coberturas
	public void actualizarCoberturas(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) throws ExcepcionDeAccesoADatos {
 		beanRemotoLineaCobertura.update(lineaSoporteCoberturaDTO);
	  }
	
	public List<SoporteReaseguroDTO> obtenerSoporteReaseguroPorEstatusLineas(Integer estatusLineaSoporte) {
		return beanRemoto.obtenerSoporteReaseguroPorEstatusLineas(estatusLineaSoporte);
	}

	public SoporteReaseguroDTO obtenerSoporteReaseguroPorPolizaEndoso(BigDecimal idToPoliza, Integer numeroEndoso) {
		return beanRemoto.obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
	}

	public SoporteReaseguroDTO obtenerSoporteReaseguroPorPolizaEndoso(BigDecimal idToCotizacion, BigDecimal idToPoliza, Integer numeroEndoso) {
		return beanRemoto.obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
	}
	
	public void cancelarCotizacionFacultativo(BigDecimal idToCotizacion){
		beanRemotoSlip.cancelarCotizacionFacultativo(idToCotizacion);
	}	
	
	public boolean notificarEmisionEndoso(SoporteReaseguroDTO soporteReaseguroOriginal, BigDecimal idToPoliza,int numeroEndoso,
			BigDecimal idToCotizacionEndosoCancelacion,Date fechaEmisionCancelacion,Date fechaInicioVigencia,BigDecimal factorPrima,short tipoEndoso,List<String> listaErrores,boolean ignorarSoporteExistente){
		return beanRemoto.notificarEmisionEndoso(soporteReaseguroOriginal, idToPoliza, numeroEndoso, 
				idToCotizacionEndosoCancelacion, fechaEmisionCancelacion,fechaInicioVigencia,factorPrima,tipoEndoso, listaErrores,ignorarSoporteExistente);
	}
	
	public double obtenerDiferenciaPrimaSoportePoliza(BigDecimal idToPoliza,Integer numeroEndoso){
		return beanRemoto.obtenerDiferenciaPrimaSoportePoliza(idToPoliza, numeroEndoso);
	}
	
	public SoporteReaseguroDTO notificarEmision(BigDecimal idToSoporteReaseguro, BigDecimal idToPoliza, int numeroEndoso, Date fechaEmision){
		return beanRemoto.notificarEmision(idToSoporteReaseguro, idToPoliza, numeroEndoso, fechaEmision);
	}
	
	public List<String> validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToPoliza,int numeroEndoso) {
		return beanRemoto.validarSoporteReaseguroDTOListoParaDistribucion(idToPoliza, numeroEndoso);
	}
	
	public List<String> validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToSoporteReaseguro) {
		return beanRemoto.validarSoporteReaseguroDTOListoParaDistribucion(idToSoporteReaseguro);
	}
	
	public List<BigDecimal> consultarSoportesPendientesDistribucion(){
		return beanRemoto.consultarSoportesPendientesDistribucion();
	}
	
	public boolean actualizarBanderaLineasAplicanDistribucion(BigDecimal idToSoporteReaseguro){
		return beanRemoto.actualizarBanderaLineasAplicanDistribucion(idToSoporteReaseguro);
	}
	
	public BigDecimal calcularFactorAplicacionEndosoCancelacionRehabilitacion(BigDecimal idToPoliza,int numeroEndosoCanceladoRehabilitado,short claveTipoEndoso){
		return beanRemoto.calcularFactorAplicacionEndosoCancelacionRehabilitacion(idToPoliza, numeroEndosoCanceladoRehabilitado,claveTipoEndoso);
	}
	
	public LineaDTO obtenerLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo) throws ExcepcionDeAccesoADatos, SystemException {
    	return beanRemoto.obtenerLineaDTO(fechaInicioVigencia, idSubramo);
	}
	
	public LineaDTO obtenerLineaDTOVigente(Date fechaInicioVigencia, BigDecimal idSubramo) throws ExcepcionDeAccesoADatos, SystemException {
    	return beanRemoto.obtenerLineaDTOVigente(fechaInicioVigencia, idSubramo);
	}
	
	public List<SoporteReaseguroDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion){
		return beanRemoto.listarPorIdToCotizacion(idToCotizacion);
	}
}
