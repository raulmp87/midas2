package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;



/**
 * 
 * @author Aram Salinas
 * @version 1.0
 * @created 01-nov-2009 12:21:00 PM
 */
public class LineaSoporteReaseguro {

	/**
	 * Esta lista contiene la Lista de LienaSoporteReaseguroCobrertura 
	 */
	//private List<LineaSoporteCoberturaDTO> listaDeCoberturas;
//	@SuppressWarnings("unused")
//	private BigDecimal idSlip;
	private LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO;
	private LineaSoporteReaseguro lineaSoporteReaseguroEndosoAnterior;
	
	
	//estatus facultativo
	public static final int SOPORTADO_POR_REASEGURO = 0;
	public static final int REQUIERE_FACULTATIVO = 1;
	public static final int FACULTATIVO_SOLICITADO = 2;
	public static final int FACULTATIVO_COTIZADO = 3;
	public static final int FACULTATIVO_INTEGRADO = 4;
	public static final int FACULTATIVO_CONTRATADO = 5;
	public static final int FACULTATIVO_POR_CANCELAR = 6;
	public static final int FACULTATIVO_CANCELADO = 7;
	
	//estatus de la linea
	public static final int COTIZANDO = 1;
	public static final int EMITIDO_SIN_DISTIBUIR = 2;
	public static final int EMITIDO_Y_DITRIBUIDO = 3;
	
	//estatus del SLIP
	
	public void finalize() throws Throwable {

	}

	public LineaSoporteReaseguro(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO){
		this.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
 	}

	/**
	 * TODO : Borrame
	 */
	public LineaSoporteReaseguro(){

	}	

	public BigDecimal getIdMonedaMonto(){
		return this.lineaSoporteReaseguroDTO.getIdMoneda();
	}

	/**
	 * 
	 * @param idMoneda
	 */
	public void setIdMonedaMonto(BigDecimal idMoneda){
		if (this.esSoporteCotizacion()){
			this.lineaSoporteReaseguroDTO.setIdMoneda(idMoneda);
		}
	}

	public Double getPorcentajePleno(){
		if (lineaSoporteReaseguroDTO.getPorcentajePleno() !=null){
	 	  return this.lineaSoporteReaseguroDTO.getPorcentajePleno().doubleValue();
		}else{
		  this.setPorcentajePleno(100.0);
		  return  Double.valueOf(100.0);	
		}
	}

	/**
	 * 
	 * @param porcentajePleno
	 */
	public void setPorcentajePleno(Double porcentajePleno){
		if (this.esSoporteCotizacion()){
			this.lineaSoporteReaseguroDTO.setPorcentajePleno(new BigDecimal(porcentajePleno));
		}
	}

	public Double getMontoSumaAsegurada(){
		if(this.lineaSoporteReaseguroDTO.getMontoSumaAsegurada() != null){ 
		 return this.lineaSoporteReaseguroDTO.getMontoSumaAsegurada().doubleValue();
		}else{
			this.lineaSoporteReaseguroDTO.setMontoSumaAsegurada(BigDecimal.ZERO);
			return Double.valueOf(0);
		}
	}

	/**
	 * 
	 * @param montoSumaAsegurada
	 */
	public void setMontoSumaAsegurada(Double montoSumaAsegurada){
		if (this.esSoporteCotizacion()){
			this.lineaSoporteReaseguroDTO.setMontoSumaAsegurada(BigDecimal.ZERO);
		}
	}

	public BigDecimal getIdLineaReaseguro(){
		return this.lineaSoporteReaseguroDTO.getLineaDTO().getIdTmLinea();
	}

	public BigDecimal getIdLineaSoporteReaseguro(){
		return this.lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro();
	}

	public BigDecimal getIdCotizacion(){
		return this.lineaSoporteReaseguroDTO.getIdToCotizacion();
	}

	/**
	 * 
	 * @param idToCotizacion
	 */
	public void setIdCotizacion(BigDecimal idToCotizacion){
		if (this.esSoporteCotizacion()){
			this.lineaSoporteReaseguroDTO.setIdToCotizacion(idToCotizacion);
		}
	}

	public BigDecimal getIdEndoso(){
		//TODO : Implementar el metodo
		return null; //this.LineaSoporteReaseguroDTO.getIdToEndoso();
	}

	/**
	 * 
	 * @param idToCotizacion
	 */
	public void setIdEndoso(BigDecimal idToEndoso){
		if (this.esSoporteCotizacion()){
			//TODO : Implementar el metodo
			//  this.LineaSoporteReaseguroDTO.setIdToEndoso(idToEndoso);
		}
	}	

	public BigDecimal getIdPoliza(){
		return this.lineaSoporteReaseguroDTO.getIdToPoliza();
	}

	/**
	 * 
	 * @param idToPoliza
	 */
	public void setIdPoliza(BigDecimal idToPoliza){
		if (this.esSoporteCotizacion()){
			this.lineaSoporteReaseguroDTO.setIdToPoliza(idToPoliza);
		}
	}



	public Double getTipoCambio(){
		return this.lineaSoporteReaseguroDTO.getTipoCambio();
	}

	/**
	 *  El tipo de Cambio es el Operativo, que se utiliza para transformar la Suma Asegurara de Pesos a Dolares
	 *  este metodo debera ser extraido basado en la fecha en que se creo el Soporte.
	 * @param tipoCambio
	 */
	public void setTipoCambio(Double tipoCambio){
		this.lineaSoporteReaseguroDTO.setTipoCambio(tipoCambio);

	}

	public BigDecimal getPorcentajeRetencion(){
		if (this.lineaSoporteReaseguroDTO.getPorcentajeRetencion() != null){
		return this.lineaSoporteReaseguroDTO.getPorcentajeRetencion();
		}else{
			this.lineaSoporteReaseguroDTO.setPorcentajeRetencion(BigDecimal.ZERO);
			return BigDecimal.ZERO;
		}
		
	}

	/**
	 * 
	 * @param porcentajeRetencion
	 */
	private void setPorcentajeRetencion(BigDecimal porcentajeRetencion){
		this.lineaSoporteReaseguroDTO.setPorcentajeRetencion(porcentajeRetencion);
	}	

	public BigDecimal getPorcentajeCuotaParte(){
		if (this.lineaSoporteReaseguroDTO.getPorcentajeCuotaParte() != null){
		  return this.lineaSoporteReaseguroDTO.getPorcentajeCuotaParte();
		}else{
			this.lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(BigDecimal.ZERO);
			return BigDecimal.ZERO;
		}
	}

	/**
	 * 
	 * @param porcentajeCuotaParte
	 */
	private void setPorcentajeCuotaParte(BigDecimal porcentajeCuotaParte){
		this.lineaSoporteReaseguroDTO.setPorcentajeCuotaParte(porcentajeCuotaParte);
	}


	public BigDecimal getPorcentajePrimerExcedente(){
		if(this.lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente()!= null){
		  return this.lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente();
		}else{
			this.lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(BigDecimal.ZERO);
			return BigDecimal.ZERO;
		}
	}

	/**
	 * 
	 * @param porcentajePrimerExcedente
	 */
	private void setPorcentajePrimerExcedente(BigDecimal porcentajePrimerExcedente){
		this.lineaSoporteReaseguroDTO.setPorcentajePrimerExcedente(porcentajePrimerExcedente);
	}

	public BigDecimal getPorcentajeFacultativo(){
		if (this.lineaSoporteReaseguroDTO.getPorcentajeFacultativo() != null){
		   return this.lineaSoporteReaseguroDTO.getPorcentajeFacultativo();
		}else{
			this.lineaSoporteReaseguroDTO.setPorcentajeFacultativo(BigDecimal.valueOf(100.0));
			return BigDecimal.valueOf(100);
		}
	}

	/**
	 * 
	 * @param porcentajePrimerExcedente
	 */
	private void setPorcentajeFacultativo(BigDecimal porcentajeFacultativo){
		this.lineaSoporteReaseguroDTO.setPorcentajeFacultativo(porcentajeFacultativo);
	}	

	public BigDecimal getIdSubramo(){
		return this.lineaSoporteReaseguroDTO.getLineaDTO().getSubRamo().getIdTcSubRamo();
	}

	
	public List<LineaSoporteCoberturaDTO> getListaDeCoberturas(){
		return  lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs();
	}

	/**
	 * TODO : Es muy probable que se tenga que borrar
	 * @param listaSoporteCoberturas
	 */
	public void setListadeCoberturas(List<LineaSoporteCoberturaDTO> listaSoporteCoberturas){
		lineaSoporteReaseguroDTO.setLineaSoporteCoberturaDTOs(listaSoporteCoberturas);
	}

	public boolean isAplicaPrimerRiesgo(){
		return this.lineaSoporteReaseguroDTO.getEsPrimerRiesgo();
	}

	/**
	 * 
	 * @param aplicaPrimerRiesgo
	 */
	public void setAplicaPrimerRiesgo(boolean aplicaPrimerRiesgo){
		this.lineaSoporteReaseguroDTO.setEsPrimerRiesgo(aplicaPrimerRiesgo);
	}	

	/**
	 * Leyendas
	 * 0 = Soportado por Reaseguro
	 * Este estatus se determina cuando al validar el soporte de la linea resulta que
	 * % Facultato es 0.
	 * 
	 * 1 = Requiere Facultativo
	 * Este estatus se determina cuando al validar el soporte de la linea resulta que
	 * % Facultato es mayor a 0.
	 * 
	 * 2 = Facultativo Solicitado.
	 * Este se define cuando Suscripcion Solicita Facultar la linea.
	 * 3 = Facultativo Cotizado (Procede a Vender)
	 * Se define cuando Reaseguro Autoriza la Cotizaci�n.
	 * 4 = Facultativo Integrado (Es cuando se igualan las primas
	 * que Facultativo envio)
	 * Se define cuando Suscripcion iguala Primas en Cotizaci�n, este estatus es
	 * definido por Suscripci�n.
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 * @throws ExcepcionDeLogicaNegocio 
	 * 
	 */
	public Integer getEstatusFacultativo() throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		SlipDN slipSN = SlipDN.getInstancia();
		SlipDTO slipDTO = null;
		if(lineaSoporteReaseguroDTO.getId() != null){
 	  	     slipDTO = slipSN.obtenerSlipLineaSoporte(lineaSoporteReaseguroDTO);
		}
 	  	SoporteReaseguroSN soporteReaseguroSN = new SoporteReaseguroSN();
	   	if(slipDTO != null ){ 
	   		List<LineaSoporteCoberturaDTO> listaDeCoberturas =  soporteReaseguroSN.obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro",lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
	    	//   if(lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() == 2 || lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() == 3  ){	
	  	    	if(slipDTO.getEstatusCotizacion().intValue() == 1){
	  	  	      if (listaDeCoberturas != null){
	  	  		      if(comparaMontosCoberturas(listaDeCoberturas)){
	  	  		    	  return Integer.valueOf(FACULTATIVO_INTEGRADO);
	  	  		      }else{
	  	  		    	  return Integer.valueOf(FACULTATIVO_COTIZADO);
	  	  		      }
	  	  	      }else{
	  	  	    	   return Integer.valueOf(3);//escribir en el log negocio las coberturas 
	  	  	      }
	    	   }else{
	    		 if(slipDTO.getEstatusCotizacion().intValue() == 3){  
	    			 return Integer.valueOf(FACULTATIVO_POR_CANCELAR); 
	    		 }else{	 
	    		  if (slipDTO.getEstatusCotizacion().intValue() == 2){
	    			return Integer.valueOf(FACULTATIVO_CONTRATADO); 
	    		   }else{
	    			   if (slipDTO.getEstatusCotizacion().intValue() == 4){
	   	    				return Integer.valueOf(FACULTATIVO_CANCELADO); 
	   	    		   }else if (slipDTO.getEstatusCotizacion().intValue() == 0)
	   	    			   return Integer.valueOf(FACULTATIVO_SOLICITADO);
	   	    		   else
	   	    			   throw new SystemException("Estatus del slip incorrecto: "+slipDTO.getEstatusCotizacion()+". Debe estar entre 0 y 4.");
	   	    		   
	    		   }
	    	     }
	    	   }	 
	   	//   }else{
	   	//	   throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","La linea se encuentra en estatus cotizacion");
	   	//    }
	     }else{
			if(lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() == COTIZANDO){
			    if (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().equals(new Double(0.0000000000))){
			    	if (this.getLineaSoporteReaseguroEndosoAnterior() != null && !this.getLineaSoporteReaseguroEndosoAnterior().getPorcentajeFacultativo().equals(new Double(0.0000000000))){
			    		return Integer.valueOf(REQUIERE_FACULTATIVO);	
			    	}			    	
			         return Integer.valueOf(SOPORTADO_POR_REASEGURO);	
			    }else{
			    	 return Integer.valueOf(REQUIERE_FACULTATIVO);
			    }
		    }else{ 
		    	throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","El estatus de la linea es: " + lineaSoporteReaseguroDTO.getEstatusLineaSoporte()+" Se esperaba un estatus 1");
		    }
	   }
 	}
	
	public Integer getEstatusFacultativo(List<LineaSoporteCoberturaDTO> listaDeCoberturas) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		SlipDN slipSN = SlipDN.getInstancia();
		SlipDTO slipDTO = null;
		if(lineaSoporteReaseguroDTO.getId() != null){
			slipDTO = slipSN.obtenerSlipLineaSoporte(lineaSoporteReaseguroDTO);
		}
	   	if(slipDTO != null ){ 
	   		if(slipDTO.getEstatusCotizacion().intValue() == 1){
	   			if (listaDeCoberturas != null){
	   				if(comparaMontosCoberturas(listaDeCoberturas)){
	   					return Integer.valueOf(FACULTATIVO_INTEGRADO);
	   				}else{
	   					return Integer.valueOf(FACULTATIVO_COTIZADO);
	   				}
	   			}else{
	   				return Integer.valueOf(3);//escribir en el log negocio las coberturas 
	   			}
	   		}else{
	   			if(slipDTO.getEstatusCotizacion().intValue() == 3){  
	   				return Integer.valueOf(FACULTATIVO_POR_CANCELAR); 
	   			}else{	 
	   				if (slipDTO.getEstatusCotizacion().intValue() == 2){
	   					return Integer.valueOf(FACULTATIVO_CONTRATADO); 
	   				}else{
	   					return Integer.valueOf(FACULTATIVO_SOLICITADO);
	   				}
	   			}
	   		}
	   	}else{
	   		if(lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue() == COTIZANDO){
	   			if (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().equals(new Double(0.0000000000))){
	   				if (this.getLineaSoporteReaseguroEndosoAnterior() != null && !this.getLineaSoporteReaseguroEndosoAnterior().getPorcentajeFacultativo().equals(new Double(0.0000000000))){
	   					return Integer.valueOf(REQUIERE_FACULTATIVO);	
	   				}
	   				return Integer.valueOf(SOPORTADO_POR_REASEGURO);	
	   			}else{
	   				return Integer.valueOf(REQUIERE_FACULTATIVO);
	   			}
	   		}else{ 
	   			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","El estatus de la linea es: " + lineaSoporteReaseguroDTO.getEstatusLineaSoporte()+" Se esperaba un estatus 1");
	   		}
	   	}
	}
	
	
	private boolean comparaMontosCoberturas(List<LineaSoporteCoberturaDTO> lineaSoporteCoberturaList){
		boolean coberturasIguales = false; 
	 	for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteCoberturaList){
			  if (!(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion().equals(lineaSoporteCoberturaDTO.getMontoPrimaFacultativo()))){
			       return false;  	  
			  } 
			  coberturasIguales = true;
		 }
		 return coberturasIguales; 
 	}
	

	/**
	 * Leyendas
	 * 0 = Soportado por Reaseguro
	 * Este estatus se determina cuando al validar el soporte de la linea resulta que
	 * % Facultato es 0.
	 * 
	 * 1 = Requiere Facultativo
	 * Este estatus se determina cuando al validar el soporte de la linea resulta que
	 * % Facultato es mayor a 0.
	 * 
	 * 2 = Facultativo Solicitado.
	 * Este se define cuando Suscripcion Solicita Facultar la linea.
	 * 3 = Facultativo Cotizado (Procede a Vender)
	 * Se define cuando Reaseguro Autoriza la Cotizaci�n.
	 * 4 = Facultativo Integrado (Es cuando se igualan las primas
	 * que Facultativo envio)
	 * Se define cuando Suscripcion iguala Primas en Cotizaci�n, este estatus es
	 * definido por Suscripci�n.
	 * 
	 * @param newVal
	 */
	public void setEstatusFacultativo(Integer estatusFacultativo){
 	   try {
 		   SoporteReaseguroSN soporteReaseguroSN = new SoporteReaseguroSN();
 		   if (estatusFacultativo.intValue() == 4){
			   if(this.getEstatusFacultativo().intValue() == 3){
	 			  List<LineaSoporteCoberturaDTO> listaDeCoberturas =  soporteReaseguroSN.obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro",lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
				  List<LineaSoporteCoberturaDTO> listaDeCoberturasCabioAxu = new ArrayList<LineaSoporteCoberturaDTO>();  
	 			  for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : listaDeCoberturas){
			           if(lineaSoporteCoberturaDTO.getMontoPrimaSuscripcion() != lineaSoporteCoberturaDTO.getMontoPrimaFacultativo()){
			        	    listaDeCoberturasCabioAxu.add(lineaSoporteCoberturaDTO);
			           }
				    }
	 			    for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : listaDeCoberturasCabioAxu){
	 				     lineaSoporteCoberturaDTO.setMontoPrimaSuscripcion(lineaSoporteCoberturaDTO.getMontoPrimaFacultativo()); 
	 				     soporteReaseguroSN.actualizarCoberturas(lineaSoporteCoberturaDTO);
	 			    }
	 			   lineaSoporteReaseguroDTO.setEstatusFacultativo(LineaSoporteReaseguroDN.ESTATUS_FACULTATIVO_INTEGRADO);
	 			   LineaSoporteReaseguroDN.getInstancia().actualizar(lineaSoporteReaseguroDTO);
	 		     }else{
				    throw new RuntimeException("El estatus de la linea es diferente de tres");
			     }
 		   }	
		} catch (ExcepcionDeAccesoADatos e) {
	 		e.printStackTrace();
		} catch (SystemException e) {
	 		e.printStackTrace();
		} catch (ExcepcionDeLogicaNegocio e) {
	 		e.printStackTrace();
		}
 	}

	/**
	 * Este metodo revisa si existe un contrato Facultativo y si este contrato
	 * requiere control de Reclamo.
	 * Nota: Si no hay aun Contrato Facultativo retorna nulo.
	 * @throws SystemException 
	 */
	public boolean isAplicaControlReclamo() throws SystemException{
	  	SoporteReaseguroSN soporteReaseguroSN = new SoporteReaseguroSN(); 
	  	if (lineaSoporteReaseguroDTO.getId() != null){
	 	  List<LineaSoporteCoberturaDTO> lineaSoporteCoberturasList = soporteReaseguroSN.obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro", lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
	  	   for(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteCoberturasList){
	  	 	     if(lineaSoporteCoberturaDTO.getAplicaControlReclamo() == true){
	  	 	    	 return true; 
	  	 	     }
	   	    }
	  	}   
 	 	return false;
	}

	public BigDecimal getIdSlip(){
		// TODO : Revisar si se requiere Facultativo y luego buscar el Slip 
		SlipDN slipSN = SlipDN.getInstancia();
		SlipDTO slipDTO = null;
		if(lineaSoporteReaseguroDTO.getId() != null){
 	  	     try {
				slipDTO = slipSN.obtenerSlipLineaSoporte(lineaSoporteReaseguroDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Problemas de Acceso de Datos al Buscar el Slip", Level.FINEST, e);
			} catch (SystemException e) {
				LogDeMidasWeb.log("Problemas al Buscar el Slip", Level.FINEST, e);
			}
		}
		
		if (slipDTO != null){
		return slipDTO.getIdToSlip();
		}
		return null;
	}

	public BigDecimal getNumeroInciso() {
	 	 	int numeroInciso = 0;
	 	    int bandera = 1;
	 	    if (lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs().size() == 0){
	 	    	//listaDeCoberturas = lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs();
	 	    	SoporteReaseguroSN soporteReaseguroSN;
	 	    	try {
	 	    		soporteReaseguroSN = new SoporteReaseguroSN();
	 	    	//	listaDeCoberturas = soporteReaseguroSN.obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro", this.lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro());
	 	    		this.setListadeCoberturas(soporteReaseguroSN.obtenerCoberturasPorPropiedad("id.idTmLineaSoporteReaseguro", this.lineaSoporteReaseguroDTO.getId().getIdTmLineaSoporteReaseguro()));
	 	    	} catch (SystemException e) {
	 	    		e.printStackTrace();
	 	    	}

	 	    }
	 	    for (LineaSoporteCoberturaDTO LineaSoporteCoberturaDTO : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() ) {
	 	    	if (bandera == 1 || numeroInciso == LineaSoporteCoberturaDTO.getNumeroInciso().intValue()){
	 	    		numeroInciso = LineaSoporteCoberturaDTO.getNumeroInciso().intValue();
	 	    		bandera = 0;
	 	    	}else{
	 	    		bandera = 1;
	 	    		break;
	 	    	}
	 	    }
	 	    
		 		if (bandera == 0) {
					return new BigDecimal(numeroInciso);
				} else {
					return new BigDecimal(0);	
				}
		}

 	public BigDecimal getNumeroSubInciso() {
		int numeroSubInciso = 0;
 	    int bandera = 1;
			for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() ) {
				
 				if (lineaSoporteCoberturaDTO.getNumeroSubInciso() != null && (bandera == 1 || numeroSubInciso == lineaSoporteCoberturaDTO.getNumeroSubInciso().intValue())){
					numeroSubInciso = lineaSoporteCoberturaDTO.getNumeroSubInciso().intValue();
					bandera = 0;
		 		}else{
		 			bandera = 1;
		 			break;
		 		}
	 		}
	 		if (bandera == 0) {
				return new BigDecimal(numeroSubInciso);
			} else {
				return new BigDecimal(0);	
			}
	 }
 /**	
 	public BigDecimal getIdToSeccion() {
		BigDecimal idToSeccion = null;
 	    int bandera = 1;
			for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : lineaSoporteReaseguroDTO.getLineaSoporteCoberturaDTOs() ) {
				
 				if (lineaSoporteCoberturaDTO.getIdToSeccion() != null && (bandera == 1 || idToSeccion.equals(lineaSoporteCoberturaDTO.getIdToSeccion()) ) ){
 					idToSeccion = lineaSoporteCoberturaDTO.getIdToSeccion();
					bandera = 0;
		 		}else{
		 			bandera = 1;
		 			break;
		 		}
	 		}
	 		if (bandera == 0) {
				return idToSeccion;
			} else {
				return null;	
			}
	 } 	
 **/	

	public void setLineaSoporteReaseguroDTO(LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO) {
		this.lineaSoporteReaseguroDTO = lineaSoporteReaseguroDTO;
  	}

	public LineaSoporteReaseguroDTO getLineaSoporteReaseguroDTO() {
		return this.lineaSoporteReaseguroDTO;
	}

	/**
	 * Este metodo es para el proceso autom�tico de Distribuci�n de Primas UNICAMENTE
	 * @param estatusLineaSoporte
	 */
	public void setEstatusLineaSoporte(int estatusLineaSoporte){
		this.lineaSoporteReaseguroDTO.setEstatusLineaSoporte(estatusLineaSoporte);
	}

	public int getEstatusLineaSoporte(){
		return this.lineaSoporteReaseguroDTO.getEstatusLineaSoporte().intValue();
	}

	public int getTipoDistribucion(){
		int tipoDistribucion = 0;
		if (this.lineaSoporteReaseguroDTO.getLineaDTO() != null) {
			tipoDistribucion = this.lineaSoporteReaseguroDTO.getLineaDTO().getTipoDistribucion().intValue();
		}
		return tipoDistribucion;	
	}


	private boolean esSoporteCotizacion(){
		if (this.getEstatusLineaSoporte() == 1){
			return true;
		}
		return false;
	}

	private LineaDTO getLineaDTO(){
		return this.getLineaSoporteReaseguroDTO().getLineaDTO();
	}
	
	public void setLineaDTO(LineaDTO lineaDTO) {
		this.getLineaSoporteReaseguroDTO().setLineaDTO(lineaDTO);
	}
	

	public BigDecimal getMontoSumaAseguradaUSD(){
		BigDecimal montoSumaAsegurada = BigDecimal.valueOf(this.getMontoSumaAsegurada()).setScale(2,RoundingMode.HALF_UP);
		BigDecimal montoTipoCambio = BigDecimal.valueOf(this.getTipoCambio()).setScale(2,RoundingMode.HALF_UP);
		BigDecimal montoSumaAseguradaUSD = montoSumaAsegurada;
		
		if (this.getIdMonedaMonto().equals(new BigDecimal(Sistema.MONEDA_DOLARES))){
		return montoSumaAseguradaUSD;
		} else {
			montoSumaAseguradaUSD = montoSumaAsegurada.divide(montoTipoCambio,RoundingMode.HALF_UP);
		return 	montoSumaAseguradaUSD;
		}
	}
	
	private BigDecimal getMontoUSD(BigDecimal montoSegunMoneda){
		BigDecimal montoTipoCambio = BigDecimal.valueOf(this.getTipoCambio()).setScale(2,RoundingMode.HALF_UP);
		BigDecimal montoUSD = montoSegunMoneda;

		if (this.getIdMonedaMonto().equals(new BigDecimal(Sistema.MONEDA_DOLARES))){
			return montoUSD;
			} else {
				montoUSD = montoSegunMoneda.divide(montoTipoCambio,RoundingMode.HALF_UP);
			return 	montoUSD;
			}		
	}

	/**
	 * Para calcular el Soporte de Reaseguro primero tendra que validar si la linea tiene todos los datos necesarios para 
	 * ello.
	 * Condiciones:
	 * 1.- Debe tener una LineaDTO autorizada
	 * 2.- Debe tener Suma Asegurada mayor de 0
	 * 3.- Debe tener Porcentaje de Pleno mayor de %0 hasta 100%.
	 * @throws ExcepcionDeLogicaNegocio 
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 *  
	 */
	public void calculaSoporteReaseguro() throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{

		int estatusFacultativo = this.getEstatusFacultativo();
		if (this.getLineaDTO() != null && esSoporteCotizacion()){
			if (this.getLineaDTO().getEstatus().intValue() == new BigDecimal(1).intValue() &&
					this.getPorcentajePleno() >= 0.0 && 
					this.getMontoSumaAsegurada() >= 0.0 &&
					(estatusFacultativo == SOPORTADO_POR_REASEGURO || estatusFacultativo == REQUIERE_FACULTATIVO)
			){
				defineSoporteReaseguro(null,null);
			}
		}
		this.esCorrectoRepartoSoporte();
	}
	
	public void reCalculaSoporteReaseguroPorMontoParaFacultativo(BigDecimal montoNuevoParaFacultar) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		if (this.getLineaDTO() != null && esSoporteCotizacion()){
			if (this.getLineaDTO().getEstatus().intValue() == new BigDecimal(1).intValue() &&
					this.getPorcentajePleno() >= 0.0 && 
					this.getMontoSumaAsegurada() >= 0.0 &&
					(this.getEstatusFacultativo() == FACULTATIVO_SOLICITADO)
			){		
					BigDecimal montoNuevoParaFacultarUSD = getMontoUSD(montoNuevoParaFacultar);
					this.defineSoporteReaseguro(montoNuevoParaFacultarUSD, null);
			}else{
				throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede definir un nuevo Monto a Facultar ya que no se encuentra en Proceso de Negociaci�n ");
			}

		}
		this.esCorrectoRepartoSoporte();
	}
	
	public void reCalculaSoporteReaseguroPorPorcentajeParaFacultativo(BigDecimal porcentajeNuevoParaFacultar) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		if (this.getLineaDTO() != null && esSoporteCotizacion()){
			if (this.getLineaDTO().getEstatus().intValue() == new BigDecimal(1).intValue() &&
					this.getPorcentajePleno() >= 0.0 && 
					this.getMontoSumaAsegurada() >= 0.0 &&
					(this.getEstatusFacultativo() == FACULTATIVO_SOLICITADO)
			){		
				this.defineSoporteReaseguro(null, porcentajeNuevoParaFacultar);
			}else{
				throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede definir un nuevo Porcentaje a Facultar ya que no se encuentra en Proceso de Negociaci�n ");
			}
		}
		this.esCorrectoRepartoSoporte();
	}
	
	private void defineSoporteReaseguro(BigDecimal montoNuevoParaFacultarUSD, BigDecimal porcentajeNuevoParaFacultar) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
				
				int escala = 12;
				
				BigDecimal cero = new BigDecimal(0).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal maximo  = new BigDecimal(this.getLineaDTO().getMaximo());
				BigDecimal porcentajeCesionCP = cero;
				BigDecimal porcentajeRetencion = cero;
				BigDecimal plenosPrimerExcedente = cero;
				BigDecimal montoPlenoPrimerExcedente = cero;
				BigDecimal capacidadPrimerExcedente  = cero;
				BigDecimal cien = new BigDecimal(100.00).setScale(escala,RoundingMode.HALF_UP);
				BigDecimal porcentajePleno = new BigDecimal(this.getPorcentajePleno()).setScale(escala,RoundingMode.HALF_UP);

				
				BigDecimal porcentajeACesionCP = cero;
				BigDecimal porcentajeARetencion = cero;				
				BigDecimal porcentajePrimerExcedente = cero;
				BigDecimal porcentajeFacultativo = cero;
				BigDecimal diferienciaPorcentual = cero;
				
				
				// Paso 1 .- Obtiene datos de la Linea de Reaseguro
				if (this.getLineaDTO().getContratoCuotaParte() != null){
					porcentajeCesionCP = new BigDecimal(this.getLineaDTO().getContratoCuotaParte().getPorcentajeCesion()).setScale(escala,RoundingMode.HALF_UP);;
					porcentajeRetencion = new BigDecimal(this.getLineaDTO().getContratoCuotaParte().getPorcentajeRetencion()).setScale(escala,RoundingMode.HALF_UP);;
				}else{
					porcentajeRetencion = cien;
				}

				if (this.getLineaDTO().getContratoPrimerExcedente() != null ) {
					plenosPrimerExcedente = this.getLineaDTO().getContratoPrimerExcedente().getNumeroPlenos();
					montoPlenoPrimerExcedente = new BigDecimal(this.getLineaDTO().getContratoPrimerExcedente().getMontoPleno()).setScale(escala,RoundingMode.HALF_UP);;
					capacidadPrimerExcedente = montoPlenoPrimerExcedente.multiply(plenosPrimerExcedente);
				} 
				//Paso 1.- Redefine la capacidad de la linea basado en la estimaci�n del pleno de suscripci�n.
				maximo = (maximo.multiply(porcentajePleno)).divide(cien,RoundingMode.HALF_UP);
				capacidadPrimerExcedente = (capacidadPrimerExcedente.multiply(porcentajePleno)).divide(cien,RoundingMode.HALF_UP);

				BigDecimal capacidadMaximaLinea = maximo.add(capacidadPrimerExcedente);

				//Paso 3.- Obtener participacion Facultativa
				BigDecimal montoParaFacultar = cero;
			    BigDecimal montoSumaAseguradaUSD = this.getMontoSumaAseguradaUSD(); 
				
				if( montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 ) {
					montoParaFacultar = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea); 
					//this.setPorcentajeFacultativo((montoParaFacultar.divide(montoSumaAseguradaUSD,RoundingMode.HALF_DOWN)).multiply(cien).doubleValue());
					porcentajeFacultativo = (montoParaFacultar.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien);
				} 

			    //Paso 3a.- Si se requiere facultar una suma � porcentaje en especifico
                if (montoNuevoParaFacultarUSD != null && montoNuevoParaFacultarUSD.compareTo(cero) > 0){
                	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
                	if (montoNuevoParaFacultarUSD.compareTo(montoParaFacultar) >= 0){
                		//y menor o igual a la suma asegurada total
                		if (montoNuevoParaFacultarUSD.compareTo(montoSumaAseguradaUSD) <= 0){
                			montoParaFacultar = montoNuevoParaFacultarUSD;
                			porcentajeFacultativo = (montoNuevoParaFacultarUSD.divide(montoSumaAseguradaUSD,escala,RoundingMode.HALF_UP)).multiply(cien);		
                		}else{
                			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Mayor <br>a la Cotizada: <br>USD$" + montoSumaAseguradaUSD.setScale(2, RoundingMode.UP) + "<br> MX$" + montoSumaAseguradaUSD.multiply(new BigDecimal(this.getTipoCambio())).setScale(2, RoundingMode.UP));
                		}
                	}else{
                		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una Suma Menor <br>a la Originalmente Requerida <br>por superaci�n de contratos : <br>USD$" + montoParaFacultar.setScale(2, RoundingMode.UP) + "<br> MX$" + montoParaFacultar.multiply(new BigDecimal(this.getTipoCambio())).setScale(2, RoundingMode.UP));
                	}
                } else if (porcentajeNuevoParaFacultar != null && porcentajeNuevoParaFacultar.compareTo(cero) > 0){
                	//debe ser mayor 0 igual a lo originalmente calculado a facultar, de lo contrario no procede.
                	if (porcentajeNuevoParaFacultar.compareTo(porcentajeFacultativo) >= 0){
                		//y menor o igual al 100%  de la suma asegurada
                		if (porcentajeNuevoParaFacultar.compareTo(cien) <= 0){
                			montoParaFacultar = montoSumaAseguradaUSD.multiply(porcentajeNuevoParaFacultar).divide(cien,RoundingMode.HALF_UP);
                			porcentajeFacultativo = porcentajeNuevoParaFacultar;		
                		}else{
                			throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","No puede Facultar una porcentaje mayor "+ porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP) +"%  al " + cien +"%");
                		}
                	}else{
                		throw new ExcepcionDeLogicaNegocio("LineaSoporteReaseguro","Este porcentaje "+porcentajeNuevoParaFacultar.setScale(4, RoundingMode.HALF_UP)+"% no es mayor <br> al necesario requerido por superaci�n de la Linea Reaseguro <br> De : " + porcentajeFacultativo.setScale(4, RoundingMode.UP) + "%");
                	}                	
                }
				
				// Paso 3b.- Ajuste de capacidades basado en lo que se faculta.
                if (montoSumaAseguradaUSD.compareTo(capacidadMaximaLinea) > 0 || porcentajeFacultativo.compareTo(cero) > 0){
                	BigDecimal excedente = montoSumaAseguradaUSD.subtract(capacidadMaximaLinea);
                	if ((montoParaFacultar.subtract(excedente)).compareTo(capacidadPrimerExcedente) >= 0){
                	 maximo = maximo.subtract((montoParaFacultar.subtract(excedente)).subtract(capacidadPrimerExcedente));
                	}
                }
				
				
				// Paso 4.- Calcula la Suma Asegurada a Cuota Parte, Retenci�n y la de Primer Excedente
				BigDecimal montoParaMaximo = cero;
				BigDecimal montoParaPrimerExcedente = new BigDecimal(0.00).setScale(escala,RoundingMode.HALF_UP);
            	
				if (( montoSumaAseguradaUSD.subtract(maximo)).compareTo(cero) > 0){
					montoParaMaximo = maximo;
					montoParaPrimerExcedente = montoSumaAseguradaUSD.subtract(maximo).subtract(montoParaFacultar);
				} else {
					montoParaMaximo = montoSumaAseguradaUSD;
				}

				BigDecimal montoParaCuotaParte = cero;
				// Paso 5.- Obtener participacion de Cuota Parte
				if ( porcentajeCesionCP.compareTo(cero) > 0 && ( montoParaMaximo.compareTo(cero) > 0 ) ) {
				montoParaCuotaParte = ((montoParaMaximo.multiply(porcentajeCesionCP)).divide(cien));
				//this.setPorcentajeCuotaParte((montoParaCuotaParte.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
				porcentajeACesionCP = (montoParaCuotaParte.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien);
				} 

				BigDecimal montoParaRetencion = cero;
				// Paso 6.- Obtener participacion de Retencion
				if ( porcentajeRetencion.compareTo(cero) > 0 && ( montoParaMaximo.compareTo(cero) > 0 ) ) {
				montoParaRetencion = ((montoParaMaximo.multiply(porcentajeRetencion)).divide(cien));
				//this.setPorcentajeRetencion((montoParaRetencion.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
				porcentajeARetencion = (montoParaRetencion.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien);
				}
 				// Paso 7.- Obtener participacion de Primer Excedente
				if (montoParaPrimerExcedente.compareTo(cero) > 0){
					//this.setPorcentajePrimerExcedente((montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien).doubleValue());
					porcentajePrimerExcedente = (montoParaPrimerExcedente.divide(montoSumaAseguradaUSD,RoundingMode.HALF_UP)).multiply(cien);
				}
				
				
				
				//Proceso para Corregir desviaciones sobre la distribucion al 100%
				if (!porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo).equals(cien)){
					
					diferienciaPorcentual = cien.subtract(porcentajeARetencion.add(porcentajeACesionCP).add(porcentajePrimerExcedente).add(porcentajeFacultativo));
					
					if (porcentajeFacultativo.compareTo(cero) > 0){
						porcentajeFacultativo = porcentajeFacultativo.add(diferienciaPorcentual);
					} else if (porcentajePrimerExcedente.compareTo(cero) > 0) {
						porcentajePrimerExcedente = porcentajePrimerExcedente.add(diferienciaPorcentual);
					}
				}
				this.setPorcentajeRetencion(porcentajeARetencion);
				this.setPorcentajeCuotaParte(porcentajeACesionCP);
				this.setPorcentajePrimerExcedente(porcentajePrimerExcedente);
				this.setPorcentajeFacultativo(porcentajeFacultativo);
		
	}
	
	
	
	/*
	 * Este metodo te retorna true cuando la reparticion de la linea de Soporte esta al 100% sobre la suma asegurada
	 */
	public boolean esCorrectoRepartoSoporte(){
		
		BigDecimal porcentajeRiesgoCubierto = this.getPorcentajeRetencion().add(getPorcentajeCuotaParte()).add(getPorcentajePrimerExcedente()).add(getPorcentajeFacultativo());
		
		if (porcentajeRiesgoCubierto.compareTo(new BigDecimal(100d)) == 0){

			return true;
		} else {
			LogDeMidasWeb.log("La Reparticion sobre Linea de Reaseguro y Contratos es Incorrecta:" +
					" %Ret["+ this.getPorcentajeRetencion() +"] %CP["+ this.getPorcentajeCuotaParte() +"] %1E["+ this.getPorcentajePrimerExcedente() +"] %F["+ this.getPorcentajeFacultativo() +"] Total["+ porcentajeRiesgoCubierto +"]" +
							"Para la Linea:" + this.getLineaDTO().getSubRamo().getDescription() +" TipoDistribucion:" + this.getTipoDistribucion() , Level.FINEST, null);
		}
		
		return false;
	}

	
	public Double getMontoPrimaContratos(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO){
		Double montoPrimaRetencion = lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() * this.getPorcentajeRetencion().doubleValue()/100;
		Double montoPrimaCP = lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() * this.getPorcentajeCuotaParte().doubleValue()/100;
		Double montoPrima1E = lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() * this.getPorcentajePrimerExcedente().doubleValue()/100;
		//Double montoPrimaFacultativa = lineaSoporteReaseguroCoberturaDTO.getMontoPrimaSuscripcion().doubleValue() * this.getPorcentajeFacultativo();
		Double montoPrimaContratos = montoPrimaRetencion + montoPrimaCP + montoPrima1E;
		
		return montoPrimaContratos;
	}
	
	public Double getMontoPrimaFacultada(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO){
		return lineaSoporteCoberturaDTO.getMontoPrimaFacultativo().doubleValue() * this.getPorcentajeFacultativo().doubleValue()/100;
	}
	public void setLineaSoporteReaseguroEndosoAnterior(
			LineaSoporteReaseguro lineaSoporteReaseguroEndosoAnterior) {
		this.lineaSoporteReaseguroEndosoAnterior = lineaSoporteReaseguroEndosoAnterior;
		// Como la raz�n de conversi�n se mantiene de la primer Emisi�n, se hereda el tipo de cambio del endoso anterior.
		if(lineaSoporteReaseguroEndosoAnterior != null && lineaSoporteReaseguroEndosoAnterior.getTipoCambio() != null){
			this.setTipoCambio(lineaSoporteReaseguroEndosoAnterior.getTipoCambio());
		}
		
		
	}
	public LineaSoporteReaseguro getLineaSoporteReaseguroEndosoAnterior() {
		return lineaSoporteReaseguroEndosoAnterior;
	}
	
	/**
	 * Este metodo es para saber si sobre este cumulo(LineaSoporteCobertura) se debe aplicar un Control de Reclamo indicado por Reaseguro
	 * normalmente cuando lleva un Contrato Facultativo.
	 * 
	 * NOTA: Este Objeto debera trabajar con DTOs con relaci�n EAGER en capa de Persistencia
	 * 
	 * @return true (verdadero) Cuando alguna de las coberturas del c�lulo(LineaSoporteCobertura) ha sido marcada para llevar Control de Reclamo
	 */
	public boolean getAplicaControlDeReclamo() {
	boolean respuesta = false; 
		
	   for (LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO : this.getListaDeCoberturas()){
		   
		   if (lineaSoporteCoberturaDTO.getAplicaControlReclamo()){
			   respuesta = true;
			   break;
		   }
	   }
	return respuesta;
	}
	
}