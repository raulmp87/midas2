package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.contratofacultativo.DetalleContratoFacultativoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class LineaSoporteCoberturaSN {
	LineaSoporteCoberturaFacadeRemote beanRemoto;
	
	public LineaSoporteCoberturaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(
					LineaSoporteCoberturaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public LineaSoporteCoberturaDTO agregar(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO) {
		try{
			return beanRemoto.save(lineaSoporteCoberturaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void modificar(LineaSoporteCoberturaDTO lineaSoporteCoberturaDTO){
		beanRemoto.update(lineaSoporteCoberturaDTO);
	}
	
	public LineaSoporteCoberturaDTO getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(DetalleContratoFacultativoDTO detalleContratoFacultativoDTO){
		return beanRemoto.getLineaSoporteCoberturaDTOByDetalleContratoFacultativoDTO(detalleContratoFacultativoDTO);
	}
	
	public List<LineaSoporteCoberturaDTO> getPorPropiedad(String propiedad, Object valor){
		return beanRemoto.findByProperty(propiedad, valor);
	}
	
	public void eliminarLineasSoporteCoberturaPorLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro){
		beanRemoto.eliminarLineasSoporteCoberturaPorLineaSoporteReaseguro(idTmLineaSoporteReaseguro);
	}
	
	public void notificarEmisionCoberturas(BigDecimal idToSoporteReaseguro, BigDecimal idToPoliza){
		beanRemoto.notificarEmisionCoberturasSoporteReaseguro(idToSoporteReaseguro, idToPoliza);
	}
	
	public void integrarCoberturasSoporteReaseguro (BigDecimal idTmLineaSoporteReaseguro){
		beanRemoto.integrarCoberturasSoporteReaseguro(idTmLineaSoporteReaseguro);
	}
}
