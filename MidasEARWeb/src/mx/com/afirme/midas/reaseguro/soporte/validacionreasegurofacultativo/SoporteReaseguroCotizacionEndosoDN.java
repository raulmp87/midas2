package mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;

public class SoporteReaseguroCotizacionEndosoDN extends SoporteReaseguroBase{
	private SoporteReaseguroDTO soporteReaseguroAnteriorDTO;
	
	public SoporteReaseguroCotizacionEndosoDN(BigDecimal idToPoliza,Integer numeroEndosoAnterior,BigDecimal idToCotizacion) throws SystemException{
		this(idToPoliza,numeroEndosoAnterior,idToCotizacion,true);
	}
	
	public SoporteReaseguroCotizacionEndosoDN(BigDecimal idToPoliza,Integer numeroEndosoAnterior,BigDecimal idToCotizacion,boolean generaNuevoSoporte) throws SystemException{
		if(idToPoliza == null || numeroEndosoAnterior == null || idToCotizacion == null)
			throw new SystemException("Parametros inv�lidos: idToPoliza:"+idToPoliza+", numeroEndosoAnterior: "+numeroEndosoAnterior+", idToCotizacion: "+idToCotizacion);
		this.soporteReaseguroAnteriorDTO = SoporteReaseguroDN.getInstancia().obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndosoAnterior);
		if(soporteReaseguroAnteriorDTO == null)
			throw new SystemException("No existe Soporte Reaseguro para la p�liza: "+idToPoliza+", endoso: "+numeroEndosoAnterior);
		this.idToCotizacion = idToCotizacion;
		List<SoporteReaseguroDTO> listaSoporteReaseguroPorCotizacion = SoporteReaseguroDN.getInstancia().listarPorIdToCotizacion(idToCotizacion);
		if ( listaSoporteReaseguroPorCotizacion != null && !listaSoporteReaseguroPorCotizacion.isEmpty() ){
			soporteReaseguroDTO = listaSoporteReaseguroPorCotizacion.get(0);
		}
		else if(generaNuevoSoporte){
			soporteReaseguroDTO = new SoporteReaseguroDTO();
		    soporteReaseguroDTO.setIdToCotizacion(idToCotizacion);
		    soporteReaseguroDTO.setFechaCreacion(new Date());
 		    soporteReaseguroDTO.setFechaModificacion(new Date());
 		    soporteReaseguroDTO.setNumeroEndoso(new Integer(0));
 		    soporteReaseguroDTO.setIdToSoporteReaseguroEndosoAnterior(soporteReaseguroAnteriorDTO.getIdToSoporteReaseguro());
 		    if(soporteReaseguroAnteriorDTO != null)
 		    	soporteReaseguroDTO.setNumeroEndoso(soporteReaseguroAnteriorDTO.getNumeroEndoso()+1);
		    soporteReaseguroDTO = SoporteReaseguroDN.getInstancia().agregar(soporteReaseguroDTO);
		    soporteReaseguroReciente = true;
		} else{
			throw new SystemException("No existe soporte para la cotizacion "+idToCotizacion, 20);
		}
	}
	
	public void preparaCargaCumulos() throws SystemException{
		LineaSoporteReaseguroDN.getInstancia().eliminarLineaSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDN.ESTATUS_SOPORTADO_POR_REASEGURO);
		LineaSoporteReaseguroDN.getInstancia().eliminarLineaSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDN.ESTATUS_REQUIERE_FACULTATIVO);
		//Se eliminan las lineas cuyo estatus sea "soportado por reaseguro" y "requiere facultativo"
		LineaSoporteReaseguroDN.getInstancia().eliminarLineaSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro(), LineaSoporteReaseguroDN.ESTATUS_LINEA_ENDOSO_FACULTATIVO);
		listaLineasPendientes = LineaSoporteReaseguroDN.getInstancia().listarLineaSoporteReaseguroPorSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro());
	}
	
	public LineaDTO obtenerLineaPorSubRamo(Date fechaInicioVigencia, BigDecimal idTcSubramo) throws SystemException{
		//Se busca la lineaDTO en las LineaSoporteReaseguroDTO del soporte anterior
		LineaDTO lineaDTO = null;
		if(soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs() == null || soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs().isEmpty()){
			consultarInformacionSoporteReaseguroAnterior();
		}
		for(LineaSoporteReaseguroDTO lineaSoporteReaseguroAnterior : soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs()){
			if(lineaSoporteReaseguroAnterior.getLineaDTO().getSubRamo().getIdTcSubRamo().compareTo(idTcSubramo) == 0){
				lineaDTO = lineaSoporteReaseguroAnterior.getLineaDTO();
				break;
			}
		}
		if (lineaDTO == null){
			lineaDTO = SoporteReaseguroDN.getInstancia().obtenerLineaDTO(fechaInicioVigencia,idTcSubramo);
		}
		if(lineaDTO == null){
			throw new SystemException("No se encontr� Linea para los datos recibidos: fechaInicioVigencia = "+fechaInicioVigencia+", idTcSubRamo: "+idTcSubramo);
		}
		return lineaDTO;
	}
	
	public void insertarCumulosLinea(List<CumuloPoliza> listaCumuloPoliza,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		if(listaCumuloPoliza != null && !listaCumuloPoliza.isEmpty()){
			if(soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs() == null || soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs().isEmpty()){
				consultarInformacionSoporteReaseguroAnterior();
			}
			//Se obtiene el tipo de cambio del soporte anterior
			if(tipoCambio == null && !soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs().isEmpty()){
//				tipoCambio = soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs().get(0).getTipoCambio();
				/*
				 * 06/08/2010. Jos� Luis Arellano. Se debe consultar el tipo de cambio del mes actual para el soporte del endoso, en vez de mantenerlo historicamente.
				 */
				tipoCambio  =  TipoCambioDN.getInstancia(nombreUsuario).obtieneTipoCambioPorDia(new Date(), Integer.valueOf(Sistema.MONEDA_DOLARES).shortValue());
			}
			if(mapaLineasPorDistribucion == null)
				mapaLineasPorDistribucion = new HashMap<BigDecimal, List<LineaSoporteReaseguroDTO>>();
			if(tipoCambio == null)
				throw new SystemException("No se pudo recuperar el tipo de cambio del mes actual");
		}
		List<LineaSoporteReaseguroDTO> listaNuevasLineasSoporte = new ArrayList<LineaSoporteReaseguroDTO>();
		for(CumuloPoliza cumuloPoliza: listaCumuloPoliza){
			//verificar si existe la lineaSoporteReaseguro correspondiente al c�mulo, si existe, se deja intacta.
			LineaSoporteReaseguroDTO nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguroPorCumulo(cumuloPoliza.getLineaDTO(), cumuloPoliza.getNumeroInciso(), cumuloPoliza.getNumeroSubInciso(),listaLineasPendientes);
			if(nuevaLineaSoporte == null){
				/**
				 * 20/05/2010
				 * Si la linea no existe, se busca en las lineas del soporte anterior, para validar el cambio en SA
				 */
				LineaSoporteReaseguroDTO lineaCorrespondienteSoporteAnterior = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguroPorCumulo(cumuloPoliza.getLineaDTO(), cumuloPoliza.getNumeroInciso(), cumuloPoliza.getNumeroSubInciso(), soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs());
				Short numeroEndoso = soporteReaseguroDTO.getNumeroEndoso() != null? soporteReaseguroDTO.getNumeroEndoso().shortValue() : (short)0;
				if(lineaCorrespondienteSoporteAnterior != null){
					BigDecimal cumuloPolizaSA = new BigDecimal(cumuloPoliza.getSumaAsegurada());
					BigDecimal lineaSoporteSA = lineaCorrespondienteSoporteAnterior.getMontoSumaAsegurada();
					if (cumuloPolizaSA.compareTo(lineaSoporteSA) > 0){
						nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguro(cumuloPoliza,lineaCorrespondienteSoporteAnterior, tipoCambio,soporteReaseguroDTO.getIdToSoporteReaseguro(),numeroEndoso,true,true);
					}else{
						nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguro(cumuloPoliza,lineaCorrespondienteSoporteAnterior, tipoCambio,soporteReaseguroDTO.getIdToSoporteReaseguro(),numeroEndoso,true,false);
					}
				}else{
					nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguro(cumuloPoliza,null, tipoCambio,soporteReaseguroDTO.getIdToSoporteReaseguro(),numeroEndoso,true,true);
				}
				listaNuevasLineasSoporte.add(nuevaLineaSoporte);
			}
		}
		if(!listaNuevasLineasSoporte.isEmpty()){
			LineaSoporteReaseguroDN.getInstancia().registrarLineasSoporteReaseguro(listaNuevasLineasSoporte);
		}
		listaNuevasLineasSoporte = null;
		Runtime.getRuntime().gc();
		listaNuevasLineasSoporte = LineaSoporteReaseguroDN.getInstancia().listarLineaSoporteReaseguroPorSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro());
		for(LineaSoporteReaseguroDTO lineaEnCurso:listaNuevasLineasSoporte){
			if(mapaLineasPorDistribucion.get(lineaEnCurso.getLineaDTO().getTipoDistribucion()) == null){
				mapaLineasPorDistribucion.put(lineaEnCurso.getLineaDTO().getTipoDistribucion(), new ArrayList<LineaSoporteReaseguroDTO>());
			}
			mapaLineasPorDistribucion.get(lineaEnCurso.getLineaDTO().getTipoDistribucion()).add(lineaEnCurso);
		}
	}
	
	public void copiarPorcentajesDistribucionEndosoCancelado(BigDecimal idtoPoliza,Integer numeroEndosoOriginal) throws SystemException{
		//Consultar el soporte del endoso original.
		SoporteReaseguroDTO soporteEndosoOriginal = SoporteReaseguroDN.getInstancia().obtenerSoporteReaseguroPorPolizaEndoso(
				idtoPoliza, numeroEndosoOriginal);
		
		soporteEndosoOriginal = SoporteReaseguroDN.getInstancia().consultarLineasSoporte(soporteEndosoOriginal);
		consultarInformacionSoporteReaseguroAnterior();
		LineaSoporteReaseguroDTO lineaSoporteActual = null;
		LineaSoporteReaseguroDTO lineaSoporteAnterior = null;
		
		//Iterar las lineas del soporte anterior, aplicar los porcentajes de cada l�nea a las del soporte actual
		for(LineaSoporteReaseguroDTO lineaSoporteOriginal : soporteEndosoOriginal.getLineaSoporteReaseguroDTOs()){
			lineaSoporteActual = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguroPorCumulo(
					lineaSoporteOriginal.getLineaDTO(),
					LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteOriginal),
					LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporteOriginal),
					soporteReaseguroDTO.getLineaSoporteReaseguroDTOs());
			
			lineaSoporteAnterior = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguroPorCumulo(
					lineaSoporteOriginal.getLineaDTO(),
					LineaSoporteReaseguroDN.getInstancia().obtenerNumeroInciso(lineaSoporteOriginal),
					LineaSoporteReaseguroDN.getInstancia().obtenerNumeroSubInciso(lineaSoporteOriginal),
					soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs());
			
			if(lineaSoporteOriginal.getAplicaDistribucionPrima() == null ||
					lineaSoporteOriginal.getAplicaDistribucionPrima().intValue() == LineaSoporteReaseguroDTO.APLICA_DISTRIBUCION_PRIMA){
				
				lineaSoporteActual = LineaSoporteReaseguroDN.getInstancia().asignarPorcentajesCancelacionEndoso(
						lineaSoporteActual, lineaSoporteAnterior, lineaSoporteOriginal);
				
				LineaSoporteReaseguroDN.getInstancia().actualizar(lineaSoporteActual);
			}
			
			if(lineaSoporteActual.getEstatusFacultativo().intValue() != LineaSoporteReaseguroDTO.ESTATUS_SOPORTADO_POR_REASEGURO){
				//si la linea es facultativa, copiar el contrato anterior
				LineaSoporteReaseguroDN.getInstancia().copiarContratoFacultativo(
						lineaSoporteActual, lineaSoporteOriginal,lineaSoporteActual.getNumeroEndoso().intValue());
			}
		}
	}

	private void consultarInformacionSoporteReaseguroAnterior() throws SystemException{
		if(soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs() == null || soporteReaseguroAnteriorDTO.getLineaSoporteReaseguroDTOs().isEmpty()){
			soporteReaseguroAnteriorDTO.setLineaSoporteReaseguroDTOs(LineaSoporteReaseguroDN.getInstancia().listarLineaSoporteReaseguroPorSoporte(soporteReaseguroAnteriorDTO.getIdToSoporteReaseguro()));
			for(LineaSoporteReaseguroDTO lineaSoporteReaseguro : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
				if(lineaSoporteReaseguro.getLineaSoporteCoberturaDTOs() == null || lineaSoporteReaseguro.getLineaSoporteCoberturaDTOs().isEmpty()){
					lineaSoporteReaseguro.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lineaSoporteReaseguro.getId().getIdTmLineaSoporteReaseguro()));
				}
			}
		}
	}
}