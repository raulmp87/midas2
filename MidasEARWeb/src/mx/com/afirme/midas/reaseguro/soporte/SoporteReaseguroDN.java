package mx.com.afirme.midas.reaseguro.soporte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.contratos.linea.LineaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SoporteReaseguroDN {
	private static final SoporteReaseguroDN INSTANCIA = new SoporteReaseguroDN();
	
	private SoporteReaseguroDN(){}
	
	public static SoporteReaseguroDN getInstancia(){
		return INSTANCIA;
	}
	
	public SoporteReaseguroDTO actualizar(SoporteReaseguroDTO soporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return new SoporteReaseguroSN(false, false, false).actualizar(soporteReaseguroDTO);
	}
	
	public List<SoporteReaseguroDTO> buscarPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		return new SoporteReaseguroSN().getPorPropiedad(propiedad, valor);
	}
	
	public List<SoporteReaseguroDTO> listarPorIdToCotizacion(BigDecimal idToCotizacion) throws ExcepcionDeAccesoADatos, SystemException{
		return new SoporteReaseguroSN().listarPorIdToCotizacion(idToCotizacion);
	}

	public SoporteReaseguroDTO agregar(SoporteReaseguroDTO soporteReaseguroDTO) throws ExcepcionDeAccesoADatos, SystemException{
    	return new SoporteReaseguroSN().agregar(soporteReaseguroDTO);
    }
	
	public LineaDTO obtenerLineaDTO(Date fechaInicioVigencia, BigDecimal idSubramo) throws ExcepcionDeAccesoADatos, SystemException {
    	return new SoporteReaseguroSN().obtenerLineaDTO(fechaInicioVigencia, idSubramo);
	}
	
	public LineaDTO obtenerLineaDTOVigente(Date fechaInicioVigencia, BigDecimal idSubramo) throws ExcepcionDeAccesoADatos, SystemException {
    	return new SoporteReaseguroSN().obtenerLineaDTOVigente(fechaInicioVigencia, idSubramo);
	}
    
    public SoporteReaseguroDTO obtenerSoporteReaseguroPorPolizaEndoso(BigDecimal idToPoliza, Integer numeroEndoso) throws SystemException{
    	return new SoporteReaseguroSN(false,false,false).obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
    }
    
    public void cancelarCotizacionFacultativo(BigDecimal idToCotizacion) throws SystemException{
    	new SoporteReaseguroSN().cancelarCotizacionFacultativo(idToCotizacion);
    }
    
    public double obtenerDiferenciaPrimaSoportePoliza(BigDecimal idToPoliza,Integer numeroEndoso) throws SystemException{
    	return new SoporteReaseguroSN(false,false,false).obtenerDiferenciaPrimaSoportePoliza(idToPoliza, numeroEndoso);
    }
    
    public SoporteReaseguroDTO notificarEmision(BigDecimal idToSoporteReaseguro,BigDecimal idToPoliza,int numeroEndoso,Date fechaEmision) throws SystemException{
    	return new SoporteReaseguroSN(false, false, false).notificarEmision(idToSoporteReaseguro, idToPoliza, numeroEndoso, fechaEmision);
    }
    
	//Pass-through method
    public boolean validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToPoliza,int numeroEndoso,List<String> listaErrores) throws SystemException{
    	List<String> erroresValidacionSoporteList =
    		new SoporteReaseguroSN().validarSoporteReaseguroDTOListoParaDistribucion(idToPoliza, numeroEndoso);
    	boolean esValido = true;
    	if (erroresValidacionSoporteList.size() > 0) {
    		for (String errorMsg : erroresValidacionSoporteList) {
    			listaErrores.add(errorMsg);
    		}
    		esValido = false;
    	}
    	
    	return esValido;
    }
    
    public boolean validarSoporteReaseguroDTOListoParaDistribucion(BigDecimal idToSoporteReaseguro,List<String> listaErrores) throws SystemException{
    	List<String> erroresValidacionSoporteList = 
    		new SoporteReaseguroSN().validarSoporteReaseguroDTOListoParaDistribucion(idToSoporteReaseguro);
    	boolean esValido = true;
    	if (erroresValidacionSoporteList.size() > 0) {
    		for (String errorMsg : erroresValidacionSoporteList) {
    			listaErrores.add(errorMsg);
    		}
    		esValido = false;
    	}
    	
    	return esValido;
    }
    
    public SoporteReaseguroDTO consultarLineasSoporte(SoporteReaseguroDTO soporteReaseguroDTO) throws SystemException{
		if(soporteReaseguroDTO != null && soporteReaseguroDTO.getIdToSoporteReaseguro() != null){
			if(soporteReaseguroDTO.getLineaSoporteReaseguroDTOs() == null || soporteReaseguroDTO.getLineaSoporteReaseguroDTOs().isEmpty()){
				soporteReaseguroDTO.setLineaSoporteReaseguroDTOs(LineaSoporteReaseguroDN.getInstancia().listarLineaSoporteReaseguroPorSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro()));
			}
			for(LineaSoporteReaseguroDTO lsr : soporteReaseguroDTO.getLineaSoporteReaseguroDTOs()){
				if(lsr.getLineaSoporteCoberturaDTOs() == null || lsr.getLineaSoporteCoberturaDTOs().isEmpty()){
					lsr.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(lsr.getId().getIdTmLineaSoporteReaseguro()));
				}
			}
		}
		return soporteReaseguroDTO;
	}
    
    public List<BigDecimal> consultarSoportesPendientesDistribucion() throws SystemException{
    	return new SoporteReaseguroSN(false, false, false).consultarSoportesPendientesDistribucion();
    }
    
    public SoporteReaseguroDTO buscarPorIdToSoporteReaseguro(BigDecimal idToSoporteReaseguro) throws ExcepcionDeAccesoADatos, SystemException{
    	return new SoporteReaseguroSN(false, false, false).getPorId(idToSoporteReaseguro);
    }
    
    public boolean actualizarBanderaLineasAplicanDistribucion(BigDecimal idToSoporteReaseguro) throws SystemException{
    	return new SoporteReaseguroSN(false,false,false).actualizarBanderaLineasAplicanDistribucion(idToSoporteReaseguro);
    }
    
    public BigDecimal calcularFactorAplicacionEndosoCancelacionRehabilitacion(BigDecimal idToPoliza,int numeroEndosoCanceladoRehabilitado, short claveTipoEndoso) throws SystemException{
    	return new SoporteReaseguroSN(false,false,false).calcularFactorAplicacionEndosoCancelacionRehabilitacion(idToPoliza, numeroEndosoCanceladoRehabilitado, claveTipoEndoso);
    }
}
