package mx.com.afirme.midas.reaseguro.soporte.validacionreasegurofacultativo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.contratofacultativo.slip.SlipDN;
import mx.com.afirme.midas.contratofacultativo.slip.SlipDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionMovSiniestroDTO;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionMovSiniestroSN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteCoberturaDTOId;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroId;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroDTO;
import mx.com.afirme.midas.reaseguro.soporte.SoporteReaseguroSN;
import mx.com.afirme.midas.reaseguro.soporte.validacion.CumuloPoliza;
import mx.com.afirme.midas.reaseguro.soporte.validacion.DetalleCobertura;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeLogicaNegocio;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class SoporteReaseguroCotizacionDN extends SoporteReaseguroBase{
	
	public SoporteReaseguroCotizacionDN(BigDecimal idToCotizacion) throws SystemException{
		this.idToCotizacion = idToCotizacion;
		List<SoporteReaseguroDTO> listaSoporteReaseguroPorCotizacion = SoporteReaseguroDN.getInstancia().listarPorIdToCotizacion(idToCotizacion);
		if ( listaSoporteReaseguroPorCotizacion != null && !listaSoporteReaseguroPorCotizacion.isEmpty() ){
			soporteReaseguroDTO = listaSoporteReaseguroPorCotizacion.get(0);
		}
		else{
			soporteReaseguroDTO = new SoporteReaseguroDTO();
		    soporteReaseguroDTO.setIdToCotizacion(idToCotizacion);
		    soporteReaseguroDTO.setFechaCreacion(new Date());
 		    soporteReaseguroDTO.setFechaModificacion(new Date());
 		    soporteReaseguroDTO.setNumeroEndoso(new Integer(0));
		    soporteReaseguroDTO = SoporteReaseguroDN.getInstancia().agregar(soporteReaseguroDTO);
		    soporteReaseguroReciente = true;
		}
	}
	
	public SoporteReaseguroCotizacionDN(BigDecimal idToCotizacion,boolean generarNuevoSoporte) throws SystemException{
		this.idToCotizacion = idToCotizacion;
		List<SoporteReaseguroDTO> listaSoporteReaseguroPorCotizacion = SoporteReaseguroDN.getInstancia().listarPorIdToCotizacion(idToCotizacion);
		if ( listaSoporteReaseguroPorCotizacion != null && !listaSoporteReaseguroPorCotizacion.isEmpty() ){
			soporteReaseguroDTO = listaSoporteReaseguroPorCotizacion.get(0);
		} else if (generarNuevoSoporte){
			soporteReaseguroDTO = new SoporteReaseguroDTO();
		    soporteReaseguroDTO.setIdToCotizacion(idToCotizacion);
		    soporteReaseguroDTO.setFechaCreacion(new Date());
 		    soporteReaseguroDTO.setFechaModificacion(new Date());
 		    soporteReaseguroDTO.setNumeroEndoso(new Integer(0));
		    soporteReaseguroDTO = SoporteReaseguroDN.getInstancia().agregar(soporteReaseguroDTO);
		    soporteReaseguroReciente = true;
		}
//		else{
//			throw new SystemException("No existe soporte para la cotizacion "+idToCotizacion, 20);
//		}
	}
	
	public boolean notificarEndosoCancelacion(BigDecimal idToPoliza,int numeroEndoso,BigDecimal idToCotizacionEndosoCancelacion,Date fechaEmisionCancelacion,
			Date fechaInicioVigenciaEndoso,double factorPrima,List<String> listaErrores, Usuario usuario,Boolean ignorarSoporteExistente,Boolean ignorarSoporteFacultativo) throws SystemException{
		boolean ignorarSoporteExistenteLocal = (ignorarSoporteExistente != null ? ignorarSoporteExistente : false);
		boolean ignorarSoporteFacultativoLocal = (ignorarSoporteFacultativo != null ? ignorarSoporteFacultativo: false);
		BigDecimal factorAplicacion = null;
		try{
			factorAplicacion = SoporteReaseguroDN.getInstancia().calcularFactorAplicacionEndosoCancelacionRehabilitacion(idToPoliza, numeroEndoso, EndosoDTO.TIPO_ENDOSO_CANCELACION);
		}
		catch(Exception e){
			LogDeMidasWeb.log("Ocurri� un error al calcular el factor de aplicaci�n de la p�liza: "+idToPoliza+", endoso:"+numeroEndoso+". Se utilizar� el factor recibido: "+factorPrima, Level.SEVERE, e);
			factorAplicacion = new BigDecimal(factorPrima);
		}
		boolean resultado = new SoporteReaseguroSN(false,false,false).notificarEmisionEndoso(this.soporteReaseguroDTO, idToPoliza, 
				numeroEndoso, idToCotizacionEndosoCancelacion, fechaEmisionCancelacion,fechaInicioVigenciaEndoso,factorAplicacion,EndosoDTO.TIPO_ENDOSO_CANCELACION, listaErrores,ignorarSoporteExistenteLocal);
		procesarReasultadoNotificacionEndoso(idToPoliza, numeroEndoso, idToCotizacionEndosoCancelacion, fechaEmisionCancelacion, EndosoDTO.TIPO_ENDOSO_CANCELACION,resultado, listaErrores,usuario,ignorarSoporteFacultativoLocal);
		return resultado;
	}
	
	public boolean notificarEndosoRehabilitacion(BigDecimal idToPoliza,int numeroEndoso,BigDecimal idToCotizacionEndosoRehabilitacion,Date fechaEmisionRehabilitacion,
			Date fechaInicioVigenciaEndoso,double factorPrima,List<String> listaErrores,Usuario usuario,Boolean ignorarSoporteExistente,Boolean ignorarSoporteFacultativo) throws SystemException{
		boolean ignorarSoporteExistenteLocal = (ignorarSoporteExistente != null ? ignorarSoporteExistente : false);
		boolean ignorarSoporteFacultativoLocal = (ignorarSoporteFacultativo != null ? ignorarSoporteFacultativo: false);
		BigDecimal factorAplicacion = null;
		try{
			factorAplicacion = SoporteReaseguroDN.getInstancia().calcularFactorAplicacionEndosoCancelacionRehabilitacion(idToPoliza, numeroEndoso, EndosoDTO.TIPO_ENDOSO_REHABILITACION);
		}
		catch(Exception e){
			LogDeMidasWeb.log("Ocurri� un error al calcular el factor de aplicaci�n de la p�liza: "+idToPoliza+", endoso:"+numeroEndoso+". Se utilizar� el factor recibido: "+factorPrima, Level.SEVERE, e);
			factorAplicacion = new BigDecimal(factorPrima);
		}
		boolean resultado = new SoporteReaseguroSN(false,false,false).notificarEmisionEndoso(soporteReaseguroDTO, idToPoliza, 
				numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion,fechaInicioVigenciaEndoso,factorAplicacion,EndosoDTO.TIPO_ENDOSO_REHABILITACION, listaErrores,ignorarSoporteExistenteLocal);
		procesarReasultadoNotificacionEndoso(idToPoliza, numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion, EndosoDTO.TIPO_ENDOSO_REHABILITACION,resultado, listaErrores,usuario,ignorarSoporteFacultativoLocal);
		return resultado;
	}
	
	public boolean notificarEndosoCambioFormaPago(BigDecimal idToPoliza,int numeroEndoso,BigDecimal idToCotizacionEndosoRehabilitacion,Date fechaEmisionRehabilitacion,
			Date fechaInicioVigenciaEndoso,double factorPrima,List<String> listaErrores,Usuario usuario,Boolean ignorarSoporteExistente,Boolean ignorarSoporteFacultativo) throws SystemException{
		boolean ignorarSoporteExistenteLocal = (ignorarSoporteExistente != null ? ignorarSoporteExistente : false);
		boolean ignorarSoporteFacultativoLocal = (ignorarSoporteFacultativo != null ? ignorarSoporteFacultativo: false);
		BigDecimal factorAplicacion = new BigDecimal(factorPrima);
		boolean resultado = new SoporteReaseguroSN(false,false,false).notificarEmisionEndoso(soporteReaseguroDTO, idToPoliza, 
				numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion,fechaInicioVigenciaEndoso,factorAplicacion,EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO, listaErrores,ignorarSoporteExistenteLocal);
		procesarReasultadoNotificacionEndoso(idToPoliza, numeroEndoso, idToCotizacionEndosoRehabilitacion, fechaEmisionRehabilitacion, EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO,resultado, listaErrores, usuario,ignorarSoporteFacultativoLocal);
		return resultado;
	}
	
	private void procesarReasultadoNotificacionEndoso(BigDecimal idToPoliza,int numeroEndoso,
			BigDecimal idToCotizacionEndoso,Date fechaEmisionEndoso,short tipoEndoso,boolean resultado,List<String> listaErrores, Usuario usuario,boolean ignorarSoporteFacultativo) throws SystemException{
		String operacion = (tipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION ? "Cancelacion" : tipoEndoso == EndosoDTO.TIPO_ENDOSO_REHABILITACION ? "Rehabilitacion" : tipoEndoso == EndosoDTO.TIPO_ENDOSO_CAMBIO_FORMA_DE_PAGO ? "Cambio de forma de pago" : null);
		//no se debe validar por la cantidad de slips, sino por el porcentaje facultativo en las lineas
		if(resultado){
			int lineasConFacultativo = LineaSoporteReaseguroDN.getInstancia().contarLineaSoporteReaseguroPorPorcentajeFacultativo(soporteReaseguroDTO.getIdToSoporteReaseguro(), 0d, false);
			/*
			 * 23/06/2011 Se elimina la validaci�n debido a que una vez emitido el endoso, se debe proceder con la distribuci�n para evitar descuadres da�os-reaseguro
			 */
			SoporteReaseguroDTO soporteReaseguroNuevoEndoso = SoporteReaseguroDN.getInstancia().obtenerSoporteReaseguroPorPolizaEndoso(idToPoliza, numeroEndoso);
			
			if(soporteReaseguroNuevoEndoso != null){
				iniciarTimerDistribucionSoporte(soporteReaseguroNuevoEndoso.getIdToSoporteReaseguro(),idToPoliza, numeroEndoso);
			}
			
			if(lineasConFacultativo > 0 ){
				
				LogDeMidasWeb.log("Se realiz� la "+operacion+" de una cotizaci�n con facultativo: idToCotizacion = "+soporteReaseguroDTO.getIdToCotizacion()+
						"se encontraron "+lineasConFacultativo+" Lineas con facultativo.", Level.INFO, null);
				
				try{
					SoporteDanosDN.getInstancia().enviaCorreoNotificacionEndosoFacultativo(soporteReaseguroDTO, usuario, tipoEndoso,operacion);
				}catch(Exception e){
					String error = "Ocurri� un error al intentar enviar un correo de notificacion de "+operacion+" de una p�liza con contratos facultativos.";
					LogDeMidasWeb.log(error, Level.SEVERE, e);
					UtileriasWeb.registraLogInteraccionReaseguro("procesarReasultadoNotificacionEndoso", (short)2, 
							this.toString(), "procesarReasultadoNotificacionEndoso", usuario.getNombreUsuario(), "idToPoliza: "+idToPoliza+",numeroEndoso: "+numeroEndoso+"idToCotizacionEndoso: "+idToCotizacionEndoso+
							",fechaEmisionEndoso: "+fechaEmisionEndoso+", tipoEndoso:"+tipoEndoso+"resultado: "+resultado, "void", e, error, error, this.toString(), (short)2);
				}
				
			}
		} else{
			LogDeMidasWeb.log("Fall� la creaci�n de un soporteReaseguro de endoso de "+operacion+" para la cotizaci�n: idToCotizacion = "+soporteReaseguroDTO.getIdToCotizacion()+
					"idToPoliza: "+soporteReaseguroDTO.getIdToPoliza(), Level.INFO, null);
			String errores = "";
			if(listaErrores != null)
				errores = obtenerErrores(listaErrores);
				
			UtileriasWeb.registraLogInteraccionReaseguro("notificarEndoso"+operacion, (short)2, this.toString(), "notificarEndoso"+operacion, "SISTEMA",
					"idToPoliza:"+idToPoliza+",numeroEndoso:"+numeroEndoso+",idToCotizacionEndoso"+operacion+":"+idToCotizacionEndoso+",fechaEmision"+operacion+":"+fechaEmisionEndoso+",listaErrores:"+listaErrores,
					""+resultado, null, "Ocurri� un error al generar un SoporteReaseguro de endoso de "+operacion+"de una poliza de facultativo. IdtoPoliza: "+idToPoliza, "ListaErrores:"+errores,
					this.toString(), (short)2);
		}
	}
	
	public String obtenerErrores(List<String> listaErrores) {
		StringBuilder errores = new StringBuilder("");
		for(String error : listaErrores)
			errores.append(" ").append(error).append(".");
		return errores.toString();
	}

	public void insertarCumulosLinea(List<CumuloPoliza> listaCumuloPoliza,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		if(listaCumuloPoliza != null && !listaCumuloPoliza.isEmpty()){
			if(tipoCambio == null)
				tipoCambio  =  TipoCambioDN.getInstancia(nombreUsuario).obtieneTipoCambioPorDia(new Date(), Integer.valueOf(Sistema.MONEDA_DOLARES).shortValue());
			if(tipoCambio == null)
				throw new SystemException("Imposible obtener el tipo de cambio actual");
			for(CumuloPoliza cumuloPoliza: listaCumuloPoliza){
				//verificar si existe la lineaSoporteReaseguro correspondiente al c�mulo, si existe, se deja intacta.
				LineaSoporteReaseguroDTO nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguroPorCumulo(cumuloPoliza.getLineaDTO(), cumuloPoliza.getNumeroInciso(), cumuloPoliza.getNumeroSubInciso(),this.listaLineasPendientes);
				if(nuevaLineaSoporte == null){
					Short numeroEndoso = soporteReaseguroDTO.getNumeroEndoso() != null? soporteReaseguroDTO.getNumeroEndoso().shortValue() : (short)0;
					nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguro(cumuloPoliza,null, tipoCambio,soporteReaseguroDTO.getIdToSoporteReaseguro(),numeroEndoso,false,true);
					List<LineaSoporteCoberturaDTO> listaCoberturas = nuevaLineaSoporte.getLineaSoporteCoberturaDTOs();
					nuevaLineaSoporte.setLineaSoporteCoberturaDTOs(null);
					nuevaLineaSoporte = LineaSoporteReaseguroDN.getInstancia().agregar(nuevaLineaSoporte);
					List<LineaSoporteCoberturaDTO> listaCoberturasRegistradas = new ArrayList<LineaSoporteCoberturaDTO>();
					if(listaCoberturas != null){
						for(LineaSoporteCoberturaDTO cobertura: listaCoberturas){
							cobertura.setId(new LineaSoporteCoberturaDTOId());
							cobertura.getId().setIdTmLineaSoporteReaseguro(nuevaLineaSoporte.getId().getIdTmLineaSoporteReaseguro());
							cobertura.getId().setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
							listaCoberturasRegistradas.add(LineaSoporteCoberturaDN.getInstancia().agregar(cobertura));
						}
						nuevaLineaSoporte.setLineaSoporteCoberturaDTOs(listaCoberturasRegistradas);
					}
				}
				if(mapaLineasPorDistribucion.get(cumuloPoliza.getLineaDTO().getTipoDistribucion()) == null){
					mapaLineasPorDistribucion.put(cumuloPoliza.getLineaDTO().getTipoDistribucion(), new ArrayList<LineaSoporteReaseguroDTO>());
				}
				mapaLineasPorDistribucion.get(cumuloPoliza.getLineaDTO().getTipoDistribucion()).add(nuevaLineaSoporte);
			}
		}
	}
	
	/**
	 * Este m�todo es utilizado para regenerar los detalles de coberturas de las lineas pertenecinetes al soporteReaseguro instanciado.
	 * A las lineas que tengan un SLIP, o relacion con alg�n DistribucionMovSiniestroDTO, se les insertan las nuevas coberturas. 
	 * Las Lineas restantes son eliminadas y registradas nuevamente con las nuevas coberturas. 
	 * @param listaCumuloPoliza
	 * @param nombreUsuario
	 * @throws ExcepcionDeAccesoADatos
	 * @throws SystemException
	 * @throws ExcepcionDeLogicaNegocio
	 */
	public void actualizarCumulosLinea(List<CumuloPoliza> listaCumuloPoliza,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException, ExcepcionDeLogicaNegocio{
		if(this.soporteReaseguroReciente)
			insertarCumulosLinea(listaCumuloPoliza, nombreUsuario);
		else{
			if(listaCumuloPoliza != null && !listaCumuloPoliza.isEmpty()){
				if(mapaLineasPorDistribucion == null)
					mapaLineasPorDistribucion = new HashMap<BigDecimal, List<LineaSoporteReaseguroDTO>>();
			}
			//recuperar las lineas registradas actualmente
			if(listaLineasPendientes == null)
				listaLineasPendientes = LineaSoporteReaseguroDN.getInstancia().listarLineaSoporteReaseguroPorSoporte(soporteReaseguroDTO.getIdToSoporteReaseguro());
			//recuperar el tipo de cambio de cualquiera de las lineas
			Integer estatusLineas = null;
			if(listaLineasPendientes != null && !listaLineasPendientes.isEmpty()){
				tipoCambio = listaLineasPendientes.get(0).getTipoCambio();
				estatusLineas = listaLineasPendientes.get(0).getEstatusLineaSoporte();
			}//si no hay lineas significa que no se ha validado
			else
				return;
			BigDecimal idToPoliza = soporteReaseguroDTO.getIdToPoliza() != null ? soporteReaseguroDTO.getIdToPoliza() : null ;
			Date fechaEmision = soporteReaseguroDTO.getFechaEmision() != null ? soporteReaseguroDTO.getFechaEmision() : null;
			
			//recuperar los slips generados para el soporte reaseguro en curso.
			SlipDTO slipTMP = new SlipDTO();
			slipTMP.setIdToCotizacion(idToCotizacion);
			slipTMP.setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
			List<SlipDTO> listaSlipRegistrados = SlipDN.getInstancia().listarFiltrado(slipTMP);
			int cantidadMovimientosSiniestros = 0;
			//Recuperar los registros DistribucionMovSiniestroDTO relacionados con el soporte
			DistribucionMovSiniestroDTO siniestro = new DistribucionMovSiniestroDTO();
			siniestro.setLineaSoporteReaseguroDTO(new LineaSoporteReaseguroDTO());
			siniestro.getLineaSoporteReaseguroDTO().setId(new LineaSoporteReaseguroId());
			siniestro.getLineaSoporteReaseguroDTO().getId().setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
			List<DistribucionMovSiniestroDTO> listaMovimientosSiniestro = new DistribucionMovSiniestroSN().listarFiltrado(siniestro);
			
			List<LineaSoporteReaseguroDTO> listaLineasPorInsertar = new ArrayList<LineaSoporteReaseguroDTO>();
			List<BigDecimal> listaIdTmLineasNoEliminables = new ArrayList<BigDecimal>();
			for(CumuloPoliza cumuloPoliza: listaCumuloPoliza){
				Short numeroEndoso = soporteReaseguroDTO.getNumeroEndoso() != null? soporteReaseguroDTO.getNumeroEndoso().shortValue() : (short)0;
				LineaSoporteReaseguroDTO lineaSoporteActual = LineaSoporteReaseguroDN.getInstancia().obtenerLineaSoporteReaseguro(cumuloPoliza,null, tipoCambio,soporteReaseguroDTO.getIdToSoporteReaseguro(),numeroEndoso,false,true);
				lineaSoporteActual.setEstatusLineaSoporte(estatusLineas);
				SlipDTO slip = obtenerSlip(cumuloPoliza.getIdSubRamo(), cumuloPoliza.getNumeroInciso(), cumuloPoliza.getNumeroSubInciso(), listaSlipRegistrados);
				//Si encuentra slip, se obtiene el id de su linea y se registran las coberturas para esa linea
				if(slip != null){
					List<LineaSoporteCoberturaDTO> listaCoberturas = lineaSoporteActual.getLineaSoporteCoberturaDTOs();
					lineaSoporteActual.setLineaSoporteCoberturaDTOs(null);
					//validar si la linea existente no tiene coberturas, si las tiene, no se guardan las nuevas coberturas
					LineaSoporteReaseguroDTO lineaActual = obtenerLineaSoporteReaseguro(slip.getIdTmLineaSoporteReaseguro());
					if(lineaActual != null){
						lineaActual.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(slip.getIdTmLineaSoporteReaseguro()));
						if(lineaActual.getLineaSoporteCoberturaDTOs().isEmpty()){
							if(listaCoberturas != null){
								List<LineaSoporteCoberturaDTO> listaCoberturasRegistradas = new ArrayList<LineaSoporteCoberturaDTO>();
								for(LineaSoporteCoberturaDTO cobertura: listaCoberturas){
									cobertura.setId(new LineaSoporteCoberturaDTOId());
									cobertura.getId().setIdTmLineaSoporteReaseguro(slip.getIdTmLineaSoporteReaseguro());
									cobertura.getId().setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
									cobertura.setIdToPoliza(idToPoliza);
									listaCoberturasRegistradas.add(LineaSoporteCoberturaDN.getInstancia().agregar(cobertura));
								}
								lineaSoporteActual.setLineaSoporteCoberturaDTOs(listaCoberturasRegistradas);
								listaIdTmLineasNoEliminables.add(slip.getIdTmLineaSoporteReaseguro());
								ordenarLinea(cumuloPoliza.getLineaDTO().getTipoDistribucion(), lineaSoporteActual);
							}
						}
					}
				}
				else{
					//intentar recuperar el movimiento de siniestro relacionado con la linea
					if(cantidadMovimientosSiniestros < listaMovimientosSiniestro.size()){
						DistribucionMovSiniestroDTO movto = null;
						for(CumuloPoliza cumulo : listaCumuloPoliza){
							for(DetalleCobertura detalle : cumulo.getDetalleCoberturas()){
								movto = obtenerDistribucionMovimiento(detalle.getIdSeccion(), detalle.getIdCobertura(), detalle.getNumeroInciso(), detalle.getNumeroSubinciso(), listaMovimientosSiniestro);
								if(movto != null){
									cantidadMovimientosSiniestros++;
									break;
								}
							}
						}
						if(movto != null){
							List<LineaSoporteCoberturaDTO> listaCoberturas = lineaSoporteActual.getLineaSoporteCoberturaDTOs();
							lineaSoporteActual.setLineaSoporteCoberturaDTOs(null);
							//validar si la linea existente no tiene coberturas, si las tiene, no se guardan las nuevas coberturas
							LineaSoporteReaseguroDTO lineaActual = obtenerLineaSoporteReaseguro(movto.getLineaSoporteReaseguroDTO().getId().getIdTmLineaSoporteReaseguro());
							if(lineaActual != null){
								lineaActual.setLineaSoporteCoberturaDTOs(LineaSoporteCoberturaDN.getInstancia().listarLineaSoporteCoberturaPorLineaSoporteId(movto.getLineaSoporteReaseguroDTO().getId().getIdTmLineaSoporteReaseguro()));
								if(lineaActual.getLineaSoporteCoberturaDTOs().isEmpty()){
									if(listaCoberturas != null){
										List<LineaSoporteCoberturaDTO> listaCoberturasRegistradas = new ArrayList<LineaSoporteCoberturaDTO>();
										for(LineaSoporteCoberturaDTO cobertura: listaCoberturas){
											cobertura.setId(new LineaSoporteCoberturaDTOId());
											cobertura.getId().setIdTmLineaSoporteReaseguro(movto.getLineaSoporteReaseguroDTO().getId().getIdTmLineaSoporteReaseguro());
											cobertura.getId().setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
											cobertura.setIdToPoliza(idToPoliza);
											listaCoberturasRegistradas.add(LineaSoporteCoberturaDN.getInstancia().agregar(cobertura));
										}
										lineaSoporteActual.setLineaSoporteCoberturaDTOs(listaCoberturasRegistradas);
										listaIdTmLineasNoEliminables.add(movto.getLineaSoporteReaseguroDTO().getId().getIdTmLineaSoporteReaseguro());
										ordenarLinea(cumuloPoliza.getLineaDTO().getTipoDistribucion(), lineaSoporteActual);
									}
								}
							}
						}
						else
							listaLineasPorInsertar.add(lineaSoporteActual);
					}
					else
						listaLineasPorInsertar.add(lineaSoporteActual);
				}
			}
			//Las lineas que no fueron guardadas deben ser registradas despues de que sus correspondientes sean eliminadas, ya que no hay forma de saber sus datos
			try{
				LineaSoporteReaseguroDN.getInstancia().eliminarLineaSoportePoridSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro(), listaIdTmLineasNoEliminables, false);
				//Despues del borrado, se insertan las nuevas lineas con sus coberturas
				for(LineaSoporteReaseguroDTO nuevaLinea: listaLineasPorInsertar){
					List<LineaSoporteCoberturaDTO> listaCoberturas = nuevaLinea.getLineaSoporteCoberturaDTOs();
					nuevaLinea.setLineaSoporteCoberturaDTOs(null);
					nuevaLinea.setIdToPoliza(idToPoliza);
					nuevaLinea.setFechaEmision(fechaEmision);
					nuevaLinea = LineaSoporteReaseguroDN.getInstancia().agregar(nuevaLinea);
					List<LineaSoporteCoberturaDTO> listaCoberturasRegistradas = new ArrayList<LineaSoporteCoberturaDTO>();
					if(listaCoberturas != null){
						for(LineaSoporteCoberturaDTO cobertura: listaCoberturas){
							cobertura.setId(new LineaSoporteCoberturaDTOId());
							cobertura.getId().setIdTmLineaSoporteReaseguro(nuevaLinea.getId().getIdTmLineaSoporteReaseguro());
							cobertura.getId().setIdToSoporteReaseguro(soporteReaseguroDTO.getIdToSoporteReaseguro());
							cobertura.setIdToPoliza(idToPoliza);
							listaCoberturasRegistradas.add(LineaSoporteCoberturaDN.getInstancia().agregar(cobertura));
						}
						nuevaLinea.setLineaSoporteCoberturaDTOs(listaCoberturasRegistradas);
					}
					ordenarLinea(nuevaLinea.getLineaDTO().getTipoDistribucion(), nuevaLinea);
			}
			}catch(Exception e){
				//Si ocurre una excepcion al eliminar la linea, significa que ya tiene sus coberturas, no se hace nada
			}
		}
		
	}
	
	private LineaSoporteReaseguroDTO obtenerLineaSoporteReaseguro(BigDecimal idTmLineaSoporteReaseguro){
		LineaSoporteReaseguroDTO lineaSoporte = null;
		if(listaLineasPendientes != null && !listaLineasPendientes.isEmpty()){
			for(LineaSoporteReaseguroDTO lineaTMP : listaLineasPendientes){
				if(lineaTMP.getId().getIdTmLineaSoporteReaseguro().compareTo(idTmLineaSoporteReaseguro) == 0){
					lineaSoporte = lineaTMP;
					break;
				}
			}
		}
		return lineaSoporte;
	}
	
	private SlipDTO obtenerSlip(BigDecimal idTcSubRamo,Integer numeroInciso,Integer numeroSubInciso,List<SlipDTO> listaSlips){
		SlipDTO slipEncontrado = null;
		BigDecimal numeroIncisoBigDecimal = (numeroInciso != null?BigDecimal.valueOf(numeroInciso.doubleValue()):null);
		BigDecimal numeroSubIncisoBigDecimal = (numeroSubInciso != null?BigDecimal.valueOf(numeroSubInciso.doubleValue()):null);
		for(SlipDTO slip : listaSlips){
			if(slip.getIdTcSubRamo().compareTo(idTcSubRamo) == 0){
				if(numeroInciso == null && slip.getNumeroInciso() == null){
					slipEncontrado = slip;
					break;
				} else if(numeroInciso != null && slip.getNumeroInciso() != null && slip.getNumeroInciso().compareTo(numeroIncisoBigDecimal) == 0){
					if(numeroSubInciso == null && slip.getNumeroSubInciso() == null){
						slipEncontrado = slip;
						break;
					} else if(numeroSubInciso != null && slip.getNumeroSubInciso() != null && slip.getNumeroSubInciso().compareTo(numeroSubIncisoBigDecimal) == 0){
						slipEncontrado = slip;
						break;
					}
				}
			}
		}
		return slipEncontrado;
	}
	
	private void ordenarLinea(BigDecimal tipoDistribucion,LineaSoporteReaseguroDTO nuevaLineaSoporte){
		if(mapaLineasPorDistribucion.get(tipoDistribucion) == null){
			mapaLineasPorDistribucion.put(tipoDistribucion, new ArrayList<LineaSoporteReaseguroDTO>());
		}
		mapaLineasPorDistribucion.get(tipoDistribucion).add(nuevaLineaSoporte);
	}
	
//	public LineaDTO obtenerLineaPorSubramo(Date fechaInicioVigencia, int idSubramo) throws SystemException{
//		 return SoporteReaseguroDN.getInstancia().obtenerLineaDTO(fechaInicioVigencia,new BigDecimal(idSubramo));
//	}
}