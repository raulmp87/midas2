package mx.com.afirme.midas.siniestro;

import java.math.BigDecimal;

public class SubIncisosSeccionPoliza {

	private static final long serialVersionUID = 1L;
	private BigDecimal idToSeccion;
    private String nombreComercialSeccion;
    private BigDecimal numeroSubInciso;
    private String descripcionSubInciso;
    private Short seleccionado;
    
    public SubIncisosSeccionPoliza(){
    	
    }

	/**
	 * @return the idToSeccion
	 */
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	/**
	 * @param idToSeccion the idToSeccion to set
	 */
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	/**
	 * @return the nombreComercialSeccion
	 */
	public String getNombreComercialSeccion() {
		return nombreComercialSeccion;
	}

	/**
	 * @param nombreComercialSeccion the nombreComercialSeccion to set
	 */
	public void setNombreComercialSeccion(String nombreComercialSeccion) {
		this.nombreComercialSeccion = nombreComercialSeccion;
	}

	/**
	 * @return the numeroSubInciso
	 */
	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	/**
	 * @param numeroSubInciso the numeroSubInciso to set
	 */
	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	/**
	 * @return the descripcionSubInciso
	 */
	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	/**
	 * @param descripcionSubInciso the descripcionSubInciso to set
	 */
	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	/**
	 * @return the seleccionado
	 */
	public Short getSeleccionado() {
		return seleccionado;
	}

	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Short seleccionado) {
		this.seleccionado = seleccionado;
	}

    
}
