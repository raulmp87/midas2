package mx.com.afirme.midas.siniestro;

import java.math.BigDecimal;
import java.util.Date;

public class IncisosPoliza {

	private static final long serialVersionUID = 1L;
	private BigDecimal numeroInciso;
    private String direccionInciso;
    private Date fechaInicioVigencia;
    private Date fechaFinVigencia;
    private Short seleccionado;
    
	public IncisosPoliza() {
	}

	/**
	 * @return the numeroInciso
	 */
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @return the direccionInciso
	 */
	public String getDireccionInciso() {
		return direccionInciso;
	}

	/**
	 * @param direccionInciso the direccionInciso to set
	 */
	public void setDireccionInciso(String direccionInciso) {
		this.direccionInciso = direccionInciso;
	}

	/**
	 * @return the fechaInicioVigencia
	 */
	public Date getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}

	/**
	 * @param fechaInicioVigencia the fechaInicioVigencia to set
	 */
	public void setFechaInicioVigencia(Date fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}

	/**
	 * @return the fechaFinVigencia
	 */
	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	/**
	 * @param fechaFinVigencia the fechaFinVigencia to set
	 */
	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}

	/**
	 * @return the seleccionado
	 */
	public Short getSeleccionado() {
		return seleccionado;
	}

	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Short seleccionado) {
		this.seleccionado = seleccionado;
	}

	
	
}
