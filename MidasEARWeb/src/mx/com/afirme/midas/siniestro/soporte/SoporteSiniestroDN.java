package mx.com.afirme.midas.siniestro.soporte;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class SoporteSiniestroDN {

	
private static final SoporteSiniestroDN INSTANCIA = new SoporteSiniestroDN();
	
	public static SoporteSiniestroDN getInstancia() {
		return INSTANCIA;
	}
	
	public List<SoporteReporteSiniestroDTO> getReporteSiniestro(BigDecimal idToPoliza){
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia("");
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		List<SoporteReporteSiniestroDTO> listaSoporte = new ArrayList<SoporteReporteSiniestroDTO>();

		try {
			List<ReporteSiniestroDTO> listaReportes = reporteSiniestroDN.listarReportesPorPropiedad("numeroPoliza", idToPoliza);
			
			for(ReporteSiniestroDTO reporte :listaReportes){
				SoporteReporteSiniestroDTO objetoSoporte= new SoporteReporteSiniestroDTO();
				objetoSoporte.setFechaSiniestro(reporte.getFechaHoraReporte());
				objetoSoporte.setEstatus(reporte.getEstatusSiniestro().getIdTcEstatusSiniestro());
				objetoSoporte.setDescripcionEstatus(reporte.getEstatusSiniestro().getDescripcion());
				objetoSoporte.setDescripcionSiniestro(reporte.getDescripcionSiniestro());
				objetoSoporte.setNumeroSiniestro(reporte.getNumeroReporte());
				objetoSoporte.setEstimacion(reservaDetalleDN.obtenerSumaReservaInicial(reporte.getIdToReporteSiniestro()));
				objetoSoporte.setTotalIndemnizaciones(indemnizacionRiesgoCoberturaDN.sumaPerdidaDeterminadaPorReporte(reporte.getIdToReporteSiniestro()));
				
				this.obtenerListaRiesgos(reporte.getIdToReporteSiniestro(), objetoSoporte);
				
				listaSoporte.add(objetoSoporte);
			}
			
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return listaSoporte;
	}
	
	
	private void obtenerListaRiesgos(BigDecimal idToReporteSiniestro,SoporteReporteSiniestroDTO objetoSoporte) throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		List<RiesgoAfectadoDTO> listaRiesgos = riesgoAfectadoDN.listarPorReporteSiniestro(idToReporteSiniestro);
		if(listaRiesgos.size() > 0){
			List<SoporteCoberturaRiesgoDTO> listaSoporteCoberturaRiesgo = new ArrayList<SoporteCoberturaRiesgoDTO>();
			objetoSoporte.setNumeroInciso(listaRiesgos.get(0).getId().getNumeroinciso());
			for(RiesgoAfectadoDTO objetoRiesgo: listaRiesgos){
				SoporteCoberturaRiesgoDTO objetoSoporteRiesgoCobertura = new SoporteCoberturaRiesgoDTO();
				objetoSoporteRiesgoCobertura.setIdTocobertura(objetoRiesgo.getId().getIdtocobertura());
				objetoSoporteRiesgoCobertura.setIdToRiesgo(objetoRiesgo.getId().getIdtoriesgo());
				objetoSoporteRiesgoCobertura.setIdToSeccion(objetoRiesgo.getId().getIdtoseccion());
				objetoSoporteRiesgoCobertura.setNumeroSubInciso(objetoRiesgo.getId().getNumerosubinciso());
				
				listaSoporteCoberturaRiesgo.add(objetoSoporteRiesgoCobertura);
			}
			objetoSoporte.setListaCoberturaRiesgo(listaSoporteCoberturaRiesgo);
		}
		
	}
}
