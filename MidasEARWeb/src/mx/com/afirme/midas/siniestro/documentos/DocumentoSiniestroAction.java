package mx.com.afirme.midas.siniestro.documentos;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDN;
import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoSiniestroAction extends MidasMappingDispatchAction {
	
	public ActionForward listar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			//Se revisa el reporte para el que se va a realizar la consulta
			String idReporteSiniestro = request.getParameter("idReporteSiniestro");
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(idReporteSiniestro));
			buscarPorPropiedad(request,"reporteSiniestroDTO.idToReporteSiniestro",reporteSiniestroDTO.getIdToReporteSiniestro());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	private void buscarPorPropiedad(HttpServletRequest request, String nombrePropiedad, Object valor)
	throws SystemException, ExcepcionDeAccesoADatos {
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		List<DocumentoSiniestroDTO> documentosSiniestro = documentoSiniestroDN.buscarPorPropiedad(nombrePropiedad, valor);
		request.setAttribute("documentosSiniestro", documentosSiniestro);
	}

	private String poblarDTO(DocumentoSiniestroForm documentoSiniestroForm,
			DocumentoSiniestroDTO documentoSiniestroDTO) throws SystemException, ExcepcionDeAccesoADatos {
		if (!UtileriasWeb.esCadenaVacia(documentoSiniestroForm.getNombreDocumento()))
			documentoSiniestroDTO.setNombreDocumento(documentoSiniestroForm.getNombreDocumento());
		if (!UtileriasWeb.esCadenaVacia(documentoSiniestroForm.getIdTipoDocumento())){
			TipoDocumentoSiniestroDN tipoDocumentoSiniestroDN  = TipoDocumentoSiniestroDN.getInstancia();
			TipoDocumentoSiniestroDTO tipoDocumentoSiniestroDTO = new TipoDocumentoSiniestroDTO();
			tipoDocumentoSiniestroDTO.setIdTcTipoDocumentoSiniestro(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdTipoDocumento()));
			tipoDocumentoSiniestroDTO = tipoDocumentoSiniestroDN.getTipoDocumentoSiniestroPorId(tipoDocumentoSiniestroDTO);
			documentoSiniestroDTO.setTipoDocumentoSiniestroDTO(tipoDocumentoSiniestroDTO);
		}
		if (!UtileriasWeb.esCadenaVacia(documentoSiniestroForm.getIdReporteSiniestro())){
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			reporteSiniestroDTO.setIdToReporteSiniestro(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			documentoSiniestroDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		}
		return null;
	}
	
	private String poblarForm(DocumentoSiniestroForm documentoSiniestroForm,
		DocumentoSiniestroDTO documentoSiniestroDTO) throws SystemException {
		documentoSiniestroForm.setNombreDocumento(documentoSiniestroDTO.getNombreDocumento());
		documentoSiniestroForm.setIdTipoDocumento(documentoSiniestroDTO.getTipoDocumentoSiniestroDTO().getIdTcTipoDocumentoSiniestro().toBigInteger().toString());
		documentoSiniestroForm.setIdReporteSiniestro(documentoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro().toBigInteger().toString());
		documentoSiniestroForm.setIdDocumentoSiniestro(documentoSiniestroDTO.getIdToDocumentoSiniestro().toBigInteger().toString());
		return null;
	}

	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		DocumentoSiniestroDTO documentoSiniestroDTO = new DocumentoSiniestroDTO();
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		try {
			poblarDTO(documentoSiniestroForm, documentoSiniestroDTO);
			documentoSiniestroDN.agregar(documentoSiniestroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} 
		return mapping.findForward(reglaNavegacion);
	}

	
	public ActionForward modificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		DocumentoSiniestroDTO documentoSiniestroDTO = new DocumentoSiniestroDTO();
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		try {
			poblarDTO(documentoSiniestroForm, documentoSiniestroDTO);
			documentoSiniestroDTO = documentoSiniestroDN.getDocumentoSiniestroPorId(documentoSiniestroDTO);
			poblarDTO(documentoSiniestroForm, documentoSiniestroDTO);
			documentoSiniestroDN.modificar(documentoSiniestroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward borrar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroDTO documentoSiniestroDTO = new DocumentoSiniestroDTO();
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		try {
			String idDocumentoSiniestro = request.getParameter("idDocumentoSiniestro");
			documentoSiniestroDTO.setIdToDocumentoSiniestro(UtileriasWeb.regresaBigDecimal(idDocumentoSiniestro));
			documentoSiniestroDTO = documentoSiniestroDN.getDocumentoSiniestroPorId(documentoSiniestroDTO);
			ReporteSiniestroDTO reporteSiniestroDTO = documentoSiniestroDTO.getReporteSiniestroDTO();
			documentoSiniestroDN.borrar(documentoSiniestroDTO);
			//Se revisa el reporte para el que se va a realizar la consulta
			buscarPorPropiedad(request,"reporteSiniestroDTO",reporteSiniestroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}


	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		try {
			DocumentoSiniestroDTO documentoSiniestroDTO = new DocumentoSiniestroDTO();
			documentoSiniestroDTO.setIdToDocumentoSiniestro(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdDocumentoSiniestro()));
			documentoSiniestroDTO = documentoSiniestroDN.getDocumentoSiniestroPorId(documentoSiniestroDTO);
			poblarForm(documentoSiniestroForm, documentoSiniestroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrarAgregar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

}
