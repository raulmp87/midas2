package mx.com.afirme.midas.siniestro.documentos;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class DocumentoSiniestroForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String nombreDocumento;
	private String idTipoDocumento;
	private String idReporteSiniestro;
	private String idDocumentoSiniestro;
	private String idControlArchivo;
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private String numeroPoliza;
	
	public String getIdControlArchivo() {
		return idControlArchivo;
	}

	public void setIdControlArchivo(String idControlArchivo) {
		this.idControlArchivo = idControlArchivo;
	}
	
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
		this.reporteSiniestroDTO = reporteSiniestroDTO;
	}

	public String getIdDocumentoSiniestro() {
		return idDocumentoSiniestro;
	}

	public void setIdDocumentoSiniestro(String idDocumentoSiniestro) {
		this.idDocumentoSiniestro = idDocumentoSiniestro;
	}

	public String getIdReporteSiniestro() {
		return idReporteSiniestro;
	}

	public void setIdReporteSiniestro(String idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}

	public String getIdTipoDocumento() {
		return idTipoDocumento;
	}
	
	public void setIdTipoDocumento(String idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}
	
	/**
	 * @return the nombreDocumento
	 */
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	/**
	 * @param nombreDocumento the nombreDocumento to set
	 */
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	
	
}
