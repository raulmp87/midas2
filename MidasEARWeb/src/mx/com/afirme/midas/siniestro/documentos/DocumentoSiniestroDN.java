package mx.com.afirme.midas.siniestro.documentos;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoSiniestroDN {
	
	public static final DocumentoSiniestroDN INSTANCIA = new DocumentoSiniestroDN();

	public static DocumentoSiniestroDN getInstancia (){
		return DocumentoSiniestroDN.INSTANCIA;
	}
	
	public String agregar(DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.agregar(documentoSiniestroDTO);
	}
	
	public String borrar (DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.borrar(documentoSiniestroDTO);
	}
	
	public String modificar (DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.modificar(documentoSiniestroDTO);
	}
	
	public DocumentoSiniestroDTO getDocumentoSiniestroPorId(DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.getDocumentoSiniestroPorId(documentoSiniestroDTO);
	}
	
	public List<DocumentoSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.listarTodos();
	}
	
	public List<DocumentoSiniestroDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public DocumentoSiniestroDTO obtenDocumentoSiniestro(BigDecimal idReporteSiniestro, BigDecimal idTipoDocumentoSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		DocumentoSiniestroSN documentoSiniestroSN = new DocumentoSiniestroSN();
		return documentoSiniestroSN.obtenDocumentoSiniestro(idReporteSiniestro, idTipoDocumentoSiniestro);
	}

}
