package mx.com.afirme.midas.siniestro.documentos;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DocumentoSiniestroSN {
	
	private DocumentoSiniestroFacadeRemote beanRemoto;

	public DocumentoSiniestroSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DocumentoSiniestroFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(documentoSiniestroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(documentoSiniestroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(documentoSiniestroDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<DocumentoSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<DocumentoSiniestroDTO> buscarPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public DocumentoSiniestroDTO getDocumentoSiniestroPorId(DocumentoSiniestroDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(documentoSiniestroDTO.getIdToDocumentoSiniestro());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public DocumentoSiniestroDTO obtenDocumentoSiniestro(BigDecimal idReporteSiniestro, BigDecimal idTipoDocumentoSiniestro) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenDocumentoSiniestro(idReporteSiniestro, idTipoDocumentoSiniestro);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
