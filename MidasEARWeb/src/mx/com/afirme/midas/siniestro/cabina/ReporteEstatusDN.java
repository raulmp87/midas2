package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteEstatusDN {

	private static final ReporteEstatusDN INSTANCIA = new ReporteEstatusDN();

	public static ReporteEstatusDN getInstancia() {
		return INSTANCIA;
	}
	
	public void agregarReporteStatus(ReporteEstatusDTO reporteEstatusDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReporteEstatusSN reporteStatusSN = new ReporteEstatusSN();
		reporteStatusSN.agregar(reporteEstatusDTO);
	}

	public void borrarReporteStatus(ReporteEstatusDTO reporteEstatusDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReporteEstatusSN reporteStatusSN = new ReporteEstatusSN();
		reporteStatusSN.borrar(reporteEstatusDTO);
	}
	
	public void modificarReporteStatus(ReporteEstatusDTO reporteEstatusDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReporteEstatusSN reporteStatusSN = new ReporteEstatusSN();
		reporteStatusSN.modificar(reporteEstatusDTO);
	}
	
	public List<ReporteEstatusDTO> mostrarTodosResporteStatus()  throws ExcepcionDeAccesoADatos, SystemException {
		ReporteEstatusSN reporteStatusSN = new ReporteEstatusSN();
		return reporteStatusSN.listarTodos();
	}
	
	public ReporteEstatusDTO obtieneReporteStatusPorId(ReporteEstatusDTO reporteEstatusDTO) throws ExcepcionDeAccesoADatos, SystemException {
		ReporteEstatusSN reporteStatusSN = new ReporteEstatusSN();
		return reporteStatusSN.getPorId(reporteEstatusDTO);
	}

	public List<ReporteEstatusDTO> obtieneReporteStatusPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos, SystemException {
		ReporteEstatusSN reporteStatusSN = new ReporteEstatusSN();
		return reporteStatusSN.getPorPropiedad(propiedad,valor);
	}

	
}
