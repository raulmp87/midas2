package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PolizaDummySN {
	
	private PolizaDummyFacadeRemote beanRemoto;

	public PolizaDummySN() throws SystemException{
		LogDeMidasWeb.log("Entrando en PolizaDummySN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(PolizaDummyFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public PolizaDummyDTO obtenerPoliza(BigDecimal id) throws ExcepcionDeAccesoADatos{
		return beanRemoto.obtenerPoliza(id);
	}
	
	public List<PolizaDummyDTO> listarFiltrado(PolizaDummyDTO polizaDTO){
		return beanRemoto.listarFiltrado(polizaDTO);
	}
	
	public List<RiesgoPolizaDummyDTO> listarRiesgosPoliza(PolizaDummyDTO poliza){
		return beanRemoto.listarRiesgosPoliza(poliza);
	}	
	

}
