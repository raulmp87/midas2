package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;


import mx.com.afirme.midas.siniestro.cabina.DatosTransportistaFacadeRemote;
import mx.com.afirme.midas.siniestro.cabina.DatosTransportistaDTO;


import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * 
 * @author Javier Martinez
 * @since 
 */
public class DatosTransportistaSN {
	private DatosTransportistaFacadeRemote beanRemoto;
	
	public DatosTransportistaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DatosTransportistaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(datoTransportistaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(datoTransportistaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public DatosTransportistaDTO modificar(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(datoTransportistaDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DatosTransportistaDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public DatosTransportistaDTO getPorId(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(datoTransportistaDTO.getIdToReporteSiniestro());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DatosTransportistaDTO> getPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad,valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}
	
}
