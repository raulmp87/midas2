/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;


import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author smvr
 *
 */
public class ConfigCorreoTipoNegocioForm extends MidasBaseForm {

	private String idTcTipoNegocio;//COMBO ID
	private String idConfigTipoNegocio;
	private String correo;
	private String usuarioNombre;
	private String fechaModificacion;
		
	
	/**
	 * @return the idTcTipoNegocio
	 */
	public String getIdTcTipoNegocio() {
		return idTcTipoNegocio;
	}
	
	/**
	 * @param idTcTipoNegocio the idTcTipoNegocio to set
	 */
	public void setIdTcTipoNegocio(String idTcTipoNegocio) {
		this.idTcTipoNegocio = idTcTipoNegocio;
	}
	
	/**
	 * @return the idConfigTipoNegocio
	 */
	public String getIdConfigTipoNegocio() {
		return idConfigTipoNegocio;
	}
	/**
	 * @param idConfigTipoNegocio the idConfigTipoNegocio to set
	 */
	public void setIdConfigTipoNegocio(String idConfigTipoNegocio) {
		this.idConfigTipoNegocio = idConfigTipoNegocio;
	}
	
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	/**
	 * @return the usuarioNombre
	 */
	public String getUsuarioNombre() {
		return usuarioNombre;
	}
	
	/**
	 * @param usuarioNombre the usuarioNombre to set
	 */
	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}
	
	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

}
