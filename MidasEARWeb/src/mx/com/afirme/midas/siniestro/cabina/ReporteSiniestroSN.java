/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;

import java.util.List;

import java.math.BigDecimal;

/**
 * @author admin
 * 
 */
public class ReporteSiniestroSN {
	private ReporteSiniestroFacadeRemote beanRemoto;
	private String nombreUsuario;
	
	public ReporteSiniestroSN(String nombreUsuario) throws SystemException {
		this.nombreUsuario = nombreUsuario;
		LogDeMidasWeb.log("Entrando en ReporteSiniestroSN - Constructor", Level.INFO,
				null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ReporteSiniestroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}	
		
	public ReporteSiniestroDTO crearReporte(ReporteSiniestroDTO reporteSiniestroDTO)
	throws ExcepcionDeAccesoADatos {
		reporteSiniestroDTO.setNombreUsuarioLog(this.nombreUsuario);
		if(reporteSiniestroDTO.getIdToReporteSiniestro()==null){
			return beanRemoto.save(reporteSiniestroDTO);				
		}
		else{
			return beanRemoto.update(reporteSiniestroDTO);
		}
	}
	
	public ReporteSiniestroDTO guardarReporte(ReporteSiniestroDTO reporteSiniestroDTO)
	throws ExcepcionDeAccesoADatos {
		reporteSiniestroDTO.setNombreUsuarioLog(this.nombreUsuario);
		return beanRemoto.update(reporteSiniestroDTO);
	}			
	
	public ReporteSiniestroDTO desplegarReporte(BigDecimal id)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);		
	}
	
	public List<ReporteSiniestroDTO> listarTodos()
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.findAll();
	}
	
	public List<ReporteSiniestroDTO> listarFiltrado(ReporteSiniestroDTO reporteSiniestroDTO)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(reporteSiniestroDTO);		
	}
	
	public List<ReporteSiniestroDTO> listarReportesAbiertos()
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarReportesAbiertos();		
	}
	
	public List<ReporteSiniestroDTO> listarFiltradoPorNumPoliza(String idPolizas)
		throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltradoPorNumPoliza(idPolizas);
	}
	
	public List<ReporteSiniestroDTO> listarReportesPorPropiedad(String propertyName,Object value)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByProperty(propertyName,value);
	}
	
	public List<ReporteSiniestroDTO> reportesSinInformePreliminar()
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.reportesSinInformePreliminar();
	}

}
