package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;

public class AsignacionAjustadorWrapper {
	
	private AjustadorDTO ajustadorSugerido;
	
	private List<AjustadorDTO> listaAsignacion;

	public AjustadorDTO getAjustadorSugerido() {
		return ajustadorSugerido;
	}

	public void setAjustadorSugerido(AjustadorDTO ajustadorSugerido) {
		this.ajustadorSugerido = ajustadorSugerido;
	}

	public List<AjustadorDTO> getListaAsignacion() {
		return listaAsignacion;
	}

	public void setListaAsignacion(List<AjustadorDTO> listaAsignacion) {
		this.listaAsignacion = listaAsignacion;
	}

}
