package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.siniestro.CoberturasRiesgoPoliza;
import mx.com.afirme.midas.siniestro.IncisosPoliza;
import mx.com.afirme.midas.siniestro.SubIncisosSeccionPoliza;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CoberturaRiesgoForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	
	private String idToReporteSiniestro;
	private String idToPoliza;
	private String numeroPoliza;
	private String[] coberturasRiesgo = {};
	@SuppressWarnings("unchecked")
	private List listarCoberturasRiesgo;
	private String seleccionados;
	private String analisisInterno;
	
	private String polizaNumeroEndoso;
	private String numeroInciso;
	private List<IncisosPoliza> listaIncisosPoliza;
    private List<SubIncisosSeccionPoliza> listaSeccionSubInciso;
    private String[] subIncisoSeccion = {};
    private List<CoberturasRiesgoPoliza> listaCoberturasRiesgo;
    private String[] coberturasRiesgoAfectas = {};
    
    private String calleBuscar;
    private String codigoPostalBuscar;
    private String idColoniaBuscar;
    private String idCiudadBuscar;
    private String idEstadoBuscar; 
    
    private String pantallaOrigen;
    private boolean permiteModificar; 
	
	
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	/**
	 * @return the idToPoliza
	 */
	public String getIdToPoliza() {
		return idToPoliza;
	}
	/**
	 * @param idToPoliza the idToPoliza to set
	 */
	public void setIdToPoliza(String idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the coberturasRiesgo
	 */
	public String[] getCoberturasRiesgo() {
		return coberturasRiesgo;
	}
	/**
	 * @param coberturasRiesgo the coberturasRiesgo to set
	 */
	public void setCoberturasRiesgo(String[] coberturasRiesgo) {
		this.coberturasRiesgo = coberturasRiesgo;
	}
	/**
	 * @return the listarCoberturasRiesgo
	 */
	@SuppressWarnings("unchecked")
	public List getListarCoberturasRiesgo() {
		return listarCoberturasRiesgo;
	}
	/**
	 * @param listarCoberturasRiesgo the listarCoberturasRiesgo to set
	 */
	@SuppressWarnings("unchecked")
	public void setListarCoberturasRiesgo(List listarCoberturasRiesgo) {
		this.listarCoberturasRiesgo = listarCoberturasRiesgo;
	}
	/**
	 * @return the seleccionados
	 */
	public String getSeleccionados() {
		return seleccionados;
	}
	/**
	 * @param seleccionados the seleccionados to set
	 */
	public void setSeleccionados(String seleccionados) {
		this.seleccionados = seleccionados;
	}
	/**
	 * @return the analisisInterno
	 */
	public String getAnalisisInterno() {
		return analisisInterno;
	}
	/**
	 * @param analisisInterno the analisisInterno to set
	 */
	public void setAnalisisInterno(String analisisInterno) {
		this.analisisInterno = analisisInterno;
	}
	/**
	 * @return the polizaNumeroEndoso
	 */
	public String getPolizaNumeroEndoso() {
		return polizaNumeroEndoso;
	}
	/**
	 * @param polizaNumeroEndoso the polizaNumeroEndoso to set
	 */
	public void setPolizaNumeroEndoso(String polizaNumeroEndoso) {
		this.polizaNumeroEndoso = polizaNumeroEndoso;
	}
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the listaIncisosPoliza
	 */
	public List<IncisosPoliza> getListaIncisosPoliza() {
		return listaIncisosPoliza;
	}
	/**
	 * @param listaIncisosPoliza the listaIncisosPoliza to set
	 */
	public void setListaIncisosPoliza(List<IncisosPoliza> listaIncisosPoliza) {
		this.listaIncisosPoliza = listaIncisosPoliza;
	}
	/**
	 * @return the listaSeccionSubInciso
	 */
	public List<SubIncisosSeccionPoliza> getListaSeccionSubInciso() {
		return listaSeccionSubInciso;
	}
	/**
	 * @param listaSeccionSubInciso the listaSeccionSubInciso to set
	 */
	public void setListaSeccionSubInciso(
			List<SubIncisosSeccionPoliza> listaSeccionSubInciso) {
		this.listaSeccionSubInciso = listaSeccionSubInciso;
	}
	/**
	 * @return the subIncisoSeccion
	 */
	public String[] getSubIncisoSeccion() {
		return subIncisoSeccion;
	}
	/**
	 * @param subIncisoSeccion the subIncisoSeccion to set
	 */
	public void setSubIncisoSeccion(String[] subIncisoSeccion) {
		this.subIncisoSeccion = subIncisoSeccion;
	}
	/**
	 * @return the listaCoberturasRiesgo
	 */
	public List<CoberturasRiesgoPoliza> getListaCoberturasRiesgo() {
		return listaCoberturasRiesgo;
	}
	/**
	 * @param listaCoberturasRiesgo the listaCoberturasRiesgo to set
	 */
	public void setListaCoberturasRiesgo(
			List<CoberturasRiesgoPoliza> listaCoberturasRiesgo) {
		this.listaCoberturasRiesgo = listaCoberturasRiesgo;
	}
	/**
	 * @return the calleBuscar
	 */
	public String getCalleBuscar() {
		return calleBuscar;
	}
	/**
	 * @param calleBuscar the calleBuscar to set
	 */
	public void setCalleBuscar(String calleBuscar) {
		this.calleBuscar = calleBuscar;
	}
	/**
	 * @return the codigoPostalBuscar
	 */
	public String getCodigoPostalBuscar() {
		return codigoPostalBuscar;
	}
	/**
	 * @param codigoPostalBuscar the codigoPostalBuscar to set
	 */
	public void setCodigoPostalBuscar(String codigoPostalBuscar) {
		this.codigoPostalBuscar = codigoPostalBuscar;
	}
	/**
	 * @return the idColoniaBuscar
	 */
	public String getIdColoniaBuscar() {
		return idColoniaBuscar;
	}
	/**
	 * @param idColoniaBuscar the idColoniaBuscar to set
	 */
	public void setIdColoniaBuscar(String idColoniaBuscar) {
		this.idColoniaBuscar = idColoniaBuscar;
	}
	/**
	 * @return the idCiudadBuscar
	 */
	public String getIdCiudadBuscar() {
		return idCiudadBuscar;
	}
	/**
	 * @param idCiudadBuscar the idCiudadBuscar to set
	 */
	public void setIdCiudadBuscar(String idCiudadBuscar) {
		this.idCiudadBuscar = idCiudadBuscar;
	}
	/**
	 * @return the idEstadoBuscar
	 */
	public String getIdEstadoBuscar() {
		return idEstadoBuscar;
	}
	/**
	 * @param idEstadoBuscar the idEstadoBuscar to set
	 */
	public void setIdEstadoBuscar(String idEstadoBuscar) {
		this.idEstadoBuscar = idEstadoBuscar;
	}
	/**
	 * @return the coberturasRiesgoAfectas
	 */
	public String[] getCoberturasRiesgoAfectas() {
		return coberturasRiesgoAfectas;
	}
	/**
	 * @param coberturasRiesgoAfectas the coberturasRiesgoAfectas to set
	 */
	public void setCoberturasRiesgoAfectas(String[] coberturasRiesgoAfectas) {
		this.coberturasRiesgoAfectas = coberturasRiesgoAfectas;
	}
	/**
	 * @return the pantallaOrigen
	 */
	public String getPantallaOrigen() {
		return pantallaOrigen;
	}
	/**
	 * @param pantallaOrigen the pantallaOrigen to set
	 */
	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}
	/**
	 * @return the permiteModificar
	 */
	public boolean isPermiteModificar() {
		return permiteModificar;
	}
	/**
	 * @param permiteModificar the permiteModificar to set
	 */
	public void setPermiteModificar(boolean permiteModificar) {
		this.permiteModificar = permiteModificar;
	}
	
}
