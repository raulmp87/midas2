package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.PreguntasEspecialesDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PreguntasEspecialesDN {

	private static final PreguntasEspecialesDN INSTANCIA = new PreguntasEspecialesDN();

	public static PreguntasEspecialesDN getInstancia() {
		return INSTANCIA;
	}
	
	public void agregarPreguntaEspecial(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos, SystemException {
		PreguntasEspecialesSN preguntaEspecialSN = new PreguntasEspecialesSN();
		preguntaEspecialSN.agregar(preguntasEspecialesDTO);
	}

	public void borrarPreguntaEspecial(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos, SystemException {
		PreguntasEspecialesSN preguntaEspecialSN = new PreguntasEspecialesSN();
		preguntaEspecialSN.borrar(preguntasEspecialesDTO);
	}
	
	public void modificarPreguntaEspecial(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos, SystemException {
		PreguntasEspecialesSN preguntaEspecialSN = new PreguntasEspecialesSN();
		preguntaEspecialSN.modificar(preguntasEspecialesDTO);
	}
	
	public List<PreguntasEspecialesDTO> mostrarPreguntasEspeciales() throws ExcepcionDeAccesoADatos, SystemException {
		PreguntasEspecialesSN preguntaEspecialSN = new PreguntasEspecialesSN();
		return preguntaEspecialSN.listarTodos();
	}
	
	public PreguntasEspecialesDTO obtienePreguntaEspecialPorId(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos, SystemException {
		PreguntasEspecialesSN preguntaEspecialSN = new PreguntasEspecialesSN();
		return preguntaEspecialSN.getPorId(preguntasEspecialesDTO);
	}

	public List<PreguntasEspecialesDTO> obtienePreguntaEspecialPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos, SystemException {
		PreguntasEspecialesSN preguntaEspecialSN = new PreguntasEspecialesSN();
		return preguntaEspecialSN.getPorPropiedad(propiedad,valor);
	}

	
}
