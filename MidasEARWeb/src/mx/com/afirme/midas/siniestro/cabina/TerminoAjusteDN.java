package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TerminoAjusteDN {
	public static final TerminoAjusteDN INSTANCIA = new TerminoAjusteDN();
	
	public static TerminoAjusteDN getInstancia (){
		return TerminoAjusteDN.INSTANCIA;
	}
	
	public TerminoAjusteDTO getPorId(TerminoAjusteDTO terminoAjusteDTO) throws ExcepcionDeAccesoADatos, SystemException{
		TerminoAjusteSN terminoAjusteSN = new TerminoAjusteSN();
		return terminoAjusteSN.getPorId(terminoAjusteDTO);
	}
	
	public List<TerminoAjusteDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		TerminoAjusteSN terminoAjusteSN = new TerminoAjusteSN();
		return terminoAjusteSN.listarTodos();
	}

}
