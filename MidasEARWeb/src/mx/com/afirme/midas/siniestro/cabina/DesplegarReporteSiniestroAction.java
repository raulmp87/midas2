package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.consultacobranzapoliza.ConsultaCobranzaPolizaDN;
import mx.com.afirme.midas.interfaz.consultacobranzapoliza.DatosCobranzaPolizaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DesplegarReporteSiniestroAction extends MidasMappingDispatchAction{

	/**
	 * Method desplegarDatosReporteSiniestro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward desplegarDatosReporteSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

//		int idReporte = 383;
		BigDecimal idToReporteSiniestro = request.getParameter("idToReporteSiniestro")!= null?new BigDecimal(request.getParameter("idToReporteSiniestro")):new BigDecimal(0);
		String reglaNavegacion = Sistema.EXITOSO;
		Date now = Calendar.getInstance().getTime();		
		String fecha = UtileriasWeb.getFechaString(now);		
		String hora = UtileriasWeb.getHoraString(now);
		ReporteSiniestroForm reporteSiniestroForm = (ReporteSiniestroForm) form;
		reporteSiniestroForm.setFechaReporte(fecha);
		reporteSiniestroForm.setHoraReporte(hora);
		reporteSiniestroForm.setFechaAsignacionAjustador(fecha);
		ReporteSiniestroDN  reporteSiniestroDN =  ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			ReporteSiniestroDTO   reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
			this.poblarForm(reporteSiniestroDTO, reporteSiniestroForm);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method poblarForm
	 * 
	 * @param ReporteSiniestroDTO
	 * @param ReporteSiniestroForm
	 * @throws ParseException 
	 */
	private void poblarForm(ReporteSiniestroDTO reporteSiniestroDTO,
			ReporteSiniestroForm reporteSiniestroForm) throws SystemException, ExcepcionDeAccesoADatos, ParseException{

		if (reporteSiniestroDTO.getCalle() != null)
			reporteSiniestroForm.setCalle(reporteSiniestroDTO.getCalle());
		
		if (reporteSiniestroDTO.getColonia() != null){
			reporteSiniestroForm.setIdColonia(reporteSiniestroDTO.getColonia());
			try{
				CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
				ColoniaDTO coloniaDTO = codigoPostalDN.getColoniaPorId(reporteSiniestroDTO.getColonia());
				if(coloniaDTO != null){
					reporteSiniestroForm.setDescripcionColonia(coloniaDTO.getColonyName());
				}
			}catch(ExcepcionDeAccesoADatos edad){
				reporteSiniestroForm.setDescripcionEstado("* Error de acceso *");
			}
		}
		
		if (reporteSiniestroDTO.getEventoCatastrofico() != null){
			
			reporteSiniestroForm.setDescripcionEventoCatastrofico(reporteSiniestroDTO.getEventoCatastrofico().getDescripcionEvento());
		}
		
		if (reporteSiniestroDTO.getDescripcionSiniestro() != null)
			reporteSiniestroForm.setDescripcionSiniestro(reporteSiniestroDTO.getDescripcionSiniestro());
		
		if (reporteSiniestroDTO.getFechaHoraReporte() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaHoraReporte());
			String hora = UtileriasWeb.getHoraString(reporteSiniestroDTO.getFechaHoraReporte());
			reporteSiniestroForm.setFechaReporte(fecha);
			reporteSiniestroForm.setHoraReporte(hora);			
		}
		
		if (reporteSiniestroDTO.getFechaSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaSiniestro());
			reporteSiniestroForm.setFechaSiniestro(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaAsignacionAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaAsignacionAjustador());
			reporteSiniestroForm.setFechaAsignacionAjustador(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaCerradoSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaCerradoSiniestro());
			reporteSiniestroForm.setFechaCerradoSiniestro(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaContactoAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaContactoAjustador());
			reporteSiniestroForm.setFechaContactoAjustador(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaDocumento() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaDocumento());
			reporteSiniestroForm.setFechaDocumento(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaInformeFinal() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaInformeFinal());
			reporteSiniestroForm.setFechaInformeFinal(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaInspeccionAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaInspeccionAjustador());
			reporteSiniestroForm.setFechaInspeccionAjustador(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaLlegadaAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaLlegadaAjustador());
			reporteSiniestroForm.setFechaLlegadaAjustador(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaPreliminar() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaPreliminar());
			reporteSiniestroForm.setFechaPreliminar(fecha);
		}
		
		if (reporteSiniestroDTO.getFechaTerminadoSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaTerminadoSiniestro());
			reporteSiniestroForm.setFechaTerminadoSiniestro(fecha);
		}
		
		
		if (reporteSiniestroDTO.getAjustador() != null && reporteSiniestroDTO.getAjustador().getIdTcAjustador() != null) {
			reporteSiniestroForm.setIdAjustador(reporteSiniestroDTO.getAjustador().getIdTcAjustador().toString());
			reporteSiniestroForm.setNombreAjustador(reporteSiniestroDTO.getAjustador().getNombre());
		}
		
		if (reporteSiniestroDTO.getIdCiudad() != null){
			reporteSiniestroForm.setIdCiudad(reporteSiniestroDTO.getIdCiudad());
		}
		
		if (reporteSiniestroDTO.getIdCoordinador() != null){
			reporteSiniestroForm.setIdCoordinador(reporteSiniestroDTO.getIdCoordinador().toString());
			reporteSiniestroForm.setNombreCoordinador("");
		}
		
		if (reporteSiniestroDTO.getIdEstado() != null){
			reporteSiniestroForm.setIdEstado(reporteSiniestroDTO.getIdEstado());
		}
		
		if (reporteSiniestroDTO.getIdToReporteSiniestro() != null)
			reporteSiniestroForm.setIdToReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro().toString());
		
		if (reporteSiniestroDTO.getNombreContacto() != null)
			reporteSiniestroForm.setNombreContacto(reporteSiniestroDTO.getNombreContacto());
		
		if (reporteSiniestroDTO.getNombrePersonaReporta() != null)
			reporteSiniestroForm.setNombrePersonaReporta(reporteSiniestroDTO.getNombrePersonaReporta());
		
		if (reporteSiniestroDTO.getNumeroPoliza() != null){
			reporteSiniestroForm.setIdToPoliza(reporteSiniestroDTO.getNumeroPoliza().toString());
			
			SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
//			PolizaSoporteDanosDTO polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza());
			PolizaSoporteDanosDTO polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(),reporteSiniestroDTO.getFechaSiniestro());
			
			reporteSiniestroForm.setTipoNegocio(polizaSoporteDanosDTO.getCodigoTipoNegocio().toString());
			reporteSiniestroForm.setProductoTransporte(polizaSoporteDanosDTO.isProductoTransporte());
			reporteSiniestroForm.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
//			reporteSiniestroForm.setNumeroPoliza(reporteSiniestroDTO.getNumeroPoliza().toString());
//			
//			PolizaDummyDN pdumy = PolizaDummyDN.getInstancia();
//			PolizaDummyDTO poDto = pdumy.obtenerPoliza(reporteSiniestroDTO.getNumeroPoliza());
//			if(poDto.getTipoPolizaDTO() != null){
//				TipoPolizaDTO tpoliza = poDto.getTipoPolizaDTO();
//				if(tpoliza.getIdToTipoPoliza() != null)
//					reporteSiniestroForm.setTipoNegocio(tpoliza.getIdToTipoPoliza().toString());
//				else{
//					reporteSiniestroForm.setTipoPoliza("");
//				}
//			}else{
//				reporteSiniestroForm.setTipoPoliza("");
//			}
			
//			reporteSiniestroForm.setTipoNegocio(poDto.get.getTipoNegocio().toString());
//			reporteSiniestroForm.setClaveProducto("");
//			reporteSiniestroForm.setNombreAsegurado("");
//			reporteSiniestroForm.setDireccionAsegurado("");
		}else{
			reporteSiniestroForm.setNumeroPoliza(null);
			reporteSiniestroForm.setTipoPoliza("");
			reporteSiniestroForm.setTipoNegocio("");
		}
		
		if (reporteSiniestroDTO.getNumeroReporte() != null)
			reporteSiniestroForm.setNumeroReporte(reporteSiniestroDTO.getNumeroReporte());
		
		if (reporteSiniestroDTO.getObservaciones() != null)
			reporteSiniestroForm.setObservaciones(reporteSiniestroDTO.getObservaciones());
		
		if (reporteSiniestroDTO.getTelefonoContacto() != null)
			reporteSiniestroForm.setTelefonoContacto(reporteSiniestroDTO.getTelefonoContacto().toString());
		
		if (reporteSiniestroDTO.getTelefonoPersonaReporta() != null)
			reporteSiniestroForm.setTelefonoPersonaReporta(reporteSiniestroDTO.getTelefonoPersonaReporta().toString());				
		
		if (reporteSiniestroDTO.getTerminoAjuste() != null && reporteSiniestroDTO.getTerminoAjuste().getIdTcTerminoAjuste()!=null){
			reporteSiniestroForm.setIdTerminoAjuste(reporteSiniestroDTO.getTerminoAjuste().getIdTcTerminoAjuste().toString());			
			reporteSiniestroForm.setDescripcionTerminoAjuste(reporteSiniestroDTO.getTerminoAjuste().getDescripcion());											
		}
		
		if(reporteSiniestroDTO.getReporteEstatus() !=null){
			if(reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus() != null){
				reporteSiniestroForm.setIdStatus(reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().toString());
			}
		}
		
		if(reporteSiniestroDTO.getCodigoPostal() !=null){
			reporteSiniestroForm.setCodigoPostal(reporteSiniestroDTO.getCodigoPostal().toString());
		}
		
		if(reporteSiniestroDTO.getIdCoordinador() !=null){
			
			Usuario coordinador = UsuarioWSDN.getInstancia().obtieneUsuario(reporteSiniestroDTO.getIdCoordinador().intValue());
			
			if(coordinador != null){
				reporteSiniestroForm.setNombreCoordinador(coordinador.getNombreUsuario());
			}else{
				reporteSiniestroForm.setNombreCoordinador("");
			}															
		}		
	}
	
/* ***************************** Datos de la Poliza *************************** */	

	/**
	 * Method desplegarDatosPoliza
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public ActionForward desplegarDatosPoliza(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {

//		String reglaNavegacion = Sistema.EXITOSO;
//		ReporteSiniestroDN dd 				=	ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
//		String strNumeroReporte = request.getParameter("idToReporteSiniestro");
//		BigDecimal numeroReporte 			=  new BigDecimal(new Double(strNumeroReporte));
//		ReporteSiniestroDTO dto 			= dd.desplegarReporte(numeroReporte);
//		PolizaDummyDTO	poDTO;
//	
//		PolizaDummyDN poliDN = PolizaDummyDN.getInstancia();
//		poDTO = poliDN.obtenerPoliza(dto.getNumeroPoliza());
//				
//		DesplegarDatosPolizaForm desplegarDatosPolizaForm 	= (DesplegarDatosPolizaForm) form;
//		poblarFormPoliza(poDTO,desplegarDatosPolizaForm);
//		
//		if(dto.getNumeroPoliza()!=null){
//			poDTO.setNumeroPoliza(dto.getNumeroPoliza());
//			desplegarDatosPolizaForm.setNumeroPoliza(dto.getNumeroPoliza().toString());
//		}
//		CodigoPostalDN dpdn = CodigoPostalDN.getInstancia();
//		desplegarDatosPolizaForm.setNombreAsegurado(dto.getNombrePersonaReporta());
//		
//		EstadoDTO estado 	= (dto.getIdEstado()!=null)?dpdn.getEstadoPorId(dto.getIdEstado()):new EstadoDTO(); 
//		CiudadDTO ciudad 	= (dto.getIdCiudad()!=null)?dpdn.getCiudadPorId(dto.getIdCiudad()): new CiudadDTO();
//		ColoniaDTO colonia	= (dto.getIdColonia()!=null)?dpdn.getColoniaPorId(dto.getIdColonia()):new ColoniaDTO();
//		
//		String direccion = (estado.getDescription()!=null)?estado.getDescription():""+" ";
//		direccion+= (ciudad.getDescription()!=null)?ciudad.getDescription():""+" ";
//		direccion+= (colonia.getDescription()!=null)?colonia.getDescription():""+" ";
//		direccion+= (dto.getCalle()!=null)?dto.getCalle():"";
//		desplegarDatosPolizaForm.setDireccionAsegurado(direccion);
//		
//		try {
//			
////			PolizaDummyDTO polizaDTO = new PolizaDummyDTO();
////			polizaDTO.setNumeroPoliza(d);
//			request.setAttribute("DesplegarRiesgoPolizas",PolizaDummyDN.getInstancia().listarRiesgosPoliza(poDTO));
//		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
//		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
//		}
		String reglaNavegacion = Sistema.EXITOSO;
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN =ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ConsultaCobranzaPolizaDN consultaCobranzaPolizaDN = ConsultaCobranzaPolizaDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DesplegarDatosPolizaForm desplegarDatosPolizaForm 	= (DesplegarDatosPolizaForm) form;
		
		String strNumeroReporte = request.getParameter("idToReporteSiniestro");
		ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(new BigDecimal(strNumeroReporte));
		String fechaSiniestro = request.getParameter("fechaSiniestro");
//		PolizaSoporteDanosDTO polizaSoporteDanosDTO =  soporteDanosDN.getDetallePoliza(reporteSiniestroDTO.getNumeroPoliza());
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(),UtileriasWeb.getFechaFromString(fechaSiniestro));
		
		desplegarDatosPolizaForm.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
		desplegarDatosPolizaForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
//		desplegarDatosPolizaForm.setPolizaNombreAsegurado(polizaSoporteDanosDTO.getNombreAsegurado());
		desplegarDatosPolizaForm.setDireccionAsegurado(polizaSoporteDanosDTO.getCalleAsegurado());
		desplegarDatosPolizaForm.setTipoMoneda(polizaSoporteDanosDTO.getDescripcionMoneda());
		if(polizaSoporteDanosDTO.getFechaPago()!=null){
			desplegarDatosPolizaForm.setFecPago(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaPago()));
		}
		desplegarDatosPolizaForm.setClaveOficina(polizaSoporteDanosDTO.getNombreOficina());
//		if(polizaSoporteDanosDTO.getImportePagado() > 0){
//			desplegarDatosPolizaForm.setImportePagado(String.valueOf(polizaSoporteDanosDTO.getImportePagado()));
//		}
		if(polizaSoporteDanosDTO.getCodigoProducto() >0){
			desplegarDatosPolizaForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
		}
//		if(polizaSoporteDanosDTO.getSaldoPendiente() >0){
//			desplegarDatosPolizaForm.setSaldoPendiente(String.valueOf(polizaSoporteDanosDTO.getSaldoPendiente()));
//		}
		desplegarDatosPolizaForm.setFormaPago(polizaSoporteDanosDTO.getDescripcionFormaPago());
//		if(polizaSoporteDanosDTO.getSaldoVencido() >0){
//			desplegarDatosPolizaForm.setSaldoVencido(String.valueOf(polizaSoporteDanosDTO.getSaldoVencido()));
//		}
//		desplegarDatosPolizaForm.setUltimoReciboPagado(polizaSoporteDanosDTO.getUltimoReciboPagado());
		if(polizaSoporteDanosDTO.getFechaEmision() != null){
			desplegarDatosPolizaForm.setFecEmision(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaEmision()));
		}
		if(polizaSoporteDanosDTO.getFechaInicioVigencia() != null){
			desplegarDatosPolizaForm.setFecVigencia(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaInicioVigencia()));
		}
		if(polizaSoporteDanosDTO.getFechaFinVigencia() != null){
			desplegarDatosPolizaForm.setAl(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaFinVigencia()));
		}
		if(polizaSoporteDanosDTO.getCodigoTipoNegocio() != null){
			desplegarDatosPolizaForm.setCodigoTipoNegocio(polizaSoporteDanosDTO.getCodigoTipoNegocio().toString());
		}
//		Agregados de la poliza
//		desplegarDatosPolizaForm.setPolizaNumeroEndoso(String.valueOf(polizaSoporteDanosDTO.getNumeroUltimoEndoso()));
		desplegarDatosPolizaForm.setPolizaNumeroEndoso(String.valueOf(polizaSoporteDanosDTO.getNumeroEndosoVigente()));
		if(polizaSoporteDanosDTO.getCodigoAgente() != null){
			desplegarDatosPolizaForm.setPolizaIdAgente(polizaSoporteDanosDTO.getCodigoAgente().toString());
		}
		if(polizaSoporteDanosDTO.getNombreAgente() != null){
			desplegarDatosPolizaForm.setPolizaNombreAgente(polizaSoporteDanosDTO.getNombreAgente());
		}
		if(polizaSoporteDanosDTO.getNombreOficina() != null){
			desplegarDatosPolizaForm.setPolizaNombreOficina(polizaSoporteDanosDTO.getNombreOficina());
		}
		if(polizaSoporteDanosDTO.getNombreAsegurado() != null){
			desplegarDatosPolizaForm.setPolizaNombreAsegurado(polizaSoporteDanosDTO.getNombreAsegurado());
		}
		if(polizaSoporteDanosDTO.getIdAsegurado() != null){
			desplegarDatosPolizaForm.setPolizaIdCliente(polizaSoporteDanosDTO.getIdAsegurado().toString());
		}
//		Fin de Agregados
		if(polizaSoporteDanosDTO.getTelefonoAsegurado() != null){
			desplegarDatosPolizaForm.setTelefonoAsegurado(polizaSoporteDanosDTO.getTelefonoAsegurado());
		}
		if(polizaSoporteDanosDTO.getNombreContratante() != null){
			desplegarDatosPolizaForm.setNombreContratante(polizaSoporteDanosDTO.getNombreContratante());
		}
		if(polizaSoporteDanosDTO.getCalleContratante() != null){
			desplegarDatosPolizaForm.setDireccionContratante(polizaSoporteDanosDTO.getCalleContratante());
		}
		if(polizaSoporteDanosDTO.getTelefonoContratante() != null){
			desplegarDatosPolizaForm.setTelefonoContratante(polizaSoporteDanosDTO.getTelefonoContratante());
		}
		
		DatosCobranzaPolizaDTO datosCobranzaPolizaDTO = consultaCobranzaPolizaDN.consultaCobranzaPoliza(reporteSiniestroDTO.getNumeroPoliza());
		if(datosCobranzaPolizaDTO != null){
			if(datosCobranzaPolizaDTO.getUltimoReciboPagado()!= null){
				desplegarDatosPolizaForm.setUltimoReciboPagado(datosCobranzaPolizaDTO.getUltimoReciboPagado().toString());
			}else{
				desplegarDatosPolizaForm.setUltimoReciboPagado("");
			}
			if(datosCobranzaPolizaDTO.getMontoVencido()!= null){
				desplegarDatosPolizaForm.setSaldoVencido(datosCobranzaPolizaDTO.getMontoVencido().toString());
			}else{
				desplegarDatosPolizaForm.setSaldoVencido("");
			}
			if(datosCobranzaPolizaDTO.getMontoPorVencer()!= null){
				desplegarDatosPolizaForm.setSaldoPendiente(datosCobranzaPolizaDTO.getMontoPorVencer().toString());
			}else{
				desplegarDatosPolizaForm.setSaldoPendiente("");
			}
			if(datosCobranzaPolizaDTO.getMontoPagado()!= null){
				desplegarDatosPolizaForm.setImportePagado(datosCobranzaPolizaDTO.getMontoPagado().toString());
			}else{
				desplegarDatosPolizaForm.setImportePagado("");
			}
			if(datosCobranzaPolizaDTO.getFechaUltimoPago()!= null){
				desplegarDatosPolizaForm.setFecPago(UtileriasWeb.getFechaString(datosCobranzaPolizaDTO.getFechaUltimoPago()));
			}else{
				desplegarDatosPolizaForm.setFecPago("");
			}
			if(datosCobranzaPolizaDTO.getSituacion()!= null){
				desplegarDatosPolizaForm.setEstatusCobranza(datosCobranzaPolizaDTO.getSituacion());
			}else{
				desplegarDatosPolizaForm.setEstatusCobranza("");
			}
			
		}
		
		request.setAttribute("riesgoPolizas",polizaSoporteDanosDTO.getListaDetallesPolizaSiniestros());
		
		return mapping.findForward(reglaNavegacion);
	}

//	private void poblarFormPoliza(PolizaDummyDTO poliDTO,DesplegarDatosPolizaForm poliForm) {
//		if (poliDTO.getNumeroPoliza() != null){
//			poliForm.setNumeroPoliza(poliDTO.getNumeroPoliza().toString());
//		}
//
//		if (poliDTO.getClaveOficina() != null){
//			poliForm.setClaveOficina(poliDTO.getClaveOficina());
//		}
//		
//		if (poliDTO.getFormaPago() != null){
//			poliForm.setFormaPago(poliDTO.getFormaPago());
//		}
//		
//		if (poliDTO.getUltimoReciboPagado() != null){
//			poliForm.setUltimoReciboPagado(poliDTO.getUltimoReciboPagado());
//		}
//		
//		if (poliDTO.getFechaPago() != null){
//			String fecha = UtileriasWeb.getFechaString(poliDTO.getFechaPago());
//			poliForm.setFecPago(fecha);
//		}
//		
//		if (poliDTO.getImportePagado() != null){
//			poliForm.setImportePagado(poliDTO.getImportePagado().toString());
//		}
//		
//		if (poliDTO.getSaldoPendiente() != null){
//			poliForm.setSaldoPendiente(poliDTO.getSaldoPendiente().toString());
//		}
//		
//		if (poliDTO.getSaldoVencido() != null){
//			poliForm.setSaldoVencido(poliDTO.getSaldoVencido().toString());
//		}
//
//		if (poliDTO.getFechaEmision() != null){
//			String fecha = UtileriasWeb.getFechaString(poliDTO.getFechaEmision());
//			poliForm.setFecEmision(fecha);
//		}
//		if (poliDTO.getIdMoneda() != null){
//			poliForm.setTipoMoneda(poliDTO.getIdMoneda().toString());
//		}
//		
//		if(poliDTO.getFechaInicioVigencia() != null ){
//			poliForm.setFecVigencia(UtileriasWeb.getFechaHoraString(poliDTO.getFechaInicioVigencia()));
//		}
//		if(poliDTO.getFechaFinVigencia() != null ){
//			poliForm.setAl(UtileriasWeb.getFechaHoraString(poliDTO.getFechaInicioVigencia()));
//		}
//	}
	
	
	
/* &&&&&&&&&&&&&&&&&& Datos del Estado Del Siniestro &&&&&&&&&&&&&&&&&& */
	
	/**
	 * Method desplegarDatosEstadoSiniestro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward desplegarDatosEstadoSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		ReporteSiniestroDN reporteSiniestroDN 			= ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String strNumeroReporte = request.getParameter("idToReporteSiniestro");
		BigDecimal numeroReporte 			=  new BigDecimal(new Double(strNumeroReporte));
		ReporteSiniestroDTO dto;
		EstadoSiniestroForm estaForm 	= (EstadoSiniestroForm) form;
		try {
			dto = reporteSiniestroDN.desplegarReporte(numeroReporte);
			
			poblarFormEstadoSiniestro(dto,estaForm);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	
	
	private void poblarFormEstadoSiniestro(ReporteSiniestroDTO estaDTO,EstadoSiniestroForm estaForm) {
		
		//String [] descripcionAjuste = {"Desestimiento","Indocumentado","Legal","Orden a Tercero","Pendiente","Poliza Cancelada","Rechazo","Reporte Cancelado","Sin costo","Sin firma de convenio","Solo orden a N/A"};
		if(estaDTO.getFechaAsignacionAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaAsignacionAjustador());
			estaForm.setFechaAsignacion(fecha);
		}
		
		if(estaDTO.getFechaContactoAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaContactoAjustador());
			estaForm.setFechaContactoAjustador(fecha);
		}
		
		if(estaDTO.getFechaLlegadaAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaLlegadaAjustador());
			estaForm.setFechaLlegadaAjustador(fecha);
		}
		
		if(estaDTO.getFechaInspeccionAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaInspeccionAjustador());
			estaForm.setFechaInspeccionAjustador(fecha);
		}
		
		if(estaDTO.getFechaPreliminar() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaPreliminar());
			estaForm.setFechaPreliminar(fecha);
		}

		if(estaDTO.getFechaDocumento() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaDocumento());
			estaForm.setFechaDocumento(fecha);
		}

		if(estaDTO.getFechaInformeFinal() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaInformeFinal());
			estaForm.setFechaInformeFinal(fecha);
		}
		
		if(estaDTO.getFechaSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaTerminadoSiniestro());
			estaForm.setFechaTerminacionSiniestro(fecha);
		}
		
		if(estaDTO.getFechaCerradoSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(estaDTO.getFechaCerradoSiniestro());
			estaForm.setFechaCerrado(fecha);
		}
		
		if(estaDTO.getDescripcionSiniestro() != null){
			estaForm.setDescripcionSiniestro(estaDTO.getDescripcionSiniestro());
		}
		
		if(estaDTO.getIdCabinero() != null){
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(estaDTO.getIdCabinero().intValue());
			if(usuario != null){
				estaForm.setDescripcionCabinero(usuario.getNombreUsuario());
			}else{
				estaForm.setDescripcionCabinero("");
			}
		}
		
		if(estaDTO.getReporteEstatus() != null){
			estaForm.setEstatus(estaDTO.getReporteEstatus().getDescripcion());
		}
		
		if(estaDTO.getTerminoAjuste() != null){
			estaForm.setTerminoAjuste(estaDTO.getTerminoAjuste().getDescripcion());
		}
		
		if (estaDTO.getEstatusSiniestro()!=null){
			estaForm.setIdTcEstatusSiniestro(estaDTO.getEstatusSiniestro().getIdTcEstatusSiniestro().toString());
			estaForm.setDescripcionEstatusSiniestro(estaDTO.getEstatusSiniestro().getDescripcion());
		}
		
		if (estaDTO.getSubEstatusSiniestro()!=null){
			estaForm.setIdTcSubEstatusSiniestro(estaDTO.getSubEstatusSiniestro().getIdTcSubEstatusSiniestro().toString());
			estaForm.setDescripcionSubEstatusSiniestro(estaDTO.getSubEstatusSiniestro().getDescripcion());
		}
		
		try{
			EstatusSiniestroDN estatusSiniestroDN = EstatusSiniestroDN.getInstancia();
			SubEstatusSiniestroDN subEstatusSiniestroDN = SubEstatusSiniestroDN.getInstancia();
			TerminoAjusteDN terminoAjusteDN = TerminoAjusteDN.getInstancia();
			
			List<EstatusSiniestroDTO> listaEstatusSiniestro =  estatusSiniestroDN.listarTodos();
			estaForm.setListaEstatusSiniestro(listaEstatusSiniestro);
			
			List<SubEstatusSiniestroDTO> listaSubEstatusSiniestro = subEstatusSiniestroDN.listarTodos();
			estaForm.setListaSubEstatusSiniestro(listaSubEstatusSiniestro);
			
			List<TerminoAjusteDTO> listaTerminosAjuste = terminoAjusteDN.listarTodos();
			estaForm.setListaTerminosAjuste(listaTerminosAjuste);
			
		}catch (Exception e) { e.printStackTrace();	}
	}
	
/* @@@@@@@@@@@@@@@@@@@@@@@@ Datos de Preguntas Especiales @@@@@@@@@@@@@@@@@@@@@@@@ */	

	/**
	 * Method desplegarDatosPreguntasEspeciales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public ActionForward desplegarDatosPreguntasEspeciales(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		ReporteSiniestroDN reporteSiniestroDN 			=	ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		PreguntasEspecialesDN preguntasEspecialesDN = PreguntasEspecialesDN.getInstancia();
		String strNumeroReporte = request.getParameter("idToReporteSiniestro");
		BigDecimal idToReporteSiniestro 			=  new BigDecimal(new Double(strNumeroReporte));
		
		try {
			ReporteSiniestroDTO reporteSiniestroDTO	= reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
//			PreguntasEspecialesDTO	preguntasEspecialesDTO	= reporteSiniestroDTO.getPreguntasEspeciales();
			PreguntasEspecialesDTO preguntasEspecialesDTO = new PreguntasEspecialesDTO();
			preguntasEspecialesDTO.setIdToReporteSiniestro(idToReporteSiniestro);
			preguntasEspecialesDTO	=  preguntasEspecialesDN.obtienePreguntaEspecialPorId(preguntasEspecialesDTO);
//			dto = reporteSiniestroDN.desplegarReporte(numeroReporte);
//			PreguntasEspecialesDTO	peDTO	= dto.getPreguntasEspeciales();
//			PolizaDummyDN poliza = PolizaDummyDN.getInstancia();
//			PolizaDummyDTO polizaDum = poliza.obtenerPoliza(dto.getNumeroPoliza());
			
			PreguntasEspecialesForm peForm 	= (PreguntasEspecialesForm) form;
				
			if(preguntasEspecialesDTO != null){
				poblarFormPreguntasEspeciales(preguntasEspecialesDTO,peForm);
			}
			
			if(reporteSiniestroDTO.getNumeroPoliza() != null){
				peForm.setNoPoliza(reporteSiniestroDTO.getNumeroPoliza().toString());
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}

	private void poblarFormPreguntasEspeciales(PreguntasEspecialesDTO preguntaDTO, PreguntasEspecialesForm preguntaForm) {
		
		if(preguntaDTO.getCargo() != null){
			preguntaForm.setCargoOcupa(preguntaDTO.getCargo());
		}
		
		if(preguntaDTO.getCausaReclamacion() != null){
			preguntaForm.setCausaReclamacion(preguntaDTO.getCausaReclamacion());
		}
		
		if(preguntaDTO.getComentariosReclamante() != null){
			preguntaForm.setComentario(preguntaDTO.getComentariosReclamante());
		}
		
		if(preguntaDTO.getDependencia() != null){
			preguntaForm.setDependenciaTrabaja(preguntaDTO.getDependencia());
		}
		
		if(preguntaDTO.getDescripcionPuesto() != null){
			preguntaForm.setDescripcionPuesto(preguntaDTO.getDescripcionPuesto());
		}
		
		if(preguntaDTO.getEmail() != null){
			preguntaForm.setCorreo(preguntaDTO.getEmail());
		}
		
		if(preguntaDTO.getLugarOficio() != null){
			preguntaForm.setLugarHizoOficio(preguntaDTO.getLugarOficio());
		}
		
		if(preguntaDTO.getFechaReclamacion() != null){
			String fecha = UtileriasWeb.getFechaString(preguntaDTO.getFechaReclamacion());
			preguntaForm.setFechaNotificacion(fecha);
		}
				
		//big
		if(preguntaDTO.getIdToReporteSiniestro() != null){
			preguntaForm.setIdToReporteSiniestro(preguntaDTO.getIdToReporteSiniestro().toString());
		}
		
		if(preguntaDTO.getNombreDependencia() != null){
			preguntaForm.setNombreDependencia(preguntaDTO.getNombreDependencia());
		}
		
		if(preguntaDTO.getNumeroCertificado() !=  null){
			preguntaForm.setNoCertificado(preguntaDTO.getNumeroCertificado());
		}
		
		if(preguntaDTO.getNumeroExpediente() != null){
			preguntaForm.setNumeroExpediente(preguntaDTO.getNumeroExpediente());
		}
		
		if(preguntaDTO.getNumeroOficio() != null){
			preguntaForm.setNumeroOficio(preguntaDTO.getNumeroOficio());
		}

		if(preguntaDTO.getPersonaRealizoOficio() != null){
			preguntaForm.setPersonaRealizoOficio(preguntaDTO.getPersonaRealizoOficio());
		}
		
		if(preguntaDTO.getPlazoReclamacion() != null){
			preguntaForm.setPlazoReclamacion(preguntaDTO.getPlazoReclamacion().trim());
		}

		if(preguntaDTO.getTienePlazoReclamacion() != null){
			preguntaForm.setTienePlazoReclamacion(preguntaDTO.getTienePlazoReclamacion());
		}
		
		if(preguntaDTO.getServiciosProfesionales() != null){
			preguntaForm.setServiciosProfesionales(preguntaDTO.getServiciosProfesionales());
		}

		
		if(preguntaDTO.getSiniestroCubierto() != null){
			preguntaForm.setEstaCubierto(preguntaDTO.getSiniestroCubierto());
		}
		
		if(preguntaDTO.getTelefonoFuncionario() != null){
			preguntaForm.setTelefono(preguntaDTO.getTelefonoFuncionario());
		}
		
		if(preguntaDTO.getTipoCobertura() != null){
			preguntaForm.setTipoCobertura(preguntaDTO.getTipoCobertura());
		}
		
		if(preguntaDTO.getTipoDocumento() != null){
			preguntaForm.setTipoDocumentoRecibio(preguntaDTO.getTipoDocumento());
		}
		
		if(preguntaDTO.getTipoAvisoDetalle() != null){
			preguntaForm.setDetallar(preguntaDTO.getTipoAvisoDetalle());
		}
		//Agregados 
		if(preguntaDTO.getNombrefuncionario() != null){
			preguntaForm.setNomFuncionario(preguntaDTO.getNombrefuncionario());
		}
		if(preguntaDTO.getCalle() != null){
			preguntaForm.setDireccionFuncionario(preguntaDTO.getCalle());
		}
		if(preguntaDTO.getIdestado() != null){
			preguntaForm.setIdEstado(preguntaDTO.getIdestado());
		}
		if(preguntaDTO.getIdciudad() != null){
			preguntaForm.setIdCiudad(preguntaDTO.getIdciudad());
		}
		if(preguntaDTO.getIdcolonia() != null){
			preguntaForm.setIdColonia(preguntaDTO.getIdcolonia());
		}
		if(preguntaDTO.getCodigopostal() != null){
			preguntaForm.setCodigoPostal(preguntaDTO.getCodigopostal().toString());
		}
	}
	
/* ^^^^^^^^^^^^^^^^^^^^^^^^^ Datos de Transporte ^^^^^^^^^^^^^^^^^^^^^^^^^ */	
	
	/**
	 * Method desplegarDatosTransporte
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward desplegarDatosTransporte(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)  throws SystemException,
			ExcepcionDeAccesoADatos {
		
		String reglaNavegacion = Sistema.EXITOSO;
		ReporteSiniestroDN dd 			= ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DatosTransportistaDN datosTransportistaDN = DatosTransportistaDN.getInstancia();
		String strNumeroReporte = request.getParameter("idToReporteSiniestro");
		BigDecimal numeroReporte 			=  new BigDecimal(new Double(strNumeroReporte));
		ReporteSiniestroDTO dto 		= dd.desplegarReporte(numeroReporte);
		DatosTransportistaDTO datoTransportistaDTO;
		
		if(dto.getDatosTransportista() != null){
			datoTransportistaDTO = dto.getDatosTransportista();
		}else{
			datoTransportistaDTO = new DatosTransportistaDTO();
			datoTransportistaDTO.setIdToReporteSiniestro(numeroReporte);
			datoTransportistaDTO = datosTransportistaDN.obtieneTransportistaPorId(datoTransportistaDTO);
		}
		
		TransportesForm transForm 		= (TransportesForm) form;
		if(datoTransportistaDTO != null){
			poblarFormDatosTransporte(datoTransportistaDTO,transForm);
		}
		
		
		return mapping.findForward(reglaNavegacion);
	}
	
	private void poblarFormDatosTransporte(DatosTransportistaDTO transDTO, TransportesForm transForm){
		if(transDTO.getCalle() != null){
			transForm.setCalle(transDTO.getCalle());
		}
		
		if (transDTO.getColonia() != null){
			transForm.setIdColonia(transDTO.getColonia());
		}
		
		if(transDTO.getCodigoPostal() != null){
			transForm.setCp(transDTO.getCodigoPostal().toString());
		}
		
		if(transDTO.getIdEstado() != null){
			transForm.setIdEstado(transDTO.getIdEstado());
		}
		
		if(transDTO.getIdMunicipio() != null){
			transForm.setIdCiudad(transDTO.getIdMunicipio());
		}
		
		if(transDTO.getRazonSocial() != null){
			transForm.setNombreRazonSocial(transDTO.getRazonSocial());
		}
		
		if(transDTO.getNumeroUnidad() != null){
			transForm.setNoUnidad(transDTO.getNumeroUnidad());
		}

		if(transDTO.getRfc() != null){
			transForm.setRfc_curp(transDTO.getRfc());
		}
		
		if(transDTO.getTelefono() != null){
			transForm.setTelefono(transDTO.getTelefono());
		}
		
		if(transDTO.getTipoPersona() != null){
			transForm.setTipoPersona(transDTO.getTipoPersona());
		}
	}
}
