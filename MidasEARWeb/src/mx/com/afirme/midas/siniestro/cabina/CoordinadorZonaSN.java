package mx.com.afirme.midas.siniestro.cabina;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoordinadorZonaSN {
	
	private CoordinadorZonaFacadeRemote beanRemoto;
	
	public CoordinadorZonaSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(CoordinadorZonaFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}	
	
	public Integer obtenerCoordinadorPorZona(String idEstado, String idContry){
		try {
			Integer idCoordinador = null;
			
			CoordinadorZonaId id = new CoordinadorZonaId();
			id.setIdEstado(idEstado);
			id.setIdContry(idContry);
			
			CoordinadorZonaDTO coordinador = beanRemoto.findById(id);
			
			if(coordinador != null){
				idCoordinador = coordinador.getIdCoordinador();
			}
			
			return idCoordinador;
			
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	

}
