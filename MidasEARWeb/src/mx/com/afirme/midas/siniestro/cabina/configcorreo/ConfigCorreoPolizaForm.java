/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author smvr
 *
 */
public class ConfigCorreoPolizaForm extends MidasBaseForm {

	private static final long serialVersionUID = 93865643525255357L;
	private String idConfigPoliza;
	private String codigoProducto;
	private String codigoTipoPoliza;
	private String numeroPoliza;
	private String numeroPolizaBuscar;
	private String numeroRenovacion;
	private String correo;
	private String usuarioNombre;
	private String fechaModificacion;
	
	/**
	 * @return the idConfigPoliza
	 */
	public String getIdConfigPoliza() {
		return idConfigPoliza;
	}
	
	/**
	 * @param idConfigPoliza the idConfigPoliza to set
	 */
	public void setIdConfigPoliza(String idConfigPoliza) {
		this.idConfigPoliza = idConfigPoliza;
	}
	
	/**
	 * @return the codigoProducto
	 */
	public String getCodigoProducto() {
		return codigoProducto;
	}
	
	/**
	 * @param codigoProducto the codigoProducto to set
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	
	/**
	 * @return the codigoTipoPoliza
	 */
	public String getCodigoTipoPoliza() {
		return codigoTipoPoliza;
	}
	
	/**
	 * @param codigoTipoPoliza the codigoTipoPoliza to set
	 */
	public void setCodigoTipoPoliza(String codigoTipoPoliza) {
		this.codigoTipoPoliza = codigoTipoPoliza;
	}
	
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	/**
	 * @return the numeroPolizaBuscar
	 */
	public String getNumeroPolizaBuscar() {
		return numeroPolizaBuscar;
	}

	/**
	 * @param numeroPolizaBuscar the numeroPolizaBuscar to set
	 */
	public void setNumeroPolizaBuscar(String numeroPolizaBuscar) {
		this.numeroPolizaBuscar = numeroPolizaBuscar;
	}

	/**
	 * @return the numeroRenovacion
	 */
	public String getNumeroRenovacion() {
		return numeroRenovacion;
	}
	
	/**
	 * @param numeroRenovacion the numeroRenovacion to set
	 */
	public void setNumeroRenovacion(String numeroRenovacion) {
		this.numeroRenovacion = numeroRenovacion;
	}
	
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	/**
	 * @return the usuarioNombre
	 */
	public String getUsuarioNombre() {
		return usuarioNombre;
	}
	
	/**
	 * @param usuarioNombre the usuarioNombre to set
	 */
	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}
	
	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
		
}

