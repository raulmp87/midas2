/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.EntityTransaction;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;

/**
 * @author smvr
 *
 */
public class ConfigCorreoTipoNegocioSN {

private ConfigCorreoTipoNegocioFacadeRemote beanRemoto;
	
	//private static final Logger LOG = LoggerFactory.getLogger(ConfigCorreoTipoNegocioSN.class);

	public ConfigCorreoTipoNegocioSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ConfigCorreoTipoNegocioFacadeRemote.class);
		LogDeMidasWeb.log( "bean Remoto ConfigCorreoTipoNegocioFacadeRemote instanciado", Level.INFO, null);
	}
	
	public void agregar(ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(configCorreoTipoNegocioDTO);
		}catch(EJBTransactionRolledbackException e){
			LogDeMidasWeb.log( "EXCEPCION GENERADA ::::  " + e.getMessage(), Level.SEVERE, null );
		}
	}
	
	public void eliminar(ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.delete(configCorreoTipoNegocioDTO);
	}
	
	public ConfigCorreoTipoNegociooDTO actualizar(ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.update(configCorreoTipoNegocioDTO);
	}
	
	public List<TipoNegocioDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		return beanRemoto.findAll();
	}
	
	public ConfigCorreoTipoNegociooDTO getConfigCorreoTipoNegocioPorId(BigDecimal idConfigTipoNegocio) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findById(idConfigTipoNegocio);
	}
	
	public List<ConfigCorreoTipoNegociooDTO> getConfigCorreoTipoNegocio(BigDecimal idTcTipoNegocio) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findByTipoNegocio(idTcTipoNegocio);
	}
	
	public List<TipoNegocioDTO> listarFiltrado(TipoNegocioDTO tipoNegocioDTO) throws ExcepcionDeAccesoADatos{
		return beanRemoto.listarFiltrado( tipoNegocioDTO );
	}

}
