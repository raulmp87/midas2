/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class PreguntasEspecialesForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String noPoliza;
	private String noCertificado;
	private String nomFuncionario;
	private String direccionFuncionario;
	private String telefono;
	private String correo;
	private String dependenciaTrabaja;
	private String cargoOcupa;
	private String tipoCobertura;
	private String tipoDocumentoRecibio;
	private String detallar;
	private String numeroExpediente;
	private String numeroOficio;
	private String lugarHizoOficio;
	private String nombreDependencia;
	private String personaRealizoOficio;
	private String descripcionPuesto;
	private String causaReclamacion;
	private String tienePlazoReclamacion;
	private String estaCubierto;
	private String serviciosProfesionales;
	private String comentario;
	
	private String fechaNotificacion;
	private String plazoReclamacion;
	private String idToReporteSiniestro;
	
	private String codigoPostal;
    private String idColonia;
    private String idCiudad;
    private String idEstado;    
	
	/**
	 * @return the noPoliza
	 */
	public String getNoPoliza() {
		return noPoliza;
	}
	/**
	 * @param noPoliza the noPoliza to set
	 */
	public void setNoPoliza(String noPoliza) {
		this.noPoliza = noPoliza;
	}
	/**
	 * @return the noCertificado
	 */
	public String getNoCertificado() {
		return noCertificado;
	}
	/**
	 * @param noCertificado the noCertificado to set
	 */
	public void setNoCertificado(String noCertificado) {
		this.noCertificado = noCertificado;
	}
	/**
	 * @return the nomFuncionario
	 */
	public String getNomFuncionario() {
		return nomFuncionario;
	}
	/**
	 * @param nomFuncionario the nomFuncionario to set
	 */
	public void setNomFuncionario(String nomFuncionario) {
		this.nomFuncionario = nomFuncionario;
	}
	/**
	 * @return the direccionFuncionario
	 */
	public String getDireccionFuncionario() {
		return direccionFuncionario;
	}
	/**
	 * @param direccionFuncionario the direccionFuncionario to set
	 */
	public void setDireccionFuncionario(String direccionFuncionario) {
		this.direccionFuncionario = direccionFuncionario;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @return the dependenciaTrabaja
	 */
	public String getDependenciaTrabaja() {
		return dependenciaTrabaja;
	}
	/**
	 * @param dependenciaTrabaja the dependenciaTrabaja to set
	 */
	public void setDependenciaTrabaja(String dependenciaTrabaja) {
		this.dependenciaTrabaja = dependenciaTrabaja;
	}
	/**
	 * @return the cargoOcupa
	 */
	public String getCargoOcupa() {
		return cargoOcupa;
	}
	/**
	 * @param cargoOcupa the cargoOcupa to set
	 */
	public void setCargoOcupa(String cargoOcupa) {
		this.cargoOcupa = cargoOcupa;
	}
	/**
	 * @return the tipoCobertura
	 */
	public String getTipoCobertura() {
		return tipoCobertura;
	}
	/**
	 * @param tipoCobertura the tipoCobertura to set
	 */
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	/**
	 * @return the tipoDocumentoRecibio
	 */
	public String getTipoDocumentoRecibio() {
		return tipoDocumentoRecibio;
	}
	/**
	 * @param tipoDocumentoRecibio the tipoDocumentoRecibio to set
	 */
	public void setTipoDocumentoRecibio(String tipoDocumentoRecibio) {
		this.tipoDocumentoRecibio = tipoDocumentoRecibio;
	}
	/**
	 * @return the detallar
	 */
	public String getDetallar() {
		return detallar;
	}
	/**
	 * @param detallar the detallar to set
	 */
	public void setDetallar(String detallar) {
		this.detallar = detallar;
	}
	/**
	 * @return the numeroExpediente
	 */
	public String getNumeroExpediente() {
		return numeroExpediente;
	}
	/**
	 * @param numeroExpediente the numeroExpediente to set
	 */
	public void setNumeroExpediente(String numeroExpediente) {
		this.numeroExpediente = numeroExpediente;
	}
	/**
	 * @return the numeroOficio
	 */
	public String getNumeroOficio() {
		return numeroOficio;
	}
	/**
	 * @param numeroOficio the numeroOficio to set
	 */
	public void setNumeroOficio(String numeroOficio) {
		this.numeroOficio = numeroOficio;
	}
	/**
	 * @return the lugarHizoOficio
	 */
	public String getLugarHizoOficio() {
		return lugarHizoOficio;
	}
	/**
	 * @param lugarHizoOficio the lugarHizoOficio to set
	 */
	public void setLugarHizoOficio(String lugarHizoOficio) {
		this.lugarHizoOficio = lugarHizoOficio;
	}
	/**
	 * @return the nombreDependencia
	 */
	public String getNombreDependencia() {
		return nombreDependencia;
	}
	/**
	 * @param nombreDependencia the nombreDependencia to set
	 */
	public void setNombreDependencia(String nombreDependencia) {
		this.nombreDependencia = nombreDependencia;
	}
	/**
	 * @return the personaRealizoOficio
	 */
	public String getPersonaRealizoOficio() {
		return personaRealizoOficio;
	}
	/**
	 * @param personaRealizoOficio the personaRealizoOficio to set
	 */
	public void setPersonaRealizoOficio(String personaRealizoOficio) {
		this.personaRealizoOficio = personaRealizoOficio;
	}
	/**
	 * @return the descripcionPuesto
	 */
	public String getDescripcionPuesto() {
		return descripcionPuesto;
	}
	/**
	 * @param descripcionPuesto the descripcionPuesto to set
	 */
	public void setDescripcionPuesto(String descripcionPuesto) {
		this.descripcionPuesto = descripcionPuesto;
	}
	/**
	 * @return the causaReclamacion
	 */
	public String getCausaReclamacion() {
		return causaReclamacion;
	}
	/**
	 * @param causaReclamacion the causaReclamacion to set
	 */
	public void setCausaReclamacion(String causaReclamacion) {
		this.causaReclamacion = causaReclamacion;
	}
	/**
	 * @return the tienePlazoReclamacion
	 */
	public String getTienePlazoReclamacion() {
		return tienePlazoReclamacion;
	}
	/**
	 * @param tienePlazoReclamacion the tienePlazoReclamacion to set
	 */
	public void setTienePlazoReclamacion(String tienePlazoReclamacion) {
		this.tienePlazoReclamacion = tienePlazoReclamacion;
	}
	/**
	 * @return the estaCubierto
	 */
	public String getEstaCubierto() {
		return estaCubierto;
	}
	/**
	 * @param estaCubierto the estaCubierto to set
	 */
	public void setEstaCubierto(String estaCubierto) {
		this.estaCubierto = estaCubierto;
	}
	/**
	 * @return the serviciosProfesionales
	 */
	public String getServiciosProfesionales() {
		return serviciosProfesionales;
	}
	/**
	 * @param serviciosProfesionales the serviciosProfesionales to set
	 */
	public void setServiciosProfesionales(String serviciosProfesionales) {
		this.serviciosProfesionales = serviciosProfesionales;
	}
	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}
	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	/**
	 * @return the fechaNotificacion
	 */
	public String getFechaNotificacion() {
		return fechaNotificacion;
	}
	/**
	 * @param fechaNotificacion the fechaNotificacion to set
	 */
	public void setFechaNotificacion(String fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}
	/**
	 * @return the plazoReclamacion
	 */
	public String getPlazoReclamacion() {
		return plazoReclamacion;
	}
	/**
	 * @param plazoReclamacion the plazoReclamacion to set
	 */
	public void setPlazoReclamacion(String plazoReclamacion) {
		this.plazoReclamacion = plazoReclamacion;
	}
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	/**
	 * @return the codigoPostal
	 */
	public String getCodigoPostal() {
		return codigoPostal;
	}
	/**
	 * @param codigoPostal the codigoPostal to set
	 */
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	/**
	 * @return the idColonia
	 */
	public String getIdColonia() {
		return idColonia;
	}
	/**
	 * @param idColonia the idColonia to set
	 */
	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}
	/**
	 * @return the idCiudad
	 */
	public String getIdCiudad() {
		return idCiudad;
	}
	/**
	 * @param idCiudad the idCiudad to set
	 */
	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}
	/**
	 * @return the idEstado
	 */
	public String getIdEstado() {
		return idEstado;
	}
	/**
	 * @param idEstado the idEstado to set
	 */
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	
	
}
