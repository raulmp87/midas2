/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;


import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.danios.soporte.DetallePolizaSoporteDanosSiniestrosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.consultacobranzapoliza.ConsultaCobranzaPolizaDN;
import mx.com.afirme.midas.interfaz.consultacobranzapoliza.DatosCobranzaPolizaDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author jmartinez
 *
 */
public class ReporteDatosPolizaAction extends MidasMappingDispatchAction {

	/**
	 * Method reporteDetallePoliza
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public ActionForward reporteDetallePoliza(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {

		String reglaNavegacion = Sistema.EXITOSO;
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		ConsultaCobranzaPolizaDN consultaCobranzaPolizaDN = ConsultaCobranzaPolizaDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DesplegarDatosPolizaForm desplegarDatosPolizaForm 	= (DesplegarDatosPolizaForm) form;
		
		String idToPoliza = request.getParameter("idToPoliza");
		String idToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		String fechaSiniestro = request.getParameter("fechaSiniestro");
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(new BigDecimal(idToPoliza),UtileriasWeb.getFechaFromString(fechaSiniestro));
			desplegarDatosPolizaForm.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
			desplegarDatosPolizaForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
			desplegarDatosPolizaForm.setDireccionAsegurado(polizaSoporteDanosDTO.getCalleAsegurado());
			desplegarDatosPolizaForm.setTipoMoneda(polizaSoporteDanosDTO.getDescripcionMoneda());
//			if(polizaSoporteDanosDTO.getFechaPago()!=null){
//				desplegarDatosPolizaForm.setFecPago(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaPago()));
//			}
			desplegarDatosPolizaForm.setClaveOficina(polizaSoporteDanosDTO.getNombreOficina());
//			if(polizaSoporteDanosDTO.getImportePagado() > 0){
//				desplegarDatosPolizaForm.setImportePagado(String.valueOf(polizaSoporteDanosDTO.getImportePagado()));
//			}
			if(polizaSoporteDanosDTO.getCodigoProducto() >0){
				desplegarDatosPolizaForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
			}
//			if(polizaSoporteDanosDTO.getSaldoPendiente() >0){
//				desplegarDatosPolizaForm.setSaldoPendiente(String.valueOf(polizaSoporteDanosDTO.getSaldoPendiente()));
//			}
			desplegarDatosPolizaForm.setFormaPago(polizaSoporteDanosDTO.getDescripcionFormaPago());
//			if(polizaSoporteDanosDTO.getSaldoVencido() >0){
//				desplegarDatosPolizaForm.setSaldoVencido(String.valueOf(polizaSoporteDanosDTO.getSaldoVencido()));
//			}
//			desplegarDatosPolizaForm.setUltimoReciboPagado(polizaSoporteDanosDTO.getUltimoReciboPagado());
			if(polizaSoporteDanosDTO.getFechaEmision() != null){
				desplegarDatosPolizaForm.setFecEmision(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaEmision()));
			}
			if(polizaSoporteDanosDTO.getFechaInicioVigencia() != null){
				desplegarDatosPolizaForm.setFecVigencia(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaInicioVigencia()));
			}
			if(polizaSoporteDanosDTO.getFechaFinVigencia() != null){
				desplegarDatosPolizaForm.setAl(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaFinVigencia()));
			}
			if(polizaSoporteDanosDTO.getCodigoTipoNegocio() != null){
				desplegarDatosPolizaForm.setCodigoTipoNegocio(polizaSoporteDanosDTO.getCodigoTipoNegocio().toString());
			}
			
			desplegarDatosPolizaForm.setPolizaNumeroEndoso(String.valueOf(polizaSoporteDanosDTO.getNumeroEndosoVigente()));
			if(polizaSoporteDanosDTO.getCodigoAgente() != null){
				desplegarDatosPolizaForm.setPolizaIdAgente(polizaSoporteDanosDTO.getCodigoAgente().toString());
			}
			if(polizaSoporteDanosDTO.getNombreAgente() != null){
				desplegarDatosPolizaForm.setPolizaNombreAgente(polizaSoporteDanosDTO.getNombreAgente());
			}
			if(polizaSoporteDanosDTO.getNombreOficina() != null){
				desplegarDatosPolizaForm.setPolizaNombreOficina(polizaSoporteDanosDTO.getNombreOficina());
			}
			if(polizaSoporteDanosDTO.getNombreAsegurado() != null){
				desplegarDatosPolizaForm.setPolizaNombreAsegurado(polizaSoporteDanosDTO.getNombreAsegurado());
			}
			if(polizaSoporteDanosDTO.getIdAsegurado() != null){
				desplegarDatosPolizaForm.setPolizaIdCliente(polizaSoporteDanosDTO.getIdAsegurado().toString());
			}
//			Fin de Agregados
			RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
			
			List<RiesgoAfectadoDTO> listaCoberturasGuardadas = riesgoAfectadoDN.getCoberturasRiesgoPorReporte(new BigDecimal(idToReporteSiniestro));
			if(listaCoberturasGuardadas.size() > 0 ){
				desplegarDatosPolizaForm.setAsignarPolizaPermitido(false);
			}else{
				desplegarDatosPolizaForm.setAsignarPolizaPermitido(true);
			}
			if(polizaSoporteDanosDTO.getTelefonoAsegurado() != null){
				desplegarDatosPolizaForm.setTelefonoAsegurado(polizaSoporteDanosDTO.getTelefonoAsegurado());
			}
			if(polizaSoporteDanosDTO.getNombreContratante() != null){
				desplegarDatosPolizaForm.setNombreContratante(polizaSoporteDanosDTO.getNombreContratante());
			}
			if(polizaSoporteDanosDTO.getCalleContratante() != null){
				desplegarDatosPolizaForm.setDireccionContratante(polizaSoporteDanosDTO.getCalleContratante());
			}
			if(polizaSoporteDanosDTO.getTelefonoContratante() != null){
				desplegarDatosPolizaForm.setTelefonoContratante(polizaSoporteDanosDTO.getTelefonoContratante());
			}
			desplegarDatosPolizaForm.setProductoTransporte(polizaSoporteDanosDTO.isProductoTransporte());
			
			DatosCobranzaPolizaDTO datosCobranzaPolizaDTO = consultaCobranzaPolizaDN.consultaCobranzaPoliza(new BigDecimal(idToPoliza));
			if(datosCobranzaPolizaDTO != null){
				if(datosCobranzaPolizaDTO.getUltimoReciboPagado()!= null){
					desplegarDatosPolizaForm.setUltimoReciboPagado(datosCobranzaPolizaDTO.getUltimoReciboPagado().toString());
				}else{
					desplegarDatosPolizaForm.setUltimoReciboPagado("");
				}
				if(datosCobranzaPolizaDTO.getMontoVencido()!= null){
					desplegarDatosPolizaForm.setSaldoVencido(datosCobranzaPolizaDTO.getMontoVencido().toString());
				}else{
					desplegarDatosPolizaForm.setSaldoVencido("");
				}
				if(datosCobranzaPolizaDTO.getMontoPorVencer()!= null){
					desplegarDatosPolizaForm.setSaldoPendiente(datosCobranzaPolizaDTO.getMontoPorVencer().toString());
				}else{
					desplegarDatosPolizaForm.setSaldoPendiente("");
				}
				if(datosCobranzaPolizaDTO.getMontoPagado()!= null){
					desplegarDatosPolizaForm.setImportePagado(datosCobranzaPolizaDTO.getMontoPagado().toString());
				}else{
					desplegarDatosPolizaForm.setImportePagado("");
				}
				if(datosCobranzaPolizaDTO.getFechaUltimoPago()!= null){
					desplegarDatosPolizaForm.setFecPago(UtileriasWeb.getFechaString(datosCobranzaPolizaDTO.getFechaUltimoPago()));
				}else{
					desplegarDatosPolizaForm.setFecPago("");
				}
				if(datosCobranzaPolizaDTO.getSituacion()!= null){
					desplegarDatosPolizaForm.setEstatusCobranza(datosCobranzaPolizaDTO.getSituacion());
				}else{
					desplegarDatosPolizaForm.setEstatusCobranza("");
				}
				
			}
		
			request.setAttribute("riesgoPolizas",polizaSoporteDanosDTO.getListaDetallesPolizaSiniestros());
		return mapping.findForward(reglaNavegacion);
	}
	
//	public void mostrarListaReporteDetallePoliza(ActionMapping mapping, ActionForm form,
//			HttpServletRequest request, HttpServletResponse response) throws SystemException,
//			ExcepcionDeAccesoADatos, ParseException {
//		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
//		DesplegarDatosPolizaForm desplegarDatosPolizaForm 	= (DesplegarDatosPolizaForm) form;
//		
//		String idToPoliza = request.getParameter("idToPoliza");
//		try{
//			PolizaSoporteDanosDTO polizaSoporteDanosDTO =  soporteDanosDN.getDetallePoliza(new BigDecimal(idToPoliza));
//			String json = this.jsonMostrarListaReporteDetallePoliza(polizaSoporteDanosDTO.getListaDetallesPolizaSiniestros());
//			response.setContentType("text/json");
//			PrintWriter pw = response.getWriter();
//			pw.write(json);
//			pw.flush();
//			pw.close();
//		} catch (SystemException e) {
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		} catch (ExcepcionDeAccesoADatos e) {
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		} catch (IOException e) {
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		}
//		
//	}
//	
//	private String jsonMostrarListaReporteDetallePoliza(List<DetallePolizaSoporteDanosSiniestrosDTO> detallePolizaSoporteDanosSiniestrosLista){
//		MidasJsonBase jsonBase =  new MidasJsonBase();
//		if (detallePolizaSoporteDanosSiniestrosLista!=null && detallePolizaSoporteDanosSiniestrosLista.size()>0){
//			for (DetallePolizaSoporteDanosSiniestrosDTO detallePolizaSoporteDanosSiniestrosDTO : detallePolizaSoporteDanosSiniestrosLista) {
//				MidasJsonRow row = new MidasJsonRow();
//					row.setId(detallePolizaSoporteDanosSiniestrosDTO.getIdToCobertura().toString()
//							+detallePolizaSoporteDanosSiniestrosDTO.getIdToRiesgo().toString()
//							+detallePolizaSoporteDanosSiniestrosDTO.getIdToSeccion().toString());
//					row.setDatos(
//						reparaCadenaVaciaParaJson(detallePolizaSoporteDanosSiniestrosDTO.getNumeroInciso().toString()),
//						reparaCadenaVaciaParaJson(detallePolizaSoporteDanosSiniestrosDTO.getDescripcionSubInciso()),
//						reparaCadenaVaciaParaJson(detallePolizaSoporteDanosSiniestrosDTO.getDescripcionSeccion()),
//						reparaCadenaVaciaParaJson(detallePolizaSoporteDanosSiniestrosDTO.getDescripcionCobertura()),
//						reparaCadenaVaciaParaJson(detallePolizaSoporteDanosSiniestrosDTO.getDescripcionRiesgo()),
//						reparaCadenaVaciaParaJson(UtileriasWeb.formatoMoneda(detallePolizaSoporteDanosSiniestrosDTO.getSumaAsegurada()))
//					);
//				jsonBase.addRow(row);
//			}	
//		}
//		return jsonBase.toString();
//	}
	
	private String reparaCadenaVaciaParaJson(String elemento){
		if (!UtileriasWeb.esCadenaVacia(elemento)){
			return elemento;
		}else{
			return new String();
		}
	}
	
	
	
	/**
	 * Method reporteDetallePoliza
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public ActionForward datosPolizaCrear(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {

		String reglaNavegacion = Sistema.EXITOSO;
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		ConsultaCobranzaPolizaDN consultaCobranzaPolizaDN = ConsultaCobranzaPolizaDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DesplegarDatosPolizaForm desplegarDatosPolizaForm 	= (DesplegarDatosPolizaForm) form;
		
		String idToPoliza = request.getParameter("idToPoliza");
		String fechaSiniestro = request.getParameter("fechaSiniestro");
//		PolizaSoporteDanosDTO polizaSoporteDanosDTO =  soporteDanosDN.getDetallePoliza(new BigDecimal(idToPoliza));
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(new BigDecimal(idToPoliza),UtileriasWeb.getFechaFromString(fechaSiniestro));
		
		desplegarDatosPolizaForm.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
		desplegarDatosPolizaForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
//		desplegarDatosPolizaForm.setPolizaNombreAsegurado(polizaSoporteDanosDTO.getNombreAsegurado());
		desplegarDatosPolizaForm.setDireccionAsegurado(polizaSoporteDanosDTO.getCalleAsegurado());
		desplegarDatosPolizaForm.setTipoMoneda(polizaSoporteDanosDTO.getDescripcionMoneda());
		if(polizaSoporteDanosDTO.getFechaPago()!=null){
			desplegarDatosPolizaForm.setFecPago(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaPago()));
		}
		desplegarDatosPolizaForm.setClaveOficina(polizaSoporteDanosDTO.getNombreOficina());
		if(polizaSoporteDanosDTO.getImportePagado() > 0){
			desplegarDatosPolizaForm.setImportePagado(String.valueOf(polizaSoporteDanosDTO.getImportePagado()));
		}
		if(polizaSoporteDanosDTO.getCodigoProducto() >0){
			desplegarDatosPolizaForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
		}
		if(polizaSoporteDanosDTO.getSaldoPendiente() >0){
			desplegarDatosPolizaForm.setSaldoPendiente(String.valueOf(polizaSoporteDanosDTO.getSaldoPendiente()));
		}
		desplegarDatosPolizaForm.setFormaPago(polizaSoporteDanosDTO.getDescripcionFormaPago());
		if(polizaSoporteDanosDTO.getSaldoVencido() >0){
			desplegarDatosPolizaForm.setSaldoVencido(String.valueOf(polizaSoporteDanosDTO.getSaldoVencido()));
		}
		desplegarDatosPolizaForm.setUltimoReciboPagado(polizaSoporteDanosDTO.getUltimoReciboPagado());
		if(polizaSoporteDanosDTO.getFechaEmision() != null){
			desplegarDatosPolizaForm.setFecEmision(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaEmision()));
		}
		if(polizaSoporteDanosDTO.getFechaInicioVigencia() != null){
			desplegarDatosPolizaForm.setFecVigencia(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaInicioVigencia()));
		}
		if(polizaSoporteDanosDTO.getFechaFinVigencia() != null){
			desplegarDatosPolizaForm.setAl(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaFinVigencia()));
		}
		if(polizaSoporteDanosDTO.getCodigoTipoNegocio() != null){
			desplegarDatosPolizaForm.setCodigoTipoNegocio(polizaSoporteDanosDTO.getCodigoTipoNegocio().toString());
		}
//		Agregados de la poliza
//		desplegarDatosPolizaForm.setPolizaNumeroEndoso(String.valueOf(polizaSoporteDanosDTO.getNumeroUltimoEndoso()));
		desplegarDatosPolizaForm.setPolizaNumeroEndoso(String.valueOf(polizaSoporteDanosDTO.getNumeroEndosoVigente()));
		if(polizaSoporteDanosDTO.getCodigoAgente() != null){
			desplegarDatosPolizaForm.setPolizaIdAgente(polizaSoporteDanosDTO.getCodigoAgente().toString());
		}
		if(polizaSoporteDanosDTO.getNombreAgente() != null){
			desplegarDatosPolizaForm.setPolizaNombreAgente(polizaSoporteDanosDTO.getNombreAgente());
		}
		if(polizaSoporteDanosDTO.getNombreOficina() != null){
			desplegarDatosPolizaForm.setPolizaNombreOficina(polizaSoporteDanosDTO.getNombreOficina());
		}
		if(polizaSoporteDanosDTO.getNombreAsegurado() != null){
			desplegarDatosPolizaForm.setPolizaNombreAsegurado(polizaSoporteDanosDTO.getNombreAsegurado());
		}
		if(polizaSoporteDanosDTO.getIdAsegurado() != null){
			desplegarDatosPolizaForm.setPolizaIdCliente(polizaSoporteDanosDTO.getIdAsegurado().toString());
		}
//		Fin de Agregados
		if(polizaSoporteDanosDTO.getTelefonoAsegurado() != null){
			desplegarDatosPolizaForm.setTelefonoAsegurado(polizaSoporteDanosDTO.getTelefonoAsegurado());
		}
		if(polizaSoporteDanosDTO.getNombreContratante() != null){
			desplegarDatosPolizaForm.setNombreContratante(polizaSoporteDanosDTO.getNombreContratante());
		}
		if(polizaSoporteDanosDTO.getCalleContratante() != null){
			desplegarDatosPolizaForm.setDireccionContratante(polizaSoporteDanosDTO.getCalleContratante());
		}
		if(polizaSoporteDanosDTO.getTelefonoContratante() != null){
			desplegarDatosPolizaForm.setTelefonoContratante(polizaSoporteDanosDTO.getTelefonoContratante());
		}
		DatosCobranzaPolizaDTO datosCobranzaPolizaDTO = consultaCobranzaPolizaDN.consultaCobranzaPoliza(new BigDecimal(idToPoliza));
		if(datosCobranzaPolizaDTO != null){
			if(datosCobranzaPolizaDTO.getUltimoReciboPagado()!= null){
				desplegarDatosPolizaForm.setUltimoReciboPagado(datosCobranzaPolizaDTO.getUltimoReciboPagado().toString());
			}else{
				desplegarDatosPolizaForm.setUltimoReciboPagado("");
			}
			if(datosCobranzaPolizaDTO.getMontoVencido()!= null){
				desplegarDatosPolizaForm.setSaldoVencido(datosCobranzaPolizaDTO.getMontoVencido().toString());
			}else{
				desplegarDatosPolizaForm.setSaldoVencido("");
			}
			if(datosCobranzaPolizaDTO.getMontoPorVencer()!= null){
				desplegarDatosPolizaForm.setSaldoPendiente(datosCobranzaPolizaDTO.getMontoPorVencer().toString());
			}else{
				desplegarDatosPolizaForm.setSaldoPendiente("");
			}
			if(datosCobranzaPolizaDTO.getMontoPagado()!= null){
				desplegarDatosPolizaForm.setImportePagado(datosCobranzaPolizaDTO.getMontoPagado().toString());
			}else{
				desplegarDatosPolizaForm.setImportePagado("");
			}
			if(datosCobranzaPolizaDTO.getFechaUltimoPago()!= null){
				desplegarDatosPolizaForm.setFecPago(UtileriasWeb.getFechaString(datosCobranzaPolizaDTO.getFechaUltimoPago()));
			}else{
				desplegarDatosPolizaForm.setFecPago("");
			}
			if(datosCobranzaPolizaDTO.getSituacion()!= null){
				desplegarDatosPolizaForm.setEstatusCobranza(datosCobranzaPolizaDTO.getSituacion());
			}else{
				desplegarDatosPolizaForm.setEstatusCobranza("");
			}
			
		}
		
//		request.setAttribute("riesgoPolizas",polizaSoporteDanosDTO.getListaDetallesPolizaSiniestros());
		return mapping.findForward(reglaNavegacion);
	}
	
	public void direccionPoliza(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws SystemException {
		String idToPoliza = request.getParameter("idToPoliza");
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		PolizaSoporteDanosDTO polizaSoporteDanosDTO =  soporteDanosDN.getDatosGeneralesPoliza(new BigDecimal(idToPoliza));
		try {			
			StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
//			for(Usuario usuario: coordinadores){
					buffer.append("<item>");
					buffer.append("<id>");
					buffer.append(polizaSoporteDanosDTO.getIdToPoliza());
					buffer.append("</id>");
					buffer.append("<description><![CDATA[");
					buffer.append(polizaSoporteDanosDTO.getCalleAsegurado());
					buffer.append(",");
					buffer.append(polizaSoporteDanosDTO.getCpAsegurado().trim());
					buffer.append(",");
					buffer.append(polizaSoporteDanosDTO.getIdEstadoAsegurado());
					buffer.append(",");
					buffer.append(polizaSoporteDanosDTO.getIdMunicipioAsegurado());
					buffer.append(",");
					buffer.append(polizaSoporteDanosDTO.getIdColoniaAsegurado());
					buffer.append("]]></description>");
					buffer.append("</item>");
//			}
			buffer.append("</response>");
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",ioException);
		} 
	}
	
	
}//end class
