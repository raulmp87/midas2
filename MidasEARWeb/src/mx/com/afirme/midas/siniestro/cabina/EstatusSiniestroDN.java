package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstatusSiniestroDN {
	public static final EstatusSiniestroDN INSTANCIA = new EstatusSiniestroDN();
	
	public static EstatusSiniestroDN getInstancia (){
		return EstatusSiniestroDN.INSTANCIA;
	}
	
	public EstatusSiniestroDTO getPorId(EstatusSiniestroDTO estatusSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		EstatusSiniestroSN estatusSiniestroSN = new EstatusSiniestroSN();
		return estatusSiniestroSN.getPorId(estatusSiniestroDTO);
	}
	
	public List<EstatusSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		EstatusSiniestroSN estatusSiniestroSN = new EstatusSiniestroSN();
		return estatusSiniestroSN.listarTodos();
	}

}
