/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;


import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ReporteEstadoSiniestroAction extends MidasMappingDispatchAction {
	
	
	/**
	 * Method reporteDeEstadoSiniestro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward reporteDeEstadoSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos {
		EstadoSiniestroForm estadoSiniestroForm = (EstadoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String strIdToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal idToReporteSiniestro = new BigDecimal(strIdToReporteSiniestro); 
		
		ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
		poblarForm(reporteSiniestroDTO,estadoSiniestroForm);
		return mapping.findForward(Sistema.EXITOSO);
	}

	/**
	 * Method reporteGuardarEstadoSiniestro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public void reporteGuardarEstadoSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {
		ReporteSiniestroDN reporteSiniestroDN 			= ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		EstadoSiniestroForm estadoSiniestroForm = (EstadoSiniestroForm) form;
		String strIdToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal idToReporteSiniestro = new BigDecimal(strIdToReporteSiniestro); 
		ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
		
		if(!fechaUltimaEsMayor(estadoSiniestroForm)){
			UtileriasWeb.imprimeMensajeXML("0", estadoSiniestroForm.getDescripcionDato(), response);
		}else{
			poblarDTO(estadoSiniestroForm,reporteSiniestroDTO);
			reporteSiniestroDN.guardarReporte(reporteSiniestroDTO);
			UtileriasWeb.imprimeMensajeXML("1", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar") , response);
		}
	}
	
	/**
	 * Method reporteGuardarEstadoSiniestro
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public void cerrarReporteEstadoSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {
		boolean esValidoCerrar = true;
		String mensajeError = "El reporte no fue cerrado. Por las siguientes razones:";
		EstadoSiniestroForm estadoSiniestroForm = (EstadoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(new BigDecimal(estadoSiniestroForm.getIdToReporteSiniestro()));		
		try{
			List<String> mensajesError = reporteSiniestroDN.validaPendientesReporteSiniestro(reporteSiniestroDTO);
			if (mensajesError!=null && mensajesError.size()>0){
				esValidoCerrar = false;
				mensajeError=obtenerMensajeError(mensajesError);				
			}
			
			if (esValidoCerrar){
				poblarDTO(estadoSiniestroForm, reporteSiniestroDTO);
				ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_CERRADO);
				reporteEstatusDTO = ReporteEstatusDN.getInstancia().obtieneReporteStatusPorId(reporteEstatusDTO);
				reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
				reporteSiniestroDN.guardarReporte(reporteSiniestroDTO);
				UtileriasWeb.imprimeMensajeXML("1", "El reporte fue cerrado con exito", response);
			} else {
				UtileriasWeb.imprimeMensajeXML("0", mensajeError, response);
			}
		}catch (Exception e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		}
	}
	
	public String obtenerMensajeError(List<String> mensajesError) {
		StringBuilder mensajeError = new StringBuilder("");
		for (String mensaje:mensajesError){
			mensajeError.append("<br>-").append(mensaje);
		}
		return mensajeError.toString();
	}

	private void poblarForm(ReporteSiniestroDTO reporteSiniestroDTO,EstadoSiniestroForm estadoSiniestroForm) {
		//String [] descripcionAjuste = {"Desestimiento","Indocumentado","Legal","Orden a Tercero","Pendiente","Poliza Cancelada","Rechazo","Reporte Cancelado","Sin costo","Sin firma de convenio","Solo orden a N/A"};
		if(reporteSiniestroDTO.getFechaAsignacionAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaAsignacionAjustador());
			estadoSiniestroForm.setFechaAsignacion(fecha);
		}else{
			estadoSiniestroForm.setFechaAsignacion("");
		}
		
		if(reporteSiniestroDTO.getFechaContactoAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaContactoAjustador());
			estadoSiniestroForm.setFechaContactoAjustador(fecha);
		}else{
			estadoSiniestroForm.setFechaContactoAjustador("");
		}
		
		if(reporteSiniestroDTO.getFechaLlegadaAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaLlegadaAjustador());
			estadoSiniestroForm.setFechaLlegadaAjustador(fecha);
		}else{
			estadoSiniestroForm.setFechaLlegadaAjustador("");
		}
		
		if(reporteSiniestroDTO.getFechaInspeccionAjustador() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaInspeccionAjustador());
			estadoSiniestroForm.setFechaInspeccionAjustador(fecha);
		}else{
			estadoSiniestroForm.setFechaInspeccionAjustador("");
		}
		
		if(reporteSiniestroDTO.getFechaPreliminar() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaPreliminar());
			estadoSiniestroForm.setFechaPreliminar(fecha);
		}else{
			estadoSiniestroForm.setFechaPreliminar("");
		}

		if(reporteSiniestroDTO.getFechaDocumento() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaDocumento());
			estadoSiniestroForm.setFechaDocumento(fecha);
		}else{
			estadoSiniestroForm.setFechaDocumento("");
		}

		if(reporteSiniestroDTO.getFechaInformeFinal() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaInformeFinal());
			estadoSiniestroForm.setFechaInformeFinal(fecha);
		}else{
			estadoSiniestroForm.setFechaInformeFinal("");
		}
		
		if(reporteSiniestroDTO.getFechaTerminadoSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaTerminadoSiniestro());
			estadoSiniestroForm.setFechaTerminacionSiniestro(fecha);
		}else{
			estadoSiniestroForm.setFechaTerminacionSiniestro("");
		}
		
		if(reporteSiniestroDTO.getFechaCerradoSiniestro() != null){
			String fecha = UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaCerradoSiniestro());
			estadoSiniestroForm.setFechaCerrado(fecha);
		}else{
			estadoSiniestroForm.setFechaCerrado("");
		}
		
		if(!UtileriasWeb.esCadenaVacia(reporteSiniestroDTO.getComentarios())){
			estadoSiniestroForm.setComentarios(reporteSiniestroDTO.getComentarios());
		}else{
			estadoSiniestroForm.setComentarios("");
		}
		
//		if(estaDTO.getReporteEstatus() != null){
//			estaForm.setEstatus(estaDTO.getReporteEstatus().getDescripcion());
//		}
//		
		if(reporteSiniestroDTO.getTerminoAjuste() != null){
			estadoSiniestroForm.setIdTcTerminoAjuste(reporteSiniestroDTO.getTerminoAjuste().getIdTcTerminoAjuste().toString());
			estadoSiniestroForm.setTerminoAjuste(reporteSiniestroDTO.getTerminoAjuste().getDescripcion());
			estadoSiniestroForm.setDescripcionTerminoAjuste(reporteSiniestroDTO.getTerminoAjuste().getDescripcion());
		}else{
			estadoSiniestroForm.setIdTcTerminoAjuste("0");
			estadoSiniestroForm.setTerminoAjuste("");
			estadoSiniestroForm.setDescripcionTerminoAjuste("");
		}
		
		if (reporteSiniestroDTO.getEstatusSiniestro()!=null){
			estadoSiniestroForm.setIdTcEstatusSiniestro(reporteSiniestroDTO.getEstatusSiniestro().getIdTcEstatusSiniestro().toString());
			estadoSiniestroForm.setDescripcionEstatusSiniestro(reporteSiniestroDTO.getEstatusSiniestro().getDescripcion());
		}else{
			estadoSiniestroForm.setIdTcEstatusSiniestro("1");
			estadoSiniestroForm.setDescripcionEstatusSiniestro("");
		}
		
		if (reporteSiniestroDTO.getSubEstatusSiniestro()!=null){
			estadoSiniestroForm.setIdTcSubEstatusSiniestro(reporteSiniestroDTO.getSubEstatusSiniestro().getIdTcSubEstatusSiniestro().toString());
			estadoSiniestroForm.setDescripcionSubEstatusSiniestro(reporteSiniestroDTO.getSubEstatusSiniestro().getDescripcion());
		}else{
			estadoSiniestroForm.setIdTcSubEstatusSiniestro("0");
			estadoSiniestroForm.setDescripcionSubEstatusSiniestro("");
		}
		
		if(reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().equals(ReporteSiniestroDTO.PENDIENTE_CERRADO) || reporteSiniestroDTO.getReporteEstatus().getIdTcReporteEstatus().equals(ReporteSiniestroDTO.REPORTE_TERMINADO)){
			estadoSiniestroForm.setEstatus(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.reporte.estadosinis.cerrado"));
		}else{
			estadoSiniestroForm.setEstatus(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.reporte.estadosinis.pendiente"));
		}
		
		estadoSiniestroForm.setEtapaReporte(reporteSiniestroDTO.getReporteEstatus().getDescripcion());
		if(reporteSiniestroDTO.getIdCabinero() != null){
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(reporteSiniestroDTO.getIdCabinero().intValue());
			if(usuario != null){
				estadoSiniestroForm.setDescripcionCabinero(usuario.getNombreUsuario());
			}else{
				estadoSiniestroForm.setDescripcionCabinero("");
			}
			
		}
		try{
			estadoSiniestroForm.setListaTerminosAjuste(TerminoAjusteDN.getInstancia().listarTodos());
			estadoSiniestroForm.setListaEstatusSiniestro(EstatusSiniestroDN.getInstancia().listarTodos());
			estadoSiniestroForm.setListaSubEstatusSiniestro(SubEstatusSiniestroDN.getInstancia().listarTodos());
		}catch (Exception e) {	
			e.printStackTrace();	
		}
	}
	
	private void poblarDTO(EstadoSiniestroForm estadoSiniestroForm, ReporteSiniestroDTO reporteSiniestroDTO) throws ParseException, SystemException{
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaAsignacion())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaAsignacion());
			reporteSiniestroDTO.setFechaAsignacionAjustador(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaContactoAjustador())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaContactoAjustador());
			reporteSiniestroDTO.setFechaContactoAjustador(fecha);
		}
		
//		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaLlegadaAjustador())) {			
//			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaLlegadaAjustador());
//			reporteSiniestroDTO.setFechaLlegadaAjustador(fecha);
//			reporteSiniestroDTO.setFechaInspeccionAjustador(fecha);
//		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaInspeccionAjustador())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaInspeccionAjustador());
			reporteSiniestroDTO.setFechaLlegadaAjustador(fecha);
			reporteSiniestroDTO.setFechaInspeccionAjustador(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaPreliminar())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaPreliminar());
			reporteSiniestroDTO.setFechaPreliminar(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaDocumento())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaDocumento());
			reporteSiniestroDTO.setFechaDocumento(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaInformeFinal())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaInformeFinal());
			reporteSiniestroDTO.setFechaInformeFinal(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaTerminacionSiniestro())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaTerminacionSiniestro());
//			reporteSiniestroDTO.setFechaSiniestro(fecha);
			reporteSiniestroDTO.setFechaTerminadoSiniestro(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaCerrado())) {			
			Date fecha = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaCerrado());
			reporteSiniestroDTO.setFechaCerradoSiniestro(fecha);
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getComentarios())) {
			if (estadoSiniestroForm.getComentarios().length()>=240){
				estadoSiniestroForm.setComentarios(estadoSiniestroForm.getComentarios().substring(0, 238));
			}
			reporteSiniestroDTO.setComentarios(estadoSiniestroForm.getComentarios());
		}
		
		if (!UtileriasWeb.esCadenaVacia(estadoSiniestroForm.getIdTcEstatusSiniestro())){
			EstatusSiniestroDN estatusSiniestroDN = EstatusSiniestroDN.getInstancia();
			EstatusSiniestroDTO estatusSiniestroDTO = new EstatusSiniestroDTO();
			BigDecimal idTcEstatusSiniestro = new BigDecimal(estadoSiniestroForm.getIdTcEstatusSiniestro());
			estatusSiniestroDTO.setIdTcEstatusSiniestro(idTcEstatusSiniestro);
			estatusSiniestroDTO = estatusSiniestroDN.getPorId(estatusSiniestroDTO);
			reporteSiniestroDTO.setEstatusSiniestro(estatusSiniestroDTO);
		}
		
		if (!UtileriasWeb.esCadenaVacia(estadoSiniestroForm.getIdTcSubEstatusSiniestro())){
			SubEstatusSiniestroDN subEstatusSiniestroDN = SubEstatusSiniestroDN.getInstancia();
			SubEstatusSiniestroDTO subEstatusSiniestroDTO = new SubEstatusSiniestroDTO();
			BigDecimal idTcSubEstatusSiniestro = new BigDecimal(estadoSiniestroForm.getIdTcSubEstatusSiniestro());
			subEstatusSiniestroDTO.setIdTcSubEstatusSiniestro(idTcSubEstatusSiniestro);
			subEstatusSiniestroDTO = subEstatusSiniestroDN.getPorId(subEstatusSiniestroDTO);
			reporteSiniestroDTO.setSubEstatusSiniestro(subEstatusSiniestroDTO);
		}
		
		if (!UtileriasWeb.esCadenaVacia(estadoSiniestroForm.getIdTcTerminoAjuste())){
			TerminoAjusteDN terminoAjusteDN = TerminoAjusteDN.getInstancia();
			TerminoAjusteDTO terminoAjusteDTO = new TerminoAjusteDTO();
			BigDecimal idTcTerminoAjuste = new BigDecimal(estadoSiniestroForm.getIdTcTerminoAjuste());
			terminoAjusteDTO.setIdTcTerminoAjuste(idTcTerminoAjuste);
			terminoAjusteDTO = terminoAjusteDN.getPorId(terminoAjusteDTO);
			reporteSiniestroDTO.setTerminoAjuste(terminoAjusteDTO);
		}
	}

	private boolean fechaUltimaEsMayor(EstadoSiniestroForm estadoSiniestroForm) throws ParseException{
		String informacion="";
		boolean resultado = true;
		Date fechaAsignacion = null;
		Date fechaContactoAjustador = null;
		Date fechaInspeccionAjustador = null;
		Date fechaPreliminar = null;
		Date fechaDocumento = null;
		Date fechaInformeFinal = null;
		Date fechaTerminacionSiniestro = null;
		Date fechaCerrado = null;
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaAsignacion())) {			
			fechaAsignacion = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaAsignacion());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaContactoAjustador())) {			
			fechaContactoAjustador = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaContactoAjustador());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaInspeccionAjustador())) {			
			fechaInspeccionAjustador = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaInspeccionAjustador());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaPreliminar())) {			
			fechaPreliminar = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaPreliminar());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaDocumento())) {			
			fechaDocumento = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaDocumento());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaInformeFinal())) {			
			fechaInformeFinal = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaInformeFinal());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaTerminacionSiniestro())) {			
			fechaTerminacionSiniestro = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaTerminacionSiniestro());
		}
		
		if (!StringUtil.isEmpty(estadoSiniestroForm.getFechaCerrado())) {			
			fechaCerrado = UtileriasWeb.getFechaFromString(estadoSiniestroForm.getFechaCerrado());
		}
		
		if(fechaAsignacion!=null && fechaContactoAjustador !=null && fechaAsignacion.getTime() > fechaContactoAjustador.getTime()){
			informacion = "Fecha Asignaci�n debe ser menor a fecha contacto ajustador";
			estadoSiniestroForm.setDescripcionDato(informacion);
			return false;
		}

		if(fechaInspeccionAjustador!=null && fechaPreliminar!=null && fechaInspeccionAjustador.getTime() > fechaPreliminar.getTime()){
			informacion = "Fecha inspecci�n ajustador bebe ser menor a fecha preliminar";
			estadoSiniestroForm.setDescripcionDato(informacion);
			return false;
		}
		
		if(fechaPreliminar!=null && fechaDocumento!=null && fechaPreliminar.getTime() > fechaDocumento.getTime()){
			informacion = "Fecha preliminar debe ser menor a fecha documento";
			estadoSiniestroForm.setDescripcionDato(informacion);
			return false;
		}
		
		if(fechaDocumento!=null && fechaInformeFinal!=null && fechaDocumento.getTime() > fechaInformeFinal.getTime()){
			informacion = "Fecha documento debe ser menor a fecha informe informal";
			estadoSiniestroForm.setDescripcionDato(informacion);
			return false;
		}
		
		if(fechaInformeFinal!=null && fechaTerminacionSiniestro!=null && fechaInformeFinal.getTime() > fechaTerminacionSiniestro.getTime()){
			informacion = "Fecha informe final debe ser menor a fecha terminaci�n siniestro";
			estadoSiniestroForm.setDescripcionDato(informacion);
			return false;
		}
		
		if(fechaTerminacionSiniestro!=null && fechaCerrado!=null && fechaTerminacionSiniestro.getTime() > fechaCerrado.getTime()){
			informacion = "Fecha terminaci�n siniestro debe ser menor a fecha cerrado";
			estadoSiniestroForm.setDescripcionDato(informacion);
			return false;
		}
		
		return resultado;
	}
}//end class
