package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorForm;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDN;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.contratos.contratocuotaparte.ContratoCuotaParteDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;


/**
 * @author smvr
 */
public class ConfigCorreoTipoNegocioAction extends MidasMappingDispatchAction {

	
	
	public ActionForward mostrar( ActionMapping mapping, ActionForm form,
								  HttpServletRequest request, HttpServletResponse response ) {

		List<TipoNegocioDTO> tiposNegocio = new ArrayList<TipoNegocioDTO>();
		ConfigCorreoTipoNegocioDN correoTipoNegocioDN = ConfigCorreoTipoNegocioDN.getInstancia();
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			tiposNegocio = correoTipoNegocioDN.listarTodos();			
			request.setAttribute("tiposNegocio", tiposNegocio);
			
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los datos del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los datos del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		}	
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarTiposNegocio( ActionMapping mapping,ActionForm form, HttpServletRequest request,
											  HttpServletResponse response ) {

			List<TipoNegocioDTO> listaTiposNegocio = new ArrayList<TipoNegocioDTO>();
			ConfigCorreoTipoNegocioForm configCorreoTiposNegocioForm = (ConfigCorreoTipoNegocioForm) form;
			TipoNegocioDTO tipoNegocioDTO = new TipoNegocioDTO();
			List<ConfigCorreoTipoNegociooDTO> listConfig = null;
			ConfigCorreoTipoNegocioDN correoTipoNegocioDN = ConfigCorreoTipoNegocioDN.getInstancia();
			String reglaNavegacion = Sistema.EXITOSO;
			String json = "";
			
			try {
				
				listaTiposNegocio = correoTipoNegocioDN.listarTodos();
				
				for( TipoNegocioDTO tipoNegocioDto: listaTiposNegocio ) {
					
					listConfig = correoTipoNegocioDN.getConfigCorreoTipoNegocio( tipoNegocioDto.getIdTcTipoNegocio() );
					tipoNegocioDto.setConfigCorreoTipoNegocioDTOs( listConfig );
					
				}
				
				json = getJsonTablaTipoNegocio( listaTiposNegocio );

				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			} catch (IOException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
			} catch (Exception e) {
				LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
			}			

			return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarTiposNegocioFiltrado( ActionMapping mapping,ActionForm form, HttpServletRequest request,
			  HttpServletResponse response ) {

		List<TipoNegocioDTO> listaTiposNegocio = new ArrayList<TipoNegocioDTO>();
		List<TipoNegocioDTO> tiposNegocio = new ArrayList<TipoNegocioDTO>();
		ConfigCorreoTipoNegocioForm configCorreoTiposNegocioForm = (ConfigCorreoTipoNegocioForm) form;
		TipoNegocioDTO tipoNegocioDTO = new TipoNegocioDTO();
		List<ConfigCorreoTipoNegociooDTO> listConfig = null;
		ConfigCorreoTipoNegocioDN correoTipoNegocioDN = ConfigCorreoTipoNegocioDN.getInstancia();
		String reglaNavegacion = Sistema.EXITOSO;
		String json = "";
		
		try {
		
			tiposNegocio = correoTipoNegocioDN.listarTodos();			
			request.setAttribute("tiposNegocio", tiposNegocio);
			
			tipoNegocioDTO = this.poblarDTO( configCorreoTiposNegocioForm, tipoNegocioDTO );			
			listaTiposNegocio = correoTipoNegocioDN.listarFiltrado( tipoNegocioDTO );
			
			for( TipoNegocioDTO tipoNegocioDto: listaTiposNegocio ) {
				
				listConfig = correoTipoNegocioDN.getConfigCorreoTipoNegocio( tipoNegocioDto.getIdTcTipoNegocio() );
				tipoNegocioDto.setConfigCorreoTipoNegocioDTOs( listConfig );
				
			}
			
			json = getJsonTablaTipoNegocio( listaTiposNegocio );
			
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();
			
		} catch (IOException e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		}			
		
		return mapping.findForward(reglaNavegacion);
	}
	
	public void guardar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		List<TipoNegocioDTO> tiposNegocio = new ArrayList<TipoNegocioDTO>();
		ConfigCorreoTipoNegocioDN correoTipoNegocioDN = ConfigCorreoTipoNegocioDN.getInstancia();
		ConfigCorreoTipoNegocioForm configCorreoTiposNegocioForm = (ConfigCorreoTipoNegocioForm) form;
		ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO = new ConfigCorreoTipoNegociooDTO();
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			configCorreoTipoNegocioDTO = this.poblarConfigDTO( configCorreoTiposNegocioForm, configCorreoTipoNegocioDTO );
			configCorreoTipoNegocioDTO.setUsuarioNombre( UtileriasWeb.obtieneNombreUsuario(request) );
			configCorreoTipoNegocioDTO.setFechaModificacion( new Date() );
			
			correoTipoNegocioDN.agregar( configCorreoTipoNegocioDTO );
			
			this.limpiarForm( configCorreoTiposNegocioForm );
			tiposNegocio = correoTipoNegocioDN.listarTodos();			
			request.setAttribute("tiposNegocio", tiposNegocio);
		
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al guardar el correo del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al guardar el correo del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public void actualizar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		ConfigCorreoTipoNegocioDN correoTipoNegocioDN = ConfigCorreoTipoNegocioDN.getInstancia();
		ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO = new ConfigCorreoTipoNegociooDTO();
		String idConfigTipoNegocio = request.getParameter( "idConfigTipoNegocio" );
		String correo = request.getParameter( "correo" );
		
		try {
			
			configCorreoTipoNegocioDTO = correoTipoNegocioDN.getConfigCorreoTipoNegocioPorId( new BigDecimal (idConfigTipoNegocio) );
			
			configCorreoTipoNegocioDTO.setCorreo( correo );
			configCorreoTipoNegocioDTO.setUsuarioNombre( UtileriasWeb.obtieneNombreUsuario(request) );
			configCorreoTipoNegocioDTO.setFechaModificacion( new Date() ); 
			
			correoTipoNegocioDN.actualizar( configCorreoTipoNegocioDTO );
		
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al actualizar el correo del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al actualizar el correo del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public void eliminar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		ConfigCorreoTipoNegocioDN correoTipoNegocioDN = ConfigCorreoTipoNegocioDN.getInstancia();
		ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO = new ConfigCorreoTipoNegociooDTO();
		String idConfigTipoNegocio = request.getParameter( "idConfigTipoNegocio" );
		
		try {
			
			configCorreoTipoNegocioDTO = correoTipoNegocioDN.getConfigCorreoTipoNegocioPorId( new BigDecimal (idConfigTipoNegocio) );
			correoTipoNegocioDN.eliminar( configCorreoTipoNegocioDTO );
		
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al eliminar el correo del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al eliminar el correo del tipo de negocio: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public String getJsonTablaTipoNegocio(List<TipoNegocioDTO> listaTiposNegocio ){
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		MidasJsonBase json = new MidasJsonBase();
		
		if( listaTiposNegocio != null && listaTiposNegocio.size() > 0 ) {
			
			for( TipoNegocioDTO tipoNegocioDTO : listaTiposNegocio ) {
				MidasJsonRow row;
				String tipoNegocio = "";
				String correo = "";
				String usuario = "";
				String fecha = "";
				String iconoEliminar = null;
				
				if (tipoNegocioDTO.getIdTcTipoNegocio() != null ){
					tipoNegocio   = tipoNegocioDTO.getCodigoTipoNegocio() + " - " + tipoNegocioDTO.getDescripcionTipoNegocio();
					
					if( tipoNegocioDTO.getConfigCorreoTipoNegocioDTOs() != null && tipoNegocioDTO.getConfigCorreoTipoNegocioDTOs().size() > 0 ) {
						for( ConfigCorreoTipoNegociooDTO confCorreoTipoNegocio: tipoNegocioDTO.getConfigCorreoTipoNegocioDTOs() ) {
							row = new MidasJsonRow();
							
							if( confCorreoTipoNegocio.getCorreo() != null ) {
								correo   = confCorreoTipoNegocio.getCorreo();
							}else{
								correo   = "";
							}

							if( confCorreoTipoNegocio.getUsuarioNombre() != null ) {
								usuario   = confCorreoTipoNegocio.getUsuarioNombre();
							}else {
								usuario   = "";
							}
							
							if( confCorreoTipoNegocio.getFechaModificacion() != null ) {
								fecha     = simpleDateFormat.format( confCorreoTipoNegocio.getFechaModificacion() );
							}else {
								fecha     = "";
							}
							iconoEliminar = "/MidasWeb/img/delete14.gif^Borrar^javascript:eliminarConfigTipoNegocio(" + 
											 confCorreoTipoNegocio.getIdTcTipoNegocio() + "," + 
											 confCorreoTipoNegocio.getIdConfigTipoNegocio() + ");^_self";
							
							row.setId( confCorreoTipoNegocio.getIdConfigTipoNegocio().toString() );
							row.setDatos( tipoNegocio, correo, usuario, fecha, iconoEliminar	);
							json.addRow (row);
						}
					}
				}	
				
				
			}
		}
		
		return json.toString();
	}
	
	private TipoNegocioDTO poblarDTO( ConfigCorreoTipoNegocioForm form, TipoNegocioDTO dto ) {
		List<ConfigCorreoTipoNegociooDTO> listConfig = new ArrayList<ConfigCorreoTipoNegociooDTO>();
		ConfigCorreoTipoNegociooDTO configDTO = new ConfigCorreoTipoNegociooDTO();
		
		if( form.getIdTcTipoNegocio() != null ) {
			dto.setIdTcTipoNegocio( new BigDecimal(form.getIdTcTipoNegocio()) );
		}
		
		if( form.getCorreo() != null ) {
			configDTO.setCorreo( form.getCorreo() );
		}
		
		listConfig.add( configDTO );
		dto.setConfigCorreoTipoNegocioDTOs( listConfig );
		
		return dto;
	}
	
	private ConfigCorreoTipoNegociooDTO poblarConfigDTO( ConfigCorreoTipoNegocioForm form, ConfigCorreoTipoNegociooDTO dto ) {
		
		if( form.getIdTcTipoNegocio() != null ) {
			dto.setIdTcTipoNegocio( new BigDecimal(form.getIdTcTipoNegocio()) );
		}
		
		if( form.getCorreo() != null ) {
			dto.setCorreo( form.getCorreo() );
		}
		
		return dto;
	}
	
	private void limpiarForm(ConfigCorreoTipoNegocioForm form){
		
		form.setIdTcTipoNegocio("0");
		form.setCorreo("");
	}

}
