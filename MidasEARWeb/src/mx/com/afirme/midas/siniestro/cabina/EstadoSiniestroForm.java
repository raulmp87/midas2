/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class EstadoSiniestroForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idToReporteSiniestro;
	private String fechaAsignacion;
	private String fechaContactoAjustador;
	private String fechaLlegadaAjustador;
	private String fechaInspeccionAjustador;
	private String fechaPreliminar;
	private String fechaDocumento;
	private String fechaInformeFinal;
	private String fechaTerminacionSiniestro;
	private String fechaCerrado;
	private String descripcionSiniestro;
	private String comentarios;
	private String nombreAsegurado;
	private String estatus;
	private String terminoAjuste;
	
	private String dato;
	private String descripcionDato;
	
	private String descripcionCabinero;
	private String etapaReporte;
	
	private String idTcTerminoAjuste;
	private String descripcionTerminoAjuste;
	private String idTcEstatusSiniestro;
	private String descripcionEstatusSiniestro;
	private String idTcSubEstatusSiniestro;
	private String descripcionSubEstatusSiniestro;
	private List<TerminoAjusteDTO> listaTerminosAjuste;
	private List<EstatusSiniestroDTO> listaEstatusSiniestro;
	private List<SubEstatusSiniestroDTO> listaSubEstatusSiniestro;
	
	
	
	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}
	/**
	 * @param comentarios the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	/**
	 * @return the descripcionTerminoAjuste
	 */
	public String getDescripcionTerminoAjuste() {
		return descripcionTerminoAjuste;
	}
	/**
	 * @param descripcionTerminoAjuste the descripcionTerminoAjuste to set
	 */
	public void setDescripcionTerminoAjuste(String descripcionTerminoAjuste) {
		this.descripcionTerminoAjuste = descripcionTerminoAjuste;
	}
	/**
	 * @return the descripcionEstatusSiniestro
	 */
	public String getDescripcionEstatusSiniestro() {
		return descripcionEstatusSiniestro;
	}
	/**
	 * @param descripcionEstatusSiniestro the descripcionEstatusSiniestro to set
	 */
	public void setDescripcionEstatusSiniestro(String descripcionEstatusSiniestro) {
		this.descripcionEstatusSiniestro = descripcionEstatusSiniestro;
	}
	/**
	 * @return the descripcionSubEstatusSiniestro
	 */
	public String getDescripcionSubEstatusSiniestro() {
		return descripcionSubEstatusSiniestro;
	}
	/**
	 * @param descripcionSubEstatusSiniestro the descripcionSubEstatusSiniestro to set
	 */
	public void setDescripcionSubEstatusSiniestro(
			String descripcionSubEstatusSiniestro) {
		this.descripcionSubEstatusSiniestro = descripcionSubEstatusSiniestro;
	}
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	/**
	 * @return the idTcTerminoAjuste
	 */
	public String getIdTcTerminoAjuste() {
		return idTcTerminoAjuste;
	}
	/**
	 * @param idTcTerminoAjuste the idTcTerminoAjuste to set
	 */
	public void setIdTcTerminoAjuste(String idTcTerminoAjuste) {
		this.idTcTerminoAjuste = idTcTerminoAjuste;
	}
	/**
	 * @return the idTcEstatusSiniestro
	 */
	public String getIdTcEstatusSiniestro() {
		return idTcEstatusSiniestro;
	}
	/**
	 * @param idTcEstatusSiniestro the idTcEstatusSiniestro to set
	 */
	public void setIdTcEstatusSiniestro(String idTcEstatusSiniestro) {
		this.idTcEstatusSiniestro = idTcEstatusSiniestro;
	}
	/**
	 * @return the idTcSubEstatusSiniestro
	 */
	public String getIdTcSubEstatusSiniestro() {
		return idTcSubEstatusSiniestro;
	}
	/**
	 * @param idTcSubEstatusSiniestro the idTcSubEstatusSiniestro to set
	 */
	public void setIdTcSubEstatusSiniestro(String idTcSubEstatusSiniestro) {
		this.idTcSubEstatusSiniestro = idTcSubEstatusSiniestro;
	}
	/**
	 * @return the listaTerminosAjuste
	 */
	public List<TerminoAjusteDTO> getListaTerminosAjuste() {
		return listaTerminosAjuste;
	}
	/**
	 * @param listaTerminosAjuste the listaTerminosAjuste to set
	 */
	public void setListaTerminosAjuste(List<TerminoAjusteDTO> listaTerminosAjuste) {
		this.listaTerminosAjuste = listaTerminosAjuste;
	}
	/**
	 * @return the listaEstatusSiniestro
	 */
	public List<EstatusSiniestroDTO> getListaEstatusSiniestro() {
		return listaEstatusSiniestro;
	}
	/**
	 * @param listaEstatusSiniestro the listaEstatusSiniestro to set
	 */
	public void setListaEstatusSiniestro(
			List<EstatusSiniestroDTO> listaEstatusSiniestro) {
		this.listaEstatusSiniestro = listaEstatusSiniestro;
	}
	/**
	 * @return the listaSubEstatusSiniestro
	 */
	public List<SubEstatusSiniestroDTO> getListaSubEstatusSiniestro() {
		return listaSubEstatusSiniestro;
	}
	/**
	 * @param listaSubEstatusSiniestro the listaSubEstatusSiniestro to set
	 */
	public void setListaSubEstatusSiniestro(
			List<SubEstatusSiniestroDTO> listaSubEstatusSiniestro) {
		this.listaSubEstatusSiniestro = listaSubEstatusSiniestro;
	}
	/**
	 * @return the fechaAsignacion
	 */
	public String getFechaAsignacion() {
		return fechaAsignacion;
	}
	/**
	 * @param fechaAsignacion the fechaAsignacion to set
	 */
	public void setFechaAsignacion(String fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}
	/**
	 * @return the fechaContactoAjustador
	 */
	public String getFechaContactoAjustador() {
		return fechaContactoAjustador;
	}
	/**
	 * @param fechaContactoAjustador the fechaContactoAjustador to set
	 */
	public void setFechaContactoAjustador(String fechaContactoAjustador) {
		this.fechaContactoAjustador = fechaContactoAjustador;
	}
	/**
	 * @return the fechaLlegadaAjustador
	 */
	public String getFechaLlegadaAjustador() {
		return fechaLlegadaAjustador;
	}
	/**
	 * @param fechaLlegadaAjustador the fechaLlegadaAjustador to set
	 */
	public void setFechaLlegadaAjustador(String fechaLlegadaAjustador) {
		this.fechaLlegadaAjustador = fechaLlegadaAjustador;
	}
	/**
	 * @return the fechaInspeccionAjustador
	 */
	public String getFechaInspeccionAjustador() {
		return fechaInspeccionAjustador;
	}
	/**
	 * @param fechaInspeccionAjustador the fechaInspeccionAjustador to set
	 */
	public void setFechaInspeccionAjustador(String fechaInspeccionAjustador) {
		this.fechaInspeccionAjustador = fechaInspeccionAjustador;
	}
	/**
	 * @return the fechaPreliminar
	 */
	public String getFechaPreliminar() {
		return fechaPreliminar;
	}
	/**
	 * @param fechaPreliminar the fechaPreliminar to set
	 */
	public void setFechaPreliminar(String fechaPreliminar) {
		this.fechaPreliminar = fechaPreliminar;
	}
	/**
	 * @return the fechaDocumento
	 */
	public String getFechaDocumento() {
		return fechaDocumento;
	}
	/**
	 * @param fechaDocumento the fechaDocumento to set
	 */
	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	/**
	 * @return the fechaInformeFinal
	 */
	public String getFechaInformeFinal() {
		return fechaInformeFinal;
	}
	/**
	 * @param fechaInformeFinal the fechaInformeFinal to set
	 */
	public void setFechaInformeFinal(String fechaInformeFinal) {
		this.fechaInformeFinal = fechaInformeFinal;
	}
	/**
	 * @return the fechaTerminacionSiniestro
	 */
	public String getFechaTerminacionSiniestro() {
		return fechaTerminacionSiniestro;
	}
	/**
	 * @param fechaTerminacionSiniestro the fechaTerminacionSiniestro to set
	 */
	public void setFechaTerminacionSiniestro(String fechaTerminacionSiniestro) {
		this.fechaTerminacionSiniestro = fechaTerminacionSiniestro;
	}
	/**
	 * @return the fechaCerrado
	 */
	public String getFechaCerrado() {
		return fechaCerrado;
	}
	/**
	 * @param fechaCerrado the fechaCerrado to set
	 */
	public void setFechaCerrado(String fechaCerrado) {
		this.fechaCerrado = fechaCerrado;
	}
	/**
	 * @return the descripcionSiniestro
	 */
	public String getDescripcionSiniestro() {
		return descripcionSiniestro;
	}
	/**
	 * @param descripcionSiniestro the descripcionSiniestro to set
	 */
	public void setDescripcionSiniestro(String descripcionSiniestro) {
		this.descripcionSiniestro = descripcionSiniestro;
	}
	/**
	 * @return the nombreAsegurado
	 */
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	/**
	 * @param nombreAsegurado the nombreAsegurado to set
	 */
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the terminoAjuste
	 */
	public String getTerminoAjuste() {
		return terminoAjuste;
	}
	/**
	 * @param terminoAjuste the terminoAjuste to set
	 */
	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}
	/**
	 * @return the dato
	 */
	public String getDato() {
		return dato;
	}
	/**
	 * @param dato the dato to set
	 */
	public void setDato(String dato) {
		this.dato = dato;
	}
	/**
	 * @return the descripcionDato
	 */
	public String getDescripcionDato() {
		return descripcionDato;
	}
	/**
	 * @param descripcionDato the descripcionDato to set
	 */
	public void setDescripcionDato(String descripcionDato) {
		this.descripcionDato = descripcionDato;
	}
	/**
	 * @return the descripcionCabinero
	 */
	public String getDescripcionCabinero() {
		return descripcionCabinero;
	}
	/**
	 * @param descripcionCabinero the descripcionCabinero to set
	 */
	public void setDescripcionCabinero(String descripcionCabinero) {
		this.descripcionCabinero = descripcionCabinero;
	}
	/**
	 * @return the etapaReporte
	 */
	public String getEtapaReporte() {
		return etapaReporte;
	}
	/**
	 * @param etapaReporte the etapaReporte to set
	 */
	public void setEtapaReporte(String etapaReporte) {
		this.etapaReporte = etapaReporte;
	}
	
	
}
