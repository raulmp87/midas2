/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;


import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author jmartinez
 *
 */
public class ReportePreguntasEspecialesAction extends MidasMappingDispatchAction {

	/**
	 * Method reporteDePreguntasEspeciales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public ActionForward reporteDePreguntasEspeciales(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {

		PreguntasEspecialesForm preguntasEspecialesForm 	= (PreguntasEspecialesForm) form;
		
		ReporteSiniestroDN reporteSiniestroDN =	ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));

		BigDecimal idToReporteSiniestro	= new BigDecimal(preguntasEspecialesForm.getIdToReporteSiniestro()); 

		ReporteSiniestroDTO reporteSiniestroDTO	= reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
		PreguntasEspecialesDTO	preguntasEspecialesDTO	= reporteSiniestroDTO.getPreguntasEspeciales();
		
		if(preguntasEspecialesDTO != null){
			poblarForm(preguntasEspecialesDTO,preguntasEspecialesForm);
		}
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		return mapping.findForward(reglaNavegacion);
	}


	/**
	 * Method reporteDePreguntasEspeciales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 * @throws ParseException 
	 */
	public void guardarReportePreguntasEspeciales(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws ParseException{

		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			PreguntasEspecialesDTO	peDTO;
			
			ReporteSiniestroDN dd 			= ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			
			String strIdToReporteSiniestro  = request.getParameter("idToReporteSiniestro");
			BigDecimal d 					= new BigDecimal(strIdToReporteSiniestro); 

			ReporteSiniestroDTO dto 		= dd.desplegarReporte(d);
			
			if(dto.getPreguntasEspeciales() != null){
				peDTO = dto.getPreguntasEspeciales();
			}else{
				peDTO = new PreguntasEspecialesDTO();
				peDTO.setIdToReporteSiniestro(d);
			}
			
			PreguntasEspecialesForm peForm 	= (PreguntasEspecialesForm) form;
			poblarDTO(peForm,peDTO);
		
			if(dto.getPreguntasEspeciales() != null){
				PreguntasEspecialesDN.getInstancia().borrarPreguntaEspecial(peDTO);
				PreguntasEspecialesDN.getInstancia().agregarPreguntaEspecial(peDTO);
			}else{
				PreguntasEspecialesDN.getInstancia().agregarPreguntaEspecial(peDTO);
			}
			UtileriasWeb.imprimeMensajeXML("1", "Las Preguntas especiales \n se guardaron de forma correcta" , response);
			
		} catch (SystemException e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.sistema.nodisponible") , response);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		}
	}

	
	
	private void poblarForm(PreguntasEspecialesDTO preguntaDTO, PreguntasEspecialesForm preguntaForm) {
		
		if(preguntaDTO.getCargo() != null){
			preguntaForm.setCargoOcupa(preguntaDTO.getCargo());
		}
		
		if(preguntaDTO.getCausaReclamacion() != null){
			preguntaForm.setCausaReclamacion(preguntaDTO.getCausaReclamacion());
		}
		
		if(preguntaDTO.getComentariosReclamante() != null){
			preguntaForm.setComentario(preguntaDTO.getComentariosReclamante());
		}
		
		if(preguntaDTO.getDependencia() != null){
			preguntaForm.setDependenciaTrabaja(preguntaDTO.getDependencia());
		}
		
		if(preguntaDTO.getDescripcionPuesto() != null){
			preguntaForm.setDescripcionPuesto(preguntaDTO.getDescripcionPuesto());
		}
		
		if(preguntaDTO.getEmail() != null){
			preguntaForm.setCorreo(preguntaDTO.getEmail());
		}
		
		if(preguntaDTO.getLugarOficio() != null){
			preguntaForm.setLugarHizoOficio(preguntaDTO.getLugarOficio());
		}
		
		if(preguntaDTO.getFechaReclamacion() != null){
			String fecha = UtileriasWeb.getFechaString(preguntaDTO.getFechaReclamacion());
			preguntaForm.setFechaNotificacion(fecha);
		}
				
		//big
		if(preguntaDTO.getIdToReporteSiniestro() != null){
			preguntaForm.setIdToReporteSiniestro(preguntaDTO.getIdToReporteSiniestro().toString());
		}
		
		if(preguntaDTO.getNombreDependencia() != null){
			preguntaForm.setNombreDependencia(preguntaDTO.getNombreDependencia());
		}
		
		if(preguntaDTO.getNumeroCertificado() !=  null){
			preguntaForm.setNoCertificado(preguntaDTO.getNumeroCertificado());
		}
		
		if(preguntaDTO.getNumeroExpediente() != null){
			preguntaForm.setNumeroExpediente(preguntaDTO.getNumeroExpediente());
		}
		
		if(preguntaDTO.getNumeroOficio() != null){
			preguntaForm.setNumeroOficio(preguntaDTO.getNumeroOficio());
		}

		if(preguntaDTO.getPersonaRealizoOficio() != null){
			preguntaForm.setPersonaRealizoOficio(preguntaDTO.getPersonaRealizoOficio());
		}
		
		if(preguntaDTO.getPlazoReclamacion() != null){
			preguntaForm.setPlazoReclamacion(preguntaDTO.getPlazoReclamacion().trim());
		}

		if(preguntaDTO.getTienePlazoReclamacion() != null){
			preguntaForm.setTienePlazoReclamacion(preguntaDTO.getTienePlazoReclamacion());
		}
		
		if(preguntaDTO.getServiciosProfesionales() != null){
			preguntaForm.setServiciosProfesionales(preguntaDTO.getServiciosProfesionales());
		}

		
		if(preguntaDTO.getSiniestroCubierto() != null){
			preguntaForm.setEstaCubierto(preguntaDTO.getSiniestroCubierto());
		}
		
		if(preguntaDTO.getTelefonoFuncionario() != null){
			preguntaForm.setTelefono(preguntaDTO.getTelefonoFuncionario());
		}
		
		if(preguntaDTO.getTipoCobertura() != null){
			preguntaForm.setTipoCobertura(preguntaDTO.getTipoCobertura());
		}
		
		if(preguntaDTO.getTipoDocumento() != null){
			preguntaForm.setTipoDocumentoRecibio(preguntaDTO.getTipoDocumento());
		}
		
		if(preguntaDTO.getTipoAvisoDetalle() != null){
			preguntaForm.setDetallar(preguntaDTO.getTipoAvisoDetalle());
		}
		//Agregados 
		if(preguntaDTO.getNombrefuncionario() != null){
			preguntaForm.setNomFuncionario(preguntaDTO.getNombrefuncionario());
		}
		if(preguntaDTO.getCalle() != null){
			preguntaForm.setDireccionFuncionario(preguntaDTO.getCalle());
		}
		if(preguntaDTO.getIdestado() != null){
			preguntaForm.setIdEstado(preguntaDTO.getIdestado());
		}
		if(preguntaDTO.getIdciudad() != null){
			preguntaForm.setIdCiudad(preguntaDTO.getIdciudad());
		}
		if(preguntaDTO.getIdcolonia() != null){
			preguntaForm.setIdColonia(preguntaDTO.getIdcolonia());
		}
		if(preguntaDTO.getCodigopostal() != null){
			preguntaForm.setCodigoPostal(preguntaDTO.getCodigopostal().toString());
		}
		

	}
	
	private void poblarDTO(PreguntasEspecialesForm preguntaForm,PreguntasEspecialesDTO preguntaDTO)throws SystemException, ParseException{
		
		if(!StringUtil.isEmpty(preguntaForm.getCargoOcupa())){
			preguntaDTO.setCargo(preguntaForm.getCargoOcupa());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getCausaReclamacion()) ){
			preguntaDTO.setCausaReclamacion(preguntaForm.getCausaReclamacion());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getComentario())){
			preguntaDTO.setComentariosReclamante(preguntaForm.getComentario());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getDependenciaTrabaja())){
			preguntaDTO.setDependencia(preguntaForm.getDependenciaTrabaja());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getDescripcionPuesto())){
			preguntaDTO.setDescripcionPuesto(preguntaForm.getDescripcionPuesto());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getCorreo())){
			preguntaDTO.setEmail(preguntaForm.getCorreo());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getLugarHizoOficio())){
			preguntaDTO.setLugarOficio(preguntaForm.getLugarHizoOficio());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getFechaNotificacion())){
			Date fecha = UtileriasWeb.getFechaFromString(preguntaForm.getFechaNotificacion());
			preguntaDTO.setFechaReclamacion(fecha);
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getNombreDependencia())){
			preguntaDTO.setNombreDependencia(preguntaForm.getNombreDependencia());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getNoCertificado())){
			preguntaDTO.setNumeroCertificado(preguntaForm.getNoCertificado());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getNumeroExpediente())){
			preguntaDTO.setNumeroExpediente(preguntaForm.getNumeroExpediente());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getNumeroOficio())){
			preguntaDTO.setNumeroOficio(preguntaForm.getNumeroOficio());
		}

		if(!StringUtil.isEmpty(preguntaForm.getPersonaRealizoOficio())){
			preguntaDTO.setPersonaRealizoOficio(preguntaForm.getPersonaRealizoOficio());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getPlazoReclamacion())){
			preguntaDTO.setPlazoReclamacion(preguntaForm.getPlazoReclamacion());
		}

		if(!StringUtil.isEmpty(preguntaForm.getServiciosProfesionales())){
			preguntaDTO.setServiciosProfesionales(preguntaForm.getServiciosProfesionales());
		}

		if(!StringUtil.isEmpty(preguntaForm.getTelefono())){
			preguntaDTO.setTelefonoFuncionario(preguntaForm.getTelefono());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getDetallar())){
			preguntaDTO.setTipoAvisoDetalle(preguntaForm.getDetallar());
		}
		
		if(preguntaDTO.getTipoAvisoDetalle() != null){
				preguntaForm.setDetallar(preguntaDTO.getTipoAvisoDetalle());
		}

		if(!StringUtil.isEmpty(preguntaForm.getTipoCobertura())){
			preguntaDTO.setTipoCobertura(preguntaForm.getTipoCobertura());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getTipoDocumentoRecibio())){
			preguntaDTO.setTipoDocumento(preguntaForm.getTipoDocumentoRecibio());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getTienePlazoReclamacion())){
			preguntaDTO.setTienePlazoReclamacion(preguntaForm.getTienePlazoReclamacion());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getTipoDocumentoRecibio())){
			preguntaDTO.setTipoDocumento(preguntaForm.getTipoDocumentoRecibio());
		}
		
		if(!StringUtil.isEmpty(preguntaForm.getEstaCubierto())){
			preguntaDTO.setSiniestroCubierto(preguntaForm.getEstaCubierto());
		}
		
		//Agregados de Preguntas Especiales
		if(!StringUtil.isEmpty(preguntaForm.getNomFuncionario())){
			preguntaDTO.setNombrefuncionario(preguntaForm.getNomFuncionario());
		}
		if(!StringUtil.isEmpty(preguntaForm.getDireccionFuncionario())){
			preguntaDTO.setCalle(preguntaForm.getDireccionFuncionario());
		}
		if(!StringUtil.isEmpty(preguntaForm.getIdEstado())){
			preguntaDTO.setIdestado(preguntaForm.getIdEstado());
		}
		if(!StringUtil.isEmpty(preguntaForm.getIdCiudad())){
			preguntaDTO.setIdciudad(preguntaForm.getIdCiudad());
		}
		if(!StringUtil.isEmpty(preguntaForm.getIdColonia())){
			preguntaDTO.setIdcolonia(preguntaForm.getIdColonia());
		}
		if(!StringUtil.isEmpty(preguntaForm.getCodigoPostal())){
			preguntaDTO.setCodigopostal(new BigDecimal(preguntaForm.getCodigoPostal()));
		}
		
	}
	
}//end class
