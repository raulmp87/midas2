/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.giro.GiroDN;
import mx.com.afirme.midas.catalogos.giro.GiroDTO;
import mx.com.afirme.midas.catalogos.giro.GiroSN;
import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author smvr
 *
 */
public class ConfigCorreoTipoNegocioDN {

	public static final ConfigCorreoTipoNegocioDN INSTANCIA = new ConfigCorreoTipoNegocioDN();

	public static ConfigCorreoTipoNegocioDN getInstancia() {
		return ConfigCorreoTipoNegocioDN.INSTANCIA;
	}

	public void agregar(ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		configCorreoTipoNegocioSN.agregar(configCorreoTipoNegocioDTO);
	}

	public void eliminar(ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		configCorreoTipoNegocioSN.eliminar(configCorreoTipoNegocioDTO);
	}

	public ConfigCorreoTipoNegociooDTO actualizar(ConfigCorreoTipoNegociooDTO configCorreoTipoNegocioDTO) throws ExcepcionDeAccesoADatos,
			SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		return configCorreoTipoNegocioSN.actualizar(configCorreoTipoNegocioDTO);
	}

	public ConfigCorreoTipoNegociooDTO getConfigCorreoTipoNegocioPorId(BigDecimal idConfigTipoNegocio)
			throws ExcepcionDeAccesoADatos, SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		return configCorreoTipoNegocioSN.getConfigCorreoTipoNegocioPorId(idConfigTipoNegocio);
	}
	
	public List<ConfigCorreoTipoNegociooDTO> getConfigCorreoTipoNegocio(BigDecimal idTcTipoNegocio)
		throws ExcepcionDeAccesoADatos, SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		return configCorreoTipoNegocioSN.getConfigCorreoTipoNegocio(idTcTipoNegocio);
	}

	public List<TipoNegocioDTO> listarTodos() throws ExcepcionDeAccesoADatos,
			SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		return configCorreoTipoNegocioSN.listarTodos();
	}

	public List<TipoNegocioDTO> listarFiltrado(TipoNegocioDTO tipoNegocioDTO)
			throws ExcepcionDeAccesoADatos, SystemException {
		ConfigCorreoTipoNegocioSN configCorreoTipoNegocioSN = new ConfigCorreoTipoNegocioSN();
		return configCorreoTipoNegocioSN.listarFiltrado(tipoNegocioDTO);
	}
}
