/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;


import java.math.BigDecimal;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * @author jmartinez
 *
 */
public class ReporteTransporteAction extends MidasMappingDispatchAction {

	/**
	 * Method reporteDeTransportes
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward reporteDeTransportes(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)  throws SystemException,
			ExcepcionDeAccesoADatos {

		ReporteSiniestroDN dd 			= ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DatosTransportistaDN datosTransportistaDN = DatosTransportistaDN.getInstancia();
		String strIdToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal d 				   = new BigDecimal(strIdToReporteSiniestro); 

		DatosTransportistaDTO transDTO = new DatosTransportistaDTO();
		transDTO.setIdToReporteSiniestro(d);
		
		transDTO = datosTransportistaDN.obtieneTransportistaPorId(transDTO);
		
		TransportesForm transForm 		= (TransportesForm) form;
		if(transDTO!=null){
			poblarForm(transDTO,transForm);
		}
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		return mapping.findForward(reglaNavegacion);
	}

	/**
	 * Method reporteDePreguntasEspeciales
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 * @throws ParseException 
	 */
	public void guardarReporteTransportista(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		try {
			TransportesForm traForm 	= (TransportesForm)form;
			DatosTransportistaDN datosTransportistaDN = DatosTransportistaDN.getInstancia();
			
			BigDecimal idToReporteSiniestro   = new BigDecimal(traForm.getIdToReporteSiniestro()); 

			DatosTransportistaDTO datoTransportistaDTO = new DatosTransportistaDTO();
			
			datoTransportistaDTO.setIdToReporteSiniestro(idToReporteSiniestro);
			datoTransportistaDTO = datosTransportistaDN.obtieneTransportistaPorId(datoTransportistaDTO);
			
			if(datoTransportistaDTO != null){
				poblarDTO(traForm,datoTransportistaDTO);
				datosTransportistaDN.modificarTransportista(datoTransportistaDTO);
			}else{
				datoTransportistaDTO = new DatosTransportistaDTO();
				poblarDTO(traForm,datoTransportistaDTO);
				datosTransportistaDN.agregarTransportista(datoTransportistaDTO);
			}
			
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}

	
	private void poblarForm(DatosTransportistaDTO transDTO, TransportesForm transForm){
		if(transDTO.getCalle() != null){
			transForm.setCalle(transDTO.getCalle());
		}
		
		if (transDTO.getColonia() != null){
			transForm.setIdColonia(transDTO.getColonia());
		}
		
		if(transDTO.getCodigoPostal() != null){
			transForm.setCp(transDTO.getCodigoPostal().toString());
		}
		
		if(transDTO.getIdEstado() != null){
			transForm.setIdEstado(transDTO.getIdEstado());
		}
		
		if(transDTO.getIdMunicipio() != null){
			transForm.setIdCiudad(transDTO.getIdMunicipio());
		}
		
		if(transDTO.getRazonSocial() != null){
			transForm.setNombreRazonSocial(transDTO.getRazonSocial());
		}
		
		if(transDTO.getNumeroUnidad() != null){
			transForm.setNoUnidad(transDTO.getNumeroUnidad());
		}

		if(transDTO.getRfc() != null){
			transForm.setRfc_curp(transDTO.getRfc());
		}
		
		if(transDTO.getTelefono() != null){
			transForm.setTelefono(transDTO.getTelefono());
		}
		
		if(transDTO.getTipoPersona() != null){
			transForm.setTipoPersona(transDTO.getTipoPersona());
		}
		
		if(transDTO.getIdToReporteSiniestro() != null){
			transForm.setIdToReporteSiniestro(transDTO.getIdToReporteSiniestro().toString());
		}
	}
	
	private void poblarDTO(TransportesForm transForm,DatosTransportistaDTO transDTO) throws SystemException{
		
		if (!StringUtil.isEmpty(transForm.getCalle())){
			transDTO.setCalle(transForm.getCalle());
		}
		
		if (!StringUtil.isEmpty(transForm.getIdColonia())){
			transDTO.setColonia(transForm.getIdColonia());
		}
		
		if (!StringUtil.isEmpty(transForm.getCp())){
			transDTO.setCodigoPostal(new BigDecimal(transForm.getCp()));
		}
		
		if (!StringUtil.isEmpty(transForm.getIdEstado())){
			transDTO.setIdEstado(transForm.getIdEstado());
		}
		
		if (!StringUtil.isEmpty(transForm.getIdCiudad())){
			transDTO.setIdMunicipio(transForm.getIdCiudad());
		}
		
		if (!StringUtil.isEmpty(transForm.getNombreRazonSocial())){
			transDTO.setRazonSocial(transForm.getNombreRazonSocial());
		}
		
		if (!StringUtil.isEmpty(transForm.getNoUnidad())){
			transDTO.setNumeroUnidad(transForm.getNoUnidad());
		}
		
		if (!StringUtil.isEmpty(transForm.getRfc_curp())){
			transDTO.setRfc(transForm.getRfc_curp());
		}
		
		if (!StringUtil.isEmpty(transForm.getTelefono())){
			transDTO.setTelefono(transForm.getTelefono());
		}
		
		if (!StringUtil.isEmpty(transForm.getTipoPersona())){
			transDTO.setTipoPersona(transForm.getTipoPersona());
		}
		
		if (!StringUtil.isEmpty(transForm.getIdToReporteSiniestro())){
			transDTO.setIdToReporteSiniestro(new BigDecimal(transForm.getIdToReporteSiniestro()));
		}
	}

}
