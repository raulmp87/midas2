package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.siniestro.cabina.PreguntasEspecialesFacadeRemote;
import mx.com.afirme.midas.siniestro.cabina.PreguntasEspecialesDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PreguntasEspecialesSN {

	private PreguntasEspecialesFacadeRemote beanRemoto;
	
	public PreguntasEspecialesSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(PreguntasEspecialesFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(preguntasEspecialesDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(preguntasEspecialesDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public PreguntasEspecialesDTO modificar(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(preguntasEspecialesDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<PreguntasEspecialesDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public PreguntasEspecialesDTO getPorId(PreguntasEspecialesDTO preguntasEspecialesDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(preguntasEspecialesDTO.getIdToReporteSiniestro());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<PreguntasEspecialesDTO> getPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad,valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	
	
}
