/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

	/**
	 * @author smvr
	 *
	 */
	public class ConfigCorreoPolizaDN {

		public static final ConfigCorreoPolizaDN INSTANCIA = new ConfigCorreoPolizaDN();

		public static ConfigCorreoPolizaDN getInstancia() {
			return ConfigCorreoPolizaDN.INSTANCIA;
		}

		public void agregar(ConfigCorreoPolizaDTO configCorreoPolizaDTO) throws ExcepcionDeAccesoADatos,
				SystemException {
			ConfigCorreoPolizaSN configCorreoPolizaSN = new ConfigCorreoPolizaSN();
			configCorreoPolizaSN.agregar(configCorreoPolizaDTO);
		}

		public void eliminar(ConfigCorreoPolizaDTO configCorreoPolizaDTO) throws ExcepcionDeAccesoADatos,
				SystemException {
			ConfigCorreoPolizaSN configCorreoPolizaSN = new ConfigCorreoPolizaSN();
			configCorreoPolizaSN.eliminar(configCorreoPolizaDTO);
		}

		public ConfigCorreoPolizaDTO actualizar(ConfigCorreoPolizaDTO configCorreoPolizaDTO) throws ExcepcionDeAccesoADatos,
				SystemException {
			ConfigCorreoPolizaSN configCorreoPolizaSN = new ConfigCorreoPolizaSN();
			return configCorreoPolizaSN.actualizar(configCorreoPolizaDTO);
		}

		public ConfigCorreoPolizaDTO getConfigCorreoPolizaPorId(BigDecimal idConfigPoliza)
				throws ExcepcionDeAccesoADatos, SystemException {
			ConfigCorreoPolizaSN configCorreoPolizaSN = new ConfigCorreoPolizaSN();
			return configCorreoPolizaSN.getConfigCorreoPolizaPorId(idConfigPoliza);
		}

		public List<ConfigCorreoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos,
				SystemException {
			ConfigCorreoPolizaSN configCorreoPolizaSN = new ConfigCorreoPolizaSN();
			return configCorreoPolizaSN.listarTodos();
		}

		public List<ConfigCorreoPolizaDTO> listarFiltrado(String numeroPoliza)
				throws ExcepcionDeAccesoADatos, SystemException {
			ConfigCorreoPolizaSN configCorreoPolizaSN = new ConfigCorreoPolizaSN();
			return configCorreoPolizaSN.listarFiltrado(numeroPoliza);
		}
	}
