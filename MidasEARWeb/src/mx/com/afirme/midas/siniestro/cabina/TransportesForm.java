/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class TransportesForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nombreRazonSocial;
	private String tipoPersona;
	private String calle;
	private String cp;
	private String idEstado;
	private String idCiudad; 
	private String idColonia;
	private String telefono;
	private String rfc_curp;
	private String noUnidad;
	
	private String idToReporteSiniestro;
	/**
	 * @return the nombreRazonSocial
	 */
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}
	/**
	 * @param nombreRazonSocial the nombreRazonSocial to set
	 */
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}
	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return the calle
	 */
	public String getCalle() {
		return calle;
	}
	/**
	 * @param calle the calle to set
	 */
	public void setCalle(String calle) {
		this.calle = calle;
	}
	/**
	 * @return the cp
	 */
	public String getCp() {
		return cp;
	}
	/**
	 * @param cp the cp to set
	 */
	public void setCp(String cp) {
		this.cp = cp;
	}
	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the rfc_curp
	 */
	public String getRfc_curp() {
		return rfc_curp;
	}
	/**
	 * @param rfc_curp the rfc_curp to set
	 */
	public void setRfc_curp(String rfc_curp) {
		this.rfc_curp = rfc_curp;
	}
	/**
	 * @return the noUnidad
	 */
	public String getNoUnidad() {
		return noUnidad;
	}
	/**
	 * @param noUnidad the noUnidad to set
	 */
	public void setNoUnidad(String noUnidad) {
		this.noUnidad = noUnidad;
	}
	/**
	 * @return the idEstado
	 */
	public String getIdEstado() {
		return idEstado;
	}
	/**
	 * @param idEstado the idEstado to set
	 */
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	/**
	 * @return the idCiudad
	 */
	public String getIdCiudad() {
		return idCiudad;
	}
	/**
	 * @param idCiudad the idCiudad to set
	 */
	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}
	/**
	 * @return the idColonia
	 */
	public String getIdColonia() {
		return idColonia;
	}
	/**
	 * @param idColonia the idColonia to set
	 */
	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	
}
