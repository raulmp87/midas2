package mx.com.afirme.midas.siniestro.cabina;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CoordinadorZonaDN {
	
	private static final CoordinadorZonaDN INSTANCIA = new CoordinadorZonaDN();
	
	public static CoordinadorZonaDN getInstancia() {
		return INSTANCIA;
	}
	
	public Integer obtenerCoordinadorPorZona(String idEstado, String idContry) throws ExcepcionDeAccesoADatos, SystemException{
		CoordinadorZonaSN coordinadorZonaSN = new CoordinadorZonaSN();
		return coordinadorZonaSN.obtenerCoordinadorPorZona(idEstado, idContry);
	}
		

}
