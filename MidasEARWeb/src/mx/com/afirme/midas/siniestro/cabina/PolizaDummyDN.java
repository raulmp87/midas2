package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PolizaDummyDN {
	private static final PolizaDummyDN INSTANCIA = new PolizaDummyDN();
	
	public static PolizaDummyDN getInstancia(){
		return INSTANCIA;
	}
	
	public PolizaDummyDTO obtenerPoliza(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
//		return polizaDummySN.obtenerPoliza(id);
		return llenaPoliza(id);
	}
	
	public List<PolizaDummyDTO> listarFiltrado(PolizaDummyDTO polizaDTO) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		List<PolizaDummyDTO> dd = new ArrayList<PolizaDummyDTO>();
		PolizaDummyDTO ddt = llenaPoliza(polizaDTO.getNumeroPoliza());
		
		ddt.setNombreAsegurado(polizaDTO.getNombreAsegurado());
		
		dd.add(ddt);
		return dd;
		
		//PolizaDummySN polizaDummySN = new PolizaDummySN();
		//return polizaDummySN.listarFiltrado(polizaDTO);
	}
	
	public List<RiesgoPolizaDummyDTO> listarRiesgosPoliza(PolizaDummyDTO poliza) throws SystemException, ExcepcionDeAccesoADatos, ParseException{
		//PolizaDummySN polizaDummySN = new PolizaDummySN();
		//return polizaDummySN.listarRiesgosPoliza(poliza);
		List<RiesgoPolizaDummyDTO> dd = new ArrayList<RiesgoPolizaDummyDTO>();
		if(poliza.getNumeroPoliza() != null){
			RiesgoPolizaDummyDTO rr = this.llenaPolizaRiesgo(poliza);
			dd.add(rr);
		}
		
		return dd;
	}
	
	
	public PolizaDummyDTO llenaPoliza(BigDecimal id) throws ParseException{
		
		PolizaDummyDTO d = new PolizaDummyDTO();
		TipoPolizaDTO tpoliza = new TipoPolizaDTO();
		tpoliza.setIdToTipoPoliza(new BigDecimal("95"));
		
		
		d.setIdMoneda(new BigDecimal(33));
		d.setNumeroPoliza(id);
		d.setClaveOficina("clave oficina");
		d.setFormaPago("forma pago");
		d.setUltimoReciboPagado("ult recibo");
		Date fecha = UtileriasWeb.getFechaFromString("01/01/2009");
		d.setFechaPago(fecha);
		d.setImportePagado(new BigDecimal("12"));
		d.setSaldoPendiente(new BigDecimal("13"));
		d.setSaldoVencido(new BigDecimal("14"));
		Date fecha2 = UtileriasWeb.getFechaFromString("02/02/2009");
		d.setFechaEmision(fecha2);
		d.setTipoNegocio(new BigDecimal("12"));
		d.setDescripcionProducto("desc producto");
		d.setDescripcionSubRamo("desc subramo");
		Date fechaVigencia = UtileriasWeb.getFechaFromString("08/11/2013 08:00");
		d.setFechaInicioVigencia(fechaVigencia);
		d.setFechaFinVigencia(fechaVigencia);
		d.setNombreAsegurado("Maria Guadalupe");
		
		d.setTipoPolizaDTO(tpoliza);
		
		return d;
	}

	public RiesgoPolizaDummyDTO llenaPolizaRiesgo(PolizaDummyDTO poliza) throws ParseException{
		RiesgoPolizaDummyDTO rdto = new RiesgoPolizaDummyDTO();
		
		rdto.setSubInciso("subinciso");
		rdto.setValorSumaAsegurada(12d);
		rdto.setRiesgoAfectado(false);
		return rdto;
	}



}
