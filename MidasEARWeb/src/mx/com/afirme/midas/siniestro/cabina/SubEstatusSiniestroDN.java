package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SubEstatusSiniestroDN {
	public static final SubEstatusSiniestroDN INSTANCIA = new SubEstatusSiniestroDN();
	
	public static SubEstatusSiniestroDN getInstancia (){
		return SubEstatusSiniestroDN.INSTANCIA;
	}
	
	public SubEstatusSiniestroDTO getPorId(SubEstatusSiniestroDTO subEstatusSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SubEstatusSiniestroSN subEstatusSiniestroSN = new SubEstatusSiniestroSN();
		return subEstatusSiniestroSN.getPorId(subEstatusSiniestroDTO);
	}
	
	public List<SubEstatusSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		SubEstatusSiniestroSN subEstatusSiniestroSN = new SubEstatusSiniestroSN();
		return subEstatusSiniestroSN.listarTodos();
	}

}
