/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDN;
import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.decoradores.Ajustador;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;

/**
 * @author smvr
 *
 */
public class ConfigCorreoAjustadorAction extends MidasMappingDispatchAction {

	
	
	public ActionForward mostrar( ActionMapping mapping, ActionForm form,
								  HttpServletRequest request, HttpServletResponse response ) {

		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarAjustadores( ActionMapping mapping,ActionForm form, HttpServletRequest request,
											  HttpServletResponse response ) {

		List<AjustadorDTO> ajustadores = new ArrayList<AjustadorDTO>();
		AjustadorDN ajustadorDN = AjustadorDN.getInstancia();
		String reglaNavegacion = Sistema.EXITOSO;
			String json = "";
			
			try {
				
				ajustadores = ajustadorDN.listarTodos();
				
				json = getJsonTablaAjustador(ajustadores);

				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			} catch (IOException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(),
						request);
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por ajustador: " + e.getMessage(), Level.SEVERE, null);
			} catch (Exception e) {
				LogDeMidasWeb.log("Ocurrió un error al buscar los datos de la configuración por ajustador: " + e.getMessage(), Level.SEVERE, null);
			}			

			return mapping.findForward(reglaNavegacion);
	}
	
	public void actualizar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		List<AjustadorDTO> ajustadores = new ArrayList<AjustadorDTO>();
		AjustadorDN ajustadorDN = AjustadorDN.getInstancia();
		AjustadorDTO ajustadorDTO = new AjustadorDTO();
		String idAjustador = request.getParameter( "idAjustador" );
		String correo = request.getParameter( "correo" );
		
		try {
			ajustadorDTO.setIdTcAjustador( new BigDecimal (idAjustador) );
			
			ajustadorDTO = ajustadorDN.getPorId( ajustadorDTO );
			
			ajustadorDTO.setEmail( correo );
			ajustadorDTO.setUsuarioNombre( UtileriasWeb.obtieneNombreUsuario(request) );
			ajustadorDTO.setFechaModificacion( new Date() ); 
			
			ajustadorDN.modificar( ajustadorDTO );
		
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al actualizar el correo del ajustador: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al actualizar el correo del ajustador: " + e.getMessage(), Level.SEVERE, null);
		}	
	}

public String getJsonTablaAjustador(List<AjustadorDTO> listaAjustadores ){
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		MidasJsonBase json = new MidasJsonBase();
		
		if( listaAjustadores != null && listaAjustadores.size() > 0 ) {
			
			for( AjustadorDTO ajustadorDTO : listaAjustadores ) {
				MidasJsonRow row;
				String ajustador = "";
				String correo = "";
				String usuario = "";
				String fecha = "";
				
				if (ajustadorDTO.getIdTcAjustador() != null ){

					row = new MidasJsonRow();
					ajustador = ajustadorDTO.getNombre(); 
					
					if( ajustadorDTO.getEmail() != null ) {
						correo  = ajustadorDTO.getEmail();
					}else {
						correo  = "";
					}
					
					if( ajustadorDTO.getUsuarioNombre() != null ) {
						usuario = ajustadorDTO.getUsuarioNombre();
					}else {
						usuario = "";
					}
					
					if( ajustadorDTO.getFechaModificacion() != null ) {
						fecha   = simpleDateFormat.format( ajustadorDTO.getFechaModificacion() );
					}else {
						fecha   = "";
					}
					
					row.setId( ajustadorDTO.getIdTcAjustador().toString() );
					row.setDatos( ajustador, correo, usuario, fecha	);
					json.addRow (row);
				}
			}
		}
		
		return json.toString();
	}

}
