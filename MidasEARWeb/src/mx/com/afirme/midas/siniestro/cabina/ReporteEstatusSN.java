package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusFacadeRemote;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReporteEstatusSN {

	private ReporteEstatusFacadeRemote beanRemoto;
	
	public ReporteEstatusSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReporteEstatusFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public void agregar(ReporteEstatusDTO reporteStatusDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.save(reporteStatusDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(ReporteEstatusDTO reporteStatusDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(reporteStatusDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public ReporteEstatusDTO modificar(ReporteEstatusDTO reporteStatusDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.update(reporteStatusDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<ReporteEstatusDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}

	public ReporteEstatusDTO getPorId(ReporteEstatusDTO reporteStatusDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(reporteStatusDTO.getIdTcReporteEstatus());
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<ReporteEstatusDTO> getPorPropiedad(String propiedad, Object valor) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByProperty(propiedad,valor);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}

	}
	

	
	
	
	
}
