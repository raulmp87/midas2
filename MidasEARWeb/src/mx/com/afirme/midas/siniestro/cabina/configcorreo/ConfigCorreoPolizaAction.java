/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.mail.MailAction;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;

/**
 * @author smvr
 *
 */
public class ConfigCorreoPolizaAction  extends MidasMappingDispatchAction {

	
	
	public ActionForward mostrar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		String reglaNavegacion = Sistema.EXITOSO;
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarPolizas( ActionMapping mapping,ActionForm form, HttpServletRequest request,
											  HttpServletResponse response ) {

			ConfigCorreoPolizaDN configCorreoPolizaDN = ConfigCorreoPolizaDN.getInstancia();
			List<ConfigCorreoPolizaDTO> listaPolizas = new ArrayList<ConfigCorreoPolizaDTO>();
			String reglaNavegacion = Sistema.EXITOSO;
			
			try {
				listaPolizas = configCorreoPolizaDN.listarTodos();
				
				String json = this.getJsonTablaPoliza(listaPolizas);
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
			
			} catch (ExcepcionDeAccesoADatos e) {
				LogDeMidasWeb.log("Ocurrió un error al buscar los correos de las pólizas: " + e.getMessage(), Level.SEVERE, null);
			} catch (Exception e) {
				LogDeMidasWeb.log("Ocurrió un error al buscar los correos de las pólizas: " + e.getMessage(), Level.SEVERE, null);
			}			

			return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarPolizasFiltrado( ActionMapping mapping,ActionForm form, HttpServletRequest request,
			  HttpServletResponse response ) {

		ConfigCorreoPolizaDN configCorreoPolizaDN = ConfigCorreoPolizaDN.getInstancia();
		List<ConfigCorreoPolizaDTO> listaPolizas = new ArrayList<ConfigCorreoPolizaDTO>();
		String numeroPoliza = request.getParameter("numeroPoliza");
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			listaPolizas = configCorreoPolizaDN.listarFiltrado(numeroPoliza);
			
			String json = this.getJsonTablaPoliza(listaPolizas);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json);
			pw.flush();
			pw.close();

		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los correos de las pólizas: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al buscar los correos de las pólizas: " + e.getMessage(), Level.SEVERE, null);
		}			
	
		return mapping.findForward(reglaNavegacion);	
	}
	
	public void buscarGuardar( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) {
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		List<PolizaSoporteDanosDTO> listaPolizasSiniestro = new ArrayList<PolizaSoporteDanosDTO>();
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
		
		ConfigCorreoPolizaDN correoPolizaDN = ConfigCorreoPolizaDN.getInstancia();
		ConfigCorreoPolizaForm configCorreoPolizaForm = (ConfigCorreoPolizaForm) form;
		ConfigCorreoPolizaDTO configCorreoPolizaDTO = new ConfigCorreoPolizaDTO();
		String resultado = "true";
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
			
			polizaSoporteDanosDTO.setNumeroPoliza( configCorreoPolizaForm.getNumeroPolizaBuscar() );
			listaPolizasSiniestro = soporteDanosDN.buscarPolizasFiltrado( polizaSoporteDanosDTO );
			
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			
			if( listaPolizasSiniestro != null && listaPolizasSiniestro.size() > 0 ) {
				
				configCorreoPolizaDTO = this.poblarConfigDTO( configCorreoPolizaForm, configCorreoPolizaDTO );
				configCorreoPolizaDTO.setUsuarioNombre( UtileriasWeb.obtieneNombreUsuario(request) );
				configCorreoPolizaDTO.setFechaModificacion( new Date() ); 
				
				correoPolizaDN.agregar( configCorreoPolizaDTO );
				
				this.limpiarForm( configCorreoPolizaForm );
			}else {
				resultado = "false";	
			}
			response.setContentLength(resultado.length());
			response.getWriter().write( resultado );
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al guardar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al guardar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public void guardar( ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response ) {
		
		ConfigCorreoPolizaDN correoPolizaDN = ConfigCorreoPolizaDN.getInstancia();
		ConfigCorreoPolizaForm configCorreoPolizaForm = (ConfigCorreoPolizaForm) form;
		ConfigCorreoPolizaDTO configCorreoPolizaDTO = new ConfigCorreoPolizaDTO();
		String reglaNavegacion = Sistema.EXITOSO;
		
		try {
				
				configCorreoPolizaDTO = this.poblarConfigDTO( configCorreoPolizaForm, configCorreoPolizaDTO );
				configCorreoPolizaDTO.setUsuarioNombre( UtileriasWeb.obtieneNombreUsuario(request) );
				configCorreoPolizaDTO.setFechaModificacion( new Date() ); 
				
				correoPolizaDN.agregar( configCorreoPolizaDTO );
				
				this.limpiarForm( configCorreoPolizaForm );
			
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al guardar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al guardar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public void actualizar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		ConfigCorreoPolizaDN correoPolizaDN = ConfigCorreoPolizaDN.getInstancia();
		ConfigCorreoPolizaDTO configCorreoPolizaDTO = new ConfigCorreoPolizaDTO();
		String idConfigPoliza = request.getParameter( "idConfigPoliza" );
		String correo = request.getParameter( "correo" );
		
		try {
			
			configCorreoPolizaDTO = correoPolizaDN.getConfigCorreoPolizaPorId( new BigDecimal (idConfigPoliza) );
			
			configCorreoPolizaDTO.setCorreo( correo );
			configCorreoPolizaDTO.setUsuarioNombre( UtileriasWeb.obtieneNombreUsuario(request) );
			configCorreoPolizaDTO.setFechaModificacion( new Date() ); 
			
			correoPolizaDN.actualizar( configCorreoPolizaDTO );
		
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al actualizar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al actualizar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public void eliminar( ActionMapping mapping, ActionForm form,
			  HttpServletRequest request, HttpServletResponse response ) {

		ConfigCorreoPolizaDN correoPolizaDN = ConfigCorreoPolizaDN.getInstancia();
		ConfigCorreoPolizaDTO configCorreoPolizaDTO = new ConfigCorreoPolizaDTO();
		String idConfigPoliza = request.getParameter( "idConfigPoliza" );
		
		try {
			
			configCorreoPolizaDTO = correoPolizaDN.getConfigCorreoPolizaPorId( new BigDecimal (idConfigPoliza) );
			correoPolizaDN.eliminar( configCorreoPolizaDTO );
			
		} catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("Ocurrió un error al eliminar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurrió un error al eliminar el correo de la póliza: " + e.getMessage(), Level.SEVERE, null);
		}	
	}
	
	public String getJsonTablaPoliza(List<ConfigCorreoPolizaDTO> listaPolizas ){
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		MidasJsonBase json = new MidasJsonBase();
		
		if( listaPolizas != null && listaPolizas.size() > 0 ) {
			
			for( ConfigCorreoPolizaDTO configCorreoPolizaDTO : listaPolizas ) {
				MidasJsonRow row;
				String numeroPoliza = "";
				String correo = "";
				String usuario = "";
				String fecha = "";
				String iconoEliminar = null;
				
				if (configCorreoPolizaDTO.getIdConfigPoliza() != null ){

					row = new MidasJsonRow();
					numeroPoliza  = UtileriasWeb.llenarIzquierda( configCorreoPolizaDTO.getCodigoProducto(), "0", 2 ) + 
								    UtileriasWeb.llenarIzquierda( configCorreoPolizaDTO.getCodigoTipoPoliza(), "0", 2 ) + "-" + 
								    UtileriasWeb.llenarIzquierda( configCorreoPolizaDTO.getNumeroPoliza().toString(), "0", 6 ) + "-" + 
								    UtileriasWeb.llenarIzquierda( configCorreoPolizaDTO.getNumeroRenovacion().toString(), "0", 2 );
					
					if( configCorreoPolizaDTO.getCorreo() != null ) {
						correo   = configCorreoPolizaDTO.getCorreo();
					}else{
						correo   = "";
					}
					
					if( configCorreoPolizaDTO.getUsuarioNombre() != null ) {
						usuario   = configCorreoPolizaDTO.getUsuarioNombre();
					}else {
						usuario   = "";
					}
					
					if( configCorreoPolizaDTO.getFechaModificacion() != null ) {
						fecha     = simpleDateFormat.format( configCorreoPolizaDTO.getFechaModificacion() );
					}else {
						fecha     = "";
					}
					iconoEliminar = "/MidasWeb/img/delete14.gif^Borrar^javascript: eliminarConfigPoliza(" + configCorreoPolizaDTO.getIdConfigPoliza()+ ");^_self";
					
					row.setId( configCorreoPolizaDTO.getId() );
					row.setDatos( numeroPoliza, correo, usuario, fecha, iconoEliminar	);
					json.addRow (row);
				}
			}
		}
		
		return json.toString();
	}

	private ConfigCorreoPolizaDTO poblarConfigDTO( ConfigCorreoPolizaForm form, ConfigCorreoPolizaDTO dto ) {
		
		if( form.getIdConfigPoliza() != null ) {
			dto.setIdConfigPoliza( new BigDecimal(form.getIdConfigPoliza()) );
		}
		
		if( form.getNumeroPolizaBuscar() != null ) {
			String[] numeroDePolizaArray = form.getNumeroPolizaBuscar().split("-");
	        String codigoProducto = numeroDePolizaArray[0].substring(0, 2);
	        String codigoTipoPoliza = numeroDePolizaArray[0].substring(2, 4);
	        Integer numPoliza = new Integer(numeroDePolizaArray[1]);
	        Integer numeroRenovacion = new Integer(numeroDePolizaArray[2]);
	        
	        dto.setCodigoProducto(codigoProducto);
	        dto.setCodigoTipoPoliza(codigoTipoPoliza);
	        dto.setNumeroPoliza(numPoliza);
	        dto.setNumeroRenovacion(numeroRenovacion);
		}
		
		if( form.getCorreo() != null ) {
			dto.setCorreo( form.getCorreo() );
		}
		
		return dto;
	}
	
	private void limpiarForm(ConfigCorreoPolizaForm form){
		
		form.setCorreo("");
	}
}