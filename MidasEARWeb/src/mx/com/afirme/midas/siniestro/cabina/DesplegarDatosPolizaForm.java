/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.danios.soporte.DetallePolizaSoporteDanosSiniestrosDTO;
import mx.com.afirme.midas.direccion.SeccionSubIncisoSoporteDanosSiniestrosDTO;
import mx.com.afirme.midas.endoso.inciso.IncisoSoporteDaniosSiniestrosDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author jmartinez
 *
 */
public class DesplegarDatosPolizaForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroPoliza;
	private String codigoTipoNegocio;
	private String claveProducto;
//	private String nombreAsegurado;
	private String direccionAsegurado;
	private String telefonoAsegurado;
	
	private String inciso;
	private String subinciso;
	private String seccion;
	private String cobertura;
	private String riesgo;
	private String sumaAsegurada;

	private String tipoMoneda;
	private String claveOficina;
	private String importePagado;
	private String saldoPendiente;
	private String formaPago;
	private String saldoVencido;
	private String ultimoReciboPagado;
	
	private String fecPago;
	private String fecEmision;
	private String fecVigencia;
	private String al;
	
	private String polizaNumeroEndoso;
    private String polizaIdAgente;
    private String polizaNombreAgente;
    private String polizaNombreOficina;
    private String polizaNombreAsegurado;
    private String polizaIdCliente;
    private String nombreContratante;
    private String direccionContratante;
    private String telefonoContratante;
    
    private List<IncisoSoporteDaniosSiniestrosDTO> listaIncisosPoliza;
    private List<SeccionSubIncisoSoporteDanosSiniestrosDTO> listaSeccionSubInciso;
    private String[] subIncisoSeccion = {};
    private String numeroInciso;
    List<DetallePolizaSoporteDanosSiniestrosDTO> listaCoberturasRiesgo;
    
    
    private String calleBuscar;
    private String codigoPostalBuscar;
    private String idColoniaBuscar;
    private String idCiudadBuscar;
    private String idEstadoBuscar;   
    
    private boolean asignarPolizaPermitido;
    
    private boolean productoTransporte;
    private String estatusCobranza;
    
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	/**
	 * @return the claveProducto
	 */
	public String getClaveProducto() {
		return claveProducto;
	}
	/**
	 * @param claveProducto the claveProducto to set
	 */
	public void setClaveProducto(String claveProducto) {
		this.claveProducto = claveProducto;
	}
//	/**
//	 * @return the nombreAsegurado
//	 */
//	public String getNombreAsegurado() {
//		return nombreAsegurado;
//	}
//	/**
//	 * @param nombreAsegurado the nombreAsegurado to set
//	 */
//	public void setNombreAsegurado(String nombreAsegurado) {
//		this.nombreAsegurado = nombreAsegurado;
//	}
	/**
	 * @return the direccionAsegurado
	 */
	public String getDireccionAsegurado() {
		return direccionAsegurado;
	}
	/**
	 * @param direccionAsegurado the direccionAsegurado to set
	 */
	public void setDireccionAsegurado(String direccionAsegurado) {
		this.direccionAsegurado = direccionAsegurado;
	}
	/**
	 * @return the inciso
	 */
	public String getInciso() {
		return inciso;
	}
	/**
	 * @param inciso the inciso to set
	 */
	public void setInciso(String inciso) {
		this.inciso = inciso;
	}
	/**
	 * @return the subinciso
	 */
	public String getSubinciso() {
		return subinciso;
	}
	/**
	 * @param subinciso the subinciso to set
	 */
	public void setSubinciso(String subinciso) {
		this.subinciso = subinciso;
	}
	/**
	 * @return the seccion
	 */
	public String getSeccion() {
		return seccion;
	}
	/**
	 * @param seccion the seccion to set
	 */
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	/**
	 * @return the cobertura
	 */
	public String getCobertura() {
		return cobertura;
	}
	/**
	 * @param cobertura the cobertura to set
	 */
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	/**
	 * @return the riesgo
	 */
	public String getRiesgo() {
		return riesgo;
	}
	/**
	 * @param riesgo the riesgo to set
	 */
	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}
	/**
	 * @return the sumaAsegurada
	 */
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	/**
	 * @param sumaAsegurada the sumaAsegurada to set
	 */
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	/**
	 * @return the claveOficina
	 */
	public String getClaveOficina() {
		return claveOficina;
	}
	/**
	 * @param claveOficina the claveOficina to set
	 */
	public void setClaveOficina(String claveOficina) {
		this.claveOficina = claveOficina;
	}
	/**
	 * @return the importePagado
	 */
	public String getImportePagado() {
		return importePagado;
	}
	/**
	 * @param importePagado the importePagado to set
	 */
	public void setImportePagado(String importePagado) {
		this.importePagado = importePagado;
	}
	/**
	 * @return the saldoPendiente
	 */
	public String getSaldoPendiente() {
		return saldoPendiente;
	}
	/**
	 * @param saldoPendiente the saldoPendiente to set
	 */
	public void setSaldoPendiente(String saldoPendiente) {
		this.saldoPendiente = saldoPendiente;
	}
	/**
	 * @return the formaPago
	 */
	public String getFormaPago() {
		return formaPago;
	}
	/**
	 * @param formaPago the formaPago to set
	 */
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	/**
	 * @return the saldoVencido
	 */
	public String getSaldoVencido() {
		return saldoVencido;
	}
	/**
	 * @param saldoVencido the saldoVencido to set
	 */
	public void setSaldoVencido(String saldoVencido) {
		this.saldoVencido = saldoVencido;
	}
	/**
	 * @return the ultimoReciboPagado
	 */
	public String getUltimoReciboPagado() {
		return ultimoReciboPagado;
	}
	/**
	 * @param ultimoReciboPagado the ultimoReciboPagado to set
	 */
	public void setUltimoReciboPagado(String ultimoReciboPagado) {
		this.ultimoReciboPagado = ultimoReciboPagado;
	}
	/**
	 * @return the fecPago
	 */
	public String getFecPago() {
		return fecPago;
	}
	/**
	 * @param fecPago the fecPago to set
	 */
	public void setFecPago(String fecPago) {
		this.fecPago = fecPago;
	}
	/**
	 * @return the fecEmision
	 */
	public String getFecEmision() {
		return fecEmision;
	}
	/**
	 * @param fecEmision the fecEmision to set
	 */
	public void setFecEmision(String fecEmision) {
		this.fecEmision = fecEmision;
	}
	/**
	 * @return the fecVigencia
	 */
	public String getFecVigencia() {
		return fecVigencia;
	}
	/**
	 * @param fecVigencia the fecVigencia to set
	 */
	public void setFecVigencia(String fecVigencia) {
		this.fecVigencia = fecVigencia;
	}
	/**
	 * @return the al
	 */
	public String getAl() {
		return al;
	}
	/**
	 * @param al the al to set
	 */
	public void setAl(String al) {
		this.al = al;
	}
	/**
	 * @return the codigoTipoNegocio
	 */
	public String getCodigoTipoNegocio() {
		return codigoTipoNegocio;
	}
	/**
	 * @param codigoTipoNegocio the codigoTipoNegocio to set
	 */
	public void setCodigoTipoNegocio(String codigoTipoNegocio) {
		this.codigoTipoNegocio = codigoTipoNegocio;
	}
	/**
	 * @return the polizaNumeroEndoso
	 */
	public String getPolizaNumeroEndoso() {
		return polizaNumeroEndoso;
	}
	/**
	 * @param polizaNumeroEndoso the polizaNumeroEndoso to set
	 */
	public void setPolizaNumeroEndoso(String polizaNumeroEndoso) {
		this.polizaNumeroEndoso = polizaNumeroEndoso;
	}
	/**
	 * @return the polizaIdAgente
	 */
	public String getPolizaIdAgente() {
		return polizaIdAgente;
	}
	/**
	 * @param polizaIdAgente the polizaIdAgente to set
	 */
	public void setPolizaIdAgente(String polizaIdAgente) {
		this.polizaIdAgente = polizaIdAgente;
	}
	/**
	 * @return the polizaNombreAgente
	 */
	public String getPolizaNombreAgente() {
		return polizaNombreAgente;
	}
	/**
	 * @param polizaNombreAgente the polizaNombreAgente to set
	 */
	public void setPolizaNombreAgente(String polizaNombreAgente) {
		this.polizaNombreAgente = polizaNombreAgente;
	}
	/**
	 * @return the polizaNombreOficina
	 */
	public String getPolizaNombreOficina() {
		return polizaNombreOficina;
	}
	/**
	 * @param polizaNombreOficina the polizaNombreOficina to set
	 */
	public void setPolizaNombreOficina(String polizaNombreOficina) {
		this.polizaNombreOficina = polizaNombreOficina;
	}
	/**
	 * @return the polizaNombreAsegurado
	 */
	public String getPolizaNombreAsegurado() {
		return polizaNombreAsegurado;
	}
	/**
	 * @param polizaNombreAsegurado the polizaNombreAsegurado to set
	 */
	public void setPolizaNombreAsegurado(String polizaNombreAsegurado) {
		this.polizaNombreAsegurado = polizaNombreAsegurado;
	}
	/**
	 * @return the polizaIdCliente
	 */
	public String getPolizaIdCliente() {
		return polizaIdCliente;
	}
	/**
	 * @param polizaIdCliente the polizaIdCliente to set
	 */
	public void setPolizaIdCliente(String polizaIdCliente) {
		this.polizaIdCliente = polizaIdCliente;
	}
	/**
	 * @return the telefonoAsegurado
	 */
	public String getTelefonoAsegurado() {
		return telefonoAsegurado;
	}
	/**
	 * @param telefonoAsegurado the telefonoAsegurado to set
	 */
	public void setTelefonoAsegurado(String telefonoAsegurado) {
		this.telefonoAsegurado = telefonoAsegurado;
	}
	/**
	 * @return the nombreContratante
	 */
	public String getNombreContratante() {
		return nombreContratante;
	}
	/**
	 * @param nombreContratante the nombreContratante to set
	 */
	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}
	/**
	 * @return the direccionContratante
	 */
	public String getDireccionContratante() {
		return direccionContratante;
	}
	/**
	 * @param direccionContratante the direccionContratante to set
	 */
	public void setDireccionContratante(String direccionContratante) {
		this.direccionContratante = direccionContratante;
	}
	/**
	 * @return the telefonoContratante
	 */
	public String getTelefonoContratante() {
		return telefonoContratante;
	}
	/**
	 * @param telefonoContratante the telefonoContratante to set
	 */
	public void setTelefonoContratante(String telefonoContratante) {
		this.telefonoContratante = telefonoContratante;
	}
	/**
	 * @return the listaIncisosPoliza
	 */
	public List<IncisoSoporteDaniosSiniestrosDTO> getListaIncisosPoliza() {
		return listaIncisosPoliza;
	}
	/**
	 * @param listaIncisosPoliza the listaIncisosPoliza to set
	 */
	public void setListaIncisosPoliza(
			List<IncisoSoporteDaniosSiniestrosDTO> listaIncisosPoliza) {
		this.listaIncisosPoliza = listaIncisosPoliza;
	}
	/**
	 * @return the listaSeccionSubInciso
	 */
	public List<SeccionSubIncisoSoporteDanosSiniestrosDTO> getListaSeccionSubInciso() {
		return listaSeccionSubInciso;
	}
	/**
	 * @param listaSeccionSubInciso the listaSeccionSubInciso to set
	 */
	public void setListaSeccionSubInciso(
			List<SeccionSubIncisoSoporteDanosSiniestrosDTO> listaSeccionSubInciso) {
		this.listaSeccionSubInciso = listaSeccionSubInciso;
	}
	/**
	 * @return the subIncisoSeccion
	 */
	public String[] getSubIncisoSeccion() {
		return subIncisoSeccion;
	}
	/**
	 * @param subIncisoSeccion the subIncisoSeccion to set
	 */
	public void setSubIncisoSeccion(String[] subIncisoSeccion) {
		this.subIncisoSeccion = subIncisoSeccion;
	}
	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}
	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	/**
	 * @return the listaCoberturasRiesgo
	 */
	public List<DetallePolizaSoporteDanosSiniestrosDTO> getListaCoberturasRiesgo() {
		return listaCoberturasRiesgo;
	}
	/**
	 * @param listaCoberturasRiesgo the listaCoberturasRiesgo to set
	 */
	public void setListaCoberturasRiesgo(
			List<DetallePolizaSoporteDanosSiniestrosDTO> listaCoberturasRiesgo) {
		this.listaCoberturasRiesgo = listaCoberturasRiesgo;
	}
	/**
	 * @return the calleBuscar
	 */
	public String getCalleBuscar() {
		return calleBuscar;
	}
	/**
	 * @param calleBuscar the calleBuscar to set
	 */
	public void setCalleBuscar(String calleBuscar) {
		this.calleBuscar = calleBuscar;
	}
	/**
	 * @return the codigoPostalBuscar
	 */
	public String getCodigoPostalBuscar() {
		return codigoPostalBuscar;
	}
	/**
	 * @param codigoPostalBuscar the codigoPostalBuscar to set
	 */
	public void setCodigoPostalBuscar(String codigoPostalBuscar) {
		this.codigoPostalBuscar = codigoPostalBuscar;
	}
	/**
	 * @return the idColoniaBuscar
	 */
	public String getIdColoniaBuscar() {
		return idColoniaBuscar;
	}
	/**
	 * @param idColoniaBuscar the idColoniaBuscar to set
	 */
	public void setIdColoniaBuscar(String idColoniaBuscar) {
		this.idColoniaBuscar = idColoniaBuscar;
	}
	/**
	 * @return the idCiudadBuscar
	 */
	public String getIdCiudadBuscar() {
		return idCiudadBuscar;
	}
	/**
	 * @param idCiudadBuscar the idCiudadBuscar to set
	 */
	public void setIdCiudadBuscar(String idCiudadBuscar) {
		this.idCiudadBuscar = idCiudadBuscar;
	}
	/**
	 * @return the idEstadoBuscar
	 */
	public String getIdEstadoBuscar() {
		return idEstadoBuscar;
	}
	/**
	 * @param idEstadoBuscar the idEstadoBuscar to set
	 */
	public void setIdEstadoBuscar(String idEstadoBuscar) {
		this.idEstadoBuscar = idEstadoBuscar;
	}
	/**
	 * @return the asignarPolizaPermitido
	 */
	public boolean isAsignarPolizaPermitido() {
		return asignarPolizaPermitido;
	}
	/**
	 * @param asignarPolizaPermitido the asignarPolizaPermitido to set
	 */
	public void setAsignarPolizaPermitido(boolean asignarPolizaPermitido) {
		this.asignarPolizaPermitido = asignarPolizaPermitido;
	}
	/**
	 * @return the productoTransporte
	 */
	public boolean isProductoTransporte() {
		return productoTransporte;
	}
	/**
	 * @param productoTransporte the productoTransporte to set
	 */
	public void setProductoTransporte(boolean productoTransporte) {
		this.productoTransporte = productoTransporte;
	}
	/**
	 * @return the estatusCobranza
	 */
	public String getEstatusCobranza() {
		return estatusCobranza;
	}
	/**
	 * @param estatusCobranza the estatusCobranza to set
	 */
	public void setEstatusCobranza(String estatusCobranza) {
		this.estatusCobranza = estatusCobranza;
	}
	
	
	
}
