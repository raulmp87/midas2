/**
 * 
 */
package mx.com.afirme.midas.siniestro.cabina.configcorreo;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.catalogos.tiponegocio.TipoNegocioDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;

/**
 * @author smvr
 *
 */
public class ConfigCorreoPolizaSN {

	private ConfigCorreoPolizaFacadeRemote beanRemoto;
	
	

	public ConfigCorreoPolizaSN() throws SystemException {
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB( ConfigCorreoPolizaFacadeRemote.class );
		LogDeMidasWeb.log( "bean Remoto ConfigCorreoPolizaFacadeRemote instanciado", Level.INFO, null);
	}
	
	public void agregar( ConfigCorreoPolizaDTO configCorreoPolizaDTO ) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save( configCorreoPolizaDTO );
		}catch( EJBTransactionRolledbackException e ){
			LogDeMidasWeb.log( "EXCEPCION GENERADA ::::  " + e.getMessage(), Level.SEVERE, null );
		}
	}
	
	public void eliminar(ConfigCorreoPolizaDTO configCorreoPolizaDTO) throws ExcepcionDeAccesoADatos{
		beanRemoto.delete(configCorreoPolizaDTO);
	}
	
	public ConfigCorreoPolizaDTO actualizar( ConfigCorreoPolizaDTO configCorreoPolizaDTO ) throws ExcepcionDeAccesoADatos{
		return beanRemoto.update( configCorreoPolizaDTO );
	}
	
	public List<ConfigCorreoPolizaDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		return beanRemoto.findAll();
	}
	
	public ConfigCorreoPolizaDTO getConfigCorreoPolizaPorId( BigDecimal idConfigPoliza ) throws ExcepcionDeAccesoADatos{
		return beanRemoto.findById( idConfigPoliza );
	}
	
	public List<ConfigCorreoPolizaDTO> listarFiltrado(String numeroPoliza) throws ExcepcionDeAccesoADatos{
		return beanRemoto.listarFiltrado( numeroPoliza );
	}
}
