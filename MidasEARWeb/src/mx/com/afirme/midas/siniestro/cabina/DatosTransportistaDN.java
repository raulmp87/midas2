package mx.com.afirme.midas.siniestro.cabina;

import java.util.List;


import mx.com.afirme.midas.siniestro.cabina.DatosTransportistaDTO;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DatosTransportistaDN {

	private static final DatosTransportistaDN INSTANCIA = new DatosTransportistaDN();

	public static DatosTransportistaDN getInstancia() {
		return INSTANCIA;
	}
	
	public void agregarTransportista(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DatosTransportistaSN transportistaSN = new DatosTransportistaSN();
		transportistaSN.agregar(datoTransportistaDTO);
	}

	public void borrarTransportista(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DatosTransportistaSN transportistaSN = new DatosTransportistaSN();
		transportistaSN.borrar(datoTransportistaDTO);
	}
	
	public void modificarTransportista(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DatosTransportistaSN transportistaSN = new DatosTransportistaSN();
		transportistaSN.modificar(datoTransportistaDTO);
	}
	
	public List<DatosTransportistaDTO> mostrarTodosTransportistas()  throws ExcepcionDeAccesoADatos, SystemException {
		DatosTransportistaSN transportistaSN = new DatosTransportistaSN();
		return transportistaSN.listarTodos();
	}
	
	public DatosTransportistaDTO obtieneTransportistaPorId(DatosTransportistaDTO datoTransportistaDTO) throws ExcepcionDeAccesoADatos, SystemException {
		DatosTransportistaSN transportistaSN = new DatosTransportistaSN();
		return transportistaSN.getPorId(datoTransportistaDTO);
	}

	public List<DatosTransportistaDTO> obtieneTransportistaPorPropiedad(String propiedad,Object valor) throws ExcepcionDeAccesoADatos, SystemException {
		DatosTransportistaSN transportistaSN = new DatosTransportistaSN();
		return transportistaSN.getPorPropiedad(propiedad,valor);
	}

}
