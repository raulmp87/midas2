package mx.com.afirme.midas.siniestro.cabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDN;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;


public class ReporteSiniestroDN {
	private static final ReporteSiniestroDN INSTANCIA = new ReporteSiniestroDN();
	private static String nombreUsuario;
	
	public static ReporteSiniestroDN getInstancia(String nombreUsuario) {
		ReporteSiniestroDN.nombreUsuario = nombreUsuario;
		return ReporteSiniestroDN.INSTANCIA;
	}
	
	public ReporteSiniestroDTO crearReporte(ReporteSiniestroDTO reporteSiniestroDTO)throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		return reporteSiniestroSN.crearReporte(reporteSiniestroDTO);
	}
	
	public ReporteSiniestroDTO guardarReporte(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
//		BigDecimal tipoNegocio = reporteSiniestroDTO.getPoliza().getTipoNegocio();
//		asignacionPendientesCrearReporte(reporteSiniestroDTO);
		return reporteSiniestroSN.guardarReporte(reporteSiniestroDTO);
	}		
	
	public ReporteSiniestroDTO desplegarReporte(BigDecimal id) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		return reporteSiniestroSN.desplegarReporte(id);		
	}
	
	public List<ReporteSiniestroDTO> listarTodos() throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		return reporteSiniestroSN.listarTodos();
	}
	
	public List<ReporteSiniestroDTO> listarFiltrado(ReporteSiniestroDTO rpo) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);		
		return reporteSiniestroSN.listarFiltrado(rpo);
	}
	
	public List<ReporteSiniestroDTO> listarReportesAbiertos() throws SystemException, ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);		
		return reporteSiniestroSN.listarReportesAbiertos();
	}
	
	public List<ReporteSiniestroDTO> listarFiltradoPorNumPoliza(String idPolizas)
			throws SystemException, ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(
				ReporteSiniestroDN.nombreUsuario);
		return reporteSiniestroSN.listarFiltradoPorNumPoliza(idPolizas);
	}

	public boolean esAsignacionAjustadorPermitido(BigDecimal varTipoNegocio, Usuario varUsuario){
		boolean resultado = false;
//		if((tipoNegocio == null || UtileriasWeb.esTipoNegocio(tipoNegocio,99)) && 
//			UtileriasWeb.contieneRol(usuario.getRoles(), Sistema.ROL_CABINERO,Sistema.ROL_COORDINADOR_SINIESTROS,Sistema.ROL_GERENTE_SINIESTROS)){
//			resultado = true;
//		}else if(UtileriasWeb.esTipoNegocio(tipoNegocio, 98,97,96,95) &&
//				  UtileriasWeb.contieneRol(usuario.getRoles(), Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO,Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
//			resultado = true;
//		}
//		return resultado;
		
		if((UtileriasWeb.esTipoNegocio(varTipoNegocio, 99) || varTipoNegocio == null) && UtileriasWeb.contieneRol(varUsuario.getRoles(),Sistema.ROL_CABINERO,Sistema.ROL_COORDINADOR_SINIESTROS)){
			resultado = true;
		}
		if(UtileriasWeb.esTipoNegocio(varTipoNegocio, 95,96,97,98) && UtileriasWeb.contieneRol(varUsuario.getRoles(), Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO)){
			resultado = true;
		}
		return resultado;
	}
	
	public ReporteSiniestroDTO actualizarInformacion(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		
		ReporteSiniestroDTO informacionReporte = reporteSiniestroSN.desplegarReporte(reporteSiniestroDTO.getIdToReporteSiniestro());
		informacionReporte.setFechaAsignacionAjustador(reporteSiniestroDTO.getFechaAsignacionAjustador());
		informacionReporte.setAjustador(reporteSiniestroDTO.getAjustador());
		informacionReporte.setReporteEstatus(reporteSiniestroDTO.getReporteEstatus());
		informacionReporte.setFechaEnvioCorreo( reporteSiniestroDTO.getFechaEnvioCorreo() );
		
//		BigDecimal tipoNegocio = reporteSiniestroDTO.getPoliza().getTipoNegocio();
//		asignacionPendientesCrearReporte(reporteSiniestroDTO);
		return reporteSiniestroSN.guardarReporte(informacionReporte);
	}	
	
	public ReporteSiniestroDTO rechazarAjustador(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		
		ReporteSiniestroDTO informacionReporte = reporteSiniestroSN.desplegarReporte(reporteSiniestroDTO.getIdToReporteSiniestro());
		informacionReporte.setFechaAsignacionAjustador(reporteSiniestroDTO.getFechaAsignacionAjustador());
		AjustadorDTO ajustadorTmp = informacionReporte.getAjustador();
		ajustadorTmp.setIdTcAjustador(null);
		informacionReporte.setAjustador(ajustadorTmp);
		informacionReporte.setAjustador(reporteSiniestroDTO.getAjustador());
		informacionReporte.setReporteEstatus(reporteSiniestroDTO.getReporteEstatus());
		
		return reporteSiniestroSN.guardarReporte(informacionReporte);
	}	
	
	public ReporteSiniestroDTO actualizar(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		
		return reporteSiniestroSN.guardarReporte(reporteSiniestroDTO);
	}
	
	public ReporteSiniestroDTO getPorId(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException,
	ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);
		return reporteSiniestroSN.desplegarReporte(reporteSiniestroDTO.getIdToReporteSiniestro());		
	}
	
	public List<ReporteSiniestroDTO> listarFiltrado(ReporteSiniestroDTO reporteSiniestroDTO, String nombreAsegurado) throws SystemException,
	ExcepcionDeAccesoADatos {
		List<ReporteSiniestroDTO> reportesSiniestro = new ArrayList<ReporteSiniestroDTO>();
		List<PolizaSoporteDanosDTO> polizasDanosList = new ArrayList<PolizaSoporteDanosDTO>();
		boolean filtroPorDanios = false;
		
		Map<String, ReporteSiniestroDTO> mapReporteSiniestroNombreAsegurado = new HashMap<String, ReporteSiniestroDTO>();
		Map<String, ReporteSiniestroDTO> mapReporteSiniestroPoliza = new HashMap<String, ReporteSiniestroDTO>();
		
		ReporteSiniestroDTO reporte = new ReporteSiniestroDTO();
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);		
		List<ReporteSiniestroDTO> reporteSiniestroDTOList = reporteSiniestroSN.listarFiltrado(reporteSiniestroDTO);	
		List<ReporteSiniestroDTO> reporteSiniestroPolizaList = new ArrayList<ReporteSiniestroDTO>();

		if (!UtileriasWeb.esCadenaVacia(nombreAsegurado) || !UtileriasWeb.esCadenaVacia(reporteSiniestroDTO.getNumeroPolizaCliente())){
			filtroPorDanios = true;
			SoporteDanosDN soporteDanosDN = new SoporteDanosDN();
			PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
			if (!UtileriasWeb.esCadenaVacia(nombreAsegurado))
				polizaSoporteDanosDTO.setNombreAsegurado(nombreAsegurado);
			polizasDanosList = soporteDanosDN.buscarPolizasFiltrado(polizaSoporteDanosDTO);
			String idPolizas = new String();
			if (polizasDanosList!=null && polizasDanosList.size()>0){
				for (PolizaSoporteDanosDTO polizaSoporte:polizasDanosList){
					idPolizas += polizaSoporte.getIdToPoliza()+",";
				}
				if (idPolizas.length()>0){
					idPolizas = idPolizas.substring(0, idPolizas.length()-1);
				}
				reporteSiniestroPolizaList = reporteSiniestroSN.listarFiltradoPorNumPoliza(idPolizas);
				for (ReporteSiniestroDTO registro: reporteSiniestroPolizaList){
					mapReporteSiniestroNombreAsegurado.put(registro.getIdToReporteSiniestro().toString(), reporte);
					if (UtileriasWeb.esCadenaVacia(reporteSiniestroDTO.getNumeroPolizaCliente())){
						mapReporteSiniestroPoliza.put(registro.getIdToReporteSiniestro().toString(), reporte);
					}
				}
			}
			
			if (!UtileriasWeb.esCadenaVacia(reporteSiniestroDTO.getNumeroPolizaCliente())){
				polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
				polizaSoporteDanosDTO.setNumeroPoliza(reporteSiniestroDTO.getNumeroPolizaCliente());				
				polizasDanosList = soporteDanosDN.buscarPolizasFiltrado(polizaSoporteDanosDTO);
				idPolizas = new String();
				if (polizasDanosList!=null && polizasDanosList.size()>0){
					for (PolizaSoporteDanosDTO polizaSoporte:polizasDanosList){
						idPolizas += polizaSoporte.getIdToPoliza()+",";
					}
					if (idPolizas.length()>0){
						idPolizas = idPolizas.substring(0, idPolizas.length()-1);
					}
					reporteSiniestroPolizaList = reporteSiniestroSN.listarFiltradoPorNumPoliza(idPolizas);
					for (ReporteSiniestroDTO registro: reporteSiniestroPolizaList){
						if (mapReporteSiniestroNombreAsegurado.containsKey(registro.getIdToReporteSiniestro().toString())){
							mapReporteSiniestroPoliza.put(registro.getIdToReporteSiniestro().toString(), reporte);
						}else{
							if (UtileriasWeb.esCadenaVacia(nombreAsegurado)){
								mapReporteSiniestroPoliza.put(registro.getIdToReporteSiniestro().toString(), reporte);
							}
						}
					}
				}
			}
		}
		
		if (!filtroPorDanios){
			for (ReporteSiniestroDTO registro: reporteSiniestroDTOList){
				reportesSiniestro.add(registro);
			}
		}else{
			if (reporteSiniestroDTOList!=null && reporteSiniestroDTOList.size()>0){
				for (ReporteSiniestroDTO registro: reporteSiniestroDTOList){
					if (mapReporteSiniestroPoliza.containsKey(registro.getIdToReporteSiniestro().toString())){
						reportesSiniestro.add(registro);
					}
				}
			}
		}
		return reportesSiniestro;
	}
	
	public String tipoMonedaPoliza(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		String result = "";
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		ReporteSiniestroDTO reporteSiniestroDTO = this.desplegarReporte(idToReporteSiniestro);				
		BigDecimal idToPoliza = reporteSiniestroDTO.getNumeroPoliza();
		
		if(idToPoliza != null){
//			PolizaSoporteDanosDTO polizaSoporteDanosDTO= soporteDanosDN.getDatosGeneralesPoliza(idToPoliza);
			PolizaSoporteDanosDTO polizaSoporteDanosDTO= soporteDanosDN.getDatosGeneralesPoliza(idToPoliza,reporteSiniestroDTO.getFechaSiniestro());
			result = polizaSoporteDanosDTO.getDescripcionMoneda();
		}
				
		return result;
	}
	
	//Devuelve una lista con los mensajes de error que se produjeron
	public List<String> validaPendientesReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO)throws ExcepcionDeAccesoADatos, SystemException{
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		
		List<String> mensajesError = new ArrayList<String>();
		
		List<IngresoSiniestroDTO> listaIngresosSiniestro = ingresoSiniestroDN.getIngresosPendientes(reporteSiniestroDTO);
		if (listaIngresosSiniestro!=null && listaIngresosSiniestro.size()>0){
			mensajesError.add("No deben existir ingresos pendientes");
		}
		
		List<GastoSiniestroDTO> listaGastosSiniestro = gastoSiniestroDN.listarGastosPendientes(reporteSiniestroDTO); 
		if (listaGastosSiniestro!=null && listaGastosSiniestro.size()>0){
			mensajesError.add("No deben existir gastos pendientes");
		}
		
		List<SalvamentoSiniestroDTO> listaSalvamentosSiniestro = salvamentoSiniestroDN.listarSalvamentosPendientes(reporteSiniestroDTO);
		if (listaSalvamentosSiniestro!=null && listaSalvamentosSiniestro.size()>0){
			mensajesError.add("No deben existir salvamentos pendientes");
		}
		List<IndemnizacionDTO> listaIndemnizaciones = indemnizacionDN.obtenIndemnizacionPorEstatusNOCancelado(reporteSiniestroDTO);
		if(listaIndemnizaciones.size() > 0){
			String mensajeErrorIndemnizacion = indemnizacionDN.validaIndemnizacionesTerminarReporte(reporteSiniestroDTO);
			if (!UtileriasWeb.esCadenaVacia(mensajeErrorIndemnizacion)){
				mensajesError.add(mensajeErrorIndemnizacion);
			}
		}else{
			ReservaDTO ultimaReservaCreada = reservaDN.obtenerUltimaReservaCreada(reporteSiniestroDTO.getIdToReporteSiniestro());
			if(ultimaReservaCreada != null){
				if(ultimaReservaCreada.getAutorizada()){
					Double diferencia = reservaDetalleDN.diferenciaDeReserva(ultimaReservaCreada.getReporteSiniestroDTO().getIdToReporteSiniestro());
					if(!(diferencia.doubleValue() == 0.0)){
						mensajesError.add("Los ajustes de mas y de menos no son igual a 0");
					}
				}else{
					mensajesError.add("Existe una reserva pendiente de autorizar");
				}
			}
		}
		/*
		if (reporteSiniestroDTO.getInformePreliminarValido()!=null || reporteSiniestroDTO.getInformeFinalValido()!=null){
			if (reporteSiniestroDTO.getInformePreliminarValido()!=null){
				if (reporteSiniestroDTO.getInformePreliminarValido()){
					if (reporteSiniestroDTO.getInformeFinalValido()!=null && reporteSiniestroDTO.getInformeFinalValido()){
						String mensajeErrorIndemnizacion = indemnizacionDN.validaIndemnizacionesTerminarReporte(reporteSiniestroDTO);
						if (!UtileriasWeb.esCadenaVacia(mensajeErrorIndemnizacion)){
							mensajesError.add(mensajeErrorIndemnizacion);
						}
					}else{
						if (!(reporteSiniestroDTO.getTerminoAjuste()!=null && reporteSiniestroDTO.getFechaTerminadoSiniestro()!=null)){
							mensajesError.add("Debe haber termino de ajuste y fecha de termino");
						}
					}
				}else{
					if (!(reporteSiniestroDTO.getTerminoAjuste()!=null && reporteSiniestroDTO.getFechaTerminadoSiniestro()!=null)){
						mensajesError.add("Debe haber termino de ajuste y fecha de termino");
					}
				}	
			} else {
				mensajesError.add("Debe existir el informe preliminar");
			}
		}
		*/
		return mensajesError;
	}
	
	public List<ReporteSiniestroDTO> listarReportesPorPropiedad(String propertyName,Object value) throws SystemException, ExcepcionDeAccesoADatos {
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);		
		return reporteSiniestroSN.listarReportesPorPropiedad(propertyName, value);
	}
	
	public List<ReporteSiniestroDTO> reportesSinInformePreliminar() throws SystemException{
		ReporteSiniestroSN reporteSiniestroSN = new ReporteSiniestroSN(ReporteSiniestroDN.nombreUsuario);		
		return reporteSiniestroSN.reportesSinInformePreliminar();
	}

	public ReporteSiniestroDTO obtenerReporteSiniestroPorNumeroReporte(String numeroReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		ReporteSiniestroDTO reporteSiniestroDTO = null;
		if (!UtileriasWeb.esCadenaVacia(numeroReporteSiniestro)){
			reporteSiniestroDTO = new ReporteSiniestroDTO();
			reporteSiniestroDTO.setNumeroReporte(numeroReporteSiniestro);
			
			List<ReporteSiniestroDTO> listaReportesSiniestro = listarFiltrado(reporteSiniestroDTO);
			if(listaReportesSiniestro != null && listaReportesSiniestro.size() == 1){
				reporteSiniestroDTO = listaReportesSiniestro.get(0);
			}
			else{
				reporteSiniestroDTO = null;
			}
		}
		return reporteSiniestroDTO;
	}
}
