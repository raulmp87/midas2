package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.interfaz.ingreso.referencia.ReferenciaIngresoDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IngresoSiniestroDN {
	private static final IngresoSiniestroDN INSTANCIA = new IngresoSiniestroDN();
	
	public static IngresoSiniestroDN getInstancia(){
		return INSTANCIA;
	}
	
	
	public IngresoSiniestroDTO agregarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO)throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();
		return ingresoSiniestroSN.agregarIngreso(ingresoSiniestroDTO);
	}
	
	public void actualizarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO)throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();
		ingresoSiniestroSN.actualizarIngreso(ingresoSiniestroDTO);
	}
	
	public void eliminarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO)throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();
		ingresoSiniestroSN.eliminarIngreso(ingresoSiniestroDTO);
	}
	
	public IngresoSiniestroDTO findById(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();
		return ingresoSiniestroSN.findById(id);
	}
	
	public List<IngresoSiniestroDTO> listarIngresosSiniestro(BigDecimal idReporteSiniestro) throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();	
		return ingresoSiniestroSN.listarIngresosSiniestro(idReporteSiniestro);		
	}
	
	public List<IngresoSiniestroDTO> listarIngresosPendientes() throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();	
		return ingresoSiniestroSN.listarIngresosPendientes();		
	}
	
	public List<IngresoSiniestroDTO> getIngresosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params) throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();	
		return ingresoSiniestroSN.getIngresosPorReporteYEstatus(idReporteSiniestro,params);		
	}
	
	public List<IngresoSiniestroDTO> getIngresosNoCanceladosPorReporte(BigDecimal idReporteSiniestro) throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();	
		return ingresoSiniestroSN.getIngresosPorReporteYEstatus(idReporteSiniestro,EstatusFinanzasDTO.ABIERTO, EstatusFinanzasDTO.AUTORIZADO, EstatusFinanzasDTO.ORDEN_PAGO, EstatusFinanzasDTO.PAGADO, EstatusFinanzasDTO.POR_AUTORIZAR);		
	}
	
	public List<IngresoSiniestroDTO> getIngresosPendientes(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException, ExcepcionDeAccesoADatos {
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();	
		return ingresoSiniestroSN.getIngresosPorReporteYEstatus(reporteSiniestroDTO.getIdToReporteSiniestro(),EstatusFinanzasDTO.ABIERTO, EstatusFinanzasDTO.POR_AUTORIZAR);		
	}
	
	public List<IngresoSiniestroDTO> listarIngresosPorAplicar(IngresoSiniestroFiltroDTO criteriosDeFiltrado) throws SystemException{
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();	
		return ingresoSiniestroSN.listarIngresosPorAplicar(criteriosDeFiltrado);				
	}
	
	public List<IngresoSiniestroDTO> getIngresosNoCancelados(BigDecimal idReporteSiniestro) throws SystemException, ExcepcionDeAccesoADatos{
		IngresoSiniestroSN ingresoSiniestroSN = new IngresoSiniestroSN();
		return ingresoSiniestroSN.getIngresosNoCancelados(idReporteSiniestro);
	}

	public List<ReferenciaIngresoDTO> obtenerListaReferenciasIngreso(String nombreUsuario) throws SystemException{
		return new ReferenciaIngresoSN().obtenerListaReferenciasIngreso(nombreUsuario);
	}
}
