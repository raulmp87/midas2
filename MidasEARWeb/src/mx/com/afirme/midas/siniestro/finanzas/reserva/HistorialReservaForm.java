package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.util.List;

import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class HistorialReservaForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String idToReporteSiniestro;	
	private Double sumaAsegurada;
	private String tipoMoneda;
	private List<RegistroHistorialReserva> modificacionesReserva;
	private List<ReservaDetalleDTO> detalleReserva;
	private String reservaSeleccionada;
	private List<ReservaDetalleBean> listaReservaDetalleBean;
	
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public List<ReservaDetalleBean> getListaReservaDetalleBean() {
		return listaReservaDetalleBean;
	}
	public void setListaReservaDetalleBean(List<ReservaDetalleBean> listaReservaDetalleBean) {
		this.listaReservaDetalleBean = listaReservaDetalleBean;
	}
	public String getReservaSeleccionada() {
		return reservaSeleccionada;
	}
	public void setReservaSeleccionada(String reservaSeleccionada) {
		this.reservaSeleccionada = reservaSeleccionada;
	}
	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public List<RegistroHistorialReserva> getModificacionesReserva() {
		return modificacionesReserva;
	}
	public void setModificacionesReserva(List<RegistroHistorialReserva> modificacionesReserva) {
		this.modificacionesReserva = modificacionesReserva;
	}
	public List<ReservaDetalleDTO> getDetalleReserva() {
		return detalleReserva;
	}
	public void setDetalleReserva(List<ReservaDetalleDTO> detalleReserva) {
		this.detalleReserva = detalleReserva;
	}
		
}