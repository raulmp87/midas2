package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class AprobarInformeFinalForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private BigDecimal idDeReporte;
	private String tipoMoneda = "";
	private Boolean valido = false;
	private List<Registro> listadoCoberturas;
	private List<RegistroDeReserva> listadoDeReservas;
	private String total = "";

	public BigDecimal getIdDeReporte() {
		return idDeReporte;
	}

	public void setIdDeReporte(BigDecimal idDeReporte) {
		this.idDeReporte = idDeReporte;
	}

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}

	/**
	 * @param tipoMoneda
	 *            the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	/**
	 * @return the informePreliminarValido
	 */
	public Boolean getValido() {
		return valido;
	}

	/**
	 * @return the informePreliminarValido
	 */
	public Boolean isValido() {
		return valido;
	}

	/**
	 * @param informePreliminarValido
	 *            the informePreliminarValido to set
	 */
	public void setValido(Boolean valido) {
		this.valido = valido;
	}

	/**
	 * @return the listadoCoberturas
	 */
	public List<Registro> getListadoCoberturas() {
		return listadoCoberturas;
	}

	/**
	 * @param listadoCoberturas
	 *            the listadoCoberturas to set
	 */
	public void setListadoCoberturas(List<Registro> listadoCoberturas) {
		this.listadoCoberturas = listadoCoberturas;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	public void setListadoDeReservas(List<RegistroDeReserva> listadoDeReservas) {
		this.listadoDeReservas = listadoDeReservas;
	}

	public List<RegistroDeReserva> getListadoDeReservas() {
		return listadoDeReservas;
	}

}
