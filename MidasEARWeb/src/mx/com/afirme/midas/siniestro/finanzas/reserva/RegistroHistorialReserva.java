package mx.com.afirme.midas.siniestro.finanzas.reserva;

import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;

public class RegistroHistorialReserva{

	private static final long serialVersionUID = 1L;
	
	private String descripcionTipoMovimiento;
	private ReservaDTO reservaDTO;
	private Double montoReserva;
	
	public String getDescripcionTipoMovimiento() {
		return descripcionTipoMovimiento;
	}
	public void setDescripcionTipoMovimiento(String descripcionTipoMovimiento) {
		this.descripcionTipoMovimiento = descripcionTipoMovimiento;
	}
	public ReservaDTO getReservaDTO() {
		return reservaDTO;
	}
	public void setReservaDTO(ReservaDTO reservaDTO) {
		this.reservaDTO = reservaDTO;
	}
	public Double getMontoReserva() {
		return montoReserva;
	}
	public void setMontoReserva(Double montoReserva) {
		this.montoReserva = montoReserva;
	}
		
}