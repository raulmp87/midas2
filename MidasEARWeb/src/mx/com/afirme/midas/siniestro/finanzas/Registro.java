package mx.com.afirme.midas.siniestro.finanzas;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.sistema.SystemException;

public class Registro {
	private String inciso;
	private String subInciso;
	private String seccion;
	private String cobertura;
	private String riesgo;
	private Double sumaAsegurada;
	private String tipoSA;

	public Registro(RiesgoAfectadoDTO riesgoAfectadoDTO) {
		try{ 
			SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
			CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
			RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();		
			
			inciso = riesgoAfectadoDTO.getId().getNumeroinciso().toString();
			subInciso = riesgoAfectadoDTO.getId().getNumerosubinciso().toString();
			
			SeccionSoporteDanosDTO seccionSoporteDanosDTO = soporteDanosDN.getSeccionSoporte(riesgoAfectadoDTO.getId().getIdtoseccion());
			seccion = seccionSoporteDanosDTO.getDescripcionSeccion();
			
			coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(riesgoAfectadoDTO.getId().getIdtocobertura()); 
			cobertura = coberturaSoporteDanosDTO.getDescripcionCobertura();
			
			riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(riesgoAfectadoDTO.getId().getIdtoriesgo());
			riesgo = riesgoSoporteDanosDTO.getDescripcionRiesgo();
			
			sumaAsegurada = riesgoAfectadoDTO.getSumaAsegurada();
			tipoSA = coberturaSoporteDanosDTO.getDescripcionTipoSumaAsegurada();
		}catch(SystemException ex){ex.printStackTrace();}
	}
	
	public String getInciso() {
		return inciso;
	}
	public String getSubInciso() {
		return subInciso;
	}
	public String getSeccion() {
		return seccion;
	}
	public String getCobertura() {
		return cobertura;
	}
	public String getRiesgo() {
		return riesgo;
	}
	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}
	public String getTipoSA() {
		return tipoSA;
	}
}
