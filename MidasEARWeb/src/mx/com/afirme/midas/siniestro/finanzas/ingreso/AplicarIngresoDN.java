package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AplicarIngresoDN {

	private static final AplicarIngresoDN INSTANCIA = new AplicarIngresoDN();
	
	public static AplicarIngresoDN getInstancia(){
		return INSTANCIA;
	}
	
	public AplicacionIngresoDTO agregarAplicacionIngreso(AplicacionIngresoDTO aplicacionIngresoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.agregarAplicacionIngreso(aplicacionIngresoDTO);
	}
	
	public AplicacionIngresoDTO actualizar(AplicacionIngresoDTO aplicacionIngresoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.actualizar(aplicacionIngresoDTO);
	}
	
	public void eliminarIngreso(AplicacionIngresoDTO aplicacionIngresoDTO)throws SystemException, ExcepcionDeAccesoADatos {
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		aplicarIngresoSN.eliminarIngreso(aplicacionIngresoDTO);
	}
	
	public AplicacionIngresoDTO findById(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos {
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.findById(id);
	}
	
	public List<AplicacionIngresoDTO> findAll()throws SystemException, ExcepcionDeAccesoADatos {
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.findAll();
	}
	
	public AplicacionIngresoDTO buscarAplicarIngresoPorAT(BigDecimal idToAutorizacionTecnica,Short estatus)throws SystemException, ExcepcionDeAccesoADatos{
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.buscarAplicarIngresoPorAT(idToAutorizacionTecnica,estatus);
	}
	
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPorEstatus(Short estatus)throws SystemException, ExcepcionDeAccesoADatos{
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.buscarAplicarIngresoPorEstatus(estatus);
	}
	
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPendientes()throws SystemException, ExcepcionDeAccesoADatos{
		AplicarIngresoSN aplicarIngresoSN = new AplicarIngresoSN();
		return aplicarIngresoSN.buscarAplicarIngresoPendientes();
	}
}
