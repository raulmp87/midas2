package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroFacadeRemote;


public class IngresoSiniestroSN {
	private IngresoSiniestroFacadeRemote beanRemoto;
	
	public IngresoSiniestroSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en IngresoSiniestroSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(IngresoSiniestroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado IngresoSiniestroSN", Level.FINEST, null);		
	}
	
	public IngresoSiniestroDTO agregarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO){
		return beanRemoto.save(ingresoSiniestroDTO);
	}
	
	public void actualizarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO){
		beanRemoto.update(ingresoSiniestroDTO);
	}
	
	public void eliminarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO){
		beanRemoto.delete(ingresoSiniestroDTO);
	}
	
	public IngresoSiniestroDTO findById(BigDecimal id){
		return beanRemoto.findById(id);
	}

	public List<IngresoSiniestroDTO> listarIngresosSiniestro(BigDecimal idReporteSiniestro){
		List<IngresoSiniestroDTO> ingresosSiniestro = null;
		
		ingresosSiniestro = beanRemoto.findByIdReporteSiniestro(idReporteSiniestro);
		
		return ingresosSiniestro;		
	}	
	
	public List<IngresoSiniestroDTO> listarIngresosPendientes(){
		return beanRemoto.listarIngresosPendientes();
	}	
	
	public List<IngresoSiniestroDTO> getIngresosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params){
		return beanRemoto.getIngresosPorReporteYEstatus(idReporteSiniestro,params);
	}
	
	public List<IngresoSiniestroDTO> listarIngresosPorAplicar(IngresoSiniestroFiltroDTO criteriosDeFiltrado){
		return beanRemoto.listarIngresosPorAplicar(criteriosDeFiltrado);
	}
	
	public List<IngresoSiniestroDTO> getIngresosNoCancelados(BigDecimal idReporteSiniestro){
		return beanRemoto.getIngresosNoCancelados(idReporteSiniestro);
	}
}
