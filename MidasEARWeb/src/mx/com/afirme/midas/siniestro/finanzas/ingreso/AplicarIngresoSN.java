package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class AplicarIngresoSN {
	private AplicacionIngresoFacadeRemote beanRemoto;
	
	public AplicarIngresoSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en AplicarIngresoSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(AplicacionIngresoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado AplicarIngresoSN", Level.FINEST, null);		
	}
	
	public AplicacionIngresoDTO agregarAplicacionIngreso(AplicacionIngresoDTO aplicacionIngresoDTO){
		return beanRemoto.save(aplicacionIngresoDTO);
	}
	
	public AplicacionIngresoDTO actualizar(AplicacionIngresoDTO aplicacionIngresoDTO){
		return beanRemoto.update(aplicacionIngresoDTO);
	}
	
	public void eliminarIngreso(AplicacionIngresoDTO aplicacionIngresoDTO){
		beanRemoto.delete(aplicacionIngresoDTO);
	}
	
	public AplicacionIngresoDTO findById(BigDecimal id){
		return beanRemoto.findById(id);
	}
	
	public List<AplicacionIngresoDTO> findAll(){
		return beanRemoto.findAll();
	}
	
	public AplicacionIngresoDTO buscarAplicarIngresoPorAT(BigDecimal idToAutorizacionTecnica,Short estatus){
		return beanRemoto.buscarAplicarIngresoPorAT(idToAutorizacionTecnica,estatus);
	}
	
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPorEstatus(Short estatus){
		return beanRemoto.buscarAplicarIngresoPorEstatus(estatus);
	}
	
	public List<AplicacionIngresoDTO> buscarAplicarIngresoPendientes(){
		return beanRemoto.buscarAplicarIngresoPendientes();
	}
	
	
}
