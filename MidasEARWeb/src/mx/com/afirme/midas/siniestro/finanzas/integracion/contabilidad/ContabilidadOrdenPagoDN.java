package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.siniestro.servicios.SinietroServiciosDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ContabilidadOrdenPagoDN {

	private static final ContabilidadOrdenPagoDN INSTANCIA = new ContabilidadOrdenPagoDN();
	
	public static ContabilidadOrdenPagoDN getInstancia(){
		return INSTANCIA;
	}
	
	public void guardar(ContabilidadOrdenPagoDTO contabilidadOrdenPagoDTO) throws SystemException{
		ContabilidadOrdenPagoSN contabilidadOrdenPagoSN = new ContabilidadOrdenPagoSN();		
		contabilidadOrdenPagoSN.guardar(contabilidadOrdenPagoDTO);
	}
	
	public ContabilidadOrdenPagoDTO actualizar(ContabilidadOrdenPagoDTO contabilidadOrdenPagoDTO) throws SystemException{
		ContabilidadOrdenPagoSN contabilidadOrdenPagoSN = new ContabilidadOrdenPagoSN();		
		return contabilidadOrdenPagoSN.actualizar(contabilidadOrdenPagoDTO);
	}
	
	public ContabilidadOrdenPagoDTO findById(ContabilidadOrdenPagoId contabilidadOrdenPagoId) throws SystemException{
		ContabilidadOrdenPagoSN contabilidadOrdenPagoSN = new ContabilidadOrdenPagoSN();		
		return contabilidadOrdenPagoSN.findById(contabilidadOrdenPagoId);
	}
	
	public void borrar(ContabilidadOrdenPagoDTO entity) throws SystemException{
		ContabilidadOrdenPagoSN contabilidadOrdenPagoSN = new ContabilidadOrdenPagoSN();		
		contabilidadOrdenPagoSN.borrar(entity);		
	}
	
	public BigDecimal contabilizarOrdenDePago(AutorizacionTecnicaDTO autorizacionTecnicaDTO, BigDecimal idToOrdenPago, String usuario) throws SystemException{		
		BigDecimal idReporteSiniestro = autorizacionTecnicaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		BigDecimal idSolicitudCheque = null;
		
		List<RiesgoAfectadoDTO> coberturasRiesgo = null;	
		List<IndemnizacionRiesgoCoberturaDTO> coberturasRiesgoIndemnizacion = null;
		
		List<SoporteContabilidadOrdenPagoDTO> coberturasPorSubramo = null;				
		
		if(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica() == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO){
			SoporteContabilidad soporteContabilidadBean = SoporteContabilidad.getInstancia();
			
			coberturasRiesgo = this.obtenerCoberturasRiesgo(idReporteSiniestro);			
			coberturasPorSubramo = soporteContabilidadBean.agruparCoberturasPorSubramo(idReporteSiniestro, coberturasRiesgo);			
		}else if(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica() == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION){
			IndemnizacionDTO indemnizacionDTO = autorizacionTecnicaDTO.getIndemnizacionDTO(); 
			coberturasRiesgoIndemnizacion = this.obtenerCoberturasRiesgoIndemnizacion(indemnizacionDTO);			
			coberturasPorSubramo = this.agruparCoberturasIndemnizacionPorSubramo(idReporteSiniestro, coberturasRiesgoIndemnizacion);						
		}								
		
		List<ContabilidadOrdenPagoDTO> asientosOrdenDePago = 
			this.generarAsientosOrdenDePago(autorizacionTecnicaDTO, idToOrdenPago, coberturasPorSubramo, usuario);			
				
		this.registrarAsientosOrdenDePago(asientosOrdenDePago);
		
		try{
			idSolicitudCheque = this.solicitarCheque(asientosOrdenDePago.get(0), usuario);
			if(idSolicitudCheque == null){
				throw new Exception("No se pudo obtener el numero de solicitud de cheque");
			}
		}catch(Exception ex){
			LogDeMidasWeb.log("Error al solicitar cheque", Level.SEVERE, ex);
			// elimina los movimientos contabilizados
			if(idSolicitudCheque == null){
				this.eliminarAsientosContables(asientosOrdenDePago);
			}
			
			throw new SystemException(ex);
		}				
		
		return idSolicitudCheque;
	}

	public BigDecimal contabilizarOrdenDePago(BigDecimal idToOrdenPago,
			String usuario, List<AutorizacionTecnicaDTO> autorizaciones,
			int idTipoAutorizacionTecnica) throws SystemException {
		BigDecimal idSolicitudCheque = null;
		List<RiesgoAfectadoDTO> coberturasRiesgo = null;
		List<SoporteContabilidadOrdenPagoDTO> coberturasPorSubramo = null;
		List<ContabilidadOrdenPagoDTO> asientosOrdenDePago = null;
		
		if(idTipoAutorizacionTecnica == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO){
			SoporteContabilidad soporteContabilidadBean = SoporteContabilidad.getInstancia();
			BigDecimal idReporteSiniestro = null;
			for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
				idReporteSiniestro = autorizacion.getReporteSiniestroDTO().getIdToReporteSiniestro();
				coberturasRiesgo = this.obtenerCoberturasRiesgo(idReporteSiniestro);
				coberturasPorSubramo = soporteContabilidadBean.agruparCoberturasPorSubramo(idReporteSiniestro, coberturasRiesgo);
				asientosOrdenDePago = 
					this.generarAsientosOrdenDePago(autorizacion, idToOrdenPago, coberturasPorSubramo, usuario);
				
				this.registrarAsientosOrdenDePago(asientosOrdenDePago);
			}
			
		}							
		try {
			idSolicitudCheque = this.solicitarCheque(
					asientosOrdenDePago.get(0), usuario);
			if (idSolicitudCheque == null) {
				throw new Exception(
						"No se pudo obtener el numero de solicitud de cheque");
			}
		} catch (Exception ex) {
			LogDeMidasWeb.log("Error al solicitar cheque", Level.SEVERE, ex);
			if (idSolicitudCheque == null) {
				this.eliminarAsientosContables(asientosOrdenDePago);
			}

			throw new SystemException(ex);
		}		
		
		return idSolicitudCheque;
	}
	
	private List<RiesgoAfectadoDTO> obtenerCoberturasRiesgo(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		return riesgoAfectadoDN.listarPorReporteSiniestro(idToReporteSiniestro);
	}
	
	private List<IndemnizacionRiesgoCoberturaDTO> obtenerCoberturasRiesgoIndemnizacion(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{				
		IndemnizacionRiesgoCoberturaDN indemnizacionRC = IndemnizacionRiesgoCoberturaDN.getInstancia();
		return indemnizacionRC.buscarPorIndemnizacion(indemnizacionDTO);		
	}	
		
	private List<ContabilidadOrdenPagoDTO> generarAsientosOrdenDePago(AutorizacionTecnicaDTO autorizacionTecnicaDTO, BigDecimal idToOrdenPago, List<SoporteContabilidadOrdenPagoDTO> coberturasAgrupadasPorSubramo, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		List<ContabilidadOrdenPagoDTO> asientosOrdenDePago = new ArrayList<ContabilidadOrdenPagoDTO>();
		BigDecimal idReporteSiniestro = autorizacionTecnicaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		String numeroReporte = autorizacionTecnicaDTO.getReporteSiniestroDTO().getNumeroReporte();
		SoporteContabilidad soporteContabilidad = SoporteContabilidad.getInstancia();
		
		PolizaSoporteDanosDTO poliza = null;
		PrestadorServiciosDTO prestadorServicios = null;
		Double totalSumaAsegurada = soporteContabilidad.getSumaAseguradaPorReporte(idReporteSiniestro);
		Double tipoDeCambio = null;
		Double importeCero = new Double(0.00);		
		
		for(SoporteContabilidadOrdenPagoDTO cobertura : coberturasAgrupadasPorSubramo){
			
			if(poliza == null){
				poliza = soporteContabilidad.getPolizaPorId(cobertura.getIdPoliza());
				tipoDeCambio = soporteContabilidad.getTipoCambio(poliza.getIdMoneda().shortValue(), usuario);
			}
					
			ContabilidadOrdenPagoId idAsientoContable = new ContabilidadOrdenPagoId();
			idAsientoContable.setIdToOrdenPago(idToOrdenPago);
			idAsientoContable.setIdSubr(cobertura.getCodigoSubRamo());
			idAsientoContable.setIdToAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica());
			
			ContabilidadOrdenPagoDTO asientoContable = new ContabilidadOrdenPagoDTO();
			asientoContable.setId(idAsientoContable);
			
			asientoContable.setIdRamo(cobertura.getCodigoRamo());
			asientoContable.setConceptoPol("PAGO DE SINIESTROS MIDAS " + numeroReporte);
			asientoContable.setIdMoneda(poliza.getIdMoneda());
			asientoContable.setTipoCambio(tipoDeCambio); 
			asientoContable.setAuxiliar(new Integer(SoporteContabilidad.AUXULIAR)); 
			asientoContable.setCcosto(SoporteContabilidad.CENTRO_COSTOS); 
			asientoContable.setUsuario(usuario);
			asientoContable.setEstatus(new BigDecimal(0));
			asientoContable.setIdAgente(poliza.getCodigoAgente());
			asientoContable.setImpSalvamento(importeCero);
			asientoContable.setImpPrimasCob(importeCero);
			
			double porcentajeSumaAsegurada = cobertura.getMontoSumaAsegurada() / totalSumaAsegurada.doubleValue();
			
			if(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica() == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO){								
				GastoSiniestroDTO gastoSiniestroDTO = autorizacionTecnicaDTO.getGastoSiniestroDTO();				
				BigDecimal idPrestadorServicios = gastoSiniestroDTO.getIdTcPrestadorServicios();				
								
				if(prestadorServicios == null){
					prestadorServicios = getPrestadorServicios(idPrestadorServicios, usuario);
				}				
												
				asientoContable.setImpDeducible(importeCero);
				asientoContable.setImpCoaseguro(importeCero);
				asientoContable.setIdPrestadorServicio(idPrestadorServicios);								
				asientoContable.setPBeneficiario(prestadorServicios.getNombrePrestador());				
				asientoContable.setTipoPago("TB");
				asientoContable.setImpNeto(gastoSiniestroDTO.getMontoGasto() * porcentajeSumaAsegurada);
				asientoContable.setPtransCont("GASTOS");
				asientoContable.setCptoPago(gastoSiniestroDTO.getConceptoGasto().getSubCuenta());
				
				if(gastoSiniestroDTO.getMontoIVA() != null){
					asientoContable.setImpIva(gastoSiniestroDTO.getMontoIVA() * porcentajeSumaAsegurada);
				}else{
					asientoContable.setImpIva(importeCero);
				}
				
				if(gastoSiniestroDTO.getMontoIVARetencion() != null){
					asientoContable.setImpIvaRet(gastoSiniestroDTO.getMontoIVARetencion() * porcentajeSumaAsegurada);
				}else{
					asientoContable.setImpIvaRet(importeCero);
				}
				
				if(gastoSiniestroDTO.getMontoISRRetencion() != null){
					asientoContable.setImpIsrRet(gastoSiniestroDTO.getMontoISRRetencion() * porcentajeSumaAsegurada);
				}else{
					asientoContable.setImpIsrRet(importeCero);
				}
				
				if(gastoSiniestroDTO.getMontoOtros()!= null){
					asientoContable.setImpOtrosImpuestos(gastoSiniestroDTO.getMontoOtros() * porcentajeSumaAsegurada);
				}else{
					asientoContable.setImpOtrosImpuestos(importeCero);
				}								
			}else{
				IndemnizacionDTO indemnizacionDTO = autorizacionTecnicaDTO.getIndemnizacionDTO();
				asientoContable.setTipoPago("SC");
				asientoContable.setImpNeto(cobertura.getMontoIndemnizacion());
				asientoContable.setPBeneficiario(indemnizacionDTO.getBeneficiario());
				asientoContable.setPtransCont("INDEMNIZACIONES");
				asientoContable.setImpDeducible(cobertura.getMontoDeducible());
				asientoContable.setImpCoaseguro(cobertura.getMontoCoaseguro());												
				
				if(indemnizacionDTO.getMontoIva() != null){
					double porcentajeIVA = indemnizacionDTO.getPorcentajeIva().doubleValue() / 100 ;
					asientoContable.setImpIva(cobertura.getMontoIndemnizacion() * porcentajeIVA);
				}else{
					asientoContable.setImpIva(importeCero);
				}
				
				if(indemnizacionDTO.getMontoIvaRetencion() != null){
					double porcentajeIVARet = indemnizacionDTO.getPorcentajeIvaRetencion().doubleValue() / 100;
					asientoContable.setImpIvaRet(cobertura.getMontoIndemnizacion() * porcentajeIVARet);
				}else{
					asientoContable.setImpIvaRet(importeCero);
				}
				
				if(indemnizacionDTO.getMontoIsrRetencion() != null){
					double porcentajeISRRet = indemnizacionDTO.getPorcentajeIsrRetencion().doubleValue() / 100;
					asientoContable.setImpIsrRet(cobertura.getMontoIndemnizacion() * porcentajeISRRet);
				}else{
					asientoContable.setImpIsrRet(importeCero);
				}
				
				if(indemnizacionDTO.getMontoOtros() != null){
					double porcentajeOtros = indemnizacionDTO.getPorcentajeOtros().doubleValue() / 100;
					asientoContable.setImpOtrosImpuestos(cobertura.getMontoIndemnizacion() * porcentajeOtros);
				}else{
					asientoContable.setImpOtrosImpuestos(importeCero);
				}											
			}
			 
			asientosOrdenDePago.add(asientoContable);
		}
		
		return asientosOrdenDePago;
	}
	
	private void registrarAsientosOrdenDePago(List<ContabilidadOrdenPagoDTO> asientosOrdenDePago) throws SystemException{
		for(ContabilidadOrdenPagoDTO asientoOrdenPago : asientosOrdenDePago){			
			try{
				this.guardar(asientoOrdenPago);
			}catch(SystemException ex){
				throw ex;
			}finally{
				LogDeMidasWeb.log(asientoOrdenPago.toString(), Level.INFO, null);
			}			
		}
	}
	
	private BigDecimal solicitarCheque(ContabilidadOrdenPagoDTO asientoContable, String usuario) throws SystemException{
		BigDecimal idSolicitudCheque = null;
		
//		SolicitudChequeDN solicitudChequeDN = SolicitudChequeDN.getInstancia(usuario);
		SinietroServiciosDN sinietroServiciosDN = SinietroServiciosDN.getInstancia(usuario);
		idSolicitudCheque = sinietroServiciosDN.solicitaCheque(
//		idSolicitudCheque = solicitudChequeDN.solicitaCheque(
									asientoContable.getId().getIdToOrdenPago(),
									asientoContable.getPtransCont(),
									asientoContable.getIdMoneda().shortValue(),
									asientoContable.getTipoCambio(),
									asientoContable.getIdPrestadorServicio(),
									asientoContable.getPBeneficiario(),
									asientoContable.getTipoPago(),
									asientoContable.getConceptoPol());
		
		if(idSolicitudCheque == null){
			throw new ExcepcionDeAccesoADatos("No se pudo obtener el numero de solicitud de cheque");
		}
		
		return idSolicitudCheque;
	}	
		
	private void eliminarAsientosContables(List<ContabilidadOrdenPagoDTO> asientosOrdenDePago) throws SystemException{
		for(ContabilidadOrdenPagoDTO asientoContable : asientosOrdenDePago){
			this.borrar(asientoContable);	
		}
	}
	
	private PrestadorServiciosDTO getPrestadorServicios(BigDecimal idPrestadorServicios, String nombreUsuario) throws SystemException{
		PrestadorServiciosDTO prestadorDeSerivicios = null;
		
		PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN.getInstancia();
		prestadorDeSerivicios = prestadorServiciosDN.detallePrestador(idPrestadorServicios, nombreUsuario);
		
		return prestadorDeSerivicios;		
	}		

	private List<SoporteContabilidadOrdenPagoDTO> agruparCoberturasIndemnizacionPorSubramo(BigDecimal idReporteSiniestro, List<IndemnizacionRiesgoCoberturaDTO> coberturasRiesgo) throws ExcepcionDeAccesoADatos, SystemException{
		List<SoporteContabilidadOrdenPagoDTO> coberturasAgrupadas = new ArrayList<SoporteContabilidadOrdenPagoDTO>();
		
		Map<BigDecimal, SoporteContabilidadOrdenPagoDTO> agrupadorCoberturas = new HashMap<BigDecimal, SoporteContabilidadOrdenPagoDTO>();
		agrupadorCoberturas.clear();
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		
		for(IndemnizacionRiesgoCoberturaDTO coberturaRiesgo : coberturasRiesgo){										 
			CoberturaSoporteDanosDTO coberturaSoporteDTO = soporteDanosDN.getCoberturaSoporte(coberturaRiesgo.getId().getIdToCobertura());
			
			if(coberturaSoporteDTO != null){				
				BigDecimal idSubRamo = coberturaSoporteDTO.getIdTcSubRamo();
				SoporteContabilidadOrdenPagoDTO soporteContabilidadOrdenPago = null;
				
				if(agrupadorCoberturas.containsKey(idSubRamo)){
					soporteContabilidadOrdenPago = agrupadorCoberturas.get(idSubRamo);
					
					double montoIndemnizacion = soporteContabilidadOrdenPago.getMontoIndemnizacion();
					double montoDeducible = soporteContabilidadOrdenPago.getMontoDeducible();
					double montoCoaseguro = soporteContabilidadOrdenPago.getMontoCoaseguro();
					
					montoIndemnizacion += coberturaRiesgo.getMontoPago().doubleValue();
					montoDeducible += coberturaRiesgo.getDeducible().doubleValue();
					montoCoaseguro += coberturaRiesgo.getCoaseguro().doubleValue();					
					
					soporteContabilidadOrdenPago.setMontoIndemnizacion(montoIndemnizacion);
					soporteContabilidadOrdenPago.setMontoDeducible(montoDeducible);
					soporteContabilidadOrdenPago.setMontoCoaseguro(montoCoaseguro);					
				}else{
					soporteContabilidadOrdenPago = new SoporteContabilidadOrdenPagoDTO();				
					soporteContabilidadOrdenPago.setIdRamo(coberturaSoporteDTO.getIdTcRamo());
					soporteContabilidadOrdenPago.setCodigoRamo(coberturaSoporteDTO.getCodigoRamo());
					soporteContabilidadOrdenPago.setIdSubRamo(idSubRamo);
					soporteContabilidadOrdenPago.setCodigoSubRamo(coberturaSoporteDTO.getCodigoSubRamo());								
					soporteContabilidadOrdenPago.setMontoIndemnizacion(coberturaRiesgo.getMontoPago().doubleValue());
					soporteContabilidadOrdenPago.setMontoDeducible(coberturaRiesgo.getDeducible().doubleValue());
					soporteContabilidadOrdenPago.setMontoCoaseguro(coberturaRiesgo.getCoaseguro().doubleValue());
					
					soporteContabilidadOrdenPago.setIdPoliza(coberturaRiesgo.getId().getIdToPoliza());
					
					agrupadorCoberturas.put(idSubRamo, soporteContabilidadOrdenPago);
				}
				
				LogDeMidasWeb.log(soporteContabilidadOrdenPago.toString(), Level.INFO, null);															
			}																					
		}			
		
		if(!agrupadorCoberturas.isEmpty()){
			coberturasAgrupadas.addAll(agrupadorCoberturas.values());
		}
		
		return coberturasAgrupadas;		
	}		
}
 