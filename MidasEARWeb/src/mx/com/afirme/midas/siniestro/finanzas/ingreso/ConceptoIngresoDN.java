package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConceptoIngresoDN {
	private static final ConceptoIngresoDN INSTANCIA = new ConceptoIngresoDN();
	
	public static ConceptoIngresoDN getInstancia(){
		return INSTANCIA;
	}

	public void agregarConceptoIngreso(ConceptoIngresoDTO conceptoIngresoDTO)throws SystemException, ExcepcionDeAccesoADatos{
		ConceptoIngresoSN conceptoIngresoSN = new ConceptoIngresoSN();
		conceptoIngresoSN.agregarConceptoIngreso(conceptoIngresoDTO);
	}
	
	public void eliminarConceptoIngreso(ConceptoIngresoDTO conceptoIngresoDTO)throws SystemException, ExcepcionDeAccesoADatos{
		ConceptoIngresoSN conceptoIngresoSN = new ConceptoIngresoSN();
		conceptoIngresoSN.eliminarConceptoIngreso(conceptoIngresoDTO);	
	}
	
	public ConceptoIngresoDTO actualizarConceptoIngreso(ConceptoIngresoDTO conceptoIngresoDTO)throws SystemException, ExcepcionDeAccesoADatos{
		ConceptoIngresoSN conceptoIngresoSN = new ConceptoIngresoSN();
		return conceptoIngresoSN.actualizarConceptoIngreso(conceptoIngresoDTO);
	}
	
	public ConceptoIngresoDTO findById(BigDecimal id)throws SystemException, ExcepcionDeAccesoADatos{
		ConceptoIngresoSN conceptoIngresoSN = new ConceptoIngresoSN();
		return conceptoIngresoSN.findById(id);
	}
 
	public List<ConceptoIngresoDTO> findAll()throws SystemException, ExcepcionDeAccesoADatos{
		ConceptoIngresoSN conceptoIngresoSN = new ConceptoIngresoSN();
		return conceptoIngresoSN.findAll();
	} 	

}
