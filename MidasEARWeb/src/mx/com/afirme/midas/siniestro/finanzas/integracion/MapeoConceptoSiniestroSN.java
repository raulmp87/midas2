package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.math.BigDecimal;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class MapeoConceptoSiniestroSN {
	MapeoConceptoSiniestroFacadeRemote beanRemoto;
	
	public MapeoConceptoSiniestroSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en MapeoConceptoSiniestroSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(MapeoConceptoSiniestroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado MapeoConceptoSiniestroSN", Level.FINEST, null);
	}
	
	public MapeoConceptoSiniestroDTO getMapeoByIdConcepto(BigDecimal idConcepto, BigDecimal concepto){
		return beanRemoto.getMapeoByIdConcepto(idConcepto, concepto);
	}
}
