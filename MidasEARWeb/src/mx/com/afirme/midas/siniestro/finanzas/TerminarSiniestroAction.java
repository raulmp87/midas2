package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteDN;
import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class TerminarSiniestroAction extends MidasMappingDispatchAction{
	
	public ActionForward mostrarReporteSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		
		String strIdReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal idReporteSiniestro = new BigDecimal(strIdReporteSiniestro);

		TerminoAjusteDN terminoAjusteDN = TerminoAjusteDN.getInstancia();
		List<TerminoAjusteDTO> terminoAjusteList = new ArrayList<TerminoAjusteDTO>();
		ReporteSiniestroDTO reporteSiniestroDTO = null;
		TerminarSiniestroForm beanForm = (TerminarSiniestroForm)form;
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
				
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(nombreUsuario);
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDTO reservaDTO = null;		
		
		try{
			reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idReporteSiniestro);
			terminoAjusteList = terminoAjusteDN.listarTodos();
			beanForm.setIdReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro());
			beanForm.setNumeroReporte(reporteSiniestroDTO.getNumeroReporte());
			beanForm.setTerminosAjuste(terminoAjusteList);
			Double sumaAsegurada = riesgoAfectadoDN.sumaAseguradaPorReporte(idReporteSiniestro);
			beanForm.setSumaAsegurada(sumaAsegurada);
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(idReporteSiniestro);
			reservaDTO = reservaDN.listarReservaActual(idReporteSiniestro);
			if(reservaDTO != null){
				beanForm.setListaCoberturas(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDTO.getReservaDetalleDTOs()));
			}
			
			beanForm.setTipoMoneda(tipoMoneda);
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}	
								
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward terminarReporteSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		TerminarSiniestroForm beanForm = (TerminarSiniestroForm)form;
		BigDecimal idReporteSiniestro = beanForm.getIdReporteSiniestro();
		
		String strIdTerminoAjuste = beanForm.getIdTerminoAjuste();
		String descripcionTerminoAjuste = beanForm.getDescripcionTerminoAjuste();		
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		Long idTerminoAjuste = new Long(strIdTerminoAjuste);
		
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(nombreUsuario);
		ReporteSiniestroDTO reporteSiniestroDTO = null;
		
		try{
			TerminoAjusteDN terminoAjusteDN = TerminoAjusteDN.getInstancia();
			TerminoAjusteDTO terminoAjusteDTO = new TerminoAjusteDTO();
			reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idReporteSiniestro);
			terminoAjusteDTO.setIdTcTerminoAjuste(new BigDecimal(idTerminoAjuste.toString()));
			terminoAjusteDTO = terminoAjusteDN.getPorId(terminoAjusteDTO);
			reporteSiniestroDTO.setTerminoAjuste(terminoAjusteDTO);
			reporteSiniestroDTO.setDescripcionTerminoAjuste(descripcionTerminoAjuste);			
			ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
			
			if(idTerminoAjuste.intValue() == TerminoAjusteDTO.ESTATUS_RECHAZO){
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_ASOCIAR_CARTA_RECHAZO);				
			}else{
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.REPORTE_TERMINADO);
			}						
			
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);					
		}catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}				
		
		return mapping.findForward(reglaNavegacion);
	}
		
	public ActionForward cancelarReporteSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		return mapping.findForward(reglaNavegacion);
	}	
}
