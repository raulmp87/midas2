package mx.com.afirme.midas.siniestro.finanzas;

public class Dummy {
	private static final long serialVersionUID = 1L;

	private String valor1;
	private String valor2;
	private String valor3;
	private String valor4;
	private String valor5;
	private String valor6;
	private String valor7;
	private String valor8;
	private String valor9;
	
	public Dummy(){
		
	}
	
	/**
	 * @return the valor1
	 */
	public String getValor1() {
		return valor1;
	}
	/**
	 * @param valor1 the valor1 to set
	 */
	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}
	/**
	 * @return the valor2
	 */
	public String getValor2() {
		return valor2;
	}
	/**
	 * @param valor2 the valor2 to set
	 */
	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}
	/**
	 * @return the valor3
	 */
	public String getValor3() {
		return valor3;
	}
	/**
	 * @param valor3 the valor3 to set
	 */
	public void setValor3(String valor3) {
		this.valor3 = valor3;
	}
	/**
	 * @return the valor4
	 */
	public String getValor4() {
		return valor4;
	}
	/**
	 * @param valor4 the valor4 to set
	 */
	public void setValor4(String valor4) {
		this.valor4 = valor4;
	}
	/**
	 * @return the valor5
	 */
	public String getValor5() {
		return valor5;
	}
	/**
	 * @param valor5 the valor5 to set
	 */
	public void setValor5(String valor5) {
		this.valor5 = valor5;
	}
	/**
	 * @return the valor6
	 */
	public String getValor6() {
		return valor6;
	}
	/**
	 * @param valor6 the valor6 to set
	 */
	public void setValor6(String valor6) {
		this.valor6 = valor6;
	}
	/**
	 * @return the valor7
	 */
	public String getValor7() {
		return valor7;
	}
	/**
	 * @param valor7 the valor7 to set
	 */
	public void setValor7(String valor7) {
		this.valor7 = valor7;
	}

	/**
	 * @return the valor8
	 */
	public String getValor8() {
		return valor8;
	}

	/**
	 * @param valor8 the valor8 to set
	 */
	public void setValor8(String valor8) {
		this.valor8 = valor8;
	}

	/**
	 * @return the valor9
	 */
	public String getValor9() {
		return valor9;
	}

	/**
	 * @param valor9 the valor9 to set
	 */
	public void setValor9(String valor9) {
		this.valor9 = valor9;
	}
	
	
}
