package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class OrdenDePagoSN {
	private OrdenDePagoFacadeRemote beanRemoto;

	public OrdenDePagoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en OrdenDePagoSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(OrdenDePagoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado OrdenDePagoSN",
				Level.FINEST, null);
	}

	public OrdenDePagoDTO guardarOrdenDePago(OrdenDePagoDTO ordenDePagoDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.save(ordenDePagoDTO);
	}

	public List<OrdenDePagoDTO> findByProperty(String propertyName, Object value)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByProperty(propertyName, value);
	}

	public List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestro(
			BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos {
		return beanRemoto
				.obtenerOrdenesDePagoXReporteSiniestro(idToReporteSiniestro);
	}
	
	public List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestroIndividual(
			BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos {
		return beanRemoto
				.obtenerOrdenesDePagoXReporteSiniestroIndividual(idToReporteSiniestro);
	}

	public OrdenDePagoDTO findById(BigDecimal idToOrdenPago)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(idToOrdenPago);
	}

	public OrdenDePagoDTO update(OrdenDePagoDTO ordenDePagoDTO)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.update(ordenDePagoDTO);
	}

	public void borrar(OrdenDePagoDTO ordenDePagoDTO) {
		beanRemoto.delete(ordenDePagoDTO);
	}

	public OrdenDePagoDTO obtenerOrdenPorIdAutorizacionTecnica(
			BigDecimal idAutorizacionTecnica) {
		return beanRemoto
				.obtenerOrdenPorIdAutorizacionTecnica(idAutorizacionTecnica);
	}

	public OrdenDePagoDTO obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(
			BigDecimal idAutorizacionTecnica) {
		return beanRemoto
				.obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(idAutorizacionTecnica);
	}

	public OrdenDePagoDTO buscaPrimerRegistro() {
		try {
			return beanRemoto.buscaPrimerRegistro();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

	public List<DetalleOrdenPagoDTO> listarOrdenesDePagoFiltrado(
			SoportePagosDTO soportePagosDTO) throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarOrdenesDePagoFiltrado(soportePagosDTO);
	}

	public List<DetalleOrdenPagoDTO> buscarDetallePorAutorizacionTecnica(
			BigDecimal idToAutorizacionTecnica) throws ExcepcionDeAccesoADatos {
		return beanRemoto
				.buscarDetallePorAutorizacionTecnica(idToAutorizacionTecnica);
	}

	public void agregarDetalleOrdenDePago(DetalleOrdenPagoDTO detalle)
			throws ExcepcionDeAccesoADatos {
		beanRemoto.agregarDetalleOrdenDePago(detalle);
	}

}
