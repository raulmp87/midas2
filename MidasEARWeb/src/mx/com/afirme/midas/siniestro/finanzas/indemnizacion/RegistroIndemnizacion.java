package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

public class RegistroIndemnizacion{

	private static final long serialVersionUID = 1L;
	
	private IndemnizacionDTO indemnizacionDTO;
	private String descripcionTipoMovimiento;
	private Double totalPerdida;
	private Double totalDeducible;
	private Double totalCoaseguro;
	private Double totalIndemnizacion;
	private String tipoPago;
	
	public IndemnizacionDTO getIndemnizacionDTO() {
		return indemnizacionDTO;
	}
	public void setIndemnizacionDTO(IndemnizacionDTO indemnizacionDTO) {
		this.indemnizacionDTO = indemnizacionDTO;
	}
	public String getDescripcionTipoMovimiento() {
		return descripcionTipoMovimiento;
	}
	public void setDescripcionTipoMovimiento(String descripcionTipoMovimiento) {
		this.descripcionTipoMovimiento = descripcionTipoMovimiento;
	}
	public Double getTotalPerdida() {
		return totalPerdida;
	}
	public void setTotalPerdida(Double totalPerdida) {
		this.totalPerdida = totalPerdida;
	}
	public Double getTotalDeducible() {
		return totalDeducible;
	}
	public void setTotalDeducible(Double totalDeducible) {
		this.totalDeducible = totalDeducible;
	}
	public Double getTotalCoaseguro() {
		return totalCoaseguro;
	}
	public void setTotalCoaseguro(Double totalCoaseguro) {
		this.totalCoaseguro = totalCoaseguro;
	}
	public Double getTotalIndemnizacion() {
		return totalIndemnizacion;
	}
	public void setTotalIndemnizacion(Double totalIndemnizacion) {
		this.totalIndemnizacion = totalIndemnizacion;
	}
	public String getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	
}