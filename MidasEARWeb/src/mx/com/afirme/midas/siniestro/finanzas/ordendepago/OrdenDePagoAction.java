package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle.DetalleOrdenDePagoDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class OrdenDePagoAction extends MidasMappingDispatchAction{

	private static final String RECURSO = "mx.com.afirme.midas.RecursoDeMensajes";
	
	public ActionForward generarOrdenPago(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;

		OrdenDePagoForm forma = (OrdenDePagoForm) form;
		String strIdToAutorizacionTecnica = request.getParameter("idToAutorizacionTecnica");
//		String strIdToAutorizacionTecnica = "99";
		String strIdToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal idToAutorizacionTecnica = strIdToAutorizacionTecnica!=null?new BigDecimal(strIdToAutorizacionTecnica):new BigDecimal(99);
//		BigDecimal idToReporteSiniestro = strIdToReporteSiniestro!=null?new BigDecimal(strIdToReporteSiniestro):new BigDecimal(1);
		
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		try {
			AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.findById(idToAutorizacionTecnica);
			forma.setIdToAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toString());
			if(autorizacionTecnicaDTO.getGastoSiniestroDTO() != null){
				forma.setTipoAutorizacionTecnica(UtileriasWeb.getMensajeRecurso(RECURSO, "siniestro.finanzas.ordenPago.tipo.gasto"));
				forma.setIdTipoAutorizacionTecnica(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_GASTO);
				forma.setId(autorizacionTecnicaDTO.getGastoSiniestroDTO().getIdToGastoSiniestro().toString());
			}else{
				forma.setTipoAutorizacionTecnica(UtileriasWeb.getMensajeRecurso(RECURSO, "siniestro.finanzas.ordenPago.tipo.indemnizacion"));
				forma.setIdTipoAutorizacionTecnica(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_INDEMNIZACION);
				forma.setId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion().toString());
			}
			forma.setMontoNeto(UtileriasWeb.formatoMoneda(autorizacionTecnicaDTO.getMontoNeto()));
			forma.setIdToAutorizacionTecnica(strIdToAutorizacionTecnica);
			forma.setIdToReporteSiniestro(strIdToReporteSiniestro);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method guardarOrdenDePago
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void guardarOrdenDePago(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException {
		BigDecimal numeroAutorizacionTecnica = new BigDecimal(request.getParameter("numeroAutorizacionTecnica"));
		String usuario = null;

		try{		
			usuario = UtileriasWeb.obtieneNombreUsuario(request);
			OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
			AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
					
			AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.findById(numeroAutorizacionTecnica);					
			
			OrdenDePagoDTO ordenDePagoDTO = new OrdenDePagoDTO();
			ordenDePagoDTO.setAutorizacionTecnica(autorizacionTecnicaDTO);
			ordenDePagoDTO.setEstatus(OrdenDePagoDTO.ESTATUS_PENDIENTE);
			
			ordenDePagoDTO = ordenDePagoDN.generarOrdenDePago(ordenDePagoDTO, autorizacionTecnicaDTO, usuario);
			
			StringBuffer buffer = new StringBuffer();
			if(ordenDePagoDTO.getIdSolicitudCheque() != null){			
				buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
				buffer.append("<response>");
				buffer.append("<id>");
				buffer.append(ordenDePagoDTO.getIdToOrdenPago());
				buffer.append("</id>");
				buffer.append("<valor>");
				buffer.append(UtileriasWeb.llenarIzquierda(ordenDePagoDTO.getIdToOrdenPago().toString(), "0", 8));
				buffer.append("</valor>");			
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());								
			}else{
				buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
				buffer.append("<response>");
				buffer.append("<id>0</id>");
				buffer.append("<valor>0</valor>");
				buffer.append("</response>");
				response.setContentType("text/xml");
				response.setHeader("Cache-Control", "no-cache");
				response.setContentLength(buffer.length());
				response.getWriter().write(buffer.toString());																
			}
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (SystemException e) {
			throw new SystemException("El sistema no est� disponible por el momento.", e);			
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag", ioException);
		} catch (ExcepcionDeAccesoADatos e) {
			throw new SystemException("Error de acceso de datos.", e);
		} catch (Exception e) {
			throw new SystemException("Error de acceso de datos.", e);
		} 			
	}	
	
	public ActionForward listarOrdenesDePago(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		OrdenDePagoForm forma = (OrdenDePagoForm) form;
		String strIdToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal idToReporteSiniestro = strIdToReporteSiniestro!=null?new BigDecimal(strIdToReporteSiniestro):new BigDecimal(1);
		
		
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
		try {
			List<OrdenDePagoDTO> listaOrdenes = ordenDePagoDN.obtenerOrdenesDePagoXReporteSiniestroIndividual(idToReporteSiniestro);
			for(OrdenDePagoDTO onjOrden:listaOrdenes){
				onjOrden.setNumeroOrdenPago(UtileriasWeb.llenarIzquierda(onjOrden.getIdToOrdenPago().toString(), "0", 8));
			}
			forma.setListaOrdenesDePago(listaOrdenes);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method guardarOrdenDePago
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public void cancelarOrdenDePago(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException {
		try{
			OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
			OrdenDePagoForm forma = (OrdenDePagoForm) form;
			SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
			IndemnizacionDN indemnizacionDN =IndemnizacionDN.getInstancia();
			
			OrdenDePagoDTO ordenDePagoDTO;
			BigDecimal idToOrdenPago = new BigDecimal(request.getParameter("idOrdenPago"));
			BigDecimal idToReporteSiniestro =  new BigDecimal(forma.getIdToReporteSiniestro());
			
			ordenDePagoDTO = ordenDePagoDN.findById(idToOrdenPago);
			StringBuffer buffer = new StringBuffer();
			List<DetalleOrdenPagoDTO> detalles = DetalleOrdenDePagoDN.getInstancia().detallesOrdenDePago(idToOrdenPago);
			for(DetalleOrdenPagoDTO detalleOrden: detalles){
				AutorizacionTecnicaDTO autorizacionTecnicaDTO = detalleOrden.getAutorizacionTecnicaDTO();
				if(!siniestroMovimientosDN.validaMovimientoPagado(autorizacionTecnicaDTO)){
					ordenDePagoDTO.setEstatus(OrdenDePagoDTO.ESTATUS_CANCELADA);
					ordenDePagoDTO = ordenDePagoDN.update(ordenDePagoDTO);
					
					if(autorizacionTecnicaDTO.getIndemnizacionDTO() == null || indemnizacionDN.tienePagosParciales(idToReporteSiniestro)){
						siniestroMovimientosDN.actualizarEstatusMovimiento(autorizacionTecnicaDTO, EstatusFinanzasDTO.AUTORIZADO);
						if(autorizacionTecnicaDTO.getIndemnizacionDTO() !=null){
							IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion());
							if(indemnizacionDTO.getUltimoPago()){
								siniestroMovimientosDN.cambiaEstatusReporte(idToReporteSiniestro, UtileriasWeb.obtieneNombreUsuario(request), ReporteSiniestroDTO.PENDIENTE_GENERAR_ORDEN_DE_PAGO);
							}
						} 
						buffer = this.generaMsjExito(idToOrdenPago);
					}else{
						siniestroMovimientosDN.actualizarEstatusMovimiento(autorizacionTecnicaDTO, EstatusFinanzasDTO.AUTORIZADO);
						siniestroMovimientosDN.cambiaEstatusReporte(idToReporteSiniestro, UtileriasWeb.obtieneNombreUsuario(request), ReporteSiniestroDTO.PENDIENTE_GENERAR_ORDEN_DE_PAGO);
					}
				}else{
					buffer = this.generaMsjExito(idToOrdenPago);
				}
				
			}
			response.setContentType("text/xml");
			response.setHeader("Cache-Control", "no-cache");
			response.setContentLength(buffer.length());
			response.getWriter().write(buffer.toString());	
		} catch (RemoteException rException) {
			throw new SystemException(rException);
		} catch (SystemException e) {
			throw new SystemException("El sistema no est� disponible por el momento.",e);			
		} catch (IOException ioException) {
			throw new SystemException("Unable to render select tag",ioException);
		} catch (ExcepcionDeAccesoADatos e) {
			throw new SystemException("Error de acceso de datos.",e);
		} 			
	}	
	
	private StringBuffer generaMsjExito(BigDecimal idToOrdenPago){
		StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<id>");
			buffer.append(idToOrdenPago);
			buffer.append("</id>");
			buffer.append("<valor>");
			buffer.append(UtileriasWeb.llenarIzquierda(idToOrdenPago.toString(), "0", 8));
			buffer.append("</valor>");			
			buffer.append("</response>");
		return buffer;
	}
	
	public ActionForward revisarEstatusOrdenDePago(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws SystemException {
		String reglaNavegacion = Sistema.EXITOSO;
		
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
		String strIdOrdenDePago = request.getParameter("idOrdenPago");
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		
		if(strIdOrdenDePago != null){
			BigDecimal idToOrdenDePago = new BigDecimal(strIdOrdenDePago);
			try {
				ordenDePagoDN.verificarEstatus(idToOrdenDePago, nombreUsuario);
			} catch (Exception e) {
				reglaNavegacion = Sistema.NO_DISPONIBLE;
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}
		}
		
		return mapping.findForward(reglaNavegacion);
	}		
}

