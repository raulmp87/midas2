package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDN;
import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ContabilidadSiniestroDN {
	private static final BigDecimal ID_MOVIMIENTO_CONTABILIZAR_SINIESTRO = new BigDecimal(1);
	private static final String CENTRO_COSTOS_CONTABILIZAR_SINIESTRO = "1.101";
	
	private static final ContabilidadSiniestroDN INSTANCIA = new ContabilidadSiniestroDN();
	
	public static ContabilidadSiniestroDN getInstancia(){
		return INSTANCIA;
	}
	
	public void guardar(ContabilidadSiniestroDTO contabilidadSiniestroDTO) throws SystemException{
		ContabilidadSiniestroSN contabilidadSiniestro = new ContabilidadSiniestroSN();		
		contabilidadSiniestro.guardar(contabilidadSiniestroDTO);
	}
	
	public ContabilidadSiniestroDTO actualizar(ContabilidadSiniestroDTO contabilidadSiniestroDTO) throws SystemException{
		ContabilidadSiniestroSN contabilidadSiniestro = new ContabilidadSiniestroSN();		
		return contabilidadSiniestro.actualizar(contabilidadSiniestroDTO);
	}
	
	public ContabilidadSiniestroDTO findById(ContabilidadSiniestroId contabilidadSiniestroId) throws SystemException{
		ContabilidadSiniestroSN contabilidadSiniestro = new ContabilidadSiniestroSN();		
		return contabilidadSiniestro.findById(contabilidadSiniestroId);
	}
	
	public void contabilizarReserva(ReservaDTO reservaDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		//BigDecimal idToReporteSiniestro = reservaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		String identificadorContable = "";
		
		Map<BigDecimal, SoporteContabilidadSiniestroDTO> listaAgrupacionCoberturasReserva =
			getAgrupacionMovimientosReserva(reservaDTO, usuario);
		
		List<ContabilidadSiniestroDTO> movimientos = 
			getMovimientosReservaPorContabilizar(reservaDTO, listaAgrupacionCoberturasReserva, usuario);
		
		registrarMovimientosReserva(movimientos);
		
		identificadorContable = getIdentificadorContable(movimientos.get(0));
		
		List<AsientoContableDTO> asientosContables = contabilizar(identificadorContable, usuario);	
		
		actualizarMovimientosContabilizados(movimientos, asientosContables);
	}
		
	private Map<BigDecimal, SoporteContabilidadSiniestroDTO> getAgrupacionMovimientosReserva(ReservaDTO reservaDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		BigDecimal idToReservaEstimada = reservaDTO.getIdtoreservaestimada();
		BigDecimal tipoReserva = reservaDTO.getTipoajuste();
		
		BigDecimal idToReporteSiniestro = reservaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		
		Map<BigDecimal, SoporteContabilidadSiniestroDTO> dist = new HashMap<BigDecimal, SoporteContabilidadSiniestroDTO>();
		
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
//		List<ReservaDetalleDTO> reservaDetalle = reservaDetalleDN.listarDetalleReserva(idToReservaEstimada);
		List<ReservaDetalleDTO> reservaDetalle = reservaDetalleDN.listarReservaDetalle(idToReservaEstimada);
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		
		for(ReservaDetalleDTO beanReserva : reservaDetalle){
			BigDecimal idToCobertura = beanReserva.getId().getIdtocobertura();					
			CoberturaSoporteDanosDTO coberturaSoporteDTO = soporteDanosDN.getCoberturaSoporte(idToCobertura);
			
			if(coberturaSoporteDTO != null){
				BigDecimal subRamo = coberturaSoporteDTO.getIdTcSubRamo();
				SoporteContabilidadSiniestroDTO sc = null;
				
				if(tipoReserva.intValue() == 0){// Reserva Inicial{
					double estimacion = beanReserva.getEstimacion().doubleValue();
					
					if(dist.containsKey(subRamo)){
						sc = dist.get(subRamo);
						double estimacionAcumulada = sc.getEstimacion().doubleValue();
						sc.setEstimacion(new Double(estimacion + estimacionAcumulada));					
					}else{
						sc = new SoporteContabilidadSiniestroDTO();						
						sc.setIdToReporteSiniestro(idToReporteSiniestro);
						sc.setIdTcMovimientoSiniestro(ID_MOVIMIENTO_CONTABILIZAR_SINIESTRO);
						sc.setIdRegistro(idToReservaEstimada);
						sc.setCodigoSubRamo(coberturaSoporteDTO.getCodigoSubRamo());											
						sc.setCodigoRamo(coberturaSoporteDTO.getCodigoRamo());
						sc.setCveConceptoMov(SoporteContabilidadSiniestroDTO.CLAVE_MOVIMIENTO_ESTIMACION_ORIGINAL); 											
						sc.setEstimacion(new Double(estimacion));						
						sc.setConceptoMov(SoporteContabilidadSiniestroDTO.CONCEPTO_MOVIMIENTO_ESTIMACION_ORIGINAL);						
						sc.setIdTcSubRamo(coberturaSoporteDTO.getIdTcSubRamo());
						
						dist.put(coberturaSoporteDTO.getIdTcSubRamo(), sc);
					}					
				}else{
					double estimacion = beanReserva.getAjusteMas().doubleValue() - beanReserva.getAjusteMenos();
					
					if(dist.containsKey(subRamo)){
						sc = dist.get(subRamo);										
						double estimacionAcumulada = sc.getEstimacion().doubleValue();
						double ajuste = beanReserva.getAjusteMas().doubleValue() - beanReserva.getAjusteMenos(); 
						sc.setEstimacion(new Double(estimacionAcumulada + ajuste));
						
						if(Math.round(sc.getEstimacion().doubleValue()) > 0){
							sc.setCveConceptoMov(SoporteContabilidadSiniestroDTO.CLAVE_MOVIMIENTO_AJUSTE_AUMENTO);																							
							sc.setConceptoMov(SoporteContabilidadSiniestroDTO.CONCEPTO_MOVIMIENTO_AJUSTE_AUMENTO);																			
						}else{
							sc.setCveConceptoMov(SoporteContabilidadSiniestroDTO.CLAVE_MOVIMIENTO_AJUSTE_DISMINUCION);																							
							sc.setConceptoMov(SoporteContabilidadSiniestroDTO.CONCEPTO_MOVIMIENTO_AJUSTE_DISMINUCION);																										
						}																
					}else{
						sc = new SoporteContabilidadSiniestroDTO();						
						sc.setIdToReporteSiniestro(idToReporteSiniestro);
						sc.setIdTcMovimientoSiniestro(ID_MOVIMIENTO_CONTABILIZAR_SINIESTRO);
						sc.setIdRegistro(idToReservaEstimada);
						sc.setCodigoSubRamo(coberturaSoporteDTO.getCodigoSubRamo());											
						sc.setCodigoRamo(coberturaSoporteDTO.getCodigoRamo());						
						sc.setEstimacion(new Double(estimacion));
						sc.setIdTcSubRamo(coberturaSoporteDTO.getIdTcSubRamo());
						
						if(Math.round(sc.getEstimacion().doubleValue()) > 0){
							sc.setCveConceptoMov(SoporteContabilidadSiniestroDTO.CLAVE_MOVIMIENTO_AJUSTE_AUMENTO);																							
							sc.setConceptoMov(SoporteContabilidadSiniestroDTO.CONCEPTO_MOVIMIENTO_AJUSTE_AUMENTO);																										
						}else{
							sc.setCveConceptoMov(SoporteContabilidadSiniestroDTO.CLAVE_MOVIMIENTO_AJUSTE_DISMINUCION);																							
							sc.setConceptoMov(SoporteContabilidadSiniestroDTO.CONCEPTO_MOVIMIENTO_AJUSTE_DISMINUCION);																										
						}																
																								
						dist.put(coberturaSoporteDTO.getIdTcSubRamo(), sc);
						
						LogDeMidasWeb.log(sc.toString(), Level.INFO, null);
					}					
				}
				
				LogDeMidasWeb.log(coberturaSoporteDTO.toString(), Level.INFO, null );
			}						
		}
		
		return dist;
	}
	
	private List<ContabilidadSiniestroDTO> getMovimientosReservaPorContabilizar(ReservaDTO reservaDTO, Map<BigDecimal, SoporteContabilidadSiniestroDTO> coberturasReserva, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		List<ContabilidadSiniestroDTO> coberturasPorContabilizar = new ArrayList<ContabilidadSiniestroDTO>();
		
		BigDecimal numeroPoliza = reservaDTO.getReporteSiniestroDTO().getNumeroPoliza();	
		String numeroReporte = reservaDTO.getReporteSiniestroDTO().getNumeroReporte();
		
		PolizaSoporteDanosDTO poliza = getPolizaPorNumeroPoliza(numeroPoliza);		
		BigDecimal idMoneda = poliza.getIdMoneda();		
		String fechaAplico = UtileriasWeb.getFechaString(Calendar.getInstance().getTime());
		String[] fecha = fechaAplico.split("/");
		
		Double tipoCambio = getTipoCambioActual(usuario, (short)idMoneda.intValue());
		
		for(BigDecimal key :  coberturasReserva.keySet()){
			SoporteContabilidadSiniestroDTO sc = coberturasReserva.get(key); 
			
			if(Math.round(sc.getEstimacion().doubleValue()) != 0){				
				ContabilidadSiniestroDTO contabilidad = new ContabilidadSiniestroDTO();
				ContabilidadSiniestroId contabilidadId = new ContabilidadSiniestroId();
				String conceptoPoliza = 
					getConceptoPoliza(numeroReporte, poliza.getNumeroPoliza());
				
				contabilidadId.setIdToReporteSiniestro(sc.getIdToReporteSiniestro());
				contabilidadId.setIdTcMovimientoSiniestro(sc.getIdTcMovimientoSiniestro());
				contabilidadId.setIdRegistro(sc.getIdRegistro());			
				contabilidadId.setIdSubr(sc.getCodigoSubRamo());
				contabilidadId.setCveConceptoMov(sc.getCveConceptoMov());
				
				contabilidad.setId(contabilidadId);
				contabilidad.setIdRamo(sc.getCodigoRamo());
				//contabilidad.setConceptoPol("Siniestro" + numeroReporte); 
				contabilidad.setConceptoPol(conceptoPoliza);
				contabilidad.setIdMoneda(idMoneda); 
				contabilidad.setTipoCambio(tipoCambio); 
				
				//contabilidad.setCveConceptoMov(conceptoPoliza);
				contabilidad.setAuxiliar(new Integer(1000)); // constante
				contabilidad.setCCosto(CENTRO_COSTOS_CONTABILIZAR_SINIESTRO); // constante
				contabilidad.setTipoPersona(" "); // este no se pasa de acuerdo a la especificacion original
				contabilidad.setImpNeto(Math.abs(sc.getEstimacion()));
				contabilidad.setImpBonificacion(new Double(0)); // constante
				contabilidad.setImpDescuento(new Double(0)); // constante
				contabilidad.setImpDerechos(new Double(0)); // constante
				contabilidad.setImpRecargos(new Double(0));// constante
				contabilidad.setImpIva(new Double(0));// constante
				contabilidad.setImpComision(new Double(0)); // constante
				contabilidad.setImpGastos(new Double(0)); // constante
				contabilidad.setImpOtrosConc(new Double(0)); //constante
				contabilidad.setImpOtrosImp(new Double(0)); // constante
				contabilidad.setImpIvaRet(new Double(0));
				contabilidad.setImpIsrRet(new Double(0));
				String conceptoMov = "Movimiento "+ fecha[2] +"/"+numeroReporte+"/"+sc.getCodigoSubRamo()+"/"+sc.getIdTcMovimientoSiniestro()+"/"+sc.getCveConceptoMov();
				contabilidad.setConceptoMov(conceptoMov);
				contabilidad.setUsuario(usuario); 
				contabilidad.setIdAgente(poliza.getCodigoAgente());
				
				coberturasPorContabilizar.add(contabilidad);
				
				LogDeMidasWeb.log(contabilidad.toString(), Level.INFO, null);
			}							
			
			LogDeMidasWeb.log(sc.toString(), Level.INFO, null);
		}
		
		return coberturasPorContabilizar;		
	}
	
	private void registrarMovimientosReserva(List<ContabilidadSiniestroDTO> movimientosReserva) throws SystemException{
		ContabilidadSiniestroDN contabilidadSiniestroDN = ContabilidadSiniestroDN.getInstancia();
		
		for(ContabilidadSiniestroDTO movimiento : movimientosReserva){
			contabilidadSiniestroDN.guardar(movimiento);
		}		
	}
		
	private List<AsientoContableDTO> contabilizar(String idToReporteSiniestro, String usuario) throws SystemException{
		List<AsientoContableDTO> asientosContables = null;
		
		AsientoContableDN asientoContableDN = AsientoContableDN.getInstancia(usuario);
//		SinietroServiciosDN sinietroServiciosDN = SinietroServiciosDN.getInstancia(usuario);
//		asientosContables = sinietroServiciosDN.contabilizaMovimientos(idToReporteSiniestro, Sistema.SINIESTRO);
		asientosContables = asientoContableDN.contabilizaMovimientos(idToReporteSiniestro, Sistema.SINIESTRO);
		return asientosContables;
	}

	private Double getTipoCambioActual(String usuario, Short idMoneda){
		Double tipoCambio = null;
		
		Date now = Calendar.getInstance().getTime();
				
		TipoCambioDN tipoCambioDN = TipoCambioDN.getInstancia(usuario);
		tipoCambio = tipoCambioDN.obtieneTipoCambioPorDia(now, idMoneda);
		
		return tipoCambio;
	}
	
	private PolizaSoporteDanosDTO getPolizaPorNumeroPoliza(BigDecimal numeroPoliza) throws ExcepcionDeAccesoADatos, SystemException{
		PolizaSoporteDanosDTO poliza = null;
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		poliza = soporteDanosDN.getDatosGeneralesPoliza(numeroPoliza);
		
		return poliza;
	}
	
	private void actualizarMovimientosContabilizados(List<ContabilidadSiniestroDTO> movimientos, List<AsientoContableDTO> asientosContabilizados) throws SystemException{
		if((asientosContabilizados != null) && (asientosContabilizados.size() > 0)){			
			for(ContabilidadSiniestroDTO movimiento : movimientos){									
				actualizarMovimiento(movimiento.getId());
				LogDeMidasWeb.log(movimiento.toString(), Level.INFO, null);
			}										
		}
	}
	
	private void actualizarMovimiento(ContabilidadSiniestroId contabilidadSiniestroId) throws SystemException{
		ContabilidadSiniestroDN contabilidadSiniestroDN = ContabilidadSiniestroDN.getInstancia();
		ContabilidadSiniestroDTO contabilidadSiniestroDTO = contabilidadSiniestroDN.findById(contabilidadSiniestroId);
		if(contabilidadSiniestroDTO != null){
			contabilidadSiniestroDTO.setIdPolizaContable(new BigDecimal(ContabilidadSiniestroDTO.ESTATUS_CONTABILIZADO));
			contabilidadSiniestroDN.actualizar(contabilidadSiniestroDTO);
		}
	}
	
	private String getIdentificadorContable(ContabilidadSiniestroDTO contabilidadSiniestroDTO){
		ContabilidadSiniestroId id = contabilidadSiniestroDTO.getId();
		
		StringBuilder sb = new StringBuilder();
		sb.append(id.getIdToReporteSiniestro().toString());
		sb.append("|");
		sb.append(id.getIdTcMovimientoSiniestro().toString());
		sb.append("|");
		sb.append(id.getIdRegistro());
		sb.append("|");
		sb.append(id.getCveConceptoMov());
		
		return sb.toString();
	}
	
	private String getConceptoPoliza(String numeroReporte, String numeroPoliza){
		StringBuilder sb = new StringBuilder();
		sb.append("Siniestro ");
		sb.append(numeroReporte);
		sb.append("/P-");
		sb.append(numeroPoliza);
		
		return sb.toString();		
	}
	
	
	public void contabilizarIngreso(IngresoSiniestroDTO ingresoSiniestroDTO,BigDecimal idToAplicacionIngreso, List<SoporteContabilidadOrdenPagoDTO> riesgosAfectadosAgrupados, String usuario, String conceptoMovimiento,BigDecimal idTcMovimientoSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		//BigDecimal idToReporteSiniestro = reservaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		String identificadorContable = "";
		
		List<ContabilidadSiniestroDTO> movimientos =this.getMovimientosIngresosPorContabilizar(ingresoSiniestroDTO,idToAplicacionIngreso, riesgosAfectadosAgrupados, usuario, conceptoMovimiento, idTcMovimientoSiniestro); 
		
		registrarMovimientosReserva(movimientos);
		
		identificadorContable = getIdentificadorContable(movimientos.get(0));
		
		List<AsientoContableDTO> asientosContables = contabilizar(identificadorContable, usuario);	
		
		actualizarMovimientosContabilizados(movimientos, asientosContables);
	}
	
	private List<ContabilidadSiniestroDTO> getMovimientosIngresosPorContabilizar(IngresoSiniestroDTO ingresoSiniestroDTO, BigDecimal idToAplicacionIngreso, List<SoporteContabilidadOrdenPagoDTO> riesgosAfectadosAgrupados, String usuario, String conceptoMovimiento,BigDecimal idTcMovimientoSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		List<ContabilidadSiniestroDTO> coberturasPorContabilizar = new ArrayList<ContabilidadSiniestroDTO>();
		SoporteContabilidad soporteContabilidad = SoporteContabilidad.getInstancia();		
		
		BigDecimal numeroPoliza = ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza();	
		String numeroReporte = ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte();
		
		PolizaSoporteDanosDTO poliza = getPolizaPorNumeroPoliza(numeroPoliza);		
		BigDecimal idMoneda = poliza.getIdMoneda();	
		String fechaAplico = UtileriasWeb.getFechaString(Calendar.getInstance().getTime());
		String[] fecha = fechaAplico.split("/");
		
		Double tipoCambio = getTipoCambioActual(usuario, (short)idMoneda.intValue());
		Double totalSumaAsegurada = soporteContabilidad.getSumaAseguradaPorReporte(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
		
		for(SoporteContabilidadOrdenPagoDTO soporteContabilidadSiniestroDTO :  riesgosAfectadosAgrupados){
			
				ContabilidadSiniestroDTO contabilidad = new ContabilidadSiniestroDTO();
				ContabilidadSiniestroId contabilidadId = new ContabilidadSiniestroId();
				String conceptoPoliza =  getConceptoPoliza(numeroReporte, poliza.getNumeroPoliza());
				
				contabilidadId.setIdToReporteSiniestro(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
				contabilidadId.setIdTcMovimientoSiniestro(idTcMovimientoSiniestro);
				contabilidadId.setIdRegistro(idToAplicacionIngreso);			
				contabilidadId.setIdSubr(soporteContabilidadSiniestroDTO.getCodigoSubRamo());
				contabilidadId.setCveConceptoMov(conceptoMovimiento);
				
				contabilidad.setId(contabilidadId);
				contabilidad.setIdRamo(soporteContabilidadSiniestroDTO.getCodigoRamo());
				//contabilidad.setConceptoPol("Siniestro" + numeroReporte); 
				contabilidad.setConceptoPol(conceptoPoliza);
				contabilidad.setIdMoneda(idMoneda); 
				contabilidad.setTipoCambio(tipoCambio); 
				
				//contabilidad.setCveConceptoMov(conceptoPoliza);
				contabilidad.setAuxiliar(new Integer(1000)); // constante
				contabilidad.setCCosto(CENTRO_COSTOS_CONTABILIZAR_SINIESTRO); // constante
				contabilidad.setTipoPersona(" "); // este no se pasa de acuerdo a la especificacion original
				double porcentajeSumaAsegurada = soporteContabilidadSiniestroDTO.getMontoSumaAsegurada() / totalSumaAsegurada.doubleValue();
				
				if(ingresoSiniestroDTO.getMonto() != null){
					contabilidad.setImpNeto(ingresoSiniestroDTO.getMonto() * porcentajeSumaAsegurada);
				}else{
					contabilidad.setImpNeto(new Double(0));
				}
				contabilidad.setImpBonificacion(new Double(0)); // constante
				contabilidad.setImpDescuento(new Double(0)); // constante
				contabilidad.setImpDerechos(new Double(0)); // constante
				contabilidad.setImpRecargos(new Double(0));// constante
				if(ingresoSiniestroDTO.getMontoIVA() != null){
					contabilidad.setImpIva(ingresoSiniestroDTO.getMontoIVA() * porcentajeSumaAsegurada);
				}else{
					contabilidad.setImpIva(new Double(0));
				}
				contabilidad.setImpComision(new Double(0)); // constante
				contabilidad.setImpGastos(new Double(0)); // constante
				contabilidad.setImpOtrosConc(new Double(0)); //constante
				if(ingresoSiniestroDTO.getMontoOtros() != null){
					contabilidad.setImpOtrosImp(ingresoSiniestroDTO.getMontoOtros() * porcentajeSumaAsegurada);
				}else{
					contabilidad.setImpOtrosImp(new Double(0));
				}
				if(ingresoSiniestroDTO.getMontoIVARetencion() != null){
					contabilidad.setImpIvaRet(ingresoSiniestroDTO.getMontoIVARetencion() * porcentajeSumaAsegurada);
				}else{
					contabilidad.setImpIvaRet(new Double(0));
				}
				if(ingresoSiniestroDTO.getMontoISRRetencion() != null){
					contabilidad.setImpIsrRet(ingresoSiniestroDTO.getMontoISRRetencion() * porcentajeSumaAsegurada);
				}else{
					contabilidad.setImpIsrRet(new Double(0));
				}
				String conceptoMov = "Movimiento "+fecha[2]+"/"+numeroReporte+"/"+soporteContabilidadSiniestroDTO.getCodigoSubRamo()+"/"+idTcMovimientoSiniestro+"/"+conceptoMovimiento;
				contabilidad.setConceptoMov(conceptoMov);
				contabilidad.setUsuario(usuario); 
				contabilidad.setIdAgente(poliza.getCodigoAgente());
				
				coberturasPorContabilizar.add(contabilidad);
				
				LogDeMidasWeb.log(contabilidad.toString(), Level.INFO, null);
			}							
			
		return coberturasPorContabilizar;		
	}
}