package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SoporteContabilidad {
	public static final int AUXULIAR = 1000;
	public static final String CENTRO_COSTOS = "1.101"; 
	
	private static SoporteContabilidad INSTANCIA = new SoporteContabilidad();
	
	private SoporteContabilidad(){		
	}
	
	public static SoporteContabilidad getInstancia(){
		return INSTANCIA;
	}
	
	public List<SoporteContabilidadOrdenPagoDTO> agruparCoberturasPorSubramo(BigDecimal idReporteSiniestro, List<RiesgoAfectadoDTO> coberturasRiesgo) throws ExcepcionDeAccesoADatos, SystemException{
		List<SoporteContabilidadOrdenPagoDTO> coberturasAgrupadas = new ArrayList<SoporteContabilidadOrdenPagoDTO>();
		
		Map<BigDecimal, SoporteContabilidadOrdenPagoDTO> agrupadorCoberturas = new HashMap<BigDecimal, SoporteContabilidadOrdenPagoDTO>();
		agrupadorCoberturas.clear();
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		
		for(RiesgoAfectadoDTO coberturaRiesgo : coberturasRiesgo){							
			CoberturaSoporteDanosDTO coberturaSoporteDTO = soporteDanosDN.getCoberturaSoporte(coberturaRiesgo.getId().getIdtocobertura());			 
			
			if(coberturaSoporteDTO != null){				
				BigDecimal idSubRamo = coberturaSoporteDTO.getIdTcSubRamo();
				SoporteContabilidadOrdenPagoDTO soporteContabilidadOrdenPago = null;
				
				if(agrupadorCoberturas.containsKey(idSubRamo)){
					soporteContabilidadOrdenPago = agrupadorCoberturas.get(idSubRamo);
					
					double montoSumaAsegurada =  soporteContabilidadOrdenPago.getMontoSumaAsegurada();
					montoSumaAsegurada += coberturaRiesgo.getSumaAsegurada();					
					soporteContabilidadOrdenPago.setMontoSumaAsegurada(montoSumaAsegurada);					
				}else{
					soporteContabilidadOrdenPago = new SoporteContabilidadOrdenPagoDTO();				
					soporteContabilidadOrdenPago.setIdRamo(coberturaSoporteDTO.getIdTcRamo());
					soporteContabilidadOrdenPago.setCodigoRamo(coberturaSoporteDTO.getCodigoRamo());
					soporteContabilidadOrdenPago.setIdSubRamo(idSubRamo);
					soporteContabilidadOrdenPago.setCodigoSubRamo(coberturaSoporteDTO.getCodigoSubRamo());								
					soporteContabilidadOrdenPago.setMontoSumaAsegurada(coberturaRiesgo.getSumaAsegurada());
					soporteContabilidadOrdenPago.setIdPoliza(coberturaRiesgo.getId().getIdtopoliza());
					
					agrupadorCoberturas.put(idSubRamo, soporteContabilidadOrdenPago);
				}
				
				LogDeMidasWeb.log(soporteContabilidadOrdenPago.toString(), Level.INFO, null);															
			}																					
		}			
		
		if(!agrupadorCoberturas.isEmpty()){
			coberturasAgrupadas.addAll(agrupadorCoberturas.values());
		}
		
		return coberturasAgrupadas;		
	}	
	
	public List<RiesgoAfectadoDTO> obtenerCoberturasRiesgo(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		return riesgoAfectadoDN.listarPorReporteSiniestro(idToReporteSiniestro);
	}
	
	public Double getSumaAseguradaPorReporte(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		
		Double totalSumaAsegurada = riesgoAfectadoDN.sumaAseguradaPorReporte(idReporteSiniestro);
		
		return totalSumaAsegurada;
	}

	public PolizaSoporteDanosDTO getPolizaPorId(BigDecimal idPoliza) throws ExcepcionDeAccesoADatos, SystemException{
		PolizaSoporteDanosDTO poliza = null;
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		poliza = soporteDanosDN.getDatosGeneralesPoliza(idPoliza);
		
		return poliza;
	}
	
	public Double getTipoCambio(short idMoneda, String usuario){
		Double tipoCambio = null;
		
		Date now = Calendar.getInstance().getTime();
				
		TipoCambioDN tipoCambioDN = TipoCambioDN.getInstancia(usuario);
		tipoCambio = tipoCambioDN.obtieneTipoCambioPorDia(now, idMoneda);
		
		return tipoCambio;
	}		
}