/**
 * 
 */
package mx.com.afirme.midas.siniestro.finanzas;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author user
 *
 */
public class ModificarReservaForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;

	private String tipoMoneda;
	private List listaSumaAsegurada;
	private String totalSumaAsegurada;
	private List listaReservaActual;
	private String totalReservaActual;
	private List listaNuevaReserva;
	private String totalNuevaReserva;
	private String descripcionAjuste;
	
	public ModificarReservaForm() {
	}

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}

	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	/**
	 * @return the listaSumaAsegurada
	 */
	public List getListaSumaAsegurada() {
		return listaSumaAsegurada;
	}

	/**
	 * @param listaSumaAsegurada the listaSumaAsegurada to set
	 */
	public void setListaSumaAsegurada(List listaSumaAsegurada) {
		this.listaSumaAsegurada = listaSumaAsegurada;
	}

	/**
	 * @return the totalSumaAsegurada
	 */
	public String getTotalSumaAsegurada() {
		return totalSumaAsegurada;
	}

	/**
	 * @param totalSumaAsegurada the totalSumaAsegurada to set
	 */
	public void setTotalSumaAsegurada(String totalSumaAsegurada) {
		this.totalSumaAsegurada = totalSumaAsegurada;
	}

	/**
	 * @return the listaReservaActual
	 */
	public List getListaReservaActual() {
		return listaReservaActual;
	}

	/**
	 * @param listaReservaActual the listaReservaActual to set
	 */
	public void setListaReservaActual(List listaReservaActual) {
		this.listaReservaActual = listaReservaActual;
	}

	/**
	 * @return the totalReservaActual
	 */
	public String getTotalReservaActual() {
		return totalReservaActual;
	}

	/**
	 * @param totalReservaActual the totalReservaActual to set
	 */
	public void setTotalReservaActual(String totalReservaActual) {
		this.totalReservaActual = totalReservaActual;
	}

	/**
	 * @return the listaNuevaReserva
	 */
	public List getListaNuevaReserva() {
		return listaNuevaReserva;
	}

	/**
	 * @param listaNuevaReserva the listaNuevaReserva to set
	 */
	public void setListaNuevaReserva(List listaNuevaReserva) {
		this.listaNuevaReserva = listaNuevaReserva;
	}

	/**
	 * @return the totalNuevaReserva
	 */
	public String getTotalNuevaReserva() {
		return totalNuevaReserva;
	}

	/**
	 * @param totalNuevaReserva the totalNuevaReserva to set
	 */
	public void setTotalNuevaReserva(String totalNuevaReserva) {
		this.totalNuevaReserva = totalNuevaReserva;
	}

	/**
	 * @return the descripcionAjuste
	 */
	public String getDescripcionAjuste() {
		return descripcionAjuste;
	}

	/**
	 * @param descripcionAjuste the descripcionAjuste to set
	 */
	public void setDescripcionAjuste(String descripcionAjuste) {
		this.descripcionAjuste = descripcionAjuste;
	}

	
}
