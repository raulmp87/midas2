package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoId;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Usuario;


public class ReservaAction extends MidasMappingDispatchAction {

	public ActionForward estimarReserva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		List <ReservaDetalleBean> reservaDetalleBeanList = new ArrayList<ReservaDetalleBean>();
		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		ReservaForm forma = (ReservaForm) form;
//		BigDecimal idReporteSiniestro = idReporteSiniestro new BigDecimal(383);
		BigDecimal idToReporteSiniestro = request.getParameter("idToReporteSiniestro")!= null?new BigDecimal(request.getParameter("idToReporteSiniestro")):new BigDecimal(0);

		try {
			RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
			riesgoAfectadoId.setIdtoreportesiniestro(idToReporteSiniestro);
			ReservaDN reservaDN = ReservaDN.getInstancia();
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			List<RiesgoAfectadoDTO> listaCoberturasRiesgo = reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
			reservaDetalleBeanList = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(listaCoberturasRiesgo);
			forma.setListaSumaAseguradaCobertura(reservaDetalleBeanList);
			forma.setListaEstimacionInicialCobertura(reservaDetalleBeanList);
			//forma.setListaSumaAseguradaCobertura(listaCoberturasRiesgo);
			//forma.setListaEstimacionInicialCobertura(listaCoberturasRiesgo);
			String sumaAsegurada = reservaDN.realizaSumaAsegurada(reservaDetalleBeanList);
			forma.setTotalSumaAsegurada(sumaAsegurada);
			forma.setTotalSumaAseguradaFormato(UtileriasWeb.formatoMoneda(new Double(sumaAsegurada)));
			forma.setTotalEstimacionInicial("0");
			forma.setIdToReporteSiniestro(idToReporteSiniestro.toString());
			
		
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(idToReporteSiniestro);
			forma.setTipoMoneda(tipoMoneda);

		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}

		return mapping.findForward(reglaNavegacion);
	}	
	
	public ActionForward guardarEstimacion(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		ReservaForm forma = (ReservaForm) form;

		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDTO reservaDTO = new ReservaDTO();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);

		try {

			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN
					.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN
					.desplegarReporte(new BigDecimal(forma
							.getIdToReporteSiniestro()));
			reservaDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
			if(forma.getDescripcionEstimacion() != null){
            	String descripcion = forma.getDescripcionEstimacion();
    			if(descripcion.length() > 240){
    				reservaDTO.setDescripcion( descripcion.substring(0, 239) );
    			}else{
    				reservaDTO.setDescripcion(descripcion);
    			}	
            }
			reservaDTO.setTipoajuste(new BigDecimal(0));
			Date fecha = Calendar.getInstance().getTime();
			reservaDTO.setFechaestimacion(fecha);
			reservaDTO.setIdtousuariocreacion(BigDecimal.valueOf(usuario
					.getId().intValue()));
			reservaDTO.setAutorizada(ReservaDTO.NO_AUTORIZADA);
			RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
			riesgoAfectadoId.setIdtoreportesiniestro(new BigDecimal(forma
					.getIdToReporteSiniestro()));
			List<RiesgoAfectadoDTO> listaCoberturasRiesgo = reservaDN
					.listarCoberturasRiesgo(riesgoAfectadoId);

			List<ReservaDetalleDTO> listaReservaDetalle = reservaDN
					.generaListaDetalle(forma, listaCoberturasRiesgo,
							reservaDTO);
			List<ReservaDetalleBean> reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(listaReservaDetalle,true);
			String mensajeError = reservaDN.validaSumaReservaParaRiesgos(reservaDetalleList);
			
			if (!UtileriasWeb.esCadenaVacia(mensajeError)){
				UtileriasWeb.imprimeMensajeXML("0", mensajeError, response);
				return mapping.findForward(reglaNavegacion);
			}
			
			reservaDTO = reservaDN.agregarReserva(reservaDTO);
			for (ReservaDetalleDTO reservaDetalleDTO: listaReservaDetalle){
				reservaDetalleDTO.getId().setIdtoreservaestimada(reservaDTO.getIdtoreservaestimada());
				reservaDetalleDTO.setReservaDTO(reservaDTO);
				reservaDetalleDTO.setEstatus(ReservaDetalleDTO.ACTIVO);
			}
			reservaDTO.setReservaDetalleDTOs(listaReservaDetalle);
			reservaDN.actualizarReserva(reservaDTO);

			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO
					.getReporteEstatus();
			reporteEstatusDTO
					.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AUTORIZAR_RESERVA);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			
			UtileriasWeb.imprimeMensajeXML("1", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar") , response);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
			e.printStackTrace();
		} catch (SystemException e) {
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
			e.printStackTrace();
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward mostrarReservaAutorizar(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ReservaDTO reservaActualDTO = null;
		ReservaDTO reservaAutorizarDTO = null;
		String descripcionTipoAjuste = "";

		String strReporteSiniestro = request.getParameter("idToReporteSiniestro");
		BigDecimal idToReporteSiniestro = new BigDecimal(strReporteSiniestro);
		ReservaForm forma = (ReservaForm) form;
		
		forma.setIdToReporteSiniestro(strReporteSiniestro);

		RiesgoAfectadoId riesgoAfectado = new RiesgoAfectadoId();
		riesgoAfectado.setIdtoreportesiniestro(idToReporteSiniestro);
		
		try {
			List<RiesgoAfectadoDTO> coberturasRiesgo = reservaDN.listarCoberturasRiesgo(riesgoAfectado);
			List<ReservaDetalleBean> reservaDetalleLista = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(coberturasRiesgo); 
			forma.setListaSumaAseguradaCobertura(reservaDetalleLista);
			//forma.setListaSumaAseguradaCobertura(coberturasRiesgo);

			String totalSumaAsegurada = reservaDN.realizaSumaAsegurada(reservaDetalleLista);
//			forma.setTotalSumaAsegurada(totalSumaAsegurada);
			forma.setTotalSumaAseguradaFormato(UtileriasWeb.formatoMoneda(new Double(totalSumaAsegurada)));
			reservaActualDTO = reservaDN.listarReservaActual(idToReporteSiniestro);
			reservaAutorizarDTO = reservaDN.listarReservaPorAutorizar(idToReporteSiniestro);
			
			if(reservaActualDTO == null){
//				List<ReservaDetalleBean> listaReservaDetalle = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaAutorizarDTO.getReservaDetalleDTOs());
				List<ReservaDetalleBean> listaReservaDetalle = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalleDN.listarDetalleReserva(reservaAutorizarDTO.getIdtoreservaestimada()));
				forma.setListaEstimacionInicialCobertura(listaReservaDetalle);
				//forma.setListaEstimacionInicialCobertura(reservaAutorizarDTO.getReservaDetalleDTOs());
				forma.setTotalEstimacionInicial(forma.getTotalReservaAutorizar());
				
				Double totalReserva = reservaDN.obtenerTotalReserva(reservaDetalleDN.listarDetalleReserva(reservaAutorizarDTO.getIdtoreservaestimada()));
//				forma.setTotalEstimacionInicial(String.valueOf(totalReserva));
				forma.setTotalEstimacionInicialFormato(UtileriasWeb.formatoMoneda(new Double(0)));
				
				forma.setListaReservaPorAutorizar(listaReservaDetalle);
				//forma.setListaReservaPorAutorizar(reservaAutorizarDTO.getReservaDetalleDTOs());
				forma.setTotalReservaAutorizar(String.valueOf(totalReserva));				
				forma.setTotalReservaAutorizarFormato(UtileriasWeb.formatoMoneda(totalReserva));
				forma.setIdentificador("inicial");
			}else{
//				forma.setListaEstimacionInicialCobertura(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaActualDTO.getReservaDetalleDTOs()));
				forma.setListaEstimacionInicialCobertura(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalleDN.listarDetalleReserva(reservaActualDTO.getIdtoreservaestimada())));
				Double totalReserva = reservaDN.obtenerTotalReserva(reservaDetalleDN.listarDetalleReserva(reservaActualDTO.getIdtoreservaestimada()));
//				forma.setTotalEstimacionInicial(String.valueOf(totalReserva));
				forma.setTotalEstimacionInicialFormato(UtileriasWeb.formatoMoneda(totalReserva));
				
				if(reservaAutorizarDTO != null){
//					forma.setListaReservaPorAutorizar(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaAutorizarDTO.getReservaDetalleDTOs()));
					forma.setListaReservaPorAutorizar(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalleDN.listarDetalleReserva(reservaAutorizarDTO.getIdtoreservaestimada())));
					totalReserva = reservaDN.obtenerTotalReserva(reservaDetalleDN.listarDetalleReserva(reservaAutorizarDTO.getIdtoreservaestimada()) );
					forma.setTotalReservaAutorizar(String.valueOf(totalReserva));
					forma.setTotalReservaAutorizarFormato(UtileriasWeb.formatoMoneda(totalReserva));
				}				
			}
			
			switch (reservaAutorizarDTO.getTipoajuste().intValue()) {
				case 0: {
					descripcionTipoAjuste = 
						UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.finanzas.autorizarReserva.ajuste.estimacionInicial"); 
					break;
				}
				case 1: {
					descripcionTipoAjuste = 
						UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.finanzas.autorizarReserva.ajuste.ajusteDeMas"); 
					break;
				}
				case 2: {
					descripcionTipoAjuste = 
						UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.finanzas.autorizarReserva.ajuste.ajusteDeMenos"); 
					break;
				}
			}
			
			forma.setTipoAjuste(descripcionTipoAjuste);
			forma.setDescripcionEstimacion(reservaAutorizarDTO.getDescripcion());
			forma.setIdToReservaEstimada(reservaAutorizarDTO.getIdtoreservaestimada().intValue());
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(idToReporteSiniestro);
			forma.setTipoMoneda(tipoMoneda);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward autorizarReserva(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		ReservaForm forma = (ReservaForm) form;
		
		int idToReservaEstimada = forma.getIdToReservaEstimada();
		String strIdReporteSiniestro = forma.getIdToReporteSiniestro();
		String strTotalReservaAutorizar = forma.getTotalReservaAutorizar();

		BigDecimal idToReservaEstimanda = new BigDecimal(idToReservaEstimada);

		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDTO reservaDTO = null;

		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(
				request, Sistema.USUARIO_ACCESO_MIDAS);
		
		String userName = UtileriasWeb.obtieneNombreUsuario(request);
		
		Date fecha = Calendar.getInstance().getTime();		
		try {
			reservaDTO = reservaDN.findById(idToReservaEstimanda);
			reservaDTO.setAutorizada(true);
			reservaDTO.setFechaautorizacion(fecha);
			reservaDTO.setIdtousuarioautoriza(new BigDecimal(usuario.getId()));
			reservaDN.actualizarReserva(reservaDTO);
			
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(new BigDecimal(forma.getIdToReporteSiniestro()));
			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_EVALUACION_INFORME_FINAL);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);		
			try{
				this.contabilizarReserva(reservaDTO, userName);
			}catch (SystemException ex) {
				LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
				ex.printStackTrace();
			}
			this.distribuirReserva(reservaDTO,userName);
		} catch (ExcepcionDeAccesoADatos ex) {
			LogDeMidasWeb.log("Excepcion capa Web de tipo " + 
					ex.getClass().getCanonicalName(), Level.SEVERE, ex);
			ex.printStackTrace();			
		} catch (SystemException ex) {
			LogDeMidasWeb.log("Excepcion capa Web de tipo " + 
					ex.getClass().getCanonicalName(), Level.SEVERE, ex);
			ex.printStackTrace();			
		} catch (Exception ex){
			LogDeMidasWeb.log("Excepcion capa Web de tipo " + 
					ex.getClass().getCanonicalName(), Level.SEVERE, ex);
			ex.printStackTrace();
		}

		Double totalReservaAutorizar = Double.parseDouble(strTotalReservaAutorizar);
		//TODO: Constante que debe ser remplazada por un parametro
		if (totalReservaAutorizar.intValue() > 300000) {
			enviarNotificacionAutorizacionReserva(strIdReporteSiniestro, strTotalReservaAutorizar);
		}
						
		return mapping.findForward(reglaNavegacion);
	}
	
	private void enviarNotificacionAutorizacionReserva(String IdReporteSiniestro, String idToReservaEstimanda){
		List<String> destinatarios = new ArrayList<String>();
//		destinatarios.add("jamatitla@gmail.com");
		destinatarios.add("jorgelsierras@gmail.com");
		
		String titulo = "Autorizacion de Reserva";		
		String body01 = "Se ha autorizado la reserva para el reporte n&uacute;mero <b>" + IdReporteSiniestro;
		String body02 = "</b> por la cantidad de <b>" + idToReservaEstimanda + "</b>";
		
		StringBuilder contenido = new StringBuilder();
		contenido.append("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'> ");
		contenido.append("<html>");
		contenido.append("<head>");
		contenido.append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
		contenido.append("</head>");
		contenido.append("<body>");
		contenido.append(body01);
		contenido.append(body02);		
		contenido.append("</body>");
		contenido.append("</html>");
				
		MailAction.enviaCorreo(destinatarios, titulo, contenido.toString());		
	}
	
	public ActionForward listarHistorial(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		//Se asume que la operacion sera exitosa
		String reglaNavegacion = Sistema.EXITOSO;
		//Se obtiene la forma
		HistorialReservaForm forma = (HistorialReservaForm) form;
		//Se obtiene el delegado de negocios
		ReservaDN  reservaDN = ReservaDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		//Se revisa el reporte para el que se va a realizar la consulta
//		String idToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		String pantallaOrigen = request.getParameter("pantallaOrigen");
		request.setAttribute("pantallaOrigen", pantallaOrigen);
		String idToReporteSiniestro = forma.getIdToReporteSiniestro();
		try {
			//Se pone el locale para los formatos
			//HttpSession sesion = request.getSession();
//			sesion.setAttribute("localeMexico", new Locale("es", "MX"));
			//Se llena la forma
			RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
			forma.setSumaAsegurada(riesgoAfectadoDN.sumaAseguradaPorReporte(UtileriasWeb.regresaBigDecimal(idToReporteSiniestro)));
//			forma.setTipoMoneda("MXP");
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(idToReporteSiniestro));
			forma.setTipoMoneda(tipoMoneda);
			List<RegistroHistorialReserva> listadoRegistrosReserva = reservaDN.listarRegistrosReserva(UtileriasWeb.regresaBigDecimal(idToReporteSiniestro));
			forma.setModificacionesReserva(listadoRegistrosReserva);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarModificarReserva(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		ReservaForm forma = (ReservaForm) form;
		BigDecimal idToReporteSiniestro= new BigDecimal(request.getParameter("idToReporteSiniestro"));
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		
		try {
			RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
			riesgoAfectadoId.setIdtoreportesiniestro(idToReporteSiniestro);
			ReservaDN  reservaDN = ReservaDN.getInstancia();
			ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
			
			List<RiesgoAfectadoDTO> listaCoberturasRiesgo= reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
			List<ReservaDetalleBean> reservaDetalleLista = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(listaCoberturasRiesgo);
			String strSumaAsegurada = reservaDN.realizaSumaAsegurada(reservaDetalleLista);
			
			forma.setListaSumaAseguradaCobertura(reservaDetalleLista);			
			forma.setTotalSumaAsegurada(strSumaAsegurada);
			forma.setTotalSumaAseguradaFormato(UtileriasWeb.formatoMoneda(new Double(strSumaAsegurada)));
			
			forma.setIdToReporteSiniestro(idToReporteSiniestro.toString());
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(idToReporteSiniestro);
			forma.setTipoMoneda(tipoMoneda);
			
			List<ReservaDetalleDTO> reservaDetalle = null;
			List<ReservaDetalleBean> reservaDetalleBean = null; 

			// Traer la ultima reserva autorizada
			ReservaDTO reservaActual = reservaDN.listarReservaActual(idToReporteSiniestro);
			if(reservaActual != null){
				forma.setListaEstimacionInicialCobertura(null);
				 
				ReservaDTO reservaPorAutorizar = reservaDN.listarReservaPorAutorizar(idToReporteSiniestro);
				// Si ya hay una reserva que no esta aurtorizada
				if(reservaPorAutorizar != null){
					// Datos de la reserva autorizada
					reservaDetalle = reservaDetalleDN.listarDetalleReserva(reservaActual.getIdtoreservaestimada());
					reservaDetalleBean = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalle);
										
					forma.setListaEstimacionInicialCobertura(reservaDetalleBean);

					String totalReservaActual = String.valueOf(reservaDN.obtenerTotalReserva(reservaDetalle));					
					forma.setTotalEstimacionInicial(totalReservaActual);
					forma.setTotalEstimacionInicialFormato(UtileriasWeb.formatoMoneda(new Double(totalReservaActual)));										
					
					//Datos de la reserva por autorizar
					reservaDetalle = reservaDetalleDN.listarDetalleReserva(reservaPorAutorizar.getIdtoreservaestimada());
					reservaDetalle = reservaDN.crearReservaDetalleDeRiesgosAfectados(reservaDetalle, listaCoberturasRiesgo);
					
					String totalReservaPorAutorizar = String.valueOf(reservaDN.obtenerTotalReserva(reservaDetalle));
					
					forma.setListaReservaPorAutorizar(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalle));
					forma.setTotalReservaAutorizar(UtileriasWeb.formatoMoneda(new Double(totalReservaPorAutorizar)));
					forma.setDescripcionEstimacion(reservaPorAutorizar.getDescripcion());														
				}else{
					// Datos de la reserva autorizada
					reservaDetalle = reservaDetalleDN.listarDetalleReserva(reservaActual.getIdtoreservaestimada());					
					reservaDetalleBean = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalle);					
					
					forma.setListaEstimacionInicialCobertura(reservaDetalleBean);

					String totalReservaActual = String.valueOf(reservaDN.obtenerTotalReserva(reservaDetalle));					
					forma.setTotalEstimacionInicial(totalReservaActual);
					forma.setTotalEstimacionInicialFormato(UtileriasWeb.formatoMoneda(new Double(totalReservaActual)));										
					
					// Datos de la reserva por modificar
					reservaDetalle = reservaDN.crearReservaDetalleDeRiesgosAfectados(reservaDetalle, listaCoberturasRiesgo);
					
					String totalReservaPorAutorizar = String.valueOf(reservaDN.obtenerTotalReserva(reservaDetalle));
					
					forma.setListaReservaPorAutorizar(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalle));
					forma.setTotalReservaAutorizar(UtileriasWeb.formatoMoneda(new Double(totalReservaPorAutorizar)));
					forma.setDescripcionEstimacion(reservaActual.getDescripcion());
				}															
			}else{
				forma.setTotalEstimacionInicialFormato("$0.00");
				
				reservaActual = reservaDN.listarReservaPorAutorizar(idToReporteSiniestro);							
				
				reservaDetalle = reservaDetalleDN.listarDetalleReserva(reservaActual.getIdtoreservaestimada());
				reservaDetalle = reservaDN.crearReservaDetalleDeRiesgosAfectados(reservaDetalle, listaCoberturasRiesgo);
				
				String totalReservaActual = String.valueOf(reservaDN.obtenerTotalReserva(reservaDetalle));
																				
				forma.setListaReservaPorAutorizar(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalle));
				forma.setTotalReservaAutorizar(UtileriasWeb.formatoMoneda(new Double(totalReservaActual)));			
				forma.setDescripcionEstimacion(reservaActual.getDescripcion());							
			}					
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward agregarReserva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {		
		String reglaNavegacion = Sistema.EXITOSO;
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);		
		String userName = UtileriasWeb.obtieneNombreUsuario(request);
		
        ReservaForm forma = (ReservaForm) form;
        ReservaDN reservaDN = ReservaDN.getInstancia();
        ReservaDTO reservaDTO = null;	
        SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
        
        Date fecha = Calendar.getInstance().getTime();
        String strIdToReporteSiniestro = forma.getIdToReporteSiniestro();
        BigDecimal idToReporteSiniestro = new BigDecimal(strIdToReporteSiniestro);
        
        String[] strEstimaciones = forma.getReservas();
        Double[] estimacionesRiesgos = siniestroMovimientosDN.removerFormatoMonedaEstimaciones(strEstimaciones);
        List<RiesgoAfectadoDTO> riesgosAfectados = null;        
        ReporteSiniestroDTO reporteSiniestroDTO = null;
        boolean distribuirReserva = false;
                                
        try{
        	ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
        	ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
        	
        	RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
			riesgoAfectadoId.setIdtoreportesiniestro(idToReporteSiniestro);
        	
			riesgosAfectados = reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
        	reservaDTO = reservaDN.listarReservaActual(idToReporteSiniestro);        	
        	reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
        	
			List<ReservaDetalleDTO> nuevaReserva = null;
			List<ReservaDetalleBean> reservaDetalleList = null;
			List<ReservaDetalleDTO> reservaAutorizada = null;
			List<SoporteMovimientosReservaBean> movimientosReserva = null;			
        	
			if(reservaDTO != null){
				ReservaDTO reservaPorAutorizar = reservaDN.listarReservaPorAutorizar(idToReporteSiniestro);
				if(reservaPorAutorizar != null){
					nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, estimacionesRiesgos);
					reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(nuevaReserva, true);					

					String mensajeError = reservaDN.validaSumaReservaParaRiesgos(reservaDetalleList);
			           
		            if (!UtileriasWeb.esCadenaVacia(mensajeError)){
		                UtileriasWeb.imprimeMensajeXML("0", mensajeError, response);
		                return mapping.findForward(reglaNavegacion);
		            }					
										
					reservaAutorizada = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
					movimientosReserva = reservaDN.obtenerDiferenciasReservas(reservaAutorizada, nuevaReserva);
					BigDecimal tipoAjuste = reservaDN.determinarTipoAjusteNuevaReserva(movimientosReserva);										
																				
					reservaPorAutorizar.setTipoajuste(tipoAjuste);
					reservaPorAutorizar.setFechaestimacion(fecha);
					reservaPorAutorizar.setIdtousuariocreacion(BigDecimal.valueOf(usuario.getId().intValue()));					
		            if(forma.getDescripcionEstimacion() != null){
		            	String descripcion = forma.getDescripcionEstimacion();
		    			if(descripcion.length() > 240){
		    				reservaPorAutorizar.setDescripcion( descripcion.substring(0, 239) );
		    			}else{
		    				reservaPorAutorizar.setDescripcion(descripcion);
		    			}	
		            }						
							           		            
		            //Determinar el pendiente y el estatus de la Reserva
		            ReporteEstatusDTO reporteEstatusDTO = 
		            	reservaDN.determinarPendienteDeModificacionDeReserva(reporteSiniestroDTO, usuario.getNombreUsuario());
		            
		            if(reporteEstatusDTO != null){
		            	reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
		            	reporteSiniestroDN.actualizar(reporteSiniestroDTO);
		            }else{
	            		reservaPorAutorizar.setAutorizada(Boolean.TRUE);
	            		reservaPorAutorizar.setIdtousuarioautoriza(BigDecimal.valueOf(usuario.getId().intValue()));
	            		reservaPorAutorizar.setFechaautorizacion(fecha);
	            		distribuirReserva = true;		            	
		            }
		            
		            
		            reservaDN.actualizarReserva(reservaPorAutorizar);		            		            		            		           
					reservaDN.borrarReservaDetalle(reservaPorAutorizar.getIdtoreservaestimada());
					reservaDN.guardarNuevaReserva(reservaPorAutorizar, movimientosReserva);
					
					if(distribuirReserva){
						try{
							this.contabilizarReserva(reservaPorAutorizar, userName);
						}catch (SystemException ex) {
							LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
							ex.printStackTrace();
						}
						this.distribuirReserva(reservaPorAutorizar,userName);
					}

	                Double totalReservaAutorizar = UtileriasWeb.eliminaFormatoMoneda(forma.getTotalReservaAutorizar());               
	                
	                if (totalReservaAutorizar.intValue() > 300000) {
	                    enviarNotificacionAutorizacionReserva(forma.getIdToReporteSiniestro(), forma.getTotalReservaAutorizar());
	                }
					
				}else{
					nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, estimacionesRiesgos);
					reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(nuevaReserva, true);

					String mensajeError = reservaDN.validaSumaReservaParaRiesgos(reservaDetalleList);
			           
		            if (!UtileriasWeb.esCadenaVacia(mensajeError)){
		                UtileriasWeb.imprimeMensajeXML("0", mensajeError, response);
		                return mapping.findForward(reglaNavegacion);
		            }										
					
					reservaAutorizada = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
					movimientosReserva = reservaDN.obtenerDiferenciasReservas(reservaAutorizada, nuevaReserva);
					BigDecimal tipoAjuste = reservaDN.determinarTipoAjusteNuevaReserva(movimientosReserva);
					
					//Crea la instancia de la reserva				
					ReservaDTO reservaNueva = new ReservaDTO();									
					reservaNueva.setReporteSiniestroDTO(reporteSiniestroDTO);
		            reservaNueva.setTipoajuste(tipoAjuste);
		            reservaNueva.setFechaestimacion(fecha);
		            reservaNueva.setIdtousuariocreacion(BigDecimal.valueOf(usuario.getId().intValue()));
		            reservaNueva.setAutorizada(ReservaDTO.NO_AUTORIZADA);
		            
		            if(forma.getDescripcionEstimacion() != null){
		            	String descripcion = forma.getDescripcionEstimacion();
		    			if(descripcion.length() > 240){
		    				reservaNueva.setDescripcion( descripcion.substring(0, 239) );
		    			}else{
		    				reservaNueva.setDescripcion(descripcion);
		    			}	
		            }
		            
		            //Determinar el pendiente y el estatus de la Reserva
		            ReporteEstatusDTO reporteEstatusDTO = 
		            	reservaDN.determinarPendienteDeModificacionDeReserva(reporteSiniestroDTO, usuario.getNombreUsuario());
		            
		            if(reporteEstatusDTO != null){
		            	reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
		            	reporteSiniestroDN.actualizar(reporteSiniestroDTO);
		            }else{
	            		reservaNueva.setAutorizada(Boolean.TRUE);
	            		reservaNueva.setIdtousuarioautoriza(BigDecimal.valueOf(usuario.getId().intValue()));
	            		reservaNueva.setFechaautorizacion(fecha);
	            		distribuirReserva = true;		            	
		            }
		            			
		            reservaNueva = reservaDN.agregarReserva(reservaNueva);
		            reservaDN.guardarNuevaReserva(reservaNueva, movimientosReserva);
		            
					if(distribuirReserva){
						try{	
							this.contabilizarReserva(reservaNueva, userName);
						}catch (SystemException ex) {
							LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
							ex.printStackTrace();
						}
						this.distribuirReserva(reservaNueva,userName);
					}		            
					
	                Double totalReservaAutorizar = UtileriasWeb.eliminaFormatoMoneda(forma.getTotalReservaAutorizar());               
	                
	                if (totalReservaAutorizar.intValue() > 300000) {
	                    enviarNotificacionAutorizacionReserva(forma.getIdToReporteSiniestro(), forma.getTotalReservaAutorizar());
	                }					
				}
			}else{						
				reservaDTO = reservaDN.listarReservaPorAutorizar(idToReporteSiniestro);
				
				nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, estimacionesRiesgos);				
				reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaReservaDetalle(nuevaReserva, true);

				String mensajeError = reservaDN.validaSumaReservaParaRiesgos(reservaDetalleList);
		           
	            if (!UtileriasWeb.esCadenaVacia(mensajeError)){
	                UtileriasWeb.imprimeMensajeXML("0", mensajeError, response);
	                return mapping.findForward(reglaNavegacion);
	            } 									
				
	            if(forma.getDescripcionEstimacion() != null){
	            	String descripcion = forma.getDescripcionEstimacion();
	    			if(descripcion.length() > 240){
	    				reservaDTO.setDescripcion( descripcion.substring(0, 239) );
	    			}else{
	    				reservaDTO.setDescripcion(descripcion);
	    			}	
	            }
				
	            reservaDTO.setIdtousuariocreacion(BigDecimal.valueOf(usuario.getId().intValue()));
	            reservaDTO.setFechaestimacion(fecha);
												
	            reservaDN.actualizarReserva(reservaDTO);
				reservaDN.borrarReservaDetalle(reservaDTO.getIdtoreservaestimada());												
				reservaDN.guardarReservaDetalle(reservaDTO, nuevaReserva);								
				
				ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AUTORIZAR_RESERVA);
				reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
				reporteSiniestroDN.actualizar(reporteSiniestroDTO);												
			}	                                   
			
            UtileriasWeb.imprimeMensajeXML("1", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar") , response);
        }catch (ExcepcionDeAccesoADatos e) {
            UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
            e.printStackTrace();
        } catch (SystemException e) {
            UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
            e.printStackTrace();
        }
       
        return mapping.findForward(reglaNavegacion);		
	}	
	
	private void distribuirReserva(ReservaDTO reservaDTO,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		IntegracionReaseguroDN integracionReaseguro = IntegracionReaseguroDN.getInstancia();
		integracionReaseguro.distribuirReserva(reservaDTO,nombreUsuario);
	}
	
	private void contabilizarReserva(ReservaDTO reservaDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		ContabilidadSiniestroDN contabilidad = ContabilidadSiniestroDN.getInstancia();
		contabilidad.contabilizarReserva(reservaDTO, usuario);
	}			
}
