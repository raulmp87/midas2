package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReservaDetalleSN {
	
	private ReservaDetalleFacadeRemote beanRemoto;

	public ReservaDetalleSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReservaDetalleFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}	
	
	public List<ReservaDetalleDTO> listarDetalleReserva(BigDecimal idReserva) throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.listarDetalleReserva(idReserva);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Double sumaReserva(BigDecimal idReserva) throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.sumaReserva(idReserva);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public void borrarReservaDetalle(ReservaDetalleDTO entity) {				
		try {
			beanRemoto.delete(entity);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public void guardarReservaDetalle(ReservaDetalleDTO entity) {
		try {
			beanRemoto.save(entity);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}				
	}
	
	public List<ReservaDetalleDTO> listarReservaDetalle(BigDecimal idReserva){
		try {
			return  beanRemoto.listarReservaDetalle(idReserva);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public Double diferenciaDeReserva(BigDecimal idToReporteSiniestro){
		try {
			return  beanRemoto.diferenciaDeReserva(idToReporteSiniestro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	public Double obtenerSumaReservaInicial(BigDecimal idToReporteSiniestro){ 
		try {
			return  beanRemoto.obtenerSumaReservaInicial(idToReporteSiniestro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
}
