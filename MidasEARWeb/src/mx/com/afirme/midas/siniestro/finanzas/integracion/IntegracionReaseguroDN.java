package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IntegracionReaseguroDN {
	private static final IntegracionReaseguroDN INSTANCIA = new IntegracionReaseguroDN();
	
	public static IntegracionReaseguroDN getInstancia(){
		return INSTANCIA;
	}
	
	/**
	 * @throws SystemException 
	 * Realiza la distribucion de reaseguro de los 
	 * movimientos contenidos en la lista movimientosReaseguro 
	 * 
	 * @param List<IntegracionReaseguroDTO> movimientosReaseguro Movimientos a Distribuir
	 * @return null
	 * @throws SystemException
	 */	
	private void distribuirReaseguro(List<IntegracionReaseguroDTO> movimientosReaseguro,String nombreUsuario) throws SystemException{
		DistribucionReaseguroDN  distribucionReaseguro = DistribucionReaseguroDN.getInstancia();
		
		for(IntegracionReaseguroDTO reaseguro : movimientosReaseguro){
			try {
				
				if(Math.round(reaseguro.getMontoSiniestro()) > 0){
					LogDeMidasWeb.log("Se env�an los datos de distribuci�n de siniestro a reaseguro: "+reaseguro.toString(), Level.SEVERE, null);
					
					distribucionReaseguro.distribuirMontoSiniestro(
							reaseguro.getIdToPoliza(),
							reaseguro.getNumeroEndoso(),
							reaseguro.getIdToSeccion(),
							reaseguro.getIdToCobertura(),
							new Integer(reaseguro.getNumeroInciso().intValue()),
							new Integer(reaseguro.getNumeroSubInciso().intValue()),
							reaseguro.getConceptoMovimiento(),
							reaseguro.getIdMoneda(),
							reaseguro.getFechaRegistroMovimiento(),
							reaseguro.getIdMovimientoSiniestro(),
							new BigDecimal(reaseguro.getMontoSiniestro().doubleValue()),
							reaseguro.getTipoMovimiento(),
							reaseguro.getIdToReporteSiniestro(),nombreUsuario);
					
					LogDeMidasWeb.log("Se enviaron los datos de distribuci�n de siniestro a reaseguro: "+reaseguro.toString(), Level.SEVERE, null);
				}
				else{
					LogDeMidasWeb.log("Se omite el env�o de los datos de distribuci�n de siniestro a reaseguro, debido a que el monto es menor o igual a cero. Datos: "+reaseguro.toString(), Level.SEVERE, null);
				}
				
				LogDeMidasWeb.log(reaseguro.toString(), Level.INFO, null);
			} catch (SystemException e) {
				LogDeMidasWeb.log("Ocurri� un error al enviar los datos de distribuci�n de siniestro a reaseguro: "+reaseguro.toString(), Level.SEVERE, e);
				throw e;
			} catch (Exception ex) {
				LogDeMidasWeb.log("Ocurri� un error al enviar los datos de distribuci�n de siniestro a reaseguro: "+reaseguro.toString(), Level.SEVERE, ex);
			} 
		}						
	}				
	
	/**
	 * Realiza la distribucion de reaseguro de la
	 * reserva asociada a un reporte de siniestro
	 * La reserva es especificada en el parametro reservaDTO
	 * 
	 * @param ReservaDTO reservaDTO Objeto con la informacion de la Reserva a Distribuir
	 * @return null
	 * @throws SystemException
	 */
	public void distribuirReserva(ReservaDTO reservaDTO,String nombreUsuario) throws SystemException{		
		List<IntegracionReaseguroDTO> listaReservaDetalle = this.getListaReservaDetalle(reservaDTO);
		this.distribuirReaseguro(listaReservaDetalle,nombreUsuario);
	}			
			
	/**
	 * Obtiene los movimientos de distribucion de reaseguro
	 * de la reserva asociada a un reporte de siniestro
	 * La reserva es especificada en el parametro reservaDTO
	 * 
	 * @param ReservaDTO reservaDTO
	 * @return List<IntegracionReaseguroDTO> Representa los movimientos que se deben distribuir
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */	
	private List<IntegracionReaseguroDTO> getListaReservaDetalle(ReservaDTO reservaDTO) throws ExcepcionDeAccesoADatos, SystemException{		
		List<IntegracionReaseguroDTO> reservaDetalle = null;
						
		ReporteSiniestroDTO reporteSiniestroDTO  =	reservaDTO.getReporteSiniestroDTO();
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		PolizaSoporteDanosDTO polizaDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza());
		BigDecimal noEndosoOrigenReporte = reporteSiniestroDTO.getNumeroEndoso();
				
		IntegracionReaseguroSN integracionReaseguroSN = new IntegracionReaseguroSN();
		BigDecimal idToReservaEstimada = reservaDTO.getIdtoreservaestimada();
		
		switch(reservaDTO.getTipoajuste().intValue()){		
			case 0:{ // reserva inicial
				reservaDetalle = integracionReaseguroSN.getReservaDetalleInicial(idToReservaEstimada);
				completarListaReservaInicial(reservaDetalle, polizaDTO, noEndosoOrigenReporte);
				break;
			}
			case 1: // Ajuste de Mas y Ajuste de Menos
			case 2:{
				reservaDetalle = integracionReaseguroSN.getReservaDetalle(idToReservaEstimada);
				completarListaReserva(reservaDetalle, polizaDTO, noEndosoOrigenReporte);
				break;
			}
		}																 		
		
		return reservaDetalle;											
	}				
	
	/**
	 * Asigna la informacion de la poliza a los moviemientos de distribucion 
	 * de reaseguro de una reserva inicial asociada a un reporte de siniestro. 
	 * El parametro listaReservaDetalle representa la lista de movimientos
	 * que se van a completar.
	 * 
	 * @param List<IntegracionReaseguroDTO> listaReservaDetalle Lista con los movimientos a los que se les asocia la informacion de la poliza
	 * @paran PolizaSoporteDanosDTO poliza Objeto con la informacion de la poliza
	 * @return void
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */	
	private void completarListaReservaInicial(List<IntegracionReaseguroDTO> listaReservaDetalle, PolizaSoporteDanosDTO poliza, BigDecimal noEndosoOrigenReporte) throws ExcepcionDeAccesoADatos, SystemException{
		for(IntegracionReaseguroDTO reservaDetalle : listaReservaDetalle){
			reservaDetalle.setIdToPoliza(poliza.getIdToPoliza());
			reservaDetalle.setIdMoneda(poliza.getIdMoneda());
//			reservaDetalle.setNumeroEndoso(poliza.getNumeroUltimoEndoso());
			reservaDetalle.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reservaDetalle.setConceptoMovimiento(ConceptoMovimientoSiniestro.RESERVA_INICIAL); 
			reservaDetalle.setFechaRegistroMovimiento(Calendar.getInstance().getTime());			
//			reservaDetalle.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			reservaDetalle.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
		}
	}
	
	/**
	 * Asigna la informacion de la poliza a los moviemientos de distribucion 
	 * de reaseguro de un ajuste de reserva asociada a un reporte de siniestro. 	  
	 * El parametro listaReservaDetalle representa la lista de movimientos
	 * que se van a completar.
	 * 
	 * @param List<IntegracionReaseguroDTO> listaReservaDetalle Lista con los movimientos a los que se les asocia la informacion de la poliza
	 * @paran PolizaSoporteDanosDTO poliza Objeto con la informacion de la poliza
	 * @return void
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */	
	private void completarListaReserva(List<IntegracionReaseguroDTO> listaReservaDetalle, PolizaSoporteDanosDTO poliza, BigDecimal noEndosoOrigenReporte) throws ExcepcionDeAccesoADatos, SystemException{
		for(IntegracionReaseguroDTO reservaDetalle : listaReservaDetalle){
			reservaDetalle.setIdToPoliza(poliza.getIdToPoliza());
			reservaDetalle.setIdMoneda(poliza.getIdMoneda());
//			reservaDetalle.setNumeroEndoso(poliza.getNumeroUltimoEndoso());
			reservaDetalle.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reservaDetalle.setFechaRegistroMovimiento(Calendar.getInstance().getTime());
			
			
			long montoSiniestro = Math.round(reservaDetalle.getMontoSiniestro());
			
			if(montoSiniestro < 0){
				reservaDetalle.setConceptoMovimiento(ConceptoMovimientoSiniestro.AJUSTE_DE_MENOS_EN_RESERVA);
				reservaDetalle.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			}else {
				reservaDetalle.setConceptoMovimiento(ConceptoMovimientoSiniestro.AJUSTE_DE_MAS_EN_RESERVA);
				reservaDetalle.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
			}
			// Asegurar que el monto vaya positivo aun y cuando el ajuste sea de menos;
			double montoAbsoluto = Math.abs(reservaDetalle.getMontoSiniestro().doubleValue());
			reservaDetalle.setMontoSiniestro(new Double(montoAbsoluto));
		}		
	}
	
	/**
	 * Realiza la distribucion de reaseguro de un gasto asociado a un reporte de siniestro.
	 *  	
	 * @param GastoSiniestroDTO gastoSiniestroDTO Objeto que representa el Gasto a Distribuir
	 * @return null
	 * @throws SystemException
	 */	
	public void distribuirGasto(GastoSiniestroDTO gastoSiniestroDTO, int tipoDistribucion,Date fechaMovimiento,String nombreUsuario) throws SystemException{
		BigDecimal idToReporteSiniestro = gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		BigDecimal numeroPoliza = gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza();
		BigDecimal noEndosoOrigenReporte = gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroEndoso();
		List<IntegracionReaseguroDTO> movimientosReaseguro = 
			this.getDistribucionReaseguro(idToReporteSiniestro);
		
		PolizaSoporteDanosDTO polizaDTO = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO reaseguro : movimientosReaseguro){
			reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.GASTOS_DE_AJUSTE);
//			reaseguro.setFechaRegistroMovimiento(Calendar.getInstance().getTime());
			reaseguro.setFechaRegistroMovimiento(fechaMovimiento);
			reaseguro.setIdMovimientoSiniestro(gastoSiniestroDTO.getIdToGastoSiniestro());
//			reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
//			reaseguro.setNumeroEndoso(polizaDTO.getNumeroUltimoEndoso());
			reaseguro.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reaseguro.setIdMoneda(polizaDTO.getIdMoneda());
			reaseguro.setMontoSiniestro(gastoSiniestroDTO.getMontoGasto());
			
			if(tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION){
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.GASTOS_DE_AJUSTE);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
			}else{
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.CANCELACION_DE_GASTO_DE_AJUSTE);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			}
			
			double montoADistribuir = 
				gastoSiniestroDTO.getMontoGasto().doubleValue() * reaseguro.getPorcentajeSumaAsegurada().doubleValue();			
			
			reaseguro.setMontoSiniestro(new Double(montoADistribuir));																		
		}
		
		distribuirReaseguro(movimientosReaseguro,nombreUsuario);		
	}		
	
	/**
	 * Realiza la distribucion de reaseguro de un ingreso asociado a un reporte de siniestro.
	 * 
	 * @param IngresoSiniestroDTO ingresoSiniestroDTO Objeto que representa el Ingreso a Distribuir
	 * @return null
	 * @throws ExcepcionDeAccesoADatos, SystemException 
	 */		
	public void distribuirIngreso(IngresoSiniestroDTO ingresoSiniestroDTO, int tipoDistribucion,Date fechaMovimiento,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{		
		BigDecimal idToReporteSiniestro = ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		BigDecimal numeroPoliza = ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza();
		BigDecimal noEndosoOrigenReporte = ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		PolizaSoporteDanosDTO polizaDTO = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		List<IntegracionReaseguroDTO> movimientosReaseguro = 
			this.getDistribucionReaseguro(idToReporteSiniestro);
						
		Integer conceptoMovimiento = null;		
		BigDecimal conceptoMapeo = null;
		
		conceptoMapeo = new BigDecimal(tipoDistribucion);
		
//		if(tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION){
//			conceptoMapeo = new BigDecimal(IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION);
//		}else{
//			conceptoMapeo = new BigDecimal(IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION);
//		}
		
		conceptoMovimiento = getConceptoDistribucionSiniestro(ingresoSiniestroDTO.getConceptoIngreso().getIdTcConceptoIngreso(), conceptoMapeo);							
		
		for(IntegracionReaseguroDTO reaseguro : movimientosReaseguro){			
//			reaseguro.setFechaRegistroMovimiento(Calendar.getInstance().getTime());
			reaseguro.setFechaRegistroMovimiento(fechaMovimiento);
			reaseguro.setIdMovimientoSiniestro(ingresoSiniestroDTO.getIdToIngresoSiniestro());
			reaseguro.setConceptoMovimiento(conceptoMovimiento);
			
			if(tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO || 
					tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION){
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			}else{
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
			}
									
//			reaseguro.setNumeroEndoso(polizaDTO.getNumeroUltimoEndoso());
			reaseguro.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reaseguro.setIdMoneda(polizaDTO.getIdMoneda());						
																
			double montoADistribuir = 
				ingresoSiniestroDTO.getMonto().doubleValue() * reaseguro.getPorcentajeSumaAsegurada().doubleValue();
			
			reaseguro.setMontoSiniestro(new Double(montoADistribuir));												
		}
		
		distribuirReaseguro(movimientosReaseguro,nombreUsuario);										
	}
	
	/**
	 * Realiza la distribucion de reaseguro de un ingreso asociado a un reporte de siniestro.
	 * 
	 * @param IndemnizacionDTO indemnizacionDTO Objeto que representa el Ingreso a Distribuir
	 * @return null
	 * @throws SystemException
	 */			
	public void distribuirIndemnizacion(IndemnizacionDTO indemnizacionDTO, int tipoDistribucion,Date fechaMovimiento,String nombreUsuario) throws SystemException{						
		BigDecimal numeroPoliza = indemnizacionDTO.getReporteSiniestroDTO().getNumeroPoliza();		
		BigDecimal noEndosoOrigenReporte = indemnizacionDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		List<IntegracionReaseguroDTO> movimientosReaseguro = 
			this.getDistribucionDeIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
		
		PolizaSoporteDanosDTO polizaDTO = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO reaseguro : movimientosReaseguro){			
//			reaseguro.setNumeroEndoso(polizaDTO.getNumeroUltimoEndoso());
			reaseguro.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reaseguro.setIdMoneda(polizaDTO.getIdMoneda());
//			reaseguro.setFechaRegistroMovimiento(Calendar.getInstance().getTime());		
			reaseguro.setFechaRegistroMovimiento(fechaMovimiento);
			
			if(tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO ||
					tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION){
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.INDEMNIZACION);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
			}else{
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.CANCELACION_DE_INDEMNIZACION);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			}																
		}
		
		distribuirReaseguro(movimientosReaseguro,nombreUsuario);
	}
	
	/**
	 * Realiza la distribucion de reaseguro del deducible de la indemnizacion asociada a un reporte de siniestro.
	 * 
	 * @param IndemnizacionDTO indemnizacionDTO Objeto que representa el Ingreso a Distribuir
	 * @return null
	 * @throws SystemException
	 */			
	public void distribuirDeducibleIndemnizacion(IndemnizacionDTO indemnizacionDTO, int tipoDistribucion,Date fechaMovimiento,String nombreUsuario) throws SystemException{						
		BigDecimal numeroPoliza = indemnizacionDTO.getReporteSiniestroDTO().getNumeroPoliza();		
		BigDecimal noEndosoOrigenReporte = indemnizacionDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		List<IntegracionReaseguroDTO> movimientosReaseguro = 
			this.getDistribucionDeDeducibleIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
		
		PolizaSoporteDanosDTO polizaDTO = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO reaseguro : movimientosReaseguro){			
//			reaseguro.setNumeroEndoso(polizaDTO.getNumeroUltimoEndoso());
			reaseguro.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reaseguro.setIdMoneda(polizaDTO.getIdMoneda());
//			reaseguro.setFechaRegistroMovimiento(Calendar.getInstance().getTime());	
			reaseguro.setFechaRegistroMovimiento(fechaMovimiento);
			
			if(tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION || 
					tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO){
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.DEDUCIBLE);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			}else{
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.CANCELACION_DE_DEDUCIBLE);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
			}																
		}
		
		distribuirReaseguro(movimientosReaseguro,nombreUsuario);
	}
	
	/**
	 * Realiza la distribucion de reaseguro del deducible de la indemnizacion asociada a un reporte de siniestro.
	 * 
	 * @param IndemnizacionDTO indemnizacionDTO Objeto que representa el Ingreso a Distribuir
	 * @return null
	 * @throws SystemException
	 */			
	public void distribuirCoaseguroIndemnizacion(IndemnizacionDTO indemnizacionDTO, int tipoDistribucion,Date fechaMovimiento,String nombreUsuario) throws SystemException{						
		BigDecimal numeroPoliza = indemnizacionDTO.getReporteSiniestroDTO().getNumeroPoliza();		
		BigDecimal noEndosoOrigenReporte = indemnizacionDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		List<IntegracionReaseguroDTO> movimientosReaseguro = 
			this.getDistribucionDeCoaseguroIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
		
		PolizaSoporteDanosDTO polizaDTO = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO reaseguro : movimientosReaseguro){			
//			reaseguro.setNumeroEndoso(polizaDTO.getNumeroUltimoEndoso());
			reaseguro.setNumeroEndoso(noEndosoOrigenReporte.intValue());
			reaseguro.setIdMoneda(polizaDTO.getIdMoneda());
//			reaseguro.setFechaRegistroMovimiento(Calendar.getInstance().getTime());			
			reaseguro.setFechaRegistroMovimiento(fechaMovimiento);
			
			if(tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION || 
					tipoDistribucion == IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO){
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.INGRESO_COASEGURO);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_COBRAR);
			}else{
				reaseguro.setConceptoMovimiento(ConceptoMovimientoSiniestro.CANCELACION_DE_INGRESO_COASEGURO);
				reaseguro.setTipoMovimiento(TipoMovimientoSiniestro.CUENTA_POR_PAGAR);
			}																
		}
		
		distribuirReaseguro(movimientosReaseguro,nombreUsuario);						
	}
		
	/**
	 * Obtiene los movimientos de distribucion de reaseguro
	 * asociados a un Gasto o Ingreso de un reporte de siniestros 
	 * 
	 * @param BigDecimal idToReporteSiniestro Id del Reporte de Siniestro que servira de base para obtener los movimientos
	 * @return Lista que representa los movimientos que se deben distribuir
	 * @throws throws SystemException
	 */			
	private List<IntegracionReaseguroDTO> getDistribucionReaseguro(BigDecimal idToReporteSiniestro) throws SystemException{
		IntegracionReaseguroSN integracionReaseguroSN = new IntegracionReaseguroSN();
		return integracionReaseguroSN.getDistribucionReaseguro(idToReporteSiniestro);		
	}
	
	/**
	 * Obtiene los movimientos de distribucion de reaseguro
	 * asociados a una indemnizacion de un reporte de siniestros
	 * 
	 * @param BigDecimal idToIndemnizacion Id de la Indemnizacion para obtener los movimientos a distribuir
	 * @return Lista que representa los movimientos que se deben distribuir
	 * @throws SystemException
	 */			
	private List<IntegracionReaseguroDTO> getDistribucionDeIndemnizacion(BigDecimal idToIndemnizacion) throws SystemException{
		IntegracionReaseguroSN integracionReaseguroSN = new IntegracionReaseguroSN();
		return integracionReaseguroSN.getDistribucionDeIndemnizacion(idToIndemnizacion);				
	}	
	
	/**
	 * Obtiene la informacion de la Poliza asociada a un reporte de siniestro 
	 * 
	 * @param BigDecimal idToReporteSiniestro Id del Reporte de Siniestro
	 * @return Lista que representa los movimientos que se deben distribuir
	 * @throws throws ExcepcionDeAccesoADatos, SystemException
	 */				
	private PolizaSoporteDanosDTO getPolizaPorIdReporteSiniestro(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		PolizaSoporteDanosDTO poliza = null;
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		poliza = soporteDanosDN.getDatosGeneralesPoliza(idToReporteSiniestro);
		
		return poliza;
	}						
	
	/**
	 * Obtiene los detalles de la distribucion de un Gasto de Siniestro. 
	 * 
	 * @param GastoSiniestroDTO gastoSiniestroDTO 
	 * @return Lista que representa los detalles de la distribucion del gasto
	 * @throws SystemException
	 */					
	public List<SoporteDistribucionReaseguroDTO> getDistribucionDeGasto(GastoSiniestroDTO gastoSiniestroDTO) throws SystemException{
		List <SoporteDistribucionReaseguroDTO> distribucion = new ArrayList<SoporteDistribucionReaseguroDTO>();
		
//		BigDecimal numeroPoliza = gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza();
		BigDecimal idToReporteSiniestro = gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		BigDecimal noEndosoOrigenReporte = gastoSiniestroDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		List<IntegracionReaseguroDTO> distribucionesGasto = this.getDistribucionReaseguro(idToReporteSiniestro);			
		DistribucionReaseguroDN  distribucionReaseguro = DistribucionReaseguroDN.getInstancia();		
//		PolizaSoporteDanosDTO poliza = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO obj : distribucionesGasto){
			SoporteDistribucionReaseguroDTO soporteDistribucion = new SoporteDistribucionReaseguroDTO(); 			
			CoberturaSoporteDanosDTO coberturaDTO = this.getCoberturaDTO(obj.getIdToCobertura());
			SeccionSoporteDanosDTO seccionDTO = this.getSeccionDTO(obj.getIdToSeccion());
			
			soporteDistribucion.setCoberturaDTO(coberturaDTO);
			soporteDistribucion.setSeccionDTO(seccionDTO);			
			
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = 
				distribucionReaseguro.getPorcentajeDistribucion(
								obj.getIdToPoliza(), 
//								poliza.getNumeroUltimoEndoso(),
								noEndosoOrigenReporte.intValue(),
								obj.getIdToSeccion(), 
								obj.getIdToCobertura(), 
								new Integer(obj.getNumeroInciso().intValue()), 
								new Integer(obj.getNumeroSubInciso().intValue()));
			
			double montoADistribuir = gastoSiniestroDTO.getMontoGasto() * 
												obj.getPorcentajeSumaAsegurada().doubleValue();
			
			soporteDistribucion.setMonto(montoADistribuir);
			
			if(lineaSoporteReaseguroDTO != null){
				soporteDistribucion.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
				
				double montoCuotaparte = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeCuotaParte().doubleValue() / 100);
				soporteDistribucion.setMontoCuotaParte(montoCuotaparte);
				
				double montoFacultativo = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() / 100);
				soporteDistribucion.setMontoFacultativo(montoFacultativo);
				
				double montoPrimerExcedente = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente().doubleValue() / 100);
				soporteDistribucion.setMontoPrimerExcedente(montoPrimerExcedente);
				
				double montoRetencion = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeRetencion().doubleValue() / 100);
				soporteDistribucion.setMontoRetenido(montoRetencion);			
			}else{
				soporteDistribucion.setMontoCuotaParte(0.0);
				soporteDistribucion.setMontoFacultativo(0.0);
				soporteDistribucion.setMontoPrimerExcedente(0.0);
				soporteDistribucion.setMontoRetenido(0.0);
			}			
			
			distribucion.add(soporteDistribucion);			
		}
														
		return distribucion;
	}
	
	/**
	 * Obtiene los detalles de la distribucion de un Ingreso de Siniestro. 
	 * 
	 * @param IngresoSiniestroDTO ingresoSiniestroDTO 
	 * @return Lista que representa los detalles de la distribucion del Ingreso
	 * @throws SystemException
	 */						
	public List<SoporteDistribucionReaseguroDTO> getDistribucionDeIngreso(IngresoSiniestroDTO ingresoSiniestroDTO) throws SystemException{					
		List <SoporteDistribucionReaseguroDTO> distribucion = new ArrayList<SoporteDistribucionReaseguroDTO>();
		
//		BigDecimal numeroPoliza = ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza();
		BigDecimal idToReporteSiniestro = ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		BigDecimal noEndosoOrigenReporte = ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		List<IntegracionReaseguroDTO> distribucionesIngreso = this.getDistribucionReaseguro(idToReporteSiniestro);			
		DistribucionReaseguroDN  distribucionReaseguro = DistribucionReaseguroDN.getInstancia();		
//		PolizaSoporteDanosDTO poliza = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO obj : distribucionesIngreso){
			SoporteDistribucionReaseguroDTO soporteDistribucion = new SoporteDistribucionReaseguroDTO(); 			
			CoberturaSoporteDanosDTO coberturaDTO = this.getCoberturaDTO(obj.getIdToCobertura());
			SeccionSoporteDanosDTO seccionDTO = this.getSeccionDTO(obj.getIdToSeccion());
			
			soporteDistribucion.setCoberturaDTO(coberturaDTO);
			soporteDistribucion.setSeccionDTO(seccionDTO);
			
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = 
				distribucionReaseguro.getPorcentajeDistribucion(
								obj.getIdToPoliza(), 
//								poliza.getNumeroUltimoEndoso(),
								noEndosoOrigenReporte.intValue(),
								obj.getIdToSeccion(), 
								obj.getIdToCobertura(), 
								new Integer(obj.getNumeroInciso().intValue()), 
								new Integer(obj.getNumeroSubInciso().intValue()));
			
			double montoADistribuir = ingresoSiniestroDTO.getMonto().doubleValue() * 
												obj.getPorcentajeSumaAsegurada().doubleValue();
			
			soporteDistribucion.setMonto(montoADistribuir);
			
			if(lineaSoporteReaseguroDTO != null){
				soporteDistribucion.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
				
				double montoCuotaparte = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeCuotaParte().doubleValue() / 100);
				soporteDistribucion.setMontoCuotaParte(montoCuotaparte);
				
				double montoFacultativo = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() / 100);
				soporteDistribucion.setMontoFacultativo(montoFacultativo);
				
				double montoPrimerExcedente = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente().doubleValue() / 100);
				soporteDistribucion.setMontoPrimerExcedente(montoPrimerExcedente);
				
				double montoRetencion = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeRetencion().doubleValue() / 100);
				soporteDistribucion.setMontoRetenido(montoRetencion);			
			}else{
				soporteDistribucion.setMontoCuotaParte(0.0);
				soporteDistribucion.setMontoFacultativo(0.0);
				soporteDistribucion.setMontoPrimerExcedente(0.0);
				soporteDistribucion.setMontoRetenido(0.0);
			}			
			
			distribucion.add(soporteDistribucion);			
		}
														
		return distribucion;								
	}	
	

	/**
	 * Obtiene los detalles de la distribucion de una Indemnizacion 
	 * 
	 * @param IndemnizacionDTO indemnizacionDTO
	 * @return Lista que representa los detalles de la distribucion de la Indemnizacion
	 * @throws SystemException
	 */						
	public List<SoporteDistribucionReaseguroDTO> getDistribucionDeIndemnizacion(IndemnizacionDTO indemnizacionDTO) throws SystemException{				
		List <SoporteDistribucionReaseguroDTO> distribucion = new ArrayList<SoporteDistribucionReaseguroDTO>();
		
//		BigDecimal numeroPoliza = indemnizacionDTO.getReporteSiniestroDTO().getNumeroPoliza();		
		BigDecimal idToIndemnizacion = indemnizacionDTO.getIdToIndemnizacion();
		BigDecimal noEndosoOrigenReporte = indemnizacionDTO.getReporteSiniestroDTO().getNumeroEndoso();
		
		List<IntegracionReaseguroDTO> distribucionesIndemnizacion = this.getDistribucionDeIndemnizacion(idToIndemnizacion);			
		DistribucionReaseguroDN  distribucionReaseguro = DistribucionReaseguroDN.getInstancia();		
//		PolizaSoporteDanosDTO poliza = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO obj : distribucionesIndemnizacion){
			SoporteDistribucionReaseguroDTO soporteDistribucion = new SoporteDistribucionReaseguroDTO();			
			CoberturaSoporteDanosDTO coberturaDTO = this.getCoberturaDTO(obj.getIdToCobertura());
			SeccionSoporteDanosDTO seccionDTO = this.getSeccionDTO(obj.getIdToSeccion());
			
			soporteDistribucion.setCoberturaDTO(coberturaDTO);
			soporteDistribucion.setSeccionDTO(seccionDTO);
											
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = 
				distribucionReaseguro.getPorcentajeDistribucion(
								obj.getIdToPoliza(), 
//								poliza.getNumeroUltimoEndoso(), 
								noEndosoOrigenReporte.intValue(),
								obj.getIdToSeccion(), 
								obj.getIdToCobertura(), 
								new Integer(obj.getNumeroInciso().intValue()), 
								new Integer(obj.getNumeroSubInciso().intValue()));
			
			double montoADistribuir = obj.getMontoSiniestro().doubleValue(); //indemnizacionDTO.getTotalPago().doubleValue();
			
			soporteDistribucion.setMonto(montoADistribuir);
			
			if(lineaSoporteReaseguroDTO != null){
				soporteDistribucion.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
				
				double montoCuotaparte = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeCuotaParte().doubleValue() / 100);
				soporteDistribucion.setMontoCuotaParte(montoCuotaparte);
				
				double montoFacultativo = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() / 100);
				soporteDistribucion.setMontoFacultativo(montoFacultativo);
				
				double montoPrimerExcedente = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente().doubleValue() / 100);
				soporteDistribucion.setMontoPrimerExcedente(montoPrimerExcedente);
				
				double montoRetencion = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeRetencion().doubleValue() / 100);
				soporteDistribucion.setMontoRetenido(montoRetencion);			
			}else{
				soporteDistribucion.setMontoCuotaParte(0.0);
				soporteDistribucion.setMontoFacultativo(0.0);
				soporteDistribucion.setMontoPrimerExcedente(0.0);
				soporteDistribucion.setMontoRetenido(0.0);
			}			
			
			distribucion.add(soporteDistribucion);			
		}
														
		return distribucion;														
	}	
		
	/**
	 * Obtiene la informacion de una Cobertura 
	 * 
	 * @param BigDecimal idToCobertura Id de la Cobertura  
	 * @return CoberturaSoporteDanosDTO 
	 * @return null Si el Id no pertenece a una Cobertura existente
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */							
	private CoberturaSoporteDanosDTO getCoberturaDTO(BigDecimal idToCobertura) throws ExcepcionDeAccesoADatos, SystemException{
		CoberturaSoporteDanosDTO coberturaDTO = null;
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		coberturaDTO =  soporteDanosDN.getCoberturaSoporte(idToCobertura);
		
		return coberturaDTO;
	}
	
	/**
	 * Obtiene la informacion de una Seccion 
	 * 
	 * @param BigDecimal idToSeccion Id de la Seccion  
	 * @return SeccionSoporteDanosDTO 
	 * @return null Si el Id no pertenece a una Seccion existente
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */							
	private SeccionSoporteDanosDTO getSeccionDTO(BigDecimal idToSeccion) throws ExcepcionDeAccesoADatos, SystemException{
		SeccionSoporteDanosDTO seccionDTO = null;
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		seccionDTO = soporteDanosDN.getSeccionSoporte(idToSeccion);
		
		return seccionDTO;
	}
	/**
	 * Obtiene el Concepto del Ingreso que se envia a la distribucion de un ingreso 
	 * 
	 * @param BigDecimal idConcepto Es el IdConceptoIngreso asociado a un Ingreso de Siniestro
	 * @param String concepto 0- Si es una Autorizacion , 1 - Si es una cancelacion  
	 * @return Integer que representa el codigo de concepto 
	 * @return null Si no se encuentra el IdConceptoIngreso 
	 * @throws ExcepcionDeAccesoADatos, SystemException
	 */								
	private Integer getConceptoDistribucionSiniestro(BigDecimal idConcepto, BigDecimal concepto) throws SystemException{
		Integer conceptoDistribucion = null;
		
		MapeoConceptoSiniestroDN mapeoConceptoSiniestroDN = MapeoConceptoSiniestroDN.getInstancia();
		MapeoConceptoSiniestroDTO mapeoConceptoSiniestroDTO = mapeoConceptoSiniestroDN.getMapeoByIdConcepto(idConcepto, concepto);
		
		if(mapeoConceptoSiniestroDTO != null){
			conceptoDistribucion = new Integer(mapeoConceptoSiniestroDTO.getId().getValorMapeo().intValue());
		}
		
		return conceptoDistribucion;
	}
	
	
	public void getDistribucionDeReporteSiniestro(List<SoporteDistribucionReaseguroDTO> soporteDistribucionReaseguroLista, double montoADistribuir) throws SystemException{
		if (soporteDistribucionReaseguroLista==null || soporteDistribucionReaseguroLista.isEmpty()) return;
		for (SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO : soporteDistribucionReaseguroLista) {
			this.realizaDistribucionDeReporteSiniestro(soporteDistribucionReaseguroDTO, montoADistribuir);
		}
	}
	
	public List<SoporteDistribucionReaseguroDTO> getDistribucionDeReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO, double montoADistribuir) throws SystemException{
		if (reporteSiniestroDTO==null || reporteSiniestroDTO.getIdToReporteSiniestro()==null) return null;
		
		List<IntegracionReaseguroDTO> integracionReaseguroList = this.getDistribucionReaseguro(reporteSiniestroDTO.getIdToReporteSiniestro());
		return this.getDistribucionDeReporteSiniestro(reporteSiniestroDTO, integracionReaseguroList, montoADistribuir);
	}
	
	/**
	 * Obtiene los detalles de la distribucion de una Indemnizacion 
	 * 
	 * @param IndemnizacionDTO indemnizacionDTO
	 * @return Lista que representa los detalles de la distribucion de la Indemnizacion
	 * @throws SystemException
	 */						
	public List<SoporteDistribucionReaseguroDTO> getDistribucionDeReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO, List<IntegracionReaseguroDTO> integracionReaseguroList,double montoADistribuir) throws SystemException{				
		List <SoporteDistribucionReaseguroDTO> distribucion = new ArrayList<SoporteDistribucionReaseguroDTO>();
		
//		BigDecimal numeroPoliza = reporteSiniestroDTO.getNumeroPoliza();
		BigDecimal noEndosoOrigenReporte = reporteSiniestroDTO.getNumeroEndoso();
		
		DistribucionReaseguroDN  distribucionReaseguro = DistribucionReaseguroDN.getInstancia();		
//		PolizaSoporteDanosDTO poliza = getPolizaPorIdReporteSiniestro(numeroPoliza);
		
		for(IntegracionReaseguroDTO obj : integracionReaseguroList){
			SoporteDistribucionReaseguroDTO soporteDistribucion = new SoporteDistribucionReaseguroDTO(); 			
			CoberturaSoporteDanosDTO coberturaDTO = getCoberturaDTO(obj.getIdToCobertura());
			SeccionSoporteDanosDTO seccionDTO = getSeccionDTO(obj.getIdToSeccion());
			soporteDistribucion.setCoberturaDTO(coberturaDTO);
			soporteDistribucion.setSeccionDTO(seccionDTO);
			soporteDistribucion.setNumeroSubInciso(obj.getNumeroSubInciso());
			soporteDistribucion.setNumeroInciso(obj.getNumeroInciso());
			
			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = 
				distribucionReaseguro.getPorcentajeDistribucion(
								obj.getIdToPoliza(), 
//								poliza.getNumeroUltimoEndoso(),
								noEndosoOrigenReporte.intValue(),
								obj.getIdToSeccion(), 
								obj.getIdToCobertura(), 
								new Integer(obj.getNumeroInciso().intValue()), 
								new Integer(obj.getNumeroSubInciso().intValue()));
			
			
			soporteDistribucion.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
			soporteDistribucion.setPorcentajeSumaAsegurada(obj.getPorcentajeSumaAsegurada());
			this.realizaDistribucionDeReporteSiniestro(soporteDistribucion, montoADistribuir);
			distribucion.add(soporteDistribucion);			
		}
														
		return distribucion;														
	}
	
	/**
	 * Obtiene los detalles de la distribucion de una Indemnizacion 
	 * 
	 * @param IndemnizacionDTO indemnizacionDTO
	 * @return Lista que representa los detalles de la distribucion de la Indemnizacion
	 * @throws SystemException
	 */						
	public void realizaDistribucionDeReporteSiniestro(SoporteDistribucionReaseguroDTO soporteDistribucion, double montoOriginal) throws SystemException{				
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = soporteDistribucion.getLineaSoporteReaseguroDTO();
		double montoADistribuir = montoOriginal * soporteDistribucion.getPorcentajeSumaAsegurada();
		soporteDistribucion.setMonto(montoADistribuir);
		
		if(lineaSoporteReaseguroDTO != null){
			double montoCuotaparte = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeCuotaParte().doubleValue() / 100);
			soporteDistribucion.setMontoCuotaParte(montoCuotaparte);
			
			double montoFacultativo = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() / 100);
			soporteDistribucion.setMontoFacultativo(montoFacultativo);
			
			double montoPrimerExcedente = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente().doubleValue() / 100);
			soporteDistribucion.setMontoPrimerExcedente(montoPrimerExcedente);
			
			double montoRetencion = montoADistribuir * (lineaSoporteReaseguroDTO.getPorcentajeRetencion().doubleValue() / 100);
			soporteDistribucion.setMontoRetenido(montoRetencion);			
		}else{
			soporteDistribucion.setMontoCuotaParte(0.0);
			soporteDistribucion.setMontoFacultativo(0.0);
			soporteDistribucion.setMontoPrimerExcedente(0.0);
			soporteDistribucion.setMontoRetenido(0.0);
		}			
	}
	
	
	/**
	 * Obtiene los movimientos de distribucion de reaseguro
	 * asociados al Deducible de una indemnizacion de un reporte de siniestros
	 * 
	 * @param BigDecimal idToIndemnizacion Id de la Indemnizacion para obtener los movimientos a distribuir
	 * @return Lista que representa los movimientos que se deben distribuir
	 * @throws SystemException
	 */			
	private List<IntegracionReaseguroDTO> getDistribucionDeDeducibleIndemnizacion(BigDecimal idToIndemnizacion) throws SystemException{
		IntegracionReaseguroSN integracionReaseguroSN = new IntegracionReaseguroSN();
		return integracionReaseguroSN.getDistribucionDeDeducibleIndemnizacion(idToIndemnizacion);				
	}	
	
	/**
	 * Obtiene los movimientos de distribucion de reaseguro
	 * asociados al Coaseguro de una indemnizacion de un reporte de siniestros
	 * 
	 * @param BigDecimal idToIndemnizacion Id de la Indemnizacion para obtener los movimientos a distribuir
	 * @return Lista que representa los movimientos que se deben distribuir
	 * @throws SystemException
	 */			
	private List<IntegracionReaseguroDTO> getDistribucionDeCoaseguroIndemnizacion(BigDecimal idToIndemnizacion) throws SystemException{
		IntegracionReaseguroSN integracionReaseguroSN = new IntegracionReaseguroSN();
		return integracionReaseguroSN.getDistribucionDeCoaseguroIndemnizacion(idToIndemnizacion);				
	}	

}

