package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.gasto.ConceptoGastoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.ConceptoIngresoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class ConceptoIngresoSN {
	private ConceptoIngresoFacadeRemote beanRemoto;
	
	public ConceptoIngresoSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en ConceptoIngresoSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ConceptoIngresoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado ConceptoIngresoSN", Level.FINEST, null);
	}		

	
	public void agregarConceptoIngreso(ConceptoIngresoDTO conceptoIngresoDTO){
		beanRemoto.save(conceptoIngresoDTO);
	}
	
	public void eliminarConceptoIngreso(ConceptoIngresoDTO conceptoIngresoDTO){
		beanRemoto.delete(conceptoIngresoDTO);	
	}
	
	public ConceptoIngresoDTO actualizarConceptoIngreso(ConceptoIngresoDTO conceptoIngresoDTO){
		return beanRemoto.update(conceptoIngresoDTO);
	}
	
	public ConceptoIngresoDTO findById(BigDecimal id){
		return beanRemoto.findById(id);
	}
 
	public List<ConceptoIngresoDTO> findAll(){
		return beanRemoto.findAll();
	} 	
}
