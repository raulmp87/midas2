package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class IntegracionReaseguroSN {
	IntegracionReaseguroFacadeRemote beanRemoto;
	
	public IntegracionReaseguroSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en IntegracionReaseguroSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(IntegracionReaseguroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado IntegracionReaseguroSN", Level.FINEST, null);
	}
		
	public List<IntegracionReaseguroDTO> getReservaDetalleInicial(BigDecimal idToReservaEstimada){
		return beanRemoto.getReservaDetalleInicial(idToReservaEstimada);
	}
	
	public List<IntegracionReaseguroDTO> getReservaDetalle(BigDecimal idToReservaEstimada){
		return beanRemoto.getReservaDetalle(idToReservaEstimada);
	}
	
	public List<IntegracionReaseguroDTO> getDistribucionReaseguro(BigDecimal idToReporteSiniestro){
		return beanRemoto.getDistribucionReaseguro(idToReporteSiniestro);
	}
	
	public List<IntegracionReaseguroDTO> getDistribucionDeIndemnizacion(BigDecimal idToIndemnizacion){
		return beanRemoto.getDistribucionDeIndemnizacion(idToIndemnizacion);
	}
	
	public List<IntegracionReaseguroDTO> getDistribucionDeDeducibleIndemnizacion(BigDecimal idToIndemnizacion){
		return beanRemoto.getDistribucionDeDeducibleIndemnizacion(idToIndemnizacion);
	}
	
	public List<IntegracionReaseguroDTO> getDistribucionDeCoaseguroIndemnizacion(BigDecimal idToIndemnizacion){
		return beanRemoto.getDistribucionDeCoaseguroIndemnizacion(idToIndemnizacion);
	}
}
