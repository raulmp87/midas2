package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AprobarInformeFinalAction extends MidasMappingDispatchAction{

	public ActionForward presentarInformeFinal(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		AprobarInformeFinalForm forma = (AprobarInformeFinalForm) form;
		String idToReporteSiniestroS = request.getParameter("idToReporteSiniestro");
		BigDecimal idToReporteSiniestro = new BigDecimal(idToReporteSiniestroS);
		forma.setIdDeReporte(idToReporteSiniestro);
		ArrayList<Registro> listaDeRegistros = new ArrayList<Registro>();
		ArrayList<RegistroDeReserva> listaDeRegistrosDeReserva = new ArrayList<RegistroDeReserva>();
		ReporteSiniestroDTO reporteSiniestro;

		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		RiesgoAfectadoDN riesgoAfectado = RiesgoAfectadoDN.getInstancia();
		ReservaDN reserva = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN= ReservaDetalleDN.getInstancia();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		List<RiesgoAfectadoDTO> l;
		List<ReservaDetalleDTO> lr;
		try {
			reporteSiniestro = reporteSiniestroDN.desplegarReporte(forma.getIdDeReporte());
			forma.setValido(reporteSiniestro.getInformeFinalValido());
			l = riesgoAfectado.listarRiesgosAfectadosPara(idToReporteSiniestro);
			ReservaDTO r = reserva.listarUltimaReservaAutorizada(idToReporteSiniestro);
//			lr = r.getReservaDetalleDTOs();
			lr = reservaDetalleDN.listarDetalleReserva(r.getIdtoreservaestimada());
//			lr = siniestroMovimientosDN.removerAjustesDeMenos(lr);
			Registro registro;
			for (RiesgoAfectadoDTO a : l) {
				registro = new Registro(a);
				listaDeRegistros.add(registro);
			}
			
			RegistroDeReserva rr;
			for (ReservaDetalleDTO rd : lr) {
				rr = new RegistroDeReserva(rd);
				listaDeRegistrosDeReserva.add(rr);
			}
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(idToReporteSiniestro);
			forma.setTipoMoneda(tipoMoneda);
//			forma.setTipoMoneda("MXP");
			forma.setListadoCoberturas(listaDeRegistros);
			forma.setListadoDeReservas(listaDeRegistrosDeReserva);
		} catch (ExcepcionDeAccesoADatos e) {
			return mapping.findForward(Sistema.NO_EXITOSO);
		} catch (SystemException e) {
			return mapping.findForward(Sistema.NO_DISPONIBLE);
		}
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward noProcedeAIndemnizar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		return marcarReporte(mapping, form, request, ReporteSiniestroDTO.PENDIENTE_TERMINAR_REPORTE_NO_INDEMNIZADO,false);
	}
	
	public ActionForward procedeAIndemnizar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		return marcarReporte(mapping, form, request, ReporteSiniestroDTO.PENDIENTE_AGREGAR_INDEMNIZACION,true);
	}
	
	private ActionForward marcarReporte(ActionMapping mapping, ActionForm form, HttpServletRequest request, byte estatus,boolean informeFinalValido) {
		AprobarInformeFinalForm forma = (AprobarInformeFinalForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ReporteSiniestroDTO reporteSiniestro = new ReporteSiniestroDTO();
		try {
			reporteSiniestro = reporteSiniestroDN.desplegarReporte(forma.getIdDeReporte());
			reporteSiniestro.setInformeFinalValido(informeFinalValido);
			reporteSiniestro.getReporteEstatus().setIdTcReporteEstatus(estatus);
			reporteSiniestroDN.actualizar(reporteSiniestro);
		} catch (ExcepcionDeAccesoADatos e) {
			return mapping.findForward(Sistema.NO_EXITOSO);
		} catch (SystemException e) {
			return mapping.findForward(Sistema.NO_DISPONIBLE);
		}
		return mapping.findForward(Sistema.EXITOSO);		
	}
}
