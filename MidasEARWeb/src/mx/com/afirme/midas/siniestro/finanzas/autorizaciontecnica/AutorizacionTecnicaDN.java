package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class AutorizacionTecnicaDN {
	private static final AutorizacionTecnicaDN INSTANCIA = new AutorizacionTecnicaDN(); 
	
	public static AutorizacionTecnicaDN getInstancia(){
		return INSTANCIA;
	}
	
	public AutorizacionTecnicaDTO findById(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN(); 
		return autorizacionTecnicaSN.findById(id);
	}

	public void agregarAutorizacionTecnica(AutorizacionTecnicaDTO autorizacionTecnicaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		autorizacionTecnicaSN.agregarAutorizacionTecnica(autorizacionTecnicaDTO);
	}
	
	public void actualizarAutorizacionTecnica(AutorizacionTecnicaDTO autorizacionTecnicaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		autorizacionTecnicaSN.actualizarAutorizacionTecnica(autorizacionTecnicaDTO);
	}
	
	public void eliminarAutorizacionTecnica(AutorizacionTecnicaDTO autorizacionTecnicaDTO) throws SystemException, ExcepcionDeAccesoADatos {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		autorizacionTecnicaSN.eliminarAutorizacionTecnica(autorizacionTecnicaDTO);
	}
	
	public List<AutorizacionTecnicaDTO> listarAutorizacionesTecnicas() throws SystemException, ExcepcionDeAccesoADatos {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		return autorizacionTecnicaSN.listarAutorizacionesTecnicas();
	}
	
	public List<AutorizacionTecnicaDTO> findByProperty(String propertyName, final Object value) throws SystemException, ExcepcionDeAccesoADatos {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		return autorizacionTecnicaSN.findByProperty(propertyName, value);		
	}
		
	public AutorizacionTecnicaDTO obtenerAutorizacionIndemnizacion(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion) throws SystemException, ExcepcionDeAccesoADatos{
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		return autorizacionTecnicaSN.obtenerAutorizacionIndemnizacion(idToReporteSiniestro, idToIndemnizacion);
	}
	public AutorizacionTecnicaDTO obtenerAutorizacionIngreso(BigDecimal idToReporteSiniestro,BigDecimal idToIngresoSiniestro) throws SystemException, ExcepcionDeAccesoADatos{
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		return autorizacionTecnicaSN.obtenerAutorizacionIngreso(idToReporteSiniestro, idToIngresoSiniestro);
	}
	public AutorizacionTecnicaDTO obtenerAutorizacionGasto(BigDecimal idToReporteSiniestro,BigDecimal idToGastoSiniestro) throws SystemException, ExcepcionDeAccesoADatos{
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		return autorizacionTecnicaSN.obtenerAutorizacionGasto(idToReporteSiniestro, idToGastoSiniestro);
	}
	
//	public AutorizacionTecnicaDTO cancelarAutorizacionTecnica(BigDecimal idToAutorizacionTecnica,HttpServletRequest request)throws SystemException, ExcepcionDeAccesoADatos{
//		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
//		
//		AutorizacionTecnicaDTO autorizacionTecnicaDTO = this.findById(idToAutorizacionTecnica);
//		autorizacionTecnicaDTO.setEstatus(AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
//		this.actualizarAutorizacionTecnica(autorizacionTecnicaDTO);
//		if(autorizacionTecnicaDTO.getIndemnizacionDTO() != null){
//			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion());
//			if(!(indemnizacionDTO.getVariosPagos().booleanValue()) || (indemnizacionDTO.getUltimoPago().booleanValue())){
//				ConvenioFiniquitoDN convenioFiniquitoDN = ConvenioFiniquitoDN.getInstancia();
//				convenioFiniquitoDN.cancelarConvenioFiniquito(autorizacionTecnicaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(),indemnizacionDTO.getIdToIndemnizacion());
//				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
//				ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(autorizacionTecnicaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
//				ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
//				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AGREGAR_INDEMNIZACION);
//				reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
//				reporteSiniestroDN.actualizar(reporteSiniestroDTO);
//			}
//			EstatusFinanzasDTO estatusFinanzasDTO = indemnizacionDTO.getEstatusFinanzasDTO();
//			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
//			indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
//			indemnizacionDN.modificar(indemnizacionDTO);
//			//
//			//distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION,
//			//			IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION, indemnizacionDTO);										
//			
//		}else if(autorizacionTecnicaDTO.getGastoSiniestroDTO() != null){
//			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
//			GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN.findById(autorizacionTecnicaDTO.getGastoSiniestroDTO().getIdToGastoSiniestro());
//			EstatusFinanzasDTO estatusFinanzasDTO = gastoSiniestroDTO.getEstatusFinanzas();
//			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
//			gastoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
//			gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
//			//
//			distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO,
//					IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION, gastoSiniestroDTO);
//			
//		}else if(autorizacionTecnicaDTO.getIngresoSiniestroDTO() != null){
//			IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
//			IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(autorizacionTecnicaDTO.getIngresoSiniestroDTO().getIdToIngresoSiniestro());
//			EstatusFinanzasDTO estatusFinanzasDTO = ingresoSiniestroDTO.getEstatusFinanzas();
//			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
//			ingresoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
//			ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
//			//
//			distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO, 
//					IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION, ingresoSiniestroDTO);													
//		}
//		return autorizacionTecnicaDTO;
//	}
	
	
	public List<AutorizacionTecnicaDTO> getAutorizacionesPorIdReporte(BigDecimal idReporteSiniestro) throws SystemException{
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN(); 
		return autorizacionTecnicaSN.getAutorizacionesPorIdReporte(idReporteSiniestro);		
	}

	public List<AutorizacionTecnicaDTO> listarAutorizacionesGastosFiltrado (
			SoportePagosDTO soportePagosDTO) throws SystemException{
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		return autorizacionTecnicaSN
				.listarAutorizacionesGastosFiltrado(soportePagosDTO);
	}
}
