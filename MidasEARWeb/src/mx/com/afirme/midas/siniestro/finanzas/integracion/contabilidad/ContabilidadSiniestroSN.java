package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class ContabilidadSiniestroSN {
	ContabilidadSiniestroFacadeRemote beanRemoto;
	
	public ContabilidadSiniestroSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en ContabilidadSiniestroSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ContabilidadSiniestroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado ContabilidadSiniestroSN", Level.FINEST, null);
	}
	
	public void guardar(ContabilidadSiniestroDTO contabilidadSiniestroDTO){
		beanRemoto.save(contabilidadSiniestroDTO);
	}
	
	public ContabilidadSiniestroDTO actualizar(ContabilidadSiniestroDTO contabilidadSiniestroDTO){
		return beanRemoto.update(contabilidadSiniestroDTO);
	}
	
	public ContabilidadSiniestroDTO findById(ContabilidadSiniestroId contabilidadSiniestroId){
		return beanRemoto.findById(contabilidadSiniestroId);
	}
}
