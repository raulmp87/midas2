/**
 * 
 */
package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

/**
 * @author admin
 * 
 */
public class RiesgoAfectadoSN {
	private RiesgoAfectadoFacadeRemote beanRemoto;

	public RiesgoAfectadoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en RiesgoAfectadoSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(RiesgoAfectadoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public List<RiesgoAfectadoDTO> listarRiesgosAfectadosPara(BigDecimal id)
			throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByProperty("reporteSiniestroDTO.idToReporteSiniestro", id);
	}
	
	public List<RiesgoAfectadoDTO> listarFiltrado(RiesgoAfectadoId id)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.listarFiltrado(id);		
	}
	
	public List<RiesgoAfectadoDTO> listarPorReporteSiniestro(BigDecimal idReporteSiniestro)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.findByProperty("reporteSiniestroDTO.idToReporteSiniestro", idReporteSiniestro);	
	}
	
	public Double sumaAseguradaPorReporte(BigDecimal idReporteSiniestro)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.sumaAseguradaPorReporte(idReporteSiniestro);	
	}
	
	public void guardarRiesgoAfectado(RiesgoAfectadoDTO riesgoAfectadoDTO)
	throws ExcepcionDeAccesoADatos {
		beanRemoto.save(riesgoAfectadoDTO);
	}

	public void eliminarRiesgoAfectado(RiesgoAfectadoDTO riesgoAfectadoDTO)
	throws ExcepcionDeAccesoADatos {
		beanRemoto.delete(riesgoAfectadoDTO);
	}
	
	public List<SubRamoDTO> getSubramosFromReservaEstimada(BigDecimal idToReservaEstimada){
		return beanRemoto.getSubramosFromReservaEstimada(idToReservaEstimada);
	}
	
	public Double getSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo){
		return beanRemoto.getSumaAseguradaPorReporteYSubramo(idReporteSiniestro, idSubramo);
	}
	
	public Double getPorcentajeSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo){
		return beanRemoto.getPorcentajeSumaAseguradaPorReporteYSubramo(idReporteSiniestro, idSubramo);
	}	
	
	public BigDecimal getIncisoPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.getIncisoPorReportePoliza(idToReporteSiniestro, idToPoliza);
	}	
	
	public List<RiesgoAfectadoId> getSubIncisoSeccionPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza,BigDecimal numeroInciso)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.getSubIncisoSeccionPorReportePoliza(idToReporteSiniestro, idToPoliza,numeroInciso);
	}
	
	public RiesgoAfectadoDTO actualizarRiesgoAfectado(RiesgoAfectadoDTO riesgoAfectadoDTO)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.update(riesgoAfectadoDTO);
	}
	
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.getCoberturasRiesgoPorReporte(idToReporteSiniestro);
	}
	
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro,Byte estatus)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.getCoberturasRiesgoPorReporte(idToReporteSiniestro,estatus);
	}
	
	public RiesgoAfectadoDTO findById(RiesgoAfectadoId id)
	throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);		
	}
}


