package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.util.List;

import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleBean;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class IndemnizacionForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String idReporteSiniestro;
	private String tipoIndemnizacion;
	private List<ReservaDetalleBean> listaRiesgo;
	private List<ReservaDetalleBean> listaReserva;
	private List<ReservaDetalleBean> listaDetalleIndemnizaciones;	
	private List<RegistroIndemnizacion> listaIndemnizaciones;
	private String tipoMoneda;
	private String habilitaDeducible;
	private String habilitaCoaseguro;
	private String[] perdidas;
	private String[] deducibles;
	private String[] coaseguros;
	private String[] indemnizaciones;
	private Double[] estimacionesReserva;
	private String fechaPago;
	private String beneficiario;
	private String comentarios;
	private String habilitaIVA;
	private Double porcentajeIVA;
	private String montoIVA;
	private String habilitaIVARet;
	private Double porcentajeIVARet;
	private String montoIVARet;
	private String habilitaISR;
	private Double porcentajeISR;
	private String montoISR;
	private String habilitaISRRet;
	private Double porcentajeISRRet;
	private String montoISRRet;
	private String habilitaOtros;
	private Double porcentajeOtros;
	private String montoOtros;
	private Double totalSumaAsegurada;
	private Double totalReserva;
	private String totalPerdidas;
	private String totalDeducibles;
	private String totalCoaseguros;
	private String totalIndemnizaciones;
	private String idIndemnizacion;
	private String ultimoPago;
	private String estatusIndemnizacion;
	
	private String permitePagoParcial;
	private String habilitaUltimoPago;
	
	public String getIdReporteSiniestro() {
		return idReporteSiniestro;
	}
	public void setIdReporteSiniestro(String idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}
	public String getTipoIndemnizacion() {
		return tipoIndemnizacion;
	}
	public void setTipoIndemnizacion(String tipoIndemnizacion) {
		this.tipoIndemnizacion = tipoIndemnizacion;
	}
	public List<ReservaDetalleBean> getListaDetalleIndemnizaciones() {
		return listaDetalleIndemnizaciones;
	}
	public void setListaDetalleIndemnizaciones(
			List<ReservaDetalleBean> listaDetalleIndemnizaciones) {
		this.listaDetalleIndemnizaciones = listaDetalleIndemnizaciones;
	}
	public List<ReservaDetalleBean> getListaRiesgo() {
		return listaRiesgo;
	}
	
	public void setListaRiesgo(List<ReservaDetalleBean> listaRiesgo) {
		this.listaRiesgo = listaRiesgo;
	}
	
	public List<ReservaDetalleBean> getListaReserva() {
		return listaReserva;
	}
	
	public void setListaReserva(List<ReservaDetalleBean> listaReserva) {
		this.listaReserva = listaReserva;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public String getHabilitaDeducible() {
		return habilitaDeducible;
	}
	public void setHabilitaDeducible(String habilitaDeducible) {
		this.habilitaDeducible = habilitaDeducible;
	}
	public String getHabilitaCoaseguro() {
		return habilitaCoaseguro;
	}
	public void setHabilitaCoaseguro(String habilitaCoaseguro) {
		this.habilitaCoaseguro = habilitaCoaseguro;
	}
	public String[] getPerdidas() {
		return perdidas;
	}
	public void setPerdidas(String[] perdidas) {
		this.perdidas = perdidas;
	}
	public String[] getDeducibles() {
		return deducibles;
	}
	public void setDeducibles(String[] deducibles) {
		this.deducibles = deducibles;
	}
	public String[] getCoaseguros() {
		return coaseguros;
	}
	public void setCoaseguros(String[] coaseguros) {
		this.coaseguros = coaseguros;
	}
	public String[] getIndemnizaciones() {
		return indemnizaciones;
	}
	public void setIndemnizaciones(String[] indemnizaciones) {
		this.indemnizaciones = indemnizaciones;
	}
	public String getFechaPago() {
		return fechaPago;
	}
	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getComentarios() {
		return comentarios;
	}
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}
	public String getHabilitaIVA() {
		return habilitaIVA;
	}
	public void setHabilitaIVA(String habilitaIVA) {
		this.habilitaIVA = habilitaIVA;
	}
	public Double getPorcentajeIVA() {
		return porcentajeIVA;
	}
	public void setPorcentajeIVA(Double porcentajeIVA) {
		this.porcentajeIVA = porcentajeIVA;
	}
	public String getMontoIVA() {
		return montoIVA;
	}
	public void setMontoIVA(String montoIVA) {
		this.montoIVA = montoIVA;
	}
	public String getHabilitaIVARet() {
		return habilitaIVARet;
	}
	public void setHabilitaIVARet(String habilitaIVARet) {
		this.habilitaIVARet = habilitaIVARet;
	}
	public Double getPorcentajeIVARet() {
		return porcentajeIVARet;
	}
	public void setPorcentajeIVARet(Double porcentajeIVARet) {
		this.porcentajeIVARet = porcentajeIVARet;
	}
	public String getMontoIVARet() {
		return montoIVARet;
	}
	public void setMontoIVARet(String montoIVARet) {
		this.montoIVARet = montoIVARet;
	}
	public String getHabilitaISR() {
		return habilitaISR;
	}
	public void setHabilitaISR(String habilitaISR) {
		this.habilitaISR = habilitaISR;
	}
	public Double getPorcentajeISR() {
		return porcentajeISR;
	}
	public void setPorcentajeISR(Double porcentajeISR) {
		this.porcentajeISR = porcentajeISR;
	}
	public String getMontoISR() {
		return montoISR;
	}
	public void setMontoISR(String montoISR) {
		this.montoISR = montoISR;
	}
	public String getHabilitaISRRet() {
		return habilitaISRRet;
	}
	public void setHabilitaISRRet(String habilitaISRRet) {
		this.habilitaISRRet = habilitaISRRet;
	}
	public Double getPorcentajeISRRet() {
		return porcentajeISRRet;
	}
	public void setPorcentajeISRRet(Double porcentajeISRRet) {
		this.porcentajeISRRet = porcentajeISRRet;
	}
	public String getMontoISRRet() {
		return montoISRRet;
	}
	public void setMontoISRRet(String montoISRRet) {
		this.montoISRRet = montoISRRet;
	}
	public String getHabilitaOtros() {
		return habilitaOtros;
	}
	public void setHabilitaOtros(String habilitaOtros) {
		this.habilitaOtros = habilitaOtros;
	}
	public Double getPorcentajeOtros() {
		return porcentajeOtros;
	}
	public void setPorcentajeOtros(Double porcentajeOtros) {
		this.porcentajeOtros = porcentajeOtros;
	}
	public String getMontoOtros() {
		return montoOtros;
	}
	public void setMontoOtros(String montoOtros) {
		this.montoOtros = montoOtros;
	}
	public Double getTotalSumaAsegurada() {
		return totalSumaAsegurada;
	}
	public void setTotalSumaAsegurada(Double totalSumaAsegurada) {
		this.totalSumaAsegurada = totalSumaAsegurada;
	}
	public Double getTotalReserva() {
		return totalReserva;
	}
	public void setTotalReserva(Double totalReserva) {
		this.totalReserva = totalReserva;
	}
	public String getTotalPerdidas() {
		return totalPerdidas;
	}
	public void setTotalPerdidas(String totalPerdidas) {
		this.totalPerdidas = totalPerdidas;
	}
	public String getTotalDeducibles() {
		return totalDeducibles;
	}
	public void setTotalDeducibles(String totalDeducibles) {
		this.totalDeducibles = totalDeducibles;
	}
	public String getTotalCoaseguros() {
		return totalCoaseguros;
	}
	public void setTotalCoaseguros(String totalCoaseguros) {
		this.totalCoaseguros = totalCoaseguros;
	}
	public String getTotalIndemnizaciones() {
		return totalIndemnizaciones;
	}
	public void setTotalIndemnizaciones(String totalIndemnizaciones) {
		this.totalIndemnizaciones = totalIndemnizaciones;
	}
	
	public String getIdIndemnizacion() {
		return idIndemnizacion;
	}
	public void setIdIndemnizacion(String idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}
	public List<RegistroIndemnizacion> getListaIndemnizaciones() {
		return listaIndemnizaciones;
	}
	public void setListaIndemnizaciones(List<RegistroIndemnizacion> listaIndemnizaciones) {
		this.listaIndemnizaciones = listaIndemnizaciones;
	}
	public String getUltimoPago() {
		return ultimoPago;
	}
	public void setUltimoPago(String ultimoPago) {
		this.ultimoPago = ultimoPago;
	}
	public Double[] getEstimacionesReserva() {
		return estimacionesReserva;
	}
	public void setEstimacionesReserva(Double[] estimacionesReserva) {
		this.estimacionesReserva = estimacionesReserva;
	}
	public void setEstatusIndemnizacion(String estatusIndemnizacion) {
		this.estatusIndemnizacion = estatusIndemnizacion;
	}
	public String getEstatusIndemnizacion() {
		return estatusIndemnizacion;
	}
	public String getPermitePagoParcial() {
		return permitePagoParcial;
	}
	public void setPermitePagoParcial(String permitePagoParcial) {
		this.permitePagoParcial = permitePagoParcial;
	}
	public String getHabilitaUltimoPago() {
		return habilitaUltimoPago;
	}
	public void setHabilitaUltimoPago(String habilitaUltimoPago) {
		this.habilitaUltimoPago = habilitaUltimoPago;
	}
	
}
