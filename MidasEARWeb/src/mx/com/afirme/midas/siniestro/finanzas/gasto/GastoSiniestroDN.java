package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class GastoSiniestroDN {
	private static final GastoSiniestroDN INSTANCIA = new GastoSiniestroDN();

	public static GastoSiniestroDN getInstancia() {
		return INSTANCIA;
	}

	public void agregarGasto(GastoSiniestroDTO gastoSiniestroDTO) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();
		gastoSiniestroSN.agregarGasto(gastoSiniestroDTO);
	}

	public void actualizarGasto(GastoSiniestroDTO gastoSiniestroDTO) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();
		gastoSiniestroSN.actualizarGasto(gastoSiniestroDTO);
	}

	public void eliminarGasto(GastoSiniestroDTO gastoSiniestroDTO) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();
		gastoSiniestroSN.eliminarGasto(gastoSiniestroDTO);
	}

	public GastoSiniestroDTO findById(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();
		return gastoSiniestroSN.findById(id);
	}
	
	public List<GastoSiniestroDTO> listarGastosSiniestro(BigDecimal idReporteSiniestro) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();	
		return gastoSiniestroSN.listarGastosSiniestro(idReporteSiniestro);		
	}	
	
	public List<GastoSiniestroDTO> listarGastosPendientes() throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();	
		return gastoSiniestroSN.listarGastosPendientes();		
	}
	
	public List<GastoSiniestroDTO> gastosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();	
		return gastoSiniestroSN.getGastosPorReporteYEstatus(idReporteSiniestro,params);		
	}
	
	public List<GastoSiniestroDTO> gastosNoCanceladosPorReporte(BigDecimal idReporteSiniestro) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();	
		return gastoSiniestroSN.getGastosPorReporteYEstatus(idReporteSiniestro,EstatusFinanzasDTO.ABIERTO, EstatusFinanzasDTO.AUTORIZADO, EstatusFinanzasDTO.ORDEN_PAGO, EstatusFinanzasDTO.PAGADO, EstatusFinanzasDTO.POR_AUTORIZAR);		
	}
	
	public List<GastoSiniestroDTO> listarGastosPendientes(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();	
		return gastoSiniestroSN.listarGastosPendientes(reporteSiniestroDTO);		
	}
	
	public List<GastoSiniestroDTO> getGastosNoCancelados(BigDecimal idReporteSiniestro) throws SystemException, ExcepcionDeAccesoADatos {
		GastoSiniestroSN gastoSiniestroSN = new GastoSiniestroSN();	
		return gastoSiniestroSN.getGastosNoCancelados(idReporteSiniestro);		
	}
	
}
