package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class GastosAction extends MidasMappingDispatchAction {

	public ActionForward gastosAjuste(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		GastosForm forma = (GastosForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String tipoMoneda = "";
		try {
			tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		forma.setTipoMoneda(tipoMoneda);
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward modificarGasto(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;

		GastosForm forma = (GastosForm) form;
								
		GastoSiniestroDTO gastoSiniestroDTO = new GastoSiniestroDTO();
		BigDecimal idGastoSiniestro = new BigDecimal(request.getParameter("idGastoSiniestro"));
		
		try {
			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
			gastoSiniestroDTO = gastoSiniestroDN.findById(idGastoSiniestro);
			llenarForm(gastoSiniestroDTO, forma);
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ReporteSiniestroDTO reporteSiniestroDTO = gastoSiniestroDTO.getReporteSiniestroDTO(); 
			if (reporteSiniestroDTO!=null && reporteSiniestroDTO.getIdToReporteSiniestro()!=null){
				//String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
				String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(reporteSiniestroDTO.getIdToReporteSiniestro());
				forma.setTipoMoneda(tipoMoneda);
			}
			
			boolean modoLectura = this.isGastoSoloLectura(gastoSiniestroDTO);
			request.setAttribute("modoLectura", modoLectura);
			
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}					

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarGastos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		GastosForm forma = (GastosForm) form;
		
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		List<GastoSiniestroDTO> gastosSiniestro = null;
		
		String idToReporteSiniestro = request.getParameter("idToReporteSiniestro"); 
		
		if (UtileriasWeb.esCadenaVacia(idToReporteSiniestro)){
			idToReporteSiniestro = forma.getIdToReporteSiniestro();
		}
		try {
			gastosSiniestro = gastoSiniestroDN.listarGastosSiniestro(new BigDecimal(idToReporteSiniestro));			
			gastosSiniestro = asignarNombrePrestadorServicios(gastosSiniestro);
			
			forma.setListaGastos(gastosSiniestro);
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
			forma.setTipoMoneda(tipoMoneda);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward eliminarGastos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		GastoSiniestroDTO gastoSiniestroDTO = null;
				
		GastosForm forma = (GastosForm) form;
		
		String[] strIdsGastoSiniestro = forma.getEliminarGasto();
		
		for(String strIdGastoSiniestro : strIdsGastoSiniestro){
			BigDecimal idGastoSiniestro = new BigDecimal(strIdGastoSiniestro);
			
			try{				
				List<AutorizacionTecnicaDTO>  listaATs = autorizacionTecnicaDN.findByProperty("gastoSiniestroDTO.idToGastoSiniestro", idGastoSiniestro);
				gastoSiniestroDTO = gastoSiniestroDN.findById(idGastoSiniestro);
				if(listaATs != null && listaATs.size() > 0){
					EstatusFinanzasDTO estatusFinanzasDTO = gastoSiniestroDTO.getEstatusFinanzas();
					estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
					gastoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
					gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
				}else{
					
					gastoSiniestroDN.eliminarGasto(gastoSiniestroDTO);
				}
			} catch (ExcepcionDeAccesoADatos e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			} catch (SystemException e) {
				UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			}							
		}
								
		return mapping.findForward(reglaNavegacion);		
	}

	public ActionForward cancelarGastos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;

		GastosForm forma = (GastosForm) form;		
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		GastoSiniestroDTO gastoSiniestroDTO = null;
		
		String[] strIdsGastoSiniestro = forma.getCancelarGasto();

		try{				
			for(String strIdGastoSiniestro : strIdsGastoSiniestro) {
				BigDecimal idGastoSiniestro = new BigDecimal(strIdGastoSiniestro);
				gastoSiniestroDTO = gastoSiniestroDN.findById(idGastoSiniestro);
				
				if(gastoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas() != EstatusFinanzasDTO.PAGADO){

					if(siniestroMovimientosDN.cancelaMovimientoCompleto(gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), idGastoSiniestro, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO)){
						EstatusFinanzasDTO estatusFinanzasDTO = gastoSiniestroDTO.getEstatusFinanzas();
						estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
						gastoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
						gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
					}
				}
			}
			List<GastoSiniestroDTO> listaGastoSiniestro = null; 
			
			listaGastoSiniestro = gastoSiniestroDN.listarGastosSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
			forma.setListaGastos(listaGastoSiniestro );
		} catch (ExcepcionDeAccesoADatos e) {			
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}							
		
		return mapping.findForward(reglaNavegacion);		
	}
	
	public void guardarGastoSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		String reglaNavegacion = Sistema.EXITOSO;		
				 
		GastoSiniestroDTO gastoSiniestroDTO = new GastoSiniestroDTO();
		EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
		GastosForm forma = (GastosForm) form;
		String idToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		String mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar");
		
		try {
			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
			llenarDTO(gastoSiniestroDTO, forma);
			Date fechaActual = Calendar.getInstance().getTime();
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			gastoSiniestroDTO.setFechaCreacion(fechaActual);
			gastoSiniestroDTO.setIdTcUsuarioCreacion(new BigDecimal(usuario.getId()));
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.ABIERTO);			
			gastoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
			
			if (!UtileriasWeb.esCadenaVacia(idToReporteSiniestro)){
				ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(idToReporteSiniestro));
				reporteSiniestroDTO = reporteSiniestroDN.getPorId(reporteSiniestroDTO);
				gastoSiniestroDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
			}
			gastoSiniestroDN.agregarGasto(gastoSiniestroDTO);
			UtileriasWeb.imprimeMensajeXML("1", mensaje , response);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		}
	}
	
	public void guardarCambiosGastoSiniestro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		String mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar");

		GastosForm forma = (GastosForm) form;		
		GastoSiniestroDTO gastoSiniestroDTO;
		
		try {
			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
			gastoSiniestroDTO = gastoSiniestroDN.findById(new BigDecimal(forma.getIdToGastoSiniestro()));
			llenarDTO(gastoSiniestroDTO, forma);
			Date fechaActual = Calendar.getInstance().getTime();
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			gastoSiniestroDTO.setFechaModificacion(fechaActual);
			gastoSiniestroDTO.setIdTcUsuarioModificacion(new BigDecimal(usuario.getId()));			
			gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
			UtileriasWeb.imprimeMensajeXML("1", mensaje , response);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);			
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		}						
		
		//return mapping.findForward(reglaNavegacion);
	}
	
	private void llenarDTO(GastoSiniestroDTO gastoSiniestroDTO, GastosForm forma){		
		if(forma.getIdToReporteSiniestro() != null){
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
			gastoSiniestroDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		}
		
		if(forma.getFechaGasto() != null){
			Date fecha = null;
			try {
				fecha = UtileriasWeb.getFechaFromString(forma.getFechaGasto());				
			} catch (ParseException e) {
				e.printStackTrace();
			}			
			gastoSiniestroDTO.setFechaGasto(fecha);
		}
		
		if((forma.getFechaRecepcion() != null) && (forma.getFechaRecepcion().length() > 0)){			
			Date fecha = null;
			try {
				fecha = UtileriasWeb.getFechaFromString(forma.getFechaRecepcion());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gastoSiniestroDTO.setFechaRecepcion(fecha);
		}
		
		if(forma.getFechaEstimacionPago()!= null && forma.getFechaEstimacionPago().length() > 0){
			Date fecha = null;
			try {
				fecha = UtileriasWeb.getFechaFromString(forma.getFechaEstimacionPago());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gastoSiniestroDTO.setFechaEstimacionPago(fecha);
		}
		
		if(forma.getFechaEntrega()!= null && forma.getFechaEntrega().length() > 0){
			Date fecha = null;
			try {
				fecha = UtileriasWeb.getFechaFromString(forma.getFechaEntrega());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			gastoSiniestroDTO.setFechaEntrega(fecha);
		}
						
		if(forma.getMontoGasto() != null){
			Double montoGasto = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoGasto());
			gastoSiniestroDTO.setMontoGasto(montoGasto);
		}		
				
		 //valores de Porcentajes y montos
		if(forma.isIndIva()){	
			Double montoIVA = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIva());
			
			gastoSiniestroDTO.setMontoIVA(montoIVA);
			gastoSiniestroDTO.setPorcentajeIVA(new Double(forma.getPorcentajeIva()));
		}else{
			gastoSiniestroDTO.setMontoIVA(null);
			gastoSiniestroDTO.setPorcentajeIVA(null);
		}		
		if(forma.isIndIvaRet()){
			Double montoIvaRetencion = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIvaRetencion());
			
			gastoSiniestroDTO.setMontoIVARetencion(montoIvaRetencion);
			gastoSiniestroDTO.setPorcentajeIVARetencion(new Double(forma.getPorcentajeIvaRetencion()));
		}else{
			gastoSiniestroDTO.setMontoIVARetencion(null);
			gastoSiniestroDTO.setPorcentajeIVARetencion(null);
		}
		if(forma.isIndIsr()){			
			Double montoISR = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIsr());
			
			gastoSiniestroDTO.setMontoISR(montoISR);
			gastoSiniestroDTO.setPorcentajeISR(new Double(forma.getPorcentajeIsr()));
		}else{
			gastoSiniestroDTO.setMontoISR(null);
			gastoSiniestroDTO.setPorcentajeISR(null);
		}
		
		if(forma.isIndIsrRet()){
			Double montoISRRetencion = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIsrRetencion());
			
			gastoSiniestroDTO.setMontoISRRetencion(montoISRRetencion);
			gastoSiniestroDTO.setPorcentajeISRRetencion(new Double(forma.getPorcentajeIsrRetencion()));
		}else{
			gastoSiniestroDTO.setMontoISRRetencion(null);
			gastoSiniestroDTO.setPorcentajeISRRetencion(null);
		}
		
		if(forma.isIndOtros()){
			Double montoOtros = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoOtros());
			
			gastoSiniestroDTO.setMontoOtros(montoOtros);
			gastoSiniestroDTO.setPorcentajeOtros(new Double(forma.getPorcentajeOtros()));
		}else{
			gastoSiniestroDTO.setMontoOtros(null);
			gastoSiniestroDTO.setPorcentajeOtros(null);
		}
		//Fin valores de Porcentajes y montos
		
		if(forma.getConceptoGasto() != null){
			ConceptoGastoDTO conceptoGastoDTO = new ConceptoGastoDTO();
			conceptoGastoDTO.setIdTcConceptoGasto(new BigDecimal(forma.getConceptoGasto()));
			gastoSiniestroDTO.setConceptoGasto(conceptoGastoDTO);			
		}
		
		if(forma.getPrestadorServicios() != null){
			gastoSiniestroDTO.setIdTcPrestadorServicios(new BigDecimal(forma.getPrestadorServicios()));
		}
		
		if(forma.getDescripcionGasto() != null){
			String descripcion = forma.getDescripcionGasto();
			if(descripcion.length() > 240){
				gastoSiniestroDTO.setDescripcion( descripcion.substring(0, 239) );
			}else{
				gastoSiniestroDTO.setDescripcion(descripcion);
			}			
		}
		
		if(forma.getNumeroCheque() != null && forma.getNumeroCheque().length() > 0){
			gastoSiniestroDTO.setNumeroCheque(new BigDecimal(forma.getNumeroCheque()));
		}
		
		if(forma.getNumeroTransferencia() != null && forma.getNumeroTransferencia().length() > 0){
			gastoSiniestroDTO.setNumeroTransferencia(new BigDecimal(forma.getNumeroTransferencia()));
		}
		
		if(forma.getLugarEnvio() != null && forma.getLugarEnvio().length() > 0){
			gastoSiniestroDTO.setLugarEnvio(forma.getLugarEnvio());
		}			
	}
	
	private void llenarForm(GastoSiniestroDTO gastoSiniestroDTO, GastosForm forma){
		DecimalFormat formateadorMonto = new DecimalFormat("########0.00");
		if(gastoSiniestroDTO.getIdToGastoSiniestro() != null ){
			forma.setIdToGastoSiniestro(gastoSiniestroDTO.getIdToGastoSiniestro().toString());			
		}
				
//		if(gastoSiniestroDTO.getReporteSiniestroDTO() != null){
//			forma.setIdToReporteSiniestro(gastoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro().toString());
//		}
		
		if(gastoSiniestroDTO.getFechaGasto() != null){
			forma.setFechaGasto(UtileriasWeb.getFechaString(gastoSiniestroDTO.getFechaGasto()));
		}
		
		if(gastoSiniestroDTO.getFechaRecepcion() != null){
			forma.setFechaRecepcion(UtileriasWeb.getFechaString(gastoSiniestroDTO.getFechaRecepcion()));
		}
		
		if(gastoSiniestroDTO.getFechaEstimacionPago() != null){
			forma.setFechaEstimacionPago(UtileriasWeb.getFechaString(gastoSiniestroDTO.getFechaEstimacionPago()));
		}
		
		if(gastoSiniestroDTO.getFechaEntrega() != null){
			forma.setFechaEntrega(UtileriasWeb.getFechaString(gastoSiniestroDTO.getFechaEntrega()));
		}
		
		if(gastoSiniestroDTO.getMontoGasto() != null){
			forma.setMontoGasto(UtileriasWeb.formatoMoneda(gastoSiniestroDTO.getMontoGasto()));
		}
		
		if(gastoSiniestroDTO.getMontoIVA() != null){
			forma.setMontoIva(UtileriasWeb.formatoMoneda(gastoSiniestroDTO.getMontoIVA()));
			forma.setIndIva(true);
			forma.setPorcentajeIva(gastoSiniestroDTO.getPorcentajeIVA().toString());
		}
		
		if(gastoSiniestroDTO.getMontoIVARetencion() != null){
			forma.setMontoIvaRetencion(UtileriasWeb.formatoMoneda(gastoSiniestroDTO.getMontoIVARetencion()));
			forma.setIndIvaRet(true);
			forma.setPorcentajeIvaRetencion(gastoSiniestroDTO.getPorcentajeIVARetencion().toString());
		}
		
		if(gastoSiniestroDTO.getMontoISR() != null){
			forma.setMontoIsr(UtileriasWeb.formatoMoneda(gastoSiniestroDTO.getMontoISR()));
			forma.setIndIsr(true);
			forma.setPorcentajeIsr(gastoSiniestroDTO.getPorcentajeISR().toString());
		}
		
		if(gastoSiniestroDTO.getMontoISRRetencion() != null){
			forma.setMontoIsrRetencion(UtileriasWeb.formatoMoneda(gastoSiniestroDTO.getMontoISRRetencion()));
			forma.setIndIsrRet(true);
			forma.setPorcentajeIsrRetencion(gastoSiniestroDTO.getPorcentajeISRRetencion().toString());
		}
		
		if(gastoSiniestroDTO.getMontoOtros() != null){
			forma.setMontoOtros(UtileriasWeb.formatoMoneda(gastoSiniestroDTO.getMontoOtros()));
			forma.setIndOtros(true);
			forma.setPorcentajeOtros(gastoSiniestroDTO.getPorcentajeOtros().toString());
		}
						
		if(gastoSiniestroDTO.getConceptoGasto() != null){
			forma.setConceptoGasto(gastoSiniestroDTO.getConceptoGasto().getIdTcConceptoGasto().toString());
		}		
		if(gastoSiniestroDTO.getIdTcPrestadorServicios() != null){
			forma.setPrestadorServicios(gastoSiniestroDTO.getIdTcPrestadorServicios().toString());
		}		
		if(gastoSiniestroDTO.getDescripcion() != null){
			forma.setDescripcionGasto(gastoSiniestroDTO.getDescripcion());
		}
		if(gastoSiniestroDTO.getNumeroCheque() != null){
			forma.setNumeroCheque(gastoSiniestroDTO.getNumeroCheque().toString());
		}		
		if(gastoSiniestroDTO.getNumeroTransferencia() != null){
			forma.setNumeroTransferencia(gastoSiniestroDTO.getNumeroTransferencia().toString());
		}		
		if(gastoSiniestroDTO.getLugarEnvio() != null){
			forma.setLugarEnvio(gastoSiniestroDTO.getLugarEnvio());
		}
	}
 	
	private List<GastoSiniestroDTO> asignarNombrePrestadorServicios(List<GastoSiniestroDTO> gastosDeSiniestro) throws SystemException{
		List<GastoSiniestroDTO> gastos = gastosDeSiniestro;
			
		for(GastoSiniestroDTO gasto : gastosDeSiniestro){						
			BigDecimal idPrestadorServicios = gasto.getIdTcPrestadorServicios();
			PrestadorServiciosDTO prestadorServicios = null;
			
			prestadorServicios = getPrestadorServicios(idPrestadorServicios, "ADMIN");
			
			if(prestadorServicios != null){
				gasto.setNombrePrestadorServicios(prestadorServicios.getNombrePrestador());
			}else{
				gasto.setNombrePrestadorServicios("");
			}
			
			//gastos.add(gasto);
		}
		
		return gastos;			
	}
	
	private PrestadorServiciosDTO getPrestadorServicios(BigDecimal idPrestadorServicios, String nombreUsuario) throws SystemException{
		PrestadorServiciosDTO prestadorDeSerivicios = null;
		
		PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN.getInstancia();
		prestadorDeSerivicios = prestadorServiciosDN.detallePrestador(idPrestadorServicios, nombreUsuario);
		
		return prestadorDeSerivicios;
		
	}
	
	private boolean isGastoSoloLectura(GastoSiniestroDTO gastoSiniestro){
		boolean result = true;
		
		result = gastoSiniestro.getEstatusFinanzas().getIdTcEstatusfinanzas().shortValue() != EstatusFinanzasDTO.ABIERTO.shortValue();
		
		return result;
	}
}
