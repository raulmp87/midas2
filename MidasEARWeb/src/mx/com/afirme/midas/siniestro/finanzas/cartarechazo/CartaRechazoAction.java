package mx.com.afirme.midas.siniestro.finanzas.cartarechazo;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.tipodocumentosiniestro.TipoDocumentoSiniestroDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteDN;
import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteDTO;
import mx.com.afirme.midas.siniestro.documentos.DocumentoSiniestroDN;
import mx.com.afirme.midas.siniestro.documentos.DocumentoSiniestroDTO;
import mx.com.afirme.midas.siniestro.documentos.DocumentoSiniestroForm;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class CartaRechazoAction extends MidasMappingDispatchAction {
	
	public ActionForward asociarCartaRechazo(ActionMapping mapping, ActionForm form,
											HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		String numPoliza ="";
		try {
			//Se carga los datos del reporte a la forma
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			if(reporteSiniestroDTO.getNumeroPoliza() != null){
				numPoliza = soporteDanosDN.getNumeroPoliza(reporteSiniestroDTO.getNumeroPoliza());
			}
			documentoSiniestroForm.setNumeroPoliza(numPoliza);
			//Se pone el tipo de documento de carta de rechazo
			documentoSiniestroForm.setIdTipoDocumento(String.valueOf(TipoDocumentoSiniestroDTO.CARTA_RECHAZO));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);

	}
	
	public ActionForward mostrarAprobarCartaRechazo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		String numPoliza ="";
		try {
			//Se carga los datos del reporte a la forma
			ReporteSiniestroDTO reporteSiniestroDTO =reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			//Se carga la carta de rechazo
			DocumentoSiniestroDTO documentoSiniestroDTO = documentoSiniestroDN.obtenDocumentoSiniestro(documentoSiniestroForm.getReporteSiniestroDTO().getIdToReporteSiniestro(),new BigDecimal(TipoDocumentoSiniestroDTO.CARTA_RECHAZO));
			documentoSiniestroForm.setIdControlArchivo(String.valueOf(documentoSiniestroDTO.getIdToControArchivo()));
			documentoSiniestroForm.setNombreDocumento(controlArchivoDN.getPorId(documentoSiniestroDTO.getIdToControArchivo()).getNombreArchivoOriginal());
			if(reporteSiniestroDTO.getNumeroPoliza() != null){
				numPoliza = soporteDanosDN.getNumeroPoliza(reporteSiniestroDTO.getNumeroPoliza());
			}
			documentoSiniestroForm.setNumeroPoliza(numPoliza);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward aprobarCartaRechazo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			//Se carga los datos del reporte a la forma
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_IMPRIMIR_CARTA_RECHAZO);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			documentoSiniestroForm.setMensaje("La carta de rechazo fue aprobada con \u00e9xito");
			documentoSiniestroForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward rechazarCartaRechazo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			//Se carga los datos del reporte a la forma
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_MODIFICAR_CARTA_RECHAZO);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			documentoSiniestroForm.setMensaje("La carta de rechazo fue rechazada con \u00e9xito");
			documentoSiniestroForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarImprimirCartaRechazo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		String numPoliza ="";
		try {
			//Se carga los datos del reporte a la forma
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			//Se carga la carta de rechazo
			DocumentoSiniestroDTO documentoSiniestroDTO = documentoSiniestroDN.obtenDocumentoSiniestro(documentoSiniestroForm.getReporteSiniestroDTO().getIdToReporteSiniestro(),new BigDecimal(TipoDocumentoSiniestroDTO.CARTA_RECHAZO));
			documentoSiniestroForm.setIdControlArchivo(String.valueOf(documentoSiniestroDTO.getIdToControArchivo()));
			documentoSiniestroForm.setNombreDocumento(controlArchivoDN.getPorId(documentoSiniestroDTO.getIdToControArchivo()).getNombreArchivoOriginal());
			if(reporteSiniestroDTO.getNumeroPoliza() != null){
				numPoliza = soporteDanosDN.getNumeroPoliza(reporteSiniestroDTO.getNumeroPoliza());
			}
			documentoSiniestroForm.setNumeroPoliza(numPoliza);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward impresaCartaRechazo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			//Se carga los datos del reporte a la forma
			TerminoAjusteDN terminoAjusteDN = TerminoAjusteDN.getInstancia();
			TerminoAjusteDTO terminoAjusteDTO = new TerminoAjusteDTO();
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.REPORTE_TERMINADO);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaTerminadoSiniestro(new Date());
			
			terminoAjusteDTO.setIdTcTerminoAjuste(new BigDecimal(TerminoAjusteDTO.ESTATUS_RECHAZO));
			terminoAjusteDTO = terminoAjusteDN.getPorId(terminoAjusteDTO);
			reporteSiniestroDTO.setTerminoAjuste(terminoAjusteDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			documentoSiniestroForm.setMensaje("La carta de rechazo fue marcada como impresa con \u00e9xito");
			documentoSiniestroForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificarCartaRechazo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		DocumentoSiniestroForm documentoSiniestroForm = (DocumentoSiniestroForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		DocumentoSiniestroDN documentoSiniestroDN = DocumentoSiniestroDN.getInstancia();
		ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		String numPoliza ="";
		try {
			//Se carga los datos del reporte a la forma
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(documentoSiniestroForm.getIdReporteSiniestro()));
			documentoSiniestroForm.setReporteSiniestroDTO(reporteSiniestroDTO);
			//Se pone el tipo de documento de carta de rechazo
			documentoSiniestroForm.setIdTipoDocumento(String.valueOf(TipoDocumentoSiniestroDTO.CARTA_RECHAZO));
			//Se carga la carta de rechazo
			DocumentoSiniestroDTO documentoSiniestroDTO = documentoSiniestroDN.obtenDocumentoSiniestro(documentoSiniestroForm.getReporteSiniestroDTO().getIdToReporteSiniestro(),new BigDecimal(TipoDocumentoSiniestroDTO.CARTA_RECHAZO));
			documentoSiniestroForm.setIdDocumentoSiniestro(documentoSiniestroDTO.getIdToDocumentoSiniestro().toString());
			documentoSiniestroForm.setIdControlArchivo(String.valueOf(documentoSiniestroDTO.getIdToControArchivo()));
			documentoSiniestroForm.setNombreDocumento(controlArchivoDN.getPorId(documentoSiniestroDTO.getIdToControArchivo()).getNombreArchivoOriginal());
			if(reporteSiniestroDTO.getNumeroPoliza() != null){
				numPoliza = soporteDanosDN.getNumeroPoliza(reporteSiniestroDTO.getNumeroPoliza());
			}
			documentoSiniestroForm.setNumeroPoliza(numPoliza);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}

}
