package mx.com.afirme.midas.siniestro.finanzas;

public class SumaAsegurada {
	public static String getDescripcionDeTipo(byte tipo) {
		switch (tipo) {
		case 1:
			return "Basica";
		case 2:
			return "Amparada";
		case 3:
			return "Sublimite";
		default:
			throw new RuntimeException("El valor " + tipo + " para valores de tipo se encuentra fuera de rango.");
		}
	}

	public static String getDescripcionDeTipo(String tipo) {
		try {
			return getDescripcionDeTipo(Byte.parseByte(tipo));
		} catch (NumberFormatException e) {
			return "---";
		}
	}
}
