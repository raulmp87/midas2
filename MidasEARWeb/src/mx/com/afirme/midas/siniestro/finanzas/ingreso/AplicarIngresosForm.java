package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.interfaz.ingreso.referencia.ReferenciaIngresoDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class AplicarIngresosForm extends MidasBaseForm{

private static final long serialVersionUID = 1L;
	
	private String idToReporteSiniestro;
	private String idToIngresoSiniestro;
	private String descripcionIngreso;
	private String producto;
	private String numeroAutorizacionTecnica;
	private String cveAsegurado;
	private String asegurado;
	private String fechaOcurrio;
	private BigDecimal idAgente;
	private String agente;
	
	private String numeroPoliza;
	private String numeroEndoso;
	private String numeroFactura;
	private String fechaSolicitud;
	private String beneficiario;
	private String fechaEstimadaPago;
	private String conceptoIngreso;
	
	private Double montoIngreso;
	private Double montoIVA;
	private Double montoISR;
	private Double montoIVARetencion;
	private Double montoISRRetencion;
	private Double montoOtros;	
	private Double monto;
	private Double montoDeducible;
	private Double montoCoaseguro;
	private Double montoTotal;
	
	private String observaciones;
	private String referencias;
	
	private String idToReporteSiniestroFiltro;
	private String fechaCobroFiltro;
	private String montoIngresoFiltro;
	private BigDecimal conceptoIngresoFiltro;
	
	private int estatusIngreso;
	private int permiteAplicarIngreso;
	
	private String idReferenciaExterna;
	
	private List<SoporteDistribucionReaseguroDTO> detalleDistribucion;
	
	private List<ReferenciaIngresoDTO> listaReferenciasIngreso;

	
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}

	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	/**
	 * @return the idToIngresoSiniestro
	 */
	public String getIdToIngresoSiniestro() {
		return idToIngresoSiniestro;
	}

	/**
	 * @param idToIngresoSiniestro the idToIngresoSiniestro to set
	 */
	public void setIdToIngresoSiniestro(String idToIngresoSiniestro) {
		this.idToIngresoSiniestro = idToIngresoSiniestro;
	}

	/**
	 * @return the descripcionIngreso
	 */
	public String getDescripcionIngreso() {
		return descripcionIngreso;
	}

	/**
	 * @param descripcionIngreso the descripcionIngreso to set
	 */
	public void setDescripcionIngreso(String descripcionIngreso) {
		this.descripcionIngreso = descripcionIngreso;
	}

	/**
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}

	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}

	/**
	 * @return the numeroAutorizacionTecnica
	 */
	public String getNumeroAutorizacionTecnica() {
		return numeroAutorizacionTecnica;
	}

	/**
	 * @param numeroAutorizacionTecnica the numeroAutorizacionTecnica to set
	 */
	public void setNumeroAutorizacionTecnica(String numeroAutorizacionTecnica) {
		this.numeroAutorizacionTecnica = numeroAutorizacionTecnica;
	}

	/**
	 * @return the cveAsegurado
	 */
	public String getCveAsegurado() {
		return cveAsegurado;
	}

	/**
	 * @param cveAsegurado the cveAsegurado to set
	 */
	public void setCveAsegurado(String cveAsegurado) {
		this.cveAsegurado = cveAsegurado;
	}

	/**
	 * @return the asegurado
	 */
	public String getAsegurado() {
		return asegurado;
	}

	/**
	 * @param asegurado the asegurado to set
	 */
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	/**
	 * @return the fechaOcurrio
	 */
	public String getFechaOcurrio() {
		return fechaOcurrio;
	}

	/**
	 * @param fechaOcurrio the fechaOcurrio to set
	 */
	public void setFechaOcurrio(String fechaOcurrio) {
		this.fechaOcurrio = fechaOcurrio;
	}

	/**
	 * @return the idAgente
	 */
	public BigDecimal getIdAgente() {
		return idAgente;
	}

	/**
	 * @param idAgente the idAgente to set
	 */
	public void setIdAgente(BigDecimal idAgente) {
		this.idAgente = idAgente;
	}

	/**
	 * @return the agente
	 */
	public String getAgente() {
		return agente;
	}

	/**
	 * @param agente the agente to set
	 */
	public void setAgente(String agente) {
		this.agente = agente;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	/**
	 * @return the numeroEndoso
	 */
	public String getNumeroEndoso() {
		return numeroEndoso;
	}

	/**
	 * @param numeroEndoso the numeroEndoso to set
	 */
	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	/**
	 * @return the numeroFactura
	 */
	public String getNumeroFactura() {
		return numeroFactura;
	}

	/**
	 * @param numeroFactura the numeroFactura to set
	 */
	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the fechaSolicitud
	 */
	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	/**
	 * @param fechaSolicitud the fechaSolicitud to set
	 */
	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return the fechaEstimadaPago
	 */
	public String getFechaEstimadaPago() {
		return fechaEstimadaPago;
	}

	/**
	 * @param fechaEstimadaPago the fechaEstimadaPago to set
	 */
	public void setFechaEstimadaPago(String fechaEstimadaPago) {
		this.fechaEstimadaPago = fechaEstimadaPago;
	}

	/**
	 * @return the conceptoIngreso
	 */
	public String getConceptoIngreso() {
		return conceptoIngreso;
	}

	/**
	 * @param conceptoIngreso the conceptoIngreso to set
	 */
	public void setConceptoIngreso(String conceptoIngreso) {
		this.conceptoIngreso = conceptoIngreso;
	}

	/**
	 * @return the montoIngreso
	 */
	public Double getMontoIngreso() {
		return montoIngreso;
	}

	/**
	 * @param montoIngreso the montoIngreso to set
	 */
	public void setMontoIngreso(Double montoIngreso) {
		this.montoIngreso = montoIngreso;
	}

	/**
	 * @return the montoIVA
	 */
	public Double getMontoIVA() {
		return montoIVA;
	}

	/**
	 * @param montoIVA the montoIVA to set
	 */
	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}

	/**
	 * @return the montoISR
	 */
	public Double getMontoISR() {
		return montoISR;
	}

	/**
	 * @param montoISR the montoISR to set
	 */
	public void setMontoISR(Double montoISR) {
		this.montoISR = montoISR;
	}

	/**
	 * @return the montoIVARetencion
	 */
	public Double getMontoIVARetencion() {
		return montoIVARetencion;
	}

	/**
	 * @param montoIVARetencion the montoIVARetencion to set
	 */
	public void setMontoIVARetencion(Double montoIVARetencion) {
		this.montoIVARetencion = montoIVARetencion;
	}

	/**
	 * @return the montoISRRetencion
	 */
	public Double getMontoISRRetencion() {
		return montoISRRetencion;
	}

	/**
	 * @param montoISRRetencion the montoISRRetencion to set
	 */
	public void setMontoISRRetencion(Double montoISRRetencion) {
		this.montoISRRetencion = montoISRRetencion;
	}

	/**
	 * @return the montoOtros
	 */
	public Double getMontoOtros() {
		return montoOtros;
	}

	/**
	 * @param montoOtros the montoOtros to set
	 */
	public void setMontoOtros(Double montoOtros) {
		this.montoOtros = montoOtros;
	}

	/**
	 * @return the monto
	 */
	public Double getMonto() {
		return monto;
	}

	/**
	 * @param monto the monto to set
	 */
	public void setMonto(Double monto) {
		this.monto = monto;
	}

	/**
	 * @return the montoDeducible
	 */
	public Double getMontoDeducible() {
		return montoDeducible;
	}

	/**
	 * @param montoDeducible the montoDeducible to set
	 */
	public void setMontoDeducible(Double montoDeducible) {
		this.montoDeducible = montoDeducible;
	}

	/**
	 * @return the montoCoaseguro
	 */
	public Double getMontoCoaseguro() {
		return montoCoaseguro;
	}

	/**
	 * @param montoCoaseguro the montoCoaseguro to set
	 */
	public void setMontoCoaseguro(Double montoCoaseguro) {
		this.montoCoaseguro = montoCoaseguro;
	}

	/**
	 * @return the montoTotal
	 */
//	public Double getMontoTotal() {
//		return montoTotal;
//	}
	public Double getMontoTotal() {		
		double mntConcepto = 0.0;
		double mntIVA = 0.0;
		double mntISR  = 0.0;
		double mntIVARetencion  = 0.0;
		double mntISRRetencion = 0.0;
		double mntOtros = 0.0;		
		
		if(montoIngreso != null) {
			mntConcepto = montoIngreso.doubleValue(); 
		}
		if(montoIVA != null) {
			mntIVA = montoIVA.doubleValue(); 
		}
		if(montoISR != null) {
			mntISR  = montoISR.doubleValue(); 
		}
		if(montoIVARetencion != null) {
			mntIVARetencion = montoIVARetencion.doubleValue(); 
		}
		if(montoISRRetencion != null) {
			mntISRRetencion = montoISRRetencion.doubleValue(); 
		}
		if(montoOtros != null) {
			mntOtros = montoOtros.doubleValue(); 
		}			
		
		montoTotal = new Double((mntConcepto + mntIVA + mntISR +  mntOtros) - (mntISRRetencion + mntIVARetencion));
				
		return montoTotal;
	}

	/**
	 * @param montoTotal the montoTotal to set
	 */
	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the referencias
	 */
	public String getReferencias() {
		return referencias;
	}

	/**
	 * @param referencias the referencias to set
	 */
	public void setReferencias(String referencias) {
		this.referencias = referencias;
	}

	/**
	 * @return the idToReporteSiniestroFiltro
	 */
	public String getIdToReporteSiniestroFiltro() {
		return idToReporteSiniestroFiltro;
	}

	/**
	 * @param idToReporteSiniestroFiltro the idToReporteSiniestroFiltro to set
	 */
	public void setIdToReporteSiniestroFiltro(String idToReporteSiniestroFiltro) {
		this.idToReporteSiniestroFiltro = idToReporteSiniestroFiltro;
	}

	/**
	 * @return the fechaCobroFiltro
	 */
	public String getFechaCobroFiltro() {
		return fechaCobroFiltro;
	}

	/**
	 * @param fechaCobroFiltro the fechaCobroFiltro to set
	 */
	public void setFechaCobroFiltro(String fechaCobroFiltro) {
		this.fechaCobroFiltro = fechaCobroFiltro;
	}

	/**
	 * @return the montoIngresoFiltro
	 */
	public String getMontoIngresoFiltro() {
		return montoIngresoFiltro;
	}

	/**
	 * @param montoIngresoFiltro the montoIngresoFiltro to set
	 */
	public void setMontoIngresoFiltro(String montoIngresoFiltro) {
		this.montoIngresoFiltro = montoIngresoFiltro;
	}

	/**
	 * @return the conceptoIngresoFiltro
	 */
	public BigDecimal getConceptoIngresoFiltro() {
		return conceptoIngresoFiltro;
	}

	/**
	 * @param conceptoIngresoFiltro the conceptoIngresoFiltro to set
	 */
	public void setConceptoIngresoFiltro(BigDecimal conceptoIngresoFiltro) {
		this.conceptoIngresoFiltro = conceptoIngresoFiltro;
	}

	/**
	 * @return the detalleDistribucion
	 */
	public List<SoporteDistribucionReaseguroDTO> getDetalleDistribucion() {
		return detalleDistribucion;
	}

	/**
	 * @param detalleDistribucion the detalleDistribucion to set
	 */
	public void setDetalleDistribucion(
			List<SoporteDistribucionReaseguroDTO> detalleDistribucion) {
		this.detalleDistribucion = detalleDistribucion;
	}

	/**
	 * @return the estatusIngreso
	 */
	public int getEstatusIngreso() {
		return estatusIngreso;
	}

	/**
	 * @param estatusIngreso the estatusIngreso to set
	 */
	public void setEstatusIngreso(int estatusIngreso) {
		this.estatusIngreso = estatusIngreso;
	}

	/**
	 * @return the permiteAplicarIngreso
	 */
	public int getPermiteAplicarIngreso() {
		return permiteAplicarIngreso;
	}

	/**
	 * @param permiteAplicarIngreso the permiteAplicarIngreso to set
	 */
	public void setPermiteAplicarIngreso(int permiteAplicarIngreso) {
		this.permiteAplicarIngreso = permiteAplicarIngreso;
	}

	public String getIdReferenciaExterna() {
		return idReferenciaExterna;
	}

	public void setIdReferenciaExterna(String idReferenciaExterna) {
		this.idReferenciaExterna = idReferenciaExterna;
	}

	public List<ReferenciaIngresoDTO> getListaReferenciasIngreso() {
		return listaReferenciasIngreso;
	}

	public void setListaReferenciasIngreso(List<ReferenciaIngresoDTO> listaReferenciasIngreso) {
		this.listaReferenciasIngreso = listaReferenciasIngreso;
	}
}
