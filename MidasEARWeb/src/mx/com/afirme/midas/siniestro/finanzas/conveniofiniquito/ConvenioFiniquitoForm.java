package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ConvenioFiniquitoForm extends MidasBaseForm{
	/**
	 * Este formulario sirve para llenar un reporte al renombrar las variables = hacerlo en el reporte
	 * conveniofiniquito.jrxml
	 */
	private static final long serialVersionUID = 469671825544831752L;
	private String idToConvenioFiniquito;	
	private String idToReporteSiniestro;	
	private String idToIndemnizacion;
	private String numReporteSiniestro;	
	private String fechaReporte;	
	private String ajustador;
	private String numPoliza;
	private String claveProducto;
	private String claveSubRamo;
	private String claveRamo;
	private String fechaInicioVigencia;
	private String fechaFinVigencia;
	private String asegurado;
	private String descripcionDanio;
	private String indemnizacion;
	private String indemnizacionLetra;
	private String tipoCuenta;
	private String numeroCuenta;
	private String fechaConvenio;
	private String bancoReceptor;
	private String nombreBeneficiario;
	private String lugarConvenio;
	private String tipoMoneda;
	private String sumaAsegurada;
	
	private String pagoConCheque;
	private boolean seleccionadoPagoConCheque;
	private int estatus;
	
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getIdToConvenioFiniquito() {
		return idToConvenioFiniquito;
	}
	public void setIdToConvenioFiniquito(String idToConvenioFiniquito) {
		this.idToConvenioFiniquito = idToConvenioFiniquito;
	}
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public String getIdToIndemnizacion() {
		return idToIndemnizacion;
	}
	public void setIdToIndemnizacion(String idToIndemnizacion) {
		this.idToIndemnizacion = idToIndemnizacion;
	}
	public String getNumReporteSiniestro() {
		return numReporteSiniestro;
	}
	public void setNumReporteSiniestro(String numReporteSiniestro) {
		this.numReporteSiniestro = numReporteSiniestro;
	}
	public String getFechaReporte() {
		return fechaReporte;
	}
	public void setFechaReporte(String fechaReporte) {
		this.fechaReporte = fechaReporte;
	}
	public String getAjustador() {
		return ajustador;
	}
	public void setAjustador(String ajustador) {
		this.ajustador = ajustador;
	}
	public String getNumPoliza() {
		return numPoliza;
	}
	public void setNumPoliza(String numPoliza) {
		this.numPoliza = numPoliza;
	}
	public String getClaveProducto() {
		return claveProducto;
	}
	public void setClaveProducto(String claveProducto) {
		this.claveProducto = claveProducto;
	}
	public String getClaveSubRamo() {
		return claveSubRamo;
	}
	public void setClaveSubRamo(String claveSubRamo) {
		this.claveSubRamo = claveSubRamo;
	}
	public String getClaveRamo() {
		return claveRamo;
	}
	public void setClaveRamo(String claveRamo) {
		this.claveRamo = claveRamo;
	}
	public String getFechaInicioVigencia() {
		return fechaInicioVigencia;
	}
	public void setFechaInicioVigencia(String fechaInicioVigencia) {
		this.fechaInicioVigencia = fechaInicioVigencia;
	}
	public String getFechaFinVigencia() {
		return fechaFinVigencia;
	}
	public void setFechaFinVigencia(String fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}
	public String getAsegurado() {
		return asegurado;
	}
	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}
	public String getDescripcionDanio() {
		return descripcionDanio;
	}
	public void setDescripcionDanio(String descripcionDanio) {
		this.descripcionDanio = descripcionDanio;
	}
	public String getIndemnizacion() {
		return indemnizacion;
	}
	public void setIndemnizacion(String indemnizacion) {
		this.indemnizacion = indemnizacion;
	}
	public String getIndemnizacionLetra() {
		return indemnizacionLetra;
	}
	public void setIndemnizacionLetra(String indemnizacionLetra) {
		this.indemnizacionLetra = indemnizacionLetra;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getFechaConvenio() {
		return fechaConvenio;
	}
	public void setFechaConvenio(String fechaConvenio) {
		this.fechaConvenio = fechaConvenio;
	}
	public String getBancoReceptor() {
		return bancoReceptor;
	}
	public void setBancoReceptor(String bancoReceptor) {
		this.bancoReceptor = bancoReceptor;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getLugarConvenio() {
		return lugarConvenio;
	}
	public void setLugarConvenio(String lugarConvenio) {
		this.lugarConvenio = lugarConvenio;
	}
	public String getPagoConCheque() {
		return pagoConCheque;
	}
	public void setPagoConCheque(String pagoConCheque) {
		this.pagoConCheque = pagoConCheque;
	}
	public boolean isSeleccionadoPagoConCheque() {
		return seleccionadoPagoConCheque;
	}
	public void setSeleccionadoPagoConCheque(boolean seleccionadoPagoConCheque) {
		this.seleccionadoPagoConCheque = seleccionadoPagoConCheque;
	}
	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	public int getEstatus() {
		return estatus;
	}	
}