package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConvenioFiniquitoDN {
	
	public static final ConvenioFiniquitoDN INSTANCIA = new ConvenioFiniquitoDN();

	public static ConvenioFiniquitoDN getInstancia (){
		return ConvenioFiniquitoDN.INSTANCIA;
	}
	
	public ConvenioFiniquitoDTO agregar(ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN documentoSiniestroSN = new ConvenioFiniquitoSN();
		return documentoSiniestroSN.agregar(convenioFiniquitoDTO);
	}
	
	public void borrar(ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		convenioFiniquitoSN.borrar(convenioFiniquitoDTO);
	}
	
	public String modificar (ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		return convenioFiniquitoSN.modificar(convenioFiniquitoDTO);
	}
	
	public ConvenioFiniquitoDTO getPorId(ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		return convenioFiniquitoSN.getPorId(convenioFiniquitoDTO);
	}
	
	public List<ConvenioFiniquitoDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		return convenioFiniquitoSN.listarTodos();
	}
	
	public List<ConvenioFiniquitoDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		return convenioFiniquitoSN.buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public ConvenioFiniquitoDTO getPorReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		return convenioFiniquitoSN.getPorReporteSiniestro(reporteSiniestroDTO);
	}
	
	public ConvenioFiniquitoDTO obtenerConvenioCancelar(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion,Short estatus) throws ExcepcionDeAccesoADatos, SystemException{
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		return convenioFiniquitoSN.obtenerConvenioCancelar(idToReporteSiniestro, idToIndemnizacion, estatus);
	}
	
	public void cancelarConvenioFiniquito(BigDecimal idToReporteSiniestro, BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos, SystemException{
		@SuppressWarnings("unused")
		ConvenioFiniquitoSN convenioFiniquitoSN = new ConvenioFiniquitoSN();
		ConvenioFiniquitoDTO convenioFiniquitoDTO = this.obtenerConvenioCancelar(idToReporteSiniestro, idToIndemnizacion, EstatusFinanzasDTO.CANCELADO);
		convenioFiniquitoDTO.setEstatus(Sistema.CANCELADO);
		this.modificar(convenioFiniquitoDTO);
		this.modificar(convenioFiniquitoDTO);
	}

}
