package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IndemnizacionDN {
	
	public static final IndemnizacionDN INSTANCIA = new IndemnizacionDN();

	public static IndemnizacionDN getInstancia (){
		return IndemnizacionDN.INSTANCIA;
	}
	
	public IndemnizacionDTO agregar(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.agregar(indemnizacionDTO);
	}
	
	public String borrar (IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.borrar(indemnizacionDTO);
	}
	
	public IndemnizacionDTO modificar (IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.modificar(indemnizacionDTO);
	}
	
	public IndemnizacionDTO getIndemnizacionPorId(BigDecimal idIndemnizacion) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.getIndemnizacionPorId(idIndemnizacion);
	}
	
	public List<IndemnizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.listarTodos();
	}
	
	public List<IndemnizacionDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public IndemnizacionDTO obtenIndemnizacionPorAutorizar(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.obtenIndemnizacionPorAutorizar(idReporteSiniestro);
	}
	
	public List<IndemnizacionDTO> buscaPagosParciales(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.buscaPagosParciales(idReporteSiniestro);
	}
	
	public List<RegistroIndemnizacion> listaPagosParciales(BigDecimal idReporteSiniestro,IndemnizacionForm indemnizacionForm) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		//Se obtiene la lista de indemnizaciones
		List<IndemnizacionDTO> listadoIndemnizaciones=buscaPagosParciales(idReporteSiniestro);
		List<RegistroIndemnizacion> listadoRegistrosIndemnizacion = new ArrayList<RegistroIndemnizacion>();
		//Se genera la lista de registros en base a la lista de indemnizacion
		for(IndemnizacionDTO indemnizacionDTO :listadoIndemnizaciones){
			RegistroIndemnizacion registroIndemnizacion= new RegistroIndemnizacion();
			registroIndemnizacion.setIndemnizacionDTO(indemnizacionDTO);
			//Se obtiene el detalle de esta indemnizacion
			List<IndemnizacionRiesgoCoberturaDTO> listaDetalle = indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
			//Se obtienen los totales
			registroIndemnizacion.setTotalPerdida(indemnizacionRiesgoCoberturaDN.obtenerTotalPerdidas(listaDetalle));
			registroIndemnizacion.setTotalDeducible(indemnizacionRiesgoCoberturaDN.obtenerTotalDeducibles(listaDetalle));
			registroIndemnizacion.setTotalCoaseguro(indemnizacionRiesgoCoberturaDN.obtenerTotalCoaseguros(listaDetalle));
			registroIndemnizacion.setTotalIndemnizacion(indemnizacionRiesgoCoberturaDN.obtenerTotalIndemnizaciones(listaDetalle));
			
			
			if(indemnizacionDTO.getUltimoPago()){
				if(indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().shortValue()  != EstatusFinanzasDTO.CANCELADO.shortValue()){
					indemnizacionForm.setPermitePagoParcial("false");
				}
				registroIndemnizacion.setTipoPago("Ultimo Pago");
			}else{
				registroIndemnizacion.setTipoPago("Pago Parcial");
			}
			listadoRegistrosIndemnizacion.add(registroIndemnizacion);
		}
		return listadoRegistrosIndemnizacion;
	}
	
	public boolean tienePagosParciales(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.tienePagosParciales(idReporteSiniestro);
	}
	
	public List<IndemnizacionDTO> buscarPorReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO)
		throws SystemException, ExcepcionDeAccesoADatos {
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.buscarPorReporteSiniestro(reporteSiniestroDTO);
	}
	
	public List<IndemnizacionDTO> obtenIndemnizacionPorEstatusNOCancelado(ReporteSiniestroDTO reporteSiniestroDTO)
		throws SystemException, ExcepcionDeAccesoADatos {
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.obtenIndemnizacionPorEstatusNOCancelado(reporteSiniestroDTO);
	}
	
	public IndemnizacionDTO obtenerUltimoPagoUnicoPago(BigDecimal idReporteSiniestro,Short statusUnicoPago,Short statusUltimoPago) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.obtenerUltimoPagoUnicoPago(idReporteSiniestro,statusUnicoPago,statusUltimoPago);
	} 

	public List<IndemnizacionDTO> obtenerVariosPagos() throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.obtenerVariosPagos();
	}
	
//	public String[] sumaDetalleMontosPagosParciales(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
//		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
//		//Se obtiene la lista de indemnizaciones
//		List<IndemnizacionDTO> listadoIndemnizaciones=buscaPagosParciales(idReporteSiniestro);
//		ArrayList<Double> sumas = new ArrayList<Double>();
//		//Se genera la lista de registros en base a la lista de indemnizacion
//		for(IndemnizacionDTO indemnizacionDTO :listadoIndemnizaciones){
//			//Se obtiene el detalle de esta indemnizacion
//			List<IndemnizacionRiesgoCoberturaDTO> listaDetalle = indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
//			//Solo cuando no se tiene la cantidad de registros se inicializa
//			if(sumas.size() == 0){
//				for(int i=0; i < listaDetalle.size() ;i++){
//					sumas.add(new Double(0));
//				}
//			}
//			//Se obtienen las sumas
//			for(int i=0; i < listaDetalle.size() ;i++){
//				IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO = listaDetalle.get(i);
//				double suma = sumas.get(i).doubleValue() + indemnizacionRiesgoCoberturaDTO.getMontoPago().doubleValue()
//								+ indemnizacionRiesgoCoberturaDTO.getDeducible().doubleValue() + indemnizacionRiesgoCoberturaDTO.getCoaseguro().doubleValue();
////				double suma = sumas.get(i).doubleValue() + indemnizacionRiesgoCoberturaDTO.get().doubleValue();
//				sumas.set(i, new Double(suma));
//			}
//		}
//		//Se pasa el Arraylist a un arreglo de Strings
//		String[] sumaIndemnizaciones = new String[sumas.size()];
//		for(int i=0; i < sumas.size() ;i++){
//			sumaIndemnizaciones[i] = String.valueOf(sumas.get(i));
//		}
//		return sumaIndemnizaciones;
//	}
	
//	public String[] sumaDetalleMontosPagosParcialesModificar(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
//		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
//		//Se obtiene la lista de indemnizaciones
//		List<IndemnizacionDTO> listadoIndemnizaciones=buscaPagosParcialesSinUltimoPago(idReporteSiniestro);
//		ArrayList<Double> sumas = new ArrayList<Double>();
//		//Se genera la lista de registros en base a la lista de indemnizacion
//		for(IndemnizacionDTO indemnizacionDTO :listadoIndemnizaciones){
//			//Se obtiene el detalle de esta indemnizacion
//			List<IndemnizacionRiesgoCoberturaDTO> listaDetalle = indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
//			//Solo cuando no se tiene la cantidad de registros se inicializa
//			if(sumas.size() == 0){
//				for(int i=0; i < listaDetalle.size() ;i++){
//					sumas.add(new Double(0));
//				}
//			}
//			//Se obtienen las sumas
//			for(int i=0; i < listaDetalle.size() ;i++){
//				IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO = listaDetalle.get(i);
//				double suma = sumas.get(i).doubleValue() + indemnizacionRiesgoCoberturaDTO.getMontoPago().doubleValue()
//								+ indemnizacionRiesgoCoberturaDTO.getDeducible().doubleValue() + indemnizacionRiesgoCoberturaDTO.getCoaseguro().doubleValue();
////				double suma = sumas.get(i).doubleValue() + indemnizacionRiesgoCoberturaDTO.get().doubleValue();
//				sumas.set(i, new Double(suma));
//			}
//		}
//		//Se pasa el Arraylist a un arreglo de Strings
//		String[] sumaIndemnizaciones = new String[sumas.size()];
//		for(int i=0; i < sumas.size() ;i++){
//			sumaIndemnizaciones[i] = String.valueOf(sumas.get(i));
//		}
//		return sumaIndemnizaciones;
//	}
	
	public List<IndemnizacionDTO> getIndemnizacionPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params)  throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.getIndemnizacionPorReporteYEstatus(idReporteSiniestro,params);
	}
	
	public List<IndemnizacionDTO> getIndemnizacionesPendientes(ReporteSiniestroDTO reporteSiniestroDTO)  throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.getIndemnizacionPendientes(reporteSiniestroDTO);
	}
	
	public IndemnizacionDTO obtenerUltimoPago(BigDecimal idToReporteSiniestro)  throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.obtenerUltimoPago(idToReporteSiniestro);
	}
	
	public IndemnizacionDTO obtenerUltimoPagoGenerado(BigDecimal idToReporteSiniestro)  throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionSN indemnizacionSN = new IndemnizacionSN();
		return indemnizacionSN.obtenerUltimoPagoGenerado(idToReporteSiniestro);
	} 
	public String validaIndemnizacionesTerminarReporte(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		List<IndemnizacionDTO> listaIndemnizaciones = this.buscarPorReporteSiniestro(reporteSiniestroDTO);
		String mensajeError = "";
		short estatusIndemnizacion = 0;
		boolean ultimoPago = false;
		
		if (listaIndemnizaciones==null || listaIndemnizaciones.size()<=0) return mensajeError;
		
		for (IndemnizacionDTO indemnizacionDTO:listaIndemnizaciones){
			estatusIndemnizacion = indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().shortValue();
			if (indemnizacionDTO.getUltimoPago()){
				if (estatusIndemnizacion != EstatusFinanzasDTO.PAGADO){
					mensajeError = "Debe pagar el &uacute;ltimo pago";
					break;
				}
				ultimoPago = true;
			}else{
				if (estatusIndemnizacion !=EstatusFinanzasDTO.PAGADO.shortValue() && estatusIndemnizacion!=EstatusFinanzasDTO.CANCELADO.shortValue() ){
					mensajeError = "No deben existir indemnizaciones pendientes";
					break;
				}
			}
		}
		
		if (!ultimoPago && mensajeError.length()<=0){
			mensajeError = "Debe generar el &uacute;ltimo pago";
		}
		
		return mensajeError;
	}
}
