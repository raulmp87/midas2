package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;

import java.math.BigDecimal;
import java.util.List;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class AutorizacionTecnicaForm extends MidasBaseForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idToReporteSiniestro;
	private int idToConcepto;
	private String descripcionConcepto;
	private String numeroReporte;
	private String producto;
	private int numeroAutorizacionTecnica;
	private String cveAutorizacionTecnica;
	private String cveAsegurado;
	private String asegurado;
	private String numeroPoliza;
	private String numeroEndoso;
	private String fechaOcurrio;
	private BigDecimal idAgente;
	private String agente;
	private String numeroFactura;
	private String fechaSolicitud;
	private String cvePrestadorServicios; 
	private String beneficiario;
	private String fechaEstimadaPago;
	private String conceptoPago; 	
	private Double montoConcepto;
	private Double montoIVA;
	private Double montoISR;
	private Double montoIVARetencion;
	private Double montoISRRetencion;
	private Double montoOtros;	
	private Double monto;
	private Double montoDeducible;
	private Double montoCoaseguro;
	private Double montoTotal;
	
	private String codigoUsuarioCreacion;
	private String nombreUsuarioCreacion;
    private String fechaCreacion;
    private String codigoUsuarioAutoriza;
    private String nombreUsuarioAutoriza;
    private String fechaAutorizacion;    
    
    private int permitirAutorizar;
    private int permitirCancelar;
    private String idOrdenPago;
    private BigDecimal idSolicitudCheque;
	
	public BigDecimal getIdSolicitudCheque() {
		return idSolicitudCheque;
	}

	public void setIdSolicitudCheque(BigDecimal idSolicitudCheque) {
		this.idSolicitudCheque = idSolicitudCheque;
	}

	public String getIdOrdenPago() {
		return idOrdenPago;
	}

	public void setIdOrdenPago(String idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}

	public String getNombreUsuarioCreacion() {
		return nombreUsuarioCreacion;
	}

	public void setNombreUsuarioCreacion(String nombreUsuarioCreacion) {
		this.nombreUsuarioCreacion = nombreUsuarioCreacion;
	}

	public String getNombreUsuarioAutoriza() {
		return nombreUsuarioAutoriza;
	}

	public void setNombreUsuarioAutoriza(String nombreUsuarioAutoriza) {
		this.nombreUsuarioAutoriza = nombreUsuarioAutoriza;
	}

	public String getCveAutorizacionTecnica() {
		return cveAutorizacionTecnica;
	}

	public void setCveAutorizacionTecnica(String cveAutorizacionTecnica) {
		this.cveAutorizacionTecnica = cveAutorizacionTecnica;
	}

	public String getCodigoUsuarioCreacion() {
		return codigoUsuarioCreacion;
	}

	public void setCodigoUsuarioCreacion(String codigoUsuarioCreacion) {
		this.codigoUsuarioCreacion = codigoUsuarioCreacion;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getCodigoUsuarioAutoriza() {
		return codigoUsuarioAutoriza;
	}

	public void setCodigoUsuarioAutoriza(String codigoUsuarioAutoriza) {
		this.codigoUsuarioAutoriza = codigoUsuarioAutoriza;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	private AutorizacionTecnicaDTO autorizacionTecnica;
	private int idTipoAutorizacionTecnica;
	private List<SoporteDistribucionReaseguroDTO> detalleDistribucion;
	private List<AutorizacionTecnicaDTO> autorizacionesTecnicas;
		
	private int pantalla;
		
	public void setIdToReporteSiniestro(int idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	public int getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getProducto() {
		return producto;
	}

	public void setNumeroAutorizacionTecnica(int numeroAutorizacionTecnica) {
		this.numeroAutorizacionTecnica = numeroAutorizacionTecnica;
	}

	public int getNumeroAutorizacionTecnica() {
		return numeroAutorizacionTecnica;
	}

	public void setCveAsegurado(String cveAsegurado) {
		this.cveAsegurado = cveAsegurado;
	}

	public String getCveAsegurado() {
		return cveAsegurado;
	}

	public void setAsegurado(String asegurado) {
		this.asegurado = asegurado;
	}

	public String getAsegurado() {
		return asegurado;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroEndoso(String numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public String getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setFechaOcurrio(String fechaOcurrio) {
		this.fechaOcurrio = fechaOcurrio;
	}

	public String getFechaOcurrio() {
		return fechaOcurrio;
	}

	public void setIdAgente(BigDecimal idAgente) {
		this.idAgente = idAgente;
	}

	public BigDecimal getIdAgente() {
		return idAgente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getAgente() {
		return agente;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setFechaSolicitud(String fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public String getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setCvePrestadorServicios(String cvePrestadorServicios) {
		this.cvePrestadorServicios = cvePrestadorServicios;
	}

	public String getCvePrestadorServicios() {
		return cvePrestadorServicios;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setFechaEstimadaPago(String fechaEstimadaPago) {
		this.fechaEstimadaPago = fechaEstimadaPago;
	}

	public String getFechaEstimadaPago() {
		return fechaEstimadaPago;
	}

	public void setConceptoPago(String conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	public String getConceptoPago() {
		return conceptoPago;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMontoConcepto(Double montoConcepto) {
		this.montoConcepto = montoConcepto;
	}

	public Double getMontoConcepto() {
		return montoConcepto;
	}

	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}

	public Double getMontoIVA() {
		return montoIVA;
	}

	public void setMontoISR(Double montoISR) {
		this.montoISR = montoISR;
	}

	public Double getMontoISR() {
		return montoISR;
	}

	public void setMontoIVARetencion(Double montoIVARetencion) {
		this.montoIVARetencion = montoIVARetencion;
	}

	public Double getMontoIVARetencion() {
		return montoIVARetencion;
	}

	public void setMontoISRRetencion(Double montoISRRetencion) {
		this.montoISRRetencion = montoISRRetencion;
	}

	public Double getMontoISRRetencion() {
		return montoISRRetencion;
	}

	public void setMontoOtros(Double montoOtros) {
		this.montoOtros = montoOtros;
	}

	public Double getMontoOtros() {
		return montoOtros;
	}

	public void setIdToConcepto(int idToConcepto) {
		this.idToConcepto = idToConcepto;
	}

	public int getIdToConcepto() {
		return idToConcepto;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setMontoDeducible(Double montoDeducible) {
		this.montoDeducible = montoDeducible;
	}

	public Double getMontoDeducible() {
		return montoDeducible;
	}

	public void setMontoCoaseguro(Double montoCoaseguro) {
		this.montoCoaseguro = montoCoaseguro;
	}

	public Double getMontoCoaseguro() {
		return montoCoaseguro;
	}

	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Double getMontoTotal() {		
		double mntConcepto = 0.0;
		double mntIVA = 0.0;
		double mntISR  = 0.0;
		double mntIVARetencion  = 0.0;
		double mntISRRetencion = 0.0;
		double mntOtros = 0.0;		
		double mntCoaseguro = 0.0;
		double mntDeducible = 0.0;		
		
		if(montoConcepto != null) {
			mntConcepto = montoConcepto.doubleValue(); 
		}
		if(montoIVA != null) {
			mntIVA = montoIVA.doubleValue(); 
		}
		if(montoISR != null) {
			mntISR  = montoISR.doubleValue(); 
		}
		if(montoIVARetencion != null) {
			mntIVARetencion = montoIVARetencion.doubleValue(); 
		}
		if(montoISRRetencion != null) {
			mntISRRetencion = montoISRRetencion.doubleValue(); 
		}
		if(montoOtros != null) {
			mntOtros = montoOtros.doubleValue(); 
		}			
		
		if(montoCoaseguro != null) {
			mntCoaseguro = montoCoaseguro.doubleValue(); 
		}			
		
		if(montoDeducible != null) {
			mntDeducible = montoDeducible.doubleValue(); 
		}			
						
		if(this.idTipoAutorizacionTecnica == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION){
			montoTotal = new Double((mntConcepto + mntIVA + mntISR +  mntOtros) - (mntISRRetencion + mntIVARetencion + mntCoaseguro + mntDeducible));
		}else{
			montoTotal = new Double((mntConcepto + mntIVA + mntISR +  mntOtros) - (mntISRRetencion + mntIVARetencion));
		}		
				
		return montoTotal;
	}

	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}

	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}

	public void setPantalla(int pantalla) {
		this.pantalla = pantalla;
	}

	public int getPantalla() {
		return pantalla;
	}

	public void setAutorizacionTecnica(AutorizacionTecnicaDTO autorizacionTecnica) {
		this.autorizacionTecnica = autorizacionTecnica;
	}

	public AutorizacionTecnicaDTO getAutorizacionTecnica() {
		return autorizacionTecnica;
	}

	public void setIdTipoAutorizacionTecnica(int idTipoAutorizacionTecnica) {
		this.idTipoAutorizacionTecnica = idTipoAutorizacionTecnica;
	}

	public int getIdTipoAutorizacionTecnica() {
		return idTipoAutorizacionTecnica;
	}

	public void setDetalleDistribucion(List<SoporteDistribucionReaseguroDTO> detalleDistribucion) {
		this.detalleDistribucion = detalleDistribucion;
	}

	public List<SoporteDistribucionReaseguroDTO> getDetalleDistribucion() {
		return detalleDistribucion;
	}

	public void setAutorizacionesTecnicas(List<AutorizacionTecnicaDTO> autorizacionesTecnicas) {
		this.autorizacionesTecnicas = autorizacionesTecnicas;
	}

	public List<AutorizacionTecnicaDTO> getAutorizacionesTecnicas() {
		return autorizacionesTecnicas;
	}
	
	public JRBeanCollectionDataSource getDetalleDistribucionJasper() {
		return new JRBeanCollectionDataSource(detalleDistribucion);
	}

	public void setPermitirAutorizar(int permitirAutorizar) {
		this.permitirAutorizar = permitirAutorizar;
	}

	public int getPermitirAutorizar() {
		return permitirAutorizar;
	}

	public void setPermitirCancelar(int permitirCancelar) {
		this.permitirCancelar = permitirCancelar;
	}

	public int getPermitirCancelar() {
		return permitirCancelar;
	}
}
