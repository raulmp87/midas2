package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConceptoGastoDN {
	private static final ConceptoGastoDN INSTANCIA = new ConceptoGastoDN();
	
	public static ConceptoGastoDN getInstancia(){
		return INSTANCIA;
	}

	public void agregarConceptoGasto(ConceptoGastoDTO conceptoGastoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ConceptoGastoSN conceptoGastoSN = new ConceptoGastoSN();
		conceptoGastoSN.agregarConceptoGasto(conceptoGastoDTO);
	}
	
	public void eliminarConceptoGasto(ConceptoGastoDTO conceptoGastoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ConceptoGastoSN conceptoGastoSN = new ConceptoGastoSN();
		conceptoGastoSN.eliminarConceptoGasto(conceptoGastoDTO);	
	}
	
	public ConceptoGastoDTO actualizarConceptoGasto(ConceptoGastoDTO conceptoGastoDTO) throws SystemException, ExcepcionDeAccesoADatos {
		ConceptoGastoSN conceptoGastoSN = new ConceptoGastoSN();
		return conceptoGastoSN.actualizarConceptoGasto(conceptoGastoDTO);
	}
	
	public ConceptoGastoDTO findById(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos {
		ConceptoGastoSN conceptoGastoSN = new ConceptoGastoSN();
		return conceptoGastoSN.findById(id);
	}
 
	public List<ConceptoGastoDTO> findAll() throws SystemException, ExcepcionDeAccesoADatos {
		ConceptoGastoSN conceptoGastoSN = new ConceptoGastoSN();
		return conceptoGastoSN.findAll();
	} 
	
}
