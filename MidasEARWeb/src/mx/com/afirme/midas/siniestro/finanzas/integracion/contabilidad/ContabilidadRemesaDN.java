package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.interfaz.remesa.RemesaDN;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.AplicacionIngresoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.AplicarIngresoDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.ConceptoIngresoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class ContabilidadRemesaDN {
	private static final ContabilidadRemesaDN INSTANCIA = new ContabilidadRemesaDN();
	private static final BigDecimal ID_MOVIMIENTO_SALVAMENTO = new BigDecimal(2);
	private static final BigDecimal ID_MOVIMIENTO_RECUPERACION = new BigDecimal(3);
	private static final BigDecimal ID_MOVIMIENTO_INGRESO = new BigDecimal(4);
	
	public static ContabilidadRemesaDN getInstancia(){
		return INSTANCIA;
	}
	
    public void guardar(ContabilidadRemesaDTO entity) throws SystemException{
    	ContabilidadRemesaSN contabilidadRemesaSN = new ContabilidadRemesaSN();
    	contabilidadRemesaSN.guardar(entity);    	
    }
    
    public void borrar(ContabilidadRemesaDTO entity) throws SystemException{
    	ContabilidadRemesaSN contabilidadRemesaSN = new ContabilidadRemesaSN();
    	contabilidadRemesaSN.borrar(entity);
    }

    public ContabilidadRemesaDTO actualizar(ContabilidadRemesaDTO entity) throws SystemException{
    	ContabilidadRemesaSN contabilidadRemesaSN = new ContabilidadRemesaSN();
    	return contabilidadRemesaSN.actualizar(entity);
    }
	
	public ContabilidadRemesaDTO findById( ContabilidadRemesaId id) throws SystemException{
		ContabilidadRemesaSN contabilidadRemesaSN = new ContabilidadRemesaSN();
		return contabilidadRemesaSN.findById(id);
	}		
	
	public List<ContabilidadRemesaDTO> buscarMovimientosPendientesActualizar(BigDecimal idToAplicacionIngreso) throws SystemException{
		ContabilidadRemesaSN contabilidadRemesaSN = new ContabilidadRemesaSN();
		return contabilidadRemesaSN.buscarMovimientosPendientesActualizar(idToAplicacionIngreso);
	}
	
	public BigDecimal contabilizarRemesa(IngresoSiniestroDTO ingresoSiniestroDTO, Usuario user,String conceptoMovimiento,String observaciones, String referencias, String idReferenciaExterna) throws SystemException{
		SoporteContabilidad soporteContabilidad = SoporteContabilidad.getInstancia();
		ContabilidadSiniestroDN contabilidadSiniestroDN = ContabilidadSiniestroDN.getInstancia();
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		
		String conceptoMovimientoContabilizar = "";
		String conceptoMovimientoIngreso = "";
		BigDecimal resultado = null;
		
		List<RiesgoAfectadoDTO> riesgosAfectados = null;
		List<SoporteContabilidadOrdenPagoDTO> riesgosAfectadosAgrupados = null;
		BigDecimal idToReporteSiniestro = ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
		BigDecimal idTcMovimientoSiniestro = null;
		BigDecimal idTcMovimientoIngreso = ID_MOVIMIENTO_INGRESO;
		boolean contabilizar = false;
		try {
			AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionIngreso(idToReporteSiniestro, ingresoSiniestroDTO.getIdToIngresoSiniestro());
			riesgosAfectados =	soporteContabilidad.obtenerCoberturasRiesgo(idToReporteSiniestro);						
			riesgosAfectadosAgrupados =	soporteContabilidad.agruparCoberturasPorSubramo(idToReporteSiniestro, riesgosAfectados);
			
			switch(ingresoSiniestroDTO.getConceptoIngreso().getIdTcConceptoIngreso().shortValue()){		
				case ConceptoIngresoDTO.SALVAMENTO :{ 
					contabilizar = true;
					idTcMovimientoSiniestro = ID_MOVIMIENTO_SALVAMENTO;
					if(conceptoMovimiento.equals(IngresoSiniestroDTO.ALTA_INGRESO)){
						conceptoMovimientoContabilizar = IngresoSiniestroDTO.ALTA_SALVAMENTO;
						conceptoMovimientoIngreso = IngresoSiniestroDTO.INGRESO_SALVAMENTO;
					}else{
						conceptoMovimientoContabilizar = IngresoSiniestroDTO.CANCELACION_SALVAMENTO;
						conceptoMovimientoIngreso = IngresoSiniestroDTO.CANCELACION_INGRESO_SALVAMENTO;
					}
					break;
				}
				case  ConceptoIngresoDTO.RECUPERACION:{
					contabilizar = true;
					idTcMovimientoSiniestro = ID_MOVIMIENTO_RECUPERACION;
					if(conceptoMovimiento.equals(IngresoSiniestroDTO.ALTA_INGRESO)){
						conceptoMovimientoContabilizar = IngresoSiniestroDTO.ALTA_RECUPERACION;
						conceptoMovimientoIngreso = IngresoSiniestroDTO.INGRESO_RECUPERACION;
					}else{
						conceptoMovimientoContabilizar = IngresoSiniestroDTO.CANCELACION_RECUPERACION;
						conceptoMovimientoIngreso = IngresoSiniestroDTO.CANCELACION_INGRESO_RECUPERACION;
					}
					break;
				} 
				case  ConceptoIngresoDTO.DEDUCIBLE:{
					if(conceptoMovimiento.equals(IngresoSiniestroDTO.ALTA_INGRESO)){
						conceptoMovimientoIngreso = IngresoSiniestroDTO.INGRESO_DEDUCIBLE;
					}else{
						conceptoMovimientoIngreso = IngresoSiniestroDTO.CANCELACION_INGRESO_DEDUCIBLE;
					}
					break;
				} 
				case  ConceptoIngresoDTO.COASEGURO:{
					
					if(conceptoMovimiento.equals(IngresoSiniestroDTO.ALTA_INGRESO)){
						conceptoMovimientoIngreso = IngresoSiniestroDTO.INGRESO_COASEGURO;
					}else{
						conceptoMovimientoIngreso = IngresoSiniestroDTO.CANCELACION_INGRESO_COASEGURO;
					}
					break;
				} 
		}		
			
			AplicacionIngresoDTO aplicacionIngresoDTO = new AplicacionIngresoDTO();
			Date fechaMovimiento = Calendar.getInstance().getTime();
			int tipoDistribucion;
			if(conceptoMovimiento.equals(IngresoSiniestroDTO.ALTA_INGRESO)){
				if (UtileriasWeb.esCadenaVacia(idReferenciaExterna))
					throw new SystemException("Debe seleccionar una referencia del ingreso.");
				aplicacionIngresoDTO = this.registraAplicacionIngresoDTO(observaciones, referencias, autorizacionTecnicaDTO, user.getId(),idReferenciaExterna);
				//04/03/2011 Se modifica el valor "tipoDistribucion" para que tome los conceptos de reaseguro usados para indicar a seycos
				//que se trata de un movimiento por ingreso y no por retención
				tipoDistribucion = IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_INGRESO;
			}else{
				aplicacionIngresoDTO = this.cancelarAplicacionIngresoDTO(observaciones, referencias, autorizacionTecnicaDTO, user.getId());
				tipoDistribucion = IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION_POR_INGRESO;
			}
			try{//agregar el nuevo campo en estos registros
				contabilidadSiniestroDN.contabilizarIngreso(ingresoSiniestroDTO, aplicacionIngresoDTO.getIdToAplicacionIngreso(), riesgosAfectadosAgrupados, user.getNombreUsuario(), conceptoMovimientoIngreso, idTcMovimientoIngreso);
			}catch (SystemException ex) {
				LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
				ex.printStackTrace();
			}
			
			siniestroMovimientosDN.distribuirReaseguro(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica(),
						tipoDistribucion,autorizacionTecnicaDTO.getObjectTipoAutorizacionTecnica(),fechaMovimiento,user.getNombreUsuario());
			
			if(aplicacionIngresoDTO.getIdToAplicacionIngreso() != null){
				if(contabilizar){
					try{
						contabilidadSiniestroDN.contabilizarIngreso(ingresoSiniestroDTO, aplicacionIngresoDTO.getIdToAplicacionIngreso(), riesgosAfectadosAgrupados, user.getNombreUsuario(), conceptoMovimientoContabilizar, idTcMovimientoSiniestro);
					}catch (SystemException ex) {
						LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
						ex.printStackTrace();
					}
				}
				if(conceptoMovimiento.equals(IngresoSiniestroDTO.ALTA_INGRESO)){
					this.modificaAplicacionIngreso(aplicacionIngresoDTO, AplicacionIngresoDTO.CONTABILIZADO);
				}else{
					this.modificaAplicacionIngreso(aplicacionIngresoDTO, AplicacionIngresoDTO.CANCELADO);
				}
					
			}
		} catch (ExcepcionDeAccesoADatos ead) {
			throw ead;
		} catch (SystemException se) {
			throw se;
		}		
		return resultado;
	}	
	
	private AplicacionIngresoDTO registraAplicacionIngresoDTO(String observaciones,String referencias, AutorizacionTecnicaDTO autorizacionTecnicaDTO,Integer idTcUsuario,String idReferenciaExterna) throws SystemException{
		AplicarIngresoDN  aplicarIngresoDN = AplicarIngresoDN.getInstancia();
		AplicacionIngresoDTO aplicacionIngresoDTO = new AplicacionIngresoDTO();
		aplicacionIngresoDTO.setAutorizacionTecnicaDTO(autorizacionTecnicaDTO);
		aplicacionIngresoDTO.setEstatus(AplicacionIngresoDTO.PENDIENTE);
		Date fechaAplicoIngreso = Calendar.getInstance().getTime();
		aplicacionIngresoDTO.setFechaAplicacoIngreso(fechaAplicoIngreso);
		aplicacionIngresoDTO.setIdTcUsuarioAplicoIngreso(new BigDecimal(idTcUsuario));
		aplicacionIngresoDTO.setObservacionAplicacion(observaciones);
		aplicacionIngresoDTO.setReferenciasAplicacion(referencias);
		aplicacionIngresoDTO.setIdReferenciaExterna(idReferenciaExterna);
		
		aplicacionIngresoDTO = aplicarIngresoDN.agregarAplicacionIngreso(aplicacionIngresoDTO);
		return aplicacionIngresoDTO;
	}	
	
	private AplicacionIngresoDTO cancelarAplicacionIngresoDTO(String observaciones,String referencias, AutorizacionTecnicaDTO autorizacionTecnicaDTO,Integer idTcUsuario) throws SystemException{
		AplicarIngresoDN  aplicarIngresoDN = AplicarIngresoDN.getInstancia();
		
		AplicacionIngresoDTO aplicacionIngresoDTO = aplicarIngresoDN.buscarAplicarIngresoPorAT(autorizacionTecnicaDTO.getIdToAutorizacionTecnica(), AplicacionIngresoDTO.CANCELADO);
		aplicacionIngresoDTO.setObservacionAplicacion(observaciones);
		aplicacionIngresoDTO.setReferenciasAplicacion(referencias);
		aplicacionIngresoDTO.setIdTcUsuarioCancelacion(new BigDecimal(idTcUsuario));
		Date fechaCancelacionIngreso = Calendar.getInstance().getTime();
		aplicacionIngresoDTO.setFechaCancelacion(fechaCancelacionIngreso);
		
		aplicacionIngresoDTO = aplicarIngresoDN.actualizar(aplicacionIngresoDTO);
		return aplicacionIngresoDTO;
	}	
	
//	private List<ContabilidadRemesaDTO> generarMovimientosRemesa(IngresoSiniestroDTO ingresoSiniestroDTO,AplicacionIngresoDTO aplicacionIngresoDTO, List<SoporteContabilidadOrdenPagoDTO> riesgosAgrupados, String usuario,String cveConceptoMov) throws ExcepcionDeAccesoADatos, SystemException{
//		List <ContabilidadRemesaDTO> movimientosRemesa = new ArrayList<ContabilidadRemesaDTO>();
//		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
//		
//		BigDecimal idReporteSiniestro = ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro();
//		String numeroPoliza = soporteDanosDN.getNumeroPoliza(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza());
//		SoporteContabilidad soporteContabilidad = SoporteContabilidad.getInstancia();		
//		
//		PolizaSoporteDanosDTO poliza = null;	
//		Double totalSumaAsegurada = soporteContabilidad.getSumaAseguradaPorReporte(idReporteSiniestro);
//		Double tipoDeCambio = null;
//		Double importeCero = new Double(0.00);
//		String fechaAplico = UtileriasWeb.getFechaString(Calendar.getInstance().getTime());
//		String[] fecha = fechaAplico.split("/");
//		
//		for(SoporteContabilidadOrdenPagoDTO riesgo : riesgosAgrupados){
//			
//			if(poliza == null){
//				poliza = soporteContabilidad.getPolizaPorId(riesgo.getIdPoliza());
//				tipoDeCambio = soporteContabilidad.getTipoCambio(poliza.getIdMoneda().shortValue(), usuario);
//			}
//			
//			ContabilidadRemesaId contabilidadRemesaId = new ContabilidadRemesaId();
//			contabilidadRemesaId.setCveConceptoMov(cveConceptoMov);
//			contabilidadRemesaId.setIdSubr(riesgo.getCodigoSubRamo());
//			contabilidadRemesaId.setIdToAplicacionIngreso(aplicacionIngresoDTO.getIdToAplicacionIngreso());
//			
//			ContabilidadRemesaDTO contabilidadRemesaDTO = new ContabilidadRemesaDTO();
//			contabilidadRemesaDTO.setId(contabilidadRemesaId);
//			contabilidadRemesaDTO.setIdToReporteSiniestro(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
//			contabilidadRemesaDTO.setIdRamo(riesgo.getCodigoRamo());
//			
//			contabilidadRemesaDTO.setConceptoPol(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte()+"/"+numeroPoliza);
//			contabilidadRemesaDTO.setIdMoneda(poliza.getIdMoneda());
//			contabilidadRemesaDTO.setTipoCambio(tipoDeCambio); 
//			contabilidadRemesaDTO.setAuxiliar(new Integer(SoporteContabilidad.AUXULIAR)); 
//			String conceptoMovimiento = "Movimiento "+ fecha[2] +"/"+ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte()+"/"+riesgo.getCodigoSubRamo()+"/"+ingresoSiniestroDTO.getIdToIngresoSiniestro()+"/"+ingresoSiniestroDTO.getConceptoIngreso().getDescripcion(); 
//			contabilidadRemesaDTO.setConceptoMov(conceptoMovimiento);
//			contabilidadRemesaDTO.setCcosto(SoporteContabilidad.CENTRO_COSTOS);
//			contabilidadRemesaDTO.setUsuario(usuario);
//			
//			double porcentajeSumaAsegurada = riesgo.getMontoSumaAsegurada() / totalSumaAsegurada.doubleValue();
//						
//			contabilidadRemesaDTO.setImpNeto(ingresoSiniestroDTO.getMonto() * porcentajeSumaAsegurada);
//			
//			if(ingresoSiniestroDTO.getMontoIVA() != null){
//				contabilidadRemesaDTO.setImpIva(ingresoSiniestroDTO.getMontoIVA() * porcentajeSumaAsegurada);
//			}else{
//				contabilidadRemesaDTO.setImpIva(importeCero);
//			}
//			
//			if(ingresoSiniestroDTO.getMontoIVARetencion() != null){
//				contabilidadRemesaDTO.setImpIvaRet(ingresoSiniestroDTO.getMontoIVARetencion() * porcentajeSumaAsegurada);
//			}else{
//				contabilidadRemesaDTO.setImpIvaRet(importeCero);
//			}
//			
//			if(ingresoSiniestroDTO.getMontoISR() != null){
//				contabilidadRemesaDTO.setImpIsr(ingresoSiniestroDTO.getMontoISR() * porcentajeSumaAsegurada);
//			}else{
//				contabilidadRemesaDTO.setImpIsr(importeCero);
//			}			
//			
//			if(ingresoSiniestroDTO.getMontoISRRetencion() != null){
//				contabilidadRemesaDTO.setImpIsrRet(ingresoSiniestroDTO.getMontoISRRetencion() * porcentajeSumaAsegurada);
//			}else{
//				contabilidadRemesaDTO.setImpIsrRet(importeCero);
//			}
//			
//			if(ingresoSiniestroDTO.getMontoOtros()!= null){
//				contabilidadRemesaDTO.setImpOtrosImpuestos(ingresoSiniestroDTO.getMontoOtros() * porcentajeSumaAsegurada);
//			}else{
//				contabilidadRemesaDTO.setImpOtrosImpuestos(importeCero);
//			}								
//									
//			movimientosRemesa.add(contabilidadRemesaDTO);
//		}												
//		
//		return movimientosRemesa;		
//	}
//	
//	private void registrarMovimientosRemesa(List<ContabilidadRemesaDTO> movimientosRemesa) throws SystemException{
//		for(ContabilidadRemesaDTO movimientoRemesa : movimientosRemesa){			
//			try{
//				this.guardar(movimientoRemesa);
//			}catch(SystemException ex){
//				throw ex;
//			}finally{
//				LogDeMidasWeb.log(movimientoRemesa.toString(), Level.INFO, null);
//			}			
//		}
//	}	
	
	private void actualizarMovimientosRemesa(List<ContabilidadRemesaDTO> movimientosRemesa,BigDecimal idRemesa) throws SystemException{
		for(ContabilidadRemesaDTO movimientoRemesa : movimientosRemesa){			
			try{
				movimientoRemesa.setIdPolizaContable(idRemesa);
				this.actualizar(movimientoRemesa);
			}catch(SystemException ex){
				throw ex;
			}finally{
				LogDeMidasWeb.log(movimientoRemesa.toString(), Level.INFO, null);
			}			
		}
	}	
	
	private void modificaAplicacionIngreso(AplicacionIngresoDTO aplicacionIngresoDTO,Short estatus) throws SystemException{
		AplicarIngresoDN aplicarIngresoDN = AplicarIngresoDN.getInstancia();
		aplicacionIngresoDTO.setEstatus(estatus);
		aplicarIngresoDN.actualizar(aplicacionIngresoDTO);
	}	

	
	public void procesarRemesasPendientes(String nombreUsuario) throws Exception{
		RemesaDN remesaDN = RemesaDN.getInstancia();
		AplicarIngresoDN aplicarIngresoDN = AplicarIngresoDN.getInstancia();
		IngresoSiniestroDN ingresoSiniestroDN  = IngresoSiniestroDN.getInstancia();
		
		BigDecimal idRemesa = null;
//		List<AplicacionIngresoDTO> listaPendientes = aplicarIngresoDN.buscarAplicarIngresoPorEstatus(AplicacionIngresoDTO.PENDIENTE);
		List<AplicacionIngresoDTO> listaPendientes = aplicarIngresoDN.buscarAplicarIngresoPendientes();
		if (listaPendientes != null) {
			for (AplicacionIngresoDTO ingreso : listaPendientes) {
				if(ingreso.getAutorizacionTecnicaDTO() != null){
					BigDecimal idToIngresoSiniestro = ingreso.getAutorizacionTecnicaDTO().getIngresoSiniestroDTO().getIdToIngresoSiniestro();
					
					LogDeMidasWeb.log("Consultando estatus actual de la Remesa con id de Aplicacion Ingreso: " + ingreso.getIdToAplicacionIngreso() + " y id de Ingreso: "+ idToIngresoSiniestro , 
							Level.INFO, null);
					
					idRemesa = remesaDN.registraRemesa(idToIngresoSiniestro, nombreUsuario);
					if(idRemesa != null){
						List<ContabilidadRemesaDTO> movimientosRemesa = this.buscarMovimientosPendientesActualizar(ingreso.getIdToAplicacionIngreso());
						this.actualizarMovimientosRemesa(movimientosRemesa, idRemesa);
						ContabilidadRemesaDTO contabilidadRemesaDTO = movimientosRemesa.get(0);
						IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(idToIngresoSiniestro);
						if(contabilidadRemesaDTO.getId().getCveConceptoMov().equals(IngresoSiniestroDTO.ALTA_INGRESO)){
							this.modificaAplicacionIngreso(ingreso, AplicacionIngresoDTO.CONTABILIZADO);
							ingresoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.PAGADO);
							
						}else{
							this.modificaAplicacionIngreso(ingreso, AplicacionIngresoDTO.CANCELADO);
							ingresoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO);
						}
						ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
						LogDeMidasWeb.log("Ejecucion exitosa en la Actualizacion de estatus la Remesa con id de Aplicacion Ingreso: " + 
								ingreso.getIdToAplicacionIngreso() + " y id de Ingreso: "+ idToIngresoSiniestro,
									Level.INFO, null);
					}
					
														
				}
			}
		}		
	}
}
