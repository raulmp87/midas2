package mx.com.afirme.midas.siniestro.finanzas;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.sistema.SystemException;

public class RegistroDeReserva {
	private String inciso;
	private String subInciso;
	private String seccion;
	private String cobertura;
	private String riesgo;
	private Double reserva;

	public RegistroDeReserva(ReservaDetalleDTO reservaDetalleDTO) {
		try{
			SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
			CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
			RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
			
			inciso = reservaDetalleDTO.getId().getNumeroinciso().toString();
			subInciso = reservaDetalleDTO.getId().getNumerosubinciso().toString();
			
			SeccionSoporteDanosDTO seccionSoporteDanosDTO = soporteDanosDN.getSeccionSoporte(reservaDetalleDTO.getId().getIdtoseccion());
			seccion = seccionSoporteDanosDTO.getDescripcionSeccion();
			
			coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(reservaDetalleDTO.getId().getIdtocobertura()); 
			cobertura = coberturaSoporteDanosDTO.getDescripcionCobertura();
			
			riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(reservaDetalleDTO.getId().getIdtoriesgo());
			riesgo = riesgoSoporteDanosDTO.getDescripcionRiesgo();
			
			reserva = reservaDetalleDTO.getEstimacion();
		}catch (SystemException ex) {ex.printStackTrace();}
	}
	public String getInciso() {
		return inciso;
	}
	public String getSubInciso() {
		return subInciso;
	}
	public String getSeccion() {
		return seccion;
	}
	public String getCobertura() {
		return cobertura;
	}
	public String getRiesgo() {
		return riesgo;
	}
	public Double getReserva() {
		return reserva;
	}
}
