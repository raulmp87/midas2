package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.siniestro.cabina.EstatusSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroFacadeRemote;

public class GastoSiniestroSN {
	private GastoSiniestroFacadeRemote beanRemoto;
	
	public GastoSiniestroSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en GastoSiniestroSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(GastoSiniestroFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado GastoSiniestroSN", Level.FINEST, null);
	}
	
	public void agregarGasto(GastoSiniestroDTO gastoSiniestroDTO){
		beanRemoto.save(gastoSiniestroDTO);
	}
	
	public void actualizarGasto(GastoSiniestroDTO gastoSiniestroDTO){
		beanRemoto.update(gastoSiniestroDTO);
	}
	
	public void eliminarGasto(GastoSiniestroDTO gastoSiniestroDTO){
		beanRemoto.delete(gastoSiniestroDTO);
	}
	
	public GastoSiniestroDTO findById(BigDecimal id){
		return beanRemoto.findById(id);
	}
	
	public List<GastoSiniestroDTO> listarGastosSiniestro(BigDecimal idReporteSiniestro){
		List<GastoSiniestroDTO> gastosSiniestro = null;
		
		gastosSiniestro = beanRemoto.findByIdReporteSiniestro(idReporteSiniestro);
		
		return gastosSiniestro;		
	}	
	
	public List<GastoSiniestroDTO> listarGastosPendientes(){
		return beanRemoto.listarGastosPendientes();
	}	
	
	public List<GastoSiniestroDTO> getGastosPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params){
		return beanRemoto.getGastosPorReporteYEstatus(idReporteSiniestro,params);
	}
	
	public List<GastoSiniestroDTO> listarGastosPendientes(ReporteSiniestroDTO reporteSiniestroDTO){
		return this.getGastosPorReporteYEstatus(reporteSiniestroDTO.getIdToReporteSiniestro(), EstatusFinanzasDTO.ABIERTO, EstatusFinanzasDTO.AUTORIZADO, 
				EstatusFinanzasDTO.ORDEN_PAGO,EstatusFinanzasDTO.POR_AUTORIZAR);
	}
	
	public List<GastoSiniestroDTO> getGastosNoCancelados(BigDecimal idReporteSiniestro){
		return beanRemoto.getGastosNoCancelados(idReporteSiniestro);
	}
}
