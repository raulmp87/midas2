package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.util.List;

import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class OrdenDePagoForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	private String idToReporteSiniestro;
	private String idToAutorizacionTecnica;
	private String tipoAutorizacionTecnica;
	private String idTipoAutorizacionTecnica;
	private String montoNeto;
	
	private String id;
	
	
	List<OrdenDePagoDTO> listaOrdenesDePago;
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	/**
	 * @return the idToAutorizacionTecnica
	 */
	public String getIdToAutorizacionTecnica() {
		return idToAutorizacionTecnica;
	}
	/**
	 * @param idToAutorizacionTecnica the idToAutorizacionTecnica to set
	 */
	public void setIdToAutorizacionTecnica(String idToAutorizacionTecnica) {
		this.idToAutorizacionTecnica = idToAutorizacionTecnica;
	}
	/**
	 * @return the tipoAutorizacionTecnica
	 */
	public String getTipoAutorizacionTecnica() {
		return tipoAutorizacionTecnica;
	}
	/**
	 * @param tipoAutorizacionTecnica the tipoAutorizacionTecnica to set
	 */
	public void setTipoAutorizacionTecnica(String tipoAutorizacionTecnica) {
		this.tipoAutorizacionTecnica = tipoAutorizacionTecnica;
	}
	/**
	 * @return the montoNeto
	 */
	public String getMontoNeto() {
		return montoNeto;
	}
	/**
	 * @param montoNeto the montoNeto to set
	 */
	public void setMontoNeto(String montoNeto) {
		this.montoNeto = montoNeto;
	}
	/**
	 * @return the listaOrdenesDePago
	 */
	public List<OrdenDePagoDTO> getListaOrdenesDePago() {
		return listaOrdenesDePago;
	}
	/**
	 * @param listaOrdenesDePago the listaOrdenesDePago to set
	 */
	public void setListaOrdenesDePago(List<OrdenDePagoDTO> listaOrdenesDePago) {
		this.listaOrdenesDePago = listaOrdenesDePago;
	}
	/**
	 * @return the idTipoAutorizacionTecnica
	 */
	public String getIdTipoAutorizacionTecnica() {
		return idTipoAutorizacionTecnica;
	}
	/**
	 * @param idTipoAutorizacionTecnica the idTipoAutorizacionTecnica to set
	 */
	public void setIdTipoAutorizacionTecnica(String idTipoAutorizacionTecnica) {
		this.idTipoAutorizacionTecnica = idTipoAutorizacionTecnica;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	
}
