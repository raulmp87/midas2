package mx.com.afirme.midas.siniestro.finanzas;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionMovSiniestroDTO;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionMovSiniestroSN;
import mx.com.afirme.midas.reaseguro.movimiento.MovimientoReaseguroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionForm;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle.DetalleOrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.siniestro.finanzas.reserva.SoporteMovimientosReservaBean;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.mail.MailAction;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.sistema.temporizador.TemporizadorDistribuirSiniestroSN;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

/**
 * @author clozada
 *
 */
public class SiniestroMovimientosDN {
	private static final SiniestroMovimientosDN INSTANCIA = new SiniestroMovimientosDN();

	public static SiniestroMovimientosDN getInstancia() {
		return SiniestroMovimientosDN.INSTANCIA;
	}
	
	/**
	 * @param idToAutorizacionTecnica
	 * @param estatus
	 * @return
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public void actualizarEstatusMovimiento(AutorizacionTecnicaDTO autorizacionTecnicaDTO,Short estatus)throws SystemException, ExcepcionDeAccesoADatos{
	
		switch(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica()){
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO: {
				GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
				GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN.findById(autorizacionTecnicaDTO.getGastoSiniestroDTO().getIdToGastoSiniestro());
				EstatusFinanzasDTO estatusFinanzasDTO = gastoSiniestroDTO.getEstatusFinanzas();
				estatusFinanzasDTO.setIdTcEstatusfinanzas(estatus);
				gastoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
				gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
				break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO:{
				IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
				IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(autorizacionTecnicaDTO.getIngresoSiniestroDTO().getIdToIngresoSiniestro());
				EstatusFinanzasDTO estatusFinanzasDTO = ingresoSiniestroDTO.getEstatusFinanzas();
				estatusFinanzasDTO.setIdTcEstatusfinanzas(estatus);
				ingresoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
				ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
				break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION: {
				IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
				IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion());
				EstatusFinanzasDTO estatusFinanzasDTO = indemnizacionDTO.getEstatusFinanzasDTO();
				estatusFinanzasDTO.setIdTcEstatusfinanzas(estatus);
				indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
				indemnizacionDN.modificar(indemnizacionDTO);
				break;
			}						
		}	
	}
	
	/**
	 * @param idToAutorizacionTecnica
	 * @param estatus
	 * @return
	 * @throws SystemException
	 * @throws ExcepcionDeAccesoADatos
	 */
	public boolean validaMovimientoPagado(AutorizacionTecnicaDTO autorizacionTecnicaDTO)throws SystemException, ExcepcionDeAccesoADatos{
		Short estatusMovimiento = 0;
		switch(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica()){
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO: {
				GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
				GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN.findById(autorizacionTecnicaDTO.getGastoSiniestroDTO().getIdToGastoSiniestro());
				estatusMovimiento = gastoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas();
				break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO:{
				IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
				IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(autorizacionTecnicaDTO.getIngresoSiniestroDTO().getIdToIngresoSiniestro());
				estatusMovimiento = ingresoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas();
				break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION: {
				IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
				IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion());
				estatusMovimiento = indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas();
				break;
			}				
		}
		if(estatusMovimiento == EstatusFinanzasDTO.PAGADO){
			return true;
		}
		return false;
	}
	
	public boolean cancelaMovimientoCompleto(BigDecimal idToReporteSiniestro,BigDecimal idMovimiento,int tipoMovimiento){
		boolean resultado = false;
		AutorizacionTecnicaDN  autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
		AutorizacionTecnicaDTO autorizacionTecnicaDTO = null;
//		int tipoDistribucion = 0;
		try{
			switch(tipoMovimiento){
				case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO: {
					autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionGasto(idToReporteSiniestro, idMovimiento);
//					tipoDistribucion = AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO;
					break;
				}
				case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO: {
					autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionIngreso(idToReporteSiniestro, idMovimiento);
//					tipoDistribucion = AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO;
					break;
				}
				case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION: {
					autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionIndemnizacion(idToReporteSiniestro, idMovimiento);
					break;
				}
			}
			
			if(autorizacionTecnicaDTO != null){
				// TODO: Habilitar en liberacion a prod OrdenDePagoDTO ordenDePagoDTO = null;ordenDePagoDN.obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica());
				OrdenDePagoDTO ordenDePagoDTO = ordenDePagoDN.obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica());
				if(ordenDePagoDTO != null){
					ordenDePagoDTO.setEstatus(OrdenDePagoDTO.ESTATUS_CANCELADA);
					ordenDePagoDN.update(ordenDePagoDTO);
				}
				
				autorizacionTecnicaDTO.setEstatus(AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
				autorizacionTecnicaDN.actualizarAutorizacionTecnica(autorizacionTecnicaDTO);
//				if(tipoMovimiento != AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION){
//					this.distribuirReaseguro(tipoDistribucion,
//							IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION,
//							autorizacionTecnicaDTO.getIngresoSiniestroDTO());
//				}
			}
			resultado = true;
		} catch (ExcepcionDeAccesoADatos e) {
			return resultado;
		} catch (SystemException e) {
			return resultado;
		}		
		return resultado;
	}
	
	public void cambiaEstatusReporte(BigDecimal idToReporteSiniestro,String nombreUsuario,Byte estatus) throws ExcepcionDeAccesoADatos, SystemException{
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(nombreUsuario);
		ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
		
		ReporteEstatusDTO  reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
		reporteEstatusDTO.setIdTcReporteEstatus(estatus);
		reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
		reporteSiniestroDN.actualizar(reporteSiniestroDTO);
		
	}
	
	public boolean isMovimientoIndemnizacion(AutorizacionTecnicaDTO autorizacionTecnicaDTO)throws SystemException, ExcepcionDeAccesoADatos{
		if(autorizacionTecnicaDTO.getIndemnizacionDTO() != null){
			return true;
		}
		return false;
	}
	
	public boolean validaMovimientosExistentes(BigDecimal idToReporteSiniestro, List<RiesgoAfectadoDTO> listaEliminar) throws ExcepcionDeAccesoADatos, SystemException{
		boolean resultado = true;
		
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia(); 
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		IndemnizacionDN indemnizacionDN =IndemnizacionDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		
		List<IngresoSiniestroDTO> listaIngresos = ingresoSiniestroDN.getIngresosPorReporteYEstatus(idToReporteSiniestro, EstatusFinanzasDTO.ABIERTO,EstatusFinanzasDTO.POR_AUTORIZAR,EstatusFinanzasDTO.AUTORIZADO,EstatusFinanzasDTO.ORDEN_PAGO);
		List<GastoSiniestroDTO> listaGastos = gastoSiniestroDN.gastosPorReporteYEstatus(idToReporteSiniestro, EstatusFinanzasDTO.ABIERTO,EstatusFinanzasDTO.POR_AUTORIZAR,EstatusFinanzasDTO.AUTORIZADO,EstatusFinanzasDTO.ORDEN_PAGO);
		List<IndemnizacionDTO> listaIndemnizaciones= indemnizacionDN.getIndemnizacionPorReporteYEstatus(idToReporteSiniestro, EstatusFinanzasDTO.ABIERTO,EstatusFinanzasDTO.POR_AUTORIZAR,EstatusFinanzasDTO.AUTORIZADO,EstatusFinanzasDTO.ORDEN_PAGO);
		
		if(listaIngresos.size() > 0){
			return false;
		}
		if(listaGastos.size() > 0){
			return false;
		}
		for(IndemnizacionDTO indemnizacionDTO: listaIndemnizaciones){
			List<IndemnizacionRiesgoCoberturaDTO> listaDetalle = indemnizacionRiesgoCoberturaDN.buscarPorIndemnizacion(indemnizacionDTO);
			for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO: listaDetalle){
				for(RiesgoAfectadoDTO riesgoAfectadoDTO: listaEliminar){
					if(comparaRiesgoyIndemnizacionRiesgoCobertura(riesgoAfectadoDTO,indemnizacionRiesgoCoberturaDTO)){
						return false;
					}
				}
			}
			
		}
		return resultado;
	}
	
	private boolean comparaRiesgoyIndemnizacionRiesgoCobertura(RiesgoAfectadoDTO riesgoAfectadoDTO,IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO){
		if(
		riesgoAfectadoDTO.getId().getIdtoreportesiniestro().equals(indemnizacionRiesgoCoberturaDTO.getId().getIdToReporteSiniestro())
		&& riesgoAfectadoDTO.getId().getIdtopoliza().equals(indemnizacionRiesgoCoberturaDTO.getId().getIdToPoliza())
		&& riesgoAfectadoDTO.getId().getNumeroinciso().equals(indemnizacionRiesgoCoberturaDTO.getId().getNumeroInciso()) 
		&& riesgoAfectadoDTO.getId().getNumerosubinciso().equals(indemnizacionRiesgoCoberturaDTO.getId().getNumeroSubinciso())
		&& riesgoAfectadoDTO.getId().getIdtoseccion().equals(indemnizacionRiesgoCoberturaDTO.getId().getIdToSeccion())
		&& riesgoAfectadoDTO.getId().getIdtocobertura().equals(indemnizacionRiesgoCoberturaDTO.getId().getIdToCobertura())
		&& riesgoAfectadoDTO.getId().getIdtoriesgo().equals(indemnizacionRiesgoCoberturaDTO.getId().getIdToRiesgo())
		&& indemnizacionRiesgoCoberturaDTO.getMontoPago() > 0
		){
			return true;
		}
		return false;
	}
	
	public boolean comparaRiesgoyDetalleReserva(RiesgoAfectadoDTO riesgoAfectadoDTO,ReservaDetalleDTO reservaDetalle){
		if(
		riesgoAfectadoDTO.getId().getIdtoreportesiniestro().equals(reservaDetalle.getId().getIdtoreportesiniestro())
		&& riesgoAfectadoDTO.getId().getIdtopoliza().equals(reservaDetalle.getId().getIdtopoliza())
		&& riesgoAfectadoDTO.getId().getNumeroinciso().equals(reservaDetalle.getId().getNumeroinciso()) 
		&& riesgoAfectadoDTO.getId().getNumerosubinciso().equals(reservaDetalle.getId().getNumerosubinciso())
		&& riesgoAfectadoDTO.getId().getIdtoseccion().equals(reservaDetalle.getId().getIdtoseccion())
		&& riesgoAfectadoDTO.getId().getIdtocobertura().equals(reservaDetalle.getId().getIdtocobertura())
		&& riesgoAfectadoDTO.getId().getIdtoriesgo().equals(reservaDetalle.getId().getIdtoriesgo())
		){
			return true;
		}
		return false;
	}
	
	public ReservaDetalleDTO comparaDetallesReserva(ReservaDetalleDTO reservaDetalle,List<ReservaDetalleDTO> listaDetalle){
		ReservaDetalleDTO reservaDetalleDTO = null;
		for(ReservaDetalleDTO detalleReserva: listaDetalle){
			if(
			detalleReserva.getId().getIdtoreportesiniestro().equals(reservaDetalle.getId().getIdtoreportesiniestro())
			&& detalleReserva.getId().getIdtopoliza().equals(reservaDetalle.getId().getIdtopoliza())
			&& detalleReserva.getId().getNumeroinciso().equals(reservaDetalle.getId().getNumeroinciso()) 
			&& detalleReserva.getId().getNumerosubinciso().equals(reservaDetalle.getId().getNumerosubinciso())
			&& detalleReserva.getId().getIdtoseccion().equals(reservaDetalle.getId().getIdtoseccion())
			&& detalleReserva.getId().getIdtocobertura().equals(reservaDetalle.getId().getIdtocobertura())
			&& detalleReserva.getId().getIdtoriesgo().equals(reservaDetalle.getId().getIdtoriesgo())
			){
				reservaDetalleDTO =  detalleReserva;
				break;
			}
		}
		return reservaDetalleDTO;
	}
	
	public StringBuffer generaMsjXml(Object id,Object valor){
		StringBuffer buffer = new StringBuffer();
			buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			buffer.append("<response>");
			buffer.append("<id>");
			buffer.append(id);
			buffer.append("</id>");
			buffer.append("<valor>");
			buffer.append(valor);
			buffer.append("</valor>");			
			buffer.append("</response>");
		return buffer;
	}

	public ReservaDTO completaUltimoPagoIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion,IndemnizacionForm indemnizacionForm,ReporteSiniestroDTO reporteSiniestroDTO,BigDecimal idToUsuarioMovimiento, String userName ) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDTO result = null;
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		
		List<RiesgoAfectadoDTO> riesgosAfectados = null;    
		List<ReservaDetalleDTO> nuevaReserva = null;
		List<ReservaDetalleDTO> reservaAutorizada = null;
		List<SoporteMovimientosReservaBean> movimientosReserva = null;
		List<ReservaDetalleDTO> reservaDetalle = new ArrayList<ReservaDetalleDTO>();
		 ReservaDTO reservaDTO = null;
		
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		riesgoAfectadoId.setIdtoreportesiniestro(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
    	riesgosAfectados = reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
		
		nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, generaPerdidasIndemnizacion(indemnizacionForm));
		reservaDTO = reservaDN.listarReservaActual(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());     
		
		reservaAutorizada = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
		movimientosReserva = reservaDN.obtenerDiferenciasReservas(reservaAutorizada, nuevaReserva);
		BigDecimal tipoAjuste = reservaDN.determinarTipoAjusteNuevaReserva(movimientosReserva);
		for(SoporteMovimientosReservaBean soporteMovimiento :  movimientosReserva){
			reservaDetalle.add(soporteMovimiento.getReservaDetalleDTO());
		}
		
		Date fechaMovimiento = Calendar.getInstance().getTime();
		ReservaDTO nuevaReservaDTO = new ReservaDTO();
		nuevaReservaDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		nuevaReservaDTO.setDescripcion("Ajuste Indemnizacion en automatico");
		nuevaReservaDTO.setTipoajuste(tipoAjuste);
		nuevaReservaDTO.setFechaestimacion(fechaMovimiento);
		nuevaReservaDTO.setFechaautorizacion(fechaMovimiento);
		nuevaReservaDTO.setIdtousuariocreacion(idToUsuarioMovimiento);
		nuevaReservaDTO.setAutorizada(Boolean.TRUE);
		nuevaReservaDTO.setIdtousuarioautoriza(idToUsuarioMovimiento);
		
		result = reservaDN.ajustarReservaeIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion, nuevaReservaDTO, reservaDetalle, userName);
		return result;
	} 
	
	public ReservaDTO completaModificarUltimoPagoIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion,IndemnizacionForm indemnizacionForm,ReporteSiniestroDTO reporteSiniestroDTO,BigDecimal idToUsuarioMovimiento, String userName ) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDTO result = null;
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		
		List<RiesgoAfectadoDTO> riesgosAfectados = null;    
		List<ReservaDetalleDTO> nuevaReserva = null;
		List<ReservaDetalleDTO> reservaAutorizada = null;
		List<SoporteMovimientosReservaBean> movimientosReserva = null;
		List<ReservaDetalleDTO> reservaDetalle = new ArrayList<ReservaDetalleDTO>();
		 ReservaDTO reservaDTO = null;
		
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		riesgoAfectadoId.setIdtoreportesiniestro(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
    	riesgosAfectados = reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
		
		nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, generaPerdidasIndemnizacionModificacion(indemnizacionForm));
		reservaDTO = reservaDN.listarReservaActual(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());     
		
		reservaAutorizada = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
		movimientosReserva = reservaDN.obtenerDiferenciasReservas(reservaAutorizada, nuevaReserva);
		BigDecimal tipoAjuste = reservaDN.determinarTipoAjusteNuevaReserva(movimientosReserva);
		for(SoporteMovimientosReservaBean soporteMovimiento :  movimientosReserva){
			reservaDetalle.add(soporteMovimiento.getReservaDetalleDTO());
		}
		
		Date fechaMovimiento = Calendar.getInstance().getTime();
		ReservaDTO nuevaReservaDTO = new ReservaDTO();
		nuevaReservaDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		nuevaReservaDTO.setDescripcion("Ajuste Indemnizacion en automatico Modificar");
		nuevaReservaDTO.setTipoajuste(tipoAjuste);
		nuevaReservaDTO.setFechaestimacion(fechaMovimiento);
		nuevaReservaDTO.setFechaautorizacion(fechaMovimiento);
		nuevaReservaDTO.setIdtousuariocreacion(idToUsuarioMovimiento);
		nuevaReservaDTO.setAutorizada(Boolean.TRUE);
		nuevaReservaDTO.setIdtousuarioautoriza(idToUsuarioMovimiento);
		
		result = reservaDN.ajustarReservaeModificacionIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion, nuevaReservaDTO, reservaDetalle, userName);
		
		return result;
	} 
	
	public ReservaDTO completaCancelarIndemnizacion(IndemnizacionDTO indemnizacionDTO,ReporteSiniestroDTO reporteSiniestroDTO,BigDecimal idToUsuarioMovimiento, String userName ) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDTO result = null;
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		
		List<RiesgoAfectadoDTO> riesgosAfectados = null;    
		List<ReservaDetalleDTO> nuevaReserva = null;
		List<ReservaDetalleDTO> reservaAutorizada = null;
		List<SoporteMovimientosReservaBean> movimientosReserva = null;
		List<ReservaDetalleDTO> reservaDetalle = new ArrayList<ReservaDetalleDTO>();
		 ReservaDTO reservaDTO = null;
		
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		riesgoAfectadoId.setIdtoreportesiniestro(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
    	riesgosAfectados = reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
    	Double[] sumaIndemninacionesBD = indemnizacionRiesgoCoberturaDN.sumaPagosParcialesModificar(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), indemnizacionDTO.getIdToIndemnizacion());
		nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, sumaIndemninacionesBD);
		reservaDTO = reservaDN.listarReservaActual(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());     
		
		reservaAutorizada = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
		movimientosReserva = reservaDN.obtenerDiferenciasReservas(reservaAutorizada, nuevaReserva);
		BigDecimal tipoAjuste = reservaDN.determinarTipoAjusteNuevaReserva(movimientosReserva);
		for(SoporteMovimientosReservaBean soporteMovimiento :  movimientosReserva){
			reservaDetalle.add(soporteMovimiento.getReservaDetalleDTO());
		}
		
		Date fechaMovimiento = Calendar.getInstance().getTime();
		ReservaDTO nuevaReservaDTO = new ReservaDTO();
		nuevaReservaDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		nuevaReservaDTO.setDescripcion("Ajuste Indemnizacion en automatico Modificar");
		nuevaReservaDTO.setTipoajuste(tipoAjuste);
		nuevaReservaDTO.setFechaestimacion(fechaMovimiento);
		nuevaReservaDTO.setFechaautorizacion(fechaMovimiento);
		nuevaReservaDTO.setIdtousuariocreacion(idToUsuarioMovimiento);
		nuevaReservaDTO.setAutorizada(Boolean.TRUE);
		nuevaReservaDTO.setIdtousuarioautoriza(idToUsuarioMovimiento);
		
		result = reservaDN.completaCancelarIndemnizacion(indemnizacionDTO, nuevaReservaDTO, reservaDetalle, userName);
		
		return result;
	} 
	
	public ReservaDTO completaEliminarIndemnizacion(IndemnizacionDTO indemnizacionDTO,ReporteSiniestroDTO reporteSiniestroDTO,BigDecimal idToUsuarioMovimiento, String userName ) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDTO result = null;
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		
		List<RiesgoAfectadoDTO> riesgosAfectados = null;    
		List<ReservaDetalleDTO> nuevaReserva = null;
		List<ReservaDetalleDTO> reservaAutorizada = null;
		List<SoporteMovimientosReservaBean> movimientosReserva = null;
		List<ReservaDetalleDTO> reservaDetalle = new ArrayList<ReservaDetalleDTO>();
		 ReservaDTO reservaDTO = null;
		
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		riesgoAfectadoId.setIdtoreportesiniestro(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
    	riesgosAfectados = reservaDN.listarCoberturasRiesgo(riesgoAfectadoId);
    	Double[] sumaIndemninacionesBD = indemnizacionRiesgoCoberturaDN.sumaPagosParcialesModificar(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), indemnizacionDTO.getIdToIndemnizacion());
		nuevaReserva = reservaDN.crearReservaDetalle(riesgosAfectados, sumaIndemninacionesBD);
		reservaDTO = reservaDN.listarReservaActual(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());     
		
		reservaAutorizada = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
		movimientosReserva = reservaDN.obtenerDiferenciasReservas(reservaAutorizada, nuevaReserva);
		BigDecimal tipoAjuste = reservaDN.determinarTipoAjusteNuevaReserva(movimientosReserva);
		for(SoporteMovimientosReservaBean soporteMovimiento :  movimientosReserva){
			reservaDetalle.add(soporteMovimiento.getReservaDetalleDTO());
		}
		
		Date fechaMovimiento = Calendar.getInstance().getTime();
		ReservaDTO nuevaReservaDTO = new ReservaDTO();
		nuevaReservaDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		nuevaReservaDTO.setDescripcion("Ajuste Indemnizacion en automatico Modificar");
		nuevaReservaDTO.setTipoajuste(tipoAjuste);
		nuevaReservaDTO.setFechaestimacion(fechaMovimiento);
		nuevaReservaDTO.setFechaautorizacion(fechaMovimiento);
		nuevaReservaDTO.setIdtousuariocreacion(idToUsuarioMovimiento);
		nuevaReservaDTO.setAutorizada(Boolean.TRUE);
		nuevaReservaDTO.setIdtousuarioautoriza(idToUsuarioMovimiento);
		
		result = reservaDN.completaEliminarIndemnizacion(indemnizacionDTO, nuevaReservaDTO, reservaDetalle, userName);
		
		return result;
	} 
	private Double[] generaPerdidasIndemnizacion(IndemnizacionForm indemnizacionForm) throws ExcepcionDeAccesoADatos, SystemException{
		Double[] resul = new Double[indemnizacionForm.getPerdidas().length];
		String[] perdidas = indemnizacionForm.getPerdidas();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		Double[] sumaIndemninacionesBD = indemnizacionRiesgoCoberturaDN.sumaPagosParcialesAgregar(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
		if(sumaIndemninacionesBD.length > 0){
			for(int i = 0; i < sumaIndemninacionesBD.length; i++){
				Double montoEstimacion = sumaIndemninacionesBD[i]+ UtileriasWeb.eliminaFormatoMoneda(perdidas[i]);
				resul[i] = montoEstimacion;
			}
		}else{
			for(int i = 0; i < perdidas.length; i++){
				Double montoEstimacion = UtileriasWeb.eliminaFormatoMoneda(perdidas[i]);
				resul[i] = montoEstimacion;
			}
		}
		return resul;
	}
	
	private Double[] generaPerdidasIndemnizacionModificacion(IndemnizacionForm indemnizacionForm) throws ExcepcionDeAccesoADatos, SystemException{
		Double[] resul = new Double[indemnizacionForm.getPerdidas().length];
		String[] perdidas = indemnizacionForm.getPerdidas();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		Double[] sumaIndemninacionesBD = indemnizacionRiesgoCoberturaDN.sumaPagosParcialesModificar(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()),UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
		if(sumaIndemninacionesBD.length > 0){
			for(int i = 0; i < sumaIndemninacionesBD.length; i++){
				Double montoEstimacion = sumaIndemninacionesBD[i]+ UtileriasWeb.eliminaFormatoMoneda(perdidas[i]);
				resul[i] = montoEstimacion;
			}
		}else{
			for(int i = 0; i < perdidas.length; i++){
				Double montoEstimacion = UtileriasWeb.eliminaFormatoMoneda(perdidas[i]);
				resul[i] = montoEstimacion;
			}
		}
		return resul;
	}
	
	public Double[] removerFormatoMonedaEstimaciones(String[] estimaciones){
		int itemsCount = estimaciones.length; 		
		Double[] result = new Double[itemsCount];
			
		for(int i = 0; i < itemsCount; i++){
			Double montoEstimacion = UtileriasWeb.eliminaFormatoMoneda(estimaciones[i]);
			result[i] = montoEstimacion;
		}
		return result;		
	}
	
//	public void distribuirReaseguro(int idTipoAutorizacionTecnica, int tipoDistribucion, Object conceptoADistribuir) throws SystemException{
//		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
//		autorizacionTecnicaDN.distribuirReaseguro(idTipoAutorizacionTecnica, tipoDistribucion, conceptoADistribuir);		
//	}
	
	public void completarOrdenDePago(OrdenDePagoDTO entity, String estatusSolicitud,String nombreUsuario) throws Exception{	
		EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(nombreUsuario);
		MovimientoReaseguroDN movimientoReaseguroDN = MovimientoReaseguroDN.getInstancia();
		
		if(estatusSolicitud.equalsIgnoreCase("CANCELADO")){
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO);
		}else if(estatusSolicitud.equalsIgnoreCase("TERMINADO")){
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.PAGADO);	
		}							
		List<DetalleOrdenPagoDTO> detalles = DetalleOrdenDePagoDN.getInstancia().detallesOrdenDePago(entity.getIdToOrdenPago());
		
		for(DetalleOrdenPagoDTO detalleOrden: detalles){
			AutorizacionTecnicaDTO autorizacionTecnicaDTO = detalleOrden.getAutorizacionTecnicaDTO();
			if(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica() == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO){
				GastoSiniestroDTO gastoSiniestroDTO = autorizacionTecnicaDTO.getGastoSiniestroDTO();
				gastoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
				
				try{
					gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
				}catch(Exception ex){
					LogDeMidasWeb.log("Error al actualizar el estatus de pagado" , Level.WARNING, ex);
					throw ex;
				}
				
			}else if(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica() == AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION){
				IndemnizacionDTO indemnizacionDTO = autorizacionTecnicaDTO.getIndemnizacionDTO();
				indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
				
				try{
					indemnizacionDTO = indemnizacionDN.modificar(indemnizacionDTO);				
					if(estatusSolicitud.equalsIgnoreCase("TERMINADO")){
						if(!(indemnizacionDTO.getVariosPagos().booleanValue()) || (indemnizacionDTO.getUltimoPago().booleanValue())){
							try{
								ReporteSiniestroDTO reporteSiniestroDTO = indemnizacionDTO.getReporteSiniestroDTO();
								if(this.validaPagosDeIndemnizacion(reporteSiniestroDTO)){
									
									ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
									reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.REPORTE_TERMINADO);
									reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
									reporteSiniestroDTO = reporteSiniestroDN.actualizar(reporteSiniestroDTO);
									movimientoReaseguroDN.acumularMovimientosTerminacionSiniestros(reporteSiniestroDTO.getIdToReporteSiniestro());
								}
							}catch(Exception ex){					
								LogDeMidasWeb.log("Error al actualizar el estatus del reporte de siniestro " , Level.WARNING, ex);
								throw ex;
							}
						}					
					}				
				}catch(Exception ex){
					LogDeMidasWeb.log("Error al actualizar el estatus de la indemnizacion" , Level.WARNING, ex);
					throw ex;
				}						
			}			
			
		}
	}
	
	public void distribuirReaseguro(int idTipoAutorizacionTecnica, int tipoDistribucion, Object conceptoADistribuir,Date fechaMovimiento,String nombreUsuario) throws SystemException{
		IntegracionReaseguroDN integracionReaseguro = IntegracionReaseguroDN.getInstancia();
		switch(idTipoAutorizacionTecnica){
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO : {
				GastoSiniestroDTO gastoSiniestroDTO = (GastoSiniestroDTO)conceptoADistribuir;
				integracionReaseguro.distribuirGasto(gastoSiniestroDTO, tipoDistribucion,fechaMovimiento,nombreUsuario);
				break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO : {
				IngresoSiniestroDTO ingresoSiniestroDTO = (IngresoSiniestroDTO)conceptoADistribuir;
				integracionReaseguro.distribuirIngreso(ingresoSiniestroDTO, tipoDistribucion,fechaMovimiento,nombreUsuario);
				break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION : {
				IndemnizacionDTO indemnizacionDTO = (IndemnizacionDTO)conceptoADistribuir;
				integracionReaseguro.distribuirIndemnizacion(indemnizacionDTO, tipoDistribucion,fechaMovimiento,nombreUsuario);
				integracionReaseguro.distribuirDeducibleIndemnizacion(indemnizacionDTO, tipoDistribucion,fechaMovimiento,nombreUsuario);
				integracionReaseguro.distribuirCoaseguroIndemnizacion(indemnizacionDTO, tipoDistribucion,fechaMovimiento,nombreUsuario);
				break;
			}
		}
	}
	
	public boolean validaPagosDeIndemnizacion(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		boolean result = true;
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		IndemnizacionDTO ultimoPago = indemnizacionDN.obtenerUltimoPago(reporteSiniestroDTO.getIdToReporteSiniestro());
		if(ultimoPago != null && ultimoPago.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().shortValue() == EstatusFinanzasDTO.PAGADO.shortValue()){
			List<IndemnizacionDTO> listaIndemnizaciones = indemnizacionDN.obtenIndemnizacionPorEstatusNOCancelado(reporteSiniestroDTO);
			for(IndemnizacionDTO indemnizacionDTO:listaIndemnizaciones){
				if(indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().shortValue() != EstatusFinanzasDTO.PAGADO.shortValue()){
					result = false;
					break;
				}
			}
		}else{
			result = false;
		}
		return result;
	}
	
	public void distribuirRegistrosMovSiniestroDTO(BigDecimal idToMovimiento, BigDecimal idToReporteSiniestro, BigDecimal idTcConceptoMovimiento) throws SystemException{
			
			//TODO Cambiar las 5 primeras lineas por el metodo agregado para siniestros en DistribucionMovSiniestroSN
//			DistribucionMovSiniestroDTO distribucionMovSiniestroDTO = new DistribucionMovSiniestroDTO();
//			LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = new LineaSoporteReaseguroDTO();
//			LineaSoporteReaseguroId lineaSoporteReaseguroId = new LineaSoporteReaseguroId();
//			lineaSoporteReaseguroId.setIdToSoporteReaseguro(new BigDecimal(218));
//			lineaSoporteReaseguroDTO.setId(lineaSoporteReaseguroId);
//			distribucionMovSiniestroDTO.setLineaSoporteReaseguroDTO(lineaSoporteReaseguroDTO);
		List<DistribucionMovSiniestroDTO> listaDistribuirReaseguro = new DistribucionMovSiniestroSN().obtenerMovimientosNoDistribuidos(idToMovimiento, idToReporteSiniestro, idTcConceptoMovimiento.intValue());
		for(DistribucionMovSiniestroDTO registro: listaDistribuirReaseguro){
			
			if(registro.getEstatus().intValue() == 1){
			new TemporizadorDistribuirSiniestroSN().iniciar(
					registro.getIdToPoliza(),registro.getNumeroEndoso(),registro.getIdToSeccion(),registro.getIdToCobertura(), 
					registro.getNumeroInciso(),registro.getNumeroSubInciso(), registro.getConceptoMovimientoDetalleDTO().getIdConceptoDetalle(), 
					registro.getIdTcMoneda(), registro.getFechaMovimiento(),registro.getIdMovimientoSiniestro(), 
					registro.getMonto(),registro.getTipoMovimiento(), registro.getIdToReporteSiniestro(), 
					registro.getIdToDistribucionMovSiniestro());
			}
		}
		
	}
	
	public void enviarNotificacionReporteSinInformePreliminar() throws ExcepcionDeAccesoADatos, SystemException{
		List<String> destinatarios = new ArrayList<String>();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia("");
		destinatarios.add(Sistema.ROL_COORDINADOR_SINIESTROS);
		destinatarios.add(Sistema.ROL_COORDINADOR_SINIESTROS_FACULTATIVO);
		destinatarios.add(Sistema.ROL_GERENTE_SINIESTROS);
		destinatarios.add(Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO);
		
		List<ReporteSiniestroDTO> listaReporteSiniestro = reporteSiniestroDN.reportesSinInformePreliminar();
		Usuario coordinador = null;
		if(listaReporteSiniestro != null && listaReporteSiniestro.size() > 0 ){
		
			String titulo = "Reportes de Siniestro sin Informe Preliminar";	
			
			StringBuilder bodyMail = new StringBuilder();
			bodyMail.append("Se les notifica que los siguientes reportes no cuentan con Informe Preliminar: <br /> <br />");
			for(ReporteSiniestroDTO reporteSiniestroDTO:listaReporteSiniestro){
				bodyMail.append("<br />No. Reporte: <b>");
				bodyMail.append(reporteSiniestroDTO.getNumeroReporte());
				bodyMail.append("</b> <br />Fecha del Reporte: <b>");
				bodyMail.append(UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaHoraReporte()));
				bodyMail.append("</b> <br />No. P�liza Asegurado: <b>");
				if(reporteSiniestroDTO.getNumeroPoliza() != null){
					PolizaSoporteDanosDTO PolizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza());
					bodyMail.append(PolizaSoporteDanosDTO.getNumeroPoliza());
					bodyMail.append("</b> <br />Nombre Asegurado: <b>");
					bodyMail.append(PolizaSoporteDanosDTO.getNombreAsegurado());
				}else{
					bodyMail.append("Aun no se ha asignado una poliza al reporte");
				}
				bodyMail.append("</b> <br />Coordinador: <b>");
				if(reporteSiniestroDTO.getIdCoordinador() != null && reporteSiniestroDTO.getIdCoordinador().intValue() != 0  ){
					coordinador = UsuarioWSDN.getInstancia().obtieneUsuario(reporteSiniestroDTO.getIdCoordinador().intValue());
					if(coordinador != null){
						bodyMail.append(coordinador.getNombre());
					}else{
						bodyMail.append(" ");
					}
					
				}else{
					bodyMail.append("Aun no se ha asignado coordinador al reporte");
				}
				bodyMail.append("</b> <br />Estatus del Reporte: <b>");
				bodyMail.append(reporteSiniestroDTO.getReporteEstatus().getDescripcion());
				bodyMail.append("</b> <br/> <br/>");
			}
				StringBuilder contenido = new StringBuilder();
				contenido.append("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'> ");
				contenido.append("<html>");
				contenido.append("<head>");
				contenido.append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
				contenido.append("</head>");
				contenido.append("<body>");
				contenido.append(bodyMail.toString());		
				contenido.append("</body>");
				contenido.append("</html>");
					
			MailAction.enviarCorreosPorRoles(destinatarios, titulo, contenido.toString(),coordinador);		
		}
	}
	
}
