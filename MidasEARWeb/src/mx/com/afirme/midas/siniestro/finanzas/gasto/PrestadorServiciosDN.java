package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class PrestadorServiciosDN {
	private static final PrestadorServiciosDN INSTANCIA = new PrestadorServiciosDN();

	public static PrestadorServiciosDN getInstancia() {
		return INSTANCIA;
	}

	public void agregarPrestadorServicio(PrestadorServiciosDTO prestadorServicioDTO) throws SystemException, ExcepcionDeAccesoADatos {
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		prestadorServiciosSN.agregarPrestadorServicio(prestadorServicioDTO);
	}
	
	public void eliminarPrestadorServicio(PrestadorServiciosDTO prestadorServicioDTO) throws SystemException, ExcepcionDeAccesoADatos {
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		prestadorServiciosSN.eliminarPrestadorServicio(prestadorServicioDTO);	
	}
	
	public PrestadorServiciosDTO actualizarPrestadorServicio(PrestadorServiciosDTO prestadorServicioDTO) throws SystemException, ExcepcionDeAccesoADatos {
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		return prestadorServiciosSN.actualizarPrestadorServicio(prestadorServicioDTO);
	}
	
	public PrestadorServiciosDTO findById(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos {
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		return prestadorServiciosSN.findById(id);
	}
 
	public List<PrestadorServiciosDTO> findAll() throws SystemException, ExcepcionDeAccesoADatos {
		PrestadorServiciosSN prestadorServiciosSN = new PrestadorServiciosSN();
		return prestadorServiciosSN.findAll();
	} 
	
	public List<mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO> getPrestadoresServicio() throws SystemException{
		List<mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO> prestadoresServicio = null;
		
		mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN ps = 
			mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN.getInstancia();	
		
		prestadoresServicio = ps.listarPrestadores(null, "ADMIN");
				
		return prestadoresServicio;	
	}
	
	public mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO getPrestadorServiciosPorId(BigDecimal id) throws SystemException, ExcepcionDeAccesoADatos {
		mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO prestadoresServicio = null;
		
		mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN ps = 
			mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN.getInstancia();	

		prestadoresServicio = ps.detallePrestador(id, "ADMIN");
		
		return prestadoresServicio;
	}
}
