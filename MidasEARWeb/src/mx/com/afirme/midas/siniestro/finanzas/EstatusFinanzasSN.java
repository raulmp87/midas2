package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstatusFinanzasSN {
	
	private EstatusFinanzasFacadeRemote beanRemoto;

	public EstatusFinanzasSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(EstatusFinanzasFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(estatusFinanzasDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(estatusFinanzasDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(estatusFinanzasDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<EstatusFinanzasDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<EstatusFinanzasDTO> buscarPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public EstatusFinanzasDTO getEstatusFinanzasPorId(EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(estatusFinanzasDTO.getIdTcEstatusfinanzas());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
