package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ValidarInformePreliminarForm extends MidasBaseForm {
	private static final long serialVersionUID = 1L;
	private BigDecimal idDeReporte;
	private BigDecimal idToReporteSiniestro;
	private String idToPoliza;
	
	public BigDecimal getIdDeReporte() {
		return idDeReporte;
	}

	public void setIdDeReporte(BigDecimal idDeReporte) {
		this.idDeReporte = idDeReporte;
	}

	private String tipoMoneda = "";
	private Boolean valido = false;
	private List<Registro> listadoCoberturas;
	private String total = "";
	
	private List<?> listado;
	private String[] coberturasRiesgo = {};

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}

	/**
	 * @param tipoMoneda
	 *            the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	/**
	 * @return the informePreliminarValido
	 */
	public Boolean getValido() {
		return valido;
	}

	/**
	 * @return the informePreliminarValido
	 */
	public Boolean isValido() {
		return valido;
	}

	/**
	 * @param informePreliminarValido
	 *            the informePreliminarValido to set
	 */
	public void setValido(Boolean valido) {
		this.valido = valido;
	}

	/**
	 * @return the listadoCoberturas
	 */
	public List<Registro> getListadoCoberturas() {
		return listadoCoberturas;
	}

	/**
	 * @param listadoCoberturas
	 *            the listadoCoberturas to set
	 */
	public void setListadoCoberturas(List<Registro> listadoCoberturas) {
		this.listadoCoberturas = listadoCoberturas;
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}

	public void setListado(List<?> listado) {
		this.listado = listado;
	}

	public List<?> getListado() {
		return listado;
	}

	public void setCoberturasRiesgo(String[] coberturasRiesgo) {
		this.coberturasRiesgo = coberturasRiesgo;
	}

	public String[] getCoberturasRiesgo() {
		return coberturasRiesgo;
	}

	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}

	public String getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(String idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	
}
