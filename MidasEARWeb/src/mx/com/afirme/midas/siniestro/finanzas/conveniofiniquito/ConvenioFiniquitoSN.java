package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ConvenioFiniquitoSN {
	private String REPORTE_SINIESTRO_ID_FIELD="reporteSiniestro.idToReporteSiniestro";
	
	private ConvenioFiniquitoFacadeRemote beanRemoto;

	public ConvenioFiniquitoSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ConvenioFiniquitoFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public ConvenioFiniquitoDTO agregar(ConvenioFiniquitoDTO documentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(documentoSiniestroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public void borrar(ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(convenioFiniquitoDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public String modificar(ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(convenioFiniquitoDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<ConvenioFiniquitoDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ConvenioFiniquitoDTO> buscarPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ConvenioFiniquitoDTO getPorId(ConvenioFiniquitoDTO convenioFiniquitoDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(convenioFiniquitoDTO.getIdToConvenioFiniquito());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ConvenioFiniquitoDTO getPorReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			List<ConvenioFiniquitoDTO> convenios = this.buscarPorPropiedad(REPORTE_SINIESTRO_ID_FIELD, reporteSiniestroDTO.getIdToReporteSiniestro());
			if (convenios!=null && convenios.size()==1)
				return convenios.get(0);
			else return null;
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ConvenioFiniquitoDTO obtenerConvenioCancelar(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion,Short estatus) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenerConvenioCancelar(idToReporteSiniestro, idToIndemnizacion, estatus);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}

}
