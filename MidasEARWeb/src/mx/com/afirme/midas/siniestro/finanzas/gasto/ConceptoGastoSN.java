package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.gasto.ConceptoGastoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class ConceptoGastoSN {
	private ConceptoGastoFacadeRemote beanRemoto;
	
	public ConceptoGastoSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en ConceptoGastoSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ConceptoGastoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado ConceptoGastoSN", Level.FINEST, null);
	}		
	
	public void agregarConceptoGasto(ConceptoGastoDTO conceptoGastoDTO){
		beanRemoto.save(conceptoGastoDTO);
	}
	
	public void eliminarConceptoGasto(ConceptoGastoDTO conceptoGastoDTO){
		beanRemoto.delete(conceptoGastoDTO);	
	}
	
	public ConceptoGastoDTO actualizarConceptoGasto(ConceptoGastoDTO conceptoGastoDTO){
		return beanRemoto.update(conceptoGastoDTO);
	}
	
	public ConceptoGastoDTO findById(BigDecimal id){
		return beanRemoto.findById(id);
	}
 
	public List<ConceptoGastoDTO> findAll(){
		return beanRemoto.findAll();
	} 
}
