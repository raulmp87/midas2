/**
 * 
 */
package mx.com.afirme.midas.siniestro.finanzas;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author user
 *
 */
public class ListarReservaForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;

	private String sumaAsegurada;
	private String tipoMoneda;
	private List listaModificacionesReserva;
	private List listaDetalleReserva;
	private String valorRadio;

	public ListarReservaForm() {
	}

	/**
	 * @return the sumaAsegurada
	 */
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}

	/**
	 * @param sumaAsegurada the sumaAsegurada to set
	 */
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}

	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	/**
	 * @return the listaModificacionesReserva
	 */
	public List getListaModificacionesReserva() {
		return listaModificacionesReserva;
	}

	/**
	 * @param listaModificacionesReserva the listaModificacionesReserva to set
	 */
	public void setListaModificacionesReserva(List listaModificacionesReserva) {
		this.listaModificacionesReserva = listaModificacionesReserva;
	}

	/**
	 * @return the listaDetalleReserva
	 */
	public List getListaDetalleReserva() {
		return listaDetalleReserva;
	}

	/**
	 * @param listaDetalleReserva the listaDetalleReserva to set
	 */
	public void setListaDetalleReserva(List listaDetalleReserva) {
		this.listaDetalleReserva = listaDetalleReserva;
	}

	/**
	 * @return the valorRadio
	 */
	public String getValorRadio() {
		return valorRadio;
	}

	/**
	 * @param valorRadio the valorRadio to set
	 */
	public void setValorRadio(String valorRadio) {
		this.valorRadio = valorRadio;
	}

	
	
}
