package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ValidarInformePreliminarAction extends MidasMappingDispatchAction{

	public ActionForward presentarInformePreliminar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		ValidarInformePreliminarForm forma = (ValidarInformePreliminarForm) form;
		BigDecimal idToReporteSiniestro = new BigDecimal(request.getParameter("idToReporteSiniestro"));
		forma.setIdDeReporte(idToReporteSiniestro);
		ArrayList<Registro> listaDeRegistros = new ArrayList<Registro>();
		ReporteSiniestroDTO reporteSiniestro;

		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		RiesgoAfectadoDN riesgoAfectado = RiesgoAfectadoDN.getInstancia();
		List<RiesgoAfectadoDTO> l;
		String tipoMoneda ="";
		String idToPoliza = "";
		try {
			reporteSiniestro = reporteSiniestroDN.desplegarReporte(forma.getIdDeReporte());
			forma.setValido(reporteSiniestro.getInformePreliminarValido());
			l = riesgoAfectado.listarRiesgosAfectadosPara(idToReporteSiniestro);
			
			if(reporteSiniestro.getNumeroPoliza() != null){
				tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(idToReporteSiniestro);
				idToPoliza = reporteSiniestro.getNumeroPoliza() !=null?reporteSiniestro.getNumeroPoliza().toString():"";				
			}			
			
		} catch (ExcepcionDeAccesoADatos e) {
			return mapping.findForward(Sistema.NO_EXITOSO);
		} catch (SystemException e) {
			return mapping.findForward(Sistema.NO_DISPONIBLE);
		}
		Registro r;
		for (RiesgoAfectadoDTO a : l) {
			r = new Registro(a);
			listaDeRegistros.add(r);
		}
		
//		forma.setTipoMoneda("MXP");
		
		forma.setTipoMoneda(tipoMoneda);
		forma.setListadoCoberturas(listaDeRegistros);
		forma.setIdToPoliza(idToPoliza);
		
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public ActionForward noProcedeElAnalisisInterno(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		return marcarReporte(mapping, form, request, 0, ReporteSiniestroDTO.PENDIENTE_TERMINAR_REPORTE_NO_INDEMNIZADO);
	}
	
	public ActionForward procedeElAnalisisInterno(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		return marcarReporte(mapping, form, request, 1, ReporteSiniestroDTO.PENDIENTE_ESTIMAR_RESERVA);
	}
	
	private ActionForward marcarReporte(ActionMapping mapping, ActionForm form, HttpServletRequest request, int hayQueAnalizar, byte estatus) {
		ValidarInformePreliminarForm forma = (ValidarInformePreliminarForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		ReporteSiniestroDTO reporteSiniestro = new ReporteSiniestroDTO();
		try {
			reporteSiniestro = reporteSiniestroDN.desplegarReporte(forma.getIdDeReporte());
			reporteSiniestro.setInformePreliminarValido(forma.getValido());
			reporteSiniestro.setAnalisisInterno(new BigDecimal(hayQueAnalizar));			
			reporteSiniestro.getReporteEstatus().setIdTcReporteEstatus(estatus);
			reporteSiniestroDN.actualizar(reporteSiniestro);
		} catch (ExcepcionDeAccesoADatos e) {
			return mapping.findForward(Sistema.NO_EXITOSO);
		} catch (SystemException e) {
			return mapping.findForward(Sistema.NO_DISPONIBLE);
		}
		return mapping.findForward(Sistema.EXITOSO);		
	}
}
