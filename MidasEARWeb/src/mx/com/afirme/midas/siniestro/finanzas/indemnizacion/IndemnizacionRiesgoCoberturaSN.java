package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IndemnizacionRiesgoCoberturaSN {
	
	private IndemnizacionRiesgoCoberturaFacadeRemote beanRemoto;

	public IndemnizacionRiesgoCoberturaSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(IndemnizacionRiesgoCoberturaFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public String agregar(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(indemnizacionRiesgoCoberturaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String borrar(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(indemnizacionRiesgoCoberturaDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public String modificar(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(indemnizacionRiesgoCoberturaDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public List<IndemnizacionRiesgoCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IndemnizacionRiesgoCoberturaDTO> buscarPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public IndemnizacionRiesgoCoberturaDTO getIndemnizacionRiesgoCoberturaPorId(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(indemnizacionRiesgoCoberturaDTO.getId());
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public Double getTotalCoaseguroPorIdAutorizacionTecnica(BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getTotalCoaseguroPorIdAutorizacionTecnica(idToIndemnizacion);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public Double getTotalDeduciblePorIdAutorizacionTecnica(BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getTotalDeduciblePorIdAutorizacionTecnica(idToIndemnizacion);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}				
	}
	
	public Double obtenerMontoTotalIndeminizacionCobertura(BigDecimal idToPoliza, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion, BigDecimal idToCobertura)throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenerMontoTotalIndeminizacionCobertura(idToPoliza, numeroInciso,
					numeroSubInciso, idToSeccion, idToCobertura);
		}catch(Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public Double[] sumaPagosParcialesAgregar(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.sumaPagosParcialesAgregar(idToReporteSiniestro);
		}catch(Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public Double[] sumaPagosParcialesModificar(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.sumaPagosParcialesModificar(idToReporteSiniestro,idToIndemnizacion);
		}catch(Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
	
	public Double sumaPerdidaDeterminadaPorReporte(BigDecimal idToReporteSiniestro)throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.sumaPerdidaDeterminadaPorReporte(idToReporteSiniestro);
		}catch(Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}		
	}
}
