package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.io.Serializable;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.poliza.cobertura.CoberturaSoporteDaniosSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;


public class ReservaDetalleBean implements Serializable {
	private static final long serialVersionUID = -2244258379729159200L;
	private CoberturaSoporteDanosDTO coberturaSoporteDanosDTO;
	private CoberturaSoporteDaniosSiniestroDTO coberturaBasicaSoporteDanosDTO;
	private RiesgoSoporteDanosDTO riesgoSoporteDanosDTO;
	private SeccionSoporteDanosDTO seccionSoporteDanosDTO;
	private RiesgoAfectadoDTO riesgoAfectadoDTO;
	private ReservaDetalleDTO reservaDetalleDTO; 
	private IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO;
	private double montoCuotaParte;
	private double montoPrimerExcedente;
	private double montoFacultativo;
	private double montoRetencion;	
	private String tipoCobertura;
	private String nombreCoberturaBasica;
	private double sumaAseguradaDisponible;
	
	
	/**
	 * @return the sumaAseguradaDisponible
	 */
	public double getSumaAseguradaDisponible() {
		return sumaAseguradaDisponible;
	}
	/**
	 * @param sumaAseguradaDisponible the sumaAseguradaDisponible to set
	 */
	public void setSumaAseguradaDisponible(double sumaAseguradaDisponible) {
		this.sumaAseguradaDisponible = sumaAseguradaDisponible;
	}
	/**
	 * @return the nombreCoberturaBasica
	 */
	public String getNombreCoberturaBasica() {
		return nombreCoberturaBasica;
	}
	/**
	 * @param nombreCoberturaBasica the nombreCoberturaBasica to set
	 */
	public void setNombreCoberturaBasica(String nombreCoberturaBasica) {
		this.nombreCoberturaBasica = nombreCoberturaBasica;
	}
	/**
	 * @return the coberturaBasicaSoporteDanosDTO
	 */
	public CoberturaSoporteDaniosSiniestroDTO getCoberturaBasicaSoporteDanosDTO() {
		return coberturaBasicaSoporteDanosDTO;
	}
	/**
	 * @param coberturaBasicaSoporteDanosDTO the coberturaBasicaSoporteDanosDTO to set
	 */
	public void setCoberturaBasicaSoporteDanosDTO(
			CoberturaSoporteDaniosSiniestroDTO coberturaBasicaSoporteDanosDTO) {
		this.coberturaBasicaSoporteDanosDTO = coberturaBasicaSoporteDanosDTO;
	}
	/**
	 * @return the indemnizacionRiesgoCoberturaDTO
	 */
	public IndemnizacionRiesgoCoberturaDTO getIndemnizacionRiesgoCoberturaDTO() {
		return indemnizacionRiesgoCoberturaDTO;
	}
	/**
	 * @return the tipoCobertura
	 */
	public String getTipoCobertura() {
		return tipoCobertura;
	}
	/**
	 * @param tipoCobertura the tipoCobertura to set
	 */
	public void setTipoCobertura(String tipoCobertura) {
		this.tipoCobertura = tipoCobertura;
	}
	/**
	 * @param indemnizacionRiesgoCoberturaDTO the indemnizacionRiesgoCoberturaDTO to set
	 */
	public void setIndemnizacionRiesgoCoberturaDTO(
			IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) {
		this.indemnizacionRiesgoCoberturaDTO = indemnizacionRiesgoCoberturaDTO;
	}
	/**
	 * @return the riesgoAfectadoDTO
	 */
	public RiesgoAfectadoDTO getRiesgoAfectadoDTO() {
		return riesgoAfectadoDTO;
	}
	/**
	 * @param riesgoAfectadoDTO the riesgoAfectadoDTO to set
	 */
	public void setRiesgoAfectadoDTO(RiesgoAfectadoDTO riesgoAfectadoDTO) {
		this.riesgoAfectadoDTO = riesgoAfectadoDTO;
	}

	
	/**
	 * @return the coberturaSoporteDanosDTO
	 */
	public CoberturaSoporteDanosDTO getCoberturaSoporteDanosDTO() {
		return coberturaSoporteDanosDTO;
	}
	/**
	 * @param coberturaSoporteDanosDTO the coberturaSoporteDanosDTO to set
	 */
	public void setCoberturaSoporteDanosDTO(
			CoberturaSoporteDanosDTO coberturaSoporteDanosDTO) {
		this.coberturaSoporteDanosDTO = coberturaSoporteDanosDTO;
	}
	/**
	 * @return the riesgoSoporteDanosDTO
	 */
	public RiesgoSoporteDanosDTO getRiesgoSoporteDanosDTO() {
		return riesgoSoporteDanosDTO;
	}
	/**
	 * @param riesgoSoporteDanosDTO the riesgoSoporteDanosDTO to set
	 */
	public void setRiesgoSoporteDanosDTO(RiesgoSoporteDanosDTO riesgoSoporteDanosDTO) {
		this.riesgoSoporteDanosDTO = riesgoSoporteDanosDTO;
	}
	/**
	 * @return the seccionSoporteDanosDTO
	 */
	public SeccionSoporteDanosDTO getSeccionSoporteDanosDTO() {
		return seccionSoporteDanosDTO;
	}
	/**
	 * @param seccionSoporteDanosDTO the seccionSoporteDanosDTO to set
	 */
	public void setSeccionSoporteDanosDTO(
			SeccionSoporteDanosDTO seccionSoporteDanosDTO) {
		this.seccionSoporteDanosDTO = seccionSoporteDanosDTO;
	}
	/**
	 * @return the reservaDetalleDTO
	 */
	public ReservaDetalleDTO getReservaDetalleDTO() {
		return reservaDetalleDTO;
	}
	/**
	 * @param reservaDetalleDTO the reservaDetalleDTO to set
	 */
	public void setReservaDetalleDTO(ReservaDetalleDTO reservaDetalleDTO) {
		this.reservaDetalleDTO = reservaDetalleDTO;
	}
	public void setMontoCuotaParte(double montoCuotaParte) {
		this.montoCuotaParte = montoCuotaParte;
	}
	public double getMontoCuotaParte() {
		return montoCuotaParte;
	}
	public void setMontoPrimerExcedente(double montoPrimerExcedente) {
		this.montoPrimerExcedente = montoPrimerExcedente;
	}
	public double getMontoPrimerExcedente() {
		return montoPrimerExcedente;
	}
	public void setMontoFacultativo(double montoFacultativo) {
		this.montoFacultativo = montoFacultativo;
	}
	public double getMontoFacultativo() {
		return montoFacultativo;
	}
	public void setMontoRetencion(double montoRetencion) {
		this.montoRetencion = montoRetencion;
	}
	public double getMontoRetencion() {
		return montoRetencion;
	}
	
	
}
