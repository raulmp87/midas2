package mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetalleOrdenDePagoSN {
	private DetalleOrdenPagoFacadeRemote beanRemoto;

	public DetalleOrdenDePagoSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en DetalleOrdenDePagoSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(DetalleOrdenPagoFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado DetalleOrdenDePagoSN",
				Level.FINEST, null);
	}

	public DetalleOrdenPagoDTO detalleOrdenDePago(BigDecimal idOrdenDePago)
			throws ExcepcionDeAccesoADatos {
		List<DetalleOrdenPagoDTO> ordenes = beanRemoto.findByProperty(
				"ordenDePagoDTO.idToOrdenPago", idOrdenDePago);
		if (ordenes != null) {
			return ordenes.get(0);
		} else {
			return null;
		}
	}

	public List<DetalleOrdenPagoDTO> detallesOrdenDePago(
			BigDecimal idOrdenDePago) throws ExcepcionDeAccesoADatos {
		List<DetalleOrdenPagoDTO> ordenes = beanRemoto.findByProperty(
				"ordenDePagoDTO.idToOrdenPago", idOrdenDePago);
		return ordenes;
	}
}
