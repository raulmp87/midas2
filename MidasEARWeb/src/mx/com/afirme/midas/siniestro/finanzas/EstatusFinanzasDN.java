package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class EstatusFinanzasDN {
	
	public static final EstatusFinanzasDN INSTANCIA = new EstatusFinanzasDN();

	public static EstatusFinanzasDN getInstancia (){
		return EstatusFinanzasDN.INSTANCIA;
	}
	
	public String agregar(EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos, SystemException{
		EstatusFinanzasSN estatusFinanzasSN = new EstatusFinanzasSN();
		return estatusFinanzasSN.agregar(estatusFinanzasDTO);
	}
	
	public String borrar (EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos, SystemException{
		EstatusFinanzasSN estatusFinanzasSN = new EstatusFinanzasSN();
		return estatusFinanzasSN.borrar(estatusFinanzasDTO);
	}
	
	public String modificar (EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos, SystemException{
		EstatusFinanzasSN estatusFinanzasSN = new EstatusFinanzasSN();
		return estatusFinanzasSN.modificar(estatusFinanzasDTO);
	}
	
	public EstatusFinanzasDTO getEstatusFinanzasPorId(EstatusFinanzasDTO estatusFinanzasDTO) throws ExcepcionDeAccesoADatos, SystemException{
		EstatusFinanzasSN estatusFinanzasSN = new EstatusFinanzasSN();
		return estatusFinanzasSN.getEstatusFinanzasPorId(estatusFinanzasDTO);
	}
	
	public List<EstatusFinanzasDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		EstatusFinanzasSN estatusFinanzasSN = new EstatusFinanzasSN();
		return estatusFinanzasSN.listarTodos();
	}
	
	public List<EstatusFinanzasDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		EstatusFinanzasSN estatusFinanzasSN = new EstatusFinanzasSN();
		return estatusFinanzasSN.buscarPorPropiedad(nombrePropiedad, valor);
	}

}
