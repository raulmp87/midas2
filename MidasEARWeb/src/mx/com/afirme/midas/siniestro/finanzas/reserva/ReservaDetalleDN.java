package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoId;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaId;
import mx.com.afirme.midas.siniestro.reportes.RegistroEstimacionInicial;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class ReservaDetalleDN {

	private static final ReservaDetalleDN INSTANCIA = new ReservaDetalleDN();

	public static ReservaDetalleDN getInstancia() {
		return ReservaDetalleDN.INSTANCIA;
	}
	
	public List<ReservaDetalleDTO> listarDetalleReserva(BigDecimal idReserva)throws SystemException,ExcepcionDeAccesoADatos {	
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		return reservaDetalleSN.listarDetalleReserva(idReserva);
	}
	
	public Double sumaReserva(BigDecimal idReserva)throws SystemException,ExcepcionDeAccesoADatos {	
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		return reservaDetalleSN.sumaReserva(idReserva);
	}
	
	public Double obtenerTotalReserva(List<ReservaDetalleDTO> listaReservas){
		double total = 0;
		for(ReservaDetalleDTO reservaDetalleDTO : listaReservas){
			total += reservaDetalleDTO.getEstimacion().doubleValue();
		}
		return new Double(total) ;
	}
	
	public void guardarReservaDetalle(ReservaDetalleDTO entity) throws SystemException {
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		reservaDetalleSN.guardarReservaDetalle(entity);
		
	}	
	
	public void borrarReservaDetalle(ReservaDetalleDTO entity) throws SystemException {
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		reservaDetalleSN.borrarReservaDetalle(entity);		
	}
	
	public boolean reservaIgualAIndemnizaciones(List<ReservaDetalleDTO> listaReservas, String[] indemnizaciones){
		boolean igual = true;
		for(int i=0; i < listaReservas.size(); i++){
			ReservaDetalleDTO reservaDetalleDTO = listaReservas.get(i);
			if(reservaDetalleDTO.getEstimacion().doubleValue() != UtileriasWeb.parseaformatoMoneda(indemnizaciones[i]).doubleValue()){
				igual = false;
				break;
			}
		}
		return igual;
	}
	
	public boolean reservaIgualAIndemnizacionesVariosPagos(List<ReservaDetalleDTO> listaReservas, Double[] indemnizaciones,String[] perdidasIndemnizacionActual){
		boolean igual = true;
		for(int i=0; i < listaReservas.size(); i++){
			ReservaDetalleDTO reservaDetalleDTO = listaReservas.get(i);
			double suma = 0;
			if(indemnizaciones.length > 0){
				suma = indemnizaciones[i]+UtileriasWeb.eliminaFormatoMoneda(perdidasIndemnizacionActual[i]);
			}else{
				suma = UtileriasWeb.eliminaFormatoMoneda(perdidasIndemnizacionActual[i]);
			}
			if(reservaDetalleDTO.getEstimacion().doubleValue() != suma){
				igual = false;
				break;
			}
		}
		return igual;
	}
	
	public boolean reservaIgualAIndemnizacionesEliminar(List<ReservaDetalleDTO> listaReservas, Double[] indemnizaciones){
		boolean igual = true;
		for(int i=0; i < listaReservas.size(); i++){
			ReservaDetalleDTO reservaDetalleDTO = listaReservas.get(i);
			if(reservaDetalleDTO.getEstimacion().doubleValue() != indemnizaciones[i]){
				igual = false;
				break;
			}
		}
		return igual;
	}
	
	public List<RegistroEstimacionInicial> listarRegistrosEstimacionInicial(List<ReservaDetalleDTO> listaReservas)throws SystemException,ExcepcionDeAccesoADatos {
		//IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		//IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		List<RegistroEstimacionInicial> listadoRegistrosEstimacionInicial = new ArrayList<RegistroEstimacionInicial>();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		IndemnizacionRiesgoCoberturaId indemnizacionRiesgoCoberturaId = new IndemnizacionRiesgoCoberturaId();
		RiesgoAfectadoDTO riesgoAfectadoDTO = new RiesgoAfectadoDTO();
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		//List<IndemnizacionRiesgoCoberturaDTO> indemnizaciones;
		//IndemnizacionDTO indemnizacionDTO = new IndemnizacionDTO();
		
		for(ReservaDetalleDTO reservaDetalleDTO : listaReservas){
			RegistroEstimacionInicial registroEstimacionInicial= new RegistroEstimacionInicial();
			//Se llenan los datos del registro
			riesgoAfectadoDTO = reservaDetalleDTO.getRiesgoAfectadoDTO();
			if (riesgoAfectadoDTO==null||riesgoAfectadoDTO.getId()==null) continue;
			indemnizacionRiesgoCoberturaId = new IndemnizacionRiesgoCoberturaId();
			riesgoAfectadoId = riesgoAfectadoDTO.getId();
			
			//indemnizacionDTO = indemnizacionDN.obtenIndemnizacionPorAutorizar(reservaDetalleDTO.getId().getIdtoreportesiniestro());
			//indemnizaciones = indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
			
			indemnizacionRiesgoCoberturaId.setNumeroInciso(riesgoAfectadoId.getNumeroinciso());
			indemnizacionRiesgoCoberturaId.setIdToReporteSiniestro(reservaDetalleDTO.getId().getIdtoreportesiniestro());
			indemnizacionRiesgoCoberturaId.setIdToSeccion(riesgoAfectadoId.getIdtoseccion());
			indemnizacionRiesgoCoberturaId.setNumeroSubinciso(riesgoAfectadoId.getNumerosubinciso());
			indemnizacionRiesgoCoberturaId.setIdToCobertura(riesgoAfectadoId.getIdtocobertura());
			indemnizacionRiesgoCoberturaId.setIdToRiesgo(riesgoAfectadoId.getIdtoriesgo());
			indemnizacionRiesgoCoberturaId.setIdToPoliza(reservaDetalleDTO.getId().getIdtopoliza());
			
		
			coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(reservaDetalleDTO.getId().getIdtocobertura());
			registroEstimacionInicial.setCobertura(coberturaSoporteDanosDTO.getDescripcionCobertura());
			
			riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(reservaDetalleDTO.getId().getIdtoriesgo());
			registroEstimacionInicial.setRiesgo(riesgoSoporteDanosDTO.getDescripcionRiesgo());
			listadoRegistrosEstimacionInicial.add(registroEstimacionInicial);
			registroEstimacionInicial.setEstimacionInicial(new Double(0));
			registroEstimacionInicial.setEstimacionInicialCuotaParte(new Double(0));
			registroEstimacionInicial.setEstimacionInicialFacultativo(new Double(0));
			registroEstimacionInicial.setEstimacionInicialPrimerExcedente(new Double(0));
			registroEstimacionInicial.setEstimacionInicialRetencion(new Double(0));
		}
		
		return listadoRegistrosEstimacionInicial;
	}
	
	public List<ReservaDetalleDTO> listarReservaDetalle(BigDecimal idReserva) throws SystemException{
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		return reservaDetalleSN.listarReservaDetalle(idReserva);		
	}
	
	public Double diferenciaDeReserva(BigDecimal idToReporteSiniestro) throws SystemException{
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		return reservaDetalleSN.diferenciaDeReserva(idToReporteSiniestro);		
	}
	
	public Double obtenerSumaReservaInicial(BigDecimal idToReporteSiniestro) throws SystemException{
		ReservaDetalleSN reservaDetalleSN = new ReservaDetalleSN();
		return reservaDetalleSN.obtenerSumaReservaInicial(idToReporteSiniestro);		
	}
}
