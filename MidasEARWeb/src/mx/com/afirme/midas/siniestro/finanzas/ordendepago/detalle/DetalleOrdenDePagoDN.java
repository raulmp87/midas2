package mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DetalleOrdenDePagoDN {
	private static final DetalleOrdenDePagoDN INSTANCIA = new DetalleOrdenDePagoDN();

	public static DetalleOrdenDePagoDN getInstancia() {
		return DetalleOrdenDePagoDN.INSTANCIA;
	}

	public DetalleOrdenPagoDTO detalleOrdenDePago(BigDecimal idOrdenDePago)
			throws SystemException, ExcepcionDeAccesoADatos {
		DetalleOrdenDePagoSN detalleOrdenDePagoSN = new DetalleOrdenDePagoSN();
		return detalleOrdenDePagoSN.detalleOrdenDePago(idOrdenDePago);
	}

	public List<DetalleOrdenPagoDTO> detallesOrdenDePago(
			BigDecimal idOrdenDePago) throws SystemException,
			ExcepcionDeAccesoADatos {
		DetalleOrdenDePagoSN detalleOrdenDePagoSN = new DetalleOrdenDePagoSN();
		return detalleOrdenDePagoSN.detallesOrdenDePago(idOrdenDePago);
	}

}
