package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReservaSN {
	
	private ReservaFacadeRemote beanRemoto;

	public ReservaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en ReservaSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ReservaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado ReservaSN", Level.FINEST, null);
	}	
	
	public ReservaDTO agregarReserva(ReservaDTO reservaDTO)	throws ExcepcionDeAccesoADatos {
		return beanRemoto.save(reservaDTO);		
	}
	
	public ReservaDTO actualizarReserva(ReservaDTO reservaDTO)throws ExcepcionDeAccesoADatos {
		return beanRemoto.update(reservaDTO);		
	}
	
	public ReservaDTO findById(BigDecimal id)throws ExcepcionDeAccesoADatos {
		return beanRemoto.findById(id);		
	}
	
	public List<ReservaDTO> listarPorFechaEstimacion(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.listarPorFechaEstimacion(idReporteSiniestro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<ReservaDTO> listarReserva(BigDecimal idToReporteSiniestro, Boolean autorizada){
		return beanRemoto.obtenerUltimaReserva(idToReporteSiniestro, autorizada);
	}	
	
	public ReservaDTO obtenerUltimaReservaCreada(BigDecimal idToReporteSiniestro){
		return beanRemoto.obtenerUltimaReservaCreada(idToReporteSiniestro);
	}	
	
	public ReservaDTO obtenReservaEstimacionInicial(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos {
		try {
			return  beanRemoto.obtenReservaEstimacionInicial(idReporteSiniestro);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public ReservaDTO findByIdReporteSiniestro(BigDecimal idToReporteSiniestro){
		List<ReservaDTO> listReservaDTO = null;
		ReservaDTO reservaDTO = null;
				
		listReservaDTO = beanRemoto.findByProperty("reporteSiniestroDTO.idToReporteSiniestro", idToReporteSiniestro);
		
		if(listReservaDTO != null && listReservaDTO.size() > 0){
			reservaDTO = listReservaDTO.get(0); 
		}
 
		return reservaDTO;
	}
	
	public ReservaDTO ajustarReservayRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO reservaDTO = null;
		
		reservaDTO = beanRemoto.ajustarReservayRiesgosAfectados(riesgosAfectados, reserva, reservaDetalle);
		
		return reservaDTO;
	}
	
	public boolean ajustarEstimacionInicialyRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, List<ReservaDetalleDTO> reservaDetalle){
		boolean result = false;
		
		result = beanRemoto.ajustarEstimacionInicialyRiesgosAfectados(riesgosAfectados, reservaDetalle);
		
		return result;
	}
	
	public ReservaDTO ajustarReservaeIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO reservaDTO = null;
		reservaDTO = beanRemoto.ajustarReservaeIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion,reserva, reservaDetalle);
		return reservaDTO;
	}
	
	public ReservaDTO ajustarReservaeModificacionIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO reservaDTO = null;
		reservaDTO = beanRemoto.ajustarReservaeModificacionIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion,reserva, reservaDetalle);
		return reservaDTO;
	}
	
	public ReservaDTO completaCancelarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO reservaDTO = null;
		reservaDTO = beanRemoto.ajustarReservaCancelarIndemnizacion(indemnizacionDTO, reserva, reservaDetalle);
		return reservaDTO;
	}
	
	public ReservaDTO completaEliminarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle){
		ReservaDTO reservaDTO = null;
		reservaDTO = beanRemoto.ajustarReservaEliminarIndemnizacion(indemnizacionDTO, reserva, reservaDetalle);
		return reservaDTO;
	}
}
