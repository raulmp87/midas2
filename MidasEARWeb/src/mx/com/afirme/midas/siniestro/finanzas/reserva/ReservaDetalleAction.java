package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class ReservaDetalleAction extends MidasMappingDispatchAction{

	public ActionForward listarHistorialDetalle(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		//Se asume que la operacion sera exitosa
		String reglaNavegacion = Sistema.EXITOSO;
		//Se obtiene la forma
		HistorialReservaForm forma = (HistorialReservaForm) form;
		//Se obtiene el delegado de negocios
		ReservaDetalleDN  reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		try {
			//Se obtiene el detalle
			List<ReservaDetalleDTO> reservaDetalleDTOList = reservaDetalleDN.listarDetalleReserva(UtileriasWeb.regresaBigDecimal(forma.getReservaSeleccionada()));
			forma.setListaReservaDetalleBean(reservaDN.obtenReservaDetalleBeanParaReservaDetalle(reservaDetalleDTOList));
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
}
