package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;


public class ContabilidadOrdenPagoSN {

	ContabilidadOrdenPagoFacadeRemote beanRemoto;
	
	public ContabilidadOrdenPagoSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en ContabilidadOrdenPagoSN - Constructor", Level.INFO,null);
		
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ContabilidadOrdenPagoFacadeRemote.class);
		
		LogDeMidasWeb.log("bean Remoto instanciado ContabilidadOrdenPagoSN", Level.FINEST, null);
	}
	
	public void guardar(ContabilidadOrdenPagoDTO contabilidadOrdenPagoDTO){
		beanRemoto.save(contabilidadOrdenPagoDTO);
	}
	
	public ContabilidadOrdenPagoDTO actualizar(ContabilidadOrdenPagoDTO contabilidadOrdenPagoDTO){
		return beanRemoto.update(contabilidadOrdenPagoDTO);
	}
	
	public ContabilidadOrdenPagoDTO findById(ContabilidadOrdenPagoId contabilidadOrdenPagoId){
		return beanRemoto.findById(contabilidadOrdenPagoId);
	}	
	
	public void borrar(ContabilidadOrdenPagoDTO entity){
		beanRemoto.delete(entity);
	}
}
