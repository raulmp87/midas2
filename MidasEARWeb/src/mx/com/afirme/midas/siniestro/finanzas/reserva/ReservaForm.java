package mx.com.afirme.midas.siniestro.finanzas.reserva;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReservaForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String tipoMoneda;
	private String tipoCambioPromedio;
	private String descripcionEstimacion;
	private List<ReservaDetalleBean> listaSumaAseguradaCobertura;
	private String totalSumaAsegurada;
	private List<ReservaDetalleBean> listaEstimacionInicialCobertura;

	private String totalEstimacionInicial;
	
	private String idToReporteSiniestro;
	
	private String[] estimacion;
	
	private List<ReservaDetalleBean> listaReservaDetalleBean;

	private List<ReservaDetalleBean> listaReservaPorAutorizar;
	private String tipoAjuste;
	private int idToReservaEstimada;
	private String totalReservaAutorizar;	
	
	private String[] reservas;
	private String origen;
	
	private String totalSumaAseguradaFormato;
	private String totalReservaAutorizarFormato;
	private String totalEstimacionInicialFormato;
	private String identificador;

	public ReservaForm() {
	}
	
	/**
	 * @return the listaReservaDetalleBean
	 */
	public List<ReservaDetalleBean> getListaReservaDetalleBean() {
		return listaReservaDetalleBean;
	}

	/**
	 * @param listaReservaDetalleBean the listaReservaDetalleBean to set
	 */
	public void setListaReservaDetalleBean(
			List<ReservaDetalleBean> listaReservaDetalleBean) {
		this.listaReservaDetalleBean = listaReservaDetalleBean;
	}
	/**
	 * @return the listaSumaAseguradaCobertura
	 */
	public List<ReservaDetalleBean> getListaSumaAseguradaCobertura() {
		return listaSumaAseguradaCobertura;
	}


	/**
	 * @param listaSumaAseguradaCobertura the listaSumaAseguradaCobertura to set
	 */
	public void setListaSumaAseguradaCobertura(
			List<ReservaDetalleBean> listaSumaAseguradaCobertura) {
		this.listaSumaAseguradaCobertura = listaSumaAseguradaCobertura;
	}


	/**
	 * @return the listaEstimacionInicialCobertura
	 */
	public List<ReservaDetalleBean> getListaEstimacionInicialCobertura() {
		return listaEstimacionInicialCobertura;
	}


	/**
	 * @param listaEstimacionInicialCobertura the listaEstimacionInicialCobertura to set
	 */
	public void setListaEstimacionInicialCobertura(
			List<ReservaDetalleBean> listaEstimacionInicialCobertura) {
		this.listaEstimacionInicialCobertura = listaEstimacionInicialCobertura;
	}

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}



	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}


	/**
	 * @return the tipoCambioPromedio
	 */
	public String getTipoCambioPromedio() {
		return tipoCambioPromedio;
	}



	/**
	 * @param tipoCambioPromedio the tipoCambioPromedio to set
	 */
	public void setTipoCambioPromedio(String tipoCambioPromedio) {
		this.tipoCambioPromedio = tipoCambioPromedio;
	}



	/**
	 * @return the descripcionEstimacion
	 */
	public String getDescripcionEstimacion() {
		return descripcionEstimacion;
	}



	/**
	 * @param descripcionEstimacion the descripcionEstimacion to set
	 */
	public void setDescripcionEstimacion(String descripcionEstimacion) {
		this.descripcionEstimacion = descripcionEstimacion;
	}



	/**
	 * @return the listaSumaAseguradaCobertura
	 
	public List getListaSumaAseguradaCobertura() {
		return listaSumaAseguradaCobertura;
	}



	/**
	 * @param listaSumaAseguradaCobertura the listaSumaAseguradaCobertura to set
	 
	public void setListaSumaAseguradaCobertura(List listaSumaAseguradaCobertura) {
		this.listaSumaAseguradaCobertura = listaSumaAseguradaCobertura;
	}
	 */


	/**
	 * @return the totalSumaAsegurada
	 */
	public String getTotalSumaAsegurada() {
		return totalSumaAsegurada;
	}



	/**
	 * @param totalSumaAsegurada the totalSumaAsegurada to set
	 */
	public void setTotalSumaAsegurada(String totalSumaAsegurada) {
		this.totalSumaAsegurada = totalSumaAsegurada;
	}



	/**
	 * @return the listaEstimacionInicialCobertura
	 
	public List getListaEstimacionInicialCobertura() {
		return listaEstimacionInicialCobertura;
	}



	/**
	 * @param listaEstimacionInicialCobertura the listaEstimacionInicialCobertura to set
	 
	public void setListaEstimacionInicialCobertura(
			List listaEstimacionInicialCobertura) {
		this.listaEstimacionInicialCobertura = listaEstimacionInicialCobertura;
	}
	*/


	/**
	 * @return the totalEstimacionInicial
	 */
	public String getTotalEstimacionInicial() {
		return totalEstimacionInicial;
	}



	/**
	 * @param totalEstimacionInicial the totalEstimacionInicial to set
	 */
	public void setTotalEstimacionInicial(String totalEstimacionInicial) {
		this.totalEstimacionInicial = totalEstimacionInicial;
	}




	/**
	 * @return the estimacion
	 */
	public String[] getEstimacion() {
		return estimacion;
	}

	/**
	 * @param estimacion the estimacion to set
	 */
	public void setEstimacion(String[] estimacion) {
		this.estimacion = estimacion;
	}


	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}

	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}

	public void setListaReservaPorAutorizar(List<ReservaDetalleBean> listaReservaPorAutorizar){
		this.listaReservaPorAutorizar = listaReservaPorAutorizar;
	}
	
	public List<ReservaDetalleBean> getListaReservaPorAutorizar(){
		return this.listaReservaPorAutorizar;
	}
	
	public String getTipoAjuste(){
		return this.tipoAjuste;
	}
	
	public void setTipoAjuste(String tipoAjuste){
		this.tipoAjuste = tipoAjuste;	
	}

	public void setIdToReservaEstimada(int idToReservaEstimada) {
		this.idToReservaEstimada = idToReservaEstimada;
	}


	public int getIdToReservaEstimada() {
		return idToReservaEstimada;
	}

	public void setTotalReservaAutorizar(String totalReservaAutorizar) {
		this.totalReservaAutorizar = totalReservaAutorizar;
	}


	public String getTotalReservaAutorizar() {
		return totalReservaAutorizar;
	}



	/**
	 * @return the reservas
	 */
	public String[] getReservas() {
		return reservas;
	}



	/**
	 * @param reservas the reservas to set
	 */
	public void setReservas(String[] reservas) {
		this.reservas = reservas;
	}



	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}



	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the totalSumaAseguradaFormato
	 */
	public String getTotalSumaAseguradaFormato() {
		return totalSumaAseguradaFormato;
	}

	/**
	 * @param totalSumaAseguradaFormato the totalSumaAseguradaFormato to set
	 */
	public void setTotalSumaAseguradaFormato(String totalSumaAseguradaFormato) {
		this.totalSumaAseguradaFormato = totalSumaAseguradaFormato;
	}

	/**
	 * @return the totalReservaAutorizarFormato
	 */
	public String getTotalReservaAutorizarFormato() {
		return totalReservaAutorizarFormato;
	}

	/**
	 * @param totalReservaAutorizarFormato the totalReservaAutorizarFormato to set
	 */
	public void setTotalReservaAutorizarFormato(String totalReservaAutorizarFormato) {
		this.totalReservaAutorizarFormato = totalReservaAutorizarFormato;
	}

	/**
	 * @return the totalEstimacionInicialFormato
	 */
	public String getTotalEstimacionInicialFormato() {
		return totalEstimacionInicialFormato;
	}

	/**
	 * @param totalEstimacionInicialFormato the totalEstimacionInicialFormato to set
	 */
	public void setTotalEstimacionInicialFormato(
			String totalEstimacionInicialFormato) {
		this.totalEstimacionInicialFormato = totalEstimacionInicialFormato;
	}

	/**
	 * @return the identificador
	 */
	public String getIdentificador() {
		return identificador;
	}

	/**
	 * @param identificador the identificador to set
	 */
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	
	
	
	

}
