package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.TerminoAjusteDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class TerminarSiniestroForm extends MidasBaseForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroReporte;
	private BigDecimal idReporteSiniestro;
	private Double sumaAsegurada;
	private List<TerminoAjusteDTO> terminosAjuste;
	private String descripcionTerminoAjuste;
	private String idTerminoAjuste; 
	private List<?> listaCoberturas;
	private String tipoMoneda;
	
	public void setIdReporteSiniestro(BigDecimal idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}
	public BigDecimal getIdReporteSiniestro() {
		return idReporteSiniestro;
	}
	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setTerminosAjuste(List<TerminoAjusteDTO> terminosAjuste) {
		this.terminosAjuste = terminosAjuste;
	}
	public List<TerminoAjusteDTO> getTerminosAjuste() {
		return terminosAjuste;
	}
	public void setDescripcionTerminoAjuste(String descripcionTerminoAjuste) {
		this.descripcionTerminoAjuste = descripcionTerminoAjuste;
	}
	public String getDescripcionTerminoAjuste() {
		return descripcionTerminoAjuste;
	}
	public void setIdTerminoAjuste(String idTerminoAjuste) {
		this.idTerminoAjuste = idTerminoAjuste;
	}
	public String getIdTerminoAjuste() {
		return idTerminoAjuste;
	}
	public void setListaCoberturas(List<?> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public List<?> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	public String getNumeroReporte() {
		return numeroReporte;
	}
	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}	
	
}
