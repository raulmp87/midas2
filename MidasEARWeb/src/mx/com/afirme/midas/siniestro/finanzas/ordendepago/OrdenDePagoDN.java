package mx.com.afirme.midas.siniestro.finanzas.ordendepago;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDN;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadOrdenPagoDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle.DetalleOrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class OrdenDePagoDN {
	private static final OrdenDePagoDN INSTANCIA = new OrdenDePagoDN();

	public static OrdenDePagoDN getInstancia() {
		return OrdenDePagoDN.INSTANCIA;
	}

	public OrdenDePagoDTO guardarOrdenDePago(OrdenDePagoDTO ordenDePagoDTO)throws SystemException,ExcepcionDeAccesoADatos {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.guardarOrdenDePago(ordenDePagoDTO);
	}
	
	public  List<OrdenDePagoDTO> findByProperty(String propertyName, Object value)	throws ExcepcionDeAccesoADatos, SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.findByProperty(propertyName, value);	
	}
	
	public  List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestro(BigDecimal idToReporteSiniestro)	throws ExcepcionDeAccesoADatos, SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.obtenerOrdenesDePagoXReporteSiniestro(idToReporteSiniestro);	
	}
	
	public  List<OrdenDePagoDTO> obtenerOrdenesDePagoXReporteSiniestroIndividual(BigDecimal idToReporteSiniestro)	throws ExcepcionDeAccesoADatos, SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.obtenerOrdenesDePagoXReporteSiniestroIndividual(idToReporteSiniestro);	
	}
	
	public OrdenDePagoDTO findById(BigDecimal idToOrdenPago)	throws ExcepcionDeAccesoADatos, SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.findById(idToOrdenPago);
	}
	
	public OrdenDePagoDTO update(OrdenDePagoDTO ordenDePagoDTO)	throws ExcepcionDeAccesoADatos, SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.update(ordenDePagoDTO);
	}

	public void borrar(OrdenDePagoDTO ordenDePagoDTO) throws SystemException{
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		ordenDePagoSN.borrar(ordenDePagoDTO);		
	}	
	
	public OrdenDePagoDTO obtenerOrdenPorIdAutorizacionTecnica(BigDecimal idAutorizacionTecnica) throws SystemException{
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.obtenerOrdenPorIdAutorizacionTecnica(idAutorizacionTecnica);				
	}
	
	public Map<String, String> generarOrdenDePago(String[] autorizaciones, String usuario){
		
		return null;
	}
	/**
	 * TODO: cambiar todo a la capa de Servicios.
	 */
	@Deprecated
	public OrdenDePagoDTO generarOrdenDePago(OrdenDePagoDTO entity, AutorizacionTecnicaDTO autorizacionTecnicaDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		BigDecimal idSolicitudCheque = null;
		OrdenDePagoDTO ordenDePagoDTO = this.guardarOrdenDePago(entity);				
		
		idSolicitudCheque = contabilizarOrdenDePago(autorizacionTecnicaDTO, ordenDePagoDTO.getIdToOrdenPago(), usuario);
		
		if(idSolicitudCheque != null){
			ordenDePagoDTO.setIdSolicitudCheque(idSolicitudCheque);
			this.update(ordenDePagoDTO);
			actualizarMovimiento(autorizacionTecnicaDTO, usuario);					
		}else{
			this.borrar(ordenDePagoDTO);
		}
						
		return ordenDePagoDTO;		
	}	
	
	public OrdenDePagoDTO generarOrdenDePago(OrdenDePagoDTO entity,
			List<AutorizacionTecnicaDTO> autorizaciones, String usuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		BigDecimal idSolicitudCheque = null;
		OrdenDePagoDTO ordenDePagoDTO = this.guardarOrdenDePago(entity);
		
		idSolicitudCheque = contabilizarOrdenDePago(autorizaciones,
				ordenDePagoDTO.getIdToOrdenPago(), usuario);
		
		if(idSolicitudCheque != null){
			ordenDePagoDTO.setIdSolicitudCheque(idSolicitudCheque);
			this.update(ordenDePagoDTO);
			actualizarMovimiento(autorizaciones, usuario);
			DetalleOrdenPagoDTO detalle= new DetalleOrdenPagoDTO();
			detalle.setOrdenDePagoDTO(ordenDePagoDTO);
			for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
				detalle.setAutorizacionTecnicaDTO(autorizacion);
				this.agregarDetalleOrdenDePago(detalle);
			}
			
		}else{
			this.borrar(ordenDePagoDTO);
		}
						
		return ordenDePagoDTO;		
	}		
	private BigDecimal contabilizarOrdenDePago(AutorizacionTecnicaDTO autorizacionTecnicaDTO, BigDecimal idToOrdenPago, String usuario){
		BigDecimal idSolicitudCheque = null;
		
		ContabilidadOrdenPagoDN contabilidadOP = ContabilidadOrdenPagoDN.getInstancia();

		try {
			idSolicitudCheque = contabilidadOP.contabilizarOrdenDePago(autorizacionTecnicaDTO, idToOrdenPago, usuario);
		} catch (SystemException e) {
			LogDeMidasWeb.log("contabilizarOrdenDePago", Level.SEVERE, e);
		}
		
		return idSolicitudCheque;
	}
	
	private BigDecimal contabilizarOrdenDePago(
			List<AutorizacionTecnicaDTO> autorizaciones,
			BigDecimal idToOrdenPago, String usuario) {
		BigDecimal idSolicitudCheque = null;

		ContabilidadOrdenPagoDN contabilidadOP = ContabilidadOrdenPagoDN
				.getInstancia();

		try {
			idSolicitudCheque = contabilidadOP.contabilizarOrdenDePago(idToOrdenPago, usuario, autorizaciones, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO);
		} catch (SystemException e) {
			LogDeMidasWeb.log("contabilizarOrdenDePago", Level.SEVERE, e);
		}

		return idSolicitudCheque;
	}	
	private void actualizarMovimiento(AutorizacionTecnicaDTO autorizacionTecnicaDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		if(autorizacionTecnicaDTO.getGastoSiniestroDTO() != null){
			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
			GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN.findById(autorizacionTecnicaDTO.getGastoSiniestroDTO().getIdToGastoSiniestro());
			gastoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.ORDEN_PAGO);
			gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);
		}else if(autorizacionTecnicaDTO.getIndemnizacionDTO() != null){
			IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion());
			indemnizacionDTO.getEstatusFinanzasDTO().setIdTcEstatusfinanzas(EstatusFinanzasDTO.ORDEN_PAGO);
			indemnizacionDN.modificar(indemnizacionDTO);
			if(indemnizacionDTO.getVariosPagos() == false || (indemnizacionDTO.getVariosPagos() == true && indemnizacionDTO.getUltimoPago() == true)){
				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(usuario);
				ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(autorizacionTecnicaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
				reporteSiniestroDTO.getReporteEstatus().setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_EMITIR_CHEQUE);
				reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			}
		}
	}

	private void actualizarMovimiento(
			List<AutorizacionTecnicaDTO> autorizaciones, String usuario)
			throws ExcepcionDeAccesoADatos, SystemException {
		for (AutorizacionTecnicaDTO autorizacion : autorizaciones) {
			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
			GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN
					.findById(autorizacion.getGastoSiniestroDTO()
							.getIdToGastoSiniestro());
			gastoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(
					EstatusFinanzasDTO.ORDEN_PAGO);
			gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);

		}
	}
	
	public OrdenDePagoDTO obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(BigDecimal idAutorizacionTecnica) throws SystemException{
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(idAutorizacionTecnica);				
	}
	
	public OrdenDePagoDTO buscaPrimerRegistro() throws SystemException {
		return new OrdenDePagoSN().buscaPrimerRegistro();
	}
	
//	private void actualizaOrdenCheque(BigDecimal idOrdenPago, String estatusSolicitud) throws Exception {
//		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
//		ordenDePagoSN.actualizaOrdenCheque(idOrdenPago, estatusSolicitud);						
//	}
	
	public void verificarEstatus(BigDecimal idToOrdenDePago, String nombreUsuario) throws Exception{
		BigDecimal idSolicitudCheque = null;
		try {
			OrdenDePagoDTO ordenDePagoDTO = this.findById(idToOrdenDePago);
			if(ordenDePagoDTO.getIdSolicitudCheque()!=null){
				idSolicitudCheque = ordenDePagoDTO.getIdSolicitudCheque();
			}
			SolicitudChequeDN solicitudChequeDN = SolicitudChequeDN.getInstancia(nombreUsuario);
			SolicitudChequeDTO estatusOrdenDePago = solicitudChequeDN.consultaEstatusSolicitudCheque(idSolicitudCheque);
//			SinietroServiciosDN sinietroServiciosDN = SinietroServiciosDN.getInstancia(nombreUsuario);
//			SolicitudChequeDTO estatusOrdenDePago = sinietroServiciosDN.consultaEstatusSolicitudCheque(idSolicitudCheque);
//			String estatusOrdenDePago = "TERMINADO";
			if (estatusOrdenDePago != null){
				this.actualizaOrdenCheque(idToOrdenDePago, estatusOrdenDePago,nombreUsuario); 
			}
		}catch (ExcepcionDeAccesoADatos e) {
			LogDeMidasWeb.log("No se pudo recuperar la orden de pago", Level.SEVERE, e);
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void procesarOrdenesPendientes(String nombreUsuario) throws Exception{
		SolicitudChequeDN solicitudChequeDN = SolicitudChequeDN.getInstancia(nombreUsuario);
//		SinietroServiciosDN sinietroServiciosDN = SinietroServiciosDN.getInstancia(nombreUsuario);
		SolicitudChequeDTO estatusActual = null;
		BigDecimal idSolicitudCheque = null;
		System.out.println("Entrando a procesarOrdenesPendientes");
		List<OrdenDePagoDTO> ordenPagoList = 
			this.findByProperty("estatus", OrdenDePagoDTO.ESTATUS_PENDIENTE);
		
		if (ordenPagoList != null) {
			System.out.println("Las Ordenes pendientes a Procesar son: "+ordenPagoList.size());
			for (OrdenDePagoDTO ordenPago : ordenPagoList) {
				System.out.println("Consultando estatus actual de la solicitud de cheque con id de Orden de pago: " + 
						ordenPago.getIdToOrdenPago() + " e id de Solicitud de cheque: " + ordenPago.getIdSolicitudCheque());
//				LogDeMidasWeb.log("Consultando estatus actual de la solicitud de cheque con id de Orden de pago: " + 
//						ordenPago.getIdToOrdenPago() + " e id de Solicitud de cheque: " + ordenPago.getIdSolicitudCheque(),
//						Level.INFO, null);
				
				if(ordenPago.getIdSolicitudCheque()!=null){
					idSolicitudCheque = ordenPago.getIdSolicitudCheque();
				}
				estatusActual = solicitudChequeDN.consultaEstatusSolicitudCheque(idSolicitudCheque);
				
				if (estatusActual != null) {
					System.out.println("estatus actual de la solicitud de cheque con id de Orden de pago: "+ordenPago.getIdToOrdenPago() +" Estatus: "+estatusActual.getSituacionPago() );
					this.actualizaOrdenCheque(ordenPago.getIdToOrdenPago(), estatusActual,nombreUsuario);
					
					System.out.println("Ejecucion exitosa en la Actualizacion de estatus la solicitud de cheque con id de Orden de pago: " + 
							ordenPago.getIdToOrdenPago() + " e id de Solicitud de cheque: " + ordenPago.getIdSolicitudCheque());
					
//					LogDeMidasWeb.log("Ejecucion exitosa en la Actualizacion de estatus la solicitud de cheque con id de Orden de pago: " + 
//							ordenPago.getIdToOrdenPago() + " e id de Solicitud de cheque: " + ordenPago.getIdSolicitudCheque(),
//							Level.INFO, null);					
				}				
			}
		}		
	}
	
	
	public void actualizaOrdenCheque(BigDecimal idOrdenPago, SolicitudChequeDTO solicitudChequeDTO,String nombreUsuario) throws Exception{
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		String estatusSolicitud = solicitudChequeDTO.getSituacionPago();
		Date fechaMovimiento = null;
		if(estatusSolicitud != null && idOrdenPago != null){
			if(estatusSolicitud.equalsIgnoreCase("PROCESO")){
				LogDeMidasWeb.log("La solicitud esta en proceso : " + idOrdenPago.toString(), Level.INFO, null);
			}else if(estatusSolicitud.equalsIgnoreCase("EN SOLICITUD MIZAR")){
				LogDeMidasWeb.log("La solicitud fue requerida a MIZAR : " + idOrdenPago.toString(), Level.INFO, null);
			}else if(estatusSolicitud.equalsIgnoreCase("CANCELADO")){
				OrdenDePagoDTO entity = null;
				
				try{
					entity = findById(idOrdenPago);
					if(entity!= null){
						if(entity.getEstatus().shortValue() == OrdenDePagoDTO.ESTATUS_PAGADA){
							if(solicitudChequeDTO.getFechaMovimiento() != null ){
								fechaMovimiento = solicitudChequeDTO.getFechaMovimiento();
							}else{
								fechaMovimiento = Calendar.getInstance().getTime();
							}
							
							List<DetalleOrdenPagoDTO> detalles = DetalleOrdenDePagoDN.getInstancia().detallesOrdenDePago(idOrdenPago);
							for(DetalleOrdenPagoDTO detalleOrden: detalles){
								AutorizacionTecnicaDTO autorizacionTecnicaDTO = detalleOrden.getAutorizacionTecnicaDTO();
								switch(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica()){
									case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO:{
										siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO,
												IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION_POR_RETENCION,
												autorizacionTecnicaDTO.getGastoSiniestroDTO(),fechaMovimiento,nombreUsuario);
										//break;
									}
									case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION:{
										siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION,
												IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION_POR_RETENCION,
												autorizacionTecnicaDTO.getIndemnizacionDTO(),fechaMovimiento,nombreUsuario);
										
										//break;
									}
								}								
							}
						}
						entity.setEstatus(OrdenDePagoDTO.ESTATUS_CANCELADA);
						entity.setFechaPago(fechaMovimiento);
						this.update(entity);
						siniestroMovimientosDN.completarOrdenDePago(entity, estatusSolicitud,nombreUsuario);
						
					}
				}catch(RuntimeException ex){
					LogDeMidasWeb.log("Error en actualizaOrdenCheque, actualizar estatus" + estatusSolicitud, Level.WARNING, ex);
				}				
			}else if(estatusSolicitud.equalsIgnoreCase("TERMINADO")){
				OrdenDePagoDTO entity = null;
				
				try{
					entity = findById(idOrdenPago);
					if(entity!= null){
						if(entity.getFechaPago() == null){
							entity.setEstatus(OrdenDePagoDTO.ESTATUS_PAGADA);
							entity.setCuentaBancariaEmisora(solicitudChequeDTO.getCuentaBancariaEmisora());
							entity.setTipo(solicitudChequeDTO.getTipoPago());
							entity.setFolio(solicitudChequeDTO.getFolio());
							if(solicitudChequeDTO.getFechaMovimiento() != null){
								entity.setFechaPago(solicitudChequeDTO.getFechaMovimiento());
								fechaMovimiento = solicitudChequeDTO.getFechaMovimiento();
							}
							this.update(entity);
							siniestroMovimientosDN.completarOrdenDePago(entity, estatusSolicitud,nombreUsuario);
							if(fechaMovimiento != null){
								List<DetalleOrdenPagoDTO> detalles = DetalleOrdenDePagoDN.getInstancia().detallesOrdenDePago(idOrdenPago);
								for(DetalleOrdenPagoDTO detalleOrden: detalles){
									AutorizacionTecnicaDTO autorizacionTecnicaDTO = detalleOrden.getAutorizacionTecnicaDTO();
									switch(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica()){
										case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO:{
											siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO,
													IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION,
													autorizacionTecnicaDTO.getGastoSiniestroDTO(),fechaMovimiento,nombreUsuario);
											//break;
										}
										case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION:{
											siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION,
													IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION_POR_RETENCION,
													autorizacionTecnicaDTO.getIndemnizacionDTO(),fechaMovimiento,nombreUsuario);
											
											//break;
										}
									}
									
								}
							}
						}
					}	
				}catch (Exception e) {
					LogDeMidasWeb.log(e.getMessage(), Level.WARNING, e);
					throw e;
				}								
			}else{
				LogDeMidasWeb.log("Estatus de solicitud desconocido : " + estatusSolicitud, Level.INFO, null);
			}
		}		
	}
	
	public List<DetalleOrdenPagoDTO> listarOrdenesDePagoFiltrado(
			SoportePagosDTO soportePagosDTO) throws SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		return ordenDePagoSN.listarOrdenesDePagoFiltrado(soportePagosDTO);
	}

	public void agregarDetalleOrdenDePago(DetalleOrdenPagoDTO detalle)
			throws SystemException {
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		ordenDePagoSN.agregarDetalleOrdenDePago(detalle);
	}
}
	