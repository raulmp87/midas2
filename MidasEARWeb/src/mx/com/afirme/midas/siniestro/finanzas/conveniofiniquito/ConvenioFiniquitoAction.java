package mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDN;
import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.reportes.ReporteSiniestro;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;

public class ConvenioFiniquitoAction extends MidasMappingDispatchAction {
	
	public ActionForward mostrarGenerarConvenioFiniquito(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		request.getSession().setAttribute("banderaRefresh", "true");
		ConvenioFiniquitoDN convenioFiniquitoDN = ConvenioFiniquitoDN.getInstancia();
		ConvenioFiniquitoForm convenioFiniquitoForm = (ConvenioFiniquitoForm)form;
		ConvenioFiniquitoDTO convenioFiniquitoDTO = new ConvenioFiniquitoDTO();
		
		String reglaNavegacion = Sistema.EXITOSO;
		String idToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		if (!UtileriasWeb.esCadenaVacia(idToReporteSiniestro))
			idToReporteSiniestro = convenioFiniquitoForm.getIdToReporteSiniestro();
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		try{
			reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(idToReporteSiniestro));
			reporteSiniestroDTO = reporteSiniestroDN.getPorId(reporteSiniestroDTO);
			convenioFiniquitoDTO = convenioFiniquitoDN.getPorReporteSiniestro(reporteSiniestroDTO);
			if (convenioFiniquitoDTO==null || convenioFiniquitoDTO.getIdToConvenioFiniquito()==null){
				convenioFiniquitoDTO = new ConvenioFiniquitoDTO();
				convenioFiniquitoDTO.setReporteSiniestro(reporteSiniestroDTO);
			}
			
			this.poblarForm(convenioFiniquitoDTO, convenioFiniquitoForm, request);
		}catch (Exception e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);		
	}
	
	public ActionForward mostrarRecibirConvenioFirmado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		ConvenioFiniquitoDN convenioFiniquitoDN = ConvenioFiniquitoDN.getInstancia();
		ConvenioFiniquitoForm convenioFiniquitoForm = (ConvenioFiniquitoForm)form;
		ConvenioFiniquitoDTO convenioFiniquitoDTO = new ConvenioFiniquitoDTO();
		
		String reglaNavegacion = Sistema.EXITOSO;
		String idToReporteSiniestro = request.getParameter("idToReporteSiniestro");
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		if (!UtileriasWeb.esCadenaVacia(idToReporteSiniestro))
			idToReporteSiniestro = convenioFiniquitoForm.getIdToReporteSiniestro();
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		try{
			reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(idToReporteSiniestro));
			reporteSiniestroDTO = reporteSiniestroDN.getPorId(reporteSiniestroDTO);
			convenioFiniquitoDTO = convenioFiniquitoDN.getPorReporteSiniestro(reporteSiniestroDTO);
			
			this.poblarForm(convenioFiniquitoDTO, convenioFiniquitoForm, request);
		}catch (Exception e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		}
		return mapping.findForward(reglaNavegacion);		
	}
	
	public void generarConvenioFiniquito(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ConvenioFiniquitoDTO convenioFiniquitoDTO = new ConvenioFiniquitoDTO();
		ConvenioFiniquitoDN convenioFiniquitoDN = ConvenioFiniquitoDN.getInstancia();
		ConvenioFiniquitoForm convenioFiniquitoForm = (ConvenioFiniquitoForm)form;
		String mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar");
		try{
			if (!UtileriasWeb.esCadenaVacia(convenioFiniquitoForm.getIdToConvenioFiniquito())){
				BigDecimal idToConvenioFiniquito = new BigDecimal(convenioFiniquitoForm.getIdToConvenioFiniquito());
				convenioFiniquitoDTO.setIdToConvenioFiniquito(idToConvenioFiniquito);
				convenioFiniquitoDTO = convenioFiniquitoDN.getPorId(convenioFiniquitoDTO);
			}
			this.poblarDTO(convenioFiniquitoForm, convenioFiniquitoDTO, request);
			this.llenarFormConCadenaVacia(convenioFiniquitoForm);
			mensaje += enviarCorreo(convenioFiniquitoForm, request, response);
			
			if (convenioFiniquitoDTO.getIdToConvenioFiniquito()!=null)
				convenioFiniquitoDN.modificar(convenioFiniquitoDTO);
			else{
				convenioFiniquitoDTO.setEstatus(ConvenioFiniquitoDTO.ESTATUS_CREADO);	
				convenioFiniquitoDN.agregar(convenioFiniquitoDTO);
			}
			
			if (convenioFiniquitoDTO!=null){
				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				ReporteSiniestroDTO reporteSiniestroDTO = convenioFiniquitoDTO.getReporteSiniestro();
				ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_RECEPCION_CONVENIO_FIRMADO);
				reporteSiniestroDN.actualizar(reporteSiniestroDTO);
				UtileriasWeb.imprimeMensajeXML("1", mensaje , response);
			}else{
				UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
			}
		}catch (Exception e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		}
	}
	
	public void recibirConvenioFirmado(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ConvenioFiniquitoDTO convenioFiniquitoDTO = new ConvenioFiniquitoDTO();
		ConvenioFiniquitoDN convenioFiniquitoDN = ConvenioFiniquitoDN.getInstancia();
		ConvenioFiniquitoForm convenioFiniquitoForm = (ConvenioFiniquitoForm)form;
		try{
			if (!UtileriasWeb.esCadenaVacia(convenioFiniquitoForm.getIdToConvenioFiniquito())){
				BigDecimal idToConvenioFiniquito = new BigDecimal(convenioFiniquitoForm.getIdToConvenioFiniquito());
				convenioFiniquitoDTO.setIdToConvenioFiniquito(idToConvenioFiniquito);
				convenioFiniquitoDTO = convenioFiniquitoDN.getPorId(convenioFiniquitoDTO);
			}
			
			if (convenioFiniquitoDTO!=null){
				ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
				ReporteSiniestroDTO reporteSiniestroDTO = convenioFiniquitoDTO.getReporteSiniestro();
				ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
				
				convenioFiniquitoDTO.setEstatus(ConvenioFiniquitoDTO.ESTATUS_RECIBIDO);
				convenioFiniquitoDN.modificar(convenioFiniquitoDTO);
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_GENERAR_AUTORIZACION_TECNICA);
				reporteSiniestroDN.actualizar(reporteSiniestroDTO);
				UtileriasWeb.imprimeMensajeXML("1", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.informacion.exito.guardar") , response);
			}else{
				UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
			}
		}catch (Exception e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "mensaje.excepcion.acceso.dto") , response);
		}
	}
	
	private void llenarFormConCadenaVacia(ConvenioFiniquitoForm convenioFiniquitoForm){
		if (convenioFiniquitoForm.getAjustador()==null)convenioFiniquitoForm.setAjustador("");
		if (convenioFiniquitoForm.getAsegurado()==null)convenioFiniquitoForm.setAsegurado("");
		if (convenioFiniquitoForm.getBancoReceptor()==null)convenioFiniquitoForm.setBancoReceptor("");
		if (convenioFiniquitoForm.getClaveProducto()==null)convenioFiniquitoForm.setClaveProducto("");
		if (convenioFiniquitoForm.getClaveRamo()==null)convenioFiniquitoForm.setClaveRamo("");
		if (convenioFiniquitoForm.getClaveSubRamo()==null)convenioFiniquitoForm.setClaveSubRamo("");
		if (convenioFiniquitoForm.getDescripcionDanio()==null)convenioFiniquitoForm.setDescripcionDanio("");
		if (convenioFiniquitoForm.getFechaConvenio()==null)convenioFiniquitoForm.setFechaConvenio("");
		if (convenioFiniquitoForm.getFechaFinVigencia()==null)convenioFiniquitoForm.setFechaFinVigencia("");
		if (convenioFiniquitoForm.getFechaInicioVigencia()==null)convenioFiniquitoForm.setFechaInicioVigencia("");
		if (convenioFiniquitoForm.getFechaReporte()==null)convenioFiniquitoForm.setFechaReporte("");
		if (convenioFiniquitoForm.getIndemnizacion()==null)convenioFiniquitoForm.setIndemnizacion("");
		if (convenioFiniquitoForm.getIndemnizacionLetra()==null)convenioFiniquitoForm.setIndemnizacionLetra("");
		if (convenioFiniquitoForm.getLugarConvenio()==null)convenioFiniquitoForm.setLugarConvenio("");
		if (convenioFiniquitoForm.getNombreBeneficiario()==null)convenioFiniquitoForm.setNombreBeneficiario("");
		if (convenioFiniquitoForm.getNumeroCuenta()==null)convenioFiniquitoForm.setNumeroCuenta("");
		if (convenioFiniquitoForm.getNumPoliza()==null)convenioFiniquitoForm.setNumPoliza("");
		if (convenioFiniquitoForm.getNumReporteSiniestro()==null)convenioFiniquitoForm.setNumReporteSiniestro("");
	}
	
	private String enviarCorreo(ConvenioFiniquitoForm convenioFiniquitoForm,HttpServletRequest request, HttpServletResponse response){
		try{
//			String noEnvioCorreo = "<br/> El correo del ajustador" + UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "errors.email", "");
			String noEnvioCorreo = "<br/> No se puede enviar el correo en este momento ";
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			if (UtileriasWeb.esCadenaVacia(convenioFiniquitoForm.getIdToReporteSiniestro()))
					return noEnvioCorreo;
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			reporteSiniestroDTO.setIdToReporteSiniestro(UtileriasWeb.regresaBigDecimal(convenioFiniquitoForm.getIdToReporteSiniestro()));
			reporteSiniestroDTO = reporteSiniestroDN.getPorId(reporteSiniestroDTO);
			if (reporteSiniestroDTO==null||reporteSiniestroDTO.getAjustador()==null||UtileriasWeb.esCadenaVacia(reporteSiniestroDTO.getAjustador().getEmail()))
				return noEnvioCorreo;
			String emailAjustador = reporteSiniestroDTO.getAjustador().getEmail(); 
//			String emailAjustador = "lohucar13@gmail.com";
			String contenido = "Buen d&iacute;a <br/>";
			contenido += "A continuaci&oacute;n se adjuntan el convenio de finiquito para el reporte de siniestro " + convenioFiniquitoForm.getNumReporteSiniestro();
			contenido += "<br/>Saludos cordiales.<br/> Atte. Sistema MIDAS";
			String titulo = "Datos del Convenio Finiquito";
			List<String> destinatarios = new ArrayList<String>();
			List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment byteArrayAttachment;
			
			//Objeto para reportes
			ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
			
			llenarFormConCadenaVacia(convenioFiniquitoForm);
			if(convenioFiniquitoForm.getTipoCuenta() != null){
				if (convenioFiniquitoForm.getTipoCuenta().equalsIgnoreCase("clabe")){
					convenioFiniquitoForm.setTipoCuenta("CLABE");
				}else{
					convenioFiniquitoForm.setTipoCuenta("CUENTA BANCARIA");
				}
			}else{
				convenioFiniquitoForm.setTipoCuenta("");
			}
			
			byte[] reporte = reporteSiniestro.mostrarReporteConvenioSiniestro(convenioFiniquitoForm, request);
			
//			if(convenioFiniquitoForm.getTipoCuenta() != null){
//				if (convenioFiniquitoForm.getTipoCuenta().equalsIgnoreCase("clabe")){
//					convenioFiniquitoForm.setTipoCuenta("clabe");
//				}else{
//					convenioFiniquitoForm.setTipoCuenta("cuentaBanco");
//				}
//			}else{
//				convenioFiniquitoForm.setTipoCuenta("0");
//			}
			
			//TODO Revisar si llega el correo al ajustador
			destinatarios.add(emailAjustador);
			byteArrayAttachment = new ByteArrayAttachment("ConvenioFiniquito", ByteArrayAttachment.TipoArchivo.PDF, reporte);
			adjuntos.add(byteArrayAttachment);
			mx.com.afirme.midas.sistema.mail.MailAction.enviaCorreo(destinatarios, titulo, contenido, adjuntos);
			//super.writeBytes(response, reporte, Sistema.TIPO_PDF, "conveniofiniquito");
		}catch (Exception e) {
			e.printStackTrace();
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return "";
	}
	
	public void poblarForm(ConvenioFiniquitoDTO convenioFiniquitoDTO,ConvenioFiniquitoForm convenioFiniquitoForm, HttpServletRequest request)
	throws SystemException{		
		
		//TODO revisar la clave del ramo (que dato es el adecuado)
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		PolizaDN polizaDN = PolizaDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		IndemnizacionDTO indemnizacionDTO = null;
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		AjustadorDN ajustadorDN = AjustadorDN.getInstancia();
		
		PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
		PolizaDTO polizaDTO = new PolizaDTO();
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();		
		List<RiesgoAfectadoDTO> riesgos = new ArrayList<RiesgoAfectadoDTO>();
		List<IndemnizacionDTO> indemnizaciones = new ArrayList<IndemnizacionDTO>();
		Double sumaAsegurada;
		
		if (convenioFiniquitoDTO==null) return;
	
		if (convenioFiniquitoDTO.getIdToConvenioFiniquito()!=null)
			convenioFiniquitoForm.setIdToConvenioFiniquito(convenioFiniquitoDTO.getIdToConvenioFiniquito().toString());
		
		if (!UtileriasWeb.esCadenaVacia(convenioFiniquitoDTO.getNumeroCuenta()))
				convenioFiniquitoForm.setNumeroCuenta(convenioFiniquitoDTO.getNumeroCuenta());
		
		if (convenioFiniquitoDTO.getFechaConvenio()!=null)
			convenioFiniquitoForm.setFechaConvenio(UtileriasWeb.getFechaString(convenioFiniquitoDTO.getFechaConvenio()));
		else 
			convenioFiniquitoForm.setFechaConvenio(UtileriasWeb.getFechaString(new Date()));
		
		if (!UtileriasWeb.esCadenaVacia(convenioFiniquitoDTO.getBancoReceptor()))
			convenioFiniquitoForm.setBancoReceptor(convenioFiniquitoDTO.getBancoReceptor());
		
		if (!UtileriasWeb.esCadenaVacia(convenioFiniquitoDTO.getNombreBeneficiario()))
			convenioFiniquitoForm.setNombreBeneficiario(convenioFiniquitoDTO.getNombreBeneficiario());
		
		if (!UtileriasWeb.esCadenaVacia(convenioFiniquitoDTO.getLugarConvenio()))
			convenioFiniquitoForm.setLugarConvenio(convenioFiniquitoDTO.getLugarConvenio());
		
		if (convenioFiniquitoDTO.getTipoCuenta() == ConvenioFiniquitoDTO.TIPO_ESTADO_CUENTA){
			convenioFiniquitoForm.setTipoCuenta("cuentaBanco");
		}else{ 
			convenioFiniquitoForm.setTipoCuenta("clabe");
		}
		
		if (convenioFiniquitoDTO.getIndemnizacion()!=null && convenioFiniquitoDTO.getIndemnizacion().getIdToIndemnizacion()!=null)
			convenioFiniquitoForm.setIdToIndemnizacion(convenioFiniquitoDTO.getIndemnizacion().getIdToIndemnizacion().toString());
		
		if (convenioFiniquitoDTO.getPagoconcheque()!=null){
			convenioFiniquitoForm.setPagoConCheque(convenioFiniquitoDTO.getPagoconcheque().toString());
		}
		
		convenioFiniquitoForm.setEstatus(convenioFiniquitoDTO.getEstatus());
		
		reporteSiniestroDTO = convenioFiniquitoDTO.getReporteSiniestro();
		//Si existe un reporte de siniestro asociado
		if (reporteSiniestroDTO!=null && reporteSiniestroDTO.getIdToReporteSiniestro()!=null){
			AjustadorDTO ajustadorDTO= new AjustadorDTO();
			ajustadorDTO.setIdTcAjustador(reporteSiniestroDTO.getAjustador().getIdTcAjustador());
			ajustadorDTO = ajustadorDN.getPorId(ajustadorDTO);
			convenioFiniquitoForm.setAjustador(ajustadorDTO.getNombre());
			convenioFiniquitoForm.setIdToReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro().toString());
						
			double indemnizacionTotal = 0.0;
			
			indemnizaciones = indemnizacionDN.obtenIndemnizacionPorEstatusNOCancelado(reporteSiniestroDTO);
			if (indemnizaciones!=null&&indemnizaciones.size()>0){
				int i = 0;
				indemnizacionDTO = indemnizaciones.get(0);
				for (IndemnizacionDTO indemnizacion: indemnizaciones){
					indemnizacionTotal += indemnizacion.getTotalPago();
					if (indemnizacion.getVariosPagos() && indemnizacion.getUltimoPago())
						indemnizacionDTO = indemnizaciones.get(i); 
					i++;
				}
			}
			if (indemnizacionDTO!=null && indemnizacionDTO.getIdToIndemnizacion()!=null)
				convenioFiniquitoForm.setIdToIndemnizacion(indemnizacionDTO.getIdToIndemnizacion().toString());
			
			convenioFiniquitoForm.setIndemnizacion(UtileriasWeb.formatoMoneda(new Double(indemnizacionTotal)));		

			String numeroLetra = "";
			numeroLetra = NumeroALetra.convertir( indemnizacionTotal ).trim(); 
			
			convenioFiniquitoForm.setIndemnizacionLetra( numeroLetra.toUpperCase() ) ;

			if (!UtileriasWeb.esCadenaVacia(reporteSiniestroDTO.getNumeroReporte()))
				convenioFiniquitoForm.setNumReporteSiniestro(reporteSiniestroDTO.getNumeroReporte());
			if (reporteSiniestroDTO.getFechaHoraReporte()!=null)
				convenioFiniquitoForm.setFechaReporte(UtileriasWeb.getFechaString(reporteSiniestroDTO.getFechaHoraReporte()));
			if (reporteSiniestroDTO.getNumeroPoliza()!=null)
				convenioFiniquitoForm.setNumPoliza(reporteSiniestroDTO.getNumeroPoliza().toString());
			
			if (reporteSiniestroDTO.getNumeroPoliza()!=null){
				polizaDTO = polizaDN.getPorId(reporteSiniestroDTO.getNumeroPoliza());
				polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza());
				if (polizaSoporteDanosDTO!=null){
						convenioFiniquitoForm.setClaveProducto(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
					if (polizaSoporteDanosDTO.getNumeroPoliza()!=null)
						convenioFiniquitoForm.setNumPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
					
					if (!UtileriasWeb.esCadenaVacia(polizaSoporteDanosDTO.getDescripcionTipoPoliza()))
						convenioFiniquitoForm.setClaveSubRamo(polizaSoporteDanosDTO.getDescripcionTipoPoliza());					
					convenioFiniquitoForm.setClaveRamo(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
					if (polizaSoporteDanosDTO.getFechaInicioVigencia()!=null)
						convenioFiniquitoForm.setFechaInicioVigencia(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaInicioVigencia()));
					if (polizaSoporteDanosDTO.getFechaFinVigencia()!=null)
						convenioFiniquitoForm.setFechaFinVigencia(UtileriasWeb.getFechaString(polizaSoporteDanosDTO.getFechaFinVigencia()));
					if (!UtileriasWeb.esCadenaVacia(polizaSoporteDanosDTO.getNombreAsegurado()))
							convenioFiniquitoForm.setAsegurado(polizaSoporteDanosDTO.getNombreAsegurado());
					if (polizaDTO!=null)
						if (polizaDTO.getCotizacionDTO()!=null&&polizaDTO.getCotizacionDTO().getSolicitudDTO()!=null
								&&polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO()!=null)
							convenioFiniquitoForm.setClaveRamo(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getNombreComercial());
					convenioFiniquitoForm.setTipoMoneda(polizaSoporteDanosDTO.getDescripcionMoneda());
				}
				
				String descripcionDanio = "";
				RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = null;
				riesgos = riesgoAfectadoDN.listarRiesgosAfectadosPara(reporteSiniestroDTO.getIdToReporteSiniestro());
				if (riesgos!=null && riesgos.size()>0){
					for (RiesgoAfectadoDTO riesgoAfectadoDTO:riesgos){
						riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
						riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(riesgoAfectadoDTO.getId().getIdtoriesgo());
						descripcionDanio += riesgoSoporteDanosDTO.getDescripcionRiesgo() + ",";
					}
					if (!UtileriasWeb.esCadenaVacia(descripcionDanio)){
						descripcionDanio = descripcionDanio.substring(0, descripcionDanio.length()-1);
						convenioFiniquitoForm.setDescripcionDanio(descripcionDanio);
					}
				}
				sumaAsegurada = riesgoAfectadoDN.obtenerTotalSumaAsegurada(riesgoAfectadoDN.listarPorReporteSiniestro(
													reporteSiniestroDTO.getIdToReporteSiniestro()));
				convenioFiniquitoForm.setSumaAsegurada(UtileriasWeb.formatoMoneda(sumaAsegurada));
			}
		}//End ReporteSiniestro
	}//Function PoblarForm
	
	private void poblarDTO(ConvenioFiniquitoForm form, ConvenioFiniquitoDTO convenioFiniquitoDTO, HttpServletRequest request) 
		throws SystemException, ParseException{
		if (form==null)return;
		
		if (!UtileriasWeb.esCadenaVacia(form.getIdToReporteSiniestro())){
			BigDecimal idToReporteSiniestro = new BigDecimal(form.getIdToReporteSiniestro());
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
			convenioFiniquitoDTO.setReporteSiniestro(reporteSiniestroDTO);
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getIdToIndemnizacion())){
			BigDecimal idToIndemnizacion = new BigDecimal(form.getIdToIndemnizacion());
			IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
			IndemnizacionDTO indemnizacionDTO = new IndemnizacionDTO();
			indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(idToIndemnizacion);
			convenioFiniquitoDTO.setIndemnizacion(indemnizacionDTO);
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getDescripcionDanio())){					
			if(form.getDescripcionDanio().length() > 255){
				convenioFiniquitoDTO.setDescripcionDanios(form.getDescripcionDanio().substring(0, 254));
			}else{
				convenioFiniquitoDTO.setDescripcionDanios(form.getDescripcionDanio());
			}						
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getIndemnizacion()))
			convenioFiniquitoDTO.setMontoIndemnizacion(Double.valueOf(UtileriasWeb.eliminaFormatoMoneda(form.getIndemnizacion())));
		if (!UtileriasWeb.esCadenaVacia(form.getTipoCuenta())){
			if (form.getTipoCuenta().equalsIgnoreCase("clabe")){
				convenioFiniquitoDTO.setTipoCuenta(ConvenioFiniquitoDTO.TIPO_CLABE);
			}else{
				convenioFiniquitoDTO.setTipoCuenta(ConvenioFiniquitoDTO.TIPO_ESTADO_CUENTA);
			}
		}else{
			convenioFiniquitoDTO.setTipoCuenta(ConvenioFiniquitoDTO.TIPO_ESTADO_CUENTA);
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getNumeroCuenta())){
			convenioFiniquitoDTO.setNumeroCuenta(form.getNumeroCuenta());
		}else{
			convenioFiniquitoDTO.setNumeroCuenta(null);
		}
		if (!UtileriasWeb.esCadenaVacia(form.getFechaConvenio()))
			convenioFiniquitoDTO.setFechaConvenio(UtileriasWeb.getFechaFromString(form.getFechaConvenio()));
		if (!UtileriasWeb.esCadenaVacia(form.getBancoReceptor())){
			convenioFiniquitoDTO.setBancoReceptor(form.getBancoReceptor());
		}else{
			convenioFiniquitoDTO.setBancoReceptor(null);
		}
		if (!UtileriasWeb.esCadenaVacia(form.getNombreBeneficiario())){
			convenioFiniquitoDTO.setNombreBeneficiario(form.getNombreBeneficiario());
		}else{
			convenioFiniquitoDTO.setNombreBeneficiario(null);
		}
		if (!UtileriasWeb.esCadenaVacia(form.getLugarConvenio())){
			convenioFiniquitoDTO.setLugarConvenio(form.getLugarConvenio());
		}else{
			convenioFiniquitoDTO.setLugarConvenio(null);
		}
		if (form.getPagoConCheque()!= null ){
			convenioFiniquitoDTO.setPagoconcheque(new BigDecimal(form.getPagoConCheque()));
		}else{
			convenioFiniquitoDTO.setPagoconcheque(new BigDecimal(0));
		}
	}
}
