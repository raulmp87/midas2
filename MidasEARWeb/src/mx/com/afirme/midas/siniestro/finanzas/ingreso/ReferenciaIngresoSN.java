package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.interfaz.ingreso.referencia.ReferenciaIngresoDTO;
import mx.com.afirme.midas.interfaz.ingreso.referencia.ReferenciaIngresoFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReferenciaIngresoSN {
	ReferenciaIngresoFacadeRemote beanRemoto;
	
	ReferenciaIngresoSN() throws SystemException{
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(ReferenciaIngresoFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}

		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public List<ReferenciaIngresoDTO> obtenerListaReferenciasIngreso(String nombreUsuario){
		try {
			return beanRemoto.obtenerReferenciasIngreso(nombreUsuario);
		} catch (Exception e) {
			LogDeMidasWeb.log("Ocurri� un error al consultar las referencias de ingresos en seycos.", Level.SEVERE, e);
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
}
