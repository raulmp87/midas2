package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IndemnizacionRiesgoCoberturaDN {
	
	public static final IndemnizacionRiesgoCoberturaDN INSTANCIA = new IndemnizacionRiesgoCoberturaDN();

	public static IndemnizacionRiesgoCoberturaDN getInstancia (){
		return IndemnizacionRiesgoCoberturaDN.INSTANCIA;
	}
	
	public String agregar(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.agregar(indemnizacionRiesgoCoberturaDTO);
	}
	
	public String borrar (IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.borrar(indemnizacionRiesgoCoberturaDTO);
	}
	
	public String modificar (IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.modificar(indemnizacionRiesgoCoberturaDTO);
	}
	
	public IndemnizacionRiesgoCoberturaDTO getIndemnizacionRiesgoCoberturaPorId(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.getIndemnizacionRiesgoCoberturaPorId(indemnizacionRiesgoCoberturaDTO);
	}
	
	public List<IndemnizacionRiesgoCoberturaDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.listarTodos();
	}
	
	public List<IndemnizacionRiesgoCoberturaDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public Double getTotalCoaseguroPorIdAutorizacionTecnica(BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.getTotalCoaseguroPorIdAutorizacionTecnica(idToIndemnizacion);
	}
	
	public Double getTotalDeduciblePorIdAutorizacionTecnica(BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.getTotalDeduciblePorIdAutorizacionTecnica(idToIndemnizacion);
	}
	
	public Double obtenerTotalDeducibles(List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizaciones){
		double total = 0;
		for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO : listaDetalleIndemnizaciones){
			total += indemnizacionRiesgoCoberturaDTO.getDeducible().doubleValue();
		}
		return new Double(total) ;
	}
	
	public Double obtenerTotalCoaseguros(List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizaciones){
		double total = 0;
		for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO : listaDetalleIndemnizaciones){
			total += indemnizacionRiesgoCoberturaDTO.getCoaseguro().doubleValue();
		}
		return new Double(total) ;
	}
	
	public Double obtenerTotalIndemnizaciones(List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizaciones){
		double total = 0;
		for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO : listaDetalleIndemnizaciones){
			total += indemnizacionRiesgoCoberturaDTO.getMontoPago().doubleValue();
		}
		return new Double(total) ;
	}
	
	public Double obtenerTotalPerdidas(List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizaciones){
		double total = 0;
		for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO : listaDetalleIndemnizaciones){
			total += indemnizacionRiesgoCoberturaDTO.getMontoPago().doubleValue() +
						indemnizacionRiesgoCoberturaDTO.getCoaseguro().doubleValue() +
						indemnizacionRiesgoCoberturaDTO.getDeducible().doubleValue();
		}
		return new Double(total) ;
	}
	
	public boolean borrarDetalle(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		boolean exito=false;
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		//Se obtiene el detalle y se eliminan los registros
		List<IndemnizacionRiesgoCoberturaDTO> listaDetalle = buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
		for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO : listaDetalle){
			indemnizacionRiesgoCoberturaSN.borrar(indemnizacionRiesgoCoberturaDTO);
		}
		return exito;
	}
	
	public Double obtenerMontoTotalIndeminizacionCobertura(BigDecimal idToPoliza, BigDecimal numeroInciso,
			BigDecimal numeroSubInciso, BigDecimal idToSeccion, BigDecimal idToCobertura)throws ExcepcionDeAccesoADatos, SystemException{	
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();	
		return indemnizacionRiesgoCoberturaSN.obtenerMontoTotalIndeminizacionCobertura(idToPoliza, numeroInciso, numeroSubInciso, idToSeccion, idToCobertura);
	}
	
	public List<IndemnizacionRiesgoCoberturaDTO> buscarPorReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO)
		throws ExcepcionDeAccesoADatos, SystemException{
		if (reporteSiniestroDTO==null||reporteSiniestroDTO.getIdToReporteSiniestro()==null)return null;
		return this.buscarPorPropiedad("id.idToReporteSiniestro", reporteSiniestroDTO.getIdToReporteSiniestro());
	}
	
	public List<IndemnizacionRiesgoCoberturaDTO> buscarPorIndemnizacion(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos, SystemException{
		return this.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
	}
	
	public Double[] sumaPagosParcialesAgregar(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.sumaPagosParcialesAgregar(idToReporteSiniestro);
	}
	
	public Double[] sumaPagosParcialesModificar(BigDecimal idToReporteSiniestro,BigDecimal idToIndemnizacion) throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.sumaPagosParcialesModificar(idToReporteSiniestro, idToIndemnizacion);
	}
	
	public Double sumaPerdidaDeterminadaPorReporte(BigDecimal idToReporteSiniestro)throws ExcepcionDeAccesoADatos, SystemException{
		IndemnizacionRiesgoCoberturaSN indemnizacionRiesgoCoberturaSN = new IndemnizacionRiesgoCoberturaSN();
		return indemnizacionRiesgoCoberturaSN.sumaPerdidaDeterminadaPorReporte(idToReporteSiniestro);
	}
}
