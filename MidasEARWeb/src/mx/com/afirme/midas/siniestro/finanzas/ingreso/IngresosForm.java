package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class IngresosForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String tipoMoneda;
	private String idToReporteSiniestro;
	private String idToIngresoSiniestro;
	private String idToSalvamentoSiniestro;
	
	private String fechaCobro;
	private String montoIngreso;
	private String iva;
	
	private boolean indIva;
	private boolean indIvaRet;
	private boolean indIsr;
	private boolean indIsrRet;
	private boolean indOtros;
	
	private String montoIva;
	private String montoIvaRetencion;
	private String montoIsr;
	private String montoIsrRetencion;
	private String montoOtros;
	
	private String porcentajeIva;
	private String porcentajeIvaRetencion;
	private String porcentajeIsr;
	private String porcentajeIsrRetencion;
	private String porcentajeOtros;
	
	private BigDecimal conceptoIngreso;
	private String descripcionIngreso;
	private String transferencia;
	private String fichaDeposito;
	
	private List listaIngresos;	
	private String[] eliminarIngreso = {};
	private String[] cancelarIngreso = {};		
	
	
	/**
	 * @return the idToSalvamentoSiniestro
	 */
	public String getIdToSalvamentoSiniestro() {
		return idToSalvamentoSiniestro;
	}
	public void setIdToSalvamentoSiniestro(String idToSalvamentoSiniestro) {
		this.idToSalvamentoSiniestro = idToSalvamentoSiniestro;
	}
	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	/**
	 * @return the idToIngresoSiniestro
	 */
	public String getIdToIngresoSiniestro() {
		return idToIngresoSiniestro;
	}
	/**
	 * @param idToIngresoSiniestro the idToIngresoSiniestro to set
	 */
	public void setIdToIngresoSiniestro(String idToIngresoSiniestro) {
		this.idToIngresoSiniestro = idToIngresoSiniestro;
	}
	/**
	 * @return the fechaCobro
	 */
	public String getFechaCobro() {
		return fechaCobro;
	}
	/**
	 * @param fechaCobro the fechaCobro to set
	 */
	public void setFechaCobro(String fechaCobro) {
		this.fechaCobro = fechaCobro;
	}
	/**
	 * @return the montoIngreso
	 */
	public String getMontoIngreso() {
		return montoIngreso;
	}
	/**
	 * @param montoIngreso the montoIngreso to set
	 */
	public void setMontoIngreso(String montoIngreso) {
		this.montoIngreso = montoIngreso;
	}
	/**
	 * @return the iva
	 */
	public String getIva() {
		return iva;
	}
	/**
	 * @param iva the iva to set
	 */
	public void setIva(String iva) {
		this.iva = iva;
	}
	
	/**
	 * @return the indIva
	 */
	public boolean isIndIva() {
		return indIva;
	}
	/**
	 * @param indIva the indIva to set
	 */
	public void setIndIva(boolean indIva) {
		this.indIva = indIva;
	}
	/**
	 * @return the indIvaRet
	 */
	public boolean isIndIvaRet() {
		return indIvaRet;
	}
	/**
	 * @param indIvaRet the indIvaRet to set
	 */
	public void setIndIvaRet(boolean indIvaRet) {
		this.indIvaRet = indIvaRet;
	}
	/**
	 * @return the indIsr
	 */
	public boolean isIndIsr() {
		return indIsr;
	}
	/**
	 * @param indIsr the indIsr to set
	 */
	public void setIndIsr(boolean indIsr) {
		this.indIsr = indIsr;
	}
	/**
	 * @return the indIsrRet
	 */
	public boolean isIndIsrRet() {
		return indIsrRet;
	}
	/**
	 * @param indIsrRet the indIsrRet to set
	 */
	public void setIndIsrRet(boolean indIsrRet) {
		this.indIsrRet = indIsrRet;
	}
	/**
	 * @return the indOtros
	 */
	public boolean isIndOtros() {
		return indOtros;
	}
	/**
	 * @param indOtros the indOtros to set
	 */
	public void setIndOtros(boolean indOtros) {
		this.indOtros = indOtros;
	}
	/**
	 * @return the montoIva
	 */
	public String getMontoIva() {
		return montoIva;
	}
	/**
	 * @param montoIva the montoIva to set
	 */
	public void setMontoIva(String montoIva) {
		this.montoIva = montoIva;
	}
	/**
	 * @return the montoIvaRetencion
	 */
	public String getMontoIvaRetencion() {
		return montoIvaRetencion;
	}
	/**
	 * @param montoIvaRetencion the montoIvaRetencion to set
	 */
	public void setMontoIvaRetencion(String montoIvaRetencion) {
		this.montoIvaRetencion = montoIvaRetencion;
	}
	/**
	 * @return the montoIsr
	 */
	public String getMontoIsr() {
		return montoIsr;
	}
	/**
	 * @param montoIsr the montoIsr to set
	 */
	public void setMontoIsr(String montoIsr) {
		this.montoIsr = montoIsr;
	}
	/**
	 * @return the montoIsrRetencion
	 */
	public String getMontoIsrRetencion() {
		return montoIsrRetencion;
	}
	/**
	 * @param montoIsrRetencion the montoIsrRetencion to set
	 */
	public void setMontoIsrRetencion(String montoIsrRetencion) {
		this.montoIsrRetencion = montoIsrRetencion;
	}
	/**
	 * @return the montoOtros
	 */
	public String getMontoOtros() {
		return montoOtros;
	}
	/**
	 * @param montoOtros the montoOtros to set
	 */
	public void setMontoOtros(String montoOtros) {
		this.montoOtros = montoOtros;
	}
	/**
	 * @return the porcentajeIva
	 */
	public String getPorcentajeIva() {
		return porcentajeIva;
	}
	/**
	 * @param porcentajeIva the porcentajeIva to set
	 */
	public void setPorcentajeIva(String porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	/**
	 * @return the porcentajeIvaRetencion
	 */
	public String getPorcentajeIvaRetencion() {
		return porcentajeIvaRetencion;
	}
	/**
	 * @param porcentajeIvaRetencion the porcentajeIvaRetencion to set
	 */
	public void setPorcentajeIvaRetencion(String porcentajeIvaRetencion) {
		this.porcentajeIvaRetencion = porcentajeIvaRetencion;
	}
	/**
	 * @return the porcentajeIsr
	 */
	public String getPorcentajeIsr() {
		return porcentajeIsr;
	}
	/**
	 * @param porcentajeIsr the porcentajeIsr to set
	 */
	public void setPorcentajeIsr(String porcentajeIsr) {
		this.porcentajeIsr = porcentajeIsr;
	}
	/**
	 * @return the porcentajeIsrRetencion
	 */
	public String getPorcentajeIsrRetencion() {
		return porcentajeIsrRetencion;
	}
	/**
	 * @param porcentajeIsrRetencion the porcentajeIsrRetencion to set
	 */
	public void setPorcentajeIsrRetencion(String porcentajeIsrRetencion) {
		this.porcentajeIsrRetencion = porcentajeIsrRetencion;
	}
	/**
	 * @return the porcentajeOtros
	 */
	public String getPorcentajeOtros() {
		return porcentajeOtros;
	}
	/**
	 * @param porcentajeOtros the porcentajeOtros to set
	 */
	public void setPorcentajeOtros(String porcentajeOtros) {
		this.porcentajeOtros = porcentajeOtros;
	}
	/**
	 * @return the conceptoIngreso
	 */
	public BigDecimal getConceptoIngreso() {
		return conceptoIngreso;
	}
	/**
	 * @param conceptoIngreso the conceptoIngreso to set
	 */
	public void setConceptoIngreso(BigDecimal conceptoIngreso) {
		this.conceptoIngreso = conceptoIngreso;
	}
	/**
	 * @return the descripcionIngreso
	 */
	public String getDescripcionIngreso() {
		return descripcionIngreso;
	}
	/**
	 * @param descripcionIngreso the descripcionIngreso to set
	 */
	public void setDescripcionIngreso(String descripcionIngreso) {
		this.descripcionIngreso = descripcionIngreso;
	}
	/**
	 * @return the transferencia
	 */
	public String getTransferencia() {
		return transferencia;
	}
	/**
	 * @param transferencia the transferencia to set
	 */
	public void setTransferencia(String transferencia) {
		this.transferencia = transferencia;
	}
	/**
	 * @return the fichaDeposito
	 */
	public String getFichaDeposito() {
		return fichaDeposito;
	}
	/**
	 * @param fichaDeposito the fichaDeposito to set
	 */
	public void setFichaDeposito(String fichaDeposito) {
		this.fichaDeposito = fichaDeposito;
	}
	
	public void setListaIngresos(List listaIngresos) {
		this.listaIngresos = listaIngresos;
	}
	public List getListaIngresos() {
		return listaIngresos;
	}
	public void setEliminarIngreso(String[] eliminarIngreso) {
		this.eliminarIngreso = eliminarIngreso;
	}
	public String[] getEliminarIngreso() {
		return eliminarIngreso;
	}
	public void setCancelarIngreso(String[] cancelarIngreso) {
		this.cancelarIngreso = cancelarIngreso;
	}
	public String[] getCancelarIngreso() {
		return cancelarIngreso;
	}	
}
