package mx.com.afirme.midas.siniestro.finanzas.integracion;

import java.math.BigDecimal;

import mx.com.afirme.midas.sistema.SystemException;

public class MapeoConceptoSiniestroDN {
	private static final MapeoConceptoSiniestroDN INSTANCIA = new MapeoConceptoSiniestroDN();
	
	public static MapeoConceptoSiniestroDN getInstancia(){
		return INSTANCIA;
	}
	
	public MapeoConceptoSiniestroDTO getMapeoByIdConcepto(BigDecimal idConcepto, BigDecimal concepto) throws SystemException{
		MapeoConceptoSiniestroSN mapeoConceptoSiniestroSN = new MapeoConceptoSiniestroSN(); 
		return mapeoConceptoSiniestroSN.getMapeoByIdConcepto(idConcepto, concepto);
	}
}
