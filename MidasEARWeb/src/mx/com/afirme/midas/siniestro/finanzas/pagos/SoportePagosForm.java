package mx.com.afirme.midas.siniestro.finanzas.pagos;

import java.util.List;

import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.grid.RespuestaXMLForm;

public class SoportePagosForm extends MidasBaseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SoportePagosDTO soportePagosDTO = new SoportePagosDTO();
	private List<AutorizacionTecnicaDTO> autorizaciones;
	private List<DetalleOrdenPagoDTO> detalleOrdenesDePago;
	private RespuestaXMLForm respuestaXMLForm;
	private String tituloVentana;
	private String tipoOperacion;
	private GastoSiniestroDTO gastoAcumulado;
	private OrdenDePagoDTO ordenDePagoDTO;
	
	public static final String TITULO_CREACION = "Generar Orden de Pago";
	public static final String TITULO_CANCELACION = "Cancelación de Orden de Pago";
	public static final String TITULO_DETALLE = "Detalle de Orden de Pago";
	
	public static final String TIPO_OPERACION_CREACION = "generar";
	public static final String TIPO_OPERACION_CANCELACION = "cancelar";
	public static final String TIPO_OPERACION_DETALLE = "detalle";

	public String setTitulo(String tipo){
		if(tipo != null && tipo.endsWith(TIPO_OPERACION_CREACION)){
			return TITULO_CREACION;
		}else if(tipo != null && tipo.endsWith(TIPO_OPERACION_CANCELACION)){
			return TITULO_CANCELACION;
		}else if(tipo != null && tipo.endsWith(TIPO_OPERACION_DETALLE)){
			return TITULO_DETALLE;
		}else{
			return "N/D";
		}
	}
	public List<AutorizacionTecnicaDTO> getAutorizaciones() {
		return autorizaciones;
	}

	public void setAutorizaciones(List<AutorizacionTecnicaDTO> autorizaciones) {
		this.autorizaciones = autorizaciones;
	}

	public SoportePagosDTO getSoportePagosDTO() {
		return soportePagosDTO;
	}

	public void setSoportePagosDTO(SoportePagosDTO soportePagosDTO) {
		this.soportePagosDTO = soportePagosDTO;
	}

	public List<DetalleOrdenPagoDTO> getOrdenesDePago() {
		return detalleOrdenesDePago;
	}

	public void setOrdenesDePago(List<DetalleOrdenPagoDTO> detalleOrdenesDePago) {
		this.detalleOrdenesDePago = detalleOrdenesDePago;
	}

	public RespuestaXMLForm getRespuestaXMLForm() {
		return respuestaXMLForm;
	}

	public void setRespuestaXMLForm(RespuestaXMLForm respuestaXMLForm) {
		this.respuestaXMLForm = respuestaXMLForm;
	}
	public String getTituloVentana() {
		return tituloVentana;
	}
	public void setTituloVentana(String tituloVentana) {
		this.tituloVentana = tituloVentana;
	}
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	public GastoSiniestroDTO getGastoAcumulado() {
		return gastoAcumulado;
	}
	public void setGastoAcumulado(GastoSiniestroDTO gastoAcumulado) {
		this.gastoAcumulado = gastoAcumulado;
	}
	public OrdenDePagoDTO getOrdenDePagoDTO() {
		return ordenDePagoDTO;
	}
	public void setOrdenDePagoDTO(OrdenDePagoDTO ordenDePagoDTO) {
		this.ordenDePagoDTO = ordenDePagoDTO;
	}
}
