/**
 * 
 */
package mx.com.afirme.midas.siniestro.finanzas;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

/**
 * @author user
 *
 */
public class AutorizarReservaForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String tipoMoneda;
	private List listaSumaAsegurada;
	private String totalSumaAsegurada;
	private List listaReservaActual;
	private String totalReservaActual;
	private List listaReservaAutorizar;
	private String totalReservaAutorizar;
	private String tipoAutorizacion;
	private String descripcionAjuste;

	/**
	 * 
	 */
	public AutorizarReservaForm() {
		
	}

	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}

	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	/**
	 * @return the listaSumaAsegurada
	 */
	public List getListaSumaAsegurada() {
		return listaSumaAsegurada;
	}

	/**
	 * @param listaSumaAsegurada the listaSumaAsegurada to set
	 */
	public void setListaSumaAsegurada(List listaSumaAsegurada) {
		this.listaSumaAsegurada = listaSumaAsegurada;
	}

	/**
	 * @return the totalSumaAsegurada
	 */
	public String getTotalSumaAsegurada() {
		return totalSumaAsegurada;
	}

	/**
	 * @param totalSumaAsegurada the totalSumaAsegurada to set
	 */
	public void setTotalSumaAsegurada(String totalSumaAsegurada) {
		this.totalSumaAsegurada = totalSumaAsegurada;
	}

	/**
	 * @return the listaReservaActual
	 */
	public List getListaReservaActual() {
		return listaReservaActual;
	}

	/**
	 * @param listaReservaActual the listaReservaActual to set
	 */
	public void setListaReservaActual(List listaReservaActual) {
		this.listaReservaActual = listaReservaActual;
	}

	/**
	 * @return the totalReservaActual
	 */
	public String getTotalReservaActual() {
		return totalReservaActual;
	}

	/**
	 * @param totalReservaActual the totalReservaActual to set
	 */
	public void setTotalReservaActual(String totalReservaActual) {
		this.totalReservaActual = totalReservaActual;
	}

	/**
	 * @return the listaReservaAutorizar
	 */
	public List getListaReservaAutorizar() {
		return listaReservaAutorizar;
	}

	/**
	 * @param listaReservaAutorizar the listaReservaAutorizar to set
	 */
	public void setListaReservaAutorizar(List listaReservaAutorizar) {
		this.listaReservaAutorizar = listaReservaAutorizar;
	}

	/**
	 * @return the totalReservaAutorizar
	 */
	public String getTotalReservaAutorizar() {
		return totalReservaAutorizar;
	}

	/**
	 * @param totalReservaAutorizar the totalReservaAutorizar to set
	 */
	public void setTotalReservaAutorizar(String totalReservaAutorizar) {
		this.totalReservaAutorizar = totalReservaAutorizar;
	}

	/**
	 * @return the tipoAutorizacion
	 */
	public String getTipoAutorizacion() {
		return tipoAutorizacion;
	}

	/**
	 * @param tipoAutorizacion the tipoAutorizacion to set
	 */
	public void setTipoAutorizacion(String tipoAutorizacion) {
		this.tipoAutorizacion = tipoAutorizacion;
	}

	/**
	 * @return the descripcionAjuste
	 */
	public String getDescripcionAjuste() {
		return descripcionAjuste;
	}

	/**
	 * @param descripcionAjuste the descripcionAjuste to set
	 */
	public void setDescripcionAjuste(String descripcionAjuste) {
		this.descripcionAjuste = descripcionAjuste;
	}

	
	
}
