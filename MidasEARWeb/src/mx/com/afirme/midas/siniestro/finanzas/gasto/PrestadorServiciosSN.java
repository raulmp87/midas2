package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.siniestro.finanzas.gasto.PrestadorServiciosFacadeRemote;


public class PrestadorServiciosSN {
	private PrestadorServiciosFacadeRemote beanRemoto;
	
	public PrestadorServiciosSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en PrestadorServiciosSN - Constructor", Level.INFO,null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(PrestadorServiciosFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado PrestadorServiciosSN", Level.FINEST, null);
	}	

	public void agregarPrestadorServicio(PrestadorServiciosDTO prestadorServicioDTO){
		beanRemoto.save(prestadorServicioDTO);
	}
	
	public void eliminarPrestadorServicio(PrestadorServiciosDTO prestadorServicioDTO){
		beanRemoto.delete(prestadorServicioDTO);	
	}
	
	public PrestadorServiciosDTO actualizarPrestadorServicio(PrestadorServiciosDTO prestadorServicioDTO){
		return beanRemoto.update(prestadorServicioDTO);
	}
	
	public PrestadorServiciosDTO findById(BigDecimal id){
		return beanRemoto.findById(id);
	}
 
	public List<PrestadorServiciosDTO> findAll(){
		return beanRemoto.findAll();
	} 
	
	
}
