/**
 * 
 */
package mx.com.afirme.midas.siniestro.finanzas;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author user
 *
 */
public class ListarReservaAction extends MidasMappingDispatchAction{

	
	public ActionForward inicio(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		System.out.println("**********************8  siii pasooo la mugree");

		ListarReservaForm forma = (ListarReservaForm) form;
		forma.setSumaAsegurada("Rete mucho");
		forma.setTipoMoneda("pocos pesos $$$");
		ArrayList lista = new ArrayList();
		Dummy dummy = new Dummy();
		dummy.setValor1("x1");
		dummy.setValor2("x2");
		dummy.setValor3("x3");
		dummy.setValor4("x4");
		dummy.setValor5("x5");
		dummy.setValor6("x6");
		dummy.setValor7("x7");
		dummy.setValor7("x8");
		dummy.setValor7("x9");
		Dummy dummy2 = new Dummy();
		dummy2.setValor1("patranas1");
		dummy2.setValor2("patranas2");
		dummy2.setValor3("patranas3");
		dummy2.setValor4("patranas4");
		dummy2.setValor5("patranas5");
		dummy2.setValor6("patranas6");
		dummy2.setValor7("patranas7");
		dummy2.setValor8("patranas8");
		dummy2.setValor9("patranas9");
		lista.add(dummy);
		lista.add(dummy2);
		forma.setListaModificacionesReserva(lista);
		ArrayList listDetalle = new ArrayList();
		Dummy dummyDetalle = new Dummy();
		dummyDetalle.setValor1("Detalle1");
		dummyDetalle.setValor2("Detallex2");
		dummyDetalle.setValor3("Detallex3");
		dummyDetalle.setValor4("Detallex4");
		dummyDetalle.setValor5("Detallex5");
		dummyDetalle.setValor6("Detallex6");
		Dummy dummyDetalle2 = new Dummy();
		dummyDetalle2.setValor1("Detalle patranas1");
		dummyDetalle2.setValor2("Detalle patranas2");
		dummyDetalle2.setValor3("Detalle patranas3");
		dummyDetalle2.setValor4("Detalle patranas4");
		dummyDetalle2.setValor5("Detalle patranas5");
		dummyDetalle2.setValor6("Detalle patranas6");
		listDetalle.add(dummyDetalle);
		listDetalle.add(dummyDetalle2);
		forma.setListaDetalleReserva(listDetalle);
		return mapping.findForward(reglaNavegacion);
	}


	public ActionForward detalleReserva(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
	
		String reglaNavegacion = Sistema.EXITOSO;
		
	
		ListarReservaForm forma = (ListarReservaForm) form;
		forma.setSumaAsegurada("Rete mucho");
		forma.setTipoMoneda("pocos pesos $$$");
		System.out.println("********************** radio valor="+forma.getValorRadio());
		ArrayList lista = new ArrayList();
		Dummy dummy = new Dummy();
		dummy.setValor1("x1");
		dummy.setValor2("x2");
		dummy.setValor3("x3");
		dummy.setValor4("x4");
		dummy.setValor5("x5");
		dummy.setValor6("x6");
		dummy.setValor7("x7");
		dummy.setValor7("x8");
		dummy.setValor7("x9");
		Dummy dummy2 = new Dummy();
		dummy2.setValor1("patranas1");
		dummy2.setValor2("patranas2");
		dummy2.setValor3("patranas3");
		dummy2.setValor4("patranas4");
		dummy2.setValor5("patranas5");
		dummy2.setValor6("patranas6");
		dummy2.setValor7("patranas7");
		dummy2.setValor8("patranas8");
		dummy2.setValor9("patranas9");
		lista.add(dummy);
		lista.add(dummy2);
		forma.setListaModificacionesReserva(lista);
		ArrayList listDetalle = new ArrayList();
		Dummy dummyDetalle = new Dummy();
		dummyDetalle.setValor1("Detalle1 nvo");
		dummyDetalle.setValor2("Detallex2  nvo");
		dummyDetalle.setValor3("Detallex3  nvo");
		dummyDetalle.setValor4("Detallex4 nvo");
		dummyDetalle.setValor5("Detallex5 nvo");
		dummyDetalle.setValor6("Detallex6 nvo");
		Dummy dummyDetalle2 = new Dummy();
		dummyDetalle2.setValor1("Detalle patranas1 nvo");
		dummyDetalle2.setValor2("Detalle patranas2 nvo");
		dummyDetalle2.setValor3("Detalle patranas3 nvo");
		dummyDetalle2.setValor4("Detalle patranas4 nvo");
		dummyDetalle2.setValor5("Detalle patranas5 nvo");
		dummyDetalle2.setValor6("Detalle patranas6 nvo");
		listDetalle.add(dummyDetalle);
		listDetalle.add(dummyDetalle2);
		forma.setListaDetalleReserva(listDetalle);
		return mapping.findForward(reglaNavegacion);
	}
}
