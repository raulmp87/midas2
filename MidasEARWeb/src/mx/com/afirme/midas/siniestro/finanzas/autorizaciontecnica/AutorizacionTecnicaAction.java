package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;

import static mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO.ESTATUS_CANCELADA;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.siniestro.reportes.PLAutorizacionTecnica;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDN;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AutorizacionTecnicaAction extends MidasMappingDispatchAction {
	public void mostrarReporteAT(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		AutorizacionTecnicaForm autorizacionTecnicaForm = new AutorizacionTecnicaForm();
		AutorizacionTecnicaDN autorizacionTecnicaDN = new AutorizacionTecnicaDN();
		AutorizacionTecnicaDTO autorizacionTecnicaDTO = new AutorizacionTecnicaDTO();
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
		OrdenDePagoDTO ordenDePagoDTO = new OrdenDePagoDTO();
		
		String tipoAutorizacionTecnica = request.getParameter("tipoAutorizacionTecnica");		
		String numeroAutorizacionStr = request.getParameter("numeroAutorizacion");
		BigDecimal numeroAutorizacion = new BigDecimal(0.0);
		
		try{
			if (!UtileriasWeb.esCadenaVacia(numeroAutorizacionStr)){
				numeroAutorizacion = new BigDecimal(numeroAutorizacionStr);
				autorizacionTecnicaDTO = autorizacionTecnicaDN.findById(numeroAutorizacion);
				ordenDePagoDTO = ordenDePagoDN.obtenerOrdenPorIdAutorizacionTecnica(numeroAutorizacion);
			}
			
			PLAutorizacionTecnica plAutorizacionTecnica = null;
			if (!UtileriasWeb.esCadenaVacia(tipoAutorizacionTecnica)){
				if (tipoAutorizacionTecnica.equalsIgnoreCase("Gasto")){
					autorizacionTecnicaGasto(mapping, autorizacionTecnicaForm, request, response);
					this.poblarForma(autorizacionTecnicaForm, autorizacionTecnicaDTO, ordenDePagoDTO);
					plAutorizacionTecnica = new PLAutorizacionTecnica(autorizacionTecnicaForm,"Gasto");
				}
				
				if (tipoAutorizacionTecnica.equalsIgnoreCase("Indemnizacion")){
					autorizacionTecnicaIndemnizacion(mapping, autorizacionTecnicaForm, request, response);
					this.poblarForma(autorizacionTecnicaForm, autorizacionTecnicaDTO, ordenDePagoDTO);
					plAutorizacionTecnica = new PLAutorizacionTecnica(autorizacionTecnicaForm,"Indemnizacion");
				}
				
				byte[] reporte = plAutorizacionTecnica.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
				if (reporte!=null && reporte.length>0)
					super.writeBytes(response, reporte, Sistema.TIPO_PDF, "OrdenDePago");
			}
		}catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch (IOException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
	}

	public ActionForward autorizacionTecnicaGasto(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		String strIdGastoSiniestro  = request.getParameter("id");
		String strNumeroAutorizacion = request.getParameter("numeroAutorizacion");
		String strPantalla = request.getParameter("pantalla");		
		
		BigDecimal numeroAutorizacion = null;							
		BigDecimal idGastoSiniestro = new BigDecimal(strIdGastoSiniestro);
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm)form;
		
		try{
			if(strPantalla != null){
				int idPantalla = Integer.parseInt(strPantalla);
				beanForm.setPantalla(idPantalla);
			}
			
			if(strNumeroAutorizacion != null){
				numeroAutorizacion = new BigDecimal(strNumeroAutorizacion);
				beanForm.setNumeroAutorizacionTecnica(numeroAutorizacion.intValue());
			}			
			
			GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN.findById(idGastoSiniestro);
			ReporteSiniestroDTO reporteSiniestroDTO = gastoSiniestroDTO.getReporteSiniestroDTO(); 			
			//PolizaSoporteDanosDTO poliza = soporteDanosDN.getDetallePoliza(reporteSiniestroDTO.getNumeroPoliza());
			PolizaSoporteDanosDTO poliza = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getFechaSiniestro());
			
			poblarForma(beanForm, reporteSiniestroDTO, poliza, gastoSiniestroDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO);	
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
														
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward autorizacionTecnicaIngreso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		String strIdIngresoSiniestro  = request.getParameter("id");
		String strNumeroAutorizacion = request.getParameter("numeroAutorizacion");
		String strPantalla = request.getParameter("pantalla");
		
		BigDecimal numeroAutorizacion = null;									
		
		BigDecimal idIngresoSiniestro = new BigDecimal(strIdIngresoSiniestro);
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm)form;
		
		SalvamentoSiniestroDN salvamentoSiniestro = SalvamentoSiniestroDN.getInstancia();				
		
		try{						
			if(strPantalla != null){
				int idPantalla = Integer.parseInt(strPantalla);
				beanForm.setPantalla(idPantalla);
			}
			
			if(strNumeroAutorizacion != null){
				numeroAutorizacion = new BigDecimal(strNumeroAutorizacion);
				beanForm.setNumeroAutorizacionTecnica(numeroAutorizacion.intValue());
			}
			
			IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(idIngresoSiniestro);
			ReporteSiniestroDTO reporteSiniestroDTO = ingresoSiniestroDTO.getReporteSiniestroDTO();
			
			List<SalvamentoSiniestroDTO> salvamentos = salvamentoSiniestro.buscarPorPropiedad("ingresoSiniestro", ingresoSiniestroDTO);
			
			if((salvamentos != null) && (salvamentos.size() > 0)){
				SalvamentoSiniestroDTO salvamento = salvamentos.get(0);
				if((salvamento != null) && (salvamento.getNumeroFactura() != null)){
					beanForm.setNumeroFactura(salvamento.getNumeroFactura().toString());
				}
				
			}					
			
			//PolizaSoporteDanosDTO poliza = soporteDanosDN.getDetallePoliza(reporteSiniestroDTO.getNumeroPoliza());
			PolizaSoporteDanosDTO poliza = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getFechaSiniestro());
			
			poblarForma(beanForm, reporteSiniestroDTO, poliza, ingresoSiniestroDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO);			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward autorizacionTecnicaIndemnizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		String strIdIndemnizacion  = request.getParameter("id");
		String strNumeroAutorizacion = request.getParameter("numeroAutorizacion");
		String strPantalla = request.getParameter("pantalla");
	
		BigDecimal numeroAutorizacion = null;									
		BigDecimal idIndemnizacion = new BigDecimal(strIdIndemnizacion);
		
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm)form;
		
		try{
			if(strPantalla != null){
				int idPantalla = Integer.parseInt(strPantalla);
				beanForm.setPantalla(idPantalla);
			}
			
			if(strNumeroAutorizacion != null){
				numeroAutorizacion = new BigDecimal(strNumeroAutorizacion);
				beanForm.setNumeroAutorizacionTecnica(numeroAutorizacion.intValue());
			}								
			
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(idIndemnizacion);
			ReporteSiniestroDTO reporteSiniestroDTO = indemnizacionDTO.getReporteSiniestroDTO(); 			
			//PolizaSoporteDanosDTO poliza = soporteDanosDN.getDetallePoliza(reporteSiniestroDTO.getNumeroPoliza());
			PolizaSoporteDanosDTO poliza = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getFechaSiniestro());
			
			poblarForma(beanForm, reporteSiniestroDTO, poliza, indemnizacionDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}	

	public ActionForward generarAutorizacionTecnicaGasto(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm) form; 
		BigDecimal id = new BigDecimal(beanForm.getIdToConcepto());
				
		try{
			GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
			GastoSiniestroDTO gastoSiniestroDTO = gastoSiniestroDN.findById(id);
			gastoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.POR_AUTORIZAR.shortValue());
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			crearRegistroDeAutorizacionTecnica(gastoSiniestroDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO,usuario.getNombreUsuario());						
			gastoSiniestroDN.actualizarGasto(gastoSiniestroDTO);			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward generarAutorizacionTecnicaIngreso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm) form; 
		BigDecimal id = new BigDecimal(beanForm.getIdToConcepto());
		
		try{
			IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
			IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(id);
			ingresoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.POR_AUTORIZAR.shortValue());
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			crearRegistroDeAutorizacionTecnica(ingresoSiniestroDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO,usuario.getNombreUsuario());
			ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}

		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward generarAutorizacionTecnicaIndemnizacion(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm) form; 
		BigDecimal id = new BigDecimal(beanForm.getIdToConcepto());
		
		try{
			IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(id);								
			indemnizacionDTO.getEstatusFinanzasDTO().setIdTcEstatusfinanzas(EstatusFinanzasDTO.POR_AUTORIZAR.shortValue());
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			crearRegistroDeAutorizacionTecnica(indemnizacionDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION,usuario.getNombreUsuario());
			if(indemnizacionDTO.getVariosPagos()){
				indemnizacionDN.modificar(indemnizacionDTO);
			}
			
			if(!(indemnizacionDTO.getVariosPagos().booleanValue()) || (indemnizacionDTO.getUltimoPago().booleanValue())){
//				String usuario = UtileriasWeb.obtieneNombreUsuario(request);
				actualizarEstatusReporte(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), ReporteSiniestroDTO.PENDIENTE_AUTORIZAR_AUTORIZACION_TECNICA, usuario.getNombre());
			}
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
				
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward autorizarAutorizacionTecnica(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm) form;		
		BigDecimal idAutorizacionTecnica = new BigDecimal(beanForm.getNumeroAutorizacionTecnica());		
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();		
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();	
				
		try {
			AutorizacionTecnicaDTO autorizacionTecnica = autorizacionTecnicaDN.findById(idAutorizacionTecnica);
			
			if(autorizacionTecnica.getEstatus().intValue() == AutorizacionTecnicaDTO.ESTATUS_PENDIENTE_AUTORIZAR){
				
				Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
				Date fechaActual = Calendar.getInstance().getTime();
				autorizacionTecnica.setCodigoUsuarioAutoriza(usuario.getNombreUsuario());
				autorizacionTecnica.setFechaAutorizacion(fechaActual);
				autorizacionTecnica.setEstatus(AutorizacionTecnicaDTO.ESTATUS_AUTORIZADA);
				autorizacionTecnicaDN.actualizarAutorizacionTecnica(autorizacionTecnica);																		
				
				switch(autorizacionTecnica.getIdTipoAutorizacionTecnica()){
					case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO:{
						completarAutorizacionDeGasto(autorizacionTecnica.getGastoSiniestroDTO());
//						siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO,
//								IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION,
//								autorizacionTecnica.getGastoSiniestroDTO());
						break;
					}
					case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO:{
						completarAutorizacionDeIngreso(autorizacionTecnica.getIngresoSiniestroDTO());
//						siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO,
//								IntegracionReaseguroDTO.DISTRIBUCION_DE_AUTORIZACION,
//								autorizacionTecnica.getIngresoSiniestroDTO());
						break;
					}
					case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION:{
						String usuarioAutoriza = UtileriasWeb.obtieneNombreUsuario(request);
						complelarAutorizacionDeIndemnizacion(autorizacionTecnica.getIndemnizacionDTO(), usuarioAutoriza);
						break;
					}
				}			
			}			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	} 
	
	public ActionForward cancelarAutorizacionTecnica(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm) form;		
		BigDecimal idAutorizacionTecnica = new BigDecimal(beanForm.getNumeroAutorizacionTecnica());
						
		try {
			AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();	
			SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
			IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
			OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
			BigDecimal idToReporteSiniestro =  new BigDecimal(beanForm.getIdToReporteSiniestro());
			
			AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.findById(idAutorizacionTecnica);
			
			if(autorizacionTecnicaDTO.getEstatus() != AutorizacionTecnicaDTO.ESTATUS_CANCELADA){
//				AutorizacionTecnicaDTO autorizacionTecnica = autorizacionTecnicaDN.cancelarAutorizacionTecnica(idAutorizacionTecnica,request);
				if(!siniestroMovimientosDN.validaMovimientoPagado(autorizacionTecnicaDTO)){
					OrdenDePagoDTO ordenDePagoDTO = ordenDePagoDN.obtenerOrdenNoCanceladaPorIdAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica());
					if(ordenDePagoDTO != null){
						ordenDePagoDTO.setEstatus(ESTATUS_CANCELADA);
						ordenDePagoDN.update(ordenDePagoDTO);
					}
					autorizacionTecnicaDTO.setEstatus(AutorizacionTecnicaDTO.ESTATUS_CANCELADA);
					autorizacionTecnicaDN.actualizarAutorizacionTecnica(autorizacionTecnicaDTO);
//					switch(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica()){
//						case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO:{
//							siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO,
//									IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION,
//									autorizacionTecnicaDTO.getGastoSiniestroDTO());
//							break;
//						}
//						case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO:{
//							siniestroMovimientosDN.distribuirReaseguro(AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO,
//									IntegracionReaseguroDTO.DISTRIBUCION_DE_CANCELACION,
//									autorizacionTecnicaDTO.getIngresoSiniestroDTO());
//							break;
//						}
//					}		
					if(autorizacionTecnicaDTO.getIndemnizacionDTO() == null || indemnizacionDN.tienePagosParciales(idToReporteSiniestro)){
						
						siniestroMovimientosDN.actualizarEstatusMovimiento(autorizacionTecnicaDTO, EstatusFinanzasDTO.ABIERTO);
						
						if(autorizacionTecnicaDTO.getIndemnizacionDTO() !=null){
							IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion());
							if(indemnizacionDTO.getUltimoPago()){
								siniestroMovimientosDN.cambiaEstatusReporte(idToReporteSiniestro, UtileriasWeb.obtieneNombreUsuario(request), ReporteSiniestroDTO.PENDIENTE_GENERAR_AUTORIZACION_TECNICA);
							}
						} 
					}else{
							siniestroMovimientosDN.actualizarEstatusMovimiento(autorizacionTecnicaDTO, EstatusFinanzasDTO.AUTORIZADO);
							siniestroMovimientosDN.cambiaEstatusReporte(idToReporteSiniestro, UtileriasWeb.obtieneNombreUsuario(request), ReporteSiniestroDTO.PENDIENTE_GENERAR_AUTORIZACION_TECNICA);
					}
					UtileriasWeb.imprimeMensajeXML("1", "La autorizaci\u00f3n t\u00e9cnica se cancel\u00f3 con \u00e9xito" , response);
				}else{
					UtileriasWeb.imprimeMensajeXML("0", "La autorizaci\u00f3n t\u00e9cnica no se puede cancelar <br> el movimiento ya fue pagado", response);
				}
			}else{
				UtileriasWeb.imprimeMensajeXML("0", "La autorizaci\u00f3n t\u00e9cnica no se puede cancelar, ya fue cancelada" , response);
			}
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
										
		return mapping.findForward(reglaNavegacion);
	}			
	
	public ActionForward mostrarAutorizacionTecnica(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		String strIdAutorizacionTecnica = request.getParameter("idAutorizacionTecnica");
		BigDecimal idAutorizacionTecnica = new BigDecimal(strIdAutorizacionTecnica);
		
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		AutorizacionTecnicaDTO autorizacionTecnicaDTO = null;
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm)form;
		
		try {
			autorizacionTecnicaDTO = autorizacionTecnicaDN.findById(idAutorizacionTecnica);									

			switch(autorizacionTecnicaDTO.getEstatus()){
				case AutorizacionTecnicaDTO.ESTATUS_PENDIENTE_AUTORIZAR:{
					autorizacionTecnicaDTO.setDescripcionEstatus(AutorizacionTecnicaDTO.DESCRIPCION_ESTATUS_PENDIENTE_AUTORIZAR);
					break;
				}
				case AutorizacionTecnicaDTO.ESTATUS_AUTORIZADA:{
					autorizacionTecnicaDTO.setDescripcionEstatus(AutorizacionTecnicaDTO.DESCRIPCION_ESTATUS_AUTORIZADA);
					break;				
				}			
				case AutorizacionTecnicaDTO.ESTATUS_CANCELADA:{
					autorizacionTecnicaDTO.setDescripcionEstatus(AutorizacionTecnicaDTO.DESCRIPCION_ESTATUS_CANCELADA);
					break;								
				}
			}			
			
			if(autorizacionTecnicaDTO.getIngresoSiniestroDTO() == null){
				BigDecimal ordenDePago = getOrdenDePago(autorizacionTecnicaDTO);
				if(ordenDePago != null){
					autorizacionTecnicaDTO.setIdToOrdenPago(ordenDePago);
				}
			}
			
			switch(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica()){
				case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO: {
					autorizacionTecnicaDTO.setTipoAutorizacionTecnica(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_GASTO);
					beanForm.setIdToConcepto(autorizacionTecnicaDTO.getGastoSiniestroDTO().getIdToGastoSiniestro().intValue());
					break;
				}
				case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO:{
					autorizacionTecnicaDTO.setTipoAutorizacionTecnica(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_INGRESO);
					beanForm.setIdToConcepto(autorizacionTecnicaDTO.getIngresoSiniestroDTO().getIdToIngresoSiniestro().intValue());
					break;
				}
				case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION: {
					autorizacionTecnicaDTO.setTipoAutorizacionTecnica(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_INDEMNIZACION);
					beanForm.setIdToConcepto(autorizacionTecnicaDTO.getIndemnizacionDTO().getIdToIndemnizacion().intValue());
					break;
				}						
			}				
			
			beanForm.setAutorizacionTecnica(autorizacionTecnicaDTO);
			beanForm.setIdTipoAutorizacionTecnica(autorizacionTecnicaDTO.getIdTipoAutorizacionTecnica());
			beanForm.setIdToReporteSiniestro(autorizacionTecnicaDTO.getReporteSiniestroDTO().getIdToReporteSiniestro().intValue());
			beanForm.setNumeroAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().intValue());
			beanForm.setPantalla(3);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		
		return mapping.findForward(reglaNavegacion);
	}							
				
	private void crearRegistroDeAutorizacionTecnica(Object id, int tipoAutorizacionTecnica,String idUsuario) throws ExcepcionDeAccesoADatos, SystemException{		
		AutorizacionTecnicaDTO autorizacionTecnicaDTO = new AutorizacionTecnicaDTO();
		
		Date fechaActual = Calendar.getInstance().getTime();
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		autorizacionTecnicaDTO.setEstatus(AutorizacionTecnicaDTO.ESTATUS_PENDIENTE_AUTORIZAR);
		autorizacionTecnicaDTO.setFecha(fechaActual);	
		autorizacionTecnicaDTO.setFechaCreacion(fechaActual);
		autorizacionTecnicaDTO.setCodigoUsuarioCreacion(idUsuario);
		
		switch(tipoAutorizacionTecnica){
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO: {
					GastoSiniestroDTO idToGastoSiniestro = (GastoSiniestroDTO)id;
					autorizacionTecnicaDTO.setGastoSiniestroDTO(idToGastoSiniestro);
					autorizacionTecnicaDTO.setReporteSiniestroDTO(idToGastoSiniestro.getReporteSiniestroDTO());
					autorizacionTecnicaDTO.setMontoNeto(idToGastoSiniestro.getMontoGasto());
					break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO :{
					IngresoSiniestroDTO idToIngresoSiniestro = (IngresoSiniestroDTO)id;
					autorizacionTecnicaDTO.setIngresoSiniestroDTO(idToIngresoSiniestro);
					autorizacionTecnicaDTO.setReporteSiniestroDTO(idToIngresoSiniestro.getReporteSiniestroDTO());
					autorizacionTecnicaDTO.setMontoNeto(idToIngresoSiniestro.getMonto());
					break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION :{
					IndemnizacionDTO idToIndemnizacion = (IndemnizacionDTO)id;
					autorizacionTecnicaDTO.setIndemnizacionDTO(idToIndemnizacion);
					autorizacionTecnicaDTO.setReporteSiniestroDTO(idToIndemnizacion.getReporteSiniestroDTO());
					autorizacionTecnicaDTO.setMontoNeto(idToIndemnizacion.getTotalPago());
					break;
			}		
		}
		
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		autorizacionTecnicaDN.agregarAutorizacionTecnica(autorizacionTecnicaDTO);
	}
	private void poblarForma(AutorizacionTecnicaForm form, AutorizacionTecnicaDTO autorizacionTecnicaDTO){
		if (autorizacionTecnicaDTO==null) return;
		String nombreCompletoUsuario = new String();
		if (autorizacionTecnicaDTO.getIdToAutorizacionTecnica()!=null){
			form.setNumeroAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().intValue());
			form.setCveAutorizacionTecnica(UtileriasWeb.llenarIzquierda(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toString(), "0", 8));
		}
		if (!UtileriasWeb.esCadenaVacia(autorizacionTecnicaDTO.getCodigoUsuarioAutoriza())){
			form.setCodigoUsuarioAutoriza(autorizacionTecnicaDTO.getCodigoUsuarioAutoriza());
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(autorizacionTecnicaDTO.getCodigoUsuarioAutoriza());
			if (usuario!=null && usuario.getId()!=null){
				nombreCompletoUsuario = new String();
				if (!UtileriasWeb.esCadenaVacia(usuario.getNombre()))
					nombreCompletoUsuario += usuario.getNombre() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoMaterno()))
					nombreCompletoUsuario += usuario.getApellidoMaterno() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoPaterno()))
					nombreCompletoUsuario += usuario.getApellidoPaterno() + " ";
				form.setNombreUsuarioAutoriza( nombreCompletoUsuario );
			}
		}
		if (!UtileriasWeb.esCadenaVacia(autorizacionTecnicaDTO.getCodigoUsuarioCreacion())){
			form.setCodigoUsuarioCreacion(autorizacionTecnicaDTO.getCodigoUsuarioCreacion());
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(autorizacionTecnicaDTO.getCodigoUsuarioCreacion());
			if (usuario!=null && usuario.getId()!=null){
				nombreCompletoUsuario = new String();
				if (!UtileriasWeb.esCadenaVacia(usuario.getNombre()))
					nombreCompletoUsuario += usuario.getNombre() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoMaterno()))
					nombreCompletoUsuario += usuario.getApellidoMaterno() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoPaterno()))
					nombreCompletoUsuario += usuario.getApellidoPaterno() + " ";
				form.setNombreUsuarioCreacion( nombreCompletoUsuario );
			}
		}
		if (autorizacionTecnicaDTO.getFechaAutorizacion()!=null){
			form.setFechaAutorizacion(UtileriasWeb.getFechaString(autorizacionTecnicaDTO.getFechaAutorizacion()));
		}
		if (autorizacionTecnicaDTO.getFechaCreacion()!=null){
			form.setFechaCreacion(UtileriasWeb.getFechaString(autorizacionTecnicaDTO.getFechaCreacion()));
		}
	}
	
	private void poblarForma(AutorizacionTecnicaForm form, AutorizacionTecnicaDTO autorizacionTecnicaDTO, OrdenDePagoDTO ordenDePagoDTO){
		if (autorizacionTecnicaDTO==null) return;
		String nombreCompletoUsuario = new String();
		if (autorizacionTecnicaDTO.getIdToAutorizacionTecnica()!=null){
			form.setNumeroAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().intValue());
			form.setCveAutorizacionTecnica(UtileriasWeb.llenarIzquierda(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toString(), "0", 8));
		}
		if (!UtileriasWeb.esCadenaVacia(autorizacionTecnicaDTO.getCodigoUsuarioAutoriza())){
			form.setCodigoUsuarioAutoriza(autorizacionTecnicaDTO.getCodigoUsuarioAutoriza());
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(autorizacionTecnicaDTO.getCodigoUsuarioAutoriza());
			if (usuario!=null && usuario.getId()!=null){
				nombreCompletoUsuario = new String();
				if (!UtileriasWeb.esCadenaVacia(usuario.getNombre()))
					nombreCompletoUsuario += usuario.getNombre() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoMaterno()))
					nombreCompletoUsuario += usuario.getApellidoMaterno() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoPaterno()))
					nombreCompletoUsuario += usuario.getApellidoPaterno() + " ";
				form.setNombreUsuarioAutoriza( nombreCompletoUsuario );
			}
		}
		if (!UtileriasWeb.esCadenaVacia(autorizacionTecnicaDTO.getCodigoUsuarioCreacion())){
			form.setCodigoUsuarioCreacion(autorizacionTecnicaDTO.getCodigoUsuarioCreacion());
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(autorizacionTecnicaDTO.getCodigoUsuarioCreacion());
			if (usuario!=null && usuario.getId()!=null){
				nombreCompletoUsuario = new String();
				if (!UtileriasWeb.esCadenaVacia(usuario.getNombre()))
					nombreCompletoUsuario += usuario.getNombre() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoMaterno()))
					nombreCompletoUsuario += usuario.getApellidoMaterno() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoPaterno()))
					nombreCompletoUsuario += usuario.getApellidoPaterno() + " ";
				form.setNombreUsuarioCreacion( nombreCompletoUsuario );
			}
		}
		if (autorizacionTecnicaDTO.getFechaAutorizacion()!=null){
			form.setFechaAutorizacion(UtileriasWeb.getFechaString(autorizacionTecnicaDTO.getFechaAutorizacion()));
		}
		if (autorizacionTecnicaDTO.getFechaCreacion()!=null){
			form.setFechaCreacion(UtileriasWeb.getFechaString(autorizacionTecnicaDTO.getFechaCreacion()));
		}
		if(ordenDePagoDTO.getIdToOrdenPago()!= null){
			form.setIdOrdenPago(UtileriasWeb.llenarIzquierda(ordenDePagoDTO.getIdToOrdenPago().toString(), "0", 8));
		}
		if(ordenDePagoDTO.getIdSolicitudCheque()!= null){
			form.setIdSolicitudCheque(ordenDePagoDTO.getIdSolicitudCheque());
		}
	}
	
	private void poblarForma(AutorizacionTecnicaForm form, ReporteSiniestroDTO reporteSiniestroDTO, PolizaSoporteDanosDTO poliza, Object objConcepto, int tipoAutorizacionTecnica) throws ExcepcionDeAccesoADatos, SystemException{				
		
		form.setIdToReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro().intValue());
		
		//Datos de la poliza		
		form.setProducto(String.valueOf(poliza.getCodigoProducto()));
		
		if(poliza.getIdAsegurado() != null){
			form.setCveAsegurado(poliza.getIdAsegurado().toString());
		}
		
		if(poliza.getNombreAsegurado() != null){
			form.setAsegurado(poliza.getNombreAsegurado());
		}
		
		form.setNumeroReporte(reporteSiniestroDTO.getNumeroReporte());
		
		if(poliza.getNumeroPoliza() != null){
			form.setNumeroPoliza(poliza.getNumeroPoliza());
		}
			
		form.setNumeroEndoso(String.valueOf(poliza.getNumeroUltimoEndoso()));
		
		if(reporteSiniestroDTO.getFechaHoraReporte() != null){
			String fechaOcurrio = UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaHoraReporte());
			form.setFechaOcurrio(fechaOcurrio);
		}
		
		if(poliza.getCodigoAgente() != null){
			form.setIdAgente(poliza.getCodigoAgente());
			form.setAgente(poliza.getNombreAgente());
		}
		
		form.setIdTipoAutorizacionTecnica(tipoAutorizacionTecnica);
		
		IntegracionReaseguroDN integracionReaseguroDN = IntegracionReaseguroDN.getInstancia();
		List<SoporteDistribucionReaseguroDTO> distribucion = null;
						
		switch(tipoAutorizacionTecnica){
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO : {
					GastoSiniestroDTO gastoSiniestroDTO = (GastoSiniestroDTO)objConcepto;
					distribucion = integracionReaseguroDN.getDistribucionDeGasto(gastoSiniestroDTO); 
					
					form.setDescripcionConcepto(gastoSiniestroDTO.getDescripcion());
					form.setNumeroFactura("");
					form.setIdToConcepto(gastoSiniestroDTO.getIdToGastoSiniestro().intValue());				
					
					if(gastoSiniestroDTO.getIdTcPrestadorServicios() != null){												
						form.setCvePrestadorServicios(String.valueOf(gastoSiniestroDTO.getIdTcPrestadorServicios()));						
						
						PrestadorServiciosDTO prestadorServicios = getPrestadorServicios(gastoSiniestroDTO.getIdTcPrestadorServicios(), "ADMIN");
						
						if(prestadorServicios != null){
							form.setBeneficiario(prestadorServicios.getNombrePrestador());
						}else{
							form.setBeneficiario("");
						}
					}
						
					if(gastoSiniestroDTO.getFechaEstimacionPago() != null){
						String fechaEstimadaPago = UtileriasWeb.getFechaString(gastoSiniestroDTO.getFechaEstimacionPago());
						form.setFechaEstimadaPago(fechaEstimadaPago);
					}
					
					if(gastoSiniestroDTO.getConceptoGasto().getDescripcion() != null){
						form.setConceptoPago(gastoSiniestroDTO.getConceptoGasto().getDescripcion());
					}
					
					if(gastoSiniestroDTO.getMontoGasto() != null){
						form.setMontoConcepto(gastoSiniestroDTO.getMontoGasto());
					}
					
					if(gastoSiniestroDTO.getMontoIVA() != null){
						form.setMontoIVA(gastoSiniestroDTO.getMontoIVA());
					}
					
					if(gastoSiniestroDTO.getMontoISR() != null){
						form.setMontoISR(gastoSiniestroDTO.getMontoISR());
					}
					
					if(gastoSiniestroDTO.getMontoIVARetencion() != null){
						form.setMontoIVARetencion(gastoSiniestroDTO.getMontoIVARetencion());
					}
					
					if(gastoSiniestroDTO.getMontoISRRetencion() != null){
						form.setMontoISRRetencion(gastoSiniestroDTO.getMontoISRRetencion());
					}
					
					if(gastoSiniestroDTO.getMontoOtros() != null){
						form.setMontoOtros(gastoSiniestroDTO.getMontoOtros());
					}
					
					form.setDetalleDistribucion(distribucion);
					
					break;
			}									
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO :{				
					IngresoSiniestroDTO ingresoSiniestroDTO = (IngresoSiniestroDTO)objConcepto;		
					distribucion = integracionReaseguroDN.getDistribucionDeIngreso(ingresoSiniestroDTO);
					
					form.setDescripcionConcepto(ingresoSiniestroDTO.getDescripcion());
					form.setIdToConcepto(ingresoSiniestroDTO.getIdToIngresoSiniestro().intValue());	
					
					if(ingresoSiniestroDTO.getConceptoIngreso().getDescripcion() != null){
						form.setConceptoPago(ingresoSiniestroDTO.getConceptoIngreso().getDescripcion());
					}					
					
					if(ingresoSiniestroDTO.getMonto() != null){
						form.setMontoConcepto(ingresoSiniestroDTO.getMonto());
					}
					
					if(ingresoSiniestroDTO.getMontoIVA() != null){
						form.setMontoIVA(ingresoSiniestroDTO.getMontoIVA());
					}
					
					if(ingresoSiniestroDTO.getMontoISR() != null){
						form.setMontoISR(ingresoSiniestroDTO.getMontoISR());
					}
					
					if(ingresoSiniestroDTO.getMontoIVARetencion() != null){
						form.setMontoIVARetencion(ingresoSiniestroDTO.getMontoIVARetencion());
					}
					
					if(ingresoSiniestroDTO.getMontoISRRetencion() != null){
						form.setMontoISRRetencion(ingresoSiniestroDTO.getMontoISRRetencion());
					}
					
					if(ingresoSiniestroDTO.getMontoOtros() != null){
						form.setMontoOtros(ingresoSiniestroDTO.getMontoOtros());
					}
					
					form.setDetalleDistribucion(distribucion);
					
					break;
			}
			case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION :{
				IndemnizacionDTO indemnizacionDTO = (IndemnizacionDTO)objConcepto;				
				distribucion = integracionReaseguroDN.getDistribucionDeIndemnizacion(indemnizacionDTO);
				
				form.setIdToConcepto(indemnizacionDTO.getIdToIndemnizacion().intValue());
				form.setDescripcionConcepto(indemnizacionDTO.getComentarios());
				form.setBeneficiario(indemnizacionDTO.getBeneficiario());
				form.setConceptoPago(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_INDEMNIZACION);			
								
				if(indemnizacionDTO.getMontoIva() != null){
					form.setMontoIVA(indemnizacionDTO.getMontoIva());
				}
				
				if(indemnizacionDTO.getMontoIsr() != null){
					form.setMontoISR(indemnizacionDTO.getMontoIsr());
				}
				
				if(indemnizacionDTO.getMontoIvaRetencion() != null){
					form.setMontoIVARetencion(indemnizacionDTO.getMontoIvaRetencion());
				}
				
				if(indemnizacionDTO.getMontoIsrRetencion() != null){
					form.setMontoISRRetencion(indemnizacionDTO.getMontoIsrRetencion());
				}
				
				if(indemnizacionDTO.getMontoOtros() != null){
					form.setMontoOtros(indemnizacionDTO.getMontoOtros());
				}
				
				IndemnizacionRiesgoCoberturaDN indemnizacionDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
				
				Double montoCoaseguro = indemnizacionDN.getTotalCoaseguroPorIdAutorizacionTecnica(indemnizacionDTO.getIdToIndemnizacion());
				Double montoDeducible = indemnizacionDN.getTotalDeduciblePorIdAutorizacionTecnica(indemnizacionDTO.getIdToIndemnizacion());
				
				double granTotal = indemnizacionDTO.getTotalPago().doubleValue() +
										montoCoaseguro.doubleValue() + montoDeducible.doubleValue();
				
				form.setMontoConcepto(granTotal);			
				
				form.setMontoCoaseguro(montoCoaseguro);
				form.setMontoDeducible(montoDeducible);
				form.setDetalleDistribucion(distribucion);
					
				break;
			}		
		}					
	}	
	
	public ActionForward listarAutorizacionesTecnicas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		
		String strIdReporteSiniestro = request.getParameter("idReporteSiniestro");
		BigDecimal idReporteSiniestro = new BigDecimal(strIdReporteSiniestro);
	
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		AutorizacionTecnicaForm beanForm = (AutorizacionTecnicaForm) form;
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia(); 
		
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		List<Rol> rolesUsuario = usuario.getRoles();
		
		try {
			beanForm.setIdToReporteSiniestro(idReporteSiniestro.intValue());
			List<AutorizacionTecnicaDTO> autorizacionesTecnicas = autorizacionTecnicaDN.getAutorizacionesPorIdReporte(idReporteSiniestro);
			
			for(AutorizacionTecnicaDTO autorizacionTecnica :autorizacionesTecnicas){
				BigDecimal idAutorizacionTecnica = autorizacionTecnica.getIdToAutorizacionTecnica();
				
				OrdenDePagoDTO ordenDePagoDTO = ordenDePagoDN.obtenerOrdenPorIdAutorizacionTecnica(idAutorizacionTecnica);
				
				if(ordenDePagoDTO != null){
					autorizacionTecnica.setIdToOrdenPago(ordenDePagoDTO.getIdToOrdenPago());
					autorizacionTecnica.setIdSolicitudCheque(ordenDePagoDTO.getIdSolicitudCheque());
				}
								
				switch(autorizacionTecnica.getIdTipoAutorizacionTecnica()){
					case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO : {
						autorizacionTecnica.setTipoAutorizacionTecnica("Gasto");
						break;	
					}
					case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO:{
						autorizacionTecnica.setTipoAutorizacionTecnica("Ingreso");
						break;
					}
					case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION:{
						autorizacionTecnica.setTipoAutorizacionTecnica("Indemnizaci\u00f3n");
						break;
					}	
				}
				//
				switch(autorizacionTecnica.getEstatus()){
					case AutorizacionTecnicaDTO.ESTATUS_PENDIENTE_AUTORIZAR: {
						autorizacionTecnica.setDescripcionEstatus(AutorizacionTecnicaDTO.DESCRIPCION_ESTATUS_PENDIENTE_AUTORIZAR);
						break;
					}
					case AutorizacionTecnicaDTO.ESTATUS_AUTORIZADA :{
						autorizacionTecnica.setDescripcionEstatus(AutorizacionTecnicaDTO.DESCRIPCION_ESTATUS_AUTORIZADA);
						break;
					}
					case AutorizacionTecnicaDTO.ESTATUS_CANCELADA:{
						autorizacionTecnica.setDescripcionEstatus(AutorizacionTecnicaDTO.DESCRIPCION_ESTATUS_CANCELADA);
						break;
					}
				}
			}
			
			if(permitirAutorizar(rolesUsuario)){
				beanForm.setPermitirAutorizar(1);
			}else{
				beanForm.setPermitirAutorizar(0);
			}
			
			if(permitirCancelar(rolesUsuario)){
				beanForm.setPermitirCancelar(1);
			}else{
				beanForm.setPermitirCancelar(0);
			}
			
			beanForm.setAutorizacionesTecnicas(autorizacionesTecnicas);			
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		
		return mapping.findForward(reglaNavegacion);				
	} 
	
	public ActionForward showLigas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		
		return mapping.findForward(reglaNavegacion);
	}	
			
	private BigDecimal getOrdenDePago(AutorizacionTecnicaDTO autorizacionTecnicaDTO) throws ExcepcionDeAccesoADatos, SystemException{
		BigDecimal ordenDePago = null;
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia();
		
		List<OrdenDePagoDTO> ordenesDePagoDTO = ordenDePagoDN.findByProperty("autorizacionTecnica", autorizacionTecnicaDTO);
		if(ordenesDePagoDTO != null && ordenesDePagoDTO.size() > 0){
			if(ordenesDePagoDTO.get(0).getIdToOrdenPago() != null){
				ordenDePago = ordenesDePagoDTO.get(0).getIdToOrdenPago();
			}
		}
		
		return ordenDePago;
	}
	
	private void actualizarEstatusReporte(BigDecimal idToReporteSiniestro, Byte reporteEstatus, String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(nombreUsuario);
		ReporteSiniestroDTO reporteSiniestroDTO = null; 
		
		reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
		reporteSiniestroDTO.getReporteEstatus().setIdTcReporteEstatus(reporteEstatus);
		reporteSiniestroDN.actualizar(reporteSiniestroDTO);				
	}
		
	private void completarAutorizacionDeGasto(GastoSiniestroDTO gastoSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		gastoSiniestro.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO.shortValue());
		
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		gastoSiniestroDN.actualizarGasto(gastoSiniestro);
	}
	
	private  void completarAutorizacionDeIngreso(IngresoSiniestroDTO ingresoSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		ingresoSiniestro.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO.shortValue());
		
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		ingresoSiniestroDN.actualizarIngreso(ingresoSiniestro);		
	}
	
	private void complelarAutorizacionDeIndemnizacion(IndemnizacionDTO indemnizacion, String usuarioAutoriza) throws ExcepcionDeAccesoADatos, SystemException{
		indemnizacion.getEstatusFinanzasDTO().setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO.shortValue());
		
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		indemnizacionDN.modificar(indemnizacion);
		
		if(!(indemnizacion.getVariosPagos().booleanValue()) || (indemnizacion.getUltimoPago().booleanValue())){
			actualizarEstatusReporte(indemnizacion.getReporteSiniestroDTO().getIdToReporteSiniestro(), ReporteSiniestroDTO.PENDIENTE_GENERAR_ORDEN_DE_PAGO, usuarioAutoriza);
		}				
	}
		
	
	
	private PrestadorServiciosDTO getPrestadorServicios(BigDecimal idPrestadorServicios, String nombreUsuario) throws SystemException{
		PrestadorServiciosDTO prestadorDeSerivicios = null;
		
		PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN.getInstancia();
		prestadorDeSerivicios = prestadorServiciosDN.detallePrestador(idPrestadorServicios, nombreUsuario);
		
		return prestadorDeSerivicios;
		
	}
	
	private boolean permitirAutorizar(List<Rol> rolesUsuario){
		boolean result = false;
		
		for(Rol rol : rolesUsuario){
			String strRol = rol.getDescripcion();
			
			result =  strRol.equalsIgnoreCase(Sistema.ROL_GERENTE_SINIESTROS) ||
			 strRol.equalsIgnoreCase(Sistema.ROL_GERENTE_SINIESTROS_FACULTATIVO);
			
			if(result){
				break;
			}
		}
				 
		return result;
	}
	
	private boolean permitirCancelar(List<Rol> rolesUsuario){
		boolean result = false;
		
		for(Rol rol : rolesUsuario){
			String strRol = rol.getDescripcion();
			
			result = strRol.equalsIgnoreCase(Sistema.ROL_ANALISTA_ADMINISTRATIVO) || 
			 strRol.equalsIgnoreCase(Sistema.ROL_ANALISTA_ADMINISTRATIVO_FACULTATIVO);
			
			if(result){
				break;
			}
		}
				 
		return result;	
	}
}