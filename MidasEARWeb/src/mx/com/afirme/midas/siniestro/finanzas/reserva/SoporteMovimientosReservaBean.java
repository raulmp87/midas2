package mx.com.afirme.midas.siniestro.finanzas.reserva;

import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;

public class SoporteMovimientosReservaBean {
	private ReservaDetalleDTO reservaDetalleDTO;
	private double montoDiferenciaMovimiento;
	
	public SoporteMovimientosReservaBean(){	
	}

	public void setReservaDetalleDTO(ReservaDetalleDTO reservaDetalleDTO) {
		this.reservaDetalleDTO = reservaDetalleDTO;
	}

	public ReservaDetalleDTO getReservaDetalleDTO() {
		return reservaDetalleDTO;
	}

	public void setMontoDiferenciaMovimiento(double montoDiferenciaMovimiento) {
		this.montoDiferenciaMovimiento = montoDiferenciaMovimiento;
	}

	public double getMontoDiferenciaMovimiento() {
		return montoDiferenciaMovimiento;
	}		
}
