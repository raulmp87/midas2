package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.poliza.cobertura.CoberturaSoporteDaniosSiniestroDTO;
import mx.com.afirme.midas.reaseguro.distribucion.DistribucionReaseguroDN;
import mx.com.afirme.midas.reaseguro.soporte.LineaSoporteReaseguroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.reserva.RegistroHistorialReserva;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleBean;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaForm;
import mx.com.afirme.midas.siniestro.finanzas.reserva.SoporteMovimientosReservaBean;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class ReservaDN {

	private static final ReservaDN INSTANCIA = new ReservaDN();

	public static ReservaDN getInstancia() {
		return ReservaDN.INSTANCIA;
	}
	
	public List<RiesgoAfectadoDTO> listarCoberturasRiesgo(RiesgoAfectadoId riesgoAfectadoId)throws SystemException,ExcepcionDeAccesoADatos {
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		List<RiesgoAfectadoDTO> riesgosAfectados = riesgoAfectadoDN.listarFiltrado(riesgoAfectadoId); 
		return riesgosAfectados;
	}
	
	public ReservaDTO agregarReserva(ReservaDTO reservaDTO)throws SystemException,ExcepcionDeAccesoADatos {
		ReservaSN reservaSN = new ReservaSN();
		return reservaSN.agregarReserva(reservaDTO);
	}
	
	public ReservaDTO actualizarReserva(ReservaDTO reservaDTO)throws SystemException,ExcepcionDeAccesoADatos {
		ReservaSN reservaSN = new ReservaSN();
		return reservaSN.actualizarReserva(reservaDTO);
	}
	
	public ReservaDTO findById(BigDecimal id)throws ExcepcionDeAccesoADatos, SystemException {
		ReservaSN reservaSN = new ReservaSN();
		return reservaSN.findById(id);	
	}
	
	public String realizaSumaAsegurada(List<ReservaDetalleBean> listaReservaDetalleBean)throws SystemException  {
		double suma = 0.0;
		String key = new String();
		CoberturaSoporteDaniosSiniestroDTO coberturaBasica = new CoberturaSoporteDaniosSiniestroDTO();
		Entry<String,ReservaDetalleBean> reservaDetalleEntry = null;
		ReservaDetalleBean reservaDetalleBean = new ReservaDetalleBean();
		@SuppressWarnings("unused")
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		
			if (listaReservaDetalleBean!=null && !listaReservaDetalleBean.isEmpty()){
				Map<String,ReservaDetalleBean> reservaDetalleMap = new HashMap<String, ReservaDetalleBean>();
				for (ReservaDetalleBean reservaDetalle : listaReservaDetalleBean){
					coberturaSoporteDanosDTO = reservaDetalle.getCoberturaSoporteDanosDTO();
					coberturaBasica = reservaDetalle.getCoberturaBasicaSoporteDanosDTO();
					key = this.generaClaveCoberturaBasicaSoporteDanio(coberturaBasica);
					reservaDetalleMap.put(key, reservaDetalle);
				}
				Iterator<Entry<String,ReservaDetalleBean>> reservaDetalleIterator = reservaDetalleMap.entrySet().iterator();
				
				while(reservaDetalleIterator.hasNext()){
					reservaDetalleEntry = reservaDetalleIterator.next();
					reservaDetalleBean = reservaDetalleEntry.getValue();
					suma += reservaDetalleBean.getSumaAseguradaDisponible();
				}
			}
		return String.valueOf(suma);
	}
	
	public Double obtenerTotalReserva(List<ReservaDetalleDTO> listaReservaDetalle){
		Double suma = new Double(0);
		for(ReservaDetalleDTO reservaDetalleDTO:listaReservaDetalle){
			if(reservaDetalleDTO.getEstatus().byteValue() == ReservaDetalleDTO.ACTIVO.byteValue()){
				suma = suma + reservaDetalleDTO.getEstimacion();
			}
		}
		return suma;
	}
	
	public List<ReservaDetalleDTO> generaListaDetalle(ReservaForm forma,List<RiesgoAfectadoDTO> listaRiesgo,ReservaDTO reservaDTO){
		List<ReservaDetalleDTO> listaFinal= new ArrayList<ReservaDetalleDTO>();
		String valoresEstimacion[] = forma.getEstimacion();
		
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
		int i=0;
		for(RiesgoAfectadoDTO riesgoAfectadoDTO :listaRiesgo){
			ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
			
			ReservaDetalleId id = new ReservaDetalleId();
				id.setIdtoreservaestimada(reservaDTO.getIdtoreservaestimada());
				id.setIdtocobertura(riesgoAfectadoDTO.getId().getIdtocobertura());
				id.setIdtopoliza(riesgoAfectadoDTO.getId().getIdtopoliza());
				id.setIdtoreportesiniestro(riesgoAfectadoDTO.getId().getIdtoreportesiniestro());
				id.setIdtoriesgo(riesgoAfectadoDTO.getId().getIdtoriesgo());
				id.setIdtoseccion(riesgoAfectadoDTO.getId().getIdtoseccion());
				id.setNumeroinciso(riesgoAfectadoDTO.getId().getNumeroinciso());
				id.setNumerosubinciso(riesgoAfectadoDTO.getId().getNumerosubinciso());
			reservaDetalleDTO.setId(id);
			reservaDetalleDTO.setReservaDTO(reservaDTO);
			reservaDetalleDTO.setRiesgoAfectadoDTO(riesgoAfectadoDTO);
			reservaDetalleDTO.setAjusteMas(new Double(0));
			reservaDetalleDTO.setAjusteMenos(new Double(0));
			Double estimacion = UtileriasWeb.parseaformatoMoneda(valoresEstimacion[i]);
			reservaDetalleDTO.setEstimacion(estimacion);
			listaFinal.add(reservaDetalleDTO);
			i++;
		}
		return listaFinal;
	}
		
	public ReservaDTO listarReservaActual(BigDecimal idToReporteSiniestro)throws ExcepcionDeAccesoADatos, SystemException {
		ReservaSN reservaSN = new ReservaSN();
		ReservaDTO reservaDTO =null;
		List<ReservaDTO> listaReservaDTO = reservaSN.listarReserva(idToReporteSiniestro, true);
		if(listaReservaDTO != null){
			if(listaReservaDTO.size() > 0){
				reservaDTO = listaReservaDTO.get(0);
				
			}
		}
		
		return reservaDTO;	
	}
	
	public ReservaDTO listarReservaPorAutorizar(BigDecimal idToReporteSiniestro)throws ExcepcionDeAccesoADatos, SystemException {
		ReservaSN reservaSN = new ReservaSN();
		ReservaDTO reservaDTO =null;
		List<ReservaDTO> listaReservaDTO = reservaSN.listarReserva(idToReporteSiniestro, false);
		if(listaReservaDTO != null){
			if(listaReservaDTO.size() > 0){
				reservaDTO = listaReservaDTO.get(0);
				
			}
		}
		
		return reservaDTO;	
	}
	public ReservaDTO listarUltimaReservaAutorizada(BigDecimal idToReporteSiniestro)throws ExcepcionDeAccesoADatos, SystemException {
		ReservaSN reservaSN = new ReservaSN();
		ReservaDTO reservaDTO =null;
		List<ReservaDTO> listaReservaDTO = reservaSN.listarReserva(idToReporteSiniestro, true);
		if(listaReservaDTO != null){
			if(listaReservaDTO.size() > 0){
				reservaDTO = listaReservaDTO.get(0);
				
			}
		}
		
		return reservaDTO;	
	}
	
	public ReservaDTO obtenerUltimaReservaCreada(BigDecimal idToReporteSiniestro) throws SystemException,ExcepcionDeAccesoADatos {
		ReservaSN reservaSN = new ReservaSN();
		return reservaSN.obtenerUltimaReservaCreada(idToReporteSiniestro);
	}
	
	
	public List<ReservaDetalleDTO> generaListaDetalleModificar(ReservaForm forma,ReservaDTO reservaDTO,ReservaDTO reservaActual) throws ExcepcionDeAccesoADatos, SystemException{
		List<ReservaDetalleDTO> listaFinal= new ArrayList<ReservaDetalleDTO>();
		String valoresReserva[] = forma.getReservas();
		
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
		int i=0;
		Double sumaActual = new Double(0);
		Double sumaReserva = new Double(0);
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		List<ReservaDetalleDTO> listaDetalleReservaActual =  reservaDetalleDN.listarDetalleReserva(reservaActual.getIdtoreservaestimada());
		for(ReservaDetalleDTO reservaDetalleDTOActual :listaDetalleReservaActual){
			ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
			
			ReservaDetalleId id = new ReservaDetalleId();
				id.setIdtoreservaestimada(reservaDTO.getIdtoreservaestimada());
				id.setIdtocobertura(reservaDetalleDTOActual.getId().getIdtocobertura());
				id.setIdtopoliza(reservaDetalleDTOActual.getId().getIdtopoliza());
				id.setIdtoreportesiniestro(reservaDetalleDTOActual.getId().getIdtoreportesiniestro());
				id.setIdtoriesgo(reservaDetalleDTOActual.getId().getIdtoriesgo());
				id.setIdtoseccion(reservaDetalleDTOActual.getId().getIdtoseccion());
				id.setNumeroinciso(reservaDetalleDTOActual.getId().getNumeroinciso());
				id.setNumerosubinciso(reservaDetalleDTOActual.getId().getNumerosubinciso());
			reservaDetalleDTO.setId(id);
			reservaDetalleDTO.setReservaDTO(reservaDTO);
			reservaDetalleDTO.setRiesgoAfectadoDTO(reservaDetalleDTOActual.getRiesgoAfectadoDTO());
			Double reservaValor = UtileriasWeb.parseaformatoMoneda(valoresReserva[i]);
			sumaReserva = sumaReserva + reservaValor;
			Double valorReservaActual = reservaDetalleDTOActual.getEstimacion();
			sumaActual = sumaActual+valorReservaActual;
			Double diferenciaReserva = reservaValor - valorReservaActual;
			if(reservaValor > valorReservaActual){
				reservaDetalleDTO.setAjusteMas(diferenciaReserva);
				reservaDetalleDTO.setAjusteMenos(new Double(0));
			}else if(reservaValor < valorReservaActual){
				reservaDetalleDTO.setAjusteMas(new Double(0));
				reservaDetalleDTO.setAjusteMenos(diferenciaReserva*-1);
			}else{
				reservaDetalleDTO.setAjusteMas(new Double(0));
				reservaDetalleDTO.setAjusteMenos(new Double(0));
			} 
			reservaDetalleDTO.setEstimacion(reservaValor);
			
			listaFinal.add(reservaDetalleDTO);
			i++;
		}
		if(sumaReserva > sumaActual){
			reservaDTO.setTipoajuste(new BigDecimal(1));
		}else if(sumaReserva < sumaActual){
			reservaDTO.setTipoajuste(new BigDecimal(2));
		}else{
			reservaDTO.setTipoajuste(new BigDecimal(0));
		}
		
		return listaFinal;
	}
	
	public List<ReservaDTO> listarPorFechaEstimacion(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaSN reservaSN = new ReservaSN();
		return reservaSN.listarPorFechaEstimacion(idReporteSiniestro);
	}
	
	public List<RegistroHistorialReserva> listarRegistrosReserva(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		//Se obtiene la lista de reservas
		List<ReservaDTO> listadoReservas=listarPorFechaEstimacion(idReporteSiniestro);
		List<RegistroHistorialReserva> listadoRegistrosReserva = new ArrayList<RegistroHistorialReserva>();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		//Se genera la lista de registros en base a la lista de reservas
		for(ReservaDTO reservaDTO :listadoReservas){
			RegistroHistorialReserva registroHistorialReserva= new RegistroHistorialReserva();
			registroHistorialReserva.setReservaDTO(reservaDTO);
			//Se agrega la descripcion del tipo de Movimiento
			String descripcionTipoMovimiento = "";
			switch (reservaDTO.getTipoajuste().intValue()){
				case 0: descripcionTipoMovimiento = "Estimación Inicial";
					break;
				case 1: descripcionTipoMovimiento = "Ajuste de mas";
					break;
				case 2: descripcionTipoMovimiento = "Ajuste de menos";
					break;
			};
			registroHistorialReserva.setDescripcionTipoMovimiento(descripcionTipoMovimiento);
			//Se agrega el monto de Reserva
			registroHistorialReserva.setMontoReserva(reservaDetalleDN.sumaReserva(reservaDTO.getIdtoreservaestimada()));
			listadoRegistrosReserva.add(registroHistorialReserva);
		}
		return listadoRegistrosReserva;
	}
	
	public ReservaDTO obtenReservaEstimacionInicial(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException {
		ReservaSN reservaSN = new ReservaSN();
		return reservaSN.obtenReservaEstimacionInicial(idReporteSiniestro);
	}
	
	public List<ReservaDetalleBean> obtenReservaDetalleBeanParaRiesgoAfectado(List<RiesgoAfectadoDTO> listaCoberturasRiesgo) throws SystemException{
		if (listaCoberturasRiesgo==null||listaCoberturasRiesgo.size()<=0)return null;
		ReporteSiniestroDN reporteSiniestroDN = new ReporteSiniestroDN();
		Map<String,Double> sumaDisponibleCoberturaMap = new HashMap<String, Double>();
		Double sumaDisponibleCobertura = new Double(0.0);
		String key = new String();
		List <ReservaDetalleBean> reservaDetalleBeanList = new ArrayList<ReservaDetalleBean>();
		SoporteDanosDN soporteDanosDN = new SoporteDanosDN();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		CoberturaSoporteDaniosSiniestroDTO coberturaBasicaSoporteDanosDTO = new CoberturaSoporteDaniosSiniestroDTO();
		RiesgoAfectadoDTO primerRiesgoAfectado = new RiesgoAfectadoDTO();
		
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		SeccionSoporteDanosDTO seccionSoporteDanosDTO = new SeccionSoporteDanosDTO();
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		
		ReservaDetalleBean reservaDetalleBean = new ReservaDetalleBean();
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		
		if (listaCoberturasRiesgo!=null&&listaCoberturasRiesgo.size()>0){
			primerRiesgoAfectado = listaCoberturasRiesgo.get(0);
			reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(primerRiesgoAfectado.getId().getIdtoreportesiniestro());
			for (RiesgoAfectadoDTO riesgoAfectadoDTO : listaCoberturasRiesgo) {
				reservaDetalleBean = new ReservaDetalleBean();
				if (riesgoAfectadoDTO.getId()!=null){
					riesgoAfectadoId = riesgoAfectadoDTO.getId();
					
					coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(riesgoAfectadoId.getIdtocobertura());
					seccionSoporteDanosDTO = soporteDanosDN.getSeccionSoporte(riesgoAfectadoId.getIdtoseccion());
					riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(riesgoAfectadoId.getIdtoriesgo());
					coberturaBasicaSoporteDanosDTO = soporteDanosDN.obtenerCoberturaBasica(riesgoAfectadoId.getIdtopoliza()
							,new Short(reporteSiniestroDTO.getNumeroEndoso().toString()), riesgoAfectadoId.getNumeroinciso(), 
							riesgoAfectadoId.getIdtoseccion(), riesgoAfectadoId.getIdtocobertura(), riesgoAfectadoId.getNumerosubinciso());
					
					//Valida el tipo de suma asegurada y desplega un texto dependiendo el tipo
					//Si no es basica despliega el nombre de su cobertura base
					if (coberturaSoporteDanosDTO.getClaveTipoSumaAsegurada() 
							!= (new Integer(Sistema.CLAVE_SUMA_ASEGURADA_BASICA)).intValue()){
						reservaDetalleBean.setNombreCoberturaBasica(coberturaBasicaSoporteDanosDTO.getNombreComercial());
						
					}else{
						reservaDetalleBean.setNombreCoberturaBasica("");
					}
					
					reservaDetalleBean.setTipoCobertura(coberturaSoporteDanosDTO.getDescripcionTipoSumaAsegurada());
					reservaDetalleBean.setCoberturaBasicaSoporteDanosDTO(coberturaBasicaSoporteDanosDTO);
					reservaDetalleBean.setRiesgoAfectadoDTO(riesgoAfectadoDTO);
					reservaDetalleBean.setCoberturaSoporteDanosDTO(coberturaSoporteDanosDTO);
					reservaDetalleBean.setRiesgoSoporteDanosDTO(riesgoSoporteDanosDTO);
					reservaDetalleBean.setSeccionSoporteDanosDTO(seccionSoporteDanosDTO);
					key = this.generaClaveCoberturaBasicaSoporteDanio(coberturaBasicaSoporteDanosDTO);
					if (!sumaDisponibleCoberturaMap.containsKey(key)){
						sumaDisponibleCobertura = this.obtenerSumaAseguradaDisponible(reservaDetalleBean);
						reservaDetalleBean.setSumaAseguradaDisponible(sumaDisponibleCobertura);
						sumaDisponibleCoberturaMap.put(key, sumaDisponibleCobertura);
					}else{
						sumaDisponibleCobertura = sumaDisponibleCoberturaMap.get(key);
						reservaDetalleBean.setSumaAseguradaDisponible(sumaDisponibleCobertura);
					}
					reservaDetalleBeanList.add(reservaDetalleBean);
				}
			}
		}
		
		return reservaDetalleBeanList;
	}
	
	public List<ReservaDetalleBean> obtenReservaDetalleBeanParaReservaDetalle(List<ReservaDetalleDTO> listaReservaDetalleDTO) throws SystemException{
		return this.obtenReservaDetalleBeanParaReservaDetalle(listaReservaDetalleDTO,false);
	}
	
	public List<ReservaDetalleBean> obtenReservaDetalleBeanParaReservaDetalle(List<ReservaDetalleDTO> listaReservaDetalleDTO, boolean seValidaraSuma) throws SystemException{
		if (listaReservaDetalleDTO==null||listaReservaDetalleDTO.size()<=0)return null;
		Map<String,Double> sumaDisponibleCoberturaMap = new HashMap<String, Double>();
		String key = new String();
		Double sumaDisponibleCobertura = 0.0;
		List <ReservaDetalleBean> reservaDetalleBeanList = new ArrayList<ReservaDetalleBean>();
		SoporteDanosDN soporteDanosDN = new SoporteDanosDN();
		//TODO mandar el usuario logeado
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia("");
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		CoberturaSoporteDaniosSiniestroDTO coberturaBasicaSoporteDanosDTO = new CoberturaSoporteDaniosSiniestroDTO();
		ReservaDetalleDTO reservaDetalle = new ReservaDetalleDTO();
		
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		SeccionSoporteDanosDTO seccionSoporteDanosDTO = new SeccionSoporteDanosDTO();
		RiesgoAfectadoDTO primerRiesgoAfectado = new RiesgoAfectadoDTO();
		ReservaDetalleBean reservaDetalleBean = new ReservaDetalleBean();
		
		if (listaReservaDetalleDTO!=null&&listaReservaDetalleDTO.size()>0){
			reservaDetalle = listaReservaDetalleDTO.get(0);
			primerRiesgoAfectado = reservaDetalle.getRiesgoAfectadoDTO();
			reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(primerRiesgoAfectado.getId().getIdtoreportesiniestro());
			for (ReservaDetalleDTO reservaDetalleDTO : listaReservaDetalleDTO) {
				reservaDetalleBean = new ReservaDetalleBean();
				if (reservaDetalleDTO.getId()!=null){
					coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(reservaDetalleDTO.getId().getIdtocobertura());
					
					seccionSoporteDanosDTO = soporteDanosDN.getSeccionSoporte(reservaDetalleDTO.getId().getIdtoseccion());
					riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(reservaDetalleDTO.getId().getIdtoriesgo());
					
					riesgoAfectadoId = reservaDetalleDTO.getRiesgoAfectadoDTO().getId();					
					
					reservaDetalleBean.setRiesgoAfectadoDTO(reservaDetalleDTO.getRiesgoAfectadoDTO());
					reservaDetalleBean.setReservaDetalleDTO(reservaDetalleDTO);
					reservaDetalleBean.setCoberturaSoporteDanosDTO(coberturaSoporteDanosDTO);
					reservaDetalleBean.setRiesgoSoporteDanosDTO(riesgoSoporteDanosDTO);
					reservaDetalleBean.setSeccionSoporteDanosDTO(seccionSoporteDanosDTO);
					if (seValidaraSuma){
						coberturaBasicaSoporteDanosDTO = soporteDanosDN.obtenerCoberturaBasica(riesgoAfectadoId.getIdtopoliza()
								,new Short(reporteSiniestroDTO.getNumeroEndoso().toString()), riesgoAfectadoId.getNumeroinciso(), 
								riesgoAfectadoId.getIdtoseccion(), riesgoAfectadoId.getIdtocobertura(), riesgoAfectadoId.getNumerosubinciso());
						reservaDetalleBean.setCoberturaBasicaSoporteDanosDTO(coberturaBasicaSoporteDanosDTO);
						key = this.generaClaveCoberturaBasicaSoporteDanio(coberturaBasicaSoporteDanosDTO);
						if (!sumaDisponibleCoberturaMap.containsKey(key)){
							sumaDisponibleCobertura = this.obtenerSumaAseguradaDisponible(reservaDetalleBean);
							reservaDetalleBean.setSumaAseguradaDisponible(sumaDisponibleCobertura);
							sumaDisponibleCoberturaMap.put(key, sumaDisponibleCobertura);
						}else{
							sumaDisponibleCobertura = sumaDisponibleCoberturaMap.get(key);
							reservaDetalleBean.setSumaAseguradaDisponible(sumaDisponibleCobertura);
						}
					}
					
					reservaDetalleBeanList.add(reservaDetalleBean);
					
					//TODO Comparar con el repositorio
					LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = null;
					
					try{
						 lineaSoporteReaseguroDTO = getDatosDistribucion(
								reservaDetalleDTO.getRiesgoAfectadoDTO().getId().getIdtopoliza(),
								new Integer(reservaDetalleDTO.getReservaDTO().getReporteSiniestroDTO().getNumeroEndoso().intValue()),
								reservaDetalleDTO.getRiesgoAfectadoDTO().getId().getIdtoseccion(),
								reservaDetalleDTO.getRiesgoAfectadoDTO().getId().getIdtocobertura(),
								new Integer(reservaDetalleDTO.getRiesgoAfectadoDTO().getId().getNumeroinciso().intValue()),
								new Integer(reservaDetalleDTO.getRiesgoAfectadoDTO().getId().getNumerosubinciso().intValue()));
					}catch (Exception e) {}
					
					if(lineaSoporteReaseguroDTO != null){
						double estimacion = reservaDetalleDTO.getEstimacion().doubleValue();
						
						double montoCuotaparte = 
								estimacion * (lineaSoporteReaseguroDTO.getPorcentajeCuotaParte().doubleValue() / 100);
						
						double montoPrimerExcedente = 
								estimacion * (lineaSoporteReaseguroDTO.getPorcentajePrimerExcedente().doubleValue() / 100);
						
						double montoFacultativo = 
								estimacion * (lineaSoporteReaseguroDTO.getPorcentajeFacultativo().doubleValue() / 100);
						
						double montoRetencion = 
								estimacion * (lineaSoporteReaseguroDTO.getPorcentajeRetencion().doubleValue() / 100);
						
						reservaDetalleBean.setMontoCuotaParte(montoCuotaparte);
						reservaDetalleBean.setMontoFacultativo(montoFacultativo);
						reservaDetalleBean.setMontoPrimerExcedente(montoPrimerExcedente);
						reservaDetalleBean.setMontoRetencion(montoRetencion);				
					}else{
						reservaDetalleBean.setMontoCuotaParte(0.0);
						reservaDetalleBean.setMontoFacultativo(0.0);
						reservaDetalleBean.setMontoPrimerExcedente(0.0);
						reservaDetalleBean.setMontoRetencion(0.0);
					}				
				}
			}
		}
		
		return reservaDetalleBeanList;
	}
	
	public List<ReservaDetalleBean> obtenReservaDetalleBeanParaIndemnizacionRiesgoCobertura(List<IndemnizacionRiesgoCoberturaDTO> listaIndemnizacionDTO) throws SystemException{
		if (listaIndemnizacionDTO==null||listaIndemnizacionDTO.size()<=0)return null;
		List <ReservaDetalleBean> reservaDetalleBeanList = new ArrayList<ReservaDetalleBean>();
		RiesgoAfectadoDTO riesgoAfectadoDTO = new RiesgoAfectadoDTO();
		SoporteDanosDN soporteDanosDN = new SoporteDanosDN();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		SeccionSoporteDanosDTO seccionSoporteDanosDTO = new SeccionSoporteDanosDTO();
		ReservaDetalleBean reservaDetalleBean = new ReservaDetalleBean();
		
		for (IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO : listaIndemnizacionDTO) {
			reservaDetalleBean = new ReservaDetalleBean();
			riesgoAfectadoDTO = new RiesgoAfectadoDTO();
			riesgoAfectadoDTO = indemnizacionRiesgoCoberturaDTO.getRiesgoAfectadoDTO();
			if (riesgoAfectadoDTO!=null && riesgoAfectadoDTO.getId()!=null){
				coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(riesgoAfectadoDTO.getId().getIdtocobertura());
				seccionSoporteDanosDTO = soporteDanosDN.getSeccionSoporte(riesgoAfectadoDTO.getId().getIdtoseccion());
				riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(riesgoAfectadoDTO.getId().getIdtoriesgo());
				
				reservaDetalleBean.setIndemnizacionRiesgoCoberturaDTO(indemnizacionRiesgoCoberturaDTO);
				reservaDetalleBean.setRiesgoAfectadoDTO(riesgoAfectadoDTO);
				reservaDetalleBean.setCoberturaSoporteDanosDTO(coberturaSoporteDanosDTO);
				reservaDetalleBean.setRiesgoSoporteDanosDTO(riesgoSoporteDanosDTO);
				reservaDetalleBean.setSeccionSoporteDanosDTO(seccionSoporteDanosDTO);
				reservaDetalleBeanList.add(reservaDetalleBean);
			}
		}
		
		return reservaDetalleBeanList;
	}
	
	private LineaSoporteReaseguroDTO getDatosDistribucion(BigDecimal idToPoliza,  Integer numeroEndoso,
													 BigDecimal idToSeccion, BigDecimal idToCobertura,
													 Integer numeroInciso, Integer numeroSubInciso) throws SystemException{
		
		DistribucionReaseguroDN  distribucionReaseguro = DistribucionReaseguroDN.getInstancia();		
		
		LineaSoporteReaseguroDTO lineaSoporteReaseguroDTO = distribucionReaseguro.getPorcentajeDistribucion(idToPoliza,
															 numeroEndoso, idToSeccion, 
															 idToCobertura, numeroInciso, 
															 numeroSubInciso);

		return lineaSoporteReaseguroDTO;
	}
	
	private Double obtenerSumaAseguradaDisponible(ReservaDetalleBean reservaDetalleBean) throws ExcepcionDeAccesoADatos, SystemException {
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		//TODO hacer llegar el nombre del usuario
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia("");
		ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
		RiesgoAfectadoDTO riesgoAfectadoDTO = new RiesgoAfectadoDTO();
		CoberturaSoporteDaniosSiniestroDTO coberturaBasicaSoporteDaniosSiniestroDTO = new CoberturaSoporteDaniosSiniestroDTO();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		RiesgoAfectadoId riesgoAfectadoId = new RiesgoAfectadoId();
		List<CoberturaSoporteDaniosSiniestroDTO> coberturaSoporteDaniosSiniestroList = new ArrayList<CoberturaSoporteDaniosSiniestroDTO>();
		
		double sumaAseguradaDisponible = 0.0;		
		double sumatoriaTotalIndemnizaciones = 0.0;
		double sumaAseguradaBasica = 0.0; 
		double sumaAseguradaDisponibleSubLimite = 0.0;
		double sumaAseguradaPrimerRiesgoLUC = 0.0;
		double sumaAseguradaCobertura = 0.0;
		double indemnizacionCobertura = 0.0;
		
		BigDecimal idToReporteSiniestro=null;
		BigDecimal idToPoliza = null;
		BigDecimal numeroInciso = null;
		BigDecimal idToSeccion = null;
		BigDecimal idToCobertura = null;
		BigDecimal numeroSubInciso = null;
		Short numeroEndoso = null;
		Short claveTipoSumaAseguradaSubLimite = new Short(Sistema.CLAVE_SUMA_ASEGURADA_SUBLIMITE);
		
		riesgoAfectadoDTO = reservaDetalleBean.getRiesgoAfectadoDTO();
		if (riesgoAfectadoDTO==null||riesgoAfectadoDTO.getId()==null)return new Double("0");
			
		riesgoAfectadoId = riesgoAfectadoDTO.getId();
		coberturaBasicaSoporteDaniosSiniestroDTO = reservaDetalleBean.getCoberturaBasicaSoporteDanosDTO();
		
		idToPoliza = riesgoAfectadoId.getIdtopoliza();
		
		numeroSubInciso = riesgoAfectadoId.getNumerosubinciso();
		numeroInciso = coberturaBasicaSoporteDaniosSiniestroDTO.getNumeroInciso();
		idToSeccion = coberturaBasicaSoporteDaniosSiniestroDTO.getIdToSeccion();
		idToCobertura = coberturaBasicaSoporteDaniosSiniestroDTO.getIdToCobertura();
		
		idToReporteSiniestro = riesgoAfectadoId.getIdtoreportesiniestro();
		reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
		if (reporteSiniestroDTO!=null && reporteSiniestroDTO.getNumeroEndoso()!=null)
			numeroEndoso = new Short(reporteSiniestroDTO.getNumeroEndoso().toString());
		
		//No aplico primer riesgo
		int aplicoPrimerRiesgoCobertura = soporteDanosDN.aplicoPrimerRiesgoCobertura(idToPoliza, numeroInciso, idToSeccion, idToCobertura, numeroEndoso); 
		if ( aplicoPrimerRiesgoCobertura == Sistema.SD_NO_APLICA_PRIMER_RIESGO){
			
			sumaAseguradaBasica = coberturaBasicaSoporteDaniosSiniestroDTO.getSumaAsegurada().doubleValue();
			sumatoriaTotalIndemnizaciones += indemnizacionRiesgoCoberturaDN.obtenerMontoTotalIndeminizacionCobertura(
					idToPoliza, numeroInciso, numeroSubInciso, idToSeccion, idToCobertura);
			
			coberturaSoporteDaniosSiniestroList = soporteDanosDN.obtenerCoberturasAmparadasSubLimites(idToPoliza, numeroInciso, idToSeccion, idToCobertura, numeroEndoso);
			for (CoberturaSoporteDaniosSiniestroDTO coberturaSoporteDaniosSiniestroDTO : coberturaSoporteDaniosSiniestroList) {
				numeroInciso = coberturaSoporteDaniosSiniestroDTO.getNumeroInciso();
				idToSeccion = coberturaSoporteDaniosSiniestroDTO.getIdToSeccion();
				idToCobertura = coberturaSoporteDaniosSiniestroDTO.getIdToCobertura();
				numeroSubInciso = null;
				
				sumatoriaTotalIndemnizaciones += indemnizacionRiesgoCoberturaDN.obtenerMontoTotalIndeminizacionCobertura(
						idToPoliza, numeroInciso, numeroSubInciso, idToSeccion, idToCobertura);
			}
			
			sumaAseguradaDisponible = sumaAseguradaBasica - sumatoriaTotalIndemnizaciones;
			
			if (coberturaSoporteDanosDTO.getClaveTipoSumaAsegurada() == claveTipoSumaAseguradaSubLimite){
				sumaAseguradaDisponibleSubLimite = riesgoAfectadoDTO.getSumaAsegurada().doubleValue() - indemnizacionCobertura;
				
				if (sumaAseguradaDisponible>sumaAseguradaDisponibleSubLimite)
					sumaAseguradaDisponible = sumaAseguradaDisponibleSubLimite;
			}
		}

		//Aplico primer riesgo
		if (aplicoPrimerRiesgoCobertura	== Sistema.SD_APLICA_PRIMER_RIESGO){

			sumaAseguradaPrimerRiesgoLUC = soporteDanosDN.obtenerSumaAseguradaCoberturaPrimerRiesgoLUC(
					idToPoliza, numeroInciso, idToSeccion, idToCobertura, numeroEndoso);

			coberturaSoporteDaniosSiniestroList = soporteDanosDN
					.obtenerCoberturasPrimerRiesgoLUC(idToPoliza, numeroEndoso,
							numeroInciso, idToCobertura);
			for (CoberturaSoporteDaniosSiniestroDTO coberturaSoporteDaniosSiniestroDTO : coberturaSoporteDaniosSiniestroList) {
				numeroInciso = coberturaSoporteDaniosSiniestroDTO.getNumeroInciso();
				idToSeccion = coberturaSoporteDaniosSiniestroDTO.getIdToSeccion();
				idToCobertura = coberturaSoporteDaniosSiniestroDTO.getIdToCobertura();
				numeroSubInciso = null;
				
				sumatoriaTotalIndemnizaciones += indemnizacionRiesgoCoberturaDN.obtenerMontoTotalIndeminizacionCobertura(
						idToPoliza, numeroInciso, numeroSubInciso, idToSeccion, idToCobertura);
			}

			sumaAseguradaDisponible = sumaAseguradaPrimerRiesgoLUC - sumatoriaTotalIndemnizaciones;
			
			numeroInciso = coberturaBasicaSoporteDaniosSiniestroDTO.getNumeroInciso();
			idToSeccion = coberturaBasicaSoporteDaniosSiniestroDTO.getIdToSeccion();
			idToCobertura = coberturaBasicaSoporteDaniosSiniestroDTO.getIdToCobertura();
			
			indemnizacionCobertura = indemnizacionRiesgoCoberturaDN.obtenerMontoTotalIndeminizacionCobertura(
					idToPoliza, numeroInciso, numeroSubInciso, idToSeccion, idToCobertura);
			
			coberturaSoporteDaniosSiniestroList = soporteDanosDN.obtenerCoberturasAmparadasSubLimites(idToPoliza, numeroInciso, idToSeccion, idToCobertura, numeroEndoso);
			for (CoberturaSoporteDaniosSiniestroDTO coberturaSoporteDaniosSiniestroDTO : coberturaSoporteDaniosSiniestroList) {
				numeroInciso = coberturaSoporteDaniosSiniestroDTO.getNumeroInciso();
				idToSeccion = coberturaSoporteDaniosSiniestroDTO.getIdToSeccion();
				idToCobertura = coberturaSoporteDaniosSiniestroDTO.getIdToCobertura();
				
				indemnizacionCobertura += indemnizacionRiesgoCoberturaDN.obtenerMontoTotalIndeminizacionCobertura(
						idToPoliza, numeroInciso, numeroSubInciso, idToSeccion, idToCobertura);
			}
			
			sumaAseguradaCobertura = riesgoAfectadoDTO.getSumaAsegurada() - indemnizacionCobertura;

			if (sumaAseguradaDisponible > sumaAseguradaCobertura){
				sumaAseguradaDisponible = sumaAseguradaCobertura;
			}
		}
		return new Double(sumaAseguradaDisponible);
	}
	
	public ReservaDTO findByIdReporteSiniestro(BigDecimal idToReporteSiniestro) throws SystemException{
		ReservaDTO reservaDTO = null;
		ReservaSN reservaSN = new ReservaSN();
		
		reservaDTO = reservaSN.findByIdReporteSiniestro(idToReporteSiniestro);
		
		return reservaDTO;
	}
	
	public List<ReservaDetalleDTO> crearReservaDetalleDeRiesgosAfectados(List<ReservaDetalleDTO> detalleReserva, List<RiesgoAfectadoDTO> riesgosAfectados){
		List<ReservaDetalleDTO> listaReservaDetalle = new ArrayList<ReservaDetalleDTO>(riesgosAfectados.size());
		BigDecimal idToReservaEstimada = detalleReserva.get(0).getId().getIdtoreservaestimada();
		
		for(RiesgoAfectadoDTO riesgoAfectado : riesgosAfectados){			
			int countReserva = detalleReserva.size();
			boolean existeRiesgo = false;
			
			for(int i = countReserva - 1; i >= 0; i--){
				if(riesgoAfectado.getId().equals(detalleReserva.get(i).getRiesgoAfectadoDTO().getId())){
					listaReservaDetalle.add(detalleReserva.get(i));
					detalleReserva.remove(i);
					existeRiesgo = true;
					break;
				}
			}		
			
			if(!existeRiesgo){
				ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
				
				ReservaDetalleId reservaDetalleId = new ReservaDetalleId();
				reservaDetalleId.setIdtoreservaestimada(idToReservaEstimada);
				reservaDetalleId.setIdtoreportesiniestro(riesgoAfectado.getId().getIdtoreportesiniestro());				
				reservaDetalleId.setIdtopoliza(riesgoAfectado.getId().getIdtopoliza());
				reservaDetalleId.setNumeroinciso(riesgoAfectado.getId().getNumeroinciso());
				reservaDetalleId.setIdtoriesgo(riesgoAfectado.getId().getIdtoriesgo());
				reservaDetalleId.setIdtocobertura(riesgoAfectado.getId().getIdtocobertura());
				reservaDetalleId.setIdtoseccion(riesgoAfectado.getId().getIdtoseccion());
				reservaDetalleId.setNumerosubinciso(riesgoAfectado.getId().getNumerosubinciso());
				
				reservaDetalleDTO.setId(reservaDetalleId);
				reservaDetalleDTO.setRiesgoAfectadoDTO(riesgoAfectado);
				reservaDetalleDTO.setEstatus(riesgoAfectado.getEstatus()); 
				reservaDetalleDTO.setEstimacion(new Double(0.0));				
				
				listaReservaDetalle.add(reservaDetalleDTO);
			}
		}
		
		return listaReservaDetalle;
	}
	
	public void borrarReservaDetalle(BigDecimal idToReservaEstimada) throws ExcepcionDeAccesoADatos, SystemException{
		List<ReservaDetalleDTO> reservaExistente = null;
		
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		reservaExistente = reservaDetalleDN.listarReservaDetalle(idToReservaEstimada);
		
		for(ReservaDetalleDTO reservaDetalle : reservaExistente){
			reservaDetalleDN.borrarReservaDetalle(reservaDetalle);
		}		
	}
	
	public List<ReservaDetalleDTO> crearReservaDetalle(List<RiesgoAfectadoDTO> riesgosAfectados, Double[] estimaciones){
		List<ReservaDetalleDTO> listaReservaDetalle = new ArrayList<ReservaDetalleDTO>(riesgosAfectados.size());
		
		int i = 0;
		
		for(RiesgoAfectadoDTO riesgoAfectado : riesgosAfectados){						
			ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
			
			ReservaDetalleId reservaDetalleId = new ReservaDetalleId();
			reservaDetalleId.setIdtoreportesiniestro(riesgoAfectado.getId().getIdtoreportesiniestro());				
			reservaDetalleId.setIdtopoliza(riesgoAfectado.getId().getIdtopoliza());
			reservaDetalleId.setNumeroinciso(riesgoAfectado.getId().getNumeroinciso());
			reservaDetalleId.setIdtoriesgo(riesgoAfectado.getId().getIdtoriesgo());
			reservaDetalleId.setIdtocobertura(riesgoAfectado.getId().getIdtocobertura());
			reservaDetalleId.setIdtoseccion(riesgoAfectado.getId().getIdtoseccion());
			reservaDetalleId.setNumerosubinciso(riesgoAfectado.getId().getNumerosubinciso());
			
			reservaDetalleDTO.setId(reservaDetalleId);
			reservaDetalleDTO.setRiesgoAfectadoDTO(riesgoAfectado);			
			reservaDetalleDTO.setEstimacion(estimaciones[i]);
			reservaDetalleDTO.setAjusteMas(new Double(0.0));
			reservaDetalleDTO.setAjusteMenos(new Double(0.0));
			reservaDetalleDTO.setEstatus(ReservaDetalleDTO.ACTIVO);			
						
			listaReservaDetalle.add(reservaDetalleDTO);
			i++;
		}
				
		return listaReservaDetalle;		
	}
	
	public void guardarReservaDetalle(ReservaDTO reservaDTO, List<ReservaDetalleDTO> reservaDetalle) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		BigDecimal idToReservaEstimada = reservaDTO.getIdtoreservaestimada();
		
		for(ReservaDetalleDTO reservaBean : reservaDetalle){
			ReservaDetalleId id = reservaBean.getId();
			id.setIdtoreservaestimada(idToReservaEstimada);
			
			reservaBean.setReservaDTO(reservaDTO);
			reservaDetalleDN.guardarReservaDetalle(reservaBean);
		}		
	}
	
	public List<SoporteMovimientosReservaBean> obtenerDiferenciasReservas(List<ReservaDetalleDTO> reservaDetalleAutorizada, List<ReservaDetalleDTO> nuevaReservaDetalle){
		List<SoporteMovimientosReservaBean> reservaDetallePorRegistrar = new ArrayList<SoporteMovimientosReservaBean>();
		
		int totalElementosReservaAutorizada = reservaDetalleAutorizada.size();
		
		for(int i = totalElementosReservaAutorizada -1 ; i >= 0; i--){									
			ReservaDetalleDTO beanReservaDetalleAutorizada = reservaDetalleAutorizada.get(i);
			
			boolean existeMovimiento = false;			
			int totalElementosNuevaReserva = nuevaReservaDetalle.size();			
			
			for(int j = totalElementosNuevaReserva - 1 ; j >= 0; j--){				
				ReservaDetalleDTO beanReservaDetalleNueva = nuevaReservaDetalle.get(j);
				
				if(beanReservaDetalleAutorizada.getRiesgoAfectadoDTO().getId().equals(beanReservaDetalleNueva.getRiesgoAfectadoDTO().getId())){					
					Double estimacionOriginal = beanReservaDetalleAutorizada.getEstimacion();
					Double montoNuevaEstimacion = beanReservaDetalleNueva.getEstimacion();
					double diferenciaEstimacion = estimacionOriginal.doubleValue() - montoNuevaEstimacion.doubleValue();
					//Ajuste de Menos
					if(Math.round(diferenciaEstimacion) > 0){
						beanReservaDetalleNueva.setAjusteMenos(new Double(diferenciaEstimacion));
					//Ajuste de mas
					}else if(Math.round(diferenciaEstimacion) < 0){						
						beanReservaDetalleNueva.setAjusteMas(new Double(diferenciaEstimacion * -1));
					}
					
					diferenciaEstimacion *= -1;
					SoporteMovimientosReservaBean soporteMovimientoReserva = new SoporteMovimientosReservaBean();
					soporteMovimientoReserva.setReservaDetalleDTO(beanReservaDetalleNueva);
					soporteMovimientoReserva.setMontoDiferenciaMovimiento(diferenciaEstimacion);
					
					reservaDetallePorRegistrar.add(soporteMovimientoReserva);
					
					existeMovimiento = true;					
					nuevaReservaDetalle.remove(j);
					break;
				}			
			}
			
			if(!existeMovimiento){				
				double estimacion = beanReservaDetalleAutorizada.getEstimacion();				
				double ajusteMenos = beanReservaDetalleAutorizada.getAjusteMenos();
				Double diferencia = new Double(estimacion - ajusteMenos);
								
				if(diferencia.intValue() != 0){				
					beanReservaDetalleAutorizada.setAjusteMenos(new Double(estimacion));
					beanReservaDetalleAutorizada.setEstatus(ReservaDetalleDTO.INACTIVO);
					
					SoporteMovimientosReservaBean soporteMovimientoReserva = new SoporteMovimientosReservaBean();
					soporteMovimientoReserva.setReservaDetalleDTO(beanReservaDetalleAutorizada);
					soporteMovimientoReserva.setMontoDiferenciaMovimiento(estimacion * -1);
					
					reservaDetallePorRegistrar.add(soporteMovimientoReserva);				
				}
			}			
		}
		
		if(nuevaReservaDetalle.size() > 0){
			for(ReservaDetalleDTO reservaDetalle : nuevaReservaDetalle){
				double estimacion = reservaDetalle.getEstimacion();
				reservaDetalle.setAjusteMas(new Double(estimacion));
				
				SoporteMovimientosReservaBean soporteMovimientoReserva = new SoporteMovimientosReservaBean();
				soporteMovimientoReserva.setReservaDetalleDTO(reservaDetalle);
				soporteMovimientoReserva.setMontoDiferenciaMovimiento(estimacion);
				
				reservaDetallePorRegistrar.add(soporteMovimientoReserva);									
			}			
		}
		
		return reservaDetallePorRegistrar;
	}
	
	public void guardarNuevaReserva(ReservaDTO reservaDTO, List<SoporteMovimientosReservaBean> movimientosReserva) throws ExcepcionDeAccesoADatos, SystemException{
		List<ReservaDetalleDTO> reservaDetalle = new ArrayList<ReservaDetalleDTO>(movimientosReserva.size());
		
		for(SoporteMovimientosReservaBean soporteMovimiento :  movimientosReserva){
			reservaDetalle.add(soporteMovimiento.getReservaDetalleDTO());
		}						
		
		this.guardarReservaDetalle(reservaDTO, reservaDetalle);
	}
	
	public BigDecimal determinarTipoAjusteNuevaReserva(List<SoporteMovimientosReservaBean> movimientosReserva){	
		BigDecimal tipoAjuste = null;
		
		double diferenciaMonto = 0.0;
		
		for(SoporteMovimientosReservaBean soporteMovimiento :  movimientosReserva){	
			diferenciaMonto += soporteMovimiento.getMontoDiferenciaMovimiento();
		}						
		
		if(Math.round(diferenciaMonto) > 0){
			tipoAjuste = new BigDecimal(1); 
		}else{
			tipoAjuste = new BigDecimal(2);
		}
		
		return tipoAjuste;
	}

	@SuppressWarnings("unused")
	public String validaSumaReservaParaRiesgos(List<ReservaDetalleBean> reservaDetalleList){
		String mensaje = new String();
		String key = new String();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		CoberturaSoporteDaniosSiniestroDTO coberturaBasica = new CoberturaSoporteDaniosSiniestroDTO();
		Map<String,ReservaDetalleBean> reservaDetalleMap = new HashMap<String, ReservaDetalleBean>();
		Map<String,Double> estimacionesUsuarioMap = new HashMap<String, Double>();
		Entry<String,ReservaDetalleBean> reservaDetalleEntry = null;
		ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
		ReservaDetalleId reservaDetalleId = new ReservaDetalleId();
		ReservaDetalleBean reservaDetalleBean = new ReservaDetalleBean();
		Double estimacionUsuarioActual = null;
		
			for (ReservaDetalleBean reservaDetalle : reservaDetalleList){
				coberturaSoporteDanosDTO = reservaDetalle.getCoberturaSoporteDanosDTO();
				coberturaBasica = reservaDetalle.getCoberturaBasicaSoporteDanosDTO();
				
				reservaDetalleDTO = reservaDetalle.getReservaDetalleDTO();
				reservaDetalleId = reservaDetalleDTO.getId();
				//Almacena las diferentes coberturas basicas
				key = coberturaBasica.getNumeroInciso().toString() + "_" + coberturaBasica.getNumeroSubInciso().toString() + "_" +coberturaBasica.getIdToSeccion().toString() + "_" + coberturaBasica.getIdToCobertura().toString();
				reservaDetalleMap.put(key, reservaDetalle);
				
				if (reservaDetalle.getSumaAseguradaDisponible() < reservaDetalleDTO.getEstimacion().doubleValue()){
					mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.finanzas.cobertura.sobrePasoMontoSumaDisponible");
					return mensaje;
				}
				
				if (estimacionesUsuarioMap.isEmpty() || (!estimacionesUsuarioMap.isEmpty()&&!estimacionesUsuarioMap.containsKey(key))){
					estimacionUsuarioActual = reservaDetalleDTO.getEstimacion();
				}else{
					estimacionUsuarioActual = estimacionesUsuarioMap.get(key);
					estimacionUsuarioActual = estimacionUsuarioActual + reservaDetalleDTO.getEstimacion();
				}
				estimacionesUsuarioMap.put(key, estimacionUsuarioActual);
			}
			
			Iterator<Entry<String,ReservaDetalleBean>> reservaDetalleIterator = reservaDetalleMap.entrySet().iterator();
			
			while(reservaDetalleIterator.hasNext()){
				reservaDetalleEntry = reservaDetalleIterator.next();
				reservaDetalleBean = reservaDetalleEntry.getValue();
				key = reservaDetalleEntry.getKey();
				if (reservaDetalleMap.containsKey(key)){
					estimacionUsuarioActual = estimacionesUsuarioMap.get(key);
					if (reservaDetalleBean.getSumaAseguradaDisponible()<estimacionUsuarioActual.doubleValue()){
						//La suma de las reservas es mayor de la suma de las disponibles\
						mensaje = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "siniestro.finanzas.cobertura.sobrePasoSumaReservaSumaDisponible");
						return mensaje;
					}
				}
			}
		return mensaje;
	}
	
	public ReporteEstatusDTO determinarPendienteDeModificacionDeReserva(ReporteSiniestroDTO reporteSiniestro, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		ReporteEstatusDTO reporteEstatus = null;
								
		if(reporteSiniestro.getInformeFinalValido() == null){	
			reporteEstatus = new ReporteEstatusDTO();
			reporteEstatus.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AUTORIZAR_RESERVA);						
		}
				
		return reporteEstatus; 
	}		
	
	private String generaClaveCoberturaBasicaSoporteDanio(CoberturaSoporteDaniosSiniestroDTO coberturaBasica){
		return coberturaBasica.getNumeroInciso().toString() + "_" + coberturaBasica.getNumeroSubInciso().toString() +"_" + coberturaBasica.getIdToSeccion().toString() + "_" +coberturaBasica.getIdToCobertura().toString();
	}
		
	public ReservaDTO modificarReservaRiesgoAfectado(ReporteSiniestroDTO reporteSiniestroDTO, List<RiesgoAfectadoDTO> riesgosAfectados, BigDecimal idToUsuarioMovimiento, String userName) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDTO reservaGeneradaDTO = null;
		
		if(riesgosAfectados != null && riesgosAfectados.size() > 0){
			ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
			
			ReservaDTO reservaDTO = this.listarUltimaReservaAutorizada(reporteSiniestroDTO.getIdToReporteSiniestro());
			ReservaDTO reservaActualDTO = this.obtenerUltimaReservaCreada(reporteSiniestroDTO.getIdToReporteSiniestro());
			List<ReservaDetalleDTO> reservaDetalleAutorizada = null;
			List<ReservaDetalleDTO> reservaDetalleActual = null;
			List<ReservaDetalleDTO> reservaDetallePorRegistrar = null;
			
			reservaDetalleAutorizada = reservaDetalleDN.listarReservaDetalle(reservaDTO.getIdtoreservaestimada());	
			if(reservaActualDTO != null && reservaActualDTO.getIdtoreservaestimada().intValue() != reservaDTO.getIdtoreservaestimada().intValue()){
				reservaDetalleActual = reservaDetalleDN.listarReservaDetalle(reservaActualDTO.getIdtoreservaestimada());
			}else{
				reservaActualDTO = null;
			}
			
			if((reservaActualDTO == null && reservaDTO.getAutorizada()) || reservaActualDTO.getAutorizada()){
				reservaDetallePorRegistrar = this.obtenerReservaPorAjustar(reservaDetalleAutorizada, riesgosAfectados);
			}else{
				reservaDetallePorRegistrar = this.obtenerReservaPorAjustar(reservaDetalleAutorizada, reservaDetalleActual, riesgosAfectados);
			}
			
			Date fechaMovimiento = Calendar.getInstance().getTime();
			
			ReservaDTO nuevaReservaDTO = new ReservaDTO();
			nuevaReservaDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
			nuevaReservaDTO.setDescripcion("Ajuste de menos en automatico");
			nuevaReservaDTO.setTipoajuste(new BigDecimal(2));
			nuevaReservaDTO.setFechaestimacion(fechaMovimiento);
			nuevaReservaDTO.setFechaautorizacion(fechaMovimiento);
			nuevaReservaDTO.setIdtousuariocreacion(idToUsuarioMovimiento);
			nuevaReservaDTO.setAutorizada(Boolean.TRUE);
			nuevaReservaDTO.setIdtousuarioautoriza(idToUsuarioMovimiento);
			
			reservaGeneradaDTO = this.ajustarReservayRiesgosAfectados(riesgosAfectados, nuevaReservaDTO, reservaDetallePorRegistrar);
			if(reservaGeneradaDTO != null){
				try{
					contabilizarReserva(reservaGeneradaDTO, userName);
				}catch (SystemException ex) {
					LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
					ex.printStackTrace();
				}
				distribuirReserva(reservaGeneradaDTO,userName);
			}
		}		
		
		return reservaGeneradaDTO;
	}
	
	
	public boolean actualizarReservaRiesgoAfectado(ReporteSiniestroDTO reporteSiniestroDTO, List<RiesgoAfectadoDTO> riesgosAfectados, ReservaDTO estimacionInicial) throws ExcepcionDeAccesoADatos, SystemException{
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaSN reservaSN = new ReservaSN();
		boolean result = false;
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		List<ReservaDetalleDTO> detalleEstimacionInicial = reservaDetalleDN.listarReservaDetalle(estimacionInicial.getIdtoreservaestimada());
		List<ReservaDetalleDTO> detalleEstimacionInicialEliminar = new ArrayList<ReservaDetalleDTO>();
		for(ReservaDetalleDTO detalle: detalleEstimacionInicial){
			for(RiesgoAfectadoDTO riesgo: riesgosAfectados){
				if(siniestroMovimientosDN.comparaRiesgoyDetalleReserva(riesgo,detalle) && riesgo.getEstatus().byteValue() == RiesgoAfectadoDTO.INACTIVO.byteValue()){
//					detalle.setEstatus(ReservaDetalleDTO.INACTIVO);
					detalleEstimacionInicialEliminar.add(detalle);
				}
			}
		}
		result = reservaSN.ajustarEstimacionInicialyRiesgosAfectados(riesgosAfectados, detalleEstimacionInicialEliminar);
		
		return result;
	}
	
	private ReservaDTO ajustarReservayRiesgosAfectados(List<RiesgoAfectadoDTO> riesgosAfectados, ReservaDTO reserva, List<ReservaDetalleDTO> reservaDetalle) throws SystemException{
		ReservaDTO reservaDTO = null;
		
		ReservaSN reservaSN = new ReservaSN();
		List<RiesgoAfectadoDTO> riesgosPorActualizar = new ArrayList<RiesgoAfectadoDTO>();
		
		for(RiesgoAfectadoDTO riesgoAfectado : riesgosAfectados){			
			if(riesgoAfectado.getEstatus().byteValue() == RiesgoAfectadoDTO.INACTIVO.byteValue()){
				riesgosPorActualizar.add(riesgoAfectado);
			}	
		}
				
		reservaDTO = reservaSN.ajustarReservayRiesgosAfectados(riesgosPorActualizar, reserva, reservaDetalle);
		
		return reservaDTO;
	}		
	
	
	
	private List<ReservaDetalleDTO> obtenerReservaPorAjustar(List<ReservaDetalleDTO> reservaDetalle, List<RiesgoAfectadoDTO> riesgosAfectados){
		List<ReservaDetalleDTO> reservaComparada = new ArrayList<ReservaDetalleDTO>();
		
		int totalRiesgos = riesgosAfectados.size();
								
		for(int i = totalRiesgos - 1; i >= 0; i--){
			RiesgoAfectadoDTO riesgoAfectado = riesgosAfectados.get(i);
			@SuppressWarnings("unused")
			boolean found = false;
			
			int totalElementosReserva = reservaDetalle.size();
			
			for(int j = totalElementosReserva -1; j >= 0; j-- ){
				ReservaDetalleDTO reserva = reservaDetalle.get(j);
				
				if(riesgoAfectado.getId().equals(reserva.getRiesgoAfectadoDTO().getId())){
					if(reserva.getEstatus().byteValue() == ReservaDetalleDTO.ACTIVO.byteValue()){
						if(riesgoAfectado.getEstatus().byteValue() == RiesgoAfectadoDTO.INACTIVO.byteValue()){
							reserva.setAjusteMenos(reserva.getEstimacion());
							reserva.setEstatus(ReservaDetalleDTO.INACTIVO);
						}
						reservaComparada.add(reserva);
					}
					reservaDetalle.remove(j);
					
					found = true;
					break;					
				}
			}
//			if(! found){
//				ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
//				
//				ReservaDetalleId reservaDetalleId = new ReservaDetalleId();
//				reservaDetalleId.setIdtoreportesiniestro(riesgoAfectado.getId().getIdtoreportesiniestro());				
//				reservaDetalleId.setIdtopoliza(riesgoAfectado.getId().getIdtopoliza());
//				reservaDetalleId.setNumeroinciso(riesgoAfectado.getId().getNumeroinciso());
//				reservaDetalleId.setIdtoriesgo(riesgoAfectado.getId().getIdtoriesgo());
//				reservaDetalleId.setIdtocobertura(riesgoAfectado.getId().getIdtocobertura());
//				reservaDetalleId.setIdtoseccion(riesgoAfectado.getId().getIdtoseccion());
//				reservaDetalleId.setNumerosubinciso(riesgoAfectado.getId().getNumerosubinciso());
//				
//				reservaDetalleDTO.setId(reservaDetalleId);
//				reservaDetalleDTO.setRiesgoAfectadoDTO(riesgoAfectado);			
//				reservaDetalleDTO.setEstimacion(new Double(0.0));
//				reservaDetalleDTO.setAjusteMas(new Double(0.0));
//				reservaDetalleDTO.setAjusteMenos(new Double(0.0));
//				reservaDetalleDTO.setEstatus(riesgoAfectado.getEstatus());
//							
//				reservaComparada.add(reservaDetalleDTO);				
//			}			
		}
		return reservaComparada;
	}
	
	private List<ReservaDetalleDTO> obtenerReservaPorAjustar(List<ReservaDetalleDTO> reservaDetalleAutorizada,List<ReservaDetalleDTO> reservaDetalleActual, List<RiesgoAfectadoDTO> riesgosAfectados){
		List<ReservaDetalleDTO> reservaComparada = new ArrayList<ReservaDetalleDTO>();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		int totalRiesgos = riesgosAfectados.size();
								
		for(int i = totalRiesgos - 1; i >= 0; i--){
			RiesgoAfectadoDTO riesgoAfectado = riesgosAfectados.get(i);
			@SuppressWarnings("unused")
			boolean found = false;
			
			int totalElementosReserva = reservaDetalleActual.size();
			
			for(int j = totalElementosReserva -1; j >= 0; j-- ){
				ReservaDetalleDTO reservaActual = reservaDetalleActual.get(j);
				
				if(riesgoAfectado.getId().equals(reservaActual.getRiesgoAfectadoDTO().getId())){
					if(reservaActual.getEstatus().byteValue() == ReservaDetalleDTO.ACTIVO.byteValue()){
						if(riesgoAfectado.getEstatus().byteValue() == RiesgoAfectadoDTO.INACTIVO.byteValue()){
							ReservaDetalleDTO reservaAutorizada = siniestroMovimientosDN.comparaDetallesReserva(reservaActual,reservaDetalleAutorizada);
							reservaActual.setAjusteMas(reservaAutorizada.getAjusteMas());
							reservaActual.setEstimacion(reservaAutorizada.getEstimacion());
							reservaActual.setAjusteMenos(reservaAutorizada.getEstimacion());
							reservaActual.setEstatus(ReservaDetalleDTO.INACTIVO);
							
						}
						reservaComparada.add(reservaActual);
					}
					reservaDetalleActual.remove(j);
					
					found = true;
					break;					
				}
			}
			
//			if(! found){
//				ReservaDetalleDTO reservaDetalleDTO = new ReservaDetalleDTO();
//				
//				ReservaDetalleId reservaDetalleId = new ReservaDetalleId();
//				reservaDetalleId.setIdtoreportesiniestro(riesgoAfectado.getId().getIdtoreportesiniestro());				
//				reservaDetalleId.setIdtopoliza(riesgoAfectado.getId().getIdtopoliza());
//				reservaDetalleId.setNumeroinciso(riesgoAfectado.getId().getNumeroinciso());
//				reservaDetalleId.setIdtoriesgo(riesgoAfectado.getId().getIdtoriesgo());
//				reservaDetalleId.setIdtocobertura(riesgoAfectado.getId().getIdtocobertura());
//				reservaDetalleId.setIdtoseccion(riesgoAfectado.getId().getIdtoseccion());
//				reservaDetalleId.setNumerosubinciso(riesgoAfectado.getId().getNumerosubinciso());
//				
//				reservaDetalleDTO.setId(reservaDetalleId);
//				reservaDetalleDTO.setRiesgoAfectadoDTO(riesgoAfectado);			
//				reservaDetalleDTO.setEstimacion(new Double(0.0));
//				reservaDetalleDTO.setAjusteMas(new Double(0.0));
//				reservaDetalleDTO.setAjusteMenos(new Double(0.0));
//				reservaDetalleDTO.setEstatus(riesgoAfectado.getEstatus());
//							
//				reservaComparada.add(reservaDetalleDTO);				
//			}			
		}
				
		return reservaComparada;
	}
	
	public ReservaDTO ajustarReservaeIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reservaDTO, List<ReservaDetalleDTO> reservaDetalle,String userName ) throws ExcepcionDeAccesoADatos,SystemException{
		ReservaDTO reservaGeneradaDTO = null;
		
		ReservaSN reservaSN = new ReservaSN();
				
		reservaGeneradaDTO = reservaSN.ajustarReservaeIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion, reservaDTO, reservaDetalle);
		if(reservaGeneradaDTO != null){
			try{
				contabilizarReserva(reservaGeneradaDTO, userName);
			}catch (SystemException ex) {
				LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
				ex.printStackTrace();
			}
			distribuirReserva(reservaGeneradaDTO,userName);
		}
		return reservaGeneradaDTO;
	}		
	
	public ReservaDTO ajustarReservaeModificacionIndemnizacion(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion, ReservaDTO reservaDTO, List<ReservaDetalleDTO> reservaDetalle,String userName ) throws ExcepcionDeAccesoADatos,SystemException{
		ReservaDTO reservaGeneradaDTO = null;
		
		ReservaSN reservaSN = new ReservaSN();
				
		reservaGeneradaDTO = reservaSN.ajustarReservaeModificacionIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion, reservaDTO, reservaDetalle);
		if(reservaGeneradaDTO != null){
			try{
				contabilizarReserva(reservaGeneradaDTO, userName);
			}catch (SystemException ex) {
				LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
				ex.printStackTrace();
			}
			distribuirReserva(reservaGeneradaDTO,userName);
		}
		return reservaGeneradaDTO;
	}
	
	public ReservaDTO completaCancelarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reservaDTO, List<ReservaDetalleDTO> reservaDetalle,String userName ) throws ExcepcionDeAccesoADatos,SystemException{
		ReservaDTO reservaGeneradaDTO = null;
		
		ReservaSN reservaSN = new ReservaSN();
				
		reservaGeneradaDTO = reservaSN.completaCancelarIndemnizacion(indemnizacionDTO, reservaDTO, reservaDetalle);
		if(reservaGeneradaDTO != null){
			try{
				contabilizarReserva(reservaGeneradaDTO, userName);
			}catch (SystemException ex) {
				LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
				ex.printStackTrace();
			}
			distribuirReserva(reservaGeneradaDTO,userName);
		}
		return reservaGeneradaDTO;
	}
	
	public ReservaDTO completaEliminarIndemnizacion(IndemnizacionDTO indemnizacionDTO, ReservaDTO reservaDTO, List<ReservaDetalleDTO> reservaDetalle,String userName ) throws ExcepcionDeAccesoADatos,SystemException{
		ReservaDTO reservaGeneradaDTO = null;
		
		ReservaSN reservaSN = new ReservaSN();
				
		reservaGeneradaDTO = reservaSN.completaEliminarIndemnizacion(indemnizacionDTO, reservaDTO, reservaDetalle);
		if(reservaGeneradaDTO != null){
			try{
				contabilizarReserva(reservaGeneradaDTO, userName);
			}catch (SystemException ex) {
				LogDeMidasWeb.log("Excepcion capa Web de tipo " + ex.getClass().getCanonicalName(), Level.SEVERE, ex);
				ex.printStackTrace();
			}
			distribuirReserva(reservaGeneradaDTO,userName);
		}
		return reservaGeneradaDTO;
	}

	public void distribuirReserva(ReservaDTO reservaDTO,String nombreUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		IntegracionReaseguroDN integracionReaseguro = IntegracionReaseguroDN.getInstancia();
		integracionReaseguro.distribuirReserva(reservaDTO,nombreUsuario);		
	}
	
	private void contabilizarReserva(ReservaDTO reservaDTO, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		ContabilidadSiniestroDN contabilidad = ContabilidadSiniestroDN.getInstancia();
		contabilidad.contabilizarReserva(reservaDTO, usuario);
	}	
}