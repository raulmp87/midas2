package mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class ContabilidadRemesaSN {
	ContabilidadRemesaFacadeRemote beanRemoto;
	
	public  ContabilidadRemesaSN() throws SystemException{
		LogDeMidasWeb.log("Entrando en ContabilidadRemesaSN - Constructor", Level.INFO,null);
		
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator.getEJB(ContabilidadRemesaFacadeRemote.class);
		
		LogDeMidasWeb.log("bean Remoto instanciado ContabilidadRemesaSN", Level.FINEST, null);
	}
	
    public void guardar(ContabilidadRemesaDTO entity){
    	beanRemoto.save(entity);    	
    }
    
    public void borrar(ContabilidadRemesaDTO entity){
    	beanRemoto.delete(entity);
    }

    public ContabilidadRemesaDTO actualizar(ContabilidadRemesaDTO entity){
    	return beanRemoto.update(entity);
    }
	
	public ContabilidadRemesaDTO findById( ContabilidadRemesaId id){
		return beanRemoto.findById(id);
	}	
	
	public List<ContabilidadRemesaDTO> buscarMovimientosPendientesActualizar(BigDecimal idToAplicacionIngreso){
		return beanRemoto.buscarMovimientosPendientesActualizar(idToAplicacionIngreso);
	}
}
