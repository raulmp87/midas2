package mx.com.afirme.midas.siniestro.finanzas.gasto;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class GastosForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	private String idToGastoSiniestro;
	private String tipoMoneda;
	private String idToReporteSiniestro;
	
	private String fechaGasto;
	private String fechaRecepcion;
	private String fechaEstimacionPago;
	private String fechaEntrega;
	private String montoGasto;
	
	private String montoIva;
	private String montoIvaRetencion;
	private String montoIsr;
	private String montoIsrRetencion;
	private String montoOtros;
	
	private String porcentajeIva;
	private String porcentajeIvaRetencion;
	private String porcentajeIsr;
	private String porcentajeIsrRetencion;
	private String porcentajeOtros;
		
	private boolean indIva;
	private boolean indIvaRet;
	private boolean indIsr;
	private boolean indIsrRet;
	private boolean indOtros;	
		
	private String conceptoGasto;
	private String prestadorServicios;
	
	private String descripcionGasto;
	private String numeroCheque;
	private String numeroTransferencia;
	private String lugarEnvio;
	
	private List<?> listaGastos;	
	private String[] eliminarGasto = {};
	private String[] cancelarGasto = {};		
	
	/**
	 * @return the tipoMoneda
	 */
	public String getTipoMoneda() {
		return tipoMoneda;
	}
	/**
	 * @param tipoMoneda the tipoMoneda to set
	 */
	public void setTipoMoneda(String tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	/**
	 * @return the idToReporteSiniestro
	 */
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	/**
	 * @param idToReporteSiniestro the idToReporteSiniestro to set
	 */
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	/**
	 * @return the fechaGasto
	 */
	public String getFechaGasto() {
		return fechaGasto;
	}
	/**
	 * @param fechaGasto the fechaGasto to set
	 */
	public void setFechaGasto(String fechaGasto) {
		this.fechaGasto = fechaGasto;
	}
	/**
	 * @return the fechaRecepcion
	 */
	public String getFechaRecepcion() {
		return fechaRecepcion;
	}
	/**
	 * @param fechaRecepcion the fechaRecepcion to set
	 */
	public void setFechaRecepcion(String fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}
	/**
	 * @return the fechaEstimacionPago
	 */
	public String getFechaEstimacionPago() {
		return fechaEstimacionPago;
	}
	/**
	 * @param fechaEstimacionPago the fechaEstimacion to set
	 */
	public void setFechaEstimacionPago(String fechaEstimacionPago) {
		this.fechaEstimacionPago = fechaEstimacionPago;
	}
	/**
	 * @return the fechaEntrega
	 */
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	/**
	 * @param fechaEntrega the fechaEntrega to set
	 */
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	/**
	 * @return the montoGasto
	 */
	public String getMontoGasto() {
		return montoGasto;
	}
	/**
	 * @param montoGasto the montoGasto to set
	 */
	public void setMontoGasto(String montoGasto) {
		this.montoGasto = montoGasto;
	}
	/**
	 * @return the montoIva
	 */
	public String getMontoIva() {
		return montoIva;
	}
	/**
	 * @param montoIva the montoIva to set
	 */
	public void setMontoIva(String montoIva) {
		this.montoIva = montoIva;
	}
	/**
	 * @return the montoIvaRetencion
	 */
	public String getMontoIvaRetencion() {
		return montoIvaRetencion;
	}
	/**
	 * @param montoIvaRetencion the montoIvaRetencion to set
	 */
	public void setMontoIvaRetencion(String montoIvaRetencion) {
		this.montoIvaRetencion = montoIvaRetencion;
	}
	/**
	 * @return the montoIsr
	 */
	public String getMontoIsr() {
		return montoIsr;
	}
	/**
	 * @param montoIsr the montoIsr to set
	 */
	public void setMontoIsr(String montoIsr) {
		this.montoIsr = montoIsr;
	}
	/**
	 * @return the montoIsrRetencion
	 */
	public String getMontoIsrRetencion() {
		return montoIsrRetencion;
	}
	/**
	 * @param montoIsrRetencion the montoIsrRetencion to set
	 */
	public void setMontoIsrRetencion(String montoIsrRetencion) {
		this.montoIsrRetencion = montoIsrRetencion;
	}
	/**
	 * @return the montoOtros
	 */
	public String getMontoOtros() {
		return montoOtros;
	}
	/**
	 * @param montoOtros the montoOtros to set
	 */
	public void setMontoOtros(String montoOtros) {
		this.montoOtros = montoOtros;
	}
	/**
	 * @return the porcentajeIva
	 */
	public String getPorcentajeIva() {
		return porcentajeIva;
	}
	/**
	 * @param porcentajeIva the porcentajeIva to set
	 */
	public void setPorcentajeIva(String porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}
	/**
	 * @return the porcentajeIvaRetencion
	 */
	public String getPorcentajeIvaRetencion() {
		return porcentajeIvaRetencion;
	}
	/**
	 * @param porcentajeIvaRetencion the porcentajeIvaRetencion to set
	 */
	public void setPorcentajeIvaRetencion(String porcentajeIvaRetencion) {
		this.porcentajeIvaRetencion = porcentajeIvaRetencion;
	}
	/**
	 * @return the porcentajeIsr
	 */
	public String getPorcentajeIsr() {
		return porcentajeIsr;
	}
	/**
	 * @param porcentajeIsr the porcentajeIsr to set
	 */
	public void setPorcentajeIsr(String porcentajeIsr) {
		this.porcentajeIsr = porcentajeIsr;
	}
	/**
	 * @return the porcentajeIsrRetencion
	 */
	public String getPorcentajeIsrRetencion() {
		return porcentajeIsrRetencion;
	}
	/**
	 * @param porcentajeIsrRetencion the porcentajeIsrRetencion to set
	 */
	public void setPorcentajeIsrRetencion(String porcentajeIsrRetencion) {
		this.porcentajeIsrRetencion = porcentajeIsrRetencion;
	}
	/**
	 * @return the porcentajeOtros
	 */
	public String getPorcentajeOtros() {
		return porcentajeOtros;
	}
	/**
	 * @param porcentajeOtros the porcentajeOtros to set
	 */
	public void setPorcentajeOtros(String porcentajeOtros) {
		this.porcentajeOtros = porcentajeOtros;
	}
	/**
	 * @return the conceptoGasto
	 */
	public String getConceptoGasto() {
		return conceptoGasto;
	}
	/**
	 * @param conceptoGasto the conceptoGasto to set
	 */
	public void setConceptoGasto(String conceptoGasto) {
		this.conceptoGasto = conceptoGasto;
	}
	/**
	 * @return the prestadorServicios
	 */
	public String getPrestadorServicios() {
		return prestadorServicios;
	}
	/**
	 * @param prestadorServicios the prestadorServicios to set
	 */
	public void setPrestadorServicios(String prestadorServicios) {
		this.prestadorServicios = prestadorServicios;
	}
	/**
	 * @return the descripcionGasto
	 */
	public String getDescripcionGasto() {
		return descripcionGasto;
	}
	/**
	 * @param descripcionGasto the descripcionGasto to set
	 */
	public void setDescripcionGasto(String descripcionGasto) {
		this.descripcionGasto = descripcionGasto;
	}
	/**
	 * @return the numeroCheque
	 */
	public String getNumeroCheque() {
		return numeroCheque;
	}
	/**
	 * @param numeroCheque the numeroCheque to set
	 */
	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}
	/**
	 * @return the lugarEnvio
	 */
	public String getLugarEnvio() {
		return lugarEnvio;
	}
	/**
	 * @param lugarEnvio the lugarEnvio to set
	 */
	public void setLugarEnvio(String lugarEnvio) {
		this.lugarEnvio = lugarEnvio;
	}
	
	public List<?> getListaGastos() {
		return listaGastos;
	}

	public void setListaGastos(List<?> listaGastos) {
		this.listaGastos = listaGastos;
	}

	public String[] getEliminarGasto() {
		return eliminarGasto;
	}	
	
	public void setEliminarGasto(String[] eliminarGasto) {
		this.eliminarGasto = eliminarGasto;
	}
	
	public String[] getCancelarGasto() {
		return cancelarGasto;
	}
	
	public void setCancelarGasto(String[] cancelarGasto) {
		this.cancelarGasto = cancelarGasto;
	}
	public void setIdToGastoSiniestro(String idToGastoSiniestro) {
		this.idToGastoSiniestro = idToGastoSiniestro;
	}
	public String getIdToGastoSiniestro() {
		return idToGastoSiniestro;
	}
	public void setIndIva(boolean indIva) {
		this.indIva = indIva;
	}
	public boolean isIndIva() {
		return indIva;
	}
	public void setIndIvaRet(boolean indIvaRet) {
		this.indIvaRet = indIvaRet;
	}
	public boolean isIndIvaRet() {
		return indIvaRet;
	}
	public void setIndIsr(boolean indIsr) {
		this.indIsr = indIsr;
	}
	public boolean isIndIsr() {
		return indIsr;
	}
	public void setIndIsrRet(boolean indIsrRet) {
		this.indIsrRet = indIsrRet;
	}
	public boolean isIndIsrRet() {
		return indIsrRet;
	}
	public void setIndOtros(boolean indOtros) {
		this.indOtros = indOtros;
	}
	public boolean isIndOtros() {
		return indOtros;
	}
	public void setNumeroTransferencia(String numeroTransferencia) {
		this.numeroTransferencia = numeroTransferencia;
	}
	public String getNumeroTransferencia() {
		return numeroTransferencia;
	}	
}
