package mx.com.afirme.midas.siniestro.finanzas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.DetallePolizaSoporteDanosSiniestrosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.direccion.SeccionSubIncisoSoporteDanosSiniestrosDTO;
import mx.com.afirme.midas.endoso.inciso.IncisoSoporteDaniosSiniestrosDTO;
import mx.com.afirme.midas.siniestro.CoberturasRiesgoPoliza;
import mx.com.afirme.midas.siniestro.IncisosPoliza;
import mx.com.afirme.midas.siniestro.SubIncisosSeccionPoliza;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class RiesgoAfectadoDN {
	private static final RiesgoAfectadoDN INSTANCIA = new RiesgoAfectadoDN();
	private static Short SELECCIONADO = 1;
	private static Short NO_SELECCIONADO = 0;

	public static RiesgoAfectadoDN getInstancia() {
		return RiesgoAfectadoDN.INSTANCIA;
	}
	
	public List<RiesgoAfectadoDTO> listarRiesgosAfectadosPara(BigDecimal id) throws SystemException,
	ExcepcionDeAccesoADatos {
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.listarRiesgosAfectadosPara(id);
	}
	
	public List<RiesgoAfectadoDTO> listarFiltrado(RiesgoAfectadoId id) throws SystemException,
	ExcepcionDeAccesoADatos {
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();		
		return riesgoAfectadoSN.listarFiltrado(id);
	}
	
	public List<RiesgoAfectadoDTO> listarCoberturasRiesgo(BigDecimal idReporteSiniestro) throws SystemException,
	ExcepcionDeAccesoADatos {
		List<RiesgoAfectadoDTO> lista = new ArrayList<RiesgoAfectadoDTO>();
//		lista = llenaListaDTO();
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		lista = riesgoAfectadoSN.listarRiesgosAfectadosPara(idReporteSiniestro);
		return lista;
	}
	
	private List<RiesgoAfectadoDTO> llenaListaDTO(){
		List<RiesgoAfectadoDTO> lista = new ArrayList<RiesgoAfectadoDTO>();
//		RiesgoAfectadoDTO riesgoAfectadoDTO = new RiesgoAfectadoDTO();
//			riesgoAfectadoDTO.setNumeroInciso(new BigDecimal(1));
//			riesgoAfectadoDTO.setNumeroSubinciso(new BigDecimal(2));
//			riesgoAfectadoDTO.setIdToSeccion(new BigDecimal(3));
//			riesgoAfectadoDTO.setIdToCobertura(new BigDecimal(4));
//			riesgoAfectadoDTO.setIdToRiesgo(new BigDecimal(5));
//			riesgoAfectadoDTO.setSumaAsegurada(new BigDecimal(9800));
//			riesgoAfectadoDTO.setTipoSA("tipo TEST");
//		lista.add(riesgoAfectadoDTO);
//		RiesgoAfectadoDTO riesgoAfectadoDTO2 = new RiesgoAfectadoDTO();
//			riesgoAfectadoDTO2.setNumeroInciso(new BigDecimal(10));
//			riesgoAfectadoDTO2.setNumeroSubinciso(new BigDecimal(20));
//			riesgoAfectadoDTO2.setIdToSeccion(new BigDecimal(30));
//			riesgoAfectadoDTO2.setIdToCobertura(new BigDecimal(40));
//			riesgoAfectadoDTO2.setIdToRiesgo(new BigDecimal(50));
//			riesgoAfectadoDTO2.setSumaAsegurada(new BigDecimal(7600));
//			riesgoAfectadoDTO2.setTipoSA("tipo TEST2");
//		lista.add(riesgoAfectadoDTO2);	
		return lista;
	}
	
	public String ObtenerSumaAseguradaxCobertura(List<RiesgoAfectadoDTO> listaCoberturasRiesgo){
		float suma = 0;
//		for(RiesgoAfectadoDTO objRiesgo:listaCoberturasRiesgo){
//			suma = suma + objRiesgo.getSumaAsegurada().intValue();
//		}
		return String.valueOf(suma) ;
	}
	
	public List<RiesgoAfectadoDTO> listarPorReporteSiniestro(BigDecimal idReporteSiniestro)
	throws ExcepcionDeAccesoADatos, SystemException {
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.listarPorReporteSiniestro(idReporteSiniestro);
	}
	
	public Double sumaAseguradaPorReporte(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.sumaAseguradaPorReporte(idReporteSiniestro);	
	}
	
	public Double obtenerTotalSumaAsegurada(List<RiesgoAfectadoDTO> listaRiesgos) throws SystemException{
		double total = 0;
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
		for(RiesgoAfectadoDTO riesgoAfectadoDTO : listaRiesgos){
			total += riesgoAfectadoDTO.getSumaAsegurada().doubleValue();
			coberturaSoporteDanosDTO = new CoberturaSoporteDanosDTO();
			coberturaSoporteDanosDTO = soporteDanosDN.getCoberturaSoporte(riesgoAfectadoDTO.getId().getIdtocobertura());
			//Se actualiza la descripcion de suma asegurada
			riesgoAfectadoDTO.setDescripcionTipoSA(coberturaSoporteDanosDTO.getDescripcionTipoSumaAsegurada());
		}
		return new Double(total) ;
	}
	
	public void guardarRiesgoAfectado(RiesgoAfectadoDTO riesgoAfectadoDTO)throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		riesgoAfectadoSN.guardarRiesgoAfectado(riesgoAfectadoDTO);	
	}
	public void eliminarRiesgoAfectado(RiesgoAfectadoDTO riesgoAfectadoDTO)throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		riesgoAfectadoSN.eliminarRiesgoAfectado(riesgoAfectadoDTO);	
	}
	
	public List<SubRamoDTO> getSubramosFromReservaEstimada(BigDecimal idToReservaEstimada) throws SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getSubramosFromReservaEstimada(idToReservaEstimada);			
	}
	
	public Double getSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo) throws SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getSumaAseguradaPorReporteYSubramo(idReporteSiniestro, idSubramo);
	}
	
	public Double getPorcentajeSumaAseguradaPorReporteYSubramo(BigDecimal idReporteSiniestro, BigDecimal idSubramo) throws SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getPorcentajeSumaAseguradaPorReporteYSubramo(idReporteSiniestro, idSubramo);
	}	
	
	public BigDecimal getIncisoPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza) throws SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getIncisoPorReportePoliza(idToReporteSiniestro, idToPoliza);
	}
	
	public List<IncisosPoliza> generaListaIncisosCoberturaRiesgo(List<IncisoSoporteDaniosSiniestrosDTO> listaIncisosPoliza,BigDecimal idToReporteSiniestro, BigDecimal idToPoliza) throws SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		List<IncisosPoliza> listaIncisosFinal = new ArrayList<IncisosPoliza>();
		BigDecimal incisoCoberturaRiesgo = this.getIncisoPorReportePoliza(idToReporteSiniestro, idToPoliza);
		for(IncisoSoporteDaniosSiniestrosDTO incisoSoporteDaniosSiniestrosDTO:listaIncisosPoliza){
			IncisosPoliza incisosPoliza = new IncisosPoliza();
			incisosPoliza.setDireccionInciso(incisoSoporteDaniosSiniestrosDTO.getDireccionInciso());
			incisosPoliza.setFechaFinVigencia(incisoSoporteDaniosSiniestrosDTO.getFechaFinVigencia());
			incisosPoliza.setFechaInicioVigencia(incisoSoporteDaniosSiniestrosDTO.getFechaInicioVigencia());
			incisosPoliza.setNumeroInciso(incisoSoporteDaniosSiniestrosDTO.getNumeroInciso());
			if(incisoCoberturaRiesgo.compareTo(incisoSoporteDaniosSiniestrosDTO.getNumeroInciso()) == 0){
				incisosPoliza.setSeleccionado(SELECCIONADO);
			}else{
				incisosPoliza.setSeleccionado(NO_SELECCIONADO);
			}
			listaIncisosFinal.add(incisosPoliza);
		}
		
		return listaIncisosFinal;
	}	
	public List<RiesgoAfectadoId> getSubIncisoSeccionPorReportePoliza(BigDecimal idToReporteSiniestro, BigDecimal idToPoliza,BigDecimal numeroInciso) throws SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getSubIncisoSeccionPorReportePoliza(idToReporteSiniestro, idToPoliza,numeroInciso);
	}
	private boolean existeSubIncisoSeccion(SeccionSubIncisoSoporteDanosSiniestrosDTO subIncisoSeccion,List<RiesgoAfectadoId> listaSeccionesBD){
		for(RiesgoAfectadoId riesgoAfectadoId :listaSeccionesBD){
			if(riesgoAfectadoId.getNumerosubinciso().compareTo(subIncisoSeccion.getNumeroSubInciso()) == 0 
				&& riesgoAfectadoId.getIdtoseccion().compareTo(subIncisoSeccion.getIdToSeccion()) == 0){
				return true;
			}
		}
		return false;
	}
	
	public List<SubIncisosSeccionPoliza> generaListaSeccionSubIncisos(List<SeccionSubIncisoSoporteDanosSiniestrosDTO> listaSubIncisosSeccionPoliza,BigDecimal idToReporteSiniestro, BigDecimal idToPoliza,BigDecimal numeroInciso) throws SystemException{
		List<SubIncisosSeccionPoliza> listaSubIncisosSeccionFinal = new ArrayList<SubIncisosSeccionPoliza>();
		List<RiesgoAfectadoId> listaSeccionesBD = this.getSubIncisoSeccionPorReportePoliza(idToReporteSiniestro, idToPoliza,numeroInciso);
		for(SeccionSubIncisoSoporteDanosSiniestrosDTO subIncisoSeccion:listaSubIncisosSeccionPoliza){
			SubIncisosSeccionPoliza subIncisosSeccionPoliza = new SubIncisosSeccionPoliza();
			subIncisosSeccionPoliza.setDescripcionSubInciso(subIncisoSeccion.getDescripcionSubInciso());
			subIncisosSeccionPoliza.setIdToSeccion(subIncisoSeccion.getIdToSeccion());
			subIncisosSeccionPoliza.setNombreComercialSeccion(subIncisoSeccion.getNombreComercialSeccion());
			subIncisosSeccionPoliza.setNumeroSubInciso(subIncisoSeccion.getNumeroSubInciso());
			
			if(this.existeSubIncisoSeccion(subIncisoSeccion, listaSeccionesBD)){
				subIncisosSeccionPoliza.setSeleccionado(SELECCIONADO);
			}else{
				subIncisosSeccionPoliza.setSeleccionado(NO_SELECCIONADO);
			}
			listaSubIncisosSeccionFinal.add(subIncisosSeccionPoliza);
		}
		
		return listaSubIncisosSeccionFinal;
	}	
	
	private boolean existeSeccionCobertura(DetallePolizaSoporteDanosSiniestrosDTO coberturaRiesgoPoliza,List<RiesgoAfectadoDTO> listaRiesgosBD){
		for(RiesgoAfectadoDTO riesgoAfectado :listaRiesgosBD){
			if( riesgoAfectado.getId().getNumeroinciso().compareTo(coberturaRiesgoPoliza.getNumeroInciso()) == 0
				&& riesgoAfectado.getId().getNumerosubinciso().compareTo(coberturaRiesgoPoliza.getNumeroSubInciso()) == 0
				&& riesgoAfectado.getId().getIdtoseccion().compareTo(coberturaRiesgoPoliza.getIdToSeccion() ) == 0
				&& riesgoAfectado.getId().getIdtocobertura().compareTo(coberturaRiesgoPoliza.getIdToCobertura()) == 0
				&& riesgoAfectado.getId().getIdtoriesgo().compareTo(coberturaRiesgoPoliza.getIdToRiesgo()) == 0
				){
				return true;
			}
		}
		return false;
	}
	public List<CoberturasRiesgoPoliza> generaListaCoberturasRiesgoAfectar(List<DetallePolizaSoporteDanosSiniestrosDTO> listaCoberturasPoliza,BigDecimal idToReporteSiniestro) throws SystemException{
		List<CoberturasRiesgoPoliza> listaCoberturasFinal = new ArrayList<CoberturasRiesgoPoliza>();
		List<RiesgoAfectadoDTO> listaRiesgosBD = this.listarCoberturasRiesgo(idToReporteSiniestro);
		for(DetallePolizaSoporteDanosSiniestrosDTO coberturaPoliza:listaCoberturasPoliza){
			CoberturasRiesgoPoliza coberturasRiesgoPoliza = new CoberturasRiesgoPoliza();
			coberturasRiesgoPoliza.setIdToSeccion(coberturaPoliza.getIdToSeccion());
			coberturasRiesgoPoliza.setDescripcionSeccion(coberturaPoliza.getDescripcionSeccion());
			coberturasRiesgoPoliza.setIdToCobertura(coberturaPoliza.getIdToCobertura());
			coberturasRiesgoPoliza.setDescripcionCobertura(coberturaPoliza.getDescripcionCobertura());
			coberturasRiesgoPoliza.setIdToRiesgo(coberturaPoliza.getIdToRiesgo());
			coberturasRiesgoPoliza.setDescripcionRiesgo(coberturaPoliza.getDescripcionRiesgo());
			coberturasRiesgoPoliza.setPrimaNeta(coberturaPoliza.getPrimaNeta());
			coberturasRiesgoPoliza.setDeducible(coberturaPoliza.getDeducible());
			coberturasRiesgoPoliza.setCoaseguro(coberturaPoliza.getCoaseguro());
			coberturasRiesgoPoliza.setSumaAsegurada(coberturaPoliza.getSumaAsegurada());
			
			coberturasRiesgoPoliza.setNumeroSubInciso(coberturaPoliza.getNumeroSubInciso());
			
			if(this.existeSeccionCobertura(coberturaPoliza, listaRiesgosBD)){
				coberturasRiesgoPoliza.setSeleccionado(SELECCIONADO);
			}else{
				coberturasRiesgoPoliza.setSeleccionado(NO_SELECCIONADO);
			}
			listaCoberturasFinal.add(coberturasRiesgoPoliza);
		}
		
		return listaCoberturasFinal;
	}
	
	public RiesgoAfectadoDTO actualizarRiesgoAfectado(RiesgoAfectadoDTO riesgoAfectadoDTO)throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.actualizarRiesgoAfectado(riesgoAfectadoDTO);	
	}
	
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro)throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getCoberturasRiesgoPorReporte(idToReporteSiniestro);	
	}
	
	public List<RiesgoAfectadoDTO> getCoberturasRiesgoPorReporte(BigDecimal idToReporteSiniestro,Byte estatus)throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.getCoberturasRiesgoPorReporte(idToReporteSiniestro, estatus);
	}
	
	public RiesgoAfectadoDTO findById(RiesgoAfectadoId id)throws ExcepcionDeAccesoADatos, SystemException{
		RiesgoAfectadoSN riesgoAfectadoSN = new RiesgoAfectadoSN();
		return riesgoAfectadoSN.findById(id);
	}
}
