package mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaFacadeRemote;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;

public class AutorizacionTecnicaSN {
	private AutorizacionTecnicaFacadeRemote beanRemoto;

	public AutorizacionTecnicaSN() throws SystemException {
		LogDeMidasWeb.log("Entrando en AutorizacionTecnicaSN - Constructor",
				Level.INFO, null);
		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		beanRemoto = serviceLocator
				.getEJB(AutorizacionTecnicaFacadeRemote.class);
		LogDeMidasWeb.log("bean Remoto instanciado AutorizacionTecnicaSN",
				Level.FINEST, null);
	}

	public AutorizacionTecnicaDTO findById(BigDecimal id) {
		return beanRemoto.findById(id);
	}

	public void agregarAutorizacionTecnica(
			AutorizacionTecnicaDTO autorizacionTecnicaDTO) {
		beanRemoto.save(autorizacionTecnicaDTO);
	}

	public void actualizarAutorizacionTecnica(
			AutorizacionTecnicaDTO autorizacionTecnicaDTO) {
		beanRemoto.update(autorizacionTecnicaDTO);
	}

	public void eliminarAutorizacionTecnica(
			AutorizacionTecnicaDTO autorizacionTecnicaDTO) {
		beanRemoto.delete(autorizacionTecnicaDTO);
	}

	public List<AutorizacionTecnicaDTO> listarAutorizacionesTecnicas() {
		return beanRemoto.findAll();
	}

	public List<AutorizacionTecnicaDTO> findByProperty(String propertyName,
			final Object value) {
		return beanRemoto.findByProperty(propertyName, value);
	}

	public AutorizacionTecnicaDTO obtenerAutorizacionIndemnizacion(
			BigDecimal idToReporteSiniestro, BigDecimal idToIndemnizacion) {
		return beanRemoto.obtenerAutorizacionIndemnizacion(
				idToReporteSiniestro, idToIndemnizacion);
	}

	public AutorizacionTecnicaDTO obtenerAutorizacionIngreso(
			BigDecimal idToReporteSiniestro, BigDecimal idToIngresoSiniestro) {
		return beanRemoto.obtenerAutorizacionIngreso(idToReporteSiniestro,
				idToIngresoSiniestro);
	}

	public AutorizacionTecnicaDTO obtenerAutorizacionGasto(
			BigDecimal idToReporteSiniestro, BigDecimal idToGastoSiniestro) {
		return beanRemoto.obtenerAutorizacionGasto(idToReporteSiniestro,
				idToGastoSiniestro);
	}

	public List<AutorizacionTecnicaDTO> getAutorizacionesPorIdReporte(
			BigDecimal idReporteSiniestro) {
		List<AutorizacionTecnicaDTO> autorizacionesTecnicas = null;

		autorizacionesTecnicas = beanRemoto.findByProperty(
				"reporteSiniestroDTO.idToReporteSiniestro", idReporteSiniestro);

		return autorizacionesTecnicas;
	}

	public List<AutorizacionTecnicaDTO> listarAutorizacionesGastosFiltrado(
			SoportePagosDTO soportePagosDTO) {
		List<AutorizacionTecnicaDTO> autorizacionesTecnicas = null;

		autorizacionesTecnicas = beanRemoto
				.listarAutorizacionesGastosFiltrado(soportePagosDTO);

		return autorizacionesTecnicas;
	}
}
