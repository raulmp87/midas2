package mx.com.afirme.midas.siniestro.finanzas.pagos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaSN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaSN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoSN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle.DetalleOrdenDePagoDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SoportePagosDN {
	private static final SoportePagosDN INSTANCIA = new SoportePagosDN();

	public static SoportePagosDN getInstancia() {
		return SoportePagosDN.INSTANCIA;
	}
	
	public Map<String, String> generarOrdenDePago(String usuario,
			BigDecimal... autorizaciones){
		
		Map<String, String>  respuesta = new HashMap<String, String>();
		OrdenDePagoDN ordenDePagoDN = OrdenDePagoDN.getInstancia(); 
		try {
			if (autorizaciones != null && autorizaciones.length > 0) {
				List<AutorizacionTecnicaDTO> autorizacionesDTO = cargarAutorizaciones(autorizaciones);
				Set<String> errores = validarAutorizaciones(autorizacionesDTO);
				if (errores != null && errores.size()>0){
					respuesta.put("icono", "10");//Exito
					respuesta.put("mensaje", errores.toString());
					return respuesta;
				}
					
				OrdenDePagoDTO ordenDePagoDTO = new OrdenDePagoDTO();
				ordenDePagoDTO.setEstatus(OrdenDePagoDTO.ESTATUS_PENDIENTE);
				try{
					ordenDePagoDTO = ordenDePagoDN.generarOrdenDePago(ordenDePagoDTO, autorizacionesDTO, usuario);
					if(ordenDePagoDTO.getIdSolicitudCheque()!= null){
						
						respuesta.put("icono", "30");//Exito
						respuesta.put("mensaje", "Se gener� correctamente la orden de pago, el n�mero es: "+ordenDePagoDTO.getIdToOrdenPago());													
					}else{
						respuesta.put("icono", "10");//Error
						respuesta.put("mensaje", "Error al generar la orden de pago: no se genero correctamente la orden de pago");						
					}
				}catch (Exception e){
					respuesta.put("icono", "10");//Error
					respuesta.put("mensaje", "Error al generar la orden de pago: " + e.getCause());		
					return respuesta;
				}					
			} else {
				respuesta.put("icono", "10");//Error
				respuesta.put("mensaje",
							 "Error: Las autorizaciones tecnicas proporcionadas no son validas");
			}
		} catch (SystemException re) {	
			LogDeMidasWeb.log("generarOrdenDePago failed", Level.SEVERE, re);
			respuesta.put("icono", "10");//Error
			respuesta.put("mensaje", "Error al generar la orden de pago.");
			return respuesta;
		}
		return respuesta;
	}
	
	protected Set<String>  validarAutorizaciones(List<AutorizacionTecnicaDTO> autorizaciones) throws SystemException{
		Set<String> errores = new HashSet<String>();
		OrdenDePagoSN ordenDePagoSN = new OrdenDePagoSN();
		PolizaSN polizaSN = new PolizaSN();
		List<DetalleOrdenPagoDTO> detalleList;
		double validadorPrestadorDeServicios = 0D;
		double validadorMoneda = 0D;
		PolizaDTO polizaDTO = null;
		if(autorizaciones != null && autorizaciones.size()>0){
			for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
				
				validadorPrestadorDeServicios += autorizacion.getGastoSiniestroDTO().getIdTcPrestadorServicios().doubleValue();
				
				detalleList = ordenDePagoSN.buscarDetallePorAutorizacionTecnica(autorizacion.getIdToAutorizacionTecnica()); 
				
				if (detalleList != null && detalleList.size() > 0) 
					errores.add(UtileriasWeb
									.getMensajeRecurso(
											Sistema.ARCHIVO_RECURSOS,
											"siniestros.finanzas.pagos.rdn.autorizacion.asignada",
											autorizacion
													.getIdToAutorizacionTecnica(),
											detalleList.get(0).getOrdenDePagoDTO()
													.getIdToOrdenPago()));

				if(autorizacion.getGastoSiniestroDTO() ==  null )
					errores.add(UtileriasWeb
							.getMensajeRecurso(
									Sistema.ARCHIVO_RECURSOS,
									"siniestros.finanzas.pagos.rdn.autorizacion.concepto.invalido",
									autorizacion
											.getIdToAutorizacionTecnica()));

				polizaDTO = polizaSN.getPorId(autorizacion.getGastoSiniestroDTO().getReporteSiniestroDTO().getNumeroPoliza()); 
				if(polizaDTO != null)
					validadorMoneda += polizaDTO.getCotizacionDTO().getIdMoneda().doubleValue();
					
			}
			
			if(validadorPrestadorDeServicios/autorizaciones.size() != autorizaciones.get(0).getGastoSiniestroDTO().getIdTcPrestadorServicios().doubleValue())
				errores.add(UtileriasWeb
						.getMensajeRecurso(
								Sistema.ARCHIVO_RECURSOS,
								"siniestros.finanzas.pagos.rdn.autorizacion.mismo.proveedor"));
			
			if(polizaDTO != null)
				if(validadorMoneda/autorizaciones.size() != polizaDTO.getCotizacionDTO().getIdMoneda().doubleValue())
					errores.add(UtileriasWeb
							.getMensajeRecurso(
									Sistema.ARCHIVO_RECURSOS,
									"siniestros.finanzas.pagos.rdn.autorizacion.misma.moneda"));
			
		}else{
			errores.add("Ocurrio un error, favor de intentar mas tarde");
		}
			
		return errores;
	}

	protected List<AutorizacionTecnicaDTO> cargarAutorizaciones(
			BigDecimal... autorizaciones) throws SystemException {
		AutorizacionTecnicaSN autorizacionTecnicaSN = new AutorizacionTecnicaSN();
		List<AutorizacionTecnicaDTO> autorizacionesList = new ArrayList<AutorizacionTecnicaDTO>();
		for (BigDecimal idToAutorizacion : autorizaciones) {
			autorizacionesList.add(autorizacionTecnicaSN
					.findById(idToAutorizacion));
		}
		return autorizacionesList;
	}

	protected GastoSiniestroDTO acumulaGastos(
			List<AutorizacionTecnicaDTO> autorizaciones) {
		GastoSiniestroDTO gastoSiniestroDTO = new GastoSiniestroDTO();
		Double montoGasto = 0D;
		Double montoISR = 0D;
		Double montoIVA = 0D;
		Double montoIVARetencion = 0D;
		Double montoISRRetencion = 0D;
		for (AutorizacionTecnicaDTO autorizacion : autorizaciones) {
			montoGasto += autorizacion.getGastoSiniestroDTO().getMontoGasto() != null ? autorizacion
					.getGastoSiniestroDTO().getMontoGasto()
					: 0D;
			montoISR += autorizacion.getGastoSiniestroDTO().getMontoISR() != null ? autorizacion
					.getGastoSiniestroDTO().getMontoISR()
					: 0D;
			montoIVA += autorizacion.getGastoSiniestroDTO().getMontoIVA() != null ? autorizacion
					.getGastoSiniestroDTO().getMontoIVA()
					: 0D;
			montoIVARetencion += autorizacion.getGastoSiniestroDTO()
					.getMontoIVARetencion() != null ? autorizacion
					.getGastoSiniestroDTO().getMontoIVARetencion() : 0D;
			montoISRRetencion += autorizacion.getGastoSiniestroDTO()
					.getMontoISRRetencion() != null ? autorizacion
					.getGastoSiniestroDTO().getMontoISRRetencion() : 0D;
		}
		gastoSiniestroDTO.setMontoGasto(montoGasto);
		gastoSiniestroDTO.setMontoISR(montoISR);
		gastoSiniestroDTO.setMontoIVA(montoIVA);
		gastoSiniestroDTO.setMontoIVARetencion(montoIVARetencion);
		gastoSiniestroDTO.setMontoISRRetencion(montoISRRetencion);
		gastoSiniestroDTO.setMontoOtros(montoGasto + montoIVA
				- montoIVARetencion - montoISRRetencion);
		return gastoSiniestroDTO;
	}
	public Map<String, String> cancelarOrdenDePago(String usuario,
			BigDecimal idOrdenDePago) throws ExcepcionDeAccesoADatos, SystemException{
		Map<String, String>  respuesta = new HashMap<String, String>();
		
		OrdenDePagoDTO ordenDePagoDTO = OrdenDePagoDN.getInstancia().findById(idOrdenDePago);
		if(ordenDePagoDTO != null){
			List<DetalleOrdenPagoDTO> detalle = ordenDePagoDTO.getDetalleOrdenDePago();
			List<AutorizacionTecnicaDTO> autorizacionesDTO = new ArrayList<AutorizacionTecnicaDTO>();
			for(DetalleOrdenPagoDTO detalleOrden: detalle){
				autorizacionesDTO.add(detalleOrden.getAutorizacionTecnicaDTO());
			}			
			if(autorizacionesDTO != null){
				if(validaAutorizaciones(autorizacionesDTO)){
					ordenDePagoDTO.setEstatus(OrdenDePagoDTO.ESTATUS_CANCELADA);
					ordenDePagoDTO = OrdenDePagoDN.getInstancia().update(ordenDePagoDTO);
					try{
						actualizaAutorizaciones(autorizacionesDTO, usuario);
					}catch (ExcepcionDeAccesoADatos e){
						respuesta.put("icono", "10");//ERROR
						respuesta.put("mensaje", "Error al generar la cancelaci�n de la orden de pago: " + e.getMessage());														
					}catch (SystemException e){
						respuesta.put("icono", "10");//ERROR
						respuesta.put("mensaje", "Error al generar la cancelaci�n de la orden de pago: " + e.getMessage());						
					}
					respuesta.put("icono", "30");//EXITO
					respuesta.put("mensaje", "La orden de pago:"+ idOrdenDePago +" se cancelo exitosamente");																		
				}else{
					respuesta.put("icono", "20");//INFO
					respuesta.put("mensaje", "Error al generar la cancelaci�n de la orden de pago: Las autorizaciones presentan inconsitencias");													
				}
			}else{
				respuesta.put("icono", "20");//INFO
				respuesta.put("mensaje", "Error al generar la cancelaci�n de la orden de pago: Ocurrio un error al cargar las autorizaciones");								
			}
		}else{
			respuesta.put("icono", "20");//INFO
			respuesta.put("mensaje", "Error al generar la cancelaci�n de la orden de pago: No se encontro la Orden de Pago");				
		}		
		return respuesta;
	}
	private boolean validaAutorizaciones (List<AutorizacionTecnicaDTO> autorizaciones){
		boolean esValido = true;
		
		if(autorizaciones == null || autorizaciones.size()==0)
			esValido = false;
		try{
			SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
			for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
				if(siniestroMovimientosDN.validaMovimientoPagado(autorizacion)){
					esValido = false;
					break;
				}
			}
		}catch (Exception e) {
			esValido = false;
		}
		return esValido;
	}
	private void actualizaAutorizaciones(List<AutorizacionTecnicaDTO> autorizaciones, String usuario) throws ExcepcionDeAccesoADatos, SystemException{
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia(); 
		for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
			if(autorizacion.getIndemnizacionDTO() == null || indemnizacionDN.tienePagosParciales(autorizacion.getReporteSiniestroDTO().getIdToReporteSiniestro())){
				siniestroMovimientosDN.actualizarEstatusMovimiento(autorizacion, EstatusFinanzasDTO.AUTORIZADO);
				if(autorizacion.getIndemnizacionDTO() !=null){
					IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(autorizacion.getIndemnizacionDTO().getIdToIndemnizacion());
					if(indemnizacionDTO.getUltimoPago()){
						siniestroMovimientosDN.cambiaEstatusReporte(autorizacion.getReporteSiniestroDTO().getIdToReporteSiniestro(), usuario, ReporteSiniestroDTO.PENDIENTE_GENERAR_ORDEN_DE_PAGO);
					}
				} 
			}else{
				siniestroMovimientosDN.actualizarEstatusMovimiento(autorizacion, EstatusFinanzasDTO.AUTORIZADO);
				siniestroMovimientosDN.cambiaEstatusReporte(autorizacion.getReporteSiniestroDTO().getIdToReporteSiniestro(), usuario, ReporteSiniestroDTO.PENDIENTE_GENERAR_ORDEN_DE_PAGO);
			}	
		}
	}
	
	public SoportePagosForm cargaDatosImpresionOrdenDePago(String usuario,
			BigDecimal idOrdenDePago) throws ExcepcionDeAccesoADatos, SystemException{
		SoportePagosForm pagosForm = new SoportePagosForm();
		OrdenDePagoDTO ordenPagoDTO = OrdenDePagoDN.getInstancia().findById(idOrdenDePago);
		pagosForm.getSoportePagosDTO().setNumeroOrdenDePago(idOrdenDePago.toString());
		pagosForm.setOrdenDePagoDTO(ordenPagoDTO);
		if(ordenPagoDTO != null){

			List<DetalleOrdenPagoDTO> detalle = ordenPagoDTO.getDetalleOrdenDePago();
			if(detalle == null || detalle.size()<1){
				detalle = DetalleOrdenDePagoDN.getInstancia().detallesOrdenDePago(ordenPagoDTO.getIdToOrdenPago());
			}
			pagosForm.setOrdenesDePago(detalle);
			List<AutorizacionTecnicaDTO> autorizacionesDTO = new ArrayList<AutorizacionTecnicaDTO>();
			for(DetalleOrdenPagoDTO detalleOrden: detalle){
				autorizacionesDTO.add(detalleOrden.getAutorizacionTecnicaDTO());
			}

			for(AutorizacionTecnicaDTO autorizacion: autorizacionesDTO){
				autorizacion.getReporteSiniestroDTO().setPolizaDTO(PolizaDN.getInstancia().getPorId(autorizacion.getReporteSiniestroDTO().getNumeroPoliza()));
				autorizacion.getGastoSiniestroDTO().setNombrePrestadorServicios(PrestadorServiciosDN.getInstancia().detallePrestador(autorizacion.getGastoSiniestroDTO().getIdTcPrestadorServicios(), usuario).getNombrePrestador());
			}
			pagosForm.setAutorizaciones(autorizacionesDTO);
			pagosForm.setGastoAcumulado(SoportePagosDN.getInstancia().acumulaGastos(autorizacionesDTO));

		}		
		return pagosForm;
	}
	
}
