package mx.com.afirme.midas.siniestro.finanzas.ingreso;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.ingreso.referencia.ReferenciaIngresoDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadRemesaDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.contabilidad.ContabilidadRemesaDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class IngresosAction extends MidasMappingDispatchAction{
	
	public ActionForward ingresos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;

		IngresosForm forma = (IngresosForm) form;
//		forma.setIdToReporteSiniestro("383");
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String tipoMoneda = "";
		try {
			tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		forma.setTipoMoneda(tipoMoneda);
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward guardarIngresosSinietro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		IngresosForm forma = (IngresosForm) form;
		 
		IngresoSiniestroDTO ingresoSiniestroDTO = new IngresoSiniestroDTO();
		
		try {
			IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
			llenarDTO(ingresoSiniestroDTO, forma);
			Date fechaActual =Calendar.getInstance().getTime();
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			ingresoSiniestroDTO.setFechaCreacion(fechaActual);
			ingresoSiniestroDTO.setIdTcUsuarioCreacion(new BigDecimal(usuario.getId()));
//			ingresoSiniestroDTO.setEstatus(Sistema.ABIERTO);
			EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.ABIERTO);
			ingresoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
			
			//INICIO Codigo para registrar salvamento
			if (!UtileriasWeb.esCadenaVacia(forma.getIdToSalvamentoSiniestro())){				
				request.getSession().setAttribute("ingresoSiniestroDTO", ingresoSiniestroDTO);
				return mapping.findForward(Sistema.ACCION_GUARDAR);
			}
			//FIN Codigo para registrar salvamento
			
			ingresoSiniestroDN.agregarIngreso(ingresoSiniestroDTO);			
			
			// TODO 
			//Distribuir Cuenta por Pagar (la cantidad monetaria, el tipo de cambio de moneda, el concepto del ingreso, el n�mero de p�liza).
//			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia();
//			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(new BigDecimal(forma.getIdToReporteSiniestro()));
//			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
//			reporteEstatusDTO.setIdTcReporteEstatus(PENDIENTE_GENERAR_AUTORIZACION_TECNICA);
//			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
//			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			mensajeExitoAgregar(forma,"El Ingreso se guardo de forma exitosa");
			request.setAttribute("idToReporteSiniestro", forma.getIdToReporteSiniestro());
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificarIngresos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;

		IngresosForm forma = (IngresosForm) form;
		IngresoSiniestroDTO ingresoSiniestroDTO;
		BigDecimal idToIngresoSiniestro = new BigDecimal(request.getParameter("idToIngresoSiniestro"));
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		String tipoMoneda = "";
		try {
			IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
			ingresoSiniestroDTO = ingresoSiniestroDN.findById(idToIngresoSiniestro);
			llenarForm(ingresoSiniestroDTO,forma);
			tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
			forma.setTipoMoneda(tipoMoneda);
			
			boolean modoLectura = this.isIngresoSoloLectura(ingresoSiniestroDTO);
			request.setAttribute("modoLectura", modoLectura);						
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	public ActionForward guardarModificarIngresosSinietro(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;
		
		IngresosForm forma = (IngresosForm) form;
		 
		IngresoSiniestroDTO ingresoSiniestroDTO;
		
		try {
			IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
			ingresoSiniestroDTO = ingresoSiniestroDN.findById(new BigDecimal(forma.getIdToIngresoSiniestro()));
			llenarDTO(ingresoSiniestroDTO, forma);
			Date fechaActual =Calendar.getInstance().getTime();
			
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			ingresoSiniestroDTO.setFechaModificacion(fechaActual);
			ingresoSiniestroDTO.setIdTcUsuarioModificacion(new BigDecimal(usuario.getId()));
//			EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
//			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.ABIERTO);
//			ingresoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
//			ingresoSiniestroDTO.setEstatus(false);
			
			ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
			mensajeExitoModificar(forma,"El Ingreso se Modifico de forma correcta");
			request.setAttribute("idToReporteSiniestro", forma.getIdToReporteSiniestro());
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	public ActionForward listarIngresos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		IngresosForm forma = (IngresosForm) form;

		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		List<IngresoSiniestroDTO> listaIngresoSiniestro = null; 
		
		try {
			listaIngresoSiniestro = ingresoSiniestroDN.listarIngresosSiniestro(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
			forma.setListaIngresos(listaIngresoSiniestro);
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(forma.getIdToReporteSiniestro()));
			forma.setTipoMoneda(tipoMoneda);
//			forma.setIdToReporteSiniestro(idToReporteSiniestro.toString());
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return mapping.findForward(reglaNavegacion);
	}
	public ActionForward eliminarIngresos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		IngresoSiniestroDTO ingresoSiniestroDTO = null;
		List<IngresoSiniestroDTO> listaIngresoSiniestro = null; 
		IngresosForm forma = (IngresosForm) form;
		String[] strIdsIngresoSiniestro = forma.getEliminarIngreso();
		
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		
		for(String strIdIngresoSiniestro : strIdsIngresoSiniestro){
			BigDecimal idIngresoSiniestro = new BigDecimal(strIdIngresoSiniestro);
			try{		
				List<AutorizacionTecnicaDTO>  listaATs = autorizacionTecnicaDN.findByProperty("ingresoSiniestroDTO.idToIngresoSiniestro", idIngresoSiniestro);
				ingresoSiniestroDTO = ingresoSiniestroDN.findById(idIngresoSiniestro);
				if(listaATs != null && listaATs.size() > 0){
					EstatusFinanzasDTO estatusFinanzasDTO = ingresoSiniestroDTO.getEstatusFinanzas();
					estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
					ingresoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
					ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
				}else{
					ingresoSiniestroDN.eliminarIngreso(ingresoSiniestroDTO);
				}
			} catch (ExcepcionDeAccesoADatos e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}			
		}
		try {
			listaIngresoSiniestro = ingresoSiniestroDN.listarIngresosSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
			forma.setListaIngresos(listaIngresoSiniestro);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		mensajeExitoBorrar(forma, "Los Ingresos se eliminaron de forma correcta");
		
		return mapping.findForward(reglaNavegacion);		
	}

	public ActionForward cancelarIngresos(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;

		IngresosForm forma = (IngresosForm) form;
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		IngresoSiniestroDTO ingresoSiniestroDTO = null;
		
		String[] strIdsIngresoSiniestro = forma.getCancelarIngreso();

		try{				
			for(String strIdIngresoSiniestro : strIdsIngresoSiniestro){
				BigDecimal idIngresoSiniestro = new BigDecimal(strIdIngresoSiniestro);
				ingresoSiniestroDTO = ingresoSiniestroDN.findById(idIngresoSiniestro);
				if(ingresoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas() != EstatusFinanzasDTO.PAGADO){

					if(siniestroMovimientosDN.cancelaMovimientoCompleto(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), idIngresoSiniestro, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO)){
						EstatusFinanzasDTO estatusFinanzasDTO = ingresoSiniestroDTO.getEstatusFinanzas();
						estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
						ingresoSiniestroDTO.setEstatusFinanzas(estatusFinanzasDTO);
						ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
					}
				}
			}
			List<IngresoSiniestroDTO> listaIngresoSiniestro = null; 
			
			listaIngresoSiniestro = ingresoSiniestroDN.listarIngresosSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
			forma.setListaIngresos(listaIngresoSiniestro);
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}							
		
		return mapping.findForward(reglaNavegacion);
	}
	
	private void llenarDTO(IngresoSiniestroDTO ingresoSiniestroDTO,IngresosForm forma){
		
		if(forma.getIdToReporteSiniestro() != null){
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();
			reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(forma.getIdToReporteSiniestro()));
			ingresoSiniestroDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
		}
		if(!UtileriasWeb.esCadenaVacia(forma.getFechaCobro())){
			Date fecha = null;
			try {
				fecha = UtileriasWeb.getFechaFromString(forma.getFechaCobro());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			ingresoSiniestroDTO.setFechaCobro(fecha);
		}
		if(!UtileriasWeb.esCadenaVacia(forma.getMontoIngreso())){
			
			Double montoIngreso = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIngreso());
			ingresoSiniestroDTO.setMonto(montoIngreso);
		}
		if(!UtileriasWeb.esCadenaVacia(forma.getIva())){			
			Double Iva = UtileriasWeb.eliminaFormatoMoneda(forma.getIva());
			ingresoSiniestroDTO.setIva(Iva);
		}
		//valores de Porcentajes y montos
		if(forma.isIndIva() && !UtileriasWeb.esCadenaVacia(forma.getPorcentajeIva())){
//			ingresoSiniestroDTO.setIndIVA(forma.isIndIva());			
			Double montoIVA = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIva());
			
			ingresoSiniestroDTO.setMontoIVA(montoIVA);
			ingresoSiniestroDTO.setPorcentajeIVA(new Double(forma.getPorcentajeIva()));
		}else{
			ingresoSiniestroDTO.setMontoIVA(null);
			ingresoSiniestroDTO.setPorcentajeIVA(null);
		}
		if(forma.isIndIvaRet() && !UtileriasWeb.esCadenaVacia(forma.getPorcentajeIvaRetencion())){
//			ingresoSiniestroDTO.setIndIVARet(forma.isIndIvaRet());
			Double montoIvaRetencion = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIvaRetencion());
			
			ingresoSiniestroDTO.setMontoIVARetencion(montoIvaRetencion);
			ingresoSiniestroDTO.setPorcentajeIVARetencion(new Double(forma.getPorcentajeIvaRetencion()));
		}else{
			ingresoSiniestroDTO.setMontoIVARetencion(null);
			ingresoSiniestroDTO.setPorcentajeIVARetencion(null);
		}
		if(forma.isIndIsr() && !UtileriasWeb.esCadenaVacia(forma.getPorcentajeIsr())){
//			ingresoSiniestroDTO.setIndISR(forma.isIndIsr());
			Double montoISR = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIsr());
			
			ingresoSiniestroDTO.setMontoISR(montoISR);
			ingresoSiniestroDTO.setPorcentajeISR(new Double(forma.getPorcentajeIsr()));
		}else{
			ingresoSiniestroDTO.setMontoISR(null);
			ingresoSiniestroDTO.setPorcentajeISR(null);
		}
		if(forma.isIndIsrRet() && !UtileriasWeb.esCadenaVacia(forma.getPorcentajeIsrRetencion())){
//			ingresoSiniestroDTO.setIndISRRet(forma.isIndIsrRet());
			Double montoISRRetencion = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIsrRetencion());
			
			ingresoSiniestroDTO.setMontoISRRetencion(montoISRRetencion);
			ingresoSiniestroDTO.setPorcentajeISRRetencion(new Double(forma.getPorcentajeIsrRetencion()));
		}else{
			ingresoSiniestroDTO.setMontoISRRetencion(null);
			ingresoSiniestroDTO.setPorcentajeISRRetencion(null);
		}
		if(forma.isIndOtros() && !UtileriasWeb.esCadenaVacia(forma.getPorcentajeOtros())){
//			ingresoSiniestroDTO.setOtros(forma.isIndOtros());
			Double montoOtros = UtileriasWeb.eliminaFormatoMoneda(forma.getMontoOtros());
			
			ingresoSiniestroDTO.setMontoOtros(montoOtros);
			ingresoSiniestroDTO.setPorcentajeOtros(new Double(forma.getPorcentajeOtros()));
		}else{
			ingresoSiniestroDTO.setMontoOtros(null);
			ingresoSiniestroDTO.setPorcentajeOtros(null);
		}
		//Fin valores de Porcentajes y montos
		
		if(forma.getConceptoIngreso() != null){
			ConceptoIngresoDTO conceptoIngresoDTO = new ConceptoIngresoDTO();
			conceptoIngresoDTO.setIdTcConceptoIngreso(forma.getConceptoIngreso());
			ingresoSiniestroDTO.setConceptoIngreso(conceptoIngresoDTO);
		}
		
		if(!UtileriasWeb.esCadenaVacia(forma.getDescripcionIngreso())){		
			String descripcion = forma.getDescripcionIngreso();
			
			if(descripcion.length() > 240){
				ingresoSiniestroDTO.setDescripcion(descripcion.substring(0, 239) );
			}else{
				ingresoSiniestroDTO.setDescripcion(descripcion);
			}						
		}
				
		if(!UtileriasWeb.esCadenaVacia(forma.getTransferencia())){
			ingresoSiniestroDTO.setTransferencia(new BigDecimal(forma.getTransferencia()));
		}
		if(!UtileriasWeb.esCadenaVacia(forma.getFichaDeposito())){
			ingresoSiniestroDTO.setFichaDeposito(new BigDecimal(forma.getFichaDeposito()));
		}
		
	}
	
	private void llenarForm(IngresoSiniestroDTO ingresoSiniestroDTO,IngresosForm forma){
		if(ingresoSiniestroDTO.getIdToIngresoSiniestro() != null ){
			forma.setIdToIngresoSiniestro(ingresoSiniestroDTO.getIdToIngresoSiniestro().toString());
		}
//		if(ingresoSiniestroDTO.getReporteSiniestroDTO()!= null){
//			forma.setIdToReporteSiniestro(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro().toString());
//		}
		if(ingresoSiniestroDTO.getFechaCobro() != null){
			forma.setFechaCobro(UtileriasWeb.getFechaString(ingresoSiniestroDTO.getFechaCobro()));
		}
		if(ingresoSiniestroDTO.getMonto() != null){
			//forma.setMontoIngreso(formateadorMonto.format(ingresoSiniestroDTO.getMonto()));
			forma.setMontoIngreso(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMonto()));			
		}
		if(ingresoSiniestroDTO.getIva() != null){			
			//forma.setIva(ingresoSiniestroDTO.getIva().toString());
			forma.setIva(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getIva()));
		}
		if(ingresoSiniestroDTO.getPorcentajeIVA() != null){
			forma.setPorcentajeIva(ingresoSiniestroDTO.getPorcentajeIVA().toString());
			//forma.setMontoIva(formateadorMonto.format(ingresoSiniestroDTO.getMontoIVA()));
			forma.setMontoIva(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMontoIVA()));
			forma.setIndIva(true);
		}		
		if(ingresoSiniestroDTO.getPorcentajeIVARetencion() != null){
			forma.setPorcentajeIvaRetencion(ingresoSiniestroDTO.getPorcentajeIVARetencion().toString());
			//forma.setMontoIvaRetencion(formateadorMonto.format(ingresoSiniestroDTO.getMontoIVARetencion()));
			forma.setMontoIvaRetencion(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMontoIVARetencion()));
			forma.setIndIvaRet(true);
		}	
		if(ingresoSiniestroDTO.getPorcentajeISR() != null){
			forma.setPorcentajeIsr(ingresoSiniestroDTO.getPorcentajeISR().toString());
			//forma.setMontoIsr(formateadorMonto.format(ingresoSiniestroDTO.getMontoISR()));
			forma.setMontoIsr(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMontoISR()));
			forma.setIndIsr(true);
		}	
		if(ingresoSiniestroDTO.getPorcentajeISRRetencion() != null){
			forma.setPorcentajeIsrRetencion(ingresoSiniestroDTO.getPorcentajeISRRetencion().toString());
			//forma.setMontoIsrRetencion(formateadorMonto.format(ingresoSiniestroDTO.getMontoISRRetencion()));
			forma.setMontoIsrRetencion(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMontoISRRetencion()));
			forma.setIndIsrRet(true);
		}	
		if(ingresoSiniestroDTO.getPorcentajeOtros() != null){
			forma.setPorcentajeOtros(ingresoSiniestroDTO.getPorcentajeOtros().toString());
			//forma.setMontoOtros(formateadorMonto.format(ingresoSiniestroDTO.getMontoOtros()));
			forma.setMontoOtros(UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMontoOtros()));
			forma.setIndOtros(true);
		}
		if(ingresoSiniestroDTO.getConceptoIngreso() != null){
			forma.setConceptoIngreso(ingresoSiniestroDTO.getConceptoIngreso().getIdTcConceptoIngreso());
		}
		if(ingresoSiniestroDTO.getDescripcion() != null){
			forma.setDescripcionIngreso(ingresoSiniestroDTO.getDescripcion());
		}
		if(ingresoSiniestroDTO.getTransferencia() != null){
			forma.setTransferencia(ingresoSiniestroDTO.getTransferencia().toString());
		}
		if(ingresoSiniestroDTO.getFichaDeposito() != null){
			forma.setFichaDeposito(ingresoSiniestroDTO.getFichaDeposito().toString());
		}
	}
	
	private boolean isIngresoSoloLectura(IngresoSiniestroDTO ingresoSiniestro){
		boolean result = true;
		
		result = ingresoSiniestro.getEstatusFinanzas().getIdTcEstatusfinanzas().shortValue() != EstatusFinanzasDTO.ABIERTO.shortValue();
		
		return result;
	}	
	public void listarIngresosPorAplicar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		@SuppressWarnings("unused")
		String reglaNavegacion = Sistema.EXITOSO;
		IngresosForm forma = (IngresosForm) form;
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		IngresoSiniestroFiltroDTO criteriosDeFiltrado = new IngresoSiniestroFiltroDTO();
		try {
			if(forma.getIdToReporteSiniestro() != null && !forma.getIdToReporteSiniestro().equals("")){
				criteriosDeFiltrado.setNumeroReporte(forma.getIdToReporteSiniestro());
			}
			if(forma.getFechaCobro() != null && !forma.getFechaCobro().equals("")){
				criteriosDeFiltrado.setFechaIngreso(forma.getFechaCobro());
			}
			if(forma.getMontoIngreso() != null && !forma.getMontoIngreso().equals("")){
				criteriosDeFiltrado.setMonto(UtileriasWeb.eliminaFormatoMoneda(forma.getMontoIngreso()));
			}
			if(forma.getConceptoIngreso() != null && !forma.getConceptoIngreso().equals("")){
				criteriosDeFiltrado.setIdConceptoIngreso(forma.getConceptoIngreso());
			}
			
			
			List<IngresoSiniestroDTO> listaIngresosPorAplicar = new ArrayList<IngresoSiniestroDTO>();
			
			String json;
				if(criteriosDeFiltrado != null){
					listaIngresosPorAplicar =  ingresoSiniestroDN.listarIngresosPorAplicar(criteriosDeFiltrado);
				}
				json = this.jsonListaIngresosPorAplicar(listaIngresosPorAplicar);
				response.setContentType("text/json");
				PrintWriter pw = response.getWriter();
				pw.write(json);
				pw.flush();
				pw.close();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public ActionForward mostrarListarIngresosPorAplicar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		IngresosForm aplicarIngresosForm = new IngresosForm();
		if(request.getAttribute("idToReporteSiniestro") != null){
			aplicarIngresosForm.setIdToReporteSiniestro(request.getParameter("idToReporteSiniestro"));
		}
		
		return mapping.findForward(reglaNavegacion);
	}
		
	
	public ActionForward mostrarIngresoPorAplicar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		String reglaNavegacion = Sistema.EXITOSO;
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		AplicarIngresoDN aplicarIngresoDN = AplicarIngresoDN.getInstancia();
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		IntegracionReaseguroDN integracionReaseguroDN = IntegracionReaseguroDN.getInstancia();
		ContabilidadRemesaDN contabilidadRemesaDN = ContabilidadRemesaDN.getInstancia();
		AplicarIngresosForm aplicarIngresosForm = (AplicarIngresosForm) form;
		try {
			IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(UtileriasWeb.regresaBigDecimal(aplicarIngresosForm.getIdToIngresoSiniestro()) );
			
			this.poblarAplicarIngresosForm(aplicarIngresosForm, ingresoSiniestroDTO);
			List<SoporteDistribucionReaseguroDTO>  distribucion = integracionReaseguroDN.getDistribucionDeIngreso(ingresoSiniestroDTO);
			aplicarIngresosForm.setDetalleDistribucion(distribucion);
			
			List<ReferenciaIngresoDTO> listaReferenciasIngresoDTO = null;
			try{
				listaReferenciasIngresoDTO = ingresoSiniestroDN.obtenerListaReferenciasIngreso(UtileriasWeb.obtieneNombreUsuario(request));
			}catch(Exception e){
				listaReferenciasIngresoDTO = new ArrayList<ReferenciaIngresoDTO>();
			}
			aplicarIngresosForm.setListaReferenciasIngreso(listaReferenciasIngresoDTO);
			
			AutorizacionTecnicaDTO autorizacionTecnicaDTO = autorizacionTecnicaDN.obtenerAutorizacionIngreso(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), ingresoSiniestroDTO.getIdToIngresoSiniestro());
			AplicacionIngresoDTO aplicacionIngresoDTO = aplicarIngresoDN.buscarAplicarIngresoPorAT(autorizacionTecnicaDTO.getIdToAutorizacionTecnica(), AplicacionIngresoDTO.CANCELADO);
			List<ContabilidadRemesaDTO> listaContabilidadRemesas = new ArrayList<ContabilidadRemesaDTO>();
			if(aplicacionIngresoDTO != null){
				listaContabilidadRemesas = contabilidadRemesaDN.buscarMovimientosPendientesActualizar(aplicacionIngresoDTO.getIdToAplicacionIngreso());
			}
			if((aplicacionIngresoDTO != null && aplicacionIngresoDTO.getEstatus().intValue() == AplicacionIngresoDTO.PENDIENTE) || listaContabilidadRemesas.size() > 0){
				aplicarIngresosForm.setPermiteAplicarIngreso(0);
			}else{
				aplicarIngresosForm.setPermiteAplicarIngreso(1);
			}
			
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
		
	public void aplicarIngreso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		AplicarIngresosForm aplicarIngresosForm = (AplicarIngresosForm) form;
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		ContabilidadRemesaDN contabilidadRemesaDN = ContabilidadRemesaDN.getInstancia();
		String observaciones = "";
		String referencias = "";
		Usuario user = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(UtileriasWeb.regresaBigDecimal(aplicarIngresosForm.getIdToIngresoSiniestro()) );
			if(aplicarIngresosForm.getObservaciones() != null || aplicarIngresosForm.getReferencias() != null){
				observaciones = aplicarIngresosForm.getObservaciones();
				referencias = aplicarIngresosForm.getReferencias();
			}
			if(UtileriasWeb.esCadenaVacia(aplicarIngresosForm.getIdReferenciaExterna())){
				UtileriasWeb.imprimeMensajeXML("0", "Debe seleccionar una referencia del ingreso." , response);
			}
			else{
				contabilidadRemesaDN.contabilizarRemesa(ingresoSiniestroDTO, user,IngresoSiniestroDTO.ALTA_INGRESO,observaciones,referencias,aplicarIngresosForm.getIdReferenciaExterna());
				ingresoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.PAGADO);
				ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
				UtileriasWeb.imprimeMensajeXML("1", "El ingreso se aplico de forma correcta" , response);
			}
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.imprimeMensajeXML("0", "El ingreso no se pudo aplicar" , response);
		} catch (SystemException e) {
			UtileriasWeb.imprimeMensajeXML("0", "El ingreso no se pudo aplicar" , response);
		}
	}		
	
	public void cancelarIngreso(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute("banderaRefresh", "true");
		AplicarIngresosForm aplicarIngresosForm = (AplicarIngresosForm) form;
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		ContabilidadRemesaDN contabilidadRemesaDN = ContabilidadRemesaDN.getInstancia();
		Usuario user = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		String observaciones = "";
		String referencias = "";
		try {
			IngresoSiniestroDTO ingresoSiniestroDTO = ingresoSiniestroDN.findById(UtileriasWeb.regresaBigDecimal(aplicarIngresosForm.getIdToIngresoSiniestro()) );
			if(aplicarIngresosForm.getObservaciones() != null || aplicarIngresosForm.getReferencias() != null){
				observaciones = aplicarIngresosForm.getObservaciones();
				referencias = aplicarIngresosForm.getReferencias();
			}
			contabilidadRemesaDN.contabilizarRemesa(ingresoSiniestroDTO,user,IngresoSiniestroDTO.CANCELACION_INGRESO,observaciones,referencias,null);
//			if(idRemesa != null){	
			ingresoSiniestroDTO.getEstatusFinanzas().setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO);
			ingresoSiniestroDN.actualizarIngreso(ingresoSiniestroDTO);
			UtileriasWeb.imprimeMensajeXML("1", "La remesa se cancelo de forma correcta" , response);
//			}else{
//				UtileriasWeb.imprimeMensajeXML("1", "La remesa esta en proceso de cancelacion" , response);
//			}
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.imprimeMensajeXML("0", "La remesa no se pudo cancelar" , response);
		} catch (SystemException e) {
			UtileriasWeb.imprimeMensajeXML("0", "La remesa no se pudo cancelar" , response);
		}
	}	
	
	private String jsonListaIngresosPorAplicar(List<IngresoSiniestroDTO> listaIngresosPorAplicar) throws SystemException{
		SoporteDanosDN soporteDanosDN  = SoporteDanosDN.getInstancia();
		MidasJsonBase jsonBase =  new MidasJsonBase();
		if (listaIngresosPorAplicar!=null && listaIngresosPorAplicar.size()>0){
			for (IngresoSiniestroDTO ingresoSiniestroDTO : listaIngresosPorAplicar) {
				MidasJsonRow row = new MidasJsonRow();
					row.setId(ingresoSiniestroDTO.getIdToIngresoSiniestro().toString());
					row.setDatos(
							UtileriasWeb.getFechaString(ingresoSiniestroDTO.getFechaCobro()),
							ingresoSiniestroDTO.getConceptoIngreso().getDescripcion(),
							ingresoSiniestroDTO.getEstatusFinanzas().getDescripcion(),
							ingresoSiniestroDTO.getReporteSiniestroDTO().getNombreAsegurado(),
							soporteDanosDN.getNumeroPoliza(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza()),
							ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte(),
							UtileriasWeb.formatoMoneda(ingresoSiniestroDTO.getMonto()),
							"/MidasWeb/img/moneyplus.gif^Aplicar^javascript:mostrarIngresoPorAplicar("	+ ingresoSiniestroDTO.getIdToIngresoSiniestro()+ ");^_self"
					);
				jsonBase.addRow(row);
			}	
		}
		return jsonBase.toString();
	}
	
	private void poblarAplicarIngresosForm(AplicarIngresosForm aplicarIngresosForm,IngresoSiniestroDTO ingresoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		
		//PolizaSoporteDanosDTO poliza = soporteDanosDN.getDetallePoliza(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza());
		PolizaSoporteDanosDTO poliza = soporteDanosDN.getDatosGeneralesPoliza(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroPoliza(), ingresoSiniestroDTO.getReporteSiniestroDTO().getFechaSiniestro());
		AutorizacionTecnicaDTO autorizacionTecnicaDTO =	autorizacionTecnicaDN.obtenerAutorizacionIngreso(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), ingresoSiniestroDTO.getIdToIngresoSiniestro());
		
		if(ingresoSiniestroDTO.getIdToIngresoSiniestro() != null){
			aplicarIngresosForm.setIdToIngresoSiniestro(ingresoSiniestroDTO.getIdToIngresoSiniestro().toString());
		}
		if(ingresoSiniestroDTO.getReporteSiniestroDTO() != null){
			aplicarIngresosForm.setIdToReporteSiniestro(ingresoSiniestroDTO.getReporteSiniestroDTO().getNumeroReporte());
		}
		if(ingresoSiniestroDTO.getDescripcion() != null){
			aplicarIngresosForm.setDescripcionIngreso(ingresoSiniestroDTO.getDescripcion());
		}
		if(poliza != null){
			aplicarIngresosForm.setProducto(String.valueOf(poliza.getCodigoProducto()));
			if(poliza.getIdAsegurado() != null){
				aplicarIngresosForm.setCveAsegurado(poliza.getIdAsegurado().toString());
			}
			
			if(poliza.getNombreAsegurado() != null){
				aplicarIngresosForm.setAsegurado(poliza.getNombreAsegurado());
			}
			if(poliza.getNumeroPoliza() != null){
				aplicarIngresosForm.setNumeroPoliza(poliza.getNumeroPoliza());
			}
			aplicarIngresosForm.setNumeroEndoso(String.valueOf(poliza.getNumeroUltimoEndoso()));
		}
		if(autorizacionTecnicaDTO != null){
			aplicarIngresosForm.setNumeroAutorizacionTecnica(autorizacionTecnicaDTO.getIdToAutorizacionTecnica().toString());
		}
		if(ingresoSiniestroDTO.getFechaCreacion() != null){
			aplicarIngresosForm.setFechaOcurrio(UtileriasWeb.getFechaString(ingresoSiniestroDTO.getFechaCreacion()));
		}
		if(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdAgente() != null){
			aplicarIngresosForm.setIdAgente(ingresoSiniestroDTO.getReporteSiniestroDTO().getIdAgente());
		}
		if(ingresoSiniestroDTO.getReporteSiniestroDTO().getNombreAgente() != null){
			aplicarIngresosForm.setAgente(ingresoSiniestroDTO.getReporteSiniestroDTO().getNombreAgente());
		}
		//TODO faltaa validar de donde sale esta informacion
		aplicarIngresosForm.setNumeroFactura("");
		aplicarIngresosForm.setFechaSolicitud("");
		aplicarIngresosForm.setBeneficiario("");
		aplicarIngresosForm.setFechaEstimadaPago("");
		
		if(ingresoSiniestroDTO.getConceptoIngreso() != null){
			aplicarIngresosForm.setConceptoIngreso(ingresoSiniestroDTO.getConceptoIngreso().getDescripcion());
		}
		if(ingresoSiniestroDTO.getMonto() != null){
			aplicarIngresosForm.setMontoIngreso(ingresoSiniestroDTO.getMonto());
		}
		if(ingresoSiniestroDTO.getMontoIVA() != null){
			aplicarIngresosForm.setMontoIVA(ingresoSiniestroDTO.getMontoIVA());
		}
		if(ingresoSiniestroDTO.getMontoIVARetencion() != null){
			aplicarIngresosForm.setMontoIVARetencion(ingresoSiniestroDTO.getMontoIVARetencion());
		}
		if(ingresoSiniestroDTO.getMontoISR() != null){
			aplicarIngresosForm.setMontoISR(ingresoSiniestroDTO.getMontoISR());
		}
		if(ingresoSiniestroDTO.getMontoISRRetencion() != null){
			aplicarIngresosForm.setMontoISRRetencion(ingresoSiniestroDTO.getMontoISRRetencion());
		}
		if(ingresoSiniestroDTO.getMontoOtros() != null){
			aplicarIngresosForm.setMontoOtros(ingresoSiniestroDTO.getMontoOtros());
		}
		if(ingresoSiniestroDTO.getEstatusFinanzas() != null){
			aplicarIngresosForm.setEstatusIngreso(ingresoSiniestroDTO.getEstatusFinanzas().getIdTcEstatusfinanzas().intValue());
		}
		if(aplicarIngresosForm.getConceptoIngresoFiltro() == null){
			aplicarIngresosForm.setConceptoIngresoFiltro(new BigDecimal("99"));
		}
		
	}
	
	@SuppressWarnings("unused")
	private boolean validaPendientes(List<ContabilidadRemesaDTO> listaContabilidadRemesas){
		boolean result = false;
		for(ContabilidadRemesaDTO contabilidadRemesaDTO:listaContabilidadRemesas){
			if(contabilidadRemesaDTO.getIdPolizaContable() == null){
				result = true;
				break;
			}
		}
		return result;
	}
}


