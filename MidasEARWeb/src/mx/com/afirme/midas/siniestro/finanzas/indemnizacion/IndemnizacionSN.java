package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.siniestro.cabina.EstatusSiniestroDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class IndemnizacionSN {
	
	private IndemnizacionFacadeRemote beanRemoto;

	public IndemnizacionSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(IndemnizacionFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public IndemnizacionDTO agregar(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.save(indemnizacionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public String borrar(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(indemnizacionDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
		return null;
	}
	
	public IndemnizacionDTO modificar(IndemnizacionDTO indemnizacionDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.update(indemnizacionDTO);
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<IndemnizacionDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IndemnizacionDTO> buscarPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public IndemnizacionDTO getIndemnizacionPorId(BigDecimal idIndemnizacion) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(idIndemnizacion);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public IndemnizacionDTO obtenIndemnizacionPorAutorizar(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenIndemnizacionPorAutorizar(idReporteSiniestro);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IndemnizacionDTO> buscaPagosParciales(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.buscaPagosParciales(idReporteSiniestro);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public boolean tienePagosParciales(BigDecimal idReporteSiniestro) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.tienePagosParciales(idReporteSiniestro);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	public List<IndemnizacionDTO> buscarPorReporteSiniestro(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos{
		LogDeMidasWeb.log("Buscando indemnizacion por reporte siniestro", Level.INFO,null);
		if (reporteSiniestroDTO!=null && reporteSiniestroDTO.getIdToReporteSiniestro()!=null)
			return beanRemoto.findByProperty("reporteSiniestroDTO.idToReporteSiniestro", reporteSiniestroDTO.getIdToReporteSiniestro());
		else return null;
	}
	
	public List<IndemnizacionDTO> obtenIndemnizacionPorEstatusNOCancelado(ReporteSiniestroDTO reporteSiniestroDTO)
		throws ExcepcionDeAccesoADatos{
		LogDeMidasWeb.log("Buscando indemnizacion finalizada para el reporte siniestro", Level.INFO,null);
		return beanRemoto.obtenIndemnizacionPorEstatusNOCancelado(reporteSiniestroDTO);
	}
	
	public IndemnizacionDTO obtenerUltimoPagoUnicoPago(BigDecimal idReporteSiniestro,Short statusUnicoPago,Short statusUltimoPago) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenerUltimoPagoUnicoPago(idReporteSiniestro,statusUnicoPago,statusUltimoPago);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IndemnizacionDTO> obtenerVariosPagos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenerVariosPagos();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IndemnizacionDTO> getIndemnizacionPorReporteYEstatus(BigDecimal idReporteSiniestro,Object... params) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getIndemnizacionPorReporteYEstatus(idReporteSiniestro,params);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<IndemnizacionDTO> getIndemnizacionPendientes(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getIndemnizacionPorReporteYEstatus(reporteSiniestroDTO.getIdToReporteSiniestro(), EstatusFinanzasDTO.ABIERTO, EstatusFinanzasDTO.AUTORIZADO, EstatusFinanzasDTO.ORDEN_PAGO, EstatusFinanzasDTO.POR_AUTORIZAR);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public  IndemnizacionDTO obtenerUltimoPago(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos{
		try{
				return beanRemoto.obtenerUltimoPago(idToReporteSiniestro);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public  IndemnizacionDTO obtenerUltimoPagoGenerado(BigDecimal idToReporteSiniestro) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.obtenerUltimoPagoGenerado(idToReporteSiniestro);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
