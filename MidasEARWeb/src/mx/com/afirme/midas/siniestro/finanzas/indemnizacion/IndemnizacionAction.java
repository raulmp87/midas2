package mx.com.afirme.midas.siniestro.finanzas.indemnizacion;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.reaseguro.movimiento.MovimientoReaseguroDN;
import mx.com.afirme.midas.siniestro.cabina.CoberturaRiesgoForm;
import mx.com.afirme.midas.siniestro.cabina.ReporteEstatusDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.EstatusFinanzasDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.SiniestroMovimientosDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleBean;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

public class IndemnizacionAction extends MidasMappingDispatchAction {
	
	public ActionForward seleccionarTipoIndemnizacion (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarAgregar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		
		try {
			//Se obtiene la lista y el total de riesgos
			List<RiesgoAfectadoDTO> riesgos = riesgoAfectadoDN.listarPorReporteSiniestro(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleBean> reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(riesgos);
			indemnizacionForm.setListaRiesgo(reservaDetalleList);
			indemnizacionForm.setTotalSumaAsegurada(new Double(reservaDN.realizaSumaAsegurada(reservaDetalleList)));
			//Se agrega la lista a session para usar los datos al guardar
			HttpSession sesion = request.getSession();
			sesion.setAttribute("listaRiesgosParaIndemnizaciones", indemnizacionForm.getListaRiesgo());
			//Se obtiene la lista y el total de reserva
			ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
//			reservas = siniestroMovimientosDN.removerAjustesDeMenos(reservas);			
			indemnizacionForm.setListaReserva(reservaDN.obtenReservaDetalleBeanParaReservaDetalle( reservas ) );
			indemnizacionForm.setTotalReserva(reservaDetalleDN.obtenerTotalReserva(reservas));
			//Se obtiene la moneda
//			indemnizacionForm.setTipoMoneda("MXP");
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			indemnizacionForm.setTipoMoneda(tipoMoneda);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward pendientePagosParciales(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			//Se obtienen los datos del reporte
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AGREGAR_PAGO_PARCIAL_INDEMNIZACION);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			indemnizacionForm.setMensaje("Se ha generado un pendiente para agregar pagos parciales");
			indemnizacionForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward agregar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = new IndemnizacionRiesgoCoberturaDN();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		
		try {
			//Se obtienen los datos del reporte
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Se genera la indemnizacion
			IndemnizacionDTO indemnizacionDTO = new IndemnizacionDTO();
			EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.POR_AUTORIZAR);
			indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
			indemnizacionDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
			indemnizacionDTO.setVariosPagos(Boolean.valueOf(indemnizacionForm.getTipoIndemnizacion()));
			indemnizacionDTO.setTotalPago(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getTotalIndemnizaciones()));
			indemnizacionDTO.setFechaDelPago(new Timestamp(UtileriasWeb.getFechaFromString(indemnizacionForm.getFechaPago()).getTime()));
			if(indemnizacionForm.getComentarios() != null){
            	String comentarios = indemnizacionForm.getComentarios();
    			if(comentarios.length() > 250){
    				indemnizacionDTO.setComentarios(comentarios.substring(0, 249) );
    			}else{
    				indemnizacionDTO.setComentarios(comentarios);
    			}	
            }
			indemnizacionDTO.setBeneficiario(indemnizacionForm.getBeneficiario());
			//Si no se habilito el IVA entonces se ponen ceros
			if (indemnizacionForm.getHabilitaIVA() == null){
				indemnizacionDTO.setPorcentajeIva(new Double(0));
				indemnizacionDTO.setMontoIva(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIva(indemnizacionForm.getPorcentajeIVA());
				indemnizacionDTO.setMontoIva(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoIVA()));
			}
			//Si no se habilito la retencion de IVA entonces se ponen ceros
			if (indemnizacionForm.getHabilitaIVARet() == null){
				indemnizacionDTO.setPorcentajeIvaRetencion(new Double(0));
				indemnizacionDTO.setMontoIvaRetencion(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIvaRetencion(indemnizacionForm.getPorcentajeIVARet());
				indemnizacionDTO.setMontoIvaRetencion(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoIVARet()));
			}
			//Si no se habilito el ISR entonces se ponen ceros
			if (indemnizacionForm.getHabilitaISR() == null){
				indemnizacionDTO.setPorcentajeIsr(new Double(0));
				indemnizacionDTO.setMontoIsr(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIsr(indemnizacionForm.getPorcentajeISR());
				indemnizacionDTO.setMontoIsr(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoISR()));
			}
			//Si no se habilito la retencion de ISR entonces se ponen ceros
			if (indemnizacionForm.getHabilitaISRRet() == null){
				indemnizacionDTO.setPorcentajeIsrRetencion(new Double(0));
				indemnizacionDTO.setMontoIsrRetencion(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIsrRetencion(indemnizacionForm.getPorcentajeISRRet());
				indemnizacionDTO.setMontoIsrRetencion(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoISRRet()));
			}
			//Si no se habilito otros entonces se ponen ceros
			if (indemnizacionForm.getHabilitaOtros() == null){
				indemnizacionDTO.setPorcentajeOtros(new Double(0));
				indemnizacionDTO.setMontoOtros(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeOtros(indemnizacionForm.getPorcentajeOtros());
				indemnizacionDTO.setMontoOtros(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoOtros()));
			}
			indemnizacionDTO.setUltimoPago(new Boolean(false));
			//Se guarda la indemnizacion
			indemnizacionDTO = indemnizacionDN.agregar(indemnizacionDTO);
			//Se genera el detalle de la indemnizacion
			HttpSession sesion = request.getSession();
			List<ReservaDetalleBean> listaRiesgosParaIndemnizaciones = (List<ReservaDetalleBean>)sesion.getAttribute("listaRiesgosParaIndemnizaciones");
			for(int i=0; i < listaRiesgosParaIndemnizaciones.size() ;i++){
				RiesgoAfectadoDTO riesgoAfectadoDTO = listaRiesgosParaIndemnizaciones.get(i).getRiesgoAfectadoDTO();
				IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO = new IndemnizacionRiesgoCoberturaDTO();
				IndemnizacionRiesgoCoberturaId id = new IndemnizacionRiesgoCoberturaId();
				//Se genera el id
				id.setIdToIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
				id.setNumeroInciso(riesgoAfectadoDTO.getId().getNumeroinciso());
				id.setIdToReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro());
				id.setIdToSeccion(riesgoAfectadoDTO.getId().getIdtoseccion());
				id.setNumeroSubinciso(riesgoAfectadoDTO.getId().getNumerosubinciso());
				id.setIdToCobertura(riesgoAfectadoDTO.getId().getIdtocobertura());
				id.setIdToRiesgo(riesgoAfectadoDTO.getId().getIdtoriesgo());
				id.setIdToPoliza(riesgoAfectadoDTO.getId().getIdtopoliza());
				indemnizacionRiesgoCoberturaDTO.setId(id);
				indemnizacionRiesgoCoberturaDTO.setRiesgoAfectadoDTO(riesgoAfectadoDTO);
				indemnizacionRiesgoCoberturaDTO.setIndemnizacionDTO(indemnizacionDTO);
				//Si no se habilitaron los deducibles entonces se ponen cero
				if (indemnizacionForm.getHabilitaDeducible() == null){
					indemnizacionRiesgoCoberturaDTO.setDeducible(new Double(0));
				}else{
					indemnizacionRiesgoCoberturaDTO.setDeducible(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getDeducibles()[i]));
				}
				//Si no se habilitaron los coaseguros entonces se ponen cero
				if (indemnizacionForm.getHabilitaCoaseguro() == null){
					indemnizacionRiesgoCoberturaDTO.setCoaseguro(new Double(0));
				}else{
					indemnizacionRiesgoCoberturaDTO.setCoaseguro(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getCoaseguros()[i]));
				}
				indemnizacionRiesgoCoberturaDTO.setMontoPago(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getIndemnizaciones()[i]));
				//Se agrega el detalle
				indemnizacionRiesgoCoberturaDN.agregar(indemnizacionRiesgoCoberturaDTO);
			}
			//Se cambia el estatus del reporte para generar su nuevo pendiente
//			ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
			//Se verifican los montos de reserva
			ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Si los montos de reserva no coinciden con los montos de indemnizacion entonces se genera un pendiente de modificar reserva
			List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
			if(reservaDetalleDN.reservaIgualAIndemnizaciones(reservas, indemnizacionForm.getPerdidas()) == false){
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_MODIFICAR_RESERVA);
			//Si si coinciden entonces se genera un pendiente de autorizar indemnizacion
			}else{
				reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AUTORIZAR_INDEMNIZACION);
			}
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			//Se muestra el mensaje de exito
			indemnizacionForm.setMensaje("Se agrego la indemnizaci\u00f3n con \u00e9xito");
			indemnizacionForm.setTipoMensaje("30");
			//Se elimina el listado de la sesion
			sesion.removeAttribute("listaRiesgosParaIndemnizaciones");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException pe){
			indemnizacionForm.setMensaje("Formato de fecha incorrecto.");
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarAutorizar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		
		try {
			//Se obtiene la lista y el total de riesgos
			List<RiesgoAfectadoDTO> riesgos = riesgoAfectadoDN.listarPorReporteSiniestro(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleBean> reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(riesgos);
			indemnizacionForm.setListaRiesgo(reservaDetalleList);
			indemnizacionForm.setTotalSumaAsegurada(new Double(reservaDN.realizaSumaAsegurada(reservaDetalleList)));
			
			//Se obtiene la lista y el total de reserva
			ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
//			reservas = siniestroMovimientosDN.removerAjustesDeMenos(reservas);
			indemnizacionForm.setListaReserva(reservaDN.obtenReservaDetalleBeanParaReservaDetalle( reservas ) );
			indemnizacionForm.setTotalReserva(reservaDetalleDN.obtenerTotalReserva(reservas));
			
			//Se obtienen la indemnizacion y su detalle
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.obtenIndemnizacionPorAutorizar(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<IndemnizacionRiesgoCoberturaDTO> indemnizaciones =indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
			indemnizacionForm.setListaDetalleIndemnizaciones(reservaDN.obtenReservaDetalleBeanParaIndemnizacionRiesgoCobertura(indemnizaciones));
			indemnizacionForm.setTotalPerdidas(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalPerdidas(indemnizaciones)));
			indemnizacionForm.setTotalDeducibles(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalDeducibles(indemnizaciones)));
			indemnizacionForm.setTotalCoaseguros(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalCoaseguros(indemnizaciones)));
			indemnizacionForm.setTotalIndemnizaciones(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalIndemnizaciones(indemnizaciones)));
			indemnizacionForm.setBeneficiario(indemnizacionDTO.getBeneficiario());
			indemnizacionForm.setComentarios(indemnizacionDTO.getComentarios());
			indemnizacionForm.setFechaPago(UtileriasWeb.getFechaString(indemnizacionDTO.getFechaDelPago()));
			indemnizacionForm.setIdIndemnizacion(indemnizacionDTO.getIdToIndemnizacion().toString());
			indemnizacionForm.setEstatusIndemnizacion(indemnizacionDTO.getEstatusFinanzasDTO().getDescripcion());
			//Se obtiene la moneda
//			indemnizacionForm.setTipoMoneda("MXP");
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			indemnizacionForm.setTipoMoneda(tipoMoneda);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward autorizar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		try {
			//Se obtienen los datos del reporte
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Se obtiene los datos de la indemnizacion
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
			//Se cambia el estatus a autorizado
			EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.AUTORIZADO);
			indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
			//Se autoriza la indemnizacion
			indemnizacionDN.modificar(indemnizacionDTO);
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_GENERAR_CONVENIO_FINIQUITO);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			//Se muestra el mensaje de exito
			indemnizacionForm.setMensaje("Se autoriz\u00f3 la indemnizaci\u00f3n con \u00e9xito");
			indemnizacionForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward cancelar(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		try {
			//Se obtienen los datos del reporte
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Se obtiene los datos de la indemnizacion
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
			//Se cambia el estatus a cancelado
			EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
			indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
			//Se autoriza la indemnizacion
			indemnizacionDN.modificar(indemnizacionDTO);
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_EVALUACION_INFORME_FINAL);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			//Se muestra el mensaje de exito
			indemnizacionForm.setMensaje("Se cancel\u00f3 la indemnizaci\u00f3n con \u00e9xito");
			indemnizacionForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarAgregarPagoParcial (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		
		try {
			//Se obtiene la lista y el total de riesgos
			List<RiesgoAfectadoDTO> riesgos = riesgoAfectadoDN.listarPorReporteSiniestro(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleBean> reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(riesgos);
			indemnizacionForm.setListaRiesgo(reservaDetalleList);
			indemnizacionForm.setTotalSumaAsegurada(new Double(reservaDN.realizaSumaAsegurada(reservaDetalleList)));
			
			//Se agrega la lista a session para usar los datos al guardar
			HttpSession sesion = request.getSession();
			sesion.setAttribute("listaRiesgosParaIndemnizaciones", indemnizacionForm.getListaRiesgo());
			//Se obtiene la lista y el total de reserva
			ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
//			reservas = siniestroMovimientosDN.removerAjustesDeMenos(reservas);			
			indemnizacionForm.setListaReserva(reservaDN.obtenReservaDetalleBeanParaReservaDetalle( reservas ) );
			indemnizacionForm.setTotalReserva(reservaDetalleDN.obtenerTotalReserva(reservas));
			//Se obtiene la moneda
//			indemnizacionForm.setTipoMoneda("MXP");
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			indemnizacionForm.setTipoMoneda(tipoMoneda);
			indemnizacionForm.setTipoIndemnizacion("true");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward agregarPagoParcial(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = new IndemnizacionRiesgoCoberturaDN();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		Date fechaActual = Calendar.getInstance().getTime();
		
		try {
			//Se obtienen los datos del reporte
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Se genera la indemnizacion
			IndemnizacionDTO indemnizacionDTO = new IndemnizacionDTO();
			EstatusFinanzasDTO estatusFinanzasDTO = new EstatusFinanzasDTO();
			estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.ABIERTO);
			indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
			indemnizacionDTO.setReporteSiniestroDTO(reporteSiniestroDTO);
			indemnizacionDTO.setVariosPagos(Boolean.valueOf(indemnizacionForm.getTipoIndemnizacion()));
			indemnizacionDTO.setTotalPago(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getTotalIndemnizaciones()));
			indemnizacionDTO.setFechaDelPago(new Timestamp(UtileriasWeb.getFechaFromString(indemnizacionForm.getFechaPago()).getTime()));
			if(indemnizacionForm.getComentarios() != null){
            	String comentarios = indemnizacionForm.getComentarios();
    			if(comentarios.length() > 250){
    				indemnizacionDTO.setComentarios(comentarios.substring(0, 249) );
    			}else{
    				indemnizacionDTO.setComentarios(comentarios);
    			}	
            }
			indemnizacionDTO.setBeneficiario(indemnizacionForm.getBeneficiario());
			indemnizacionDTO.setFechacreacion(fechaActual);
			indemnizacionDTO.setIdtcusuariocreacion(new BigDecimal(usuario.getId()));
			//Si no se habilito el IVA entonces se ponen ceros
			if (indemnizacionForm.getHabilitaIVA() == null){
				indemnizacionDTO.setPorcentajeIva(new Double(0));
				indemnizacionDTO.setMontoIva(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIva(indemnizacionForm.getPorcentajeIVA());
				indemnizacionDTO.setMontoIva(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoIVA()));
			}
			//Si no se habilito la retencion de IVA entonces se ponen ceros
			if (indemnizacionForm.getHabilitaIVARet() == null){
				indemnizacionDTO.setPorcentajeIvaRetencion(new Double(0));
				indemnizacionDTO.setMontoIvaRetencion(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIvaRetencion(indemnizacionForm.getPorcentajeIVARet());
				indemnizacionDTO.setMontoIvaRetencion(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoIVARet()));
			}
			//Si no se habilito el ISR entonces se ponen ceros
			if (indemnizacionForm.getHabilitaISR() == null){
				indemnizacionDTO.setPorcentajeIsr(new Double(0));
				indemnizacionDTO.setMontoIsr(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIsr(indemnizacionForm.getPorcentajeISR());
				indemnizacionDTO.setMontoIsr(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoISR()));
			}
			//Si no se habilito la retencion de ISR entonces se ponen ceros
			if (indemnizacionForm.getHabilitaISRRet() == null){
				indemnizacionDTO.setPorcentajeIsrRetencion(new Double(0));
				indemnizacionDTO.setMontoIsrRetencion(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIsrRetencion(indemnizacionForm.getPorcentajeISRRet());
				indemnizacionDTO.setMontoIsrRetencion(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoISRRet()));
			}
			//Si no se habilito otros entonces se ponen ceros
			if (indemnizacionForm.getHabilitaOtros() == null){
				indemnizacionDTO.setPorcentajeOtros(new Double(0));
				indemnizacionDTO.setMontoOtros(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeOtros(indemnizacionForm.getPorcentajeOtros());
				indemnizacionDTO.setMontoOtros(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoOtros()));
			}
			if(indemnizacionForm.getUltimoPago()!=null && indemnizacionForm.getUltimoPago().equals("true"))
				indemnizacionDTO.setUltimoPago(new Boolean(true));
			else
				indemnizacionDTO.setUltimoPago(new Boolean(false));
			//Se guarda la indemnizacion
//indemnizacionDTO = indemnizacionDN.agregar(indemnizacionDTO);
			//Se genera el detalle de la indemnizacion
			HttpSession sesion = request.getSession();
			List<ReservaDetalleBean> listaRiesgosParaIndemnizaciones = (List<ReservaDetalleBean>)sesion.getAttribute("listaRiesgosParaIndemnizaciones");
			List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion = new ArrayList<IndemnizacionRiesgoCoberturaDTO>();
			for(int i=0; i < listaRiesgosParaIndemnizaciones.size() ;i++){
				RiesgoAfectadoDTO riesgoAfectadoDTO = listaRiesgosParaIndemnizaciones.get(i).getRiesgoAfectadoDTO();
				IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO = new IndemnizacionRiesgoCoberturaDTO();
				IndemnizacionRiesgoCoberturaId id = new IndemnizacionRiesgoCoberturaId();
				//Se genera el id
//id.setIdToIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
				id.setNumeroInciso(riesgoAfectadoDTO.getId().getNumeroinciso());
				id.setIdToReporteSiniestro(reporteSiniestroDTO.getIdToReporteSiniestro());
				id.setIdToSeccion(riesgoAfectadoDTO.getId().getIdtoseccion());
				id.setNumeroSubinciso(riesgoAfectadoDTO.getId().getNumerosubinciso());
				id.setIdToCobertura(riesgoAfectadoDTO.getId().getIdtocobertura());
				id.setIdToRiesgo(riesgoAfectadoDTO.getId().getIdtoriesgo());
				id.setIdToPoliza(riesgoAfectadoDTO.getId().getIdtopoliza());
				indemnizacionRiesgoCoberturaDTO.setId(id);
				indemnizacionRiesgoCoberturaDTO.setRiesgoAfectadoDTO(riesgoAfectadoDTO);
				indemnizacionRiesgoCoberturaDTO.setIndemnizacionDTO(indemnizacionDTO);
				//Si no se habilitaron los deducibles entonces se ponen cero
				if (indemnizacionForm.getHabilitaDeducible() == null){
					indemnizacionRiesgoCoberturaDTO.setDeducible(new Double(0));
				}else{
					indemnizacionRiesgoCoberturaDTO.setDeducible(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getDeducibles()[i]));
				}
				//Si no se habilitaron los coaseguros entonces se ponen cero
				if (indemnizacionForm.getHabilitaCoaseguro() == null){
					indemnizacionRiesgoCoberturaDTO.setCoaseguro(new Double(0));
				}else{
					indemnizacionRiesgoCoberturaDTO.setCoaseguro(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getCoaseguros()[i]));
				}
				indemnizacionRiesgoCoberturaDTO.setMontoPago(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getIndemnizaciones()[i]));
				listaDetalleIndemnizacion.add(indemnizacionRiesgoCoberturaDTO);
				//Se agrega el detalle
//indemnizacionRiesgoCoberturaDN.agregar(indemnizacionRiesgoCoberturaDTO);
			}
			//Se verifica si es el ultimo pago
			if(indemnizacionDTO.getUltimoPago() == true){
				ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
				//Si la suma de indemnizaciones parciales no es igual a cada uno de los montos de reserva entonces se manda a modificar la reserva				
				List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
//				if(reservaDetalleDN.reservaIgualAIndemnizacionesVariosPagos(reservas, indemnizacionDN.sumaDetalleMontosPagosParciales(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro())),indemnizacionForm.getPerdidas()) == false){
				if(reservaDetalleDN.reservaIgualAIndemnizacionesVariosPagos(reservas, indemnizacionRiesgoCoberturaDN.sumaPagosParcialesAgregar(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro())),indemnizacionForm.getPerdidas()) == false){
					ReservaDTO reservaGeneradaDTO = siniestroMovimientosDN.completaUltimoPagoIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion, indemnizacionForm, reporteSiniestroDTO, new BigDecimal(usuario.getId()),usuario.getNombreUsuario());
					if(reservaGeneradaDTO != null){
						indemnizacionForm.setMensaje("Se agrego el pago parcial con \u00e9xito");
						indemnizacionForm.setTipoMensaje("30");
						sesion.removeAttribute("listaRiesgosParaIndemnizaciones");
					}else{
						indemnizacionForm.setMensaje("Ocurrio un error al guardar la Indemnizacion");
						indemnizacionForm.setTipoMensaje("10");
					}
				//Si si es igual entonces se manda a generar la autorizacion tecnica
				}else{
					this.guardaPagoParcial(indemnizacionDTO, listaDetalleIndemnizacion);
//					ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
//					reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_GENERAR_CONVENIO_FINIQUITO);
//					reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
					indemnizacionForm.setMensaje("Se agrego el pago parcial con \u00e9xito");
					indemnizacionForm.setTipoMensaje("30");
					sesion.removeAttribute("listaRiesgosParaIndemnizaciones");
				}
			}else{
				//Si no es el ultimo pago entonces se agrega un pendiente de proceso
				this.guardaPagoParcial(indemnizacionDTO, listaDetalleIndemnizacion);
				
				indemnizacionForm.setMensaje("Se agrego el pago parcial con \u00e9xito");
				indemnizacionForm.setTipoMensaje("30");
				sesion.removeAttribute("listaRiesgosParaIndemnizaciones");
			}
			//Se cambia el estatus del reporte para generar su nuevo pendiente
			ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
			reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_PROCESO_INDEMNIZACION);
			reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
			reporteSiniestroDTO.setFechaModificacion(new Date());
			reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
			//Se actualizan los datos del reporte
			reporteSiniestroDN.actualizar(reporteSiniestroDTO);
			//Se elimina el listado de la sesion
			
		} catch (SystemException e) {
			indemnizacionForm.setMensaje("Ocurrio un error al guardar la Indemnizacion");
			indemnizacionForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			indemnizacionForm.setMensaje("Ocurrio un error al guardar la Indemnizacion");
			indemnizacionForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException pe){
			indemnizacionForm.setMensaje("Formato de fecha incorrecto.");
			indemnizacionForm.setTipoMensaje("10");
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listar (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		try {
			//Se obtiene la lista de Pagos Parciales
			indemnizacionForm.setListaIndemnizaciones(indemnizacionDN.listaPagosParciales(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()),indemnizacionForm));
			//Se obtiene la moneda
//			indemnizacionForm.setTipoMoneda("MXP");
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			indemnizacionForm.setTipoMoneda(tipoMoneda);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward listarDetalle (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		try {
			//Se obtienen la indemnizacion y su detalle
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
			List<IndemnizacionRiesgoCoberturaDTO> indemnizaciones =indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
			indemnizacionForm.setListaDetalleIndemnizaciones(reservaDN.obtenReservaDetalleBeanParaIndemnizacionRiesgoCobertura(indemnizaciones));
			indemnizacionForm.setTotalDeducibles(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalDeducibles(indemnizaciones)));
			indemnizacionForm.setTotalCoaseguros(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalCoaseguros(indemnizaciones)));
			indemnizacionForm.setTotalIndemnizaciones(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalIndemnizaciones(indemnizaciones)));
			indemnizacionForm.setComentarios(indemnizacionDTO.getComentarios());
			indemnizacionForm.setIdIndemnizacion(indemnizacionDTO.getIdToIndemnizacion().toString());
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward cancelarPagoParcial(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			//Se obtiene los datos de la indemnizacion
			BigDecimal idToIndemnizacion = new BigDecimal(indemnizacionForm.getIdIndemnizacion());
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(idToIndemnizacion);
			
			if(indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas() != EstatusFinanzasDTO.PAGADO){
				
				if(siniestroMovimientosDN.cancelaMovimientoCompleto(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro(), idToIndemnizacion, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION)){
				
					//Se cambia el estatus 
					EstatusFinanzasDTO estatusFinanzasDTO =indemnizacionDTO.getEstatusFinanzasDTO();
					estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
					indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
					//Se modifica la indemnizacion
//indemnizacionDN.modificar(indemnizacionDTO);
					//Si ya no quedan pagos parciales
					ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
					if(indemnizacionDN.tienePagosParciales(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro())==false){
						indemnizacionDN.modificar(indemnizacionDTO);
						
						//Se cambia el estatus del reporte para generar su nuevo pendiente
						ReporteEstatusDTO reporteEstatusDTO = reporteSiniestroDTO.getReporteEstatus();
						reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.PENDIENTE_AGREGAR_PAGO_PARCIAL_INDEMNIZACION);
						reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
						reporteSiniestroDTO.setFechaModificacion(new Date());
						reporteSiniestroDTO.setIdTcUsuarioModifica(UtileriasWeb.regresaBigDecimal(usuario.getId().toString()));
						//Se actualizan los datos del reporte
						reporteSiniestroDN.actualizar(reporteSiniestroDTO);
					}else{
						ReservaDTO reservaDTO = reservaDN.listarReservaActual(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
						//Si la suma de indemnizaciones parciales no es igual a cada uno de los montos de reserva entonces se realiza la reserva en automatico				
						List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
						IndemnizacionDTO ultimoPagoExiste = indemnizacionDN.obtenerUltimoPagoGenerado(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
						if(ultimoPagoExiste != null && reservaDetalleDN.reservaIgualAIndemnizacionesEliminar(reservas, indemnizacionRiesgoCoberturaDN.sumaPagosParcialesModificar(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()), UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()))) == false){
						
							siniestroMovimientosDN.completaCancelarIndemnizacion(indemnizacionDTO,reporteSiniestroDTO, new BigDecimal(usuario.getId()),usuario.getNombreUsuario());
						}else{
							indemnizacionDN.modificar(indemnizacionDTO);
						}
						
					}
					//Se muestra el mensaje de exito
					indemnizacionForm.setMensaje("Se cancel\u00f3 el pago parcial de la indemnizaci\u00f3n con \u00e9xito");
					indemnizacionForm.setTipoMensaje("30");
				}else{
					indemnizacionForm.setMensaje("No se pudo cancelar el pago parcial de la indemnizaci\u00f3n <br> debido a que ocurrio un error al cancelar la AT o la Orden de pago ");
					indemnizacionForm.setTipoMensaje("10");
				}
			}else{
				indemnizacionForm.setMensaje("No se pudo cancelar el pago parcial de la indemnizaci\u00f3n <br> debido a que este ya fue pagado");
				indemnizacionForm.setTipoMensaje("10");
			}
			
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
//	public ActionForward listarEliminar (ActionMapping mapping, ActionForm form,
//			HttpServletRequest request, HttpServletResponse response){
//		String reglaNavegacion = Sistema.EXITOSO;
//		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
//		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
//		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
//		try {
//			//Se obtiene la lista de Pagos Parciales
//			indemnizacionForm.setListaIndemnizaciones(indemnizacionDN.listaPagosParciales(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro())));
//			//Se obtiene la moneda
////			indemnizacionForm.setTipoMoneda("MXP");
//			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
//			indemnizacionForm.setTipoMoneda(tipoMoneda);
//		} catch (SystemException e) {
//			reglaNavegacion = Sistema.NO_DISPONIBLE;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		} catch (ExcepcionDeAccesoADatos e) {
//			reglaNavegacion = Sistema.NO_EXITOSO;
//			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
//		}
//		return mapping.findForward(reglaNavegacion);
//	}
	
	public ActionForward eliminarPagoParcial(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		AutorizacionTecnicaDN autorizacionTecnicaDN = AutorizacionTecnicaDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = new IndemnizacionRiesgoCoberturaDN();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		try {
			//Se obtiene los datos de la indemnizacion
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
			//Se elimina la indemnizacion
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
			IndemnizacionDTO ultimoPagoExiste = indemnizacionDN.obtenerUltimoPago(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<AutorizacionTecnicaDTO>  listaATs = autorizacionTecnicaDN.findByProperty("indemnizacionDTO.idToIndemnizacion", indemnizacionDTO.getIdToIndemnizacion());
			if(ultimoPagoExiste != null){
				if(listaATs != null && listaATs.size() > 0){
					siniestroMovimientosDN.completaCancelarIndemnizacion(indemnizacionDTO,reporteSiniestroDTO, new BigDecimal(usuario.getId()),usuario.getNombreUsuario());
				}else{
					siniestroMovimientosDN.completaEliminarIndemnizacion(indemnizacionDTO,reporteSiniestroDTO, new BigDecimal(usuario.getId()),usuario.getNombreUsuario());
				}
			}else{
				if(listaATs != null && listaATs.size() > 0){
					EstatusFinanzasDTO estatusFinanzasDTO =indemnizacionDTO.getEstatusFinanzasDTO();
					estatusFinanzasDTO.setIdTcEstatusfinanzas(EstatusFinanzasDTO.CANCELADO);
					indemnizacionDTO.setEstatusFinanzasDTO(estatusFinanzasDTO);
					indemnizacionDN.modificar(indemnizacionDTO);
				}else{
					indemnizacionRiesgoCoberturaDN.borrarDetalle(indemnizacionDTO);
					indemnizacionDN.borrar(indemnizacionDTO);
				}
				
			}
			
			indemnizacionForm.setMensaje("Se elimin\u00f3 el pago parcial de la indemnizaci\u00f3n con \u00e9xito");
			indemnizacionForm.setTipoMensaje("30");
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward mostrarModificarPagoParcial (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String reglaNavegacion = Sistema.EXITOSO;
		request.getSession().setAttribute("banderaRefresh", "true");
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		
		try {
			//Se obtiene la lista y el total de riesgos
			List<RiesgoAfectadoDTO> riesgos = riesgoAfectadoDN.listarPorReporteSiniestro(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleBean> reservaDetalleList = reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(riesgos);
			indemnizacionForm.setListaRiesgo(reservaDetalleList);
			indemnizacionForm.setTotalSumaAsegurada(new Double(reservaDN.realizaSumaAsegurada(reservaDetalleList)));
			
			//Se obtiene la lista y el total de reserva
			ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
//			reservas = siniestroMovimientosDN.removerAjustesDeMenos(reservas);
			indemnizacionForm.setListaReserva(reservaDN.obtenReservaDetalleBeanParaReservaDetalle( reservas ) );
			indemnizacionForm.setTotalReserva(reservaDetalleDN.obtenerTotalReserva(reservas));
			//Se obtienen la indemnizacion y su detalle
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
			List<IndemnizacionRiesgoCoberturaDTO> indemnizaciones = indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
			indemnizacionForm.setListaDetalleIndemnizaciones(reservaDN.obtenReservaDetalleBeanParaIndemnizacionRiesgoCobertura(indemnizaciones));
			//Se llena los datos de la forma para la tabla de perdidas
			indemnizacionForm.setTotalPerdidas(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalPerdidas(indemnizaciones)));
			indemnizacionForm.setTotalDeducibles(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalDeducibles(indemnizaciones)));
			
			
			//Si el total de deducibles es mayor que cero entonces se habilita la captura de deducibles
			if(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getTotalDeducibles()).doubleValue() > 0)
				indemnizacionForm.setHabilitaDeducible("1");
			indemnizacionForm.setTotalCoaseguros(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalCoaseguros(indemnizaciones)));
			//Si el total de coaseguros es mayor que cero entonces se habilita la captura de coaseguros
			if(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getTotalCoaseguros()).doubleValue() > 0)
				indemnizacionForm.setHabilitaCoaseguro("1");
			indemnizacionForm.setTotalIndemnizaciones(UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDN.obtenerTotalIndemnizaciones(indemnizaciones)));
			String[] perdidas = new String[indemnizacionForm.getListaDetalleIndemnizaciones().size()];
			String[] deducibles = new String[indemnizacionForm.getListaDetalleIndemnizaciones().size()]; 
			String[] coaseguros = new String[indemnizacionForm.getListaDetalleIndemnizaciones().size()]; 
			String[] indemnizacionesDouble = new String[indemnizacionForm.getListaDetalleIndemnizaciones().size()]; 
			for(int i=0; i < indemnizacionForm.getListaDetalleIndemnizaciones().size(); i++){
				IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO = (IndemnizacionRiesgoCoberturaDTO)indemnizaciones.get(i);
				perdidas[i] = UtileriasWeb.formatoMoneda(new Double(indemnizacionRiesgoCoberturaDTO.getDeducible().doubleValue() +
											indemnizacionRiesgoCoberturaDTO.getCoaseguro().doubleValue() +
											indemnizacionRiesgoCoberturaDTO.getMontoPago().doubleValue()));
				deducibles[i] = UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDTO.getDeducible());
				coaseguros[i] = UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDTO.getCoaseguro());
				indemnizacionesDouble[i] = UtileriasWeb.formatoMoneda(indemnizacionRiesgoCoberturaDTO.getMontoPago());
			}
			indemnizacionForm.setPerdidas(perdidas);
			indemnizacionForm.setDeducibles(deducibles);
			indemnizacionForm.setCoaseguros(coaseguros);
			indemnizacionForm.setIndemnizaciones(indemnizacionesDouble);
			//Se llena los datos de la forma para los impuestos
			indemnizacionForm.setPorcentajeIVA(indemnizacionDTO.getPorcentajeIva());
			//Si el porcentaje de IVA es mayor que cero entonces se habilitan los campos
			if(indemnizacionForm.getPorcentajeIVA().doubleValue() > 0)
				indemnizacionForm.setHabilitaIVA("1");
			indemnizacionForm.setMontoIVA(UtileriasWeb.formatoMoneda(indemnizacionDTO.getMontoIva()));
			indemnizacionForm.setPorcentajeIVARet(indemnizacionDTO.getPorcentajeIvaRetencion());
			//Si el porcentaje de IVA Retenido es mayor que cero entonces se habilitan los campos
			if(indemnizacionForm.getPorcentajeIVARet().doubleValue() > 0)
				indemnizacionForm.setHabilitaIVARet("1");
			indemnizacionForm.setMontoIVARet(UtileriasWeb.formatoMoneda(indemnizacionDTO.getMontoIvaRetencion()));
			indemnizacionForm.setPorcentajeISR(indemnizacionDTO.getPorcentajeIsr());
			//Si el porcentaje de ISR es mayor que cero entonces se habilitan los campos
			if(indemnizacionForm.getPorcentajeISR().doubleValue() > 0)
				indemnizacionForm.setHabilitaISR("1");
			indemnizacionForm.setMontoISR(UtileriasWeb.formatoMoneda(indemnizacionDTO.getMontoIsr()));
			indemnizacionForm.setPorcentajeISRRet(indemnizacionDTO.getPorcentajeIsrRetencion());
			//Si el porcentaje de ISR Retenido es mayor que cero entonces se habilitan los campos
			if(indemnizacionForm.getPorcentajeISRRet().doubleValue() > 0)
				indemnizacionForm.setHabilitaISRRet("1");
			indemnizacionForm.setMontoISRRet(UtileriasWeb.formatoMoneda(indemnizacionDTO.getMontoIsrRetencion()));
			indemnizacionForm.setPorcentajeOtros(indemnizacionDTO.getPorcentajeOtros());
			//Si el porcentaje de otros es mayor que cero entonces se habilitan los campos
			if(indemnizacionForm.getPorcentajeOtros().doubleValue() > 0)
				indemnizacionForm.setHabilitaOtros("1");
			indemnizacionForm.setMontoOtros(UtileriasWeb.formatoMoneda(indemnizacionDTO.getMontoOtros()));
			//Se llena los datos de la forma para los datos generales
			indemnizacionForm.setBeneficiario(indemnizacionDTO.getBeneficiario());
			indemnizacionForm.setComentarios(indemnizacionDTO.getComentarios());
			indemnizacionForm.setFechaPago(UtileriasWeb.getFechaString(indemnizacionDTO.getFechaDelPago()));
			indemnizacionForm.setIdIndemnizacion(indemnizacionDTO.getIdToIndemnizacion().toString());
			if(indemnizacionDTO.getUltimoPago().booleanValue()){
				indemnizacionForm.setUltimoPago("true");
			}
			IndemnizacionDTO datosUltimoPago = indemnizacionDN.obtenerUltimoPago(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			
			
			if( datosUltimoPago == null || (datosUltimoPago != null && indemnizacionDTO.getUltimoPago().booleanValue()) ){
				indemnizacionForm.setHabilitaUltimoPago("true");
			}
			//Se obtiene la moneda
//			indemnizacionForm.setTipoMoneda("MXP");
			String tipoMoneda = reporteSiniestroDN.tipoMonedaPoliza(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			indemnizacionForm.setTipoMoneda(tipoMoneda);
			indemnizacionForm.setTipoIndemnizacion("true");
			
			boolean modoLectura = this.isIndemnizacionSoloLectura(indemnizacionDTO);
			request.setAttribute("modoLectura", modoLectura);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public ActionForward modificarPagoParcial(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
		//ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
		SiniestroMovimientosDN siniestroMovimientosDN = SiniestroMovimientosDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = new IndemnizacionRiesgoCoberturaDN();
		MovimientoReaseguroDN movimientoReaseguroDN = MovimientoReaseguroDN.getInstancia();
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		Date fechaActual = Calendar.getInstance().getTime();
		
		try {
			//Se obtienen los datos del reporte
			//ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			//Se obtiene la indemnizacion y se actualizan sus datos
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.getIndemnizacionPorId(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion()));
			indemnizacionDTO.setTotalPago(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getTotalIndemnizaciones()));
			indemnizacionDTO.setFechaDelPago(new Timestamp(UtileriasWeb.getFechaFromString(indemnizacionForm.getFechaPago()).getTime()));
			if(indemnizacionForm.getComentarios() != null){
            	String comentarios = indemnizacionForm.getComentarios();
    			if(comentarios.length() > 250){
    				indemnizacionDTO.setComentarios(comentarios.substring(0, 249) );
    			}else{
    				indemnizacionDTO.setComentarios(comentarios);
    			}	
            }
			indemnizacionDTO.setBeneficiario(indemnizacionForm.getBeneficiario());
			indemnizacionDTO.setFechamodificacion(fechaActual);
			indemnizacionDTO.setIdtcusuariomodificacion(new BigDecimal(usuario.getId()));
			//Si no se habilito el IVA entonces se ponen ceros
			if (indemnizacionForm.getHabilitaIVA() == null){
				indemnizacionDTO.setPorcentajeIva(new Double(0));
				indemnizacionDTO.setMontoIva(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIva(indemnizacionForm.getPorcentajeIVA());
				indemnizacionDTO.setMontoIva(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoIVA()));
			}
			//Si no se habilito la retencion de IVA entonces se ponen ceros
			if (indemnizacionForm.getHabilitaIVARet() == null){
				indemnizacionDTO.setPorcentajeIvaRetencion(new Double(0));
				indemnizacionDTO.setMontoIvaRetencion(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIvaRetencion(indemnizacionForm.getPorcentajeIVARet());
				indemnizacionDTO.setMontoIvaRetencion(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoIVARet()));
			}
			//Si no se habilito el ISR entonces se ponen ceros
			if (indemnizacionForm.getHabilitaISR() == null){
				indemnizacionDTO.setPorcentajeIsr(new Double(0));
				indemnizacionDTO.setMontoIsr(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIsr(indemnizacionForm.getPorcentajeISR());
				indemnizacionDTO.setMontoIsr(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoISR()));
			}
			//Si no se habilito la retencion de ISR entonces se ponen ceros
			if (indemnizacionForm.getHabilitaISRRet() == null){
				indemnizacionDTO.setPorcentajeIsrRetencion(new Double(0));
				indemnizacionDTO.setMontoIsrRetencion(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeIsrRetencion(indemnizacionForm.getPorcentajeISRRet());
				indemnizacionDTO.setMontoIsrRetencion(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoISRRet()));
			}
			//Si no se habilito otros entonces se ponen ceros
			if (indemnizacionForm.getHabilitaOtros() == null){
				indemnizacionDTO.setPorcentajeOtros(new Double(0));
				indemnizacionDTO.setMontoOtros(new Double(0));
			}else{
				indemnizacionDTO.setPorcentajeOtros(indemnizacionForm.getPorcentajeOtros());
				indemnizacionDTO.setMontoOtros(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getMontoOtros()));
			}
			if(indemnizacionForm.getUltimoPago()!=null && indemnizacionForm.getUltimoPago().equals("true"))
				indemnizacionDTO.setUltimoPago(new Boolean(true));
			else
				indemnizacionDTO.setUltimoPago(new Boolean(false));
			//Se actualiza la indemnizacion
//indemnizacionDTO = indemnizacionDN.modificar(indemnizacionDTO);
			//Se actualiza el detalle de la indemnizacion
			List<IndemnizacionRiesgoCoberturaDTO> listaDetalle = indemnizacionRiesgoCoberturaDN.buscarPorPropiedad("indemnizacionDTO", indemnizacionDTO);
			List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion = new ArrayList<IndemnizacionRiesgoCoberturaDTO>();
			for(int i=0; i < listaDetalle.size() ;i++){
				IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO = listaDetalle.get(i);
				//Si no se habilitaron los deducibles entonces se ponen cero
				if (indemnizacionForm.getHabilitaDeducible() == null && indemnizacionForm.getDeducibles() == null){
					indemnizacionRiesgoCoberturaDTO.setDeducible(new Double(0));
				}else{
					indemnizacionRiesgoCoberturaDTO.setDeducible(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getDeducibles()[i]));
				}
				//Si no se habilitaron los coaseguros entonces se ponen cero
				if (indemnizacionForm.getHabilitaCoaseguro() == null && indemnizacionForm.getCoaseguros() == null){
					indemnizacionRiesgoCoberturaDTO.setCoaseguro(new Double(0));
				}else{
					indemnizacionRiesgoCoberturaDTO.setCoaseguro(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getCoaseguros()[i]));
				}
				indemnizacionRiesgoCoberturaDTO.setMontoPago(UtileriasWeb.parseaformatoMoneda(indemnizacionForm.getIndemnizaciones()[i]));
				listaDetalleIndemnizacion.add(indemnizacionRiesgoCoberturaDTO);
				//Se actualiza el detalle
//indemnizacionRiesgoCoberturaDN.modificar(indemnizacionRiesgoCoberturaDTO);
			}
			IndemnizacionDTO ultimoPagoExiste = indemnizacionDN.obtenerUltimoPago(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
			if(indemnizacionDTO.getUltimoPago() == true || ultimoPagoExiste != null){
				ReservaDTO reservaDTO = reservaDN.listarReservaActual(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()));
				//Si la suma de indemnizaciones parciales no es igual a cada uno de los montos de reserva entonces se manda a modificar la reserva				
				List<ReservaDetalleDTO> reservas = reservaDetalleDN.listarDetalleReserva(reservaDTO.getIdtoreservaestimada());
//				if(reservaDetalleDN.reservaIgualAIndemnizacionesVariosPagos(reservas, indemnizacionDN.sumaDetalleMontosPagosParciales(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro())),indemnizacionForm.getPerdidas()) == false){
				ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(indemnizacionDTO.getReporteSiniestroDTO().getIdToReporteSiniestro());
				if(reservaDetalleDN.reservaIgualAIndemnizacionesVariosPagos(reservas, indemnizacionRiesgoCoberturaDN.sumaPagosParcialesModificar(UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdReporteSiniestro()), UtileriasWeb.regresaBigDecimal(indemnizacionForm.getIdIndemnizacion())),indemnizacionForm.getPerdidas()) == false){
					siniestroMovimientosDN.completaModificarUltimoPagoIndemnizacion(indemnizacionDTO, listaDetalleIndemnizacion, indemnizacionForm, reporteSiniestroDTO, new BigDecimal(usuario.getId()),usuario.getNombreUsuario());
					indemnizacionForm.setMensaje("El pago parcial fue actualizado con \u00e9xito");
					indemnizacionForm.setTipoMensaje("30");
				}else{
					this.modificarPagoParcial(indemnizacionDTO, listaDetalleIndemnizacion);
					indemnizacionForm.setMensaje("El pago parcial fue actualizado con \u00e9xito");
					indemnizacionForm.setTipoMensaje("30");
				}
				if(siniestroMovimientosDN.validaPagosDeIndemnizacion(reporteSiniestroDTO)){
					ReporteEstatusDTO reporteEstatusDTO = new ReporteEstatusDTO();
					reporteEstatusDTO.setIdTcReporteEstatus(ReporteSiniestroDTO.REPORTE_TERMINADO);
					reporteSiniestroDTO.setReporteEstatus(reporteEstatusDTO);
					reporteSiniestroDTO = reporteSiniestroDN.actualizar(reporteSiniestroDTO);
					movimientoReaseguroDN.acumularMovimientosTerminacionSiniestros(reporteSiniestroDTO.getIdToReporteSiniestro());
				}
			}else{
				this.modificarPagoParcial(indemnizacionDTO, listaDetalleIndemnizacion);
				indemnizacionForm.setMensaje("El pago parcial fue actualizado con \u00e9xito");
				indemnizacionForm.setTipoMensaje("30");
			}
					
		} catch (SystemException e) {
			indemnizacionForm.setMensaje("El pago parcial no se pudo actualizar");
			indemnizacionForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			indemnizacionForm.setMensaje("El pago parcial no se pudo actualizar");
			indemnizacionForm.setTipoMensaje("10");
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}catch(ParseException pe){
			indemnizacionForm.setMensaje("Formato de fecha incorrecto.");
			indemnizacionForm.setTipoMensaje("10");
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	private boolean isIndemnizacionSoloLectura(IndemnizacionDTO indemnizacionDTO){
		boolean result = true;
		
		result = indemnizacionDTO.getEstatusFinanzasDTO().getIdTcEstatusfinanzas().shortValue() != EstatusFinanzasDTO.ABIERTO.shortValue();
		
		return result;
	}
	
	public void validarUltimoPago(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)throws SystemException,
			ExcepcionDeAccesoADatos, ParseException {

		try {
			IndemnizacionForm indemnizacionForm = (IndemnizacionForm) form;
			IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
			BigDecimal idToReporteSiniestro =  new  BigDecimal(indemnizacionForm.getIdReporteSiniestro());
			BigDecimal idToIndemnizacion =  new  BigDecimal(indemnizacionForm.getIdIndemnizacion());
			IndemnizacionDTO indemnizacionDTO = indemnizacionDN.obtenerUltimoPagoGenerado(idToReporteSiniestro);
			
			if(indemnizacionDTO==null){
				UtileriasWeb.imprimeMensajeXML("1", "", response);
			}else if(indemnizacionDTO.getIdToIndemnizacion().compareTo(idToIndemnizacion) == 0 ){
				UtileriasWeb.imprimeMensajeXML("1", "", response);
			}else{
				UtileriasWeb.imprimeMensajeXML("0", "La indemnizacion no puede ser marcada como ultimo pago", response);
			}
		} catch (SystemException e) {
			UtileriasWeb.imprimeMensajeXML("0", "Ocurrio un error al validar la informacion", response);
			LogDeMidasWeb.log("", Level.WARNING, e);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.imprimeMensajeXML("0", "Ocurrio un error al validar la informacion", response);
			LogDeMidasWeb.log("", Level.WARNING, e);
			
		}
	}
	
	
	private void guardaPagoParcial(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion ){
		try {
			IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
			IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = new IndemnizacionRiesgoCoberturaDN();
			indemnizacionDTO = indemnizacionDN.agregar(indemnizacionDTO);
			for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO:listaDetalleIndemnizacion){
				indemnizacionRiesgoCoberturaDTO.getId().setIdToIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
				indemnizacionRiesgoCoberturaDTO.setIndemnizacionDTO(indemnizacionDTO);
				indemnizacionRiesgoCoberturaDN.agregar(indemnizacionRiesgoCoberturaDTO);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
			
		}
	}
	
	private void modificarPagoParcial(IndemnizacionDTO indemnizacionDTO, List<IndemnizacionRiesgoCoberturaDTO> listaDetalleIndemnizacion ){
		try {
			IndemnizacionDN indemnizacionDN = new IndemnizacionDN();
			IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = new IndemnizacionRiesgoCoberturaDN();
			indemnizacionDTO = indemnizacionDN.modificar(indemnizacionDTO);
			for(IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO:listaDetalleIndemnizacion){
				indemnizacionRiesgoCoberturaDN.modificar(indemnizacionRiesgoCoberturaDTO);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
			
		}
	}
	
}
