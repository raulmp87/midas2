package mx.com.afirme.midas.siniestro.finanzas.pagos;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDN;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaForm;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.DetalleOrdenPagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDN;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.OrdenDePagoDTO;
import mx.com.afirme.midas.siniestro.finanzas.ordendepago.detalle.DetalleOrdenDePagoDN;
import mx.com.afirme.midas.siniestro.reportes.PLAutorizacionTecnica;
import mx.com.afirme.midas.siniestro.reportes.ReporteOrdenDePago;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.wsCliente.seguridad.UsuarioWSDN;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.lowagie.text.DocumentException;

public class SoportePagosAction extends MidasMappingDispatchAction {

	public ActionForward mostrarTab(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {

		String reglaNavegacion = Sistema.EXITOSO;

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarAutorizaciones(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		SoportePagosForm pagosForm = (SoportePagosForm) form;
		String reglaNavegacion = Sistema.EXITOSO;
		try {
			List<AutorizacionTecnicaDTO> autorizaciones = AutorizacionTecnicaDN.getInstancia()
			.listarAutorizacionesGastosFiltrado(
					pagosForm.getSoportePagosDTO());
			if(autorizaciones != null && autorizaciones.size()>0){
				for(AutorizacionTecnicaDTO autorizacion: autorizaciones){
					autorizacion.getReporteSiniestroDTO().setPolizaDTO(PolizaDN.getInstancia().getPorId(autorizacion.getReporteSiniestroDTO().getNumeroPoliza()));
				}				
				pagosForm.setAutorizaciones(autorizaciones);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward listarPagos(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		SoportePagosForm pagosForm = (SoportePagosForm) form;
		String reglaNavegacion = Sistema.EXITOSO;
		String usuario = UtileriasWeb.obtieneNombreUsuario(request);
		try {
			List<DetalleOrdenPagoDTO> ordenesDePago = OrdenDePagoDN.getInstancia()
			.listarOrdenesDePagoFiltrado(
					pagosForm.getSoportePagosDTO());
			if(ordenesDePago != null && ordenesDePago.size()>0){
				pagosForm.setOrdenesDePago(ordenesDePago);
				try{
					for(DetalleOrdenPagoDTO ordenDePago: ordenesDePago){
						if(ordenDePago.getOrdenDePagoDTO().getEstatus().shortValue() != OrdenDePagoDTO.ESTATUS_PAGADA) 
							OrdenDePagoDN.getInstancia().verificarEstatus(ordenDePago.getOrdenDePagoDTO().getIdToOrdenPago(), usuario);
					}
				}catch(Exception e){}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}

		return mapping.findForward(reglaNavegacion);
	}

	public ActionForward generarOrdenPagoAgrupado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		
		SoportePagosForm pagosForm = (SoportePagosForm) form;
		String reglaDeNavegacion = Sistema.EXITOSO;
		String usuario = UtileriasWeb.obtieneNombreUsuario(request);
		String autorizaciones = request.getParameter("ids");
		String[] autorizacionesSplit = null;
		Map<String, String> mensaje;
		try{
			if(autorizaciones != null){
				if (autorizaciones.contains(",")) {
					autorizacionesSplit = autorizaciones.split(",");
				} else {
					autorizacionesSplit = new String[1];
					autorizacionesSplit[0] = autorizaciones;
				}
				BigDecimal[] autorizacionesIds = new BigDecimal[autorizacionesSplit.length];
				for(int i=0; i< autorizacionesSplit.length; i++){
					try {
						autorizacionesIds[i] = UtileriasWeb.regresaBigDecimal(autorizacionesSplit[i]);
					} catch (SystemException e) {
						e.printStackTrace();
					}
				}
				mensaje = SoportePagosDN.getInstancia().generarOrdenDePago(usuario, autorizacionesIds);
				agregarMensaje(mensaje.get("mensaje"), mensaje.get("icono"), pagosForm);	
			}
		}catch (Exception e) {
			mensajeExcepcion(pagosForm, e);		
		}
		
		return mapping.findForward(reglaDeNavegacion);
	}

	public ActionForward cancelarOrdenPagoAgrupado(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
	
		SoportePagosForm pagosForm = (SoportePagosForm) form;
		String reglaDeNavegacion = Sistema.EXITOSO;
		String usuario = UtileriasWeb.obtieneNombreUsuario(request);
		String autorizaciones = request.getParameter("ids");
		Map<String, String> mensaje;
		try{
			if( autorizaciones != null){
				try {
					mensaje = SoportePagosDN.getInstancia().cancelarOrdenDePago(usuario, UtileriasWeb.regresaBigDecimal(autorizaciones));
					agregarMensaje(mensaje.get("mensaje"), mensaje.get("icono"), pagosForm);	
				} catch (ExcepcionDeAccesoADatos e) {
					mensajeExcepcion(pagosForm, e);	
				} catch (SystemException e) {
					mensajeExcepcion(pagosForm, e);	
				}
			}			

		}catch (Exception e) {
			mensajeExcepcion(pagosForm, e);		
		}
		
		return mapping.findForward(reglaDeNavegacion);
	}	
	public ActionForward mostrarGenerarOrdenDePago(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaDeNavegacion = Sistema.EXITOSO;
		SoportePagosForm pagosForm = (SoportePagosForm) form;
		String autorizaciones = request.getParameter("ids");
		String tipo = request.getParameter("tipo");
		Set<String> errores = new HashSet<String>();
	
		if(autorizaciones != null){
			try {
				pagosForm.getSoportePagosDTO().setIdsAutorizaciones(autorizaciones);
				List<AutorizacionTecnicaDTO> autorizacionesDTO = AutorizacionTecnicaDN.getInstancia()
				.listarAutorizacionesGastosFiltrado(pagosForm.getSoportePagosDTO());
				errores = SoportePagosDN.getInstancia().validarAutorizaciones(autorizacionesDTO);
					if(errores == null || errores.size() == 0){
						for(AutorizacionTecnicaDTO autorizacion: autorizacionesDTO){
							autorizacion.getReporteSiniestroDTO().setPolizaDTO(PolizaDN.getInstancia().getPorId(autorizacion.getReporteSiniestroDTO().getNumeroPoliza()));
						}
						pagosForm.setAutorizaciones(autorizacionesDTO);
						pagosForm.setGastoAcumulado(SoportePagosDN.getInstancia().acumulaGastos(autorizacionesDTO));
					}else{
						agregarMensajes(errores, "20", pagosForm, "Las autorizaciones seleccionadas presentaron los siguientes errores:");
					}
	
				pagosForm.setTituloVentana(pagosForm.setTitulo(tipo));
				pagosForm.setTipoOperacion(tipo);
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		
		return mapping.findForward(reglaDeNavegacion);		
	}
	
	public ActionForward mostrarDetalleOrdenDePago(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		String reglaDeNavegacion = Sistema.EXITOSO;
		SoportePagosForm pagosForm = (SoportePagosForm) form;
		String autorizaciones = request.getParameter("ids");
		String tipo = request.getParameter("tipo");
		String usuario = UtileriasWeb.obtieneNombreUsuario(request);
		Set<String> errores = new HashSet<String>();
		OrdenDePagoDTO ordenPagoDTO = null;
		
		if(tipo != null && autorizaciones != null){
			try {
				ordenPagoDTO = OrdenDePagoDN.getInstancia().findById(UtileriasWeb.regresaBigDecimal(autorizaciones));
				if(ordenPagoDTO == null)
					agregarMensajes(errores, "20", pagosForm, "No se encontro la informacion solicitada");
				pagosForm.getSoportePagosDTO().setNumeroOrdenDePago(autorizaciones);
			} catch (ExcepcionDeAccesoADatos e) {
				agregarMensajes(errores, "10", pagosForm, "Error de Acceso a Datos: La consuta solicitada no esta disponible");
			} catch (SystemException e) {
				agregarMensajes(errores, "10", pagosForm, "Ocurrio un error al realizar la consulta");
			}
		}
		if(ordenPagoDTO != null){
		
			try {

				List<DetalleOrdenPagoDTO> detalle = ordenPagoDTO.getDetalleOrdenDePago();
				if(detalle == null || detalle.size()<1){
					detalle = DetalleOrdenDePagoDN.getInstancia().detallesOrdenDePago(ordenPagoDTO.getIdToOrdenPago());
				}
				List<AutorizacionTecnicaDTO> autorizacionesDTO = new ArrayList<AutorizacionTecnicaDTO>();
				for(DetalleOrdenPagoDTO detalleOrden: detalle){
					autorizacionesDTO.add(detalleOrden.getAutorizacionTecnicaDTO());
				}
					if(errores == null || errores.size() == 0){
						for(AutorizacionTecnicaDTO autorizacion: autorizacionesDTO){
							autorizacion.getReporteSiniestroDTO().setPolizaDTO(PolizaDN.getInstancia().getPorId(autorizacion.getReporteSiniestroDTO().getNumeroPoliza()));
							autorizacion.getGastoSiniestroDTO().setNombrePrestadorServicios(PrestadorServiciosDN.getInstancia().detallePrestador(autorizacion.getGastoSiniestroDTO().getIdTcPrestadorServicios(), usuario).getNombrePrestador());
						}
						pagosForm.setAutorizaciones(autorizacionesDTO);
						pagosForm.setGastoAcumulado(SoportePagosDN.getInstancia().acumulaGastos(autorizacionesDTO));
					}else{
						agregarMensajes(errores, "20", pagosForm, "La Orden de Pago seleccionada presenta los siguientes errores:");
					}
	
				pagosForm.setTituloVentana(pagosForm.setTitulo(tipo));
				pagosForm.setTipoOperacion(tipo);
			} catch (SystemException e) {
				agregarMensajes(errores, "30", pagosForm, "Ocurrio un error al consultar los datos de la orden de pago seleccionada");
			}
		}
		
		return mapping.findForward(reglaDeNavegacion);		
	}
	public ActionForward imprimirOrdenDePago(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		AutorizacionTecnicaForm autorizacionTecnicaForm = new AutorizacionTecnicaForm();
		AutorizacionTecnicaDTO autorizacionTecnicaDTO = new AutorizacionTecnicaDTO();
		
		String tipoAutorizacionTecnica = request.getParameter("tipoAutorizacionTecnica");		
		String ordenDePago = request.getParameter("id");
		SoportePagosForm pagosForm = null;
		try{
			
			
			if(!UtileriasWeb.esCadenaVacia(ordenDePago)){
				pagosForm = SoportePagosDN.getInstancia().cargaDatosImpresionOrdenDePago(UtileriasWeb.obtieneNombreUsuario(request), UtileriasWeb.regresaBigDecimal(ordenDePago));
			}
			OrdenDePagoDTO ordenPagoDTO = pagosForm.getOrdenDePagoDTO();
			
			List<DetalleOrdenPagoDTO> detalles = pagosForm.getOrdenesDePago();
			
			List<byte[]> listaReportesByte = new ArrayList<byte[]>();
			
			ReporteOrdenDePago reporte = new ReporteOrdenDePago(pagosForm);
			byte[] caratula = reporte.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
			listaReportesByte.add(caratula);
			PLAutorizacionTecnica plAutorizacionTecnica = null;
			
			for(DetalleOrdenPagoDTO detalle: detalles){
				
				if (tipoAutorizacionTecnica.equalsIgnoreCase("Gasto")){
					poblarAutorizacionTecnicaGasto(autorizacionTecnicaForm,detalle);
					this.poblarForma(autorizacionTecnicaForm, autorizacionTecnicaDTO, ordenPagoDTO);
					plAutorizacionTecnica = new PLAutorizacionTecnica(autorizacionTecnicaForm,"Gasto");
					
					byte[] reporteBytes = null; 
					reporteBytes = plAutorizacionTecnica.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));						
					if (reporteBytes != null)
						listaReportesByte.add(reporteBytes);
				}
			
			}
			
			if (listaReportesByte.size() > 0 && plAutorizacionTecnica != null){		
				byte[] reportefinal = null;

				reportefinal = MidasReporteBase.concatenarReportes(listaReportesByte,response);					

				super.writeBytes(response, reportefinal, Sistema.TIPO_PDF, "OrdenDePago" + ordenDePago);
			}
			return null;
		}catch (SystemException e) {
			LogDeMidasWeb.log("Error al imprimir la orden de pago: "+ordenDePago, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>Orden de Pago"+ordenDePago+" , tipo de reporte: Orden de Pago Agrupada</h3>");
			return mapping.findForward("errorImpresion");
		}catch (IOException e) {
			LogDeMidasWeb.log("Error al imprimir la orden de pago: "+ordenDePago, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>Orden de Pago"+ordenDePago+" , tipo de reporte: Orden de Pago Agrupada</h3>");
			return mapping.findForward("errorImpresion");
		}catch (DocumentException e) {
			LogDeMidasWeb.log("Error al imprimir la orden de pago: "+ordenDePago, Level.SEVERE, e);
			request.setAttribute("titulo", "MidasWeb no puede imprimir su documento.");
			request.setAttribute("leyenda", "<h2>Notifique al administrador lo siguiente:</h2><h3>Orden de Pago"+ordenDePago+" , tipo de reporte: Orden de Pago Agrupada</h3>");
			return mapping.findForward("errorImpresion");
		}
	}
	
	public void poblarAutorizacionTecnicaGasto(
			AutorizacionTecnicaForm beanForm, DetalleOrdenPagoDTO detalle)
			throws ExcepcionDeAccesoADatos, SystemException {
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		beanForm.setNumeroAutorizacionTecnica(detalle
				.getAutorizacionTecnicaDTO().getIdTipoAutorizacionTecnica());
		GastoSiniestroDTO gastoSiniestroDTO = detalle
				.getAutorizacionTecnicaDTO().getGastoSiniestroDTO();
		ReporteSiniestroDTO reporteSiniestroDTO = gastoSiniestroDTO
				.getReporteSiniestroDTO();

		PolizaSoporteDanosDTO poliza = soporteDanosDN.getDatosGeneralesPoliza(
				reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO
						.getFechaSiniestro());
		
		poblarForma(beanForm, reporteSiniestroDTO, poliza, gastoSiniestroDTO, AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO);	

	}

	private void poblarForma(AutorizacionTecnicaForm form,
			AutorizacionTecnicaDTO autorizacionTecnicaDTO,
			OrdenDePagoDTO ordenDePagoDTO) {
		if (autorizacionTecnicaDTO == null)
			return;
		String nombreCompletoUsuario = new String();
		if (autorizacionTecnicaDTO.getIdToAutorizacionTecnica() != null) {
			form.setNumeroAutorizacionTecnica(autorizacionTecnicaDTO
					.getIdToAutorizacionTecnica().intValue());
			form.setCveAutorizacionTecnica(UtileriasWeb.llenarIzquierda(
					autorizacionTecnicaDTO.getIdToAutorizacionTecnica()
							.toString(), "0", 8));
		}
		if (!UtileriasWeb.esCadenaVacia(autorizacionTecnicaDTO
				.getCodigoUsuarioAutoriza())) {
			form.setCodigoUsuarioAutoriza(autorizacionTecnicaDTO
					.getCodigoUsuarioAutoriza());
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(
					autorizacionTecnicaDTO.getCodigoUsuarioAutoriza());
			if (usuario != null && usuario.getId() != null) {
				nombreCompletoUsuario = new String();
				if (!UtileriasWeb.esCadenaVacia(usuario.getNombre()))
					nombreCompletoUsuario += usuario.getNombre() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoMaterno()))
					nombreCompletoUsuario += usuario.getApellidoMaterno() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoPaterno()))
					nombreCompletoUsuario += usuario.getApellidoPaterno() + " ";
				form.setNombreUsuarioAutoriza(nombreCompletoUsuario);
			}
		}
		if (!UtileriasWeb.esCadenaVacia(autorizacionTecnicaDTO
				.getCodigoUsuarioCreacion())) {
			form.setCodigoUsuarioCreacion(autorizacionTecnicaDTO
					.getCodigoUsuarioCreacion());
			Usuario usuario = UsuarioWSDN.getInstancia().obtieneUsuario(
					autorizacionTecnicaDTO.getCodigoUsuarioCreacion());
			if (usuario != null && usuario.getId() != null) {
				nombreCompletoUsuario = new String();
				if (!UtileriasWeb.esCadenaVacia(usuario.getNombre()))
					nombreCompletoUsuario += usuario.getNombre() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoMaterno()))
					nombreCompletoUsuario += usuario.getApellidoMaterno() + " ";
				if (!UtileriasWeb.esCadenaVacia(usuario.getApellidoPaterno()))
					nombreCompletoUsuario += usuario.getApellidoPaterno() + " ";
				form.setNombreUsuarioCreacion(nombreCompletoUsuario);
			}
		}
		if (autorizacionTecnicaDTO.getFechaAutorizacion() != null) {
			form.setFechaAutorizacion(UtileriasWeb
					.getFechaString(autorizacionTecnicaDTO
							.getFechaAutorizacion()));
		}
		if (autorizacionTecnicaDTO.getFechaCreacion() != null) {
			form.setFechaCreacion(UtileriasWeb
					.getFechaString(autorizacionTecnicaDTO.getFechaCreacion()));
		}
		if (ordenDePagoDTO.getIdToOrdenPago() != null) {
			form.setIdOrdenPago(UtileriasWeb.llenarIzquierda(ordenDePagoDTO
					.getIdToOrdenPago().toString(), "0", 8));
		}
		if (ordenDePagoDTO.getIdSolicitudCheque() != null) {
			form.setIdSolicitudCheque(ordenDePagoDTO.getIdSolicitudCheque());
		}
	}	
	private void poblarForma(AutorizacionTecnicaForm form,
			ReporteSiniestroDTO reporteSiniestroDTO,
			PolizaSoporteDanosDTO poliza, Object objConcepto,
			int tipoAutorizacionTecnica) throws ExcepcionDeAccesoADatos,
			SystemException {

		form.setIdToReporteSiniestro(reporteSiniestroDTO
				.getIdToReporteSiniestro().intValue());

		// Datos de la poliza
		form.setProducto(String.valueOf(poliza.getCodigoProducto()));

		if (poliza.getIdAsegurado() != null) {
			form.setCveAsegurado(poliza.getIdAsegurado().toString());
		}

		if (poliza.getNombreAsegurado() != null) {
			form.setAsegurado(poliza.getNombreAsegurado());
		}

		form.setNumeroReporte(reporteSiniestroDTO.getNumeroReporte());

		if (poliza.getNumeroPoliza() != null) {
			form.setNumeroPoliza(poliza.getNumeroPoliza());
		}

		form.setNumeroEndoso(String.valueOf(poliza.getNumeroUltimoEndoso()));

		if (reporteSiniestroDTO.getFechaHoraReporte() != null) {
			String fechaOcurrio = UtileriasWeb
					.getFechaHoraString(reporteSiniestroDTO
							.getFechaHoraReporte());
			form.setFechaOcurrio(fechaOcurrio);
		}

		if (poliza.getCodigoAgente() != null) {
			form.setIdAgente(poliza.getCodigoAgente());
			form.setAgente(poliza.getNombreAgente());
		}

		form.setIdTipoAutorizacionTecnica(tipoAutorizacionTecnica);

		IntegracionReaseguroDN integracionReaseguroDN = IntegracionReaseguroDN
				.getInstancia();
		List<SoporteDistribucionReaseguroDTO> distribucion = null;

		switch (tipoAutorizacionTecnica) {
		case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_GASTO: {
			GastoSiniestroDTO gastoSiniestroDTO = (GastoSiniestroDTO) objConcepto;
			distribucion = integracionReaseguroDN
					.getDistribucionDeGasto(gastoSiniestroDTO);

			form.setDescripcionConcepto(gastoSiniestroDTO.getDescripcion());
			form.setNumeroFactura("");
			form.setIdToConcepto(gastoSiniestroDTO.getIdToGastoSiniestro()
					.intValue());

			if (gastoSiniestroDTO.getIdTcPrestadorServicios() != null) {
				form.setCvePrestadorServicios(String.valueOf(gastoSiniestroDTO
						.getIdTcPrestadorServicios()));

				PrestadorServiciosDTO prestadorServicios = getPrestadorServicios(
						gastoSiniestroDTO.getIdTcPrestadorServicios(), "ADMIN");

				if (prestadorServicios != null) {
					form.setBeneficiario(prestadorServicios
							.getNombrePrestador());
				} else {
					form.setBeneficiario("");
				}
			}

			if (gastoSiniestroDTO.getFechaEstimacionPago() != null) {
				String fechaEstimadaPago = UtileriasWeb
						.getFechaString(gastoSiniestroDTO
								.getFechaEstimacionPago());
				form.setFechaEstimadaPago(fechaEstimadaPago);
			}

			if (gastoSiniestroDTO.getConceptoGasto().getDescripcion() != null) {
				form.setConceptoPago(gastoSiniestroDTO.getConceptoGasto()
						.getDescripcion());
			}

			if (gastoSiniestroDTO.getMontoGasto() != null) {
				form.setMontoConcepto(gastoSiniestroDTO.getMontoGasto());
			}

			if (gastoSiniestroDTO.getMontoIVA() != null) {
				form.setMontoIVA(gastoSiniestroDTO.getMontoIVA());
			}

			if (gastoSiniestroDTO.getMontoISR() != null) {
				form.setMontoISR(gastoSiniestroDTO.getMontoISR());
			}

			if (gastoSiniestroDTO.getMontoIVARetencion() != null) {
				form.setMontoIVARetencion(gastoSiniestroDTO
						.getMontoIVARetencion());
			}

			if (gastoSiniestroDTO.getMontoISRRetencion() != null) {
				form.setMontoISRRetencion(gastoSiniestroDTO
						.getMontoISRRetencion());
			}

			if (gastoSiniestroDTO.getMontoOtros() != null) {
				form.setMontoOtros(gastoSiniestroDTO.getMontoOtros());
			}

			form.setDetalleDistribucion(distribucion);

			break;
		}
		case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INGRESO: {
			IngresoSiniestroDTO ingresoSiniestroDTO = (IngresoSiniestroDTO) objConcepto;
			distribucion = integracionReaseguroDN
					.getDistribucionDeIngreso(ingresoSiniestroDTO);

			form.setDescripcionConcepto(ingresoSiniestroDTO.getDescripcion());
			form.setIdToConcepto(ingresoSiniestroDTO.getIdToIngresoSiniestro()
					.intValue());

			if (ingresoSiniestroDTO.getConceptoIngreso().getDescripcion() != null) {
				form.setConceptoPago(ingresoSiniestroDTO.getConceptoIngreso()
						.getDescripcion());
			}

			if (ingresoSiniestroDTO.getMonto() != null) {
				form.setMontoConcepto(ingresoSiniestroDTO.getMonto());
			}

			if (ingresoSiniestroDTO.getMontoIVA() != null) {
				form.setMontoIVA(ingresoSiniestroDTO.getMontoIVA());
			}

			if (ingresoSiniestroDTO.getMontoISR() != null) {
				form.setMontoISR(ingresoSiniestroDTO.getMontoISR());
			}

			if (ingresoSiniestroDTO.getMontoIVARetencion() != null) {
				form.setMontoIVARetencion(ingresoSiniestroDTO
						.getMontoIVARetencion());
			}

			if (ingresoSiniestroDTO.getMontoISRRetencion() != null) {
				form.setMontoISRRetencion(ingresoSiniestroDTO
						.getMontoISRRetencion());
			}

			if (ingresoSiniestroDTO.getMontoOtros() != null) {
				form.setMontoOtros(ingresoSiniestroDTO.getMontoOtros());
			}

			form.setDetalleDistribucion(distribucion);

			break;
		}
		case AutorizacionTecnicaDTO.ID_TIPO_AUTORIZACION_INDEMNIZACION: {
			IndemnizacionDTO indemnizacionDTO = (IndemnizacionDTO) objConcepto;
			distribucion = integracionReaseguroDN
					.getDistribucionDeIndemnizacion(indemnizacionDTO);

			form.setIdToConcepto(indemnizacionDTO.getIdToIndemnizacion()
					.intValue());
			form.setDescripcionConcepto(indemnizacionDTO.getComentarios());
			form.setBeneficiario(indemnizacionDTO.getBeneficiario());
			form
					.setConceptoPago(AutorizacionTecnicaDTO.DESCRIPCION_TIPO_AUTORIZACION_INDEMNIZACION);

			if (indemnizacionDTO.getMontoIva() != null) {
				form.setMontoIVA(indemnizacionDTO.getMontoIva());
			}

			if (indemnizacionDTO.getMontoIsr() != null) {
				form.setMontoISR(indemnizacionDTO.getMontoIsr());
			}

			if (indemnizacionDTO.getMontoIvaRetencion() != null) {
				form.setMontoIVARetencion(indemnizacionDTO
						.getMontoIvaRetencion());
			}

			if (indemnizacionDTO.getMontoIsrRetencion() != null) {
				form.setMontoISRRetencion(indemnizacionDTO
						.getMontoIsrRetencion());
			}

			if (indemnizacionDTO.getMontoOtros() != null) {
				form.setMontoOtros(indemnizacionDTO.getMontoOtros());
			}

			IndemnizacionRiesgoCoberturaDN indemnizacionDN = IndemnizacionRiesgoCoberturaDN
					.getInstancia();

			Double montoCoaseguro = indemnizacionDN
					.getTotalCoaseguroPorIdAutorizacionTecnica(indemnizacionDTO
							.getIdToIndemnizacion());
			Double montoDeducible = indemnizacionDN
					.getTotalDeduciblePorIdAutorizacionTecnica(indemnizacionDTO
							.getIdToIndemnizacion());

			double granTotal = indemnizacionDTO.getTotalPago().doubleValue()
					+ montoCoaseguro.doubleValue()
					+ montoDeducible.doubleValue();

			form.setMontoConcepto(granTotal);

			form.setMontoCoaseguro(montoCoaseguro);
			form.setMontoDeducible(montoDeducible);
			form.setDetalleDistribucion(distribucion);

			break;
		}
		}
	}

	private PrestadorServiciosDTO getPrestadorServicios(
			BigDecimal idPrestadorServicios, String nombreUsuario)
			throws SystemException {
		PrestadorServiciosDTO prestadorDeSerivicios = null;

		PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN
				.getInstancia();
		prestadorDeSerivicios = prestadorServiciosDN.detallePrestador(
				idPrestadorServicios, nombreUsuario);

		return prestadorDeSerivicios;

	}	
}
