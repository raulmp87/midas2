package mx.com.afirme.midas.siniestro.servicios;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.interfaz.asientocontable.AsientoContableDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.sistema.SystemException;


public class SinietroServiciosDN {

	private static final SinietroServiciosDN INSTANCIA = new SinietroServiciosDN();
	private static String nombreUsuario;
	
	public static SinietroServiciosDN getInstancia(String nombreUsuario) {
		SinietroServiciosDN.nombreUsuario = nombreUsuario;
		return INSTANCIA;
		
	}	
	
	
	/**
	 * Genera asientos contables para los diferentes movimientos
	 * @param idObjetoContable Id del objeto a contabilizar (Poliza, Siniestro, Endoso, etc...)
	 * @param claveTransaccionContable Clave de transacci�n Contable (Sistema.REASEGURO_EMIS, Sistema.SINIESTRO � Sistema.REASEGURO_SIN)
	 * @return Datos Contables Movimientos Reaseguro Emision, Siniestros o Reaseguro Siniestro
	 * @throws SystemException
	 */
	public List<AsientoContableDTO> contabilizaMovimientos(String idObjetoContable, String claveTransaccionContable) throws SystemException {
		SinietroServiciosSN sinietroServiciosSN;
		try {
			sinietroServiciosSN = new SinietroServiciosSN();
			return sinietroServiciosSN.contabilizaMovimientos(idObjetoContable, claveTransaccionContable,SinietroServiciosDN.nombreUsuario);
		} catch (SystemException e) {
			throw e;
		}
	}
	
	/**
	 * Registra pagos de Midas a Seycos
	 * @param idPago Identificador de Pago de MIDAS
	 * @param claveTransaccionContable Transacci�n Contable (Valor Constante) 'INDEMNIZACIONES' �  'GASTOS'
	 * @param idMoneda Identificador de la moneda
	 * @param tipoCambio Tipo de cambio
	 * @param idPrestadorServicio Identificador Prestador de Servicio
	 * @param nombreBeneficiario Nombre del Beneficiario
	 * @param tipoPago Tipo de Pago 'SC' Solicitud de Cheque  'TB' Transferencia Bancaria
	 * @param conceptoPoliza Concepto de la P�liza Ej. 'PAGO DE SINIESTROS' / id_pago_midas
	 * @return Id de la Solicitud de Cheque
	 * @throws SystemException
	 */
	public BigDecimal solicitaCheque (BigDecimal idPago, String claveTransaccionContable, Short idMoneda,
			Double tipoCambio, BigDecimal idPrestadorServicio, String nombreBeneficiario, String tipoPago,
			String conceptoPoliza) throws SystemException {
		
		SinietroServiciosSN sinietroServiciosSN = new SinietroServiciosSN();
		
		SolicitudChequeDTO solicitudCheque = new SolicitudChequeDTO();
		
		solicitudCheque.setIdPago(idPago);
		solicitudCheque.setClaveTransaccionContable(claveTransaccionContable);
		solicitudCheque.setIdMoneda(idMoneda);
		solicitudCheque.setTipoCambio(tipoCambio);
		solicitudCheque.setIdPrestadorServicio(idPrestadorServicio);
		solicitudCheque.setNombreBeneficiario(nombreBeneficiario);
		solicitudCheque.setTipoPago(tipoPago);
		solicitudCheque.setConceptoPoliza(conceptoPoliza);

		return sinietroServiciosSN.solicitaCheque(solicitudCheque, SinietroServiciosDN.nombreUsuario);
	}
	
	/**
	 * Consulta el estatus de la solicitud de Cheque
	 * @param idPago Identificador de Pago de MIDAS
	 * @return Estatus de la solicitud de Cheque
	 * @throws SystemException
	 */
	public SolicitudChequeDTO consultaEstatusSolicitudCheque (BigDecimal idPago) throws SystemException {
		
		SinietroServiciosSN sinietroServiciosSN = new SinietroServiciosSN();
		return sinietroServiciosSN.consultaEstatusSolicitudCheque(idPago, SinietroServiciosDN.nombreUsuario);
	}
	
	/**
	 * Cancela una solicitud de Cheque
	 * @param idPago Identificador de Pago de MIDAS
	 * @return Estatus de la solicitud de Cheque
	 * @throws SystemException
	 */
	public String cancelaSolicitudCheque (BigDecimal idPago) throws SystemException {
		SinietroServiciosSN sinietroServiciosSN = new SinietroServiciosSN();
		return sinietroServiciosSN.cancelaSolicitudCheque(idPago, SinietroServiciosDN.nombreUsuario);
	}
}
