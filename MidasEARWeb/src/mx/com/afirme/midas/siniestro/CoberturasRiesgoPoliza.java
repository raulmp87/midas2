package mx.com.afirme.midas.siniestro;

import java.math.BigDecimal;

public class CoberturasRiesgoPoliza {
	
	private static final long serialVersionUID = 1L;
	private	BigDecimal numeroInciso;	 
	private	BigDecimal idToSeccion;
	private	BigDecimal numeroSubInciso;
	private	BigDecimal idToCobertura;
	private	BigDecimal idToRiesgo;
	private	String descripcionSeccion;
	private	String descripcionSubInciso;
	private	String descripcionCobertura;
	private	String descripcionRiesgo;
	private	Double sumaAsegurada;
	private	int claveTipoSumaAsegurada;
	private	int claveTipoAgrupacion;
	private Double primaNeta;
	private Double deducible;
	private Double coaseguro;
	
	private Short seleccionado;
	
	public CoberturasRiesgoPoliza(){
	}

	/**
	 * @return the numeroInciso
	 */
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param numeroInciso the numeroInciso to set
	 */
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @return the idToSeccion
	 */
	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	/**
	 * @param idToSeccion the idToSeccion to set
	 */
	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	/**
	 * @return the numeroSubInciso
	 */
	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}

	/**
	 * @param numeroSubInciso the numeroSubInciso to set
	 */
	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}

	/**
	 * @return the idToCobertura
	 */
	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	/**
	 * @param idToCobertura the idToCobertura to set
	 */
	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	/**
	 * @return the idToRiesgo
	 */
	public BigDecimal getIdToRiesgo() {
		return idToRiesgo;
	}

	/**
	 * @param idToRiesgo the idToRiesgo to set
	 */
	public void setIdToRiesgo(BigDecimal idToRiesgo) {
		this.idToRiesgo = idToRiesgo;
	}

	/**
	 * @return the descripcionSeccion
	 */
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}

	/**
	 * @param descripcionSeccion the descripcionSeccion to set
	 */
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}

	/**
	 * @return the descripcionSubInciso
	 */
	public String getDescripcionSubInciso() {
		return descripcionSubInciso;
	}

	/**
	 * @param descripcionSubInciso the descripcionSubInciso to set
	 */
	public void setDescripcionSubInciso(String descripcionSubInciso) {
		this.descripcionSubInciso = descripcionSubInciso;
	}

	/**
	 * @return the descripcionCobertura
	 */
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}

	/**
	 * @param descripcionCobertura the descripcionCobertura to set
	 */
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}

	/**
	 * @return the descripcionRiesgo
	 */
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}

	/**
	 * @param descripcionRiesgo the descripcionRiesgo to set
	 */
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}

	/**
	 * @return the sumaAsegurada
	 */
	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	/**
	 * @param sumaAsegurada the sumaAsegurada to set
	 */
	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	/**
	 * @return the claveTipoSumaAsegurada
	 */
	public int getClaveTipoSumaAsegurada() {
		return claveTipoSumaAsegurada;
	}

	/**
	 * @param claveTipoSumaAsegurada the claveTipoSumaAsegurada to set
	 */
	public void setClaveTipoSumaAsegurada(int claveTipoSumaAsegurada) {
		this.claveTipoSumaAsegurada = claveTipoSumaAsegurada;
	}

	/**
	 * @return the claveTipoAgrupacion
	 */
	public int getClaveTipoAgrupacion() {
		return claveTipoAgrupacion;
	}

	/**
	 * @param claveTipoAgrupacion the claveTipoAgrupacion to set
	 */
	public void setClaveTipoAgrupacion(int claveTipoAgrupacion) {
		this.claveTipoAgrupacion = claveTipoAgrupacion;
	}

	/**
	 * @return the primaNeta
	 */
	public Double getPrimaNeta() {
		return primaNeta;
	}

	/**
	 * @param primaNeta the primaNeta to set
	 */
	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	/**
	 * @return the deducible
	 */
	public Double getDeducible() {
		return deducible;
	}

	/**
	 * @param deducible the deducible to set
	 */
	public void setDeducible(Double deducible) {
		this.deducible = deducible;
	}

	/**
	 * @return the coaseguro
	 */
	public Double getCoaseguro() {
		return coaseguro;
	}

	/**
	 * @param coaseguro the coaseguro to set
	 */
	public void setCoaseguro(Double coaseguro) {
		this.coaseguro = coaseguro;
	}

	/**
	 * @return the seleccionado
	 */
	public Short getSeleccionado() {
		return seleccionado;
	}

	/**
	 * @param seleccionado the seleccionado to set
	 */
	public void setSeleccionado(Short seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	
}
