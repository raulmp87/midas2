package mx.com.afirme.midas.siniestro.salvamento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SalvamentoSiniestroSN {
	
	private SalvamentoSiniestroFacadeRemote beanRemoto;

	public SalvamentoSiniestroSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SalvamentoSiniestroFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public SalvamentoSiniestroDTO getPorId(SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(salvamentoSiniestroDTO.getIdToSalvamentoSiniestro());
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public SalvamentoSiniestroDTO agregar(SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.save(salvamentoSiniestroDTO);
			return salvamentoSiniestroDTO;
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public void borrar(SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.delete(salvamentoSiniestroDTO);
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}		
	}
	
	public SalvamentoSiniestroDTO modificar(SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos{
		try{
			beanRemoto.update(salvamentoSiniestroDTO);
			return salvamentoSiniestroDTO;
		}catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SalvamentoSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SalvamentoSiniestroDTO> buscarPorPropiedad(String propertyName, Object value) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByProperty(propertyName, value);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SalvamentoSiniestroDTO> listarPorEstatusNoBorrado(ReporteSiniestroDTO reporteSiniestroDTO)throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findByNotDeletedStatus(reporteSiniestroDTO);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public List<SalvamentoSiniestroDTO> listarPendientes(ReporteSiniestroDTO reporteSiniestroDTO)throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.getSalvamentosPorReporteYEstatus(reporteSiniestroDTO, SalvamentoEstatusDTO.ESTATUS_POR_VENDER,SalvamentoEstatusDTO.ESTATUS_LEGAL, SalvamentoEstatusDTO.ESTATUS_OTROS);
		}catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
}
