package mx.com.afirme.midas.siniestro.salvamento;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.reportes.RegistroEstimacionInicial;
import mx.com.afirme.midas.siniestro.reportes.RegistroSalvamento;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SalvamentoSiniestroDN {
	private final String NAME_ID_REPORTE_SINIESTRO = "reporteSiniestro.idToReporteSiniestro";
	public static final SalvamentoSiniestroDN INSTANCIA = new SalvamentoSiniestroDN();

	public static SalvamentoSiniestroDN getInstancia (){
		return SalvamentoSiniestroDN.INSTANCIA;
	}
	
	public SalvamentoSiniestroDTO getPorId(SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.getPorId(salvamentoSiniestroDTO);
	}
	
	public SalvamentoSiniestroDTO agregar(SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.agregar(salvamentoSiniestroDTO);
	}
	
	public void borrar (SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		salvamentoSiniestroSN.borrar(salvamentoSiniestroDTO);
	}
	
	public SalvamentoSiniestroDTO modificar (SalvamentoSiniestroDTO salvamentoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.modificar(salvamentoSiniestroDTO);
	}
	
	public List<SalvamentoSiniestroDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.listarTodos();
	}
	
	public List<SalvamentoSiniestroDTO> buscarPorPropiedad(String nombrePropiedad, Object valor) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.buscarPorPropiedad(nombrePropiedad, valor);
	}
	
	public List<SalvamentoSiniestroDTO> listarPorEstatusNoBorrado(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.listarPorEstatusNoBorrado(reporteSiniestroDTO);
	}
	
	public void modificar(SalvamentoSiniestroDTO salvamentoSiniestroDTO, IngresoSiniestroDTO ingresoSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		ingresoSiniestroDTO = ingresoSiniestroDN.agregarIngreso(ingresoSiniestroDTO);
		salvamentoSiniestroDTO.setIngresoSiniestro(ingresoSiniestroDTO);
		salvamentoSiniestroSN.modificar(salvamentoSiniestroDTO);
	}
	
	public List<RegistroSalvamento> listarRegistroSalvamento(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		List<RegistroSalvamento> listadoRegistrosSalvamento = new ArrayList<RegistroSalvamento>();
		List<SalvamentoSiniestroDTO> listadoSalvamentos = listarPorEstatusNoBorrado(reporteSiniestroDTO);
		for(SalvamentoSiniestroDTO salvamentoSiniestroDTO : listadoSalvamentos){
			RegistroSalvamento registroSalvamento = new RegistroSalvamento();
			//Se cargan los datos al registro de salvamento
			registroSalvamento.setSalvamento(salvamentoSiniestroDTO);
			registroSalvamento.setSalvamentoMonto(new Double(0));
			registroSalvamento.setSalvamentoCuotaParte(new Double(0));
			registroSalvamento.setSalvamentoFacultativo(new Double(0));
			registroSalvamento.setSalvamentoPrimerExcedente(new Double(0));
			registroSalvamento.setSalvamentoRetencion(new Double(0));
			listadoRegistrosSalvamento.add(registroSalvamento);
		}
		return listadoRegistrosSalvamento;
	}
	
	public List<SalvamentoSiniestroDTO> listarSalvamentosPendientes(ReporteSiniestroDTO reporteSiniestroDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoSiniestroSN salvamentoSiniestroSN = new SalvamentoSiniestroSN();
		return salvamentoSiniestroSN.listarPendientes(reporteSiniestroDTO);
	}
}
