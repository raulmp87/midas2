package mx.com.afirme.midas.siniestro.salvamento;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

public class SalvamentoSiniestroAction extends MidasMappingDispatchAction {
	
	/**
	 * Method listar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward listar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;				
		try {
			double valorEstimadoTotal=0.0;
			double valorVentaTotal=0.0;
			SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm)form;
			String idToReporteSiniestro = request.getParameter("idToReporteSiniestro");
			if (UtileriasWeb.esCadenaVacia(idToReporteSiniestro)){
				idToReporteSiniestro = salvamentoSiniestroForm.getIdToReporteSiniestro();
			}
			ReporteSiniestroDTO reporteSiniestroDTO = new ReporteSiniestroDTO();		
			salvamentoSiniestroForm.setIdToReporteSiniestro(idToReporteSiniestro);
			reporteSiniestroDTO.setIdToReporteSiniestro(new BigDecimal(idToReporteSiniestro));
			
			SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
			List<SalvamentoSiniestroDTO> items = new ArrayList<SalvamentoSiniestroDTO>();
			items = salvamentoSiniestroDN.listarPorEstatusNoBorrado(reporteSiniestroDTO);
			for (SalvamentoSiniestroDTO salvamentoSiniestroDTO : items) {
				if (salvamentoSiniestroDTO.getValorEstimado()!= null)
					valorEstimadoTotal += salvamentoSiniestroDTO.getValorEstimado().doubleValue();
				if (salvamentoSiniestroDTO.getValorReal() != null)
					valorVentaTotal += salvamentoSiniestroDTO.getValorReal().doubleValue();
			}
			salvamentoSiniestroForm.setValorEstimadoTotal(UtileriasWeb.formatoMoneda(valorEstimadoTotal));
			salvamentoSiniestroForm.setValorVentaTotal(UtileriasWeb.formatoMoneda(valorVentaTotal));
			request.setAttribute("items",items);
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		}
		return mapping.findForward(reglaNavegacion);
	}

	
	/**
	 * Method agregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward agregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();

		try {
			this.poblarDTO(salvamentoSiniestroForm, salvamentoSiniestroDTO, request);
			salvamentoSiniestroDTO.setFechaCreacion(new Date());
			Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
			salvamentoSiniestroDTO.setIdTcUsuarioCreacion(new BigDecimal(usuario.getId().toString()));
			salvamentoSiniestroDTO.setEstatusInterno(SalvamentoSiniestroDTO.ESTATUS_ACTIVO);
			salvamentoSiniestroDN.agregar(salvamentoSiniestroDTO);
			this.listar(mapping, salvamentoSiniestroForm, request, response);			
	 	} catch (SystemException e) {
	 		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
	 	} catch (ExcepcionDeAccesoADatos e) {
	 		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ParseException e) {
	 		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
	 	}
 		return mapping.findForward(reglaNavegacion);

	}
	
	private void setDatosModiificacion(SalvamentoSiniestroDTO salvamentoSiniestroDTO ,HttpServletRequest request){
		Usuario usuario = (Usuario)UtileriasWeb.obtenValorSessionScope(request, Sistema.USUARIO_ACCESO_MIDAS);
		salvamentoSiniestroDTO.setFechaModificacion(new Date());
		salvamentoSiniestroDTO.setIdTcUsuarioModificacion(new BigDecimal(usuario.getId().toString()));
	}
	
	/**
	 * Method modificar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward modificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		
		String reglaNavegacion = Sistema.EXITOSO;
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		try {
			if (!StringUtil.isEmpty(salvamentoSiniestroForm.getIdToSalvamentoSiniestro())){
				salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(salvamentoSiniestroForm.getIdToSalvamentoSiniestro()));
				salvamentoSiniestroDTO = salvamentoSiniestroDN.getPorId(salvamentoSiniestroDTO);
			}
			this.poblarDTO(salvamentoSiniestroForm, salvamentoSiniestroDTO, request);
			salvamentoSiniestroDTO.setEstatusInterno(SalvamentoSiniestroDTO.ESTATUS_ACTIVO);
			salvamentoSiniestroDN.modificar(salvamentoSiniestroDTO);
			this.listar(mapping, salvamentoSiniestroForm, request, response);
  		} catch (SystemException e) {
  			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_DISPONIBLE;
 		} catch (ExcepcionDeAccesoADatos e) {
 			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
 		} catch (ParseException e) {
	 		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
	 	}
		return mapping.findForward(reglaNavegacion);

	}

	
	/**
	 * Method borrar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward borrar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		String reglaNavegacion = Sistema.EXITOSO;
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		try {
			if (!StringUtil.isEmpty(salvamentoSiniestroForm.getIdToSalvamentoSiniestro())){
				salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(salvamentoSiniestroForm.getIdToSalvamentoSiniestro()));
				salvamentoSiniestroDTO = salvamentoSiniestroDN.getPorId(salvamentoSiniestroDTO);
				if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroForm.getObservaciones()))
					salvamentoSiniestroDTO.setObservaciones(salvamentoSiniestroForm.getObservaciones());
				salvamentoSiniestroDTO.setEstatusInterno(SalvamentoSiniestroDTO.ESTATUS_ELIMINADO);
				salvamentoSiniestroDN.modificar(salvamentoSiniestroDTO);
			}
		} catch (SystemException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
		} 
		return mapping.findForward(reglaNavegacion);

	}

	/**
	 * Method mostrarAgregar
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarAgregar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm)form;
		try{
			this.limpiarForm(salvamentoSiniestroForm);
			List<SalvamentoEstatusDTO> listaEstatusSalvamento = SalvamentoEstatusDN.getInstancia().listarNoVendidos();
			salvamentoSiniestroForm.setListaEstatusSalvamento(listaEstatusSalvamento);
		}catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarModificar(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		String idToSalvamentoSiniestro = request.getParameter("idToSalvamentoSiniestro");
		
		salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(idToSalvamentoSiniestro));
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		try {
			salvamentoSiniestroDTO = salvamentoSiniestroDN.getPorId(salvamentoSiniestroDTO);
			this.limpiarForm(salvamentoSiniestroForm);
			this.poblarForm(salvamentoSiniestroDTO, salvamentoSiniestroForm);
			List<SalvamentoEstatusDTO> listaEstatusSalvamento = SalvamentoEstatusDN.getInstancia().listarNoVendidos();
			salvamentoSiniestroForm.setListaEstatusSalvamento(listaEstatusSalvamento);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} 

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarDetalle
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarDetalle(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		String idToSalvamentoSiniestro = request.getParameter("idToSalvamentoSiniestro");
		
		salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(idToSalvamentoSiniestro));
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		try {
			salvamentoSiniestroDTO = salvamentoSiniestroDN.getPorId(salvamentoSiniestroDTO);
			this.limpiarForm(salvamentoSiniestroForm);
			this.poblarForm(salvamentoSiniestroDTO, salvamentoSiniestroForm);
			List<SalvamentoEstatusDTO> listaEstatusSalvamento = SalvamentoEstatusDN.getInstancia().listarTodos();
			salvamentoSiniestroForm.setListaEstatusSalvamento(listaEstatusSalvamento);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} 

		return mapping.findForward(reglaNavegacion);
	}
	
	/**
	 * Method mostrarVender
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return ActionForward
	 */
	public ActionForward mostrarVender(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String formularioOrigen = request.getParameter("formularioOrigen");
		
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroForm formularioRequest = (SalvamentoSiniestroForm)request.getAttribute("salvamentoSiniestroForm");
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		String idToSalvamentoSiniestro = request.getParameter("idToSalvamentoSiniestro");
		
		salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(idToSalvamentoSiniestro));
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		try {
			salvamentoSiniestroDTO = salvamentoSiniestroDN.getPorId(salvamentoSiniestroDTO);
			if (formularioOrigen!=null && formularioOrigen.equalsIgnoreCase("agregarIngresoSiniestro")){
				if (formularioRequest!=null){
					this.poblarDTO(formularioRequest, salvamentoSiniestroDTO, request);
				}
			}
			request.getSession().removeAttribute("salvamentoSiniestroForm");
			this.poblarForm(salvamentoSiniestroDTO, salvamentoSiniestroForm);
			List<SalvamentoEstatusDTO> listaEstatusSalvamento = SalvamentoEstatusDN.getInstancia().listarTodos();
			salvamentoSiniestroForm.setListaEstatusSalvamento(listaEstatusSalvamento);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
		} catch (ParseException e) {
	 		UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			reglaNavegacion = Sistema.NO_EXITOSO;
	 	}

		return mapping.findForward(reglaNavegacion);
	}
	
	public void vender(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm) form;
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = new SalvamentoSiniestroDTO();
		IngresoSiniestroDTO ingresoSiniestroDTO = new IngresoSiniestroDTO();
		SalvamentoEstatusDTO salvamentoEstatusDTO = new SalvamentoEstatusDTO();   
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		SalvamentoEstatusDN salvamentoEstatusDN = SalvamentoEstatusDN.getInstancia();
		
		try {
			if (request.getSession().getAttribute("ingresoSiniestroDTO")!=null){
				salvamentoEstatusDTO.setIdTcSalvamentoEstatus(new BigDecimal(SalvamentoEstatusDTO.ESTATUS_VENDIDO));
				salvamentoEstatusDTO = salvamentoEstatusDN.getPorId(salvamentoEstatusDTO);
				
				ingresoSiniestroDTO = (IngresoSiniestroDTO)request.getSession().getAttribute("ingresoSiniestroDTO");
				salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(salvamentoSiniestroForm.getIdToSalvamentoSiniestro()));
				salvamentoSiniestroDTO = salvamentoSiniestroDN.getPorId(salvamentoSiniestroDTO);
				this.poblarDTO(salvamentoSiniestroForm, salvamentoSiniestroDTO, request);
				salvamentoSiniestroDTO.setEstatusInterno(SalvamentoSiniestroDTO.ESTATUS_VENDIDO);
				salvamentoSiniestroDTO.setSalvamentoEstatus(salvamentoEstatusDTO);
				salvamentoSiniestroDN.modificar(salvamentoSiniestroDTO, ingresoSiniestroDTO);
				UtileriasWeb.imprimeMensajeXML("1", "Se realiz� la venta del item seleccionado", response);
			}else{				
				UtileriasWeb.imprimeMensajeXML("0", "Necesita agregar un ingreso para poder guardar", response);	
			}
			
			request.getSession().removeAttribute("salvamentoSiniestroForm");
			this.poblarForm(salvamentoSiniestroDTO, salvamentoSiniestroForm);
			
		} catch (SystemException e) {			
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", "Ocurri� un error al guardar los datos", response);
		} catch (ExcepcionDeAccesoADatos e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", "Ocurri� un error al guardar los datos", response);
		} catch (ParseException e) {
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
			UtileriasWeb.imprimeMensajeXML("0", "Ocurri� un error al guardar los datos", response);
		} 
		finally {
			request.getSession().removeAttribute("salvamentoSiniestroForm");
			request.getSession().removeAttribute("ingresoSiniestroDTO");
		}
	}	
	
	public void guardarSalvamentoSession(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		SalvamentoSiniestroForm salvamentoSiniestroForm = (SalvamentoSiniestroForm)form;
		request.getSession().setAttribute("salvamentoSiniestroForm", salvamentoSiniestroForm);
	}
	/**
	 * Method poblarForm
	 * 
	 * @param SalvamentoSiniestroDTO
	 * @param SalvamentoSiniestroForm
	 */

	private void limpiarForm(SalvamentoSiniestroForm form) throws SystemException{
		String off = "off";
		form.setTieneIVA(off);
		form.setTieneIVARet(off);
		form.setTieneISR(off);
		form.setTieneISRRet(off);
		form.setTieneOtros(off);
		form.setPorcentajeIVA("0.0");
		form.setPorcentajeIVARet("0.0");
		form.setPorcentajeISR("0.0");
		form.setPorcentajeISRRet("0.0");
		form.setPorcentajeOtros("0.0");
		form.setMontoIVA("0.0");
		form.setMontoIVARet("0.0");
		form.setMontoISR("0.0");
		form.setMontoISRRet("0.0");
		form.setMontoOtros("0.0");
		
		form.setPorcentajeCuatoParte("0.0");
		form.setPorcentajeRetencion("0.0");
		form.setPorcentajePrimerExcedente("0.0");
		form.setPorcentajeFacultativo("0.0");
		form.setFechaProvision(UtileriasWeb.getFechaString(new Date()));
	}
	
	private void poblarForm(SalvamentoSiniestroDTO salvamentoSiniestroDTO, SalvamentoSiniestroForm form) {		
		float valorReal = 0;
		String on = "on";
		String off = "off";
		NumberFormat formatter = new DecimalFormat("############.##"); 
		
		if (salvamentoSiniestroDTO==null)return;
		if (salvamentoSiniestroDTO.getReporteSiniestro()!=null && salvamentoSiniestroDTO.getReporteSiniestro().getIdToReporteSiniestro()!=null)
			form.setIdToReporteSiniestro(salvamentoSiniestroDTO.getReporteSiniestro().getIdToReporteSiniestro().toString());
		if (salvamentoSiniestroDTO.getIdToSalvamentoSiniestro()!=null)
			form.setIdToSalvamentoSiniestro(salvamentoSiniestroDTO.getIdToSalvamentoSiniestro().toString());
		if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroDTO.getNombreItem()))
			form.setArticulo(salvamentoSiniestroDTO.getNombreItem());
		if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroDTO.getDescripcionItem()))
			form.setDescripcion(salvamentoSiniestroDTO.getDescripcionItem());
		if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroDTO.getUbicacion()))
			form.setUbicacion(salvamentoSiniestroDTO.getUbicacion());
		if (salvamentoSiniestroDTO.getCantidad()!=null)
			form.setCantidad(salvamentoSiniestroDTO.getCantidad().toString());
		if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroDTO.getUnidadMedida()))
			form.setUnidad(salvamentoSiniestroDTO.getUnidadMedida());
		
		if (salvamentoSiniestroDTO.getSalvamentoEstatus()!=null && salvamentoSiniestroDTO.getSalvamentoEstatus().getIdTcSalvamentoEstatus()!=null){
			form.setEstatusSalvamentoSeleccionado(salvamentoSiniestroDTO.getSalvamentoEstatus().getIdTcSalvamentoEstatus().toString());
			form.setEstatusSalvamentoSeleccionadoDescripcion(salvamentoSiniestroDTO.getSalvamentoEstatus().getDescripcion());
		}
		/*
		if (salvamentoSiniestroDTO.getEstatus()!=null)
			form.setEstatus(salvamentoSiniestroDTO.getEstatus().toString());
		*/
		if (salvamentoSiniestroDTO.getValorEstimado()!=null){
			//form.setValorEstimado(formatter.format(salvamentoSiniestroDTO.getValorEstimado()));
			
			form.setValorEstimado(UtileriasWeb.formatoMoneda(salvamentoSiniestroDTO.getValorEstimado()));
		}
		
		if (salvamentoSiniestroDTO.getFechaProvision()!=null)
			form.setFechaProvision(UtileriasWeb.getFechaString(salvamentoSiniestroDTO.getFechaProvision()));
		
		if (salvamentoSiniestroDTO.getValorReal()!=null){
			//form.setValorVenta(formatter.format(salvamentoSiniestroDTO.getValorReal()));
			
			form.setValorVenta(UtileriasWeb.formatoMoneda(salvamentoSiniestroDTO.getValorReal()));			
		}
		
		if (salvamentoSiniestroDTO.getInspeccion()!=null)
			form.setInspeccionado(salvamentoSiniestroDTO.getInspeccion().toString());
		
		if (salvamentoSiniestroDTO.getPorcentajeIVA()!=null){
			form.setPorcentajeIVA(salvamentoSiniestroDTO.getPorcentajeIVA().toString());
			if (salvamentoSiniestroDTO.getPorcentajeIVA().floatValue()>0){
				form.setTieneIVA(on);
				form.setMontoIVA(String.valueOf(valorReal*salvamentoSiniestroDTO.getPorcentajeIVA().floatValue()/100.0));
			}else{ 
				form.setTieneIVA(off);
				form.setMontoIVA("0.0");
			}
		}
		
		if (salvamentoSiniestroDTO.getPorcentajeIVARetencion()!=null){
			form.setPorcentajeIVARet(salvamentoSiniestroDTO.getPorcentajeIVARetencion().toString());
			if (salvamentoSiniestroDTO.getPorcentajeIVARetencion().floatValue()>0){
				form.setTieneIVARet(on);
				form.setMontoIVARet(String.valueOf(valorReal*salvamentoSiniestroDTO.getPorcentajeIVARetencion().floatValue()/100.0));
			}else{ 
				form.setTieneIVARet(off);
				form.setMontoIVARet("0.0");
			}
		}
		
		if (salvamentoSiniestroDTO.getPorcentajeISR()!=null){
			form.setPorcentajeISR(salvamentoSiniestroDTO.getPorcentajeISR().toString());
			if (salvamentoSiniestroDTO.getPorcentajeISR().floatValue()>0){
				form.setTieneISR(on);
				form.setMontoISR(String.valueOf(valorReal*salvamentoSiniestroDTO.getPorcentajeISR().floatValue()/100.0));
			}else{ 
				form.setTieneISR(off);
				form.setMontoISR("0.0");
			}
		}
		
		if (salvamentoSiniestroDTO.getPorcentajeISRRetencion()!=null){
			form.setPorcentajeISRRet(salvamentoSiniestroDTO.getPorcentajeISRRetencion().toString());
			if (salvamentoSiniestroDTO.getPorcentajeISRRetencion().floatValue()>0){
				form.setTieneISRRet(on);
				form.setMontoISRRet(String.valueOf(valorReal*salvamentoSiniestroDTO.getPorcentajeISRRetencion().floatValue()/100.0));
			}else{ 
				form.setTieneISRRet(off);
				form.setMontoISRRet("0.0");
			}
		}
		
		if (salvamentoSiniestroDTO.getPorcentajeOtros()!=null){
			form.setPorcentajeOtros(salvamentoSiniestroDTO.getPorcentajeOtros().toString());
			if (salvamentoSiniestroDTO.getPorcentajeOtros().floatValue()>0){
				form.setTieneOtros(on);
				form.setMontoOtros(String.valueOf(valorReal*salvamentoSiniestroDTO.getPorcentajeOtros().floatValue()/100.0));
			}else{ 
				form.setTieneOtros(off);
				form.setMontoOtros("0.0");
			}
		}
		
		//TODO se requieren datos de soporteReaseguro
		form.setPorcentajeCuatoParte("0.0");
		form.setPorcentajeRetencion("0.0");
		form.setPorcentajePrimerExcedente("0.0");
		form.setPorcentajeFacultativo("0.0");
		
		if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroDTO.getObservaciones()))
			form.setObservaciones(salvamentoSiniestroDTO.getObservaciones());
		
		if (salvamentoSiniestroDTO.getNumeroFactura()!=null)
			form.setNumFactura(salvamentoSiniestroDTO.getNumeroFactura().toString());
		if (!UtileriasWeb.esCadenaVacia(salvamentoSiniestroDTO.getNombreComprador()));
			form.setComprador(salvamentoSiniestroDTO.getNombreComprador());
		if (salvamentoSiniestroDTO.getSalvamentoPerecedero()!=null)
			form.setTienePerecederos(salvamentoSiniestroDTO.getSalvamentoPerecedero().toString());
		
	}
	
	/**
	 * Method poblarDTO
	 * 
	 * @param SalvamentoSiniestroForm
	 * @param SalvamentoSiniestroDTO
	 * @throws ParseException 
	 */
	private void poblarDTO(SalvamentoSiniestroForm form, SalvamentoSiniestroDTO salvamentoSiniestroDTO, HttpServletRequest request) throws SystemException, ParseException{
		if (form==null)return;
		
		if (!UtileriasWeb.esCadenaVacia(form.getIdToReporteSiniestro())){
			BigDecimal idToReporteSiniestro = new BigDecimal(form.getIdToReporteSiniestro());
			ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(idToReporteSiniestro);
			salvamentoSiniestroDTO.setReporteSiniestro(reporteSiniestroDTO);
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getIdToSalvamentoSiniestro()))
			salvamentoSiniestroDTO.setIdToSalvamentoSiniestro(new BigDecimal(form.getIdToSalvamentoSiniestro()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getArticulo()))
			salvamentoSiniestroDTO.setNombreItem(form.getArticulo());
		
		if (!UtileriasWeb.esCadenaVacia(form.getDescripcion())) {			
			String descripcion = form.getDescripcion();
			if(descripcion.length() > 240){
				salvamentoSiniestroDTO.setDescripcionItem(descripcion.substring(0, 239) );
			}else{
				salvamentoSiniestroDTO.setDescripcionItem(descripcion);
			}
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getUbicacion())) {			
			String ubicacion = form.getUbicacion();
			if(ubicacion.length() > 500){
				salvamentoSiniestroDTO.setUbicacion(ubicacion.substring(0, 499) );
			}else{
				salvamentoSiniestroDTO.setUbicacion(ubicacion);
			}
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getCantidad()))
			salvamentoSiniestroDTO.setCantidad(new BigDecimal(form.getCantidad()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getUnidad()))
			salvamentoSiniestroDTO.setUnidadMedida(form.getUnidad());
		
		if (!UtileriasWeb.esCadenaVacia(form.getEstatusSalvamentoSeleccionado())){
			SalvamentoEstatusDN salvamentoEstatusDN = SalvamentoEstatusDN.getInstancia();
			BigDecimal idTcSalvamentoEstatus = new BigDecimal(form.getEstatusSalvamentoSeleccionado());
			SalvamentoEstatusDTO salvamentoEstatusDTO = new SalvamentoEstatusDTO();
			salvamentoEstatusDTO.setIdTcSalvamentoEstatus(idTcSalvamentoEstatus);
			salvamentoEstatusDTO = salvamentoEstatusDN.getPorId(salvamentoEstatusDTO);
			salvamentoSiniestroDTO.setSalvamentoEstatus(salvamentoEstatusDTO);
		}
		/*
		if (!UtileriasWeb.esCadenaVacia(form.getEstatus()))
			salvamentoSiniestroDTO.setEstatus(form.getEstatus());
		*/
		
		if (!UtileriasWeb.esCadenaVacia(form.getInspeccionado()))
			salvamentoSiniestroDTO.setInspeccion(new Boolean(form.getValorEstimado()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getValorEstimado())){		
			Double valorEstimado = UtileriasWeb.eliminaFormatoMoneda(form.getValorEstimado());
			salvamentoSiniestroDTO.setValorEstimado(valorEstimado);
		}	
		
		if (!UtileriasWeb.esCadenaVacia(form.getFechaProvision()))
			salvamentoSiniestroDTO.setFechaProvision(UtileriasWeb.getFechaFromString(form.getFechaProvision()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getValorVenta())){
			Double valorVenta = UtileriasWeb.eliminaFormatoMoneda(form.getValorVenta());
			salvamentoSiniestroDTO.setValorReal(valorVenta);
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getInspeccionado()))
			if (form.getInspeccionado().equalsIgnoreCase("on"))salvamentoSiniestroDTO.setInspeccion(new Boolean(true));
			else salvamentoSiniestroDTO.setInspeccion(new Boolean(false));
		else salvamentoSiniestroDTO.setInspeccion(new Boolean(false));
		
		if (!UtileriasWeb.esCadenaVacia(form.getPorcentajeIVA()))
			salvamentoSiniestroDTO.setPorcentajeIVA(Double.valueOf(form.getPorcentajeIVA()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getPorcentajeIVARet()))
			salvamentoSiniestroDTO.setPorcentajeIVARetencion(Double.valueOf(form.getPorcentajeIVARet()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getPorcentajeISR()))
			salvamentoSiniestroDTO.setPorcentajeISR(Double.valueOf(form.getPorcentajeISR()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getPorcentajeISRRet()))
			salvamentoSiniestroDTO.setPorcentajeISRRetencion(Double.valueOf(form.getPorcentajeISRRet()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getPorcentajeOtros()))
			salvamentoSiniestroDTO.setPorcentajeOtros(Double.valueOf(form.getPorcentajeOtros()));
				
		if (!UtileriasWeb.esCadenaVacia(form.getObservaciones())) {			
			String observaciones = form.getObservaciones();
			if(observaciones.length() > 240){
				salvamentoSiniestroDTO.setObservaciones(observaciones.substring(0, 239) );
			}else{
				salvamentoSiniestroDTO.setObservaciones(observaciones);
			}
		}
		if (!UtileriasWeb.esCadenaVacia(form.getNumFactura()))
			salvamentoSiniestroDTO.setNumeroFactura(new BigDecimal(form.getNumFactura()));
		
		if (!UtileriasWeb.esCadenaVacia(form.getComprador())) {			
			String comprador = form.getComprador();
			if(comprador.length() > 200){
				salvamentoSiniestroDTO.setObservaciones(comprador.substring(0, 199) );
			}else{
				salvamentoSiniestroDTO.setObservaciones(comprador);
			}
		}
		
		if (!UtileriasWeb.esCadenaVacia(form.getTienePerecederos())){
			if (form.getTienePerecederos().equalsIgnoreCase("on"))salvamentoSiniestroDTO.setSalvamentoPerecedero(new Boolean(true));
			else{
				salvamentoSiniestroDTO.setObservaciones("");
				salvamentoSiniestroDTO.setSalvamentoPerecedero(new Boolean(false));
			}
		}else{
			salvamentoSiniestroDTO.setObservaciones("");
			salvamentoSiniestroDTO.setSalvamentoPerecedero(new Boolean(false));
		}
		
		this.setDatosModiificacion(salvamentoSiniestroDTO, request);
	}

}
