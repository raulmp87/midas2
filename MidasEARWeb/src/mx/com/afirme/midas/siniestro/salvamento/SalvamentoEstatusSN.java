package mx.com.afirme.midas.siniestro.salvamento;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SalvamentoEstatusSN {
	private SalvamentoEstatusFacadeRemote beanRemoto;

	public SalvamentoEstatusSN() throws SystemException {
		try{
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(SalvamentoEstatusFacadeRemote.class);
		}catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
	
	public SalvamentoEstatusDTO getPorId(SalvamentoEstatusDTO salvamentoEstatusDTO) throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findById(salvamentoEstatusDTO.getIdTcSalvamentoEstatus());
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SalvamentoEstatusDTO> listarTodos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findAll();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}
	
	public List<SalvamentoEstatusDTO> listarNoVendidos() throws ExcepcionDeAccesoADatos{
		try{
			return beanRemoto.findNotSaled();
		}catch(EJBTransactionRolledbackException e){
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	} 
}
