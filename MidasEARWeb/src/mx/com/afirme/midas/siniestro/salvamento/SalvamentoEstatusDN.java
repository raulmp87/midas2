package mx.com.afirme.midas.siniestro.salvamento;

import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class SalvamentoEstatusDN {
	public static final SalvamentoEstatusDN INSTANCIA = new SalvamentoEstatusDN();
	
	public static SalvamentoEstatusDN getInstancia (){
		return SalvamentoEstatusDN.INSTANCIA;
	}
	
	public SalvamentoEstatusDTO getPorId(SalvamentoEstatusDTO salvamentoEstatusDTO) throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoEstatusSN salvamentoEstatusSN = new SalvamentoEstatusSN();
		return salvamentoEstatusSN.getPorId(salvamentoEstatusDTO);
	}
	
	public List<SalvamentoEstatusDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoEstatusSN salvamentoEstatusSN = new SalvamentoEstatusSN();
		return salvamentoEstatusSN.listarTodos();
	}
	public List<SalvamentoEstatusDTO> listarNoVendidos() throws ExcepcionDeAccesoADatos, SystemException{
		SalvamentoEstatusSN salvamentoEstatusSN = new SalvamentoEstatusSN();
		return salvamentoEstatusSN.listarNoVendidos();
	}
}
