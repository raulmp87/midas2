package mx.com.afirme.midas.siniestro.salvamento;

import java.util.List;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class SalvamentoSiniestroForm extends MidasBaseForm {
	private static final long serialVersionUID = 2809431654209639217L;

	private String idToReporteSiniestro;
	private String idToSalvamentoSiniestro;
	private String idToIngresoSiniestro;
	
	private String articulo;
	private String descripcion;
	private String ubicacion;
	private String cantidad;
	private String unidad;
	private String estatus;	
	private String valorEstimado;	
	private String fechaProvision;
	private String valorVenta;
	private String inspeccionado;	
	
	private String tieneIVA;
	private String tieneIVARet;
	private String tieneISR;
	private String tieneISRRet;
	private String tieneOtros;
	
	private String porcentajeIVA;
	private String porcentajeIVARet;
	private String porcentajeISR;
	private String porcentajeISRRet;
	private String porcentajeOtros;
	
	private String montoIVA;
	private String montoIVARet;
	private String montoISR;
	private String montoISRRet;
	private String montoOtros;
	
	private String porcentajeCuatoParte;
	private String porcentajeRetencion;
	private String porcentajePrimerExcedente;
	private String porcentajeFacultativo;
	private String observaciones;
	private String numFactura;
	private String comprador;
	private String tienePerecederos;
		
	private String valorVentaTotal;
	private String valorEstimadoTotal;
	private List<SalvamentoEstatusDTO> listaEstatusSalvamento;
	private String estatusSalvamentoSeleccionado;
	private String estatusSalvamentoSeleccionadoDescripcion;
	
	public String getEstatusSalvamentoSeleccionadoDescripcion() {
		return estatusSalvamentoSeleccionadoDescripcion;
	}
	public void setEstatusSalvamentoSeleccionadoDescripcion(
			String estatusSalvamentoSeleccionadoDescripcion) {
		this.estatusSalvamentoSeleccionadoDescripcion = estatusSalvamentoSeleccionadoDescripcion;
	}
	public List<SalvamentoEstatusDTO> getListaEstatusSalvamento() {
		return listaEstatusSalvamento;
	}
	public void setListaEstatusSalvamento(
			List<SalvamentoEstatusDTO> listaEstatusSalvamento) {
		this.listaEstatusSalvamento = listaEstatusSalvamento;
	}
	
	public String getEstatusSalvamentoSeleccionado() {
		return estatusSalvamentoSeleccionado;
	}
	public void setEstatusSalvamentoSeleccionado(
			String estatusSalvamentoSeleccionado) {
		this.estatusSalvamentoSeleccionado = estatusSalvamentoSeleccionado;
	}
	public String getIdToIngresoSiniestro() {
		return idToIngresoSiniestro;
	}
	public void setIdToIngresoSiniestro(String idToIngresoSiniestro) {
		this.idToIngresoSiniestro = idToIngresoSiniestro;
	}
	public String getValorVentaTotal() {
		return valorVentaTotal;
	}
	public void setValorVentaTotal(String valorVentaTotal) {
		this.valorVentaTotal = valorVentaTotal;
	}
	public String getValorEstimadoTotal() {
		return valorEstimadoTotal;
	}
	public void setValorEstimadoTotal(String valorEstimadoTotal) {
		this.valorEstimadoTotal = valorEstimadoTotal;
	}
	public String getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(String idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public String getIdToSalvamentoSiniestro() {
		return idToSalvamentoSiniestro;
	}
	public void setIdToSalvamentoSiniestro(String idToSalvamentoSiniestro) {
		this.idToSalvamentoSiniestro = idToSalvamentoSiniestro;
	}
	public String getArticulo() {
		return articulo;
	}
	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getValorEstimado() {
		return valorEstimado;
	}
	public void setValorEstimado(String valorEstimado) {
		this.valorEstimado = valorEstimado;
	}
	public String getFechaProvision() {
		return fechaProvision;
	}
	public void setFechaProvision(String fechaProvision) {
		this.fechaProvision = fechaProvision;
	}
	public String getValorVenta() {
		return valorVenta;
	}
	public void setValorVenta(String valorVenta) {
		this.valorVenta = valorVenta;
	}
	public String getInspeccionado() {
		return inspeccionado;
	}
	public void setInspeccionado(String inspeccionado) {
		this.inspeccionado = inspeccionado;
	}
	public String getTieneIVA() {
		return tieneIVA;
	}
	public void setTieneIVA(String tieneIVA) {
		this.tieneIVA = tieneIVA;
	}
	public String getTieneIVARet() {
		return tieneIVARet;
	}
	public void setTieneIVARet(String tieneIVARet) {
		this.tieneIVARet = tieneIVARet;
	}
	public String getTieneISR() {
		return tieneISR;
	}
	public void setTieneISR(String tieneISR) {
		this.tieneISR = tieneISR;
	}
	public String getTieneISRRet() {
		return tieneISRRet;
	}
	public void setTieneISRRet(String tieneISRRet) {
		this.tieneISRRet = tieneISRRet;
	}
	public String getTieneOtros() {
		return tieneOtros;
	}
	public void setTieneOtros(String tieneOtros) {
		this.tieneOtros = tieneOtros;
	}
	public String getPorcentajeIVA() {
		return porcentajeIVA;
	}
	public void setPorcentajeIVA(String porcentajeIVA) {
		this.porcentajeIVA = porcentajeIVA;
	}
	public String getPorcentajeIVARet() {
		return porcentajeIVARet;
	}
	public void setPorcentajeIVARet(String porcentajeIVARet) {
		this.porcentajeIVARet = porcentajeIVARet;
	}
	public String getPorcentajeISR() {
		return porcentajeISR;
	}
	public void setPorcentajeISR(String porcentajeISR) {
		this.porcentajeISR = porcentajeISR;
	}
	public String getPorcentajeISRRet() {
		return porcentajeISRRet;
	}
	public void setPorcentajeISRRet(String porcentajeISRRet) {
		this.porcentajeISRRet = porcentajeISRRet;
	}
	public String getPorcentajeOtros() {
		return porcentajeOtros;
	}
	public void setPorcentajeOtros(String porcentajeOtros) {
		this.porcentajeOtros = porcentajeOtros;
	}
	public String getMontoIVA() {
		return montoIVA;
	}
	public void setMontoIVA(String montoIVA) {
		this.montoIVA = montoIVA;
	}
	public String getMontoIVARet() {
		return montoIVARet;
	}
	public void setMontoIVARet(String montoIVARet) {
		this.montoIVARet = montoIVARet;
	}
	public String getMontoISR() {
		return montoISR;
	}
	public void setMontoISR(String montoISR) {
		this.montoISR = montoISR;
	}
	public String getMontoISRRet() {
		return montoISRRet;
	}
	public void setMontoISRRet(String montoISRRet) {
		this.montoISRRet = montoISRRet;
	}
	public String getMontoOtros() {
		return montoOtros;
	}
	public void setMontoOtros(String montoOtros) {
		this.montoOtros = montoOtros;
	}
	public String getPorcentajeCuatoParte() {
		return porcentajeCuatoParte;
	}
	public void setPorcentajeCuatoParte(String porcentajeCuatoParte) {
		this.porcentajeCuatoParte = porcentajeCuatoParte;
	}
	public String getPorcentajeRetencion() {
		return porcentajeRetencion;
	}
	public void setPorcentajeRetencion(String porcentajeRetencion) {
		this.porcentajeRetencion = porcentajeRetencion;
	}
	public String getPorcentajePrimerExcedente() {
		return porcentajePrimerExcedente;
	}
	public void setPorcentajePrimerExcedente(String porcentajePrimerExcedente) {
		this.porcentajePrimerExcedente = porcentajePrimerExcedente;
	}
	public String getPorcentajeFacultativo() {
		return porcentajeFacultativo;
	}
	public void setPorcentajeFacultativo(String porcentajeFacultativo) {
		this.porcentajeFacultativo = porcentajeFacultativo;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getNumFactura() {
		return numFactura;
	}
	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}
	public String getComprador() {
		return comprador;
	}
	public void setComprador(String comprador) {
		this.comprador = comprador;
	}
	public String getTienePerecederos() {
		return tienePerecederos;
	}
	public void setTienePerecederos(String tienePerecederos) {
		this.tienePerecederos = tienePerecederos;
	}
}
