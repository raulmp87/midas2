package mx.com.afirme.midas.siniestro.reportes;

import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDTO;

public class RegistroSalvamento{

	private static final long serialVersionUID = 1L;
	
	private SalvamentoSiniestroDTO salvamento;
	private Double salvamentoMonto;
	private Double salvamentoCuotaParte;
	private Double salvamentoFacultativo;
	private Double salvamentoPrimerExcedente;
	private Double salvamentoRetencion;
	
	public SalvamentoSiniestroDTO getSalvamento() {
		return salvamento;
	}
	public void setSalvamento(SalvamentoSiniestroDTO salvamento) {
		this.salvamento = salvamento;
	}
	public Double getSalvamentoMonto() {
		return salvamentoMonto;
	}
	public void setSalvamentoMonto(Double salvamentoMonto) {
		this.salvamentoMonto = salvamentoMonto;
	}
	public Double getSalvamentoCuotaParte() {
		return salvamentoCuotaParte;
	}
	public void setSalvamentoCuotaParte(Double salvamentoCuotaParte) {
		this.salvamentoCuotaParte = salvamentoCuotaParte;
	}
	public Double getSalvamentoFacultativo() {
		return salvamentoFacultativo;
	}
	public void setSalvamentoFacultativo(Double salvamentoFacultativo) {
		this.salvamentoFacultativo = salvamentoFacultativo;
	}
	public Double getSalvamentoPrimerExcedente() {
		return salvamentoPrimerExcedente;
	}
	public void setSalvamentoPrimerExcedente(Double salvamentoPrimerExcedente) {
		this.salvamentoPrimerExcedente = salvamentoPrimerExcedente;
	}
	public Double getSalvamentoRetencion() {
		return salvamentoRetencion;
	}
	public void setSalvamentoRetencion(Double salvamentoRetencion) {
		this.salvamentoRetencion = salvamentoRetencion;
	}
		
}