package mx.com.afirme.midas.siniestro.reportes;

import java.util.List;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleBean;
import mx.com.afirme.midas.sistema.MidasBaseForm;

public class CaratulaSiniestroForm extends MidasBaseForm{

	private static final long serialVersionUID = 1L;
	
	//Datos del reporte
	private String idReporteSiniestro;
	private String numeroReporte;
	private String idAjustadorReporte;
	private String nombreAjustadorReporte;
	private String fechaHoraReporte;
	private String fechaOcurrioReporte;
	private String descripcionSiniestro;
	private String calleSiniestro;
	private String coloniaSiniestro;
	private String codigoPostalSiniestro;
	private String municipioSiniestro;
	private String estadoSiniestro;
	//Datos de la poliza
	private String tipoMonedaPoliza;
	private String numeroPoliza;
	private String claveOficinaPoliza;
	private String endosoPoliza;
	private String claveProductoPoliza;
	private String incisoPoliza;
	private String claveSubRamoPoliza;
	private String descripcionSubRamoPoliza;
	private String fechaEmisionPoliza;
	private String fechaInicioVigenciaPoliza;
	private String fechaTerminacionVigenciaPoliza;
	private String idAseguradoPoliza;
	private String nombreAseguradoPoliza;
	private String callePoliza;
	private String coloniaPoliza;
	private String codigoPostalPoliza;
	private String municipioPoliza;
	private String estadoPoliza;
	private String idAgentePoliza;
	private String nombreAgentePoliza;
	//Datos Cobranza
	private String formaPagoPoliza;
	private String ultimoReciboPagadoPoliza;
	private String estatusCobranza;
	private String fechaPagoPoliza;
	private String montoPagoPoliza;
	private String saldoPendientePoliza;
	private String saldoVencidoPoliza;
	private List<ReservaDetalleBean> listaSumaAsegurada;
	private List<CaratulaSiniestroBean> listaPorcentajesSumaAsegurada;
	private List<CaratulaSiniestroBean> listaEstimacionInicial;
	private List<CaratulaSiniestroBean> listaIndemnizaciones;
	private List<CaratulaSiniestroBean> listaGastos;
	private List<CaratulaSiniestroBean> listaIngresos;
	private List<CaratulaSiniestroBean> listaSalvamentos;

	
	public List<CaratulaSiniestroBean> getListaPorcentajesSumaAsegurada() {
		return listaPorcentajesSumaAsegurada;
	}

	public void setListaPorcentajesSumaAsegurada(
			List<CaratulaSiniestroBean> listaPorcentajesSumaAsegurada) {
		this.listaPorcentajesSumaAsegurada = listaPorcentajesSumaAsegurada;
	}

	public String getIdReporteSiniestro() {
		return idReporteSiniestro;
	}

	public void setIdReporteSiniestro(String idReporteSiniestro) {
		this.idReporteSiniestro = idReporteSiniestro;
	}

	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public String getIdAjustadorReporte() {
		return idAjustadorReporte;
	}

	public void setIdAjustadorReporte(String idAjustadorReporte) {
		this.idAjustadorReporte = idAjustadorReporte;
	}

	public String getNombreAjustadorReporte() {
		return nombreAjustadorReporte;
	}

	public void setNombreAjustadorReporte(String nombreAjustadorReporte) {
		this.nombreAjustadorReporte = nombreAjustadorReporte;
	}

	public String getFechaHoraReporte() {
		return fechaHoraReporte;
	}

	public void setFechaHoraReporte(String fechaHoraReporte) {
		this.fechaHoraReporte = fechaHoraReporte;
	}

	public String getFechaOcurrioReporte() {
		return fechaOcurrioReporte;
	}

	public void setFechaOcurrioReporte(String fechaOcurrioReporte) {
		this.fechaOcurrioReporte = fechaOcurrioReporte;
	}

	public String getDescripcionSiniestro() {
		return descripcionSiniestro;
	}

	public void setDescripcionSiniestro(String descripcionSiniestro) {
		this.descripcionSiniestro = descripcionSiniestro;
	}

	public String getCalleSiniestro() {
		return calleSiniestro;
	}

	public void setCalleSiniestro(String calleSiniestro) {
		this.calleSiniestro = calleSiniestro;
	}

	public String getColoniaSiniestro() {
		return coloniaSiniestro;
	}

	public void setColoniaSiniestro(String coloniaSiniestro) {
		this.coloniaSiniestro = coloniaSiniestro;
	}

	public String getCodigoPostalSiniestro() {
		return codigoPostalSiniestro;
	}

	public void setCodigoPostalSiniestro(String codigoPostalSiniestro) {
		this.codigoPostalSiniestro = codigoPostalSiniestro;
	}

	public String getMunicipioSiniestro() {
		return municipioSiniestro;
	}

	public void setMunicipioSiniestro(String municipioSiniestro) {
		this.municipioSiniestro = municipioSiniestro;
	}

	public String getEstadoSiniestro() {
		return estadoSiniestro;
	}

	public void setEstadoSiniestro(String estadoSiniestro) {
		this.estadoSiniestro = estadoSiniestro;
	}

	public String getTipoMonedaPoliza() {
		return tipoMonedaPoliza;
	}

	public void setTipoMonedaPoliza(String tipoMonedaPoliza) {
		this.tipoMonedaPoliza = tipoMonedaPoliza;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getClaveOficinaPoliza() {
		return claveOficinaPoliza;
	}

	public void setClaveOficinaPoliza(String claveOficinaPoliza) {
		this.claveOficinaPoliza = claveOficinaPoliza;
	}

	public String getEndosoPoliza() {
		return endosoPoliza;
	}

	public void setEndosoPoliza(String endosoPoliza) {
		this.endosoPoliza = endosoPoliza;
	}

	public String getClaveProductoPoliza() {
		return claveProductoPoliza;
	}

	public void setClaveProductoPoliza(String claveProductoPoliza) {
		this.claveProductoPoliza = claveProductoPoliza;
	}

	public String getIncisoPoliza() {
		return incisoPoliza;
	}

	public void setIncisoPoliza(String incisoPoliza) {
		this.incisoPoliza = incisoPoliza;
	}

	public String getClaveSubRamoPoliza() {
		return claveSubRamoPoliza;
	}

	public void setClaveSubRamoPoliza(String claveSubRamoPoliza) {
		this.claveSubRamoPoliza = claveSubRamoPoliza;
	}

	public String getDescripcionSubRamoPoliza() {
		return descripcionSubRamoPoliza;
	}

	public void setDescripcionSubRamoPoliza(String descripcionSubRamoPoliza) {
		this.descripcionSubRamoPoliza = descripcionSubRamoPoliza;
	}

	public String getFechaEmisionPoliza() {
		return fechaEmisionPoliza;
	}

	public void setFechaEmisionPoliza(String fechaEmisionPoliza) {
		this.fechaEmisionPoliza = fechaEmisionPoliza;
	}

	public String getFechaInicioVigenciaPoliza() {
		return fechaInicioVigenciaPoliza;
	}

	public void setFechaInicioVigenciaPoliza(String fechaInicioVigenciaPoliza) {
		this.fechaInicioVigenciaPoliza = fechaInicioVigenciaPoliza;
	}

	public String getFechaTerminacionVigenciaPoliza() {
		return fechaTerminacionVigenciaPoliza;
	}

	public void setFechaTerminacionVigenciaPoliza(
			String fechaTerminacionVigenciaPoliza) {
		this.fechaTerminacionVigenciaPoliza = fechaTerminacionVigenciaPoliza;
	}

	public String getIdAseguradoPoliza() {
		return idAseguradoPoliza;
	}

	public void setIdAseguradoPoliza(String idAseguradoPoliza) {
		this.idAseguradoPoliza = idAseguradoPoliza;
	}

	public String getNombreAseguradoPoliza() {
		return nombreAseguradoPoliza;
	}

	public void setNombreAseguradoPoliza(String nombreAseguradoPoliza) {
		this.nombreAseguradoPoliza = nombreAseguradoPoliza;
	}

	public String getCallePoliza() {
		return callePoliza;
	}

	public void setCallePoliza(String callePoliza) {
		this.callePoliza = callePoliza;
	}

	public String getColoniaPoliza() {
		return coloniaPoliza;
	}

	public void setColoniaPoliza(String coloniaPoliza) {
		this.coloniaPoliza = coloniaPoliza;
	}

	public String getCodigoPostalPoliza() {
		return codigoPostalPoliza;
	}

	public void setCodigoPostalPoliza(String codigoPostalPoliza) {
		this.codigoPostalPoliza = codigoPostalPoliza;
	}

	public String getMunicipioPoliza() {
		return municipioPoliza;
	}

	public void setMunicipioPoliza(String municipioPoliza) {
		this.municipioPoliza = municipioPoliza;
	}

	public String getEstadoPoliza() {
		return estadoPoliza;
	}

	public void setEstadoPoliza(String estadoPoliza) {
		this.estadoPoliza = estadoPoliza;
	}

	public String getIdAgentePoliza() {
		return idAgentePoliza;
	}

	public void setIdAgentePoliza(String idAgentePoliza) {
		this.idAgentePoliza = idAgentePoliza;
	}

	public String getNombreAgentePoliza() {
		return nombreAgentePoliza;
	}

	public void setNombreAgentePoliza(String nombreAgentePoliza) {
		this.nombreAgentePoliza = nombreAgentePoliza;
	}

	public String getFormaPagoPoliza() {
		return formaPagoPoliza;
	}

	public void setFormaPagoPoliza(String formaPagoPoliza) {
		this.formaPagoPoliza = formaPagoPoliza;
	}

	public String getUltimoReciboPagadoPoliza() {
		return ultimoReciboPagadoPoliza;
	}

	public void setUltimoReciboPagadoPoliza(String ultimoReciboPagadoPoliza) {
		this.ultimoReciboPagadoPoliza = ultimoReciboPagadoPoliza;
	}

	public String getEstatusCobranza() {
		return estatusCobranza;
	}

	public void setEstatusCobranza(String estatusCobranza) {
		this.estatusCobranza = estatusCobranza;
	}

	public String getFechaPagoPoliza() {
		return fechaPagoPoliza;
	}

	public void setFechaPagoPoliza(String fechaPagoPoliza) {
		this.fechaPagoPoliza = fechaPagoPoliza;
	}

	public String getMontoPagoPoliza() {
		return montoPagoPoliza;
	}

	public void setMontoPagoPoliza(String montoPagoPoliza) {
		this.montoPagoPoliza = montoPagoPoliza;
	}

	public String getSaldoPendientePoliza() {
		return saldoPendientePoliza;
	}

	public void setSaldoPendientePoliza(String saldoPendientePoliza) {
		this.saldoPendientePoliza = saldoPendientePoliza;
	}

	public String getSaldoVencidoPoliza() {
		return saldoVencidoPoliza;
	}

	public void setSaldoVencidoPoliza(String saldoVencidoPoliza) {
		this.saldoVencidoPoliza = saldoVencidoPoliza;
	}

	public List<ReservaDetalleBean> getListaSumaAsegurada() {
		return listaSumaAsegurada;
	}

	public void setListaSumaAsegurada(List<ReservaDetalleBean> listaSumaAsegurada) {
		this.listaSumaAsegurada = listaSumaAsegurada;
	}

	public List<CaratulaSiniestroBean> getListaEstimacionInicial() {
		return listaEstimacionInicial;
	}

	public void setListaEstimacionInicial(
			List<CaratulaSiniestroBean> listaEstimacionInicial) {
		this.listaEstimacionInicial = listaEstimacionInicial;
	}

	public List<CaratulaSiniestroBean> getListaIndemnizaciones() {
		return listaIndemnizaciones;
	}

	public void setListaIndemnizaciones(
			List<CaratulaSiniestroBean> listaIndemnizaciones) {
		this.listaIndemnizaciones = listaIndemnizaciones;
	}

	public List<CaratulaSiniestroBean> getListaGastos() {
		return listaGastos;
	}

	public void setListaGastos(List<CaratulaSiniestroBean> listaGastos) {
		this.listaGastos = listaGastos;
	}

	public List<CaratulaSiniestroBean> getListaIngresos() {
		return listaIngresos;
	}

	public void setListaIngresos(List<CaratulaSiniestroBean> listaIngresos) {
		this.listaIngresos = listaIngresos;
	}

	public List<CaratulaSiniestroBean> getListaSalvamentos() {
		return listaSalvamentos;
	}

	public void setListaSalvamentos(List<CaratulaSiniestroBean> listaSalvamentos) {
		this.listaSalvamentos = listaSalvamentos;
	}

	public JRBeanCollectionDataSource getListaSumaAseguradaJasper(){
		return new JRBeanCollectionDataSource(this.listaSumaAsegurada);
	}
	
	public JRBeanCollectionDataSource getListaPorcentajesSumaAseguradaJasper() {
		return new JRBeanCollectionDataSource(this.listaPorcentajesSumaAsegurada);
	}
	
	public JRBeanCollectionDataSource getListaEstimacionInicialJasper(){
		return new JRBeanCollectionDataSource(this.listaEstimacionInicial);
	}
	
	public JRBeanCollectionDataSource getListaIndemnizacionesJasper(){
		return new JRBeanCollectionDataSource(this.listaIndemnizaciones);
	}
	
	public JRBeanCollectionDataSource getListaGastosJasper(){
		return new JRBeanCollectionDataSource(this.listaGastos);
	}
	
	public JRBeanCollectionDataSource getListaIngresosJasper(){
		return new JRBeanCollectionDataSource(this.listaIngresos);
	}
	
	public JRBeanCollectionDataSource getListaSalvamentosJasper(){
		return new JRBeanCollectionDataSource(this.listaSalvamentos);
	}
	
}
