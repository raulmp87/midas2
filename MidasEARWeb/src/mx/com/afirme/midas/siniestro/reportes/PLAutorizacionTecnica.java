package mx.com.afirme.midas.siniestro.reportes; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PLAutorizacionTecnica extends MidasPlantillaBase{
	private AutorizacionTecnicaForm autorizacionTecnicaForm;
	
	public PLAutorizacionTecnica(){
		
	}
	
	public PLAutorizacionTecnica(AutorizacionTecnicaForm autorizacionTecnicaForm, String tipoAutorizacionTecnica){
		super();
		String nombreSubReporte=new String();
		String jasperFile = new String();
		this.autorizacionTecnicaForm = autorizacionTecnicaForm;
		setListaRegistrosContenido( new ArrayList<Object>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		
		if (tipoAutorizacionTecnica.equalsIgnoreCase("Gasto")){
			setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.ordenPagoGasto"));
		}
		if (tipoAutorizacionTecnica.equalsIgnoreCase("Indemnizacion")){
			setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.ordenPagoIndemnizacion"));
		}
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.paquete"));
		parametros.put("PIMAGE", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.distribucionAT");
		JasperReport subReporteDistribucion = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_DISTRIBUCION", subReporteDistribucion);
			
		setParametrosVariablesReporte(parametros);
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.autorizacionTecnicaForm==null)return;
		getListaRegistrosContenido().add(this.autorizacionTecnicaForm);
		try {
			super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			e.printStackTrace();
			setByteArrayReport( null );
		}
	}
	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
}