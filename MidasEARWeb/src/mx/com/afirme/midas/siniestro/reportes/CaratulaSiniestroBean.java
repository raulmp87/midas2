/**
 * 
 */
package mx.com.afirme.midas.siniestro.reportes;

import java.math.BigDecimal;

/**
 * @author ERIC
 *
 */
public class CaratulaSiniestroBean implements java.io.Serializable {
	
	private static final long serialVersionUID = -1273192730740942533L;
	private BigDecimal idToReporteSiniestro;
	private BigDecimal idToPoliza;
	private BigDecimal numeroInciso;
	private BigDecimal numeroSubInciso;
	private BigDecimal idToGastoSiniestro;
	private BigDecimal idToIngresoSiniestro;
	private BigDecimal idToIndemnizacion;
	
	private Short numeroEndoso;
	private Double monto;
	private Double montoCuotaParte;
	private Double montoPrimerExcedente;
	private Double montoFacultativo;
	private Double montoRetencion;
	
	private String descripcionSeccion;
	private String descripcionCobertura;
	private String descripcionRamo;
	private String descripcionSubRamo;
	private String descripcionRiesgo;
	
	//Para Coberturas
	private Double estimacionInicial;
	
	//Para Ingresos
	private String nombrePrestadorServicio;
	private String fechaHoraIngreso;
	//Para salvamento
	private String nombreItem;

	//para gastos
	private String fechaHoraGasto;
	
	//para indemnizaciones
	private String fechaHoraIndemnizacion;
	
	
	public String getFechaHoraIngreso() {
		return fechaHoraIngreso;
	}
	public void setFechaHoraIngreso(String fechaHoraIngreso) {
		this.fechaHoraIngreso = fechaHoraIngreso;
	}
	public String getFechaHoraGasto() {
		return fechaHoraGasto;
	}
	public void setFechaHoraGasto(String fechaHoraGasto) {
		this.fechaHoraGasto = fechaHoraGasto;
	}
	public BigDecimal getIdToIndemnizacion() {
		return idToIndemnizacion;
	}
	public void setIdToIndemnizacion(BigDecimal idToIndemnizacion) {
		this.idToIndemnizacion = idToIndemnizacion;
	}
	public String getFechaHoraIndemnizacion() {
		return fechaHoraIndemnizacion;
	}
	public void setFechaHoraIndemnizacion(String fechaHoraIndemnizacion) {
		this.fechaHoraIndemnizacion = fechaHoraIndemnizacion;
	}
	public Double getEstimacionInicial() {
		return estimacionInicial;
	}
	public String getDescripcionRiesgo() {
		return descripcionRiesgo;
	}
	public void setDescripcionRiesgo(String descripcionRiesgo) {
		this.descripcionRiesgo = descripcionRiesgo;
	}
	public void setEstimacionInicial(Double estimacionInicial) {
		this.estimacionInicial = estimacionInicial;
	}
	public BigDecimal getIdToReporteSiniestro() {
		return idToReporteSiniestro;
	}
	public void setIdToReporteSiniestro(BigDecimal idToReporteSiniestro) {
		this.idToReporteSiniestro = idToReporteSiniestro;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public BigDecimal getNumeroSubInciso() {
		return numeroSubInciso;
	}
	public void setNumeroSubInciso(BigDecimal numeroSubInciso) {
		this.numeroSubInciso = numeroSubInciso;
	}
	public BigDecimal getIdToGastoSiniestro() {
		return idToGastoSiniestro;
	}
	public void setIdToGastoSiniestro(BigDecimal idToGastoSiniestro) {
		this.idToGastoSiniestro = idToGastoSiniestro;
	}
	public BigDecimal getIdToIngresoSiniestro() {
		return idToIngresoSiniestro;
	}
	public void setIdToIngresoSiniestro(BigDecimal idToIngresoSiniestro) {
		this.idToIngresoSiniestro = idToIngresoSiniestro;
	}
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}
	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
	public Double getMontoCuotaParte() {
		return montoCuotaParte;
	}
	public void setMontoCuotaParte(Double montoCuotaParte) {
		this.montoCuotaParte = montoCuotaParte;
	}
	public Double getMontoPrimerExcedente() {
		return montoPrimerExcedente;
	}
	public void setMontoPrimerExcedente(Double montoPrimerExcedente) {
		this.montoPrimerExcedente = montoPrimerExcedente;
	}
	public Double getMontoFacultativo() {
		return montoFacultativo;
	}
	public void setMontoFacultativo(Double montoFacultativo) {
		this.montoFacultativo = montoFacultativo;
	}
	public Double getMontoRetencion() {
		return montoRetencion;
	}
	public void setMontoRetencion(Double montoRetencion) {
		this.montoRetencion = montoRetencion;
	}
	public String getDescripcionSeccion() {
		return descripcionSeccion;
	}
	public void setDescripcionSeccion(String descripcionSeccion) {
		this.descripcionSeccion = descripcionSeccion;
	}
	public String getDescripcionCobertura() {
		return descripcionCobertura;
	}
	public void setDescripcionCobertura(String descripcionCobertura) {
		this.descripcionCobertura = descripcionCobertura;
	}
	public String getDescripcionRamo() {
		return descripcionRamo;
	}
	public void setDescripcionRamo(String descripcionRamo) {
		this.descripcionRamo = descripcionRamo;
	}
	public String getDescripcionSubRamo() {
		return descripcionSubRamo;
	}
	public void setDescripcionSubRamo(String descripcionSubRamo) {
		this.descripcionSubRamo = descripcionSubRamo;
	}
	public String getNombrePrestadorServicio() {
		return nombrePrestadorServicio;
	}
	public void setNombrePrestadorServicio(String nombrePrestadorServicio) {
		this.nombrePrestadorServicio = nombrePrestadorServicio;
	}
	public String getNombreItem() {
		return nombreItem;
	}
	public void setNombreItem(String nombreItem) {
		this.nombreItem = nombreItem;
	}
	
}
