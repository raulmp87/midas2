package mx.com.afirme.midas.siniestro.reportes;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.consultacobranzapoliza.ConsultaCobranzaPolizaDN;
import mx.com.afirme.midas.interfaz.consultacobranzapoliza.DatosCobranzaPolizaDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDN;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDN;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDN;
import mx.com.afirme.midas.siniestro.finanzas.RiesgoAfectadoDTO;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDN;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresosDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.siniestro.finanzas.reserva.ReservaDetalleDN;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDN;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class CaratulaSiniestroAction extends MidasMappingDispatchAction {
	
	public ActionForward mostrarCaratulaSiniestro (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		CaratulaSiniestroDN caratulaSiniestroDN = CaratulaSiniestroDN.getInstancia();
		RiesgoAfectadoDN riesgoAfectadoDN = RiesgoAfectadoDN.getInstancia();		
		ReservaDetalleDN reservaDetalleDN = ReservaDetalleDN.getInstancia();
		ReservaDN reservaDN = ReservaDN.getInstancia();
		IndemnizacionDN indemnizacionDN = IndemnizacionDN.getInstancia();
		GastoSiniestroDN gastoSiniestroDN = GastoSiniestroDN.getInstancia();
		IngresoSiniestroDN ingresoSiniestroDN = IngresoSiniestroDN.getInstancia();
		SalvamentoSiniestroDN salvamentoSiniestroDN = SalvamentoSiniestroDN.getInstancia();
		ConsultaCobranzaPolizaDN consultaCobranzaPolizaDN = ConsultaCobranzaPolizaDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		
		String nombreUsuario = UtileriasWeb.obtieneNombreUsuario(request);
		
		List<SoporteDistribucionReaseguroDTO> listaSoporteDistribucionReaseguro = new ArrayList<SoporteDistribucionReaseguroDTO>();
		Map<String, SoporteDistribucionReaseguroDTO> mapaSoporteDistribucionReaseguro = new HashMap<String, SoporteDistribucionReaseguroDTO>(); 
		
		List<CaratulaSiniestroBean> porcentajesSumaAseguradaCaratula = new ArrayList<CaratulaSiniestroBean>();
		List<CaratulaSiniestroBean> reservasCaratula = new ArrayList<CaratulaSiniestroBean>();
		List<CaratulaSiniestroBean> indemnizacionesCaratula = new ArrayList<CaratulaSiniestroBean>();
		List<CaratulaSiniestroBean> gastosCaratula = new ArrayList<CaratulaSiniestroBean>();
		List<CaratulaSiniestroBean> ingresosCaratula = new ArrayList<CaratulaSiniestroBean>();
		List<CaratulaSiniestroBean> salvamentosCaratula = new ArrayList<CaratulaSiniestroBean>();
		
		ReservaDTO reservaDTO = new ReservaDTO();
		List<IndemnizacionDTO> indemnizaciones = new ArrayList<IndemnizacionDTO>();
		List<GastoSiniestroDTO> gastos = new ArrayList<GastoSiniestroDTO>();
		List<IngresoSiniestroDTO> ingresos = new ArrayList<IngresoSiniestroDTO>();
		List<SalvamentoSiniestroDTO> salvamentos = new ArrayList<SalvamentoSiniestroDTO>();
		
		String reglaNavegacion = Sistema.EXITOSO;
		CaratulaSiniestroForm caratulaSiniestroForm = (CaratulaSiniestroForm) form;
		
		ReporteSiniestroDN reporteSiniestroDN = ReporteSiniestroDN.getInstancia(UtileriasWeb.obtieneNombreUsuario(request));
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		try {
			//Se obtienen los datos del reporte
			ReporteSiniestroDTO reporteSiniestroDTO = reporteSiniestroDN.desplegarReporte(UtileriasWeb.regresaBigDecimal(caratulaSiniestroForm.getIdReporteSiniestro()));
			//Se obtiene la lista de Suma Asegurada
			List<RiesgoAfectadoDTO> riesgos = riesgoAfectadoDN.listarPorReporteSiniestro(UtileriasWeb.regresaBigDecimal(caratulaSiniestroForm.getIdReporteSiniestro()));
			caratulaSiniestroForm.setListaSumaAsegurada(reservaDN.obtenReservaDetalleBeanParaRiesgoAfectado(riesgos));
			
			//Se generan las tablas del reporte de siniestro
			listaSoporteDistribucionReaseguro = caratulaSiniestroDN.obtenSoporteDistribucionReaseguroDTO(reporteSiniestroDTO);
			mapaSoporteDistribucionReaseguro = caratulaSiniestroDN.obtenSoporteDistribucionReaseguroMap(reporteSiniestroDTO, listaSoporteDistribucionReaseguro);
			
			reservaDTO = reservaDN.obtenReservaEstimacionInicial(UtileriasWeb.regresaBigDecimal(caratulaSiniestroForm.getIdReporteSiniestro()));
			indemnizaciones = indemnizacionDN.obtenIndemnizacionPorEstatusNOCancelado(reporteSiniestroDTO);
			gastos = gastoSiniestroDN.getGastosNoCancelados(reporteSiniestroDTO.getIdToReporteSiniestro());
			ingresos = ingresoSiniestroDN.getIngresosNoCancelados(reporteSiniestroDTO.getIdToReporteSiniestro());
			salvamentos = salvamentoSiniestroDN.listarPorEstatusNoBorrado(reporteSiniestroDTO);
			
			porcentajesSumaAseguradaCaratula = caratulaSiniestroDN.llenaTablaPorcentajesParaCaratula(listaSoporteDistribucionReaseguro);
			caratulaSiniestroForm.setListaPorcentajesSumaAsegurada(porcentajesSumaAseguradaCaratula);
			reservasCaratula = caratulaSiniestroDN.llenaTablaEstimacionInicialParaCaratula(reporteSiniestroDTO, reservaDTO, mapaSoporteDistribucionReaseguro);
			caratulaSiniestroForm.setListaEstimacionInicial(reservasCaratula);
			indemnizacionesCaratula = caratulaSiniestroDN.llenaTablaIndemnizacionParaCaratula(reporteSiniestroDTO, indemnizaciones, mapaSoporteDistribucionReaseguro);
			caratulaSiniestroForm.setListaIndemnizaciones(indemnizacionesCaratula);
			gastosCaratula = caratulaSiniestroDN.llenaTablaGastoParaCaratula(gastos, listaSoporteDistribucionReaseguro, nombreUsuario);
			caratulaSiniestroForm.setListaGastos(gastosCaratula);
			ingresosCaratula = caratulaSiniestroDN.llenaTablaIngresosParaCaratula(ingresos, listaSoporteDistribucionReaseguro);
			caratulaSiniestroForm.setListaIngresos(ingresosCaratula);			
			salvamentosCaratula = caratulaSiniestroDN.llenaTablaSalvamentoParaCaratula(salvamentos, listaSoporteDistribucionReaseguro);
			caratulaSiniestroForm.setListaSalvamentos(salvamentosCaratula);
			
			//Se obtienen los datos de la poliza
			PolizaSoporteDanosDTO polizaSoporteDanosDTO = soporteDanosDN.getDatosGeneralesPoliza(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getFechaSiniestro());
			//Se llenan los datos del reporte para la caratula
			caratulaSiniestroForm.setNumeroReporte(reporteSiniestroDTO.getNumeroReporte());
			AjustadorDTO ajustadorDTO = reporteSiniestroDTO.getAjustador();
			if (ajustadorDTO!=null && ajustadorDTO.getIdTcAjustador()!=null){
				caratulaSiniestroForm.setIdAjustadorReporte(ajustadorDTO.getIdTcAjustador().toString());
				caratulaSiniestroForm.setNombreAjustadorReporte(ajustadorDTO.getNombre());
			}
			caratulaSiniestroForm.setFechaHoraReporte(UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaHoraReporte()));
			caratulaSiniestroForm.setFechaOcurrioReporte(UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaSiniestro()));
			caratulaSiniestroForm.setDescripcionSiniestro(reporteSiniestroDTO.getDescripcionSiniestro());
			caratulaSiniestroForm.setCalleSiniestro(reporteSiniestroDTO.getCalle());
			//caratulaSiniestroForm.setColoniaSiniestro(reporteSiniestroDTO.getColonia());
			caratulaSiniestroForm.setCodigoPostalSiniestro(reporteSiniestroDTO.getCodigoPostal().toString());
			if (reporteSiniestroDTO.getColonia() !=null){
				ColoniaDTO coloniaDTO = codigoPostalDN.getColoniaPorId(reporteSiniestroDTO.getColonia());
				if (coloniaDTO != null){
					caratulaSiniestroForm.setColoniaSiniestro(coloniaDTO.getColonyName());
				}
			}
			if (reporteSiniestroDTO.getIdCiudad()!=null){
				CiudadDTO ciudadDTO = codigoPostalDN.getCiudadPorId(reporteSiniestroDTO.getIdCiudad());
				if (ciudadDTO!=null){
					caratulaSiniestroForm.setMunicipioSiniestro(ciudadDTO.getDescription());
				}
			}
			if (reporteSiniestroDTO.getIdEstado()!=null){
				EstadoDTO estadoDTO = codigoPostalDN.getEstadoPorId(reporteSiniestroDTO.getIdEstado());
				if (estadoDTO!=null){
					caratulaSiniestroForm.setEstadoSiniestro(estadoDTO.getDescription());
				}
			}
			if (polizaSoporteDanosDTO!=null){
				//Se llenan los datos de la poliza para la caratula
				caratulaSiniestroForm.setTipoMonedaPoliza(polizaSoporteDanosDTO.getDescripcionMoneda());
				caratulaSiniestroForm.setNumeroPoliza(polizaSoporteDanosDTO.getNumeroPoliza());
				caratulaSiniestroForm.setClaveOficinaPoliza(polizaSoporteDanosDTO.getNombreOficina());
				caratulaSiniestroForm.setEndosoPoliza(String.valueOf(polizaSoporteDanosDTO.getNumeroUltimoEndoso()));
				caratulaSiniestroForm.setClaveProductoPoliza(String.valueOf(polizaSoporteDanosDTO.getCodigoProducto()));
				caratulaSiniestroForm.setIncisoPoliza("");
				caratulaSiniestroForm.setClaveSubRamoPoliza(""); //falta agregar a la poliza
				caratulaSiniestroForm.setDescripcionSubRamoPoliza(polizaSoporteDanosDTO.getDescripcionTipoPoliza());
				caratulaSiniestroForm.setFechaEmisionPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaEmision()));
				caratulaSiniestroForm.setFechaInicioVigenciaPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaInicioVigencia()));
				caratulaSiniestroForm.setFechaTerminacionVigenciaPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaFinVigencia()));
				if (polizaSoporteDanosDTO.getIdAsegurado()!=null)
					caratulaSiniestroForm.setIdAseguradoPoliza(polizaSoporteDanosDTO.getIdAsegurado().toString());
				caratulaSiniestroForm.setNombreAseguradoPoliza(polizaSoporteDanosDTO.getNombreAsegurado());
				caratulaSiniestroForm.setCallePoliza(polizaSoporteDanosDTO.getCalleAsegurado());
				caratulaSiniestroForm.setCodigoPostalPoliza(polizaSoporteDanosDTO.getCpAsegurado());
				
				if (!UtileriasWeb.esCadenaVacia(polizaSoporteDanosDTO.getCpAsegurado())){
					ColoniaDTO coloniaDTO = codigoPostalDN.getColoniaPorId(polizaSoporteDanosDTO.getCpAsegurado().trim() + "-" +polizaSoporteDanosDTO.getIdColoniaAsegurado().toString());
					if (coloniaDTO!=null&&coloniaDTO.getId()!=null)
						caratulaSiniestroForm.setColoniaPoliza(coloniaDTO.getDescription());
					CiudadDTO ciudadDTOPoliza = codigoPostalDN.getCiudadPorId(coloniaDTO.getCityId());
					if (ciudadDTOPoliza!=null&&ciudadDTOPoliza.getId()!=null)
						caratulaSiniestroForm.setMunicipioPoliza(ciudadDTOPoliza.getDescription());
					
					EstadoDTO estadoDTOPoliza = codigoPostalDN.getEstadoPorId(ciudadDTOPoliza.getStateId());
					if (estadoDTOPoliza!=null&&estadoDTOPoliza.getId()!=null)
						caratulaSiniestroForm.setEstadoPoliza(estadoDTOPoliza.getDescription());
				}
				
				if (polizaSoporteDanosDTO.getCodigoAgente()!=null)
					caratulaSiniestroForm.setIdAgentePoliza(polizaSoporteDanosDTO.getCodigoAgente().toString());
				
				caratulaSiniestroForm.setNombreAgentePoliza(polizaSoporteDanosDTO.getNombreAgente());
				caratulaSiniestroForm.setFormaPagoPoliza(polizaSoporteDanosDTO.getDescripcionFormaPago());
				//caratulaSiniestroForm.setUltimoReciboPagadoPoliza(polizaSoporteDanosDTO.getUltimoReciboPagado());
				
				//caratulaSiniestroForm.setEstatusCobranza("");
				//if(polizaSoporteDanosDTO.getFechaPago()==null)
				//	caratulaSiniestroForm.setFechaPagoPoliza("");
				//else
				//	caratulaSiniestroForm.setFechaPagoPoliza(UtileriasWeb.getFechaHoraString(polizaSoporteDanosDTO.getFechaPago()));
				//caratulaSiniestroForm.setMontoPagoPoliza(UtileriasWeb.formatoMoneda(polizaSoporteDanosDTO.getImportePagado()));
				//caratulaSiniestroForm.setSaldoPendientePoliza(UtileriasWeb.formatoMoneda(polizaSoporteDanosDTO.getSaldoPendiente()));
				//caratulaSiniestroForm.setSaldoVencidoPoliza(UtileriasWeb.formatoMoneda(polizaSoporteDanosDTO.getSaldoVencido()));
			}
			
			DatosCobranzaPolizaDTO datosCobranzaPolizaDTO = consultaCobranzaPolizaDN.consultaCobranzaPoliza(reporteSiniestroDTO.getNumeroPoliza());
			
			if (datosCobranzaPolizaDTO != null){
				if(datosCobranzaPolizaDTO.getUltimoReciboPagado()!= null){
					caratulaSiniestroForm.setUltimoReciboPagadoPoliza(UtileriasWeb.getFechaHoraString(datosCobranzaPolizaDTO.getFechaUltimoPago()));
				}else{
					caratulaSiniestroForm.setUltimoReciboPagadoPoliza("");
				}
				if(datosCobranzaPolizaDTO.getMontoVencido() != null){
					caratulaSiniestroForm.setSaldoVencidoPoliza(UtileriasWeb.formatoMoneda(datosCobranzaPolizaDTO.getMontoVencido()));
				}else{
					caratulaSiniestroForm.setSaldoVencidoPoliza("");
				}
				if(datosCobranzaPolizaDTO.getMontoPorVencer() != null){
					caratulaSiniestroForm.setSaldoPendientePoliza(UtileriasWeb.formatoMoneda(datosCobranzaPolizaDTO.getMontoPorVencer()));
				}else{
					caratulaSiniestroForm.setSaldoPendientePoliza("");
				}
				if(datosCobranzaPolizaDTO.getMontoPagado() != null){
					caratulaSiniestroForm.setMontoPagoPoliza(UtileriasWeb.formatoMoneda(datosCobranzaPolizaDTO.getMontoPagado()));
				}else{
					caratulaSiniestroForm.setMontoPagoPoliza("");
				}
				if(datosCobranzaPolizaDTO.getSituacion() != null){
					caratulaSiniestroForm.setEstatusCobranza(datosCobranzaPolizaDTO.getSituacion());
				}else{
					caratulaSiniestroForm.setEstatusCobranza("");
				}
				if(datosCobranzaPolizaDTO.getFechaUltimoPago() != null){
					caratulaSiniestroForm.setFechaPagoPoliza(UtileriasWeb.getFechaHoraString(datosCobranzaPolizaDTO.getFechaUltimoPago()));
				}else{
					caratulaSiniestroForm.setFechaPagoPoliza("");
				}
			}
			//Se agrega la informacion de la caratula a session
			HttpSession sesion = request.getSession();
			sesion.setAttribute("datosCaratulaSiniestro", caratulaSiniestroForm);
		} catch (SystemException e) {
			reglaNavegacion = Sistema.NO_DISPONIBLE;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		} catch (ExcepcionDeAccesoADatos e) {
			reglaNavegacion = Sistema.NO_EXITOSO;
			UtileriasWeb.mandaMensajeExcepcionRegistrado(e.getMessage(), request);
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void mostrarCaratulaSiniestroPDF (ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		//Se obtiene la informacion de la caratula de session
		HttpSession sesion = request.getSession();
		CaratulaSiniestroForm caratulaSiniestroForm = (CaratulaSiniestroForm)sesion.getAttribute("datosCaratulaSiniestro");
		ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
		byte[] reporte = reporteSiniestro.mostrarCaratulaSiniestro(caratulaSiniestroForm, request);
		try {
			super.writeBytes(response, reporte, Sistema.TIPO_PDF,"caratulaSiniestro");
		} catch (IOException e) {
			e.printStackTrace();
		}
		//Se elimina la informacion de la caratula de sesion
		//sesion.removeAttribute("datosCaratulaSiniestro");
	}
		
}
