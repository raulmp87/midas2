package mx.com.afirme.midas.siniestro.reportes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.danios.soporte.CoberturaSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.RiesgoSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SeccionSoporteDanosDTO;
import mx.com.afirme.midas.danios.soporte.SoporteDanosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDN;
import mx.com.afirme.midas.interfaz.prestadorservicios.PrestadorServiciosDTO;
import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleDTO;
import mx.com.afirme.midas.siniestro.finanzas.ReservaDetalleId;
import mx.com.afirme.midas.siniestro.finanzas.gasto.GastoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDN;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.indemnizacion.IndemnizacionRiesgoCoberturaId;
import mx.com.afirme.midas.siniestro.finanzas.ingreso.IngresoSiniestroDTO;
import mx.com.afirme.midas.siniestro.finanzas.integracion.IntegracionReaseguroDN;
import mx.com.afirme.midas.siniestro.finanzas.integracion.SoporteDistribucionReaseguroDTO;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class CaratulaSiniestroDN {
	private static final CaratulaSiniestroDN INSTANCIA = new CaratulaSiniestroDN();
	
	public static CaratulaSiniestroDN getInstancia() {
		return CaratulaSiniestroDN.INSTANCIA;
	}
	
	public List<CaratulaSiniestroBean> llenaTablaEstimacionInicialParaCaratula(ReporteSiniestroDTO reporteSiniestroDTO, ReservaDTO reservaDTO, Map<String, SoporteDistribucionReaseguroDTO> mapaSoporteDistribucionReaseguro)
		throws SystemException{
		List<CaratulaSiniestroBean> listaCaratulaSiniestro = new ArrayList<CaratulaSiniestroBean>();
		List<ReservaDetalleDTO> listaReservaDetalle = null;
		CaratulaSiniestroBean caratulaSiniestroBean = new CaratulaSiniestroBean();
		BigDecimal idToRiesgo = new BigDecimal("0");
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		Double montoADistribuir = new Double("0.0");
		ReservaDetalleId reservaDetalleId = new ReservaDetalleId();
		SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO = new SoporteDistribucionReaseguroDTO();
		String llave = "";
		
			if (reservaDTO==null||reservaDTO.getIdtoreservaestimada()==null 
					|| reservaDTO.getReservaDetalleDTOs()==null || reservaDTO.getReservaDetalleDTOs().size()<=0) return listaCaratulaSiniestro;
			listaReservaDetalle = reservaDTO.getReservaDetalleDTOs();
		
			for (ReservaDetalleDTO reservaDetalleDTO:listaReservaDetalle) {
				soporteDistribucionReaseguroDTO = new SoporteDistribucionReaseguroDTO();
				caratulaSiniestroBean = new CaratulaSiniestroBean();
				reservaDetalleId = reservaDetalleDTO.getId();
				llave = this.generarLlaveSoporteDistribucionReaseguro(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getNumeroEndoso().intValue(), 
						reservaDetalleId.getIdtoseccion(), reservaDetalleId.getIdtocobertura(), 
						reservaDetalleId.getNumeroinciso(), reservaDetalleId.getNumerosubinciso());
				
				soporteDistribucionReaseguroDTO = mapaSoporteDistribucionReaseguro.get(llave);
				reservaDetalleDTO.getRiesgoAfectadoDTO().getId().getIdtoriesgo();				
				caratulaSiniestroBean.setNumeroSubInciso(soporteDistribucionReaseguroDTO.getNumeroSubInciso());
				idToRiesgo = reservaDetalleDTO.getId().getIdtoriesgo();
				riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(idToRiesgo);
				caratulaSiniestroBean.setDescripcionRiesgo(riesgoSoporteDanosDTO.getDescripcionRiesgo());
				
				montoADistribuir = reservaDetalleDTO.getEstimacion() / soporteDistribucionReaseguroDTO.getPorcentajeSumaAsegurada(); 
				llenaBeanDeSoporteDistribucionReaseguroDTO(soporteDistribucionReaseguroDTO, caratulaSiniestroBean, montoADistribuir);
				listaCaratulaSiniestro.add(caratulaSiniestroBean);
				
			}
		return listaCaratulaSiniestro;
	}
	
	public List<CaratulaSiniestroBean> llenaTablaPorcentajesParaCaratula(List<SoporteDistribucionReaseguroDTO> listaSoporteDistribucionReaseguro)
		throws SystemException{
		List<CaratulaSiniestroBean> listaCaratulaSiniestro = new ArrayList<CaratulaSiniestroBean>();
		CaratulaSiniestroBean caratulaSiniestroBean = new CaratulaSiniestroBean();
		Double montoADistribuir = new Double("0.0");
		
		for (SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO : listaSoporteDistribucionReaseguro){
			caratulaSiniestroBean = new CaratulaSiniestroBean();
			caratulaSiniestroBean.setNumeroSubInciso(soporteDistribucionReaseguroDTO.getNumeroSubInciso());
			montoADistribuir = 1.0/soporteDistribucionReaseguroDTO.getPorcentajeSumaAsegurada();
			llenaBeanDeSoporteDistribucionReaseguroDTO(soporteDistribucionReaseguroDTO, caratulaSiniestroBean, montoADistribuir);
			listaCaratulaSiniestro.add(caratulaSiniestroBean);
		}
		
		return listaCaratulaSiniestro;
	}
		
	public List<CaratulaSiniestroBean> llenaTablaIndemnizacionParaCaratula(ReporteSiniestroDTO reporteSiniestroDTO, List<IndemnizacionDTO> listaIndemnizaciones, Map<String, SoporteDistribucionReaseguroDTO> mapaSoporteDistribucionReaseguro)
		throws SystemException{
		List<IndemnizacionRiesgoCoberturaDTO> indemnizacionRiesgoCoberturaList = new ArrayList<IndemnizacionRiesgoCoberturaDTO>();
		IndemnizacionRiesgoCoberturaDN indemnizacionRiesgoCoberturaDN = IndemnizacionRiesgoCoberturaDN.getInstancia();
		RiesgoSoporteDanosDTO riesgoSoporteDanosDTO = new RiesgoSoporteDanosDTO();
		SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
		BigDecimal idToRiesgo = new BigDecimal("0");
		Double montoADistribuir = new Double("0.0");
		IndemnizacionRiesgoCoberturaId indemnizacionRiesgoCoberturaId = new IndemnizacionRiesgoCoberturaId();
		SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO = new SoporteDistribucionReaseguroDTO();
		String llave = "";
		
		List<CaratulaSiniestroBean> listaCaratulaSiniestro = new ArrayList<CaratulaSiniestroBean>();
		CaratulaSiniestroBean caratulaSiniestroBean = new CaratulaSiniestroBean();
			if (listaIndemnizaciones==null||listaIndemnizaciones.isEmpty()) return listaCaratulaSiniestro;
			for (IndemnizacionDTO indemnizacionDTO:listaIndemnizaciones) {
				indemnizacionRiesgoCoberturaList = indemnizacionRiesgoCoberturaDN.buscarPorIndemnizacion(indemnizacionDTO);
				if (indemnizacionRiesgoCoberturaList!=null){
					for (IndemnizacionRiesgoCoberturaDTO indemnizacionRiesgoCoberturaDTO:indemnizacionRiesgoCoberturaList){
						soporteDistribucionReaseguroDTO = new SoporteDistribucionReaseguroDTO();
						caratulaSiniestroBean = new CaratulaSiniestroBean();
						indemnizacionRiesgoCoberturaId = indemnizacionRiesgoCoberturaDTO.getId();
						
						llave = this.generarLlaveSoporteDistribucionReaseguro(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getNumeroEndoso().intValue(),
								indemnizacionRiesgoCoberturaId.getIdToSeccion(), indemnizacionRiesgoCoberturaId.getIdToCobertura(), 
								indemnizacionRiesgoCoberturaId.getNumeroInciso(), indemnizacionRiesgoCoberturaId.getNumeroSubinciso());
						soporteDistribucionReaseguroDTO = mapaSoporteDistribucionReaseguro.get(llave);
						
						caratulaSiniestroBean.setNumeroSubInciso(soporteDistribucionReaseguroDTO.getNumeroSubInciso());
						caratulaSiniestroBean.setIdToIndemnizacion(indemnizacionDTO.getIdToIndemnizacion());
						caratulaSiniestroBean.setFechaHoraIndemnizacion(UtileriasWeb.getFechaHoraString(new Date(indemnizacionDTO.getFechaDelPago().getTime())));
						idToRiesgo = indemnizacionRiesgoCoberturaDTO.getId().getIdToRiesgo();
						riesgoSoporteDanosDTO = soporteDanosDN.getRiesgoSoporte(idToRiesgo);
						caratulaSiniestroBean.setDescripcionRiesgo(riesgoSoporteDanosDTO.getDescripcionRiesgo());
						
						montoADistribuir = indemnizacionRiesgoCoberturaDTO.getMontoPago()/soporteDistribucionReaseguroDTO.getPorcentajeSumaAsegurada();
						if (montoADistribuir>0.0){
							llenaBeanDeSoporteDistribucionReaseguroDTO(soporteDistribucionReaseguroDTO, caratulaSiniestroBean, montoADistribuir);							
							listaCaratulaSiniestro.add(caratulaSiniestroBean);
						}
					}
				}
			}
		return listaCaratulaSiniestro;
	}
	
	public List<CaratulaSiniestroBean> llenaTablaGastoParaCaratula(List<GastoSiniestroDTO> listaGastos, List<SoporteDistribucionReaseguroDTO> listaSoporteDistribucionReaseguro, String nombreUsuario)
		throws SystemException{
		List<CaratulaSiniestroBean> listaCaratulaSiniestro = new ArrayList<CaratulaSiniestroBean>();
		CaratulaSiniestroBean caratulaSiniestroBean = new CaratulaSiniestroBean();
		Double montoADistribuir = new Double("0.0");
		PrestadorServiciosDN prestadorServiciosDN = PrestadorServiciosDN.getInstancia();
		PrestadorServiciosDTO prestadorServiciosDTO;
		
		
			if (listaGastos==null||listaGastos.isEmpty()) return listaCaratulaSiniestro;
				for (GastoSiniestroDTO gastoSiniestroDTO:listaGastos) {
					for (SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO : listaSoporteDistribucionReaseguro){
						caratulaSiniestroBean = new CaratulaSiniestroBean();
						
						prestadorServiciosDTO = null;
						prestadorServiciosDTO = prestadorServiciosDN.detallePrestador(gastoSiniestroDTO.getIdTcPrestadorServicios(),nombreUsuario);
						if (prestadorServiciosDTO.getNombrePrestador() != null){
							caratulaSiniestroBean.setNombrePrestadorServicio(prestadorServiciosDTO.getNombrePrestador());
						}
						//caratulaSiniestroBean.setNombrePrestadorServicio(gastoSiniestroDTO.getNombrePrestadorServicios());
						caratulaSiniestroBean.setFechaHoraGasto(UtileriasWeb.getFechaHoraString(gastoSiniestroDTO.getFechaGasto()));
						caratulaSiniestroBean.setIdToGastoSiniestro(gastoSiniestroDTO.getIdToGastoSiniestro());
						montoADistribuir = gastoSiniestroDTO.getMontoGasto();
						llenaBeanDeSoporteDistribucionReaseguroDTO(soporteDistribucionReaseguroDTO, caratulaSiniestroBean,montoADistribuir);
						listaCaratulaSiniestro.add(caratulaSiniestroBean);
					}
				}
		return listaCaratulaSiniestro;
	}
	
	public List<CaratulaSiniestroBean> llenaTablaIngresosParaCaratula(List<IngresoSiniestroDTO> listaIngresos, List<SoporteDistribucionReaseguroDTO> listaSoporteDistribucionReaseguro)
		throws SystemException{
		List<CaratulaSiniestroBean> listaCaratulaSiniestro = new ArrayList<CaratulaSiniestroBean>();
		CaratulaSiniestroBean caratulaSiniestroBean = new CaratulaSiniestroBean();
		Double montoADistribuir = new Double("0.0");
		
			if (listaIngresos==null||listaIngresos.isEmpty()) return listaCaratulaSiniestro;
				for (IngresoSiniestroDTO ingresoSiniestroDTO:listaIngresos) {
					for (SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO : listaSoporteDistribucionReaseguro){
						caratulaSiniestroBean = new CaratulaSiniestroBean();
						caratulaSiniestroBean.setIdToIngresoSiniestro(ingresoSiniestroDTO.getIdToIngresoSiniestro());
						caratulaSiniestroBean.setFechaHoraIngreso(UtileriasWeb.getFechaHoraString(ingresoSiniestroDTO.getFechaCreacion()));
						montoADistribuir = ingresoSiniestroDTO.getMonto();
						llenaBeanDeSoporteDistribucionReaseguroDTO(soporteDistribucionReaseguroDTO, caratulaSiniestroBean,montoADistribuir);
						listaCaratulaSiniestro.add(caratulaSiniestroBean);
					}
				}
		return listaCaratulaSiniestro;
	}
	
	public List<CaratulaSiniestroBean> llenaTablaSalvamentoParaCaratula(List<SalvamentoSiniestroDTO> listaSalvamentos, List<SoporteDistribucionReaseguroDTO> listaSoporteDistribucionReaseguro)
		throws SystemException{
		List<CaratulaSiniestroBean> listaCaratulaSiniestro = new ArrayList<CaratulaSiniestroBean>();
		CaratulaSiniestroBean caratulaSiniestroBean = new CaratulaSiniestroBean();
		Double montoADistribuir = new Double("0.0");
		
			if (listaSalvamentos==null || listaSalvamentos.isEmpty()) return listaCaratulaSiniestro;
			for (SalvamentoSiniestroDTO salvamentoSiniestroDTO:listaSalvamentos) {
				for (SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO : listaSoporteDistribucionReaseguro){
					caratulaSiniestroBean = new CaratulaSiniestroBean();
					caratulaSiniestroBean.setNombreItem(salvamentoSiniestroDTO.getNombreItem());
					montoADistribuir = salvamentoSiniestroDTO.getValorEstimado();
					llenaBeanDeSoporteDistribucionReaseguroDTO(soporteDistribucionReaseguroDTO, caratulaSiniestroBean, montoADistribuir);
					listaCaratulaSiniestro.add(caratulaSiniestroBean);
				}
			}
		return listaCaratulaSiniestro;
	}
	
	private void llenaBeanDeSoporteDistribucionReaseguroDTO(SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO, CaratulaSiniestroBean caratulaSiniestroBean, Double montoADistribuir)
		throws SystemException{
		if (soporteDistribucionReaseguroDTO==null)return ;
		IntegracionReaseguroDN integracionReaseguroDN = IntegracionReaseguroDN.getInstancia();
		CoberturaSoporteDanosDTO coberturaSoporteDanosDTO = soporteDistribucionReaseguroDTO.getCoberturaDTO();
		SeccionSoporteDanosDTO seccionSoporteDanosDTO = soporteDistribucionReaseguroDTO.getSeccionDTO();
		
		integracionReaseguroDN.realizaDistribucionDeReporteSiniestro(soporteDistribucionReaseguroDTO, montoADistribuir);
		if (coberturaSoporteDanosDTO!=null){
			caratulaSiniestroBean.setDescripcionRamo(coberturaSoporteDanosDTO.getDescripcionRamo());
			caratulaSiniestroBean.setDescripcionSubRamo(coberturaSoporteDanosDTO.getDescripcionSubramo());
			caratulaSiniestroBean.setDescripcionCobertura(coberturaSoporteDanosDTO.getDescripcionCobertura());
		}
		if (seccionSoporteDanosDTO!=null){
			caratulaSiniestroBean.setDescripcionSeccion(seccionSoporteDanosDTO.getDescripcionSeccion());
		}
		caratulaSiniestroBean.setMonto(new Double(soporteDistribucionReaseguroDTO.getMonto()));
		caratulaSiniestroBean.setMontoCuotaParte(new Double(soporteDistribucionReaseguroDTO.getMontoCuotaParte()));
		caratulaSiniestroBean.setMontoPrimerExcedente(new Double(soporteDistribucionReaseguroDTO.getMontoPrimerExcedente()));
		caratulaSiniestroBean.setMontoRetencion(new Double(soporteDistribucionReaseguroDTO.getMontoRetenido()));
		caratulaSiniestroBean.setMontoFacultativo(new Double(soporteDistribucionReaseguroDTO.getMontoFacultativo()));
	}
	
	public List<SoporteDistribucionReaseguroDTO> obtenSoporteDistribucionReaseguroDTO(ReporteSiniestroDTO reporteSiniestroDTO) throws SystemException{
		IntegracionReaseguroDN integracionReaseguroDN = IntegracionReaseguroDN.getInstancia();
		return integracionReaseguroDN.getDistribucionDeReporteSiniestro(reporteSiniestroDTO, 0);
	}
	
	public Map<String, SoporteDistribucionReaseguroDTO> obtenSoporteDistribucionReaseguroMap(ReporteSiniestroDTO reporteSiniestroDTO,List<SoporteDistribucionReaseguroDTO> soporteDistribucionReaseguroList) throws SystemException{
		Map<String, SoporteDistribucionReaseguroDTO> soporteDistribucionReaseguroMap = null;
		String clave = new String();
		
		if (soporteDistribucionReaseguroList!=null && soporteDistribucionReaseguroList.size()>0){
			soporteDistribucionReaseguroMap = new HashMap<String, SoporteDistribucionReaseguroDTO>();
			for (SoporteDistribucionReaseguroDTO soporteDistribucionReaseguroDTO : soporteDistribucionReaseguroList){
				clave = this.generarLlaveSoporteDistribucionReaseguro(reporteSiniestroDTO.getNumeroPoliza(), reporteSiniestroDTO.getNumeroEndoso().intValue(),
						soporteDistribucionReaseguroDTO.getSeccionDTO().getIdToSeccion(), soporteDistribucionReaseguroDTO.getCoberturaDTO().getIdToCobertura(), 
						soporteDistribucionReaseguroDTO.getNumeroInciso(), soporteDistribucionReaseguroDTO.getNumeroSubInciso());
				soporteDistribucionReaseguroMap.put(clave, soporteDistribucionReaseguroDTO);
			}
		}
		return soporteDistribucionReaseguroMap;
	}
	
	private String generarLlaveSoporteDistribucionReaseguro(BigDecimal idToPoliza, int numeroEndoso,
			BigDecimal idToSeccion, BigDecimal idToCobertura,BigDecimal numeroInciso, BigDecimal numeroSubInciso){
		String key = new String();
		if (idToPoliza!=null)
			key += idToPoliza.toString() + "_";
		key += String.valueOf(numeroEndoso) + "_";
		if (idToSeccion!=null)
			key += idToSeccion.toString() + "_";
		if (idToCobertura!=null)
			key += idToCobertura.toString() + "_";
		if (numeroInciso!=null)
			key += numeroInciso.toString() + "_";
		if (numeroSubInciso!=null)
			key += numeroSubInciso.toString();

		return key;
	}
}
