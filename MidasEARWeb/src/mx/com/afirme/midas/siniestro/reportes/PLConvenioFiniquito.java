package mx.com.afirme.midas.siniestro.reportes; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito.ConvenioFiniquitoForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PLConvenioFiniquito extends MidasPlantillaBase{
	private ConvenioFiniquitoForm convenioFiniquitoForm;
	
	public PLConvenioFiniquito(ConvenioFiniquitoForm convenioFiniquitoForm){
		super();
		this.convenioFiniquitoForm = convenioFiniquitoForm;
		super.setListaRegistrosContenido( new ArrayList<Object>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		super.setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.conveniofiniquito"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.paquete"));
		parametros.put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		setParametrosVariablesReporte(parametros);
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.convenioFiniquitoForm==null)return;
		getListaRegistrosContenido().add(this.convenioFiniquitoForm);
		try {
			super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			e.printStackTrace();
			setByteArrayReport( null );
		}
	}
	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
}