package mx.com.afirme.midas.siniestro.reportes;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.DataSourceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PLSiniestros extends MidasPlantillaBase {

	public void inicializaReporte(String nombrePlantilla, Map<String, Object> parametros, String tipoArchivo){
		setListaRegistrosContenido(new ArrayList<Object>());
		setNombrePlantilla(nombrePlantilla);
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(
				Sistema.ARCHIVO_RECURSOS,
				"midas.sistema.siniestro.reportes.paquete"));
		setParametrosVariablesReporte(parametros);
		setTipoReporte(tipoArchivo);
	}
	
	public PLSiniestros(String nombrePlantilla, Map<String, Object> parametros){
		super();
		inicializaReporte(nombrePlantilla, parametros, Sistema.TIPO_PDF);
	}
	public PLSiniestros(String nombrePlantilla, Map<String, Object> parametros, String tipoArchivo) {
		super();
		inicializaReporte(nombrePlantilla, parametros, tipoArchivo);
	}

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		procesarDatosReporte(claveUsuario);
		return getByteArrayReport();
	}

	private void procesarDatosReporte(String claveUsuario)
			throws SystemException {
		JasperReport reporte;
		byte[] reporteGenerado = null;
				
		reporte = super.getJasperReport(getPaquetePlantilla()
				+ getNombrePlantilla());
	
		setDataSource(DataSourceLocator.getDataSource(Sistema.MIDAS_DATASOURCE));
		Connection connection = null;
		try {
			connection = getDataSource().getConnection();
			reporteGenerado = generaReporte(getTipoReporte(), reporte,
					getParametrosVariablesReporte(), connection);

		} catch (SQLException e1) {
		} catch (JRException e) {
			
		}finally{
		    DbUtils.closeQuietly(connection);
		}
		setByteArrayReport(reporteGenerado);
	}

}
