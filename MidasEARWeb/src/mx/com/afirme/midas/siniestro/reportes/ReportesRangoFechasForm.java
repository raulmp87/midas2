package mx.com.afirme.midas.siniestro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReportesRangoFechasForm extends MidasBaseForm{
	private static final long serialVersionUID = 1L;
	
	private String nombrePlantilla;
	private String fechaInicial;
	private String fechaFinal;
	
	
	public String getNombrePlantilla() {
		return nombrePlantilla;
	}
	public void setNombrePlantilla(String nombrePlantilla) {
		this.nombrePlantilla = nombrePlantilla;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	
	

}
