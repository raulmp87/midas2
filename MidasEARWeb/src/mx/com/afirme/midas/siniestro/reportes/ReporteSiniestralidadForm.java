package mx.com.afirme.midas.siniestro.reportes;

import mx.com.afirme.midas.sistema.MidasBaseForm;

public class ReporteSiniestralidadForm  extends MidasBaseForm{
	private static final long serialVersionUID = 1L;
	
	private String nombreDelAsegurado;
	private String nombreDelAgente;
	private String numeroDePoliza;
	private String fechaInicial;
	private String fechaFinal;
	

	public String getNombreDelAsegurado() {
		return nombreDelAsegurado;
	}

	public void setNombreDelAsegurado(String nombreDelAsegurado) {
		this.nombreDelAsegurado = nombreDelAsegurado;
	}
	
	public String getNombreDelAgente() {
		return nombreDelAgente;
	}
	
	public void setNombreDelAgente(String nombreDelAgente) {
		this.nombreDelAgente = nombreDelAgente;
	}
	
	public String getNumeroDePoliza() {
		return numeroDePoliza;
	}
	
	public void setNumeroDePoliza(String numeroDePoliza) {
		this.numeroDePoliza = numeroDePoliza;
	}
	
	public String getFechaInicial() {
		return fechaInicial;
	}
	
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	
	public String getFechaFinal() {
		return fechaFinal;
	}
	
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}	
}
