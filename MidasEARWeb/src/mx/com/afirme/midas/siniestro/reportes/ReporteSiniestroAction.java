package mx.com.afirme.midas.siniestro.reportes;
/**
 * 
 */
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author user
 *
 */
public class ReporteSiniestroAction extends MidasMappingDispatchAction{ 
	
	private final String rutaImagen;
	private final String pcoderrm;
	private final String pdescerrm;
	
	public ReporteSiniestroAction(){
		rutaImagen = Sistema.LOGOTIPO_SEGUROS_AFIRME;
		pcoderrm = "0";
		pdescerrm = null;
	}
	
	public ActionForward cargarJSPReporte(ActionMapping mapping, ActionForm form,HttpServletRequest request, 
			HttpServletResponse response) {
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public void mostrarReporteSPSinParametros(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String nombrePlantilla = request.getParameter("nombrePlantilla");
		String tipoArchivo = request.getParameter("tipoArchivo");
		String extensionArchivo = Sistema.TIPO_PDF;
		ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
		
		if (!UtileriasWeb.esCadenaVacia(tipoArchivo)){
			if (tipoArchivo.equalsIgnoreCase("TIPO_XLS")){
				extensionArchivo = Sistema.TIPO_XLS;
			}
		}
		
		byte[] reporte = reporteSiniestro.mostrarReporteSPSinParametros(nombrePlantilla+".jrxml", rutaImagen, pcoderrm, pdescerrm, extensionArchivo);
		try {
			super.writeBytes(response, reporte, extensionArchivo, nombrePlantilla);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	public void mostrarReporteSiniestralidadYAnexo(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String nombrePlantilla = request.getParameter("nombrePlantilla");
		String nombreDelAsegurado = request.getParameter("nombreDelAsegurado");
		String nombreDelAgente = request.getParameter("nombreDelAgente");
		String numeroDePoliza = request.getParameter("numeroDePoliza");
		String fechaInicial = request.getParameter("fechaInicial");
		String fechaFinal = request.getParameter("fechaFinal");
		
//		if (!UtileriasWeb.esCadenaVacia(numeroDePoliza)){
//			SoporteDanosDN soporteDanosDN = SoporteDanosDN.getInstancia();
//			List<PolizaSoporteDanosDTO> polizasDanosList = new ArrayList<PolizaSoporteDanosDTO>();
//			PolizaSoporteDanosDTO polizaSoporteDanosDTO = new PolizaSoporteDanosDTO();
//			polizaSoporteDanosDTO.setNumeroPoliza(numeroDePoliza);
//			try{
//				numeroDePoliza = "";
//				polizasDanosList = soporteDanosDN.buscarPolizasFiltrado(polizaSoporteDanosDTO);
//				if (polizasDanosList!=null && polizasDanosList.size()>0){
//					polizaSoporteDanosDTO = polizasDanosList.get(0);
//				}
//				if (polizaSoporteDanosDTO!=null && polizaSoporteDanosDTO.getIdToPoliza()!=null){
//					numeroDePoliza = polizaSoporteDanosDTO.getIdToPoliza().toString();
//				}
//			}catch (SystemException e) {}
//		}
		
		ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
		byte[] reporte = reporteSiniestro.mostrarReporteSiniestralidadYAnexo(nombrePlantilla+".jrxml",rutaImagen, pcoderrm, pdescerrm, 
				nombreDelAsegurado, nombreDelAgente, numeroDePoliza, fechaInicial, fechaFinal);
		try {
			super.writeBytes(response, reporte, Sistema.TIPO_XLS, nombrePlantilla);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void mostrarReporteSiniestrosRRCSONORv7(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String nombrePlantilla = request.getParameter("nombrePlantilla");
		String fechaInicial = request.getParameter("fechaInicial");
		String fechaFinal = request.getParameter("fechaFinal");
		ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
		if (!UtileriasWeb.esCadenaVacia(nombrePlantilla) && !UtileriasWeb.esCadenaVacia(fechaInicial)&&!UtileriasWeb.esCadenaVacia(fechaFinal)){
			byte[] reporte = reporteSiniestro.mostrarReporteSiniestrosRRCSONORv7(nombrePlantilla+".jrxml",rutaImagen, pcoderrm, pdescerrm, 
					fechaInicial, fechaFinal);
			try {
				super.writeBytes(response, reporte, Sistema.TIPO_XLS, nombrePlantilla);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public ActionForward cargarReporteSPFechas(ActionMapping mapping, ActionForm form,HttpServletRequest request, 
			HttpServletResponse response) {
		String nombrePlantilla = request.getParameter("nombrePlantilla");
		ReportesRangoFechasForm reportesRangoFechasForm = (ReportesRangoFechasForm)form; 
		reportesRangoFechasForm.setNombrePlantilla(nombrePlantilla);
		return mapping.findForward(Sistema.EXITOSO);
	}
	
	public void mostrarReporteSPFechas(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String nombrePlantilla = request.getParameter("nombrePlantilla");
		String fechaInicial = request.getParameter("fechaInicial");
		String fechaFinal = request.getParameter("fechaFinal");
		
		ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
		byte[] reporte = reporteSiniestro.mostrarReporteSPFechas(nombrePlantilla+".jrxml",rutaImagen, pcoderrm, pdescerrm, fechaInicial, fechaFinal);
		try {
			super.writeBytes(response, reporte, Sistema.TIPO_XLS, nombrePlantilla);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
