package mx.com.afirme.midas.siniestro.reportes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.siniestro.finanzas.conveniofiniquito.ConvenioFiniquitoForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ReporteSiniestro extends MidasReporteBase{
	
	
	public byte[] mostrarReporteSPSinParametros(String nombrePlantilla, String rutaImagen, String pcoderrm,String pdescerrm){
		return mostrarReporteSPSinParametros(nombrePlantilla, rutaImagen, pcoderrm, pdescerrm, Sistema.TIPO_PDF);
	}
	
	public byte[] mostrarReporteSPSinParametros(String nombrePlantilla, String rutaImagen, String pcoderrm,String pdescerrm, String tipoArchivo){
		setListaPlantillas(new ArrayList<byte[]>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("P_RUTA_IMAGEN", rutaImagen);
		parametros.put("P_CODERRM", pcoderrm);
		parametros.put("P_DESCERRM", pdescerrm);
		parametros.put("PRUTAIMAGEN", rutaImagen);
		parametros.put("PCODERRM", pcoderrm);
		parametros.put("PCODERRM", pdescerrm);
		
		byte[] reporteTMP = null;
		PLSiniestros plSiniestros = new PLSiniestros(nombrePlantilla, parametros, tipoArchivo);
		try {
			reporteTMP = plSiniestros.obtenerReporte("");
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
		}	
		return reporteTMP;
	}
	
	
	public byte[] mostrarReporteSiniestralidadYAnexo(String nombrePlantilla, String rutaImagen, String pcoderrm,String pdescerrm,
			String nombreDelAsegurado, String nombreDelAgente, String numeroDePoliza, String fechaInicial, String fechaFinal){
		setListaPlantillas(new ArrayList<byte[]>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("PFECHAINICIALVIG", fechaInicial);
		parametros.put("PFECHAFINALVIG", fechaFinal);
		parametros.put("PNUMPOLIZA", numeroDePoliza);
		parametros.put("PAGENTE", nombreDelAgente);
		parametros.put("PASEGURADO", nombreDelAsegurado);
		parametros.put("PDESCERRM", pdescerrm);
		parametros.put("PCODERRM", pcoderrm);
		parametros.put("PRUTAIMAGEN", rutaImagen.toString());
		
		byte[] reporteTMP = null;
		PLSiniestros plSiniestros = new PLSiniestros(nombrePlantilla, parametros,Sistema.TIPO_XLS);
		try {
			reporteTMP = plSiniestros.obtenerReporte("");
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
		}	
		return reporteTMP;
	}
	
	public byte[] mostrarReporteSiniestrosRRCSONORv7(String nombrePlantilla, String rutaImagen, String pcoderrm,String pdescerrm,
			String fechaInicial, String fechaFinal){
		setListaPlantillas(new ArrayList<byte[]>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("P_RUTA_IMAGEN", rutaImagen);
		parametros.put("P_CODERRM", pcoderrm);
		parametros.put("P_DESCERRM", pdescerrm);
		parametros.put("PFECHAINICIAL", fechaInicial);
		parametros.put("PFECHAFINAL", fechaFinal);
		parametros.put("PRUTAIMAGEN", rutaImagen);
//		parametros.put("PCODERRM", pcoderrm);
//		parametros.put("PCODERRM", pdescerrm);
		
		byte[] reporteTMP = null;
		PLSiniestros plSiniestros = new PLSiniestros(nombrePlantilla, parametros,Sistema.TIPO_XLS);
		try {
			reporteTMP = plSiniestros.obtenerReporte("");
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
		}	
		return reporteTMP;
//		return this.mostrarReporteSPFechas(nombrePlantilla, rutaImagen, pcoderrm, pdescerrm, fechaInicial, fechaFinal);
	}
	
	public byte[] mostrarReporteConvenioSiniestro(ConvenioFiniquitoForm convenioFiniquitoForm, HttpServletRequest request){
		setListaPlantillas(new ArrayList<byte[]>());
		byte[] reporteConvenioSiniestro = null;
		PLConvenioFiniquito plConvenioFiniquito = new PLConvenioFiniquito(convenioFiniquitoForm);
		try{
			reporteConvenioSiniestro = plConvenioFiniquito.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
		}catch (SystemException e1) {}
		if (plConvenioFiniquito!=null){
			getListaPlantillas().add(reporteConvenioSiniestro);
		}
		return reporteConvenioSiniestro;
	}
	
	public byte[] mostrarCaratulaSiniestro(CaratulaSiniestroForm caratulaSiniestroForm, HttpServletRequest request){
		setListaPlantillas(new ArrayList<byte[]>());
		byte[] caratulaSiniestro = null;
		PLCaratulaSiniestro plCaratulaSiniestro = new PLCaratulaSiniestro(caratulaSiniestroForm);
		try{
			caratulaSiniestro = plCaratulaSiniestro.obtenerReporte(UtileriasWeb.obtieneNombreUsuario(request));
		}catch (SystemException e1) {}
		if (plCaratulaSiniestro!=null){
			getListaPlantillas().add(caratulaSiniestro);
		}
		return caratulaSiniestro;
	}
	
	public byte[] mostrarReporteSPFechas(String nombrePlantilla, String rutaImagen, String pcoderrm,String pdescerrm,
			String fechaInicial, String fechaFinal){
		setListaPlantillas(new ArrayList<byte[]>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("PRUTAIMAGEN", rutaImagen);
		parametros.put("PDESCERRM", pdescerrm);
		parametros.put("PCODERRM", pcoderrm);
		parametros.put("PFECHAINICIAL", fechaInicial);
		parametros.put("PFECHAFINAL", fechaFinal);
		
		byte[] reporteTMP = null;
		PLSiniestros plSiniestros = new PLSiniestros(nombrePlantilla, parametros,Sistema.TIPO_XLS);
		try {
			reporteTMP = plSiniestros.obtenerReporte("");
		} catch (SystemException e1) {}
		if (reporteTMP !=null){
			getListaPlantillas().add(reporteTMP);
		}	
		return reporteTMP;
	}
	
}