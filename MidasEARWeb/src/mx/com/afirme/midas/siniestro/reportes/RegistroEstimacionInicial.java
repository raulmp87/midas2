package mx.com.afirme.midas.siniestro.reportes;

public class RegistroEstimacionInicial{

	private static final long serialVersionUID = 1L;
	
	private String cobertura;
	private String riesgo;
	private Double estimacionInicial;
	private Double estimacionInicialCuotaParte;
	private Double estimacionInicialFacultativo;
	private Double estimacionInicialPrimerExcedente;
	private Double estimacionInicialRetencion;
	
	public String getCobertura() {
		return cobertura;
	}
	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}
	public String getRiesgo() {
		return riesgo;
	}
	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}
	public Double getEstimacionInicial() {
		return estimacionInicial;
	}
	public void setEstimacionInicial(Double estimacionInicial) {
		this.estimacionInicial = estimacionInicial;
	}
	public Double getEstimacionInicialCuotaParte() {
		return estimacionInicialCuotaParte;
	}
	public void setEstimacionInicialCuotaParte(Double estimacionInicialCuotaParte) {
		this.estimacionInicialCuotaParte = estimacionInicialCuotaParte;
	}
	public Double getEstimacionInicialFacultativo() {
		return estimacionInicialFacultativo;
	}
	public void setEstimacionInicialFacultativo(Double estimacionInicialFacultativo) {
		this.estimacionInicialFacultativo = estimacionInicialFacultativo;
	}
	public Double getEstimacionInicialPrimerExcedente() {
		return estimacionInicialPrimerExcedente;
	}
	public void setEstimacionInicialPrimerExcedente(Double estimacionInicialPrimerExcedente) {
		this.estimacionInicialPrimerExcedente = estimacionInicialPrimerExcedente;
	}
	public Double getEstimacionInicialRetencion() {
		return estimacionInicialRetencion;
	}
	public void setEstimacionInicialRetencion(Double estimacionInicialRetencion) {
		this.estimacionInicialRetencion = estimacionInicialRetencion;
	}
		
}