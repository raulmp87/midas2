package mx.com.afirme.midas.siniestro.reportes;

import java.math.BigDecimal;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;

public class ReporteOrdenDePago extends MidasReporteBase {

	private String tipoReporte = Sistema.TIPO_PDF;
	private BigDecimal idToOrdenPago;
	private SoportePagosForm pagosForm;

	public ReporteOrdenDePago (BigDecimal idToOrdenPago){
		this.idToOrdenPago = idToOrdenPago;
	}
	public ReporteOrdenDePago (SoportePagosForm pagosForm){
		this.pagosForm = pagosForm;
	}	

	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		byte byteArray[] = null;
		PlantillaOrdenDePago plantilla;
		if (this.pagosForm != null){
			plantilla= new PlantillaOrdenDePago(pagosForm);
		}else{
			plantilla= new PlantillaOrdenDePago(idToOrdenPago);
		}
		plantilla.setTipoReporte(tipoReporte);		
		byteArray = plantilla.obtenerReporte(claveUsuario);
		return byteArray;
	}
	public String getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public BigDecimal getIdToOrdenPago() {
		return idToOrdenPago;
	}

	public void setIdToOrdenPago(BigDecimal idToOrdenPago) {
		this.idToOrdenPago = idToOrdenPago;
	}
	public SoportePagosForm getPagosForm() {
		return pagosForm;
	}
	public void setPagosForm(SoportePagosForm pagosForm) {
		this.pagosForm = pagosForm;
	}

}
