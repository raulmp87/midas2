package mx.com.afirme.midas.siniestro.reportes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.siniestro.finanzas.autorizaciontecnica.AutorizacionTecnicaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosDN;
import mx.com.afirme.midas.siniestro.finanzas.pagos.SoportePagosForm;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.apache.commons.lang.NullArgumentException;

public class PlantillaOrdenDePago extends MidasPlantillaBase {
	
	private BigDecimal idToOrdenPago;
	private SoportePagosForm pagosForm;
	
	public PlantillaOrdenDePago(BigDecimal idToOrdenPago) throws NullArgumentException {
		this.idToOrdenPago = idToOrdenPago;
		inicializarDatosPlantilla();
	}
	public PlantillaOrdenDePago(SoportePagosForm pagosForm) throws NullArgumentException {
		this.pagosForm = pagosForm;
		inicializarDatosPlantilla();
	}	
	public byte[] obtenerReporte(String claveUsuario) throws SystemException {
		generarReporte(claveUsuario);
		return getByteArrayReport();
	}

	private void generarReporte(String claveUsuario) throws ExcepcionDeAccesoADatos, SystemException{
		if(this.pagosForm != null){
			super.getListaRegistrosContenido().addAll(pagosForm.getAutorizaciones());
			poblarParametrosPlantilla(claveUsuario, pagosForm);			
		}else if (this.idToOrdenPago != null) {
			SoportePagosForm pagosForm = SoportePagosDN.getInstancia()
					.cargaDatosImpresionOrdenDePago(claveUsuario,
							this.idToOrdenPago);
			if (pagosForm != null) {
				super.getListaRegistrosContenido().addAll(pagosForm.getAutorizaciones());
				poblarParametrosPlantilla(claveUsuario, pagosForm);
			}
		}
		super.finalizarReporte();
	}
	private void poblarParametrosPlantilla(String claveUsuario, SoportePagosForm pagosForm) throws SystemException{
		AutorizacionTecnicaDTO autorizacionBase = null;
		StringBuilder polizasAfectadas  = new StringBuilder("");
		StringBuilder reportes = new StringBuilder("");
		if (idToOrdenPago != null){
			getParametrosVariablesReporte().put("IDTOORDENPAGO", idToOrdenPago);
		}else{
			getParametrosVariablesReporte().put("IDTOORDENPAGO", pagosForm.getOrdenDePagoDTO().getIdToOrdenPago());
		}
		if(pagosForm.getAutorizaciones()!= null && pagosForm.getAutorizaciones().size() > 0){
			autorizacionBase = pagosForm.getAutorizaciones().get(0);
			for(AutorizacionTecnicaDTO autorizacion: pagosForm.getAutorizaciones()){
				polizasAfectadas.append(autorizacion.getReporteSiniestroDTO().getPolizaDTO().getNumeroPolizaFormateada()).append(" | ");
				reportes.append(autorizacion.getReporteSiniestroDTO().getNumeroReporte()).append(" | ");
			}
		}
		
		if(autorizacionBase != null){
			if(autorizacionBase.getReporteSiniestroDTO().getPolizaDTO().getCotizacionDTO().getIdMoneda().intValue() == Sistema.MONEDA_PESOS){
				getParametrosVariablesReporte().put("MONEDA", "PESOS");
			}else{
				getParametrosVariablesReporte().put("MONEDA", "DOLARES");
			}
			getParametrosVariablesReporte().put("REPORTES", reportes.toString());
			getParametrosVariablesReporte().put("NOMBRE_PROVEEDOR", autorizacionBase.getGastoSiniestroDTO().getNombrePrestadorServicios());
			getParametrosVariablesReporte().put("POLIZAS_AFECTADAS", polizasAfectadas.toString());
			getParametrosVariablesReporte().put("FECHA_PAGO", pagosForm.getOrdenDePagoDTO().getFechaPago());
			getParametrosVariablesReporte().put("ESTATUS", pagosForm.getOrdenDePagoDTO().getDescripcionEstatus());
			getParametrosVariablesReporte().put("CUENTA_BANCARIA", pagosForm.getOrdenDePagoDTO().getCuentaBancariaEmisora());
			getParametrosVariablesReporte().put("FOLIO", pagosForm.getOrdenDePagoDTO().getFolio());
			getParametrosVariablesReporte().put("MONTO_NETO", pagosForm.getGastoAcumulado().getMontoGasto());
			getParametrosVariablesReporte().put("VALOR_IVA", pagosForm.getGastoAcumulado().getMontoIVA());
			getParametrosVariablesReporte().put("VALOR_IVA_RETENIDO", pagosForm.getGastoAcumulado().getMontoIVARetencion());
			getParametrosVariablesReporte().put("VALOR_ISR_RETENIDO", pagosForm.getGastoAcumulado().getMontoISRRetencion());
			getParametrosVariablesReporte().put("TOTAL", pagosForm.getGastoAcumulado().getMontoOtros());
		}

		getParametrosVariablesReporte().put("URL_LOGO_AFIRME", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
	}
	private void inicializarDatosPlantilla(){
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.siniestros.finanzas.reporte.plantillaOrdenDePago.paquete"));
		setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.siniestros.finanzas.reporte.plantillaOrdenDePago"));
		super.setParametrosVariablesReporte(new HashMap<String, Object>());
		super.setListaRegistrosContenido(new ArrayList<Object>());
		setTipoReporte(Sistema.TIPO_PDF);
	}
	
	public BigDecimal getIdToOrdenPago() {
		return idToOrdenPago;
	}

	public void setIdToOrdenPago(BigDecimal idToOrdenPago) {
		this.idToOrdenPago = idToOrdenPago;
	}
	public SoportePagosForm getPagosForm() {
		return pagosForm;
	}
	public void setPagosForm(SoportePagosForm pagosForm) {
		this.pagosForm = pagosForm;
	}

}
