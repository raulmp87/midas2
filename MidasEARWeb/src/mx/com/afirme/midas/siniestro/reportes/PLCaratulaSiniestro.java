package mx.com.afirme.midas.siniestro.reportes; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import mx.com.afirme.midas.danios.reportes.MidasPlantillaBase;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class PLCaratulaSiniestro extends MidasPlantillaBase{
	private CaratulaSiniestroForm caratulaSiniestroForm;
	
	public PLCaratulaSiniestro(){
		
	}
	
	public PLCaratulaSiniestro(CaratulaSiniestroForm caratulaSiniestroForm){
		super();
		String nombreSubReporte=new String();
		this.caratulaSiniestroForm = caratulaSiniestroForm;
		setListaRegistrosContenido( new ArrayList<Object>());
		Map<String, Object> parametros = new HashMap<String, Object>();
		setNombrePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro"));
		setPaquetePlantilla(UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.paquete"));
		parametros.put("PIMAGE", Sistema.LOGOTIPO_SEGUROS_AFIRME);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.sumaAsegurada");
		JasperReport subReporteSumaAsegurada = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_SUMA_ASEGURADA", subReporteSumaAsegurada);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.estimacionInicial");
		JasperReport subReporteEstimacionInicial = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_ESTIMACION_INICIAL", subReporteEstimacionInicial);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.porcentajesSumaAsegurada");
		JasperReport subReportePorcentajeSumaAsegurada = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_PORC_SA", subReportePorcentajeSumaAsegurada);
		
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.indemnizaciones");
		JasperReport subReporteIndemnizacionesCaratula = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_INDEMNIZACION", subReporteIndemnizacionesCaratula);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.gastos");
		JasperReport subReporteGastosCaratula = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_GASTO", subReporteGastosCaratula);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.ingresos");
		JasperReport subReporteIngresosCaratula = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_INGRESO", subReporteIngresosCaratula);
		
		nombreSubReporte = getPaquetePlantilla()+ 
		UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.sistema.siniestros.reportes.caratulaSiniestro.salvamentos");
		JasperReport subReporteSalvamentosCaratula = getJasperReport(nombreSubReporte);
		parametros.put("SUBREPORTE_SALVAMENTO", subReporteSalvamentosCaratula);
			
		setParametrosVariablesReporte(parametros);
		setTipoReporte( Sistema.TIPO_PDF );
	}
	
	private void procesarDatosReporte(String claveUsuario) throws SystemException {
		if (this.caratulaSiniestroForm==null)return;
		getListaRegistrosContenido().add(this.caratulaSiniestroForm);
		try {
			super.setByteArrayReport( generaReporte(Sistema.TIPO_PDF, getPaquetePlantilla()+getNombrePlantilla(), 
					getParametrosVariablesReporte(), getListaRegistrosContenido()));
		} catch (JRException e) {
			e.printStackTrace();
			setByteArrayReport( null );
		}
	}
	public byte[] obtenerReporte(String nombreUsuario) throws SystemException {
		procesarDatosReporte(nombreUsuario);
		return getByteArrayReport();
	}
}