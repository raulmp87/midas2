package mx.com.afirme.midas.direccion;

import mx.com.afirme.midas.sistema.MidasBaseForm;
import mx.com.afirme.midas.sistema.UtileriasWeb;


public class DireccionForm extends MidasBaseForm{
	private static final long serialVersionUID = -3715080951037045503L;
	private String idToDireccion;
    private String nombreCalle;
    private String numeroExterior;
    private String numeroInterior;
    private String entreCalles;
    private String idColonia;
    private String nombreDelegacion;
    private String idMunicipio;
    private String idEstado;
    private String codigoPostal;
    private String idPadre;
    private String codigoDireccion; //"1"-Direccion contratante. "2"-Direccion cobro. "3"-Direccion asegurado. Para el caso de Cotizacion
    private String descripcionPadre; //String para indicar a qu� tipo de entidad pertenece la direcci�n.
    private String municipio;
    private String estado;
    private String colonia;
	public String getIdToDireccion() {
		return idToDireccion;
	}
	public void setIdToDireccion(String idToDireccion) {
		this.idToDireccion = idToDireccion;
	}
	public String getNombreCalle() {
		return nombreCalle;
	}
	public void setNombreCalle(String nombreCalle) {
		this.nombreCalle = nombreCalle;
	}
	public String getNumeroExterior() {
		return numeroExterior;
	}
	public void setNumeroExterior(String numeroExterior) {
		this.numeroExterior = numeroExterior;
	}
	public String getNumeroInterior() {
		return numeroInterior;
	}
	public void setNumeroInterior(String numeroInterior) {
		this.numeroInterior = numeroInterior;
	}
	public String getEntreCalles() {
		return entreCalles;
	}
	public void setEntreCalles(String entreCalles) {
		this.entreCalles = entreCalles;
	}
	public String getNombreDelegacion() {
		return nombreDelegacion;
	}
	public void setNombreDelegacion(String nombreDelegacion) {
		this.nombreDelegacion = nombreDelegacion;
	}
	public String getIdMunicipio() {
		return idMunicipio;
	}
	public void setIdMunicipio(String idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	/**
	 * Regresa el tipo de direccion de la entidad padre.
	 * En el caso de la p�liza, �sta contiene tres direcciones y los valores permitidos para la p�liza son:
	 * "1"-Direccion contratante. "2"-Direccion cobro. "3"-Direccion asegurado.
	 * @param codigoDireccion
	 */
	public String getCodigoDireccion() {
		return codigoDireccion;
	}
	/**
	 * Establece el tipo de direccion de la entidad padre.
	 * En el caso de la p�liza, �sta contiene tres direcciones y los valores permitidos para la p�liza son:
	 * "1"-Direccion contratante. "2"-Direccion cobro. "3"-Direccion asegurado.
	 * @param codigoDireccion
	 */
	public void setCodigoDireccion(String codigoDireccion) {
		this.codigoDireccion = codigoDireccion;
	}
	public String getIdColonia() {
		return idColonia;
	}
	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}
	public String getDescripcionPadre() {
		return descripcionPadre;
	}
	public void setDescripcionPadre(String descripcionPadre) {
		this.descripcionPadre = descripcionPadre;
	}

	public String toString() {
		String direccion = "";
		direccion += UtileriasWeb.esCadenaVacia(nombreCalle) ? "" : nombreCalle + ", ";
		direccion += UtileriasWeb.esCadenaVacia(numeroExterior) ? "" : "No. "+ numeroExterior + ", ";
		direccion += UtileriasWeb.esCadenaVacia(this.numeroInterior) ? "" : "int. " + numeroInterior + ", ";
		direccion += UtileriasWeb.esCadenaVacia(colonia) ? "" : "Col. " + colonia + ", ";
		direccion += UtileriasWeb.esCadenaVacia(municipio) ? "" : municipio + ", ";
		direccion += UtileriasWeb.esCadenaVacia(estado) ? "" : estado + ", ";
		direccion += UtileriasWeb.esCadenaVacia(codigoPostal) ? "" : "C.P. " + this.codigoPostal;
		return direccion;
	}
}
