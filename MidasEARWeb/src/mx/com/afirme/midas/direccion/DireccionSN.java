package mx.com.afirme.midas.direccion;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class DireccionSN {
	private DireccionFacadeRemote beanRemoto;

	public DireccionSN() throws SystemException {
		try {
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			beanRemoto = serviceLocator.getEJB(DireccionFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}

	public DireccionDTO agregar(DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.save(direccionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void borrar(DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.delete(direccionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public void modificar(DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos {
		try {
			beanRemoto.update(direccionDTO);
		} catch (EJBTransactionRolledbackException e) {
			throw new ExcepcionDeAccesoADatos(e.getClass().getCanonicalName());
		}
	}

	public List<DireccionDTO> listarTodos() throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findAll();
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	public DireccionDTO getPorId(BigDecimal idToDireccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findById(idToDireccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Encuentra un registro de DireccionDTO especificado por un id de CotizacionDTO.
	 * Debido a que CotizacionDTO contiene tres referencias a DireccionDTO, el m�todo recibe un entero indicando qu� direcci�n 
	 * se desea obtener, en base al siguiente orden: 
	 * 1 - Direccion contratante.
	 * 2 - Direccion cobro.
	 * 3 - Direccion asegurado.    
	 * @param int direccionOrden
	  @param idToCotizacion el id del registro idToCotizacion
	  	  @return DireccionDTO found by query
	 */
	public DireccionDTO getPorIdCotizacion(BigDecimal idToCotizacion,int direccionOrden) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByIdCotizacion(idToCotizacion, direccionOrden);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	/**
	 * Encuentra un registro de DireccionDTO especificado por un id de ProveedorInspeccionDTO.
	  @param idToProveedorInspeccion el id del registro ProveedorInspeccionDTO
	  	  @return DireccionDTO found by query
	 */
	public DireccionDTO getPorIdProveedorInspeccion(BigDecimal idToProveedorInspeccion) throws ExcepcionDeAccesoADatos {
		try {
			return beanRemoto.findByIdProveedorInspeccion(idToProveedorInspeccion);
		} catch (Exception e) {
			throw new ExcepcionDeAccesoADatos(Sistema.EXCEPCION_OBTENER_DTO);
		}
	}
	
	
	public List<BigDecimal> listarIdToDireccion(DireccionDTO direccionDTO){
		
			return beanRemoto.listarIdToDirecciones(direccionDTO);
		
	}
	
	
	
}
