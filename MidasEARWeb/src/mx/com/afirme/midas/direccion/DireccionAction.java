package mx.com.afirme.midas.direccion;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionSN;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDTO;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.lang.StringUtils;
/**
 * DireccionAction
 * @author Jose Luis Arellano
 * @since 8 de septiembre de 2009
 */
public class DireccionAction extends MidasMappingDispatchAction {

	/**
	 * M�todo mostrarAgregarDireccion.  
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 */
	public ActionForward mostrarAgregarDireccion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		String reglaNavegacion = Sistema.EXITOSO;
		String idPadre = request.getParameter("idPadre");
		String codigoDireccion = request.getParameter("codigoDireccion");
		String idToDireccion = request.getParameter("idToDireccion");
		String descripcionPadre = request.getParameter("descripcionPadre");
		DireccionForm direccionForm=(DireccionForm) form;
		try {
			if (!UtileriasWeb.esCadenaVacia(idToDireccion))
				//Si el id de la direccion es diferente de cero, buscar el registro para mostrarlo en la JSP
				if (!idToDireccion.equals("0"))
					poblarDireccionForm(direccionForm, DireccionDN.getInstancia().getPorId(UtileriasWeb.regresaBigDecimal(idToDireccion)));
			//Verificar a qu� entidad pertenecer� la direcci�n
			if (!UtileriasWeb.esCadenaVacia(descripcionPadre) && !UtileriasWeb.esCadenaVacia(idPadre) && !UtileriasWeb.esCadenaVacia(codigoDireccion)){
				direccionForm.setIdPadre(idPadre);
				direccionForm.setCodigoDireccion(codigoDireccion);
				direccionForm.setDescripcionPadre(descripcionPadre);
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return mapping.findForward(reglaNavegacion);
	}
	
	public void agregarDireccion(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) {
		DireccionForm direccionForm=(DireccionForm) form;
		DireccionDTO direccionDTO = new DireccionDTO();
		DireccionDN direccionDN = DireccionDN.getInstancia();
		StringBuffer buffer = new StringBuffer();
		StringBuffer bufferErrors = new StringBuffer();
		boolean existenErrores = false;
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><response><item><id>");

		if(direccionForm.getCodigoPostal()== null ||
				direccionForm.getCodigoPostal().equals("")){
			existenErrores = true;
			bufferErrors.append("<li>El C&oacute;digo postal es requerido.</li>");
		}
		if(direccionForm.getNombreCalle() == null ||
			direccionForm.getNombreCalle().equals("")){
			existenErrores = true;
			bufferErrors.append("<li>La Calle es requerido.</li>");				
		}
		if(direccionForm.getIdColonia() == null ||
				direccionForm.getIdColonia().equals("")){
			existenErrores = true;
			bufferErrors.append("<li>La Colonia es requerido.</li>");
		}
		if(direccionForm.getIdEstado() == null ||
				direccionForm.getIdEstado().equals("")){
			existenErrores = true;
			bufferErrors.append("<li>El Estado es requerido.</li>");
		}

		if(direccionForm.getIdMunicipio() == null ||
				direccionForm.getIdMunicipio().equals("")){
			existenErrores = true;
			bufferErrors.append("<li>El Municipio es requerido.</li>");
		}
		if(direccionForm.getNumeroExterior() == null ||
				direccionForm.getNumeroExterior().equals("")){
			existenErrores = true;
			bufferErrors.append("<li>El N&uacute;mero Exterior es requerido.</li>");
		}		
		if (!existenErrores){
			try {				
				if (!UtileriasWeb.esCadenaVacia(direccionForm.getIdToDireccion())){	
					if (!direccionForm.getIdToDireccion().equals("0")){ //Actualizacion de registro
						direccionDTO = direccionDN.getPorId(UtileriasWeb.regresaBigDecimal(direccionForm.getIdToDireccion()));
						poblarDireccionDTO(direccionDTO, direccionForm);
						direccionDN.modificar(direccionDTO);
						buffer.append("1</id><description><![CDATA[La direcci&oacute;n se actualiz&oacute; correctamente.]]></description><clave>" + direccionDTO.getIdToDireccion() + "</clave>");
					}
					else{	//Nuevo registro
						poblarDireccionDTO(direccionDTO, direccionForm);
						direccionDTO.setIdToDireccion(null);
						direccionDTO = direccionDN.agregar(direccionDTO);
						buffer.append("1</id><description><![CDATA[La direcci&oacute;n se registr&oacute; correctamente.]]></description><clave>" + direccionDTO.getIdToDireccion() + "</clave>");
					}
				}
				//Si se recibe un idPadre, se verifica la descripcion del padre y se registrar� la nueva direcci�n a la entidad correspondiente.
				//La direccion ser� la correspondiente al c�digo contenido en el form.
				if (!UtileriasWeb.esCadenaVacia(direccionForm.getIdPadre()) && !UtileriasWeb.esCadenaVacia(direccionForm.getDescripcionPadre())){
					if (direccionForm.getDescripcionPadre().equals("cotizacion")){
						CotizacionDTO cotizacionDTO = new CotizacionDTO();
						cotizacionDTO.setIdToCotizacion(UtileriasWeb.regresaBigDecimal(direccionForm.getIdPadre()));
						CotizacionSN cotizacionSN = new CotizacionSN(UtileriasWeb.obtieneNombreUsuario(request));
						cotizacionDTO = cotizacionSN.getPorId(cotizacionDTO);
						cotizacionDTO = cotizacionSN.obtenerDatosForaneos(cotizacionDTO);
						switch (Integer.valueOf(direccionForm.getCodigoDireccion())){
							case 1:	//"1"-Direccion contratante.
								//TODO Verificar el cambio en la tabla cotizacion
								//cotizacionDTO.setDireccionContratanteDTO(direccionDTO);
								break;
							case 2: //"2"-Direccion cobro.
								cotizacionDTO.setDireccionCobroDTO(direccionDTO);
								break;
							case 3: //"3"-Direccion asegurado
								//TODO Verificar el cambio en la tabla cotizacion
								//cotizacionDTO.setDireccionAseguradoDTO(direccionDTO);
								break;
						}
						cotizacionSN.modificar(cotizacionDTO);
					}
					else if (direccionForm.getDescripcionPadre().startsWith("proveedorInspeccion")){
						//Si es un nuevo proveedor, a�n no se tiene su ID, se usar� la sesi�n para guardar el DTO.
						if (direccionForm.getDescripcionPadre().endsWith("Nuevo")){
							request.getSession().setAttribute("direccionProveedor", direccionDTO);
						}
						else{
							ProveedorInspeccionDTO proveedorDTO = new ProveedorInspeccionDTO();
							proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().getPorId(UtileriasWeb.regresaBigDecimal(direccionForm.getIdPadre()));
							proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
							proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarDireccionDTO(proveedorDTO);
							proveedorDTO.setDireccionDTO(direccionDTO);
							ProveedorInspeccionDN.getINSTANCIA().modificar(proveedorDTO);
						}
					}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			} catch (Exception e){
				buffer.append("2</id><description><![CDATA[Ocurri&oacute; un error al registrar la direcci&oacute;n.\nVerifique los datos.]]></description>");
			}				
		
		}else{
			buffer.append("2</id><description><![CDATA[Ocurri&oacute; un error al registrar la direcci&oacute;n.\n"+bufferErrors.toString()+"]]></description>");
		}
		buffer.append("</item></response>");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength(buffer.length());
		try {
			response.getWriter().write(buffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}				
	}
	
	public static void poblarDireccionForm(DireccionForm direccionForm, DireccionDTO direccionDTO) throws SystemException{
		if (direccionForm != null && direccionDTO != null){
			if (direccionDTO.getIdToDireccion() != null)
				direccionForm.setIdToDireccion(direccionDTO.getIdToDireccion().toString());
			if (direccionDTO.getCodigoPostal() != null)
				direccionForm.setCodigoPostal(direccionDTO.getCodigoPostal().toBigInteger().toString());
			if (direccionDTO.getIdEstado() != null){
				EstadoFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(EstadoFacadeRemote.class);
				//El Estado proviene de la vista VW_STATE y el campo que funciona como ID de los estados es STATE_ID
				//pero este campo es de tipo VARCHAR y en la clase EstadoDTO se mape� como "String stateId", por lo cual los registros se deben 
				//buscar enviando un atributo tipo String, el cual debe contener 5 caracteres, ya que as� est�n registrados en la vista.
				//Por lo tanto, al obtener el IdEstado del objeto DireccionDTO, se debe validar si el String correspondiente tiene esta longitud.
				//De no ser as�, a la cadena se le agregar�n tantos ceros a la izquierda como sean necesarios para completar la longitud de 5 caracteres.
				String idEstado = direccionDTO.getIdEstado().toBigInteger().toString();
				direccionForm.setEstado(beanRemoto.findById(obtenerId(idEstado)).getStateName());
				direccionForm.setIdEstado(idEstado);
			}
			if (direccionDTO.getIdMunicipio() != null){
				//El Municipio proviene de la vista VW_MUNICIPALITY y el campo que funciona como ID de los estados es MUNICIPALITY_ID
				//pero este campo es de tipo VARCHAR y en la clase MunicipioDTO se mape� como "String municipalityId", por lo cual los registros se deben 
				//buscar enviando un atributo tipo String, el cual debe contener 5 caracteres, ya que as� est�n registrados en la vista.
				//Por lo tanto, al obtener el IdMunicipio del objeto DireccionDTO, se debe validar si el String correspondiente tiene esta longitud.
				//De no ser as�, a la cadena se le agregar�n tantos ceros a la izquierda como sean necesarios para completar la longitud de 5 caracteres.
				MunicipioFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(MunicipioFacadeRemote.class);
				String idMunicipio = direccionDTO.getIdMunicipio().toBigInteger().toString();
				direccionForm.setMunicipio(beanRemoto.findById(obtenerId(idMunicipio)).getMunicipalityName());
				direccionForm.setIdMunicipio(idMunicipio);				
			}
			if (direccionDTO.getNumeroExterior() != null)
				direccionForm.setNumeroExterior(direccionDTO.getNumeroExterior());
			if (direccionDTO.getNombreColonia() != null){
				direccionForm.setIdColonia(direccionDTO.getNombreColonia());
				ColoniaFacadeRemote beanRemoto = ServiceLocator.getInstance().getEJB(ColoniaFacadeRemote.class);
				String descripcionColonia;
				try{
					descripcionColonia = beanRemoto.findById(direccionDTO.getNombreColonia()).getColonyName();
				}catch(NullPointerException e){	descripcionColonia =direccionDTO.getNombreColonia();}
				direccionForm.setColonia(descripcionColonia );
			}
			direccionForm.setEntreCalles(direccionDTO.getEntreCalles());
			direccionForm.setNombreCalle(direccionDTO.getNombreCalle());
			direccionForm.setNumeroInterior(direccionDTO.getNumeroInterior());
		}
	}
	
	public static  String obtenerId(String id) {
		return StringUtils.leftPad(id, 5, '0');	
	}

	public static void poblarDireccionDTO(DireccionDTO direccionDTO, DireccionForm direccionForm) throws Exception{
		if (direccionForm != null && direccionDTO != null){
			if (!UtileriasWeb.esCadenaVacia(direccionForm.getIdToDireccion()))
				direccionDTO.setIdToDireccion(UtileriasWeb.regresaBigDecimal(direccionForm.getIdToDireccion()));
			if (!UtileriasWeb.esCadenaVacia(direccionForm.getCodigoPostal() )) {
				ServiceLocator serviceLocator = ServiceLocator.getInstance();
				ColoniaFacadeRemote beanRemoto = serviceLocator.getEJB(ColoniaFacadeRemote.class);
				List<ColoniaDTO> list = beanRemoto.findByProperty("zipCode",direccionForm.getCodigoPostal());
				if(list.isEmpty()) throw new Exception("No existe Codigo Postal.");
				direccionDTO.setCodigoPostal(UtileriasWeb.regresaBigDecimal(direccionForm.getCodigoPostal()));
			}
			if (!UtileriasWeb.esCadenaVacia(direccionForm.getIdEstado() ))
				direccionDTO.setIdEstado(UtileriasWeb.regresaBigDecimal(direccionForm.getIdEstado()));
			if (!UtileriasWeb.esCadenaVacia(direccionForm.getIdMunicipio()))
				direccionDTO.setIdMunicipio(UtileriasWeb.regresaBigDecimal(direccionForm.getIdMunicipio()));
			if (!UtileriasWeb.esCadenaVacia(direccionForm.getNumeroExterior()))
				direccionDTO.setNumeroExterior(direccionForm.getNumeroExterior());
			direccionDTO.setEntreCalles(direccionForm.getEntreCalles());
			direccionDTO.setNombreCalle(direccionForm.getNombreCalle());
			direccionDTO.setNombreColonia(direccionForm.getIdColonia());
			direccionDTO.setNumeroInterior(direccionForm.getNumeroInterior());
		}
	}
}
