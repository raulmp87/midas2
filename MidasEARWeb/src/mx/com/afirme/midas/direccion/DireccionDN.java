package mx.com.afirme.midas.direccion;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;


public class DireccionDN {
	public static final DireccionDN INSTANCIA = new DireccionDN();

	public static DireccionDN getInstancia (){
		return DireccionDN.INSTANCIA;
	}
	public DireccionDTO agregar(DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos, SystemException {
		return new DireccionSN().agregar(direccionDTO);
	}

	public void borrar(DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos, SystemException {
		new DireccionSN().borrar(direccionDTO);
	}

	public void modificar(DireccionDTO direccionDTO) throws ExcepcionDeAccesoADatos, SystemException {
		new DireccionSN().modificar(direccionDTO);
	}

	public List<DireccionDTO> listarTodos() throws ExcepcionDeAccesoADatos, SystemException {
		return new DireccionSN().listarTodos();
	}
	
	public DireccionDTO getPorId(BigDecimal idToDireccion) throws ExcepcionDeAccesoADatos, SystemException{
		return new DireccionSN().getPorId(idToDireccion);
	}
	
	public String obtenerDescripcionDireccion(DireccionDTO direccionDTO){
		if (direccionDTO != null){
			String descripcion = "";
			descripcion += (UtileriasWeb.esCadenaVacia(direccionDTO.getNombreCalle())? "" : direccionDTO.getNombreCalle());
			descripcion += (!UtileriasWeb.esCadenaVacia(direccionDTO.getNumeroExterior())) ? " No. "+direccionDTO.getNumeroExterior(): "";
			descripcion += (!UtileriasWeb.esCadenaVacia(direccionDTO.getNumeroInterior())) ? " Int. "+direccionDTO.getNumeroInterior() : "";
			if (!UtileriasWeb.esCadenaVacia(direccionDTO.getNombreColonia())){
				String descripcionColonia = "Colonia no disponible";
				ColoniaFacadeRemote beanRemoto;
				try {
					beanRemoto = ServiceLocator.getInstance().getEJB(ColoniaFacadeRemote.class);
					try{
						descripcionColonia = beanRemoto.findById(direccionDTO.getNombreColonia()).getColonyName();
					}catch(Exception e){	descripcionColonia =direccionDTO.getNombreColonia();}
				} catch (SystemException e1) {
				}
				descripcion += ", Col. "+descripcionColonia;
			}
			if (direccionDTO.getIdMunicipio() != null){
				//El Municipio proviene de la vista VW_MUNICIPALITY y el campo que funciona como ID de los estados es MUNICIPALITY_ID
				//pero este campo es de tipo VARCHAR y en la clase MunicipioDTO se mape� como "String municipalityId", por lo cual los registros se deben 
				//buscar enviando un atributo tipo String, el cual debe contener 5 caracteres, ya que as� est�n registrados en la vista.
				//Por lo tanto, al obtener el IdMunicipio del objeto DireccionDTO, se debe validar si el String correspondiente tiene esta longitud.
				//De no ser as�, a la cadena se le agregar�n tantos ceros a la izquierda como sean necesarios para completar la longitud de 5 caracteres.
				MunicipioFacadeRemote beanRemoto;
				try {
					beanRemoto = ServiceLocator.getInstance().getEJB(MunicipioFacadeRemote.class);
					String idMunicipio = direccionDTO.getIdMunicipio().toBigInteger().toString();
					descripcion += ", "+beanRemoto.findById(obtenerId(idMunicipio)).getMunicipalityName();
				} catch (SystemException e) {}
				catch(Exception e){}
			}
			if (direccionDTO.getIdEstado() != null){
				//El Estado proviene de la vista VW_STATE y el campo que funciona como ID de los estados es STATE_ID
				//pero este campo es de tipo VARCHAR y en la clase EstadoDTO se mape� como "String stateId", por lo cual los registros se deben 
				//buscar enviando un atributo tipo String, el cual debe contener 5 caracteres, ya que as� est�n registrados en la vista.
				//Por lo tanto, al obtener el IdEstado del objeto DireccionDTO, se debe validar si el String correspondiente tiene esta longitud.
				//De no ser as�, a la cadena se le agregar�n tantos ceros a la izquierda como sean necesarios para completar la longitud de 5 caracteres.
				EstadoFacadeRemote beanRemoto;
				try {
					beanRemoto = ServiceLocator.getInstance().getEJB(EstadoFacadeRemote.class);
					String idEstado = direccionDTO.getIdEstado().toBigInteger().toString();
					descripcion+= ", " + beanRemoto.findById(obtenerId(idEstado)).getStateName();
				} catch (SystemException e) {}
				catch(Exception e){}
			}
			descripcion += (direccionDTO.getCodigoPostal() != null)? " C.P. "+direccionDTO.getCodigoPostal().toBigInteger() : "";
			return descripcion;
		}
		else
			return null;
	}
	
	
	public String obtenerId(String id) {
		return StringUtils.leftPad(id, 5, '0');	
	}
	public List<BigDecimal> listarIdToDirecciones(DireccionDTO direccionDTO) throws SystemException{
		return  new DireccionSN().listarIdToDireccion(direccionDTO);
	}
	
	
}
