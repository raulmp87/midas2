package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.zonahidro.ZonaHidroDTO;

import org.displaytag.decorator.TableDecorator;

public class ZonaHidro extends TableDecorator {

	public String getAcciones() {
		ZonaHidroDTO zonaHidroDTO = (ZonaHidroDTO) getCurrentRowObject();
		String idZonaHidro = zonaHidroDTO.getIdTcZonaHidro().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonahidro/mostrarDetalle.do?id="
				+ idZonaHidro
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonahidro/mostrarModificar.do?id="
				+ idZonaHidro
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonahidro/mostrarBorrar.do?id="
				+ idZonaHidro
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
