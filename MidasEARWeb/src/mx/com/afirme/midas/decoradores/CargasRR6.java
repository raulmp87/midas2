package mx.com.afirme.midas.decoradores;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.danios.reportes.reporterr6.CargaRR6DTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.displaytag.decorator.TableDecorator;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class CargasRR6 extends TableDecorator {
	public static final int bloqueado = 1;
	
	public String getFechaFin() {
		CargaRR6DTO documentoDigitalRR6DTO = (CargaRR6DTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getFechaFin());
	}
	
	public String getFechaCarga() {
		CargaRR6DTO documentoDigitalRR6DTO = (CargaRR6DTO) getCurrentRowObject();
		return UtileriasWeb.getFechaHoraString(documentoDigitalRR6DTO.getFechaCarga());
	}
	
	public String getReporte() {
		CargaRR6DTO documentoDigitalRR6DTO = (CargaRR6DTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getReporte();
	}
		
	public String getEstatus() {
		CargaRR6DTO documentoDigitalRR6DTO = (CargaRR6DTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getEstatus();
	}
	
	public String getUsuario() {
		CargaRR6DTO documentoDigitalRR6DTO = (CargaRR6DTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getUsuario();
	}
	
	private SistemaContext getSistemaContext(HttpServletRequest request) {
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext());		
		return webApplicationContext.getBean(SistemaContext.class);
	}
	
	/**
	 * toma la cadena para mostrar las acciones de carga
	 * 
	 * @return String El texto HTML para la columna de acciones Carga
	 */
	public String getAcciones() {
		
		HttpServletRequest request = (HttpServletRequest) super.getPageContext().getRequest();
		SistemaContext sistemaContext = getSistemaContext(request);
		
		Usuario usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, sistemaContext.getUsuarioAccesoMidas());
		

		CargaRR6DTO cargaDTO = (CargaRR6DTO) getCurrentRowObject();
		String id = cargaDTO.getIdcarga().toString();
		String idArchivo = cargaDTO.getIdArchivo().toString();
		String candado = cargaDTO.getCandado().toString();
		String fechaFin = UtileriasWeb.getFechaString(cargaDTO.getFechaFin());
		StringBuffer buffer = new StringBuffer();
		String titulo = "";
		if(cargaDTO.getCandado() == bloqueado){
			titulo = "Desbloquear carga";
			buffer.append("<center>");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: window.open('/MidasWeb/danios/reportes/reporterr6/descargarReporte.do?id="
							+ id +"&idControlArchivo=" + idArchivo
							+"&fechaFin=" + cargaDTO.getFechaFin()
							+"&fechaCarga=" + cargaDTO.getFechaCarga()
							+"iniEjecuccion=" + cargaDTO.getReporte()
							+"&estatus=" + cargaDTO.getEstatus()
							+"&usuario=" + cargaDTO.getUsuario()
							+"&candado=" + candado
							+ "', 'resultados',null);\"><img border='0px' title='Descargar' src='/MidasWeb/img/download-icon.jpg'/></a>&nbsp;");
			buffer.append("<img border='0px' title='Bloqueado para carga' src='/MidasWeb/img/icons/ico_bloquear.gif'/>&nbsp;");
			if(validaRolCandado(usuario)){
			buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null, '/MidasWeb/danios/reportes/reporterr6/actualizarEstatusCarga.do?id="
				+ id +"&estatusCarga="+ 0 +"&fechaFinal=" + fechaFin
				+ "', 'resultados',null);\"><img border='0px' title="+titulo+ " src='/MidasWeb/img/menu_icons/save.gif'/></a>");
		  }
			buffer.append("&nbsp;");			
		}else{
			titulo = "Bloquear carga";
			buffer.append("<center>");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: window.open('/MidasWeb/danios/reportes/reporterr6/descargarReporte.do?id="
							+ id +"&idControlArchivo=" + idArchivo
							+"&fechaFin=" + cargaDTO.getFechaFin()
							+"&fechaCarga=" + cargaDTO.getFechaCarga()
							+"iniEjecuccion=" + cargaDTO.getReporte()
							+"&estatus=" + cargaDTO.getEstatus()
							+"&candado=" + candado
							+ "', 'resultados',null);\"><img border='0px' title='Descargar' src='/MidasWeb/img/download-icon.jpg'/></a>&nbsp;");
			buffer.append("<img border='0px' title='Disponible para carga' src='/MidasWeb/img/icons/check_in.png'/>&nbsp;");
			if(validaRolCandado(usuario)){
				buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null, '/MidasWeb/danios/reportes/reporterr6/actualizarEstatusCarga.do?id="
					+ id +"&estatusCarga="+ 1 +"&fechaFinal=" + fechaFin
					+ "', 'resultados',null);\"><img border='0px' title="+titulo+ " src='/MidasWeb/img/menu_icons/save.gif'/></a>");
			}
			buffer.append("&nbsp;");	
		}
		
		
		return buffer.toString();
	}
	
	private boolean validaRolCandado(Usuario usuario){
		List<Rol> roles = usuario.getRoles();
		for (Rol rol: roles){
			if (rol.getDescripcion().equalsIgnoreCase("Rol_Director_Rea")){
				return true;
			}
		}
		return false;
	}
	
}
