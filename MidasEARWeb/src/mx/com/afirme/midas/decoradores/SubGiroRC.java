package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.girorc.SubGiroRCDTO;

import org.displaytag.decorator.TableDecorator;

public class SubGiroRC extends TableDecorator{
	
//	private BigDecimal idTcSubGiro;
//	private BigDecimal descripcionGiro;
//	private GiroDTO giroDTO;
//	private BigDecimal codigoSubGiro;
//	private String descripcionSubGiro;
//	private BigDecimal idGrupoRobo;
//	private BigDecimal idGrupoDineroValores;
//	private BigDecimal idGrupoCristales;
//	private BigDecimal idGrupoPlenos;
//	private Short claveInspeccion;
	
	public String getDescripcionGiroRC(){
		SubGiroRCDTO subGiroRCDTO= (SubGiroRCDTO) getCurrentRowObject();
		return subGiroRCDTO.getGiroRC().getDescripcionGiroRC();
	}
	
	public String getCodigoSubGiroRC(){
		SubGiroRCDTO subGiroRCDTO= (SubGiroRCDTO) getCurrentRowObject();
		return subGiroRCDTO.getCodigoSubGiroRC().toBigInteger().toString();
	}
	
	public String getIdTcSubGiroRC(){
		SubGiroRCDTO subGiroRCDTO = (SubGiroRCDTO) getCurrentRowObject();
		return subGiroRCDTO.getIdTcSubGiroRC().toBigInteger().toString();
	}
	
	public String getDescripcion(){
		SubGiroRCDTO subGiroRCDTO = (SubGiroRCDTO) getCurrentRowObject();
		return subGiroRCDTO.getDescripcionSubGiroRC();
	}
	
	public String getAcciones() {

		SubGiroRCDTO subGiroRCDTO = (SubGiroRCDTO) getCurrentRowObject();
		String idSubGiroRC = subGiroRCDTO.getIdTcSubGiroRC().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subgirorc/mostrarDetalle.do?id="
				+ idSubGiroRC
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subgirorc/mostrarModificar.do?id="
				+ idSubGiroRC
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subgirorc/mostrarBorrar.do?id="
				+ idSubGiroRC
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
