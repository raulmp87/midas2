package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tiporecipientepresion.SubtipoRecipientePresionDTO;

import org.displaytag.decorator.TableDecorator;

public class SubTipoRecPresion extends TableDecorator{
	
	/**
	 * fecha: 04/Agosto/2009
	 */

	public String getDescripcionSubTipoRecPresion(){
		SubtipoRecipientePresionDTO subTipoRecPresionDTO = new SubtipoRecipientePresionDTO();
		String  subTipoRecPresion = subTipoRecPresionDTO.getDescripcionSubtipoRecPresion();
		return subTipoRecPresion;
	}
	
	public String getCodigoSubtipoRecPresion(){
		SubtipoRecipientePresionDTO subTipoRecPresionDTO = new SubtipoRecipientePresionDTO();
		String  subTipoRecPresion = subTipoRecPresionDTO.getCodigoSubtipoRecipientePresion().toBigInteger().toString();
		return subTipoRecPresion;
	}
	
	public String getAcciones(){
		SubtipoRecipientePresionDTO subTipoRecPresionDTO = (SubtipoRecipientePresionDTO)getCurrentRowObject();
		String idSubTipoRecPresion = subTipoRecPresionDTO.getIdSubtipoRecipientePresion().toBigInteger().toString();
		
		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subtiporecipientepresion/mostrarDetalle.do?id="
				+ idSubTipoRecPresion
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subtiporecipientepresion/mostrarModificar.do?id="
				+ idSubTipoRecPresion
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subtiporecipientepresion/mostrarBorrar.do?id="
				+ idSubTipoRecPresion
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
