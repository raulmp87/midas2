package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.direccion.DireccionAction;
import mx.com.afirme.midas.direccion.DireccionForm;
import mx.com.afirme.midas.sistema.SystemException;

import org.displaytag.decorator.TableDecorator;

public class IncisoCotizacion extends TableDecorator {
	public String getUbicacion() throws SystemException {
		IncisoCotizacionDTO incisoCotizacionDTO = (IncisoCotizacionDTO) getCurrentRowObject();
		DireccionForm direccionForm = new DireccionForm();
		DireccionAction.poblarDireccionForm(direccionForm , incisoCotizacionDTO.getDireccionDTO());
		return direccionForm.toString();
	}
	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {

		IncisoCotizacionDTO incisoCotizacionDTO = (IncisoCotizacionDTO) getCurrentRowObject();
		String id = incisoCotizacionDTO.getId().getIdToCotizacion().toString();
		String numeroInciso = incisoCotizacionDTO.getId().getNumeroInciso().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/inciso/mostrarBorrar.do?id="
						+ id + "&numeroInciso=" + numeroInciso
						+ "', 'configuracion_detalle','mostrarDatosInciso(" + id + ", 1, " + numeroInciso + ", 1)');\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/inciso/mostrarModificar.do?id="
						+ id + "&numeroInciso=" + numeroInciso
						+ "', 'configuracion_detalle','mostrarDatosInciso(" + id + ", 1, " + numeroInciso + ", 0)');\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/inciso/mostrarDetalle.do?id="
						+ id + "&numeroInciso=" + numeroInciso
						+ "', 'configuracion_detalle','mostrarDatosInciso(" + id + ", 1, " + numeroInciso + ", 1)');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: mostrarCopiarInciso("
				+ id + "," + numeroInciso
				+ ");\"><img border='0px' alt='Copiar inciso' title='Copiar inciso' src='/MidasWeb/img/ico_Doc_ok.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/inciso/separarInciso.do?idToCotizacion="
				+ id + "&idInciso=" + numeroInciso
				+ "', 'contenido','mostrarListaIncisos("+id+","+numeroInciso+");');\"><img border='0px' alt='Separar inciso' title='Separar inciso' src='/MidasWeb/img/ico_Doc_ok.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: window.open('/MidasWeb/cotizacion/inciso/generarTemplateCargaMasiva.do?idToCotizacion="
				+ id + "&numeroInciso=" + numeroInciso
				+ "', 'download');\"><img border='0px' alt='Generar Template de Carga Masiva' title='Generar Template de Carga Masiva' src='/MidasWeb/img/logoXLS.png'/></a>");
		buffer.append("&nbsp;");
		buffer.append("</center>");
		return buffer.toString();
	}
	
	
	public String getAccionesSoloLectura() {

		IncisoCotizacionDTO incisoCotizacionDTO = (IncisoCotizacionDTO) getCurrentRowObject();
		String id = incisoCotizacionDTO.getId().getIdToCotizacion().toString();
		String numeroInciso = incisoCotizacionDTO.getId().getNumeroInciso().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacionsololectura/mostrarDetalleInciso.do?id="
						+ id + "&numeroInciso=" + numeroInciso
						+ "', 'configuracion_detalle','mostrarDatosIncisoSoloLectura(" + id + ", 1, " + numeroInciso + ", 1)');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		buffer.append("&nbsp;");
		
		buffer.append("</center>");
		return buffer.toString();
	}
	
	public String getAccionesSoloLecturaOrden() {

		IncisoCotizacionDTO incisoCotizacionDTO = (IncisoCotizacionDTO) getCurrentRowObject();
		String id = incisoCotizacionDTO.getId().getIdToCotizacion().toString();
		String numeroInciso = incisoCotizacionDTO.getId().getNumeroInciso().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/ordentrabajosololectura/mostrarDetalleInciso.do?id="
						+ id + "&numeroInciso=" + numeroInciso
						+ "', 'configuracion_detalle','mostrarDatosIncisoSoloLectura(" + id + ", 1, " + numeroInciso + ", 1)');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		buffer.append("&nbsp;");
		
		buffer.append("</center>");
		return buffer.toString();
	}

}
