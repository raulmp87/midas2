package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipomuro.TipoMuroDTO;
import org.displaytag.decorator.TableDecorator;

public class TipoMuro extends TableDecorator {
	
	public String getCodigoTipoMuro(){
		TipoMuroDTO tipoMuroDTO = (TipoMuroDTO) getCurrentRowObject();
		String codigoTipoMuro = tipoMuroDTO.getCodigoTipoMuro().toBigInteger().toString();
		return codigoTipoMuro;
	}
	
	public String getAcciones() {

		TipoMuroDTO tipoMuroDTO = (TipoMuroDTO) getCurrentRowObject();
		String idTipoMuro = tipoMuroDTO.getIdTipoMuro().toBigInteger().toString();
		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipomuro/mostrarDetalle.do?id="
				+ idTipoMuro
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipomuro/mostrarModificar.do?id="
				+ idTipoMuro
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipomuro/mostrarBorrar.do?id="
				+ idTipoMuro
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
