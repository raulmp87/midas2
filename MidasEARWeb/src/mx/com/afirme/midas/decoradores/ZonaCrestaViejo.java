package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.catalogos.zonacresta.ZonaCrestaViejoDTO;

import org.displaytag.decorator.TableDecorator;

public class ZonaCrestaViejo extends TableDecorator {
	
	
	public String getNumeroareaviejo(){
		ZonaCrestaViejoDTO zoCrestaViejoDTO= (ZonaCrestaViejoDTO) getCurrentRowObject();
		String numeroAreaViejo = zoCrestaViejoDTO.getNumeroareaviejo().toBigInteger().toString();
		return numeroAreaViejo;
	}
	

	public String getTipozona(){
		ZonaCrestaViejoDTO zoCrestaViejoDTO= (ZonaCrestaViejoDTO) getCurrentRowObject();
		String tipoZona = zoCrestaViejoDTO.getTipozona().toString();
			if (tipoZona.startsWith("0"))
				tipoZona = "TERREMOTO";
			else
				tipoZona = "HIDROMETEOROLOGICO";
		return tipoZona;
	}
	
	public String getAsignar(){
		ZonaCrestaViejoDTO zoCrestaViejoDTO= (ZonaCrestaViejoDTO) getCurrentRowObject();
		String idZonaCresta = zoCrestaViejoDTO.getIdtczonacrestaviejo().toString();
		
		return "<a href=\"javascript: void(0);\" "
		+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/agregarTr.do?id="
		+ idZonaCresta
		+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>";
	}
	
	public String getBorrar(){
		ZonaCrestaViejoDTO zoCrestaViejoDTO= (ZonaCrestaViejoDTO) getCurrentRowObject();
		String idZonaCresta = zoCrestaViejoDTO.getIdtczonacrestaviejo().toString();
		return 	"<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el registro seleccionado?',null,'/MidasWeb/catalogos/zonacresta/viejo/borrar.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

	
	public String getAcciones() {

		ZonaCrestaViejoDTO zoCrestaViejoDTO= (ZonaCrestaViejoDTO) getCurrentRowObject();
		String idZonaCresta = zoCrestaViejoDTO.getIdtczonacrestaviejo().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarDetalle.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarModificar.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarBorrar.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
