package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;
import java.text.ParseException;

import mx.com.afirme.midas.siniestro.cabina.PolizaDummyDN;
import mx.com.afirme.midas.siniestro.cabina.PolizaDummyDTO;
import mx.com.afirme.midas.siniestro.cabina.RiesgoPolizaDummyDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.displaytag.decorator.TableDecorator;

public class RiesgoPoliza extends TableDecorator{
	RiesgoPolizaDummyDTO rdto;
	
	public RiesgoPoliza()throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		BigDecimal d 					= new BigDecimal(241);
		PolizaDummyDTO poliza = new PolizaDummyDTO();
		poliza.setNumeroPoliza(d);
		PolizaDummyDN dd = PolizaDummyDN.getInstancia();
		rdto = dd.llenaPolizaRiesgo(poliza);
	}

	/**
	 * @return the subInciso
	 */
	public String getSubInciso() {
		return rdto.getSubInciso();
	}

	/**
	 * @return the riesgoAfectado
	 */
	public boolean isRiesgoAfectado() {
		return rdto.isRiesgoAfectado();
	}

	/**
	 * @return the valorSumaAsegurada
	 */
	public Double getValorSumaAsegurada() {
		return rdto.getValorSumaAsegurada();
	}
	
	
	
}
