package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDN;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class Poliza extends MidasBaseDecorator {
	
	private static final String DEFAULT_VALOR="1";

	public String getNumeroPoliza() {
		PolizaDTO polizaDTO = (PolizaDTO) getCurrentRowObject();
		String numeroPoliza = "";
		if((polizaDTO.getCodigoProducto()!=null && polizaDTO.getCodigoProducto().length()>0)
				&& (polizaDTO.getCodigoTipoPoliza()!=null && polizaDTO.getCodigoTipoPoliza().length()>0)
				&& (polizaDTO.getNumeroPoliza()!=null)
				&& (polizaDTO.getNumeroRenovacion()!=null)){
			try{
				numeroPoliza += String.format("%02d", Integer.parseInt(polizaDTO.getCodigoProducto()));
				numeroPoliza += String.format("%02d", Integer.parseInt(polizaDTO.getCodigoTipoPoliza()));
				numeroPoliza += "-";
				numeroPoliza += String.format("%06d", polizaDTO.getNumeroPoliza());
				numeroPoliza += "-";
				numeroPoliza += String.format("%02d", polizaDTO.getNumeroRenovacion());
			}catch (Exception e) {
				return "";
			}
			
		}
		return numeroPoliza;
	}
	public String getNombreAsegurado(){
		String cliente = "No disponible";
		PolizaDTO polizaDTO = (PolizaDTO) getCurrentRowObject();
		CotizacionDTO cotizacion = polizaDTO.getCotizacionDTO();
		ClienteDTO asegurado = null;
		if (cotizacion.getIdToPersonaAsegurado() != null){
			try{
				if(cotizacion.getIdDomicilioAsegurado() != null && cotizacion.getIdDomicilioAsegurado().intValue() > 0){
					asegurado = new ClienteDTO();
					asegurado.setIdCliente(cotizacion.getIdToPersonaAsegurado());
					asegurado.setIdDomicilio(cotizacion.getIdDomicilioAsegurado());
					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, cotizacion.getCodigoUsuarioCotizacion());
				}else{
					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacion.getIdToPersonaAsegurado(), cotizacion.getCodigoUsuarioCotizacion());
				}	
			}catch(Exception exc){}
			if (asegurado != null){
				if (asegurado.getClaveTipoPersona() == 1)
					cliente = asegurado.getNombre() + " " + asegurado.getApellidoPaterno() + " " + asegurado.getApellidoMaterno();
				else
					cliente = asegurado.getNombre();
			}
		}
		return cliente;
	}
	/*public String getNumeroPolizaAsoc() {
		PolizaDTO polizaDTO = (PolizaDTO) getCurrentRowObject();
		String numeroPoliza = "";
		if(!polizaDTO.getNumeroPolizaAsoc().equals(BigDecimal.ZERO)) {
			if((polizaDTO.getCodigoProductoAsoc()!=null && polizaDTO.getCodigoProductoAsoc().length()>0)
					&& (polizaDTO.getCodigoTipoPolizaAsoc()!=null && polizaDTO.getCodigoTipoPolizaAsoc().length()>0)
					&&(polizaDTO.getNumeroPolizaAsoc()!=null)
					&& (polizaDTO.getNumeroRenovacionAsoc()!=null)){
				try{
					numeroPoliza += String.format("%02d", Integer.parseInt(polizaDTO.getCodigoProductoAsoc()));
					numeroPoliza += String.format("%02d", Integer.parseInt(polizaDTO.getCodigoTipoPolizaAsoc()));
					numeroPoliza += "-";
					numeroPoliza += String.format("%06d", polizaDTO.getNumeroPolizaAsoc());
					numeroPoliza += "-";
					numeroPoliza += String.format("%02d", polizaDTO.getNumeroRenovacionAsoc());
				}catch (Exception e) {
					return "";
				}
			}
		}
		return numeroPoliza;
	}*/

	public String getClaveEstatus() {
		PolizaDTO polizaDTO = (PolizaDTO) getCurrentRowObject();
		String estatus = "";
		if(polizaDTO.getClaveEstatus().compareTo((short)0) == 0) {
			estatus = "CANCELADA";
		} else {
			estatus = "VIGENTE";
		}
		return estatus;
	}

	public String getAcciones() throws ExcepcionDeAccesoADatos, SystemException {
		PolizaDTO polizaDTO = (PolizaDTO) getCurrentRowObject();
		String id = polizaDTO.getIdToPoliza().toString();
		HttpServletRequest request = (HttpServletRequest) super.getPageContext().getRequest();
		StringBuffer buffer = new StringBuffer();
		BigDecimal cantidadEndosos = EndosoDN.getInstancia("").obtenerCantidadEndososPoliza(polizaDTO.getIdToPoliza());
		if (cantidadEndosos.compareTo(BigDecimal.ONE) == 0) {
			buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: imprimirPoliza(" + id
				+ ")\"><img border='0px' alt='Imprimir P\u00f3liza' title='Imprimir P\u00f3liza' src='/MidasWeb/img/ico_impresion.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacionsololectura/cotizacion/mostrar.do?id=" + polizaDTO.getCotizacionDTO().getIdToCotizacion().toString()
					+ "', 'contenido','creaArbolPolizaSoloLectura("+id+");dhx_init_tabbars();');\"><img border='0px' alt='Ver Cotizacion' title='Ver Cotizacion' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a>");
		} else {
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: mostrarImprimirEndosoPoliza(" + id
					+ ")\"><img border='0px' alt='Imprimir P\u00f3liza' title='Imprimir P\u00f3liza' src='/MidasWeb/img/ico_impresion.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: mostrarConsultaEndosoPoliza(" + id + ");\">"
					+ "<img border='0px' alt='Ver Cotizacion' title='Ver Cotizacion' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a>");
		
			if (polizaDTO.getClaveEstatus().compareTo((short)0) != 0
				&& !AgenteEspecialDN.getInstancia().esAgenteEspecial(request)) { 
				//Si la p�liza es vigente y el usuario tiene permiso de rechazarCancelar (RC) y el usuario no tiene rol de Agente Especial
				
				buffer.append("&nbsp;");
				buffer.append("&nbsp;");
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarCancelarEndosoPoliza(" + id + ");\">"
						+ "<img border='0px' alt='Cancelar endosos' title='Cancelar endosos' src='/MidasWeb/img/icons/ico_rechazar2.gif'/></a>");
				buffer.append("&nbsp;");
				buffer.append("&nbsp;");
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarRehabilitarEndosoPoliza(" + id + ");\">"
						+ "<img border='0px' alt='Rehabilitar endosos' title='Rehabilitar endosos' src='/MidasWeb/img/icons/ico_terminar.gif'/></a>");
				
			}
		
		}
		buffer.append("&nbsp;");

		return buffer.toString();
	}
	
	public String getAccionesModal() throws ExcepcionDeAccesoADatos, SystemException{
		PolizaDTO polizaDTO = (PolizaDTO) getCurrentRowObject();
		SimpleDateFormat formatoFecha= new SimpleDateFormat("dd/MM/yyyy");
		String fechaInicioUltimoEndoso= DEFAULT_VALOR;
		String fechaFinUltimoEndoso= DEFAULT_VALOR;
		EndosoDN endosoDN= new EndosoDN();
		EndosoDTO ultimoEndoso= endosoDN.getUltimoEndoso(polizaDTO.getIdToPoliza());
		BigDecimal codigoAgente=BigDecimal.ZERO;
		CotizacionDTO cotizacionDTO=null;
		
		if(ultimoEndoso != null){
		    Calendar cal= Calendar.getInstance();
		    cal.setTime(ultimoEndoso.getFechaInicioVigencia());
		    fechaInicioUltimoEndoso= formatoFecha.format(cal.getTime());
		    cal.setTime(ultimoEndoso.getFechaFinVigencia());
		    fechaFinUltimoEndoso=formatoFecha.format(cal.getTime());
		    cotizacionDTO=CotizacionDN.getInstancia("").getPorId(ultimoEndoso.getIdToCotizacion());
		    if(cotizacionDTO!=null){
			codigoAgente=cotizacionDTO.getSolicitudDTO().getCodigoAgente();
		    }
		
		}
		

		String id = polizaDTO.getIdToPoliza().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: parent.seleccionaPolizaModal('");
		buffer.append(id);
		buffer.append("','");
		buffer.append(getNumeroPoliza());
		buffer.append("','");
		buffer.append(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getNombreComercial().toUpperCase());
		buffer.append("','");
		buffer.append(fechaInicioUltimoEndoso);
		buffer.append("','");
		buffer.append(fechaFinUltimoEndoso);
		buffer.append("','");
		buffer.append(polizaDTO.getClaveEstatus().toString());
		buffer.append("','");
		buffer.append(codigoAgente);
		buffer.append("','");
		buffer.append(polizaDTO.getCotizacionDTO().getSolicitudDTO().getProductoDTO().getIdToProducto().intValue());
		buffer.append( "')\"><img border='0px' alt='Seleccionar' title='Seleccionar' src='/MidasWeb/img/icons/ico_terminar.gif'/></a>");			
		buffer.append("&nbsp;");
		return buffer.toString();
	}
			
}
