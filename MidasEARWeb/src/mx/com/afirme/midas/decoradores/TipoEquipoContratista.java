package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipoequipocontratista.TipoEquipoContratistaDTO;

import org.displaytag.decorator.TableDecorator;

public class TipoEquipoContratista extends TableDecorator{

	public String getAcciones() {

		TipoEquipoContratistaDTO tipoEquipoContratistaDTO = (TipoEquipoContratistaDTO) getCurrentRowObject();
		String idTipoEqCont = tipoEquipoContratistaDTO.getIdTipoEquipoContratista().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoequipocontratista/mostrarDetalle.do?id="
				+ idTipoEqCont
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoequipocontratista/mostrarModificar.do?id="
				+ idTipoEqCont
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoequipocontratista/mostrarBorrar.do?id="
				+ idTipoEqCont
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	
}
