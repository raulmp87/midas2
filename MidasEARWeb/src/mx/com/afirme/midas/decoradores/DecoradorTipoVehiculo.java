package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorTipoVehiculo extends MidasBaseDecorator{

	public String getAcciones() {
		TipoVehiculoDTO tipoVehiculoDTO = (TipoVehiculoDTO) getCurrentRowObject();
		String idTcTipoVehiculo= ""+tipoVehiculoDTO.getId();
		String claveTipoBien= ""+tipoVehiculoDTO.getTipoBienAutosDTO().getClaveTipoBien();
		

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipovehiculo/mostrarDetalle.do?idTcTipoVehiculo="
				+ idTcTipoVehiculo+"&claveTipoBien="+claveTipoBien
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipovehiculo/mostrarModificar.do?idTcTipoVehiculo="
				+ idTcTipoVehiculo+"&claveTipoBien="+claveTipoBien
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipovehiculo/mostrarBorrar.do?idTcTipoVehiculo="
				+ idTcTipoVehiculo+"&claveTipoBien="+claveTipoBien
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
