package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorTipoServicioVehiculo extends MidasBaseDecorator{

	public String getAcciones() {
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = (TipoServicioVehiculoDTO) getCurrentRowObject();
		String id= ""+tipoServicioVehiculoDTO.getIdTcTipoServicioVehiculo();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tiposerviciovehiculo/mostrarDetalle.do?id="
				+ id
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tiposerviciovehiculo/mostrarModificar.do?id="
				+ id
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tiposerviciovehiculo/mostrarBorrar.do?id="
				+ id
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
