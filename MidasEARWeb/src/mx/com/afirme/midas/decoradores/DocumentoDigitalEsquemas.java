package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.danios.reportes.reportercs.esquemas.DocumentoDigitalEsquemasDN;
import mx.com.afirme.midas.danios.reportes.reportercs.esquemas.DocumentoDigitalEsquemasDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

import org.displaytag.decorator.TableDecorator;

public class DocumentoDigitalEsquemas extends TableDecorator {
	public String getArchivo() throws SystemException {
		DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO = (DocumentoDigitalEsquemasDTO) getCurrentRowObject();
		DocumentoDigitalEsquemasDN solicitudDN = DocumentoDigitalEsquemasDN.getInstancia();
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setIdToControlArchivo(documentoDigitalSolicitudDTO.getIdToControlArchivo());
		controlArchivoDTO = solicitudDN.obtenerControlArchivoDTO(controlArchivoDTO);
		return controlArchivoDTO.getNombreArchivoOriginal();
	}

	public String getAnexadoPor() {
		DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO = (DocumentoDigitalEsquemasDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getNombreUsuarioCreacion();
	}
	
	public String getDescripcion() {
		DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO = (DocumentoDigitalEsquemasDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getDescripcion();
	}
	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {

		DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO = (DocumentoDigitalEsquemasDTO) getCurrentRowObject();
		String id = documentoDigitalSolicitudDTO.getIdToDocumentoDigitalSolicitud().toString();
		String idControl = documentoDigitalSolicitudDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el archivo seleccionado?', null,'/MidasWeb/danios/reportes/reporteRCS/borrarDocumento.do?id="
						+ id
						+ "', 'resultados',null);\"><img border='0px' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' title='Descargar' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}
	
	
	public String getAccionesEndoso() {

		DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO = (DocumentoDigitalEsquemasDTO) getCurrentRowObject();
		String id = documentoDigitalSolicitudDTO.getIdToDocumentoDigitalSolicitud().toString();
		String idControl = documentoDigitalSolicitudDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el archivo seleccionado?', null,'/MidasWeb/endoso/solicitud/borrarDocumento.do?id="
						+ id
						+ "', 'resultados',null);\"><img border='0px' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' title='Descargar' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}
	
	/**
	 * toma la cadena para mostrar la accion de descargar Documento
	 * 
	 * @return String El texto HTML para la columna de descargar documento
	 */
	public String getDescargar() {

		DocumentoDigitalEsquemasDTO documentoDigitalSolicitudDTO = (DocumentoDigitalEsquemasDTO) getCurrentRowObject();
		String idControl = documentoDigitalSolicitudDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}

}
