package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.materialcombustible.MaterialCombustibleDTO;
import org.displaytag.decorator.TableDecorator;

public class MaterialesCombustile  extends TableDecorator{
	

	public String getCodigoMaterialCombustible() {
		MaterialCombustibleDTO materialCombustibleDTO = (MaterialCombustibleDTO) getCurrentRowObject();
		String idMaterialesCombustible = materialCombustibleDTO.getCodigoMaterialCombustible().toBigInteger().toString();
		return idMaterialesCombustible;
	}
    
	public String getAcciones() {

		MaterialCombustibleDTO materialCombustibleDTO = (MaterialCombustibleDTO) getCurrentRowObject();
		String idTabla = materialCombustibleDTO.getIdMaterialCombustible().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/materialcombustible/mostrarDetalle.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/materialcombustible/mostrarModificar.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/materialcombustible/mostrarBorrar.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
