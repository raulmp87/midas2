package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorMarcaVehiculo extends MidasBaseDecorator{

	public String getAcciones() {
		MarcaVehiculoDTO marcaVehiculoDTO = (MarcaVehiculoDTO) getCurrentRowObject();
		String idMarcaVehiculo= ""+marcaVehiculoDTO.getIdTcMarcaVehiculo();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/marcavehiculo/mostrarDetalle.do?id="
				+ idMarcaVehiculo
				+ "', 'contenido','obtenerGridSubMarcas();');\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/marcavehiculo/mostrarModificar.do?id="
				+ idMarcaVehiculo
				+ "', 'contenido','obtenerGridSubMarcas();');\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/marcavehiculo/mostrarBorrar.do?id="
				+ idMarcaVehiculo
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
