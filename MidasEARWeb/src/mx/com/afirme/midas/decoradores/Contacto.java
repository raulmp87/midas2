package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;

import org.displaytag.decorator.TableDecorator;

public class Contacto extends TableDecorator {
		
	public String getAcciones() {
		ContactoDTO contactoDTO = (ContactoDTO) getCurrentRowObject();
		String iDContacto = contactoDTO.getIdtccontacto().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/contacto/mostrarDetalle.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/contacto/mostrarModificar.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/contacto/mostrarBorrar.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}