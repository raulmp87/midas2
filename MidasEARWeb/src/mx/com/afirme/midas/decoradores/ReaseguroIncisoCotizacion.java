package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.reaseguro.inciso.ReaseguroIncisoCotizacionForm;

import org.displaytag.decorator.TableDecorator;

public class ReaseguroIncisoCotizacion extends TableDecorator{
	
	public String getAcciones(){
		ReaseguroIncisoCotizacionForm form = (ReaseguroIncisoCotizacionForm)getCurrentRowObject();
		@SuppressWarnings("unused")
		String idTcSubRamo = form.getIdTcSubRamo();
		@SuppressWarnings("unused")
		String idToCotizacion = form.getIdToCotizacion();
		@SuppressWarnings("unused")
		String numeroInciso = form.getNumeroInciso();

		return "<center><a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: \""
				+ "><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a></center>";
	}
}
