package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte.ReporteRCSContraparteDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class ReporteContraParte extends TableDecorator {
	
		
	public String getFechaCorte() {
		ReporteRCSContraparteDTO documentoDigitalRR6DTO = (ReporteRCSContraparteDTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getFechaCorte());
	}
	
	public String getIniEjecucion() {
		ReporteRCSContraparteDTO documentoDigitalRR6DTO = (ReporteRCSContraparteDTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getIniEjecucion());
	}
	
	public String getFinEjecucion() {
		ReporteRCSContraparteDTO documentoDigitalRR6DTO = (ReporteRCSContraparteDTO) getCurrentRowObject();
		String finEjecucion = "";
		if(documentoDigitalRR6DTO.getFinEjecucion()!=null){
		finEjecucion =  UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getFinEjecucion());
		}
			
		return finEjecucion;
	}
	
	public String getReporte() {
		ReporteRCSContraparteDTO documentoDigitalRR6DTO = (ReporteRCSContraparteDTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getReporte();
	}
		
	public String getEstatus() {
		ReporteRCSContraparteDTO documentoDigitalRR6DTO = (ReporteRCSContraparteDTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getEstatus();
	}

}
