package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorModeloVehiculo extends MidasBaseDecorator{

	public String getAcciones() {
		ModeloVehiculoDTO modeloVehiculoDTO = (ModeloVehiculoDTO) getCurrentRowObject();
//		String idTipoVehiculo= ""+estiloVehiculoDTODTO.getIdTcTipoVehiculo();
		String claveEstilo = modeloVehiculoDTO.getId().getClaveEstilo();
		String claveTipoBien = modeloVehiculoDTO.getId().getClaveTipoBien();
		String idVersionCarga = modeloVehiculoDTO.getId().getIdVersionCarga().toBigInteger().toString();
		String modeloVehiculo = modeloVehiculoDTO.getId().getModeloVehiculo().toString();
		String idMoneda = modeloVehiculoDTO.getId().getIdMoneda().toBigInteger().toString();
		String valorCaratula = modeloVehiculoDTO.getValorCaratula()!=null?modeloVehiculoDTO.getValorCaratula().toString():"";
		
		String parametrosId = "?claveEstilo="+claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+
			idVersionCarga+"&modeloVehiculo="+modeloVehiculo+"&idMoneda="+idMoneda+"&valorCaratula"+valorCaratula;

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/mostrarDetalle.do"+parametrosId
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/mostrarModificar.do"+parametrosId
				+ "', 'contenido','manipulaCalendarioLineas()');\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/modelovehiculo/mostrarBorrar.do"+parametrosId
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
