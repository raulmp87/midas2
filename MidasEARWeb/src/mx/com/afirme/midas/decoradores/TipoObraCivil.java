package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipoobracivil.TipoObraCivilDTO;

import org.displaytag.decorator.TableDecorator;

public class TipoObraCivil extends TableDecorator{
	
	public String getClaveAutorizacion(){
		TipoObraCivilDTO tipoObraCivilDTO = (TipoObraCivilDTO) getCurrentRowObject();
		String clave = tipoObraCivilDTO.getClaveAutorizacion().toString();
		if (clave.equals("0"))
			clave = "NO REQUERIDA";
		if (clave.equals("1"))
			clave = "AREA TECNICA";
		if (clave.equals("2"))
			clave = "AREA DE REASEGURO";
		return clave;
	}
	
	public String getCodigoTipoObraCivil(){
		TipoObraCivilDTO tipoObraCivilDTO = (TipoObraCivilDTO) getCurrentRowObject();
		return  tipoObraCivilDTO.getCodigoTipoObraCivil().toBigInteger().toString();
	}

	public String getAcciones() {

		TipoObraCivilDTO tipoObraCivilDTO = (TipoObraCivilDTO) getCurrentRowObject();
		String idTipoObra = tipoObraCivilDTO.getIdTipoObraCivil().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoobracivil/mostrarDetalle.do?id="
				+ idTipoObra
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoobracivil/mostrarModificar.do?id="
				+ idTipoObra
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoobracivil/mostrarBorrar.do?id="
				+ idTipoObra
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	
}
