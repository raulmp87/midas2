package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.contratos.participacion.ParticipacionDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDN;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;
import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDN;
import mx.com.afirme.midas.catalogos.contacto.ContactoDTO;
import mx.com.afirme.midas.catalogos.contacto.ContactoDN;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDN;
import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import java.text.MessageFormat; 
import java.util.List;

import org.displaytag.decorator.TableDecorator;

public class Participacion extends TableDecorator {

	public String getAcciones() {
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		String id = participacionDTO.getIdTdParticipacion().toString();
		String acciones = 	"<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/contratos/participacion/mostrarModificar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
							+ "<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/contratos/participacion/mostrarBorrar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
		
		if(participacionDTO.getTipo().intValue()==Sistema.PARTICIPANTE_CORREDOR){
			ParticipacionCorredorDN participacionCorredorDN = ParticipacionCorredorDN.getInstancia();
			ParticipacionCorredorDTO participacionCorredorDTO = new ParticipacionCorredorDTO();
			participacionCorredorDTO.setParticipacion(participacionDTO);
			participacionCorredorDTO.setReaseguradorCorredor(new ReaseguradorCorredorDTO());
			
			acciones+="<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/contratos/participacioncorredor/listarReaseguradores.do?id="
				+ id + "', 'contenido',null);\">";
			
			try{
				List<ParticipacionCorredorDTO> participacionesCorredor = 
				participacionCorredorDN.listarFiltrados(participacionCorredorDTO);															
			
			if(participacionesCorredor.size()>0)
				acciones+="<img border='0px' alt='Agregar reasegurador' src='/MidasWeb/img/group_add_green.png'/>";
			else
				acciones+="<img border='0px' alt='Agregar reasegurador' src='/MidasWeb/img/group_add_red.png'/>";
			
			acciones+="</a>";
			
			}catch(ExcepcionDeAccesoADatos edaad){
				acciones+="<img border='0px' alt='Agregar reasegurador' src='/MidasWeb/img/add16.png'/>";				
				acciones+="</a>";				
			}
			 catch(SystemException se){
				 acciones+="<img border='0px' alt='Agregar reasegurador' src='/MidasWeb/img/add16.png'/>";				
				 acciones+="</a>";				
			 }
		}
		
		return acciones;
	}
				
	public String getNombreParticipante(){
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = participacionDTO.getReaseguradorCorredor();
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
		try{
			reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			return reaseguradorCorredorDTO.getNombre();								
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}catch(SystemException se){
			return ("* Error *");
		}
	}
	
	public String getCnfs(){
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		ReaseguradorCorredorDTO reaseguradorCorredorDTO = participacionDTO.getReaseguradorCorredor();
		ReaseguradorCorredorDN reaseguradorCorredorDN = ReaseguradorCorredorDN.getInstancia();
		try{
			reaseguradorCorredorDTO = reaseguradorCorredorDN.obtenerReaseguradorPorId(reaseguradorCorredorDTO);
			return reaseguradorCorredorDTO.getCnfs();								
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}catch(SystemException se){
			return ("* Error *");
		}
	}
	
	public String getCuentaPesos(){
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		CuentaBancoDTO cuentaBancoDTO = participacionDTO.getCuentaBancoPesos();
		CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();				
		try{
			cuentaBancoDTO = cuentaBancoDN.getPorId(cuentaBancoDTO);
			String cuenta = cuentaBancoDTO.getNumerocuenta().toString();
			String banco = cuentaBancoDTO.getNombrebanco();
			return cuenta.concat("/").concat(banco);					
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}catch(SystemException se){
			return ("* Error *");
		}
	}
	
	public String getCuentaDolares(){
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		CuentaBancoDTO cuentaBancoDTO = participacionDTO.getCuentaBancoDolares();
		CuentaBancoDN cuentaBancoDN = CuentaBancoDN.getInstancia();				
		try{
			cuentaBancoDTO = cuentaBancoDN.getPorId(cuentaBancoDTO);
			String cuenta = cuentaBancoDTO.getNumerocuenta().toString();
			String banco = cuentaBancoDTO.getNombrebanco();
			return cuenta.concat("/").concat(banco);					
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}catch(SystemException se){
			return ("* Error *");
		}
	}
	
	public String getContacto(){
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		ContactoDTO contactoDTO = participacionDTO.getContacto();
		ContactoDN contactoDN = ContactoDN.getInstancia();				
		try{
			contactoDTO = contactoDN.getPorId(contactoDTO);
			contactoDTO.getNombre();			
			return contactoDTO.getNombre();					
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}catch(SystemException se){
			return ("* Error *");
		}
	}
	
	public String getPorcentajeParticipacionFormato(){
		ParticipacionDTO participacionDTO = (ParticipacionDTO) getCurrentRowObject();
		Double porcentaje = participacionDTO.getPorcentajeParticipacion();				
		return MessageFormat.format("{0,number,#.##%}", porcentaje/100);		
	}
}
