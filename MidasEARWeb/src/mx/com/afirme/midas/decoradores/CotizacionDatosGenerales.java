package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import org.displaytag.decorator.TableDecorator;

public class CotizacionDatosGenerales extends TableDecorator{

	public String getAcciones() {
		
		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = (DocumentoDigitalSolicitudDTO) getCurrentRowObject();
		String idControlArchivo = "";
		String acciones = "";
		if (documentoDigitalSolicitudDTO != null && documentoDigitalSolicitudDTO.getControlArchivo() != null){
			idControlArchivo = documentoDigitalSolicitudDTO.getControlArchivo().getIdToControlArchivo().toString(); 
			documentoDigitalSolicitudDTO.getControlArchivo().getIdToControlArchivo().toString();
			acciones = "<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
					+ idControlArchivo
					+ "', 'download');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>";
			
		}
		return acciones;
	}
}