package mx.com.afirme.midas.decoradores;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.agente.AgenteEspecialDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import org.apache.commons.lang.StringUtils;

public class OrdenTrabajo extends MidasBaseDecorator{
	public String getOrdenTrabajo(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		return obtenerCodigo(cotizacion);
	}
	
	public String obtenerCodigo(CotizacionDTO cotizacion) {
		String codigo=""+cotizacion.getIdToCotizacion();
		return "ODT-"+ StringUtils.leftPad(codigo, 8, '0');	
	}
	
	public String getCliente(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String nombre = cotizacion.getSolicitudDTO().getNombrePersona();
		String aPaterno = "";
		String aMaterno = "";
		if (cotizacion.getSolicitudDTO().getApellidoPaterno()!= null && !cotizacion.getSolicitudDTO().getApellidoPaterno().equals(""))
			aPaterno = cotizacion.getSolicitudDTO().getApellidoPaterno();
		
		if (cotizacion.getSolicitudDTO().getApellidoMaterno()!= null && !cotizacion.getSolicitudDTO().getApellidoMaterno().equals(""))
			aMaterno = cotizacion.getSolicitudDTO().getApellidoMaterno();		

		String cliente = nombre+ " " + aPaterno + " " + aMaterno; 
		ClienteDTO asegurado = null;
		if (cotizacion.getIdToPersonaAsegurado() != null){
			try{
				if(cotizacion.getIdDomicilioAsegurado() != null && cotizacion.getIdDomicilioAsegurado().intValue() > 0){
					asegurado = new ClienteDTO();
					asegurado.setIdCliente(cotizacion.getIdToPersonaAsegurado());
					asegurado.setIdDomicilio(cotizacion.getIdDomicilioAsegurado());
					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, cotizacion.getCodigoUsuarioCotizacion());
				}else{
					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacion.getIdToPersonaAsegurado(), cotizacion.getCodigoUsuarioCotizacion());
				}	
			}catch(Exception exc){}
			if (asegurado != null){
				if (asegurado.getClaveTipoPersona() == 1)
					cliente = asegurado.getNombre() + " " + asegurado.getApellidoPaterno() + " " + asegurado.getApellidoMaterno();
				else
					cliente = asegurado.getNombre();
			}
		}
		return cliente;
	}
	
	public String getProducto(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String producto = "NOT FOUND";
		try {
			ProductoDTO productoDTO = new ProductoSN().encontrarPorSolicitud(cotizacion.getSolicitudDTO().getIdToSolicitud());
			producto = productoDTO.getNombreComercial();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NullPointerException e){e.printStackTrace();}
		return producto;
	}

	public String getTipoPoliza(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String poliza = "NO ASIGNADA";
		try {
			poliza = new TipoPolizaSN().encontrarPorCotizacion(cotizacion.getIdToCotizacion()).getNombreComercial();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch(NullPointerException e){
			//e.printStackTrace();
		}
		return poliza;
	}
	
	public String getEstatus(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String estatus = "NOT FOUND";
		//TODO Verificar de qu� tabla proviene el estatus, o si se van a manejar datos duros
		estatus = cotizacion.getClaveEstatus().toString();
		estatus = UtileriasWeb.getDescripcionCatalogoValorFijo(30, cotizacion.getClaveEstatus());
		if(estatus == null)
			estatus="No disponible";
		return estatus;
	}
	
	public String getTipoOrden(){
	    	CotizacionDTO cotizacionDTO = (CotizacionDTO) getCurrentRowObject();
		String descripcion = UtileriasWeb.getDescripcionCatalogoValorFijo(47, cotizacionDTO.getSolicitudDTO().getClaveTipoSolicitud());
		
		return descripcion==null ?"":descripcion;
	}
	
	public String getAcciones(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String id = cotizacion.getIdToCotizacion().toString();
		StringBuffer buffer = new StringBuffer();
		HttpServletRequest request = (HttpServletRequest) super.getPageContext().getRequest();
		
		if ((cotizacion.getClaveEstatus().shortValue() == Sistema.ESTATUS_ODT_ENPROCESO || cotizacion.getClaveEstatus().shortValue() ==  Sistema.ESTATUS_ODT_ASIGNADA)){
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/mostrarODT.do?id=" + id
					+ "', 'contenido','dhx_init_tabbars();creaArbolOrdenesDeTrabajo(null);');\"><img border='0px' alt='Editar' title='Editar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");
			buffer.append("&nbsp;");
		}else{
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "<img border='0px' src='/MidasWeb/img/blank.gif'/></a>");
			buffer.append("&nbsp;");
		}
		//autorizar o rechazar las RDN no cumplidas
		if (!AgenteEspecialDN.getInstancia().esAgenteEspecial(request)){
			if ((cotizacion.getClaveAutRetroacDifer() != null && cotizacion.getClaveAutRetroacDifer().shortValue() == Sistema.AUTORIZACION_REQUERIDA) || (cotizacion.getClaveAutVigenciaMaxMin() != null && cotizacion.getClaveAutVigenciaMaxMin().shortValue() == Sistema.AUTORIZACION_REQUERIDA)){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/autorizacion/mostrarODT.do?id=" + id
						+ "', 'contenido','dhx_init_tabbars();');\"><img border='0px' alt='Autorizar' title='Autorizar' src='/MidasWeb/img/confirmAll.gif'/></a>");
				buffer.append("&nbsp;");
			}
		}else{
			buffer.append("<a href=\"javascript: void(0);\" " + "<img border='0px' src='/MidasWeb/img/blank.gif'/></a>");
			buffer.append("&nbsp;");
		}
		

			buffer.append("<a href=\"javascript: void(0);\" "
					+ "<img border='0px' src='/MidasWeb/img/blank.gif'/></a>");
			buffer.append("&nbsp;");

		
		//Si est� en proceso o asignada, se puede rechazar/cancelar
			if (cotizacion.getClaveEstatus().intValue() == 0 || cotizacion.getClaveEstatus().intValue() == 1){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarCancelarOrdenTrabajo(" + id
						+ ");\"><img border='0px' title='Cancelar o Rechazar' alt='Cancelar o Rechazar' src='/MidasWeb/img/icons/ico_rechazar2.gif'/></a>");
				buffer.append("&nbsp;");
			}
		
			//Si est� en proceso, se puede terminar
			if (cotizacion.getClaveEstatus().shortValue() == Sistema.ESTATUS_ODT_ENPROCESO){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/terminarOrdenTrabajo.do?id=" + id
						+ "', 'contenido','procesarRespuesta(null)');\"><img border='0px' alt='Terminar' title='Terminar' src='/MidasWeb/img/icons/ico_terminar.gif'/></a>");
				buffer.append("&nbsp;");
			}

		if (!AgenteEspecialDN.getInstancia().esAgenteEspecial(request)){
			//Si est� terminada , se puede asignar
			if (cotizacion.getClaveEstatus().intValue() == 2){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarAsignarOrdenTrabajo(" + id
						+ ");\"><img border='0px' alt='Asignar' title='Asignar' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");
				buffer.append("&nbsp;");
			}
		}else{
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "<img border='0px' src='/MidasWeb/img/blank.gif'/></a>");
			buffer.append("&nbsp;");
		}

		return buffer.toString();
	}
}
