package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipomontajemaquina.TipoMontajeMaquinaDTO;

import org.displaytag.decorator.TableDecorator;

public class TipoMontajeMaquina extends TableDecorator {
	
	
	public String getCodigotipomontajemaq(){
		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = (TipoMontajeMaquinaDTO) getCurrentRowObject();
		String idCodigo = tipoMontajeMaquinaDTO.getCodigotipomontajemaq().toBigInteger().toString();
		return idCodigo;
	}
	
	public String getAcciones() {

		TipoMontajeMaquinaDTO tipoMontajeMaquinaDTO = (TipoMontajeMaquinaDTO) getCurrentRowObject();
		String idTipoMontajeMaquina = tipoMontajeMaquinaDTO.getIdtctipomontajemaq().toBigInteger().toString();
		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipomontajemaquina/mostrarDetalle.do?id="
				+ idTipoMontajeMaquina
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipomontajemaquina/mostrarModificar.do?id="
				+ idTipoMontajeMaquina
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipomontajemaquina/mostrarBorrar.do?id="
				+ idTipoMontajeMaquina
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
