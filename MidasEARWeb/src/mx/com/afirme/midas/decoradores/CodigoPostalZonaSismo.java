package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.codigopostalzonasismo.CodigoPostalZonaSismoDTO;

import org.displaytag.decorator.TableDecorator;

public class CodigoPostalZonaSismo extends TableDecorator{
    
    
    	public String getNombreColonia(){
    	CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO = (CodigoPostalZonaSismoDTO) getCurrentRowObject();
    	CodigoPostalDN codigoPostalDN= CodigoPostalDN.getInstancia();
    	ColoniaDTO coloniaDTO=    codigoPostalDN.getColoniaPorId(codigoPostalZonaSismoDTO.getId().getNombreColonia());
    	return coloniaDTO==null?"":coloniaDTO.getDescription();
    	}


	public String getAcciones() {
		CodigoPostalZonaSismoDTO codigoPostalZonaSismoDTO = (CodigoPostalZonaSismoDTO) getCurrentRowObject();
		String idZonaSismo 	= codigoPostalZonaSismoDTO.getNombreColonia();
		String codigoPostal = codigoPostalZonaSismoDTO.getCodigoPostal().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/codigopostalzonasismo/mostrarDetalle.do?id="
				+ idZonaSismo + "&cp=" + codigoPostal
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>";
	}
}
