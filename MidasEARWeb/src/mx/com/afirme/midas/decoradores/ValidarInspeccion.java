package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class ValidarInspeccion extends TableDecorator{

	public String getAcciones() {
		
		IncisoCotizacionDTO incisoCotizacion = (IncisoCotizacionDTO) getCurrentRowObject();
		String idToCotizacion = incisoCotizacion.getId().getIdToCotizacion().toBigInteger().toString();
		String numeroInciso = incisoCotizacion.getId().getNumeroInciso().toBigInteger().toString();
		String claveMensajeInspeccion = "";
		switch (incisoCotizacion.getClaveMensajeInspeccion()){
			case 2:
				claveMensajeInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.mensaje.consultarConSupervisor");
				break;
			case 3:
				claveMensajeInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.mensaje.validarContenidosDeMadera50Porciento");
				break;
			case 9:
				claveMensajeInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.mensaje.incisoExcluido");
				break;
		}
		
		String mensajeConfirmarSolicitudOmisionInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.mensaje.mensajeConfirmarSolicitudOmisionInspeccion");
		String mensajeConfirmarRequerirInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.validar.mensaje.mensajeConfirmarRequerirInspeccion");
		
		String solicitarInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.solicitarInspeccion");
		String capturarReporteInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.capturarReporteInspeccion");
		String solicitarAutorizacionNoInspeccion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.solicitarAutorizacionNoInspeccion");
		String estatusAutorizacion = "";
		String acciones = "";
		
		// Caso  ClaveInspeccion = Requerida, ClaveTipoOrigenInspeccion = Catálogo, ClaveAutInspeccion = No Requerida
		if (incisoCotizacion.getClaveEstatusInspeccion() == 1 && incisoCotizacion.getClaveTipoOrigenInspeccion() == 1 && incisoCotizacion.getClaveAutInspeccion() == 0) {
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px' title='"+ solicitarInspeccion +"' alt='"+ solicitarInspeccion +"' src='/MidasWeb/img/confirmAll_disabled.gif'/></a>"
				+ "&nbsp;<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequestFromAccordion(null, '/MidasWeb/cotizacion/validar/inspeccion/mostrarRegistrarInspeccion.do?idToCotizacion="+ idToCotizacion +"&numeroInciso="+ numeroInciso +"','contenido',null);\"><img border='0px'  title='"+ capturarReporteInspeccion +"' alt='"+ capturarReporteInspeccion +"' src='/MidasWeb/img/b_mas_agregar.gif'/></a>"
				+ "&nbsp;<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: if(confirm('"+mensajeConfirmarSolicitudOmisionInspeccion+"')) sendRequestFromAccordion(null, '/MidasWeb/cotizacion/validar/inspeccion/cambiarEstatusInspeccion.do?idToCotizacion="+ idToCotizacion +"&numeroInciso="+ numeroInciso +"&claveEstatusInspeccion=0&claveTipoOrigenInspeccion=2&claveAutInspeccion=1&claveMensajeInspeccion=0',null,'recargarAcordeonValidarCotizacion(1,"+ idToCotizacion +")');\"><img border='0px'  title='"+ solicitarAutorizacionNoInspeccion +"' alt='"+ solicitarAutorizacionNoInspeccion +"' src='/MidasWeb/img/listsicon.gif'/></a>";
			acciones += "&nbsp;";
		}
		// Caso  ClaveInspeccion = Requerida, ClaveTipoOrigenInspeccion = Voluntaria, ClaveAutInspeccion = No Requerida
		if (incisoCotizacion.getClaveEstatusInspeccion() == 1 && incisoCotizacion.getClaveTipoOrigenInspeccion() == 2 && incisoCotizacion.getClaveAutInspeccion() == 0) {
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px' title='"+ solicitarInspeccion +"' alt='"+ solicitarInspeccion +"' src='/MidasWeb/img/confirmAll_disabled.gif'/></a>"
				+ "&nbsp;<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequestFromAccordion(null, '/MidasWeb/cotizacion/validar/inspeccion/mostrarRegistrarInspeccion.do?idToCotizacion="+ idToCotizacion +"&numeroInciso="+ numeroInciso +"','contenido','recargarTablaDocumentosAnexosInspeccion()');\"><img border='0px'  title='"+ capturarReporteInspeccion +"' alt='"+ capturarReporteInspeccion +"' src='/MidasWeb/img/b_mas_agregar.gif'/></a>"
				+ "&nbsp;<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ solicitarAutorizacionNoInspeccion +"' alt='"+ solicitarAutorizacionNoInspeccion +"' src='/MidasWeb/img/listsicon_disabled.gif'/></a>";
			acciones += "&nbsp;";
		}
		// Caso  ClaveInspeccion = No Requerida, ClaveTipoOrigenInspeccion = Catálogo, ClaveAutInspeccion = No Requerida
		if (incisoCotizacion.getClaveEstatusInspeccion() == 0 && incisoCotizacion.getClaveTipoOrigenInspeccion() == 1 && incisoCotizacion.getClaveAutInspeccion() == 0){
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: if(confirm('"+mensajeConfirmarRequerirInspeccion+"')) sendRequestFromAccordion(null, '/MidasWeb/cotizacion/validar/inspeccion/cambiarEstatusInspeccion.do?idToCotizacion="+ idToCotizacion +"&numeroInciso="+ numeroInciso +"&claveEstatusInspeccion=1&claveTipoOrigenInspeccion=2&claveAutInspeccion=0&claveMensajeInspeccion=0',null,'recargarAcordeonValidarCotizacion(1,"+ idToCotizacion +")');\"><img border='0px' title='"+ solicitarInspeccion +"' alt='"+ solicitarInspeccion +"' src='/MidasWeb/img/confirmAll.gif'/></a>"
				+ "&nbsp;<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ capturarReporteInspeccion +"' alt='"+ capturarReporteInspeccion +"' src='/MidasWeb/img/b_mas_agregar_disabled.gif'/></a>"
				+ "&nbsp;<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ solicitarAutorizacionNoInspeccion +"' alt='"+ solicitarAutorizacionNoInspeccion +"' src='/MidasWeb/img/listsicon_disabled.gif'/></a>";
			acciones += "&nbsp;";
		}
	
		// Caso  ClaveInspeccion = No Requerida, ClaveTipoOrigenInspeccion = Voluntaria, ClaveAutInspeccion = Pendiente Autorización
		if (incisoCotizacion.getClaveEstatusInspeccion() == 1 &&  incisoCotizacion.getClaveAutInspeccion() == 1 && (incisoCotizacion.getClaveTipoOrigenInspeccion() == 1 || incisoCotizacion.getClaveTipoOrigenInspeccion() == 2)){
			estatusAutorizacion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.estatusSolicitudAutorizacion.pendiente");
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ estatusAutorizacion +"' alt='"+ estatusAutorizacion +"' src='/MidasWeb/img/b_pendiente.gif'/></a>";
			acciones += "&nbsp;";
		}		
		// Caso  ClaveInspeccion = No Requerida, ClaveTipoOrigenInspeccion = Voluntaria, ClaveAutInspeccion = Rechazada
		if (incisoCotizacion.getClaveEstatusInspeccion() == 1 && incisoCotizacion.getClaveAutInspeccion() == 8 && (incisoCotizacion.getClaveTipoOrigenInspeccion() == 1 || incisoCotizacion.getClaveTipoOrigenInspeccion() == 2)){
			estatusAutorizacion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.estatusSolicitudAutorizacion.rechazada");
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ estatusAutorizacion +"' alt='"+ estatusAutorizacion +"' src='/MidasWeb/img/b_rechazar.gif'/></a>";
			acciones += "&nbsp;";
		}		
		// Caso  ClaveInspeccion = No Requerida, ClaveTipoOrigenInspeccion = Voluntaria, ClaveAutInspeccion = Autorizada
		if (incisoCotizacion.getClaveEstatusInspeccion() == 1 && incisoCotizacion.getClaveAutInspeccion() == 7 && (incisoCotizacion.getClaveTipoOrigenInspeccion() == 1 || incisoCotizacion.getClaveTipoOrigenInspeccion() == 2)){
			estatusAutorizacion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "midas.cotizacion.inspeccion.estatusSolicitudAutorizacion.autorizada");
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ estatusAutorizacion +"' alt='"+ estatusAutorizacion +"' src='/MidasWeb/img/b_autorizar.gif'/></a>";
			acciones += "&nbsp;";
		}
		if (claveMensajeInspeccion.length() > 0) {
			acciones = "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: void(0);\"><img border='0px'  title='"+ claveMensajeInspeccion +"' alt='"+ claveMensajeInspeccion +"' src='/MidasWeb/img/information.gif'/></a>" + acciones;
			acciones += "&nbsp;";
		}
		
		return acciones;
	}
}