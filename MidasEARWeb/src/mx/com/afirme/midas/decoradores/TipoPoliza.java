package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;

import org.displaytag.decorator.TableDecorator;

public class TipoPoliza extends TableDecorator {
	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	
	public String getAcciones() {

		TipoPolizaDTO tipoPolizaDTO = (TipoPolizaDTO) getCurrentRowObject();
		String id = tipoPolizaDTO.getIdToTipoPoliza().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		if(tipoPolizaDTO.getClaveEstatus() == 0 && tipoPolizaDTO.getClaveActivo() == 0) {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id="
							+ id
							+ "', 'configuracion_detalle','dhx_init_tabbars()');\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id="
							+ id
							+ "', 'configuracion_detalle',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		} else {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarBorrar.do?id="
							+ id
							+ "', 'configuracion_detalle',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarModificar.do?id="
							+ id
							+ "', 'configuracion_detalle',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id="
							+ id
							+ "', 'configuracion_detalle','dhx_init_tabbars()');\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do?id="
							+ id
							+ "', 'configuracion_detalle',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		}
		buffer.append("</center>");		
		return buffer.toString();
	}

}
