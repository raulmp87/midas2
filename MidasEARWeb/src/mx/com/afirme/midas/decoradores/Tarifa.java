package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.tarifa.TarifaDTO;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDN;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaDTO;
import mx.com.afirme.midas.tarifa.configuracion.ConfiguracionTarifaId;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonDTO;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonFacadeRemote;

import org.displaytag.decorator.TableDecorator;

public class Tarifa extends TableDecorator {

	public String getValor1(){
		TarifaDTO tarifa = (TarifaDTO) getCurrentRowObject();
		return this.getValor(tarifa.getId().getIdToRiesgo(), tarifa.getId().getIdConcepto(), (short)1, tarifa.getId().getIdBase1());
	}
	
	public String getValor2(){
		TarifaDTO tarifa = (TarifaDTO) getCurrentRowObject();
		return this.getValor(tarifa.getId().getIdToRiesgo(), tarifa.getId().getIdConcepto(), (short)2, tarifa.getId().getIdBase2());
	}
	
	public String getValor3(){
		TarifaDTO tarifa = (TarifaDTO) getCurrentRowObject();
		return this.getValor(tarifa.getId().getIdToRiesgo(), tarifa.getId().getIdConcepto(), (short)3, tarifa.getId().getIdBase3());
	}
	
	public String getValor4(){
		TarifaDTO tarifa = (TarifaDTO) getCurrentRowObject();
		return this.getValor(tarifa.getId().getIdToRiesgo(), tarifa.getId().getIdConcepto(), (short)4, tarifa.getId().getIdBase4());
	}
	
	public String getValor(){
		TarifaDTO tarifa = (TarifaDTO) getCurrentRowObject();
		ConfiguracionTarifaDN confTarifaDN = ConfiguracionTarifaDN.getInstancia();
		ConfiguracionTarifaDTO confTarifa;
		ConfiguracionTarifaId confTarifaId = new ConfiguracionTarifaId();
		
		confTarifaId.setIdToRiesgo(tarifa.getId().getIdToRiesgo());
		confTarifaId.setIdConcepto(BigDecimal.valueOf(Double.valueOf(tarifa.getId().getIdConcepto())));
		confTarifaId.setIdDato((short)5);
		
		String valor = "" + tarifa.getValor();;
		
		try {
			confTarifa = confTarifaDN.getPorId(confTarifaId);
			short claveTipovalidacion = confTarifa.getClaveTipoValidacion();
				if (claveTipovalidacion == 1 || claveTipovalidacion == 4)
					valor = String.format("%.4f", tarifa.getValor());
				else if (claveTipovalidacion == 2)
					valor = String.format("%.2f", tarifa.getValor())+" %";
				else if (claveTipovalidacion == 3)
					valor = String.format("$ %.2f", tarifa.getValor());
				else if (claveTipovalidacion == 5)
					valor = String.format("%.0f",tarifa.getValor());
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		}
		return valor;
	}
	
	public String getAcciones(){
		TarifaDTO tarifaDTO = (TarifaDTO) getCurrentRowObject();
		String idToRiesgo = tarifaDTO.getId().getIdToRiesgo().toString();
		String idConcepto = tarifaDTO.getId().getIdConcepto();
		String idBase1 = tarifaDTO.getId().getIdBase1().toString();
		String idBase2 = tarifaDTO.getId().getIdBase2().toString();
		String idBase3 = tarifaDTO.getId().getIdBase3().toString();
		String idBase4 = tarifaDTO.getId().getIdBase4().toString();
		String version = tarifaDTO.getId().getVersion().toString();
		String valor = tarifaDTO.getValor().toString();
		
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarBorrarTarifa("+ idToRiesgo+","+idConcepto+","+idBase1+","+idBase2+","+idBase3+","+idBase4+","+version+","+valor
						+ ")\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarModificarTarifa("+ idToRiesgo+","+idConcepto+","+idBase1+","+idBase2+","+idBase3+","+idBase4+","+version+","+valor
						+ ")\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}
	
	private String getValor(BigDecimal idToRiesgo,String idConcepto,short idDato,BigDecimal idBaseValor){
		String valor="";
		ConfiguracionTarifaDN confTarifaDN = ConfiguracionTarifaDN.getInstancia();
		ConfiguracionTarifaDTO confTarifa;
		ConfiguracionTarifaId confTarifaId = new ConfiguracionTarifaId();
		
		confTarifaId.setIdToRiesgo(idToRiesgo);
		confTarifaId.setIdConcepto(BigDecimal.valueOf(Double.valueOf(idConcepto)));
		confTarifaId.setIdDato(idDato);
		try {
			confTarifa = confTarifaDN.getPorId(confTarifaId);
			if(confTarifa!=null){
        			if (confTarifa.getClaveTipoControl()==4)	//si es una caja de texto
        				valor = ""+idBaseValor;
        			else if (confTarifa.getClaveTipoControl()==0)	//si es un campo oculto
        				valor = "";
        			else{
        			    	if(confTarifa.getClaseRemota()!=null){
                				String claseRemota = confTarifa.getClaseRemota().trim();
                				@SuppressWarnings("rawtypes")
								MidasInterfaceBase beanBase = ServiceLocator.getInstance().getEJB(claseRemota);
                				if (beanBase instanceof CatalogoValorFijoFacadeRemote){
                					CatalogoValorFijoId id = new CatalogoValorFijoId((int)confTarifa.getIdGrupo(),idBaseValor.toBigInteger().intValue());
                					valor = ((CacheableDTO) beanBase.findById(id)).getDescription();
                				}
                				else if (beanBase instanceof EscalonFacadeRemote){
                					EscalonDTO escalon = (EscalonDTO)beanBase.findById(Double.valueOf(idBaseValor.toString()));
                					short claveTipovalidacion = confTarifa.getClaveTipoValidacion();
                					if (claveTipovalidacion == 1 || claveTipovalidacion == 4)
                						valor = String.format("%.4f - %.4f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
                					else if (claveTipovalidacion == 2)
                						valor = String.format("%.4f", escalon.getValorRangoInferior())+" % - "+String.format("%.4f",escalon.getValorRangoSuperior())+" % ";
                					else if (claveTipovalidacion == 3)
                						valor = String.format("$ %.2f - $ %.2f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
                					else if (claveTipovalidacion == 5)
                						valor = String.format("%.0f - %.0f", escalon.getValorRangoInferior(),escalon.getValorRangoSuperior());
                					else
                						valor = "" + escalon.getValorRangoInferior()+" - "+ escalon.getValorRangoSuperior();
                				}
                				else
                					valor = ((CacheableDTO) beanBase.findById(idBaseValor)).getDescription();
        			    	}
        			}
			}
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NullPointerException e){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return valor;
	}
}
