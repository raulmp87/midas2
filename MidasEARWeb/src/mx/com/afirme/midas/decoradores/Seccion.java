package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

import org.displaytag.decorator.TableDecorator;

public class Seccion extends TableDecorator {
	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {

		SeccionDTO seccionDTO = (SeccionDTO) getCurrentRowObject();
		String id = seccionDTO.getIdToSeccion().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarBorrar.do?id="
						+ id
						+ "', 'configuracion_detalle',null);\"><img border='0px' alt='Borrar'title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarModificar.do?id="
						+ id
						+ "', 'configuracion_detalle',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarDetalle.do?id="
						+ id
						+ "', 'configuracion_detalle','dhx_init_tabbars()');\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/configuracion/seccion/mostrarDetalle.do?id="
						+ id
						+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}

}
