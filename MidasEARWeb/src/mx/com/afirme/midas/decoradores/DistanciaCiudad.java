package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.distanciaciudad.DistanciaCiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.displaytag.decorator.TableDecorator;

public class DistanciaCiudad extends TableDecorator {

	public String getAcciones() {
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();
		String idDistanciaCiudad = distanciaCiudadDTO.getIdDistanciaCiudad().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/mostrarDetalle.do?id="
				+ idDistanciaCiudad
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/mostrarModificar.do?id="
				+ idDistanciaCiudad
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/mostrarBorrar.do?id="
				+ idDistanciaCiudad
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	
	public String getEstadoOrigenDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idEstadoOrigenDescripcion = distanciaCiudadDTO.getIdEstadoOrigen();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 EstadoDTO estadoDTO = codigoPostalDN.getEstadoPorId(idEstadoOrigenDescripcion);
			 return estadoDTO.getStateName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	
	public String getEstadoDestinoDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idEstadoDestinoDescripcion = distanciaCiudadDTO.getIdEstadoDestino();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 EstadoDTO estadoDTO = codigoPostalDN.getEstadoPorId(idEstadoDestinoDescripcion);
			 return estadoDTO.getStateName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	
	public String getCiudadOrigenDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idCiudadOrigenDescripcion = distanciaCiudadDTO.getIdCiudadOrigen();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 CiudadDTO ciudadDTO = codigoPostalDN.getCiudadPorId(idCiudadOrigenDescripcion);
			 return ciudadDTO.getCityName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	
	public String getCiudadDestinoDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idCiudadDestinoDescripcion = distanciaCiudadDTO.getIdCiudadDestino();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 CiudadDTO ciudadDTO = codigoPostalDN.getCiudadPorId(idCiudadDestinoDescripcion);
			 return ciudadDTO.getCityName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
}
