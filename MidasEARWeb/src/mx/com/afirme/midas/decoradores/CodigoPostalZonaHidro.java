package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.codigopostalzonahidro.CodigoPostalZonaHidroDTO;
import org.displaytag.decorator.TableDecorator;

public class CodigoPostalZonaHidro extends TableDecorator {

    
    	public String getNombreColonia(){
    	
    	   CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO = (CodigoPostalZonaHidroDTO) getCurrentRowObject();
    	 CodigoPostalDN codigoPostalDN=CodigoPostalDN.getInstancia();
    	 
    	  ColoniaDTO coloniaDTO= codigoPostalDN.getColoniaPorId(codigoPostalZonaHidroDTO.getId().getNombreColonia());
     	 
   	   return coloniaDTO==null?"":coloniaDTO.getDescription();
    	}
    
	public String getAcciones() {
		CodigoPostalZonaHidroDTO codigoPostalZonaHidroDTO = (CodigoPostalZonaHidroDTO) getCurrentRowObject();
		String idZonaHidro = codigoPostalZonaHidroDTO.getNombreColonia();
		String codigoPostal = codigoPostalZonaHidroDTO.getCodigoPostal().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/codigopostalzonahidro/mostrarDetalle.do?id="
				+ idZonaHidro + "&cp=" + codigoPostal
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>";
	}
}