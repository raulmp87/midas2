package mx.com.afirme.midas.decoradores;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import mx.com.afirme.midas.catalogos.impuestoresidenciafiscal.ImpuestoResidenciaFiscalDTO;

import org.displaytag.decorator.TableDecorator;

public class ImpuestoResidenciaFiscal extends TableDecorator {

	public String getAcciones() {
		ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO = (ImpuestoResidenciaFiscalDTO) getCurrentRowObject();
		String idImpuestoResidenciaFiscal = impuestoResidenciaFiscalDTO.getIdTcImpuestoResidenciaFiscal().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarDetalle.do?id="
				+ idImpuestoResidenciaFiscal
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarModificar.do?id="
				+ idImpuestoResidenciaFiscal
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/impuestoresidenciafiscal/mostrarBorrar.do?id="
				+ idImpuestoResidenciaFiscal
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	
	public String getPorcentaje(){
		ImpuestoResidenciaFiscalDTO impuestoResidenciaFiscalDTO = (ImpuestoResidenciaFiscalDTO) getCurrentRowObject();	
		NumberFormat fPorcentaje = new DecimalFormat("##0.00##%");
		return fPorcentaje.format(impuestoResidenciaFiscalDTO.getPorcentaje()/100);
	}	
}
