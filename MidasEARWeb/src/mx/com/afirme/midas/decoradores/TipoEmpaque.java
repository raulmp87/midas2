package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipoempaque.TipoEmpaqueDTO;

import org.displaytag.decorator.TableDecorator;

public class TipoEmpaque extends TableDecorator {
	
	/**
	 * private BigDecimal idtctipoempaque;
     private String descripciontipoempaque;
	 */
	public String getCodigoTipoEmpaque() {
		TipoEmpaqueDTO tipoEmpaqueDTO = (TipoEmpaqueDTO) getCurrentRowObject();
		return tipoEmpaqueDTO.getCodigoTipoEmpaque().toBigInteger().toString();
	}
	public String getDescripcionTipoEmpaque() {
		TipoEmpaqueDTO tipoEmpaqueDTO = (TipoEmpaqueDTO) getCurrentRowObject();
		return tipoEmpaqueDTO.getDescripcionTipoEmpaque();
	}
	
	public String getAcciones() {

		TipoEmpaqueDTO tipoEmpaqueDTO = (TipoEmpaqueDTO) getCurrentRowObject();
		String idTipoEmpaque = tipoEmpaqueDTO.getIdTipoEmpaque().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoempaque/mostrarDetalle.do?id="
				+ idTipoEmpaque
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoempaque/mostrarModificar.do?id="
				+ idTipoEmpaque
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipoempaque/mostrarBorrar.do?id="
				+ idTipoEmpaque
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
