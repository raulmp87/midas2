package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.subinciso.SubIncisoCotizacionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;

import org.apache.commons.lang.StringUtils;
import org.displaytag.decorator.TableDecorator;


public class SubIncisoCotizacion extends TableDecorator {

	public String getIdSubInciso() {
		SubIncisoCotizacionDTO subIncisoCotizacionDTO = (SubIncisoCotizacionDTO) getCurrentRowObject();

		String id = subIncisoCotizacionDTO.getId().getIdToCotizacion() + "," + subIncisoCotizacionDTO.getId().getNumeroInciso() + "," + subIncisoCotizacionDTO.getId().getIdToSeccion() + "," + subIncisoCotizacionDTO.getId().getNumeroSubInciso();
		
		
		return id;
	}
	
	public String getAcciones() {
		
		SubIncisoCotizacionDTO subIncisoCotizacionDTO = (SubIncisoCotizacionDTO) getCurrentRowObject();		
		
		final CotizacionDTO cotizacionDTO = subIncisoCotizacionDTO.getSeccionCotizacionDTO().getIncisoCotizacionDTO().getCotizacionDTO();
		final SolicitudDTO solicitudDTO = cotizacionDTO.getSolicitudDTO();
		
		final String idSubInciso = getIdSubInciso();
		String origen = StringUtils.EMPTY;
		String tipoEndoso = StringUtils.EMPTY;
		if(solicitudDTO.getIdToPolizaEndosada() == null) {
			if(cotizacionDTO.getClaveEstatus() < 10) {
				// Es orden de trabajo
				origen = "ODT";
			} else {
				// Es cotizacion
				origen = "COT";
			}
		} else {
			// Es endoso
			tipoEndoso = solicitudDTO.getClaveTipoEndoso().toString();
			origen = "END";
		}
		
		final StringBuilder result = new StringBuilder(500);
		result.append("<a href=\"javascript: void(0);\" ")
		.append("onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/subinciso/mostrarBorrar.do?origen=")
		.append(origen)
		.append("&claveTipoEndoso=")
		.append(tipoEndoso)
		.append("&idSubInciso=")
		.append(idSubInciso)
		.append("', 'configuracion_detalle','mostrarDatosSubInciso(1, 1,\\'")
		.append(tipoEndoso)
		.append("\\')');\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>")
		.append("<a href=\"javascript: void(0);\" ")
		.append("onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/subinciso/mostrarModificar.do?origen=")
		.append(origen)
		.append("&claveTipoEndoso=")
		.append(tipoEndoso)
		.append("&idSubInciso=")
		.append(idSubInciso)
		.append("', 'configuracion_detalle','mostrarDatosSubInciso(1, 0,\\'")
		.append(tipoEndoso)
		.append("\\')');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/Edit16.gif'/></a>")
		.append("<a href=\"javascript: void(0);\" ")
		.append("onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/subinciso/mostrarDetalle.do?origen=")
		.append(origen)
		.append("&claveTipoEndoso=")
		.append(tipoEndoso)
		.append("&idSubInciso=")
		.append(idSubInciso)
		.append("', 'configuracion_detalle','mostrarDatosSubInciso(1, 1,\\'")
		.append(tipoEndoso)
		.append("\\')');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		
		return result.toString();
		

	}
	
	
	public String getAccionesSoloLectura() {
		
		String idSubInciso = getIdSubInciso();

		return ""	+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacionsololectura/subinciso/mostrarDetalle.do?idSubInciso="
				+ idSubInciso
				+ "', 'configuracion_detalle','mostrarDatosSubIncisoSoloLectura(1, 1)');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>";
	}
	
	public String getAccionesSoloLecturaODT() {
		
		String idSubInciso = getIdSubInciso();

		return ""	+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/ordentrabajosololectura/mostrarDetalle.do?idSubInciso="
				+ idSubInciso
				+ "', 'configuracion_detalle','mostrarDatosSubIncisoSoloLectura(1, 1)');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>";
	}
	
	
}