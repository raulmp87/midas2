package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import org.displaytag.decorator.TableDecorator;

public class DocumentosDigitalesComplementariosCotizacion extends TableDecorator{

	public String getAcciones() {
		
		DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO = (DocumentoDigitalCotizacionDTO) getCurrentRowObject();
		String idControlArchivo = documentoDigitalCotizacionDTO.getControlArchivo().getIdToControlArchivo().toString();
		String idToCotizacion = documentoDigitalCotizacionDTO.getCotizacionDTO().getIdToCotizacion().toBigInteger().toString();
		String idDocumentoDigitalCotizacion = documentoDigitalCotizacionDTO.getIdDocumentoDigitalCotizacion().toBigInteger().toString();
		return  "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: Confirma('\u00BFDesea borrar este Documento Digital Complementario?',null,'/MidasWeb/cotizacion/borrarDocumentoComplementario.do?idControlArchivo="
				+ idControlArchivo + "&idToCotizacion=" + idToCotizacion + "&idDocumentoDigitalCotizacion=" + idDocumentoDigitalCotizacion
				+ "', 'contenido_documentosDigitalesComplementarios',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>"  
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
				+ idControlArchivo
				+ "', 'download');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>";
	}
}