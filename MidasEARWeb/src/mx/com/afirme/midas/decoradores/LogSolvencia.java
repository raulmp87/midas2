package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.danios.reportes.reportercs.log.SolvenciaLogDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class LogSolvencia extends TableDecorator {
	public String getCorte() throws SystemException {
		SolvenciaLogDTO solvenciaLogDTO = (SolvenciaLogDTO) getCurrentRowObject();
		
		return UtileriasWeb.getFechaString(solvenciaLogDTO.getCorte());
	}

	public String getRamo() {
		SolvenciaLogDTO solvenciaLogDTO = (SolvenciaLogDTO) getCurrentRowObject();
		return solvenciaLogDTO.getRamo();
	}
	
	public String getOrigen() {
		SolvenciaLogDTO solvenciaLogDTO = (SolvenciaLogDTO) getCurrentRowObject();
		return solvenciaLogDTO.getOrigen();
	}
	
	public int getCveProceso() {
		SolvenciaLogDTO solvenciaLogDTO = (SolvenciaLogDTO) getCurrentRowObject();
		return solvenciaLogDTO.getCveProceso().intValue();
	}
	
	public String getError() {
		SolvenciaLogDTO solvenciaLogDTO = (SolvenciaLogDTO) getCurrentRowObject();
		return solvenciaLogDTO.getError();
	}
}
