package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.aumentovario.AumentoVarioDTO;

import org.displaytag.decorator.TableDecorator;

public class AumentoVario extends TableDecorator {
	
	public String getClaveTipoAumento(){
		AumentoVarioDTO aumentoVarioDTO = (AumentoVarioDTO) getCurrentRowObject();
		String clave = aumentoVarioDTO.getClaveTipoAumento().toString();
		return clave.equals("1") ? "PORCENTAJE" : "IMPORTE";
	}
	
	
	public String getAcciones() {
		AumentoVarioDTO aumentoVarioDTO = (AumentoVarioDTO) getCurrentRowObject();
		String idAumentoVario = aumentoVarioDTO.getIdAumentoVario().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/aumentovario/mostrarDetalle.do?id="
				+ idAumentoVario
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/aumentovario/mostrarModificar.do?id="
				+ idAumentoVario
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/aumentovario/mostrarBorrar.do?id="
				+ idAumentoVario
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
