package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.giro.GiroDTO;

import org.displaytag.decorator.TableDecorator;

public class Giro extends TableDecorator {
	
//	SELECT idTcGiro, CODIGOGIRO, DESCRIPCIONGIRO FROM MIDAS.TCGIRO

	public String getIdTcGiro() {
		GiroDTO giroDTO = (GiroDTO) getCurrentRowObject();
		return giroDTO.getIdTcGiro().toBigInteger().toString();
	}

	public String getDescripcionGiro(){
		GiroDTO giroDTO = (GiroDTO) getCurrentRowObject();
		return giroDTO.getDescripcionGiro();
	}
	
	public String getAcciones() {
		GiroDTO giroDTO = (GiroDTO) getCurrentRowObject();
		String idGiro = giroDTO.getIdTcGiro().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/giro/mostrarDetalle.do?id="
				+ idGiro
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/giro/mostrarModificar.do?id="
				+ idGiro
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/giro/mostrarBorrar.do?id="
				+ idGiro
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
