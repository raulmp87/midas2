package mx.com.afirme.midas.decoradores;


import java.math.BigDecimal;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;

import org.displaytag.decorator.TableDecorator;

public class EsquemasReas extends TableDecorator {
		
	public String getAcciones() {
		EsquemasDTO esquemaDTO = (EsquemasDTO) getCurrentRowObject();
		String iDesquema = esquemaDTO.getIdContrato();
		BigDecimal cer = esquemaDTO.getCer();
		BigDecimal nivel = esquemaDTO.getNivel();
		BigDecimal ordenEntrada = esquemaDTO.getOrdenEntrada();

		return  "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/esquemasreas/mostrarModificar.do?idContrato="
				+ iDesquema + "&cer=" + cer + "&nivel=" + nivel + "&ordenEntrada=" + ordenEntrada
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>";
	}
}