package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class ReporteSiniestro extends TableDecorator {

	public String getFormatoFechaReporte(){
		String resultado = "";
		ReporteSiniestroDTO reporteSiniestroDTO = (ReporteSiniestroDTO) getCurrentRowObject();	
		resultado =  UtileriasWeb.getFechaHoraString(reporteSiniestroDTO.getFechaHoraReporte());
		return resultado;
	}
	
	public String getAcciones() {
		ReporteSiniestroDTO reporteSiniestroDTO = (ReporteSiniestroDTO) getCurrentRowObject();
		return reporteSiniestroDTO.getAccion();
		
		/*
		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/siniestro/cabina/mostrarDetalle.do?id="
				+ idReporteSiniestro
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/siniestro/cabina/mostrarModificar.do?id="
				+ idReporteSiniestro
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>";
				//+ "<a href=\"javascript: void(0);\" "
				//+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/distanciaciudad/mostrarBorrar.do?id="
				//+ idDistanciaCiudad
				//+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
		*/
	
	}
	
	/*
	public String getEstadoOrigenDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idEstadoOrigenDescripcion = distanciaCiudadDTO.getIdEstadoOrigen();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 EstadoDTO estadoDTO = codigoPostalDN.getEstadoPorId(idEstadoOrigenDescripcion);
			 return estadoDTO.getStateName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	
	public String getEstadoDestinoDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idEstadoDestinoDescripcion = distanciaCiudadDTO.getIdEstadoDestino();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 EstadoDTO estadoDTO = codigoPostalDN.getEstadoPorId(idEstadoDestinoDescripcion);
			 return estadoDTO.getStateName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	
	public String getCiudadOrigenDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idCiudadOrigenDescripcion = distanciaCiudadDTO.getIdCiudadOrigen();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 CiudadDTO ciudadDTO = codigoPostalDN.getCiudadPorId(idCiudadOrigenDescripcion);
			 return ciudadDTO.getCityName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	
	public String getCiudadDestinoDescripcion(){
		DistanciaCiudadDTO distanciaCiudadDTO = (DistanciaCiudadDTO) getCurrentRowObject();		
		String idCiudadDestinoDescripcion = distanciaCiudadDTO.getIdCiudadDestino();
		CodigoPostalDN codigoPostalDN = CodigoPostalDN.getInstancia();
		
		try{
			 CiudadDTO ciudadDTO = codigoPostalDN.getCiudadPorId(idCiudadDestinoDescripcion);
			 return ciudadDTO.getCityName();
		}catch(ExcepcionDeAccesoADatos edad){
			return ("* Error de acceso *");
		}		
	}
	*/
}
