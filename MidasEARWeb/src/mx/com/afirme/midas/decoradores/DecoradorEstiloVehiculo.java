package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorEstiloVehiculo extends MidasBaseDecorator{

	public String getAcciones() {
		EstiloVehiculoDTO estiloVehiculoDTO = (EstiloVehiculoDTO) getCurrentRowObject();
//		String idTipoVehiculo= ""+estiloVehiculoDTODTO.getIdTcTipoVehiculo();
		String claveEstilo = estiloVehiculoDTO.getId().getClaveEstilo();
		String claveTipoBien = estiloVehiculoDTO.getId().getClaveTipoBien();
		String idVersionCarga = estiloVehiculoDTO.getId().getIdVersionCarga().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/mostrarDetalle.do?claveEstilo="
				+ claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+idVersionCarga
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/mostrarModificar.do?claveEstilo="
				+ claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+idVersionCarga
				+ "', 'contenido','manipulaCalendarioLineas()');\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculo/mostrarBorrar.do?claveEstilo="
				+ claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+idVersionCarga
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
