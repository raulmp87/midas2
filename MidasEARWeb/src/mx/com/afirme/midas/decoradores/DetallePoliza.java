package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;
import java.text.ParseException;

import mx.com.afirme.midas.siniestro.cabina.PolizaDummyDN;
import mx.com.afirme.midas.siniestro.cabina.PolizaDummyDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.displaytag.decorator.TableDecorator;

public class DetallePoliza extends TableDecorator{
	PolizaDummyDTO pdto;
	
	public DetallePoliza() throws ExcepcionDeAccesoADatos, SystemException, ParseException{
		BigDecimal d 					= new BigDecimal(241);
		PolizaDummyDN dd = PolizaDummyDN.getInstancia();
		pdto= dd.obtenerPoliza(d);
	}
	
	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return pdto.getNumeroPoliza().toString();
	}
	/**
	 * @return the tipoNegocio
	 */
	public String getTipoNegocio() {
		return pdto.getTipoNegocio().toString();
	}
	/**
	 * @return the descripcionProducto
	 */
	public String getDescripcionProducto() {
		return pdto.getDescripcionProducto();
	}
	/**
	 * @return the descripcionSubRamo
	 */
	public String getDescripcionSubRamo() {
		return pdto.getDescripcionSubRamo();
	}
	/**
	 * @return the acciones
	 */
	public String getAcciones() {
		StringBuffer buffer = new StringBuffer();
		String id ="";
		buffer.append("<center>");
		
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/terminarOrdenTrabajo.do?id="
				+ id
				+ "', 'contenido',null);\"><img border='0px' alt='Terminar' title='Terminar' src='/MidasWeb/img/confirmAll.gif'/></a>");
		buffer.append("</center>");		
		
		return buffer.toString();
	}
	
}
