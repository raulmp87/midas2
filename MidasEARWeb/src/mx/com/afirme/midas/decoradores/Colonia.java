package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class Colonia extends MidasBaseDecorator {
    
    public String getNombreMunicipio(){
	 String nombreMunicipio="";
	 ColoniaDTO coloniaDTO= (ColoniaDTO) getCurrentRowObject();
	 if(coloniaDTO!=null){
	     MunicipioDTO municipioDTO= CodigoPostalDN.getInstancia().getMunicipioPorId(coloniaDTO.getCityId());
	    if(municipioDTO!= null){
		 nombreMunicipio= municipioDTO.getDescription();
	     }
	 }
	 return nombreMunicipio;
	
    }
    
    public String getAcciones() {
    	ColoniaDTO coloniaDTO= (ColoniaDTO) getCurrentRowObject();
    	StringBuffer buffer = new StringBuffer();

    	buffer.append("<a href=\"javascript: void(0);\" "
    			+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/colonia/mostrarDetalle.do?idColonia="
    			+ coloniaDTO.getColonyId()
    			+ "', 'contenido',null);\"><img border='0px' title='Detalle' alt='Detalle' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a>");
    	buffer.append("&nbsp;");

    	buffer.append( "<a href=\"javascript: void(0);\" "
    			+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/colonia/mostrarModificar.do?idColonia="
    			+ coloniaDTO.getColonyId()
    			+ "', 'contenido',null);\"><img border='0px' title='Modificar' alt='Modificar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");
    	buffer.append("&nbsp;");


    	return buffer.toString();

    }

}
