package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorTipoUsoVehiculo extends MidasBaseDecorator{

	public String getAcciones() {
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = (TipoUsoVehiculoDTO) getCurrentRowObject();
		String id= ""+tipoUsoVehiculoDTO.getIdTcTipoUsoVehiculo();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipousovehiculo/mostrarDetalle.do?id="
				+ id
				+ "', 'contenido','actualizacionUnitaria(1)');\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipousovehiculo/mostrarModificar.do?id="
				+ id
				+ "', 'contenido','actualizacionUnitaria(2)');\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipousovehiculo/mostrarBorrar.do?id="
				+ id
				+ "', 'contenido','actualizacionUnitaria(1)');\"><img border='0px' alt='Borrar'title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
