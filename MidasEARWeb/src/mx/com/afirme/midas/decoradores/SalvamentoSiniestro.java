package mx.com.afirme.midas.decoradores;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import mx.com.afirme.midas.siniestro.salvamento.SalvamentoEstatusDTO;
import mx.com.afirme.midas.siniestro.salvamento.SalvamentoSiniestroDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class SalvamentoSiniestro extends TableDecorator{
	private NumberFormat formateador = new DecimalFormat("#,##0.00");
	public String getArticulo() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		return salvamentoSiniestroDTO.getNombreItem();
	}

	public String getCantidad() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		return formateador.format(salvamentoSiniestroDTO.getCantidad().doubleValue());
	}

	public String getValorEstimado() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		return UtileriasWeb.formatoMoneda(salvamentoSiniestroDTO.getValorEstimado());
	}

	public String getValorVenta() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		if (salvamentoSiniestroDTO.getValorReal()!=null)
			return UtileriasWeb.formatoMoneda(salvamentoSiniestroDTO.getValorReal());
		return "$0.0";
	}

	public String getNumFactura() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		if (salvamentoSiniestroDTO.getNumeroFactura()!=null)
			return salvamentoSiniestroDTO.getNumeroFactura().toString();
		return "";
	}

	public String getEstatus() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		return salvamentoSiniestroDTO.getSalvamentoEstatus().getDescripcion();
	}

	public String getAcciones() {
		SalvamentoSiniestroDTO salvamentoSiniestroDTO = (SalvamentoSiniestroDTO)getCurrentRowObject();
		String idToSalvamentoSiniestro = salvamentoSiniestroDTO.getIdToSalvamentoSiniestro().toString();
		
		String resultado = new String();
		int salvamentoEstatus = salvamentoSiniestroDTO.getSalvamentoEstatus().getIdTcSalvamentoEstatus().intValue();
		resultado += "<center>";
		
		resultado += "<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(document.salvamentoSiniestroForm, '/MidasWeb/siniestro/salvamento/mostrarDetalle.do?idToSalvamentoSiniestro="
					+ idToSalvamentoSiniestro + "','contenido','inicializaComponentesSalvamentoSiniestro(SALVAMENTO_SINIESTRO_DETALLE)');\">"
					+ "<img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>&nbsp;";
		
		//Si no se ha vendido se pueden realizar todas las operaciones
		
		if ( !(salvamentoSiniestroDTO.getEstatusInterno() == SalvamentoSiniestroDTO.ESTATUS_VENDIDO
				|| salvamentoEstatus == SalvamentoEstatusDTO.ESTATUS_ABANDONO
				|| salvamentoEstatus == SalvamentoEstatusDTO.ESTATUS_BAJA_DEPURACION
				|| salvamentoEstatus == SalvamentoEstatusDTO.ESTATUS_INCOSTEABLE
				|| salvamentoEstatus == SalvamentoEstatusDTO.ESTATUS_NO_EXISTE
				|| salvamentoEstatus == SalvamentoEstatusDTO.ESTATUS_VENDIDO
			) ){
			resultado += "<a href=\"javascript: void(0);\" "
				+ "onclick=\"sendRequest(document.salvamentoSiniestroForm, '/MidasWeb/siniestro/salvamento/mostrarModificar.do?idToSalvamentoSiniestro="
				+ idToSalvamentoSiniestro + "','contenido','inicializaComponentesSalvamentoSiniestro(SALVAMENTO_SINIESTRO_MODIFICAR)');\">"
				+ "<img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>&nbsp;";
			
			resultado += "<a href=\"javascript: void(0);\" "
				  + "onclick=\"javascript: mostrarVenderSalvamentoSiniestro("+idToSalvamentoSiniestro+", 'listarSalvamento');\">"
				  + "<img border='0px' alt='Vender' title='Vender' src='/MidasWeb/img/sales.gif'/></a>&nbsp;";
			
			resultado += "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(document.salvamentoSiniestroForm, '/MidasWeb/siniestro/salvamento/mostrarBorrar.do?idToSalvamentoSiniestro="
				+ idToSalvamentoSiniestro + "','contenido','inicializaComponentesSalvamentoSiniestro(SALVAMENTO_SINIESTRO_BORRAR)');\">"
				+ "<img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>&nbsp;";
		}		
		
		resultado += "</center>";
		return resultado;		
	}
}
