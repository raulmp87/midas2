package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.producto.ProductoDTO;

import org.displaytag.decorator.TableDecorator;

public class Producto extends TableDecorator {
	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {

		ProductoDTO productoDTO = (ProductoDTO) getCurrentRowObject();
		String id = productoDTO.getIdToProducto().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		if(productoDTO.getClaveEstatus() == 3) {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/producto/mostrarDetalle.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		} else {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/producto/mostrarBorrar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/producto/mostrarModificar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/producto/mostrarDetalle.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		}
		buffer.append("</center>");		
		return buffer.toString();
	}

}
