package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;

import org.displaytag.decorator.TableDecorator;

public class ReaseguradorCorredor extends TableDecorator{
	/**
	 * private BigDecimal idtcreaseguradorcorredor;
     private String nombre;
     private String ubicacion;
     private String correoelectronico;
     private String cnfs;
     private String tipo;
     private BigDecimal estatus;
     private String nombrecorto;
     private String procedencia;
     private String ciudad;
     private String estado;
     private String pais;
     private BigDecimal telefonofijo;
     private BigDecimal telefonomovil;
	 * @return
	 */
	public String getNombre() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getNombre();
	}
	public String getUbicacion() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getUbicacion();
	}
	public String getCorreoElectronico() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getCorreoelectronico();
	}
	public String getCnfs() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getCnfs();
	}
	public String getTipo() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		if (reaseguradorCorredor.getTipo().equals("0"))
			return "CORREDOR";
		else
			return "REASEGURADOR";
	}
	public String getEstatus() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		if (reaseguradorCorredor.getEstatus().toString().startsWith("0"))
			return "INACTIVO";
		else
			return "ACTIVO";
	}
	public String getNombrecorto() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getNombrecorto();
	}
	public String getProcedencia() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		if (reaseguradorCorredor.getProcedencia().equals("0"))
			return "NACIONAL";
		else
			return "EXTRANJERO";
	}
	public String getCiudad() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getCiudad();
	}
	public String getEstado() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getEstado();
	}
	public String getPais() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getPais();
	}
	public String getTelefonofijo() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		return reaseguradorCorredor.getTelefonofijo();
	}
	public String getTelefonomovil() {
		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		if (reaseguradorCorredor.getTelefonomovil() != null)
			return reaseguradorCorredor.getTelefonomovil();
		else
			return null;
	}
	
	public String getAcciones() {

		ReaseguradorCorredorDTO reaseguradorCorredor = (ReaseguradorCorredorDTO) getCurrentRowObject();
		String idReaseguradorCorredor = reaseguradorCorredor.getIdtcreaseguradorcorredor().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do?id="
				+ idReaseguradorCorredor
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do?id="
				+ idReaseguradorCorredor
				+ "', 'contenido','reiniciarCalendarioDoble();');\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do?id="
				+ idReaseguradorCorredor
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
