package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.siniestro.documentos.DocumentoSiniestroDTO;

import org.displaytag.decorator.TableDecorator;

public class DocumentoSiniestro extends TableDecorator {
	
	public String getNombreDocumentoSiniestro() {
		DocumentoSiniestroDTO documentoSiniestroDTO = (DocumentoSiniestroDTO) getCurrentRowObject();
		return documentoSiniestroDTO.getNombreDocumento();
	}
	
	public String getTipoDocumentoSiniestro() {
		DocumentoSiniestroDTO documentoSiniestroDTO = (DocumentoSiniestroDTO) getCurrentRowObject();
		return documentoSiniestroDTO.getTipoDocumentoSiniestroDTO().getDescripcionTipoDocumento();
	}
	
	public String getURL() {
		DocumentoSiniestroDTO documentoSiniestroDTO = (DocumentoSiniestroDTO) getCurrentRowObject();
		return documentoSiniestroDTO.getUrl();
	}
	
	public String getAcciones() {
		DocumentoSiniestroDTO documentoSiniestroDTO = (DocumentoSiniestroDTO) getCurrentRowObject();
		String idDocumentoSiniestro = documentoSiniestroDTO.getIdToDocumentoSiniestro().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/siniestro/documentos/mostrarModificarDocumentoSiniestro.do?idDocumentoSiniestro="
				+ idDocumentoSiniestro
				+ "', 'contenido',null);\"><img border='0px' title='Modificar' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/siniestro/documentos/borrarDocumentosSiniestro.do?idDocumentoSiniestro="
				+ idDocumentoSiniestro
				+ "', 'contenido',null);\"><img border='0px' title='Borrar' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
				+ documentoSiniestroDTO.getIdToControArchivo()
				+ "', 'download');\"> "
				+ "<midas:escribe propiedad='nombreDocumento' nombre='documentoSiniestroForm'/>"
				+ "<img border='0px' title='Descargar' src='/MidasWeb/img/icons/ico_guardar.gif'/></a> ";
	}
}
