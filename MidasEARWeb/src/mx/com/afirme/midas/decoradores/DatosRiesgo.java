package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.inciso.ConfiguracionDatoIncisoCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DatosRiesgo  extends MidasBaseDecorator {

	public String getDescripcionRiesgo() {
		
		String descripcionRiesgo = "-";
		
		ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacion = 
			(ConfiguracionDatoIncisoCotizacionDTO) getCurrentRowObject();
		
		try {
			RiesgoDTO riesgo = RiesgoDN.getInstancia().getPorId(configuracionDatoIncisoCotizacion.getId().getIdToRiesgo());
			
			descripcionRiesgo = riesgo.getDescripcion();
			
		} catch (Exception e) {} 
		
		return descripcionRiesgo;
	}
	
	public String getClaveImpresionPoliza() {
		StringBuffer buffer = new StringBuffer();
		
		ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacion = 
			(ConfiguracionDatoIncisoCotizacionDTO) getCurrentRowObject();
		
		String chkId = "chk" + configuracionDatoIncisoCotizacion.getId().getIdTcRamo()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getIdTcSubramo()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getIdToRiesgo()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getClaveDetalle()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getIdDato();
		
		
		
		buffer.append("<input type=\"CHECKBOX\" name=\"chkImprimir\" id=\""+ chkId + "\"");
		
		if (configuracionDatoIncisoCotizacion.getClaveimpresionpoliza().intValue() == 1) {
			buffer.append("CHECKED");
		}
		
		buffer.append(" onclick=\"javascript:actualizaDatoRiesgo(document.datosRiesgoForm, this");
		buffer.append("," + configuracionDatoIncisoCotizacion.getId().getIdToRiesgo());
		buffer.append("," + configuracionDatoIncisoCotizacion.getId().getClaveDetalle());
		buffer.append("," + configuracionDatoIncisoCotizacion.getId().getIdDato());
		buffer.append(");\">");
		
		return buffer.toString();
	}
	
	public String getCodigoFormato() {
		StringBuffer buffer = new StringBuffer();
		int contador = 0;
		String[] codigosFormato = new String[]{"Sin Formato", "#,##0"};
	
		ConfiguracionDatoIncisoCotizacionDTO configuracionDatoIncisoCotizacion = 
			(ConfiguracionDatoIncisoCotizacionDTO) getCurrentRowObject();
		
		String comboId = "cbo" + configuracionDatoIncisoCotizacion.getId().getIdTcRamo()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getIdTcSubramo()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getIdToRiesgo()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getClaveDetalle()
		+ "_" + configuracionDatoIncisoCotizacion.getId().getIdDato();
		
		
		buffer.append("<SELECT NAME=\"cboCodFormato\" SIZE=\"1\" id=\""+ comboId + "\"");
		buffer.append(" onchange=\"javascript:actualizaCodigoFormatoDatoRiesgo(document.datosRiesgoForm, this");
		buffer.append("," + configuracionDatoIncisoCotizacion.getId().getIdToRiesgo());
		buffer.append("," + configuracionDatoIncisoCotizacion.getId().getClaveDetalle());
		buffer.append("," + configuracionDatoIncisoCotizacion.getId().getIdDato());
		buffer.append(");\">");
		
		for (String opcion : codigosFormato) {
			buffer.append("<OPTION VALUE=\"" + contador + "\" ");
			
			if (configuracionDatoIncisoCotizacion.getCodigoFormato().intValue() == contador) {
				buffer.append("SELECTED");
			}
			
			buffer.append(">" + opcion + "</OPTION>");
			contador++;
		}
		
		buffer.append("</SELECT>");
		
		return buffer.toString();
	}
	
	
}
