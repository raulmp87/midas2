package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDN;
import mx.com.afirme.midas.cotizacion.inspeccion.proveedor.ProveedorInspeccionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.displaytag.decorator.TableDecorator;

public class ProveedorInspeccion extends TableDecorator{
	
	public String getNombre(){
		String nombre = "No disponible";
		ProveedorInspeccionDTO proveedorDTO = (ProveedorInspeccionDTO) getCurrentRowObject();
		if (proveedorDTO.getPersonaDTO() == null){
			try {
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				System.out.println("Error al obtener la persona del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			} catch (SystemException e) {
				System.out.println("Error al obtener la persona del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			}
		}
		nombre = proveedorDTO.getDescription();
		return nombre;
	}
	
	public String getDireccion(){
		String direccion="No disponible";
		ProveedorInspeccionDTO proveedorDTO = (ProveedorInspeccionDTO) getCurrentRowObject();
		if (proveedorDTO.getDireccionDTO() == null){
			try {
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarDireccionDTO(proveedorDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				System.out.println("Error al obtener la direccion del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			} catch (SystemException e) {
				System.out.println("Error al obtener la direccion del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			}
		}
		direccion = proveedorDTO.getDireccionDTO().getNombreCalle()+" "+proveedorDTO.getDireccionDTO().getNumeroExterior();
		String colonia = null;
		String ciudad = null;
		String estado = null;
		try{
			colonia = CodigoPostalDN.getInstancia().getColoniaPorId(proveedorDTO.getDireccionDTO().getNombreColonia()).getColonyName();
		}catch(NullPointerException exc){}
		catch(Exception e){e.printStackTrace();}
		try{
			ciudad = CodigoPostalDN.getInstancia().getCiudadPorId(""+proveedorDTO.getDireccionDTO().getIdMunicipio()).getCityName();
		}catch(NullPointerException exc){}
		catch(Exception e){e.printStackTrace();}
		try{
			estado = CodigoPostalDN.getInstancia().getEstadoPorId(""+proveedorDTO.getDireccionDTO().getCodigoPostal()).getStateName();
		}catch(NullPointerException exc){}
		catch(Exception e){e.printStackTrace();}
		if (colonia != null)
			direccion += ", " + colonia;
		if (ciudad != null)
			direccion += ", " + ciudad;
		if (estado != null)
			direccion += ", " + estado;
		direccion += ""; 
		return direccion;
	}
	
	public String getTelefono(){
		String telefono = "No registrado";
		ProveedorInspeccionDTO proveedorDTO = (ProveedorInspeccionDTO) getCurrentRowObject();
		if (proveedorDTO.getPersonaDTO() == null){
			try {
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				System.out.println("Error al obtener la persona del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			} catch (SystemException e) {
				System.out.println("Error al obtener la persona del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			}
		}
		if (!UtileriasWeb.esCadenaVacia(proveedorDTO.getPersonaDTO().getTelefono()))
			telefono = proveedorDTO.getPersonaDTO().getTelefono();
		return telefono;
	}
	
	public String getRfc(){
		String rfc = "No registrado";
		ProveedorInspeccionDTO proveedorDTO = (ProveedorInspeccionDTO) getCurrentRowObject();
		if (proveedorDTO.getPersonaDTO() == null){
			try {
				proveedorDTO = ProveedorInspeccionDN.getINSTANCIA().poblarPersonaDTO(proveedorDTO);
			} catch (ExcepcionDeAccesoADatos e) {
				System.out.println("Error al obtener la persona del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			} catch (SystemException e) {
				System.out.println("Error al obtener la persona del Proveedor: "+proveedorDTO.getIdToProveedorInspeccion());
			}
		}
		if (!UtileriasWeb.esCadenaVacia(proveedorDTO.getPersonaDTO().getCodigoRFC()))
			rfc = proveedorDTO.getPersonaDTO().getCodigoRFC();
		return rfc;
	}
	
	public String getAcciones(){
		ProveedorInspeccionDTO proveedorDTO = (ProveedorInspeccionDTO) getCurrentRowObject();
		String id = proveedorDTO.getIdToProveedorInspeccion().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?idPadre="
				+ id + "&accion=detalle"
				+ "', 'contenido',null);\"><img border='0px' alt='Ver detalle' src='/MidasWeb/img/details.png'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?idPadre="
				+ id +"&accion=edita"
				+ "', 'contenido',null);\"><img border='0px' title='Cancelar o Rechazar' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/proveedorInspeccion/mostrarProveedorInspeccion.do?idPadre="
				+ id +"&accion=elimina"
				+ "', 'contenido',null);\"><img border='0px' alt='Asignar' title='Asignar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("</center>");		
		return buffer.toString();
	}
}
