package mx.com.afirme.midas.decoradores;

import org.displaytag.decorator.TableDecorator;

import mx.com.afirme.midas.cotizacion.reaseguro.subinciso.ReaseguroSubIncisoCotizacionForm;

public class ReaseguroSubIncisoCotizacion extends TableDecorator{
	
	public String getNumeroSubInciso(){
		ReaseguroSubIncisoCotizacionForm form = (ReaseguroSubIncisoCotizacionForm)getCurrentRowObject();
		return form.getNumeroSubInciso();
	}
	
	public String getValorPrimaNeta(){
		ReaseguroSubIncisoCotizacionForm form = (ReaseguroSubIncisoCotizacionForm)getCurrentRowObject();
		return form.getValorPrimaNeta();
	}
	
	public String getValorSumaAsegurada(){
		ReaseguroSubIncisoCotizacionForm form = (ReaseguroSubIncisoCotizacionForm)getCurrentRowObject();
		return form.getValorSumaAsegurada();
	}
	
	public String getAcciones(){
		ReaseguroSubIncisoCotizacionForm form = (ReaseguroSubIncisoCotizacionForm)getCurrentRowObject();
		@SuppressWarnings("unused")
		String idTcSubRamo = form.getIdTcSubRamo();
		@SuppressWarnings("unused")
		String idToCotizacion = form.getIdToCotizacion();
		@SuppressWarnings("unused")
		String numeroInciso = form.getNumeroInciso();
		@SuppressWarnings("unused")
		String numeroSubInciso = form.getNumeroSubInciso();

		return "<center><a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: \""
				+ "><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a></center>";
	}
}
