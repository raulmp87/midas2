package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteDTO;

import org.displaytag.decorator.TableDecorator;


public class TipoDestinoTransporte extends TableDecorator {
	
	
	public String getIdTipoDestinoTransporte() {
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = (TipoDestinoTransporteDTO) getCurrentRowObject();
		return tipoDestinoTransporteDTO.getIdTipoDestinoTransporte().toBigInteger().toString();
	}
	
	public String getCodigoTipoDestinoTransporte() {
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = (TipoDestinoTransporteDTO) getCurrentRowObject();
		return tipoDestinoTransporteDTO.getCodigoTipoDestinoTransporte().toBigInteger().toString();
	}
	
	public String getDescripcionTipoDestinoTransporte() {
		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = (TipoDestinoTransporteDTO) getCurrentRowObject();
		return tipoDestinoTransporteDTO.getDescripcionTipoDestinoTransporte();
	}
	
	
	public String getAcciones() {

		TipoDestinoTransporteDTO tipoDestinoTransporteDTO = (TipoDestinoTransporteDTO) getCurrentRowObject();
		String idTipoDestinoTransporte = tipoDestinoTransporteDTO.getIdTipoDestinoTransporte().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipodestinotransporte/mostrarDetalle.do?id="
				+ idTipoDestinoTransporte
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipodestinotransporte/mostrarModificar.do?id="
				+ idTipoDestinoTransporte
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipodestinotransporte/mostrarBorrar.do?id="
				+ idTipoDestinoTransporte
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
