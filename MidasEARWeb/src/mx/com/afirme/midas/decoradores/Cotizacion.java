package mx.com.afirme.midas.decoradores;

import java.text.Format;
import java.text.SimpleDateFormat;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.poliza.renovacion.RenovacionPolizaDN;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoSN;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaSN;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

public class Cotizacion extends MidasBaseDecorator{
	Format fmt = new SimpleDateFormat("dd/MM/yyyy");
	public String getCotizacion(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		return "COT-"+UtileriasWeb.llenarIzquierda(cotizacion.getIdToCotizacion().toString(), "0", 8);
	}
	

	public String getFechaCreacion(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		return fmt.format(cotizacion.getFechaCreacion());
	}
	
	public String getCliente(){
		String cliente = "No disponible";
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		ClienteDTO asegurado = null;
		if (cotizacion.getIdToPersonaAsegurado() != null){
			try{
				if(cotizacion.getIdDomicilioAsegurado() != null && cotizacion.getIdDomicilioAsegurado().intValue() > 0){
					asegurado = new ClienteDTO();
					asegurado.setIdCliente(cotizacion.getIdToPersonaAsegurado());
					asegurado.setIdDomicilio(cotizacion.getIdDomicilioAsegurado());
					asegurado = ClienteDN.getInstancia().verDetalleCliente(asegurado, cotizacion.getCodigoUsuarioCotizacion());
				}else{
					asegurado = ClienteDN.getInstancia().verDetalleCliente(cotizacion.getIdToPersonaAsegurado(), cotizacion.getCodigoUsuarioCotizacion());
				}				
			}catch(Exception exc){}
			if (asegurado != null){
				if (asegurado.getClaveTipoPersona() == 1)
					cliente = asegurado.getNombre() + " " + asegurado.getApellidoPaterno() + " " + asegurado.getApellidoMaterno();
				else
					cliente = asegurado.getNombre();
			}
		}
		return cliente;
	}
	
	public String getProducto(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String producto = "NOT FOUND";
		try {
			ProductoDTO productoDTO = new ProductoSN().encontrarPorSolicitud(cotizacion.getSolicitudDTO().getIdToSolicitud());
			producto = productoDTO.getNombreComercial();
		} catch (ExcepcionDeAccesoADatos e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (NullPointerException e){e.printStackTrace();}
		return producto;
	}
	
	public String getTipoPoliza(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String poliza = "NO ASIGNADA";
		try {
			poliza = new TipoPolizaSN().encontrarPorCotizacion(cotizacion.getIdToCotizacion()).getNombreComercial();
		} catch (SystemException e) {
		} catch(NullPointerException e){}
		return poliza;
	}
	
	public String getEstatus(){
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String estatus = "NOT FOUND";
		estatus = UtileriasWeb.getDescripcionCatalogoValorFijo(30, cotizacion.getClaveEstatus());
		if(estatus == null)
			estatus="No disponible";
		return estatus;
	}
	
	public String getAcciones() throws ExcepcionDeAccesoADatos, SystemException{
		CotizacionDTO cotizacion = (CotizacionDTO) getCurrentRowObject();
		String id = cotizacion.getIdToCotizacion().toString();
		StringBuffer buffer = new StringBuffer();

		//Verificar permiso para consultar la cotizacion
		//Implica ver el arbol de incisos y el tabbar, siempre se puede hacer, sin importar el estatus
		buffer.append("<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacionsololectura/cotizacion/mostrar.do?id="+id
				+ "', 'contenido','creaArbolCotizacionSoloLectura("+id+");dhx_init_tabbars();');\"><img border='0px' alt='Ver Cotizacion' title='Ver Cotizacion' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a>");			
		buffer.append("&nbsp;");

		//autorizar o rechazar las RDN no cumplidas
		
		boolean requiereAutorizacionVigencia = (cotizacion.getClaveAutRetroacDifer() != null && 
											    (cotizacion.getClaveAutRetroacDifer().shortValue() == Sistema.AUTORIZACION_REQUERIDA ||
												 cotizacion.getClaveAutRetroacDifer().shortValue() == Sistema.RECHAZADA)) || 
											   (cotizacion.getClaveAutVigenciaMaxMin() != null && 
												(cotizacion.getClaveAutVigenciaMaxMin().shortValue() == Sistema.AUTORIZACION_REQUERIDA || 
												 cotizacion.getClaveAutVigenciaMaxMin().shortValue() == Sistema.RECHAZADA));
		
		boolean requiereAutorizacionDerechos = cotizacion.getClaveAutorizacionDerechos() != null && 
											   (cotizacion.getClaveAutorizacionDerechos().shortValue() == Sistema.AUTORIZACION_REQUERIDA || 
											    cotizacion.getClaveAutorizacionDerechos().shortValue() == Sistema.RECHAZADA);
		
		boolean requiereAutorizacionRPF = cotizacion.getClaveAutorizacionRecargoPagoFraccionado() != null && 
		   								  (cotizacion.getClaveAutorizacionRecargoPagoFraccionado().shortValue() == Sistema.AUTORIZACION_REQUERIDA || 
		   								   cotizacion.getClaveAutorizacionRecargoPagoFraccionado().shortValue() == Sistema.RECHAZADA);
		
		boolean requiereAutorizacionDiasGracia = cotizacion.getClaveAutorizacionDiasGracia() != null && (
				cotizacion.getClaveAutorizacionDiasGracia().shortValue() == Sistema.AUTORIZACION_REQUERIDA ||
				cotizacion.getClaveAutorizacionDiasGracia().shortValue() == Sistema.RECHAZADA );
		
		if (requiereAutorizacionVigencia || requiereAutorizacionDerechos || requiereAutorizacionRPF || requiereAutorizacionDiasGracia){
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/autorizacion/mostrarODT.do?id=" + id
					+ "', 'contenido','dhx_init_tabbars();');\"><img border='0px' alt='Autorizar' title='Autorizar' src='/MidasWeb/img/confirmAll.gif'/></a>");
			buffer.append("&nbsp;");
		}else{
			buffer.append("<a href=\"javascript: void(0);\" " + "<img border='0px' src='/MidasWeb/img/blank.gif'/></a>");
			buffer.append("&nbsp;");
		}			
		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_ENPROCESO ){	
			//Si est� en proceso, se puede modificar

			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id=" + id
					+ "', 'contenido','creaArbolCotizacion("+id+");dhx_init_tabbars();');\"><img border='0px' alt='Editar' title='Editar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");			
			buffer.append("&nbsp;");

			//Permiso para imprimir
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: imprimirCotizacion(" + id
					+ ")\"><img border='0px' alt='Imprimir Cotizaci\u00f3n' title='Imprimir Cotizaci\u00f3n' src='/MidasWeb/img/ico_impresion.gif'/></a>");			
			buffer.append("&nbsp;");	
		}
		
		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_ASIGNADA ){	
			//Si est� en proceso, se puede modificar
			if(cotizacion.getSolicitudDTO().getEsRenovacion().shortValue() == Sistema.ES_RENOVACION){
				if(RenovacionPolizaDN.getInstancia().tieneSieniestros(cotizacion.getSolicitudDTO().getIdToPolizaAnterior(), cotizacion.getCodigoUsuarioCreacion())){
					buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: confirmaRenovacion("+id+");\"><img border='0px' alt='Editar' title='Editar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");														
				}else{
					buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id=" + id
							+ "', 'contenido','creaArbolCotizacion("+id+");dhx_init_tabbars();');\"><img border='0px' alt='Editar' title='Editar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");														
				}
			}else{
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/cotizacion/mostrar.do?id=" + id
						+ "', 'contenido','creaArbolCotizacion("+id+");dhx_init_tabbars();');\"><img border='0px' alt='Editar' title='Editar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");			

			}
			buffer.append("&nbsp;");		
		}		
		
		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_LIBERADA){
			//Si est� liberada, se puede enviar a emision
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(document.cotizacionForm,'/MidasWeb/cotizacion/enviarEmision.do?id=" + id
					+ "', 'contenido','listarCotizaciones();');\"><img border='0px' alt='Enviar a emisi&oacute;n' title='Enviar a emision' src='/MidasWeb/img/icons/ico_buscarcliente.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: cambiarEstatusCotLiberada(" + id
					+ ")\"><img border='0px' alt='Regresar a Estatus en Proceso' title='Regresar a Estatus en Proceso' src='/MidasWeb/img/icons/ico_regresar.gif'/></a>");
			buffer.append("&nbsp;");
			//Permiso para imprimir
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: imprimirCotizacion(" + id
					+ ")\"><img border='0px' alt='Imprimir Cotizaci\u00f3n' title='Imprimir Cotizaci\u00f3n' src='/MidasWeb/img/ico_impresion.gif'/></a>");			
			buffer.append("&nbsp;");
		}		
		
		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_LISTA_EMITIR){
			//Si est� lista para emitir, se puede asignar
			if(cotizacion.getSolicitudDTO().getClaveEstatus().shortValue() == Sistema.ESTATUS_SOL_PENDIENTE_AUTORIZACION){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: cambiarEstatusCotLiberada(" + id
						+ ")\"><img border='0px' alt='Regresar a Estatus en Proceso' title='Regresar a Estatus en Proceso' src='/MidasWeb/img/icons/ico_regresar.gif'/></a>");
				buffer.append("&nbsp;");						
			}else if(cotizacion.getSolicitudDTO().getClaveEstatus().shortValue() == Sistema.ESTATUS_SOL_EN_PROCESO){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: cambiarEstatusCotLiberada(" + id
						+ ")\"><img border='0px' alt='Regresar a Estatus en Proceso' title='Regresar a Estatus en Proceso' src='/MidasWeb/img/icons/ico_regresar.gif'/></a>");
				buffer.append("&nbsp;");											
			}else{
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarAsignarCotizacion(" + id
						+ ",-1);\"><img border='0px' title='Asignar' alt='Asignar' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");
				buffer.append("&nbsp;");		
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: cambiarEstatusCotLiberada(" + id
						+ ")\"><img border='0px' alt='Regresar a Estatus en Proceso' title='Regresar a Estatus en Proceso' src='/MidasWeb/img/icons/ico_regresar.gif'/></a>");
				buffer.append("&nbsp;");						
			}

			//Permiso para imprimir
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: imprimirCotizacion(" + id
					+ ")\"><img border='0px' alt='Imprimir Cotizaci\u00f3n' title='Imprimir Cotizaci\u00f3n' src='/MidasWeb/img/ico_impresion.gif'/></a>");			
			buffer.append("&nbsp;");		
		}
		
		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_ASIGNADA_EMISION){	
			//Si est� asignada, se puede emitir
				//Emitir
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: mostrarEmisorComplementar(" + id
					+ ",'COT')\"><img border='0px' alt='Emitir P\u00f3liza' title='Emitir P\u00f3liza' src='/MidasWeb/img/icons/ico_buscarcliente.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: cambiarEstatusCotLiberada(" + id
					+ ")\"><img border='0px' alt='Regresar a Estatus en Proceso' title='Regresar a Estatus en Proceso' src='/MidasWeb/img/icons/ico_regresar.gif'/></a>");
			buffer.append("&nbsp;");
		}
		
		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_EMITIDA){
			//Permiso para imprimir
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: imprimirCotizacion(" + id
					+ ")\"><img border='0px' alt='Imprimir Cotizaci\u00f3n' title='Imprimir Cotizaci\u00f3n' src='/MidasWeb/img/ico_impresion.gif'/></a>");			
			buffer.append("&nbsp;");
		}

		if (cotizacion.getClaveAutoEmisionDocOperIlicitas()!=null && cotizacion.getClaveAutoEmisionDocOperIlicitas().intValue() == 1){
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"mostrarAutorizadorComplementar(" + id + ")\">" +
					"<img border='0px' alt='Autorizacion de omision de documentos' title='Autorizacion de omision de documentos' src='/MidasWeb/img/icons/ico_calendario.gif'/></a>");
			buffer.append("&nbsp;");
		}
		
//		if (cotizacion.getClaveEstatus().intValue() == Sistema.ESTATUS_COT_PENDIENTE_AUTORIZACION ){
//			buffer.append("<a href=\"javascript: void(0);\" "
//					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/cotizacion/autorizacion/mostrarODT.do?id=" + id
//					+ "', 'contenido','dhx_init_tabbars();');\"><img border='0px' alt='Autorizar' title='Autorizar' src='/MidasWeb/img/confirmAll.gif'/></a>");
//			buffer.append("&nbsp;");		
//		}			
		return buffer.toString();
	}
	
}
