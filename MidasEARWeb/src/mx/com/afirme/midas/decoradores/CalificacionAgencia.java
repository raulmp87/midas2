package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;

import org.displaytag.decorator.TableDecorator;

public class CalificacionAgencia extends TableDecorator {
		
	public String getAcciones() {
		CalificacionAgenciaDTO contactoDTO = (CalificacionAgenciaDTO) getCurrentRowObject();
		String iDContacto = contactoDTO.getId().toString();

		return /*"<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/calificacionesreas/mostrarDetalle.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+*/ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/calificacionesreas/mostrarModificar.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/calificacionesreas/mostrarBorrar.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}