package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorTipoBienAutos extends MidasBaseDecorator{
	public String getAcciones() {
		TipoBienAutosDTO tipoBienAutosDTO = (TipoBienAutosDTO) getCurrentRowObject();
		String idTipoBienAutos = tipoBienAutosDTO.getClaveTipoBien();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipobienautos/mostrarDetalle.do?id="
				+ idTipoBienAutos
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipobienautos/mostrarModificar.do?id="
				+ idTipoBienAutos
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipobienautos/mostrarBorrar.do?id="
				+ idTipoBienAutos
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
