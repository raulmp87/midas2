package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.productoscnsf.ProductoCnsfDTO;

import org.displaytag.decorator.TableDecorator;

public class ProductoCnsf extends TableDecorator{

	public String getId() {
		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		return ProductoCnsfDTO.getId().toBigInteger().toString();
	}
	
	public String getDescripcion(){
		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		return ProductoCnsfDTO.getDescripcionProd();
	}
	
	public String getClaveProdServ(){
		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		return ProductoCnsfDTO.getClaveProdServ();
	}
	
	public String getIvaTrasladado(){
		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		return ProductoCnsfDTO.getIvaTrasladado();
	} 
	
	public String getIepsTrasladado(){
		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		return ProductoCnsfDTO.getIepsTrasladado();
	}
	
	public String getComplemento(){
		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		return ProductoCnsfDTO.getComplementoProd();
	}
	public String getAcciones() {

		ProductoCnsfDTO ProductoCnsfDTO = (ProductoCnsfDTO) getCurrentRowObject();
		String idProducto = ProductoCnsfDTO.getId().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/productoscnsf/mostrarDetalle.do?id="
				+ idProducto
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/productoscnsf/mostrarModificar.do?id="
				+ idProducto
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/productoscnsf/mostrarBorrar.do?id="
				+ idProducto
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}

