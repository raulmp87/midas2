package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.reaseguro.riesgoscontraparte.ContratosCoberturaDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class ContratosCobertura extends TableDecorator {
	
	public String getFechaCorte() {
		ContratosCoberturaDTO documentoDigitalSolicitudDTO = (ContratosCoberturaDTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalSolicitudDTO.getFechaCorte());
	}
	
	public String getRamo() {
		ContratosCoberturaDTO documentoDigitalSolicitudDTO = (ContratosCoberturaDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getRamo();
	}
	
	public String getClaveEsquema() {
		ContratosCoberturaDTO documentoDigitalSolicitudDTO = (ContratosCoberturaDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getCveEsquema();
	}
	
	public BigDecimal getMonto() {
		ContratosCoberturaDTO documentoDigitalSolicitudDTO = (ContratosCoberturaDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getMonto();
	}
	
	public String getCalificacion() {
		ContratosCoberturaDTO documentoDigitalSolicitudDTO = (ContratosCoberturaDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getCalificacion();
	}

	/**
	 * toma la cadena para mostrar las acciones Contratos
	 * 
	 * @return String El texto HTML para la columna de acciones Contrato
	 */
	public String getAcciones() {

		ContratosCoberturaDTO contratoDTO = (ContratosCoberturaDTO) getCurrentRowObject();
		String id = contratoDTO.getIdcontrato().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el archivo seleccionado?', null,'/MidasWeb/reaseguro/riesgoscontraparte/borrarDocumento.do?id="
						+ id +"&fechaCorte=" + contratoDTO.getFechaCorte()
						+"&ramo=" + contratoDTO.getRamo()
						+"&cveEsquema=" + contratoDTO.getCveEsquema()
						+"&monto=" + contratoDTO.getMonto()
						+"&calificacion=" + contratoDTO.getCalificacion()
						+ "', 'resultados',null);\"><img border='0px' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		return buffer.toString();
	}

}
