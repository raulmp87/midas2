package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.catalogos.eventocatastrofico.EventoCatastroficoDTO;

import org.displaytag.decorator.TableDecorator;

public class EventoCatastrofico extends TableDecorator {
	
	public String getAcciones() {

		EventoCatastroficoDTO eventoCatastroficoDTO = (EventoCatastroficoDTO) getCurrentRowObject();
		BigDecimal idTcEventoCatastrofico = eventoCatastroficoDTO.getIdTcEventoCatastrofico();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/eventocatastrofico/mostrarDetalle.do?idTcEventoCatastrofico="
				+ idTcEventoCatastrofico
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/eventocatastrofico/mostrarModificar.do?idTcEventoCatastrofico="
				+ idTcEventoCatastrofico
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/eventocatastrofico/mostrarBorrar.do?idTcEventoCatastrofico="
				+ idTcEventoCatastrofico
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
