package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.reaseguro.soporte.slip.SlipAnexoSoporteDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;

import org.displaytag.decorator.TableDecorator;

public class SlipAnexoEquipoContratista extends TableDecorator{
	
	public String getArchivo() throws SystemException {
		SlipAnexoSoporteDTO slipAnexoEquipoContratistaDTO = (SlipAnexoSoporteDTO) getCurrentRowObject();
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(slipAnexoEquipoContratistaDTO.getIdToControlArchivo());
		return controlArchivoDTO.getNombreArchivoOriginal();
	}

	public String getAcciones() {
		SlipAnexoSoporteDTO slipAnexoEquipoContratistaDTO = (SlipAnexoSoporteDTO) getCurrentRowObject();
		String id = slipAnexoEquipoContratistaDTO.getIdToSlipDocumentoAnexo().toString();
		String idControl = slipAnexoEquipoContratistaDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el archivo seleccionado?', null,'/MidasWeb/contratofacultativo/slip/borrarDocumento.do?id="
						+ id
						+ "', 'resultados',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}
	
}
