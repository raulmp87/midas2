package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.danios.reportes.reporterr6.ReporteRR6DTO;
import mx.com.afirme.midas.reaseguro.riesgoscontraparte.ContratosCoberturaDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;

import org.displaytag.decorator.TableDecorator;

public class ReporteRR6 extends TableDecorator {
	
		
	public String getFechaInicio() {
		ReporteRR6DTO documentoDigitalRR6DTO = (ReporteRR6DTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getFechaInicio());
	}
	
	public String getFechaFin() {
		ReporteRR6DTO documentoDigitalRR6DTO = (ReporteRR6DTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getFechaFin());
	}
	
	public String getIniEjecucion() {
		ReporteRR6DTO documentoDigitalRR6DTO = (ReporteRR6DTO) getCurrentRowObject();
		return UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getIniEjecucion());
	}
	
	public String getFinEjecucion() {
		ReporteRR6DTO documentoDigitalRR6DTO = (ReporteRR6DTO) getCurrentRowObject();
		String finEjecucion = "";
		if(documentoDigitalRR6DTO.getFinEjecucion()!=null){
		finEjecucion =  UtileriasWeb.getFechaString(documentoDigitalRR6DTO.getFinEjecucion());
		}
			
		return finEjecucion;
	}
	
	public String getReporte() {
		ReporteRR6DTO documentoDigitalRR6DTO = (ReporteRR6DTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getReporte();
	}
		
	public String getEstatus() {
		ReporteRR6DTO documentoDigitalRR6DTO = (ReporteRR6DTO) getCurrentRowObject();
		return documentoDigitalRR6DTO.getEstatus();
	}

}
