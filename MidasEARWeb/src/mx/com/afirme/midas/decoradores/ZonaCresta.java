package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.zonacresta.ZonaCrestaDTO;
import org.displaytag.decorator.TableDecorator;

public class ZonaCresta extends TableDecorator {
	
	
	public String getNumeroarea(){
		ZonaCrestaDTO zonaCrestaDTO = (ZonaCrestaDTO) getCurrentRowObject();
		String numeroArea = zonaCrestaDTO.getNumeroarea().toBigInteger().toString();
		return numeroArea;
	}
	
	
	public String getAgregarViejo(){
		ZonaCrestaDTO zonaCrestaDTO = (ZonaCrestaDTO) getCurrentRowObject();
		String idZonaCresta = zonaCrestaDTO.getIdtczonacresta().toString();
		
		return "<a href=\"javascript: void(0);\" "
		+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do?id="
		+ idZonaCresta
		+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>";
	}
	
	
	public String getAcciones() {

		ZonaCrestaDTO zonaCrestaDTO = (ZonaCrestaDTO) getCurrentRowObject();
		String idZonaCresta = zonaCrestaDTO.getIdtczonacresta().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarDetalle.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarModificar.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonacresta/mostrarBorrar.do?id="
				+ idZonaCresta
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	
}
