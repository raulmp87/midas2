package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.mediotransporte.MedioTransporteDTO;

import org.displaytag.decorator.TableDecorator;

public class MedioTransporte extends TableDecorator{
	

	public String getCodigoMedioTransporte() {
		MedioTransporteDTO medioTransporteDTO = (MedioTransporteDTO) getCurrentRowObject();
		String idMedioTransporte = medioTransporteDTO.getCodigoMedioTransporte().toBigInteger().toString();
		return idMedioTransporte;
	}


    
	public String getAcciones() {

		MedioTransporteDTO medioTransporteDTO = (MedioTransporteDTO) getCurrentRowObject();
		String idTabla = medioTransporteDTO.getIdMedioTransporte().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/mediotransporte/mostrarDetalle.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/mediotransporte/mostrarModificar.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/mediotransporte/mostrarBorrar.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	 

}
