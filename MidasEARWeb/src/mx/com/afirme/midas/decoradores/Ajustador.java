package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.ajustador.AjustadorDTO;

import org.displaytag.decorator.TableDecorator;

public class Ajustador extends TableDecorator {

	public String getAcciones() {
		AjustadorDTO ajustadorDTO = (AjustadorDTO) getCurrentRowObject();
		String iDAjustador = ajustadorDTO.getIdTcAjustador().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/ajustador/mostrarDetalle.do?id="
				+ iDAjustador
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/ajustador/mostrarModificar.do?id="
				+ iDAjustador
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/ajustador/mostrarBorrar.do?id="
				+ iDAjustador
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
