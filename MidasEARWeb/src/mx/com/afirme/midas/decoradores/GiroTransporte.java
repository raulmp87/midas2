package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.girotransporte.GiroTransporteDTO;
import org.displaytag.decorator.TableDecorator;

public class GiroTransporte  extends TableDecorator{
	

	public String getCodigoGiroTransporte() {
		GiroTransporteDTO giroTransporteDTO = (GiroTransporteDTO) getCurrentRowObject();
		String idGiroTransporte = giroTransporteDTO.getCodigoGiroTransporte().toBigInteger().toString();
		return idGiroTransporte;
	}
    
	public String getAcciones() {

		GiroTransporteDTO giroTransporteDTO = (GiroTransporteDTO) getCurrentRowObject();
		String idTabla = giroTransporteDTO.getIdGiroTransporte().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/girotransporte/mostrarDetalle.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/girotransporte/mostrarModificar.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/girotransporte/mostrarBorrar.do?id="
				+ idTabla
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
