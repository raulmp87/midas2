package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class CancelacionRehabilitacionEndoso extends MidasBaseDecorator {

	public String getNumeroEndoso(){
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		if (endosoDTO.getId().getNumeroEndoso().intValue() == 0)
			return "Poliza Original";
		else
			return endosoDTO.getId().getNumeroEndoso().toString();
	}
	
	public String getClaveTipoEndoso() throws SystemException{
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		
		BigDecimal idToCotizacion = endosoDTO.getIdToCotizacion();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia("").getPorId(idToCotizacion);
		BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();

		String result = "";
		if(claveTipoEndoso != null && claveTipoEndoso.intValue() > 2) {
			int tipoEndoso = endosoDTO.getClaveTipoEndoso().intValue();
			if (endosoDTO.getId().getNumeroEndoso().intValue() == 0)
				result = "";
			else{
				switch(tipoEndoso){
					case 1:
						result = "Endoso de Cambio";
						break;
					case 2:
						result = "Endoso de Aumento";
						break;
					case 3:
						result = "Endoso de Disminución";
						break;
					case 4:
						result = "Endoso de Disminución";
						break;
					case 5:
						result = "Endoso de Aumento";
						break;
					default:
						result = "No disponible";
						break;
				}
			}
		}
		return result;
	}

	public String getMotivoEndoso() throws SystemException {
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		BigDecimal idToCotizacion = endosoDTO.getIdToCotizacion();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia("").getPorId(idToCotizacion);
		BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();
		String descripcion = "";
		if(claveTipoEndoso != null) {
			descripcion = UtileriasWeb.getDescripcionCatalogoValorFijo(40, claveTipoEndoso.intValue());
		}
		return descripcion;
	}
	
	
	public String getFechaInicioVigencia() throws SystemException {
		
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		StringBuffer buffer = new StringBuffer();
		String idFecha = "fecha_" + endosoDTO.getId().getIdToPoliza() + "_" + endosoDTO.getId().getNumeroEndoso();
		
		
		buffer.append("");
		buffer.append("<a href=\"javascript: void(0);\" id=\"mostrarCalendario\" ");
		buffer.append("onclick=\"javascript: mostrarCalendarioCancelacionRehabilitacionEndosos(" + idFecha + ");\">");
		buffer.append("<image src=\"/MidasWeb/img/b_calendario.gif\" border=\"0\"/></a>");
		
		
		buffer.append("<input");
		buffer.append(" id=\"" + idFecha + "\"");
		buffer.append(" type=\"text\"");
		buffer.append(" maxlength=\"10\"");
		buffer.append(" size=\"5\"");
		buffer.append(" style=\"width:70px;\"");
		buffer.append(" name=\"fechaInicioVigencia\"");
		buffer.append(" class=\"cajaTexto\"");
		buffer.append(" onkeypress=\"return soloFecha(this, event, false);\"");
		buffer.append(" onfocus=\"javascript: new Mask('dd/mm/yyyy', 'date').attach(this)\">");
		
		return buffer.toString();
	}
	
	

	public String getAcciones() throws SystemException {
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		StringBuffer buffer = new StringBuffer();
		String idFecha = "fecha_" + endosoDTO.getId().getIdToPoliza() + "_" + endosoDTO.getId().getNumeroEndoso();
		
		if (endosoDTO.getCancela() == null) {//Si es un endoso cancelable
		
			buffer.append("<center><a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: cancelarEndosoPoliza(" + 
						endosoDTO.getId().getIdToPoliza().toBigInteger().toString() + "," + endosoDTO.getId().getNumeroEndoso() + "," + idFecha
					+ ")\"><img border='0px' alt='Cancelar Endoso' title='Cancelar Endoso' src='/MidasWeb/img/icons/ico_rechazar1.gif'/></a></center>");
			
		} else { //Si es un rehabilitable
			buffer.append("<center><a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: rehabilitarEndosoPoliza(" + 
						endosoDTO.getId().getIdToPoliza().toBigInteger().toString() + "," + endosoDTO.getId().getNumeroEndoso() + "," + idFecha
					+ ")\"><img border='0px' alt='Rehabilitar Endoso' title='Rehabilitar Endoso' src='/MidasWeb/img/icons/ico_terminar.gif'/></a></center>");
			
		}
		
		return buffer.toString();
	}
	
	
}
