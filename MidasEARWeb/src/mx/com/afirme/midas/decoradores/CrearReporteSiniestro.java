package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.danios.soporte.PolizaSoporteDanosDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;

import org.displaytag.decorator.TableDecorator;


public class CrearReporteSiniestro extends TableDecorator {

	
	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public String getAcciones() throws ExcepcionDeAccesoADatos, SystemException {

		PolizaSoporteDanosDTO polizaSoporteDanosDTO = (PolizaSoporteDanosDTO) getCurrentRowObject();
		BigDecimal idToPoliza = polizaSoporteDanosDTO.getIdToPoliza();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		
		buffer.append("<a href=\"javascript: void(0);\" ");
		buffer.append("onclick=\"javascript: habilitaTabPoliza('");
		buffer.append(idToPoliza);
		buffer.append( "');\"><img border='0px' alt='Habilita' src='/MidasWeb/img/Edit14.gif'/></a>");
		buffer.append("&nbsp;");
		
//		buffer.append("<a href=\"javascript: void(0);\" "
//						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/siniestro/cabina/reportePoliza.do?id="
//						+ numeroPoliza
//						+ "', 'contenido',null);\"><img border='0px' title='Borrar' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
//		buffer.append("&nbsp;");
		buffer.append("</center>");

		return buffer.toString();
	}
}
