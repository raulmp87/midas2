package mx.com.afirme.midas.decoradores;



import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCnsfDTO;

import org.displaytag.decorator.TableDecorator;

public class ReaseguradoresCNSF extends TableDecorator {
		
	public String getAcciones() {
		ReaseguradorCnsfDTO contactoDTO = (ReaseguradorCnsfDTO) getCurrentRowObject();
		String iDContacto = contactoDTO.getId().toString();

		return 
				"<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradoresCNSF/mostrarModificar.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/reaseguradoresCNSF/mostrarBorrar.do?id="
				+ iDContacto
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
