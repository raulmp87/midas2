package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;

import org.displaytag.decorator.TableDecorator;

public class RecargoVario extends TableDecorator {
	
	
	public String getClavetiporecargo(){
		RecargoVarioDTO recargoVarioDTO = (RecargoVarioDTO) getCurrentRowObject();
		String clave = recargoVarioDTO.getClavetiporecargo().toString();
		return clave.equals("1") ? "PORCENTAJE" : "IMPORTE";
	}
	
	public String getAcciones() {
		RecargoVarioDTO recargoVarioDTO = (RecargoVarioDTO) getCurrentRowObject();
		String idRecargoVario = recargoVarioDTO.getIdtorecargovario().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/recargovario/mostrarDetalle.do?id="
				+ idRecargoVario
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/recargovario/mostrarModificar.do?id="
				+ idRecargoVario
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/recargovario/mostrarBorrar.do?id="
				+ idRecargoVario
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
