package mx.com.afirme.midas.decoradores;

import java.text.MessageFormat;

import mx.com.afirme.midas.contratos.participacioncorredor.ParticipacionCorredorDTO;

import org.displaytag.decorator.TableDecorator;

public class ParticipacionCorredor extends TableDecorator {

	public String getAcciones() {
		ParticipacionCorredorDTO participacionCorredorDTO = (ParticipacionCorredorDTO) getCurrentRowObject();
		String id = participacionCorredorDTO.getIdTdParticipacionCorredor().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/mostrarModificar.do?id="
				+ id
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(document.participacionCorredorForm,'/MidasWeb/contratos/participacioncorredor/mostrarBorrar.do?id="
				+ id
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
	
	public String getNombreReasegurador(){
		ParticipacionCorredorDTO participacionCorredorDTO = (ParticipacionCorredorDTO) getCurrentRowObject();
		return participacionCorredorDTO.getReaseguradorCorredor().getNombre();
	}
	
	public String getNombreCortoReasegurador(){
		ParticipacionCorredorDTO participacionCorredorDTO = (ParticipacionCorredorDTO) getCurrentRowObject();
		return participacionCorredorDTO.getReaseguradorCorredor().getNombrecorto();
	}
	
	public String getCnfs(){
		ParticipacionCorredorDTO participacionCorredorDTO = (ParticipacionCorredorDTO) getCurrentRowObject();
		return participacionCorredorDTO.getReaseguradorCorredor().getCnfs();
	}
	
	public String getPorcentajeParticipacionFormato(){
		ParticipacionCorredorDTO participacionCorredorDTO = (ParticipacionCorredorDTO) getCurrentRowObject();
		Double porcentaje = participacionCorredorDTO.getPorcentajeParticipacion();				
		return MessageFormat.format("{0,number,#.##%}", porcentaje/100);		
	}
}
