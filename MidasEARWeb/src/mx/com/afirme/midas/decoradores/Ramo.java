package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;

import org.displaytag.decorator.TableDecorator;

public class Ramo extends TableDecorator {
	
	/**
	 *  private BigDecimal idTcRamo;
		private BigDecimal idEmision;
		private BigDecimal idTecnico;
		private String descripcion;
	 */
	public String getIdTcRamo() {
		RamoDTO ramoDTO = (RamoDTO) getCurrentRowObject();
		return ramoDTO.getIdTcRamo().toBigInteger().toString();
	}
	
	public String getDescripcion(){
		RamoDTO ramoDTO = (RamoDTO) getCurrentRowObject();
		return ramoDTO.getDescripcion();
	}
	
	public String getCodigo(){
		RamoDTO ramoDTO = (RamoDTO) getCurrentRowObject();
		return ramoDTO.getCodigo().toBigInteger().toString();
	}
	
	public String getAcciones() {

		RamoDTO ramoDTO = (RamoDTO) getCurrentRowObject();
		String idRamo = ramoDTO.getIdTcRamo().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/ramo/mostrarDetalle.do?id="
				+ idRamo
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/ramo/mostrarModificar.do?id="
				+ idRamo
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/ramo/mostrarBorrar.do?id="
				+ idRamo
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
