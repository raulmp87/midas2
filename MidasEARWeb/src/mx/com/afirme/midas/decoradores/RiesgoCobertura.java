package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;

import org.displaytag.decorator.TableDecorator;

/*
 * @author Jos� Luis Arellano
 * @since 18 de agosto de 2009
 */
public class RiesgoCobertura extends TableDecorator {
	
	public String getCodigo(){
		return ((RiesgoCoberturaDTO) getCurrentRowObject()).getRiesgoDTO().getCodigo();
	}
	
	public String getVersion(){
		return ((RiesgoCoberturaDTO) getCurrentRowObject()).getRiesgoDTO().getVersion().toString();
	}
	
	public String getDescripcion(){
		return ((RiesgoCoberturaDTO) getCurrentRowObject()).getRiesgoDTO().getDescripcion();
	}
	
	public String getNombreComercial(){
		return ((RiesgoCoberturaDTO) getCurrentRowObject()).getRiesgoDTO().getNombreComercial();
	}
	
	/*public String getClaveTipoSumaAsegurada(){
		return ((RiesgoCoberturaDTO) getCurrentRowObject()).getRiesgoDTO().getClaveTipoSumaAsegurada();
	}
	
	public String getNumeroSecuencia(){
		return ((RiesgoCoberturaDTO) getCurrentRowObject()).getRiesgoDTO().getNumeroSecuencia().toString();
	}*/
}
