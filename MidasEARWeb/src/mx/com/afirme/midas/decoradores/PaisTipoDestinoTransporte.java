package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.paistipodestinotransporte.PaisTipoDestinoTransporteDTO;
import mx.com.afirme.midas.catalogos.paistipodestinotransporte.TipoDestinoTransporteDTO;

import org.displaytag.decorator.TableDecorator;


public class PaisTipoDestinoTransporte extends TableDecorator {
	
	
	public String getIdPais() {
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = (PaisTipoDestinoTransporteDTO) getCurrentRowObject();
		return paisTipoDestinoTransporteDTO.getIdPais();
	}
	
	public TipoDestinoTransporteDTO getTipoDestinoTransporte() {
		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = (PaisTipoDestinoTransporteDTO) getCurrentRowObject();
		return paisTipoDestinoTransporteDTO.getTipoDestinoTransporte();
	}
	
	
	public String getAcciones() {

		PaisTipoDestinoTransporteDTO paisTipoDestinoTransporteDTO = (PaisTipoDestinoTransporteDTO) getCurrentRowObject();
		String idPais = paisTipoDestinoTransporteDTO.getIdPais();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/paistipodestinotransporte/mostrarDetalle.do?idPais="
				+ idPais
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/paistipodestinotransporte/mostrarBorrar.do?idPais="
				+ idPais
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
