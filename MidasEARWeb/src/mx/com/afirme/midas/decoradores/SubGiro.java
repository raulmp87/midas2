package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.giro.SubGiroDTO;
import org.displaytag.decorator.TableDecorator;

public class SubGiro extends TableDecorator{
	
//	private BigDecimal idTcSubGiro;
//	private BigDecimal descripcionGiro;
//	private GiroDTO giroDTO;
//	private BigDecimal codigoSubGiro;
//	private String descripcionSubGiro;
//	private BigDecimal idGrupoRobo;
//	private BigDecimal idGrupoDineroValores;
//	private BigDecimal idGrupoCristales;
//	private BigDecimal idGrupoPlenos;
//	private Short claveInspeccion;
	
	public String getDescripcionGiro(){
		SubGiroDTO subGiroDTO= (SubGiroDTO) getCurrentRowObject();
		return subGiroDTO.getGiroDTO().getDescripcionGiro();
	}
	
	public String getCodigoSubGiro(){
		SubGiroDTO subGiroDTO= (SubGiroDTO) getCurrentRowObject();
		return subGiroDTO.getCodigoSubGiro().toBigInteger().toString();
	}
	
	public String getIdTcSubGiro(){
		SubGiroDTO subGiroDTO = (SubGiroDTO) getCurrentRowObject();
		return subGiroDTO.getIdTcSubGiro().toBigInteger().toString();
	}
	
	public String getDescripcion(){
		SubGiroDTO subGiroDTO = (SubGiroDTO) getCurrentRowObject();
		return subGiroDTO.getDescripcionSubGiro();
	}
	
	public String getAcciones() {

		SubGiroDTO subGiroDTO = (SubGiroDTO) getCurrentRowObject();
		String idSubgiro = subGiroDTO.getIdTcSubGiro().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subgiro/mostrarDetalle.do?id="
				+ idSubgiro
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subgiro/mostrarModificar.do?id="
				+ idSubgiro
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subgiro/mostrarBorrar.do?id="
				+ idSubgiro
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
