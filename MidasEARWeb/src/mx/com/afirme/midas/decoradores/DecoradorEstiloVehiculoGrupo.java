package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.estilovehiculo.grupo.EstiloVehiculoGrupoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;

public class DecoradorEstiloVehiculoGrupo extends MidasBaseDecorator{

	public String getAcciones() {
		EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = (EstiloVehiculoGrupoDTO) getCurrentRowObject();
//		String idTipoVehiculo= ""+estiloVehiculoDTODTO.getIdTcTipoVehiculo();
		String claveEstilo = estiloVehiculoGrupoDTO.getId().getClaveEstilo();
		String claveTipoBien = estiloVehiculoGrupoDTO.getId().getClaveTipoBien();
		String idVersionCarga = estiloVehiculoGrupoDTO.getId().getIdVersionCarga().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculogrupo/mostrarDetalle.do?claveEstilo="
				+ claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+idVersionCarga
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculogrupo/mostrarModificar.do?claveEstilo="
				+ claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+idVersionCarga
				+ "', 'contenido','manipulaCalendarioLineas()');\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/estilovehiculogrupo/mostrarBorrar.do?claveEstilo="
				+ claveEstilo+"&claveTipoBien="+claveTipoBien+"&idVersionCarga="+idVersionCarga
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
