package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.cotizacion.inspeccion.documento.DocumentoAnexoInspeccionIncisoCotizacionDTO;
import org.displaytag.decorator.TableDecorator;

public class DocumentosAnexosInspeccion extends TableDecorator{

	public String getAcciones() {
		
		DocumentoAnexoInspeccionIncisoCotizacionDTO documentoAnexoInspIncisoCot = (DocumentoAnexoInspeccionIncisoCotizacionDTO) getCurrentRowObject();
		String idToDocAnexoInsp = documentoAnexoInspIncisoCot.getIdToDocumentoAnexoInspeccionIncisoCotizacion().toBigInteger().toString();
		String idControlArchivo = documentoAnexoInspIncisoCot.getIdToControlArchivo().toBigInteger().toString();
		return  "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: Confirma('Desea borrar este Documento Anexo?',null,'/MidasWeb/cotizacion/validar/inspeccion/borrarDocumentoAnexo.do"
				+ "?idToDocAnexoInsp=" + idToDocAnexoInsp
				+ "', 'tablaDocumentosAnexosInspeccion',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
				+ idControlArchivo
				+ "', 'download');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/download-icon.jpg'/></a>";
	}
}