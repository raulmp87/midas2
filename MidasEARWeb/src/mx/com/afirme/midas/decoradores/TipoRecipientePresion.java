package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tiporecipientepresion.TipoRecipientePresionDTO;

import org.displaytag.decorator.TableDecorator;

public class TipoRecipientePresion extends TableDecorator {
	
	public String getCodigoTipoRecPresion() {
		
		TipoRecipientePresionDTO tipoRecipientePresionDTO = (TipoRecipientePresionDTO) getCurrentRowObject();
		String tipoRecipientePresion = tipoRecipientePresionDTO.getCodigoTipoRecipientePresion().toBigInteger().toString();
		return tipoRecipientePresion;
	}
	
	public String getDescripcionTipoRecPresion() {
		
		TipoRecipientePresionDTO tipoRecipientePresionDTO = (TipoRecipientePresionDTO) getCurrentRowObject();
		String tipoRecipientePresion = tipoRecipientePresionDTO.getDescripcionTipoRecPresion();
		return tipoRecipientePresion;
	}
	
	public String getAcciones() {

		TipoRecipientePresionDTO tipoRecipientePresionDTO = (TipoRecipientePresionDTO) getCurrentRowObject();
		String idTipoRecPresion = tipoRecipientePresionDTO.getIdTipoRecipientePresion().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tiporecipientepresion/mostrarDetalle.do?id="
				+ idTipoRecPresion
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tiporecipientepresion/mostrarModificar.do?id="
				+ idTipoRecPresion
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tiporecipientepresion/mostrarBorrar.do?id="
				+ idTipoRecPresion
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
