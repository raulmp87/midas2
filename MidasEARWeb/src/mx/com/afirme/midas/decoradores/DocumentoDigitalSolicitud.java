package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;

import org.displaytag.decorator.TableDecorator;

public class DocumentoDigitalSolicitud extends TableDecorator {
	public String getArchivo() throws SystemException {
		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = (DocumentoDigitalSolicitudDTO) getCurrentRowObject();
		SolicitudDN solicitudDN = SolicitudDN.getInstancia();
		ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
		controlArchivoDTO.setIdToControlArchivo(documentoDigitalSolicitudDTO.getIdToControlArchivo());
		controlArchivoDTO = solicitudDN.obtenerControlArchivoDTO(controlArchivoDTO);
		return controlArchivoDTO.getNombreArchivoOriginal();
	}

	public String getAnexadoPor() {
		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = (DocumentoDigitalSolicitudDTO) getCurrentRowObject();
		return documentoDigitalSolicitudDTO.getNombreUsuarioCreacion();
	}

	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {

		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = (DocumentoDigitalSolicitudDTO) getCurrentRowObject();
		String id = documentoDigitalSolicitudDTO.getIdToDocumentoDigitalSolicitud().toString();
		String idControl = documentoDigitalSolicitudDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el archivo seleccionado?', null,'/MidasWeb/solicitud/borrarDocumento.do?id="
						+ id
						+ "', 'resultados',null);\"><img border='0px' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' title='Descargar' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}
	
	
	public String getAccionesEndoso() {

		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = (DocumentoDigitalSolicitudDTO) getCurrentRowObject();
		String id = documentoDigitalSolicitudDTO.getIdToDocumentoDigitalSolicitud().toString();
		String idControl = documentoDigitalSolicitudDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas borrar el archivo seleccionado?', null,'/MidasWeb/endoso/solicitud/borrarDocumento.do?id="
						+ id
						+ "', 'resultados',null);\"><img border='0px' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		buffer.append("&nbsp;");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' title='Descargar' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}
	
	/**
	 * toma la cadena para mostrar la accion de descargar Documento
	 * 
	 * @return String El texto HTML para la columna de descargar documento
	 */
	public String getDescargar() {

		DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = (DocumentoDigitalSolicitudDTO) getCurrentRowObject();
		String idControl = documentoDigitalSolicitudDTO.getIdToControlArchivo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: window.open('/MidasWeb/sistema/download/descargarArchivo.do?idControlArchivo="
						+ idControl
						+ "', 'download');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");
		buffer.append("</center>");		
		return buffer.toString();
	}

}
