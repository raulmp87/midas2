package mx.com.afirme.midas.decoradores;

import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.solicitud.SolicitudDN;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDN;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;

public class Solicitud extends MidasBaseDecorator{
	public String getIdToSolicitud() {
		SolicitudDTO solicitudDTO = (SolicitudDTO) getCurrentRowObject();
		return "SOL-" + String.format("%08d", new Object[]{solicitudDTO.getIdToSolicitud().intValue()});
	}

	public String getNombreAsegurado() {
		SolicitudDTO solicitudDTO = (SolicitudDTO) getCurrentRowObject();
		String nombre = "";
		if(solicitudDTO.getClaveTipoPersona().shortValue() == 1) {
			nombre += solicitudDTO.getNombrePersona();
			nombre += solicitudDTO.getApellidoPaterno() != null? " " + solicitudDTO.getApellidoPaterno() : "";
			nombre += solicitudDTO.getApellidoMaterno() != null? " " + solicitudDTO.getApellidoMaterno() : "";
		} else {
			nombre += solicitudDTO.getNombrePersona();
		}
		return nombre;
	}

	public String getClaveEstatus() {
		SolicitudDTO solicitudDTO = (SolicitudDTO) getCurrentRowObject();
		String estatus = UtileriasWeb.getDescripcionCatalogoValorFijo(29, solicitudDTO.getClaveEstatus());
		try {
			if(solicitudDTO.getClaveTipoSolicitud() == Sistema.SOLICITUD_TIPO_ENDOSO || solicitudDTO.getEsRenovacion().shortValue() == Sistema.ES_RENOVACION){
				CotizacionDTO cotizacionDTO = SolicitudDN.getInstancia().getResumenSolicitudEndoso(solicitudDTO.getIdToSolicitud());
				if (cotizacionDTO != null && cotizacionDTO.getIdToCotizacion()!= null){
					estatus = UtileriasWeb.getDescripcionCatalogoValorFijo(30, cotizacionDTO.getClaveEstatus());
				}					
			}else{
				CotizacionDTO cotizacionDTO = SolicitudDN.getInstancia().getResumenSolicitud(solicitudDTO.getIdToSolicitud());
				if (cotizacionDTO != null && cotizacionDTO.getIdToCotizacion()!= null && solicitudDTO.getClaveEstatus().shortValue() != Sistema.ESTATUS_SOL_PENDIENTE_AUTORIZACION){
					estatus = UtileriasWeb.getDescripcionCatalogoValorFijo(30, cotizacionDTO.getClaveEstatus());
				}	
			}
		} catch (SystemException e) {}
		return estatus;
	}
	
	public String getTipoSolicitud(){
		SolicitudDTO solicitudDTO = (SolicitudDTO) getCurrentRowObject();
		String descripcion = "";
		if(solicitudDTO.getEsRenovacion() != null && solicitudDTO.getEsRenovacion().shortValue() == Sistema.ES_RENOVACION)
			descripcion = "RENOVACI&Oacute;N-";
		descripcion+= UtileriasWeb.getDescripcionCatalogoValorFijo(47, solicitudDTO.getClaveTipoSolicitud());				
		return descripcion==null ?"":descripcion;
	}
	
	

	/**
	 * toma la cadena para mostrar las acciones Producto
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 * @throws SystemException 
	 * @throws ExcepcionDeAccesoADatos 
	 */
	public String getAcciones() throws ExcepcionDeAccesoADatos, SystemException {
		SolicitudDTO solicitudDTO = (SolicitudDTO) getCurrentRowObject();
		String id = solicitudDTO.getIdToSolicitud().toString();
		StringBuffer buffer = new StringBuffer();
		CotizacionDTO cotizacionDTO = SolicitudDN.getInstancia().getResumenSolicitud(solicitudDTO.getIdToSolicitud());
		

			
		//Se puede consultar independientemente de su estado
		if(solicitudDTO.getClaveTipoSolicitud()==2){
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/endoso/solicitud/mostrarDetalle.do?id="
					+ id
					+ "', 'contenido','activaDatosMotivo();');\"><img border='0px' title='Detalle' alt='Detalle' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a>");
		}else{
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/solicitud/mostrarDetalle.do?id="
					+ id
					+ "', 'contenido',null);\"><img border='0px' title='Detalle' alt='Detalle' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a>");

		}

		buffer.append("&nbsp;");


		//Si est� en proceso, se puede modificar
		if(solicitudDTO.getClaveEstatus().intValue() == 0) {
			if(solicitudDTO.getClaveTipoSolicitud()==2){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/endoso/solicitud/mostrarModificar.do?id="
						+ id
						+ "', 'contenido','mostrarTipoPersonaSolicitud();activaDatosMotivo();');\"><img border='0px' title='Modificar' alt='Modificar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");
				buffer.append("&nbsp;");

			}else{
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/solicitud/mostrarModificar.do?id="
						+ id
						+ "', 'contenido','mostrarTipoPersonaSolicitud();');\"><img border='0px' title='Modificar' alt='Modificar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");
				buffer.append("&nbsp;");
			}
		}
		//TODO Verificar esta acci�n, se defini� la siguiente condici�n para borrar:
		//Si no es una Orden de trabajo, se puede borrar
		if(solicitudDTO.getClaveEstatus().shortValue() != 1 && cotizacionDTO.getIdToCotizacion() == null) {

			if(solicitudDTO.getClaveTipoSolicitud()==2){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/endoso/solicitud/mostrarBorrar.do?id="
						+ id
						+ "', 'contenido',null);\"><img border='0px' title='Borrar' alt='Borrar' src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>");
				buffer.append("&nbsp;");
			}else{
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/solicitud/mostrarBorrar.do?id="
						+ id
						+ "', 'contenido',null);\"><img border='0px' title='Borrar' alt='Borrar' src='/MidasWeb/img/icons/ico_eliminar.gif'/></a>");
				buffer.append("&nbsp;");
			}
		}


		DocumentoDigitalSolicitudDN documentoDigitalSolicitudDN = DocumentoDigitalSolicitudDN.getInstancia();
		List<DocumentoDigitalSolicitudDTO> documentos = documentoDigitalSolicitudDN .listarDocumentosSolicitud(solicitudDTO.getIdToSolicitud());
		if ((solicitudDTO.getClaveEstatus().shortValue() == 0 && !documentos.isEmpty())
				|| (solicitudDTO.getClaveEstatus().shortValue() == 0 
				&& (solicitudDTO.getClaveTipoEndoso() != null 
				&& (solicitudDTO.getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CANCELACION 
				|| solicitudDTO.getClaveTipoEndoso().intValue() == Sistema.SOLICITUD_ENDOSO_DE_CAMBIO_FORMA_PAGO)))) {
			if(solicitudDTO.getClaveTipoSolicitud()==2){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas terminar la solicitud seleccionada?', document.solicitudForm, '/MidasWeb/endoso/solicitud/terminar.do?id="
						+ id
						+ "', 'contenido',null);\"><img border='0px' title='Terminar' alt='Terminar' src='/MidasWeb/img/icons/ico_terminar.gif'/></a>");
				buffer.append("&nbsp;");

			}else{
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: Confirma('�Realmente deseas terminar la solicitud seleccionada?', document.solicitudForm, '/MidasWeb/solicitud/terminar.do?id="
						+ id
						+ "', 'contenido',null);\"><img border='0px' title='Terminar' alt='Terminar' src='/MidasWeb/img/icons/ico_terminar.gif'/></a>");
				buffer.append("&nbsp;");
			}
		}
		else{
			buffer.append(obtenerHTMLImagenBlanco());
		}

		//Si est� terminada, se puede asignar 
		if(solicitudDTO.getClaveEstatus().shortValue() == 2 && cotizacionDTO.getIdToCotizacion() == null) {

			if(solicitudDTO.getClaveTipoSolicitud()==2){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarAsignarSolicitudEndoso("+
						id+")\"><img border='0px' title='Asignar' alt='Asignar' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");
				buffer.append("&nbsp;");

			}else{
				if(solicitudDTO.getIdToPolizaAnterior() != null && solicitudDTO.getIdToPolizaAnterior().intValue() != 0) {
					buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: mostrarAsignarSolicitudCotizacion(" + solicitudDTO.getIdToPolizaAnterior() + "," +
							id
							+ ")\"><img border='0px' title='Asignar' alt='Asignar' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");
				} else {
					buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: mostrarAsignarOrdenTrabajo(''," +
							id
							+ ")\"><img border='0px' title='Asignar' alt='Asignar' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");
				}
				buffer.append("&nbsp;");
			}
		}else if(solicitudDTO.getClaveEstatus().shortValue() == 2 && (solicitudDTO.getEsRenovacion() == Sistema.ES_RENOVACION && cotizacionDTO.getClaveEstatus().shortValue() < Sistema.ESTATUS_COT_ASIGNADA_EMISION)){
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: mostrarAsignarOrdenTrabajo(''," +
					id
					+ ")\"><img border='0px' title='Asignar' alt='Asignar' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");	
			buffer.append("&nbsp;");
		}else{
			buffer.append(obtenerHTMLImagenBlanco());
		}

		if (cotizacionDTO != null && cotizacionDTO.getIdToCotizacion()!= null || solicitudDTO.getClaveEstatus().shortValue() == 1){
			if(solicitudDTO.getClaveTipoSolicitud()==2){
				buffer
				.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarResumenEndoso("
						+ id
						+ ")\"><img border='0px' title='Resumen' alt='Resumen' src='/MidasWeb/img/confirmAll.gif'/></a>");
				buffer.append("&nbsp;");

			}else{

				buffer
				.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarResumen("
						+ id
						+ ")\"><img border='0px' title='Resumen' alt='Resumen' src='/MidasWeb/img/confirmAll.gif'/></a>");
				buffer.append("&nbsp;");	
			}
		}
		else{
			buffer.append(obtenerHTMLImagenBlanco());
		}

		if (cotizacionDTO != null && cotizacionDTO.getIdToCotizacion()!= null && solicitudDTO.getClaveEstatus().shortValue() == Sistema.ESTATUS_SOL_PENDIENTE_AUTORIZACION){

			if(cotizacionDTO.getClaveEstatus().intValue() >= Sistema.ESTATUS_COT_LIBERADA){
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: mostrarAsignarCotizacion(" + cotizacionDTO.getIdToCotizacion()
						+ ",-1);\"><img border='0px' title='Emitir' alt='Emitir' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");					
			}else{
				buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: alert('La Cotizaci\u00f3n debe ser Liberada para poder emitirse.');\"><img border='0px' title='Emitir' alt='Emitir' src='/MidasWeb/img/icons/ico_asignar.gif'/></a>");

			}
			buffer.append("&nbsp;");				
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/solicitud/mostrarModificar.do?id="
					+ id
					+ "', 'contenido','mostrarTipoPersonaSolicitud(); mostrarAccionesSolicitud()');\"><img border='0px' title='Modificar' alt='Modificar' src='/MidasWeb/img/icons/ico_editar.gif'/></a>");
			buffer.append("&nbsp;");	
		}
		else{
			buffer.append(obtenerHTMLImagenBlanco());
		}		
		return buffer.toString();
	}
	

}