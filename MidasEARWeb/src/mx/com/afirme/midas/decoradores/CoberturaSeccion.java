package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

import org.displaytag.decorator.TableDecorator;

public class CoberturaSeccion extends TableDecorator {
	
	public String getClaveObligatoriedad() {
		CoberturaSeccionDTO coberturaSeccion = (CoberturaSeccionDTO) getCurrentRowObject();
		String result;
		if (coberturaSeccion.getClaveObligatoriedad().compareTo(new BigDecimal(0))==0)
			result="no";
		else
			result="si";
		//return coberturaSeccion.getClaveObligatoriedad().toString();
		return result;
	}

	public String getCodigo() {
		CoberturaSeccionDTO coberturaSeccion = (CoberturaSeccionDTO) getCurrentRowObject();
		return coberturaSeccion.getCoberturaDTO().getCodigo();
	}
	
	public String getVersion() {
		CoberturaSeccionDTO coberturaSeccion = (CoberturaSeccionDTO) getCurrentRowObject();
		return coberturaSeccion.getCoberturaDTO().getVersion().toString();
	}
	
	public String getNombreComercial() {
		CoberturaSeccionDTO coberturaSeccion = (CoberturaSeccionDTO) getCurrentRowObject();
		return coberturaSeccion.getCoberturaDTO().getNombreComercial();
	}
	
	public String getDescripcion() {
		CoberturaSeccionDTO coberturaSeccion = (CoberturaSeccionDTO) getCurrentRowObject();
		return coberturaSeccion.getCoberturaDTO().getDescripcion();
	}
}
