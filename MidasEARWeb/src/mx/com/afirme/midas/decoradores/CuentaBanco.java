package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.cuentabanco.CuentaBancoDTO;

import org.displaytag.decorator.TableDecorator;

public class CuentaBanco extends TableDecorator {

	public String getAcciones() {
		CuentaBancoDTO cuentabancoDTO = (CuentaBancoDTO) getCurrentRowObject();
		String idCuentaBanco = cuentabancoDTO.getIdtccuentabanco().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do?id="
				+ idCuentaBanco
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cuentabanco/mostrarModificar.do?id="
				+ idCuentaBanco
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"				
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do?id="
				+ idCuentaBanco
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}