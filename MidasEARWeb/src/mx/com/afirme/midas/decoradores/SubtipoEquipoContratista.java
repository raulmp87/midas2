package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.tipoequipocontratista.SubtipoEquipoContratistaDTO;

import org.displaytag.decorator.TableDecorator;

public class SubtipoEquipoContratista extends TableDecorator {

	
	public String getAcciones(){
		
		SubtipoEquipoContratistaDTO subtipoEquipoContratistaDTO = (SubtipoEquipoContratistaDTO) getCurrentRowObject();
		String idSubtipoEqCont = subtipoEquipoContratistaDTO.getIdSubtipoEquipoContratista().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subtipoequipocontratista/mostrarDetalle.do?id="
				+ idSubtipoEqCont
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subtipoequipocontratista/mostrarModificar.do?id="
				+ idSubtipoEqCont
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subtipoequipocontratista/mostrarBorrar.do?id="
				+ idSubtipoEqCont
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
