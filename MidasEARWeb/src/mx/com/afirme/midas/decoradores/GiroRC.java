package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.girorc.GiroRCDTO;

import org.displaytag.decorator.TableDecorator;

public class GiroRC extends TableDecorator {
	public String getIdTcGiroRC() {
		GiroRCDTO giroRCDTO = (GiroRCDTO) getCurrentRowObject();
		return giroRCDTO.getIdTcGiroRC().toBigInteger().toString();
	}
	
	public String getCodigoGiroRC() {
		GiroRCDTO giroRCDTO = (GiroRCDTO) getCurrentRowObject();
		return giroRCDTO.getCodigoGiroRC().toBigInteger().toString();
	}
	
	public String getDescripcionGiroRC(){
		GiroRCDTO giroRCDTO = (GiroRCDTO) getCurrentRowObject();
		return giroRCDTO.getDescripcionGiroRC();
	}
	
	public String getAcciones() {
		GiroRCDTO giroRCDTO = (GiroRCDTO) getCurrentRowObject();
		String idGiroRC = giroRCDTO.getIdTcGiroRC().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/girorc/mostrarDetalle.do?id="
				+ idGiroRC
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/girorc/mostrarModificar.do?id="
				+ idGiroRC
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/girorc/mostrarBorrar.do?id="
				+ idGiroRC
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
