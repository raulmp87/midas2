package mx.com.afirme.midas.decoradores;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.CotizacionDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.sistema.MidasBaseDecorator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;

public class ImpresionEndososPorPoliza extends MidasBaseDecorator {
	public String getNumeroEndoso(){
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		if (endosoDTO.getId().getNumeroEndoso().intValue() == 0)
			return "Poliza Original";
		else
			return endosoDTO.getId().getNumeroEndoso().toString();
	}
	
	public String getClaveTipoEndoso() throws SystemException{
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		
		BigDecimal idToCotizacion = endosoDTO.getIdToCotizacion();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia("").getPorId(idToCotizacion);
		BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();

		String result = "";
		if(claveTipoEndoso != null && claveTipoEndoso.intValue() > 2) {
			int tipoEndoso = endosoDTO.getClaveTipoEndoso().intValue();
			if (endosoDTO.getId().getNumeroEndoso().intValue() == 0)
				result = "";
			else{
				switch(tipoEndoso){
					case 1:
						result = "Endoso de Cambio";
						break;
					case 2:
						result = "Endoso de Aumento";
						break;
					case 3:
						result = "Endoso de Disminución";
						break;
					case 4:
						result = "Endoso de Disminución";
						break;
					case 5:
						result = "Endoso de Aumento";
						break;
					case 6:
						result = "Endoso de Cambio";
						break;
					case 7:
						result = "Endoso de CE (" + endosoDTO.getCancela().intValue() + ") Gpo. " + endosoDTO.getGrupo().intValue();
						break;
					case 8:
						result = "Endoso de RE (" + endosoDTO.getCancela().intValue() + ")";
						break;
					default:
						result = "No disponible";
						break;
				}
			}
		}
		return result;
	}

	public String getMotivoEndoso() throws SystemException {
		String descripcion = null;
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		BigDecimal idToCotizacion = endosoDTO.getIdToCotizacion();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia("").getPorId(idToCotizacion);
				
		short tipoEndoso = endosoDTO.getClaveTipoEndoso().shortValue();
		if (tipoEndoso == Sistema.TIPO_ENDOSO_CE) {
			if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO) {
				descripcion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.tipo.automatica");
			} else if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_A_PETICION_DEL_ASEGURADO) {
				descripcion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.cancelacion.tipo.manual");
			}
		} else if (tipoEndoso == Sistema.TIPO_ENDOSO_RE) {
			if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_CANCELACION_AUTOMATICA_POR_FALTA_DE_PAGO) {
				descripcion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.rehabilitacion.tipo.automatica");
			} else if (cotizacionDTO.getClaveMotivoEndoso().intValue() == Sistema.MOTIVO_A_PETICION_DEL_ASEGURADO) {
				descripcion = UtileriasWeb.getMensajeRecurso(Sistema.ARCHIVO_RECURSOS, "endoso.rehabilitacion.tipo.manual");
			}
		} 
		
		if(descripcion == null) {
			BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();
			
			if(claveTipoEndoso != null) {
				descripcion = UtileriasWeb.getDescripcionCatalogoValorFijo(40, claveTipoEndoso.intValue());
			}
		}
		
		return descripcion;
	}

	public String getAcciones(){
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center><a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: parent.imprimirEndosoPoliza(" + 
					endosoDTO.getId().getIdToPoliza().toBigInteger().toString() + "," + endosoDTO.getId().getNumeroEndoso()
				+ ")\"><img border='0px' alt='Imprimir Endoso' title='Imprimir Endoso' src='/MidasWeb/img/ico_impresion.gif'/></a></center>");
		return buffer.toString();
	}

	public String getAccionesConsulta() throws SystemException {
		EndosoDTO endosoDTO = (EndosoDTO) getCurrentRowObject();
		
		BigDecimal idToCotizacion = endosoDTO.getIdToCotizacion();
		CotizacionDTO cotizacionDTO = CotizacionDN.getInstancia("").getPorId(idToCotizacion);
		BigDecimal claveTipoEndoso = cotizacionDTO.getSolicitudDTO().getClaveTipoEndoso();
		if(claveTipoEndoso == null) {
			claveTipoEndoso = BigDecimal.ZERO;
		}

		StringBuffer buffer = new StringBuffer();
		buffer.append("<center><a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: parent.consultarEndosoPoliza("
						+ endosoDTO.getIdToCotizacion()
						+ ","
						+ endosoDTO.getId().getIdToPoliza()
						+ ","
						+ endosoDTO.getId().getNumeroEndoso()
						+ ","
						+ claveTipoEndoso.intValue()
						+ ");\"><img border='0px' alt='Ver Cotizacion' title='Ver Cotizacion' src='/MidasWeb/img/icons/ico_verdetalle.gif'/></a></center>");
		return buffer.toString();
	}
}
