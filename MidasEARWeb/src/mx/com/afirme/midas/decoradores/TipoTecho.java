package mx.com.afirme.midas.decoradores;

import org.displaytag.decorator.TableDecorator;
import mx.com.afirme.midas.catalogos.tipotecho.TipoTechoDTO;
public class TipoTecho extends TableDecorator{
	
	public String getCodigoTipoTecho() {
		TipoTechoDTO tipoTechoDTO = (TipoTechoDTO) getCurrentRowObject();
		return tipoTechoDTO.getCodigoTipoTecho().toBigInteger().toString();
	}
	public String getDescripcionTipoTecho() {
		TipoTechoDTO tipoTechoDTO = (TipoTechoDTO) getCurrentRowObject();
		return tipoTechoDTO.getDescripcionTipoTecho();
	}

	public String getAcciones() {
		TipoTechoDTO tipoTechoDTO = (TipoTechoDTO) getCurrentRowObject();
		String idTipoTecho = tipoTechoDTO.getIdTipoTecho().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipotecho/mostrarDetalle.do?id="
				+ idTipoTecho
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipotecho/mostrarModificar.do?id="
				+ idTipoTecho
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/tipotecho/mostrarBorrar.do?id="
				+ idTipoTecho
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
