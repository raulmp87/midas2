package mx.com.afirme.midas.decoradores;


import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;

import org.displaytag.decorator.TableDecorator;

public class Cobertura extends TableDecorator {
	/**
	 * toma la cadena para mostrar las acciones Cobertura
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {

		CoberturaDTO coberturaDTO = (CoberturaDTO) getCurrentRowObject();
		String id = coberturaDTO.getIdToCobertura().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		if(coberturaDTO.getClaveEstatus() == 0 && coberturaDTO.getClaveActivo() == 0) {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cobertura/mostrarDetalle.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		} else {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cobertura/mostrarBorrar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cobertura/mostrarModificar.do?id="
							+ id
							+ "', 'contenido','comboCoberturaSumaAsegurada(" + coberturaDTO.getClaveTipoSumaAsegurada() +"),desabilitaCampos();');\">" 
							+ "<img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cobertura/mostrarDetalle.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
			/*buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
					+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cobertura/listarDocumentosAnexos.do?id="
					+ id
					+ "', 'contenido','mostrarDocumentosCoberturaGrid("+id+")');\"><img border='0px' alt='Detalle' src='/MidasWeb/img/b_refrescar.gif'/></a>");*/
		}
		buffer.append("</center>");		
		return buffer.toString();
	}
	
	/**
	 * toma la cadena para mostrar la accion mostrarDetalle de la Cobertura
	 * 
	 * @return String El texto HTML para la columna de detalle producto
	 */
	public String getDetalle() {
		CoberturaDTO coberturaDTO = (CoberturaDTO) getCurrentRowObject();
		String id = coberturaDTO.getIdToCobertura().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		buffer.append("<a href=\"javascript: void(0);\" "
						+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/cobertura/mostrarDetalle.do?id="
						+ id
						+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		buffer.append("</center>");
		return buffer.toString();
	}

}
