package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import org.displaytag.decorator.TableDecorator;

public class Subramo extends TableDecorator{
	
	
	/**
	 * 
	 * private BigDecimal idtcsubramo;
     private RamoDTO ramoDTO;
     private BigDecimal idemision;
     private BigDecimal idtecnico;
     private String descripcion;
     private Set<LineaDTO> lineaDTOs = new HashSet<LineaDTO>(0);
     
	 */
	
	public String getDescripcionRamo(){
		SubRamoDTO subRamoDTO= (SubRamoDTO) getCurrentRowObject();
		return subRamoDTO.getRamoDTO().getDescripcion();
	}
	
	public String getCodigo(){
		SubRamoDTO subRamoDTO= (SubRamoDTO) getCurrentRowObject();
		return subRamoDTO.getCodigoSubRamo().toBigInteger().toString();
	}
	
	public String getIdTcSubRamo(){
		SubRamoDTO subRamoDTO = (SubRamoDTO) getCurrentRowObject();
		return subRamoDTO.getIdTcSubRamo().toBigInteger().toString();
	}
	
	public String getDescripcion(){
		SubRamoDTO subRamoDTO = (SubRamoDTO) getCurrentRowObject();
		return subRamoDTO.getDescripcionSubRamo();
	}
	
	public String getAcciones() {

		SubRamoDTO subRamoDTO = (SubRamoDTO) getCurrentRowObject();
		String idSubramo = subRamoDTO.getIdTcSubRamo().toBigInteger().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subramo/mostrarDetalle.do?id="
				+ idSubramo
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subramo/mostrarModificar.do?id="
				+ idSubramo
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/subramo/mostrarBorrar.do?id="
				+ idSubramo
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
