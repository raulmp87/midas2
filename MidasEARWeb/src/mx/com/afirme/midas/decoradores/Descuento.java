package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;

import org.displaytag.decorator.TableDecorator;

public class Descuento extends TableDecorator {

	public String getClaveTipo() {
		DescuentoDTO descuento = (DescuentoDTO) getCurrentRowObject();
		String claveDescuento = descuento.getClaveTipo().toString();
		return claveDescuento.equals("1")?"PORCENTAJE":"IMPORTE";
	}

	public String getDescripcion() {
		DescuentoDTO descuento = (DescuentoDTO) getCurrentRowObject();
		return descuento.getDescripcion();
	}

	public String getAcciones() {

		DescuentoDTO descuento = (DescuentoDTO) getCurrentRowObject();
		String idDescuento = descuento.getIdToDescuentoVario().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/descuento/mostrarDetalle.do?id="
				+ idDescuento
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/descuento/mostrarModificar.do?id="
				+ idDescuento
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/descuento/mostrarBorrar.do?id="
				+ idDescuento
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}

}
