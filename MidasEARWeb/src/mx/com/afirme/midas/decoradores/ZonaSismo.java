package mx.com.afirme.midas.decoradores;

import org.displaytag.decorator.TableDecorator;

import mx.com.afirme.midas.catalogos.zonasismo.ZonaSismoDTO;

public class ZonaSismo extends TableDecorator{

	public String getAcciones() {
		ZonaSismoDTO zonaSismoDTO = (ZonaSismoDTO) getCurrentRowObject();
		String idZonaSismo = zonaSismoDTO.getIdZonaSismo().toString();

		return "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/mostrarDetalle.do?id="
				+ idZonaSismo
				+ "', 'contenido',null);\"><img border='0px' alt='Detalle' src='/MidasWeb/img/details.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/mostrarModificar.do?id="
				+ idZonaSismo
				+ "', 'contenido',null);\"><img border='0px' alt='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>"
				+ "<a href=\"javascript: void(0);\" "
				+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/zonasismo/mostrarBorrar.do?id="
				+ idZonaSismo
				+ "', 'contenido',null);\"><img border='0px' alt='Borrar' src='/MidasWeb/img/delete14.gif'/></a>";
	}
}
