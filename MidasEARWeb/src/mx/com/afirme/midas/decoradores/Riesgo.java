package mx.com.afirme.midas.decoradores;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;

import org.displaytag.decorator.TableDecorator;

public class Riesgo extends TableDecorator {
	
	public String getCodigo(){
		return ((RiesgoDTO) getCurrentRowObject()).getCodigo();
	}
	
	public String getVersion(){
		return ((RiesgoDTO) getCurrentRowObject()).getVersion().toString();
	}
	
	public String getNombreComercial(){
		return ((RiesgoDTO) getCurrentRowObject()).getNombreComercial();
	}
	
	public String getClaveActivo() {
		int activo = ((RiesgoDTO) getCurrentRowObject()).getClaveActivo();
		String descripcionActivo;
		if (activo == 1) {
			descripcionActivo = "ACTIVO";
		}
		else {
			descripcionActivo = "INACTIVO";
		}
		return descripcionActivo;
	}
	
	public String getClaveEstatus() {
		int estatus = ((RiesgoDTO) getCurrentRowObject()).getClaveEstatus();
		String descripcionEstatus = "";
		if (estatus == 0) {
			descripcionEstatus = "CREADO";
		}
		else if(estatus == 1) {
			descripcionEstatus = "ACTIVO";
		}
		else if(estatus == 2) {
			descripcionEstatus = "INACTIVO";
		}
		else if(estatus == 3) {
			descripcionEstatus = "BORRADO";
		}
		return descripcionEstatus;
	}
	/**
	 * toma la cadena para mostrar las acciones Riesgo
	 * 
	 * @return String El texto HTML para la columna de acciones producto
	 */
	public String getAcciones() {
		RiesgoDTO riesgoDTO = (RiesgoDTO) getCurrentRowObject();
		String id = riesgoDTO.getIdToRiesgo().toString();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<center>");
		if(riesgoDTO.getClaveEstatus() == 3) {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/riesgo/mostrarDetalle.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
		} else {
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/riesgo/mostrarDetalle.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Detalle' title='Detalle' src='/MidasWeb/img/details.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/riesgo/mostrarModificar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Modificar' title='Modificar' src='/MidasWeb/img/Edit14.gif'/></a>");
			buffer.append("&nbsp;");
			buffer.append("<a href=\"javascript: void(0);\" "
							+ "onclick=\"javascript: sendRequest(null,'/MidasWeb/catalogos/riesgo/mostrarBorrar.do?id="
							+ id
							+ "', 'contenido',null);\"><img border='0px' alt='Borrar' title='Borrar' src='/MidasWeb/img/delete14.gif'/></a>");
		}
		buffer.append("</center>");		
		return buffer.toString();
	}
}