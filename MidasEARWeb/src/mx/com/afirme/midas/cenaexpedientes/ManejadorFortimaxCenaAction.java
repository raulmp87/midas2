package mx.com.afirme.midas.cenaexpedientes;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.MidasMappingDispatchAction;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAAnexos;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;
import mx.com.afirme.midas2.service.negocio.antecedentes.NegocioAntecedentesService;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ManejadorFortimaxCenaAction extends MidasMappingDispatchAction {
	
	private FortimaxV2Service fortimaxV2Service;
	private NegocioAntecedentesService antecedentesService;
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	private String gavetaCena;
	private String carpetaCena; 
	
	public ManejadorFortimaxCenaAction() throws SystemException {
		try {
			gavetaCena = ""; // Gaveta Fmx
			carpetaCena = ""; // Ruta/Folder Fmx
			ServiceLocator serviceLocator = ServiceLocator.getInstance();
			fortimaxV2Service = serviceLocator.getEJB(FortimaxV2Service.class);
			antecedentesService = serviceLocator.getEJB(NegocioAntecedentesService.class);
			parametroGeneralFacade = serviceLocator.getEJB(ParametroGeneralFacadeRemote.class);
		} catch (Exception e) {
			throw new SystemException(Sistema.NO_DISPONIBLE);
		}
		LogDeMidasWeb.log("bean Remoto instanciado", Level.FINEST, null);
	}
    
	public void crearExpedienteFortimax(String fortimaxIDFile) {
		String[] fieldValues = new String[2];
		fieldValues[0]=fortimaxIDFile;
		fieldValues[1]="CENAEXPEDIENTES";
		
		String[] strReturns;
		try {
			strReturns = fortimaxV2Service.generateExpedient(getGavetaCena(), fieldValues);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		if (strReturns[0].equals("false")) {
			throw new RuntimeException("No se pudo crear el expediente en Fortimax para el Documento Anexo.");
		}
	}
	
	public void cargarArchivoFortimax(String fileName, String fortimaxIDFile, TransporteImpresionDTO transporteImpresionDTO){
		try {
			fortimaxV2Service.uploadFile(fileName, transporteImpresionDTO, getGavetaCena(), new String[]{fortimaxIDFile}, getCarpetaCena());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	 public void descargarArchivoFortimax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String fortimaxDocName = request.getParameter("fortimaxIDFile");
		NegocioCEAAnexos archivoCENA = new NegocioCEAAnexos();
		archivoCENA = buscarDocumentoCENA(fortimaxDocName);

		byte byteArray[] = fortimaxV2Service.downloadFileBP(fortimaxDocName, "D", archivoCENA.getFortimaxDocGaveta(), Long.valueOf(archivoCENA.getFortimaxDocId()));
		response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(archivoCENA.getNombre(), "ISO-8859-1"));
		response.setContentType("application/unknown");
		response.setContentLength(byteArray.length);
		response.setBufferSize(1024 * 15);
		
		OutputStream outputStream = response.getOutputStream();
		outputStream.write(byteArray);
		outputStream.flush();
		outputStream.close();
	}
	
	public byte[] descargaBytesArchivoCenaFortimax(String fortimaxDocName){
		try {
			NegocioCEAAnexos archivoCENA = new NegocioCEAAnexos();
			archivoCENA = buscarDocumentoCENA(fortimaxDocName);
			byte byteArray[] = fortimaxV2Service.downloadFileBP(fortimaxDocName, "D", archivoCENA.getFortimaxDocGaveta(), Long.valueOf(archivoCENA.getFortimaxDocId()));
            return byteArray;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public NegocioCEAAnexos buscarDocumentoCENA(String fortimaxDocName) throws SQLException, Exception{
		NegocioCEAAnexos archivoCENA = new NegocioCEAAnexos();
		List<NegocioCEAAnexos> listado = new ArrayList<NegocioCEAAnexos>();
		listado = antecedentesService.findAnexosByFmxDocName(fortimaxDocName);
		archivoCENA = (NegocioCEAAnexos)listado.get(0);
		return archivoCENA;
	}
	
	public void guardarCENA(NegocioCEAAnexos negocioCEAAnexos){
		antecedentesService.saveAnexo(negocioCEAAnexos);
	}
	
    public FortimaxV2Service getFortimaxV2Service() {
        return this.fortimaxV2Service;
    }
    public void setFortimaxV2Service(FortimaxV2Service fortimaxV2Service) {
        this.fortimaxV2Service = fortimaxV2Service;
    }
    
    public NegocioAntecedentesService getAntecedentesService() {
		return antecedentesService;
	}
	public void setAntecedentesService(
			NegocioAntecedentesService antecedentesService) {
		this.antecedentesService = antecedentesService;
	}

	public String getGavetaCena() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_FORTIMAX,
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CENA_GAVETA);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}
	public void setGavetaCena(String gavetaCena) {
		this.gavetaCena = gavetaCena;
	}

	public String getCarpetaCena() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_FORTIMAX,
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CENA_CARPETA);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}
	public void setCarpetaCena(String carpetaCena) {
		this.carpetaCena = carpetaCena;
	}
	
	public String getGavetaCenaFortimax() {
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAMETRO_ESTRUCTURA_FORTIMAX,
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_CENA_GAVETA_NEGOCIO_AUTOS);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		return parametro.getValor();
	}
	
}
