package mx.com.afirme.vida.action.asistencia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVida;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVida.AsistenciaVidaEstatus;
import mx.com.afirme.vida.domain.asistencia.AsistenciaVidaDetalle;
import mx.com.afirme.vida.dto.asistencia.AsistenciaVidaFiltro;
import mx.com.afirme.vida.service.asistencia.AsistenciaVidaService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/vida/asistencia")
@Component
@Scope("prototype")
public class AsistenciaVidaAction extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6919463219563424947L;

	
	private static final String MOSTARCONTENEDOR ="/jsp/emision/vida/asistencia/contenedor.jsp";
	private static final String MOSTARCONTENEDORGRID ="/jsp/emision/vida/asistencia/envioProveedorGrid.jsp";
	private List<AsistenciaVida> listaEnvios = new ArrayList<AsistenciaVida>();
	private AsistenciaVidaFiltro filtroEnvioProveedor = new AsistenciaVidaFiltro();
	private Long idBatch;
	private List<AsistenciaVidaEstatus> estatus;
	private TransporteImpresionDTO transporte;
	private AsistenciaVida envioProveedor;
	
	@Autowired
	@Qualifier("AsistenciaVidaServiceEJB")
	private AsistenciaVidaService asistenciaVidaService;
	
	@Override
	public void prepare(){
		estatus = new ArrayList<AsistenciaVidaEstatus>(Arrays.asList(AsistenciaVidaEstatus.values()));
	}
	
	@Action(value = "enviarProveedor")	
	public String enviarProveedor(){
		envioProveedor = asistenciaVidaService.findById(idBatch);
		asistenciaVidaService.guardaArchivo(envioProveedor);
		asistenciaVidaService.setEnviado(envioProveedor);
		return NONE;
	}
	
	@Action(value = "buscarEnvioProveedor", results = { @Result(name = SUCCESS, location = MOSTARCONTENEDORGRID) })	
	public String buscarEnvioProveedor(){
		listaEnvios = asistenciaVidaService.findByFilters(filtroEnvioProveedor);
		return SUCCESS;
	}	

	//Accion que cargar la primera pantalla del Catalogo de oficina
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDOR),
			@Result(name=INPUT,location=MOSTARCONTENEDOR)
			})
	public String mostrarContenedor(){
		return SUCCESS;
	}	
	
	@Action(value="gridEnvioProveedor",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDORGRID),
			@Result(name=INPUT,location=MOSTARCONTENEDORGRID)
			})
	public String gridEnvioProveedor(){
		listaEnvios = asistenciaVidaService.findAll();
		return SUCCESS;
	}
	
	@Action(value = "exportarEnvioProveedor", results = {
			@Result(name = SUCCESS, 
					type = "stream", 
					params = { 
						"contentType","${transporte.contentType}", 
						"contentDisposition", "attachment;filename=\"${transporte.fileName}\"", 
						"inputName","transporte.genericInputStream" 
					}),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
					
	public String exportarEnvioProveedor(){
		listaEnvios = asistenciaVidaService.findByFilters(filtroEnvioProveedor);
		ExcelExporter exporter = new ExcelExporter(AsistenciaVida.class);
		transporte = exporter.exportXLS(listaEnvios, "EnvioProveedor");
		
		return SUCCESS;
	}

	@Action(value = "exportarEnvioProveedorDetalle", results = {
			@Result(name = SUCCESS, 
					type = "stream", 
					params = { 
						"contentType","${transporte.contentType}", 
						"contentDisposition", "attachment;filename=\"${transporte.fileName}\"", 
						"inputName","transporte.genericInputStream" 
					}),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
					
	public String exportarEnvioProveedorDetalle(){
		setEnvioProveedor(asistenciaVidaService.findById(idBatch));
		ExcelExporter exporter = new ExcelExporter(AsistenciaVidaDetalle.class);
		transporte = exporter.exportXLS(getEnvioProveedor().getAsegurados(), getEnvioProveedor().getNombreArchivo());
		
		return SUCCESS;
	}	

	@Action(value = "exportarEnvioProveedorDetalleCons", results = {
			@Result(name = SUCCESS, 
					type = "stream", 
					params = { 
						"contentType","${transporte.contentType}", 
						"contentDisposition", "attachment;filename=\"${transporte.fileName}\"", 
						"inputName","transporte.genericInputStream" 
					}),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
					
	public String exportarEnvioProveedorDetalleCons(){
		listaEnvios = asistenciaVidaService.findByFilters(filtroEnvioProveedor);
		List<AsistenciaVidaDetalle> envioAsegurados = new ArrayList<AsistenciaVidaDetalle>();; 
		
		for(int i = 0; i < listaEnvios.size(); i++){
			envioAsegurados.addAll(listaEnvios.get(i).getAsegurados());
		}
		
		ExcelExporter exporter = new ExcelExporter(AsistenciaVidaDetalle.class);
		transporte = exporter.exportXLS(envioAsegurados, "EnviosProveedorDetalle");
		
		return SUCCESS;
	}	
		
	
	public Long getIdBatch() {
		return idBatch;
	}

	public void setIdBatch(Long idBatch) {
		this.idBatch = idBatch;
	}

	public List<AsistenciaVida> getListaEnvios() {
		return listaEnvios;
	}

	public void setListaEnvios(List<AsistenciaVida> listaEnvios) {
		this.listaEnvios = listaEnvios;
	}

	public AsistenciaVidaFiltro getFiltroEnvioProveedor() {
		return filtroEnvioProveedor;
	}

	public void setFiltroEnvioProveedor(AsistenciaVidaFiltro filtroEnvioProveedor) {
		this.filtroEnvioProveedor = filtroEnvioProveedor;
	}

	public List<AsistenciaVidaEstatus> getEstatus() {
		return estatus;
	}

	public void setEstatus(List<AsistenciaVidaEstatus> estatus) {
		this.estatus = estatus;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public void setEnvioProveedor(AsistenciaVida envioProveedor) {
		this.envioProveedor = envioProveedor;
	}

	public AsistenciaVida getEnvioProveedor() {
		return envioProveedor;
	}

}
