package mx.com.afirme.vida.action.catalogos.tarifa.tarifaMovilVida;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
//import mx.com.afirme.midas2.domain.movil.cotizador.TarifasMovilVidaDTO;
import mx.com.afirme.vida.domain.movil.cotizador.TarifasMovilVidaDTO;
import mx.com.afirme.vida.service.tarifa.TarifaMovilVidaService;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.excels.CargarExcelDescuentoAgenteMovil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.tarifa.DescuentoAgenteService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.DateUtils.Indicador;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/configuracion/vida")
@Component
@Scope("prototype")
public class TarifaMovilVidaAction extends CatalogoHistoricoAction implements
		Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4814866545981130881L;
	@Autowired
	private TarifaMovilVidaService tarifaMovilVidaService;
	private final String NAMESPACE = "/tarifa/configuracion/vida";
	/*** Components of view ****************************************/
	private TarifasMovilVidaDTO tarifasMovilVida;
	private List<TarifasMovilVidaDTO> listaTarifasMovilVida = new ArrayList<TarifasMovilVidaDTO>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion;
	private EntidadService entidadService;
	/* carga masiva */
	private List<String> lista = new ArrayList<String>();
	private int registrosInsertados;
	private int registrosNoInsertados;
	private String logErrores = "";
	private String tabActiva;
	/**/
	File fileUpload;
	@EJB
	private AgenteMidasService agenteMidasService;

	@Override
	public void prepare() throws Exception {
		if (tarifasMovilVida != null && "1".equals(tipoAccion)) {
			tarifasMovilVida = new TarifasMovilVidaDTO();
		}
	}

	public String mostrarTarifaMovilVida() {
		return SUCCESS;
	}
	////descuentoAgente
	public String verCatalogoDescuentoAgenteMovil(){
		this.setTabActiva("descuento_agente_movil");
		return SUCCESS;
	}
	public void prepareCargarDescuentoAgente(){
	}
	
	@Action(value = "cargaMasiva", results = { @Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "respuestaCargaMasivaAgente",
			"namespace",NAMESPACE,// "mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}",
			"registrosInsertados", "${registrosInsertados}",
			"registrosNoInsertados", "${registrosNoInsertados}", "logErrores",
			"${logErrores}" }) })
	public String cargaMasiva() {
		try {
			lista = tarifaMovilVidaService.cargaMasiva(getFileUpload()
					.toString());
			registrosInsertados = Integer.parseInt(lista.get(lista.size() - 2));
			registrosNoInsertados = Integer
					.parseInt(lista.get(lista.size() - 1));
			if (lista.size() >= 3) {
				logErrores = obtenerLogErrores(lista);
				
			}
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setMensaje("Acción realizada correctamente");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lista.clear();
		return SUCCESS;
	}

	public String obtenerLogErrores(List<String> list) {
		StringBuilder logerror = new StringBuilder(logErrores);
		for (int i = 0; i < list.size() - 2; i++) {
			logerror.append("<br>").append(list.get(i));
		}
		return logerror.toString();
	}

	@Action(value = "respuestaCargaMasivaAgente", results = { @Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/respuestaCargaMasivaTarifaMovilVidal.jsp") })
	public String respuestaCargaMasivaAgente() {
		return SUCCESS;
	}

	public void prepareMostrarContenedorPrincipal() {
	}

	@Action(value = "mostrarContenedorPrincipal", results = { @Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/contenedor.jsp")

	})
	public String mostrarContenedorPrincipal() {
		tarifasMovilVida = new TarifasMovilVidaDTO();
		return SUCCESS;
	}
	
	public void prepareMostrarContenedor() {
	}
		
	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/mostrar.jsp")
	})
	public String mostrarContenedor() {
		tarifasMovilVida = new TarifasMovilVidaDTO();
		return SUCCESS;
	}
	
	public void prepareGuardar() {
		try {
			if (tarifasMovilVida != null
					&& tarifasMovilVida.getIdTarifaMovilVida() != null) {
				TarifasMovilVidaDTO tarifa;
				tarifa = tarifaMovilVidaService.findById(tarifasMovilVida
						.getIdTarifaMovilVida());
				// for(TarifasMovilVidaDTO descuento:list){
				if (tarifa != null)
					tarifasMovilVida = tarifa;
				// }
				setIdTipoOperacion(20L);
				setIdRegistro(tarifasMovilVida.getIdTarifaMovilVida());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"tarifasMovilVida.idTarifaMovilVida",
					"${tarifasMovilVida.idTarifaMovilVida}",
					"idTipoOperacion", "${idTipoOperacion}", "idRegistro",
					"${tarifasMovilVida.idTarifaMovilVida}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}", "idTipoOperacion",
					"${idTipoOperacion}", "idRegistro",
					"${tarifasMovilVida.idTarifaMovilVida}" }) })
	@Override
	public String guardar() {
		try {			
			//boolean edadMinima=tarifaMovilVidaService.existeTarifaMovilVida("edadMinima",tarifasMovilVida.getEdadMinima());
			//boolean edadMaxima=tarifaMovilVidaService.existeTarifaMovilVida("edadMaxima",tarifasMovilVida.getEdadMaxima());
			//boolean sumaAsegurada=tarifaMovilVidaService.existeTarifaMovilVida("sumaAsegurada",tarifasMovilVida.getSumaAsegurada());
		if(tarifasMovilVida.getIdTarifaMovilVida()!=null){
			if(tarifasMovilVida.getEdadMaxima()>tarifasMovilVida.getEdadMinima()){
				result=tarifaMovilVidaService.update(tarifasMovilVida);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
				setMensaje("Acción realizada correctamente");
			}else if(tarifasMovilVida.getEdadMaxima()<tarifasMovilVida.getEdadMinima()){
				setMensaje("Error La edad Maxima no puede ser menor que la edad Minima ");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}else{
				setMensaje("Error La edad Minima no puede ser mayor que la edad Maxima ");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
		}else{
			if(tarifasMovilVida.getEdadMaxima()>tarifasMovilVida.getEdadMinima()){
				tarifasMovilVida=tarifaMovilVidaService.save(tarifasMovilVida);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
				setMensaje("Acción realizada correctamente");
			}else if(tarifasMovilVida.getEdadMaxima()<tarifasMovilVida.getEdadMinima()){
				setMensaje("Error La edad Maxima no puede ser menor que la edad Minima ");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}else{
				setMensaje("Error La edad Minima no puede ser mayor que la edad Maxima ");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
		}
					
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return SUCCESS;
	}

   public void validateGuardar() {
		String domainObjectPrefix = "tarifasMovilVida";
		addErrors(tarifasMovilVida, NewItemChecks.class, this,domainObjectPrefix);
	}

	@Action(value = "eliminar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"tarifasMovilVida.idTarifaMovilVida",
					"${tarifasMovilVida.idTarifaMovilVida}",
					"idTipoOperacion", "${idTipoOperacion}", "idRegistro",
					"${tarifasMovilVida.idTarifaMovilVida}" }),
			@Result(name = INPUT, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/detalle.jsp") })
	@Override
	public String eliminar() {
		try {
			if (tarifasMovilVida.getIdTarifaMovilVida() != null) {
				setIdTipoOperacion(20L);
				tarifasMovilVida.setBajalogica(Short.valueOf(String.valueOf(Sistema.NO_SELECCIONADO)));
				
				tarifaMovilVidaService.update(tarifasMovilVida);
				guardarHistorico(TipoOperacionHistorial.GERENCIA,
						tarifasMovilVida.getIdTarifaMovilVida(),
						"midas.gerencia.historial.accion",
						TipoAccionHistorial.BAJA);
				onSuccess();
			} else {
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("Es necesario completar la dirección para eliminar la gerencia");
				setTipoAccion("3");
			}
		} catch (Exception e) {
			tarifasMovilVida.setBajalogica(Short.valueOf(String.valueOf(Sistema.SELECCIONADO)));
			setMensajeError(e.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "lista", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/gerenciaGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/gerenciaGrid.jsp") })
	@Override
	public String listar() {
		return SUCCESS;
	}

	@Action(value = "listarFiltrado", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/tarifaMovilVidaGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/tarifaMovilVidaGrid.jsp") })
	@Override
	public String listarFiltrado() {
		try {
			if ("consulta".equals(tipoAccion)) {
				if (tarifasMovilVida == null) {
					tarifasMovilVida = new TarifasMovilVidaDTO();
				}
			}
			if (tarifasMovilVida != null) {
				listaTarifasMovilVida = tarifaMovilVidaService
						.findByFilters(tarifasMovilVida);
			} else {
				listaTarifasMovilVida = tarifaMovilVidaService.findAll();
			}

		} catch (Exception e) {
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "verDetalle", results = { @Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/detalle.jsp") })
	@Override
	public String verDetalle() {
		if (tarifasMovilVida != null) {
			if (tarifasMovilVida.getIdTarifaMovilVida() != null) {
				tarifasMovilVida = tarifaMovilVidaService
						.findById(tarifasMovilVida.getIdTarifaMovilVida());
			}
		}
		return SUCCESS;
	}

	@Action(value = "verDetalleHistorico", results = { @Result(name = SUCCESS, location = "/jsp/emision/vida/configuracion/tarifa/tarifaMovil/detalle.jsp") })
	public String verDetalleHistorico() {
		if (tarifasMovilVida != null) {
			if (tarifasMovilVida.getIdTarifaMovilVida() != null) {
				// tarifasMovilVida =
				// gerenciaJpaService.loadById(tarifasMovilVida,
				// this.getUltimaModificacion().getFechaHoraActualizacionString());
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion()
							.getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}

	@Action(value = "findById", results = {
			@Result(name = SUCCESS, type = "json", params = { "noCache",
					"true", "ignoreHierarchy", "false", "includeProperties",
					"^gerencia\\.id,^gerencia\\.descripcion",
					"excludeProperties",
					"^gerencia\\.ejecutivos.*,^gerencia\\.personaResponsable.*" }),
			@Result(name = INPUT, type = "json", params = { "noCache", "true",
					"ignoreHierarchy", "false", "includeProperties",
					"^gerencia\\.id,^gerencia\\.descripcion" }) })
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}

	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public TarifasMovilVidaDTO getTarifasMovilVida() {
		return tarifasMovilVida;
	}

	public void setTarifasMovilVida(TarifasMovilVidaDTO tarifasMovilVida) {
		this.tarifasMovilVida = tarifasMovilVida;
	}

	public List<TarifasMovilVidaDTO> getListaTarifasMovilVida() {
		return listaTarifasMovilVida;
	}

	public void setListaTarifasMovilVida(
			List<TarifasMovilVidaDTO> listaTarifasMovilVida) {
		this.listaTarifasMovilVida = listaTarifasMovilVida;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public List<String> getLista() {
		return lista;
	}

	public void setLista(List<String> lista) {
		this.lista = lista;
	}

	public int getRegistrosInsertados() {
		return registrosInsertados;
	}

	public void setRegistrosInsertados(int registrosInsertados) {
		this.registrosInsertados = registrosInsertados;
	}

	public int getRegistrosNoInsertados() {
		return registrosNoInsertados;
	}

	public void setRegistrosNoInsertados(int registrosNoInsertados) {
		this.registrosNoInsertados = registrosNoInsertados;
	}

	public String getLogErrores() {
		return logErrores;
	}

	public void setLogErrores(String logErrores) {
		this.logErrores = logErrores;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

}
