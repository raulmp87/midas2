package mx.com.afirme.midas2.timer.movil.ajustador;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestro;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.log4j.Logger;

import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;

@Singleton
@Startup
public class NotificarReportesSinConfirmarEnterado {

	
	@EJB
	private AjustadorMovilService ajustadorMovilService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private SistemaContext sistemaContext;	
	private static final Logger log = Logger.getLogger(NotificarReportesSinConfirmarEnterado.class);
	
	@Resource
	private TimerService timerService;
	
	private final long interval = 10 * 60 * 1000;
	private TimerConfig timerConfig;

	private final String skipTimeRangeStart = "1:50";
	private final String skipTimeRangeEnd = "3:00";
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
		
	@PostConstruct
	public void start() {
        timerConfig = new TimerConfig();
        timerConfig.setPersistent(false);
        if(sistemaContext.getTimerActivo()) {
        	log.info("Configurando TimerNotificarReporteSinConfirmarEnterado");
//        timerService.createSingleActionTimer(interval, timerConfig);
        }
	}
	
	
	@Timeout
	public void execute() {
		//Debido a que cuando se hacen reinicios automaticos por la noche se quedan colgadas transacciones con ASM
		//que provocan que despues no se pueda levantar la aplicacion de Midas para esto durante el tiempo del reinicio
		//se suspende.
		if (!DateUtils.isHourInInterval(DateUtils.getCurrentHour(), skipTimeRangeStart, skipTimeRangeEnd)) {
			try {
				List<AjustadorMovil> ajustadoresMoviles = ajustadorMovilService.getAjustadoresMoviles();
				for (AjustadorMovil ajustadorMovil : ajustadoresMoviles) {
					List<ReporteSiniestro> reporteSiniestrosAsignadosSinConfirmarEnterado = ajustadorMovilService
							.getReporteSiniestrosAsignadosSinConfirmarEnterado(ajustadorMovil.getIdSeycos().toString());
					int totalSinConfirmar = reporteSiniestrosAsignadosSinConfirmarEnterado.size();
					if (totalSinConfirmar > 0) {
						List<String> registrationIds = usuarioService.getRegistrationIds(ajustadorMovil.getCodigoUsuario(), sistemaContext.getAjustadorMovilDeviceApplicationId());
						if (!registrationIds.isEmpty()) {
							Sender sender = new Sender(sistemaContext.getAjustadorMovilSenderKey());
							Builder b = new Builder();
							b.addData("evento", "reportesSinConfirmarEnterado")
								.addData("message", "Tiene " + totalSinConfirmar + " reportes sin confirmar.")
								.addData("collapse_key", "reportesSinConfirmarEnterado");
							try {
								log.debug("Enviando notificacion a " + ajustadorMovil.getCodigoUsuario() 
										+ " de reportes pendientes sin confirmar. RegistrationIds: " + registrationIds);
								sender.send(b.build(), registrationIds, 0);
							} catch (IOException e) {
								log.error("No fue posible enviar notificacion de reportes sin confirmar.", e);
							}
						}
					}
				}
			}catch(Throwable e) {
				log.error("Ocurrio un el error al intentar notificar.", e);
			}
		}
		if(sistemaContext.getTimerActivo()) {
			timerService.createSingleActionTimer(interval, timerConfig);
		}
	}
	
	public static class DateUtils {

	    // format 24hre ex. 12:12 , 17:15
	    private static String  HOUR_FORMAT = "HH:mm";

	    private DateUtils() {    }
	    

	    public static String getCurrentHour() {
	        Calendar cal = Calendar.getInstance();
	        return getHour(cal.getTime());
	    }
	    
	    public static String getHour(Date date) {
	        SimpleDateFormat sdfHour = new SimpleDateFormat(HOUR_FORMAT);
	        String hour = sdfHour.format(date);
	    	return hour;
	    }

	    /**
	     * @param  target  hour to check
	     * @param  start   interval start
	     * @param  end     interval end
	     * @return true    true if the given hour is between
	     */
	    public static boolean isHourInInterval(String target, String start, String end) {
	        return ((target.compareTo(start) >= 0)
	                && (target.compareTo(end) <= 0));
	    }

	    /**
	     * @param  start   interval start
	     * @param  end     interval end
	     * @return true    true if the current hour is between
	     */
	    public static boolean isNowInInterval(String start, String end) {
	        return DateUtils.isHourInInterval
	            (DateUtils.getCurrentHour(), start, end);
	    }

	}
}
