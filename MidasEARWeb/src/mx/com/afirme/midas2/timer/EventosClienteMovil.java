package mx.com.afirme.midas2.timer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster;
import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster.Message;
import mx.com.afirme.midas2.domain.movil.cliente.NotificacionCliente;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.cometd.bayeux.server.BayeuxServer;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;
@Stateless
@Singleton
@Startup
public class EventosClienteMovil {

	@Resource
	private TimerService timerService;
	private BayeuxServer bayeuxServer;
	@EJB
	private SistemaContext sistemaContext;
	
	private static final Logger log = Logger.getLogger(EventosClienteMovil.class);
	
	private UsuarioService usuarioService;

	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Autowired
	private ClientePolizasService polizasService;
	
	
	public void start(BayeuxServer bayeuxServer) {
		this.bayeuxServer = bayeuxServer;
		ScheduleExpression expression = new ScheduleExpression();
        expression.second("*").minute("*").hour("*/3");
        TimerConfig timerConfig = new TimerConfig();
        timerConfig.setPersistent(false);
	}
	
	@Timeout
	public void execute() {
		List<Message> messages = new ArrayList<ExternalEventBroadcaster.Message>();	
		
		//Recibos
		reciboVencido();
		reciboPagado();
		reciboVencimiento();
		//polizas
		cancelacionPoliza();
		
		ExternalEventBroadcaster broadcaster = new ExternalEventBroadcaster(bayeuxServer);
    	broadcaster.onExternalEvent(messages); 
	}

	private void cancelacionPoliza() {
		List<NotificacionCliente> lstNotifica = new ArrayList<NotificacionCliente>();
		lstNotifica = polizasService.getCancelacionPoliza("");
		if (lstNotifica.size() > 0) {
			for (int i = 0; i < lstNotifica.size(); i++) {
				notification(lstNotifica.get(i).getMensaje(), "cancelaPoliza");
			}
		}
	}

	private void reciboVencimiento() {
		List<NotificacionCliente> lstNotifica = new ArrayList<NotificacionCliente>();
		lstNotifica = polizasService.getReciboVencimiento("");
		
		if (lstNotifica.size() > 0) {
			for (int i = 0; i < lstNotifica.size(); i++) {
				notification(lstNotifica.get(i).getMensaje(), "venceRecibo");
			}
		}
	}

	private void reciboPagado() {
		List<NotificacionCliente> lstNotifica = new ArrayList<NotificacionCliente>();
		lstNotifica = polizasService.getReciboPagado("");	
		if (lstNotifica.size() > 0) {
			for (int i = 0; i < lstNotifica.size(); i++) {
				notification(lstNotifica.get(i).getMensaje(), "reciboPagado");
			}
		}
		
	}

	private void reciboVencido() {
		List<NotificacionCliente> lstNotifica = new ArrayList<NotificacionCliente>();
		lstNotifica = polizasService.getReciboVencimiento("");
		if (lstNotifica.size() > 0) {
			for (int i = 0; i < lstNotifica.size(); i++) {
				notification(lstNotifica.get(i).getMensaje(), "reciboVencido");
			}
		}
		
	}
	
	public void notification(String message,  String evento){
		try {
			//buscar usuario asociado a la poliza
			final List<String> registrationId = usuarioService.getRegistrationIds(usuarioService.getUsuarioActual().getNombreUsuario());
			if(CollectionUtils.isNotEmpty(registrationId)) {
				final Builder builder = new Builder();
				builder.addData("message",  message);
				builder.addData("evento", evento);
				builder.addData("title", "Cobranza Afirme");
				final Sender sender = new Sender(sistemaContext.getClienteMovilSenderKey());//Se obtiene despues de registrar la app en la consolo de desarrolladores de google
				sender.send(builder.build(), registrationId, 0);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	public void sendNotification(String message, String senderUserName, String receiptUserName, String evento, Map<String, String> data){
		try {
			final List<String> registrationId = usuarioService.getRegistrationIds(receiptUserName);
			if(CollectionUtils.isNotEmpty(registrationId)) {
				int longitudRegistrationIdiOS = Integer.parseInt(sistemaContext.getLongitudRegistrationIdIOS());
				List<String> regAndroid = new ArrayList<String>();
				
				for (String reg: registrationId){
					//validar el tipo de token para ver si la plataforma es iOS.
					if (reg.length() == longitudRegistrationIdiOS){ // iOS
						//advertiseAPN(message, senderUserName, receiptUserName, evento, data, reg);
						}
					else{
						regAndroid.add(reg);
					}
				}
				
				if (CollectionUtils.isNotEmpty(regAndroid)) {
					notification(message, senderUserName);
				}
				
			}
		}catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}
