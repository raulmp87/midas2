package mx.com.afirme.midas2.timer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.cometd.bayeux.server.BayeuxServer;

import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster;
import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster.Message;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestro;
import mx.com.afirme.midas2.domain.sistema.Evento;
import mx.com.afirme.midas2.domain.sistema.EventoSeycos;
import mx.com.afirme.midas2.events.EventHandler;
import mx.com.afirme.midas2.events.EventHandlerRepository;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilMidasService;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilService;
import mx.com.afirme.midas2.service.sistema.EventoService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Singleton
@Startup
public class ConsumirEventosTimer {

	@EJB
	private EventoService eventoService;
	@Resource
	private TimerService timerService;
	private BayeuxServer bayeuxServer;
	@EJB
	private AjustadorMovilService ajustadorMovilService;
	@EJB
	private AjustadorMovilMidasService ajustadorMovilMidasService;
	@EJB
	private SistemaContext sistemaContext;
	
	EventHandlerRepository eventHandlerRepository = EventHandlerRepository.getInstance();
	
	private static final Logger LOG = Logger.getLogger(ConsumirEventosTimer.class);
	
	private static final String EVENTO_SEYCOS = "S";
	private static final String EVENTO_MIDAS = "M";
		
	public void start(BayeuxServer bayeuxServer) {
		if(sistemaContext.getTimerActivo()) {
			this.bayeuxServer = bayeuxServer;
			ScheduleExpression expression = new ScheduleExpression();
	        expression.second("*/15").minute("*").hour("*");
	        TimerConfig timerConfig = new TimerConfig();
	        timerConfig.setPersistent(false);
	        timerService.createCalendarTimer(expression, timerConfig);
		}
	}
	
	public static class ReporteSiniestroAsignadoMensaje {
		private Long reporteSiniestroId;
		private Long ajustadorId;
		private Long oldAjustadorId;
		
		public Long getReporteSiniestroId() {
			return reporteSiniestroId;
		}
		
		public void setReporteSiniestroId(Long reporteSiniestroId) {
			this.reporteSiniestroId = reporteSiniestroId;
		}
		
		public Long getAjustadorId() {
			return ajustadorId;
		}
		
		public void setAjustadorId(Long ajustadorId) {
			this.ajustadorId = ajustadorId;
		}
		
		public Long getOldAjustadorId() {
			return oldAjustadorId;
		}
		
		public void setOldAjustadorId(Long oldAjustadorId) {
			this.oldAjustadorId = oldAjustadorId;
		}		
	}
	
	public static class ReporteSiniestroCambioEstatusMensaje {
		private Long reporteSiniestroId;
		private String estatus;
		private String oldEstatus;
		
		public Long getReporteSiniestroId() {
			return reporteSiniestroId;
		}
		
		public void setReporteSiniestroId(Long reporteSiniestroId) {
			this.reporteSiniestroId = reporteSiniestroId;
		}
		
		public String getEstatus() {
			return estatus;
		}
		
		public void setEstatus(String estatus) {
			this.estatus = estatus;
		}
		
		public String getOldEstatus() {
			return oldEstatus;
		}
		
		public void setOldEstatus(String oldEstatus) {
			this.oldEstatus = oldEstatus;
		}		
	}
	
	
	public static class ReporteSiniestroFechaTerminacionAsignadaMensaje {
		private Long reporteSiniestroId;
		private Date fechaTerminacion;
		
		public Long getReporteSiniestroId() {
			return reporteSiniestroId;
		}
		
		public void setReporteSiniestroId(Long reporteSiniestroId) {
			this.reporteSiniestroId = reporteSiniestroId;
		}
		
		public Date getFechaTerminacion() {
			return fechaTerminacion;
		}
		
		public void setFechaTerminacion(Date fechaTerminacion) {
			this.fechaTerminacion = fechaTerminacion;
		}		
	}
	
	public static class ReporteSiniestroAsignadoBorradoMensaje {
		private Long reporteSiniestroId;
		private String mensaje;
		private Date fechaCreacion;
		
		public Long getReporteSiniestroId() {
			return reporteSiniestroId;
		}
		
		public void setReporteSiniestroId(Long reporteSiniestroId) {
			this.reporteSiniestroId = reporteSiniestroId;
		}
		
		public String getMensaje() {
			return mensaje;
		}
		
		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}
		
		public Date getFechaCreacion() {
			return fechaCreacion;
		}
		
		public void setFechaCreacion(Date fechaCreacion) {
			this.fechaCreacion = fechaCreacion;
		}
	}
	
	public enum NotificationType {
		COMETD, DEVICE
	}
	
	
	@Timeout
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void execute() {
		Sender sender = new Sender(sistemaContext.getAjustadorMovilSenderKey());
		NotificationType notificationType = NotificationType.DEVICE;
		List<Evento> eventos = eventoService.obtenerEventos();
		LOG.info("eventos: " + eventos);
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		for (Evento evento : eventos) {
			
			consumirEvento(evento);
			
			List<Message> messages = new ArrayList<ExternalEventBroadcaster.Message>();			
			String nombre = evento.getNombre();
			if (nombre.equals("reporteSiniestroAsignado")) {
				try {
					LOG.info("Nombre Evento: " + nombre);
					ObjectMapper mapper = new ObjectMapper();
					ReporteSiniestroAsignadoMensaje msgObj = mapper.readValue(evento.getMensaje(), ReporteSiniestroAsignadoMensaje.class);
					ReporteSiniestroAsignadoBorradoMensaje reporteSiniestroAsignadoBorradoMensaje = new ReporteSiniestroAsignadoBorradoMensaje();
					reporteSiniestroAsignadoBorradoMensaje.setReporteSiniestroId(msgObj.getReporteSiniestroId());
					reporteSiniestroAsignadoBorradoMensaje.setFechaCreacion(evento.getFechaCreacion());
					String tipo = obtenerTipoEvento(evento);
					ReporteSiniestro reporteSiniestro = null;
					
					if (tipo.equals(EVENTO_SEYCOS)){
						reporteSiniestro = ajustadorMovilService.getReporteSiniestro(msgObj.getReporteSiniestroId());	
					} else {
						LOG.info("Se obtiene el reporte en midas ");
						reporteSiniestro = ajustadorMovilMidasService.getReporteSiniestro(msgObj.getReporteSiniestroId());
					}
						
					if (msgObj.getAjustadorId() != null) {
						LOG.info("Obtiene el ajustador: " + msgObj.getAjustadorId());
						if(tipo.equals(EVENTO_SEYCOS)){
							reporteSiniestroAsignadoBorradoMensaje.setMensaje(
									String.format("Se ha asignado el reporte de siniestro %s para su atencion.", reporteSiniestro.getNumeroReporte()));
						} else {
							reporteSiniestroAsignadoBorradoMensaje.setMensaje(
									String.format("Se ha asignado el reporte de siniestro %s para su atencion.", reporteSiniestro.getNumeroReporteCabina()));
						}
						
						
						if (notificationType.equals(NotificationType.COMETD)) {
							messages.add(new Message(mapper.writeValueAsString(reporteSiniestroAsignadoBorradoMensaje),
									String.format("/ajustador/%s/reporte-siniestros-asignados/agregado", msgObj.getAjustadorId())));						
						} else if (notificationType.equals(NotificationType.DEVICE)) {
							Builder b = new Builder();
							b.addData("evento", "/reporte-siniestros-asignados/agregado")
								.addData("reporteSiniestroId", msgObj.getReporteSiniestroId().toString())
								.addData("message", reporteSiniestroAsignadoBorradoMensaje.getMensaje())
								.addData("fechaCreacion", df.format(reporteSiniestroAsignadoBorradoMensaje.getFechaCreacion()));
							List<String> regIds = new ArrayList<String>();
							if (tipo.equals(EVENTO_SEYCOS)){
								regIds = ajustadorMovilService.getRegistrationIds(msgObj.getAjustadorId());
							} else {
								regIds = ajustadorMovilMidasService.getRegistrationIds(msgObj.getAjustadorId());
							}
							
							if (!regIds.isEmpty()) {
								MulticastResult result = sender.send(b.build(), regIds, 0);
								//System.out.println(result);
							}
						}
					}
					if (msgObj.getOldAjustadorId() != null) {
						if(tipo.equals(EVENTO_SEYCOS)){
							reporteSiniestroAsignadoBorradoMensaje.setMensaje(
									String.format("El reporte de siniestro %s fue reasignado a otro ajustador.", reporteSiniestro.getNumeroReporte()));
						} else{
							reporteSiniestroAsignadoBorradoMensaje.setMensaje(
									String.format("El reporte de siniestro %s fue reasignado a otro ajustador.", reporteSiniestro.getNumeroReporteCabina()));
						}
						if (notificationType.equals(NotificationType.COMETD)) {							
							messages.add(new Message(mapper.writeValueAsString(reporteSiniestroAsignadoBorradoMensaje), 
									String.format("/ajustador/%s/reporte-siniestros-asignados/borrado", msgObj.getOldAjustadorId())));													
						} else if (notificationType.equals(NotificationType.DEVICE)) {
							Builder b = new Builder();
							b.addData("evento", "/reporte-siniestros-asignados/borrado")
								.addData("reporteSiniestroId", msgObj.getReporteSiniestroId().toString())
								.addData("message", reporteSiniestroAsignadoBorradoMensaje.getMensaje())
								.addData("fechaCreacion", df.format(reporteSiniestroAsignadoBorradoMensaje.getFechaCreacion()));
							
							List<String> regIds = new ArrayList<String>();
							if (tipo.equals(EVENTO_SEYCOS)){
								regIds = ajustadorMovilService.getRegistrationIds(msgObj.getOldAjustadorId());
							} else {
								regIds = ajustadorMovilMidasService.getRegistrationIds(msgObj.getOldAjustadorId());
							}
							
							if (!regIds.isEmpty()) {
								sender.send(b.build(), regIds, 0);
							}
						}
					}
					
					eventoService.borrarEventoProcesado(evento);
					
				}catch(Exception e) {
					LOG.error("No se pudo procesar evento.", e);
				}
			}else if (nombre.equals("reporteSiniestroFechaTerminacionAsignada")) {
				try {
					ObjectMapper mapper = new ObjectMapper();
					ReporteSiniestroFechaTerminacionAsignadaMensaje msgObj = mapper.readValue(evento.getMensaje(), ReporteSiniestroFechaTerminacionAsignadaMensaje.class);						
					ReporteSiniestro reporteSiniestro = ajustadorMovilService.getReporteSiniestro(msgObj.getReporteSiniestroId());					
					if (reporteSiniestro.getAjustador() != null){
						ReporteSiniestroAsignadoBorradoMensaje reporteSiniestroAsignadoBorradoMensaje = new ReporteSiniestroAsignadoBorradoMensaje();
						reporteSiniestroAsignadoBorradoMensaje.setReporteSiniestroId(msgObj.getReporteSiniestroId());
						reporteSiniestroAsignadoBorradoMensaje.setFechaCreacion(evento.getFechaCreacion());
						String tipo = obtenerTipoEvento(evento);
						if(tipo.equals(EVENTO_SEYCOS)){
							reporteSiniestroAsignadoBorradoMensaje.setMensaje(
									String.format("Se ha marcado fecha de terminacion al reporte de siniestro %s", reporteSiniestro.getNumeroReporte()));
						} else{
							reporteSiniestroAsignadoBorradoMensaje.setMensaje(
									String.format("Se ha marcado fecha de terminacion al reporte de siniestro %s", reporteSiniestro.getNumeroReporteCabina()));
						}
						
						if (notificationType.equals(NotificationType.COMETD)) {
							messages.add(new Message(mapper.writeValueAsString(reporteSiniestroAsignadoBorradoMensaje), 
								String.format("/ajustador/%s/reporte-siniestros-asignados/borrado", reporteSiniestro.getAjustador().getId())));
						} else if (notificationType.equals(NotificationType.DEVICE)) {
							Builder b = new Builder();
							b.addData("evento", "/reporte-siniestros-asignados/borrado")
								.addData("reporteSiniestroId", msgObj.getReporteSiniestroId().toString())
								.addData("message", reporteSiniestroAsignadoBorradoMensaje.getMensaje())
								.addData("fechaCreacion", df.format(reporteSiniestroAsignadoBorradoMensaje.getFechaCreacion()));
							
							List<String> regIds = new ArrayList<String>();
							if (tipo.equals(EVENTO_SEYCOS)){
								regIds = ajustadorMovilService.getRegistrationIds(reporteSiniestro.getAjustador().getId());
							} else {
								regIds = ajustadorMovilMidasService.getRegistrationIds(reporteSiniestro.getAjustador().getId());
							}
							if (!regIds.isEmpty()) {
								sender.send(b.build(), regIds, 0);
							}
						}
					}
					
					eventoService.borrarEventoProcesado(evento);
					
				}catch(Exception e) {
					LOG.error("No se pudo procesar evento.", e);
				}	
			}else if (nombre.equals("reporteSiniestroCambioEstatus")) {
				try{
					LOG.info("Nombre Evento: " + nombre);
					ObjectMapper mapper = new ObjectMapper();
					ReporteSiniestroCambioEstatusMensaje msgObj = mapper.readValue(evento.getMensaje(), ReporteSiniestroCambioEstatusMensaje.class);
					String estatus = msgObj.getEstatus();
					if (estatus.equals("CANC") || estatus.equals("RECH") || estatus.equals("PREES")) {
						String tipo = obtenerTipoEvento(evento);
						ReporteSiniestro reporteSiniestro = null;	
						if (tipo.equals(EVENTO_SEYCOS)){
							reporteSiniestro = ajustadorMovilService.getReporteSiniestro(msgObj.getReporteSiniestroId());	
						} else {
							LOG.info("Obtener el reporte en Midas: ");
							reporteSiniestro = ajustadorMovilMidasService.getReporteSiniestro(msgObj.getReporteSiniestroId());
						}						
						if (reporteSiniestro.getAjustador() != null) {
							LOG.info("Ajustador : " + reporteSiniestro.getAjustador());
							ReporteSiniestroAsignadoBorradoMensaje reporteSiniestroAsignadoBorradoMensaje = new ReporteSiniestroAsignadoBorradoMensaje();
							reporteSiniestroAsignadoBorradoMensaje.setReporteSiniestroId(msgObj.getReporteSiniestroId());
							reporteSiniestroAsignadoBorradoMensaje.setFechaCreacion(evento.getFechaCreacion());
							if(tipo.equals(EVENTO_SEYCOS)){
								reporteSiniestroAsignadoBorradoMensaje.setMensaje(
										String.format("Se ha cancelado el reporte de siniestro %s", reporteSiniestro.getNumeroReporte()));
							} else{
								reporteSiniestroAsignadoBorradoMensaje.setMensaje(
										String.format("Se ha cancelado el reporte de siniestro %s", reporteSiniestro.getNumeroReporteCabina()));
							}
							
							if (notificationType.equals(NotificationType.COMETD)) {
								messages.add(new Message(mapper.writeValueAsString(reporteSiniestroAsignadoBorradoMensaje), 
										String.format("/ajustador/%s/reporte-siniestros-asignados/borrado", reporteSiniestro.getAjustador().getId())));
							}
							else if (notificationType.equals(NotificationType.DEVICE)) {
								Builder b = new Builder();
								b.addData("evento", "/reporte-siniestros-asignados/borrado")
									.addData("reporteSiniestroId", msgObj.getReporteSiniestroId().toString())
									.addData("message", reporteSiniestroAsignadoBorradoMensaje.getMensaje())
									.addData("fechaCreacion", df.format(reporteSiniestroAsignadoBorradoMensaje.getFechaCreacion()));
								List<String> regIds = new ArrayList<String>();
								if (tipo.equals(EVENTO_SEYCOS)){
									regIds = ajustadorMovilService.getRegistrationIds(reporteSiniestro.getAjustador().getId());
								} else {
									regIds = ajustadorMovilMidasService.getRegistrationIds(reporteSiniestro.getAjustador().getId());
								}
								if (!regIds.isEmpty()) {
									sender.send(b.build(), regIds, 0);
								}
							}
						}
					}
					
					eventoService.borrarEventoProcesado(evento);
					
				}catch(Exception e) {
					LOG.error("No se pudo procesar evento.", e);
				}
			}
			
			try {
				
				ExternalEventBroadcaster broadcaster = new ExternalEventBroadcaster(bayeuxServer);
		    	broadcaster.onExternalEvent(messages);
		    	
			}catch(Exception e) {
				
				LOG.error("Error al iniciar ExternalEventBroadcaster: ", e);	 //Se maneja la excepcion para no invalidar la ejecución del timer
				
			}	
		}
	}
	
	
	private String obtenerTipoEvento(Evento evento) {
		
		return (evento instanceof EventoSeycos ? EVENTO_SEYCOS : EVENTO_MIDAS);
		
	}
	
	
	private void consumirEvento(Evento evento) {
		
		EventHandler handler = eventHandlerRepository.getEventHandler(evento.getNombre());
		
		if (handler != null) {
			
			try {
			
				handler.execute(evento);
				
				eventoService.borrarEventoProcesado(evento);
			
			} catch (Exception ex) {
				
				LOG.error("No se pudo procesar evento.", ex);
				
			}
			
		}
		
	}
	
	
}
