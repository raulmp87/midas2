package mx.com.afirme.midas2.timer;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import mx.com.afirme.midas.danios.reportes.reportercs.ReporteRCSFacadeRemote;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.service.calculos.CalculoComisionesService;
//import mx.com.afirme.midas2.service.cfdi.CancelacionesCFDIService;
//import mx.com.afirme.midas2.service.cfdi.CancelarCFDIAcuseCancService;
//import mx.com.afirme.midas2.service.cobranza.NotificaFacturaReciboService;
import mx.com.afirme.midas2.service.custShipmentTracking.TrackingService;
import mx.com.afirme.midas2.service.emision.ppct.PagoProveedorService;
import mx.com.afirme.midas2.service.emisionFacturas.EmisionFacturaService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EnvioFacturaAgenteService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionAniversarioAgenteService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionAniversarioBodaAgenteService;
//import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCalculoBonosService;
//import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCalculoComisionesService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCargosService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCedulaPorVencerAgenteService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCedulaVencidaAgenteService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCumpleAniosAgenteService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionFechaFianzaPorVencerAgenteService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionFechaFianzaVencidaAgenteService;
//import mx.com.afirme.midas2.service.jobAgentes.ProgramacionReportePRimaEmitVsPagService;
//import mx.com.afirme.midas2.service.jobAgentes.ProgramacionReporteProduccionService;
import mx.com.afirme.midas2.service.migracion.MigracionService;
import mx.com.afirme.midas2.service.migracion.TareaMigrarArchivosFortimaxService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.prestamos.PagarePrestamoAnticipoService;
import mx.com.afirme.midas2.service.provisiones.ProvisionesBonosAgenteService;
import mx.com.afirme.midas2.service.recepcionDocumentos.RecepcionDocumentosService;
import mx.com.afirme.midas2.service.reporteAgentes.ProgramacionReporteAgenteExecuteService;
//import mx.com.afirme.midas2.service.reportes.GeneracionSesasService;
import mx.com.afirme.midas2.service.reportes.GeneracionVigorService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisEjecucionesJOBSService;
import mx.com.afirme.midas2.service.siniestros.depuracion.DepuracionReservaService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.TareaNotificarRecurrentesService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososCancIncisoAutoService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososCancelacionEndosoAutoService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososCancelacionPolizaAutoService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososRehabPolAutoService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososRehabilitacionEndosoAutoService;
import mx.com.afirme.midas2.service.tarea.TareaEmiteEndososRehabilitacionIncisosService;
//import mx.com.afirme.midas2.service.tarea.TareaEnvioReporteCancelacionesService;
//import mx.com.afirme.midas2.service.tarea.TareaGeneraReporteDxPService;
import mx.com.afirme.midas2.service.tareas.informacionVehicular.EnvioVINFlotillasService;
import mx.com.afirme.midas2.service.tareas.informacionVehicular.ValidacionVINFlotillasService;
//import mx.com.afirme.vida.service.asistencia.AsistenciaVidaService;

@Singleton
@Startup
public class IniciarConfiguracionTimers {
	
	private AgenteMidasService agenteMidasService;
	
	private CalculoBonosService calculoBonosService;
	
	private CalculoComisionesService calculoComisionesService;
	
	//private CancelacionesCFDIService cancelacionesCFDIService;
	
	//private CancelarCFDIAcuseCancService cancelarCFDIAcuseCancService;
	
	private ClientePolizasService clientePolizasService;
	
	private ConfigBonosService configBonosService;
	
	private DepuracionReservaService depuracionReservaService;
	
	private EmisionFacturaService emisionFacturaService;
	
	private EmisionPendienteService emisionPendienteService;
	
	private EnvioFacturaAgenteService envioFacturaAgenteService;
	
	private EnvioVINFlotillasService envioVINFlotillasService;
	
	private FronterizosService fronterizosService;
	
//	private GeneracionSesasService generacionSesasService;
	
	private GeneracionVigorService generacionVigorService;
	
	private IngresoService ingresoService;
	
	private MigracionService migracionService;
	
	//private NotificaFacturaReciboService notificaFacturaReciboService;
	
	private PagarePrestamoAnticipoService pagarePrestamoAnticipoService;
	
	private PagoProveedorService pagoProveedorService;
	
	private PolizaSiniestroService polizaSiniestroService;
	
	private ProgramacionAniversarioAgenteService programacionAniversarioAgenteService;
	
	private ProgramacionAniversarioBodaAgenteService programacionAniversarioBodaAgenteService;
	
	//private ProgramacionCalculoBonosService programacionCalculoBonosService;
	
	//private ProgramacionCalculoComisionesService programacionCalculoComisionesService;
	
	private ProgramacionCargosService programacionCargosService;
	
	private ProgramacionCedulaPorVencerAgenteService programacionCedulaPorVencerAgenteService;
	
	private ProgramacionCedulaVencidaAgenteService programacionCedulaVencidaAgenteService;
	
	private ProgramacionCumpleAniosAgenteService programacionCumpleAniosAgenteService;
	
	private ProgramacionFechaFianzaVencidaAgenteService programacionFechaFianzaVencidaAgenteService;
	
	private ProgramacionFechaFianzaPorVencerAgenteService programacionFechaFianzaPorVencerAgenteService;
	
	private ProgramacionReporteAgenteExecuteService programacionReporteAgenteExecuteService;
	
	//private ProgramacionReportePRimaEmitVsPagService programacionReportePRimaEmitVsPagService;
	
	//private ProgramacionReporteProduccionService programacionReporteProduccionService;
	
	private ProvisionesBonosAgenteService provisionesBonosAgenteService;
	
	private RecepcionDocumentosService recepcionDocumentosService;
	
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	//private ReporteRCSFacadeRemote reporteRCSFacadeRemote;
	
	private SapAmisEjecucionesJOBSService sapAmisEjecucionesJOBSService;
	
	private TareaEmiteEndososCancelacionEndosoAutoService tareaEmiteEndososCancelacionEndosoAutoService;
	
	private TareaEmiteEndososCancelacionPolizaAutoService tareaEmiteEndososCancelacionPolizaAutoService;
	
	private TareaEmiteEndososCancIncisoAutoService tareaEmiteEndososCancIncisoAutoService;
	
	private TareaEmiteEndososRehabilitacionEndosoAutoService tareaEmiteEndososRehabilitacionEndosoAutoService;
	
	private TareaEmiteEndososRehabilitacionIncisosService tareaEmiteEndososRehabilitacionIncisosService;
	
	private TareaEmiteEndososRehabPolAutoService tareaEmiteEndososRehabPolAutoService;
	
	private TareaMigrarArchivosFortimaxService tareaMigrarArchivosFortimaxService;
	
	private TareaNotificarRecurrentesService tareaNotificarRecurrentesService;
	
	private TrackingService trackingService;
	
	private ValidacionVINFlotillasService validacionVINFlotillasService;
	
	/*private TareaEnvioReporteCancelacionesService tareaEnvioReporteCancelacionesService;
	
	private TareaGeneraReporteDxPService tareaGeneraReporteDxPService;
	*/
	
//	private AsistenciaVidaService asistenciaVidaService;

	@EJB(beanName = "agenteMidasEJB")
	public void setAgenteMidasService(
			AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB(beanName = "calculoBonosServiceEJB")
	public void setCalculoBonosService(
			CalculoBonosService calculoBonosService) {
		this.calculoBonosService = calculoBonosService;
	}
	
	@EJB(beanName = "calculoComisionesServiceEJB")
	public void setCalculoComisionesService(
			CalculoComisionesService calculoComisionesService) {
		this.calculoComisionesService = calculoComisionesService;
	}
	
//	@EJB(beanName = "cancelacionesCFDIServiceEJB")
//	public void setCancelacionesCFDIService(
//			CancelacionesCFDIService cancelacionesCFDIService) {
//		this.cancelacionesCFDIService = cancelacionesCFDIService;
//	}
//	
//	@EJB(beanName = "cancelarCFDIAcuseCancServiceEJB")
//	public void setCancelarCFDIAcuseCancService(
//			CancelarCFDIAcuseCancService cancelarCFDIAcuseCancService) {
//		this.cancelarCFDIAcuseCancService = cancelarCFDIAcuseCancService;
//	}
	
	@EJB(beanName = "clientePolizasServiceEJB")
	public void setClientePolizasService(
			ClientePolizasService clientePolizasService) {
		this.clientePolizasService = clientePolizasService;
	}	
	
	@EJB(beanName = "configBonosServiceEJB")
	public void setConfigBonosService(
			ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}
	
	@EJB(beanName = "depuracionReservaServiceEJB")
	public void setDepuracionReservaService(
			DepuracionReservaService depuracionReservaService) {
		this.depuracionReservaService = depuracionReservaService;
	}	
	
	@EJB(beanName = "emisionFacturaServiceEJB")
	public void setEmisionFacturaService(
			EmisionFacturaService emisionFacturaService) {
		this.emisionFacturaService = emisionFacturaService;
	}
	
	@EJB(beanName = "envioFacturaAgenteEJB")
	public void setEnvioFacturaAgenteService(
			EnvioFacturaAgenteService envioFacturaAgenteService) {
		this.envioFacturaAgenteService = envioFacturaAgenteService;
	}
	
	@EJB(beanName = "emisionPendienteServiceEJB")
	public void setEmisionPendienteService(
			EmisionPendienteService emisionPendienteService) {
		this.emisionPendienteService = emisionPendienteService;
	}
	
	@EJB(beanName = "envioVINFlotillasServiceEJB")
	public void setEnvioVINFlotillasService(
			EnvioVINFlotillasService envioVINFlotillasService) {
		this.envioVINFlotillasService = envioVINFlotillasService;
	}
	
/*	@EJB(beanName = "tareaEnvioReporteCancelacionesService")
	public void setTareaEnvioReporteCancelacionesService(
			TareaEnvioReporteCancelacionesService tareaEnvioReporteCancelacionesService) {
		this.tareaEnvioReporteCancelacionesService =  
				tareaEnvioReporteCancelacionesService;
	}
	
	@EJB(beanName = "tareaGeneraReporteDxPService")
	public void setTareaGeneraReporteDxPService(
			TareaGeneraReporteDxPService tareaGeneraReporteDxPService) {
		this.tareaGeneraReporteDxPService = tareaGeneraReporteDxPService;
	}*/

	@EJB(beanName = "fronterizosServiceEJB")
	public void setFronterizosService(
			FronterizosService fronterizosService) {
		this.fronterizosService = fronterizosService;
	}
	
//	@EJB(beanName = "generacionSesasServiceEJB")
//	public void setGeneracionSesasService(
//			GeneracionSesasService generacionSesasService) {
//		this.generacionSesasService = generacionSesasService;
//	}
	
	@EJB(beanName = "generacionVigorService")
	public void setGeneracionVigorService(
			GeneracionVigorService generacionVigorService) {
		this.generacionVigorService = generacionVigorService;
	}
	
	@EJB(beanName = "ingresoServiceEJB")
	public void setIngresoService(
			IngresoService ingresoService) {
		this.ingresoService = ingresoService;
	}
	
	@EJB(beanName = "migracionServiceEJB")
	public void setMigracionService(
			MigracionService migracionService) {
		this.migracionService = migracionService;
	}
	
	/*@EJB(beanName = "notificaFacturaServiceEJB")
	public void setNotificaFacturaReciboService(
			NotificaFacturaReciboService notificaFacturaReciboService) {
		this.notificaFacturaReciboService = notificaFacturaReciboService;
	}*/
	
	@EJB(beanName = "pagarePrestamoAnticipoServiceEJB")
	public void setPagarePrestamoAnticipoService(
			PagarePrestamoAnticipoService pagarePrestamoAnticipoService) {
		this.pagarePrestamoAnticipoService = pagarePrestamoAnticipoService;
	}
	
	@EJB(beanName = "pagoProveedorService")
	public void setPagoProveedorService(
			PagoProveedorService pagoProveedorService) {
		this.pagoProveedorService = pagoProveedorService;
	}
	
	@EJB(beanName = "polizaSiniestroServiceEJB")
	public void setPolizaSiniestroService(
			PolizaSiniestroService polizaSiniestroService) {
		this.polizaSiniestroService = polizaSiniestroService;
	}
	
	@EJB(beanName = "ProgramacionAniversarioAgenteEJB")
	public void setProgramacionAniversarioAgenteService(
			ProgramacionAniversarioAgenteService programacionAniversarioAgenteService) {
		this.programacionAniversarioAgenteService = programacionAniversarioAgenteService;
	}
	
	@EJB(beanName = "ProgramacionAniversarioBodaAgenteEJB")
	public void setProgramacionAniversarioBodaAgenteService(
			ProgramacionAniversarioBodaAgenteService programacionAniversarioBodaAgenteService) {
		this.programacionAniversarioBodaAgenteService = programacionAniversarioBodaAgenteService;
	}
	
	/*@EJB(beanName = "programacionCalculoBonosServiceEJB")
	public void setProgramacionCalculoBonosService(
			ProgramacionCalculoBonosService programacionCalculoBonosService) {
		this.programacionCalculoBonosService = programacionCalculoBonosService;
	}*/
	
	/*@EJB(beanName = "programacionCalculoComisionesServiceEJB")
	public void setProgramacionCalculoComisionesService(
			ProgramacionCalculoComisionesService programacionCalculoComisionesService) {
		this.programacionCalculoComisionesService = programacionCalculoComisionesService;
	}*/
	
	@EJB(beanName = "programacionCargosServiceEJB")
	public void setProgramacionCargosService(
			ProgramacionCargosService programacionCargosService) {
		this.programacionCargosService = programacionCargosService;
	}
	
	@EJB(beanName = "ProgramacionCedulaPorVencerAgenteEJB")
	public void setProgramacionCedulaPorVencerAgenteService(
			ProgramacionCedulaPorVencerAgenteService programacionCedulaPorVencerAgenteService) {
		this.programacionCedulaPorVencerAgenteService = programacionCedulaPorVencerAgenteService;
	}
	
	@EJB(beanName = "ProgramacionCedulaVencidaAgenteEJB")
	public void setProgramacionCedulaVencidaAgenteService(
			ProgramacionCedulaVencidaAgenteService programacionCedulaVencidaAgenteService) {
		this.programacionCedulaVencidaAgenteService = programacionCedulaVencidaAgenteService;
	}
	
	@EJB(beanName = "ProgramacionCumpleAniosAgenteEJB")
	public void setProgramacionCumpleAniosAgenteService(
			ProgramacionCumpleAniosAgenteService programacionCumpleAniosAgenteService) {
		this.programacionCumpleAniosAgenteService = programacionCumpleAniosAgenteService;
	}
	
	@EJB(beanName = "programacionFechaFianzaPorVencerAgenteServiceEJB")
	public void setProgramacionFechaFianzaPorVencerAgenteService(
			ProgramacionFechaFianzaPorVencerAgenteService programacionFechaFianzaPorVencerAgenteService) {
		this.programacionFechaFianzaPorVencerAgenteService = programacionFechaFianzaPorVencerAgenteService;
	}
	
	@EJB(beanName = "programacionFechaFianzaVencidaAgenteServiceEJB")
	public void setProgramacionFechaFianzaVencidaAgenteService(
			ProgramacionFechaFianzaVencidaAgenteService programacionFechaFianzaVencidaAgenteService) {
		this.programacionFechaFianzaVencidaAgenteService = programacionFechaFianzaVencidaAgenteService;
	}
	
	@EJB(beanName = "ProgramacionReporteAgenteExecuteServiceEJB")
	public void setProgramacionReporteAgenteExecuteService(
			ProgramacionReporteAgenteExecuteService programacionReporteAgenteExecuteService) {
		this.programacionReporteAgenteExecuteService = programacionReporteAgenteExecuteService;
	}
	
	/*@EJB(beanName = "programacionReportePRimaEmitVsPagServiceEJB")
	public void setProgramacionReportePRimaEmitVsPagService(
			ProgramacionReportePRimaEmitVsPagService programacionReportePRimaEmitVsPagService) {
		this.programacionReportePRimaEmitVsPagService = programacionReportePRimaEmitVsPagService;
	}*/
	
	/*@EJB(beanName = "ProgramacionReporteProduccionEJB")
	public void setProgramacionReporteProduccionService(
			ProgramacionReporteProduccionService programacionReporteProduccionService) {
		this.programacionReporteProduccionService = programacionReporteProduccionService;
	}*/
	
	@EJB(beanName = "provisionBonoAgenteEJB")
	public void setProvisionesBonosAgenteService(
			ProvisionesBonosAgenteService provisionesBonosAgenteService) {
		this.provisionesBonosAgenteService = provisionesBonosAgenteService;
	}
	
	@EJB(beanName = "recepDocumentosServiceEJB")
	public void setRecepcionDocumentosService(
			RecepcionDocumentosService recepcionDocumentosService) {
		this.recepcionDocumentosService = recepcionDocumentosService;
	}
	
	@EJB(beanName = "recuperacionSalvamentoServiceEJB")
	public void setRecuperacionSalvamentoServicee(
			RecuperacionSalvamentoService recuperacionSalvamentoService) {
		this.recuperacionSalvamentoService = recuperacionSalvamentoService;
	}
	
	/*
	@EJB(beanName = "reporteRCSFacadeRemote")
	public void setReporteRCSFacadeRemote(
			ReporteRCSFacadeRemote reporteRCSFacadeRemote) {
		this.reporteRCSFacadeRemote = reporteRCSFacadeRemote;
	}*/
	
	@EJB(beanName = "sapAmisEjecucionesJOBSServiceEJB")
	public void setSapAmisEjecucionesJOBSService(
			SapAmisEjecucionesJOBSService sapAmisEjecucionesJOBSService) {
		this.sapAmisEjecucionesJOBSService = sapAmisEjecucionesJOBSService;
	}
	
	@EJB(beanName = "tareaEmiteEndososCancelacionEndosoAutoServiceEJB")
	public void setTareaEmiteEndososCancelacionEndosoAutoService(
			TareaEmiteEndososCancelacionEndosoAutoService tareaEmiteEndososCancelacionEndosoAutoService) {
		this.tareaEmiteEndososCancelacionEndosoAutoService = tareaEmiteEndososCancelacionEndosoAutoService;
	}
	
	@EJB(beanName = "tareaEmiteEndososCancelacionPolizaAutoServiceEJB")
	public void setTareaEmiteEndososCancelacionPolizaAutoService(
			TareaEmiteEndososCancelacionPolizaAutoService tareaEmiteEndososCancelacionPolizaAutoService) {
		this.tareaEmiteEndososCancelacionPolizaAutoService = tareaEmiteEndososCancelacionPolizaAutoService;
	}
	
	@EJB(beanName = "tareaEmiteEndososCancIncisoAutoServiceEJB")
	public void setTareaEmiteEndososCancIncisoAutoService(
			TareaEmiteEndososCancIncisoAutoService tareaEmiteEndososCancIncisoAutoService) {
		this.tareaEmiteEndososCancIncisoAutoService = tareaEmiteEndososCancIncisoAutoService;
	}
	
	@EJB(beanName = "tareaEmiteEndososRehabilitacionEndosoAutoServiceEJB")
	public void setTareaEmiteEndososRehabilitacionEndosoAutoService(
			TareaEmiteEndososRehabilitacionEndosoAutoService tareaEmiteEndososRehabilitacionEndosoAutoService) {
		this.tareaEmiteEndososRehabilitacionEndosoAutoService = tareaEmiteEndososRehabilitacionEndosoAutoService;
	}
	
	@EJB(beanName = "tareaEmiteEndososRehabilitacionIncisosServiceEJB")
	public void setTareaEmiteEndososRehabilitacionIncisosService(
			TareaEmiteEndososRehabilitacionIncisosService tareaEmiteEndososRehabilitacionIncisosService) {
		this.tareaEmiteEndososRehabilitacionIncisosService = tareaEmiteEndososRehabilitacionIncisosService;
	}
	
	@EJB(beanName = "tareaEmiteEndososRehabPolAutoServiceEJB")
	public void setTareaEmiteEndososRehabPolAutoService(
			TareaEmiteEndososRehabPolAutoService tareaEmiteEndososRehabPolAutoService) {
		this.tareaEmiteEndososRehabPolAutoService = tareaEmiteEndososRehabPolAutoService;
	}
	
	@EJB(beanName = "tareaMigrarArchivosFortimaxServiceEJB")
	public void setTareaMigrarArchivosFortimaxService(
			TareaMigrarArchivosFortimaxService tareaMigrarArchivosFortimaxService) {
		this.tareaMigrarArchivosFortimaxService = tareaMigrarArchivosFortimaxService;
	}
	
	@EJB(beanName = "tareaNotificarRecurrentesServiceEJB")
	public void setTareaNotificarRecurrentesService(
			TareaNotificarRecurrentesService tareaNotificarRecurrentesService) {
		this.tareaNotificarRecurrentesService = tareaNotificarRecurrentesService;
	}
	
	@EJB(beanName = "trackingService")
	public void setTrackingService(
			TrackingService trackingService) {
		this.trackingService = trackingService;
	}
	
	@EJB(beanName = "validacionVINFlotillasServiceEJB")
	public void setValidacionVINFlotillasService(
			ValidacionVINFlotillasService validacionVINFlotillasService) {
		this.validacionVINFlotillasService = validacionVINFlotillasService;
	}

//	@EJB(beanName = "AsistenciaVidaServiceEJB")
//	public void setAsistenciaVidaService(AsistenciaVidaService asistenciaVidaService) {
//		this.asistenciaVidaService = asistenciaVidaService;
//	}

	@PostConstruct
	private void postConstruct() {
		/** Invocar el metodo initialize() de Timers .. */
		agenteMidasService.initialize();
		calculoBonosService.initialize();
		calculoComisionesService.initialize();
//		cancelacionesCFDIService.initialize();
//		cancelarCFDIAcuseCancService.initialize();
		clientePolizasService.initialize();
		configBonosService.initialize();
		depuracionReservaService.initialize();
		emisionPendienteService.initialize();
		emisionFacturaService.initialize();
//		envioFacturaAgenteService.initialize();
		envioVINFlotillasService.initialize();
		fronterizosService.initialize();
//		generacionSesasService.initialize();
		generacionVigorService.initialize();
		ingresoService.initialize();
//		migracionService.initialize();      			Deshabilitado en ambiente DEV
		//notificaFacturaReciboService.initialize();	Deshabilitado en ambiente DEV
		pagarePrestamoAnticipoService.initialize();
		pagoProveedorService.initialize();
		polizaSiniestroService.initialize();
		programacionAniversarioAgenteService.initialize();
		programacionAniversarioBodaAgenteService.initialize();
		//programacionCalculoBonosService.initialize();		Deshabilitado en ambiente DEV
		//programacionCalculoComisionesService.initialize();	Deshabilitado en ambiente DEV
		programacionCargosService.initialize();
		programacionCedulaPorVencerAgenteService.initialize();
		programacionCedulaVencidaAgenteService.initialize();
		programacionCumpleAniosAgenteService.initialize();
		programacionFechaFianzaPorVencerAgenteService.initialize();
		programacionFechaFianzaVencidaAgenteService.initialize();
		programacionReporteAgenteExecuteService.initialize();
		//programacionReportePRimaEmitVsPagService.initialize();	Deshabilitado en ambiente DEV
		//programacionReporteProduccionService.initialize();	Deshabilitado en ambiente DEV
		provisionesBonosAgenteService.initialize();
		recepcionDocumentosService.initialize();
		recuperacionSalvamentoService.initialize();
		//reporteRCSFacadeRemote.initialize();
		sapAmisEjecucionesJOBSService.initialize();
		tareaEmiteEndososCancelacionEndosoAutoService.initialize();
		tareaEmiteEndososCancelacionPolizaAutoService.initialize();
		tareaEmiteEndososCancIncisoAutoService.initialize();
		tareaEmiteEndososRehabilitacionEndosoAutoService.initialize();
		tareaEmiteEndososRehabilitacionIncisosService.initialize();
		tareaEmiteEndososRehabPolAutoService.initialize();
		tareaMigrarArchivosFortimaxService.initialize();
		tareaNotificarRecurrentesService.initialize();
		trackingService.initialize();
		validacionVINFlotillasService.initialize();
//		asistenciaVidaService.initialize();
	}
}
