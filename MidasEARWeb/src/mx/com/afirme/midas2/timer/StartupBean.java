package mx.com.afirme.midas2.timer;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import mx.com.afirme.midas2.service.NotificacionesCierreMes.NotificacionCierreMesService;

@Singleton
@Startup
public class StartupBean {

	private NotificacionCierreMesService notificacionCierreMesService;

	@EJB(beanName = "notificacionCierreMesServiceEJB")
	public void setNotificacionCierreMesService(
			NotificacionCierreMesService notificacionCierreMesService) {
		this.notificacionCierreMesService = notificacionCierreMesService;
	}


	@PostConstruct
	private void postConstruct() {
		notificacionCierreMesService.initialize();

	}

}
