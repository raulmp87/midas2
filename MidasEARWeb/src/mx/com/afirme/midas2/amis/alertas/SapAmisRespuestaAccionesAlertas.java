package mx.com.afirme.midas2.amis.alertas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SapAmisAccionesAlertas", propOrder = {
    "idAccion",
    "descAccion"
})
/**
 * Bean para llenar el grid sapAmisAccionesAlertasGrid.jsp
 * @author Luis Ibarra
 */
public class SapAmisRespuestaAccionesAlertas {
	protected long idAccion;
	protected String descAccion;
	public long getIdAccion() {
		return idAccion;
	}
	public void setIdAccion(long idAccion) {
		this.idAccion = idAccion;
	}
	public String getDescAccion() {
		return descAccion;
	}
	public void setDescAccion(String descAccion) {
		this.descAccion = descAccion;
	}
}