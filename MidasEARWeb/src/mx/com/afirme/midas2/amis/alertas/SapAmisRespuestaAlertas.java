package mx.com.afirme.midas2.amis.alertas;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SapAmisAlertas", propOrder = {
    "idAlerta",
    "sistema",
    "alerta",
    "acciones"
})

/**
 * Bean para llenar el grid sapAmisAccionesAlertasGrid.jsp
 * @author Luis Ibarra
 *
 */
public class SapAmisRespuestaAlertas {
	protected long idAlerta;
	protected String sistema;
	protected String alerta;
	protected String vin;
	protected long idRelacion;
	
	protected Collection<SapAmisRespuestaAccionesAlertas> acciones;

	public long getIdAlerta() {
		return idAlerta;
	}

	public void setIdAlerta(long idAlerta) {
		this.idAlerta = idAlerta;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getAlerta() {
		return alerta;
	}

	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}

	public Collection<SapAmisRespuestaAccionesAlertas> getAcciones() {
		return this.acciones;
	}

	
	public void setAcciones(Collection<SapAmisRespuestaAccionesAlertas> acciones) {
		this.acciones = acciones;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public long getIdRelacion() {
		return idRelacion;
	}

	public void setIdRelacion(long idRelacion) {
		this.idRelacion = idRelacion;
	}
}
