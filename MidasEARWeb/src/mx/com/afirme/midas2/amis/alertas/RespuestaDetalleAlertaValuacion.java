//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-05/12/2011 11:54 AM(foreman)-)
//


package mx.com.afirme.midas2.amis.alertas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaDetalleAlertaValuacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaDetalleAlertaValuacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numSiniestro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numValuacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaDetalleAlertaValuacion", propOrder = {
    "cia",
    "fecha",
    "monto",
    "noSerie",
    "numSiniestro",
    "numValuacion"
})
public class RespuestaDetalleAlertaValuacion {

    protected String cia;
    protected String fecha;
    protected String monto;
    protected String noSerie;
    protected String numSiniestro;
    protected String numValuacion;

    /**
     * Gets the value of the cia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCia() {
        return cia;
    }

    /**
     * Sets the value of the cia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCia(String value) {
        this.cia = value;
    }

    /**
     * Gets the value of the fecha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the monto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonto() {
        return monto;
    }

    /**
     * Sets the value of the monto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonto(String value) {
        this.monto = value;
    }

    /**
     * Gets the value of the noSerie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoSerie() {
        return noSerie;
    }

    /**
     * Sets the value of the noSerie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoSerie(String value) {
        this.noSerie = value;
    }

    /**
     * Gets the value of the numSiniestro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSiniestro() {
        return numSiniestro;
    }

    /**
     * Sets the value of the numSiniestro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSiniestro(String value) {
        this.numSiniestro = value;
    }

    /**
     * Gets the value of the numValuacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumValuacion() {
        return numValuacion;
    }

    /**
     * Sets the value of the numValuacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumValuacion(String value) {
        this.numValuacion = value;
    }

}
