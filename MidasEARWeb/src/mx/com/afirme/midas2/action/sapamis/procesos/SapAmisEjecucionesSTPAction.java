package mx.com.afirme.midas2.action.sapamis.procesos;

import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisEjecucionesSTPService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

/*******************************************************************************
 * Nombre Interface: 	SapAmisBitacorasAction.
 * 
 * Descripcion: 		Action para la ejecucion de los Procesos de Poblado.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/procesos/stp")
public class SapAmisEjecucionesSTPAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private SapAmisEjecucionesSTPService sapAmisEjecucionesSTPService;
	private SapAmisUtilsService sapAmisUtilsService;
	private String fechaInicio;
	private String fechaFin;

	@Action(value = "emisionPoblado", results = { @Result(name = SUCCESS, type="json")})
	public String emisionPoblado() {
		sapAmisEjecucionesSTPService.emisionPoblado(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Action(value = "siniestrosPoblado", results = { @Result(name = SUCCESS, type="json")})
	public String siniestrosPoblado() {
		sapAmisEjecucionesSTPService.siniestrosPoblado(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Action(value = "rechazosPoblado", results = { @Result(name = SUCCESS, type="json")})
	public String rechazosPoblado() {
		sapAmisEjecucionesSTPService.rechazosPoblado(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Action(value = "pttPoblado", results = { @Result(name = SUCCESS, type="json")})
	public String pttPoblado() {
		sapAmisEjecucionesSTPService.pttPoblado(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Action(value = "salvamentoPoblado", results = { @Result(name = SUCCESS, type="json")})
	public String salvamentoPoblado() {
		sapAmisEjecucionesSTPService.salvamentoPoblado(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Action(value = "roboPobladoREPUVE", results = { @Result(name = SUCCESS, type="json")})
	public String roboPobladoREPUVE() {
		sapAmisEjecucionesSTPService.roboPobladoREPUVE(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Action(value = "recuperacionPobladoREPUVE", results = { @Result(name = SUCCESS, type="json")})
	public String recuperacionPobladoREPUVE() {
		sapAmisEjecucionesSTPService.recuperacionPobladoREPUVE(sapAmisUtilsService.convertToDateStr(fechaInicio), sapAmisUtilsService.convertToDateStr(fechaFin));
		return SUCCESS;
	}

	@Autowired
	@Qualifier("sapAmisEjecucionesSTPServiceEJB")
	public void setSapAmisEjecucionesSTPService(SapAmisEjecucionesSTPService sapAmisEjecucionesSTPService) {
		this.sapAmisEjecucionesSTPService = sapAmisEjecucionesSTPService;
	}

	@Autowired
	@Qualifier("sapAmisUtilsServiceEJB")
	public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
		this.sapAmisUtilsService = sapAmisUtilsService;
	}
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}
