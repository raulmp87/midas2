package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisValuacion;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisValuacionService;


/*******************************************************************************
 * Nombre Interface: 	SapAmisValuacionAction.
 * 
 * Descripcion: 		
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/valuacion")
public class SapAmisValuacionAction extends ActionSupport {
    private static final long serialVersionUID = 1L;
    // Servicios 
    private SapAmisValuacionService sapAmisValuacionService;

    // Variables
    private int numRegXPag;
    private int numPagina;
	private List<SapAmisValuacion> sapAmisValuacionList = new ArrayList<SapAmisValuacion>();
	private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
	
	@Action(value = "obtenerPorFiltros", results = { 
	        @Result(name = "json", type="json", params = {
	                "includeProperties", "sapAmisValuacionList"+
	                		"sapAmisValuacionList\\[\\d+\\],"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.id,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "sapAmisValuacionList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.repuveNivNci,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.repuveNivNci\\id,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.repuveNivNci\\niv,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.repuveNivNci\\nci,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.numeroSiniestro,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.numeroValuacion,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.fechaValuacion,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.montoValuacion,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.catTipoAfectado,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.catTipoAfectado\\id,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.catTipoAfectado\\descCatTipoAfectado,"+
	                        "sapAmisValuacionList\\[\\d+\\]\\.catTipoAfectado\\estatus,"
	        })
    })
	public String obtenerPorFiltros(){
        setSapAmisValuacionList(sapAmisValuacionService.obtenerPorFiltros(parametrosConsulta,numRegXPag, numPagina));
        return "json";
    }
	
	@Autowired
    @Qualifier("sapAmisValuacionServiceEJB")
    public void setSapAmisValuacionService(SapAmisValuacionService sapAmisValuacionService) {
        this.sapAmisValuacionService = sapAmisValuacionService;
    }
	
	public List<SapAmisValuacion> getSapAmisValuacionList() {
        return sapAmisValuacionList;
    }

    public void setSapAmisValuacionList(List<SapAmisValuacion> sapAmisValuacionList) {
        this.sapAmisValuacionList = sapAmisValuacionList;
    }
	
    public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }
}
	                        