package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSalvamento;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisSalvamentoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

/*******************************************************************************
 * Nombre Interface: 	SapAmisSalvamentoAction.
 * 
 * Descripcion: 		Action que se utiliza para el manejo del Modulo de Salvamento.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/salvamento")
public class SapAmisSalvamentoAction extends ActionSupport {
    private static final long serialVersionUID = 1L;
    
    //Servicios
    private SapAmisSalvamentoService sapAmisSalvamentoService;
    private SapAmisUtilsService sapAmisUtilsService;

    //Variables
    private int numRegXPag;
    private int numPagina;
	private String fechaInicio;
	private String fechaFin;
    private List<SapAmisSalvamento> sapAmisSalvamentoList = new ArrayList<SapAmisSalvamento>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    private static final Long ESTATUS = new Long(1);
    
    @Action(value = "sendRegSapAmis", results = { @Result(name = SUCCESS, type="json")})
    public String sendRegSapAmis() {
    	if(fechaInicio != null && fechaFin != null){
    		if(fechaInicio.equals("TODO") && fechaFin.equals("TODO")){
    			sapAmisSalvamentoService.sendRegSapAmis();
    		}else{
                parametrosConsulta = new ParametrosConsulta();
                parametrosConsulta.setEstatus(ESTATUS);
                parametrosConsulta.setFechaOperacionIni(sapAmisUtilsService.convertToDateStr(fechaInicio));
                parametrosConsulta.setFechaOperacionFin(sapAmisUtilsService.convertToDateStr(fechaFin));
                sapAmisSalvamentoService.sendRegSapAmis(parametrosConsulta);
    		}
    	}
        return SUCCESS;
    }       

    @Action(value = "obtenerPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "sapAmisSalvamentoList"+
                    		"sapAmisSalvamentoList\\[\\d+\\],"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.noSerie,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.noSiniestro,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoAfectado\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoAfectado\\.descCatTipoAfectado,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoAfectado\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoVenta\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoVenta\\.descCatTipoVenta,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoVenta\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.importeVenta,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.fechaVenta,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.noFactura,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.rfcFactura,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoPersona\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoPersona\\.descCatTipoPersona,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoPersona\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catEstVehPago\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catEstVehPago\\.descCatEstVehPago,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catEstVehPago\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.remarcado,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoTransporte\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoTransporte\\.descCatTipoTransporte,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catTipoTransporte\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catMarcaVehiculo\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catMarcaVehiculo\\.descCatMarcaVehiculo,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catMarcaVehiculo\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catSubMarcaVehiculo\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catSubMarcaVehiculo\\.descCatSubMarcaVehiculo,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.catSubMarcaVehiculo\\.estatus,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.anoModelo,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "sapAmisSalvamentoList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
            })
    })
    public String obtenerPorFiltros(){
        setSapAmisSalvamentoList(sapAmisSalvamentoService.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Autowired
    @Qualifier("sapAmisSalvamentoServiceEJB")
    public void setSapAmisSalvamentoService(SapAmisSalvamentoService sapAmisSalvamentoService) {
    	this.sapAmisSalvamentoService = sapAmisSalvamentoService;
    }

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
    public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
        this.sapAmisUtilsService = sapAmisUtilsService;
    }

    public List<SapAmisSalvamento> getSapAmisSalvamentoList() {
    	return sapAmisSalvamentoList;
    }

    public void setSapAmisSalvamentoList(List<SapAmisSalvamento> sapAmisSalvamentoList) {
    	this.sapAmisSalvamentoList = sapAmisSalvamentoList;
    }

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}