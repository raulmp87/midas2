package mx.com.afirme.midas2.action.sapamis.accionesalertas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.service.sapamis.accionesalertas.SapAmisAccionesRelacionService;

@Component
@Scope("prototype")
@Namespace("/sapamis/acciones/relacion")
public class SapAmisAccionesRelacionAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private SapAmisAccionesRelacionService sapAmisAccionesRelacionService;
	private List<SapAmisAccionesRelacion> sapAmisAccionesRelacionList = new ArrayList<SapAmisAccionesRelacion>();

	@Action(value = "findAll", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "sapAmisAccionesRelacionList"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\],"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.idSapAmisAccionesRelacion,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.idCotizacion,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.vin,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.id,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.descCatAlertasSapAmis,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.catSistemas,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.catSistemas\\.id,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.catSistemas\\.descCatSistemas,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.catSistemas\\.estatus,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.catAlertasSapAmis\\.estatus,"+
		                 "sapAmisAccionesRelacionList\\[\\d+\\]\\.tipo,"
		})
	})
	public String findAll(){
		setSapAmisAccionesRelacionList(sapAmisAccionesRelacionService.findAll());
		return "json";
	}

	@Autowired
	@Qualifier("sapAmisAccionesRelacionServiceEJB")
	public void setSapAmisAccionesRelacionService(SapAmisAccionesRelacionService sapAmisAccionesRelacionService) {
		this.sapAmisAccionesRelacionService = sapAmisAccionesRelacionService;
	}

	public List<SapAmisAccionesRelacion> getSapAmisAccionesRelacionList() {
		return sapAmisAccionesRelacionList;
	}

	public void setSapAmisAccionesRelacionList(List<SapAmisAccionesRelacion> sapAmisAccionesRelacionList) {
		this.sapAmisAccionesRelacionList = sapAmisAccionesRelacionList;
	}
}