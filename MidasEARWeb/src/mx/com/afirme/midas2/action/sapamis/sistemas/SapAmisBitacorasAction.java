package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisBitacorasService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisBitacorasAction.
 * 
 * Descripcion: 		Action para la ejecucion de los Servicios de Bitacoras.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/bitacoras")
public class SapAmisBitacorasAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private SapAmisBitacorasService sapAmisBitacorasService;
	private ParametrosConsulta parametrosConsulta = new ParametrosConsulta(); 
	private List<SapAmisBitacoras> sapAmisBitacorasList = new ArrayList<SapAmisBitacoras>();
	
    @Action(value = "obtenerCifras", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "sapAmisBitacorasList"+
                    		"sapAmisBitacorasList\\[\\d+\\],"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.id,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.catOperaciones,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.catOperaciones\\.id,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.catSistemas,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.catSistemas\\.id,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.idSapAmisRegistro,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.catEstatusBitacora,"+
                            "sapAmisBitacorasList\\[\\d+\\]\\.catEstatusBitacora\\.id"
            })
    })
    public String obtenerCifras(){
        setSapAmisBitacorasList(sapAmisBitacorasService.obtenerCifras(parametrosConsulta));
        return "json";
    }

	@Autowired
	@Qualifier("sapAmisBitacorasServiceEJB")
	public void setSapAmisBitacorasService(SapAmisBitacorasService sapAmisBitacorasService) {
		this.sapAmisBitacorasService = sapAmisBitacorasService;
	}

	public List<SapAmisBitacoras> getSapAmisBitacorasList() {
		return sapAmisBitacorasList;
	}

	public void setSapAmisBitacorasList(List<SapAmisBitacoras> sapAmisBitacorasList) {
		this.sapAmisBitacorasList = sapAmisBitacorasList;
	}

	public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
		this.parametrosConsulta = parametrosConsulta;
	}
}