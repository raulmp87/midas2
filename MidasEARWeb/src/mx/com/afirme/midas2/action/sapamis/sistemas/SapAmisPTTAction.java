package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPTT;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisPTTService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

/*******************************************************************************
 * Nombre Interface: 	SapAmisPTTAction.
 * 
 * Descripcion: 		Action que se utiliza para el manejo del Modulo de Perdidas Totales.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/ptt")
public class SapAmisPTTAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	// Servicios
	private SapAmisPTTService sapAmisPTTService;
    private SapAmisUtilsService sapAmisUtilsService;

	// Variables
    private int numRegXPag;
    private int numPagina;
	private String fechaInicio;
	private String fechaFin;
	private List<SapAmisPTT> sapAmisPTTList = new ArrayList<SapAmisPTT>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    private static final Long ESTATUS = new Long(1);
    
    @Action(value = "sendRegSapAmis", results = { @Result(name = SUCCESS, type="json")})
    public String sendRegSapAmis() {
    	if(fechaInicio != null && fechaFin != null){
    		if(fechaInicio.equals("TODO") && fechaFin.equals("TODO")){
    			sapAmisPTTService.sendRegSapAmis();
    		}else{
                parametrosConsulta = new ParametrosConsulta();
                parametrosConsulta.setEstatus(ESTATUS);
                parametrosConsulta.setFechaOperacionIni(sapAmisUtilsService.convertToDateStr(fechaInicio));
                parametrosConsulta.setFechaOperacionFin(sapAmisUtilsService.convertToDateStr(fechaFin));
                sapAmisPTTService.sendRegSapAmis(parametrosConsulta);
    		}
    	}
        return SUCCESS;
	}

	@Action(value = "obtenerPorFiltros", results = { @Result(name = "json", type = "json", params = {
			"includeProperties", "sapAmisPTTList"+
                    "sapAmisPTTList\\[\\d+\\],"+
                    "sapAmisPTTList\\[\\d+\\]\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.noSerie,"+
                    "sapAmisPTTList\\[\\d+\\]\\.noSiniestro,"+
                    "sapAmisPTTList\\[\\d+\\]\\.fechaSiniestro,"+
                    "sapAmisPTTList\\[\\d+\\]\\.porcentajePerdida,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoAfectado\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoAfectado\\.descCatTipoAfectado,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoAfectado\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catCausaSiniestro\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catCausaSiniestro\\.descCatCausaSiniestro,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catCausaSiniestro\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.noMotor,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoServicio\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoServicio\\.descCatTipoServicio,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoServicio\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catDano\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catDano\\.descCatDano,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catDano\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoMonto\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoMonto\\.descCatTipoMonto,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoMonto\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.montoPagado,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoPago\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoPago\\.descCatTipoPago,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoPago\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.fechaPago,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catEstVehPago\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catEstVehPago\\.descCatEstVehPago,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catEstVehPago\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoTransporte\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoTransporte\\.descCatTipoTransporte,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catTipoTransporte\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catMarcaVehiculo\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catMarcaVehiculo\\.descCatMarcaVehiculo,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catMarcaVehiculo\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catSubMarcaVehiculo\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catSubMarcaVehiculo\\.descCatSubMarcaVehiculo,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catSubMarcaVehiculo\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.anoModelo,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catCausaCancelacion\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catCausaCancelacion\\.descCatCausaCancelacion,"+
                    "sapAmisPTTList\\[\\d+\\]\\.catCausaCancelacion\\.estatus,"+
                    "sapAmisPTTList\\[\\d+\\]\\.fechaCancelacion,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                    "sapAmisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
		}) 
	})
	public String obtenerPorFiltros() {
        setSapAmisPTTList(sapAmisPTTService.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina));
		return "json";
	}

	@Autowired
	@Qualifier("sapAmisPTTServiceEJB")
	public void setSapAmisPTTService(SapAmisPTTService sapAmisPTTService) {
		this.sapAmisPTTService = sapAmisPTTService;
	}

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
    public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
        this.sapAmisUtilsService = sapAmisUtilsService;
    }

	public List<SapAmisPTT> getSapAmisPTTList() {
		return sapAmisPTTList;
	}

	public void setSapAmisPTTList(List<SapAmisPTT> sapAmisPTTList) {
		this.sapAmisPTTList = sapAmisPTTList;
	}

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}