package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisRechazos;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisRechazosService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisRechazosAction.
 * 
 * Descripcion: 		Action que se utiliza para el manejo del Modulo de Rechazos.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/rechazos")
public class SapAmisRechazosAction extends ActionSupport {
    private static final long serialVersionUID = 1L;

    //Servicios
    private SapAmisRechazosService sapAmisRechazosService;
    private SapAmisUtilsService sapAmisUtilsService;
    
    //Variables
    private int numRegXPag;
    private int numPagina;
	private String fechaInicio;
	private String fechaFin;
    private List<SapAmisRechazos> sapAmisRechazosList = new ArrayList<SapAmisRechazos>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    private static final Long ESTATUS = new Long(1);
    
    @Action(value = "sendRegSapAmis", results = { @Result(name = SUCCESS, type="json")})
    public String sendRegSapAmis() {
    	if(fechaInicio != null && fechaFin != null){
    		if(fechaInicio.equals("TODO") && fechaFin.equals("TODO")){
    			sapAmisRechazosService.sendRegSapAmis();
    		}else{
                parametrosConsulta = new ParametrosConsulta();
                parametrosConsulta.setEstatus(ESTATUS);
                parametrosConsulta.setFechaOperacionIni(sapAmisUtilsService.convertToDateStr(fechaInicio));
                parametrosConsulta.setFechaOperacionFin(sapAmisUtilsService.convertToDateStr(fechaFin));
                sapAmisRechazosService.sendRegSapAmis(parametrosConsulta);
    		}
    	}
        return SUCCESS;
    }

    @Action(value = "obtenerPorFiltros", results = { @Result(name = "json", type="json", params = {
                "includeProperties", "sapAmisRechazosList"+
                        "sapAmisRechazosList\\[\\d+\\],"+
                        "sapAmisRechazosList\\[\\d+\\]\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.numeroSiniestro,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catTipoBeneficio,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catTipoBeneficio\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catTipoBeneficio\\.descCatTipoBeneficio,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catTipoBeneficio\\.estatus,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catCausaBeneficio,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catCausaBeneficio\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catCausaBeneficio\\.descCatCausaBeneficio,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.catCausaBeneficio\\.estatus,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catBeneficioRelacion\\.estatus,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.desistimiento,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.montoRechazo,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.detectadoSAP,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.observaciones,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catAlertasRechazos,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catAlertasRechazos\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catAlertasRechazos\\.descCatAlertasRechazos,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catAlertasRechazos\\.claveAmis,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.catAlertasRechazos\\.estatus,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.fecha,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.operacion,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.estatus,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.fechaOperacion,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.fechaRegistro,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.fechaEnvio,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                        "sapAmisRechazosList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
        })
    })
    public String obtenerPorFiltros(){
        setSapAmisRechazosList(sapAmisRechazosService.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Autowired
    @Qualifier("sapAmisRechazosServiceEJB")
    public void setSapAmisRechazosService(SapAmisRechazosService sapAmisRechazosService) {
        this.sapAmisRechazosService = sapAmisRechazosService;
    }

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
    public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
        this.sapAmisUtilsService = sapAmisUtilsService;
    }

    public List<SapAmisRechazos> getSapAmisRechazosList() {
        return sapAmisRechazosList;
    }

    public void setSapAmisRechazosList(List<SapAmisRechazos> sapAmisRechazosList) {
        this.sapAmisRechazosList = sapAmisRechazosList;
    }

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}