package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPrevenciones;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisPrevencionesService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisPrevencionesAction.
 * 
 * Descripcion: 		Action que se utiliza para el manejo del Modulo de Prevenciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/prevenciones")
public class SapAmisPrevencionesAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	// Servicios
	private SapAmisPrevencionesService sapAmisPrevencionesService;
    private SapAmisUtilsService sapAmisUtilsService;

	// Variables
    private int numRegXPag;
    private int numPagina;
	private String fechaInicio;
	private String fechaFin;
	private List<SapAmisPrevenciones> sapAmisPrevencionesList = new ArrayList<SapAmisPrevenciones>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
	private static final Long ESTATUS = new Long(1);
    
    @Action(value = "sendRegSapAmis", results = { @Result(name = SUCCESS, type="json")})
    public String sendRegSapAmis() {
    	if(fechaInicio != null && fechaFin != null){
    		if(fechaInicio.equals("TODO") && fechaFin.equals("TODO")){
    			sapAmisPrevencionesService.sendRegSapAmis();
    		}else{
                parametrosConsulta = new ParametrosConsulta();
                parametrosConsulta.setEstatus(ESTATUS);
                parametrosConsulta.setFechaOperacionIni(sapAmisUtilsService.convertToDateStr(fechaInicio));
                parametrosConsulta.setFechaOperacionFin(sapAmisUtilsService.convertToDateStr(fechaFin));
		        sapAmisPrevencionesService.sendRegSapAmis(parametrosConsulta);
    		}
    	}
        return SUCCESS;
	}

	@Action(value = "obtenerPorFiltros", results = { @Result(name = "json", type = "json", params = {
			"includeProperties", "sapAmisPrevencionesList"+
                    "sapAmisPrevencionesList\\[\\d+\\],"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.id,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.numSerie,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.causaPrevencion,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.causaPrevencion\\.id,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.causaPrevencion\\.descCatCausaPrevencion,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.causaPrevencion\\.estatus,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.placa,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.observaciones,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                    "sapAmisPrevencionesList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
		}) 
	})
	public String obtenerPorFiltros() {
        setSapAmisPrevencionesList(sapAmisPrevencionesService.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina));
		return "json";
	}

	@Autowired
	@Qualifier("sapAmisPrevencionesServiceEJB")
	public void setSapAmisPrevencionesService(SapAmisPrevencionesService sapAmisPrevencionesService) {
		this.sapAmisPrevencionesService = sapAmisPrevencionesService;
	}

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
	public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
		this.sapAmisUtilsService = sapAmisUtilsService;
	}

	public List<SapAmisPrevenciones> getSapAmisPrevencionesList() {
		return sapAmisPrevencionesList;
	}

	public void setSapAmisPrevencionesList(List<SapAmisPrevenciones> sapAmisPrevencionesList) {
		this.sapAmisPrevencionesList = sapAmisPrevencionesList;
	}

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}