package mx.com.afirme.midas2.action.sapamis.accionesalertas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;
import mx.com.afirme.midas2.service.sapamis.accionesalertas.SapAmisAccionesService;

@Component
@Scope("prototype")
@Namespace("/sapamis/acciones")
public class SapAmisAccionesAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private SapAmisAccionesService sapAmisAccionesService;
	private List<SapAmisAcciones> sapAmisAccionesList = new ArrayList<SapAmisAcciones>();

	@Action(value = "findAll", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "sapAmisAccionesList"+
		                 "sapAmisAccionesList\\[\\d+\\],"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.idSapAmisAcciones,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.descripcionAccion,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.idSapAmisAccionesRelacion,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.idCotizacion,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.vin,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.id,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.descCatAlertasSapAmis,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas\\.id,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas\\.descCatSistemas,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas\\.estatus,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.estatus,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.sapAmisAccionesRelacion\\.tipo,"+
		                 "sapAmisAccionesList\\[\\d+\\]\\.estatus,"
		})
	})
	public String findAll(){
		setSapAmisAccionesList(sapAmisAccionesService.findAll());
		return "json";
	}

	@Autowired
	@Qualifier("sapAmisAccionesServiceEJB")
	public void setSapAmisAccionesService(SapAmisAccionesService sapAmisAccionesService) {
		this.sapAmisAccionesService = sapAmisAccionesService;
	}

	public List<SapAmisAcciones> getSapAmisAccionesList() {
		return sapAmisAccionesList;
	}

	public void setSapAmisAccionesList(List<SapAmisAcciones> sapAmisAccionesList) {
		this.sapAmisAccionesList = sapAmisAccionesList;
	}
}