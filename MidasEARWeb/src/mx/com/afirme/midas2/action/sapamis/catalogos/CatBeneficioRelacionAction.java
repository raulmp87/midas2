package mx.com.afirme.midas2.action.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatBeneficioRelacion;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatBeneficioRelacionService;

@Component
@Scope("prototype")
@Namespace("/sapamis/catalogos/beneficiorelacion")
public class CatBeneficioRelacionAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private CatBeneficioRelacionService catBeneficioRelacionService;
	private List<CatBeneficioRelacion> catBeneficioRelacionList = new ArrayList<CatBeneficioRelacion>();

	@Action(value = "findByStatus", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "catBeneficioRelacionList"+
		                 "catBeneficioRelacionList\\[\\d+\\],"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.id,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catTipoBeneficio,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catTipoBeneficio\\.id,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catTipoBeneficio\\.descCatTipoBeneficio,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catTipoBeneficio\\.estatus,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catCausaBeneficio,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catCausaBeneficio\\.id,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catCausaBeneficio\\.descCatCausaBeneficio,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.catCausaBeneficio\\.estatus,"+
		                 "catBeneficioRelacionList\\[\\d+\\]\\.estatus,"
		})
	})
	public String findByStatus(){
		setCatBeneficioRelacionList(catBeneficioRelacionService.findByStatus(true));
		return "json";
	}

	@Autowired
	@Qualifier("catBeneficioRelacionServiceEJB")
	public void setCatBeneficioRelacionService(CatBeneficioRelacionService catBeneficioRelacionService) {
		this.catBeneficioRelacionService = catBeneficioRelacionService;
	}

	public List<CatBeneficioRelacion> getCatBeneficioRelacionList() {
		return catBeneficioRelacionList;
	}

	public void setCatBeneficioRelacionList(List<CatBeneficioRelacion> catBeneficioRelacionList) {
		this.catBeneficioRelacionList = catBeneficioRelacionList;
	}
}