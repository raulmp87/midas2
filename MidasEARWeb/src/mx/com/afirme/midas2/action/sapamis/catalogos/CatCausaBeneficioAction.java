package mx.com.afirme.midas2.action.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaBeneficio;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatCausaBeneficioService;

@Component
@Scope("prototype")
@Namespace("/sapamis/catalogos/causabeneficio")
public class CatCausaBeneficioAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private CatCausaBeneficioService catCausaBeneficioService;
	private List<CatCausaBeneficio> catCausaBeneficioList = new ArrayList<CatCausaBeneficio>();

	@Action(value = "findByStatus", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "catCausaBeneficioList"+
		                 "catCausaBeneficioList\\[\\d+\\],"+
		                 "catCausaBeneficioList\\[\\d+\\]\\.id,"+
		                 "catCausaBeneficioList\\[\\d+\\]\\.descCatCausaBeneficio,"+
		                 "catCausaBeneficioList\\[\\d+\\]\\.estatus,"
		})
	})
	public String findByStatus(){
		setCatCausaBeneficioList(catCausaBeneficioService.findByStatus(true));
		return "json";
	}

	@Autowired
	@Qualifier("catCausaBeneficioServiceEJB")
	public void setCatCausaBeneficioService(CatCausaBeneficioService catCausaBeneficioService) {
		this.catCausaBeneficioService = catCausaBeneficioService;
	}

	public List<CatCausaBeneficio> getCatCausaBeneficioList() {
		return catCausaBeneficioList;
	}

	public void setCatCausaBeneficioList(List<CatCausaBeneficio> catCausaBeneficioList) {
		this.catCausaBeneficioList = catCausaBeneficioList;
	}
}