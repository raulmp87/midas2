package mx.com.afirme.midas2.action.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;
import mx.com.afirme.midas2.dto.sapamis.catalogos.RelSistemaOperacion;
import mx.com.afirme.midas2.service.sapamis.catalogos.RelSistemaOperacionService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSistemasService;

/*******************************************************************************
 * Nombre Interface: 	RelSistemaOperacionAction.
 * 
 * Descripcion: 		Action para el manejo de transacciones de los Catalogos
 * 						de Sistemas y operaciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/catalogos/relacionsistemaoperacion")
public class RelSistemaOperacionAction  extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private RelSistemaOperacionService relSistemaOperacionService;
	private CatSistemasService catSistemasService;
	private List<RelSistemaOperacion> relSistemaOperacionList = new ArrayList<RelSistemaOperacion>();
	private CatSistemas catSistemas = new CatSistemas();
	private long modulo;

	@Action(value = "findByStatus", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "relSistemaOperacionList"+
		                 "relSistemaOperacionList\\[\\d+\\],"+
		                 "relSistemaOperacionList\\[\\d+\\]\\.id,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catOperaciones,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catOperaciones\\.id,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catOperaciones\\.descCatOperaciones,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catSistemas,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catSistemas\\.id,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catSistemas\\.descCatSistemas,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catSistemas\\.estatusProcesoExtraccion,"+
                         "relSistemaOperacionList\\[\\d+\\]\\.catSistemas\\.estatusProcesoEnvio,"+
		                 "relSistemaOperacionList\\[\\d+\\]\\.estatus,"+
		                 "relSistemaOperacionList\\[\\d+\\]\\.modulo,"
		})
	})
	public String findByStatus(){
		setRelSistemaOperacionList(relSistemaOperacionService.findByStatusAndOperation(true, modulo));
		return "json";
	}

	@Action(value = "updateEstatus", results = { @Result(name = SUCCESS, type="json")})
	public String updateEstatus(){
		catSistemasService.guardarRegistro(catSistemas);
		return SUCCESS;
	}

	@Autowired
	@Qualifier("relSistemaOperacionEJB")
	public void setRelSistemaOperacionService(RelSistemaOperacionService relSistemaOperacionService) {
		this.relSistemaOperacionService = relSistemaOperacionService;
	}

	@Autowired
	@Qualifier("catSistemasServiceEJB")
	public void setCatSistemasService(CatSistemasService catSistemasService) {
		this.catSistemasService = catSistemasService;
	}

	public List<RelSistemaOperacion> getRelSistemaOperacionList() {
		return relSistemaOperacionList;
	}

	public void setRelSistemaOperacionList(List<RelSistemaOperacion> relSistemaOperacionList) {
		this.relSistemaOperacionList = relSistemaOperacionList;
	}

	public void setCatSistemas(CatSistemas catSistemas) {
		this.catSistemas = catSistemas;
	}

	public long getModulo() {
		return modulo;
	}

	public void setModulo(long modulo) {
		this.modulo = modulo;
	}
}