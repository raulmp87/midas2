package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSiniestros;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisSiniestrosService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisSiniestrosAction.
 * 
 * Descripcion: 		Action que se utiliza para el manejo del Modulo de Siniestros.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/siniestros")
public class SapAmisSiniestrosAction extends ActionSupport {
    private static final long serialVersionUID = 1L;

    // Servicios 
    private SapAmisSiniestrosService sapAmisSiniestrosService;
    private SapAmisUtilsService sapAmisUtilsService;

    // Variables
    private int numRegXPag;
    private int numPagina;
	private String fechaInicio;
	private String fechaFin;
    private List<SapAmisSiniestros> sapAmisSiniestrosList = new ArrayList<SapAmisSiniestros>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    private static final Long ESTATUS = new Long(1);
    
    @Action(value = "sendRegSapAmis", results = { @Result(name = SUCCESS, type="json")})
    public String sendRegSapAmis() {
    	if(fechaInicio != null && fechaFin != null){
    		if(fechaInicio.equals("TODO") && fechaFin.equals("TODO")){
		        sapAmisSiniestrosService.sendRegSapAmis();
    		}else{
                parametrosConsulta = new ParametrosConsulta();
                parametrosConsulta.setEstatus(ESTATUS);
                parametrosConsulta.setFechaOperacionIni(sapAmisUtilsService.convertToDateStr(fechaInicio));
                parametrosConsulta.setFechaOperacionFin(sapAmisUtilsService.convertToDateStr(fechaFin));
                sapAmisSiniestrosService.sendRegSapAmis(parametrosConsulta);
    		}
    	}
        return SUCCESS;
    }

    @Action(value = "obtenerPorFiltros", results = { 
        @Result(name = "json", type="json", params = {
                "includeProperties", "sapAmisSiniestrosList"+
                        "sapAmisSiniestrosList\\[\\d+\\],"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.numSiniestro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.poliza,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.inciso,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.fechaSiniestro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.serie,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.causaSiniestro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.causaSiniestro\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.causaSiniestro\\.descCatCausaSiniestro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.causaSiniestro\\.estatus,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.monto,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.claveSeycos,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.descCatUbicacionMunicipio,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.claveSeycos,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.catUbicacionPais,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.catUbicacionPais\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.catUbicacionPais\\.claveSeycos,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.catUbicacionPais\\.descCatUbicacionPais,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.catUbicacionPais\\.estatus,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.catUbicacionEstado\\.estatus,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.ubicacionSiniestro\\.estatus,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.conductorApPaterno,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.conductorApMaterno,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.conductorNombre,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.conductorRfc,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.conductorCurp,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.conductorTelefono,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.involucradoSiniestro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.involucradoSiniestro\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.involucradoSiniestro\\.descCatTipoAfectado,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.involucradoSiniestro\\.estatus,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                        "sapAmisSiniestrosList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones,"
        })
    })
    public String obtenerPorFiltros(){
        setSapAmisSiniestrosList(sapAmisSiniestrosService.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Autowired
    @Qualifier("sapAmisSiniestrosServiceEJB")
    public void setSapAmisSiniestrosService(SapAmisSiniestrosService sapAmisSiniestrosService) {
        this.sapAmisSiniestrosService = sapAmisSiniestrosService;
    }

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
    public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
        this.sapAmisUtilsService = sapAmisUtilsService;
    }

    public List<SapAmisSiniestros> getSapAmisSiniestrosList() {
        return sapAmisSiniestrosList;
    }

    public void setSapAmisSiniestrosList(List<SapAmisSiniestros> sapAmisSiniestrosList) {
        this.sapAmisSiniestrosList = sapAmisSiniestrosList;
    }

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}