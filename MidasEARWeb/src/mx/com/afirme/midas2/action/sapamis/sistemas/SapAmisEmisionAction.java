package mx.com.afirme.midas2.action.sapamis.sistemas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisEmision;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisEmisionService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisEmisionAction.
 * 
 * Descripcion: 		Action que se utiliza para el manejo del Modulo de Emision.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/sistemas/emision")
public class SapAmisEmisionAction extends ActionSupport {
    private static final long serialVersionUID = 1L;

    // Servicios
    private SapAmisEmisionService sapAmisEmisionService;
    private SapAmisUtilsService sapAmisUtilsService;
    
    // Variables
    private int numRegXPag;
    private int numPagina;
	private String fechaInicio;
	private String fechaFin;
    private List<SapAmisEmision> sapAmisEmisionList = new ArrayList<SapAmisEmision>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
	private static final Long ESTATUS = new Long(1);
    
    @Action(value = "sendRegSapAmis", results = { @Result(name = SUCCESS, type="json")})
    public String sendRegSapAmis() {
    	if(fechaInicio != null && fechaFin != null){
    		if(fechaInicio.equals("TODO") && fechaFin.equals("TODO")){
    			sapAmisEmisionService.sendRegSapAmis();
    		}else{
                parametrosConsulta = new ParametrosConsulta();
                parametrosConsulta.setEstatus(ESTATUS);
                parametrosConsulta.setFechaOperacionIni(sapAmisUtilsService.convertToDateStr(fechaInicio));
                parametrosConsulta.setFechaOperacionFin(sapAmisUtilsService.convertToDateStr(fechaFin));
		        sapAmisEmisionService.sendRegSapAmis(parametrosConsulta);
    		}
    	}
        return SUCCESS;
    }

    @Action(value = "obtenerPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "sapAmisEmisionList"+
                            "sapAmisEmisionList\\[\\d+\\],"+
                            "sapAmisEmisionList\\[\\d+\\]\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.noPoliza,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.inciso,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.fechaVigenciaInicio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.fechaVigenciaFin,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.noSerie,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.descCatSubMarcaVehiculo,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catMarcaVehiculo,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catMarcaVehiculo\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catMarcaVehiculo\\.descCatMarcaVehiculo,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catMarcaVehiculo\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catTipoTransporte,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catTipoTransporte\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catTipoTransporte\\.descCatTipoTransporte,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.catTipoTransporte\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.vehiculo\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.modelo,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.tipoPersona,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.tipoPersona\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.tipoPersona\\.descCatTipoPersona,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.tipoPersona\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.catTipoServicio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.catTipoServicio\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.catTipoServicio\\.descCatTipoServicio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.catTipoServicio\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente1ApPaterno,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente1ApMaterno,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente1Nombre,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente1RFC,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente1CURP,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente2ApPaterno,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente2ApMaterno,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.cliente2Nombre,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioTipoPersona,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioTipoPersona\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioTipoPersona\\.descCatTipoPersona,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioTipoPersona\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioApPaterno,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioApMaterno,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioNombre,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioRFC,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.beneficiarioCURP,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.canalVentas,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.canalVentas\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.canalVentas\\.descCatCanalVentas,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.canalVentas\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.agente,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.obligatorio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.fechaEmision,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.claveSeycos,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.descCatUbicacionMunicipio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.claveSeycos,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.catUbicacionPais,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.catUbicacionPais\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.catUbicacionPais\\.claveSeycos,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.catUbicacionPais\\.descCatUbicacionPais,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.catUbicacionPais\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.catUbicacionEstado\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionAsegurado\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.claveSeycos,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.descCatUbicacionMunicipio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.claveSeycos,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.catUbicacionPais,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.catUbicacionPais\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.catUbicacionPais\\.claveSeycos,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.catUbicacionPais\\.descCatUbicacionPais,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.catUbicacionPais\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.catUbicacionEstado\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.ubicacionEmision\\.estatus,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "sapAmisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
            })
        })
    public String obtenerPorFiltros(){
        setSapAmisEmisionList(sapAmisEmisionService.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Autowired
    @Qualifier("sapAmisEmisionServiceEJB")
    public void setSapAmisEmisionService(SapAmisEmisionService sapAmisEmisionService) {
        this.sapAmisEmisionService = sapAmisEmisionService;
    }

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
	public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
		this.sapAmisUtilsService = sapAmisUtilsService;
	}

	public List<SapAmisEmision> getSapAmisEmisionList() {
        return sapAmisEmisionList;
    }

    public void setSapAmisEmisionList(List<SapAmisEmision> sapAmisEmisionList) {
        this.sapAmisEmisionList = sapAmisEmisionList;
    }

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}
	
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}