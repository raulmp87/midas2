package mx.com.afirme.midas2.action.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoBeneficio;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatTipoBeneficioService;

@Component
@Scope("prototype")
@Namespace("/sapamis/catalogos/tipobeneficio")
public class CatTipoBeneficioAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private CatTipoBeneficioService catTipoBeneficioService;
	private List<CatTipoBeneficio> catTipoBeneficioList = new ArrayList<CatTipoBeneficio>();

	@Action(value = "findByStatus", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "catTipoBeneficioList"+
		                 "catTipoBeneficioList\\[\\d+\\],"+
		                 "catTipoBeneficioList\\[\\d+\\]\\.id,"+
		                 "catTipoBeneficioList\\[\\d+\\]\\.descCatTipoBeneficio,"+
		                 "catTipoBeneficioList\\[\\d+\\]\\.estatus,"
		})
	})
	public String findByStatus(){
		setCatTipoBeneficioList(catTipoBeneficioService.findByStatus(true));
		return "json";
	}

	@Autowired
	@Qualifier("catTipoBeneficioServiceEJB")
	public void setCatTipoBeneficioService(CatTipoBeneficioService catTipoBeneficioService) {
		this.catTipoBeneficioService = catTipoBeneficioService;
	}

	public List<CatTipoBeneficio> getCatTipoBeneficioList() {
		return catTipoBeneficioList;
	}

	public void setCatTipoBeneficioList(List<CatTipoBeneficio> catTipoBeneficioList) {
		this.catTipoBeneficioList = catTipoBeneficioList;
	}
}