package mx.com.afirme.midas2.action.sapamis.accionesalertas;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesLog;
import mx.com.afirme.midas2.service.sapamis.accionesalertas.SapAmisAccionesLogService;

@Component
@Scope("prototype")
@Namespace("/sapamis/acciones/log")
public class SapAmisAccionesLogAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private SapAmisAccionesLogService sapAmisAccionesLogService;
	private List<SapAmisAccionesLog> sapAmisAccionesLogList = new ArrayList<SapAmisAccionesLog>();

	@Action(value = "findAll", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "sapAmisAccionesLogList"+
		                 "sapAmisAccionesLogList\\[\\d+\\],"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.idSapAmisAccionesLog,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.fecha,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.tipoModificacion,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.usuario,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.idSapAmisAcciones,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.descripcionAccion,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.idSapAmisAccionesRelacion,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.idCotizacion,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.vin,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.id,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.descCatAlertasSapAmis,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas\\.id,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas\\.descCatSistemas,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.catSistemas\\.estatus,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.catAlertasSapAmis\\.estatus,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.sapAmisAccionesRelacion\\.tipo,"+
		                 "sapAmisAccionesLogList\\[\\d+\\]\\.sapAmisAcciones\\.estatus,"
		})
	})
	public String findAll(){
		setSapAmisAccionesLogList(sapAmisAccionesLogService.findAll());
		return "json";
	}

	@Autowired
	@Qualifier("sapAmisAccionesLogServiceEJB")
	public void setSapAmisAccionesLogService(SapAmisAccionesLogService sapAmisAccionesLogService) {
		this.sapAmisAccionesLogService = sapAmisAccionesLogService;
	}

	public List<SapAmisAccionesLog> getSapAmisAccionesLogList() {
		return sapAmisAccionesLogList;
	}

	public void setSapAmisAccionesLogList(List<SapAmisAccionesLog> sapAmisAccionesLogList) {
		this.sapAmisAccionesLogList = sapAmisAccionesLogList;
	}
}