package mx.com.afirme.midas2.action.sapamis.catalogos;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSapAmisSupervisionCampoService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.apache.log4j.Logger;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/siniestros/catalogo/supervisionCampo")
public class CatSapAmisSupervisionCampoAction extends CatalogoAction implements Preparable {
	private static final long serialVersionUID = 1L;

	private static final String JSP_MOSTRAR_CATALOGO = "/jsp/catalogos/sapamis/supervisionCampo/supervisionCampoCatalogo.jsp";
	private static final String JSP_MOSTRAR_GRID     = "/jsp/catalogos/sapamis/supervisionCampo/supervisionCampoGrid.jsp";
	private static final String JSP_VER_DETALLE      = "/jsp/catalogos/sapamis/supervisionCampo/supervisionCampoDetalle.jsp";

	private CatSapAmisSupervisionCampo catSapAmisSupervisionCampo;
	private CatSapAmisSupervisionCampoService catSapAmisSupervisionCampoService;
	private List<CatSapAmisSupervisionCampo> catSapAmisSupervisionCampoList;
	private Long id;
	private TipoAccionDTO catalogoTipoAccionDTO;
	private String accion;
	private String duplicado;

	public CatSapAmisSupervisionCampoAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	
	@Action(value = "mostrarCatalogo", results = { 
		@Result(name = SUCCESS, location = JSP_MOSTRAR_CATALOGO) 
	})
	public String mostrarCatalogo() {
		return SUCCESS;
	}

	@Action(value = "listar", results = { 
		@Result(name = SUCCESS, location = JSP_MOSTRAR_GRID) 
	})
	public String listar() {
		catSapAmisSupervisionCampoList = entidadService.findAll(CatSapAmisSupervisionCampo.class);
		return SUCCESS;
	}

	@Action(value = "verDetalle", results = { 
		@Result(name = SUCCESS, location = JSP_VER_DETALLE) 
	})
	public String verDetalle() {
		return SUCCESS;
	}

	@Action(value = "guardar", results = { 
		@Result(name = INPUT, location = JSP_MOSTRAR_CATALOGO),
		@Result(name = SUCCESS, location = JSP_MOSTRAR_CATALOGO) 
	})
	public String guardar() {
		if(catSapAmisSupervisionCampoService.guardar(catSapAmisSupervisionCampo)){
			catSapAmisSupervisionCampo = new CatSapAmisSupervisionCampo();
			setMensajeExito();
		}
		return SUCCESS;
	}

	@Action(value = "eliminar", results = { 
		@Result(name = INPUT, location = JSP_VER_DETALLE),
		@Result(name = SUCCESS, location = JSP_MOSTRAR_CATALOGO)
	})
	public String eliminar() {
		entidadService.remove(catSapAmisSupervisionCampo);
		catSapAmisSupervisionCampo = new CatSapAmisSupervisionCampo();
		setMensajeExito();
		return SUCCESS;
	}

	@Action(value = "listarFiltrado", results = { 
		@Result(name = SUCCESS, location = JSP_MOSTRAR_GRID) 
	})
	public String listarFiltrado() {
		catSapAmisSupervisionCampoList = catSapAmisSupervisionCampoService.obtenerPorFiltros(catSapAmisSupervisionCampo);
		return SUCCESS;
	}
	
	public void validateGuardar() {
		catSapAmisSupervisionCampo.setId(getId());
		duplicado="false";
		addErrors(catSapAmisSupervisionCampo, NewItemChecks.class, this, "catSapAmisSupervisionCampo");
		if (!this.hasErrors()){	
			if (accion.equals("1")){
				if(catSapAmisSupervisionCampoService.existeClaveAmis(catSapAmisSupervisionCampo.getClaveAmis())){
					addFieldError("claveAmis", getText("error.duplicado.campo"));		
					duplicado="true";
				}
			}
		}
	}

	public void validateEditar() {
		addErrors(catSapAmisSupervisionCampo, EditItemChecks.class, this, "catSapAmisSupervisionCampo");
	}
	
	public void prepareVerDetalle(){
		filtrarPorId();
	}
	
	public void setDuplicado(String duplicado) {
		this.duplicado = duplicado;
	}

	public String getDuplicado() {
		return duplicado;
	}
	
	@Autowired
	@Qualifier("catSapAmisSupervisionCampoEJB")
	public void setCatSapAmisSupervisionCampoService(CatSapAmisSupervisionCampoService catSapAmisSupervisionCampoService) {
		this.catSapAmisSupervisionCampoService = catSapAmisSupervisionCampoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;

	}
	
	public void prepare() throws Exception {
		filtrarPorId();
	}

	private void filtrarPorId(){
		if (getId() != null) {
			catSapAmisSupervisionCampo = entidadService.findById(CatSapAmisSupervisionCampo.class,getId());	
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CatSapAmisSupervisionCampo getCatSapAmisSupervisionCampo() {
		return catSapAmisSupervisionCampo;
	}

	public void setCatSapAmisSupervisionCampo(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo) {
		this.catSapAmisSupervisionCampo = catSapAmisSupervisionCampo;
	}

	public List<CatSapAmisSupervisionCampo> getCatSapAmisSupervisionCampoList() {
		return catSapAmisSupervisionCampoList;
	}

	public void setCatSapAmisSupervisionCampoList(List<CatSapAmisSupervisionCampo> catSapAmisSupervisionCampoList) {
		this.catSapAmisSupervisionCampoList = catSapAmisSupervisionCampoList;
	}
	
	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
}