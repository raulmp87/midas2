package mx.com.afirme.midas2.action.sapamis.procesos;

import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisEjecucionesJOBSService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

/*******************************************************************************
 * Nombre Interface: 	SapAmisEjecucionesJOBSAction.
 * 
 * Descripcion: 		Action para la ejecucion del Procesos Completo de Envios
 * 						(Ejecuta de manera manual los procesos automaticos) segun
 * 						el dia en curso.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/procesos/jobs")
public class SapAmisEjecucionesJOBSAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private SapAmisEjecucionesJOBSService sapAmisEjecucionesJOBSService;

	@Action(value = "todo")
	public void todo() {
		sapAmisEjecucionesJOBSService.todo();
	}

	@Autowired
	@Qualifier("sapAmisEjecucionesJOBSServiceEJB")
	public void setSapAmisEjecucionesJOBSService(SapAmisEjecucionesJOBSService sapAmisEjecucionesJOBSService) {
		this.sapAmisEjecucionesJOBSService = sapAmisEjecucionesJOBSService;
	}
}
