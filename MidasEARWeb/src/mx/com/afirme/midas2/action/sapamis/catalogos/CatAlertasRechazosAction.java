package mx.com.afirme.midas2.action.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasRechazos;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatAlertasRechazosService;

@Component
@Scope("prototype")
@Namespace("/sapamis/catalogos/alertasrechazos")
public class CatAlertasRechazosAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	// Servicios
	private CatAlertasRechazosService catAlertasRechazosService;
	//Variables
	private List<CatAlertasRechazos> catAlertasRechazosList = new ArrayList<CatAlertasRechazos>();

	@Action(value = "findAll", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "catAlertasRechazosList"+
		                 "catAlertasRechazosList\\[\\d+\\],"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.id,"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.descCatAlertasRechazos,"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.claveAmis,"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.estatus,"
		})
	})
	public String findAll(){
		setCatAlertasRechazosList(catAlertasRechazosService.findAll());
		return "json";
	}

	@Action(value = "findByStatus", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "catAlertasRechazosList"+
		                 "catAlertasRechazosList\\[\\d+\\],"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.id,"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.descCatAlertasRechazos,"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.claveAmis,"+
		                 "catAlertasRechazosList\\[\\d+\\]\\.estatus,"
		})
	})
	public String findByStatus(){
		setCatAlertasRechazosList(catAlertasRechazosService.findByStatus(true));
		return "json";
	}

	@Autowired
	@Qualifier("catAlertasRechazosServiceEJB")
	public void setCatAlertasRechazosService(CatAlertasRechazosService catAlertasRechazosService) {
		this.catAlertasRechazosService = catAlertasRechazosService;
	}

	public List<CatAlertasRechazos> getCatAlertasRechazosList() {
		return catAlertasRechazosList;
	}

	public void setCatAlertasRechazosList(List<CatAlertasRechazos> catAlertasRechazosList) {
		this.catAlertasRechazosList = catAlertasRechazosList;
	}
}