package mx.com.afirme.midas2.action.sapamis.pantallas;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

@Component
@Scope("prototype")
@Namespace("/sapamis")
public class SapAmisBIAction extends ActionSupport {
	private static final long serialVersionUID = 1L;

	private static final String PANTALLA = "/html/sapamis/index.html";
	
	@Action(value = "index", results = { @Result(name = SUCCESS, location = PANTALLA) })
	public String index() {
		return SUCCESS;
	}
}
