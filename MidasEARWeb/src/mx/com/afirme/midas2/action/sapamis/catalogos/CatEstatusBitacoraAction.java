package mx.com.afirme.midas2.action.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstatusBitacora;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatEstatusBitacoraService;

/*******************************************************************************
 * Nombre Interface: 	RelSistemaOperacionAction.
 * 
 * Descripcion: 		Action para el manejo de transacciones del Catalogo
 * 						de Estatus de Bitacoras.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/sapamis/catalogos/estatusbitacora")
public class CatEstatusBitacoraAction  extends ActionSupport {
	private static final long serialVersionUID = 1L;
	
	private CatEstatusBitacoraService catEstatusBitacoraService;
	private List<CatEstatusBitacora> catEstatusBitacoraList = new ArrayList<CatEstatusBitacora>();

	@Action(value = "findByStatus", results = { 
		@Result(name = "json", type="json", params = {
				"includeProperties", "catEstatusBitacoraList"+
		                 "catEstatusBitacoraList\\[\\d+\\],"+
		                 "catEstatusBitacoraList\\[\\d+\\]\\.id,"+
		                 "catEstatusBitacoraList\\[\\d+\\]\\.descCatEstatusBitacora,"+
		                 "catEstatusBitacoraList\\[\\d+\\]\\.estatusRegistro,"
		})
	})
	public String findByStatus(){
		setCatEstatusBitacoraList(catEstatusBitacoraService.findByStatusReg(true));
		return "json";
	}

	@Autowired
	@Qualifier("catEstatusBitacoraEJB")
	public void setCatEstatusBitacoraService(CatEstatusBitacoraService catEstatusBitacoraService) {
		this.catEstatusBitacoraService = catEstatusBitacoraService;
	}

	public List<CatEstatusBitacora> getCatEstatusBitacoraList() {
		return catEstatusBitacoraList;
	}

	public void setCatEstatusBitacoraList(List<CatEstatusBitacora> catEstatusBitacoraList) {
		this.catEstatusBitacoraList = catEstatusBitacoraList;
	}

}
