package mx.com.afirme.midas2.action.juridico.citas;

import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.juridico.CitaJuridico;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.juridico.CitaJuridicoService;
import mx.com.afirme.midas2.service.juridico.CitaJuridicoService.CitaJuridicoFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/juridico/citas")
public class CitaJuridicoAction extends BaseAction implements Preparable{   

	private static final long serialVersionUID = -7686324918437888307L;
	
	private CitaJuridico cita;
	private Boolean soloLectura;
	private Map<Long, String> listaOficinasJuridico;
	private Map<String, String> listaUsuariosJuridico;
	private Map<String, String> listaTiposReclamacion;
	private Map<String, String> listaRamosJuridico;
	private Map<String, String> listaEstatusCitaJuridico;
	private CitaJuridicoFiltro  citaJuridicoFiltro = new CitaJuridicoFiltro();
	
	private TransporteImpresionDTO transporte;
	
	private List<CitaJuridico> listadoCitas;
	
	private CitaJuridicoService citaJuridicoService;	
	private ListadoService listadoService;	

		
	@Autowired
	@Qualifier("citaJuridicoServiceEJB")
	public void setCitaJuridicoService(CitaJuridicoService citaJuridicoService) {
		this.citaJuridicoService = citaJuridicoService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
		
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void prepareMostrarListadoCitas()
	{		
		listaOficinasJuridico = listadoService.getMapOficinaJuridico();
		listaUsuariosJuridico = listadoService.obtenerUsuariosJuridico();
		listaTiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		listaRamosJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		listaEstatusCitaJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_CITA_JURIDICO);		
	}
	
	@Action(value="mostrarListadoCitas",results={
			@Result(name=SUCCESS,location="/jsp/juridico/citas/listadoCitasJuridico.jsp")
	})	
	public String mostrarListadoCitas() {
		
		return SUCCESS;
	}
	
	@Action(value="buscarCitasPaginacion",results={
			@Result(name=SUCCESS,location="/jsp/paginadoGrid.jsp")
	})
	public String buscarCitasPaginacion()
	{	  
		if(getTotalCount() == null){
			
			setTotalCount(citaJuridicoService.filtrarPaginado(citaJuridicoFiltro));
			setListadoEnSession(null);
		}
		setPosPaginado();
		
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value="buscarCitas",results={
			@Result(name=SUCCESS,location="/jsp/juridico/citas/listadoCitasJuridicoGrid.jsp")
	})
	public String buscarCitas()
	{ 
		if(!listadoDeCache()){
			citaJuridicoFiltro.setPrimerRegistroACargar(getPosStart());
			citaJuridicoFiltro.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
			listadoCitas = citaJuridicoService.filtrar(citaJuridicoFiltro);
			setListadoEnSession(listadoCitas);
			
		}else{
			listadoCitas = (List<CitaJuridico>) getListadoPaginado();
			setListadoEnSession(listadoCitas);
		}
			
		return SUCCESS;
	}
	
	@Action(value="exportarCitas",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String exportarCitas()
	{ 
		citaJuridicoFiltro.setPrimerRegistroACargar(null);
		citaJuridicoFiltro.setNumeroMaximoRegistrosACargar(null);
		listadoCitas = citaJuridicoService.filtrar(citaJuridicoFiltro);
			
		ExcelExporter exporter = new ExcelExporter(CitaJuridico.class);
		transporte = exporter.exportXLS(listadoCitas, "Listado de Citas");
		
		return SUCCESS;
	}
	
	
	
	public void prepareMostrarCita()
	{
		if(cita != null && cita.getId() != null)
		{
			cita = citaJuridicoService.obtener(cita.getId());			
		}
		else
		{
			cita = new CitaJuridico();
			soloLectura = Boolean.FALSE;     
		}

		listaOficinasJuridico = listadoService.getMapOficinaJuridico();
		listaUsuariosJuridico = listadoService.obtenerUsuariosJuridico();    
		listaTiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		listaRamosJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		listaEstatusCitaJuridico = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_CITA_JURIDICO);			
	}	
	
	@Action(value="mostrarCita",results={
			@Result(name=SUCCESS,location="/jsp/juridico/citas/citaJuridico.jsp")
	})
	public String mostrarCita()
	{
		return SUCCESS;
	}
		
	public void prepareGuardarCita()
	{
		if(cita != null && cita.getId() != null)
		{
			cita = citaJuridicoService.obtener(cita.getId());
			cita.setCodigoUsuarioModificacion(this.getUsuarioActual().getNombreUsuario());
			cita.setFechaModificacion(new Date());			
		}
		else
		{
			cita.setCodigoUsuarioCreacion(this.getUsuarioActual().getNombreUsuario());		
		}
	}
	
	@Action(value="guardarCita",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarListadoCitas", 
			"namespace", "/juridico/citas", 		
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"})
			})	
	public String guardarCita()
	{
		citaJuridicoService.guardar(cita);
		this.setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value="eliminarCita",results={
			@Result(name = SUCCESS, type = "redirectAction", params = { 
					"actionName", "mostrarListadoCitas", 
					"namespace", "/juridico/citas", 
					"citaJuridicoFiltro.idOficina","${citaJuridicoFiltro.idOficina}",
					"citaJuridicoFiltro.numeroCita","${citaJuridicoFiltro.numeroCita}",
					"citaJuridicoFiltro.nombreUsuario","${citaJuridicoFiltro.nombreUsuario}",
					"citaJuridicoFiltro.fechaAltaInicio","${citaJuridicoFiltro.fechaAltaInicio}",
					"citaJuridicoFiltro.fechaAltaFin","${citaJuridicoFiltro.fechaAltaFin}",
					"citaJuridicoFiltro.fechaCitaInicio","${citaJuridicoFiltro.fechaCitaInicio}",
					"citaJuridicoFiltro.fechaCitaFin","${citaJuridicoFiltro.fechaCitaFin}",
					"citaJuridicoFiltro.tipoReclamacion","${citaJuridicoFiltro.tipoReclamacion}",
					"citaJuridicoFiltro.claveRamoJuridico","${citaJuridicoFiltro.claveRamoJuridico}",
					"citaJuridicoFiltro.claveEstatusJuridico","${citaJuridicoFiltro.claveEstatusJuridico}",
					"mensaje", "${mensaje}",		
					"tipoMensaje", "${tipoMensaje}"})
					})	
	public String eliminarCita()
	{
		citaJuridicoService.eliminar(cita.getId());
		this.setMensajeExito();
		return SUCCESS;
	}



	public Boolean getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(Boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

	public Map<Long, String> getListaOficinasJuridico() {
		return listaOficinasJuridico;
	}

	public void setListaOficinasJuridico(Map<Long, String> listaOficinasJuridico) {
		this.listaOficinasJuridico = listaOficinasJuridico;
	}

	public CitaJuridico getCita() {
		return cita;
	}

	public void setCita(CitaJuridico cita) {
		this.cita = cita;
	}

	public Map<String, String> getListaUsuariosJuridico() {
		return listaUsuariosJuridico;
	}

	public void setListaUsuariosJuridico(Map<String, String> listaUsuariosJuridico) {
		this.listaUsuariosJuridico = listaUsuariosJuridico;
	}

	public Map<String, String> getListaTiposReclamacion() {
		return listaTiposReclamacion;
	}

	public void setListaTiposReclamacion(Map<String, String> listaTiposReclamacion) {
		this.listaTiposReclamacion = listaTiposReclamacion;
	}

	public Map<String, String> getListaRamosJuridico() {
		return listaRamosJuridico;
	}

	public void setListaRamosJuridico(Map<String, String> listaRamosJuridico) {
		this.listaRamosJuridico = listaRamosJuridico;
	}

	public Map<String, String> getListaEstatusCitaJuridico() {
		return listaEstatusCitaJuridico;
	}

	public void setListaEstatusCitaJuridico(
			Map<String, String> listaEstatusCitaJuridico) {
		this.listaEstatusCitaJuridico = listaEstatusCitaJuridico;
	}

	public CitaJuridicoFiltro getCitaJuridicoFiltro() {
		return citaJuridicoFiltro;
	}

	public void setCitaJuridicoFiltro(CitaJuridicoFiltro citaJuridicoFiltro) {
		this.citaJuridicoFiltro = citaJuridicoFiltro;
	}

	public List<CitaJuridico> getListadoCitas() {
		return listadoCitas;
	}

	public void setListadoCitas(List<CitaJuridico> listadoCitas) {
		this.listadoCitas = listadoCitas;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

}
