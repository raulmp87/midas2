package mx.com.afirme.midas2.action.juridico;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.juridico.ReclamacionJuridico;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionJuridicoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionOficioJuridicoAaDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionOficioJuridicoCoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionOficioJuridicoDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionOficioJuridicoGeDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionReservaJuridicoDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.juridico.JuridicoService;
import mx.com.afirme.midas2.service.juridico.JuridicoService.ReclamacionJuridicoFiltro;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value="/siniestros/juridico")
public class ReclamacionJuridicoAction extends BaseAction implements Preparable{
	
	private static final String	LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP	= "/jsp/juridico/reclamaciones/contenedorReclamacionOficioJuridico.jsp";
	private static final String	LOCATION_CONTENEDORRECLAMACIONJURIDICO_JSP	= "/jsp/juridico/reclamaciones/contenedorReclamacionJuridico.jsp";


	private static final long serialVersionUID = -8344250445010336431L;
	
	
	private Map<String,String> listaEstados;
	private Map<String,String> listaMunicipios;
	private Map<String,String> listaEtapasDeJuicio;
	private Map<String,String> listaEstatus;
	private Map<String,String> listaRamos;
	private Map<Long,String> listaOficinasJuridico;
	private Map<Long,String> listaOficinasSiniestro;
	private Map<String,String> listaPartesEnJuicio;
	private Map<String,String> listaProbabilidadesExito;
	private Map<Long,String> listaProcedimientos;
	private Map<Long,String> listaMotivos;
	private Map<Long,String> listaDelegaciones;
	private Map<String,String> listaTiposReclamacion;
	private Map<String,String> listaTiposQueja;
	private Map<String,String> listaClasificacionesReclamacion;
	
	private List<ReclamacionJuridicoDTO> reclamaciones;
	private List<ReclamacionOficioJuridicoDTO> oficios;
	private ReclamacionJuridicoFiltro reclamacionFiltro;
	private List<ReclamacionReservaJuridicoDTO> reservas;
	private List<SiniestroCabinaDTO> siniestros;
	
	private List<ReclamacionOficioJuridicoGeDTO> oficiosGe;
	private List<ReclamacionOficioJuridicoCoDTO> oficiosCo;
	private List<ReclamacionOficioJuridicoAaDTO> oficiosAa;
	
	private ReclamacionJuridicoDTO reclamacion;
	private ReclamacionOficioJuridicoDTO reclamacionOficio;
	private String reservasModificadas;
	private String numeroPoliza;
	private String numeroSiniestro;
	private String tipoReclamacion;
	private Boolean esConsulta;
	private Boolean mostrarListadoVacio;
	private Boolean existePoliza;
	private Boolean esPantallaAgregarOficio;
	private Boolean deshabilitarBusquedaSiniestro;
	private Integer estatusNotificacionReserva;
	private static final Integer ESTATUS_RESERVA_EDITADO = 1;
	private static final Integer ESTATUS_RESERVA_GUARDADO = 2;
	
	//VENTANA BUSQUEDA SINIESTRO
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private List<ReporteSiniestroDTO> reportesSiniestro = new ArrayList<ReporteSiniestroDTO>();
	private Map<Long,String> oficinasActivas;
	
	private TransporteImpresionDTO transporte;
	
	@Autowired
	@Qualifier("juridicoServiceEJB")
	private JuridicoService juridicoService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;

	@Override
	public void prepare() throws Exception {
	}
	
	@Action(value = "mostrarReclamaciones", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORRECLAMACIONJURIDICO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONJURIDICO_JSP)})
	public String mostrarListadoReclamaciones(){
		return SUCCESS;
	}
	
	@Action(value = "listarReclamaciones", results = {
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/listadoReclamacionJuridicoGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONJURIDICO_JSP)})
	public String listarReclamaciones(){
		if(mostrarListadoVacio){
			reclamaciones =  new ArrayList<ReclamacionJuridicoDTO>();
		}else{
			try{
				reclamaciones = juridicoService.obtenerReclamaciones(reclamacionFiltro);
				if(reclamaciones != null && reclamaciones.isEmpty()){
					reclamaciones =  new ArrayList<ReclamacionJuridicoDTO>();
					return SUCCESS;
				}
			}catch(Exception ex){
				setMensajeError("Error al buscar las reclamaciones");
				return INPUT;
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "agregarOficio", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String agregarOficio(){
		return SUCCESS;
	}
	
	@Action(value = "editarOficio", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String editarOficio(){
		try{
			reclamacionOficio = juridicoService.obtenerOficio(reclamacionOficio.getNumeroReclamacion(), reclamacionOficio.getNumeroOficio());
		}catch(Exception ex){
			setMensajeError("Error al cargar la informacion del oficio");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarOficio", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String guardarOficio(){
		try{
			reclamacionOficio = juridicoService.salvarReclamacion(reclamacionOficio, reservas);
			if(estatusNotificacionReserva != null 
					&& estatusNotificacionReserva.compareTo(ESTATUS_RESERVA_EDITADO) == 0){
				estatusNotificacionReserva = ESTATUS_RESERVA_GUARDADO;
			}
		}catch(Exception ex){
			setMensajeError("Error al realizar el guardado del Oficio");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "listarOficios", results = {
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/listadoReclamacionOficioJuridicoGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String listarOficios(){
		if(mostrarListadoVacio){
			oficios =  new ArrayList<ReclamacionOficioJuridicoDTO>();
		}else{
			try{
				oficios = juridicoService.obtenerOficios(reclamacionOficio.getNumeroReclamacion());
			}catch(Exception ex){
				setMensajeError("Error al cargar el listado de oficios de la reclamacion");
				return INPUT;
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "listarReservas", results = {
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/listadoReclamacionReservaJuridico.jsp"),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String listarReservas(){
		if(mostrarListadoVacio){
			reservas =  new ArrayList<ReclamacionReservaJuridicoDTO>();
		}else{
			try{
				reservas = juridicoService.obtenerReservasOficio(reclamacionOficio.getNumeroReclamacion(), 
						reclamacionOficio.getNumeroOficio(), reclamacionOficio.getSiniestroId());
			}catch(Exception ex){
				setMensajeError("Error al cargar el listado de oficios de la reclamacion");
				return INPUT;
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarSiniestros", results = {
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/contenedorReclamacionJuridicoSiniestros.jsp"),
			@Result(name = INPUT, location = "/jsp/juridico/reclamaciones/contenedorReclamacionJuridicoSiniestros.jsp")})
	public String mostrarSiniestros(){
		return SUCCESS;
	}
	
	@Action(value = "listarSiniestros", results = {
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/listadoReclamacionJuridicoSiniestrosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/juridico/reclamaciones/contenedorReclamacionJuridicoSiniestros.jsp")})
	public String listarSiniestros(){
		return SUCCESS;
	}
	
	@Action(value = "mostrarVentanaTipoReclamacion", results = {
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/contenedorReclamacionJuridicoSeleccionTipo.jsp"),
			@Result(name = INPUT, location = "/jsp/juridico/reclamaciones/contenedorReclamacionJuridicoSeleccionTipo.jsp")})
	public String mostrarVentanaTipoReclamacion(){
		return SUCCESS;
	}
	
	@Action(value="exportarResultados",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarResultados(){
		reclamaciones = juridicoService.obtenerReclamaciones(reclamacionFiltro);
		transporte = new TransporteImpresionDTO();
		ExcelExporter exporter = new ExcelExporter(ReclamacionJuridicoDTO.class);
		transporte.setGenericInputStream(exporter.export(reclamaciones));
		transporte.setContentType("application/vnd.ms-excel");
		transporte.setFileName("ReclamacionesJuridico.xls");
		return SUCCESS;
	}
	
	@Action(value="exportarListadoOficios",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarListadoOficios(){
		oficios = juridicoService.obtenerOficios(reclamacionOficio.getNumeroReclamacion());
		ExcelExporter exporter = null;
		String fileName = "Reclamaciones Juridico";
		if(reclamacionOficio.getTipoReclamacion().compareTo(ReclamacionJuridico.TIPOS_RECLAMACION.GESTION_ELECTRONICA.codigo) == 0){
			
			exporter = new ExcelExporter(ReclamacionOficioJuridicoGeDTO.class);
			oficiosGe = new ArrayList<ReclamacionOficioJuridicoGeDTO>();
			for(ReclamacionOficioJuridicoDTO oficio : oficios){
				oficiosGe.add(new ReclamacionOficioJuridicoGeDTO(oficio));
			}
			transporte = exporter.exportXLS(oficiosGe, fileName);
		}else if(reclamacionOficio.getTipoReclamacion().compareTo(ReclamacionJuridico.TIPOS_RECLAMACION.CONDUSEF.codigo) == 0){
			
			exporter = new ExcelExporter(ReclamacionOficioJuridicoCoDTO.class);
			oficiosCo = new ArrayList<ReclamacionOficioJuridicoCoDTO>();
			for(ReclamacionOficioJuridicoDTO oficio : oficios){
				oficiosCo.add(new ReclamacionOficioJuridicoCoDTO(oficio));
			}
			transporte = exporter.exportXLS(oficiosCo, fileName);
		}else if(reclamacionOficio.getTipoReclamacion().compareTo(ReclamacionJuridico.TIPOS_RECLAMACION.ALTA_AUTORIDAD.codigo) == 0){
			
			exporter = new ExcelExporter(ReclamacionOficioJuridicoAaDTO.class);
			oficiosAa = new ArrayList<ReclamacionOficioJuridicoAaDTO>();
			for(ReclamacionOficioJuridicoDTO oficio : oficios){
				oficiosAa.add(new ReclamacionOficioJuridicoAaDTO(oficio));
			}
			transporte = exporter.exportXLS(oficiosAa, fileName);
		}else{
			exporter = new ExcelExporter(ReclamacionOficioJuridicoDTO.class);
			transporte = exporter.exportXLS(oficios, fileName);
		}
		
		return SUCCESS;
	}
	
	@Action(value="validarPoliza",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^existePoliza"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^existePoliza"})
		})
	public String validarPoliza(){
		existePoliza = juridicoService.validarPoliza(reclamacionOficio.getNumeroPoliza());
	return SUCCESS;
	}
	
	@Action(value="mostrarContenedorBusquedasPolizaJuridico", results={
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/contenedorBusquedaReclamacionesPoliza.jsp"),
			@Result(name = INPUT, location= "/jsp/juridico/reclamaciones/contenedorBusquedaReclamacionesPoliza.jsp")
	})
	public String mostrarContenedorBusquedasPolizaJuridico(){
		return SUCCESS;
	}
	
	
	@Action(value="buscarPolizaJuridicoGrid", results={
			@Result(name = SUCCESS, location = "/jsp/juridico/reclamaciones/contenedorBusquedaReclamacionesPolizaGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/juridico/reclamaciones/contenedorBusquedaReclamacionesPolizaGrid.jsp")
	})
	public String buscarPolizaJuridicoGrid(){
		if(reporteSiniestroDTO != null){
			reportesSiniestro = reporteCabinaService.buscarReportes(reporteSiniestroDTO);
		}
		return SUCCESS;
	}
	
	@Action(value = "notificarBloqueoAsegurado", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String notificarBloqueoAsegurado(){
		try{
			juridicoService.bloquearAsegurado(reclamacionOficio.getNumeroPoliza(), reclamacionOficio.getAsegurado());
		}catch(Exception ex){
			setMensajeError("Error al notificar el bloqueo del asegurado");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "notificarModificacionReserva", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORRECLAMACIONOFICIOJURIDICO_JSP)})
	public String notificarModificacionReserva(){
		try{
			juridicoService.notificacionReserva(reclamacionOficio.getNumeroReclamacion(), reclamacionOficio.getNumeroOficio(), 
					reclamacionOficio.getSiniestroId(), reclamacionOficio.getNumeroPoliza(), reclamacionOficio.getAsegurado());
		}catch(Exception ex){
			setMensajeError("Error al notificar la modificacion de reservas");
			return INPUT;
		}
		return SUCCESS;
	}

	/**
	 * @return the listaEstados
	 */
	public Map<String, String> getListaEstados() {
		if(listaEstados == null || listaEstados.isEmpty()){
			this.listaEstados = listadoService.getMapEstadosPorPaisMidas(null);
		}
		return listaEstados;
	}

	/**
	 * @param listaEstados the listaEstados to set
	 */
	public void setListaEstados(Map<String, String> listaEstados) {
		this.listaEstados = listaEstados;
	}

	/**
	 * @return the listaMunicipios
	 */
	public Map<String, String> getListaMunicipios() {
		if(reclamacionOficio != null && reclamacionOficio.getEstado() != null){
			listaMunicipios = listadoService.getMapMunicipiosPorEstadoMidas(reclamacionOficio.getEstado());
		}else{
			listaMunicipios = new LinkedHashMap<String, String>();
		}
		return listaMunicipios;
	}

	/**
	 * @param listaMunicipios the listaMunicipios to set
	 */
	public void setListaMunicipios(Map<String, String> listaMunicipios) {
		this.listaMunicipios = listaMunicipios;
	}

	/**
	 * @return the listaEtapasDeJuicio
	 */
	public Map<String, String> getListaEtapasDeJuicio() {
		if(listaEtapasDeJuicio == null || listaEtapasDeJuicio.isEmpty()){
			this.listaEtapasDeJuicio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ETAPA_JUICIO_JURIDICO);
		}
		return listaEtapasDeJuicio;
	}

	/**
	 * @param listaEtapasDeJuicio the listaEtapasDeJuicio to set
	 */
	public void setListaEtapasDeJuicio(Map<String, String> listaEtapasDeJuicio) {
		this.listaEtapasDeJuicio = listaEtapasDeJuicio;
	}

	/**
	 * @return the listaEstatus
	 */
	public Map<String, String> getListaEstatus() {
		if(listaEstatus == null || listaEstatus.isEmpty()){
			this.listaEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_RECLAMACION_JURIDICO);
		}
		return listaEstatus;
	}

	/**
	 * @param listaEstatus the listaEstatus to set
	 */
	public void setListaEstatus(Map<String, String> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	/**
	 * @return the listaRamos
	 */
	public Map<String, String> getListaRamos() {
		if(listaRamos == null || listaRamos.isEmpty()){
			this.listaRamos = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_JURIDICO);
		}
		return listaRamos;
	}

	/**
	 * @param listaRamos the listaRamos to set
	 */
	public void setListaRamos(Map<String, String> listaRamos) {
		this.listaRamos = listaRamos;
	}

	/**
	 * @return the listaOficinasJuridico
	 */
	public Map<Long, String> getListaOficinasJuridico() {
		if(listaOficinasJuridico == null || listaOficinasJuridico.isEmpty()){
			this.listaOficinasJuridico = listadoService.getMapOficinaJuridico();
		}
		return listaOficinasJuridico;
	}

	/**
	 * @param listaOficinasJuridico the listaOficinasJuridico to set
	 */
	public void setListaOficinasJuridico(Map<Long, String> listaOficinasJuridico) {
		this.listaOficinasJuridico = listaOficinasJuridico;
	}

	/**
	 * @return the listaPartesEnJuicio
	 */
	public Map<String, String> getListaPartesEnJuicio() {
		if(listaPartesEnJuicio == null || listaPartesEnJuicio.isEmpty()){
			this.listaPartesEnJuicio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PARTE_JUICIO_JURIDICO);
		}
		return listaPartesEnJuicio;
	}

	/**
	 * @param listaPartesEnJuicio the listaPartesEnJuicio to set
	 */
	public void setListaPartesEnJuicio(Map<String, String> listaPartesEnJuicio) {
		this.listaPartesEnJuicio = listaPartesEnJuicio;
	}

	/**
	 * @return the listaProbabilidadesExito
	 */
	public Map<String, String> getListaProbabilidadesExito() {
		if(listaProbabilidadesExito == null || listaProbabilidadesExito.isEmpty()){
			this.listaProbabilidadesExito = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PROBABILIDAD_EXITO_JURIDICO);
		}
		return listaProbabilidadesExito;
	}

	/**
	 * @param listaProbabilidadesExito the listaProbabilidadesExito to set
	 */
	public void setListaProbabilidadesExito(
			Map<String, String> listaProbabilidadesExito) {
		this.listaProbabilidadesExito = listaProbabilidadesExito;
	}

	/**
	 * @return the listaProcedimientos
	 */
	public Map<Long, String> getListaProcedimientos() {
		if(listaProcedimientos == null || listaProcedimientos.isEmpty()){
			this.listaProcedimientos = listadoService.obtenerJuridicoProcedimientos();
		}
		return listaProcedimientos;
	}

	/**
	 * @param listaProcedimientos the listaProcedimientos to set
	 */
	public void setListaProcedimientos(Map<Long, String> listaProcedimientos) {
		this.listaProcedimientos = listaProcedimientos;
	}

	/**
	 * @return the listaTiposReclamacion
	 */
	public Map<String, String> getListaTiposReclamacion() {
		if(listaTiposReclamacion == null || listaTiposReclamacion.isEmpty()){
			this.listaTiposReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECLAMACION_JURIDICO);
		}
		return listaTiposReclamacion;
	}

	/**
	 * @param listaTiposReclamacion the listaTiposReclamacion to set
	 */
	public void setListaTiposReclamacion(Map<String, String> listaTiposReclamacion) {
		this.listaTiposReclamacion = listaTiposReclamacion;
	}

	/**
	 * @return the reclamaciones
	 */
	public List<ReclamacionJuridicoDTO> getReclamaciones() {
		return reclamaciones;
	}

	/**
	 * @param reclamaciones the reclamaciones to set
	 */
	public void setReclamaciones(List<ReclamacionJuridicoDTO> reclamaciones) {
		this.reclamaciones = reclamaciones;
	}

	/**
	 * @return the reclamacionFiltro
	 */
	public ReclamacionJuridicoFiltro getReclamacionFiltro() {
		return reclamacionFiltro;
	}

	/**
	 * @param reclamacionFiltro the reclamacionFiltro to set
	 */
	public void setReclamacionFiltro(ReclamacionJuridicoFiltro reclamacionFiltro) {
		this.reclamacionFiltro = reclamacionFiltro;
	}

	/**
	 * @return the reservas
	 */
	public List<ReclamacionReservaJuridicoDTO> getReservas() {
		return reservas;
	}

	/**
	 * @param reservas the reservas to set
	 */
	public void setReservas(List<ReclamacionReservaJuridicoDTO> reservas) {
		this.reservas = reservas;
	}

	/**
	 * @return the siniestros
	 */
	public List<SiniestroCabinaDTO> getSiniestros() {
		return siniestros;
	}

	/**
	 * @param siniestros the siniestros to set
	 */
	public void setSiniestros(List<SiniestroCabinaDTO> siniestros) {
		this.siniestros = siniestros;
	}

	/**
	 * @return the numeroPoliza
	 */
	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	/**
	 * @return the tipoReclamacion
	 */
	public String getTipoReclamacion() {
		return tipoReclamacion;
	}

	/**
	 * @param tipoReclamacion the tipoReclamacion to set
	 */
	public void setTipoReclamacion(String tipoReclamacion) {
		this.tipoReclamacion = tipoReclamacion;
	}

	/**
	 * @return the esConsulta
	 */
	public Boolean getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Boolean esConsulta) {
		this.esConsulta = esConsulta;
	}

	/**
	 * @return the reservasModificadas
	 */
	public String getReservasModificadas() {
		return reservasModificadas;
	}

	/**
	 * @param reservasModificadas the reservasModificadas to set
	 */
	public void setReservasModificadas(String reservasModificadas) {
		this.reservasModificadas = reservasModificadas;
	}

	/**
	 * @return the listaMotivos
	 */
	public Map<Long, String> getListaMotivos() {
		if(listaMotivos == null || listaMotivos.isEmpty()){
			this.listaMotivos = listadoService.obtenerJuridicoMotivos();
		}
		return listaMotivos;
	}

	/**
	 * @param listaMotivos the listaMotivos to set
	 */
	public void setListaMotivos(Map<Long, String> listaMotivos) {
		this.listaMotivos = listaMotivos;
	}

	/**
	 * @return the listaDelegaciones
	 */
	public Map<Long, String> getListaDelegaciones() {
		if(listaDelegaciones == null || listaDelegaciones.isEmpty()){
			this.listaDelegaciones = listadoService.obtenerJuridicoDelegaciones();
		}
		return listaDelegaciones;
	}

	/**
	 * @param listaDelegaciones the listaDelegaciones to set
	 */
	public void setListaDelegaciones(Map<Long, String> listaDelegaciones) {
		this.listaDelegaciones = listaDelegaciones;
	}

	/**
	 * @return the reclamacion
	 */
	public ReclamacionJuridicoDTO getReclamacion() {
		return reclamacion;
	}

	/**
	 * @param reclamacion the reclamacion to set
	 */
	public void setReclamacion(ReclamacionJuridicoDTO reclamacion) {
		this.reclamacion = reclamacion;
	}

	/**
	 * @return the reclamacionOficio
	 */
	public ReclamacionOficioJuridicoDTO getReclamacionOficio() {
		return reclamacionOficio;
	}

	/**
	 * @param reclamacionOficio the reclamacionOficio to set
	 */
	public void setReclamacionOficio(ReclamacionOficioJuridicoDTO reclamacionOficio) {
		this.reclamacionOficio = reclamacionOficio;
	}

	/**
	 * @return the oficios
	 */
	public List<ReclamacionOficioJuridicoDTO> getOficios() {
		return oficios;
	}

	/**
	 * @param oficios the oficios to set
	 */
	public void setOficios(List<ReclamacionOficioJuridicoDTO> oficios) {
		this.oficios = oficios;
	}

	/**
	 * @return the mostrarListadoVacio
	 */
	public Boolean getMostrarListadoVacio() {
		return mostrarListadoVacio;
	}

	/**
	 * @param mostrarListadoVacio the mostrarListadoVacio to set
	 */
	public void setMostrarListadoVacio(Boolean mostrarListadoVacio) {
		this.mostrarListadoVacio = mostrarListadoVacio;
	}

	/**
	 * @return the listaOficinasSiniestro
	 */
	public Map<Long, String> getListaOficinasSiniestro() {
		if(listaOficinasSiniestro == null || listaOficinasSiniestro.isEmpty()){
			this.listaOficinasSiniestro = listadoService.obtenerOficinasSiniestrosSinFiltro();
		}
		return listaOficinasSiniestro;
	}

	/**
	 * @param listaOficinasSiniestro the listaOficinasSiniestro to set
	 */
	public void setListaOficinasSiniestro(Map<Long, String> listaOficinasSiniestro) {
		this.listaOficinasSiniestro = listaOficinasSiniestro;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the existePoliza
	 */
	public Boolean getExistePoliza() {
		return existePoliza;
	}

	/**
	 * @param existePoliza the existePoliza to set
	 */
	public void setExistePoliza(Boolean existePoliza) {
		this.existePoliza = existePoliza;
	}

	/**
	 * @return the reporteSiniestroDTO
	 */
	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return reporteSiniestroDTO;
	}

	/**
	 * @param reporteSiniestroDTO the reporteSiniestroDTO to set
	 */
	public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
		this.reporteSiniestroDTO = reporteSiniestroDTO;
	}

	/**
	 * @return the reportesSiniestro
	 */
	public List<ReporteSiniestroDTO> getReportesSiniestro() {
		return reportesSiniestro;
	}

	/**
	 * @param reportesSiniestro the reportesSiniestro to set
	 */
	public void setReportesSiniestro(List<ReporteSiniestroDTO> reportesSiniestro) {
		this.reportesSiniestro = reportesSiniestro;
	}

	/**
	 * @return the oficinasActivas
	 */
	public Map<Long, String> getOficinasActivas() {
		if(oficinasActivas == null || oficinasActivas.isEmpty()){
			this.oficinasActivas = listadoService.obtenerOficinasSiniestros();
		}
		return oficinasActivas;
	}

	/**
	 * @param oficinasActivas the oficinasActivas to set
	 */
	public void setOficinasActivas(Map<Long, String> oficinasActivas) {
		this.oficinasActivas = oficinasActivas;
	}

	/**
	 * @return the listaTiposQueja
	 */
	public Map<String, String> getListaTiposQueja() {
		if(listaTiposQueja == null || listaTiposQueja.isEmpty()){
			this.listaTiposQueja = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_QUEJA_JURIDICO);
		}
		return listaTiposQueja;
	}

	/**
	 * @param listaTiposQueja the listaTiposQueja to set
	 */
	public void setListaTiposQueja(Map<String, String> listaTiposQueja) {
		this.listaTiposQueja = listaTiposQueja;
	}

	/**
	 * @return the listaClasificacionesReclamacion
	 */
	public Map<String, String> getListaClasificacionesReclamacion() {
		if(listaClasificacionesReclamacion == null || listaClasificacionesReclamacion.isEmpty()){
			this.listaClasificacionesReclamacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CLASIFICACION_RECLAMACION_JURIDICO);
		}
		return listaClasificacionesReclamacion;
	}

	/**
	 * @param listaClasificacionesReclamacion the listaClasificacionesReclamacion to set
	 */
	public void setListaClasificacionesReclamacion(
			Map<String, String> listaClasificacionesReclamacion) {
		this.listaClasificacionesReclamacion = listaClasificacionesReclamacion;
	}

	/**
	 * @return the esPantallaAgregarOficio
	 */
	public Boolean getEsPantallaAgregarOficio() {
		return esPantallaAgregarOficio;
	}

	/**
	 * @param esPantallaAgregarOficio the esPantallaAgregarOficio to set
	 */
	public void setEsPantallaAgregarOficio(Boolean esPantallaAgregarOficio) {
		this.esPantallaAgregarOficio = esPantallaAgregarOficio;
	}

	/**
	 * @return the deshabilitarBusquedaSiniestro
	 */
	public Boolean getDeshabilitarBusquedaSiniestro() {
		return deshabilitarBusquedaSiniestro;
	}

	/**
	 * @param deshabilitarBusquedaSiniestro the deshabilitarBusquedaSiniestro to set
	 */
	public void setDeshabilitarBusquedaSiniestro(
			Boolean deshabilitarBusquedaSiniestro) {
		this.deshabilitarBusquedaSiniestro = deshabilitarBusquedaSiniestro;
	}

	/**
	 * @return the oficiosGe
	 */
	public List<ReclamacionOficioJuridicoGeDTO> getOficiosGe() {
		return oficiosGe;
	}

	/**
	 * @param oficiosGe the oficiosGe to set
	 */
	public void setOficiosGe(List<ReclamacionOficioJuridicoGeDTO> oficiosGe) {
		this.oficiosGe = oficiosGe;
	}

	/**
	 * @return the oficiosCo
	 */
	public List<ReclamacionOficioJuridicoCoDTO> getOficiosCo() {
		return oficiosCo;
	}

	/**
	 * @param oficiosCo the oficiosCo to set
	 */
	public void setOficiosCo(List<ReclamacionOficioJuridicoCoDTO> oficiosCo) {
		this.oficiosCo = oficiosCo;
	}

	/**
	 * @return the oficiosAa
	 */
	public List<ReclamacionOficioJuridicoAaDTO> getOficiosAa() {
		return oficiosAa;
	}

	/**
	 * @param oficiosAa the oficiosAa to set
	 */
	public void setOficiosAa(List<ReclamacionOficioJuridicoAaDTO> oficiosAa) {
		this.oficiosAa = oficiosAa;
	}

	/**
	 * @return the estatusNotificacionReserva
	 */
	public Integer getEstatusNotificacionReserva() {
		return estatusNotificacionReserva;
	}

	/**
	 * @param estatusNotificacionReserva the estatusNotificacionReserva to set
	 */
	public void setEstatusNotificacionReserva(Integer estatusNotificacionReserva) {
		this.estatusNotificacionReserva = estatusNotificacionReserva;
	}

}