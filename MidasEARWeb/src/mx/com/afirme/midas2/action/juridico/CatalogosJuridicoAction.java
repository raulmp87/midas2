package mx.com.afirme.midas2.action.juridico;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.dto.juridico.CatalogoJuridicoDTO;
import mx.com.afirme.midas2.domain.juridico.CatalogoJuridico;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.UtileriasService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.juridico.CatalogoJuridicoService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.opensymphony.xwork2.Preparable;

/**
 * Clase action que maneja las peticiones para adminsitrar los cat�logos de
 * jur�dico.
 * @author usuario
 * @version 1.0
 * @created 14-ene-2015 10:38:43 a.m.
 */

@Component
@Scope("prototype")
@Namespace("/juridico/catalogosJuridico")
public class CatalogosJuridicoAction extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2118172352625618528L;

	private static final String CONTENEDOR =	"/jsp/juridico/contenedorCatalogoJuridico.jsp";

	private static final String LISTADO =	"/jsp/juridico/listadoCatalogoJuridicoGrid.jsp";
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("utileriasServiceEJB")
	private UtileriasService utileriasService;
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@Autowired
	@Qualifier("catalogoJuridicoServiceEJB")
	private CatalogoJuridicoService catalogoJuridicoService;
	

	/**
	 * Atributo para mapear el CatalogoJuridico que se est� agregando/editando.
	 */
	private CatalogoJuridico catalogo;
	private  CatalogoJuridicoDTO filtroCatalogo;
	
	/**
	 * Atributo para almacenar el id del catalogo seleccionado en el listado en el jsp
	 */
	private Long idCatalogo;
	/**
	 * Listado de registros del catalogo juridico. Sirve para llenar el resultado que
	 * se mostrar� en pantalla.
	 */
	private List<CatalogoJuridico> listadoCatalogoJuridico;
	private List<CatalogoJuridicoDTO> listaCatalogo;
	/**
	 * Listado de Oficinas Juridico (no confundir con las de siniestros) que ser�
	 * llenado en el prepareMostrar y servir� para cargar el combo de oficinas de
	 * jur�dico.
	 */
	private Map<Long, String> listadoOficinas;
	private Map<String, String> estautsMap;
	/**
	 * Atributo que almacena el tipo de cat�logo que se est� mostrando. De cada opci�n
	 * del men� para los cat�logos se deber� mapear como par�metro
	 * <b>tipoCatalogo=</b> y utilizar el valor de catalogo valor fijo para el
	 * catalago requerido
	 */
	private String tipoCatalogo;

	

	/**
	 * M�todo que se encargar� de guardar un cat�logo.
	 * * Asegurarse que tipoCatalogo en la entidad est� correctamente seteado.
	 * * Invocar catalogoJuridicoService.guardar.
	 */
	@Action(value="guardarCatalogo",results={
			@Result(name=SUCCESS,location=CONTENEDOR),
			@Result(name=INPUT,location=CONTENEDOR)
			})
	public String guardarCatalogo(){
		if (!StringUtil.isEmpty( this.validarForma(this.filtroCatalogo))){
			super.setMensajeError(this.validarForma(this.filtroCatalogo));
			return SUCCESS;
		}
		
		
		
		if(tipoCatalogo.equalsIgnoreCase(CatalogoJuridicoService.TIPO_CAT_JURIDICO_DELEGACION)){
			this.catalogoJuridicoService.guardarDelegacion(this.filtroCatalogo);
		}else if(tipoCatalogo.equalsIgnoreCase(CatalogoJuridicoService.TIPO_CAT_JURIDICO_PROCEDIMIENTO)){
			this.catalogoJuridicoService.guardarProcedimiento(this.filtroCatalogo);
		}else if(tipoCatalogo.equalsIgnoreCase(CatalogoJuridicoService.TIPO_CAT_JURIDICO_MOTIVOS)){
			this.catalogoJuridicoService.guardarMotivoReclamacion(this.filtroCatalogo);
		}
		this.filtroCatalogo=new CatalogoJuridicoDTO();
		super.setMensajeExitoPersonalizado("El registro a sido guardado.");
		 return SUCCESS;
	}
	
	
	public String validarForma(CatalogoJuridicoDTO filtro){
		String msj=null;
		if(StringUtil.isEmpty(filtro.getNombre())){
			msj="El Nombre es un campo obligatorio "; 
		}
		
		if(StringUtil.isEmpty(filtro.getEstatus())){
			msj="El Estatus es un campo obligatorio "; 
		}
		
		if(null==filtro.getIdOficina()){
			msj="La Ofcina es un campo obligatorio "; 
		}
		return msj;
		
	}

	/**
	 * Se encarga de listar los registros para el cat�logo seleccionado en
	 * <b>tipoCatalogo.</b> El m�todo es invocado v�a AJAX por el DHTMLXGrid para el
	 * listado.
	 * 
	 * * Setear el atributo tipoCatalogo en el filtro de busqueda de tipo
	 * CatalogoJuridico.
	 * * Invocar catalogoJuridicoService.obtenerListado con el filtro.
	 */
	@Action(value="listarCatalogos",results={
			@Result(name=SUCCESS,location=LISTADO),
			@Result(name=INPUT,location=LISTADO)
			})
	public String listarCatalogos(){
		CatalogoJuridico filtro=new CatalogoJuridico();
		filtro.setEstatus(null);
		filtro.setFechaCreacion(null);
		filtro.setFechaModificacion(null);
		if(tipoCatalogo.equalsIgnoreCase(CatalogoJuridicoService.TIPO_CAT_JURIDICO_DELEGACION)){
			listaCatalogo=this.catalogoJuridicoService.obtenerDelegaciones(filtro);
		}else if (tipoCatalogo.equalsIgnoreCase(CatalogoJuridicoService.TIPO_CAT_JURIDICO_PROCEDIMIENTO)){
			listaCatalogo=this.catalogoJuridicoService.obtenerProcedimientos(filtro);
		}else if (tipoCatalogo.equalsIgnoreCase(CatalogoJuridicoService.TIPO_CAT_JURIDICO_MOTIVOS)){
			listaCatalogo=this.catalogoJuridicoService.obtenerMotivosReclamacion(filtro);
		}
		 return SUCCESS;
	}

	/**
	 * M�todo que se encargar� de realizar la carga de la pantalla para el cat�logo
	 * seleccionado. Si el atributo idCatalogo es diferente de null, invocar
	 * catalogoJuridicoService.obtener para llenar el atributo <b>catalogo </b>con el
	 * resultado y que sea desplegado en pantalla.
	 */
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location=CONTENEDOR),
			@Result(name=INPUT,location=CONTENEDOR)
			})
	public String mostrar(){
		this.filtroCatalogo=new CatalogoJuridicoDTO();
		if(null!=this.idCatalogo){
			CatalogoJuridico catalogoJuridico=new CatalogoJuridico();
			catalogoJuridico.setEstatus(null);
			catalogoJuridico.setFechaCreacion(null);
			catalogoJuridico.setFechaModificacion(null);
			catalogoJuridico.setId(this.idCatalogo);
			catalogoJuridico.setTipoCatalogo(this.tipoCatalogo);
			catalogo= this.catalogoJuridicoService.obtener(catalogoJuridico);
			if (null!=catalogo){
				this.filtroCatalogo=this.catalogoJuridicoService.getCatalogoFiltro(catalogo);
			}
			
		}
			
		 return SUCCESS;
	}


	
	@Override
	public void prepare() throws Exception {
		this.estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_CAT_JURIDICO);
		this.listadoOficinas= listadoService.getMapOficinaJuridico();
		
		
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public UtileriasService getUtileriasService() {
		return utileriasService;
	}

	public void setUtileriasService(UtileriasService utileriasService) {
		this.utileriasService = utileriasService;
	}

	public CatalogoGrupoValorService getCatalogoGrupoValorService() {
		return catalogoGrupoValorService;
	}

	public void setCatalogoGrupoValorService(
			CatalogoGrupoValorService catalogoGrupoValorService) {
		this.catalogoGrupoValorService = catalogoGrupoValorService;
	}

	public CatalogoJuridicoService getCatalogoJuridicoService() {
		return catalogoJuridicoService;
	}

	public void setCatalogoJuridicoService(
			CatalogoJuridicoService catalogoJuridicoService) {
		this.catalogoJuridicoService = catalogoJuridicoService;
	}

	public CatalogoJuridico getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(CatalogoJuridico catalogo) {
		this.catalogo = catalogo;
	}



	public CatalogoJuridicoDTO getFiltroCatalogo() {
		return filtroCatalogo;
	}

	public void setFiltroCatalogo(CatalogoJuridicoDTO filtroCatalogo) {
		this.filtroCatalogo = filtroCatalogo;
	}

	public Long getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(Long idCatalogo) {
		this.idCatalogo = idCatalogo;
	}

	public List<CatalogoJuridico> getListadoCatalogoJuridico() {
		return listadoCatalogoJuridico;
	}

	public void setListadoCatalogoJuridico(
			List<CatalogoJuridico> listadoCatalogoJuridico) {
		this.listadoCatalogoJuridico = listadoCatalogoJuridico;
	}

	public List<CatalogoJuridicoDTO> getListaCatalogo() {
		return listaCatalogo;
	}

	public void setListaCatalogo(List<CatalogoJuridicoDTO> listaCatalogo) {
		this.listaCatalogo = listaCatalogo;
	}

	public Map<Long, String> getListadoOficinas() {
		return listadoOficinas;
	}

	public void setListadoOficinas(Map<Long, String> listadoOficinas) {
		this.listadoOficinas = listadoOficinas;
	}

	public Map<String, String> getEstautsMap() {
		return estautsMap;
	}

	public void setEstautsMap(Map<String, String> estautsMap) {
		this.estautsMap = estautsMap;
	}

	public String getTipoCatalogo() {
		return tipoCatalogo;
	}

	public void setTipoCatalogo(String tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}

	public String getCONTENEDOR() {
		return CONTENEDOR;
	}

	public String getLISTADO() {
		return LISTADO;
	}
	
	

}