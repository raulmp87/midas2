package mx.com.afirme.midas2.action.tarifa;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.service.tarifa.TarifaVersionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class TarifaVersionAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = -6497078112411686749L;


	@Override
	public void prepare() throws Exception {
				
		if (monedas == null) {
			monedas = tarifaVersionService.cargarMonedas();
		}
		
		if (versiones == null && tarifaVersion != null) {
			versiones = tarifaVersionService.cargarVersiones(tarifaVersion.getId());
		}

	}
	
	public String cargarTarifaVersion() {
		tarifaVersion = tarifaVersionService.findById(tarifaVersion.getId());
		return SUCCESS;
	}
		
	public String cargarPorValorMenu() {
		tarifaVersion = tarifaVersionService.cargarPorValorMenu(valorMenu, version, idMoneda);
		versiones = tarifaVersionService.cargarVersiones(tarifaVersion.getId());
		monedas = tarifaVersionService.cargarMonedasByTarifaVersion(tarifaVersion.getId());
		if(tarifaVersion.getTarifaConcepto().getClaveNegocio()==null){
			tarifaVersion.getTarifaConcepto().setClaveNegocio(claveNegocio);
		}
		return SUCCESS;
	}
	
	public String cambiarEstatus() {
		tarifaVersion = tarifaVersionService.cambiarEstatus(tarifaVersion.getId(), estatus);
		versiones = tarifaVersionService.cargarVersiones(tarifaVersion.getId());
		setMensajeExito();
		return SUCCESS;
	}
	
	public String generarNuevaVersion() {
		tarifaVersion = tarifaVersionService.generarNuevaVersion(tarifaVersion.getId());
		versiones = tarifaVersionService.cargarVersiones(tarifaVersion.getId());
		setMensajeExito();
		return SUCCESS;
	}
		
	
	private LinkedHashMap<String, String> versiones;
	
	private Map<String, String> monedas;
	
	private Short estatus;
	
	private TarifaVersion tarifaVersion;
	
	private String valorMenu;
	
	private String claveNegocio;
	
	private TarifaVersionService tarifaVersionService;
	
	private Long version;
	
	private Long idMoneda;
	
		

	public LinkedHashMap<String, String> getVersiones() {
		return versiones;
	}

	public void setVersiones(LinkedHashMap<String, String> versiones) {
		this.versiones = versiones;
	}

	public Map<String, String> getMonedas() {
		return monedas;
	}

	public void setMonedas(Map<String, String> monedas) {
		this.monedas = monedas;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public TarifaVersion getTarifaVersion() {
		return tarifaVersion;
	}

	public void setTarifaVersion(TarifaVersion tarifaVersion) {
		this.tarifaVersion = tarifaVersion;
	}

	public String getValorMenu() {
		return valorMenu;
	}

	public void setValorMenu(String valorMenu) {
		this.valorMenu = valorMenu;
	}


	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}
	
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}


	@Autowired
	@Qualifier("tarifaVersionEJB")
	public void setTarifaVersionService(
			TarifaVersionService tarifaVersionService) {
		this.tarifaVersionService = tarifaVersionService;
	}
	

}
