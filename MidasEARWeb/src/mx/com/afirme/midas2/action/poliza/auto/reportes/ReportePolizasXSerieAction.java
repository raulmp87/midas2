package mx.com.afirme.midas2.action.poliza.auto.reportes;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.reporteAgentes.ReportePolizasXSerieService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Clase que atiende las peticiones de los usuarios
 * para generar el reporte de polizas por numerodeserie.
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 24112016
 * 
 * @version 1.0
 *
 */
@Namespace("/reporte/polizas/numeroSerie")
@Component
@Scope("prototype")
public class ReportePolizasXSerieAction extends BaseAction{

	private static final long serialVersionUID = 1L;
	private Long numeroPolizaMidas;
	private Long numeroPolizaSeycos;
	private String numeroSerie;
	private File excelFile;
	private BigDecimal idToControlArchivo;
	private String excelFileContentType;
	private String excelFileFileName;
	private TransporteImpresionDTO transporteExcel;
	private Map<String, List<Object>> mapaValores = new HashMap<String, List<Object>>();
	private String jsonValores;

	private String errorProcess;

	@Autowired
	private ReportePolizasXSerieService reporteService;

	private static final String PATH_MUESTRA_CONTENEDOR = "/jsp/reportes/reportePolizaXSerie/reportePolizaXSerie.jsp";

	/**
	 * Metodo que inicializa los elementos que ser\u00e1n utilzados
	 * para el fujo que genera el reporte.
	 * 
	 * @return jsp que contiene la vista a mostrar.
	 */
	@Action(value = "mostrarContenedor", results = {@Result(name = SUCCESS, location = PATH_MUESTRA_CONTENEDOR)})
	public String mostrarContenedor(){
		return SUCCESS;
	}

	/**
	 * M\u00e9todo que realiza el analisis del archivo, para generar el reporte correspondiente.
	 * 
	 * @return Objeto json que contiene la respuesta.
	 * 
	 * @throws Exception Error en la escritura del archivo. 
	 */
	 @Action(value = "processFileReport", interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"reporte_Serie", "xls"})}, results={
			 @Result(name = SUCCESS, type = "stream", params = {"contentType", "${transporteExcel.contentType}", "inputName", "transporteExcel.genericInputStream", "contentDisposition", "attachment;filename=\"${transporteExcel.fileName}\""}),
			 @Result(name = INPUT  , type="json", params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","errorProcess"})})
	public String processFileReport(){
		String result = SUCCESS;
		LOG.info(">>>>>>>>>>>>>> [  INFO  ] entra al processFileReport()");
		LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: Procesando Archvivo....");
		
		File excelFile =  new File(reporteService.obtenerNombreCompleto(idToControlArchivo));		 
		try{
			LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: Intenta procesar el archivo por HSSF");
			mapaValores = reporteService.procesaArchivoPorHSSF(excelFile);
			 
			if(mapaValores != null && !mapaValores.isEmpty()){
				transporteExcel = reporteService.generarReporte(mapaValores);
			}
		} catch(Exception err){
			LOG.error(":::::::::::::: [  ERROR  ] :::::::::::::: No cumple con los requisitos para procesar por HSSF.", err);
			try{
				LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: Intenta procesar el archivo por XSSF");
				mapaValores = reporteService.procesaArchivoPorXSSF(excelFile);

				if(mapaValores != null && !mapaValores.isEmpty()){
					transporteExcel = reporteService.generarReporte(mapaValores);
				}
			} catch(Exception error){
				errorProcess = "El archivo de Excel no cumple con los requisitos para poder ser procesado  Favor de validar que se este usando la plantilla correcta o que esta no pase los 1000 series a buscar";
				result = INPUT;
				LOG.error(":::::::::::::: [  ERROR  ] :::::::::::::: No cumple con los requisitos para procesar el archivo.", error);
			}
		}

		LOG.info("<<<<<<<<<<<<<< [  INFO  ] sale al processFileReport()");
		return result;
	}

	@Action(value="generaRepoSerInd", interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"reporte_Serie", "xls"})}, results={
			@Result(name = SUCCESS, type = "stream", params={ "contentType","${transporteExcel.contentType}", "inputName","transporteExcel.genericInputStream", "contentDisposition", "attachment;filename=\"${transporteExcel.fileName}\""})})
	public String generarReporteIndividual(){

		try{
			LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: Intenta generar el reporte individual.");
			transporteExcel = reporteService.generarReporte(numeroSerie, numeroPolizaSeycos, numeroPolizaMidas);
		} catch(Exception err){
			LOG.error(":::::::::::::: [  ERROR  ] :::::::::::::: No se pudo generar el reporte.", err);
			throw new RuntimeException(err);
		}
		return SUCCESS;
	}

	public Long getNumeroPolizaMidas() {
		return numeroPolizaMidas;
	}

	public void setNumeroPolizaMidas(Long numeroPolizaMidas) {
		this.numeroPolizaMidas = numeroPolizaMidas;	
	}

	public Long getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}

	public void setNumeroPolizaSeycos(Long numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;	
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileContentType() {
		return excelFileContentType;
	}

	public void setExcelFileContentType(String excelFileContentType) {
		this.excelFileContentType = excelFileContentType;
	}

	public String getExcelFileFileName() {
		return excelFileFileName;
	}

	public void setExcelFileFileName(String excelFileFileName) {
		this.excelFileFileName = excelFileFileName;
	}

	public ReportePolizasXSerieService getReporteService() {
		return reporteService;
	}

	public void setReporteService(ReportePolizasXSerieService reporteService) {
		this.reporteService = reporteService;
	}

	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public String getErrorProcess() {
		return errorProcess;
	}

	public void setErrorProcess(String errorProcess) {
		this.errorProcess = errorProcess;
	}

	public Map<String, List<Object>> getMapaValores() {
		return mapaValores;
	}

	public void setMapaValores(Map<String, List<Object>> mapaValores) {
		this.mapaValores = mapaValores;
	}

	public String getJsonValores() {
		return jsonValores;
	}

	public void setJsonValores(String jsonValores) {
		this.jsonValores = jsonValores;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}


	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

}
