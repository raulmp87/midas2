package mx.com.afirme.midas2.action.poliza.renovacionmasiva;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.Propiedad;
import mx.com.afirme.midas.sistema.excel.RegistroGuiasProveedorExcel;
import mx.com.afirme.midas.sistema.excel.Validacion;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.LayoutRenovacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.poliza.renovacionmasiva.ValidacionRenovacionDTO;
import mx.com.afirme.midas2.excels.GeneraExcelRenovacionMasiva;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.custShipmentTracking.TrackingService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class RenovacionMasivaAction extends BaseAction implements Preparable{
	
	private static final long serialVersionUID = 1L;
	private static final String PLANTILL_CARGA_GUIAS = "/mx/com/afirme/midas2/impresiones/jrxml/plantilla_carga_guias.xls";

	public static final Logger LOG = Logger.getLogger(RenovacionMasivaAction.class);
    
    private static final Short RENOVACION_EXITOSA = 1;
    private static final Short RENOVACION_FALLIDA = 2;
	
    private static final int REGISTROS_A_MOSTRAR_REN = 8000;
	
	private PolizaDTO polizaDTO;
	private List<Agente> agenteList = new ArrayList<Agente>(1);
	private List<Negocio> negocioList = new ArrayList<Negocio>(1);
	private List<NegocioProducto> productoList = new ArrayList<NegocioProducto>(1);
	private Integer idTcAgente;
	private List<PolizaDTO> polizaList = new ArrayList<PolizaDTO>(1);
	private Boolean filtrar= false;
	private String accion;
	private Short accionRenovacion = 1;
	private Map<String, String> tipoAccionesRenovacion = new HashMap<String, String>(1);
	private OrdenRenovacionMasiva ordenRenovacion;
	private Short estatusRenovacion = 0;
	private BigDecimal idToOrdenRenovacion;	
	private Map<Long, String> centrosEmisores;
	private Map<Long, String> gerencias;
	private List<OrdenRenovacionMasiva> ordenRenovacionMasivaList = new ArrayList<OrdenRenovacionMasiva>(1);
	private List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList = new ArrayList<OrdenRenovacionMasivaDet>(1);
	private List<ValidacionRenovacionDTO> validaciones;
	private BigDecimal idToNegSeccion;
	private BigDecimal idToNegTipoPoliza;
	private BigDecimal idToNegProducto;
	private BigDecimal idToNegocio;
	private NegocioSeccion negocioSeccion;
	
	private ListadoService listadoService;
	private NegocioService negocioService;
	private EntidadService entidadService;
	private CentroEmisorService centroEmisorService;
	private RenovacionMasivaService renovacionMasivaService;
	private TrackingService trackingService;
	private CondicionEspecialBitemporalService condicionEspBitService;
	private CondicionEspecialService condicionEspecialService;
	private NegocioEstadoDescuentoService negocioEstadoDescuentoService;
	private CalculoService calculoService;
	private SeguroObligatorioService seguroObligatorioService;
	
	private BigDecimal id;
	private Agente agente;

    private String descripcionBusquedaAgente = null;
    private String descripcionBusquedaNegocio = null;
    private List<BigDecimal> cotizacionesCanceladas;
    
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
    
    private Locale locale = getLocale();
    private boolean agenteControlDeshabilitado;
    private boolean promotoriaControlDeshabilitado;
    
	private List<String> errores = new ArrayList<String>(1);
	private BigDecimal idToControlArchivo;
    
    private String mensajePolizaFallida;
    private BigDecimal idToProducto;
    private String fechaInicio;
    private String fechaFin;

	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}	
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}
	
	@Autowired
	@Qualifier("renovacionMasivaServiceEJB")
	public void setRenovacionMasivaService(
			RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Override
	public void prepare() {
		if(polizaDTO == null){
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		}		
	}
	
	@Autowired
	@Qualifier("centroEmisorServiceEJB")
	public void setCentroEmisorService(CentroEmisorService centroEmisorService) {
		this.centroEmisorService = centroEmisorService;
	}
	
	public CondicionEspecialBitemporalService getCondicionEspBitService() {
		return condicionEspBitService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialBitemporalServiceEJB")
	public void setCondicionEspBitService(
			CondicionEspecialBitemporalService condicionEspBitService) {
		this.condicionEspBitService = condicionEspBitService;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}
	
	public NegocioEstadoDescuentoService getNegocioEstadoDescuentoService() {
		return negocioEstadoDescuentoService;
	}

	@Autowired
	@Qualifier("negocioEstadoDescuentoServiceEJB")
	public void setNegocioEstadoDescuentoService(
			NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}

	public boolean isAgenteControlDeshabilitado() {
		return agenteControlDeshabilitado;
	}
	
	public boolean isPromotoriaControlDeshabilitado() {
		return promotoriaControlDeshabilitado;
	}
	
	public void prepareMostrarListadoPolizas(){
		//negocios activos
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioList = negocioService.findByFilters(negocio);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			agenteControlDeshabilitado = true;
			promotoriaControlDeshabilitado = true;
			getNegociosPorAgente();
		} else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			promotoriaControlDeshabilitado = true;
		}
		
		setCentrosEmisores(centroEmisorService.listarCentrosEmisoresMap());	
		this.setGerencias(listadoService.getMapGerencias());
	}
	

	public String mostrarListadoPolizas() {
		return SUCCESS;
	}
	
	private void asignarAgente(Agente agente) {
		if (polizaDTO == null) {
			polizaDTO = new PolizaDTO();
		}
		
		if (polizaDTO.getCotizacionDTO() == null) {
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
		}
		
		if (polizaDTO.getCotizacionDTO().getSolicitudDTO() == null) {
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		}
		
		SolicitudDTO solicitud = polizaDTO.getCotizacionDTO().getSolicitudDTO();
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		solicitud.setAgente(agente);
	}
	
	public void prepareBuscarPolizasPaginado(){
	}
	
	public String buscarPolizasPaginado() {
		if (polizaDTO != null && filtrar) {
			if(getTotalCount() == null){
				setTotalCount(renovacionMasivaService.obtenerTotalPolizasRenovacion(polizaDTO, filtrar, true));
				setListadoEnSession(null);
			}
			//setPosPaginado();
			setPosPaginado(REGISTROS_A_MOSTRAR_REN);
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String buscarPolizas() {
		if (polizaDTO != null && filtrar) {
			if(!listadoDeCache()){
				polizaDTO.setPrimerRegistroACargar(getPosStart());
				//polizaDTO.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
				polizaDTO.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR_REN);
				polizaList = renovacionMasivaService.listarPolizasRenovacion(polizaDTO, filtrar, false, false);
				setListadoEnSession(polizaList);
			}else{
				polizaList = (List<PolizaDTO>) getListadoPaginado();
				setListadoEnSession(polizaList);
			}
			
		}
		return SUCCESS;
	}
	
	public void prepareVentanaAccionRenovaciones(){
		//negocios activos
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			agenteControlDeshabilitado = true;
			promotoriaControlDeshabilitado = true;
			getNegociosPorAgente();
		}else{
			negocioList = negocioService.findByFilters(negocio);
		}
		
		//Llenar radio con las acciones según los permisos del usuario.
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Renovacion_Renovar_Y_Emitir")) {
			tipoAccionesRenovacion.put("1", "Renovar y Emitir <br/>");
		}
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Renovacion_Cotizar_Renovacion")) {
			tipoAccionesRenovacion.put("2", "Cotizar y Renovar <br/>");	
		}
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Renovacion_Cotizar_Y_Renovar_Con_Otro_Negocio")) {
			tipoAccionesRenovacion.put("3", "Cotizar y Renovar con Otro Negocio <br/>");	
		}
		
		//Asignar un valor default al combo.
		if (tipoAccionesRenovacion.get("1") != null) {
			accionRenovacion = 1;
		} else if (tipoAccionesRenovacion.get("2") != null) {
			accionRenovacion = 2;
		} else if (tipoAccionesRenovacion.get("3") != null) {
			accionRenovacion = 3;
		}
		
	}

	public String ventanaAccionRenovaciones(){
		return SUCCESS;
	}
	
	public void prepareRenovarPolizas(){
		if(idToNegSeccion != null){
			negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
		}
	}
	
	public String renovarPolizas(){
		String estatusRenov = SUCCESS;
		if(polizaList != null && polizaList.size() > 0 && accionRenovacion != null){
			GeneraRenovacionMasiva generaRenovacionMasiva = new GeneraRenovacionMasiva();
			generaRenovacionMasiva.setRenovacionMasivaService(renovacionMasivaService);
			generaRenovacionMasiva.setCondicionEspBitService(condicionEspBitService);
			generaRenovacionMasiva.setCondicionEspecialService(condicionEspecialService);
			generaRenovacionMasiva.setNegocioEstadoDescuentoService(negocioEstadoDescuentoService);
			generaRenovacionMasiva.setListadoService(listadoService);
			generaRenovacionMasiva.setCalculoService(calculoService);
			generaRenovacionMasiva.setSeguroObligatorioService(seguroObligatorioService);
			validaciones = generaRenovacionMasiva.verificaPolizasARenovar(polizaList, accionRenovacion, negocioSeccion);
			if(validaciones.isEmpty()){
				idToOrdenRenovacion = generaRenovacionMasiva.validaRenovacionMasiva(polizaList, accionRenovacion, negocioSeccion);
				if(!generaRenovacionMasiva.getMensajeError().isEmpty() && !generaRenovacionMasiva.isUnExito()){
					String[] cotizacionesFallidas = generaRenovacionMasiva.getMensajeError().split("\\|");
					
					ValidacionRenovacionDTO val;
					
					for(String textoPol: cotizacionesFallidas){
						if(!textoPol.trim().isEmpty()){
							String[] errorPolza = textoPol.split("\\*");
							val = new ValidacionRenovacionDTO();
							val.setIdToPoliza(new BigDecimal(errorPolza[0]));
							val.setNumeroPolizaFormateada(errorPolza[1]);
							val.setMensajeError(errorPolza[2]);
							validaciones.add(val);
						}
					}
					
					this.setEstatusRenovacion(RENOVACION_FALLIDA);
					estatusRenov = ERROR;
				} else {
					
					if(!generaRenovacionMasiva.getMensajeError().isEmpty() && generaRenovacionMasiva.isUnExito()){
						String[] cotizacionesFallidas = generaRenovacionMasiva.getMensajeError().split("\\|");
						mensajePolizaFallida = "";
						for(String textoPol: cotizacionesFallidas){
							if(!textoPol.trim().isEmpty()){
								String[] errorPolza = textoPol.split("\\*");
								StringBuilder polizaErronea = new StringBuilder();
								polizaErronea.append("<p> La p\u00f3liza n\u00famero <b>")
									.append(errorPolza[1].replaceAll("\\n", " "))
									.append("</b> <br/> tubo la(s) siguente(s) excepciones: ")
									.append(errorPolza[2].replaceAll("\\n", " "))
									.append(" </p> <br/>");
								mensajePolizaFallida = mensajePolizaFallida + polizaErronea.toString();
							}
						}
					}
					
					this.setEstatusRenovacion(RENOVACION_EXITOSA);
				}
			}else{
				this.setEstatusRenovacion(RENOVACION_FALLIDA);
				estatusRenov = ERROR;
			}
		}
		return estatusRenov;
	}
	
	public String mostrarOrdenesRenovacion(){
		return SUCCESS;
	}
	
	
	public String ventanaBusqueda(){
		return SUCCESS;
	}
	
	public String buscarOrdenesRenovacionPaginado(){
		if(ordenRenovacion != null || !filtrar){
			if(getTotalCount() == null){
				setTotalCount(renovacionMasivaService.listarOrdenesRenovacionCount(ordenRenovacion, filtrar, usuarioService.getUsuarioActual().getNombreUsuario()));
				setListadoEnSession(null);
			}
			setPosPaginado();
		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String buscarOrdenesRenovacion(){
		if(ordenRenovacion != null || !filtrar){
			if(!listadoDeCache()){
				if(ordenRenovacion == null){
					ordenRenovacion = new OrdenRenovacionMasiva();
				}
				try{
					ordenRenovacion.setPrimerRegistroACargar(getPosStart());
					ordenRenovacion.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
					ordenRenovacionMasivaList = renovacionMasivaService.listarOrdenesRenovacion(ordenRenovacion, filtrar, usuarioService.getUsuarioActual().getNombreUsuario());
				}catch(Exception e){
					ordenRenovacionMasivaList = new ArrayList<OrdenRenovacionMasiva>(1);
					LOG.error(e.getMessage(), e);
				}
			}else{
				ordenRenovacionMasivaList = (List<OrdenRenovacionMasiva>) getListadoPaginado();
				setListadoEnSession(ordenRenovacionMasivaList);
			}
		}
		return SUCCESS;
	}
	
	public String verOrdenRenovacionDetalle(){
		if(getIdToOrdenRenovacion() != null){
			ordenRenovacion = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
		}
		return SUCCESS;
	}
	
	public String listarOrdenRenovacionDetalle(){
		if(getIdToOrdenRenovacion() != null){
			ordenRenovacionMasivaDetList = renovacionMasivaService.listarOrdenRenovacionDetalleByOrdenRenovacion(idToOrdenRenovacion);
		}
		return SUCCESS;
	}
	
	public String cancelarOrdenRenovacion(){
		GeneraRenovacionMasiva generaRenovacionMasiva = new GeneraRenovacionMasiva();
		generaRenovacionMasiva.setRenovacionMasivaService(renovacionMasivaService);
		generaRenovacionMasiva.cancelarOrdenRenovacion(idToOrdenRenovacion);
		this.setMensajeExito();
		return SUCCESS;
	}
	
	public String terminarOrdenRenovacion(){
		GeneraRenovacionMasiva generaRenovacionMasiva = new GeneraRenovacionMasiva();
		generaRenovacionMasiva.setRenovacionMasivaService(renovacionMasivaService);
		generaRenovacionMasiva.setCondicionEspBitService(condicionEspBitService);
		generaRenovacionMasiva.setCondicionEspecialService(condicionEspecialService);
		generaRenovacionMasiva.setNegocioEstadoDescuentoService(negocioEstadoDescuentoService);
		generaRenovacionMasiva.setListadoService(listadoService);
		generaRenovacionMasiva.setCalculoService(calculoService);
		generaRenovacionMasiva.setSeguroObligatorioService(seguroObligatorioService);		
		String mensajeError = generaRenovacionMasiva.terminarOrdenRenovacion(idToOrdenRenovacion);
		if(mensajeError != null && !mensajeError.isEmpty()){
			this.setMensajeError(mensajeError);
		}else{
			this.setMensajeExito();
		}
		return SUCCESS;
	}
	
	public String emitirPolizasDetalle(){
		if(polizaList != null && polizaList.size() > 0 && idToOrdenRenovacion != null){
			GeneraRenovacionMasiva generaRenovacionMasiva = new GeneraRenovacionMasiva();
			generaRenovacionMasiva.setRenovacionMasivaService(renovacionMasivaService);
			generaRenovacionMasiva.setCondicionEspBitService(condicionEspBitService);
			generaRenovacionMasiva.setCondicionEspecialService(condicionEspecialService);
			generaRenovacionMasiva.setNegocioEstadoDescuentoService(negocioEstadoDescuentoService);
			generaRenovacionMasiva.setListadoService(listadoService);
			generaRenovacionMasiva.setCalculoService(calculoService);
			generaRenovacionMasiva.setSeguroObligatorioService(seguroObligatorioService);			
			String mensajeError = generaRenovacionMasiva.terminarPolizasDetalleOrdenRenovacion(idToOrdenRenovacion, polizaList);
			if(mensajeError != null && !mensajeError.isEmpty()){
				this.setMensajeError(mensajeError);
			}else{
				this.setMensajeExito();
			}
		}
		return SUCCESS;
	}
	
	public String cancelarPolizasDetalle(){
		if(polizaList != null && polizaList.size() > 0 && idToOrdenRenovacion != null){
			GeneraRenovacionMasiva generaRenovacionMasiva = new GeneraRenovacionMasiva();
			generaRenovacionMasiva.setRenovacionMasivaService(renovacionMasivaService);
			generaRenovacionMasiva.cancelarDetalleOrdenRenovacion(idToOrdenRenovacion, polizaList);
			this.setMensajeExito();
		}
		return SUCCESS;
	}
	
	public String descargarCotizacionesRenovacion(){
		GeneraExcelRenovacionMasiva plantilla = new GeneraExcelRenovacionMasiva();
		try {
			plantilla.setRenovacionMasivaService(renovacionMasivaService);
			setPlantillaInputStream(plantilla.generaPlantillaCotizacionesRenovacion(idToOrdenRenovacion, GeneraExcelRenovacionMasiva.TIPO_EXCEL_SISTEMA));
			contentType = ConstantesReporte.TIPO_XLS;
			fileName = "OrdenRenovacion" + idToOrdenRenovacion + ".xls";	
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public String descargarXMLRenovacion(){
		GeneraExcelRenovacionMasiva plantilla = new GeneraExcelRenovacionMasiva();
		try {
			plantilla.setRenovacionMasivaService(renovacionMasivaService);
			setPlantillaInputStream(plantilla.obtieneXMLOrdenRenovacion(idToOrdenRenovacion, locale));
		   contentType = "application/pdf";
		   fileName = idToOrdenRenovacion + "XML.pdf";
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public String descargarExcelRenovacion(){
		GeneraExcelRenovacionMasiva plantilla = new GeneraExcelRenovacionMasiva();		
		try {
			plantilla.setRenovacionMasivaService(renovacionMasivaService);
			plantilla.setTrackingService(trackingService);
			setPlantillaInputStream(plantilla.generaExcelControl(idToOrdenRenovacion));
			setContentType(ConstantesReporte.TIPO_XLS);
			setFileName("Excel" + idToOrdenRenovacion + ".xls");	
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public String cancelarCotizacionValidacion(){
		if(validaciones != null && !validaciones.isEmpty()){
			GeneraRenovacionMasiva generaRenovacionMasiva = new GeneraRenovacionMasiva();
			generaRenovacionMasiva.setRenovacionMasivaService(renovacionMasivaService);
			cotizacionesCanceladas = generaRenovacionMasiva.cancelaCotizacionesValidacion(validaciones);
		}
		return SUCCESS;
	}
	
	public String enviarProveedorOrdenRenovacion(){
		if(idToOrdenRenovacion != null){
			String mensaje = null;
			try{
				GeneraExcelRenovacionMasiva plantilla = new GeneraExcelRenovacionMasiva();
				plantilla.setRenovacionMasivaService(renovacionMasivaService);
				InputStream excel = plantilla.generaExcelControl(idToOrdenRenovacion);
				List<ControlDescripcionArchivoDTO> pdfList =  plantilla.obtieneXMLOrdenRenovacionIndividual(idToOrdenRenovacion, locale);
				mensaje = renovacionMasivaService.enviarProveedorOrdenRenovacion(idToOrdenRenovacion, pdfList, excel);
				if(mensaje != null){
					throw new Exception(mensaje);
				}
				try{
					OrdenRenovacionMasiva orden = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
					Short enviado = 1;
					orden.setEnviado(enviado);
					entidadService.save(orden);
				}catch(Exception e){
				}
				this.setMensajeExito();
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
				this.setMensajeError(mensaje);
			}
		}
		return SUCCESS;
	}
	
	public String obtenerOrdenRenovacionProveedor(){
		if(idToOrdenRenovacion != null){
			try{
				setPlantillaInputStream(renovacionMasivaService.obtenerOrdenRenovacionProveedor(idToOrdenRenovacion));
				if(getPlantillaInputStream() == null){
					this.setMensajeError("Error al obtener el archivo");
					return ERROR;
				}
				setContentType(ConstantesReporte.TIPO_XLS);
				setFileName("OrdenRenovacion" + idToOrdenRenovacion + ".xls");	
				this.setMensajeExito();
			}catch(Exception e){
				this.setMensajeError("Error al obtener el archivo");
				return ERROR;				
			}
		}
		return SUCCESS;
	}
	
	public void prepareBuscarAgente(){
		if(descripcionBusquedaAgente != null){
			agenteList = listadoService.listaAgentesPorDescripcion(descripcionBusquedaAgente.trim().toUpperCase());
		}else{
			agenteList = new ArrayList<Agente>();
		}		
	}

	public String buscarAgente(){
		return SUCCESS;
	}
	
	public String saveCargaMasiva(){
		try {
			
			ControlArchivoDTO controlArchivoDTO= entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
			RegistroGuiasProveedorExcel<RegistroGuiasProveedorId> excel = new 
				RegistroGuiasProveedorExcel<RegistroGuiasProveedorId>(RegistroGuiasProveedorId.class);
			excel.setArchivo(controlArchivoDTO);
			if(excel.isValid()) {				
				Propiedad propiedad = null;
				
				propiedad = new Propiedad(0, "idGuia");
				propiedad.setDescripcion(RegistroGuiasProveedorExcel.NUM_GUIA);
				propiedad.addValidacion(new Validacion(Validacion.TipoValidacion.ALFANUMERICO));
				excel.addPropiedad(propiedad);
				
				propiedad = new Propiedad(1, "idToPolizaRenovada");
				propiedad.setDescripcion(RegistroGuiasProveedorExcel.POLIZA);
				propiedad.addValidacion(new Validacion(Validacion.TipoValidacion.ALFANUMERICO));
				excel.addPropiedad(propiedad);
				//Ignora filas completamente vacias
				excel.setExcludeEmptyRow(true);
				//Valida los heders del archivo
				if(excel.validColumnHeader(controlArchivoDTO)){
					errores.add("Favor de revisar el archivo ya que no contiene las cabeceras</br>"+
							RegistroGuiasProveedorExcel.NUM_GUIA +"</br>"+
							RegistroGuiasProveedorExcel.POLIZA +" </br> \u00F3 no es la plantilla base");
					
				}else{
					List<RegistroGuiasProveedorId> rowsValidos = excel.getRegistrosValidos();
					List<RegistroGuiasProveedorId> rowsInvalidos = excel.getRegistrosInvalidos();
					
					if(rowsInvalidos.isEmpty() || rowsInvalidos.size() == 0) {
						
						renovacionMasivaService.saveCargaMasiva(rowsValidos);	
					} else {
						StringBuilder msg = new StringBuilder("Se encontraron los siguientes errores:");
						msg.append("<br>");
						for (String mesaje : excel.getRegistrosInvalidosMensaje()) {
							msg.append("<br>").append(mesaje);
						}
						errores.add(msg.toString());
					}
				}				
			} else {
				errores.add("El archivo no es v\u00e1lido.");
			}
			
		} catch (SystemException e) {
			LOG.error(e.getMessage(), e);
		} catch (FileNotFoundException e) {
			errores.add("No se encontrol el archivo.");
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {
			errores.add("Favor de revisar el archivo.");
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			errores.add("Favor de revisar que el formato del archivo.");
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public String procesarEstatusPendientes(){
		renovacionMasivaService.procesarEstatusPendientes();
		return SUCCESS;
	}
	
	public String procesaTarea(){
		renovacionMasivaService.procesarTareaEstatusPendientes();
		return SUCCESS;
	}
	
	public String getReporteEfectividad(){

		String mensaje = "";
		TransporteImpresionDTO inputStream;
		try {
			inputStream = renovacionMasivaService.getReporteEfectividad(ordenRenovacion);
			
			plantillaInputStream = inputStream.getGenericInputStream();
			contentType = inputStream.getContentType();
			fileName = inputStream.getFileName();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;		
	}
	
	public String downLoadFormatGuias(){
		try {
			plantillaInputStream = generaPlantillaCargaMasiva();
			setFileName("plantilla_carga_guias.xls");
			contentType = ConstantesReporte.TIPO_XLS;
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public InputStream generaPlantillaCargaMasiva() throws IOException {
		HSSFWorkbook workbook = null;
		ByteArrayOutputStream bos = null;	
		
		try {
			workbook = this.getPlantillaPlantilla();
			bos = new ByteArrayOutputStream();
		
			workbook.write(bos);
		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			if (bos != null) {
				bos.close();
			}
		}
		
		if(bos == null){
			return null;
		}
		return new ByteArrayInputStream(bos.toByteArray());
	}

	private HSSFWorkbook getPlantillaPlantilla() throws IOException {
		HSSFWorkbook workbook = null;
		POIFSFileSystem plantilla = new POIFSFileSystem(
				this.getClass()
						.getResourceAsStream(PLANTILL_CARGA_GUIAS));
		workbook = new HSSFWorkbook(plantilla);
		return workbook;
	}
	
	private void getNegociosPorAgente() {
		Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
		asignarAgente(agenteUsuarioActual);			
		negocioList = negocioService.listarNegociosPorAgenteUnionNegociosLibres(agenteUsuarioActual.getId().intValue(),
				Negocio.CLAVE_NEGOCIO_AUTOS);
	}
	
	public void prepareMostrarGeneraLayout(){
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioList = negocioService.findByFilters(negocio);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			getNegociosPorAgente();
		}
	}

	public String mostrarGeneraLayout() {
		return SUCCESS;
	}
	
	public String descargarLayout(){
		try {
			ExcelExporter exporter = new ExcelExporter( LayoutRenovacion.class );
			List<LayoutRenovacion> lista = renovacionMasivaService.obtenerListaLayout(
					fechaInicio, fechaFin, 
					idToNegocio != null ? idToNegocio.longValue() : null,
					idToProducto != null ? idToProducto.longValue() : null);
			TransporteImpresionDTO transporte =  exporter.exportXLS(lista, "layout_renovacion.xls");
			
			plantillaInputStream = transporte.getGenericInputStream();
			setFileName(transporte.getFileName());
			contentType = ConstantesReporte.TIPO_XLS;
		}
		catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
		
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}
	

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public Integer getIdTcAgente() {
		return idTcAgente;
	}

	public void setIdTcAgente(Integer idTcAgente) {
		this.idTcAgente = idTcAgente;
	}

	public List<PolizaDTO> getPolizaList() {
		return polizaList;
	}

	public void setPolizaList(List<PolizaDTO> polizaList) {
		this.polizaList = polizaList;
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public void setDescripcionBusquedaAgente(
			String descripcionBusquedaAgente) {
		this.descripcionBusquedaAgente = descripcionBusquedaAgente;
	}

	public String getDescripcionBusquedaAgente() {
		return descripcionBusquedaAgente;
	}
	
	public String getDescripcionBusquedaNegocio() {
		return descripcionBusquedaNegocio;
	}

	public void setDescripcionBusquedaNegocio(String descripcionBusquedaNegocio) {
		this.descripcionBusquedaNegocio = descripcionBusquedaNegocio;
	}
	
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public void setProductoList(List<NegocioProducto> productoList) {
		this.productoList = productoList;
	}

	public List<NegocioProducto> getProductoList() {
		return productoList;
	}

	public void setCentrosEmisores(Map<Long, String> centrosEmisores) {
		this.centrosEmisores = centrosEmisores;
	}

	public Map<Long, String> getCentrosEmisores() {
		return centrosEmisores;
	}

	public void setAccionRenovacion(Short accionRenovacion) {
		this.accionRenovacion = accionRenovacion;
	}

	public Short getAccionRenovacion() {
		return accionRenovacion;
	}
	
	public Map<String, String> getTipoAccionesRenovacion() {
		return tipoAccionesRenovacion;
	}

	public void setOrdenRenovacion(OrdenRenovacionMasiva ordenRenovacion) {
		this.ordenRenovacion = ordenRenovacion;
	}

	public OrdenRenovacionMasiva getOrdenRenovacion() {
		return ordenRenovacion;
	}

	public void setEstatusRenovacion(Short estatusRenovacion) {
		this.estatusRenovacion = estatusRenovacion;
	}

	public Short getEstatusRenovacion() {
		return estatusRenovacion;
	}

	public void setIdToOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		this.idToOrdenRenovacion = idToOrdenRenovacion;
	}

	public BigDecimal getIdToOrdenRenovacion() {
		return idToOrdenRenovacion;
	}

	public void setOrdenRenovacionMasivaList(
			List<OrdenRenovacionMasiva> ordenRenovacionMasivaList) {
		this.ordenRenovacionMasivaList = ordenRenovacionMasivaList;
	}

	public List<OrdenRenovacionMasiva> getOrdenRenovacionMasivaList() {
		return ordenRenovacionMasivaList;
	}

	public void setOrdenRenovacionMasivaDetList(
			List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaDetList) {
		this.ordenRenovacionMasivaDetList = ordenRenovacionMasivaDetList;
	}

	public List<OrdenRenovacionMasivaDet> getOrdenRenovacionMasivaDetList() {
		return ordenRenovacionMasivaDetList;
	}

	public void setValidaciones(List<ValidacionRenovacionDTO> validaciones) {
		this.validaciones = validaciones;
	}

	public List<ValidacionRenovacionDTO> getValidaciones() {
		return validaciones;
	}

	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegProducto(BigDecimal idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public BigDecimal getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setCotizacionesCanceladas(List<BigDecimal> cotizacionesCanceladas) {
		this.cotizacionesCanceladas = cotizacionesCanceladas;
	}

	public List<BigDecimal> getCotizacionesCanceladas() {
		return cotizacionesCanceladas;
	}

	public void setGerencias(Map<Long, String> gerencias) {
		this.gerencias = gerencias;
	}

	public Map<Long, String> getGerencias() {
		return gerencias;
	}
	
	public List<String> getErrores() {
		return errores;
	}

	public void setErrores(List<String> errores) {
		this.errores = errores;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@Autowired
	public void setTrackingService(TrackingService trackingService) {
		this.trackingService = trackingService;
	}

	public String getMensajePolizaFallida() {
		return mensajePolizaFallida;
	}

	public void setMensajePolizaFallida(String mensajePolizaFallida) {
		this.mensajePolizaFallida = mensajePolizaFallida;
	}

	public BigDecimal getIdToProducto() {
		return idToProducto;
	}

	public void setIdToProducto(BigDecimal idToProducto) {
		this.idToProducto = idToProducto;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}	
}