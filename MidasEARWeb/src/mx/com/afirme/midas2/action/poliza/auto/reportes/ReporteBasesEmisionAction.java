package mx.com.afirme.midas2.action.poliza.auto.reportes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroDTO;
import mx.com.afirme.midas.contratos.movimiento.TipoReaseguroFacadeRemote;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO.DatosBasesEmisionParametrosDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("prototype")
@Namespace("/auto/reportes")
public class ReporteBasesEmisionAction extends BaseAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8040978887096378218L;
	
	private Date fechaInicial ;
	private  Date  fechaFinal ;
	private  BigDecimal idRamo ;
	private  BigDecimal subRamoId ;
	private  BigDecimal idTipoReaseguro;
	private  Short nivelAgrupamiento ;
	
	private RamoFacadeRemote ramoFacadeRemote;
	private SubRamoFacadeRemote subRamoFacadeRemote;
	private TipoReaseguroFacadeRemote tipoReaseguroFacadeRemote;
	
	private List<RamoDTO> ramoLista = new ArrayList<RamoDTO>(1);
	private List<SubRamoDTO> subRamoLista = new ArrayList<SubRamoDTO>(1);
	private List<TipoReaseguroDTO> tipoReaseguroLista = new ArrayList<TipoReaseguroDTO>(1);
	private List<NivelAgrupamiento> nivelAgrupamientoLista = new ArrayList<ReporteBasesEmisionAction.NivelAgrupamiento>(1);
	
	private InputStream basesEmisionAutoInputStream;
	private String contentType;
	private String fileName;	

	
	@Action(value ="mostrarReporteBasesEmisionAuto", results ={
			@Result(name= SUCCESS, location="/jsp/poliza/auto/reportes/reporteBasesEmision.jsp")
	})
	public String mostrarReporteBasesEmisionAuto(){
		//se obtiene los ramos de tipo auto.
		ramoFacadeRemote.setClaveNegocioDefault(Negocio.CLAVE_NEGOCIO_AUTOS);
		ramoLista = ramoFacadeRemote.findAll();
		nivelAgrupamientoLista = obtenerNivelAgrupamientoLista();
		this.nivelAgrupamiento = new Short("1");
		
		//tipoReaseguroLista = tipoReaseguroFacadeRemote.findAll();
		
		if(idRamo != null){
			subRamoLista =  subRamoFacadeRemote.findAll(idRamo);
		}else{
			subRamoLista = new ArrayList<SubRamoDTO>(1);
		}
		
		return SUCCESS;
	}
	
	@Action(value ="generarReporteBasesEmisionAuto", results ={
			@Result(name= SUCCESS,type="stream" , params={ 
					"contentType","${contentType}",
					"inputName","basesEmisionAutoInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name= INPUT, location="/jsp/error.jsp")
	})
	public String generarReporteBasesEmisionAuto(){	
		
		if(idRamo == null)
			idRamo = BigDecimal.ZERO;
		if(subRamoId == null)
			subRamoId = BigDecimal.ZERO;
		if(idTipoReaseguro == null)
			idTipoReaseguro = BigDecimal.ZERO;
		if(nivelAgrupamiento == null)
			nivelAgrupamiento = -1;
		
		//se crean los parametros de entrada para el reporte.
		DatosBasesEmisionParametrosDTO parametros = new DatosBasesEmisionParametrosDTO(fechaInicial, fechaFinal, 
				idRamo.intValue(), subRamoId.intValue(), idTipoReaseguro.intValue(), nivelAgrupamiento);
		//se invoca la generación del reporte BasesEmisionAuto
		TransporteImpresionDTO transporteImpresionDTO =  impresionesService.imprimirBasesEmision(parametros, getLocale());
		
		basesEmisionAutoInputStream= new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
		contentType = ConstantesReporte.TIPO_XLS;
		fileName = "BasesEmisionAuto_FI_"+obtenerFechaFormateada(fechaInicial)+"_FF_"+
		obtenerFechaFormateada(fechaFinal)+".xls";	
		
		return SUCCESS;
	}
	private String obtenerFechaFormateada(Date fecha){
		SimpleDateFormat dateFor = new SimpleDateFormat("ddMMyyyy");
		return dateFor.format(fecha);
	}

	private List<NivelAgrupamiento> obtenerNivelAgrupamientoLista(){
		List<NivelAgrupamiento> lista = new ArrayList<ReporteBasesEmisionAction.NivelAgrupamiento>(1);
		//lista.add(new NivelAgrupamiento(0, this.getText("midas.auto.reportes.basesEmision.nivelContratoReaseguro")));
		lista.add(new NivelAgrupamiento(1, this.getText("midas.auto.reportes.basesEmision.endoso")));
		lista.add(new NivelAgrupamiento(2, this.getText("midas.auto.reportes.basesEmision.poliza")));
		lista.add(new NivelAgrupamiento(3, this.getText("midas.auto.reportes.basesEmision.subRamo")));
		lista.add(new NivelAgrupamiento(4, this.getText("midas.auto.reportes.basesEmision.ramo")));
		lista.add(new NivelAgrupamiento(5, this.getText("midas.auto.reportes.basesEmision.oficinaEmision")));
		return lista;
	}
	
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public Date getFechaInicial() {
		return fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	

	public BigDecimal getIdRamo() {
		return idRamo;
	}

	public void setIdRamo(BigDecimal idRamo) {
		this.idRamo = idRamo;
	}

	public void setSubRamoId(BigDecimal subRamoId) {
		this.subRamoId = subRamoId;
	}
	public BigDecimal getSubRamoId() {
		return subRamoId;
	}

	public BigDecimal getIdTipoReaseguro() {
		return idTipoReaseguro;
	}

	public void setIdTipoReaseguro(BigDecimal idTipoReaseguro) {
		this.idTipoReaseguro = idTipoReaseguro;
	}

	public List<TipoReaseguroDTO> getTipoReaseguroLista() {
		return tipoReaseguroLista;
	}

	public void setTipoReaseguroLista(List<TipoReaseguroDTO> tipoReaseguroLista) {
		this.tipoReaseguroLista = tipoReaseguroLista;
	}

	public Short getNivelAgrupamiento() {
		return nivelAgrupamiento;
	}

	public void setNivelAgrupamiento(Short nivelAgrupamiento) {
		this.nivelAgrupamiento = nivelAgrupamiento;
	}

	public InputStream getBasesEmisionAutoInputStream() {
		return basesEmisionAutoInputStream;
	}

	public void setBasesEmisionAutoInputStream(
			InputStream basesEmisionAutoInputStream) {
		this.basesEmisionAutoInputStream = basesEmisionAutoInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	private ImpresionesService impresionesService;
	
	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}

	public RamoFacadeRemote getRamoFacadeRemote() {
		return ramoFacadeRemote;
	}

	@Autowired
	@Qualifier("ramoFacadeRemoteEJB")
	public void setRamoFacadeRemote(RamoFacadeRemote ramoFacadeRemote) {
		this.ramoFacadeRemote = ramoFacadeRemote;
	}

	public SubRamoFacadeRemote getSubRamoFacadeRemote() {
		return subRamoFacadeRemote;
	}
	@Autowired
	@Qualifier("subRamoFacadeRemoteEJB")
	public void setSubRamoFacadeRemote(SubRamoFacadeRemote subRamoFacadeRemote) {
		this.subRamoFacadeRemote = subRamoFacadeRemote;
	}
	
	public TipoReaseguroFacadeRemote getTipoReaseguroFacadeRemote() {
		return tipoReaseguroFacadeRemote;
	}

	@Autowired
	@Qualifier("tipoReaseguroFacadeRemoteEJB")
	public void setTipoReaseguroFacadeRemote(
			TipoReaseguroFacadeRemote tipoReaseguroFacadeRemote) {
		this.tipoReaseguroFacadeRemote = tipoReaseguroFacadeRemote;
	}

	public List<RamoDTO> getRamoLista() {
		return ramoLista;
	}

	public void setRamoLista(List<RamoDTO> ramoLista) {
		this.ramoLista = ramoLista;
	}

	public List<SubRamoDTO> getSubRamoLista() {
		return subRamoLista;
	}

	public void setSubRamoLista(List<SubRamoDTO> subRamoLista) {
		this.subRamoLista = subRamoLista;
	}
	
		
	public List<NivelAgrupamiento> getNivelAgrupamientoLista() {
		return nivelAgrupamientoLista;
	}

	public void setNivelAgrupamientoLista(
			List<NivelAgrupamiento> nivelAgrupamientoLista) {
		this.nivelAgrupamientoLista = nivelAgrupamientoLista;
	}


	public static class NivelAgrupamiento{
		private int id;
		private String nombre;
		
		public NivelAgrupamiento(){}
		public NivelAgrupamiento(int id, String nombre) {
			super();
			this.id = id;
			this.nombre = nombre;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		
	}
}
