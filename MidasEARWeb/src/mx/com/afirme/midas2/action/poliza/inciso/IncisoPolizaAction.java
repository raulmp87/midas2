package mx.com.afirme.midas2.action.poliza.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class IncisoPolizaAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7238046077636282324L;
	private IncisoViewService incisoService;
	
	private Collection<BitemporalInciso> biIncisoList;
	private Collection<BitemporalInciso> biIncisoCanceladosList;
	private BitemporalInciso biInciso;	
	private BitemporalAutoInciso biAutoInciso;	
	private Collection<BitemporalCoberturaSeccion> biCoberturaSeccionList;
	private Set<String> descripcionesIncisos = new HashSet<String>();
	
	private ResumenCostosDTO 	resumenCostosDTO;
	private EstiloVehiculoDTO estiloVehiculoDTO;
	private List<CoberturaCotizacionDTO> coberturaCotizacionList;
	
	private Long cotizacionId;
	private Long incisoId;
	private Date validoEn;
	private Date recordFrom;
	private Long validoEnMillis;
	private Long recordFromMillis;
	private Short claveTipoEndoso;
	
	private Long polizaId;
	private Short mostrarCancelados;
	private Integer aplicaFlotillas;
	
	private String name;
	private Boolean requerido;
	private Map<String, String> valores;
	private List<ControlDinamicoRiesgoDTO> controles;
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService;
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	private IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
	
	
	
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("configuracionDatoIncisoBitemporalServiceEJB")
	public void setConfiguracionDatoIncisoService(ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}
	
	@Autowired
    @Qualifier("listadoIncisosDinamicoServiceEJB")
	public void setListadoIncisosDinamicoService(
			ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}
	
	
	@Override
	public void prepare() throws Exception {
	}
	
	public String verDetalleInciso() {
		getInciso();
		
		return SUCCESS;
	}
	
	public String verDetalleAsegurado() {		
		getInciso();
		return SUCCESS;
	}
	
	private void getInciso() {
		//biInciso = incisoService.getInciso(incisoId, validoEn);
		//biAutoInciso = incisoService.getAutoInciso(incisoId, validoEn);
		
		//if (biInciso == null) {
		biInciso = incisoService.getInciso(incisoId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()), 
				EndosoDTO.TIPO_ENDOSO_CANCELACION);
			//}
		
			//if (biAutoInciso == null) {
		biAutoInciso = incisoService.getAutoInciso(incisoId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()), 
				EndosoDTO.TIPO_ENDOSO_CANCELACION);
			//}
	}
	
	public String verCaractVehiculo() {
		return SUCCESS;
	}
	
	public String verDetalle() {
		return SUCCESS;
	}
	
	public void prepareMostrarIncisos() {
		if (descripcionesIncisos == null || descripcionesIncisos.isEmpty()) {
			descripcionesIncisos = listadoIncisosDinamicoService.listarIncisosDescripcion((short) 0, cotizacionId, validoEn);
		}
	}

	public String mostrarIncisos() {
		return SUCCESS;
	}
	
	/**
	 * Lista los incisos de una cotizacion
	 * @return
	 */
	public String listarIncisos(){
		/*BigDecimal negocioSeccionId = null;
		if(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId() != null){
			negocioSeccionId = BigDecimal.valueOf(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId());
		}	
		List<IncisoCotizacionDTO> listaIncisos = 
			incisoService.listarIncisosConFiltro(incisoCotizacion.getId().getIdToCotizacion(),incisoCotizacion.getNumeroSecuencia(), negocioSeccionId,this.descripcionInciso,incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
		if(listaIncisos.isEmpty()){
				setMensajeError("No existen incisos para esta cotizacion");
			}else{
				setMensajeExito();
				setIncisoCotizacionList(listaIncisos);
			}*/
		return SUCCESS;
	}
	
	public String busquedaPaginada() {
		Collection<BitemporalInciso> listado = new ArrayList<BitemporalInciso>();

		if (getTotalCount() == null) {
			listado = incisoService.getLstIncisoCanceladosByCotizacion(filtros, cotizacionId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()),true);
						
			setTotalCount(new Integer(listado.size()).longValue());
		}
		setPosPaginado();
		return SUCCESS;
	}
	
	public String busquedaPaginadaCancelados() {
		Collection<BitemporalInciso> listado = new ArrayList<BitemporalInciso>();

		if (getTotalCount() == null) {
			listado = incisoService.getLstIncisosCanceladosTotales(filtros, cotizacionId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()),true);
		
			setTotalCount(new Integer(listado.size()).longValue());
		}
		setPosPaginado();
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String buscarIncisos() {		
		Collection<BitemporalInciso> listado = new ArrayList<BitemporalInciso>();
		
		if (!super.listadoDeCache()) {
			filtros.setPrimerRegistroACargar(getPosStart());
			filtros.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
			listado = incisoService.getLstIncisoCanceladosByCotizacion(filtros, cotizacionId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()),false);

			//Obtiene descripcion de inciso formal
			if (listado != null && !listado.isEmpty()) {
				List<BitemporalInciso> lista = new ArrayList<BitemporalInciso>(1);
				for (BitemporalInciso inciso : listado) {
					try {
						fillInciso(inciso);
						
					} catch(Exception e) {
					}
					
					if (lista.isEmpty()) {
						lista.add(inciso);
					} else {
						int i = 0;
						boolean agregado = false;
						for (BitemporalInciso incisoSort : lista) {
							if (incisoSort.getValue().getNumeroSecuencia().compareTo(inciso.getValue().getNumeroSecuencia()) >= 0 ) {
								lista.add(i, inciso);
								agregado = true;
								break;
							}
							i++;
						}
						if (!agregado) {
							lista.add(inciso);
						}
					}
				}
				listado = lista;
			}			
			
		} else {
			listado = (List<BitemporalInciso>) getListadoPaginado();	
		}
		
		setBiIncisoList(listado);
		setListadoEnSession(listado);

		return SUCCESS;
	}
	
	public String buscarIncisosCancelados() {		
		Collection<BitemporalInciso> listado = new ArrayList<BitemporalInciso>();
		
		filtros.setPrimerRegistroACargar(getPosStart());
		filtros.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
		listado = incisoService.getLstIncisosCanceladosTotales(filtros, cotizacionId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()),false);
			
		if (listado != null && !listado.isEmpty()) {
			for (BitemporalInciso inciso : listado) {
				try {
					fillInciso(inciso);
					
				} catch(Exception e) {
				}
			}
		}			
		
		setBiIncisoList(listado);
		setListadoEnSession(listado);

		return SUCCESS;
	}
		

	
	public String buscarVehiculo(){
		return SUCCESS;
	}
	
	public String mostrarResumenTotalesInciso(){
		//resumenCostosDTO = incisoService.obtenerResumenInciso(cotizacionId, incisoCotizacion.getIncisoAutoCot(), incisoCotizacion, coberturaCotizacionList);
		return SUCCESS;
	}
	
	public String mostrarCorberturaCotizacion() {
		biCoberturaSeccionList = incisoService.getLstCoberturasByInciso(incisoId, new DateTime(validoEnMillis), 
																			new DateTime(recordFromMillis), claveTipoEndoso, false);
		//Solo Contratadas
		if(biCoberturaSeccionList != null && !biCoberturaSeccionList.isEmpty()){
			List<BitemporalCoberturaSeccion> listContratadas = new ArrayList<BitemporalCoberturaSeccion>(1);
			for(BitemporalCoberturaSeccion item : biCoberturaSeccionList){
				if(item.getValue().getClaveContrato().equals((short) 1)){
					listContratadas.add(item);
				}
			}
			biCoberturaSeccionList = listContratadas;
		}
		return SUCCESS;
	}
	
	public String coberturaCotizacion() {
		return SUCCESS;
	}
	
	
	@SuppressWarnings("unchecked")
	public String cargaControles(){
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		if (name == null) {
			name="datosRiesgo";
		}
		if (requerido == null) {
			requerido = true;
		}
		valores = (Map<String, String>) valueStack.findValue(name,Map.class);
		if (valores == null) {
			valores = new LinkedHashMap<String, String>();
		}
		
		controles = configuracionDatoIncisoService.getDatosRiesgo(incisoId, new Date(validoEnMillis), new Date(recordFromMillis),	
																				valores, cotizacionId, null, claveTipoEndoso, false);

		return SUCCESS;
	}
	
	private void fillInciso(BitemporalInciso inciso) {
		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(new BigDecimal(inciso.getContinuity().getCotizacionContinuity().getNumero()));
		id.setNumeroInciso(new BigDecimal(inciso.getContinuity().getNumero()));
		IncisoCotizacionDTO incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, id);
		inciso.getValue().setDescripcionGiroAsegurado(incisoCotizacion.getIncisoAutoCot().getDescripcionFinal());
	}

	public Collection<BitemporalInciso> getBiIncisoList() {
		return biIncisoList;
	}
	
	public void setBiIncisoList(Collection<BitemporalInciso> biIncisoList) {
		this.biIncisoList = biIncisoList;
	}	

	public BitemporalInciso getBiInciso() {
		return biInciso;
	}
	
	public void setBiInciso(BitemporalInciso biInciso) {
		this.biInciso = biInciso;
	}
	
	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	
	public BitemporalAutoInciso getBiAutoInciso() {
		return biAutoInciso;
	}
	
	public void setBiAutoInciso(BitemporalAutoInciso biAutoInciso) {
		this.biAutoInciso = biAutoInciso;
	}
	
	public EstiloVehiculoDTO getEstiloVehiculoDTO() {
		return estiloVehiculoDTO;
	}
	
	public void setEstiloVehiculoDTO(EstiloVehiculoDTO estiloVehiculoDTO) {
		this.estiloVehiculoDTO = estiloVehiculoDTO;
	}
	
	public Collection<BitemporalCoberturaSeccion> getBiCoberturaSeccionList() {
		return biCoberturaSeccionList;
	}
	
	public void setBiCoberturaSeccionList(List<BitemporalCoberturaSeccion> biCoberturaSeccionList) {
		this.biCoberturaSeccionList = biCoberturaSeccionList;
	}
	
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionList() {
		return coberturaCotizacionList;
	}
	
	public void setCoberturaCotizacionList(
			List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		this.coberturaCotizacionList = coberturaCotizacionList;
	}
	
	public Long getCotizacionId() {
		return cotizacionId;
	}
	
	public void setCotizacionId(Long cotizacionId) {
		this.cotizacionId = cotizacionId;
	}
	
	public Long getIncisoId() {
		return incisoId;
	}
	
	public void setIncisoId(Long incisoId) {
		this.incisoId = incisoId;
	}
	
	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}
	
	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}

	public void setControles(List<ControlDinamicoRiesgoDTO> controles) {
		this.controles = controles;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Long getValidoEnMillis() {
		return validoEnMillis;
	}

	public void setValidoEnMillis(Long validoEnMillis) {
		this.validoEnMillis = validoEnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public Collection<BitemporalInciso> getBiIncisoCanceladosList() {
		return biIncisoCanceladosList;
	}

	public void setBiIncisoCanceladosList(
			Collection<BitemporalInciso> biIncisoCanceladosList) {
		this.biIncisoCanceladosList = biIncisoCanceladosList;
	}

	public Short getMostrarCancelados() {
		return mostrarCancelados;
	}

	public void setMostrarCancelados(Short mostrarCancelados) {
		this.mostrarCancelados = mostrarCancelados;
	}

	public IncisoCotizacionDTO getFiltros() {
		return filtros;
	}

	public void setFiltros(IncisoCotizacionDTO filtros) {
		this.filtros = filtros;
	}

	public Set<String> getDescripcionesIncisos() {
		return descripcionesIncisos;
	}

	public void setDescripcionesIncisos(Set<String> descripcionesIncisos) {
		this.descripcionesIncisos = descripcionesIncisos;
	}

	public Integer getAplicaFlotillas() {
		return aplicaFlotillas;
	}

	public void setAplicaFlotillas(Integer aplicaFlotillas) {
		this.aplicaFlotillas = aplicaFlotillas;
	}
	
	
	
}
