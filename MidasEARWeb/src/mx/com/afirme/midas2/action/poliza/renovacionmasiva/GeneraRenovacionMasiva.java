package mx.com.afirme.midas2.action.poliza.renovacionmasiva;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO.ArchivosPendientes;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.action.poliza.seguroobligatorio.GeneraSeguroObligatorio;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCobertura;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionId;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDetId;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexa;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.poliza.renovacionmasiva.ValidacionRenovacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GeneraRenovacionMasiva {
	//TODO Colocar los Autowired aqui y no setear los services desde RenovacionMasivaAction
	public static final Logger LOG = Logger.getLogger(RenovacionMasivaAction.class);
	private RenovacionMasivaService renovacionMasivaService;
	private CondicionEspecialBitemporalService condicionEspBitService;
	private CondicionEspecialService condicionEspecialService;
	private List<EntidadBitemporal> datoSeccionContinuityList = new ArrayList<EntidadBitemporal>(1);
	private BigDecimal idToPolizaOriginal;
	private Double descuentoNoSiniestro = 0.0;
	private String mensajeValidacionNuevoNegocio = null;
	private Boolean esAutoPlazoCotizacion = false;
	private String mensaje = "";
	private String mensajeError = "";
	private boolean unExito;
	private Map<BigDecimal, BigDecimal> mapNumeroSecuencia = new LinkedHashMap<BigDecimal, BigDecimal>();
	private NegocioEstadoDescuentoService negocioEstadoDescuentoService;
	private ListadoService listadoService;
	private CalculoService calculoService;
	private SeguroObligatorioService seguroObligatorioService;
	
	private static final BigDecimal COBERTURA_DM_COTIZACION_ORIGINAL= new BigDecimal(4816);
	private static final BigDecimal COBERTURA_RT_COTIZACION_ORIGINAL= new BigDecimal(4817);
	private static final BigDecimal COBERTURA_DM_COTIZACION_NUEVA= new BigDecimal(2510);
	private static final BigDecimal COBERTURA_RT_COTIZACION_NUEVA= new BigDecimal(2520);
	
	private static final BigDecimal COBERTURA_DM_COTIZACION_ORIGINAL_CAM= new BigDecimal(4818);
	private static final BigDecimal COBERTURA_RT_COTIZACION_ORIGINAL_CAM= new BigDecimal(4819);
	private static final BigDecimal COBERTURA_DM_COTIZACION_NUEVA_CAM= new BigDecimal(2710);
	private static final BigDecimal COBERTURA_RT_COTIZACION_NUEVA_CAM= new BigDecimal(2720);
	
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}
	
	public void setRenovacionMasivaService(RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}
	
	public RenovacionMasivaService getRenovacionMasivaService() {
		return renovacionMasivaService;
	}

	public String getMensajeValidacionNuevoNegocio() {
		return mensajeValidacionNuevoNegocio;
	}

	public void setMensajeValidacionNuevoNegocio(
			String mensajeValidacionNuevoNegocio) {
		this.mensajeValidacionNuevoNegocio = mensajeValidacionNuevoNegocio;
	}
	
	public CondicionEspecialBitemporalService getCondicionEspBitService() {
		return condicionEspBitService;
	}

	public void setCondicionEspBitService(
			CondicionEspecialBitemporalService condicionEspBitService) {
		this.condicionEspBitService = condicionEspBitService;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}

	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}

	public void setNegocioEstadoDescuentoService(
			NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}

	public NegocioEstadoDescuentoService getNegocioEstadoDescuentoService() {
		return negocioEstadoDescuentoService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public CalculoService getCalculoService() {
		return calculoService;
	}

	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	public List<ValidacionRenovacionDTO> verificaPolizasARenovar(List<PolizaDTO> polizas, Short accionRenovacion, NegocioSeccion negocioSeccion){
		List<ValidacionRenovacionDTO> validaciones = new ArrayList<ValidacionRenovacionDTO>(1);
		for(PolizaDTO item : polizas){
			PolizaDTO polizaRenovar = renovacionMasivaService.getPolizaDTOById(item.getIdToPoliza());
			System.out.println("RenovacionMasiva: " + polizaRenovar.getNumeroPolizaFormateada() + " validando ");
			Boolean esValida = true;
			Short nuevoNegocio = 3;
			String mensajeError = "";
			Negocio negocio = new Negocio();
			
			if(accionRenovacion.equals(nuevoNegocio) && negocioSeccion != null){
				negocio = negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio();
			}else{
				negocio = polizaRenovar.getCotizacionDTO().getSolicitudDTO().getNegocio();
			}
			
			LOG.trace("Limpia bitemporalidad");
			renovacionMasivaService.fixEndosoRenovacion(polizaRenovar);
			
			if(esValida){
				LOG.trace("Inicia validacion de poliza en proceso de renovacion");
				esValida = this.validarPolizaEnProcesoRenovacion(polizaRenovar);
				mensajeError = "P\u00F3liza en proceso de Renovaci\u00F3n";
			}
			
			if(esValida){
				LOG.trace("Inicia validacion de vigencia");
				esValida = this.validarVigencia(polizaRenovar, negocio);
				mensajeError = "Limitaci\u00F3n de Vigencia";
			}
			
			if(esValida){
				LOG.trace("Inicia validacion de diferimiento");
				esValida = this.validarDiferimiento(polizaRenovar, negocio);
				mensajeError = "Limitaci\u00F3n por Diferimiento";
			}
			if(esValida){
				LOG.trace("Inicia validacion de cotizaciones");
				esValida = this.validarCotizacionPorRenovar(polizaRenovar);
				mensajeError = "Cotizaci\u00F3n por Renovaci\u00F3n Vigente";
			}
			if(esValida){
				LOG.trace("Inicia validacion de endosos");
				esValida = this.validarEndososPorRenovar(polizaRenovar);
				mensajeError = "Cotizaciones de Endosos Vigente";
			}
			if(esValida){
				LOG.trace("Inicia validacion de condiciones renovacion");
				esValida = this.validarCondicionesNegocioRenovacion(polizaRenovar, negocio);
				mensajeError = "Condiciones de Renovaci\u00F3n del Negocio";
			}
			if(esValida){
				LOG.trace("Inicia validacion de incisos con perdida total");
				esValida = this.validarIncisoConPerdidaTotal(polizaRenovar);
				mensajeError = "La p\u00F3liza no puede ser renovada, por perdida total en Inciso";
			}
			
			if(esValida && accionRenovacion.equals(nuevoNegocio)){
				if(negocioSeccion != null){
					mensajeValidacionNuevoNegocio = null;
					LOG.trace("Inicia validacion de NuevoNegocio");
					esValida = this.validarNuevoNegocio(polizaRenovar, negocioSeccion);
					if(mensajeValidacionNuevoNegocio != null){
						mensajeError = mensajeValidacionNuevoNegocio;
					}else{
						mensajeError = "Nuevo negocio no valido para Cotizaci\u00F3n";
					}
				}else{
					esValida = false;
					mensajeError = "Error en Linea de Negocio";					
				}
			}
			LOG.trace("Termina validacion");
			
			if(!esValida){
				ValidacionRenovacionDTO validacion = new ValidacionRenovacionDTO();
				validacion.setIdToPoliza(polizaRenovar.getIdToPoliza());
				validacion.setNumeroPolizaFormateada(polizaRenovar.getNumeroPolizaFormateada());
				validacion.setMensajeError(mensajeError);
				validaciones.add(validacion);
			}
		}
		return validaciones;
	}
	
	private Boolean validarVigencia(PolizaDTO poliza, Negocio negocio){
		Boolean esValido = true;
		BitemporalCotizacion cotizacion = null;
		try{
		cotizacion = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(
				poliza.getCotizacionDTO().getIdToCotizacion());
		}catch(Exception e){
			//
		}
		if(cotizacion == null){
			esValido = false;
		}
		return esValido;
	}
	
	private Boolean validarDiferimiento(PolizaDTO poliza, Negocio negocio){
		Boolean esValido = true;
		if(poliza.getCotizacionDTO().getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_EN_PROCESO ||
				poliza.getCotizacionDTO().getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_TERMINADA ||
				poliza.getCotizacionDTO().getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_EMITIDA){
			BigDecimal diasDiferimiento = negocio.getDiasDiferimiento();
			Double dias = UtileriasWeb.obtenerDiasEntreFechas(new Date(), poliza.getCotizacionDTO().getFechaFinVigencia());
			if(dias < 0 || diasDiferimiento.doubleValue() < dias){
				esValido = false;
			}
		}
		return esValido;
	}
	
	private Boolean validarCotizacionPorRenovar(PolizaDTO poliza){
		Boolean esValido = true;
		//Busca si la Poliza posee una cotizacion en proceso
		/*

		*/
		//Busca Polizas Renovadas con cotizaciones pendientes
		if(esValido){
			List<CotizacionDTO> cotizacionList = renovacionMasivaService.obtieneCotizacionesPorPolizaAnterior(poliza.getIdToPoliza());
			if(cotizacionList != null && !cotizacionList.isEmpty()){
					esValido = false;
			}
		}
		return esValido;
	}
	
	private Boolean validarPolizaEnProcesoRenovacion(PolizaDTO poliza){
		Boolean esValido = true;
		if(esValido){
			List<OrdenRenovacionMasivaDet> cotizacionList = renovacionMasivaService.obtienePolizaEnProcesoRenovacion(poliza.getIdToPoliza());
			if(cotizacionList != null && !cotizacionList.isEmpty()){
					esValido = false;
			}
		}
		return esValido;
	}
	
	private Boolean validarEndososPorRenovar(PolizaDTO poliza){
		Boolean esValido = true;
		//Busca si tiene Endosos en proceso //EndosoServiceImpl initCotizacionEndoso
		try{
			BitemporalCotizacion cotizacion = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(
				poliza.getCotizacionDTO().getIdToCotizacion());
			//valida que no tenga records en proceso la cotización.
			if (cotizacion != null && cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {
				esValido = false;
			}
		}catch(Exception e){
			LOG.error(e);
		}
		
		return esValido;
	}
	
	private Boolean validarCondicionesNegocioRenovacion(PolizaDTO poliza, Negocio negocio){
		Boolean esValido = true;
		
		Short tipoRiesgo = renovacionMasivaService.obtieneTipoRiesgo(poliza.getCotizacionDTO().getIdToCotizacion());		
		NegocioRenovacionId idNormal = new NegocioRenovacionId(negocio.getIdToNegocio(), tipoRiesgo);
		NegocioRenovacion negocioRenovacion = renovacionMasivaService.obtieneNegocioRenovacionById(idNormal);
		if(negocioRenovacion != null){
			if(!negocioRenovacion.getRenuevaPoliza()){
				esValido = false;
			}
			if(negocioRenovacion.getDetenerNumeroIncisos()){
				BigDecimal totalIncisos = getTotalIncisos(poliza.getCotizacionDTO().getIdToCotizacion());
				if(totalIncisos.intValue() >= negocioRenovacion.getNumeroIncisos().intValue()){
					esValido = false;
				}
			}
		}
		
		return esValido;
	}
	
	/**
	 * Valida que si los incisos de la póliza tienen pérdida total
	 * @param poliza
	 * @param negocio
	 * @return
	 */
	private Boolean validarIncisoConPerdidaTotal(PolizaDTO poliza){
		Boolean esValido = true;	
		try{
			MensajeDTO mensaje = renovacionMasivaService.polizaConIncisosPerdidaTotal(poliza.getIdToPoliza());
			if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
				mensajeValidacionNuevoNegocio = mensaje.getMensaje();
				esValido = false;
			}
		}catch(Exception e){
			esValido = false;
			LOG.error(e);
		}
		return esValido;
	}
	
	@SuppressWarnings("rawtypes")
	private BigDecimal getTotalIncisos(BigDecimal idToCotizacion){
		BigDecimal totalIncisos = BigDecimal.ZERO;
		try{
			BitemporalCotizacion cotizacion = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(idToCotizacion);
			List<EntidadBitemporal> incisoCotinuityList = renovacionMasivaService.obtenerIncisoCotinuityList(cotizacion.getContinuity().getId());
		
			for(EntidadBitemporal item : incisoCotinuityList){			
				BitemporalInciso bitemporalInciso = (BitemporalInciso) item;
				if(bitemporalInciso != null && bitemporalInciso.getValue() != null){
					totalIncisos = totalIncisos.add(BigDecimal.ONE); 
				}
			}		
		}catch(Exception e){
			LOG.error(e);
		}
		return totalIncisos;
	}
	
	private Boolean validarNuevoNegocio(PolizaDTO poliza, NegocioSeccion negocioSeccion){
		Boolean esValido = true;	
		try{
			MensajeDTO mensaje = renovacionMasivaService.getValidaNuevoNegocio(poliza.getIdToPoliza(), negocioSeccion.getIdToNegSeccion());
			if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
				mensajeValidacionNuevoNegocio = mensaje.getMensaje();
				esValido = false;
			}
		}catch(Exception e){
			esValido = false;
			LOG.error(e);
		}
		return esValido;
	}
	
	public BigDecimal validaRenovacionMasiva(List<PolizaDTO> polizas, Short accionRenovacion, NegocioSeccion negocioSeccion) {
		BigDecimal idToOrdenRenovacion = null;
		List<OrdenRenovacionMasivaDet> detalleList = null;
		switch(accionRenovacion){
		case 1:
			//Renovar y Emitir
			detalleList = renovarEmitir(polizas);
			break;
		case 2:
			//Cotizar y Emitir
			detalleList = cotizarEmitir(polizas);
			break;
		case 3:
			//Cotizar y Renovar con Otro Negocio
			detalleList = cotizarRenovarNuevoNegocio(polizas, negocioSeccion);
			break;
		}
		idToOrdenRenovacion = guardarOrdenRenovacion(detalleList, accionRenovacion);
		
		return idToOrdenRenovacion;
	}
	
	private List<OrdenRenovacionMasivaDet> renovarEmitir(List<PolizaDTO> polizas){
		List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaList = new ArrayList<OrdenRenovacionMasivaDet>(1);
		for(PolizaDTO item : polizas){
			setDescuentoNoSiniestro(0.0);
			mensaje = "";
			PolizaDTO polizaRenovar = renovacionMasivaService.getPolizaDTOById(item.getIdToPoliza());

			LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " en Renovacion");
			idToPolizaOriginal = item.getIdToPoliza();
			CotizacionDTO cotizacionDTO = null;
			try{
				cotizacionDTO =  creaCotizacionDTOPoliza(polizaRenovar, true);
			}catch(Exception e){
				LOG.error(e);
			}
			
			if(cotizacionDTO != null){
				BigDecimal idToCotizacion = null;
				try{
					cotizacionDTO = actualizaVersionCarga(cotizacionDTO);
					cotizacionDTO = guardaCotizacionARenovar(cotizacionDTO, polizaRenovar.getNumeroRenovacion());
					idToCotizacion = cotizacionDTO.getIdToCotizacion();
				
					actualizaCondicionesRenovacionNegocio(cotizacionDTO, true, polizaRenovar.getIdToPoliza());

    				if(cotizacionDTO != null){
    					try{
    						
    						TerminarCotizacionDTO terminarCotizacionDTO = renovacionMasivaService.terminarCotizacion(cotizacionDTO.getIdToCotizacion());
    						
    						if(terminarCotizacionDTO != null){
    							String mensajeLocal = "";
    							OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
    							OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
    							id.setIdToPoliza(item.getIdToPoliza());
    							detalle.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
    							
    							if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_MONTO.getEstatus().shortValue()
    									|| terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
    								mensajeError = mensajeError + polizaRenovar.getIdToPoliza() + "*" + polizaRenovar.getNumeroPolizaFormateada() + "*";
    								
    								if(terminarCotizacionDTO.getExcepcionesList() != null && !terminarCotizacionDTO.getExcepcionesList().isEmpty()){
    									for(ExcepcionSuscripcionReporteDTO exception: terminarCotizacionDTO.getExcepcionesList()){
    										mensajeError = mensajeError + exception.getDescripcionExcepcion() + "\n" ;
    										mensajeLocal = mensajeLocal + exception.getDescripcionExcepcion() + "\n" ;
    									}
    								} else {
    									mensajeError = mensajeError + terminarCotizacionDTO.getMensajeError();
    									mensajeLocal = terminarCotizacionDTO.getMensajeError();
    								}
    								mensajeError = mensajeError + "|";
    							}
    							
    							detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_COT);
    							detalle.setId(id);
    							detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
    							detalle.setMensaje(mensajeLocal);
    							ordenRenovacionMasivaList.add(detalle);
    							
    						} else {
    							
    							PolizaDTO poliza = emiteCotizacionARenovar(cotizacionDTO);

    							OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
    							OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
    							id.setIdToPoliza(item.getIdToPoliza());
    							detalle.setIdToCotizacion(cotizacionDTO
									.getIdToCotizacion());
    							if (poliza != null) {
    								detalle.setIdToPolizaRenovada(poliza
    										.getIdToPoliza());
    								detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
    								LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fue Renovada");
    							}else{
    								detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION);
    								LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fallo Renovacion");
    							}
    							detalle.setId(id);
    							detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
    							detalle.setMensaje(mensaje);
    							ordenRenovacionMasivaList.add(detalle);
    							
    							if(!this.unExito){
    								this.unExito = Boolean.TRUE;
    							}
							}
    					}catch(Exception e){
    						mensaje = "Error al Emitir: " + e.getMessage();
    						OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
    						OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
    						id.setIdToPoliza(item.getIdToPoliza());				
    						detalle.setId(id);
    						detalle.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
    						detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION);
    						detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
    						detalle.setMensaje(mensaje);
    						ordenRenovacionMasivaList.add(detalle);
    						LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fallo Renovacion");
    						LOG.error(e);
    					}
    				}
				}catch(Exception e){
					mensaje = "Error al Emitir: " + e.getMessage();
					OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
					OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
					id.setIdToPoliza(item.getIdToPoliza());				
					detalle.setId(id);
					detalle.setIdToCotizacion(idToCotizacion);
					if(idToCotizacion != null){
						detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION);
					}else{
						detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_COT);
					}
					detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
					detalle.setMensaje(mensaje);
					ordenRenovacionMasivaList.add(detalle);
					LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fallo Renovacion");
					LOG.error(e);
				}
			}else{				
				OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToPoliza(item.getIdToPoliza());				
				detalle.setId(id);
				detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_COT);
				detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
				detalle.setMensaje(mensaje);
				ordenRenovacionMasivaList.add(detalle);
				LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fallo Renovacion");
			}
		}
		return ordenRenovacionMasivaList;
	}
	
	private CotizacionDTO creaCotizacionDTOPoliza(PolizaDTO poliza, boolean renovarEmitir){
		CotizacionDTO cotizacionARenovar = null;
		
		BitemporalCotizacion cotizacion = null;
		try{
		cotizacion = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(
				poliza.getCotizacionDTO().getIdToCotizacion());
		}catch(Exception e){
			//
		}
		
		if(cotizacion != null){
			//Obtiene cotizacion de endosos
			cotizacionARenovar = obtieneCotizacionDeBitTemporal(cotizacion, renovarEmitir);
		}else{
			//Obtiene cotizacion normal
			//cotizacionARenovar = renovacionMasivaService.obtenerCotizacionPorId(poliza.getCotizacionDTO().getIdToCotizacion());
		}
		
		if(cotizacionARenovar != null){
			ajusteValoresCotizacion(cotizacionARenovar, poliza);
			agregaDocumentos(cotizacion, cotizacionARenovar, poliza);
			cotizacionARenovar.setIdToCotizacion(null);		
			
		//Crea solicitud
		SolicitudDTO nuevaSolicitud = new SolicitudDTO();
		try{
			SolicitudDTO ultSolicitud = renovacionMasivaService.obtieneSolicitudRenovacion(poliza.getCotizacionDTO().getIdToCotizacion());
			if(ultSolicitud != null){
				BeanUtils.copyProperties(ultSolicitud, nuevaSolicitud);
			}else{
				BeanUtils.copyProperties(cotizacionARenovar.getSolicitudDTO(), nuevaSolicitud);
			}
		}catch(Exception e){
			BeanUtils.copyProperties(cotizacionARenovar.getSolicitudDTO(), nuevaSolicitud);
		}
		if(nuevaSolicitud.getNegocio() == null){
			nuevaSolicitud.setNegocio(cotizacionARenovar.getSolicitudDTO().getNegocio());
		}

		nuevaSolicitud.setIdToSolicitud(null);
		nuevaSolicitud.setIdToPolizaAnterior(poliza.getIdToPoliza());
		nuevaSolicitud.setEsRenovacion((short) 1);

		List<IncisoCotizacionDTO> incisos = cotizacionARenovar.getIncisoCotizacionDTOs();
		List<IncisoCotizacionDTO> incisosARenovar = new ArrayList<IncisoCotizacionDTO>(1);
		for(IncisoCotizacionDTO incisoCotizacion : incisos){
			IncisoCotizacionDTO nuevoIncisoCotizacion = new IncisoCotizacionDTO();
			BeanUtils.copyProperties(incisoCotizacion, nuevoIncisoCotizacion);
			incisosARenovar.add(nuevoIncisoCotizacion);
		}
		
		cotizacionARenovar.setSolicitudDTO(nuevaSolicitud);
		cotizacionARenovar.setIncisoCotizacionDTOs(incisosARenovar);
		ajusteFechasVigencia(cotizacionARenovar);
		if(renovarEmitir){
			agregarCondicionesEspecialesCotizacion(cotizacion, cotizacionARenovar);
		}
		
		}else{
			mensaje = "Error al Crear Cotizacion de Bitemporal";
		}
		//
		
		return cotizacionARenovar;
	}
	
	@SuppressWarnings("rawtypes")
	private void agregaDocumentos(BitemporalCotizacion cotizacion, CotizacionDTO cotizacionARenovar, PolizaDTO poliza){

		//List<ArchivosPendientes> archivosPendientes = null;
		//List<ComisionCotizacionDTO> comisionCotizacionDTOs = null;		
		//List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs = null;
		//List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO = null;
		//List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO = null;
		//List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO = null;
		//List<Comentario> comentarioList = null;
		
		//Solo agrega texto adicional en polizas no migradas
		if(poliza != null && poliza.getNumeroPolizaSeycos() == null){
			try{
				List<TexAdicionalCotDTO> textAdicionalCotDTOs = new ArrayList<TexAdicionalCotDTO>(1);
				List<EntidadBitemporal> texAdicionalContinuityList =  renovacionMasivaService.obtenerTexAdicionalCotCotinuityList(cotizacion.getContinuity().getId());
				for(EntidadBitemporal item2 : texAdicionalContinuityList){
					BitemporalTexAdicionalCot bitemporalTexAdicionalCot = (BitemporalTexAdicionalCot) item2;
					
					TexAdicionalCotDTO texAdicionalCot = new TexAdicionalCotDTO(bitemporalTexAdicionalCot.getValue());
					if(texAdicionalCot.getDescripcionTexto() != null && IsRichText(texAdicionalCot.getDescripcionTexto())){
						RTFEditorKit rtfParser = new RTFEditorKit();
						Document document = rtfParser.createDefaultDocument();
						rtfParser.read(IOUtils.toInputStream(texAdicionalCot.getDescripcionTexto()), document, 0);
						String text = document.getText(0, document.getLength());
						texAdicionalCot.setDescripcionTexto(text);
					}
					textAdicionalCotDTOs.add(texAdicionalCot);
				}
				if(textAdicionalCotDTOs != null && textAdicionalCotDTOs.size() > 0){
					cotizacionARenovar.setTexAdicionalCotDTOs(textAdicionalCotDTOs);
				}
			}catch(Exception e){
				LOG.error(e);
			}
		}
	}
	
	private boolean IsRichText(String testString)
    {
        if ((testString != null) &&
            (testString.trim().startsWith("{\\rtf")))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	
	@SuppressWarnings("rawtypes")
	private CotizacionDTO obtieneCotizacionDeBitTemporal(BitemporalCotizacion cotizacion, boolean renovarEmitir){
		CotizacionDTO cotizacionDTO = new CotizacionDTO(cotizacion.getValue());
		datoSeccionContinuityList = new ArrayList<EntidadBitemporal>(1);
		if(cotizacionDTO.getIdDomicilioContratante() == null){
			try{
				ClienteGenericoDTO cliente = renovacionMasivaService.obtieneClienteContratantePorId(cotizacionDTO.getIdToPersonaContratante());
				cotizacionDTO.setIdDomicilioContratante(new BigDecimal(cliente.getIdDomicilioConsulta()));
			}catch(Exception e){
				LOG.error(e);
			}
		}
		
		List<IncisoCotizacionDTO> incisos = new ArrayList<IncisoCotizacionDTO>(1);
		List<EntidadBitemporal> incisoCotinuityList = renovacionMasivaService.obtenerIncisoCotinuityList(cotizacion.getContinuity().getId());
		BigDecimal numeroIncisoSec = BigDecimal.ZERO;
		mapNumeroSecuencia.clear();
		
		//Ordena los incisos
		try{
		Collections.sort(incisoCotinuityList, new Comparator<EntidadBitemporal>(){
			public int compare(EntidadBitemporal o1, EntidadBitemporal o2) {
				if(o1 == null || o2 == null){
					return 1;
				}
				return ((BitemporalInciso)o1).getContinuity().getNumero().compareTo(
						((BitemporalInciso)o2).getContinuity().getNumero());
			}
		});
		}catch(Exception e){
			
		}
		
		for(EntidadBitemporal item : incisoCotinuityList){			
			BitemporalInciso bitemporalInciso = (BitemporalInciso) item;
			if(bitemporalInciso != null && bitemporalInciso.getValue() != null){
			
			numeroIncisoSec = numeroIncisoSec.add(BigDecimal.ONE);
			IncisoCotizacionDTO inciso = new IncisoCotizacionDTO(bitemporalInciso.getValue());
			//inciso.getId().setNumeroInciso(new BigDecimal(bitemporalInciso.getContinuity().getNumero()));
			mapNumeroSecuencia.put(new BigDecimal(bitemporalInciso.getContinuity().getNumero()), numeroIncisoSec);
			inciso.getId().setNumeroInciso(numeroIncisoSec);
			inciso.setNumeroSecuencia(numeroIncisoSec.longValue());
			
			List<EntidadBitemporal> autoIncisoCotinuityList = renovacionMasivaService.obtenerAutoIncisoCotinuityList(bitemporalInciso.getContinuity().getId());
			if(autoIncisoCotinuityList != null && autoIncisoCotinuityList.size() > 0){
				BitemporalAutoInciso autoIncisoContinuity = (BitemporalAutoInciso) autoIncisoCotinuityList.get(0);
				IncisoAutoCot incisoAutoCot = new IncisoAutoCot(autoIncisoContinuity.getValue());
				incisoAutoCot.setObservacionesSesa(null);
				inciso.setIncisoAutoCot(incisoAutoCot);
			}
			
			
			List<SeccionCotizacionDTO> seccionCotizacionList = new ArrayList<SeccionCotizacionDTO>(1);
			List<EntidadBitemporal> seccionIncisoCotinuityList =  renovacionMasivaService.obtenerSeccionIncisoCotinuityList(bitemporalInciso.getContinuity().getId());
			for(EntidadBitemporal item2 : seccionIncisoCotinuityList){
				BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso) item2;
				if(bitemporalSeccionInciso != null && bitemporalSeccionInciso.getValue() != null){
				SeccionCotizacionDTO seccionInciso = new SeccionCotizacionDTO(bitemporalSeccionInciso.getValue());
				seccionInciso.setSeccionDTO(bitemporalSeccionInciso.getContinuity().getSeccion());
				
				try{
					List<EntidadBitemporal> datoSeccionContinuityListTemp = renovacionMasivaService.obtenerDatoSeccionContinuityList(bitemporalSeccionInciso.getContinuity().getId());
					if(datoSeccionContinuityListTemp != null && !datoSeccionContinuityListTemp.isEmpty()){
						datoSeccionContinuityList.addAll(datoSeccionContinuityListTemp);
					}
				}catch(Exception e){
				}
				
				List<CoberturaCotizacionDTO> coberturaCotizacionLista = new ArrayList<CoberturaCotizacionDTO>(1);
				List<EntidadBitemporal> coberturasCotinuityList = renovacionMasivaService.obtenerCoberturasCotinuityList(bitemporalSeccionInciso.getContinuity().getId());
				for(EntidadBitemporal item3 : coberturasCotinuityList){
					BitemporalCoberturaSeccion bitemporalCoberturaSeccion = (BitemporalCoberturaSeccion) item3;
					if(bitemporalCoberturaSeccion != null && bitemporalCoberturaSeccion.getValue() != null){
					CoberturaCotizacionDTO coberturaCotizacion =  new CoberturaCotizacionDTO(bitemporalCoberturaSeccion.getValue());
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToSeccion(seccionInciso.getSeccionDTO().getIdToSeccion());
					id.setIdToCobertura(bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura());
					coberturaCotizacion.setId(id);
					
					if(coberturaCotizacion.getClaveTipoDeducible() == null){
						try{
							CoberturaDTO cobertura = renovacionMasivaService.getCobertura(coberturaCotizacion.getId().getIdToCobertura());
							coberturaCotizacion.setClaveTipoDeducible(Short.parseShort(cobertura.getClaveTipoDeducible()));
						}catch(Exception e){
							LOG.error(e);
						}
					}
					coberturaCotizacionLista.add(coberturaCotizacion);
					}
				}
				
				seccionInciso.setCoberturaCotizacionLista(coberturaCotizacionLista);
				
				
				seccionCotizacionList.add(seccionInciso);
			}
			}
			inciso.setSeccionCotizacionList(seccionCotizacionList);
			if(renovarEmitir){
				List<BitemporalCondicionEspInc> condicionesEspInc = condicionEspBitService.obtenerCondicionesIncisoContinuity(bitemporalInciso.getContinuity().getId());
				if(condicionesEspInc != null){
					for(BitemporalCondicionEspInc bitCondEspInc: condicionesEspInc){
						inciso.getCondicionesEspeciales().add(condicionEspecialService.obtenerCondicion(bitCondEspInc.getContinuity().getCondicionEspecialId()));
					}
				}
			}
			incisos.add(inciso);
		}
		}
		cotizacionDTO.setIncisoCotizacionDTOs(incisos);
		
		return cotizacionDTO;
	}
	
	private void setCoberturaValorSumaAsegurada(List<CoberturaCotizacionDTO> list,BigDecimal idToCobertura, Double valorSumaAsegurada){
		for(CoberturaCotizacionDTO coberturaNueva: list){
			if ((coberturaNueva.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_DM_COTIZACION_NUEVA) && idToCobertura.equals(GeneraRenovacionMasiva.COBERTURA_DM_COTIZACION_ORIGINAL)) ||
				(coberturaNueva.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_RT_COTIZACION_NUEVA) && idToCobertura.equals(GeneraRenovacionMasiva.COBERTURA_RT_COTIZACION_ORIGINAL)) ||
				(coberturaNueva.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_DM_COTIZACION_NUEVA_CAM) && idToCobertura.equals(GeneraRenovacionMasiva.COBERTURA_DM_COTIZACION_ORIGINAL_CAM)) ||
				(coberturaNueva.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_RT_COTIZACION_NUEVA_CAM) && idToCobertura.equals(GeneraRenovacionMasiva.COBERTURA_RT_COTIZACION_ORIGINAL_CAM))){
				coberturaNueva.setValorSumaAsegurada(valorSumaAsegurada);
				break;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void agregaCoberturasNegocio(IncisoCotizacionDTO incisoCotizacion, List<CoberturaCotizacionDTO> coberturaCotizacionLista, Short tipoRiesgo){
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = renovacionMasivaService.obtenerNegocioPaqueteSeccionPorId(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId()); 
		List<CoberturaCotizacionDTO> list = renovacionMasivaService.obtenerCoberturasNegocio(negocioPaqueteSeccion, incisoCotizacion);
		
		Double deducibleDM = 0.0;
		Double deducibleRT = 0.0;
		
		//asignar suma asegurada de DM y RT
		if (esAutoPlazoCotizacion){
			for(CoberturaCotizacionDTO coberturaOriginal: coberturaCotizacionLista){
				if (coberturaOriginal.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_DM_COTIZACION_ORIGINAL) ||
					coberturaOriginal.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_RT_COTIZACION_ORIGINAL) ||
					coberturaOriginal.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_DM_COTIZACION_ORIGINAL_CAM) ||
					coberturaOriginal.getId().getIdToCobertura().equals(GeneraRenovacionMasiva.COBERTURA_RT_COTIZACION_ORIGINAL_CAM)){
					this.setCoberturaValorSumaAsegurada(list,coberturaOriginal.getId().getIdToCobertura(),coberturaOriginal.getValorSumaAsegurada());
				}
			}
		}
		
		//Elimina cobertura de poliza que no esta en negocio
		for(int i = 0; i < coberturaCotizacionLista.size(); i++){
			CoberturaCotizacionDTO coberturaCotizacion = coberturaCotizacionLista.get(i);
			
			final BigDecimal idToCobertura = coberturaCotizacion.getId().getIdToCobertura();
			
			CoberturaDTO cobertura = renovacionMasivaService.getCobertura(idToCobertura);
			if(cobertura.getClaveTipoCalculo().equals(CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES)){
				deducibleDM = coberturaCotizacion.getPorcentajeDeducible();
			}else if(cobertura.getClaveTipoCalculo().equals(CoberturaDTO.CLAVE_CALCULO_ROBO_TOTAL)){
				deducibleRT = coberturaCotizacion.getPorcentajeDeducible();
			}
			
			Predicate predicate = new Predicate() {	
				@Override
				public boolean evaluate(Object arg0) {
					CoberturaCotizacionDTO config = (CoberturaCotizacionDTO)arg0;
					return config.getId().getIdToCobertura().equals(idToCobertura);
				}
			};	
				
			List<CoberturaCotizacionDTO> coberturaList = (List<CoberturaCotizacionDTO>) CollectionUtils.select(list, predicate);
			if(coberturaList == null || coberturaList.isEmpty()){
				coberturaCotizacionLista.remove(i);
				i--;
			}
		}
		
		//Agregar cobertura de poliza nueva en negocio
		for(CoberturaCotizacionDTO coberturaNegocio: list){
			final BigDecimal idToCobertura = coberturaNegocio.getId().getIdToCobertura();
			Predicate predicate = new Predicate() {	
				@Override
				public boolean evaluate(Object arg0) {
					CoberturaCotizacionDTO config = (CoberturaCotizacionDTO)arg0;
					return config.getId().getIdToCobertura().equals(idToCobertura);
				}
			};	
				
			List<CoberturaCotizacionDTO> coberturaList = (List<CoberturaCotizacionDTO>) CollectionUtils.select(coberturaCotizacionLista, predicate);
			if(coberturaList == null || coberturaList.isEmpty()){
				//YA NO APLICA Si es cobertura nueva obligatoria la contrata; excepto en autoplazo donde no la contrata pero da la opcion de contratarla
				/*
				if(esAutoPlazoCotizacion){
					coberturaNegocio.setClaveContrato((short) 0);
					coberturaNegocio.setClaveObligatoriedad(CoberturaDTO.OPCIONAL_DEFAULT);
				}*/
				CoberturaDTO coberturaAdd = renovacionMasivaService.getCobertura(coberturaNegocio.getId().getIdToCobertura());
				if(coberturaAdd.getClaveTipoCalculo().equals(CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES)){
					coberturaNegocio.setValorDeducible(deducibleDM);
					coberturaNegocio.setPorcentajeDeducible(deducibleDM);
				}else if(coberturaAdd.getClaveTipoCalculo().equals(CoberturaDTO.CLAVE_CALCULO_ROBO_TOTAL)){
					coberturaNegocio.setValorDeducible(deducibleRT);
					coberturaNegocio.setPorcentajeDeducible(deducibleRT);
				}
				
				coberturaCotizacionLista.add(coberturaNegocio);
			}else{				
				CoberturaCotizacionDTO coberturaCotizacion = coberturaList.get(0);
				coberturaCotizacion.setClaveObligatoriedad(coberturaNegocio.getClaveObligatoriedad());
				
				//Si la condicion de renovacion indica que aplica el default de la cobertura se actualiza
				NegocioRenovacionCobertura negocioRenovacionCobertura = new NegocioRenovacionCobertura();
				try{
					negocioRenovacionCobertura = renovacionMasivaService.obtenerNegocioRenovacionCobertura(negocioPaqueteSeccion, tipoRiesgo, 
							idToCobertura);
				}catch(Exception e){					
				}
				
				boolean aplicaAjusteSA = false;
				if(negocioRenovacionCobertura!=null){
					aplicaAjusteSA = negocioRenovacionCobertura.getAplicaDefault().intValue()==1;
				}
				//YA NO APLICA No contrata la cobertura aunque sea obligatoria en autoplazo
				//if(!esAutoPlazoCotizacion){
					if(coberturaNegocio.getClaveObligatoriedad().intValue() == 0 && coberturaNegocio.getClaveContrato().intValue() == 1){
						coberturaCotizacion.setClaveContrato(coberturaNegocio.getClaveContrato());
						if(coberturaCotizacion.getValorSumaAsegurada() == null || 
								coberturaCotizacion.getValorSumaAsegurada().doubleValue() == 0){
							coberturaCotizacion.setValorSumaAsegurada(coberturaNegocio.getValorSumaAsegurada());
						}
					}
			
					//Si es opcional default y es autoplazo la contrata
					if(esAutoPlazoCotizacion && coberturaNegocio.getClaveObligatoriedad().intValue() == 1){
						coberturaCotizacion.setClaveContrato(coberturaNegocio.getClaveContrato());
						if(coberturaCotizacion.getValorSumaAsegurada() == null  || 
								coberturaCotizacion.getValorSumaAsegurada().doubleValue() == 0){
							coberturaCotizacion.setValorSumaAsegurada(coberturaNegocio.getValorSumaAsegurada());
						}
					}

					if(coberturaCotizacion.getValorSumaAsegurada()<coberturaNegocio.getValorSumaAsegurada() 
							&& aplicaAjusteSA){
							coberturaCotizacion.setValorSumaAsegurada(coberturaNegocio.getValorSumaAsegurada());
					}
				//}
			}
		}

	}
	
	private void ajusteValoresCotizacion(CotizacionDTO cotizacionARenovar, PolizaDTO poliza){

		if(cotizacionARenovar.getPorcentajeIva() == null){
			if(poliza.getCotizacionDTO().getPorcentajeIva() != null){
				cotizacionARenovar.setPorcentajeIva(poliza.getCotizacionDTO().getPorcentajeIva());
			}else{
				cotizacionARenovar.setPorcentajeIva(new Double(16));
			}			
		}
		if(cotizacionARenovar.getPorcentajePagoFraccionado() == null){
			if(poliza.getCotizacionDTO().getPorcentajePagoFraccionado() != null){
				cotizacionARenovar.setPorcentajePagoFraccionado(poliza.getCotizacionDTO().getPorcentajePagoFraccionado());
			}else{
				cotizacionARenovar.setPorcentajePagoFraccionado(new Double(0));
			}	
		}
		if(cotizacionARenovar.getPorcentajebonifcomision() == null){
			if(poliza.getCotizacionDTO().getPorcentajebonifcomision() != null){
				cotizacionARenovar.setPorcentajebonifcomision(poliza.getCotizacionDTO().getPorcentajebonifcomision());
			}else{
				cotizacionARenovar.setPorcentajebonifcomision(new Double(0));
			}	
		}
		
		if(cotizacionARenovar.getNegocioTipoPoliza() == null){
			NegocioProducto negocioProducto = renovacionMasivaService.getNegocioProductoByCotizacion(cotizacionARenovar);
			NegocioTipoPoliza negocioTipoPoliza = renovacionMasivaService.getPorIdNegocioProductoIdToTipoPoliza(negocioProducto.getIdToNegProducto(), cotizacionARenovar.getTipoPolizaDTO().getIdToTipoPoliza());
			cotizacionARenovar.setNegocioTipoPoliza(negocioTipoPoliza);
		}
	}
	
	private void ajusteFechasVigencia(CotizacionDTO cotizacionARenovar){
		//Date fechaIniVigencia = cotizacionARenovar.getFechaInicioVigencia();
		Date fechaFinVigencia = cotizacionARenovar.getFechaFinVigencia();
		
		//Double diasVigencia = UtileriasWeb.obtenerDiasEntreFechas(fechaIniVigencia, fechaFinVigencia);
		//Fecha Inicio Vigencia = FechaFinVigencia Anterior mas 1
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(fechaFinVigencia);
		//cal.add(Calendar.DAY_OF_YEAR, 1);		
		cotizacionARenovar.setFechaInicioVigencia(cal.getTime());
		
		//if (diasVigencia > cal.getMaximum(Calendar.DAY_OF_YEAR)) {
		cotizacionARenovar.setFechaFinVigencia(UtileriasWeb.sumaMeses(cotizacionARenovar.getFechaInicioVigencia(), 12));
		//} else {
		//	GregorianCalendar cal2 = new GregorianCalendar();
		//	cal2.setTime(cotizacionARenovar.getFechaInicioVigencia());
		//	cal2.add(Calendar.DAY_OF_YEAR, diasVigencia.intValue());
		//	cotizacionARenovar.setFechaFinVigencia(cal2.getTime());
		//}
		
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private CotizacionDTO guardaCotizacionARenovar(CotizacionDTO cotizacionDTO, Integer numeroRenovacion) throws Exception{
		List<IncisoCotizacionDTO> incisos = cotizacionDTO.getIncisoCotizacionDTOs();
		cotizacionDTO.setIncisoCotizacionDTOs(null);
		
		List<ArchivosPendientes> archivosPendientes = new ArrayList<ArchivosPendientes>(1);
		if(cotizacionDTO.getArchivosPendientes() != null){
			archivosPendientes.addAll(cotizacionDTO.getArchivosPendientes());
			cotizacionDTO.getArchivosPendientes().clear();
		}
		List<ComisionCotizacionDTO> comisionCotizacionDTOs = new ArrayList<ComisionCotizacionDTO>(1);
		if(cotizacionDTO.getComisionCotizacionDTOs() != null){
			comisionCotizacionDTOs.addAll(cotizacionDTO.getComisionCotizacionDTOs());
			cotizacionDTO.getComisionCotizacionDTOs().clear();
		}
		List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs = new ArrayList<DocAnexoCotDTO>(1);
		if(cotizacionDTO.getDocumentoAnexoCotizacionDTOs() != null){
			documentoAnexoCotizacionDTOs.addAll(cotizacionDTO.getDocumentoAnexoCotizacionDTOs()); 
			cotizacionDTO.getDocumentoAnexoCotizacionDTOs().clear();
		}
		List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO = new ArrayList<DocumentoAnexoReaseguroCotizacionDTO>(1);
		if(cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs() != null){
			documentoAnexoReaseguroCotizacionDTO.addAll(cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs());
			cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs().clear();
		}
		List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO = new ArrayList<DocumentoDigitalCotizacionDTO>(1);
		if(cotizacionDTO.getDocumentoDigitalCotizacionDTOs() != null){
			documentoDigitalCotizacionDTO.addAll(cotizacionDTO.getDocumentoDigitalCotizacionDTOs()); 
			cotizacionDTO.getDocumentoDigitalCotizacionDTOs().clear();
		}
		List<TexAdicionalCotDTO> textAdicionalCotDTOs = new ArrayList<TexAdicionalCotDTO>(1);
		if(cotizacionDTO.getTexAdicionalCotDTOs() != null){
			textAdicionalCotDTOs.addAll(cotizacionDTO.getTexAdicionalCotDTOs());
			cotizacionDTO.getTexAdicionalCotDTOs().clear();
		}
		
		/*if(cotizacionDTO.getNegocioDerechoPoliza() != null){
			negocioDerechoPoliza.setIdToNegDerechoPoliza(cotizacionDTO.getNegocioDerechoPoliza().getIdToNegDerechoPoliza());
			cotizacionDTO.setNegocioDerechoPoliza(null);
		}*/
		
		
		List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO = new ArrayList<DocumentoDigitalSolicitudDTO>(1);
		if(cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs() != null){
			documentoDigitalSolicitudDTO.addAll(cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs());
			cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs().clear();
		}
		List<Comentario> comentarioList = new ArrayList<Comentario>(1);
		if(cotizacionDTO.getSolicitudDTO().getComentarioList() != null){
			comentarioList.addAll(cotizacionDTO.getSolicitudDTO().getComentarioList());
			cotizacionDTO.getSolicitudDTO().getComentarioList().clear();
		}
		
		//Crea Cotizacion con solicitud
		cotizacionDTO = renovacionMasivaService.crearCotizacion(cotizacionDTO);
		//
		//Agrega datos adicionales de cotizacion
		guardaDocumentos(archivosPendientes, comisionCotizacionDTOs, documentoAnexoCotizacionDTOs, documentoAnexoReaseguroCotizacionDTO,
				documentoDigitalCotizacionDTO, textAdicionalCotDTOs, documentoDigitalSolicitudDTO, comentarioList, 
				cotizacionDTO);
		
		//Obtiene si es AutoPlazo
		this.esAutoPlazo(incisos); 
		
		NegocioDerechoPoliza negocioDerechoPoliza = new NegocioDerechoPoliza();
		if(!esAutoPlazoCotizacion){
			negocioDerechoPoliza = renovacionMasivaService.obtenerDechosPolizaDefault(
												cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
			cotizacionDTO.setNegocioDerechoPoliza(negocioDerechoPoliza);
		}
		
		BigDecimal pctRPF = getPorcentajeRecargoFraccionado(cotizacionDTO);
		if(pctRPF != null){
			cotizacionDTO.setPorcentajePagoFraccionado(pctRPF.doubleValue());
			cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(pctRPF.doubleValue());
		}
		
		cotizacionDTO = renovacionMasivaService.guardarCotizacion(cotizacionDTO);		

		//Obtiene tipoRiesgo
		Short tipoRiesgo = renovacionMasivaService.obtieneTipoRiesgo(cotizacionDTO.getIdToCotizacion());
		StringBuilder observacionesAutoplazo = new StringBuilder("");
		for(IncisoCotizacionDTO nuevoIncisoCotizacion: incisos){
		try {				
			nuevoIncisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
			nuevoIncisoCotizacion.getIncisoAutoCot().setId(null);
			nuevoIncisoCotizacion.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			nuevoIncisoCotizacion.setCotizacionDTO(cotizacionDTO);
			List<CoberturaCotizacionDTO> coberturaCotizacionList = creaListadoCoberturas(nuevoIncisoCotizacion.getSeccionCotizacionList().get(0).getCoberturaCotizacionLista(), cotizacionDTO.getIdToCotizacion());
			try{
				//if(!esAutoPlazoCotizacion){
					agregaCoberturasNegocio(nuevoIncisoCotizacion, coberturaCotizacionList, tipoRiesgo);
				//}
			}catch(Exception e){
				LOG.error(e);
			}	

			
			//Otorgar el maximo descuento por negocio-estado permitido
			Double pctDescuentoEstado = 0.0;
			if(listadoService.getAplicaDescuentoNegocioPaqueteSeccion(nuevoIncisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId())) {
				NegocioEstadoDescuento negocioEstadoDescuento = new NegocioEstadoDescuento();
				negocioEstadoDescuento = negocioEstadoDescuentoService.findByNegocioAndEstado(
						cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), 
						nuevoIncisoCotizacion.getIncisoAutoCot().getEstadoId());
				if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuento() != null) {
					pctDescuentoEstado = negocioEstadoDescuento.getPctDescuento();
				}
			}
			nuevoIncisoCotizacion.getIncisoAutoCot().setPctDescuentoEstado(pctDescuentoEstado);

			if(esAutoPlazoCotizacion){
				observacionesAutoplazo.delete(0, observacionesAutoplazo.length());
				observacionesAutoplazo.append(nuevoIncisoCotizacion.getIncisoAutoCot().getObservacionesinciso());
				if (numeroRenovacion==0){
					String condicionAutoplazo = "POLIZA SUJETA A LAS CONDICIONES ESPECIALES DEL CERTIFICADO DE GARANTIA AUTOPLAZO.";
					if(nuevoIncisoCotizacion.getSeccionCotizacion() != null && 
							!nuevoIncisoCotizacion.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES)
							&& !UtileriasWeb.containsIgnoreCase(observacionesAutoplazo.toString().toUpperCase(), "GARANTIA AUTOPLAZO") ){
						
						observacionesAutoplazo.insert(0,condicionAutoplazo+"\n ").append(" ");
						if(observacionesAutoplazo.toString().length() > 499){
							String observacionesAutoplazoStr = observacionesAutoplazo.substring(0, 499);
							observacionesAutoplazo.delete(0, observacionesAutoplazo.length());
							observacionesAutoplazo.append(observacionesAutoplazoStr);
						}
					}
				}
				nuevoIncisoCotizacion.getIncisoAutoCot().setObservacionesinciso(observacionesAutoplazo.toString());
			}else{
				nuevoIncisoCotizacion.getIncisoAutoCot().setObservacionesinciso(null);
			}
			
			nuevoIncisoCotizacion.getSeccionCotizacionList().get(0).setCoberturaCotizacionLista(coberturaCotizacionList);
			nuevoIncisoCotizacion = renovacionMasivaService.prepareGuardarInciso(
					cotizacionDTO.getIdToCotizacion(),
					nuevoIncisoCotizacion.getIncisoAutoCot(), nuevoIncisoCotizacion,
					coberturaCotizacionList, nuevoIncisoCotizacion.getSeccionCotizacion().getValorPrimaNeta(),
					nuevoIncisoCotizacion.getSeccionCotizacion().getValorSumaAsegurada(), 
					nuevoIncisoCotizacion.getSeccionCotizacion().getClaveContrato(),
					nuevoIncisoCotizacion.getSeccionCotizacion().getClaveObligatoriedad());
			
			//DEBE HACERSE ANTES DEL CALCULO
			if(this.getDatoSeccionContinuityList() != null){
				for(EntidadBitemporal item : getDatoSeccionContinuityList()){
					try{
						BitemporalDatoSeccion bitemporalDatoSeccion = (BitemporalDatoSeccion) item;
						if(bitemporalDatoSeccion != null && bitemporalDatoSeccion.getValue() != null ){
							BigDecimal numeroInciso = mapNumeroSecuencia.get(new BigDecimal(bitemporalDatoSeccion.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity().getNumero()));
							if(numeroInciso.equals(nuevoIncisoCotizacion.getId().getNumeroInciso())){
								DatoIncisoCotAuto datoInciso =  new DatoIncisoCotAuto();
								if(bitemporalDatoSeccion.getValue() != null && bitemporalDatoSeccion.getValue().getValor() != null){
									datoInciso.setValor(bitemporalDatoSeccion.getValue().getValor());
								}else{
									mensaje = "Datos de Riesgo default 0";
									datoInciso.setValor("0");
								}
								datoInciso.setClaveDetalle(new BigDecimal(bitemporalDatoSeccion.getContinuity().getClaveDetalle()));
								datoInciso.setIdDato(new BigDecimal(bitemporalDatoSeccion.getContinuity().getDatoId()));
								datoInciso.setIdTcRamo(new BigDecimal(bitemporalDatoSeccion.getContinuity().getRamoId()));
								datoInciso.setIdTcSubRamo(new BigDecimal(bitemporalDatoSeccion.getContinuity().getSubRamoId()));
								datoInciso.setIdToCobertura(new BigDecimal(bitemporalDatoSeccion.getContinuity().getCoberturaId()));
								datoInciso.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
								
								datoInciso.setIdToSeccion(bitemporalDatoSeccion.getContinuity().getSeccionIncisoContinuity().getSeccion().getIdToSeccion());
								datoInciso.setNumeroInciso(numeroInciso);
							
								renovacionMasivaService.saveDatoIncisoCotAuto(datoInciso);
							}
						}
					}catch(Exception e){
						LOG.error(e);
					}
				}
			}				
			
			if(!esAutoPlazoCotizacion){
				renovacionMasivaService.calcular(nuevoIncisoCotizacion);
			}
		} catch (Exception e) {
			LOG.error(e);
			if(cotizacionDTO != null && cotizacionDTO.getIdToCotizacion() != null ){
				renovacionMasivaService.eliminaCotizacionFallidaPolizaAnt(cotizacionDTO.getSolicitudDTO().getIdToPolizaAnterior());
			}
			mensaje = "Error al creal el Inciso: " + e.getMessage();
			throw new Exception("Error al creal el Inciso: " + e.getMessage());
		}
		}
		
		return cotizacionDTO;
	}
	
	/**
	 * Obtner porcentaje de recargo fraccionado que corresponde a la cotizacion
	 * @param cotizacion
	 * @return
	 */
	private BigDecimal getPorcentajeRecargoFraccionado(CotizacionDTO cotizacion){
		BigDecimal porcentajeRecargoPagoFraccionado = null;
		if (cotizacion.getSolicitudDTO().getNegocio() == null || cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado() == null || 
				!cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado().booleanValue()) {
			porcentajeRecargoPagoFraccionado = null;
		} else {
			porcentajeRecargoPagoFraccionado = cotizacion.getPorcentajePagoFraccionado() != null ? 
					BigDecimal.valueOf(cotizacion.getPorcentajePagoFraccionado().doubleValue()) : null;
		}
		if (porcentajeRecargoPagoFraccionado == null && cotizacion.getIdFormaPago() != null && cotizacion.getIdMoneda() != null) {
			Double pcte = renovacionMasivaService.getPctePagoFraccionado(cotizacion.getIdFormaPago().intValue(), 
					cotizacion.getIdMoneda().shortValue());			
			porcentajeRecargoPagoFraccionado = pcte != null ? BigDecimal.valueOf(pcte) : BigDecimal.valueOf(0);
		}
		return porcentajeRecargoPagoFraccionado;
	}
	
	private void guardaDocumentos(List<ArchivosPendientes> archivosPendientes, List<ComisionCotizacionDTO> comisionCotizacionDTOs,
			List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs, List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO,
			List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO, List<TexAdicionalCotDTO> textAdicionalCotDTOs,
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO, List<Comentario> comentarioList, 
			CotizacionDTO cotizacionDTO){
		

		if(comisionCotizacionDTOs != null && comisionCotizacionDTOs.size() > 0){
			for(ComisionCotizacionDTO item: comisionCotizacionDTOs){
				try{
					item.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveComisionCotizacionDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoAnexoCotizacionDTOs != null && documentoAnexoCotizacionDTOs.size() > 0){
			for(DocAnexoCotDTO item: documentoAnexoCotizacionDTOs){
				try{
					item.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveDocAnexoCotDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoAnexoReaseguroCotizacionDTO != null && documentoAnexoReaseguroCotizacionDTO.size() > 0){
			for(DocumentoAnexoReaseguroCotizacionDTO item: documentoAnexoReaseguroCotizacionDTO){
				try{
					item.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveDocumentoAnexoReaseguroCotizacionDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoDigitalCotizacionDTO != null && documentoDigitalCotizacionDTO.size() > 0){
			for(DocumentoDigitalCotizacionDTO item: documentoDigitalCotizacionDTO){
				try{
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveDocumentoDigitalCotizacionDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(textAdicionalCotDTOs != null && textAdicionalCotDTOs.size() > 0){
			for(TexAdicionalCotDTO item: textAdicionalCotDTOs){
				try{
					item.setCotizacion(cotizacionDTO);
					renovacionMasivaService.saveTexAdicionalCotDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoDigitalSolicitudDTO != null && documentoDigitalSolicitudDTO.size() > 0){
			for(DocumentoDigitalSolicitudDTO item: documentoDigitalSolicitudDTO){
				try{
					item.setSolicitudDTO(cotizacionDTO.getSolicitudDTO());
					renovacionMasivaService.saveDocumentoDigitalSolicitudDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(comentarioList != null && comentarioList.size() > 0){
			for(Comentario item: comentarioList){
				try{
					item.setSolicitudDTO(cotizacionDTO.getSolicitudDTO());
					renovacionMasivaService.saveComentario(item);
				}catch(Exception e){
				}
			}
		}
	}
	
	private List<CoberturaCotizacionDTO> creaListadoCoberturas(List<CoberturaCotizacionDTO> coberturaCotizacionList, BigDecimal idToCotizacion){
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>(1);
		for(CoberturaCotizacionDTO coberturaCotizacionDTO : coberturaCotizacionList){
			CoberturaCotizacionDTO nuevaCoberturaCotizacionDTO = new CoberturaCotizacionDTO();
			if(coberturaCotizacionDTO.getClaveContratoBoolean() == null){
				if(coberturaCotizacionDTO.getClaveContrato() == 1){
					coberturaCotizacionDTO.setClaveContratoBoolean(true);
				}else{
					coberturaCotizacionDTO.setClaveContratoBoolean(false);
				}
			}
			//Limpia lazy list
			if(coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion() != null){
				coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion().clear();
			}
			if(coberturaCotizacionDTO.getRiesgoCotizacionLista() != null){
				coberturaCotizacionDTO.getRiesgoCotizacionLista().clear();
			}
			BeanUtils.copyProperties(coberturaCotizacionDTO, nuevaCoberturaCotizacionDTO);
			nuevaCoberturaCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			coberturas.add(nuevaCoberturaCotizacionDTO);
		}
		return coberturas;
	}
	
	private List<OrdenRenovacionMasivaDet> cotizarEmitir(List<PolizaDTO> polizas){
		List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaList = new ArrayList<OrdenRenovacionMasivaDet>(1);
		for(PolizaDTO item : polizas){
			setDescuentoNoSiniestro(0.0);
			PolizaDTO polizaRenovar = renovacionMasivaService.getPolizaDTOById(item.getIdToPoliza());

			LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " en Renovacion");
			idToPolizaOriginal = item.getIdToPoliza();
			BigDecimal idToCotizacion = null;
			try{
			CotizacionDTO cotizacionDTO =  creaCotizacionDTOPoliza(polizaRenovar, false);
			
			if(cotizacionDTO != null){
				cotizacionDTO = actualizaVersionCarga(cotizacionDTO);
				cotizacionDTO = guardaCotizacionARenovar(cotizacionDTO, polizaRenovar.getNumeroRenovacion());
				
				actualizaCondicionesRenovacionNegocio(cotizacionDTO, false, item.getIdToPoliza());

				if(cotizacionDTO != null){
					OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
					OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
					id.setIdToPoliza(item.getIdToPoliza());
					detalle.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					detalle.setId(id);
					detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION);
					detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
					detalle.setMensaje(mensaje);
					ordenRenovacionMasivaList.add(detalle);
					LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " Cotizada");
				}
			}else{
				throw new Exception(mensaje);
			}
			}catch(Exception e){
				OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToPoliza(item.getIdToPoliza());
				detalle.setId(id);
				detalle.setIdToCotizacion(idToCotizacion);
				detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_COT);
				detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
				detalle.setMensaje(mensaje);
				ordenRenovacionMasivaList.add(detalle);
				LOG.error(e);
				LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fallo Renovacion");
			}
		}
		return ordenRenovacionMasivaList;
	}
	
	private void actualizaCondicionesRenovacionNegocio(CotizacionDTO cotizacionDTO, boolean renovarEmitir, BigDecimal idToPoliza){
		
		//IF(VEHICULO POR NORMAL - ALTO RIESGO - ALTA FRECUENCIA) SOLO APLICA FLOTILLA NO AUTOPLAZO		
		try{
			if(cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas() != 1){
				Short tipoRiesgo = renovacionMasivaService.obtieneTipoRiesgo(cotizacionDTO.getIdToCotizacion());
				//Obtiene Condiciones de renovacion del negocio
				NegocioRenovacionId idNormal = new NegocioRenovacionId(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), tipoRiesgo);
				NegocioRenovacion negocioRenovacionNormal = renovacionMasivaService.obtieneNegocioRenovacionById(idNormal);
				if(negocioRenovacionNormal != null && negocioRenovacionNormal.getRenuevaPoliza()){					
					ajustaCondicionesRenovacion(cotizacionDTO, negocioRenovacionNormal, renovarEmitir);					
					if(esAutoPlazoCotizacion){
						//Autoplazo recalculo con igualacion al monto anterior
						recalculoAutoplazo(cotizacionDTO, idToPoliza);		
					}
				}
			}
		}catch(Exception e){
			mensaje = "Error al Actualizar Condiciones de Negocio: " + e.getMessage();
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void recalculoAutoplazo(CotizacionDTO cotizacionDTO, BigDecimal idToPoliza){
		try{
			//ResumenCostosDTO resumenCosto = renovacionMasivaService.obtenerResumenCotizacion(cotizacionDTO);
			ResumenCostosDTO resumenCosto = renovacionMasivaService.resumenCostosEmitidos(idToPoliza);
			renovacionMasivaService.calcularCotizacion(cotizacionDTO);
			renovacionMasivaService.igualarPrima(cotizacionDTO.getIdToCotizacion(), resumenCosto.getPrimaTotal());
			resumenCosto = renovacionMasivaService.obtenerResumenCotizacion(cotizacionDTO);
		}catch(Exception e){
			LOG.error(e);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void ajustaCondicionesRenovacion(CotizacionDTO cotizacionDTO, NegocioRenovacion condiciones, boolean renovarEmitir){

		//Condiciones de renovacion aplicadas solo cuando no es autoplazo 
		Double recargoAumentoTarifa = 0.0;
		Double descuentoCondiciones = 0.0;
		boolean recalculo = false;
		//Ajusta descuento
		if(!esAutoPlazoCotizacion){
		if(condiciones.getMantieneDescuentoComercial()){
			//
		}
		if(condiciones.getOtorgaDescuentoComercial()){
			cotizacionDTO.setPorcentajeDescuentoGlobal(new Double(0)); //Dejara de aplicarse el descuento global
			recalculo = true;
		}
		
		if(condiciones.getMantieneCesionComision()){
			//
		}else{
			cotizacionDTO.setPorcentajebonifcomision(new Double(0));
			recalculo = true;
		}
		}
		
		//Obtiene siniestros poliza anterior
		BigDecimal siniestrosPolizaAnterior = BigDecimal.ZERO;
		if(idToPolizaOriginal != null){
			PolizaDTO polizaSin = renovacionMasivaService.getCantidadSiniestro(idToPolizaOriginal);
			if(polizaSin != null){
				try{
					siniestrosPolizaAnterior = BigDecimal.valueOf(Double.parseDouble(polizaSin.getNumeroSiniestro()));
				}catch(Exception e){}
			}
		}
		
		//Se reduce el Deducible en el porcentaje indicado si cumple con los requisitos de renovacion y siniestros
		if(condiciones.getAplicaDeducibleDanos() && cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas() != 1){
			try{				
				NegocioRenovacionDescId id = new NegocioRenovacionDescId();
				id.setIdToNegocio(condiciones.getId().getIdToNegocio());
				id.setTipoDescuento( NegocioRenovacionDesc.TipoDescuento.DEDUCIBLE.getObtenerTipo());
				id.setTipoRiesgo(condiciones.getId().getTipoRiesgo());
				if(siniestrosPolizaAnterior.intValue() > 7){
					Short siete = 7;
					id.setNumSiniestros(siete);
				}else{
					id.setNumSiniestros(siniestrosPolizaAnterior.shortValue());
				}
			
				Double porcentajeDeducible = 0.0;
				NegocioRenovacionDesc deducible = renovacionMasivaService.obtenerNegocioRenovacionDescPorId(id);
				PolizaDTO polizaAnt = renovacionMasivaService.getPolizaDTOById(idToPolizaOriginal);
				if(polizaAnt != null && deducible != null){
					switch(polizaAnt.getNumeroRenovacion()){
					case 0:
						porcentajeDeducible =  deducible.getRen1();
						break;
					case 1:
						porcentajeDeducible =  deducible.getRen2();
						break;
					case 2:
						porcentajeDeducible =  deducible.getRen3();
						break;
					case 3:
						porcentajeDeducible =  deducible.getRen4();
						break;
					case 4:
						porcentajeDeducible =  deducible.getRen5();
						break;
					case 5:
						porcentajeDeducible =  deducible.getRen6();
						break;
					default:
						porcentajeDeducible =  deducible.getRen6();
						break;
					}
					renovacionMasivaService.setNuevoPorcentajeDeducibleDaniosMateriales(cotizacionDTO, porcentajeDeducible);
					recalculo = true;
				}
			}catch(Exception e){
				LOG.error(e);
			}
		}
		
		if(!esAutoPlazoCotizacion){
			
		if(recalculo){
			cotizacionDTO = renovacionMasivaService.guardarCotizacion(cotizacionDTO);
			renovacionMasivaService.calcularCotizacion(cotizacionDTO);
		}
		
		//Obtiene siniestros de la poliza
		BigDecimal siniestrosPoliza = BigDecimal.ZERO;
		if(idToPolizaOriginal != null){
			int totalSiniestros = renovacionMasivaService.getCantidadSiniestroTotal(idToPolizaOriginal);
			siniestrosPoliza = new BigDecimal(totalSiniestros);
		}
		
		//Aplica descuento adiciones si comple con condiciones de renovacion y siniestros (solo individuales)
		if(condiciones.getAplicaDescuentoNoSinietro() && cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas() != 1){
			try{
				NegocioRenovacionDescId id = new NegocioRenovacionDescId();
				id.setIdToNegocio(condiciones.getId().getIdToNegocio());
				id.setTipoDescuento( NegocioRenovacionDesc.TipoDescuento.DESCUENTO.getObtenerTipo());
				id.setTipoRiesgo(condiciones.getId().getTipoRiesgo());
				if(siniestrosPoliza.intValue() > 7){
					Short siete = 7;
					id.setNumSiniestros(siete);
				}else{
					id.setNumSiniestros(siniestrosPoliza.shortValue());
				}
				NegocioRenovacionDesc descuento = renovacionMasivaService.obtenerNegocioRenovacionDescPorId(id);
				PolizaDTO polizaAnt = renovacionMasivaService.getPolizaDTOById(idToPolizaOriginal);
				if(polizaAnt != null && descuento != null){
					switch(polizaAnt.getNumeroRenovacion()){
					//************************se cambia la operacion para que sea con getPrimaNetaConberturasPropias debe revisarlo lizandro****************************************************
					//Ahora el descuento condiciones se aplicara por inciso
					case 0:
//							descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//										*(1+(resumenCosto.getPorcentajeIva()/100))
//										*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//										* (descuento.getRen1() / 100));
						//descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen1() / 100);
						setDescuentoNoSiniestro(descuento.getRen1());
						break;
					case 1:
//						descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//								*(1+(resumenCosto.getPorcentajeIva()/100))
//								*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//								* (descuento.getRen2() / 100));
						//descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen2() / 100);
						setDescuentoNoSiniestro(descuento.getRen2());
						break;
					case 2:
						
//						descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//								*(1+(resumenCosto.getPorcentajeIva()/100))
//								*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//								* (descuento.getRen3() / 100));
//						descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen3() / 100);
						
						setDescuentoNoSiniestro(descuento.getRen3());
						break;
					case 3:
//						descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//								*(1+(resumenCosto.getPorcentajeIva()/100))
//								*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//								* (descuento.getRen4() / 100));
						//descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen4() / 100);
						setDescuentoNoSiniestro(descuento.getRen4());
						break;
					case 4:
//						descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//								*(1+(resumenCosto.getPorcentajeIva()/100))
//								*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//								* (descuento.getRen5() / 100));
						//descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen5() / 100);
						setDescuentoNoSiniestro(descuento.getRen5());
						break;
					case 5:
//						descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//								*(1+(resumenCosto.getPorcentajeIva()/100))
//								*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//								* (descuento.getRen6() / 100));
						//descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen6() / 100);
						setDescuentoNoSiniestro(descuento.getRen6());
						break;
					default:
//						descuentoCondiciones = descuentoCondiciones + ((resumenCosto.getPrimaNetaConberturasPropias()
//								*(1+(resumenCosto.getPorcentajeIva()/100))
//								*(1+(BigDecimal.valueOf(cotizacionDTO.getPorcentajePagoFraccionado()).divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP)).doubleValue()))
//								* (descuento.getRen6() / 100));
						//descuentoCondiciones = descuentoCondiciones + resumenCosto.getPrimaTotal() * (descuento.getRen6() / 100);
						setDescuentoNoSiniestro(descuento.getRen6());
						break;
					}
					//**************************************************************************************************************************************************************************
					//Sumar el descuento de renovacion al descuento por estado.
					renovacionMasivaService.calcularDescuentoPorEstado(cotizacionDTO, getDescuentoNoSiniestro(), id, renovarEmitir);
					renovacionMasivaService.calcularCotizacion(cotizacionDTO);
				}
			}catch(Exception e){
				
			}

		}
		
		ResumenCostosDTO resumenCosto = renovacionMasivaService.obtenerResumenCotizacion(cotizacionDTO);
		
		//Aumenta la prima en el porcentaje indicado
		if(condiciones.getAumentarPctTarifa()){
			recargoAumentoTarifa = recargoAumentoTarifa + (resumenCosto.getPrimaTotal() * (condiciones.getPctTarifa() / 100));
		}
		
		//Calcula nuevos valores de cotizacion
		Double primaTotal = resumenCosto.getPrimaTotal() + recargoAumentoTarifa - descuentoCondiciones;
		boolean esPrimaAumento = false;
		//Valida limite de incremento
		if(condiciones.getLimiteIncrementoTarifa()){
			//Si no tiene Sin Limite de Siniestros o el total de siniestros es menor al limite de siniestros para aumento, se valida la prima
			try{
			if(!condiciones.getSinLimiteSinietros() || (siniestrosPoliza.compareTo(condiciones.getLimiteSiniestros()) < 0)){
				PolizaDTO polizaAnterior = renovacionMasivaService.getPolizaDTOById(idToPolizaOriginal);
				//ResumenCostosDTO resumenCostoAnterior = renovacionMasivaService.obtenerResumenCotizacion(polizaAnterior.getCotizacionDTO());
				ResumenCostosDTO resumenCostoAnterior = renovacionMasivaService.resumenCostosCaratulaPoliza(polizaAnterior.getIdToPoliza());
				Double pctAumentoTotalTarifa = (primaTotal - resumenCostoAnterior.getPrimaTotal()) / resumenCostoAnterior.getPrimaTotal() * 100;
				if(pctAumentoTotalTarifa > condiciones.getPctLimiteIncremento()){
					esPrimaAumento = true;
					primaTotal = resumenCostoAnterior.getPrimaTotal() + (resumenCostoAnterior.getPrimaTotal() * (condiciones.getPctLimiteIncremento() / 100));
				}
			}
			}catch(Exception e){
				LOG.error(e);
			}
		}
		
		//Si aplica prima anterior y no es aumento se valida prima nueva contra prima anterior; si es menor se iguala a la anterior 
		try{
			if(condiciones.getAplicarPrimaAnterior() && !esPrimaAumento){
				ResumenCostosDTO primaAnt = renovacionMasivaService.resumenCostosCaratulaPoliza(cotizacionDTO.getSolicitudDTO().getIdToPolizaAnterior());
				if(primaTotal < primaAnt.getPrimaTotal().doubleValue()){				
					primaTotal = primaAnt.getPrimaTotal().doubleValue();
				}
			} 
			renovacionMasivaService.igualarPrima(cotizacionDTO.getIdToCotizacion(), primaTotal);
			resumenCosto = renovacionMasivaService.obtenerResumenCotizacion(cotizacionDTO);
			
		}catch(Exception e){
			LOG.error(e);
		}
		
		}//Fin if(!esAutoPlazoCotizacion)
	}
	
	private List<OrdenRenovacionMasivaDet> cotizarRenovarNuevoNegocio(List<PolizaDTO> polizas, NegocioSeccion negocioSeccion){
		List<OrdenRenovacionMasivaDet> ordenRenovacionMasivaList = new ArrayList<OrdenRenovacionMasivaDet>(1);
				
		for(PolizaDTO item : polizas){
			setDescuentoNoSiniestro(0.0);
			PolizaDTO polizaRenovar = renovacionMasivaService.getPolizaDTOById(item.getIdToPoliza());
			LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " en Renovacion");
			idToPolizaOriginal = item.getIdToPoliza();
			BigDecimal idToCotizacion = null;
			try{
			CotizacionDTO cotizacionDTO = this.creaCotizacionDTOPoliza(polizaRenovar, false);			
			if(cotizacionDTO != null){
				cotizacionDTO = actualizaNuevoNegocio(cotizacionDTO, negocioSeccion);
				cotizacionDTO = guardaCotizacionARenovar(cotizacionDTO, polizaRenovar.getNumeroRenovacion());
				
				actualizaCondicionesRenovacionNegocio(cotizacionDTO,false, idToPolizaOriginal);
				
				if(cotizacionDTO != null){
					OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
					OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
					id.setIdToPoliza(item.getIdToPoliza());
					detalle.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					detalle.setId(id);
					detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION);
					detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
					detalle.setMensaje(mensaje);
					ordenRenovacionMasivaList.add(detalle);
					LOG.trace("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " Cotizada");
				}
			}else{
				throw new Exception(mensaje);
			}
			}catch(Exception e){
				mensaje = "Error: " + e.getMessage();
				OrdenRenovacionMasivaDet detalle = new OrdenRenovacionMasivaDet();
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToPoliza(item.getIdToPoliza());
				detalle.setId(id);
				detalle.setIdToCotizacion(idToCotizacion);
				detalle.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_FALLO_COT);
				detalle.setDescuentoNoSiniestro(getDescuentoNoSiniestro());
				detalle.setMensaje(mensaje);
				ordenRenovacionMasivaList.add(detalle);
				LOG.error(e);
				LOG.debug("Log Renovacion Masiva: " + polizaRenovar.getNumeroPolizaFormateada() + " fallo Renovacion");
			}
		}
		return ordenRenovacionMasivaList;
	}
	
	private CotizacionDTO actualizaNuevoNegocio(CotizacionDTO cotizacion, NegocioSeccion negocioSeccion){
		
		SolicitudDTO solicitud = cotizacion.getSolicitudDTO();
		//Negocio
		solicitud.setNegocio(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio());
		//Producto
		solicitud.setProductoDTO(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getProductoDTO());
		
		cotizacion.setSolicitudDTO(solicitud);
		
		//TipoPoliza
		cotizacion.setNegocioTipoPoliza(negocioSeccion.getNegocioTipoPoliza());
				
		//Derechos
		Double importe = null;
		if(cotizacion.getDerechosPoliza() != null){
			importe =  cotizacion.getDerechosPoliza();
		}else{
			if(cotizacion.getNegocioDerechoPoliza() != null){
				importe = cotizacion.getNegocioDerechoPoliza().getImporteDerecho();
			}
		}
		List<NegocioDerechoPoliza> negocioDerechoPolizaList = renovacionMasivaService.obtieneNegocioDerechoPolizaByNegocioImporte(
				negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), importe);
		cotizacion.setNegocioDerechoPoliza(negocioDerechoPolizaList.get(0));
		cotizacion.setDerechosPoliza(negocioDerechoPolizaList.get(0).getImporteDerecho());
		
		for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){
			//Seccion	
			 inciso.getIncisoAutoCot().setNegocioSeccionId(negocioSeccion.getIdToNegSeccion().longValue());
			
			//Paquete
			List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = renovacionMasivaService.obtieneNegocioPaqueteSeccionPorNegSeccionPaqueteId(
					negocioSeccion.getIdToNegSeccion(), inciso.getIncisoAutoCot().getPaquete().getId());
			inciso.getIncisoAutoCot().setNegocioPaqueteId(negocioPaqueteSeccionList.get(0).getIdToNegPaqueteSeccion());
			
			//Estilo
			EstiloVehiculoDTO estilo = new EstiloVehiculoDTO();
			String estiloId = inciso.getIncisoAutoCot().getEstiloId();
			EstiloVehiculoId id = new EstiloVehiculoId();
			id.setStrId(estiloId);
			estilo.setId(id);
			estilo = renovacionMasivaService.getEstiloVehiculo(inciso.getIncisoAutoCot().getMarcaId(), negocioSeccion.getIdToNegSeccion(), 
					cotizacion.getIdMoneda(), id.getClaveEstilo());
			inciso.getIncisoAutoCot().setEstiloId(estilo.getId().getStrId());
			inciso.getIncisoAutoCot().setIdVersionCarga(estilo.getId().getIdVersionCarga());
			
			
			//SeccionCotizacion
			SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacionList().get(0);
			seccionCotizacion.setSeccionDTO(negocioSeccion.getSeccionDTO());
			
			//Coberturas
			List<CoberturaCotizacionDTO> coberturas = inciso.getSeccionCotizacionList().get(0).getCoberturaCotizacionLista();
			for(CoberturaCotizacionDTO cobertura : coberturas){
				cobertura.getId().setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
			}
				
		}
		
		return cotizacion;
	}
	
	private CotizacionDTO actualizaVersionCarga(CotizacionDTO cotizacion){
				
		for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){
			//Seccion	
			Long negocioSeccionId =  inciso.getIncisoAutoCot().getNegocioSeccionId();
			
			NegocioSeccion negocioSeccion = null;
			if(negocioSeccionId != null){
				try{
					negocioSeccion = renovacionMasivaService.obtieneNegocioSeccionByid(new BigDecimal(negocioSeccionId));
				}catch(Exception e){
					LOG.error(e);
				}
			}
			
			if(negocioSeccion != null){
				//Paquete
				List<NegocioPaqueteSeccion> negocioPaqueteSeccionList = renovacionMasivaService.obtieneNegocioPaqueteSeccionPorNegSeccionPaqueteId(
						negocioSeccion.getIdToNegSeccion(), inciso.getIncisoAutoCot().getPaquete().getId());
				inciso.getIncisoAutoCot().setNegocioPaqueteId(negocioPaqueteSeccionList.get(0).getIdToNegPaqueteSeccion());
				
				//Estilo
				EstiloVehiculoDTO estilo = new EstiloVehiculoDTO();
				String estiloId = inciso.getIncisoAutoCot().getEstiloId();
				EstiloVehiculoId id = new EstiloVehiculoId();
				id.setStrId(estiloId);
				estilo.setId(id);
				estilo = renovacionMasivaService.getEstiloVehiculo(inciso.getIncisoAutoCot().getMarcaId(), negocioSeccion.getIdToNegSeccion(), 
						cotizacion.getIdMoneda(), id.getClaveEstilo());
				inciso.getIncisoAutoCot().setEstiloId(estilo.getId().getStrId());
				inciso.getIncisoAutoCot().setIdVersionCarga(estilo.getId().getIdVersionCarga());
				
				
				//SeccionCotizacion
				SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacionList().get(0);
				seccionCotizacion.setSeccionDTO(negocioSeccion.getSeccionDTO());
				
				//Coberturas
				List<CoberturaCotizacionDTO> coberturas = inciso.getSeccionCotizacionList().get(0).getCoberturaCotizacionLista();
				for(CoberturaCotizacionDTO cobertura : coberturas){
					cobertura.getId().setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
				}
			}
		}
		
		return cotizacion;
	}
	
	private BigDecimal guardarOrdenRenovacion(List<OrdenRenovacionMasivaDet> detalleList, Short accionRenovacion){
		
		Usuario usuario = renovacionMasivaService.getUsuarioActual();
		OrdenRenovacionMasiva ordenRenovacion = new OrdenRenovacionMasiva();
		ordenRenovacion.setAccionRenovacion(accionRenovacion);
		boolean ordenCancelada = true;

		ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
		for(OrdenRenovacionMasivaDet item : detalleList){
			if(!item.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_FALLO_COT)){
				ordenCancelada = false;
			}
			if(!item.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA)){
				ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_INICIADA);
			}
		}
		if(ordenCancelada){
			ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_CANCELADA);
		}
		ordenRenovacion.setFechaCreacion(new Date());
		ordenRenovacion.setFechaModificacion(new Date());
		ordenRenovacion.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		ordenRenovacion.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		ordenRenovacion.setNombreUsuarioCreacion(usuario.getNombreCompleto());
		ordenRenovacion.setNumeroPolizas(new BigDecimal(detalleList.size()));
		
		BigDecimal idOrdenRenovacion = renovacionMasivaService.saveAndGetIdOrdenRenovacion(ordenRenovacion);
		
		for(OrdenRenovacionMasivaDet item : detalleList){
			item.getId().setIdToOrdenRenovacion(idOrdenRenovacion);
			item.setCotizacion(null);
			renovacionMasivaService.saveOrdenRenovacionMasivaDet(item);
		}
		
		return idOrdenRenovacion;
	}
	
	private PolizaDTO emiteCotizacionARenovar(CotizacionDTO cotizacionDTO){
		PolizaDTO poliza = null;
		TerminarCotizacionDTO terminarCotizacionDTO = renovacionMasivaService.terminarCotizacion(cotizacionDTO.getIdToCotizacion());
		
		//Se ignoran excepcion en renovacion directa
		if(terminarCotizacionDTO != null){
			//Calcular Descuento Global
			cotizacionDTO.setPorcentajeDescuentoGlobal(calculoService.calculaPorcentajeDescuentoGlobal(cotizacionDTO.getIdToCotizacion()));
			cotizacionDTO.setClaveEstatus(CotizacionDTO.ESTATUS_COT_TERMINADA);
			renovacionMasivaService.guardarCotizacionSimple(cotizacionDTO);
		}
		
			for(IncisoCotizacionDTO incisoCotizacion: cotizacionDTO.getIncisoCotizacionDTOs()){
				renovacionMasivaService.complementarDatosInciso(incisoCotizacion);
			}
			Map<String, String> mensajeEmision = renovacionMasivaService.emitir(cotizacionDTO);
			String icono = mensajeEmision.get("icono");
			if(icono.equals("30")){
				String idPoliza = mensajeEmision.get("idpoliza");
				poliza = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
				mensaje = mensajeEmision.get("mensaje");
				try{
					if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_RENOVACION)){
						GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
						generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
						generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
						generaSeguroObligatorio.setListadoService(listadoService);
						generaSeguroObligatorio.setCalculoService(calculoService);
						PolizaDTO polizaVol = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
						generaSeguroObligatorio.creaPolizaSeguroObligatorio(polizaVol, PolizaAnexa.TIPO_AUTOMATICA);
					}
				}catch(Exception e){
					LOG.error(e);
				}				
			}else{
				mensaje = "Error al Emitir: " +mensajeEmision.get("mensaje");
			}
		
		
		return poliza;
	}
	
	
	private void esAutoPlazo(List<IncisoCotizacionDTO> list){
		esAutoPlazoCotizacion = false;
		
		try{
		if(list != null && !list.isEmpty()){
			for(IncisoCotizacionDTO item: list){
				List<SeccionCotizacionDTO> seccionCotizacionList = item.getSeccionCotizacionList();
				if(seccionCotizacionList != null && !seccionCotizacionList.isEmpty()){
					SeccionCotizacionDTO seccionCotizacion = seccionCotizacionList.get(0);
					if(seccionCotizacion.getSeccionDTO().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS) ||
						seccionCotizacion.getSeccionDTO().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES)){
						esAutoPlazoCotizacion = true;
						break;
					}
				}
			}
		}
		}catch(Exception e){
			LOG.error(e);
		}
	}
	
	public String terminarOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		StringBuilder mensajeError = new StringBuilder("");
		if(idToOrdenRenovacion != null){
			OrdenRenovacionMasiva ordenRenovacion =  renovacionMasivaService.obtieneOrdenRenovacionMasiva(idToOrdenRenovacion);
			
			List<OrdenRenovacionMasivaDet> ordenesDetalle = renovacionMasivaService.listarOrdenRenovacionDetalleByOrdenRenovacion(idToOrdenRenovacion);
			for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
				LOG.trace("RenovacionMasiva Emitiendo Poliza: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada());
				try{
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
						TerminarCotizacionDTO terminarCotizacionDTO = renovacionMasivaService.terminarCotizacion(ordenDet.getIdToCotizacion());
						if(terminarCotizacionDTO == null){
						
							Map<String, String> mensajeEmision = renovacionMasivaService.emitir(ordenDet.getCotizacion());
							String icono = mensajeEmision.get("icono");							
							if(icono.equals("30")){
								String idPoliza = mensajeEmision.get("idpoliza");
								ordenDet.setMensaje(mensajeEmision.get("mensaje"));
								ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
								ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
								
								renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
								LOG.trace("RenovacionMasiva Poliza Emitida: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada());
								try{
									if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_RENOVACION)){
										GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
										generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
										generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
										generaSeguroObligatorio.setListadoService(listadoService);
										generaSeguroObligatorio.setCalculoService(calculoService);
										PolizaDTO poliza = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
										generaSeguroObligatorio.creaPolizaSeguroObligatorio(poliza, PolizaAnexa.TIPO_AUTOMATICA);
									}
								}catch(Exception e){
									LOG.error(e);
								}	
							}else if(icono.equals("10")){
								String mensaje = mensajeEmision.get("mensaje");
								mensajeError.append("Poliza Anterior ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(
								" causo una Exception. ").append(mensaje);								
							}
						}else{
							mensajeError.append("\nERROR  EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
							mensajeError.append(terminarCotizacionDTO.getMensajeError());
						}
					}
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
						Map<String, String> mensajeEmision = renovacionMasivaService.emitir(ordenDet.getCotizacion());
						String icono = mensajeEmision.get("icono");
						if(icono.equals("30")){
							String idPoliza = mensajeEmision.get("idpoliza");
							ordenDet.setMensaje(mensajeEmision.get("mensaje"));
							ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
							ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
							renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
							LOG.trace("RenovacionMasiva Poliza Emitida: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada());
							try{
								if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_RENOVACION)){
									GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
									generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
									generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
									generaSeguroObligatorio.setListadoService(listadoService);
									generaSeguroObligatorio.setCalculoService(calculoService);
									PolizaDTO poliza = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
									generaSeguroObligatorio.creaPolizaSeguroObligatorio(poliza, PolizaAnexa.TIPO_AUTOMATICA);
								}
							}catch(Exception e){
								LOG.error(e);
							}							
						}else{
							String mensaje = mensajeEmision.get("mensaje");
							mensajeError.append("Poliza Anterior ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(
							" causo una Exception. ").append(mensaje);								
						}						
					}
					
				}
				}catch(Exception e){
					LOG.error(e);
					mensajeError.append("Poliza Anterior ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(
					" causo una Exception. ").append(e.getMessage());
				}
				if(mensajeError != null && !mensajeError.toString().isEmpty()){
					ordenDet.setMensaje(mensajeError.toString());
					renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
				}
			}
			if(mensajeError == null || mensajeError.toString().isEmpty()){
				ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
				ordenRenovacion.setFechaModificacion(new Date());
				renovacionMasivaService.saveAndGetIdOrdenRenovacion(ordenRenovacion);
			}
		}
		return mensajeError.toString();
	}
	
	public String terminarPolizasDetalleOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<PolizaDTO> polizaList) {
		StringBuilder mensajeError = new StringBuilder("");
		if(idToOrdenRenovacion != null){
			for(PolizaDTO poliza: polizaList){
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToOrdenRenovacion(idToOrdenRenovacion);
				id.setIdToPoliza(poliza.getIdToPoliza());
				OrdenRenovacionMasivaDet ordenDet = renovacionMasivaService.obtieneOrdenRenovacionMasivaDet(id);

				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
						TerminarCotizacionDTO terminarCotizacionDTO =renovacionMasivaService.terminarCotizacion(ordenDet.getIdToCotizacion());
						if(terminarCotizacionDTO == null){

							Map<String, String> mensajeEmision = renovacionMasivaService.emitir(ordenDet.getCotizacion());
							String icono = mensajeEmision.get("icono");
							if(icono.equals("30")){
								String idPoliza = mensajeEmision.get("idpoliza");
								ordenDet.setMensaje(mensajeEmision.get("mensaje"));
								ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
								ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
								renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
								
								try{
									if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_RENOVACION)){
										GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
										generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
										generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
										generaSeguroObligatorio.setListadoService(listadoService);
										generaSeguroObligatorio.setCalculoService(calculoService);
										PolizaDTO polizaVol = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
										generaSeguroObligatorio.creaPolizaSeguroObligatorio(polizaVol, PolizaAnexa.TIPO_AUTOMATICA);
									}
								}catch(Exception e){
									LOG.error(e);
								}									
							}else if(icono.equals("10")){
								String mensaje = mensajeEmision.get("mensaje");
								mensajeError.append("\nERROR EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada() ).append(" \n");
								mensajeError.append(mensaje);
							}
						}else{
							mensajeError.append("\nERROR  EN POLIZA: " ).append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
							mensajeError.append(terminarCotizacionDTO.getMensajeError());							
						}
					}
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
						TerminarCotizacionDTO validacion = renovacionMasivaService.validarEmisionCotizacion(ordenDet.getIdToCotizacion());
						if(validacion != null){
							LOG.trace(validacion.getMensajeError());
						}
						//CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, ordenDet.getIdToCotizacion());
						Map<String, String> mensajeEmision = renovacionMasivaService.emitir(ordenDet.getCotizacion());
						String icono = mensajeEmision.get("icono");
						String mensaje = mensajeEmision.get("mensaje");
						if(icono.equals("30")){
							String idPoliza = mensajeEmision.get("idpoliza");
							ordenDet.setMensaje(mensajeEmision.get("mensaje"));
							ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
							ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
							renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
							
							try{
								if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_RENOVACION)){
									GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
									generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
									generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
									generaSeguroObligatorio.setListadoService(listadoService);
									generaSeguroObligatorio.setCalculoService(calculoService);
									PolizaDTO polizaVol = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
									generaSeguroObligatorio.creaPolizaSeguroObligatorio(polizaVol, PolizaAnexa.TIPO_AUTOMATICA);
								}
							}catch(Exception e){
								LOG.error(e);
							}
						}else if(icono.equals("10")){
							mensajeError.append("\nERROR EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
							mensajeError.append(mensaje);
						}					
					}
					
				}
				if(mensajeError != null && !mensajeError.toString().isEmpty()){
					ordenDet.setMensaje(mensajeError.toString());
					renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
				}
			}
			
			//Actualiza Estatus de la Orden
			boolean ordenTerminada = true;
			OrdenRenovacionMasiva ordenRenovacion = renovacionMasivaService.obtieneOrdenRenovacionMasiva(idToOrdenRenovacion);
			if(ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_INICIADA) || ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_CONTROL_USUARIO)){
				List<OrdenRenovacionMasivaDet> ordenesDetalle = renovacionMasivaService.listarOrdenRenovacionDetalleByOrdenRenovacion(idToOrdenRenovacion);
				for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
					if(ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION) || ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION)
							|| ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION)){
						ordenTerminada = false;
					}		
				}
				if(ordenTerminada){
					ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
					ordenRenovacion.setFechaModificacion(new Date());
					renovacionMasivaService.saveAndGetIdOrdenRenovacion(ordenRenovacion);
				}
			}
		}
		return mensajeError.toString();
	}
	
	public void cancelarDetalleOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<PolizaDTO> polizaList) {
		if(idToOrdenRenovacion != null){
			for(PolizaDTO poliza: polizaList){
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToOrdenRenovacion(idToOrdenRenovacion);
				id.setIdToPoliza(poliza.getIdToPoliza());
				OrdenRenovacionMasivaDet ordenDet = renovacionMasivaService.obtieneOrdenRenovacionMasivaDet(id);
				
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					renovacionMasivaService.cancelarCotizacion(ordenDet.getId().getIdToPoliza());
					ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA);
					renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
				}
			}
			//Actualiza Estatus de la Orden
			boolean ordenTerminada = true;
			boolean ordenCancelada = true;
			OrdenRenovacionMasiva ordenRenovacion = renovacionMasivaService.obtieneOrdenRenovacionMasiva(idToOrdenRenovacion);
			if(ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_INICIADA) || ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_CONTROL_USUARIO)){
				List<OrdenRenovacionMasivaDet> ordenesDetalle = renovacionMasivaService.listarOrdenRenovacionDetalleByOrdenRenovacion(idToOrdenRenovacion);
				for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
					if(ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION) || ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION)
							|| ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION)){
						ordenTerminada = false;
					}
					if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA)){
						ordenCancelada = false;
					}
				}
				if(ordenTerminada && !ordenCancelada){
					ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
					ordenRenovacion.setFechaModificacion(new Date());
					renovacionMasivaService.saveAndGetIdOrdenRenovacion(ordenRenovacion);
				}
				if(ordenCancelada){
					ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_CANCELADA);
					ordenRenovacion.setFechaModificacion(new Date());
					renovacionMasivaService.saveAndGetIdOrdenRenovacion(ordenRenovacion);
				}
			}
		}
	}
	
	public void cancelarOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		if(idToOrdenRenovacion != null){
			OrdenRenovacionMasiva ordenRenovacion = renovacionMasivaService.obtieneOrdenRenovacionMasiva(idToOrdenRenovacion);
			
			List<OrdenRenovacionMasivaDet> ordenesDetalle = renovacionMasivaService.listarOrdenRenovacionDetalleByOrdenRenovacion(idToOrdenRenovacion);
			for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					renovacionMasivaService.cancelarCotizacion(ordenDet.getId().getIdToPoliza());
					ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA);
					renovacionMasivaService.saveOrdenRenovacionMasivaDet(ordenDet);
				}
			}
			ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_CANCELADA);
			ordenRenovacion.setFechaModificacion(new Date());
			renovacionMasivaService.saveAndGetIdOrdenRenovacion(ordenRenovacion);
		}
	}
	
	public List<BigDecimal> cancelaCotizacionesValidacion(List<ValidacionRenovacionDTO> validaciones){
		List<BigDecimal> cotizacionesCanceladas = new ArrayList<BigDecimal>(1);
		for(ValidacionRenovacionDTO validacion : validaciones){
			PolizaDTO poliza = renovacionMasivaService.getPolizaDTOById(validacion.getIdToPoliza());
			List<CotizacionDTO> cotizacionList = renovacionMasivaService.obtieneCotizacionesPorPolizaAnterior(poliza.getIdToPoliza());
			for(CotizacionDTO cotizacion : cotizacionList){
				if(cotizacion.getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_EN_PROCESO ||
						cotizacion.getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_TERMINADA){
					cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_CANCELADA);
					renovacionMasivaService.guardarCotizacionSimple(cotizacion);
					cotizacionesCanceladas.add(cotizacion.getIdToCotizacion());
				}
					
			}
			
			//Busca si tiene Endosos en proceso //EndosoServiceImpl initCotizacionEndoso
			try{
				BitemporalCotizacion cotizacionBT = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(
					poliza.getCotizacionDTO().getIdToCotizacion());
				//valida que no tenga records en proceso la cotización.
				if (cotizacionBT != null && cotizacionBT.getContinuity().getCotizaciones().hasRecordsInProcess()) {
					CotizacionEndosoDTO filtrosBusqueda = new CotizacionEndosoDTO();
					filtrosBusqueda.setBusquedaEstatusEnProceso(true);
					filtrosBusqueda.setNumeroPoliza(poliza.getNumeroPoliza());
					filtrosBusqueda.setNumeroCotizacionEndoso(poliza.getCotizacionDTO().getIdToCotizacion());
					
					List<CotizacionEndosoDTO> pendientes =  renovacionMasivaService.buscarCotizacionesEndosoFiltrado(filtrosBusqueda);
					if(pendientes != null && !pendientes.isEmpty()){
						for(CotizacionEndosoDTO pendiente: pendientes){
							try{
								BitemporalCotizacion cotizacion = new BitemporalCotizacion();
								CotizacionContinuity cotizacionContinuity = new CotizacionContinuity();
								cotizacionContinuity.setId(pendiente.getContinuityId());
								cotizacion.setContinuity(cotizacionContinuity);
								
								
								renovacionMasivaService.deleteCotizacionEndoso(pendiente);
								CotizacionContinuity continuity = renovacionMasivaService.findCotizacionContinuity(cotizacion);
								cotizacion.setContinuity(continuity);
								Map<String,Object> properties = new HashMap<String,Object>();
								properties.put("cotizacionId", continuity.getNumero());
								properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
								List<ControlEndosoCot> controlEndosoCotList = renovacionMasivaService.findControlEndosoCot(properties);
								
								if(controlEndosoCotList.isEmpty())
								{
									properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_TERMINADA);
									controlEndosoCotList = renovacionMasivaService.findControlEndosoCot(properties);				
								}
								
								Iterator<ControlEndosoCot> itControlEndosoCot = controlEndosoCotList.iterator();
								
								ControlEndosoCot controlEndosoCot = null;
								Boolean movimientosBorrados = false;
								while(itControlEndosoCot.hasNext())
								{
									controlEndosoCot = itControlEndosoCot.next();
									if(!movimientosBorrados){
										renovacionMasivaService.borrarMovimientos(controlEndosoCot);
										movimientosBorrados = true;
									}
									renovacionMasivaService.removeControlEndosoCot(controlEndosoCot);
								}		
								
								//Remover Solicitudes de Autorizacion
								renovacionMasivaService.cancelarSolicitudesAutorizacion(cotizacion);
								cotizacionesCanceladas.add(pendiente.getSolicitudId());
							}catch(Exception e){
							}
						}
					}
				}
			}catch(Exception e){
				LOG.error(e);
			}
			
		}
		
		return cotizacionesCanceladas;
	}
	
	private void agregarCondicionesEspecialesCotizacion(BitemporalCotizacion bitemporalCotizacion, CotizacionDTO cotizacion){
		List<BitemporalCondicionEspCot> condicionesEspBit = condicionEspBitService.obtenerCondicionesNivelCotizacion(bitemporalCotizacion.getContinuity().getId());
		if(condicionesEspBit != null){
			for(BitemporalCondicionEspCot bitCondEsp: condicionesEspBit){
				cotizacion.getCondicionesEspeciales().add(condicionEspecialService.obtenerCondicion(bitCondEsp.getContinuity().getCondicionEspecialId()));
			}
		}
	}

	public void setDatoSeccionContinuityList(
			List<EntidadBitemporal> datoSeccionContinuityList) {
		this.datoSeccionContinuityList = datoSeccionContinuityList;
	}

	public List<EntidadBitemporal> getDatoSeccionContinuityList() {
		return datoSeccionContinuityList;
	}

	public void setIdToPolizaOriginal(BigDecimal idToPolizaOriginal) {
		this.idToPolizaOriginal = idToPolizaOriginal;
	}

	public BigDecimal getIdToPolizaOriginal() {
		return idToPolizaOriginal;
	}

	public void setDescuentoNoSiniestro(Double descuentoNoSiniestro) {
		this.descuentoNoSiniestro = descuentoNoSiniestro;
	}

	public Double getDescuentoNoSiniestro() {
		return descuentoNoSiniestro;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public boolean isUnExito() {
		return unExito;
	}

	public void setUnExito(boolean unExito) {
		this.unExito = unExito;
	}

}