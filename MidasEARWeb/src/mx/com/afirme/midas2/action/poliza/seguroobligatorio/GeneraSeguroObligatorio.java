package mx.com.afirme.midas2.action.poliza.seguroobligatorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO.ArchivosPendientes;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexa;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GeneraSeguroObligatorio {
	private RenovacionMasivaService renovacionMasivaService;
	private SeguroObligatorioService seguroObligatorioService;
	
	private String mensaje = "";
	private List<EntidadBitemporal> datoSeccionContinuityList = new ArrayList<EntidadBitemporal>(1);
	private BigDecimal idToPolizaOriginal;
	private String mensajeValidacionNuevoNegocio = null;
	private Map<BigDecimal, BigDecimal> mapNumeroSecuencia = new LinkedHashMap<BigDecimal, BigDecimal>();
	
	private ListadoService listadoService;
	private CalculoService calculoService;
	
	public void setRenovacionMasivaService(RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}
	

	public RenovacionMasivaService getRenovacionMasivaService() {
		return renovacionMasivaService;
	}
	
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	public SeguroObligatorioService getSeguroObligatorioService() {
		return seguroObligatorioService;
	}

	public String getMensajeValidacionNuevoNegocio() {
		return mensajeValidacionNuevoNegocio;
	}

	public void setMensajeValidacionNuevoNegocio(
			String mensajeValidacionNuevoNegocio) {
		this.mensajeValidacionNuevoNegocio = mensajeValidacionNuevoNegocio;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public CalculoService getCalculoService() {
		return calculoService;
	}

	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	private CotizacionDTO creaCotizacionDTOPoliza(PolizaDTO poliza, boolean renovarEmitir, BigDecimal numeroInciso){
		CotizacionDTO cotizacionARenovar = null;
		
		BitemporalCotizacion cotizacion = null;
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(poliza.getCotizacionDTO().getFechaFinVigencia());
		cal.add(GregorianCalendar.SECOND, -1);		
		
		try{
		cotizacion = seguroObligatorioService.obtieneBitemporalCotizacionByIdCotizacion(
				poliza.getCotizacionDTO().getIdToCotizacion(), new DateTime(cal.getTime()));
		}catch(Exception e){
			//
		}
		
		if(cotizacion != null){
			//Obtiene cotizacion de endosos
			cotizacionARenovar = obtieneCotizacionDeBitTemporal(cotizacion, renovarEmitir, numeroInciso, poliza);
		}else{
			//Obtiene cotizacion normal
			//cotizacionARenovar = renovacionMasivaService.obtenerCotizacionPorId(poliza.getCotizacionDTO().getIdToCotizacion());
		}
		
		if(cotizacionARenovar != null){
			ajusteValoresCotizacion(cotizacionARenovar, poliza);
			agregaDocumentos(cotizacion, cotizacionARenovar);
			cotizacionARenovar.setIdToCotizacion(null);		
			
		//Crea solicitud
		SolicitudDTO nuevaSolicitud = new SolicitudDTO();
		try{
			SolicitudDTO ultSolicitud = renovacionMasivaService.obtieneSolicitudRenovacion(poliza.getCotizacionDTO().getIdToCotizacion());
			if(ultSolicitud != null){
				BeanUtils.copyProperties(ultSolicitud, nuevaSolicitud);
			}else{
				BeanUtils.copyProperties(cotizacionARenovar.getSolicitudDTO(), nuevaSolicitud);
			}
		}catch(Exception e){
			BeanUtils.copyProperties(cotizacionARenovar.getSolicitudDTO(), nuevaSolicitud);
		}
		if(nuevaSolicitud.getNegocio() == null){
			nuevaSolicitud.setNegocio(cotizacionARenovar.getSolicitudDTO().getNegocio());
		}

		nuevaSolicitud.setIdToSolicitud(null);
		//TODO Validar si es renovacion con poliza de seguro obligatorio, tambien la nueva poliza se toma renovacion
		nuevaSolicitud.setIdToPolizaAnterior(BigDecimal.ZERO);
		nuevaSolicitud.setEsRenovacion((short) 0);

		List<IncisoCotizacionDTO> incisos = cotizacionARenovar.getIncisoCotizacionDTOs();
		List<IncisoCotizacionDTO> incisosARenovar = new ArrayList<IncisoCotizacionDTO>(1);
		for(IncisoCotizacionDTO incisoCotizacion : incisos){
			IncisoCotizacionDTO nuevoIncisoCotizacion = new IncisoCotizacionDTO();
			BeanUtils.copyProperties(incisoCotizacion, nuevoIncisoCotizacion);
			incisosARenovar.add(nuevoIncisoCotizacion);
		}
		
		cotizacionARenovar.setSolicitudDTO(nuevaSolicitud);
		cotizacionARenovar.setIncisoCotizacionDTOs(incisosARenovar);
		//ajusteFechasVigencia(cotizacionARenovar);
		
		}else{
			setMensaje("Error al Crear Cotizacion de Bitemporal");
		}
		//
		
		return cotizacionARenovar;
	}
	
	@SuppressWarnings("rawtypes")
	private void agregaDocumentos(BitemporalCotizacion cotizacion, CotizacionDTO cotizacionARenovar){

		//List<ArchivosPendientes> archivosPendientes = null;
		//List<ComisionCotizacionDTO> comisionCotizacionDTOs = null;		
		//List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs = null;
		//List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO = null;
		//List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO = null;
		//List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO = null;
		//List<Comentario> comentarioList = null;
		try{
		List<TexAdicionalCotDTO> textAdicionalCotDTOs = new ArrayList<TexAdicionalCotDTO>(1);
		List<EntidadBitemporal> texAdicionalContinuityList =  renovacionMasivaService.obtenerTexAdicionalCotCotinuityList(cotizacion.getContinuity().getId());
		for(EntidadBitemporal item2 : texAdicionalContinuityList){
			BitemporalTexAdicionalCot bitemporalTexAdicionalCot = (BitemporalTexAdicionalCot) item2;
			
			TexAdicionalCotDTO texAdicionalCot = new TexAdicionalCotDTO(bitemporalTexAdicionalCot.getValue());
			if(texAdicionalCot.getDescripcionTexto() != null && IsRichText(texAdicionalCot.getDescripcionTexto())){
				RTFEditorKit rtfParser = new RTFEditorKit();
				Document document = rtfParser.createDefaultDocument();
				rtfParser.read(IOUtils.toInputStream(texAdicionalCot.getDescripcionTexto()), document, 0);
				String text = document.getText(0, document.getLength());
				texAdicionalCot.setDescripcionTexto(text);
			}
			textAdicionalCotDTOs.add(texAdicionalCot);
		}
		if(textAdicionalCotDTOs != null && textAdicionalCotDTOs.size() > 0){
			cotizacionARenovar.setTexAdicionalCotDTOs(textAdicionalCotDTOs);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private boolean IsRichText(String testString)
    {
        if ((testString != null) &&
            (testString.trim().startsWith("{\\rtf")))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	
	@SuppressWarnings("rawtypes")
	private CotizacionDTO obtieneCotizacionDeBitTemporal(BitemporalCotizacion cotizacion, boolean renovarEmitir, BigDecimal numeroInciso, PolizaDTO poliza){
		CotizacionDTO cotizacionDTO = new CotizacionDTO(cotizacion.getValue());
		datoSeccionContinuityList = new ArrayList<EntidadBitemporal>(1);
		if(cotizacionDTO.getIdDomicilioContratante() == null){
			try{
				ClienteGenericoDTO cliente = renovacionMasivaService.obtieneClienteContratantePorId(cotizacionDTO.getIdToPersonaContratante());
				cotizacionDTO.setIdDomicilioContratante(new BigDecimal(cliente.getIdDomicilioConsulta()));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(cotizacion.getValue().getFechaFinVigencia());
		cal.add(GregorianCalendar.SECOND, -1);
		DateTime validOn = new DateTime(cal.getTime());
		
		List<IncisoCotizacionDTO> incisos = new ArrayList<IncisoCotizacionDTO>(1);
		List<EntidadBitemporal> incisoCotinuityList = seguroObligatorioService.obtenerIncisoCotinuityList(cotizacion.getContinuity().getId(), validOn);
		BigDecimal numeroIncisoSec = BigDecimal.ZERO;
		mapNumeroSecuencia.clear();
		
		//Ordena los incisos
		try{
		Collections.sort(incisoCotinuityList, new Comparator<EntidadBitemporal>(){
			public int compare(EntidadBitemporal o1, EntidadBitemporal o2) {
				if(o1 == null || o2 == null){
					return 1;
				}
				return ((BitemporalInciso)o1).getContinuity().getNumero().compareTo(
						((BitemporalInciso)o2).getContinuity().getNumero());
			}
		});
		}catch(Exception e){
			
		}
		
		for(EntidadBitemporal item : incisoCotinuityList){			
			BitemporalInciso bitemporalInciso = (BitemporalInciso) item;
			if(bitemporalInciso != null && bitemporalInciso.getValue() != null){
			
				//Valida si vehiculo entra en el proceso
			MensajeDTO mensaje = seguroObligatorioService.validaVehiculoSO(poliza.getIdToPoliza(), new BigDecimal(bitemporalInciso.getContinuity().getNumero()));
			if(mensaje == null || mensaje.getMensaje() == null || mensaje.getMensaje().isEmpty()){
				
			if(numeroInciso == null || bitemporalInciso.getContinuity().getNumero().equals(numeroInciso.intValue())){
						
				IncisoCotizacionDTO inciso = new IncisoCotizacionDTO(bitemporalInciso.getValue());
				//Conserva mismo numero Inciso
				numeroIncisoSec = new BigDecimal(bitemporalInciso.getContinuity().getNumero());
				mapNumeroSecuencia.put(new BigDecimal(bitemporalInciso.getContinuity().getNumero()), numeroIncisoSec);
				inciso.getId().setNumeroInciso(numeroIncisoSec);
				inciso.setNumeroSecuencia(numeroIncisoSec.longValue());					
				
				
			List<EntidadBitemporal> autoIncisoCotinuityList = seguroObligatorioService.obtenerAutoIncisoCotinuityList(bitemporalInciso.getContinuity().getId(), validOn);
			if(autoIncisoCotinuityList != null && autoIncisoCotinuityList.size() > 0){
				BitemporalAutoInciso autoIncisoContinuity = (BitemporalAutoInciso) autoIncisoCotinuityList.get(0);
				IncisoAutoCot incisoAutoCot = new IncisoAutoCot(autoIncisoContinuity.getValue());
				inciso.setIncisoAutoCot(incisoAutoCot);
			}
			
			
			List<SeccionCotizacionDTO> seccionCotizacionList = new ArrayList<SeccionCotizacionDTO>(1);
			List<EntidadBitemporal> seccionIncisoCotinuityList =  seguroObligatorioService.obtenerSeccionIncisoCotinuityList(bitemporalInciso.getContinuity().getId(), validOn);
			for(EntidadBitemporal item2 : seccionIncisoCotinuityList){
				BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso) item2;
				if(bitemporalSeccionInciso != null && bitemporalSeccionInciso.getValue() != null){
				SeccionCotizacionDTO seccionInciso = new SeccionCotizacionDTO(bitemporalSeccionInciso.getValue());
				seccionInciso.setSeccionDTO(bitemporalSeccionInciso.getContinuity().getSeccion());
				
				try{
					List<EntidadBitemporal> datoSeccionContinuityListTemp = seguroObligatorioService.obtenerDatoSeccionContinuityList(bitemporalSeccionInciso.getContinuity().getId(), validOn);
					if(datoSeccionContinuityListTemp != null && !datoSeccionContinuityListTemp.isEmpty()){
						datoSeccionContinuityList.addAll(datoSeccionContinuityListTemp);
					}
				}catch(Exception e){
				}
				
				List<CoberturaCotizacionDTO> coberturaCotizacionLista = new ArrayList<CoberturaCotizacionDTO>(1);
				List<EntidadBitemporal> coberturasCotinuityList = seguroObligatorioService.obtenerCoberturasCotinuityList(bitemporalSeccionInciso.getContinuity().getId(), validOn);
				for(EntidadBitemporal item3 : coberturasCotinuityList){
					BitemporalCoberturaSeccion bitemporalCoberturaSeccion = (BitemporalCoberturaSeccion) item3;
					if(bitemporalCoberturaSeccion != null && bitemporalCoberturaSeccion.getValue() != null){
					CoberturaCotizacionDTO coberturaCotizacion =  new CoberturaCotizacionDTO(bitemporalCoberturaSeccion.getValue());
					CoberturaCotizacionId id = new CoberturaCotizacionId();
					id.setIdToSeccion(seccionInciso.getSeccionDTO().getIdToSeccion());
					id.setIdToCobertura(bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura());
					coberturaCotizacion.setId(id);
					
					if(coberturaCotizacion.getClaveTipoDeducible() == null){
						try{
							CoberturaDTO cobertura = renovacionMasivaService.getCobertura(coberturaCotizacion.getId().getIdToCobertura());
							coberturaCotizacion.setClaveTipoDeducible(Short.parseShort(cobertura.getClaveTipoDeducible()));
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					coberturaCotizacionLista.add(coberturaCotizacion);
					}
				}
				
				seccionInciso.setCoberturaCotizacionLista(coberturaCotizacionLista);
				
				
				seccionCotizacionList.add(seccionInciso);
			}
			}
			inciso.setSeccionCotizacionList(seccionCotizacionList);
			incisos.add(inciso);
			}//If numeroInciso
			}
		}
		}
		cotizacionDTO.setIncisoCotizacionDTOs(incisos);
		
		return cotizacionDTO;
	}
	
	@SuppressWarnings("unchecked")
	private void agregaCoberturasNegocio(IncisoCotizacionDTO incisoCotizacion, List<CoberturaCotizacionDTO> coberturaCotizacionLista){
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = renovacionMasivaService.obtenerNegocioPaqueteSeccionPorId(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId()); 
		List<CoberturaCotizacionDTO> list = renovacionMasivaService.obtenerCoberturasNegocio(negocioPaqueteSeccion, incisoCotizacion);
		
		Short contratada = 1;
		//Elimina cobertura de poliza que no esta en negocio
		//En teoria debe eliminar todas las coberturas ya que debe ser una cobertura nueva que no se encuentre en la poliza
		for(int i = 0; i < coberturaCotizacionLista.size(); i++){
			CoberturaCotizacionDTO coberturaCotizacion = coberturaCotizacionLista.get(i);
			
			final BigDecimal idToCobertura = coberturaCotizacion.getId().getIdToCobertura();
			Predicate predicate = new Predicate() {	
				@Override
				public boolean evaluate(Object arg0) {
					CoberturaCotizacionDTO config = (CoberturaCotizacionDTO)arg0;
					return config.getId().getIdToCobertura().equals(idToCobertura);
				}
			};	
				
			List<CoberturaCotizacionDTO> coberturaList = (List<CoberturaCotizacionDTO>) CollectionUtils.select(list, predicate);
			if(coberturaList == null || coberturaList.isEmpty()){
				coberturaCotizacionLista.remove(i);
				i--;
			}
		}
		
		//Agregar cobertura de poliza nueva en negocio
		for(CoberturaCotizacionDTO coberturaNegocio: list){
			final BigDecimal idToCobertura = coberturaNegocio.getId().getIdToCobertura();
			Predicate predicate = new Predicate() {	
				@Override
				public boolean evaluate(Object arg0) {
					CoberturaCotizacionDTO config = (CoberturaCotizacionDTO)arg0;
					return config.getId().getIdToCobertura().equals(idToCobertura);
				}
			};	
				
			List<CoberturaCotizacionDTO> coberturaList = (List<CoberturaCotizacionDTO>) CollectionUtils.select(coberturaCotizacionLista, predicate);
			if(coberturaList == null || coberturaList.isEmpty()){
				//Si no existe la cobertura la agrega contratada
				coberturaNegocio.setClaveContrato(contratada);
				coberturaCotizacionLista.add(coberturaNegocio);
			}else{				
				//Si existe ajusta los valores de clave obligatoriedad, la contrata y ajusta suma asegurada en caso de que sea nulla
				CoberturaCotizacionDTO coberturaCotizacion = coberturaList.get(0);
				coberturaCotizacion.setClaveObligatoriedad(coberturaNegocio.getClaveObligatoriedad());
				coberturaCotizacion.setClaveContrato(contratada);				
				if(coberturaNegocio.getClaveObligatoriedad().intValue() == 0 && coberturaNegocio.getClaveContrato().intValue() == 1){
					//coberturaCotizacion.setClaveContrato(coberturaNegocio.getClaveContrato());
					if(coberturaCotizacion.getValorSumaAsegurada() == null || coberturaCotizacion.getValorSumaAsegurada().doubleValue() == 0){
						coberturaCotizacion.setValorSumaAsegurada(coberturaNegocio.getValorSumaAsegurada());
					}
				}
			}
		}

	}
	
	private void ajusteValoresCotizacion(CotizacionDTO cotizacionARenovar, PolizaDTO poliza){

		//Forma de pago es ANUAL siempre
		cotizacionARenovar.setIdFormaPago(BigDecimal.ONE);
		cotizacionARenovar.setPorcentajePagoFraccionado(new Double(0));
		
		//Conducto de Cobro Agente siempre
		cotizacionARenovar.setIdMedioPago(new BigDecimal(MedioPagoDTO.MEDIO_PAGO_AGENTE));
		
		if(cotizacionARenovar.getPorcentajeIva() == null){
			if(poliza.getCotizacionDTO().getPorcentajeIva() != null){
				cotizacionARenovar.setPorcentajeIva(poliza.getCotizacionDTO().getPorcentajeIva());
			}else{
				cotizacionARenovar.setPorcentajeIva(new Double(16));
			}			
		}
		/*
		if(cotizacionARenovar.getPorcentajePagoFraccionado() == null){
			if(poliza.getCotizacionDTO().getPorcentajePagoFraccionado() != null){
				cotizacionARenovar.setPorcentajePagoFraccionado(poliza.getCotizacionDTO().getPorcentajePagoFraccionado());
			}else{
				cotizacionARenovar.setPorcentajePagoFraccionado(new Double(0));
			}	
		}*/
		cotizacionARenovar.setPorcentajebonifcomision(new Double(0));
		
		if(cotizacionARenovar.getNegocioTipoPoliza() == null){
			NegocioProducto negocioProducto = renovacionMasivaService.getNegocioProductoByCotizacion(cotizacionARenovar);
			NegocioTipoPoliza negocioTipoPoliza = renovacionMasivaService.getPorIdNegocioProductoIdToTipoPoliza(negocioProducto.getIdToNegProducto(), cotizacionARenovar.getTipoPolizaDTO().getIdToTipoPoliza());
			cotizacionARenovar.setNegocioTipoPoliza(negocioTipoPoliza);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private CotizacionDTO guardaCotizacionARenovar(CotizacionDTO cotizacionDTO, Short tipo, Date fechaCreacion) throws Exception{
		List<IncisoCotizacionDTO> incisos = cotizacionDTO.getIncisoCotizacionDTOs();
		cotizacionDTO.setIncisoCotizacionDTOs(null);
		
		List<ArchivosPendientes> archivosPendientes = new ArrayList<ArchivosPendientes>(1);
		if(cotizacionDTO.getArchivosPendientes() != null){
			archivosPendientes.addAll(cotizacionDTO.getArchivosPendientes());
			cotizacionDTO.getArchivosPendientes().clear();
		}
		List<ComisionCotizacionDTO> comisionCotizacionDTOs = new ArrayList<ComisionCotizacionDTO>(1);
		if(cotizacionDTO.getComisionCotizacionDTOs() != null){
			comisionCotizacionDTOs.addAll(cotizacionDTO.getComisionCotizacionDTOs());
			cotizacionDTO.getComisionCotizacionDTOs().clear();
		}
		List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs = new ArrayList<DocAnexoCotDTO>(1);
		if(cotizacionDTO.getDocumentoAnexoCotizacionDTOs() != null){
			documentoAnexoCotizacionDTOs.addAll(cotizacionDTO.getDocumentoAnexoCotizacionDTOs()); 
			cotizacionDTO.getDocumentoAnexoCotizacionDTOs().clear();
		}
		List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO = new ArrayList<DocumentoAnexoReaseguroCotizacionDTO>(1);
		if(cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs() != null){
			documentoAnexoReaseguroCotizacionDTO.addAll(cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs());
			cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs().clear();
		}
		List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO = new ArrayList<DocumentoDigitalCotizacionDTO>(1);
		if(cotizacionDTO.getDocumentoDigitalCotizacionDTOs() != null){
			documentoDigitalCotizacionDTO.addAll(cotizacionDTO.getDocumentoDigitalCotizacionDTOs()); 
			cotizacionDTO.getDocumentoDigitalCotizacionDTOs().clear();
		}
		List<TexAdicionalCotDTO> textAdicionalCotDTOs = new ArrayList<TexAdicionalCotDTO>(1);
		if(cotizacionDTO.getTexAdicionalCotDTOs() != null){
			textAdicionalCotDTOs.addAll(cotizacionDTO.getTexAdicionalCotDTOs());
			cotizacionDTO.getTexAdicionalCotDTOs().clear();
		}
		
		/*if(cotizacionDTO.getNegocioDerechoPoliza() != null){
			negocioDerechoPoliza.setIdToNegDerechoPoliza(cotizacionDTO.getNegocioDerechoPoliza().getIdToNegDerechoPoliza());
			cotizacionDTO.setNegocioDerechoPoliza(null);
		}*/
		
		
		List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO = new ArrayList<DocumentoDigitalSolicitudDTO>(1);
		if(cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs() != null){
			documentoDigitalSolicitudDTO.addAll(cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs());
			cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs().clear();
		}
		List<Comentario> comentarioList = new ArrayList<Comentario>(1);
		if(cotizacionDTO.getSolicitudDTO().getComentarioList() != null){
			comentarioList.addAll(cotizacionDTO.getSolicitudDTO().getComentarioList());
			cotizacionDTO.getSolicitudDTO().getComentarioList().clear();
		}
		
		//Crea Cotizacion con solicitud
		cotizacionDTO = renovacionMasivaService.crearCotizacion(cotizacionDTO);
		//
		//Agrega datos adicionales de cotizacion
		guardaDocumentos(archivosPendientes, comisionCotizacionDTOs, documentoAnexoCotizacionDTOs, documentoAnexoReaseguroCotizacionDTO,
				documentoDigitalCotizacionDTO, textAdicionalCotDTOs, documentoDigitalSolicitudDTO, comentarioList, 
				cotizacionDTO);
		
		//Obtiene si es AutoPlazo
		//this.esAutoPlazo(incisos); 
		
		NegocioDerechoPoliza negocioDerechoPoliza = new NegocioDerechoPoliza();
		//Derechos Poliza = 0
		//negocioDerechoPoliza = seguroObligatorioService.obtenerDerechosPolizaCero(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
		negocioDerechoPoliza = obtieneNegocioDerechos(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), tipo, fechaCreacion);
		cotizacionDTO.setNegocioDerechoPoliza(negocioDerechoPoliza);
		cotizacionDTO.setDerechosPoliza(negocioDerechoPoliza.getImporteDerecho());

		
		BigDecimal pctRPF = getPorcentajeRecargoFraccionado(cotizacionDTO);
		if(pctRPF != null){
			cotizacionDTO.setPorcentajePagoFraccionado(pctRPF.doubleValue());
			cotizacionDTO.setPorcentajeRecargoPagoFraccionadoUsuario(pctRPF.doubleValue());
		}
		
		cotizacionDTO = renovacionMasivaService.guardarCotizacion(cotizacionDTO);		
			
		for(IncisoCotizacionDTO nuevoIncisoCotizacion: incisos){
		try {				
			nuevoIncisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
			nuevoIncisoCotizacion.getIncisoAutoCot().setId(null);
			nuevoIncisoCotizacion.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
			nuevoIncisoCotizacion.setCotizacionDTO(cotizacionDTO);
			List<CoberturaCotizacionDTO> coberturaCotizacionList = creaListadoCoberturas(nuevoIncisoCotizacion.getSeccionCotizacionList().get(0).getCoberturaCotizacionLista(), cotizacionDTO.getIdToCotizacion());
			try{
				agregaCoberturasNegocio(nuevoIncisoCotizacion, coberturaCotizacionList);
			}catch(Exception e){
				e.printStackTrace();
			}

			
			//Otorgar el maximo descuento por negocio-estado permitido
			Double pctDescuentoEstado = 0.0;
			/*if(listadoService.getAplicaDescuentoNegocioPaqueteSeccion(nuevoIncisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId())) {
				NegocioEstadoDescuento negocioEstadoDescuento = new NegocioEstadoDescuento();
				negocioEstadoDescuento = negocioEstadoDescuentoService.findByNegocioAndEstado(
						cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), 
						nuevoIncisoCotizacion.getIncisoAutoCot().getEstadoId());
				if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuento() != null) {
					pctDescuentoEstado = negocioEstadoDescuento.getPctDescuento();
				}
			}*/
			nuevoIncisoCotizacion.getIncisoAutoCot().setPctDescuentoEstado(pctDescuentoEstado);

			
			nuevoIncisoCotizacion.getSeccionCotizacionList().get(0).setCoberturaCotizacionLista(coberturaCotizacionList);
			nuevoIncisoCotizacion = renovacionMasivaService.prepareGuardarInciso(
					cotizacionDTO.getIdToCotizacion(),
					nuevoIncisoCotizacion.getIncisoAutoCot(), nuevoIncisoCotizacion,
					coberturaCotizacionList, nuevoIncisoCotizacion.getSeccionCotizacion().getValorPrimaNeta(),
					nuevoIncisoCotizacion.getSeccionCotizacion().getValorSumaAsegurada(), 
					nuevoIncisoCotizacion.getSeccionCotizacion().getClaveContrato(),
					nuevoIncisoCotizacion.getSeccionCotizacion().getClaveObligatoriedad());
			
			//DEBE HACERSE ANTES DEL CALCULO
			if(this.getDatoSeccionContinuityList() != null){
				for(EntidadBitemporal item : getDatoSeccionContinuityList()){
					try{
						BitemporalDatoSeccion bitemporalDatoSeccion = (BitemporalDatoSeccion) item;
						if(bitemporalDatoSeccion != null && bitemporalDatoSeccion.getValue() != null ){
							BigDecimal numeroInciso = mapNumeroSecuencia.get(new BigDecimal(bitemporalDatoSeccion.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity().getNumero()));
							if(numeroInciso.equals(nuevoIncisoCotizacion.getId().getNumeroInciso())){
								DatoIncisoCotAuto datoInciso =  new DatoIncisoCotAuto();
								if(bitemporalDatoSeccion.getValue() != null && bitemporalDatoSeccion.getValue().getValor() != null){
									datoInciso.setValor(bitemporalDatoSeccion.getValue().getValor());
								}else{
									setMensaje("Datos de Riesgo default 0");
									datoInciso.setValor("0");
								}
								datoInciso.setClaveDetalle(new BigDecimal(bitemporalDatoSeccion.getContinuity().getClaveDetalle()));
								datoInciso.setIdDato(new BigDecimal(bitemporalDatoSeccion.getContinuity().getDatoId()));
								datoInciso.setIdTcRamo(new BigDecimal(bitemporalDatoSeccion.getContinuity().getRamoId()));
								datoInciso.setIdTcSubRamo(new BigDecimal(bitemporalDatoSeccion.getContinuity().getSubRamoId()));
								datoInciso.setIdToCobertura(new BigDecimal(bitemporalDatoSeccion.getContinuity().getCoberturaId()));
								datoInciso.setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
								
								datoInciso.setIdToSeccion(bitemporalDatoSeccion.getContinuity().getSeccionIncisoContinuity().getSeccion().getIdToSeccion());
								datoInciso.setNumeroInciso(numeroInciso);
							
								renovacionMasivaService.saveDatoIncisoCotAuto(datoInciso);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}			
			
			renovacionMasivaService.calcular(nuevoIncisoCotizacion);
			//Actualiza prima calculada solo si es a peticion
			if(validaActualizaPrima(tipo, fechaCreacion)){
				actualizaPrima(nuevoIncisoCotizacion);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			if(cotizacionDTO != null && cotizacionDTO.getIdToCotizacion() != null ){
				renovacionMasivaService.eliminaCotizacionFallida(cotizacionDTO);
			}
			setMensaje("Error al creal el Inciso: " + e.getMessage());
			throw new Exception("Error al creal el Inciso: " + e.getMessage());
		}
		}
		
		//Actualiza prima de la cotizacion
		renovacionMasivaService.obtenerResumenCotizacion(cotizacionDTO);
		
		return cotizacionDTO;
	}
	
	private Boolean validaActualizaPrima(Short tipo, Date fechaCreacion){
		Boolean valido = false;
		
		if(tipo.equals(PolizaAnexa.TIPO_PETICION)){
			//Obtiene fecha de valides
			Date fechaValidez = seguroObligatorioService.obtieneFechaValidez();
			if(fechaCreacion.compareTo(fechaValidez) <= 0){
				valido = true;
			}
		}
		
		return valido;
	}
	
	private NegocioDerechoPoliza obtieneNegocioDerechos(Long idToNegocio, Short tipo, Date fechaCreacion){
		NegocioDerechoPoliza negocioDerechoPoliza = null;
		if(validaActualizaPrima(tipo, fechaCreacion)){
			negocioDerechoPoliza = seguroObligatorioService.obtieneNegocioDerechoPolizaSOByNegocio(idToNegocio);
		}else{
			negocioDerechoPoliza = renovacionMasivaService.obtenerDechosPolizaDefault(idToNegocio);
		}		
		return negocioDerechoPoliza;
	}
	
	private void actualizaPrima(IncisoCotizacionDTO inciso){
		List<CoberturaCotizacionDTO> coberturas = inciso.getSeccionCotizacion().getCoberturaCotizacionLista();
		double valorSumaAsegurada = seguroObligatorioService.getSumaAseguradaSeguroObligatorio(inciso.getIncisoAutoCot().getNegocioPaqueteId());
		double primaTotal = seguroObligatorioService.getPrimaTotalSeguroObligatorio();
		double primaNeta = primaTotal/1.16;
		
		double dias = UtileriasWeb.obtenerDiasEntreFechas(inciso.getCotizacionDTO().getFechaInicioVigencia(), inciso.getCotizacionDTO().getFechaFinVigencia()); 
		
		double primaDiaria = primaNeta / dias;
		for(CoberturaCotizacionDTO cobertura: coberturas){
			cobertura.setValorPrimaNeta(primaNeta);
			cobertura.setValorPrimaDiaria(primaDiaria);	
			cobertura.setValorSumaAsegurada(valorSumaAsegurada);
			seguroObligatorioService.saveCoberturaCotizacion(cobertura);
		}
	}
	
	/*
	private void esAutoPlazo(List<IncisoCotizacionDTO> list){
		esAutoPlazoCotizacion = false;
		
		try{
		if(list != null && !list.isEmpty()){
			for(IncisoCotizacionDTO item: list){
				List<SeccionCotizacionDTO> seccionCotizacionList = item.getSeccionCotizacionList();
				if(seccionCotizacionList != null && !seccionCotizacionList.isEmpty()){
					SeccionCotizacionDTO seccionCotizacion = seccionCotizacionList.get(0);
					if(seccionCotizacion.getSeccionDTO().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_AUTOS) ||
						seccionCotizacion.getSeccionDTO().getIdToSeccion().equals(SeccionDTO.LINEA_NEGOCIO_AUTOPLAZO_CAMIONES)){
						esAutoPlazoCotizacion = true;
						break;
					}
				}
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}	*/
	
	/**
	 * Obtner porcentaje de recargo fraccionado que corresponde a la cotizacion
	 * @param cotizacion
	 * @return
	 */
	private BigDecimal getPorcentajeRecargoFraccionado(CotizacionDTO cotizacion){
		BigDecimal porcentajeRecargoPagoFraccionado = null;
		if (cotizacion.getSolicitudDTO().getNegocio() == null || cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado() == null || 
				!cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado().booleanValue()) {
			porcentajeRecargoPagoFraccionado = null;
		} else {
			porcentajeRecargoPagoFraccionado = cotizacion.getPorcentajePagoFraccionado() != null ? 
					BigDecimal.valueOf(cotizacion.getPorcentajePagoFraccionado().doubleValue()) : null;
		}
		if (porcentajeRecargoPagoFraccionado == null && cotizacion.getIdFormaPago() != null && cotizacion.getIdMoneda() != null) {
			Double pcte = renovacionMasivaService.getPctePagoFraccionado(cotizacion.getIdFormaPago().intValue(), 
					cotizacion.getIdMoneda().shortValue());			
			porcentajeRecargoPagoFraccionado = pcte != null ? BigDecimal.valueOf(pcte) : BigDecimal.valueOf(0);
		}
		return porcentajeRecargoPagoFraccionado;
	}
	
	private void guardaDocumentos(List<ArchivosPendientes> archivosPendientes, List<ComisionCotizacionDTO> comisionCotizacionDTOs,
			List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs, List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO,
			List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO, List<TexAdicionalCotDTO> textAdicionalCotDTOs,
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO, List<Comentario> comentarioList, 
			CotizacionDTO cotizacionDTO){
		

		if(comisionCotizacionDTOs != null && comisionCotizacionDTOs.size() > 0){
			for(ComisionCotizacionDTO item: comisionCotizacionDTOs){
				try{
					item.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveComisionCotizacionDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoAnexoCotizacionDTOs != null && documentoAnexoCotizacionDTOs.size() > 0){
			for(DocAnexoCotDTO item: documentoAnexoCotizacionDTOs){
				try{
					item.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveDocAnexoCotDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoAnexoReaseguroCotizacionDTO != null && documentoAnexoReaseguroCotizacionDTO.size() > 0){
			for(DocumentoAnexoReaseguroCotizacionDTO item: documentoAnexoReaseguroCotizacionDTO){
				try{
					item.getId().setIdToCotizacion(cotizacionDTO.getIdToCotizacion());
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveDocumentoAnexoReaseguroCotizacionDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoDigitalCotizacionDTO != null && documentoDigitalCotizacionDTO.size() > 0){
			for(DocumentoDigitalCotizacionDTO item: documentoDigitalCotizacionDTO){
				try{
					item.setCotizacionDTO(cotizacionDTO);
					renovacionMasivaService.saveDocumentoDigitalCotizacionDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(textAdicionalCotDTOs != null && textAdicionalCotDTOs.size() > 0){
			for(TexAdicionalCotDTO item: textAdicionalCotDTOs){
				try{
					item.setCotizacion(cotizacionDTO);
					renovacionMasivaService.saveTexAdicionalCotDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(documentoDigitalSolicitudDTO != null && documentoDigitalSolicitudDTO.size() > 0){
			for(DocumentoDigitalSolicitudDTO item: documentoDigitalSolicitudDTO){
				try{
					item.setSolicitudDTO(cotizacionDTO.getSolicitudDTO());
					renovacionMasivaService.saveDocumentoDigitalSolicitudDTO(item);
				}catch(Exception e){
				}
			}
		}
		if(comentarioList != null && comentarioList.size() > 0){
			for(Comentario item: comentarioList){
				try{
					item.setSolicitudDTO(cotizacionDTO.getSolicitudDTO());
					renovacionMasivaService.saveComentario(item);
				}catch(Exception e){
				}
			}
		}
	}
	
	private List<CoberturaCotizacionDTO> creaListadoCoberturas(List<CoberturaCotizacionDTO> coberturaCotizacionList, BigDecimal idToCotizacion){
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>(1);
		for(CoberturaCotizacionDTO coberturaCotizacionDTO : coberturaCotizacionList){
			CoberturaCotizacionDTO nuevaCoberturaCotizacionDTO = new CoberturaCotizacionDTO();
			if(coberturaCotizacionDTO.getClaveContratoBoolean() == null){
				if(coberturaCotizacionDTO.getClaveContrato() == 1){
					coberturaCotizacionDTO.setClaveContratoBoolean(true);
				}else{
					coberturaCotizacionDTO.setClaveContratoBoolean(false);
				}
			}
			//Limpia lazy list
			if(coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion() != null){
				coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion().clear();
			}
			if(coberturaCotizacionDTO.getRiesgoCotizacionLista() != null){
				coberturaCotizacionDTO.getRiesgoCotizacionLista().clear();
			}
			BeanUtils.copyProperties(coberturaCotizacionDTO, nuevaCoberturaCotizacionDTO);
			nuevaCoberturaCotizacionDTO.getId().setIdToCotizacion(idToCotizacion);
			coberturas.add(nuevaCoberturaCotizacionDTO);
		}
		return coberturas;
	}
	
	private CotizacionDTO actualizaNuevoNegocio(CotizacionDTO cotizacion, NegocioSeccion negocioSeccion, Negocio negocioSegObl, Short tipo, Date fechaCreacion){
		
		SolicitudDTO solicitud = cotizacion.getSolicitudDTO();
		//Negocio
		solicitud.setNegocio(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio());
		//Producto
		solicitud.setProductoDTO(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getProductoDTO());
		
		cotizacion.setSolicitudDTO(solicitud);
		
		//TipoPoliza
		cotizacion.setNegocioTipoPoliza(negocioSeccion.getNegocioTipoPoliza());
				
		//Derechos
		/*NegocioDerechoPoliza negocioDerechoPoliza = seguroObligatorioService.obtieneNegocioDerechoPolizaSOByNegocio(
				negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio());*/
		NegocioDerechoPoliza negocioDerechoPoliza = obtieneNegocioDerechos(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), tipo, fechaCreacion);
		
		
		cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
		cotizacion.setDerechosPoliza(negocioDerechoPoliza.getImporteDerecho());
		
		//Igualacion
		//Se coloca igualacion igual true para que no agregue descuentos por volumen en flotillas
		cotizacion.setIgualacionNivelCotizacion(true);
		
		for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){
			//Obtiene Negocio Seccion Nueva Para cada Inciso
			NegocioSeccion negocioSeccionInciso = seguroObligatorioService.getNegSeccionSOInciso(negocioSegObl, cotizacion, inciso);
			
			//Seccion	
			 inciso.getIncisoAutoCot().setNegocioSeccionId(negocioSeccionInciso.getIdToNegSeccion().longValue());
			
			//Paquete
			NegocioPaqueteSeccion negocioPaqueteSeccion = seguroObligatorioService.obtieneNegocioPaqueteSeccionPorNegSeccion(
					negocioSeccionInciso.getIdToNegSeccion());
			
			inciso.getIncisoAutoCot().setNegocioPaqueteId(negocioPaqueteSeccion.getIdToNegPaqueteSeccion());
			inciso.getIncisoAutoCot().setPaquete(negocioPaqueteSeccion.getPaquete());
			
			//Estilo
			EstiloVehiculoDTO estilo = new EstiloVehiculoDTO();
			String estiloId = inciso.getIncisoAutoCot().getEstiloId();
			EstiloVehiculoId id = new EstiloVehiculoId();
			id.setStrId(estiloId);
			estilo.setId(id);
			estilo = renovacionMasivaService.getEstiloVehiculo(inciso.getIncisoAutoCot().getMarcaId(), negocioSeccionInciso.getIdToNegSeccion(), 
					cotizacion.getIdMoneda(), id.getClaveEstilo());
			inciso.getIncisoAutoCot().setEstiloId(estilo.getId().getStrId());
			inciso.getIncisoAutoCot().setIdVersionCarga(estilo.getId().getIdVersionCarga());
			
			
			//SeccionCotizacion
			SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacionList().get(0);
			seccionCotizacion.setSeccionDTO(negocioSeccionInciso.getSeccionDTO());
			
			//Coberturas
			List<CoberturaCotizacionDTO> coberturas = inciso.getSeccionCotizacionList().get(0).getCoberturaCotizacionLista();
			for(CoberturaCotizacionDTO cobertura : coberturas){
				cobertura.getId().setIdToSeccion(negocioSeccionInciso.getSeccionDTO().getIdToSeccion());
			}
				
		}
		
		return cotizacion;
	}
	
	private PolizaDTO emiteCotizacionARenovar(CotizacionDTO cotizacionDTO){
		PolizaDTO poliza = null;
		TerminarCotizacionDTO terminarCotizacionDTO = renovacionMasivaService.terminarCotizacion(cotizacionDTO.getIdToCotizacion());
		
		//Se ignoran excepcion en renovacion directa
		if(terminarCotizacionDTO != null){
			//Calcular Descuento Global
			cotizacionDTO.setPorcentajeDescuentoGlobal(calculoService.calculaPorcentajeDescuentoGlobal(cotizacionDTO.getIdToCotizacion()));
			cotizacionDTO.setClaveEstatus(CotizacionDTO.ESTATUS_COT_TERMINADA);
			renovacionMasivaService.guardarCotizacionSimple(cotizacionDTO);
		}
		
			for(IncisoCotizacionDTO incisoCotizacion: cotizacionDTO.getIncisoCotizacionDTOs()){
				renovacionMasivaService.complementarDatosInciso(incisoCotizacion);
			}
			Map<String, String> mensajeEmision = renovacionMasivaService.emitir(cotizacionDTO);
			String icono = mensajeEmision.get("icono");
			if(icono.equals("30")){
				String idPoliza = mensajeEmision.get("idpoliza");
				poliza = renovacionMasivaService.getPolizaDTOById(BigDecimal.valueOf(Long.valueOf(idPoliza)));
				setMensaje(mensajeEmision.get("mensaje"));
			}else{
				setMensaje("Error al Emitir: " +mensajeEmision.get("mensaje"));
			}
		
		
		return poliza;
	}	
	/*
	@SuppressWarnings("rawtypes")
	public BigDecimal creaPolizaSeguroObligatorioIndivualInciso(PolizaDTO poliza){
		
		try{
			//Si es poliza anexa no entra en el proceso
			if(poliza.getClaveAnexa() == null || !poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
			
				//Obtiene negocio Seguro Obligatorio
				Negocio negocioSegObl = seguroObligatorioService.getNegocioSeguroObligatorio();
				
				//Obtiene incisos a crear sus polizas
				try{
					BitemporalCotizacion cotizacion = renovacionMasivaService.obtieneBitemporalCotizacionByIdCotizacion(poliza.getCotizacionDTO().getIdToCotizacion());
					List<EntidadBitemporal> incisoCotinuityList = renovacionMasivaService.obtenerIncisoCotinuityList(cotizacion.getContinuity().getId());					
					//Ordena los incisos
					try{
					Collections.sort(incisoCotinuityList, new Comparator<EntidadBitemporal>(){
						public int compare(EntidadBitemporal o1, EntidadBitemporal o2) {
							if(o1 == null || o2 == null){
								return 1;
							}
							return ((BitemporalInciso)o1).getContinuity().getNumero().compareTo(
									((BitemporalInciso)o2).getContinuity().getNumero());
						}
					});
					}catch(Exception e){
						
					}					
					for(EntidadBitemporal item : incisoCotinuityList){			
						BitemporalInciso bitemporalInciso = (BitemporalInciso) item;
						if(bitemporalInciso != null && bitemporalInciso.getValue() != null){
							IncisoCotizacionDTO inciso =  new IncisoCotizacionDTO(bitemporalInciso.getValue());
							inciso.getId().setNumeroInciso(new BigDecimal(bitemporalInciso.getContinuity().getNumero()));
							//Valida si inciso entra en Seguro Obligatorio
							if(validaSeguroObligatorio(poliza.getIdToPoliza(), inciso.getId().getNumeroInciso())){
												
								CotizacionDTO cotizacionDTO = this.creaCotizacionDTOPoliza(poliza, false, inciso.getId().getNumeroInciso());			
								if(cotizacionDTO != null){
									//Obtiene Negocio Seccion Nueva
									NegocioSeccion negocioSeccion = seguroObligatorioService.getNegSeccionSOInciso(negocioSegObl, poliza.getCotizacionDTO(), cotizacionDTO.getIncisoCotizacionDTOs().get(0));									
									cotizacionDTO = actualizaNuevoNegocio(cotizacionDTO, negocioSeccion);
									cotizacionDTO = guardaCotizacionARenovar(cotizacionDTO);				
									if(cotizacionDTO != null){
										try{
											PolizaDTO polizaAnexa = emiteCotizacionARenovar(cotizacionDTO);
											//Llena tabla de relacion
											seguroObligatorioService.relacionaPolizaSeguroObligatorio(poliza, inciso.getId().getNumeroInciso(), polizaAnexa);
										}catch(Exception e){
											e.printStackTrace();
										}
									}
								}
							}//Validacion						
						}
					}
				}catch(Exception e){
					//
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}		
		return BigDecimal.ONE;
	}	*/
	
	public BigDecimal creaPolizaSeguroObligatorio(PolizaDTO poliza, Short tipo){
		
		try{
			//Si es poliza anexa no entra en el proceso
			if(poliza.getClaveAnexa() == null || !poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
			
				//Obtiene negocio Seguro Obligatorio
				Negocio negocioSegObl = seguroObligatorioService.getNegocioSeguroObligatorio(poliza.getIdToPoliza());
				
				//Obtiene incisos a crear sus polizas
				try{							
					//Valida si poliza entra en Seguro Obligatorio
					if(validaSeguroObligatorio(poliza.getIdToPoliza())){
												
						CotizacionDTO cotizacionDTO = this.creaCotizacionDTOPoliza(poliza, false, null);			
						if(cotizacionDTO != null){
							//Obtiene Negocio Seccion Nueva
							NegocioSeccion negocioSeccion = seguroObligatorioService.getNegSeccionSOInciso(negocioSegObl, poliza.getCotizacionDTO(), cotizacionDTO.getIncisoCotizacionDTOs().get(0));									
							cotizacionDTO = actualizaNuevoNegocio(cotizacionDTO, negocioSeccion, negocioSegObl, tipo, poliza.getFechaCreacion());
							cotizacionDTO = guardaCotizacionARenovar(cotizacionDTO, tipo, poliza.getFechaCreacion());				
							if(cotizacionDTO != null){
								try{
									PolizaDTO polizaAnexa = emiteCotizacionARenovar(cotizacionDTO);
									//Llena tabla de relacion
									seguroObligatorioService.relacionaPolizaSeguroObligatorio(poliza, polizaAnexa);
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}
					}//Validacion						
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}		
		return BigDecimal.ONE;
	}
	
	public void creaPolizaAnexaEndosoAltaInciso(PolizaDTO poliza, Short numeroEndoso){
		try{
			//Si es poliza anexa no entra en el proceso
			if(poliza.getClaveAnexa() == null || !poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
				PolizaDTO polizaAnexa = seguroObligatorioService.obtienePolizaAnexa(poliza.getIdToPoliza());
				if(polizaAnexa != null){
					try{							
						seguroObligatorioService.emiteEndosoAltaSO(poliza.getIdToPoliza(), numeroEndoso);	
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}				
	}
	
	public void creaPolizaAnexaEndosoCambioDatos(PolizaDTO poliza, Short numeroEndoso){
		try{
			//Si es poliza anexa no entra en el proceso
			if(poliza.getClaveAnexa() == null || !poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
				//Valida exista poliza Anexa
				PolizaDTO polizaAnexa = seguroObligatorioService.obtienePolizaAnexa(poliza.getIdToPoliza());
				if(polizaAnexa != null){
					try{							
						seguroObligatorioService.emiteEndosoDatosSO(poliza.getIdToPoliza(), numeroEndoso);	
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}				
	}

	private Boolean validaSeguroObligatorio(BigDecimal idToPoliza){
		Boolean esValido = false;		
		try{
			MensajeDTO mensaje = seguroObligatorioService.validaVehiculoSO(idToPoliza, null);
			if(mensaje == null || mensaje.getMensaje() == null || mensaje.getMensaje().isEmpty()){
				esValido = true;
			}else{
				this.setMensaje(mensaje.getMensaje());
			}
		}catch(Exception e){
			e.printStackTrace();
		}		
		return esValido;
	}
	
	public void creaPolizaSeguroObligatorioVigentes(String idToPolizaList){		
		List<BigDecimal> polizaList = seguroObligatorioService.obtienePolizasCrearSO(idToPolizaList);
		for(BigDecimal idPoliza: polizaList){
			PolizaDTO poliza = renovacionMasivaService.getPolizaDTOById(idPoliza);
			creaPolizaSeguroObligatorio(poliza, PolizaAnexa.TIPO_PETICION);		
		}
		//System.out.println(idToPolizaList);
	}	

	public void setDatoSeccionContinuityList(
			List<EntidadBitemporal> datoSeccionContinuityList) {
		this.datoSeccionContinuityList = datoSeccionContinuityList;
	}

	public List<EntidadBitemporal> getDatoSeccionContinuityList() {
		return datoSeccionContinuityList;
	}

	public void setIdToPolizaOriginal(BigDecimal idToPolizaOriginal) {
		this.idToPolizaOriginal = idToPolizaOriginal;
	}

	public BigDecimal getIdToPolizaOriginal() {
		return idToPolizaOriginal;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public String getMensaje() {
		return mensaje;
	}

}
