package mx.com.afirme.midas2.action.poliza.seguroobligatorio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexa;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class SeguroObligatorioAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PolizaDTO polizaDTO;
	private BigDecimal idToPoliza;
	private List<PolizaAnexa> polizaAnexaList;
	
	private RenovacionMasivaService renovacionMasivaService;
	private EntidadService entidadService;
	private SeguroObligatorioService seguroObligatorioService;
	private CalculoService calculoService;
	private ListadoService listadoService;
	private String mensajeSeguroObligatorio = "";
	
	private String idToPolizaList = "";
	
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}	
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}		
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("renovacionMasivaServiceEJB")
	public void setRenovacionMasivaService(
			RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}	
	
	@Override
	public void prepare() throws Exception {
		if(idToPoliza != null){
			polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
		}
	}
	
	public String mostrar(){
		if(polizaDTO != null && polizaDTO.getClaveAnexa() != null && polizaDTO.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
			try{
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("id.idToPolizaAnexa", idToPoliza);
				params.put("origen", PolizaAnexa.ORIGEN_MIDAS);
				List<PolizaAnexa> polizas = entidadService.findByProperties(PolizaAnexa.class, params);
				PolizaAnexa polizaAnexa = polizas.get(0);
				PolizaDTO polizaVoluntaria = entidadService.findById(PolizaDTO.class, polizaAnexa.getId().getIdToPoliza());
				mensajeSeguroObligatorio = "Poliza de Seguro Obligatorio; Poliza Voluntaria (" + polizaVoluntaria.getNumeroPolizaFormateada() + "). ";
			}catch(Exception e){
				mensajeSeguroObligatorio = "";
			}
		}else{
			mensajeSeguroObligatorio = "";
		}
		return SUCCESS;
	}	
	
	public String listar(){
		polizaAnexaList = new ArrayList<PolizaAnexa>(1);	
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id.idToPoliza", idToPoliza);
		params.put("origen", PolizaAnexa.ORIGEN_MIDAS);
		polizaAnexaList = entidadService.findByProperties(PolizaAnexa.class, params);
		//polizaAnexaList = entidadService.findByProperty(PolizaAnexa.class, "id.idToPoliza", idToPoliza);		
		return SUCCESS;
	}
	
	public String creaPolizaSeguroObligatorioVigentes(){
		
		GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
		generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
		generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
		generaSeguroObligatorio.setListadoService(listadoService);
		generaSeguroObligatorio.setCalculoService(calculoService);
		generaSeguroObligatorio.creaPolizaSeguroObligatorioVigentes(idToPolizaList);
		
		//System.out.println(idToPolizaList);
		return SUCCESS;
	}
	
	public String creaPolizaSeguroObligatorio(){
		if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_A_PETICION)){
			GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
			generaSeguroObligatorio.setRenovacionMasivaService(renovacionMasivaService);
			generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
			generaSeguroObligatorio.setListadoService(listadoService);
			generaSeguroObligatorio.setCalculoService(calculoService);
			generaSeguroObligatorio.creaPolizaSeguroObligatorio(polizaDTO, PolizaAnexa.TIPO_PETICION);
			mensajeSeguroObligatorio = generaSeguroObligatorio.getMensaje();
		}else{
			mensajeSeguroObligatorio = "Creacion de Poliza de Seguro Obligatorio a peticion no esta habilitado.";
		}
		return SUCCESS;
	}	

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaAnexaList(List<PolizaAnexa> polizaAnexaList) {
		this.polizaAnexaList = polizaAnexaList;
	}

	public List<PolizaAnexa> getPolizaAnexaList() {
		return polizaAnexaList;
	}

	public void setMensajeSeguroObligatorio(String mensajeSeguroObligatorio) {
		this.mensajeSeguroObligatorio = mensajeSeguroObligatorio;
	}

	public String getMensajeSeguroObligatorio() {
		return mensajeSeguroObligatorio;
	}

	public void setIdToPolizaList(String idToPolizaList) {
		this.idToPolizaList = idToPolizaList;
	}

	public String getIdToPolizaList() {
		return idToPolizaList;
	}

}
