package mx.com.afirme.midas2.action.poliza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.impresion.NegocioUsuarioEdicionImpresionService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class PolizaAction extends BaseAction implements Preparable {
	private PolizaDTO polizaDTO;
	private List<Agente> agenteList = new ArrayList<Agente>(1);
	private List<Negocio> negocioList = new ArrayList<Negocio>(1);
	private Integer idTcAgente;
	private List<PolizaDTO> polizaList = new ArrayList<PolizaDTO>(1);
	private Boolean filtrar = false;
	private List<EndosoDTO> endosoPolizaList = new ArrayList<EndosoDTO>(1);
	private String accion;
	private String numeroPolizaFormateada;
	private Short tipoRegreso = 0;
	private Short radioCancelados = 1;
	private Short mostrarCancelados = 0;
	private Integer aplicaFlotillas;

	private PolizaService polizaService;

	private EntidadService entidadService;
	
	private ListadoService listadoService;
	
	private BigDecimal id;
	private BigDecimal idToPoliza;
	private Agente agente;
	
	private Date validoEn;	
    private Date recordFrom;
    private Long validoEnMillis;
    private Long recordFromMillis;
    private Short claveTipoEndoso;
    private Boolean esSituacionActual = false;
	
	private BitemporalCotizacion biCotizacion;
    private ResumenCostosDTO resumenCostosDTO; 
    
	protected CotizacionFacadeRemote cotizacionFacadeRemote;
    protected PolizaFacadeRemote polizaFacadeRemote;
    private EntidadContinuityService entidadContinuityService;
	private EntidadBitemporalService entidadBitemporalService;
    protected EndosoService endosoService;
    private CalculoService calculoService;
    private IncisoViewService incisoViewService;
    
    private String descripcionBusquedaAgente = null;
    private String descripcionBusquedaNegocio = null;
	private String ultimoEndosoValidFromDateTimeStr;
	private String ultimoEndosoRecordFromDateTimeStr;
	private Short ultimoEndosoClaveTipoEndoso;
	private boolean agenteControlDeshabilitado;
	
	private NegocioUsuarioEdicionImpresionService negocioUsuarioEdicionImpresionService;
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	private boolean tienePermisosModificarEdicion;
	private boolean tienePermisosImprimirEdicion;
	private boolean existeEdicionEndoso;

    private BitacoraGuia bitacora = new BitacoraGuia();
    
    private boolean usuarioExterno;
	
    //Agregado por Luis Ibarra 09/Nov/2015 SAP-AMIS ALERTAS
    private String idToCotizacion;
    
    
	public boolean isAgenteControlDeshabilitado() {
		return agenteControlDeshabilitado;
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String busquedaPaginada() {
		if (polizaDTO != null) {
			if (filtrar) {
				if (getTotalCount() == null) {
					setTotalCount(polizaService.obtenerTotalPaginacion(polizaDTO,
							polizaDTO.getFechaCreacion(), polizaDTO.getFechaCreacionHasta()));
					setListadoEnSession(null);
				}
				setPosPaginado();
			}
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String buscarPolizas() {
		if (polizaDTO != null) {
			if (filtrar) {			
				this.saveReturnFields(PolizaAction.class, this);
				if (!listadoDeCache()) {
					usuarioExterno=usuarioService.getUsuarioActual().isOutsider();
					polizaDTO.setPrimerRegistroACargar(getPosStart());
					polizaDTO.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
					polizaList = polizaService.buscarPoliza(polizaDTO,
							polizaDTO.getFechaCreacion(), polizaDTO.getFechaCreacionHasta());
					setListadoEnSession(polizaList);
				} else {
					polizaList = (List<PolizaDTO>) getListadoPaginado();
					setListadoEnSession(polizaList);
				}
			}
		}
		return SUCCESS;
	}
	
	public String ventanaBusqueda() {
		return SUCCESS;
	}	
	
	public void prepareBuscarAgente() {
		if (descripcionBusquedaAgente != null) {
			agenteList = listadoService.listaAgentesPorDescripcion(descripcionBusquedaAgente.trim().toUpperCase());
		} else {
			agenteList = new ArrayList<Agente>();
		}		
	}
	
	public String buscarAgente() {
		return SUCCESS;
	}	
	
	public void prepareBuscarNegocio() {
		if (descripcionBusquedaNegocio != null) {
			negocioList = listadoService.listaNegociosPorDescripcion(descripcionBusquedaNegocio.trim().toUpperCase());
		} else {
			negocioList = new ArrayList<Negocio>();
		}		
	}
	
	public String buscarNegocio() {
		return SUCCESS;
	}
	
	public void prepareMostrarListadoPolizas() {
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			agenteControlDeshabilitado = true;
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			asignarAgente(agenteUsuarioActual);
		}
	}

	private void asignarAgente(Agente agente) {
		if (polizaDTO == null) {
			polizaDTO = new PolizaDTO();
		}
		
		if (polizaDTO.getCotizacionDTO() == null) {
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
		}
		
		if (polizaDTO.getCotizacionDTO().getSolicitudDTO() == null) {
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		}
		
		SolicitudDTO solicitud = polizaDTO.getCotizacionDTO().getSolicitudDTO();
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		solicitud.setAgente(agente);
	}

	public String mostrarListadoPolizas() {
		this.getReturnFields(PolizaAction.class, this);
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		if (polizaDTO == null) {
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setTipoPolizaDTO(new TipoPolizaDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());			
		}		
	}
	
	public void prepareVerDetallePoliza() {
		LogDeMidasWeb.log("Entrando a prepareVerDetallePoliza", Level.INFO, null);
		
		LogDeMidasWeb.log("id => " + getId(), Level.INFO, null);
		LogDeMidasWeb.log("ValidoEn => " +  getValidoEn(), Level.INFO, null);
		LogDeMidasWeb.log("RecordFrom => " +  getRecordFrom(), Level.INFO, null);
		LogDeMidasWeb.log("ValidoEnMillis => " +  getValidoEnMillis(), Level.INFO, null);
		LogDeMidasWeb.log("RecordFromMillis => " +  getRecordFromMillis(), Level.INFO, null);
		
		DateTime validFromDT = new DateTime(getValidoEnMillis());
		DateTime recordFromDT = new DateTime(getRecordFromMillis());
		LogDeMidasWeb.log("validFromDT => " + validFromDT, Level.INFO, null);
		LogDeMidasWeb.log("recordFromDT => " + recordFromDT, Level.INFO, null);
		
		  if (getId() != null) {
			  polizaDTO = entidadService.findById(PolizaDTO.class, getId());
			  LogDeMidasWeb.log("polizaDTO => " + polizaDTO, Level.INFO, null);
			  
			  BigDecimal idToCotizacionLocal = polizaDTO.getCotizacionDTO().getIdToCotizacion();
//			  biCotizacion = incisoService.getCotizacion(idToCotizacionLocal.longValue(), validoEn);
			  
			  CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(
					  CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacionLocal);
			  LogDeMidasWeb.log("cotizacionContinuity => " + cotizacionContinuity, Level.INFO, null);
			  biCotizacion = cotizacionContinuity.getCotizaciones().get(validFromDT, recordFromDT);

			  if (biCotizacion == null) {
				  Collection<BitemporalCotizacion> collectionBI = 
						entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuity.getId(), 
																									validFromDT, recordFromDT);
					if (!collectionBI.isEmpty()) {
						biCotizacion = collectionBI.iterator().next();	
					}
			  }
			  
			  aplicaFlotillas = biCotizacion.getValue().getTipoPoliza().getClaveAplicaFlotillas();	
			  
			  if (esSituacionActual) {
				  endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", getId(), Boolean.TRUE);
					if (endosoPolizaList != null && endosoPolizaList.size() > 0) {
						EndosoDTO ultimoEndosoDTO = endosoPolizaList.get(endosoPolizaList.size() - 1);
						ultimoEndosoRecordFromDateTimeStr = ultimoEndosoDTO.getRecordFrom().getTime() + "";
					}		
			  } else {
				  ultimoEndosoRecordFromDateTimeStr = recordFromDT.getMillis()+ "";
			  }
			  	 
			  Boolean esServicioPublico=(Boolean)listadoService.getSpvData(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio()).get("esServicioPublico");
			  polizaDTO.setEsServicioPublico(esServicioPublico);
			  usuarioExterno=usuarioService.getUsuarioActual().isOutsider();
			  LogDeMidasWeb.log("biCotizacion => " + biCotizacion, Level.INFO, null);
			  
		  }
		  
		LogDeMidasWeb.log("Saliendo de prepareVerDetallePoliza", Level.INFO, null);
	}
	
	public String verDetallePoliza() {
			return SUCCESS;
	}
	
	public String verContenedorPoliza() {
		if (validoEn == null && validoEnMillis != null) {
			validoEn = new Date(validoEnMillis);
		}
		if (recordFrom == null && recordFromMillis != null) {
			recordFrom = new Date(recordFromMillis);
		}		
		this.idToCotizacion = entidadService.findByProperty(PolizaDTO.class, "idToPoliza", id).get(0).getCotizacionDTO().getIdToCotizacion()+"";
		return SUCCESS;
	}
	
	public String mostrarResumenTotales() {
//		DateTime validFromDT = null;
		DateTime recordFromDT = null;
//		
//		if (validoEnMillis != null) {
//			validFromDT = new DateTime(validoEnMillis);
//		}
//		
		if (recordFromMillis != null) {
			recordFromDT = new DateTime(recordFromMillis);
		}
//		
		
		polizaDTO = entidadService.findById(PolizaDTO.class, polizaDTO.getIdToPoliza());		  
		//if (claveTipoEndoso != null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION) {
		Short numeroEndoso = null;
		
		if (!esSituacionActual) {
			numeroEndoso = incisoViewService.getNumeroEndoso(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue(), 
					recordFromDT.toDate());
			
			if (numeroEndoso == null) {
				numeroEndoso = (short) 0;
			}
		}
        
        if (numeroEndoso != null) {
        	resumenCostosDTO = calculoService.resumenCostosEndoso(polizaDTO.getIdToPoliza(), numeroEndoso, true);
        } else {
        	resumenCostosDTO = calculoService.resumenCostosCaratulaPoliza(polizaDTO.getIdToPoliza());
        }

			//resumenCostosDTO = calculoService.resumenCostosCaratulaPoliza(polizaDTO.getIdToPoliza());
		/*} else {
			resumenCostosDTO = calculoService.obtenerResumenCotizacionEndoso(biCotizacion.getEntidadContinuity().getBusinessKey(), 
					TimeUtils.getDateTime(validoEn), false);
		}*/		
		
		return SUCCESS;
	}
	
	public String ventanaEmails(){
		return SUCCESS;
	}
	
	public String ventanaCaractVehiculo(){
		return SUCCESS;
	}	
	
	public String ventanaDetalleEndoso(){
		if(getId()!=null){
			endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", getId(), Boolean.TRUE);
			numeroPolizaFormateada = endosoPolizaList.get(0).getPolizaDTO().getNumeroPolizaFormateada();
			
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, getId());
			double dias = UtileriasWeb.obtenerDiasEntreFechas(new Date(), poliza.getCotizacionDTO().getFechaInicioVigencia());
			Date fechaInicio = new Date();
			if(dias > 0){
				fechaInicio = poliza.getCotizacionDTO().getFechaInicioVigencia();
			}
			//EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndoso(getId().longValue(), fechaInicio);
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(getId().longValue(), fechaInicio);
			if (ultimoEndosoDTO != null) {
				ultimoEndosoValidFromDateTimeStr = ultimoEndosoDTO.getValidFrom().getTime() + "";
				ultimoEndosoRecordFromDateTimeStr = ultimoEndosoDTO.getRecordFrom().getTime() + "";
				ultimoEndosoClaveTipoEndoso = ultimoEndosoDTO.getClaveTipoEndoso();
			} else {
				ultimoEndosoValidFromDateTimeStr = fechaInicio.getTime() + "";
				ultimoEndosoRecordFromDateTimeStr = fechaInicio.getTime() + "";					
			}
			
			if(poliza != null){
				existeEdicionEndoso = generaPlantillaReporteBitemporalService.existeEdicionEndoso(poliza.getIdToPoliza(), 
					recordFrom, claveTipoEndoso);
				tienePermisosImprimirEdicion = 
					negocioUsuarioEdicionImpresionService.tienePermisoImprimir(
							usuarioService.getUsuarioActual().getNombreUsuario(), poliza.getIdToPoliza());
				tienePermisosModificarEdicion = 
					negocioUsuarioEdicionImpresionService.tienePermisoEditar(
							usuarioService.getUsuarioActual().getNombreUsuario(), poliza.getIdToPoliza());
			}
			/*
			if(endosoPolizaList != null && endosoPolizaList.size()>0){
				EndosoDTO ultimoEndosoDTO = endosoPolizaList.get(endosoPolizaList.size() - 1);
				ultimoEndosoValidFromDateTimeStr = ultimoEndosoDTO.getValidFrom().getTime() + ""; 
				ultimoEndosoRecordFromDateTimeStr = ultimoEndosoDTO.getRecordFrom().getTime() + "";
				ultimoEndosoClaveTipoEndoso = ultimoEndosoDTO.getClaveTipoEndoso();
			}*/
		}
		return SUCCESS;
	}
	
	public String ventanaDetalleBitacoEnvio(){
		return SUCCESS;
	}
	
	public String getBitacoraEnvio(){
		bitacora = polizaService.getBitacoraEnvioPoliza(polizaDTO.getIdToPoliza());
		return SUCCESS;
	}
	
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public Integer getIdTcAgente() {
		return idTcAgente;
	}

	public void setIdTcAgente(Integer idTcAgente) {
		this.idTcAgente = idTcAgente;
	}	

	public List<PolizaDTO> getPolizaList() {
		return polizaList;
	}

	public void setPolizaList(List<PolizaDTO> polizaList) {
		this.polizaList = polizaList;
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}
	
    public List<EndosoDTO> getEndosoPolizaList() {
		return endosoPolizaList;
	}

	public void setEndosoPolizaList(List<EndosoDTO> endosoPolizaList) {
		this.endosoPolizaList = endosoPolizaList;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getNumeroPolizaFormateada() {
		return numeroPolizaFormateada;
	}

	public void setNumeroPolizaFormateada(String numeroPolizaFormateada) {
		this.numeroPolizaFormateada = numeroPolizaFormateada;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}	

	public void setDescripcionBusquedaAgente(
			String descripcionBusquedaAgente) {
		this.descripcionBusquedaAgente = descripcionBusquedaAgente;
	}

	public String getDescripcionBusquedaAgente() {
		return descripcionBusquedaAgente;
	}
	
	public String getDescripcionBusquedaNegocio() {
		return descripcionBusquedaNegocio;
	}

	public void setDescripcionBusquedaNegocio(String descripcionBusquedaNegocio) {
		this.descripcionBusquedaNegocio = descripcionBusquedaNegocio;
	}
	
	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Long getValidoEnMillis() {
		return validoEnMillis;
	}

	public void setValidoEnMillis(Long validoEnMillis) {
		this.validoEnMillis = validoEnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Short getClaveTipoEndoso(){
		return this.claveTipoEndoso;
	}
	
	public void setClaveTipoEndoso(Short claveTipoEndoso){
		this.claveTipoEndoso = claveTipoEndoso;
	}
	
	public String getUltimoEndosoRecordFromDateTimeStr() {
		return ultimoEndosoRecordFromDateTimeStr;
	}

	public void setUltimoEndosoRecordFromDateTimeStr(
			String ultimoEndosoRecordFromDateTimeStr) {
		this.ultimoEndosoRecordFromDateTimeStr = ultimoEndosoRecordFromDateTimeStr;
	}

	public String getUltimoEndosoValidFromDateTimeStr() {
		return ultimoEndosoValidFromDateTimeStr;
	}

	public void setUltimoEndosoValidFromDateTimeStr(
			String ultimoEndosoValidFromDateTimeStr) {
		this.ultimoEndosoValidFromDateTimeStr = ultimoEndosoValidFromDateTimeStr;
	}

	public void setUltimoEndosoClaveTipoEndoso(Short ultimoEndosoClaveTipoEndoso){
		this.ultimoEndosoClaveTipoEndoso = ultimoEndosoClaveTipoEndoso;
	}
	
	public Short getUltimoEndosoClaveTipoEndoso(){
		return this.ultimoEndosoClaveTipoEndoso;
	}
	
	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Autowired
	@Qualifier("cotizacionFacadeRemoteEJB")
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}
	
	@Autowired
	@Qualifier("polizaEJB")
	public void setPolizaFacadeRemote(
			PolizaFacadeRemote polizaFacadeRemote) {
		this.polizaFacadeRemote = polizaFacadeRemote;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}	
	
	/**
	 * @param endosoService the endosoService to set
	 */
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}	
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("entidadContinuityServiceEJB")
	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService){
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("polizaServiceEJB")
	public void setPolizaService(PolizaService polizaService) {
		this.polizaService = polizaService;
	}
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setTipoRegreso(Short tipoRegreso) {
		this.tipoRegreso = tipoRegreso;
	}

	public Short getTipoRegreso() {
		return tipoRegreso;
	}

	public Short getRadioCancelados() {
		return radioCancelados;
	}

	public void setRadioCancelados(Short radioCancelados) {
		this.radioCancelados = radioCancelados;
	}

	public Short getMostrarCancelados() {
		return mostrarCancelados;
	}

	public void setMostrarCancelados(Short mostrarCancelados) {
		this.mostrarCancelados = mostrarCancelados;
	}

	public Integer getAplicaFlotillas() {
		return aplicaFlotillas;
	}

	public void setAplicaFlotillas(Integer aplicaFlotillas) {
		this.aplicaFlotillas = aplicaFlotillas;
	}

	public Boolean getEsSituacionActual() {
		return esSituacionActual;
	}

	public void setEsSituacionActual(Boolean esSituacionActual) {
		this.esSituacionActual = esSituacionActual;
	}

	public BitacoraGuia getBitacora() {
		return bitacora;
	}

	public void setBitacora(BitacoraGuia bitacora) {
		this.bitacora = bitacora;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public boolean isUsuarioExterno() {
		return usuarioExterno;
	}

	public void setUsuarioExterno(boolean usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}

	@Autowired
	@Qualifier("negocioUsuarioEdicionImpresionServiceEJB")
	public void setNegocioUsuarioEdicionImpresionService(
			NegocioUsuarioEdicionImpresionService negocioUsuarioEdicionImpresionService) {
		this.negocioUsuarioEdicionImpresionService = negocioUsuarioEdicionImpresionService;
	}

	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
	public void setGeneraPlantillaReporteBitemporalService(
			GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService) {
		this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService;
	}

	public boolean isTienePermisosModificarEdicion() {
		return tienePermisosModificarEdicion;
	}

	public void setTienePermisosModificarEdicion(
			boolean tienePermisosModificarEdicion) {
		this.tienePermisosModificarEdicion = tienePermisosModificarEdicion;
	}

	public boolean isTienePermisosImprimirEdicion() {
		return tienePermisosImprimirEdicion;
	}

	public void setTienePermisosImprimirEdicion(boolean tienePermisosImprimirEdicion) {
		this.tienePermisosImprimirEdicion = tienePermisosImprimirEdicion;
	}

	public boolean isExisteEdicionEndoso() {
		return existeEdicionEndoso;
	}

	public void setExisteEdicionEndoso(boolean existeEdicionEndoso) {
		this.existeEdicionEndoso = existeEdicionEndoso;
	}
		
}
