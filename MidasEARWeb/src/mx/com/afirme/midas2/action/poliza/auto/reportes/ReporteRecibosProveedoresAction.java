package mx.com.afirme.midas2.action.poliza.auto.reportes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormatSymbols;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.DatosRecibosProveedores;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

@Component
@Scope("prototype")
@Namespace("/reportes/recibosProveedores")
public class ReporteRecibosProveedoresAction extends BaseAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1115271099948547305L;
	private String mes;
	private String anio;
	private String contentType;
	private String fileName;
	private InputStream reporteStream;
	private String mesSeleccionado;
	private String anioSeleccionado;
	
	private GenerarPlantillaReporte generarPlantillaReporte;
	private ImpresionesService impresionesService;	
	private EntidadService entidadService;
	private Boolean existenDatos;
	
	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporte(
			GenerarPlantillaReporte generarPlantillaReporte) {
		this.generarPlantillaReporte = generarPlantillaReporte;
	}
	
	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Action(value ="mostrar", results ={
			@Result(name= SUCCESS, location="/jsp/poliza/auto/reportes/reporteRecibosProveedores.jsp")
	})
	public String mostrar(){
		
		return SUCCESS;
	}
	
	@Action(value ="validarExistenciaDatos", results ={
			@Result(name= SUCCESS, location="/jsp/poliza/auto/reportes/reporteRecibosProveedores.jsp")
	})
	public String validarExistenciaDatos()
	{
		mesSeleccionado = this.getMes();
		anioSeleccionado = this.getAnio();
		String mesAnio = mesSeleccionado + anioSeleccionado;
		Map<String, Object> params = new LinkedHashMap<String, Object>();		
		params.put("periodo", mesAnio.length() == 6 ? mesAnio : "0" + mesAnio);
		List<DatosRecibosProveedores> listaRecibosProv = entidadService.findByProperties(DatosRecibosProveedores.class, params);
		if(listaRecibosProv == null || listaRecibosProv.isEmpty())
		{
			this.setMensaje("No existen datos generados para el periodo seleccionado, debe generar los datos antes de intentar generar el Reporte.");
			existenDatos = Boolean.FALSE;
			return SUCCESS;
		}		
						
		existenDatos = Boolean.TRUE;
		return SUCCESS;		
	}
	
	@Action(value ="generarDatos", results ={
			@Result(name= SUCCESS, location="/jsp/poliza/auto/reportes/reporteRecibosProveedores.jsp"),
			@Result(name= INPUT, location="/jsp/poliza/auto/reportes/reporteRecibosProveedores.jsp")
	})
	public String generarDatos(){
		
		int  resultado = 0;
		String mesAnio = this.getMes() + this.getAnio();
		try {
			resultado = impresionesService.generarDatosReporteRecibosProv(mesAnio);
		} catch (Exception e) {
			
			e.printStackTrace();
			this.setMensajeError("Error al generar el Reporte: " + e.toString());
			return ERROR;
		}
		String[] monthsArray = new DateFormatSymbols(new Locale("es","MX")).getMonths();
		
		if(resultado == 0) //Con ok
		{
			this.setMensajeExitoPersonalizado("Los datos correspondientes al Periodo " + monthsArray[Integer.valueOf(this.getMes())-1] + " " 
					+ this.getAnio() + " han sido generados exitosamente.");			
		}else //Con error
		{
			this.setMensajeExitoPersonalizado("No se encontraron datos relacionados al Período seleccionado " + monthsArray[Integer.valueOf(this.getMes())-1] + " " 
					+ this.getAnio() + ".");
		}			
		
		existenDatos = Boolean.TRUE;
		
		return SUCCESS;
	}	
	
	@Action(value = "generarReporte", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteStream" }),
			@Result(name = INPUT, location = "/jsp/poliza/auto/reportes/reporteRecibosProveedores.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String generarReporte()
	{
		String mesAnio = this.getMes() + this.getAnio();
		try {
			TransporteImpresionDTO transporte = generarPlantillaReporte.imprimirReporteRecibosProveedores(mesAnio.length() == 6 ? mesAnio : "0" + mesAnio);
			
			if (transporte != null) {
				setReporteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron datos con los criterios de busqueda seleccionados.");
				return "INPUT";
			}
			
		} catch (JRException e) {			
			
			e.printStackTrace();
			this.setMensajeError("Error al generar el Reporte: " + e.toString());
			return ERROR;
		}
		
		setContentType("application/xls");
		setFileName("ReporteRecibosProveedores.xls");
		
		return SUCCESS;		
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getReporteStream() {
		return reporteStream;
	}

	public void setReporteStream(InputStream reporteStream) {
		this.reporteStream = reporteStream;
	}

	public Boolean getExistenDatos() {
		return existenDatos;
	}

	public void setExistenDatos(Boolean existenDatos) {
		this.existenDatos = existenDatos;
	}

	public String getMesSeleccionado() {
		return mesSeleccionado;
	}

	public void setMesSeleccionado(String mesSeleccionado) {
		this.mesSeleccionado = mesSeleccionado;
	}

	public String getAnioSeleccionado() {
		return anioSeleccionado;
	}

	public void setAnioSeleccionado(String anioSeleccionado) {
		this.anioSeleccionado = anioSeleccionado;
	}

}
