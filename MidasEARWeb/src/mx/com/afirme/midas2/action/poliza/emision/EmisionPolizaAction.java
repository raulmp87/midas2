package mx.com.afirme.midas2.action.poliza.emision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class EmisionPolizaAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private EntidadService entidadService;
	private IncisoViewService incisoService;
	private EntidadContinuityService entidadContinuityService;
	private EntidadBitemporalService entidadBitemporalService;
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	
	private Collection<BitemporalInciso> biIncisoList;
	private Set<String> descripcionesIncisos = new HashSet<String>();
	private BitemporalCotizacion biCotizacion;
	private BitemporalInciso biInciso;	
	private BitemporalAutoInciso biAutoInciso;	
	
	private ResumenCostosDTO resumenCostosDTO;
	private PolizaDTO polizaDTO;
	private BigDecimal polizaId;
	private Long cotizacionId;
	private Long incisoId;
	private Date validoEn;
	private Date recordFrom;
	private Long validoEnMillis;
	private Long recordFromMillis;
	private Short claveTipoEndoso;
	private boolean mostrarDatoConductorInciso = true;
	private IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
	private Integer aplicaFlotillas;


	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("entidadContinuityServiceEJB")
	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService){
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	

	@Autowired
    @Qualifier("listadoIncisosDinamicoServiceEJB")
	public void setListadoIncisosDinamicoService(
			ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}
	
	
	@Override
	public void prepare() throws Exception {
			
	}
	
	public void prepareVerEmision() {
		if (polizaId != null) {
			  polizaDTO = entidadService.findById(PolizaDTO.class, polizaId);
			  
			  if (recordFrom == null) {
				  biCotizacion = incisoService.getCotizacion(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue(), validoEn);
			  }			 
			  
			  if (biCotizacion == null) {
				  CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(
												  CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, 
												  polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue());
				  LogDeMidasWeb.log("cotizacionContinuity => " + cotizacionContinuity, Level.INFO, null);
				  biCotizacion = cotizacionContinuity.getCotizaciones().get(new DateTime(validoEnMillis), new DateTime(recordFromMillis));
				  
				  if (biCotizacion == null) {
					  DateTime validFromDT = new DateTime(getValidoEnMillis());
					  DateTime recordFromDT = new DateTime(getRecordFromMillis());
					  Collection<BitemporalCotizacion> collectionBI = 
							entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuity.getId(), 
									validFromDT, recordFromDT);
					  if (!collectionBI.isEmpty()) {
							biCotizacion = collectionBI.iterator().next();
					  }
				  }
			  }
			  
			  aplicaFlotillas = biCotizacion.getValue().getTipoPoliza().getClaveAplicaFlotillas();
			  
			  if (descripcionesIncisos == null || descripcionesIncisos.isEmpty()) {
					descripcionesIncisos = listadoIncisosDinamicoService.listarIncisosDescripcion((short) 0,  biCotizacion.getContinuity().getId(),  validoEn);
			  }
		}
	}
	
	public String verEmision() {		
		return SUCCESS;
	}

	
	public String busquedaPaginada() {
		Collection<BitemporalInciso> listado = new ArrayList<BitemporalInciso>();

		if (getTotalCount() == null) {
			
			//listado = incisoService.getLstIncisoByCotizacion(cotizacionId, validoEn);			
			
			//if (listado == null || listado.isEmpty()) {
			listado = incisoService.getLstIncisoCanceladosByCotizacion(filtros, cotizacionId, new Date(getValidoEnMillis()), 
																						new Date(getRecordFromMillis()),true);
				//}

			setTotalCount(new Integer(listado.size()).longValue());
		}
		setPosPaginado();
		return SUCCESS;
	}
	
	public String buscarIncisos() {		
		Collection<BitemporalInciso> listado = new ArrayList<BitemporalInciso>();
		
		if (!super.listadoDeCache()) {
			
			//listado = incisoService.getLstIncisoByCotizacion(cotizacionId, validoEn);			
			
			//if (listado == null || listado.isEmpty()) {
			filtros.setPrimerRegistroACargar(getPosStart());
			filtros.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
			listado = incisoService.getLstIncisoCanceladosByCotizacion(filtros, cotizacionId, new Date(getValidoEnMillis()), 
																						new Date(getRecordFromMillis()),false);
			//}
			//Obtiene descripcion de inciso formal
			if (listado != null && !listado.isEmpty()) {
				List<BitemporalInciso> lista = new ArrayList<BitemporalInciso>(1);
				for (BitemporalInciso inciso : listado) {
					try {
						IncisoCotizacionId id = new IncisoCotizacionId();
						id.setIdToCotizacion(new BigDecimal(inciso.getContinuity().getCotizacionContinuity().getNumero()));
						id.setNumeroInciso(new BigDecimal(inciso.getContinuity().getNumero()));
						IncisoCotizacionDTO incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, id);
						inciso.getValue().setDescripcionGiroAsegurado(incisoCotizacion.getIncisoAutoCot().getDescripcionFinal());
						
					} catch(Exception e) {
					}
					
					if (lista.isEmpty()) {
						lista.add(inciso);
					} else {
						int i = 0;
						boolean agregado = false;
						for (BitemporalInciso incisoSort : lista) {
							if (incisoSort.getValue().getNumeroSecuencia().compareTo(inciso.getValue().getNumeroSecuencia()) >= 0 ) {
								lista.add(i, inciso);
								agregado = true;
								break;
							}
							i++;
						}
						if (!agregado) {
							lista.add(inciso);
						}
					}
				}
				listado = lista;
			}			
		} else {
			listado = (List<BitemporalInciso>) getListadoPaginado();	
		}
		
		setBiIncisoList(listado);
		setListadoEnSession(listado);

		return SUCCESS;
	}

	public String mostrarResumenTotalesInciso(){
		//resumenCostosDTO = incisoService.obtenerResumenInciso(cotizacionId, incisoCotizacion.getIncisoAutoCot(), incisoCotizacion, coberturaCotizacionList);
		return SUCCESS;
	}
	
	public String verDatosVehiculo() {
		
		//biInciso = incisoService.getInciso(incisoId, validoEn);
		//biAutoInciso = incisoService.getAutoInciso(incisoId, validoEn);
		
		//if (biInciso == null) {
		biInciso = incisoService.getInciso(incisoId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()), 
				EndosoDTO.TIPO_ENDOSO_CANCELACION);
			//}
		
			//if (biAutoInciso == null) {
		biAutoInciso = incisoService.getAutoInciso(incisoId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()), 
				EndosoDTO.TIPO_ENDOSO_CANCELACION);
			//}
		
		mostrarDatoConductorInciso = incisoService.infoConductorEsRequerido(incisoId, new DateTime(getValidoEnMillis()),
																						new DateTime(getRecordFromMillis()), false);
		
		return SUCCESS;
	}		

	public Collection<BitemporalInciso> getBiIncisoList() {
		return biIncisoList;
	}
	
	public void setBiIncisoList(Collection<BitemporalInciso> biIncisoList) {
		this.biIncisoList = biIncisoList;
	}
	
	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	
	public BitemporalInciso getBiInciso() {
		return biInciso;
	}
	
	public void setBiInciso(BitemporalInciso biInciso) {
		this.biInciso = biInciso;
	}
	
	public BitemporalAutoInciso getBiAutoInciso() {
		return biAutoInciso;
	}
	
	public void setBiAutoInciso(BitemporalAutoInciso biAutoInciso) {
		this.biAutoInciso = biAutoInciso;
	}
	
	public Long getCotizacionId() {
		return cotizacionId;
	}
	
	public void setCotizacionId(Long cotizacionId) {
		this.cotizacionId = cotizacionId;
	}
	
	public Long getIncisoId() {
		return incisoId;
	}
	
	public void setIncisoId(Long incisoId) {
		this.incisoId = incisoId;
	}
	
	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}
	
	public BigDecimal getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}
	
	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public boolean isMostrarDatoConductorInciso() {
		return mostrarDatoConductorInciso;
	}

	public void setMostrarDatoConductorInciso(boolean mostrarDatoConductorInciso) {
		this.mostrarDatoConductorInciso = mostrarDatoConductorInciso;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Long getValidoEnMillis() {
		return validoEnMillis;
	}

	public void setValidoEnMillis(Long validoEnMillis) {
		this.validoEnMillis = validoEnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	/**
	 * @return the polizaDTO
	 */
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	/**
	 * @param polizaDTO the polizaDTO to set
	 */
	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public IncisoCotizacionDTO getFiltros() {
		return filtros;
	}

	public void setFiltros(IncisoCotizacionDTO filtros) {
		this.filtros = filtros;
	}

	public Integer getAplicaFlotillas() {
		return aplicaFlotillas;
	}

	public void setAplicaFlotillas(Integer aplicaFlotillas) {
		this.aplicaFlotillas = aplicaFlotillas;
	}

	public Set<String> getDescripcionesIncisos() {
		return descripcionesIncisos;
	}

	public void setDescripcionesIncisos(Set<String> descripcionesIncisos) {
		this.descripcionesIncisos = descripcionesIncisos;
	}	
	

}