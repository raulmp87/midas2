package mx.com.afirme.midas2.action.poliza.inciso;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class GenerarIncisoEmision {
	
	private IncisoService incisoService;

	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	public IncisoService getIncisoService() {
		return incisoService;
	}
	
	public void regeneraIncisos(BigDecimal IdCotizacion){
		List<IncisoCotizacionDTO> incisos = incisoService.getIncisos(IdCotizacion);
		//Ordena los incisos
		if(incisos != null && incisos.size() > 0){
			Collections.sort(incisos, new Comparator<IncisoCotizacionDTO>(){
				public int compare(IncisoCotizacionDTO o1, IncisoCotizacionDTO o2) {
					if(o1 == null || o2 == null){
						return 1;
					}
					return o1.getId().getNumeroInciso().compareTo(
							o2.getId().getNumeroInciso());
				}
			});
			
			long numeroInciso = 1;
			for(int i = 0; i < incisos.size(); i++){
				IncisoCotizacionDTO inciso = incisos.get(i);
				long numeroIncisoAct = inciso.getId().getNumeroInciso().longValue();
				if(numeroIncisoAct == numeroInciso){
					numeroInciso++;
				}else{
					incisoService.copiarInciso(inciso, IdCotizacion, numeroInciso);
					incisoService.borrarInciso(inciso);
					numeroInciso++;
				}
			}
		}

	}

}
