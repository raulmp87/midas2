package mx.com.afirme.midas2.action.negocio.cliente;

import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.logging.Level;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.cliente.NegocioClienteService;
import mx.com.afirme.midas2.service.negocio.cliente.grupo.NegocioGrupoClienteService;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
import com.sun.net.httpserver.Authenticator.Success;

@Component
@Scope("prototype")
public class AsociarClienteGrupoNegocioAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1L;

	private Long idCliente;
	private Long idGrupoCliente;
	private String idNegocio;
	private String tabActiva;
	private String nivelActivo = "1";
	private String claveNegocio;
	private String tipoAccion;
	private Short tipoRegreso;
	private Short tipoPersona; // par�metro del action actualizarTipoPersona, con el valor del Tipo de Persona del Negocio 
	private InputStream inputStream; // texto, valor de retorno del action actualizarTipoPersona
	private NegocioService negocioService;
	private List<NegocioCliente> listaNegocioCliente;
	private List<NegocioGrupoCliente> listaNegocioGrupoCliente;
	//EJB's
	private NegocioClienteService negocioClienteService;
	private NegocioGrupoClienteService negocioGrupoClienteService;
	
	@Override
	public void prepare() throws Exception {
	}
	
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	public String actualizarTipoPersona(){
		// modificar el Tipo de Persona del Negocio
		try {
			negocioService.cambiarTipoPersona(Long.valueOf(idNegocio), tipoPersona);
			// modificaci�n del tipo de persona fue un �xito
			inputStream = new StringBufferInputStream("success");
			return SUCCESS;
		} catch (Exception e) {
			// existe un error durante la modificaci�n del tipo de persona
			// imprimir en el log el error que ocurri� durante la transacci�n en base de datos
			LogDeMidasWeb.log("Ocurrio un error durante la transacci�n para cambiar el Tipo de Persona del Negocio: " + e, Level.INFO, null);
			inputStream = new StringBufferInputStream("error");
			return SUCCESS;
		}
	}
	
	public String getClientesAsociados(){
		//Consultar el listado de registros
		listaNegocioCliente = negocioService.listarClientesAsociados(Long.valueOf(idNegocio));//negocioClienteService.listarPorNegocio(Long.valueOf(idNegocio));
		if(listaNegocioCliente != null){
			super.setMensajeExito();
		}else{
			listaNegocioCliente = new ArrayList<NegocioCliente>();
			super.setMensajeExitoPersonalizado("No hay clientes asociados");
		}
		//Redireccionar al JSP de listado de clientes
		super.setMensajeExito();
		return SUCCESS;
	}
	
	public String asociarCliente(){
		//invocar servicio para asociar cliente, usando: idCliente e idNegocio
		if(negocioClienteService.relacionarClienteNegocio(Long.valueOf(idNegocio), new BigDecimal(idCliente))){
			super.setMensajeExito();
		}else{
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
		}
		setTabActiva("clientes");
		setNivelActivo("1");
		return "content";
	}
	
	public String desasociarCliente(){
		//invocar ajax para desasociar cliente, usando: idCliente e idNegocio
		setTabActiva("clientes");
		setNivelActivo("1");
		if(negocioClienteService.desasociarClienteNegocio(Long.valueOf(idNegocio), new BigDecimal(idCliente)) != null){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ELIMINAR);
		}else{
			super.setMensajeExito();
		}
		return "content";
	}
	
	public String eliminarRelacionesClienteGrupo(){
		setTabActiva("clientes");
		if(negocioClienteService.eliminarAsociacionesClientesYGruposNegocio(Long.valueOf(idNegocio)) != null){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ELIMINAR);
		}else{
			super.setMensajeExito();
		}
		return "content";
	}
	
	public String getGruposAsociados(){
		//Consultar el listado de registros
		listaNegocioGrupoCliente = negocioGrupoClienteService.listarPorNegocio(Long.valueOf(idNegocio));
		if(listaNegocioGrupoCliente != null){
			super.setMensajeExito();
		}else{
			listaNegocioGrupoCliente = new ArrayList<NegocioGrupoCliente>();
			super.setMensajeError("No hay grupos asociados");
			return "content";
		}
		//Redireccionar al JSP de listado de clientes
		return SUCCESS;
	}
	
	public String asociarGrupo(){
		//invocar servicio para asociar grupo-cliente, usando: idCliente e idNegocio
		setTabActiva("clientes");
		setNivelActivo("2");
		if(negocioGrupoClienteService.relacionarGrupoClienteNegocio(idGrupoCliente, Long.valueOf(idNegocio))){
			super.setMensajeExito();
		}else{
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
		}
		return "content";
	}
	
	public String desasociarGrupo(){
		setTabActiva("clientes");
		setNivelActivo("2");
		//invocar ajax para desasociar grupo-cliente, usando: idCliente e idNegocio
		if(negocioGrupoClienteService.desasociarGrupoClienteNegocio(idGrupoCliente, Long.valueOf(idNegocio)) != null){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ELIMINAR);
		}else{
			super.setMensajeExito();
		}
		return "content";
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdGrupoCliente() {
		return idGrupoCliente;
	}

	public void setIdGrupoCliente(Long idGrupoCliente) {
		this.idGrupoCliente = idGrupoCliente;
	}


	public String getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(String idNegocio) {
		this.idNegocio = idNegocio;
	}

	public List<NegocioCliente> getListaNegocioCliente() {
		return listaNegocioCliente;
	}

	public void setListaNegocioCliente(List<NegocioCliente> listaNegocioCliente) {
		this.listaNegocioCliente = listaNegocioCliente;
	}

	public List<NegocioGrupoCliente> getListaNegocioGrupoCliente() {
		return listaNegocioGrupoCliente;
	}

	public void setListaNegocioGrupoCliente(
			List<NegocioGrupoCliente> listaNegocioGrupoCliente) {
		this.listaNegocioGrupoCliente = listaNegocioGrupoCliente;
	}

	@Autowired
	@Qualifier("negocioClienteEJB")
	public void setNegocioClienteService(NegocioClienteService negocioClienteService) {
		this.negocioClienteService = negocioClienteService;
	}

	@Autowired
	@Qualifier("negocioGrupoClienteEJB")
	public void setNegocioGrupoClienteService(NegocioGrupoClienteService negocioGrupoClienteService) {
		this.negocioGrupoClienteService = negocioGrupoClienteService;
	}
	
	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public String getNivelActivo() {
		return nivelActivo;
	}

	public void setNivelActivo(String nivelActivo) {
		this.nivelActivo = nivelActivo;
	}

	public void setTipoRegreso(Short tipoRegreso) {
		this.tipoRegreso = tipoRegreso;
	}

	public Short getTipoRegreso() {
		return tipoRegreso;
	}
	
	public void setTipoPersona(Short tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Short getTipoPersona() {
		return tipoPersona;
	}
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}
}
