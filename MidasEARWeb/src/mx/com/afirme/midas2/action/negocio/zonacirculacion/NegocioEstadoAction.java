package mx.com.afirme.midas2.action.negocio.zonacirculacion;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.zonacirculacion.RelacionesNegocioZonaCirculacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioEstadoAction extends BaseAction implements Preparable{

private static final long serialVersionUID = 1L;
private Long id;
private Negocio negocio;
private EntidadService entidadService;

private RespuestaGridRelacionDTO respuesta;
private RelacionesNegocioZonaCirculacion relacionesNegocioZonaCirculacion;	
private NegocioEstadoService negocioEstadoService;
private EstadoDTO estadoDTO;
private NegocioEstado negocioEstado;

	@Override
	public void prepare() throws Exception {
	
		if (negocio==null) negocio = new Negocio();
		  if(getId()!=null){
			  negocio = entidadService.findById(Negocio.class, getId());				
		  }	
	}
	
	public String obtenerRelaciones(){
		
		relacionesNegocioZonaCirculacion = negocioEstadoService.getRelationLists(id);
		return SUCCESS;
	}
	
	
	
	public String relacionarNegocioZonaCirculacion(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(negocioEstadoService.relacionarNegocio(accion,negocioEstado) != null){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
		}else{
			super.setMensajeExito();
		}
		return SUCCESS;
	}

	public String mostrar(){
		return SUCCESS;
	}
	
	public String listar(){
		return SUCCESS;
	}



	public Negocio getNegocio() {
		return negocio;
	}
	
	
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}
	
	
	public Long getId() {
		return id;
	}
	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}
	
	
	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}
	
	
	public RelacionesNegocioZonaCirculacion getRelacionesNegocioZonaCirculacion() {
		return relacionesNegocioZonaCirculacion;
	}
	
	
	public void setRelacionesNegocioZonaCirculacion(
			RelacionesNegocioZonaCirculacion relacionesNegocioZonaCirculacion) {
		this.relacionesNegocioZonaCirculacion = relacionesNegocioZonaCirculacion;
	}
	
	
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}
	
	
	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}	
	
	public NegocioEstado getNegocioEstado() {
		return negocioEstado;
	}
	
	
	public void setNegocioEstado(NegocioEstado negocioEstado) {
		this.negocioEstado = negocioEstado;
	}


	@Autowired
	@Qualifier("negocioEstadoServiceEJB")
	public void setNegocioEstadoService(
			NegocioEstadoService negocioEstadoService) {
		this.negocioEstadoService = negocioEstadoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	

}
