package mx.com.afirme.midas2.action.negocio.bonocomision;

import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.validator.group.NegocioBonoEspecial;
import mx.com.afirme.midas2.validator.group.NegocioSesionDerechos;
import mx.com.afirme.midas2.validator.group.NegocioSobreComision;
import mx.com.afirme.midas2.validator.group.NegocioUDI;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Namespace("/negocio/bonocomision")
@Scope("prototype")
public class NegocioBonoComisionAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idToNegocio;
	private NegocioBonoComision negocioBonoComision;
	public EntidadService entidadService;
	public static final Float HUNDRED = 100F;
	
	@Override
	public void prepare() throws Exception {
		if(idToNegocio != null){
			List<NegocioBonoComision> tempList = entidadService.findByProperty(NegocioBonoComision.class,
					"negocio.idToNegocio", idToNegocio);
			if(tempList != null && tempList.size() > 0){
				negocioBonoComision= tempList.get(0);
			}
		}else{
			idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
			if(idToNegocio != null){
				this.prepare();
			}
		}
	}

	@Action(value = "mostrar", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/bonocomision/bonocomision.jsp") })
	public String mostrar() {
		return SUCCESS;
	}
	
	public void validateGuardar() {
		if (negocioBonoComision.getClaveBono()) {

			
			addErrors(negocioBonoComision, NegocioBonoEspecial.class, this,
					"negocioBonoComision");
			if(negocioBonoComision.getClavePorcentajeImporteBono()!= null && negocioBonoComision.getClavePorcentajeImporteBono()){
				if(negocioBonoComision.getPctBono() == null){

				}else{
					if(negocioBonoComision.getPctBono().floatValue() > 100){
						try{
							addFieldError("negocioBonoComision.pctBono", getText("javax.validation.constraints.DecimalMax.message","100"));
						}catch (Exception e){}
					}						
				}
			}else if(negocioBonoComision.getClavePorcentajeImporteBono()!= null){

			}
			if(negocioBonoComision.getPctAgenteBono()!= null && negocioBonoComision.getPctPromotorBono() != null){
				float val = negocioBonoComision.getPctAgenteBono().floatValue() + negocioBonoComision.getPctPromotorBono().floatValue();
				if(!(val - HUNDRED.floatValue() == 0)){
					addFieldError("negocioBonoComision.pctAgenteBono", getText("midas.negocio.validation.constraints.hundred"));
				}
			}
			

		}else{
			negocioBonoComision.setPctBono(null);
			negocioBonoComision.setImporteBono(null);
			negocioBonoComision.setPctAgenteBono(null);
			negocioBonoComision.setPctPromotorBono(null);
			negocioBonoComision.setComentariosBono(null);
			//negocioBonoComision.setClavePorcentajeImporteBono(false);
		}
		if (negocioBonoComision.getClaveDerechos()) {
			addErrors(negocioBonoComision, NegocioSesionDerechos.class, this,
					"negocioBonoComision");
			if(negocioBonoComision.getClavePorcentajeImporteDerechos()!= null && negocioBonoComision.getClavePorcentajeImporteDerechos()){
				if(negocioBonoComision.getPctDerechos() == null){
								
				}else{
					if(negocioBonoComision.getPctDerechos().floatValue() > 100){
						try{
							addFieldError("negocioBonoComision.pctDerechos", getText("javax.validation.constraints.DecimalMax.message","100"));
						}catch (Exception e){}
					}						
				}
			}else if(negocioBonoComision.getClavePorcentajeImporteDerechos()!= null){

			}			
			
			if(negocioBonoComision.getPctAgenteDerechos()!= null && negocioBonoComision.getPctPromotorDerechos() != null){
				float val = negocioBonoComision.getPctAgenteDerechos().floatValue() + negocioBonoComision.getPctPromotorDerechos().floatValue();
				if(!(val - HUNDRED.floatValue() == 0)){
					addFieldError("negocioBonoComision.pctAgenteDerechos", getText("midas.negocio.validation.constraints.hundred"));
				}
			}				
		}else{
			negocioBonoComision.setPctDerechos(null);
			negocioBonoComision.setImporteDerechos(null);
			negocioBonoComision.setPctAgenteDerechos(null);
			negocioBonoComision.setPctPromotorDerechos(null);
			negocioBonoComision.setComentariosDerechos(null);
		}
		
		if (negocioBonoComision.getClaveSobreComision()) {
			addErrors(negocioBonoComision, NegocioSobreComision.class, this,
					"negocioBonoComision");
			if(negocioBonoComision.getPctAgenteSobreComision()!= null && negocioBonoComision.getPctPromotorSobreComision() != null){
				float val = negocioBonoComision.getPctAgenteSobreComision().floatValue() + negocioBonoComision.getPctPromotorSobreComision().floatValue();
				if(!(val - HUNDRED.floatValue() == 0)){
					addFieldError("negocioBonoComision.pctAgenteSobreComision", getText("midas.negocio.validation.constraints.hundred"));
				}
			}			
		}else{
			negocioBonoComision.setPctSobreComision(null);
			negocioBonoComision.setPctAgenteSobreComision(null);
			negocioBonoComision.setPctPromotorSobreComision(null);
			negocioBonoComision.setComentariosSobreComision(null);
		}
		if (negocioBonoComision.getClaveUDI()) {
			addErrors(negocioBonoComision, NegocioUDI.class, this,
					"negocioBonoComision");
			if(negocioBonoComision.getClavePorcentajeImporteUDI()!= null && negocioBonoComision.getClavePorcentajeImporteUDI()){
				if(negocioBonoComision.getPctUDI() == null){
						
				}else{
					if(negocioBonoComision.getPctUDI().floatValue() > 100){
						try{
							addFieldError("negocioBonoComision.pctUDI", getText("javax.validation.constraints.DecimalMax.message","100"));
						}catch (Exception e){}
					}						
				}
			}else if(negocioBonoComision.getClavePorcentajeImporteUDI()!= null){
				if(negocioBonoComision.getImporteUDI() == null){
					
				}else{
					//TODO: implementar validacion de 16,2
				}
			}			
			
			if(negocioBonoComision.getPctAgenteUDI()!= null && negocioBonoComision.getPctPromotorUDI() != null){
				float val = negocioBonoComision.getPctAgenteUDI().floatValue() + negocioBonoComision.getPctPromotorUDI().floatValue();
				if(!(val - HUNDRED.floatValue() == 0)){
					addFieldError("negocioBonoComision.pctAgenteUDI", getText("midas.negocio.validation.constraints.hundred"));
				}
			}				
		}else{
			negocioBonoComision.setPctUDI(null);
			negocioBonoComision.setImporteUDI(null);
			negocioBonoComision.setPctAgenteUDI(null);
			negocioBonoComision.setPctPromotorUDI(null);
			negocioBonoComision.setComentariosUDI(null);
		}
		
		

		if(negocioBonoComision.getClaveBono()){
		  if(negocioBonoComision.getClavePorcentajeImporteBono()==null){
			 addFieldError("negocioBonoComision.clavePorcentajeImporteBono", getText("javax.validation.constraints.NotNull.message"));		
		  }	
		  if(negocioBonoComision.getClavePorcentajeImporteBono()){	
			 if(negocioBonoComision.getPctBono()==null) {
			   addFieldError("negocioBonoComision.pctBono", getText("javax.validation.constraints.NotNull.message"));	
		     }  
		  }
		  
		  if(!negocioBonoComision.getClavePorcentajeImporteBono()){	
			  if(negocioBonoComision.getImporteBono()==null) {
			    addFieldError("negocioBonoComision.importeBono", getText("javax.validation.constraints.NotNull.message"));
			  }
		  }		  
	    }
		
		if(negocioBonoComision.getClaveDerechos()){
			  if(negocioBonoComision.getClavePorcentajeImporteDerechos()==null){
				 addFieldError("negocioBonoComision.clavePorcentajeImporteDerechos", getText("javax.validation.constraints.NotNull.message"));		
			  }	
			  if(negocioBonoComision.getClavePorcentajeImporteDerechos()){	
			   if(negocioBonoComision.getPctDerechos()==null) {
				 addFieldError("negocioBonoComision.pctDerechos", getText("javax.validation.constraints.NotNull.message"));	
			   }
			  }
			  if(!negocioBonoComision.getClavePorcentajeImporteDerechos()){	
				  if(negocioBonoComision.getImporteDerechos()==null) {
				     addFieldError("negocioBonoComision.importeDerechos", getText("javax.validation.constraints.NotNull.message"));	
				} 
			 }		  
		 }
		
		
		if (negocioBonoComision.getClaveUDI()) {
		  if(negocioBonoComision.getClavePorcentajeImporteUDI()==null){
			 addFieldError("negocioBonoComision.clavePorcentajeImporteUDI", getText("javax.validation.constraints.NotNull.message"));		
		  }	
		  if(negocioBonoComision.getClavePorcentajeImporteUDI()){
			if(negocioBonoComision.getPctUDI()==null) {
			 addFieldError("negocioBonoComision.pctUDI", getText("javax.validation.constraints.NotNull.message"));	
		   }
		  }
			
		  if(!negocioBonoComision.getClavePorcentajeImporteUDI()){	
			  if(negocioBonoComision.getImporteUDI()==null) {  
			 addFieldError("negocioBonoComision.importeUDI", getText("javax.validation.constraints.NotNull.message"));	
		  }
		}
	   }	

		if (negocioBonoComision.getClaveBono()) {
			if(negocioBonoComision.getClavePorcentajeImporteBono()){
				negocioBonoComision.setImporteBono(null);	
			}
			if(!negocioBonoComision.getClavePorcentajeImporteBono()){
				negocioBonoComision.setPctBono(null);	
			}
		 } else{
			 negocioBonoComision.setClavePorcentajeImporteBono(false);	 
		 }
		
		
		if (negocioBonoComision.getClaveDerechos()) {
		  if(negocioBonoComision.getClavePorcentajeImporteDerechos()){
			  negocioBonoComision.setImporteDerechos(null);
			}
		  if(!negocioBonoComision.getClavePorcentajeImporteDerechos()){
			  negocioBonoComision.setPctDerechos(null);	
			}			
		 } else{
			 negocioBonoComision.setClavePorcentajeImporteDerechos(false);	 
		 }
			
		if (negocioBonoComision.getClaveUDI()) {
		  if(negocioBonoComision.getClavePorcentajeImporteUDI()){
			 negocioBonoComision.setImporteUDI(null);
		   }
			
		  if(!negocioBonoComision.getClavePorcentajeImporteUDI()){
			  negocioBonoComision.setPctUDI(null);	
		   }			
		 } else{
			 negocioBonoComision.setClavePorcentajeImporteUDI(false);	 
		 }
				
	}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,location="/jsp/negocio/bonocomision/bonocomision.jsp"),
			@Result(name=INPUT,location="/jsp/negocio/bonocomision/bonocomision.jsp")})	
	public String guardar() {
		
		if(this.idToNegocio != null){
			negocioBonoComision.setNegocio(entidadService.findById(Negocio.class, idToNegocio));
			entidadService.save(negocioBonoComision);
			setMensajeExito();
		}else{
			setMensajeError(getText("midas.componente.error.message"));
		}
		return SUCCESS;
	}
	
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public NegocioBonoComision getNegocioBonoComision() {
		return negocioBonoComision;
	}

	public void setNegocioBonoComision(NegocioBonoComision negocioBonoComision) {
		this.negocioBonoComision = negocioBonoComision;
	}
	@Autowired
	@Qualifier("entidadEJB")	
	public void setCatalogoService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}		

}
