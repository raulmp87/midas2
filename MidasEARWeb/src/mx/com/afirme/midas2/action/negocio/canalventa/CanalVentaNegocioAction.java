package mx.com.afirme.midas2.action.negocio.canalventa;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.negocio.canalventa.NegocioCanalVenta;
import mx.com.afirme.midas2.service.negocio.canalventa.CanalVentaNegocioService;

import com.opensymphony.xwork2.Preparable;



@Component
@Namespace(value = "/negocio/canalventa")
public class CanalVentaNegocioAction extends BaseAction implements Preparable{

	private List<NegocioCanalVenta> listaCampos;
	
	private Long idCampoEditado;
	
	private NegocioCanalVenta campoEditado;
	
	private Long idToNegocio;
	
	private Boolean activo;

	private InputStream stream;

	private String respuestaJason;
	
	private String idDiv;


	@Autowired
	@Qualifier("canalVentaNegocioServiceEJB")
	private CanalVentaNegocioService canalVentaNegocioService;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8463353350400651740L;

	public static final Logger LOGGER = Logger.getLogger(CanalVentaNegocioAction.class);

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Action(value = "mostrarContenedorCanalVenta", results = @Result(name = SUCCESS, location = "/jsp/negocio/canalventa/canalVentaNegocio.jsp"))
	public String mostrarContenedorCanalVenta() {
		//listaValores=canalVentaNegocioService.getCamposConfigurarPorCodigo(TIPO_CATALOGO.GPO_GPOS);
		return SUCCESS;
	}
	
	@Action(value = "obtenerCamposCotizador", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/canalventa/canalVentaNegocioCamposGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/canalventa/canalVentaNegocioCamposGrid.jsp")		
		})
	public String obtenerCamposCotizador(){
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		listaCampos=canalVentaNegocioService.getCamposPorTipoYNegocio(TIPO_CATALOGO.GPO_GPOS, idToNegocio);
		return SUCCESS;
	}
	
	@Action(value = "guardarVisibilidadCamposCotizador", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/canalventa/canalVentaNegocio.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/canalventa/canalVentaNegocio.jsp")		
		})
	public String guardarVisibilidadCamposCotizador(){
		LOGGER.debug("guardando..."+idCampoEditado);
		canalVentaNegocioService.saveCamposNegocio(idCampoEditado);
		return SUCCESS;
	}
	
	
	@Action(value = "guardarVisibilidadTodosCamposCotizador", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/canalventa/canalVentaNegocio.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/canalventa/canalVentaNegocio.jsp")		
		})
	public String guardarVisibilidadTodosCamposCotizador(){
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		LOGGER.debug("guardando..."+activo+"..."+idToNegocio);
		canalVentaNegocioService.saveAllCamposNegocio(activo, idToNegocio);
		return SUCCESS;
	}
	
	@Action(value = "obtenerCamposCotizadorJSON", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerCamposCotizadorJSON(){
		//idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		String json = canalVentaNegocioService.getCamposPorTipoYNegocioJSON(idDiv, idToNegocio);
		respuestaJason = json.toString();
		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;
		return SUCCESS;
	}
	
	
	public List<NegocioCanalVenta> getListaCampos() {
		return listaCampos;
	}
	public void setListaCampos(List<NegocioCanalVenta> listaCampos) {
		this.listaCampos = listaCampos;
	}
	public Long getIdToNegocio() {
		return idToNegocio;
	}
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	public Long getIdCampoEditado() {
		return idCampoEditado;
	}
	public void setIdCampoEditado(Long idCampoEditado) {
		this.idCampoEditado = idCampoEditado;
	}
	public NegocioCanalVenta getCampoEditado() {
		return campoEditado;
	}
	public void setCampoEditado(NegocioCanalVenta campoEditado) {
		this.campoEditado = campoEditado;
	}
	public Boolean getActivo() {
		return activo;
	}
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}
	public InputStream getStream() {
		return stream;
	}
	public void setStream(InputStream stream) {
		this.stream = stream;
	}
	public String getRespuestaJason() {
		return respuestaJason;
	}
	public void setRespuestaJason(String respuestaJason) {
		this.respuestaJason = respuestaJason;
	}
	public void setIdDiv(String idDiv) {
		this.idDiv = idDiv;
	}
	public String getIdDiv() {
		return idDiv;
	}
	
	
}
	