package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion.paquete;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.RelacionesNegocioPaqueteDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioPaqueteSeccionAction extends BaseAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5786735811540956078L;
	
	

	public String desplegarConfiguracion(){
		 return SUCCESS;
	}
	
	public String desplegarProductos(){
		return SUCCESS;
	}

	public String asociarProducto(){
		return SUCCESS;
	}

	public void prepareMostrar(){
		//Proviene de un hidden en la JSP
		if(getIdToNegProducto()!=null)
		//negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
		negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
	}

	public String mostrar(){
		negocioTipoPolizaList = negocioSeccionService.getNegTipoPolizaList(negocioProducto);
		negocioSeccionMap = new LinkedHashMap<BigDecimal, String>();
		return SUCCESS;
	}
	
	public void prepareObtenerRelaciones(){
		if(getIdToNegSeccion()!=null)
		negocioSeccion = entidadService.findById(NegocioSeccion.class, getIdToNegSeccion());
		
	}
	
	
	public String obtenerRelaciones(){
		if(negocioSeccion != null){
			setRelacionesNegocioPaqueteDTO(negocioPaqueteSeccionService.getRelationLists(negocioSeccion));
		}else{
			relacionesNegocioPaqueteDTO.setAsociadas(new ArrayList<NegocioPaqueteSeccion>(1));
			relacionesNegocioPaqueteDTO.setDisponibles(new ArrayList<NegocioPaqueteSeccion>(1));
		}
		return SUCCESS;
	}
	
	public void prepareRelacionarNegocioPaquete(){
		if(negocioPaqueteSeccion.getIdToNegPaqueteSeccion()!=null){
			negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, negocioPaqueteSeccion.getIdToNegPaqueteSeccion());
		}
		
	}
	
	public String relacionarNegocioPaquete(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		negocioPaqueteSeccionService.relacionarNegocioPaquete(accion, negocioPaqueteSeccion);
		return SUCCESS;
	}
	

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	private NegocioPaqueteSeccionService negocioPaqueteSeccionService;
	private EntidadService entidadService;
	private NegocioSeccionService negocioSeccionService;
	
	private Long idToNegProducto;
	private BigDecimal idToNegSeccion;
	private NegocioProducto negocioProducto;
	private List<NegocioTipoPoliza> negocioTipoPolizaList;
	private Map<BigDecimal, String>  negocioSeccionMap;
	private NegocioSeccion negocioSeccion;
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private RelacionesNegocioPaqueteDTO relacionesNegocioPaqueteDTO;

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}

	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setNegocioTipoPolizaList(
			List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}
	
	public Map<BigDecimal, String> getNegocioSeccionMap() {
		return negocioSeccionMap;
	}

	public void setNegocioSeccionMap(Map<BigDecimal, String> negocioSeccionMap) {
		this.negocioSeccionMap = negocioSeccionMap;
	}

	public RelacionesNegocioPaqueteDTO getRelacionesNegocioPaqueteDTO() {
		return relacionesNegocioPaqueteDTO;
	}

	public void setRelacionesNegocioPaqueteDTO(
			RelacionesNegocioPaqueteDTO relacionesNegocioPaqueteDTO) {
		this.relacionesNegocioPaqueteDTO = relacionesNegocioPaqueteDTO;
	}

	@Autowired
	@Qualifier("negocioPaqueteSeccionServiceEJB")
	public void setNegocioPaqueteSeccionService(NegocioPaqueteSeccionService negocioPaqueteSeccionService) {
		this.negocioPaqueteSeccionService = negocioPaqueteSeccionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(
			NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}

}