package mx.com.afirme.midas2.action.negocio.grupocliente;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cliente.CalifGrupoCliente;
import mx.com.afirme.midas2.domain.cliente.ClienteGrupoCliente;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.cliente.grupo.CalifGrupoClienteService;
import mx.com.afirme.midas2.service.cliente.grupo.GrupoClienteService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class GrupoClienteAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;
	
	private GrupoClienteService grupoClienteService;
	private CalifGrupoClienteService califGrupoClienteService;
	private String tipoAccion;

	private Long id;
	private GrupoCliente grupoCliente;
	private List<GrupoCliente> grupoClientes;
	private List<CalifGrupoCliente> califGrupoClientes;
	private List<Long> clienteIds = new ArrayList<Long>();
	
	private TipoAccionDTO catalogoTipoAccionDTO = new TipoAccionDTO();
		
	@Autowired
	public void setGrupoClienteService(GrupoClienteService grupoClienteService) {
		this.grupoClienteService = grupoClienteService;
	}
	
	@Autowired
	public void setCalifGrupoClienteService(
			CalifGrupoClienteService califGrupoClienteService) {
		this.califGrupoClienteService = califGrupoClienteService;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public GrupoCliente getGrupoCliente() {
		return grupoCliente;
	}
	
	public void setGrupoCliente(GrupoCliente grupoCliente) {
		this.grupoCliente = grupoCliente;
	}
	
	public List<GrupoCliente> getGrupoClientes() {
		return grupoClientes;
	}
	
	public void setGrupoClientes(List<GrupoCliente> grupoClientes) {
		this.grupoClientes = grupoClientes;
	}
	
	
	public List<CalifGrupoCliente> getCalifGrupoClientes() {
		if (califGrupoClientes == null) {
			califGrupoClientes = califGrupoClienteService.buscarTodos();
			Collections.sort(califGrupoClientes,
					new Comparator<CalifGrupoCliente>() {				
				public int compare(CalifGrupoCliente n1, CalifGrupoCliente n2){
					return n1.getId().compareTo(n2.getId());
				}
			});
		}
		return califGrupoClientes;
	}
	
	public void setCalifGrupoClientes(List<CalifGrupoCliente> califGrupoClientes) {
		this.califGrupoClientes = califGrupoClientes;
	}
	
	public List<Long> getClienteIds() {
		return clienteIds;
	}
	
	public void setClienteIds(List<Long> clienteIds) {
		this.clienteIds = clienteIds;
	}
	
	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}
	
	@Override
	public void prepare() throws Exception {
		if (id != null) {
			grupoCliente = grupoClienteService.buscarPorId(id);
		}
	}
		
	public String mostrar() {
		return SUCCESS;
	}
	
	public String listarGrupos(){
		return SUCCESS;
	}
	
	public void validateGuardar() {
		String domainObjectPrefix = "grupoCliente";
		if(grupoCliente.getClave() == null || grupoCliente.getClave().isEmpty()){
			grupoCliente.setClave("0");
		}
		addErrors(grupoCliente, EditItemChecks.class, this, domainObjectPrefix);
		//if (grupoClienteService.isDuplicado(grupoCliente)) {
		//	addFieldError(domainObjectPrefix + ".clave", getText("error.duplicado.campo"));
		//}
	}
	
	public String guardar() {
		populateGrupoCliente();
		grupoCliente =  grupoClienteService.guardar(grupoCliente);
		setId(grupoCliente.getId());
		setMensajeExitoPersonalizado(getText("midas.general.success.save"));
		return SUCCESS;
	}
	
	public String eliminar(){
		if (id != null) {
			grupoClienteService.eliminar(grupoCliente);
			super.setMensajeExito();
		}else{
			super.setMensajeError("No se encontro Grupo Cliente");
		}
		return SUCCESS;
	}
	
	/**
	 * Usado debido a que estos campos no son llenados por Struts.
	 */
	private void populateGrupoCliente() {
		List<ClienteGrupoCliente> clienteGrupoClientes = grupoCliente.getClienteGrupoClientes();
		List<ClienteGrupoCliente> clienteGrupoClientes2 = new ArrayList<ClienteGrupoCliente>();
		for (Long clienteId: clienteIds) {
			ClienteGrupoCliente clienteGrupoCliente = new ClienteGrupoCliente();
			clienteGrupoCliente.setIdTcCliente(clienteId);
			clienteGrupoCliente.setGrupoCliente(grupoCliente);
			clienteGrupoClientes2.add(clienteGrupoCliente);
			if (!clienteGrupoClientes.contains(clienteGrupoCliente)) {
				clienteGrupoClientes.add(clienteGrupoCliente);
			}
		}
		clienteGrupoClientes.retainAll(clienteGrupoClientes2);
	}

	public String mostrarClienteGruposDhtmlx() {
		grupoClientes = grupoClienteService.buscarPorCliente(id);
		return SUCCESS;
	}
	
	public String mostrarGruposClienteGrid(){
		grupoClientes = grupoClienteService.buscarTodos();
		return SUCCESS;
	}
	
	public String mostrarClientesDhtmlx() {
		return SUCCESS;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}
	
}
