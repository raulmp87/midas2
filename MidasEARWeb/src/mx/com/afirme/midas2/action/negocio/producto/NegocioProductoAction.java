package mx.com.afirme.midas2.action.negocio.producto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;


@Component
@Scope("prototype")
public class NegocioProductoAction extends CatalogoAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6487303164761176422L;
	private NegocioProductoService negocioProductoService;
	private NegocioService negocioService;
	private Long id;
	private String claveNegocio;
	private NegocioProducto negocioProducto;
	private Negocio negocio;
	
	private Long idNegocio;
	private Long idProducto;
	private String tabActiva;
	private String nivelActivo = "1";

	
	private List<ProductoDTO> productosDisponibles;
	private List<NegocioProducto> productosAsignados;
	private Long idToNegProducto;
	
	
	public String listarProductos(){
		productosDisponibles = negocioProductoService.listarProductosGroupBy(idNegocio);
		return NONE;
	}

	
	public String desplegarConfiguracion(){
		 return SUCCESS;
	}
	
	public String listarProductosAsociados(){
		if (idNegocio!=null){
			productosAsignados = negocioProductoService.obtenerNegociosRelacionados(idNegocio);
		}else{
			productosAsignados = new ArrayList<NegocioProducto>();
		}
		return SUCCESS;
	}
	
	public String mostrarAsignarProducto(){
		negocio = negocioService.findById(idNegocio);
		return SUCCESS;
	}

	/**
	 * Action usado para crear la estructura Negocio-Producto.
	 * @return
	 */
	public String asociarProducto(){
		try{
			negocioProductoService.agregarProducto(idNegocio, idProducto);
			super.setMensajeExitoPersonalizado("El registro se ha actualizado con \u00e9xito.");
		}catch(Exception ex){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
		}
		setTabActiva("detalle");
		setNivelActivo("1");
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		if (getId() != null) {
			negocioProducto = entidadService.findById(NegocioProducto.class, getId());
		}
	}
	
	@Override
	public String guardar() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public String eliminar() {
		negocioProductoService.deleteProducto(getIdToNegProducto());
		super.setMensajeExito();
		return SUCCESS;
	}

	
	
	@Override
	public String listar() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public String verDetalle() {
		return SUCCESS;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	@Autowired
	@Qualifier("negocioProductoServiceEJB")
	public void setNegocioProductoService(NegocioProductoService negocioProductoService) {
		this.negocioProductoService = negocioProductoService;
	}

	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}


	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public List<ProductoDTO> getProductosDisponibles() {
		return productosDisponibles;
	}

	public void setProductosDisponibles(List<ProductoDTO> productosDisponibles) {
		this.productosDisponibles = productosDisponibles;
	}

	public List<NegocioProducto> getProductosAsignados() {
		return productosAsignados;
	}

	public void setProductosAsignados(List<NegocioProducto> productosAsignados) {
		this.productosAsignados = productosAsignados;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}


	public Long getIdToNegProducto() {
		return idToNegProducto;
	}


	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}


	public String getClaveNegocio() {
		return claveNegocio;
	}


	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}


	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}


	public String getTabActiva() {
		return tabActiva;
	}


	public void setNivelActivo(String nivelActivo) {
		this.nivelActivo = nivelActivo;
	}


	public String getNivelActivo() {
		return nivelActivo;
	}

}
