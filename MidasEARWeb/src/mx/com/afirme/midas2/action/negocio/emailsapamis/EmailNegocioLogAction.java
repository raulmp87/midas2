package mx.com.afirme.midas2.action.negocio.emailsapamis;

import org.apache.struts2.convention.annotation.Namespace;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/negocio/sapamisemail/log")
public class EmailNegocioLogAction {

}
