package mx.com.afirme.midas2.action.negocio.producto.cobranza.formapago;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.dto.negocio.producto.formapago.RelacionesNegocioFormaPagoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.formapago.NegocioFormaPagoService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
@Component
@Scope("prototype")
public class NegocioFormaPagoAction extends CatalogoAction implements Preparable {

	private static final long serialVersionUID = 9026488777392912480L;

	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	public String mostrar(){
		return SUCCESS;		
	}
	
	public void prepareObtenerRelaciones(){
		negocioProducto = entidadService.findById(NegocioProducto.class, idToNegProducto);		
	}
	
	public String obtenerRelaciones(){
		if(negocioProducto != null){
			relacionesNegocioFormaPagoDTO = negocioFormaPagoService.getRelationLists(negocioProducto);
		}else{ 
			relacionesNegocioFormaPagoDTO.setAsociadas(new ArrayList<NegocioFormaPago>(1));
			relacionesNegocioFormaPagoDTO.setDisponibles(new ArrayList<NegocioFormaPago>(1));
		}
		return SUCCESS;
	}
	
	public String relacionarFormaPago(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		negocioFormaPagoService.relacionarNegocioFormaPago(accion, negocioFormaPago);
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String guardar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
	
	private List<NegocioFormaPago>negocioFormaPagoList= new ArrayList<NegocioFormaPago>();
	private NegocioFormaPago negocioFormaPago;
	private FormaPagoDTO formaPagoDTO;
	private Long idToNegProducto;
	private NegocioProducto negocioProducto;
	private RelacionesNegocioFormaPagoDTO relacionesNegocioFormaPagoDTO;
	
	private NegocioFormaPagoService negocioFormaPagoService;

	@Autowired
	@Qualifier("negocioFormaPagoServiceEJB")
	public void setNegocioFormaPagoService(
			NegocioFormaPagoService negocioFormaPagoService) {
		this.negocioFormaPagoService = negocioFormaPagoService;
	}

	public RelacionesNegocioFormaPagoDTO getRelacionesNegocioFormaPagoDTO() {
		return relacionesNegocioFormaPagoDTO;
	}

	public void setRelacionesNegocioFormaPagoDTO(
			RelacionesNegocioFormaPagoDTO relacionesNegocioFormaPagoDTO) {
		this.relacionesNegocioFormaPagoDTO = relacionesNegocioFormaPagoDTO;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public FormaPagoDTO getFormaPagoDTO() {
		return formaPagoDTO;
	}

	public void setFormaPagoDTO(FormaPagoDTO formaPagoDTO) {
		this.formaPagoDTO = formaPagoDTO;
	}

	public List<NegocioFormaPago> getNegocioFormaPagoList() {
		return negocioFormaPagoList;
	}

	public void setNegocioFormaPagoList(List<NegocioFormaPago> negocioFormaPagoList) {
		this.negocioFormaPagoList = negocioFormaPagoList;
	}

	public NegocioFormaPago getNegocioFormaPago() {
		return negocioFormaPago;
	}

	public void setNegocioFormaPago(NegocioFormaPago negocioFormaPago) {
		this.negocioFormaPago = negocioFormaPago;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
}
