package mx.com.afirme.midas2.action.negocio.antecedentes;

import java.util.Date;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.negocio.antecedentes.NegocioAntecedentesService;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAAnexos;
import mx.com.afirme.midas2.domain.negocio.antecedentes.NegocioCEAComentarios;
import mx.com.afirme.midas2.dto.negocio.antecedentes.PreparaListasNegocioAntecedentes;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioAntecedentesAction extends BaseAction implements Preparable{
	private static final long serialVersionUID = 1L;
	
	private NegocioAntecedentesService negocioAntecedentesService;
	private PreparaListasNegocioAntecedentes preparaListasNegocioAntecedentes = new PreparaListasNegocioAntecedentes();
	
	private NegocioCEAAnexos negocioCEAAnexos;
	private NegocioCEAComentarios negocioCEAComentarios;
	
	private String comentario;
	private String idToNegocio;
	
	@Override
	public void prepare() throws Exception {
	}

	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	public String obtenerAntecedentesAnexos(){
		String idToNegocioStr = ServletActionContext.getRequest().getParameter("idNegocio");
		Long idToNegocio = Long.valueOf(idToNegocioStr);
		preparaListasNegocioAntecedentes.setAnexos(negocioAntecedentesService.obtenerAntecedentesAnexos(idToNegocio));
		return SUCCESS;
	}
	
	public String obtenerAntecedentesComentarios(){
		String idToNegocioStr = ServletActionContext.getRequest().getParameter("idNegocio");		
		Long idToNegocio = Long.valueOf(idToNegocioStr);
		preparaListasNegocioAntecedentes.setComentarios(negocioAntecedentesService.obtenerAntecedentesComentarios(idToNegocio));
		return SUCCESS;
	}

	public String actualizarAntecedentesAnexos(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(negocioAntecedentesService.updateAnexo(accion, negocioCEAAnexos) != null){
			super.setMensajeExito();
		}else{
			super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String guardarAntecedentesComentarios(){
		try {
			Long idToNegocioL = Long.valueOf(idToNegocio);
			negocioCEAComentarios = new NegocioCEAComentarios();
			negocioCEAComentarios.setComentario(comentario);
			negocioCEAComentarios.setFechaCreacion(new Date());
			negocioCEAComentarios.setUsuarioNombre(usuarioService.getUsuarioActual().getNombreUsuario());
			negocioCEAComentarios.setUsuarioId(usuarioService.getUsuarioActual().getId().toString());
			negocioCEAComentarios.setIdToNegocio(idToNegocioL);
			negocioAntecedentesService.saveComentario(negocioCEAComentarios);
			super.setMensajeExito();
				
		} catch (Exception e) {
			LOG.error( "CENA guardarComentario: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}

	public NegocioCEAAnexos getNegocioCEAAnexos() {
		return negocioCEAAnexos;
	}

	public void setNegocioCEAAnexos(NegocioCEAAnexos negocioCEAAnexos) {
		this.negocioCEAAnexos = negocioCEAAnexos;
	}

	public NegocioCEAComentarios getNegocioCEAComentarios() {
		return negocioCEAComentarios;
	}

	public void setNegocioCEAComentarios(NegocioCEAComentarios negocioCEAComentarios) {
		this.negocioCEAComentarios = negocioCEAComentarios;
	}

	@Autowired
	@Qualifier("negocioAntecedentesServiceEJB")
	public void setNegocioAntecedentesService(
			NegocioAntecedentesService negocioAntecedentesService) {
		this.negocioAntecedentesService = negocioAntecedentesService;
	}
	
	public PreparaListasNegocioAntecedentes getPreparaListasNegocioAntecedentes() {
		return preparaListasNegocioAntecedentes;
	}

	public void setPreparaListasNegocioAntecedentes(
			PreparaListasNegocioAntecedentes preparaListasNegocioAntecedentes) {
		this.preparaListasNegocioAntecedentes = preparaListasNegocioAntecedentes;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(String idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
}
