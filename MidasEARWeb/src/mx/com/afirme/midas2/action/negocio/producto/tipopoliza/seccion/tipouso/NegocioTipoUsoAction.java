package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion.tipouso;

import java.math.BigDecimal;
import java.util.ArrayList;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.dto.negocio.seccion.tipouso.RelacionesNegocioTipoUsoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioTipoUsoAction extends BaseAction  implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToNegSeccion;
	private NegocioSeccion negocioSeccion;
	private NegocioTipoUso negocioTipoUso;
	private int claveDefault = 0;
	private RelacionesNegocioTipoUsoDTO relacionesNegocioTipoUsoDTO;
	
	private EntidadService entidadService;
	private NegocioTipoUsoService negocioTipoUsoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	
	@Autowired
	@Qualifier("negocioTipoUsoServiceEJB")
	public void setNegocioTipoUsoService(NegocioTipoUsoService negocioTipoUsoService) {
		this.negocioTipoUsoService = negocioTipoUsoService;
	}
	
	@Override
	public void prepare() throws Exception {
	}
	
	public void prepareMostrar(){
		
	}
	
	public String mostrar(){
		return SUCCESS;
	}
	
	public void prepareObtenerRelaciones(){
		negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
	}
	
	public String obtenerRelaciones(){
		if(negocioSeccion != null){
			relacionesNegocioTipoUsoDTO = negocioTipoUsoService.getRelationLists(negocioSeccion);
		}else{
			relacionesNegocioTipoUsoDTO.setAsociadas(new ArrayList<NegocioTipoUso>(1));
			relacionesNegocioTipoUsoDTO.setDisponibles(new ArrayList<NegocioTipoUso>(1));
		}
		return SUCCESS;
	}
	
	public String relacionarNegocioTipoUso(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(claveDefault == 1){
			negocioTipoUso.setClaveDefault(true);
		}else{
			negocioTipoUso.setClaveDefault(false);
		}
		negocioTipoUsoService.relacionarNegocioTipoUso(accion, negocioTipoUso);
		return SUCCESS;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setRelacionesNegocioTipoUsoDTO(
			RelacionesNegocioTipoUsoDTO relacionesNegocioTipoUsoDTO) {
		this.relacionesNegocioTipoUsoDTO = relacionesNegocioTipoUsoDTO;
	}

	public RelacionesNegocioTipoUsoDTO getRelacionesNegocioTipoUsoDTO() {
		return relacionesNegocioTipoUsoDTO;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioTipoUso(NegocioTipoUso negocioTipoUso) {
		this.negocioTipoUso = negocioTipoUso;
	}

	public NegocioTipoUso getNegocioTipoUso() {
		return negocioTipoUso;
	}

	public void setClaveDefault(int claveDefault) {
		this.claveDefault = claveDefault;
	}

	public int getClaveDefault() {
		return claveDefault;
	}

}
