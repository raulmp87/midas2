/**
 * 
 */
package mx.com.afirme.midas2.action.negocio.estadodescuento;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.estadodescuento.RelacionesNegocioEstadoDescuento;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioEstadoDescuentoAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private Negocio negocio;
	private EntidadService entidadService;

	private RespuestaGridRelacionDTO respuesta;
	private RelacionesNegocioEstadoDescuento relacionesNegocioEstadoDescuento;	
	private NegocioEstadoDescuentoService negocioEstadoDescuentoService;
	private EstadoDTO estadoDTO;
	private NegocioEstadoDescuento negocioEstadoDescuento;

		@Override
		public void prepare() throws Exception {
		
			if (negocio==null) negocio = new Negocio();
			  if(getId()!=null){
				  negocio = entidadService.findById(Negocio.class, getId());				
			  }	
		}
		
		public String obtenerRelaciones(){
			
			relacionesNegocioEstadoDescuento = negocioEstadoDescuentoService.getRelationLists(id);
			return SUCCESS;
		}
		
		
		
		public String relacionarNegocioEstadoDescuento(){
			String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
			if(negocioEstadoDescuentoService.relacionarNegocio(accion,negocioEstadoDescuento) != null){
				super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
			}else{
				super.setMensajeExito();
			}
			return SUCCESS;
		}
		
		public String guardar(){
			String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
			if(negocioEstadoDescuentoService.guardar(accion,negocioEstadoDescuento) != null){
				super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
			}else{
				super.setMensajeExito();
			}
			return SUCCESS;
		}

		public String mostrar(){
			return SUCCESS;
		}
		
		public String listar(){
			return SUCCESS;
		}



		public Negocio getNegocio() {
			return negocio;
		}
		
		
		public void setNegocio(Negocio negocio) {
			this.negocio = negocio;
		}
		
		
		public Long getId() {
			return id;
		}
		
		
		public void setId(Long id) {
			this.id = id;
		}
		
		
		public RespuestaGridRelacionDTO getRespuesta() {
			return respuesta;
		}
		
		
		public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
			this.respuesta = respuesta;
		}
		
		
		public RelacionesNegocioEstadoDescuento getRelacionesNegocioEstadoDescuento() {
			return relacionesNegocioEstadoDescuento;
		}
		
		
		public void setRelacionesNegocioEstadoDescuento(
				RelacionesNegocioEstadoDescuento relacionesNegocioEstadoDescuento) {
			this.relacionesNegocioEstadoDescuento = relacionesNegocioEstadoDescuento;
		}
		
		
		public EstadoDTO getEstadoDTO() {
			return estadoDTO;
		}
		
		
		public void setEstadoDTO(EstadoDTO estadoDTO) {
			this.estadoDTO = estadoDTO;
		}
		
		public NegocioEstadoDescuento getNegocioEstadoDescuento() {
			return negocioEstadoDescuento;
		}
		
		
		public void setNegocioEstadoDescuento(NegocioEstadoDescuento negocioEstadoDescuento) {
			this.negocioEstadoDescuento = negocioEstadoDescuento;
		}


		@Autowired
		@Qualifier("negocioEstadoDescuentoServiceEJB")
		public void setNegocioEstadoDescuentoService(
				NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
			this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
		}
		
		@Autowired
		@Qualifier("entidadEJB")
		public void setEntidadService(EntidadService entidadService) {
			this.entidadService = entidadService;
		}
}
