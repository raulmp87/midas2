package mx.com.afirme.midas2.action.negocio.ligaagente;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.ligaagente.ConfiguracionLigaAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.ConfigLigaAgenteDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.MonitoreoLigaAgenteDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.ligaagente.ConfiguracionLigaAgenteService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/negocio/ligaAgente")
public class ConfiguracionLigaAgenteAction extends BaseAction implements Preparable, ServletRequestAware {

	private static final long serialVersionUID = -7431223319465775816L;
	
	private static final String	LOCATION_CONTENEDOR_CONFIG_LIGASAGENTES_JSP	= "/jsp/negocio/ligasdeagente/contenedorConfigLigasAgentes.jsp";
	private static final String	LOCATION_LISTADO_CONFIG_LIGASAGENTES_JSP	= "/jsp/negocio/ligasdeagente/listadoConfigLigasAgentesGrid.jsp";
	
	private static final String LOCATION_CONTENEDOR_MONITOR_LIGASAGENTES_JSP = "/jsp/negocio/ligasdeagente/contenedorMonitorLigasAgentes.jsp";
	private static final String	LOCATION_LISTADO_MONITOR_LIGASAGENTES_JSP	= "/jsp/negocio/ligasdeagente/listadoMonitorLigasAgentesGrid.jsp";
	
	private static final String LOCATION_CONTENDOR_REGISTRO_LIGASAGENTES_JSP = "/jsp/negocio/ligasdeagente/registroLigaDeAgente.jsp";
	private static final String LOCATION_LISTADO_AGENTES_LIGASAGENTES_JSP = "/jsp/negocio/ligasdeagente/listadoAgentesLigasAgentes.jsp";
	
	private static final String[] COLUMNAS_GRID_MONITOREO_LIGAS = 
		{"id", "nombre", "idAgente", "nombreAgente", "descripcionNegocio", "idCotizacion", "fechaCotizacion", "prospectoCotizacion", 
		"telefonoProspecto", "correoProspecto", "producto", "tipoPoliza", "lineaNegocio", "marca", "modelo", "descripcionFinalVehiculo",
		"numeroSerieVehiculo", "primaNeta", "primaTotal", "numeroPoliza", "fechaEmision"};
	
	private Map<String,String> listaEstatus;
	private List<Negocio> negocios;
	private List<AgenteDTO> agentesDTO;
	private List<AgenteView> agentes;
	private String emailAgente;
	private String telefonoAgente;
	
	private List<ConfigLigaAgenteDTO> configuracionesLigasAgente;
	private ConfigLigaAgenteDTO configLigaFiltro;
	
	private List<MonitoreoLigaAgenteDTO> monitoreoLigasAgentes;
	private MonitoreoLigaAgenteDTO monitorLigaFiltro;
	private Long numeroCotizaciones = 0L;
	private Long numeroPolizas = 0L;
	
	private ConfiguracionLigaAgente configuracion;
	private List<Negocio> negociosAgente;
	private boolean esConsulta;
	private String descripcionAgente;
	
	private boolean mostrarListadoVacio;
	private TransporteImpresionDTO transporte;
	
	private HttpServletRequest request;
	

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("configuracionLigaAgenteServiceEJB")
	private ConfiguracionLigaAgenteService configuracionLigaAgenteService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	private NegocioService negocioService;
	
	@Autowired
	@Qualifier("cotizadorAgentesService")
	private CotizadorAgentesService cotizadorAgentesService;
	
	@Override
	public void prepare() throws Exception {
		
	}
	
	//BANDEJA DE CONFIGURACIONES ------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------
	
	public void prepareMostrarContenedorLigasAgentes(){
		listaEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.LIGA_ESTATUS);
		negocios = negocioService.listarNegociosNoBorrados(Negocio.CLAVE_NEGOCIO_AUTOS);
	}
	
	@Action(value = "mostrarContenedorLigasAgentes", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDOR_CONFIG_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_CONFIG_LIGASAGENTES_JSP)})
	public String mostrarContenedorLigasAgentes(){
		return SUCCESS;
	}
	
	@Action(value = "listarLigasAgentes", results = {
			@Result(name = SUCCESS, location = LOCATION_LISTADO_CONFIG_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_CONFIG_LIGASAGENTES_JSP)})
	public String listarLigasAgentes(){
		if(mostrarListadoVacio){
			configuracionesLigasAgente = new ArrayList<ConfigLigaAgenteDTO>();
		}else{
			configuracionesLigasAgente = configuracionLigaAgenteService.buscarConfiguracionesLigasAgentes(configLigaFiltro);
		}
		return SUCCESS;
	}
	
	@Action(value="exportarLigasAgentes",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarLigasAgentes(){
		
		configuracionesLigasAgente = configuracionLigaAgenteService.buscarConfiguracionesLigasAgentes(configLigaFiltro);
		ExcelExporter exporter = new ExcelExporter(ConfigLigaAgenteDTO .class);
		transporte = exporter.exportXLS(configuracionesLigasAgente, "Listado de Ligas de Agente");
		
		return SUCCESS;
	}
	
	//MONITOREO DE CONFIGURACIONES --------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------
	
	public void prepareMostrarContenedorMonitorLigas(){
		negocios = negocioService.listarNegociosNoBorrados(Negocio.CLAVE_NEGOCIO_AUTOS);
	}
	
	@Action(value = "mostrarContenedorMonitorLigas", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDOR_MONITOR_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_MONITOR_LIGASAGENTES_JSP)})
	public String mostrarContenedorMonitorLigas(){
		return SUCCESS;
	}
	
	@Action(value = "listarMonitorLigasAgentes", results = {
			@Result(name = SUCCESS, location = LOCATION_LISTADO_MONITOR_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_MONITOR_LIGASAGENTES_JSP)})
	public String listarMonitorLigasAgentes(){
		if(mostrarListadoVacio){
			monitoreoLigasAgentes = new ArrayList<MonitoreoLigaAgenteDTO>();
		}else{
			if(!configuracionLigaAgenteService.tienePermisoMonitorearTodosLosAgentes()){
				Agente agenteActual = usuarioService.getAgenteUsuarioActual();
				if(agenteActual != null){
					monitorLigaFiltro.setIdAgente(agenteActual.getId());
				}else{
					monitorLigaFiltro.setIdAgente(-1L);
				}
			}
			
			prepareBusquedaPaginada();	
			
			monitoreoLigasAgentes = configuracionLigaAgenteService.buscarMonitoreoLigasAgentesPaginado(monitorLigaFiltro);
			
			MonitoreoLigaAgenteDTO contadores = configuracionLigaAgenteService.contarResultadoMonitoreoLigasAgentes(monitorLigaFiltro); 
			
			if(super.getPosStart().intValue() == 0){
				super.setTotalCount(contadores.getContadorTotal());
			}
			
			cargarNumeroPolizasYCotizaciones(contadores);
		}
		return SUCCESS;
	}
	
	private void prepareBusquedaPaginada(){
		if(super.getCount() == null){
			super.setCount(20); 
		}
					
		if(super.getPosStart() == null){
			super.setPosStart(0);
		}
		
		monitorLigaFiltro.setPosStart(super.getPosStart());
		monitorLigaFiltro.setCount(super.getCount());
		monitorLigaFiltro.setOrderByAttribute(decodeOrderBy());
	}
	
	private String decodeOrderBy(){
		String order = null;
		if(!StringUtil.isEmpty(super.getOrderBy())){
			order = COLUMNAS_GRID_MONITOREO_LIGAS[Integer.valueOf(super.getOrderBy())];
			if(!StringUtil.isEmpty(super.getDirect())){
				order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
			}
		}
		return order;
	}
	
	/**
	 * Metodo para cargar el numero de cotizaciones y el numero de polizas que hay como resultado de la busqueda
	 * @param monitorLigaFiltro
	 * @param resultadoBusqueda
	 */
	private void cargarNumeroPolizasYCotizaciones(MonitoreoLigaAgenteDTO contadores){
		numeroPolizas = contadores.getContadorPolizas();
		numeroCotizaciones = contadores.getContadorCotizaciones();
	}
	
	@Action(value="exportarMonitorLigasAgentes",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarMonitorLigasAgentes(){
		
		monitoreoLigasAgentes = configuracionLigaAgenteService.buscarMonitoreoLigasAgentes(monitorLigaFiltro);
		ExcelExporter exporter = new ExcelExporter(MonitoreoLigaAgenteDTO .class);
		transporte = exporter.exportXLS(monitoreoLigasAgentes, "Monitoreo de Ligas de Agente");
		
		return SUCCESS;
	}
	
	
	//REGISTRAR CONFIGURACIONES DE LIGAS DE AGENTES ---------------------------------------------------
	//-------------------------------------------------------------------------------------------------
	
	public void prepareMostrarRegistro(){
		if(configuracion != null
				&& configuracion.getId() != null){
			configuracion = entidadService.findById(ConfiguracionLigaAgente.class, configuracion.getId());
		}
		this.listaEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.LIGA_ESTATUS);
		
		if(configuracion != null 
				&& configuracion.getAgente() != null
				&& configuracion.getAgente().getId() != null){
			//En caso de que todavia no se haya guardado el registro, se carga la informacion del agente
			if(configuracion.getId() == null){
				Agente agente = entidadService.findById(Agente.class, configuracion.getAgente().getId());
				configuracion.setAgente(agente);
			}
			negociosAgente = cotizadorAgentesService.getNegociosPorAgente(configuracion.getAgente().getId().intValue(), Negocio.CLAVE_NEGOCIO_AUTOS);
		}else{
			negociosAgente = new ArrayList<Negocio>();
		}
	}
	
	@Action(value = "mostrarRegistro", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENDOR_REGISTRO_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENDOR_REGISTRO_LIGASAGENTES_JSP)})
	public String mostrarRegistro() {
		
		return SUCCESS;	
	}
	
	@Action(value = "guardarConfiguracionLigaAgente", results = { 
			@Result(name = INPUT, location = LOCATION_CONTENDOR_REGISTRO_LIGASAGENTES_JSP) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarRegistro", "namespace", "/negocio/ligaAgente",
					"configuracion.id", "${configuracion.id}", "esConsulta", "${esConsulta}",	 
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
					})		
	public String guardarConfiguracionLigaAgente(){
		try{
			configuracion = configuracionLigaAgenteService.guardarConfiguracionLigaAgente(configuracion);
		}catch(NegocioEJBExeption ne){
			prepareMostrarRegistro();
			setMensajeError(((NegocioEJBExeption)ne).getMessageClean());
			LOG.error("Error al guardar la configuraci\u00F3n de la liga de agente", ne);
			return INPUT;
		}catch(Exception e){
			prepareMostrarRegistro();
			setMensajeError("Error al guardar la configuraci\u00F3n de la liga de agente");
			LOG.error("Error al guardar la configuraci\u00F3n de la liga de agente", e);
			return INPUT;
		}
		setMensaje("Guardado exitoso");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	@Action(value = "autocompletarBusquedaAgentes", results = {
			@Result(name = SUCCESS, location = LOCATION_LISTADO_AGENTES_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_LISTADO_AGENTES_LIGASAGENTES_JSP)})
	public String autocompletarBusquedaAgentes(){
		try{
			this.agentes = listadoService.listaAgentesPorDescripcionLightWeightAutorizados(descripcionAgente);
		}catch(Exception e){
			LOG.error("Error al buscar", e);
			this.agentes = new ArrayList<AgenteView>();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "cargarInformacionContactoAgente", results = {
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^emailAgente,^telefonoAgente"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^emailAgente,^telefonoAgente"}) })
	public String cargarInformacionContactoAgente(){
		try{
			if(configuracion != null
					&& configuracion.getAgente() != null
					&& configuracion.getAgente().getId() != null){
				Agente agente = entidadService.findById(Agente.class, configuracion.getAgente().getId());
				emailAgente = agente.getPersona().getEmail();
				telefonoAgente = agente.getPersona().getTelefonoCelular();
			}
		}catch(Exception ex){
			LOG.error("Error al recuperar la informacion de contacto", ex);
			this.agentes = new ArrayList<AgenteView>();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "generarLigaAgente", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENDOR_REGISTRO_LIGASAGENTES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENDOR_REGISTRO_LIGASAGENTES_JSP)})
	public String generarLigaAgente(){
		try{
			configuracion = configuracionLigaAgenteService.generarLigaAgente(configuracion.getId(),request.getRemoteHost());
		}catch(NegocioEJBExeption ne){
			prepareMostrarRegistro();
			setMensajeError(((NegocioEJBExeption)ne).getMessageClean());
			LOG.error("Error al generar la liga de agente", ne);
			return INPUT;
		}catch(Exception e){
			prepareMostrarRegistro();
			LOG.error("Error al generar la liga de agente", e);
			setMensaje("Error al generar la liga de agente");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		prepareMostrarRegistro();
		setMensaje("Liga de Agente Generada");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	@Action(value = "enviarLigaAgente", results = {
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje"}) })
	public String enviarLigaAgente(){
		try{
			configuracionLigaAgenteService.enviarLigaAgente(configuracion.getId());
		}catch(Exception e){
			LOG.error("Error al enviar el correo", e);
			setMensaje("Error al enviar el correo");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		setMensaje("Correo Enviado Exitosamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ConfiguracionLigaAgenteService getConfiguracionLigaAgenteService() {
		return configuracionLigaAgenteService;
	}

	public void setConfiguracionLigaAgenteService(
			ConfiguracionLigaAgenteService configuracionLigaAgenteService) {
		this.configuracionLigaAgenteService = configuracionLigaAgenteService;
	}

	public Map<String, String> getListaEstatus() {
		return listaEstatus;
	}

	public void setListaEstatus(Map<String, String> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	public boolean isMostrarListadoVacio() {
		return mostrarListadoVacio;
	}

	public void setMostrarListadoVacio(boolean mostrarListadoVacio) {
		this.mostrarListadoVacio = mostrarListadoVacio;
	}

	public List<Negocio> getNegocios() {
		return negocios;
	}

	public void setNegocios(List<Negocio> negocios) {
		this.negocios = negocios;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public List<ConfigLigaAgenteDTO> getConfiguracionesLigasAgente() {
		return configuracionesLigasAgente;
	}

	public void setConfiguracionesLigasAgente(
			List<ConfigLigaAgenteDTO> configuracionesLigasAgente) {
		this.configuracionesLigasAgente = configuracionesLigasAgente;
	}

	public ConfigLigaAgenteDTO getConfigLigaFiltro() {
		return configLigaFiltro;
	}

	public void setConfigLigaFiltro(ConfigLigaAgenteDTO configLigaFiltro) {
		this.configLigaFiltro = configLigaFiltro;
	}

	public List<MonitoreoLigaAgenteDTO> getMonitoreoLigasAgentes() {
		return monitoreoLigasAgentes;
	}

	public void setMonitoreoLigasAgentes(
			List<MonitoreoLigaAgenteDTO> monitoreoLigasAgentes) {
		this.monitoreoLigasAgentes = monitoreoLigasAgentes;
	}

	public MonitoreoLigaAgenteDTO getMonitorLigaFiltro() {
		return monitorLigaFiltro;
	}

	public void setMonitorLigaFiltro(MonitoreoLigaAgenteDTO monitorLigaFiltro) {
		this.monitorLigaFiltro = monitorLigaFiltro;
	}

	public ConfiguracionLigaAgente getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfiguracionLigaAgente configuracion) {
		this.configuracion = configuracion;
	}

	public List<AgenteDTO> getAgentesDTO() {
		return agentesDTO;
	}

	public void setAgentesDTO(List<AgenteDTO> agentesDTO) {
		this.agentesDTO = agentesDTO;
	}

	public List<Negocio> getNegociosAgente() {
		return negociosAgente;
	}

	public void setNegociosAgente(List<Negocio> negociosAgente) {
		this.negociosAgente = negociosAgente;
	}

	public boolean isEsConsulta() {
		return esConsulta;
	}
	
	public void setEsConsulta(boolean esConsulta) {
		this.esConsulta = esConsulta;
	}

	public List<AgenteView> getAgentes() {
		return agentes;
	}

	public void setAgentes(List<AgenteView> agentes) {
		this.agentes = agentes;
	}

	public String getDescripcionAgente() {
		return descripcionAgente;
	}

	public void setDescripcionAgente(String descripcionAgente) {
		this.descripcionAgente = descripcionAgente;
	}

	public Long getNumeroCotizaciones() {
		return numeroCotizaciones;
	}

	public void setNumeroCotizaciones(Long numeroCotizaciones) {
		this.numeroCotizaciones = numeroCotizaciones;
	}

	public Long getNumeroPolizas() {
		return numeroPolizas;
	}

	public void setNumeroPolizas(Long numeroPolizas) {
		this.numeroPolizas = numeroPolizas;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getEmailAgente() {
		return emailAgente;
	}

	public void setEmailAgente(String emailAgente) {
		this.emailAgente = emailAgente;
	}

	public String getTelefonoAgente() {
		return telefonoAgente;
	}

	public void setTelefonoAgente(String telefonoAgente) {
		this.telefonoAgente = telefonoAgente;
	}	
	
}
