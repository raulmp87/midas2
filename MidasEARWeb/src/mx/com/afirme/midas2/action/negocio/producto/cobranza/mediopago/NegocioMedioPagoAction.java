package mx.com.afirme.midas2.action.negocio.producto.cobranza.mediopago;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.dto.negocio.producto.mediopago.RelacionesNegocioMedioPagoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.mediopago.NegocioMedioPagoService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
@Component
@Scope("prototype")
public class NegocioMedioPagoAction extends CatalogoAction implements Preparable {

	private static final long serialVersionUID = 9026488777392912480L;

	public String mostrar(){
		return SUCCESS;		
	}
	
	public void prepareObtenerRelaciones(){
		negocioProducto = entidadService.findById(NegocioProducto.class, idToNegProducto);		
	}
	
	public String obtenerRelaciones(){
		if(negocioProducto != null){
			relacionesNegocioMedioPagoDTO = negocioMedioPagoService.getRelationLists(negocioProducto);
		}else{
			relacionesNegocioMedioPagoDTO.setAsociadas(new ArrayList<NegocioMedioPago>(1));
			relacionesNegocioMedioPagoDTO.setDisponibles(new ArrayList<NegocioMedioPago>(1));
		}
		return SUCCESS;
	}
	
	public String relacionarMedioPago(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		negocioMedioPagoService.relacionarNegocioMedioPago(accion, negocioMedioPago);
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String guardar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
	
	private List<NegocioMedioPago>negocioMedioPagoList= new ArrayList<NegocioMedioPago>();
	private NegocioMedioPago negocioMedioPago;
	private MedioPagoDTO medioPagoDTO;
	private Long idToNegProducto;
	private NegocioProducto negocioProducto;
	private RelacionesNegocioMedioPagoDTO relacionesNegocioMedioPagoDTO;
	
	private NegocioMedioPagoService negocioMedioPagoService;

	@Autowired
	@Qualifier("negocioMedioPagoServiceEJB")
	public void setNegocioMedioPagoService(
			NegocioMedioPagoService negocioMedioPagoService) {
		this.negocioMedioPagoService = negocioMedioPagoService;
	}

	public RelacionesNegocioMedioPagoDTO getRelacionesNegocioMedioPagoDTO() {
		return relacionesNegocioMedioPagoDTO;
	}

	public void setRelacionesNegocioMedioPagoDTO(
			RelacionesNegocioMedioPagoDTO relacionesNegocioMedioPagoDTO) {
		this.relacionesNegocioMedioPagoDTO = relacionesNegocioMedioPagoDTO;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public MedioPagoDTO getMedioPagoDTO() {
		return medioPagoDTO;
	}

	public void setMedioPagoDTO(MedioPagoDTO medioPagoDTO) {
		this.medioPagoDTO = medioPagoDTO;
	}

	public List<NegocioMedioPago> getNegocioMedioPagoList() {
		return negocioMedioPagoList;
	}

	public void setNegocioMedioPagoList(List<NegocioMedioPago> negocioMedioPagoList) {
		this.negocioMedioPagoList = negocioMedioPagoList;
	}

	public NegocioMedioPago getNegocioMedioPago() {
		return negocioMedioPago;
	}

	public void setNegocioMedioPago(NegocioMedioPago negocioMedioPago) {
		this.negocioMedioPago = negocioMedioPago;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
}
