package mx.com.afirme.midas2.action.negocio.recuotificacion;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutProgPago;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutRecibo;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica.TipoVigencia;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionUsuario;
import mx.com.afirme.midas2.dto.negocio.recuotificacion.ProgramaPagoVO;
import mx.com.afirme.midas2.dto.negocio.recuotificacion.ReciboVO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService;
import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService.TipoSaldo;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.Preparable;


@Component
@Namespace(value = "/negocio/recuotificacion")
public class NegocioRecuotificacionAction extends BaseAction implements Preparable{

	public static final Logger LOGGER = Logger.getLogger(NegocioRecuotificacionAction.class);
	
	private static final long serialVersionUID = 7226735640277347435L;
	
	/**
	 * Constantes mensajes
	 */
	
	private static final String LABEL_ACTIVA = "midas.negocio.recuotificacion.activada";
	
	private static final String LABEL_INACTIVA = "midas.negocio.recuotificacion.inactivada";
	
	private static final String LABEL_PRIMA_MODIFICADA = "midas.negocio.recuotificacion.primatotal.modificada";
	
	private static final String RECUOTIFICACION_AUTOMATICA =  "Recuotificaci\u00F3n Autom\u00E1tica";
	
	private static final String RECUOTIFICACION_MANUAL =  "Recuotificaci\u00F3n Manual";
	
	private static final String ACTIVACION_PRIMA_TOTAL =  "La Modificaci\u00F3n de Valor M\u00E1ximo de Prima Total";
	
	/**
	 * Negocio Recuotificacion
	 */
	
	private Long idToNegocio;
	
	private NegocioRecuotificacion recuotificacion;
	
	private NegocioRecuotificacionUsuario usuario;
	
	private String valor;
	
	private String primaTotal;
	
	private List<NegocioRecuotificacionUsuario> usuariosAsociados = new ArrayList<NegocioRecuotificacionUsuario>();
	
	private List<NegocioRecuotificacionUsuario> usuariosDisponibles = new ArrayList<NegocioRecuotificacionUsuario>();
	
	private Long recuotificacionId;
	
	/**
	 * Versiones
	 */
	
	private Long versionId;
	
	private NegocioRecuotificacionAutomatica automatica;
	
	private Map<Long, Integer> versiones = new LinkedHashMap<Long, Integer>();
	
	private List<NegocioRecuotificacionAutProgPago> programas = new ArrayList<NegocioRecuotificacionAutProgPago>();
	
	private Map<String, String> tipoVigencias = new LinkedHashMap<String, String>();
	
	
	/**
	 * Recibos
	 */
	
	private NegocioRecuotificacionAutProgPago programa;
	
	private List<NegocioRecuotificacionAutRecibo> recibos = new ArrayList<NegocioRecuotificacionAutRecibo>();

	private Long reciboId;
	
	/**
	 * Comunicacion json
	 */
	
	private String json;
	
	private Double saldo;
	
	private String tipoSaldo;
	
	private Long progPagoId;
	
	private String status;
	
	private String tipoVigencia;
	
	
	/**
	 * EJBs
	 */
	
	@Autowired
	@Qualifier("negocioRecuotificacionEJB")
	private NegocioRecuotificacionService negocioRecuotificacionService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")	
	private ListadoService listadoService;
	
	
	@Override
	public void prepare() throws Exception {
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
	}
	
	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp"))
	public String mostrarContenedor() {
		recuotificacion = negocioRecuotificacionService.obtener(idToNegocio);
		return SUCCESS;
	}
	
	@Action(value = "activarAutomatica", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,activar,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,activar,mensaje"})
		})
	public String activarAutomatica() {		
		try{
			boolean activar = !Boolean.valueOf(this.valor);
			negocioRecuotificacionService.activarRecuotificacionAutomatica(recuotificacion.getId(), activar);
			if(activar){
				super.setMensaje(super.getText(LABEL_ACTIVA, new String[]{RECUOTIFICACION_AUTOMATICA}));
			}else{
				super.setMensaje(super.getText(LABEL_INACTIVA, new String[]{RECUOTIFICACION_AUTOMATICA}));
			}
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}
		return SUCCESS;
	}
	
	@Action(value = "activarManual", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,activar,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,activar,mensaje"})
		})
	public String activarManual() {
		try{
			boolean activar = !Boolean.valueOf(this.valor);
			negocioRecuotificacionService.activarRecuotificacionManual(recuotificacion.getId(), activar);
			if(activar){
				super.setMensaje(super.getText(LABEL_ACTIVA, new String[]{ RECUOTIFICACION_MANUAL}));
			}else{
				super.setMensaje(super.getText(LABEL_INACTIVA, new String[]{ RECUOTIFICACION_MANUAL}));
			}
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}		
		return SUCCESS;
	}
	
	@Action(value = "activarPrimaTotal", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,activar,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,activar,mensaje"})
		})
	public String activarPrimaTotal() {
		try{
			boolean activar = !Boolean.valueOf(this.valor);
			negocioRecuotificacionService.activarRecuotificacionPrimaTotal(recuotificacion.getId(), activar);
			if(activar){
				super.setMensaje(super.getText(LABEL_ACTIVA, new String[]{ACTIVACION_PRIMA_TOTAL}));
			}else{
				super.setMensaje(super.getText(LABEL_INACTIVA, new String[]{ACTIVACION_PRIMA_TOTAL}));
			}
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}		
		return SUCCESS;
	}
	
	@Action(value = "modificarPrimaTotal", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,primaTotal,activar,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,primaTotal,activar,mensaje"})
		})
	public String modificarPrimaTotal() {
		try{
			negocioRecuotificacionService.modificarRecuotificacionPrimaTotal(recuotificacion.getId(), new BigDecimal(valor));
			super.setMensaje(super.getText(LABEL_PRIMA_MODIFICADA));
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}		
		return SUCCESS;	
	}

	/**
	 * Método que lista los usuarios en el dhtmlxGrid de
	 * Disponibles.
	 */
	@Action(value = "obtenerUsuariosDisponibles", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecuotificacionUsuarioDisponibleGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecuotificacionUsuarioDisponibleGrid.jsp")		
		})		
	public String obtenerUsuariosDisponibles() {		
		usuariosDisponibles = negocioRecuotificacionService.obtenerUsuariosDisponibles(idToNegocio);
		return SUCCESS;
	}
	
	/**
	 * Método que lista los usuarios en el dhtmlxGrid de
	 * Asociados.
	 */
	@Action(value = "obtenerUsuariosAsociados", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecuotificacionUsuarioAsociadoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecuotificacionUsuarioAsociadoGrid.jsp")		
		})		
	public String obtenerUsuariosAsociados() {		
		usuariosAsociados = negocioRecuotificacionService.obtenerUsuariosAsociados(idToNegocio);
		return SUCCESS;
	}
	
	/**
	 * Action encargado de asignar permisos a todos los usuarios de la lista.
	 */
	@Action(value = "asociarTodos", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp")		
		})
	public String asociarTodos() {
		negocioRecuotificacionService.asociarTodos(idToNegocio);
		return SUCCESS;
	}

	/**
	 * Action encargado de retirar todos los permisos del negocio.
	 */
	@Action(value = "desasociarTodos", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp")		
		})
	public String desasociarTodos() {
		negocioRecuotificacionService.desasociarTodos(idToNegocio);
		return SUCCESS;
	}
	
	/**
	 * Método que utiliza el dhtmlxProcessor para la asociación del usuario.
	 * Invoca el metodo de relacionar de negocioUsuarioImpresionEdicionService
	 */
	@Action(value = "relacionarUsuario", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecuotificacion.jsp")		
		})
	public String relacionarUsuario() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		negocioRecuotificacionService.relacionar(accion, usuario);		
		return SUCCESS;
	}
		
	private void prepareMostrar(){
		versiones = listadoService.obtenerListadoVersionesRecuotificacionNegocio(idToNegocio);
		if(versiones.isEmpty()){
			versiones.put(1L, 1);
		}
		tipoVigencias = EnumUtil.getMap(NegocioRecuotificacionAutomatica.TipoVigencia.class);
	}
	
	@Action(value = "mostrarNegRecVersion", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecVersion.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecVersion.jsp")		
		})
	public String mostrarNegRecVersion(){	
		automatica = negocioRecuotificacionService.obtenerVersionActiva(idToNegocio);
		prepareMostrar();
		return SUCCESS;
	}
	
	@Action(value = "mostrarVersion", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecVersion.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecVersion.jsp")		
		})				
	public String mostrarVersion(){
		prepareMostrar();
		automatica = negocioRecuotificacionService.obtenerVersion(versionId);
		return SUCCESS;
	}
		
	@Action(value = "activarVersion", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarVersion", "namespace",
					"/negocio/recuotificacion",
					"versionId", "${versionId}",
					"idToNegocio", "${idToNegocio}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecVersion.jsp")		
		})
	public String activarVersion(){
		try{			
			negocioRecuotificacionService.activarVersion(versionId);		
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(ex.getMessage());
			prepareMostrar();
			automatica = negocioRecuotificacionService.obtenerVersion(versionId);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "nuevaVersion", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarVersion", "namespace",
					"/negocio/recuotificacion",
					"versionId", "${versionId}",
					"idToNegocio", "${idToNegocio}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecVersion.jsp")		
		})
	public String nuevaVersion(){
		try{
			automatica = negocioRecuotificacionService.nuevaVersion(idToNegocio);	
			versionId = automatica.getId();
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(ex.getMessage());
			automatica = negocioRecuotificacionService.obtenerVersionActiva(idToNegocio);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "nuevoProgramaPago", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecProgramaPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecProgramaPagoGrid.jsp")		
	})
	public String nuevoProgramaPago(){
		try{
			negocioRecuotificacionService.nuevoProgramaPago(versionId);			
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(ex.getMessage());
			return INPUT;
		}finally{
			programas = negocioRecuotificacionService.obtenerProgramasPago(versionId);
		}			
		return SUCCESS;
	}
	
	@Action(value = "obtenerProgramasPago", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecProgramaPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecProgramaPagoGrid.jsp")		
		})
	public String obtenerProgramasPago(){
		programas = negocioRecuotificacionService.obtenerProgramasPago(versionId);
		return SUCCESS;
	}

	@Action(value = "modificarProgramasPago", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,json,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,json,mensaje"})
		})
	public String modificarProgramasPago(){
		try{
			String strJson = json;
			Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
			Type type = new TypeToken<ProgramaPagoVO>() {
			}.getType();
			ProgramaPagoVO vo = gson.fromJson(strJson, type);
			NegocioRecuotificacionAutProgPago progPago = entidadService.findById(NegocioRecuotificacionAutProgPago.class, vo.getProgPagoIdLong());
			progPago.setPctDistribucionPrima(vo.getPcteProgPagoDouble());		
			negocioRecuotificacionService.modificarProgramaPago(progPago);
		}catch(Exception ex){
			super.setMensajeError(ex.getMessage());
			LOGGER.error(ex);
			return INPUT;
		}
		return SUCCESS;
	}	
	
	@Action(value = "obtenerSaldo", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,tipoSaldo,saldo,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,tipoSaldo,saldo,mensaje"})
		})
	public String obtenerSaldo(){
		saldo = negocioRecuotificacionService.obtenerSaldo(versionId, EnumUtil.fromValue(TipoSaldo.class, tipoSaldo)).doubleValue();
		return SUCCESS;
	}
	
	@Action(value = "obtenerStatus", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,status,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,status,mensaje"})
		})
	public String obtenerStatus(){
		automatica = negocioRecuotificacionService.obtenerVersion(versionId);
		status = automatica.getStatus().toString();
		return SUCCESS;
	}
	
	@Action(value = "modificarTipoVigencia", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,tipoVigencia,status,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,tipoVigencia,status,mensaje"})
		})
	public String modificarTipoVigencia(){
		try{
			automatica = negocioRecuotificacionService.obtenerVersion(versionId);
			automatica.setTipoVigencia(EnumUtil.fromValue(TipoVigencia.class, tipoVigencia));
			entidadService.save(automatica);
			super.setMensajeExito();
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError("Error al modificar tipo vigencia de la versión: " + ex.getMessage());
			return INPUT;
			
		}
		return SUCCESS;
	}
	
	
	@Action(value = "eliminarProgramasPago", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecProgramaPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecProgramaPagoGrid.jsp")		
	})
	public String eliminarProgramasPago(){
		try{
			negocioRecuotificacionService.eliminarProgramaPago(progPagoId);
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(ex.getMessage());
			return INPUT;
		}finally{
			programas = negocioRecuotificacionService.obtenerProgramasPago(versionId);
		}			
		return SUCCESS;
	}
	
	@Action(value = "mostrarRecibos", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecRecibos.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecRecibos.jsp")		
		})
	public String mostrarRecibos(){	
		programa = negocioRecuotificacionService.obtenerPrograma(progPagoId);		
		return SUCCESS;
	}
	
	@Action(value = "obtenerRecibos", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecRecibosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecRecibosGrid.jsp")		
		})
	public String obtenerRecibos(){
		recibos = negocioRecuotificacionService.obtenerRecibos(progPagoId);
		return SUCCESS;
	}

	
	@Action(value = "generarRecibo", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecRecibosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecRecibosGrid.jsp")		
	})
	public String generarRecibo(){
		try{
			negocioRecuotificacionService.generarRecibo(progPagoId);			
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(ex.getMessage());
			return INPUT;
		}finally{
			recibos = negocioRecuotificacionService.obtenerRecibos(progPagoId);
		}			
		return SUCCESS;
	}
	
	
	@Action(value = "eliminarRecibo", results = { 
			@Result(name = SUCCESS, location = "/jsp/negocio/recuotificacion/negocioRecRecibosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/negocio/recuotificacion/negocioRecRecibosGrid.jsp")		
	})
	public String eliminarRecibo(){
		try{
			negocioRecuotificacionService.eliminarRecibo(reciboId);
		}catch(Exception ex){
			LOGGER.error(ex);
			super.setMensajeError(ex.getMessage());
			return INPUT;
		}finally{
			recibos = negocioRecuotificacionService.obtenerRecibos(progPagoId);
		}			
		return SUCCESS;
	}
	
	@Action(value = "modificarRecibo", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,json,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,json,mensaje"})
		})
	public String modificarRecibo(){
		try{
			String strJson = json;
			Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
			Type type = new TypeToken<ReciboVO>() {
			}.getType();
			ReciboVO vo = gson.fromJson(strJson, type);
			NegocioRecuotificacionAutRecibo recibo = entidadService.findById(NegocioRecuotificacionAutRecibo.class, Long.valueOf(vo.getReciboId()));
			if(TipoSaldo.PCTE_RECIBOS_PRIMA.toString().equals(vo.getTipoModificacion())){
				recibo.setPctDistPrimaNeta(vo.getPcteReciboDouble());
			}else if(TipoSaldo.PCTE_RECIBO_DERECHOS.toString().equals(vo.getTipoModificacion())){
				recibo.setPctDistDerechos(vo.getPcteReciboDouble());
			}else{
				recibo.setDiasDuracion(vo.getPcteReciboDouble().intValue());
			}
			negocioRecuotificacionService.modificarRecibo(recibo, vo.getTipoModificacionEnum());
		}catch(Exception ex){
			super.setMensajeError(ex.getMessage());
			LOGGER.error(ex);
			return INPUT;
		}
		return SUCCESS;
	}	
	
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	
	public String getPrimaTotal() {
		return primaTotal;
	}

	public void setPrimaTotal(String primaTotal) {
		this.primaTotal = primaTotal;
	}

	public NegocioRecuotificacion getRecuotificacion() {
		return recuotificacion;
	}

	public void setRecuotificacion(NegocioRecuotificacion recuotificacion) {
		this.recuotificacion = recuotificacion;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public List<NegocioRecuotificacionUsuario> getUsuariosAsociados() {
		return usuariosAsociados;
	}

	public void setUsuariosAsociados(
			List<NegocioRecuotificacionUsuario> usuariosAsociados) {
		this.usuariosAsociados = usuariosAsociados;
	}

	public List<NegocioRecuotificacionUsuario> getUsuariosDisponibles() {
		return usuariosDisponibles;
	}

	public void setUsuariosDisponibles(
			List<NegocioRecuotificacionUsuario> usuariosDisponibles) {
		this.usuariosDisponibles = usuariosDisponibles;
	}

	public NegocioRecuotificacionUsuario getUsuario() {
		return usuario;
	}

	public void setUsuario(NegocioRecuotificacionUsuario usuario) {
		this.usuario = usuario;
	}

	public NegocioRecuotificacionAutomatica getAutomatica() {
		return automatica;
	}

	public void setAutomatica(NegocioRecuotificacionAutomatica automatica) {
		this.automatica = automatica;
	}

	public Long getRecuotificacionId() {
		return recuotificacionId;
	}

	public void setRecuotificacionId(Long recuotificacionId) {
		this.recuotificacionId = recuotificacionId;
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}
	
	public void setVersionId(String versionId) {
		this.versionId = StringUtil.isEmpty(versionId)?null:Long.valueOf(versionId);
	}
	
	public List<NegocioRecuotificacionAutProgPago> getProgramas() {
		return programas;
	}

	public void setProgramas(List<NegocioRecuotificacionAutProgPago> programas) {
		this.programas = programas;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Map<Long, Integer> getVersiones() {
		return versiones;
	}

	public void setVersiones(Map<Long, Integer> versiones) {
		this.versiones = versiones;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getTipoSaldo() {
		return tipoSaldo;
	}

	public void setTipoSaldo(String tipoSaldo) {
		this.tipoSaldo = tipoSaldo;
	}

	public Long getProgPagoId() {
		return progPagoId;
	}

	public void setProgPagoId(Long progPagoId) {
		this.progPagoId = progPagoId;
	}
	
	public void setProgPagoId(String progPagoId){
		this.progPagoId = Long.valueOf(progPagoId);
	}

	public Map<String, String> getTipoVigencias() {
		return tipoVigencias;
	}

	public void setTipoVigencias(Map<String, String> tipoVigencias) {
		this.tipoVigencias = tipoVigencias;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public NegocioRecuotificacionAutProgPago getPrograma() {
		return programa;
	}

	public void setPrograma(NegocioRecuotificacionAutProgPago programa) {
		this.programa = programa;
	}

	public String getTipoVigencia() {
		return tipoVigencia;
	}

	public void setTipoVigencia(String tipoVigencia) {
		this.tipoVigencia = tipoVigencia;
	}

	public Long getReciboId() {
		return reciboId;
	}

	public void setReciboId(Long reciboId) {
		this.reciboId = reciboId;
	}
	
	
	public List<NegocioRecuotificacionAutRecibo> getRecibos() {
		return recibos;
	}

	public void setRecibos(List<NegocioRecuotificacionAutRecibo> recibos) {
		this.recibos = recibos;
	}
				
}
