package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.negocio.seccion.RelacionesNegocioSeccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioSeccionAction extends BaseAction  implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idToNegProducto;
	private BigDecimal idToTipoPoliza;
	private NegocioProducto negocioProducto;
	private NegocioTipoPoliza negocioTipoPoliza;
	private NegocioSeccion negocioSeccion = new NegocioSeccion();
	private List<NegocioTipoPoliza> negocioTipoPolizaList = new ArrayList<NegocioTipoPoliza>(1);
	private RelacionesNegocioSeccionDTO relacionesNegocioSeccionDTO = new RelacionesNegocioSeccionDTO();
	
	private EntidadService entidadService;
	
	@Override
	public void prepare() throws Exception {
		
		
	}
	
	public void prepareMostrar(){
		//BigDecimal idToNegProducto proviene de un elemento hidden
		if (negocioProducto == null) {
			negocioProducto = new NegocioProducto();
		}
		System.out.println("************ ID PRODUCTO " + idToNegProducto);
		negocioProducto.setIdToNegProducto(idToNegProducto);
		negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
		System.out.println("************ PRODUCTO ENCONTRADO " + negocioProducto.getIdToNegProducto());
	}
	
	
	public String mostrar(){		
		negocioTipoPolizaList = negocioSeccionService.getNegTipoPolizaList(negocioProducto);
		if(negocioTipoPolizaList == null){
			negocioTipoPolizaList = new ArrayList<NegocioTipoPoliza>();
		}
		return SUCCESS;
	}
	
	public void prepareObtenerRelaciones(){
		if (negocioTipoPoliza.getIdToNegTipoPoliza().intValue() > 0) {
			negocioTipoPoliza = entidadService.findById(
					NegocioTipoPoliza.class,
					negocioTipoPoliza.getIdToNegTipoPoliza());
		}
	}
	
	public String obtenerRelaciones(){
		if(negocioTipoPoliza != null && negocioTipoPoliza.getIdToNegTipoPoliza().intValue() > 0){
			setRelacionesNegocioSeccionDTO(negocioSeccionService.getRelationList(negocioTipoPoliza));
		}else{
			relacionesNegocioSeccionDTO.setAsociadas(new ArrayList<NegocioSeccion>(1));
			relacionesNegocioSeccionDTO.setDisponibles(new ArrayList<NegocioSeccion>(1));
		}
		return SUCCESS;
	}
	
	public String relacionarNegocioSeccion(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		negocioSeccionService.relacionarNegocioSeccion(accion, negocioSeccion);
		return SUCCESS;
	}
	
	
	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioTipoPolizaList(List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setIdToTipoPoliza(BigDecimal idToTipoPoliza) {
		this.idToTipoPoliza = idToTipoPoliza;
	}

	public BigDecimal getIdToTipoPoliza() {
		return idToTipoPoliza;
	}
	
	private NegocioSeccionService negocioSeccionService;
	
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(
			NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	

	public void setRelacionesNegocioSeccionDTO(
			RelacionesNegocioSeccionDTO relacionesNegocioSeccionDTO) {
		this.relacionesNegocioSeccionDTO = relacionesNegocioSeccionDTO;
	}

	public RelacionesNegocioSeccionDTO getRelacionesNegocioSeccionDTO() {
		return relacionesNegocioSeccionDTO;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}
	

}
