package mx.com.afirme.midas2.action.negocio.condicionespecial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author Softnet01
 * @version 1.0
 * @created 07-abr.-2014 05:39:20 p. m.
 */
@Component
@Namespace(value = "/negocio/condicionespecial")
public class NegocioCondicionEspecialAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -3055035001609597727L;
	
	private BigDecimal idToNegocio;
	
	private String codigoNombre;
	
	private NegocioCondicionEspecial negocioCondicionEspecial =  new NegocioCondicionEspecial();

	private List<NegocioCondicionEspecial> condicionesAsociadas = new ArrayList<NegocioCondicionEspecial>();
	
	private List<NegocioCondicionEspecial> condicionesDisponibles = new ArrayList<NegocioCondicionEspecial>();
	
	private List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();

	private String idSeccion;
	
	private Negocio negocio = new Negocio();
	
	private Integer aplicaExterno ;
	
	private String ids;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	private NegocioSeccionService negocioSeccionService;
		
	@Autowired
	@Qualifier("negocioCondicionEspecialServiceEJB")
	private NegocioCondicionEspecialService negocioCondicionEspecialService;

	public NegocioCondicionEspecialAction() {

	}
	
	/**
	 * Método para preparar atributos de la clase. En este caso obtendrá el
	 * idToNegocio (ver NegocioAgenteAction).
	 */
	@Override
	public void prepare() {
		idToNegocio = BigDecimal.valueOf((Long)ServletActionContext.getContext().getSession().get("idNegocio"));
		secciones = negocioSeccionService.listarSeccionNegocioByIdNegocio(idToNegocio);
		condicionesAsociadas = negocioCondicionEspecialService.obtenerCondicionesNegocio(idToNegocio.longValue(), NivelAplicacion.TODAS );
		aplicaExterno = 0;

		for( NegocioCondicionEspecial negCond : condicionesAsociadas  ){
			aplicaExterno = negCond.getAplicaExterno();
		}
		
	}

	/**
	 * Método que se encarga de mostrar la pantalla dentro del dhtmlxTab del
	 * negocio. Obtiene el catálogo de secciones para mostrarlo en pantalla
	 * através del servicio de
	 * negocioSeccionService.listarSeccionNegocioByIdNegocio
	 */
	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecial.jsp"))
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	@Action(value = "saveIds", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecial.jsp"))
	public String saveIds() {
		negocioCondicionEspecialService.saveIds( ids );
		return SUCCESS;
	}
	
	/**
	 * Método que lista las condiciones especiales en el dhtmlxGrid de
	 * Disponibles por la descripción de la condicion. Esta descripcion puede
	 * ser el código o el nombre de la condición. Utilizar el metodo de
	 * obtenerCondiconesEspeciales(filtro) donde el valor a buscar iria en
	 * condicionEspecial.codigoDescripcion
	 */
	@Action(value = "obtenerCondicionesDisponibles", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecialDisponibleGrid.jsp"))
	public String buscarCondicionEspecial() {
		if( StringUtil.isEmpty(codigoNombre)){
			BigDecimal idToSeccion = null;
			if( !StringUtil.isEmpty(idSeccion) ){
				idToSeccion = new BigDecimal(idSeccion);
			}
			condicionesDisponibles = negocioCondicionEspecialService.obtenerCondicionesEspecialesDisponibles(idToNegocio, idToSeccion);
		} else {
			condicionesDisponibles = negocioCondicionEspecialService.obtenerCondicionesEspecialesDisponibles(idToNegocio, codigoNombre);
		}

		return SUCCESS;
	}

	/**
	 * Método que alimenta al dhtmlxGrid de condiciones asociadas al negocio.
	 * Invoca el método obtenerCondicionesNegocio(id) de
	 * negocioCondicionEspecialService
	 */
	@Action(value = "obtenerCondicionesAsociadas", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecialAsociadoGrid.jsp"))
	public String obtenerCondicionesAsociadas() {
		Long idNegocio = idToNegocio.longValue();
		condicionesAsociadas = negocioCondicionEspecialService.obtenerCondicionesNegocio(idNegocio, NivelAplicacion.TODAS );
		return SUCCESS;
	}

	/**
	 * Método que utiliza el dhtmlxProcessor para la asociación de la
	 * condición especial. Invoca el metodo de relacionar de
	 * negocioCondicionEspecialService
	 */
	@Action(value = "relacionarNegocioCondicionEspecial", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecial.jsp"))
	public String relacionarNegocioCondicionEspecial() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		Integer idUsuario = usuarioService.getUsuarioActual().getId();
		negocio.setIdToNegocio(idToNegocio.longValue());
		negocioCondicionEspecial.setFechaCreacion(new Date());
		negocioCondicionEspecial.setNegocio(negocio);
		negocioCondicionEspecial.setCodigoUsuarioCreacion(idUsuario.toString());
		negocioCondicionEspecial.setAplicaExterno(aplicaExterno);
		negocioCondicionEspecialService.relacionar(accion, negocioCondicionEspecial);

		return SUCCESS;
	}
	

	/**
	 * Action encargado de asignar si se aplica o no, la condicion especial para externo.
	 */
	@Action(value = "aplicaExterno", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecial.jsp"))
	public String aplicaExterno() {
		negocioCondicionEspecialService.aplicarExternos(idToNegocio, aplicaExterno);
		return SUCCESS;
	}


	/**
	 * Action encargado de asignar si se aplica o no, la condicion especial para externo.
	 */
	@Action(value = "ligarTodas", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecial.jsp"))
	public String ligarTodas() {
		BigDecimal idToSeccion = new BigDecimal(idSeccion);
		negocioCondicionEspecialService.relacionarTodas(idToNegocio, idToSeccion, codigoNombre, aplicaExterno);
		return SUCCESS;
	}

	/**
	 * Action encargado de asignar si se aplica o no, la condicion especial para externo.
	 */
	@Action(value = "desligarTodas", results = @Result(name = SUCCESS, location = "/jsp/negocio/condicionespecial/negocioCondicionEspecial.jsp"))
	public String desligarTodas() {
		negocioCondicionEspecialService.desligarTodas(idToNegocio);
		return SUCCESS;
	}

	public List<NegocioCondicionEspecial> getCondicionesAsociadas() {
		return condicionesAsociadas;
	}

	public void setCondicionesAsociadas(List<NegocioCondicionEspecial> condicionesAsociadas) {
		this.condicionesAsociadas = condicionesAsociadas;
	}

	public List<SeccionDTO> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<SeccionDTO> secciones) {
		this.secciones = secciones;
	}

	public List<NegocioCondicionEspecial> getCondicionesDisponibles() {
		return condicionesDisponibles;
	}

	public void setCondicionesDisponibles( List<NegocioCondicionEspecial> condicionesDisponibles) {
		this.condicionesDisponibles = condicionesDisponibles;
	}

	public String getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}

	public Integer getAplicaExterno() {
		return aplicaExterno;
	}

	public void setAplicaExterno(Integer aplicaExterno) {
		this.aplicaExterno = aplicaExterno;
	}

	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public NegocioCondicionEspecial getNegocioCondicionEspecial() {
		return negocioCondicionEspecial;
	}

	public void setNegocioCondicionEspecial(
			NegocioCondicionEspecial negocioCondicionEspecial) {
		this.negocioCondicionEspecial = negocioCondicionEspecial;
	}

	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}


	public String getCodigoNombre() {
		return codigoNombre;
	}

	public void setCodigoNombre(String codigoNombre) {
		this.codigoNombre = codigoNombre;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}


}