package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion.paquete.cobertura;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.cobertura.RelacionesNegocioCoberturaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioCobPaqSeccionAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = -5200238448778139L;

	@Override
	public void prepare() throws Exception {

	}
		
	public void prepareMostrar(){
		//BigDecimal idToNegProducto proviene de un elemento hidden
		if(negocioProducto==null)negocioProducto = new NegocioProducto();
		negocioProducto.setIdToNegProducto(idToNegProducto);		
		negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
	}
	
	public String mostrar(){		
		negocioTipoPolizaList = negocioSeccionService.getNegTipoPolizaList(negocioProducto);
		estadoList = listadoService.listarEstadosMX();
		return SUCCESS;		
	}
	
	public void prepareObtenerRelaciones(){		
		negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);
		monedaDTO = entidadService.findById(MonedaDTO.class, idMonedaDTO);
		estadoDTO = entidadService.findById(EstadoDTO.class, stateId);
		ciudadDTO = entidadService.findById(CiudadDTO.class, cityId);
		tipoUsoVehiculoDTO = entidadService.findById(TipoUsoVehiculoDTO.class, idTcTipoUsoVehiculo);
		agente = entidadService.findById(Agente.class, agenteId);
		
		negocioCobPaqSeccion = new NegocioCobPaqSeccion();
		negocioCobPaqSeccion.setMonedaDTO(monedaDTO);
		negocioCobPaqSeccion.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
		negocioCobPaqSeccion.setRenovacionString(esRenovacionString);
		
		if(estadoDTO != null){
			negocioCobPaqSeccion.setEstadoDTO(estadoDTO);
		}else{
			estadoDTO = new EstadoDTO();
			negocioCobPaqSeccion.setEstadoDTO(estadoDTO);			
		}
		if(ciudadDTO != null){
			negocioCobPaqSeccion.setCiudadDTO(ciudadDTO);
		}else{
			ciudadDTO = new CiudadDTO();
			negocioCobPaqSeccion.setCiudadDTO(ciudadDTO);			
		}
		if(tipoUsoVehiculoDTO != null){
			negocioCobPaqSeccion.setTipoUsoVehiculo(tipoUsoVehiculoDTO);
		}else{
			tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
			negocioCobPaqSeccion.setTipoUsoVehiculo(tipoUsoVehiculoDTO);
		}
		if(agente != null){
			negocioCobPaqSeccion.setAgente(agente);
		}else{
			agente = new Agente();
			negocioCobPaqSeccion.setAgente(agente);
		}
	}
	
	public String obtenerRelaciones(){
		relacionesNegocioCoberturaDTO=negocioCobPaqSeccionService.getRelationLists(negocioCobPaqSeccion);
		return SUCCESS;
	}
	
	public void prepareRelacionarNegocioCobPaqSeccion(){
		if(negocioCobPaqSeccion.getIdToNegCobPaqSeccion() != null){
			negocioCobPaqSeccion = entidadService.findById(NegocioCobPaqSeccion.class, negocioCobPaqSeccion.getIdToNegCobPaqSeccion());
		}else{
			//Prepara nuevo
			negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, negocioCobPaqSeccion.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
			monedaDTO = entidadService.findById(MonedaDTO.class, negocioCobPaqSeccion.getMonedaDTO().getIdTcMoneda());
			estadoDTO = entidadService.findById(EstadoDTO.class, negocioCobPaqSeccion.getEstadoDTO().getStateId());
			ciudadDTO = entidadService.findById(CiudadDTO.class, negocioCobPaqSeccion.getCiudadDTO().getCityId());
			
			tipoUsoVehiculoDTO = entidadService.findById(TipoUsoVehiculoDTO.class, negocioCobPaqSeccion.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo()==null?new BigDecimal(0):negocioCobPaqSeccion.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo());
			agente = entidadService.findById(Agente.class, negocioCobPaqSeccion.getAgente().getId()==null?new Long(0):negocioCobPaqSeccion.getAgente().getId());
			esRenovacion = negocioCobPaqSeccion.isRenovacion();
			
			CoberturaDTO coberturaDTO = entidadService.findById(CoberturaDTO.class, negocioCobPaqSeccion.getCoberturaDTO().getIdToCobertura());
			negocioCobPaqSeccion.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
			negocioCobPaqSeccion.setMonedaDTO(monedaDTO);
			negocioCobPaqSeccion.setCoberturaDTO(coberturaDTO);
			negocioCobPaqSeccion.setEstadoDTO(estadoDTO);
			negocioCobPaqSeccion.setCiudadDTO(ciudadDTO);
			negocioCobPaqSeccion.setTipoUsoVehiculo(tipoUsoVehiculoDTO);
			negocioCobPaqSeccion.setAgente(agente);
			negocioCobPaqSeccion.setRenovacion(esRenovacion);
		}
	}
	
	public String relacionarNegocioCobPaqSeccion(){

		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(negocioCobPaqSeccion.getEstadoDTO().getStateId().equals("")){
			negocioCobPaqSeccion.setEstadoDTO(null);
		}
		if(negocioCobPaqSeccion.getCiudadDTO().getCityId().equals("")){
			negocioCobPaqSeccion.setCiudadDTO(null);
		}
		if(negocioCobPaqSeccion.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo()==null){
			negocioCobPaqSeccion.setTipoUsoVehiculo(null);
		}
		if(negocioCobPaqSeccion.getAgente().getId()==null){
			negocioCobPaqSeccion.setAgente(null);
		}
		
		respuesta=negocioCobPaqSeccionService.relacionarNegocioCobPaqSeccion(accion, negocioCobPaqSeccion);
		return SUCCESS;
	}

	public String mostrarDeducibles(){
		return SUCCESS;
	}
	
	private Long idToNegProducto;
	private Long idToNegTipoPoliza;
	private Long idToNegSeccion;
	private Long idToPaqueteSeccion;
	private Long idToNegPaqueteSeccion;
	private Short idMonedaDTO;
	private String stateId;
	private String cityId;
	private Boolean esRenovacion;
	private String esRenovacionString;
	private BigDecimal idTcTipoUsoVehiculo;
	private long agenteId;

	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private RelacionesNegocioCoberturaDTO relacionesNegocioCoberturaDTO;
	private List<NegocioTipoPoliza>negocioTipoPolizaList= new ArrayList<NegocioTipoPoliza>();
	private List<NegocioSeccion> secciones = new ArrayList<NegocioSeccion>(1);
	private List<NegocioPaqueteSeccion> paquetes = new ArrayList<NegocioPaqueteSeccion>(1);
	private List<MonedaDTO> monedaList = new ArrayList<MonedaDTO>(1);
	private List<EstadoDTO> estadoList = new ArrayList<EstadoDTO>(1);
	private List<CiudadDTO> municipioList = new ArrayList<CiudadDTO>(1);
	private List<TipoUsoVehiculoDTO> tipoUsoVehiculoList = new ArrayList<TipoUsoVehiculoDTO>(1);

	private NegocioTipoPoliza negocioTipoPoliza;
	private NegocioSeccion negocioSeccion;
	private NegocioProducto negocioProducto;
	private NegocioCobPaqSeccion negocioCobPaqSeccion;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;

	private TipoUsoVehiculoDTO tipoUsoVehiculoDTO;
	private Agente agente;
	private NegocioSeccionService negocioSeccionService;
	private NegocioCobPaqSeccionService negocioCobPaqSeccionService;
	private EntidadService entidadService;

	private ListadoService listadoService;
	private RespuestaGridRelacionDTO respuesta;

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(
			ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")	
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}

	@Autowired
	@Qualifier("negocioCobPaqSeccionServiceEJB")			
	public void setNegocioCobPaqSeccionService(
			NegocioCobPaqSeccionService negocioCobPaqSeccionService) {
		this.negocioCobPaqSeccionService = negocioCobPaqSeccionService;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}
	
	public NegocioCobPaqSeccion getNegocioCobPaqSeccion() {
		return negocioCobPaqSeccion;
	}

	public void setNegocioCobPaqSeccion(NegocioCobPaqSeccion negocioCobPaqSeccion) {
		this.negocioCobPaqSeccion = negocioCobPaqSeccion;
	}
	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}
	public RelacionesNegocioCoberturaDTO getRelacionesNegocioCoberturaDTO() {
		return relacionesNegocioCoberturaDTO;
	}

	public void setRelacionesNegocioCoberturaDTO(
			RelacionesNegocioCoberturaDTO relacionesNegocioCoberturaDTO) {
		this.relacionesNegocioCoberturaDTO = relacionesNegocioCoberturaDTO;
	}

	public Long getIdToPaqueteSeccion() {
		return idToPaqueteSeccion;
	}

	public void setIdToPaqueteSeccion(Long idToPaqueteSeccion) {
		this.idToPaqueteSeccion = idToPaqueteSeccion;
	}

	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}

	public Long getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(Long idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

	public Long getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegTipoPoliza(Long idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setNegocioTipoPolizaList(
			List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public void setSecciones(List<NegocioSeccion> secciones) {
		this.secciones = secciones;
	}

	public List<NegocioSeccion> getSecciones() {
		return secciones;
	}

	public void setPaquetes(List<NegocioPaqueteSeccion> paquetes) {
		this.paquetes = paquetes;
	}

	public List<NegocioPaqueteSeccion> getPaquetes() {
		return paquetes;
	}

	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}

	public Long getIdToNegPaqueteSeccion() {
		return idToNegPaqueteSeccion;
	}

	public void setMonedaList(List<MonedaDTO> monedaList) {
		this.monedaList = monedaList;
	}

	public List<MonedaDTO> getMonedaList() {
		return monedaList;
	}

	public void setIdMonedaDTO(Short idMonedaDTO) {
		this.idMonedaDTO = idMonedaDTO;
	}

	public Short getIdMonedaDTO() {
		return idMonedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}

	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}

	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}

	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setEstadoList(List<EstadoDTO> estadoList) {
		this.estadoList = estadoList;
	}

	public List<EstadoDTO> getEstadoList() {
		return estadoList;
	}

	public void setMunicipioList(List<CiudadDTO> municipioList) {
		this.municipioList = municipioList;
	}

	public List<CiudadDTO> getMunicipioList() {
		return municipioList;
	}

	public BigDecimal getIdTcTipoUsoVehiculo() {
		return idTcTipoUsoVehiculo;
	}

	public void setIdTcTipoUsoVehiculo(BigDecimal idTcTipoUsoVehiculo) {
		this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
	}

	public List<TipoUsoVehiculoDTO> getTipoUsoVehiculoList() {
		return tipoUsoVehiculoList;
	}

	public void setTipoUsoVehiculoList(List<TipoUsoVehiculoDTO> tipoUsoVehiculoList) {
		this.tipoUsoVehiculoList = tipoUsoVehiculoList;
	}

	public TipoUsoVehiculoDTO getTipoUsoVehiculoDTO() {
		return tipoUsoVehiculoDTO;
	}

	public void setTipoUsoVehiculoDTO(TipoUsoVehiculoDTO tipoUsoVehiculoDTO) {
		this.tipoUsoVehiculoDTO = tipoUsoVehiculoDTO;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Long getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(Long agenteId) {
		this.agenteId = agenteId;
	}

	public Boolean getEsRenovacion() {
		return esRenovacion;
	}

	public void setEsRenovacion(Boolean esRenovacion) {
		this.esRenovacion = esRenovacion;
	}
	
	public String getEsRenovacionString() {
		return esRenovacionString;
	}

	public void setEsRenovacionString(String esRenovacionString) {
		this.esRenovacionString = esRenovacionString;
	}

	/*****************************/
	/*SECCIÓN DE SUMAS ASEGURADAS*/
	/*****************************/
	
	public String mostrarSumasAseguradas() {
		if(idToNegCobPaqSeccion!= null)
			System.out.println(idToNegCobPaqSeccion);
		return SUCCESS;
	}
	
	public String obtenerSumasAseguradasCobertura(){
		sumasAseguradasList = negocioCobPaqSeccionService.obtenerSumasAseguradas(idToNegCobPaqSeccion);
		return SUCCESS;
	}
	
	public String accionSobreSumasAseguradasCobertura() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = negocioCobPaqSeccionService.relacionarSumasAseguradasCobertura(accion, idToNegCobPaqSeccion, idSumaAsegurada, valorSumaAsegurada, defaultValor);
		return SUCCESS;
	}

	private BigDecimal idToNegCobPaqSeccion;
	private List<NegocioCobSumAse> sumasAseguradasList;
	
	private Long idSumaAsegurada;
	private Double valorSumaAsegurada;
	private short defaultValor;
	
	public BigDecimal getIdToNegCobPaqSeccion() {
		return idToNegCobPaqSeccion;
	}
	public void setIdToNegCobPaqSeccion(BigDecimal idToNegCobPaqSeccion) {
		this.idToNegCobPaqSeccion = idToNegCobPaqSeccion;
	}

	public List<NegocioCobSumAse> getSumasAseguradasList() {
		return sumasAseguradasList;
	}
	public void setSumasAseguradasList(List<NegocioCobSumAse> sumasAseguradasList) {
		this.sumasAseguradasList = sumasAseguradasList;
	}

	public Long getIdSumaAsegurada() {
		return idSumaAsegurada;
	}

	public void setIdSumaAsegurada(Long idSumaAsegurada) {
		this.idSumaAsegurada = idSumaAsegurada;
	}

	public Double getValorSumaAsegurada() {
		return valorSumaAsegurada;
	}

	public void setValorSumaAsegurada(Double valorSumaAsegurada) {
		this.valorSumaAsegurada = valorSumaAsegurada;
	}

	public short getDefaultValor() {
		return defaultValor;
	}

	public void setDefaultValor(short defaultValor) {
		this.defaultValor = defaultValor;
	}


	
}
