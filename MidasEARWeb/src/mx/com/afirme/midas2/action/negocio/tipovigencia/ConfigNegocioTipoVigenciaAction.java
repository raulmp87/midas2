/**
 * 
 */
package mx.com.afirme.midas2.action.negocio.tipovigencia;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.negocio.tipovigencia.ConfigNegocioTipoVigenciaService;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.domain.negocio.tipovigencia.NegocioTipoVigencia;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.TipoVigencia;

/**
 * @author Adriana Flores
 *
 */
@Component
@Namespace(value = "/negocio/tipovigencia")
public class ConfigNegocioTipoVigenciaAction extends BaseAction implements Preparable {
	
	public static final Logger LOGGER = Logger.getLogger(ConfigNegocioTipoVigenciaAction.class);
	
	private static final long serialVersionUID = 7226735640277347435L;
	
	private Long idToNegocio;
	private Long idVigencia;
	private Long idNegocioTipoVigencia;
	private NegocioTipoVigencia negocioTipoVigencia;
	
	@Autowired
	@Qualifier("configNegocioTipoVigenciaEJB")
	private ConfigNegocioTipoVigenciaService configNegocioTipoVigenciaService;
	
	private List<NegocioTipoVigencia> vigenciasAsociadas = new ArrayList<NegocioTipoVigencia>();
	private List<NegocioTipoVigencia> vigenciasDisponibles = new ArrayList<NegocioTipoVigencia>();
	private TipoVigencia tipoVigencia;	
	
	@Override
	public void prepare() throws Exception {
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
	}
	
	/**
	 * Método Action para mostrar contenedor
	 */
	@Action(value="mostrarContenedor", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/contenedorConfigNegocioTipoVigencia.jsp")})
	public String mostrarContendor(){
		
		return SUCCESS;
	}
	
	/**
	 * Método Action encargado de obtener los Tipos de vigencias asociadas al negocio
	 */
	@Action(value="obtenerTiposVigenciaAsociados", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/listadoTiposVigenciaAsociadas.jsp")})
	public String obtenerTiposVigenciaAsociados(){		
		vigenciasAsociadas = configNegocioTipoVigenciaService.obtenerTiposVigenciaAsociados(idToNegocio);
		return SUCCESS;
	}	
	
	public void prepareEliminarVigencia(){
		obtenerIdVigencia();
	}
	
	@Action(value="eliminarVigencia", results = { 
			@Result(name = SUCCESS,
					type = "redirectAction", params = {
						"actionName", "mostrarContenedor", 
						"namespace", "/negocio/tipovigencia",
						"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}		
					)})
	public String eliminarVigencia(){		
		if(configNegocioTipoVigenciaService.eliminarTipoVigencia(idVigencia)){
			super.setMensajeExito();
		} else {
			super.setMensajeError(getText("midas.negocio.tipovigencia.error.message.eliminar", new String[]{"" + idVigencia}));
		}
		return SUCCESS;
	}
	
	/**
	 * Método Action encargado de obtener los Tipos de vigencias disponibles para el negocio
	 */
	@Action(value="obtenerTiposVigenciaDisponibles", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/listadoTiposVigenciaDisponibles.jsp")})
	public String obtenerTiposVigenciasDisponibles(){
		vigenciasDisponibles = configNegocioTipoVigenciaService.obtenerTiposVigenciaDisponibles(idToNegocio);
		return SUCCESS;
	}
	
	/**
	 * Método Action encargado de asignar todos los Tipos de vigencias al negocio
	 */
	@Action(value="asociarTodas", results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", params = {
					"actionName", "mostrarContenedor", 
					"namespace", "/negocio/tipovigencia",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}		
				)})
	public String asociarTodas(){
		if(configNegocioTipoVigenciaService.validarParametroFechaFinVigenciaFija(idToNegocio)){
			if(!configNegocioTipoVigenciaService.asociarTodos(idToNegocio)){
				super.setMensajeExito();
			} else{
				super.setMensajeError(getText("midas.negocio.tipovigencia.error.message.rangoProductos", new String[]{"" }));
			}		
			
		} else {
			super.setMensajeError(getText("midas.negocio.tipovigencia.error.message.fechaFinVigenciaFija", new String[]{"" }));
		}
		
		return SUCCESS;
	}
	
	/**
	 * Método Action encargado de eliminar los Tipos de vigencias asociadas al negocio
	 */
	@Action(value="desasociarTodas", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/contenedorConfigNegocioTipoVigencia.jsp")})
	public String desasociarTodas(){
		configNegocioTipoVigenciaService.desasociarTodos(idToNegocio);
		return SUCCESS;
	}
	
	@Action(value="mostrarAgregarVigencia", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/ventanaTipoVigencia.jsp")})
	public String mostrarAgregarVigencia(){
		return SUCCESS;
	}
	
	public void validateAgregarVigencia(){
		tipoVigencia.setFechaCreacion(new Date());
		try {
			tipoVigencia.setDescripcion(new String(tipoVigencia.getDescripcion().getBytes("ISO-8859-1"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {			
		}
		addErrors(tipoVigencia, NewItemChecks.class, this, "tipoVigencia");
	}
	
	@Action(value="agregarVigencia", results = { 
		@Result(name = INPUT, location = "/jsp/negocio/tipovigencia/ventanaTipoVigencia.jsp"),
		@Result(name = SUCCESS,  location = "/jsp/negocio/tipovigencia/redirectTipoVigencia.jsp")
	})
	public String agregarVigencia(){
		if(!configNegocioTipoVigenciaService.existeRegistroDiasVigencia(tipoVigencia.getDias())){			
			configNegocioTipoVigenciaService.agregarTipoVigencia(tipoVigencia);
			super.setMensajeExito();
		} else{
			super.setMensajeError(getText("midas.negocio.tipovigencia.error.message.duplicado", new String[]{"" + tipoVigencia.getDias()}));
		}
		return SUCCESS;
	}
	
	public void prepareDesasociarUna(){
		obtenerIdNegocioVigencia();
	}
	
	@Action(value="desasociarUna", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/contenedorConfigNegocioTipoVigencia.jsp")})
	public String desasociarUna(){
		configNegocioTipoVigenciaService.quitarANegocio(idNegocioTipoVigencia, idToNegocio);
		return SUCCESS;
	}
	
	public void prepareCambiarVigenciaDefault(){
		idNegocioTipoVigencia = (Long)ServletActionContext.getContext().getSession().get("idNegocioTipoVigencia");
	}
	
	@Action(value="cambiarVigenciaDefault", results = { @Result(name = SUCCESS, location = "/jsp/negocio/tipovigencia/contenedorConfigNegocioTipoVigencia.jsp")})
	public String cambiarVigenciaDefault(){
		configNegocioTipoVigenciaService.cambiarVigenciaDefault(idNegocioTipoVigencia, idToNegocio);
		return SUCCESS;
	}
	
	@Action(value="relacionarTipoVigencia", results = { 
		@Result(name = INPUT, location = "/jsp/negocio/tipovigencia/contenedorConfigNegocioTipoVigencia.jsp"),
		@Result(name = SUCCESS, type = "json", params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})
	})	
	public String relacionarTipoVigencia(){
		String accion = ServletActionContext.getRequest().getParameter("accion");		
		
		if(accion.equals("updated")){
			configNegocioTipoVigenciaService.cambiarVigenciaDefault(negocioTipoVigencia.getId(), negocioTipoVigencia.getNegocio().getIdToNegocio());
			configNegocioTipoVigenciaService.relacionarTipoVigencia(accion, negocioTipoVigencia);
		} else if(accion.equals("deleted")){
			configNegocioTipoVigenciaService.relacionarTipoVigencia(accion, negocioTipoVigencia);
		} else {
			if(configNegocioTipoVigenciaService.validarParametroFechaFinVigenciaFija(idToNegocio)){
				if(configNegocioTipoVigenciaService.validarContraVigenciaProductosNegocio(negocioTipoVigencia.getTipoVigencia(), idToNegocio)){
					configNegocioTipoVigenciaService.relacionarTipoVigencia(accion, negocioTipoVigencia);
				} else {
					super.setMensajeError(getText("midas.negocio.tipovigencia.error.message.rangoProductos"));					
				}
			} else {
				super.setMensajeError(getText("midas.negocio.tipovigencia.error.message.fechaFinVigenciaFija"));				
			}
		}
		return SUCCESS;
	}
	
	public void obtenerIdNegocioVigencia(){
		idNegocioTipoVigencia = (Long)ServletActionContext.getContext().getSession().get("idNegocioTipoVigencia");
	}
	
	public void obtenerIdVigencia(){
		idVigencia = (Long)ServletActionContext.getContext().getSession().get("idVigencia");
	}
	
	/**
	 * @return the idToNegocio
	 */
	public Long getIdToNegocio() {
		return idToNegocio;
	}

	/**
	 * @param idToNegocio the idToNegocio to set
	 */
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	/**
	 * @return the configNegocioTipoVigenciaService
	 */
	public ConfigNegocioTipoVigenciaService getConfigNegocioTipoVigenciaService() {
		return configNegocioTipoVigenciaService;
	}

	/**
	 * @param configNegocioTipoVigenciaService the configNegocioTipoVigenciaService to set
	 */
	public void setConfigNegocioTipoVigenciaService(
			ConfigNegocioTipoVigenciaService configNegocioTipoVigenciaService) {
		this.configNegocioTipoVigenciaService = configNegocioTipoVigenciaService;
	}

	/**
	 * @return the vigenciasAsociadas
	 */
	public List<NegocioTipoVigencia> getVigenciasAsociadas() {
		return vigenciasAsociadas;
	}

	/**
	 * @param vigenciasAsociadas the vigenciasAsociadas to set
	 */
	public void setVigenciasAsociadas(List<NegocioTipoVigencia> vigenciasAsociadas) {
		this.vigenciasAsociadas = vigenciasAsociadas;
	}

	/**
	 * @return the vigenciasDisponibles
	 */
	public List<NegocioTipoVigencia> getVigenciasDisponibles() {
		return vigenciasDisponibles;
	}

	/**
	 * @param vigenciasDisponibles the vigenciasDisponibles to set
	 */
	public void setVigenciasDisponibles(List<NegocioTipoVigencia> vigenciasDisponibles) {
		this.vigenciasDisponibles = vigenciasDisponibles;
	}

	/**
	 * @return the tipoVigencia
	 */
	public TipoVigencia getTipoVigencia() {
		return tipoVigencia;
	}

	/**
	 * @param tipoVigencia the tipoVigencia to set
	 */
	public void setTipoVigencia(TipoVigencia tipoVigencia) {
		this.tipoVigencia = tipoVigencia;
	}

	/**
	 * @return the idVigencia
	 */
	public Long getIdVigencia() {
		return idVigencia;
	}

	/**
	 * @param idVigencia the idVigencia to set
	 */
	public void setIdVigencia(Long idVigencia) {
		this.idVigencia = idVigencia;
	}

	/**
	 * @return the idNegocioTipoVigencia
	 */
	public Long getIdNegocioTipoVigencia() {
		return idNegocioTipoVigencia;
	}

	/**
	 * @param idNegocioTipoVigencia the idNegocioTipoVigencia to set
	 */
	public void setIdNegocioTipoVigencia(Long idNegocioTipoVigencia) {
		this.idNegocioTipoVigencia = idNegocioTipoVigencia;
	}

	/**
	 * @return the negocioTipoVigencia
	 */
	public NegocioTipoVigencia getNegocioTipoVigencia() {
		return negocioTipoVigencia;
	}

	/**
	 * @param negocioTipoVigencia the negocioTipoVigencia to set
	 */
	public void setNegocioTipoVigencia(NegocioTipoVigencia negocioTipoVigencia) {
		this.negocioTipoVigencia = negocioTipoVigencia;
	}
}
