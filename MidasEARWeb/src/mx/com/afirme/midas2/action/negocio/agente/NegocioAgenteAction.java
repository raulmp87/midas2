package mx.com.afirme.midas2.action.negocio.agente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioAgenteAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 5240456062889181802L;
	
	private Long idToNegocio;
	
	private BigDecimal idToNegocioAgente;
	
	private Negocio negocio;
	
	private NegocioAgente negocioAgente = new NegocioAgente();
	
	private Map<Long, String> gerencias;
	
	private Map<Long, String> oficinas;
	
	private Map<Long, String> promotorias;
	
	private String gerenciaId;
	
	private String promotoriaId;
	
	private String oficinaId;
	
	private ListadoService listadoService;
	
	private EntidadService entidadService;
	
	private NegocioAgenteService negocioAgenteService;
	
	private List<NegocioAgente> agentesAsociados = new ArrayList<NegocioAgente>();
	
	private List<NegocioAgente> agentesDisponibles = new ArrayList<NegocioAgente>();

	private String idsRelacionar;


	public String getIdsRelacionar() {
		return idsRelacionar;

	}

	public void setIdsRelacionar(String idsRelacionar) {
		this.idsRelacionar = idsRelacionar;
	}

	@Autowired
	@Qualifier("negocioAgenteServiceEJB")
	public void setNegocioAgenteService(NegocioAgenteService negocioAgenteService) {
		this.negocioAgenteService = negocioAgenteService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<Long, String> getGerencias() {
		return gerencias;
	}

	public void setGerencias(Map<Long, String> gerencias) {
		this.gerencias = gerencias;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
	}
	
	private void inicializarNegocio(){
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		if(negocio == null){
			negocio = new Negocio();
		}
		negocio.setIdToNegocio(idToNegocio);
		negocio = entidadService.findById(Negocio.class, idToNegocio);
	}
	
	public void prepareMostrarContenedor(){
		inicializarNegocio();
	}
	
	public String mostrarContenedor(){		
		gerencias = listadoService.getMapGerencias();
		oficinas = new LinkedHashMap<Long, String>();
		promotorias = new LinkedHashMap<Long, String>();		
		return SUCCESS;
	}
	
	public String obtenerAgentesAsociados(){
		agentesAsociados = negocioAgenteService.listarNegocioAgentesAsociadosLight(idToNegocio);
		return SUCCESS;
	}
	
	public String obtenerAgentesDisponibles(){	
		if(!UtileriasWeb.esCadenaVacia(gerenciaId) || !UtileriasWeb.esCadenaVacia(oficinaId) || !UtileriasWeb.esCadenaVacia(promotoriaId)){
			agentesDisponibles = negocioAgenteService.listarNegocioAgentesDisponibles(idToNegocio, oficinaId, gerenciaId, promotoriaId);
		}
		return SUCCESS;
	}
	
	public void prepareRelacionarNegocioAgentes(){
		inicializarNegocio();
	}
	
	public String relacionarNegocioAgentes(){
		String accion = ServletActionContext.getRequest().getParameter("accionAgente");
		boolean existe = false;
		int contador = 0;
		String[] idsPorrelacionar = getIdsRelacionar().split(",");
		for(int i = 0; i < idsPorrelacionar.length; i++){
			negocioAgente = new NegocioAgente();
			Long id = Long.valueOf(idsPorrelacionar[i]);
			BigDecimal idnegAgente;
			if(accion.equals("inserted")){
				Agente age = new Agente();
				age.setId(id);
				negocioAgente.setAgente(age);
				NegocioAgente existente = negocioAgenteService.getNegocioAgente(negocio,negocioAgente.getAgente());
				if(existente!= null){
					contador = contador +1;
					existe=true;
				}
				relacionarNegAgentes(accion, negocioAgente, existe, contador);
				existe=false;
			} else {	
				idnegAgente= new BigDecimal(id);
				negocioAgente.setIdToNegAgente(idnegAgente);
				relacionarNegAgentes(accion, negocioAgente, existe, contador);
			}
		}
		return SUCCESS ;
	}

	public void relacionarNegAgentes(String accion, NegocioAgente negocioAgente, boolean existe, int contador){
		if(!existe){
			if(negocioAgente.getIdToNegAgente()!=null){
				negocioAgente = entidadService.findById(NegocioAgente.class, negocioAgente.getIdToNegAgente());
				negocioAgente.getAgente().setId(negocioAgente.getIdTcAgente().longValue());
			}
			negocioAgente.getNegocio().setIdToNegocio((Long)ServletActionContext.getContext().getSession().get("idNegocio"));
			negocioAgenteService.relacionarNegocioAgente(accion, negocioAgente);
		}
		if (contador>0){				
			setMensajeExitoPersonalizado("Acci\u00F3n realizada correctamente pero existe(n) " + contador +" agente(s) que ya esta(n) relacionado(s)");
		}
		else{
			setMensajeExito();
		}
	}

	public String eliminarRelacion(){
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		negocioAgenteService.eliminarTodasLasRelaciones(idToNegocio);
		setMensajeExito();
		return SUCCESS;
	}
	
	
	public String getGerenciaId() {
		return gerenciaId;
	}

	public void setGerenciaId(String gerenciaId) {
		this.gerenciaId = gerenciaId;
	}

	public String getPromotoriaId() {
		return promotoriaId;
	}

	public void setPromotoriaId(String promotoriaId) {
		this.promotoriaId = promotoriaId;
	}

	public String getOficinaId() {
		return oficinaId;
	}

	public void setOficinaId(String oficinaId) {
		this.oficinaId = oficinaId;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public Map<Long, String> getPromotorias() {
		return promotorias;
	}

	public void setPromotorias(Map<Long, String> promotorias) {
		this.promotorias = promotorias;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public NegocioAgente getNegocioAgente() {
		return negocioAgente;
	}

	public void setNegocioAgente(NegocioAgente negocioAgente) {
		this.negocioAgente = negocioAgente;
	}

	public BigDecimal getIdToNegocioAgente() {
		return idToNegocioAgente;
	}

	public void setIdToNegocioAgente(BigDecimal idToNegocioAgente) {
		this.idToNegocioAgente = idToNegocioAgente;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public List<NegocioAgente> getAgentesAsociados() {
		return agentesAsociados;
	}

	public void setAgentesAsociados(List<NegocioAgente> agentesAsociados) {
		this.agentesAsociados = agentesAsociados;
	}

	public List<NegocioAgente> getAgentesDisponibles() {
		return agentesDisponibles;
	}

	public void setAgentesDisponibles(List<NegocioAgente> agentesDisponibles) {
		this.agentesDisponibles = agentesDisponibles;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
