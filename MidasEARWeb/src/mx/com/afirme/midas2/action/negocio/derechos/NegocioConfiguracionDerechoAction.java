package mx.com.afirme.midas2.action.negocio.derechos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.dto.negocio.derechos.NegocioConfiguracionDerechoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioConfiguracionDerechoService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Namespace(value = "/negocio/configuracionderecho")
public class NegocioConfiguracionDerechoAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -7224142731374018266L;
	
	private static final String	LOCATION_CONTENEDOR_CONFIG_DERECHO_JSP	= "/jsp/negocio/producto/derechos/contenedorConfiguracionDerechos.jsp";
	private static final String	LOCATION_LISTADO_CONFIG_DERECHO_GRID_JSP	= "/jsp/negocio/producto/derechos/listadoConfigDerechoGrid.jsp";
	
	private static final String[] COLUMNAS_GRID_CONFIG_DERECHO = 
	{"tipoPoliza.tipoPolizaDTO.descripcion", "seccion.seccionDTO.descripcion", "paquete.paquete.descripcion", "moneda.descripcion", "tipoDerecho", "importeDerecho", "estado.stateName", "municipio.cityName", 
	"tipoUsoVehiculo.descripcionTipoUsoVehiculo"};
	
	
	private Long idToNegProducto;
	private Long idToNegocio;
	private Long idToNegTipoPoliza;
	private Long idToNegSeccion;
	private Long idToPaqueteSeccion;
	private Long idToNegPaqueteSeccion;
	private Long idToNegDerecho;
	private Short idMonedaDTO;
	private String idTipoDerecho;
	private BigDecimal idTcTipoUsoVehiculo;
	private String stateId;
	private String cityId;
	private Long resultadoConteo;
	
	private NegocioConfiguracionDerecho negocioConfigDerecho;
	private NegocioProducto negocioProducto;
	private NegocioConfiguracionDerechoDTO filtroConfigDerecho;
	private List<NegocioConfiguracionDerecho> configuracionesDerecho;
	private List<NegocioConfiguracionDerechoDTO> listadoConfigDerecho = new ArrayList<NegocioConfiguracionDerechoDTO>();
	private List<NegocioTipoPoliza>negocioTipoPolizaList= new ArrayList<NegocioTipoPoliza>();
	private Map<BigDecimal,String> secciones = new LinkedHashMap<BigDecimal,String>(1);
	private Map<Long,String> paquetes = new LinkedHashMap<Long,String>(1);
	private Map<BigDecimal,String> monedaList = new LinkedHashMap<BigDecimal,String>(1);
	private Map<BigDecimal, String> tipoUsoList = new LinkedHashMap<BigDecimal, String>(1);
	private List<EstadoDTO> estadoList = new ArrayList<EstadoDTO>(1);
	private Map<String,String> municipioList = new LinkedHashMap<String,String>(1);
	private Map<String,String> tipoDerechoList;
	private Map<Long,Double> importesDerecho = new LinkedHashMap<Long,Double>(1);
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("negocioConfiguracionDerechoServiceEJB")
	private NegocioConfiguracionDerechoService negocioConfiguracionDerechoService;
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	private NegocioSeccionService negocioSeccionService;
	
	
	
	
	@Override
	public void prepare() throws Exception {
		
	}

	public void prepareMostrarContenedor(){
		if(negocioProducto==null)negocioProducto = new NegocioProducto();
		negocioProducto.setIdToNegProducto(idToNegProducto);
		negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
		if(negocioProducto != null
				&& negocioProducto.getNegocio() != null){
			if(negocioConfigDerecho == null){
				negocioConfigDerecho = new NegocioConfiguracionDerecho();
			}
			negocioConfigDerecho.setNegocio(negocioProducto.getNegocio());
			idToNegocio = negocioProducto.getNegocio().getIdToNegocio();
		}
		tipoDerechoList = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_CONFIG_DERECHO);
		negocioTipoPolizaList = negocioSeccionService.getNegTipoPolizaList(negocioProducto);
		estadoList = listadoService.listarEstadosMX();
		
		if(negocioConfigDerecho.getTipoPoliza() != null
				&& negocioConfigDerecho.getTipoPoliza().getIdToNegTipoPoliza() != null){
			secciones =listadoService.getMapNegocioSeccionPorTipoPoliza(negocioConfigDerecho.getTipoPoliza().getIdToNegTipoPoliza());
			monedaList = listadoService.getMapMonedaPorNegTipoPoliza(negocioConfigDerecho.getTipoPoliza().getIdToNegTipoPoliza());
		}
		if(negocioConfigDerecho.getSeccion() != null
				&& negocioConfigDerecho.getSeccion().getIdToNegSeccion() != null){
			paquetes = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(negocioConfigDerecho.getSeccion().getIdToNegSeccion());
			tipoUsoList = listadoService.getMapTipoUsoVehiculoByNegocio(negocioConfigDerecho.getSeccion().getIdToNegSeccion());
		}
		if(negocioConfigDerecho.getEstado() != null
				&& negocioConfigDerecho.getEstado().getStateId() != null
				&& negocioConfigDerecho.getMunicipio() != null
				&& negocioConfigDerecho.getMunicipio().getCityId() != null){
			municipioList = listadoService.getMapMunicipiosPorEstado(negocioConfigDerecho.getEstado().getStateId());
		}
		
	}
	
	/**
	 * Metodo que se encarga de mostrar la pantalla dentro del dhtmlxTab del
	 * producto del negocio.
	 */
	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDOR_CONFIG_DERECHO_JSP))
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	public void prepareBuscarConfiguracionesDerecho(){
		if(negocioConfigDerecho == null){
			negocioConfigDerecho = new NegocioConfiguracionDerecho();
		}
	}
	
	/**
	 * Metodo que lista las  configuraciones de derecho que ya han sido creadas en el dhtmlxGrid
	 */
	@Action(value = "buscarConfiguracionesDerecho", results = {
		@Result(name = SUCCESS, location = LOCATION_LISTADO_CONFIG_DERECHO_GRID_JSP),
		@Result(name = INPUT, location = LOCATION_CONTENEDOR_CONFIG_DERECHO_JSP)})
	public String buscarConfiguracionesDerecho() {

		prepareBusquedaPaginada();		
		
		listadoConfigDerecho = negocioConfiguracionDerechoService.buscarNegocioConfiguracionDerecho(negocioConfigDerecho);
		
		resultadoConteo = negocioConfiguracionDerechoService.conteoBusquedaConfiguracionesDerecho(negocioConfigDerecho);
		
		if(super.getPosStart().intValue() == 0){
			super.setTotalCount(resultadoConteo);
		}

		return SUCCESS;
	}
	
	private void prepareBusquedaPaginada(){
		if(super.getCount() == null){
			super.setCount(20); 
		}
					
		if(super.getPosStart() == null){
			super.setPosStart(0);
		}
		
		negocioConfigDerecho.setPosStart(super.getPosStart());
		negocioConfigDerecho.setCount(super.getCount());
		negocioConfigDerecho.setOrderByAttribute(decodeOrderBy());
	}
	
	private String decodeOrderBy(){
		String order = null;
		if(!StringUtil.isEmpty(super.getOrderBy())){
			order = COLUMNAS_GRID_CONFIG_DERECHO[Integer.valueOf(super.getOrderBy())];
			if(!StringUtil.isEmpty(super.getDirect())){
				order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
			}
		}
		return order;
	}
	
	public void prepareGuardarConfigDerecho(){
	}
	
	@Action(value = "guardarConfigDerecho", results = { 
			@Result(name = INPUT,  type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace", "/negocio/configuracionderecho",
					"idToNegProducto", "${idToNegProducto}",
					"negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza","${negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza}",
					"negocioConfigDerecho.seccion.idToNegSeccion","${negocioConfigDerecho.seccion.idToNegSeccion}",
					"negocioConfigDerecho.paquete.idToNegPaqueteSeccion","${negocioConfigDerecho.paquete.idToNegPaqueteSeccion}",
					"negocioConfigDerecho.moneda.idTcMoneda","${negocioConfigDerecho.moneda.idTcMoneda}",
					"negocioConfigDerecho.tipoDerecho","${negocioConfigDerecho.tipoDerecho}",
					"negocioConfigDerecho.estado.stateId","${negocioConfigDerecho.estado.stateId}",
					"negocioConfigDerecho.municipio.cityId","${negocioConfigDerecho.municipio.cityId}",
					"negocioConfigDerecho.importeDefault","${negocioConfigDerecho.importeDefault}",
					"negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo","${negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo}",
					"negocioConfigDerecho.idToNegDerecho","${negocioConfigDerecho.idToNegDerecho}",
					"negocioConfigDerecho.importeDerecho","${negocioConfigDerecho.importeDerecho}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace", "/negocio/configuracionderecho",
					"idToNegProducto", "${idToNegProducto}",
					"negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza","${negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza}",
					"negocioConfigDerecho.seccion.idToNegSeccion","${negocioConfigDerecho.seccion.idToNegSeccion}",
					"negocioConfigDerecho.paquete.idToNegPaqueteSeccion","${negocioConfigDerecho.paquete.idToNegPaqueteSeccion}",
					"negocioConfigDerecho.moneda.idTcMoneda","${negocioConfigDerecho.moneda.idTcMoneda}",
					"negocioConfigDerecho.tipoDerecho","${negocioConfigDerecho.tipoDerecho}",
					"negocioConfigDerecho.estado.stateId","${negocioConfigDerecho.estado.stateId}",
					"negocioConfigDerecho.municipio.cityId","${negocioConfigDerecho.municipio.cityId}",
					"negocioConfigDerecho.importeDefault","${negocioConfigDerecho.importeDefault}",
					"negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo","${negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo}",
					"negocioConfigDerecho.idToNegDerecho","${negocioConfigDerecho.idToNegDerecho}",
					"negocioConfigDerecho.importeDerecho","${negocioConfigDerecho.importeDerecho}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) })	
	public String guardarConfigDerecho(){
		try{
			if (negocioConfiguracionDerechoService.guardarNegocioConfiguracionDerecho(negocioConfigDerecho) == null){
				setMensaje("Configuraci\u00F3n del derecho no se puede duplicar");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
			}else{
				setMensaje("Guardado realizado correctamente");
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			}
		}catch(Exception e){
			LOG.error("Error al guardar la configuraci\u00F3n del derecho", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareEliminarConfigDerecho(){
	}
	
	@Action(value = "eliminarConfigDerecho", results = { 
			@Result(name = INPUT,  type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace", "/negocio/configuracionderecho",
					"idToNegProducto", "${idToNegProducto}",
					"negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza","${negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza}",
					"negocioConfigDerecho.seccion.idToNegSeccion","${negocioConfigDerecho.seccion.idToNegSeccion}",
					"negocioConfigDerecho.paquete.idToNegPaqueteSeccion","${negocioConfigDerecho.paquete.idToNegPaqueteSeccion}",
					"negocioConfigDerecho.moneda.idTcMoneda","${negocioConfigDerecho.moneda.idTcMoneda}",
					"negocioConfigDerecho.tipoDerecho","${negocioConfigDerecho.tipoDerecho}",
					"negocioConfigDerecho.estado.stateId","${negocioConfigDerecho.estado.stateId}",
					"negocioConfigDerecho.municipio.cityId","${negocioConfigDerecho.municipio.cityId}",
					"negocioConfigDerecho.importeDefault","${negocioConfigDerecho.importeDefault}",
					"negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo","${negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo}",
					"negocioConfigDerecho.idToNegDerecho","${negocioConfigDerecho.idToNegDerecho}",
					"negocioConfigDerecho.importeDerecho","${negocioConfigDerecho.importeDerecho}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace", "/negocio/configuracionderecho",
					"idToNegProducto", "${idToNegProducto}",
					"negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza","${negocioConfigDerecho.tipoPoliza.idToNegTipoPoliza}",
					"negocioConfigDerecho.seccion.idToNegSeccion","${negocioConfigDerecho.seccion.idToNegSeccion}",
					"negocioConfigDerecho.paquete.idToNegPaqueteSeccion","${negocioConfigDerecho.paquete.idToNegPaqueteSeccion}",
					"negocioConfigDerecho.moneda.idTcMoneda","${negocioConfigDerecho.moneda.idTcMoneda}",
					"negocioConfigDerecho.tipoDerecho","${negocioConfigDerecho.tipoDerecho}",
					"negocioConfigDerecho.estado.stateId","${negocioConfigDerecho.estado.stateId}",
					"negocioConfigDerecho.municipio.cityId","${negocioConfigDerecho.municipio.cityId}",
					"negocioConfigDerecho.importeDefault","${negocioConfigDerecho.importeDefault}",
					"negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo","${negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo}",
					"negocioConfigDerecho.idToNegDerecho","${negocioConfigDerecho.idToNegDerecho}",
					"negocioConfigDerecho.importeDerecho","${negocioConfigDerecho.importeDerecho}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) })	
	public String eliminarConfigDerecho(){
		try{
			negocioConfiguracionDerechoService.eliminarNegocioConfiguracionDerecho(negocioConfigDerecho);
		}catch(Exception e){
			LOG.error("Error al eliminar la configuraci\u00F3n del derecho", e);
			setMensaje("Error al guardar la informacion favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		setMensaje("Elemento eliminado correctamente");
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	

	public NegocioConfiguracionDerechoDTO getFiltroConfigDerecho() {
		return filtroConfigDerecho;
	}

	public void setFiltroConfigDerecho(
			NegocioConfiguracionDerechoDTO filtroConfigDerecho) {
		this.filtroConfigDerecho = filtroConfigDerecho;
	}

	public List<NegocioConfiguracionDerecho> getConfiguracionesDerecho() {
		return configuracionesDerecho;
	}

	public void setConfiguracionesDerecho(
			List<NegocioConfiguracionDerecho> configuracionesDerecho) {
		this.configuracionesDerecho = configuracionesDerecho;
	}

	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setNegocioTipoPolizaList(
			List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public Map<BigDecimal, String> getSecciones() {
		return secciones;
	}

	public void setSecciones(Map<BigDecimal, String> secciones) {
		this.secciones = secciones;
	}

	public Map<Long, String> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(Map<Long, String> paquetes) {
		this.paquetes = paquetes;
	}

	public Map<BigDecimal, String> getMonedaList() {
		return monedaList;
	}

	public void setMonedaList(Map<BigDecimal, String> monedaList) {
		this.monedaList = monedaList;
	}

	public List<EstadoDTO> getEstadoList() {
		return estadoList;
	}

	public void setEstadoList(List<EstadoDTO> estadoList) {
		this.estadoList = estadoList;
	}



	public Map<String, String> getMunicipioList() {
		return municipioList;
	}

	public void setMunicipioList(Map<String, String> municipioList) {
		this.municipioList = municipioList;
	}

	public Map<String, String> getTipoDerechoList() {
		return tipoDerechoList;
	}

	public void setTipoDerechoList(Map<String, String> tipoDerechoList) {
		this.tipoDerechoList = tipoDerechoList;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public Long getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegTipoPoliza(Long idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public Long getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(Long idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public Long getIdToPaqueteSeccion() {
		return idToPaqueteSeccion;
	}

	public void setIdToPaqueteSeccion(Long idToPaqueteSeccion) {
		this.idToPaqueteSeccion = idToPaqueteSeccion;
	}

	public Long getIdToNegPaqueteSeccion() {
		return idToNegPaqueteSeccion;
	}

	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}

	public Short getIdMonedaDTO() {
		return idMonedaDTO;
	}

	public void setIdMonedaDTO(Short idMonedaDTO) {
		this.idMonedaDTO = idMonedaDTO;
	}

	public String getIdTipoDerecho() {
		return idTipoDerecho;
	}

	public void setIdTipoDerecho(String idTipoDerecho) {
		this.idTipoDerecho = idTipoDerecho;
	}

	public BigDecimal getIdTcTipoUsoVehiculo() {
		return idTcTipoUsoVehiculo;
	}

	public void setIdTcTipoUsoVehiculo(BigDecimal idTcTipoUsoVehiculo) {
		this.idTcTipoUsoVehiculo = idTcTipoUsoVehiculo;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public Map<BigDecimal, String> getTipoUsoList() {
		return tipoUsoList;
	}

	public void setTipoUsoList(Map<BigDecimal, String> tipoUsoList) {
		this.tipoUsoList = tipoUsoList;
	}

	public List<NegocioConfiguracionDerechoDTO> getListadoConfigDerecho() {
		return listadoConfigDerecho;
	}

	public void setListadoConfigDerecho(
			List<NegocioConfiguracionDerechoDTO> listadoConfigDerecho) {
		this.listadoConfigDerecho = listadoConfigDerecho;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public Map<Long, Double> getImportesDerecho() {
		return importesDerecho;
	}

	public void setImportesDerecho(Map<Long, Double> importesDerecho) {
		this.importesDerecho = importesDerecho;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public Long getIdToNegDerecho() {
		return idToNegDerecho;
	}

	public void setIdToNegDerecho(Long idToNegDerecho) {
		this.idToNegDerecho = idToNegDerecho;
	}

	public NegocioConfiguracionDerecho getNegocioConfigDerecho() {
		return negocioConfigDerecho;
	}

	public void setNegocioConfigDerecho(
			NegocioConfiguracionDerecho negocioConfigDerecho) {
		this.negocioConfigDerecho = negocioConfigDerecho;
	}

	public Long getResultadoConteo() {
		return resultadoConteo;
	}

	public void setResultadoConteo(Long resultadoConteo) {
		this.resultadoConteo = resultadoConteo;
	}

	
	
}
