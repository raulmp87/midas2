package mx.com.afirme.midas2.action.negocio.producto.tipopoliza;
import java.util.List;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.opensymphony.xwork2.Preparable;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.paquete.tipopoliza.RelacionesNegocioTipoPolizaDTO;

import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;

@Component
@Scope("prototype")
public class NegocioTipoPolizaAction extends BaseAction implements Preparable {

    private static final long serialVersionUID = -4520127088974367105L;


	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		 //BidDecimal IdToNegProducto debe de estar disponible en los hidden del div recargado.
		if (negocioProducto==null) negocioProducto = new NegocioProducto();
		  if(getIdToNegProducto()!=null){
		    negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
		  }
	}
	
	public String mostrar(){
		//tipopoliza = negocioTipoPolizaService.getListarNegocioTipopoliza();
		return SUCCESS;
	}

	public void prepareObtenerRelaciones(){

	}

	public String obtenerRelaciones(){
		relacionesNegocioTipoPolizaDTO = negocioTipoPolizaService.getRelationLists(negocioProducto);
		return SUCCESS;
	}
	
	public String relacionarNegocioTipoPoliza(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		 negocioTipoPolizaService.relacionarNegocioTipoPoliza(accion,negocioTipoPoliza);
		return SUCCESS;
	}

	private List<TipoPolizaDTO> tipopoliza;
	private NegocioTipoPolizaService negocioTipoPolizaService;
	private NegocioProducto negocioProducto;
	private NegocioTipoPoliza negocioTipoPoliza;
	private Long idToNegProducto;
	private EntidadService entidadService;
	private RespuestaGridRelacionDTO respuesta;
	private RelacionesNegocioTipoPolizaDTO relacionesNegocioTipoPolizaDTO;	
	
	
	public List<TipoPolizaDTO> getTipopoliza() {
		return tipopoliza;
	}

	public void setTipopoliza(List<TipoPolizaDTO> tipopoliza) {
		this.tipopoliza = tipopoliza;
	}

    public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}
	
	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}	

	public RelacionesNegocioTipoPolizaDTO getRelacionesNegocioTipoPolizaDTO() {
		return relacionesNegocioTipoPolizaDTO;
	}

	public void setRelacionesNegocioTipoPolizaDTO(
			RelacionesNegocioTipoPolizaDTO relacionesNegocioTipoPolizaDTO) {
		this.relacionesNegocioTipoPolizaDTO = relacionesNegocioTipoPolizaDTO;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}
	
    public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

@Autowired
@Qualifier("negocioTipoPolizaServiceEJB")
	public void setNegocioTipoPolizaService(
			NegocioTipoPolizaService negocioTipoPolizaService) {
		this.negocioTipoPolizaService = negocioTipoPolizaService;
	}

@Autowired
@Qualifier("entidadEJB")
public void setEntidadService(EntidadService entidadService) {
	this.entidadService = entidadService;
}

}
