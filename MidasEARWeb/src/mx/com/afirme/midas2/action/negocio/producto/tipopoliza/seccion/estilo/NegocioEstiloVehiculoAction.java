package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion.estilo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.dto.negocio.estilovehiculo.RelacionesNegocioEstiloVehiculoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculoService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioEstiloVehiculoAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private NegocioSeccion negocioSeccion;
	private NegocioEstiloVehiculo negocioEstiloVehiculo;
	private Map<BigDecimal, String> marcas;
	private Map<BigDecimal, String> tipos;
	private Map<BigDecimal, String> monedas;
	private BigDecimal marca;
	private BigDecimal tipo;
	private BigDecimal idMoneda;
	private RelacionesNegocioEstiloVehiculoDTO relacionesNegocioEstiloVehiculoDTO;

	@Override
	public void prepare() throws Exception {
		if(getId() != null){
			negocioSeccion = entidadService.getReference(NegocioSeccion.class, getId());
		}
	}

	public String mostrarVentana(){
		marcas = listadoService.listarMarcasPorCveTipoBien(negocioSeccion
				.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		tipos = listadoService.listarTiposVehiculo(negocioSeccion
				.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		monedas = listadoService.getMapMonedaPorNegTipoPoliza(negocioSeccion.getNegocioTipoPoliza().getIdToNegTipoPoliza());
		return SUCCESS;
	}
	
	public String buscarEstilos(){
		if(getMarca()!= null && getTipo()!=null && idMoneda != null){
			if(relacionesNegocioEstiloVehiculoDTO == null)
				relacionesNegocioEstiloVehiculoDTO = new RelacionesNegocioEstiloVehiculoDTO();
			relacionesNegocioEstiloVehiculoDTO.setAsociadas(negocioEstiloVehiculoService.getEstiloVehiculoAsociados(negocioSeccion, marca, tipo));
			relacionesNegocioEstiloVehiculoDTO.setDisponibles(negocioEstiloVehiculoService.getEstiloVehiculoNoAsociados(negocioSeccion, marca, tipo, idMoneda));
		}else{
			setRelacionesNegocioEstiloVehiculoDTO(negocioEstiloVehiculoService.getRelationList(negocioSeccion));
			relacionesNegocioEstiloVehiculoDTO.setDisponibles(new ArrayList<NegocioEstiloVehiculo>());			
		}
		return SUCCESS;
	}	
	public String relacionarSeccionEstilo(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		negocioEstiloVehiculoService.relacionarNegocioEstiloVehiculo(accion, negocioEstiloVehiculo);
		return SUCCESS;
	}

	private EntidadService entidadService;
	private ListadoService listadoService;
	private NegocioEstiloVehiculoService negocioEstiloVehiculoService;
	
	@Autowired
	@Qualifier("negocioEstiloVehiculoServiceEJB")		
	public void setNegocioEstiloVehiculoService(
			NegocioEstiloVehiculoService negocioEstiloVehiculoService) {
		this.negocioEstiloVehiculoService = negocioEstiloVehiculoService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")		
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("entidadEJB")	
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	public BigDecimal getId() {
		return id;
	}


	public void setId(BigDecimal id) {
		this.id = id;
	}


	public Map<BigDecimal, String> getMarcas() {
		return marcas;
	}


	public void setMarcas(Map<BigDecimal, String> marcas) {
		this.marcas = marcas;
	}


	public Map<BigDecimal, String> getTipos() {
		return tipos;
	}


	public void setTipos(Map<BigDecimal, String> tipos) {
		this.tipos = tipos;
	}


	public NegocioEstiloVehiculo getNegocioEstiloVehiculo() {
		return negocioEstiloVehiculo;
	}


	public void setNegocioEstiloVehiculo(NegocioEstiloVehiculo negocioEstiloVehiculo) {
		this.negocioEstiloVehiculo = negocioEstiloVehiculo;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public RelacionesNegocioEstiloVehiculoDTO getRelacionesNegocioEstiloVehiculoDTO() {
		return relacionesNegocioEstiloVehiculoDTO;
	}

	public void setRelacionesNegocioEstiloVehiculoDTO(
			RelacionesNegocioEstiloVehiculoDTO relacionesNegocioEstiloVehiculoDTO) {
		this.relacionesNegocioEstiloVehiculoDTO = relacionesNegocioEstiloVehiculoDTO;
	}

	public BigDecimal getMarca() {
		return marca;
	}

	public void setMarca(BigDecimal marca) {
		this.marca = marca;
	}

	public BigDecimal getTipo() {
		return tipo;
	}

	public void setTipo(BigDecimal tipo) {
		this.tipo = tipo;
	}

	public void setMonedas(Map<BigDecimal, String> monedas) {
		this.monedas = monedas;
	}

	public Map<BigDecimal, String> getMonedas() {
		return monedas;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public BigDecimal getIdMoneda() {
		return idMoneda;
	}

}

