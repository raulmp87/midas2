package mx.com.afirme.midas2.action.negocio.renovacion;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCobertura;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionId;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioRenovacionAction extends BaseAction implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7508883898769528235L;
	
	private Short condicionTipo;
	private Long idNegocio;
	private String tipoAccion;
	private String claveNegocio;
	private BigDecimal idToNegSeccion;
	
	private Negocio negocio;
	private NegocioRenovacion negocioRenovacion;
	private List<NegocioRenovacionDesc> descuentosList = new ArrayList<NegocioRenovacionDesc>(1);
	private List<NegocioRenovacionDesc> deduciblesList = new ArrayList<NegocioRenovacionDesc>(1);
	private List<NegocioSeccion> negSeccionList = new ArrayList<NegocioSeccion>(1);
	private List<NegocioRenovacionCobertura> negCobPaqSeccionList = new ArrayList<NegocioRenovacionCobertura>(1);
	private NegocioRenovacionCobertura negocioRenovacionCobertura;
	private RespuestaGridRelacionDTO respuesta;
	
	private EntidadService entidadService;
	private NegocioSeccionService negocioSeccionService;
	private NegocioCobPaqSeccionService negocioCobPaqSeccionService;
	
	@Autowired
	@Qualifier("negocioCobPaqSeccionServiceEJB")
	public void setNegocioCobPaqSeccionService(
			NegocioCobPaqSeccionService negocioCobPaqSeccionService) {
		this.negocioCobPaqSeccionService = negocioCobPaqSeccionService;
	}	
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(
			NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void prepare() throws Exception {
		if(idNegocio != null){
			setNegocio(entidadService.findById(Negocio.class, idNegocio));
		}
	}
	
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	public void prepareMostrarCondicionesRenovacion(){
		if(idNegocio != null && condicionTipo != null){
			NegocioRenovacionId id = new NegocioRenovacionId(idNegocio, condicionTipo);
			negocioRenovacion = entidadService.findById(NegocioRenovacion.class, id);
			if(negocioRenovacion == null){
				negocioRenovacion = new NegocioRenovacion();
				negocioRenovacion.setId(id);
			}
			
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("id.idToNegocio", idNegocio);
			parametros.put("id.tipoRiesgo", condicionTipo);
			parametros.put("id.tipoDescuento", NegocioRenovacionDesc.TipoDescuento.DESCUENTO.getObtenerTipo());
			descuentosList = entidadService.findByProperties(NegocioRenovacionDesc.class, parametros);
			if(descuentosList == null || descuentosList.size() == 0){
				descuentosList = new ArrayList<NegocioRenovacionDesc>(1);
				for(int i = 0; i <= 7; i++){
					NegocioRenovacionDescId negocioRenovacionDescId = new NegocioRenovacionDescId(idNegocio, condicionTipo, NegocioRenovacionDesc.TipoDescuento.DESCUENTO.getObtenerTipo(), (short) i);
					NegocioRenovacionDesc negocioRenovacionDesc = new NegocioRenovacionDesc();
					negocioRenovacionDesc.setId(negocioRenovacionDescId);
					descuentosList.add(negocioRenovacionDesc);
				}				
			}
			
			Map<String, Object> parametros2 = new LinkedHashMap<String, Object>();
			parametros2.put("id.idToNegocio", idNegocio);
			parametros2.put("id.tipoRiesgo", condicionTipo);
			parametros2.put("id.tipoDescuento", NegocioRenovacionDesc.TipoDescuento.DEDUCIBLE.getObtenerTipo());
			deduciblesList = entidadService.findByProperties(NegocioRenovacionDesc.class, parametros2);
			if(deduciblesList == null  || deduciblesList.size() == 0){
				deduciblesList = new ArrayList<NegocioRenovacionDesc>(1);
				for(int i = 0; i <= 7; i++){
					NegocioRenovacionDescId negocioRenovacionDescId = new NegocioRenovacionDescId(idNegocio, condicionTipo, NegocioRenovacionDesc.TipoDescuento.DEDUCIBLE.getObtenerTipo(), (short) i);
					NegocioRenovacionDesc negocioRenovacionDesc = new NegocioRenovacionDesc();
					negocioRenovacionDesc.setId(negocioRenovacionDescId);
					deduciblesList.add(negocioRenovacionDesc);
				}				
			}
			
			//Carga lineas por negocio
			negSeccionList = negocioSeccionService.listarNegSeccionByIdNegocio(idNegocio);
		}
	}
	
	
	public String mostrarCondicionesRenovacion() {
		if(negocioRenovacion == null){
			negocioRenovacion = new NegocioRenovacion();
		}
		return SUCCESS;
	}
	
	public void prepareGuardarCondicionesRenovacion(){
		
	}
	
	public String guardarCondicionesRenovacion(){
		if(negocioRenovacion != null){
			idNegocio = negocioRenovacion.getId().getIdToNegocio();
			entidadService.save(negocioRenovacion);
		}
		if(descuentosList != null){
			for(NegocioRenovacionDesc item: descuentosList){
				entidadService.save(item);
			}
		}else{
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("id.idToNegocio", idNegocio);
			parametros.put("id.tipoRiesgo", condicionTipo);
			parametros.put("id.tipoDescuento", NegocioRenovacionDesc.TipoDescuento.DESCUENTO.getObtenerTipo());
			List<NegocioRenovacionDesc>  descuentosRemList = entidadService.findByProperties(NegocioRenovacionDesc.class, parametros);
			if(descuentosRemList == null || descuentosRemList.isEmpty()){
				for(NegocioRenovacionDesc item : descuentosRemList){
					try{
						entidadService.remove(item);
					}catch(Exception e){
					}
				}
			}
		}
		if(deduciblesList != null){
			for(NegocioRenovacionDesc item: deduciblesList){
				entidadService.save(item);
			}
		}else{
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("id.idToNegocio", idNegocio);
			parametros.put("id.tipoRiesgo", condicionTipo);
			parametros.put("id.tipoDescuento", NegocioRenovacionDesc.TipoDescuento.DEDUCIBLE.getObtenerTipo());
			List<NegocioRenovacionDesc>  descuentosRemList = entidadService.findByProperties(NegocioRenovacionDesc.class, parametros);
			if(descuentosRemList == null || descuentosRemList.isEmpty()){
				for(NegocioRenovacionDesc item : descuentosRemList){
					try{
						entidadService.remove(item);
					}catch(Exception e){
					}
				}
			}
		}
		this.setMensajeExito();
		return SUCCESS;
	}
	
	public String mostrarCoberturasCondRen(){		
		negCobPaqSeccionList = negocioCobPaqSeccionService.getCoberturasPorNegSeccion(idNegocio, condicionTipo, idToNegSeccion);
		return SUCCESS;
	}
	
	public String guardarCoberturasCondRen(){
		if(negocioRenovacionCobertura != null){
			//String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
			entidadService.save(negocioRenovacionCobertura);
			respuesta = new RespuestaGridRelacionDTO();
			respuesta.setTipoMensaje("30");
		}
		return SUCCESS;
	}

	public void setCondicionTipo(Short condicionTipo) {
		this.condicionTipo = condicionTipo;
	}

	public Short getCondicionTipo() {
		return condicionTipo;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setNegocioRenovacion(NegocioRenovacion negocioRenovacion) {
		this.negocioRenovacion = negocioRenovacion;
	}

	public NegocioRenovacion getNegocioRenovacion() {
		return negocioRenovacion;
	}

	public void setDescuentosList(List<NegocioRenovacionDesc> descuentosList) {
		this.descuentosList = descuentosList;
	}

	public List<NegocioRenovacionDesc> getDescuentosList() {
		return descuentosList;
	}

	public void setDeduciblesList(List<NegocioRenovacionDesc> deduciblesList) {
		this.deduciblesList = deduciblesList;
	}

	public List<NegocioRenovacionDesc> getDeduciblesList() {
		return deduciblesList;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegSeccionList(List<NegocioSeccion> negSeccionList) {
		this.negSeccionList = negSeccionList;
	}

	public List<NegocioSeccion> getNegSeccionList() {
		return negSeccionList;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setNegCobPaqSeccionList(List<NegocioRenovacionCobertura> negCobPaqSeccionList) {
		this.negCobPaqSeccionList = negCobPaqSeccionList;
	}

	public List<NegocioRenovacionCobertura> getNegCobPaqSeccionList() {
		return negCobPaqSeccionList;
	}

	public void setNegocioRenovacionCobertura(NegocioRenovacionCobertura negocioRenovacionCobertura) {
		this.negocioRenovacionCobertura = negocioRenovacionCobertura;
	}

	public NegocioRenovacionCobertura getNegocioRenovacionCobertura() {
		return negocioRenovacionCobertura;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

}
