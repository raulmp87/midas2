package mx.com.afirme.midas2.action.negocio;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioAction extends CatalogoAction implements Preparable {

	private List<Negocio> negocioList;
	private NegocioService negocioService;
	private Negocio negocio;
	private String claveNegocio;
	private Long id;
	private String tipoAccion;
	private Short claveEstatus;
	private Short tipoPersona;
	private Long idToNegocio;
	private String nombreNegocio;
	private boolean copiarEnCascada;
	private TipoAccionDTO tipoAccionDTO;
	private Date fechaFinVigencia;
	private String tabActiva;
	private String nivelActivo;
	private List<NegocioCliente> listaNegocioCliente;
	private UsuarioService usuarioService;
	private Usuario usuario = null;
	private static final long serialVersionUID = 1L;
	private String usuarioCreacion = new String();
	private ListadoService listadoService;
	private boolean negocioTieneVigenciasAsociadas = false;
	
	public NegocioAction() {
		tipoAccionDTO = new TipoAccionDTO();
	}
	
	public TipoAccionDTO getTipoAccionDTO() {
		return tipoAccionDTO;
	}

	public void setTipoAccionDTO(TipoAccionDTO tipoAccionDTO) {
		this.tipoAccionDTO = tipoAccionDTO;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio.trim();
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTipoAccion() {
		return tipoAccion;
	}
	
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public Short getClaveEstatus() {
		return claveEstatus;
	}

	public void setClaveEstatus(Short claveEstatus) {
		this.claveEstatus = claveEstatus;
	}
	
	public void setTipoPersona(Short tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Short getTipoPersona() {
		return tipoPersona;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public String getNombreNegocio() {
		return nombreNegocio;
	}

	public void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}

	public boolean isCopiarEnCascada() {
		return copiarEnCascada;
	}

	public void setCopiarEnCascada(boolean copiarEnCascada) {
		this.copiarEnCascada = copiarEnCascada;
	}
	

	public Date getFechaFinVigencia() {
		return fechaFinVigencia;
	}

	public void setFechaFinVigencia(Date fechaFinVigencia) {
		this.fechaFinVigencia = fechaFinVigencia;
	}	
	

	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}
	
	@Override
	public void prepare() throws Exception {

	}

	public void validateGuardar(){
		addErrors(negocio, NewItemChecks.class, this, "negocio");
	}
	
	public void validateEditar(){
		addErrors(negocio, EditItemChecks.class, this, "negocio");
	}
	
	
	public void prepareGuardar(){
		obtenerNegocioPorIdSession();
		try{
			negocio.setIdToNegocio((Long)ServletActionContext.getContext().getSession().get("idNegocio"));
			claveNegocio = (String)ServletActionContext.getContext().getSession().get("claveNegocio");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public String guardar() {
		tipoAccion = (String)ServletActionContext.getContext().getSession().get("tipoAccion");
			if(!negocioService.isDuplicado(negocio) ){
				negocio.setClaveNegocio(claveNegocio);
				id = negocioService.guardar(negocio, tipoAccion);
				negocio = entidadService.findById(Negocio.class, id);
				super.setMensajeExito();
				ServletActionContext.getContext().getSession().put("idNegocio", negocio.getIdToNegocio());
			}else{
				super.setMensaje(getText("error.duplicado.negocio", new String[]{negocio.getDescripcionNegocio()}));
			}

		return SUCCESS;
	}
	
	
	public void prepareEditar(){
		obtenerNegocioPorIdSession();
		try{
			negocio.setIdToNegocio((Long)ServletActionContext.getContext().getSession().get("idNegocio"));
			claveNegocio = (String)ServletActionContext.getContext().getSession().get("claveNegocio");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String editar(){
		negocio.setClaveNegocio(claveNegocio);
	    id = negocioService.guardar(negocio, null);
	    try{
	    	negocio = entidadService.findById(Negocio.class, id);
	    }catch(RuntimeException e){
	    	super.setMensajeError(e.getMessage());
	    }
		
		super.setMensajeExito();
		ServletActionContext.getContext().getSession().put("idNegocio", negocio.getIdToNegocio());
			return SUCCESS;
	}

	public void prepareActivar(){
		id = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		negocio = entidadService.findById(Negocio.class, id);
		tipoAccion = (String)ServletActionContext.getContext().getSession().get("tipoAccion");
	}

	public String activar(){
		List<String> validaciones = negocioService.activarNegocio(id);
		System.out.println(validaciones.size());
		if(validaciones.isEmpty()){
			
			super.setMensajeExitoPersonalizado("Este Negocio se ha aprobado con \u00E9xito, a partir de este momento est\u00E1 disponible para las condiciones configuradas de negocio.");
			
		}else{				
			super.setMensajeListaPersonalizado(
					"No es posible aprobar esta negociaci\u00f3n ya que no se ha revisado completamente la configuraci\u00f3n de la misma:", 
					validaciones, BaseAction.TIPO_MENSAJE_ERROR);
		}
		return SUCCESS;
	}
	
	public void prepareVerDetalle(){

		if(negocio==null)negocio = new Negocio();
		if (getId() != null) {
			negocio = entidadService.findById(Negocio.class, getId());
			ServletActionContext.getContext().getSession().put("idNegocio", id);
			if(this.getClaveNegocio() == null){
				this.setClaveNegocio(negocio.getClaveNegocio());
			}
		}
		
		if(getTipoAccion() != null){
			ServletActionContext.getContext().getSession().put("tipoAccion", getTipoAccion());
		}else{
			setTipoAccion("2");
			ServletActionContext.getContext().getSession().put("tipoAccion", getTipoAccion());
		}
	}
	
	public String verDetalle() {		
		return SUCCESS;
	}

	public String mostrarCopiarNegocio(){
		return SUCCESS;
	}
	
	public void prepareMostrarInfoGeneral(){
		obtenerNegocioPorIdSession();
		tipoAccion = (String)ServletActionContext.getContext().getSession().get("tipoAccion");
	}
	
	public String mostrarInfoGeneral(){
		if (negocio != null && negocio.getCodigoUsuarioActivacion() !=  null) {
			usuario = usuarioService.buscarUsuarioPorNombreUsuario(negocio
					.getCodigoUsuarioActivacion());
			usuarioCreacion = new String();
			if (usuario != null && usuario.getNombreCompleto() != null) {
				usuarioCreacion = usuario.getNombreCompleto();
			} else if (usuario != null) {
				usuarioCreacion = usuario.getNombre();
			} else {
				usuarioCreacion = "MIDAS";
			}
		}
		return SUCCESS;
	}
	
	public void prepareVerParametrosGenerales(){
		obtenerNegocioPorIdSession();
		if(negocio.getAplicaPctPagoFraccionado() == null){
			negocio.setAplicaPctPagoFraccionado(true);
		}
		claveNegocio = (String)ServletActionContext.getContext().getSession().get("claveNegocio");
		
		//Validar si el negocio tiene vigencias asociadas y deshabilitar el campo Fecha fin vigencia fija
		if(listadoService.obtenerTiposVigenciaNegocio(negocio.getIdToNegocio()).size() > 0 ){
			negocioTieneVigenciasAsociadas = true;
		}
	}
	
	public String verParametrosGenerales(){
		return SUCCESS;
	}

	
	public String copiarNegocio(){	
		try{
			
			Calendar now    =Calendar.getInstance();
			Calendar toDate =Calendar.getInstance();
			
			toDate.setTime(negocio.getFechaFinVigencia());
			fechaFinVigencia=negocio.getFechaFinVigencia();
		    
		    if(toDate.compareTo(now)==-1){
		     setMensaje("La fecha de Vencimiento debe de ser a Futuro");
		     return INPUT;
		    }
		    negocio = entidadService.findById(Negocio.class, idToNegocio);
		 if(!negocioService.isDuplicado(getNombreNegocio())){	
			 List<String> validaciones=negocioService.copiarNegocio(idToNegocio, nombreNegocio, copiarEnCascada,fechaFinVigencia);
			 if(validaciones.isEmpty()){	 
			    super.setMensajeExito();
			    return SUCCESS;
			  }else{				
					  super.setMensajeListaPersonalizado(
							"No es posible aprobar esta copia ya que el estatus no es correcto:",  
							validaciones, BaseAction.TIPO_MENSAJE_ERROR); 
			  }
		 }else{
			 super.setMensaje(getText("error.duplicado.negocio", new String[]{negocio.getDescripcionNegocio()}));
			 return ERROR;
		 }
		}catch (Exception e){
			
		}
		
		return SUCCESS;
	}
	
	public void prepareEliminar(){
		obtenerNegocioPorIdSession();
	}
	
	@Override
	public String eliminar() {	
		try{
			List<String> errores = negocioService.eliminarNegocio(idToNegocio);
			if(!errores.isEmpty()){
				super.setMensajeListaPersonalizado("No pudo eliminarse el negocio", errores, BaseAction.TIPO_MENSAJE_ERROR);
			}else{
				super.setMensajeExito();
			}
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_ELIMINAR);
		}
		return SUCCESS;
	}
	
	
	public void prepareListarFiltrado(){
		if(getClaveNegocio() != null){
			ServletActionContext.getContext().getSession().put("claveNegocio", getClaveNegocio());
			ServletActionContext.getContext().getSession().remove("idNegocio");
		}
	}

	public String listarFiltrado() {
		negocioList = negocioService.listarNegociosNoBorrados(claveNegocio);
		//Sort
		if(negocioList != null && !negocioList.isEmpty()){
			Collections.sort(negocioList, 
					new Comparator<Negocio>() {				
						public int compare(Negocio n1, Negocio n2){
							return n2.getIdToNegocio().compareTo(n1.getIdToNegocio());
						}
					});
		}
		return SUCCESS;
	}

	public String listar() {
		return SUCCESS;
	}
	
	
	
	/**
	 * Lista los clientes asociados a un negocio 
	 * que esten en grupos o clientes individuales
	 * @param idToNegocio
	 * @return grid con la union de los dos tipos cientes iindividuales y grupo de clinetes
	 * @author martin
	 */
	public String getClientesAsociados() {
		listaNegocioCliente = negocioService.listarClientesAsociados(idToNegocio);
		return SUCCESS;
	} 	
	
	private void obtenerNegocioPorIdSession(){
		id = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
		if (getId() != null) {
			negocio = entidadService.findById(Negocio.class, getId());			
		}
	}
	
	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public String getNivelActivo() {
		return nivelActivo;
	}

	public void setNivelActivo(String nivelActivo) {
		this.nivelActivo = nivelActivo;
	}

	public List<NegocioCliente> getListaNegocioCliente() {
		return listaNegocioCliente;
	}

	public void setListaNegocioCliente(List<NegocioCliente> listaNegocioCliente) {
		this.listaNegocioCliente = listaNegocioCliente;
	}


	/**
	 * @param usuarioService the usuarioService to set
	 */
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	/**
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the negocioTieneVigenciasAsociadas
	 */
	public boolean isNegocioTieneVigenciasAsociadas() {
		return negocioTieneVigenciasAsociadas;
	}

	/**
	 * @param negocioTieneVigenciasAsociadas the negocioTieneVigenciasAsociadas to set
	 */
	public void setNegocioTieneVigenciasAsociadas(
			boolean negocioTieneVigenciasAsociadas) {
		this.negocioTieneVigenciasAsociadas = negocioTieneVigenciasAsociadas;
	}
	
}
