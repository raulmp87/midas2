package mx.com.afirme.midas2.action.negocio.compensacionadicional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.dto.compensaciones.FiltroCompensacion;
import mx.com.afirme.midas2.service.compensaciones.CompensacionesAdicionalesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class CompensacionAdicionalAction extends BaseAction{

	private CompensacionesAdicionalesService compensacionesAdicionalesService;
	private CaCompensacion compensacionAdicional ;
	private List<CaCompensacion> listaCompensaciones;
	private List<CaParametros> listaParametros;
	private FiltroCompensacion filtroCompensacion;
	
	public String mostrar(){
		try{
			compensacionAdicional = new CaCompensacion();
			listaCompensaciones = compensacionesAdicionalesService.buscarCompensaciones(filtroCompensacion);
			if (listaCompensaciones != null && listaCompensaciones.size() > 0 ) {
				if (listaCompensaciones.size()  == 1 ) {
					compensacionAdicional = listaCompensaciones.get(0);
				}else{
					
				}
			}
		}catch(Exception ex){
			
		}
		return SUCCESS;
	}
	
	public String mostrarContenedorResumen(){
		try{
			compensacionAdicional = new CaCompensacion();
			listaCompensaciones = compensacionesAdicionalesService.buscarCompensaciones(filtroCompensacion);
			if (listaCompensaciones != null && listaCompensaciones.size() > 0 ) {
				if (listaCompensaciones.size()  == 1 ) {
					compensacionAdicional = listaCompensaciones.get(0);
				}else{
					
				}
			}
		}catch(Exception ex){
			
		}
		return SUCCESS;
	}
	
	public String mostrarResumen(){
		try{
			listaParametros = compensacionesAdicionalesService.buscarParametrosGenerales(filtroCompensacion);
			if (listaParametros == null ){
				listaParametros = new ArrayList<CaParametros>();
			}
		}catch(Exception ex){
			
		}
		return SUCCESS;
	}
	
	public String mostrarConfiguracion(){
		return SUCCESS;
	}
	
	@Autowired
	@Qualifier("compensacionesAdicionalesServiceEJB")
	public void setCompensacionesAdicionalesService(
			CompensacionesAdicionalesService compensacionesAdicionalesService) {
		this.compensacionesAdicionalesService = compensacionesAdicionalesService;
	}

	public CaCompensacion getCompensacionAdicional() {
		return compensacionAdicional;
	}

	public void setCompensacionAdicional(CaCompensacion compensacionAdicional) {
		this.compensacionAdicional = compensacionAdicional;
	}
	
	public List<CaCompensacion> getListaCompensaciones() {
		return listaCompensaciones;
	}

	public void setListaCompensaciones(List<CaCompensacion> listaCompensaciones) {
		this.listaCompensaciones = listaCompensaciones;
	}

	public List<CaParametros> getListaParametros() {
		return listaParametros;
	}

	public void setListaParametros(List<CaParametros> listaParametros) {
		this.listaParametros = listaParametros;
	}

	public FiltroCompensacion getFiltroCompensacion() {
		return filtroCompensacion;
	}

	public void setFiltroCompensacion(FiltroCompensacion filtroCompensacion) {
		this.filtroCompensacion = filtroCompensacion;
	}
	
	
}
