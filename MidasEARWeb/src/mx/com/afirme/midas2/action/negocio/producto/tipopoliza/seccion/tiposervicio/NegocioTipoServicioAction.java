package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion.tiposervicio;
import java.math.BigDecimal;
import java.util.List;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import com.opensymphony.xwork2.Preparable;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.seccion.tiposervicio.RelacionesNegocioTipoServicioDTO;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioService;

@Component
@Scope("prototype")
public class NegocioTipoServicioAction extends BaseAction implements Preparable {

    private static final long serialVersionUID = -4520127088974367105L;
	private int claveDefault = 0;




	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}
	
	public String mostrar(){
		return SUCCESS;
	}

	public void prepareObtenerRelaciones(){
		 //BidDecimal IdToNegSeccion debe de estar disponible en los hidden del div recargado.
		if (negocioSeccion==null) negocioSeccion = new NegocioSeccion();
		  if(getIdToNegSeccion()!=null){
			  negocioSeccion = entidadService.findById(NegocioSeccion.class, getIdToNegSeccion());
		  }
	}

	public String obtenerRelaciones(){
		relacionesNegocioTipoServicioDTO = negocioTipoServicioService.getRelationLists(negocioSeccion);
		return SUCCESS;
	}
	
	public String relacionarNegocioTipoServicio(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(claveDefault == 1){
			negocioTipoServicio.setClaveDefault(true);
		}else{
			negocioTipoServicio.setClaveDefault(false);
		}		
		negocioTipoServicioService.relacionarNegocioTipoServicio(accion,negocioTipoServicio);
		return SUCCESS;
	}
	

	private List<TipoServicioVehiculoDTO> tipoServicioVehiculo;
	private NegocioTipoServicioService negocioTipoServicioService;
	private NegocioSeccion negocioSeccion;
	private NegocioTipoServicio negocioTipoServicio;
	private BigDecimal idToNegSeccion;
	private EntidadService entidadService;
	private RespuestaGridRelacionDTO respuesta;
	private RelacionesNegocioTipoServicioDTO relacionesNegocioTipoServicioDTO;	
	private TipoServicioVehiculoDTO tipoServicioVehiculoDTO;
	
	public List<TipoServicioVehiculoDTO> getTipoServicioVehiculo() {
		return tipoServicioVehiculo;
	}

	public void setTipoServicioVehiculo(
			List<TipoServicioVehiculoDTO> tipoServicioVehiculo) {
		this.tipoServicioVehiculo = tipoServicioVehiculo;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioTipoServicio getNegocioTipoServicio() {
		return negocioTipoServicio;
	}

	public void setNegocioTipoServicio(NegocioTipoServicio negocioTipoServicio) {
		this.negocioTipoServicio = negocioTipoServicio;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public RelacionesNegocioTipoServicioDTO getRelacionesNegocioTipoServicioDTO() {
		return relacionesNegocioTipoServicioDTO;
	}

	public void setRelacionesNegocioTipoServicioDTO(
			RelacionesNegocioTipoServicioDTO relacionesNegocioTipoServicioDTO) {
		this.relacionesNegocioTipoServicioDTO = relacionesNegocioTipoServicioDTO;
	}

	public TipoServicioVehiculoDTO getTipoServicioVehiculoDTO() {
		return tipoServicioVehiculoDTO;
	}

	public void setTipoServicioVehiculoDTO(
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO) {
		this.tipoServicioVehiculoDTO = tipoServicioVehiculoDTO;
	}
	
	public int getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(int claveDefault) {
		this.claveDefault = claveDefault;
	}	

	public EntidadService getCatalogoService() {
		return entidadService;
	}
 	
	@Autowired
	@Qualifier("negocioTipoServicioServiceEJB")
	public void setNegocioTipoServicioService(
				NegocioTipoServicioService negocioTipoServicioService) {
			   this.negocioTipoServicioService = negocioTipoServicioService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

}
