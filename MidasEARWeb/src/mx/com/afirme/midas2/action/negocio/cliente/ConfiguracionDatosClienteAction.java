package mx.com.afirme.midas2.action.negocio.cliente;

import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccionPorNegocio;
import mx.com.afirme.midas2.dto.negocio.cliente.ResultadoActualizacionNodo;
import mx.com.afirme.midas2.service.negocio.cliente.ConfiguracionSeccionPorNegocioService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ConfiguracionDatosClienteAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1L;

	private ConfiguracionSeccionPorNegocioService configuracionSeccionPorNegocioService;
	
	private Long idNegocio;
	private String idNodo;
	private Short claveObligatorio;
	
	private List<ConfiguracionSeccionPorNegocio> listaConfiguracionSeccion;
	private ResultadoActualizacionNodo resultadoActualizacion;
	
	public void prepareMostrarContendor(){
		idNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
	}
	
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	public String getConfiguracionPorNegocio(){
		listaConfiguracionSeccion = configuracionSeccionPorNegocioService.getConfiguracionPorNegocio(idNegocio);
		return SUCCESS;
	}
	
	public String actualizarNodo(){
		
		resultadoActualizacion = configuracionSeccionPorNegocioService.actualizarNodo(idNodo,claveObligatorio);
		return SUCCESS;
	}
	
	@Autowired
	public void setConfiguracionSeccionPorNegocioService(ConfiguracionSeccionPorNegocioService configuracionSeccionPorNegocioService){
		this.configuracionSeccionPorNegocioService = configuracionSeccionPorNegocioService;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public String getIdNodo() {
		return idNodo;
	}

	public void setIdNodo(String idNodo) {
		this.idNodo = idNodo;
	}

	public Short getClaveObligatorio() {
		return claveObligatorio;
	}

	public void setClaveObligatorio(Short claveObligatorio) {
		this.claveObligatorio = claveObligatorio;
	}

	public List<ConfiguracionSeccionPorNegocio> getListaConfiguracionSeccion() {
		return listaConfiguracionSeccion;
	}

	public void setListaConfiguracionSeccion(
			List<ConfiguracionSeccionPorNegocio> listaConfiguracionSeccion) {
		this.listaConfiguracionSeccion = listaConfiguracionSeccion;
	}

	public ResultadoActualizacionNodo getResultadoActualizacion() {
		return resultadoActualizacion;
	}

	public void setResultadoActualizacion(
			ResultadoActualizacionNodo resultadoActualizacion) {
		this.resultadoActualizacion = resultadoActualizacion;
	}

	public ConfiguracionSeccionPorNegocioService getConfiguracionSeccionPorNegocioService() {
		return configuracionSeccionPorNegocioService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
}
