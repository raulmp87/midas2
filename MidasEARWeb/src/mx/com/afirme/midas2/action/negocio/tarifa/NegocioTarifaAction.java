package mx.com.afirme.midas2.action.negocio.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.negocio.tarifa.RelacionesNegocioTarifaDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioTarifaAction extends BaseAction  implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long idToNegProducto = new Long(29);
	private BigDecimal idToNegTipoPoliza;
	private BigDecimal idToNegSeccion;
	private BigDecimal idTcMoneda;
	private BigDecimal idToAgrupadorTarifa;
	private BigDecimal idVerAgrupadorTarifa;
	private NegocioProducto negocioProducto;
	private NegocioTipoPoliza negocioTipoPoliza;	
	private NegocioSeccion negocioSeccion = new NegocioSeccion();
	private List<NegocioTipoPoliza> negocioTipoPolizaList = new ArrayList<NegocioTipoPoliza>(1);
	private List<RelacionesNegocioTarifaDTO> relacionesNegocioTarifaDTOList = new ArrayList<RelacionesNegocioTarifaDTO>(1);
	private List<String> idVersiones = new ArrayList<String>();
	private List<String> idAgrupadores = new ArrayList<String>();
	private List<String> idToNegSeccions = new ArrayList<String>();
	private List<String> idTcMonedas = new ArrayList<String>();
	private EntidadService entidadService;
	private NegocioTarifaService negocioTarifaService;

	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}		
	
	@Autowired
	@Qualifier("negocioTarifaServiceEJB")
	public void setNegocioTarifaService(
			NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}		
	
	@SuppressWarnings("unused")
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		List<String> str=getIdToNegSeccions();
	}
	
	public void prepareMostrar(){
		//BigDecimal idToNegProducto proviene de un elemento hidden
		if(negocioProducto==null)negocioProducto = new NegocioProducto();
		negocioProducto.setIdToNegProducto(idToNegProducto);
		negocioProducto = entidadService.findById(NegocioProducto.class, getIdToNegProducto());
	}
	
	
	public String mostrar(){		
		negocioTipoPolizaList = negocioTarifaService.getNegTipoPolizaList(negocioProducto);
		if(negocioTipoPolizaList == null){
			negocioTipoPolizaList = new ArrayList<NegocioTipoPoliza>();
		}
		return SUCCESS;
	}
	
	public void prepareObtenerNegocioSeccion(){
		if(idToNegTipoPoliza != null){
			negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
		}
	}
	
	public String obtenerNegocioSeccion(){
		if(negocioTipoPoliza != null){			
			relacionesNegocioTarifaDTOList = negocioTarifaService.getNegSeccionList(negocioTipoPoliza);
		}
		return SUCCESS;
	}
	
	public void prepareRelacionarNegocioTarifa(){
		
	}
	
	public String relacionarNegocioTarifa(){
		negocioTarifaService.relacionarNegocioTarifa(idToNegSeccions, idTcMonedas, idAgrupadores, idVersiones);
		setMensajeExito();
		
		return SUCCESS;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioTipoPolizaList(List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setRelacionesNegocioTarifaDTOList(
			List<RelacionesNegocioTarifaDTO> relacionesNegocioTarifaDTOList) {
		this.relacionesNegocioTarifaDTOList = relacionesNegocioTarifaDTOList;
	}

	public List<RelacionesNegocioTarifaDTO> getRelacionesNegocioTarifaDTOList() {
		return relacionesNegocioTarifaDTOList;
	}


	public void setIdTcMoneda(BigDecimal idTcMoneda) {
		this.idTcMoneda = idTcMoneda;
	}

	public BigDecimal getIdTcMoneda() {
		return idTcMoneda;
	}

	public void setIdToAgrupadorTarifa(BigDecimal idToAgrupadorTarifa) {
		this.idToAgrupadorTarifa = idToAgrupadorTarifa;
	}

	public BigDecimal getIdToAgrupadorTarifa() {
		return idToAgrupadorTarifa;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	/**
	 * @return the idVerAgrupadorTarifa
	 */
	public BigDecimal getIdVerAgrupadorTarifa() {
		return idVerAgrupadorTarifa;
	}

	/**
	 * @param idVerAgrupadorTarifa the idVerAgrupadorTarifa to set
	 */
	public void setIdVerAgrupadorTarifa(BigDecimal idVerAgrupadorTarifa) {
		this.idVerAgrupadorTarifa = idVerAgrupadorTarifa;
	}

	/**
	 * @return the idVersiones
	 */
	public List<String> getIdVersiones() {
		return idVersiones;
	}

	/**
	 * @param idVersiones the idVersiones to set
	 */
	public void setIdVersiones(List<String> idVersiones) {
		this.idVersiones = idVersiones;
	}

	/**
	 * @return the idAgrupadores
	 */
	public List<String> getIdAgrupadores() {
		return idAgrupadores;
	}

	/**
	 * @param idAgrupadores the idAgrupadores to set
	 */
	public void setIdAgrupadores(List<String> idAgrupadores) {
		this.idAgrupadores = idAgrupadores;
	}

	/**
	 * @return the idToNegSeccions
	 */
	public List<String> getIdToNegSeccions() {
		return idToNegSeccions;
	}

	/**
	 * @param idToNegSeccions the idToNegSeccions to set
	 */
	public void setIdToNegSeccions(List<String> idToNegSeccions) {
		this.idToNegSeccions = idToNegSeccions;
	}

	/**
	 * @return the idTcMonedas
	 */
	public List<String> getIdTcMonedas() {
		return idTcMonedas;
	}

	/**
	 * @param idTcMonedas the idTcMonedas to set
	 */
	public void setIdTcMonedas(List<String> idTcMonedas) {
		this.idTcMonedas = idTcMonedas;
	}
 
}
