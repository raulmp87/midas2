package mx.com.afirme.midas2.action.negocio.zonacirculacion.negocioMunicipio;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.zonacirculacion.negocioMunicipio.RelacionesNegocioMunicipio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.negocioMunicipio.NegocioMunicipioService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioMunicipioAction extends BaseAction implements Preparable{

private static final long serialVersionUID = 1L;
private Long id;
private Negocio negocio;
private EntidadService entidadService;
private NegocioMunicipioService negocioMunicipioService;
Map<Long, String>  listados = new LinkedHashMap<Long, String>();


private RespuestaGridRelacionDTO respuesta;
private RelacionesNegocioMunicipio relacionesNegocioMunicipio;	
private NegocioEstadoService negocioEstadoService;
private EstadoDTO estadoDTO;
private NegocioMunicipio negocioMunicipio;
private Long  stateId;
private Long idToNegocio;
private String claveNegocio;
private String nivelActivo;

	@Override
	public void prepare() throws Exception {
	
		if (negocioMunicipio==null) negocioMunicipio = new NegocioMunicipio();
		if(getStateId()!=null){
	    	negocioMunicipio = entidadService.findById(NegocioMunicipio.class, getStateId());
		}
				 
	}
	
	
	public String mostrar(){
		return SUCCESS;
	}
	
	public String listar(){
		 listados = negocioEstadoService.getNegocioEstadosPorNegocioId(getIdToNegocio());
		return SUCCESS;
	}
	
	
	public String obtenerRelaciones(){
		if(stateId != null){
			relacionesNegocioMunicipio = negocioMunicipioService.getRelationLists(stateId);
		}
		
		return SUCCESS;
	}
	
	
	public String relacionarNegocioZonaCirculacion(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(negocioMunicipioService.relacionarNegocio(accion,negocioMunicipio) != null){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
		}else{
			super.setMensajeExito();
		}
		return SUCCESS;
	}
	
	
	public String setComboNegocioEstados(){
		
		
		return SUCCESS;
	}
	
	public Negocio getNegocio() {
		return negocio;
	}
	
	
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}
	
	
	public Long getId() {
		return id;
	}
	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}
	
	
	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}
	
	
	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}
	
	
	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}	
	
	
	public RelacionesNegocioMunicipio getRelacionesNegocioMunicipio() {
		return relacionesNegocioMunicipio;
	}
	
	
	public void setRelacionesNegocioMunicipio(
			RelacionesNegocioMunicipio relacionesNegocioMunicipio) {
		this.relacionesNegocioMunicipio = relacionesNegocioMunicipio;
	}
	
	
	public Long getStateId() {
		return stateId;
	}
	
	
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	
	
	public Long getIdToNegocio() {
		return idToNegocio;
	}
	
	
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	
	
	public NegocioMunicipio getNegocioMunicipio() {
		return negocioMunicipio;
	}
	
	
	public void setNegocioMunicipio(NegocioMunicipio negocioMunicipio) {
		this.negocioMunicipio = negocioMunicipio;
	}
	
	
	public Map<Long, String> getListados() {
		return listados;
	}
	
	public void setListados(Map<Long, String> listados) {
		this.listados = listados;
	}


	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	 }
	
	@Autowired
	@Qualifier("negocioMunicipioServiceEJB")
	public void setNegocioMunicipioService(
			NegocioMunicipioService negocioMunicipioService) {
		this.negocioMunicipioService = negocioMunicipioService;
	}
	
	@Autowired
	@Qualifier("negocioEstadoServiceEJB")
	public void setNegocioEstadoService(
			NegocioEstadoService negocioEstadoService) {
		this.negocioEstadoService = negocioEstadoService;
	}


	public String getClaveNegocio() {
		return claveNegocio;
	}


	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}


	public String getNivelActivo() {
		return nivelActivo;
	}


	public void setNivelActivo(String nivelActivo) {
		this.nivelActivo = nivelActivo;
	}
}
