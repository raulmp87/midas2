package mx.com.afirme.midas2.action.negocio.derechos;

import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioDerechosAction extends BaseAction implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7508883898769528235L;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}
	
	public String mostrar() {
		return SUCCESS;
	}
	
	
	public String obtenerDerechosPoliza() {
		derechosPoliza = negocioDerechosService.obtenerDerechosPoliza(idToNegocio);
		return SUCCESS;
	}
	
	public String obtenerDerechosEndoso() {
		derechosEndoso = negocioDerechosService.obtenerDerechosEndoso(idToNegocio, esAltaInciso);
		return SUCCESS;
	}
	
	public String accionSobreDerechosPoliza() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = negocioDerechosService.relacionarDerechosPoliza(accion, idToNegocio, idToNegDerechoPoliza, importeDerecho, claveDefault);
		if(respuesta.getTipoMensaje() == "10"){
			respuesta.setMensaje(BaseAction.MENSAJE_ERROR_ELIMINAR);
		}else if(respuesta.getTipoMensaje() == "20"){
			respuesta.setTipoMensaje("10");
			super.setMensajeError(respuesta.getMensaje());
		}else{
			respuesta.setMensaje(BaseAction.MENSAJE_EXITO);
		}
		return SUCCESS;
	}

	public String accionSobreDerechosEndoso() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = negocioDerechosService.relacionarDerechosEndoso(accion, idToNegocio, idToNegDerechoEndoso, importeDerecho, claveDefault, esAltaInciso);
		if(respuesta.getTipoMensaje() == "10"){
			super.setMensajeError(BaseAction.MENSAJE_ERROR_ELIMINAR);
		}else if(respuesta.getTipoMensaje() == "20"){
			respuesta.setTipoMensaje("10");
			super.setMensajeError(respuesta.getMensaje());
	    }else{
			respuesta.setMensaje(BaseAction.MENSAJE_EXITO);
		}
		
		return SUCCESS;
	}
	
	private Long idToNegocio;
	
	private RespuestaGridRelacionDTO respuesta;
	
	private NegocioDerechosService negocioDerechosService;
	
	private List<NegocioDerechoPoliza> derechosPoliza;
	
	private List<NegocioDerechoEndoso> derechosEndoso;
	
	private Long idToNegDerechoEndoso;
	
	private Long idToNegDerechoPoliza;
	
	private Double importeDerecho;
	
	private Short claveDefault;
	
	private Short esAltaInciso;

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public Double getImporteDerecho() {
		return importeDerecho;
	}

	public void setImporteDerecho(Double importeDerecho) {
		this.importeDerecho = importeDerecho;
	}

	public Short getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(Short claveDefault) {
		this.claveDefault = claveDefault;
	}
	
	public Short getEsAltaInciso() {
		return esAltaInciso;
	}

	public void setEsAltaInciso(Short esAltaInciso) {
		this.esAltaInciso = esAltaInciso;
	}

	public List<NegocioDerechoPoliza> getDerechosPoliza() {
		return derechosPoliza;
	}

	public List<NegocioDerechoEndoso> getDerechosEndoso() {
		return derechosEndoso;
	}

	public Long getIdToNegDerechoEndoso() {
		return idToNegDerechoEndoso;
	}

	public void setIdToNegDerechoEndoso(Long idToNegDerechoEndoso) {
		this.idToNegDerechoEndoso = idToNegDerechoEndoso;
	}

	public Long getIdToNegDerechoPoliza() {
		return idToNegDerechoPoliza;
	}

	public void setIdToNegDerechoPoliza(Long idToNegDerechoPoliza) {
		this.idToNegDerechoPoliza = idToNegDerechoPoliza;
	}

	@Autowired
	@Qualifier("negocioDerechosServiceEJB")
	public void setNegocioDerechosService(
			NegocioDerechosService negocioDerechosService) {
		this.negocioDerechosService = negocioDerechosService;
	}
	
	
	

}
