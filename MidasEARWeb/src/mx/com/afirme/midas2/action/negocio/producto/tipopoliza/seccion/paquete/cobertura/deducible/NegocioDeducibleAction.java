package mx.com.afirme.midas2.action.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible.NegocioDeduciblesService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class NegocioDeducibleAction extends BaseAction implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7508883898769528235L;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}
	
	public String mostrar() {
		if(idToNegCobPaqSeccion!= null)
			System.out.println(idToNegCobPaqSeccion);
		return SUCCESS;
	}
	
	
	public String obtenerDeduciblesCobertura() {
		deduciblesCobertura = negocioDeduciblesService.obtenerDeduciblesCobertura(idToNegCobPaqSeccion);
		return SUCCESS;
	}
	
	
	public String accionSobreDeduciblesCobertura() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = negocioDeduciblesService.relacionarDeduciblesCobertura(accion, idToNegCobPaqSeccion, id, valorDeducible, claveDefault);
		return SUCCESS;
	}
	
	private BigDecimal idToNegCobPaqSeccion;
	
	private RespuestaGridRelacionDTO respuesta;
	
	private List<NegocioDeducibleCob> deduciblesCobertura;
	
	private Long id;
	
	private Double valorDeducible;
	
	private Short claveDefault;

	private NegocioDeduciblesService negocioDeduciblesService;

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}


	public Short getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(Short claveDefault) {
		this.claveDefault = claveDefault;
	}

	public BigDecimal getIdToNegCobPaqSeccion() {
		return idToNegCobPaqSeccion;
	}

	public void setIdToNegCobPaqSeccion(BigDecimal idToNegCobPaqSeccion) {
		this.idToNegCobPaqSeccion = idToNegCobPaqSeccion;
	}

	public List<NegocioDeducibleCob> getDeduciblesCobertura() {
		return deduciblesCobertura;
	}

	public void setDeduciblesCobertura(List<NegocioDeducibleCob> deduciblesCobertura) {
		this.deduciblesCobertura = deduciblesCobertura;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValorDeducible() {
		return valorDeducible;
	}

	public void setValorDeducible(Double valorDeducible) {
		this.valorDeducible = valorDeducible;
	}

	@Autowired
	@Qualifier("negocioDeduciblesServiceEJB")
	public void setNegocioDeduciblesService(
			NegocioDeduciblesService negocioDeduciblesService) {
		this.negocioDeduciblesService = negocioDeduciblesService;
	}
}
