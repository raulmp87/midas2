package mx.com.afirme.midas2.action.negocio.emailsapamis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.general.Email;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioLog;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocio;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioAlertas;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioConfig;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioTipoUsuario;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.general.EmailService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioAlertasService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioConfigService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioTipoUsuarioService;
import mx.com.afirme.midas2.service.operacionessapamis.SapAmisService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatAlertasSapAmisService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
/**
 * 
 * @author 	Eduardo.chavez
 * @since 	2015-10-13
 *
 */

@Component
@Scope("prototype")
@Namespace("/negocio/sapamisemail")
public class EmailNegocioAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1L;
	// 	Constantes de Referencias JSP
	private final String CONTENEDOREMAILNEGOCIO 		= "/jsp/negocio/emailsapamis/contenedor/emailNegocioContenedor.jsp";
	private final String CONTENEDOREMAILCONFIG			= "/jsp/negocio/emailsapamis/contenedor/emailNegocioConfigContenedor.jsp";
	private final String OBTENEREMAILNEGOCIO 			= "/jsp/negocio/emailsapamis/obtener/emailNegocioGrid.jsp";
	private final String OBTENEREMAILNEGOCIOCONFIG 		= "/jsp/negocio/emailsapamis/obtener/emailNegocioConfigGrid.jsp";
	private final String OBTENERSAPAMISALERTAS			= "/jsp/negocio/emailsapamis/obtener/sapAmisAlertasGrid.jsp";
	private final String OBTENERCORREOSENVIADOS			= "/jsp/negocio/emailsapamis/obtener/correosEnviadosGrid.jsp";
	private final String OBTENERRESPUESTAOPERACION 		= "/jsp/negocio/emailsapamis/obtener/respuestaOperaciones.jsp";
	private final String GUARDAREMAILNEGOCIO 			= "/jsp/negocio/emailsapamis/guardaresp/emailNegocioGuardar.jsp";
	private final String GUARDARALERTAEMAILNEGOCIO		= "/jsp/negocio/emailsapamis/guardaresp/catAlertasSapAmis.jsp";
	
	
	//  Entities para obtener los datos por los JSP
	private Email		 			email		 			= new Email();
	private EmailNegocio 			emailNegocio 			= new EmailNegocio();
	private EmailNegocioTipoUsuario	emailNegocioTipoUsuario	= new EmailNegocioTipoUsuario();
	private EmailNegocioAlertas 	emailNegocioAlertas		= new EmailNegocioAlertas();
	private CatAlertasSapAmis		catAlertasSapAmis		= new CatAlertasSapAmis();
	//  Listas de las Entidades
	private List<EmailNegocio> 				emailNegocioList				= new ArrayList<EmailNegocio>();
	private List<EmailNegocioConfig>		emailNegocioConfigList			= new ArrayList<EmailNegocioConfig>();
	private List<EmailNegocioAlertas> 		emailNegocioAlertasList 		= new ArrayList<EmailNegocioAlertas>();
	private List<EmailNegocioTipoUsuario> 	emailNegocioTipoUsuarioList 	= new ArrayList<EmailNegocioTipoUsuario>();
	private List<CatAlertasSapAmis>			catAlertasSapAmisList			= new ArrayList<CatAlertasSapAmis>();
	private List<EmailNegocioLog>			emailNegocioLogList 			= new ArrayList<EmailNegocioLog>();
	
	// Variables para los parametros
	private Long idNegocioParam;
	private String emailParam;
	private String idEmailNegocioParam;
	private String estatusEmailNegocioParam;
	private String idEmailNegocioConfigParam;
	private String estatusEmailNegocioConfigParam;
	private String tipoUsuarioParam;
	private String alertaParam;
	private String sistemaParam;
	private String idAlertaEmailNegocioParam;

	private String catAlertasSapAmisParam;
	//Servicios
	private EntidadService entidadService;
	private EmailService emailService;
	private EmailNegocioService emailNegocioService;
	private EmailNegocioTipoUsuarioService emailNegocioTipoUsuarioService;
	private EmailNegocioAlertasService emailNegocioAlertasService;
	private EmailNegocioConfigService emailNegocioConfigService;
	private CatAlertasSapAmisService catAlertasSapAmisService; 
	private SapAmisService sapAmisService;
	
	//Respuesta Operaciones 
	private String respuestaAccion;

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-13
	 */
	@Action(value = "contenedorEmail", results = { @Result(name = SUCCESS, location = CONTENEDOREMAILNEGOCIO) })
	public String contenedorEmail() {
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-13
	 */
	@Action(value = "contenedorEmailConfig", results = { @Result(name = SUCCESS, location = CONTENEDOREMAILCONFIG) })
	public String contenedorEmailConfig() {
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-15
	 */
	@Action(value = "obtenerListaEmailNegocio", results = { @Result(name = SUCCESS, location = OBTENEREMAILNEGOCIO) })
	public String obtenerListaEmailNegocio() {
		String methodName = "obtenerListaEmailNegocio():: ";
		LogDeMidasWeb.log(methodName + "Inicializa. ", Level.INFO, null);
		LogDeMidasWeb.log(methodName + "  idNegocio: " + idNegocioParam, Level.INFO, null);
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("idNegocio", idNegocioParam);
		parametros.put("estatus", 0);
		emailNegocioList 			= entidadService.findByProperties(EmailNegocio.class, parametros);
		emailNegocioTipoUsuarioList = entidadService.findAll(EmailNegocioTipoUsuario.class);
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-18
	 */
	@Action(value = "guardarEmailNegocio", results = { @Result(name = SUCCESS, location = GUARDAREMAILNEGOCIO) })
	public String guardarEmailNegocio() {
		String methodName = "guardarEmailNegocio():: ";
		LogDeMidasWeb.log(methodName + "Inicializa. "	, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "Parametros: "	, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "             idNegocioParam:> " + idNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "        idEmailNegocioParam:> " + idEmailNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "                 emailParam:> " + emailParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "      EmailTipoUsuarioParam:> " + tipoUsuarioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "   estatusEmailNegocioParam:> " + estatusEmailNegocioParam	, Level.INFO, null);
		//EMAIL SERVICE
		Email emailSave = getEmailService().findByDesc(emailParam);
		LogDeMidasWeb.log(methodName + "emailSave.getIdEmail(): " +  emailSave.getIdEmail(), Level.INFO, null);
		LogDeMidasWeb.log(methodName + "emailSave.getValue(): " +  emailSave.getValue(), Level.INFO, null);
		if(emailSave.getIdEmail() == 0){
			emailSave = getEmailService().saveObject(emailSave);
		}
		EmailNegocioTipoUsuario emailNegocioTipoUsuarioSave =getEmailNegocioTipoUsuarioService().findByDesc(tipoUsuarioParam);
		if(emailNegocioTipoUsuarioSave.getIdEmailNegocioTipoUsuario() == 0){
			emailNegocioTipoUsuarioSave = getEmailNegocioTipoUsuarioService().saveObject(emailNegocioTipoUsuarioSave);
		}
		EmailNegocio emailNegocioSave = new EmailNegocio();
		emailNegocioSave.setIdEmailNegocio(new Long(idEmailNegocioParam));
		emailNegocioSave.setEmail(emailSave);
		emailNegocioSave.setIdNegocio(new Long(idNegocioParam));
		emailNegocioSave.setEmailNegocioTipoUsuario(emailNegocioTipoUsuarioSave);
		emailNegocioSave.setEstatus(new Long(estatusEmailNegocioParam));
		emailNegocioSave = emailNegocioService.saveObject(emailNegocioSave);
		LogDeMidasWeb.log(methodName + "emailNegocioSave.getIdEmailNegocio(): " +  emailNegocioSave.getIdEmailNegocio() + " emailNegocioSave.getValue(): " +  emailNegocioSave.getValue(), Level.INFO, null);
		/**
		// Seteamos los atributos del objeto EmailNegocio
		if(estatusEmailNegocioParam.equals("0")){
			emailNegocioSave.setIdEmailNegocio(Long.valueOf(idEmailNegocioParam));
//			email = emailNegocioService.obtenerObjetoEmail(emailParam);
			if(email.getIdEmail()==0){
				email.setIdEmail((Long)entidadService.saveAndGetId(email));
			}
//			emailNegocioTipoUsuario =  emailNegocioService.obtenerObjetoEmailTipoIUsuario(tipoUsuarioParam);
			if(emailNegocioTipoUsuario.getIdEmailNegocioTipoUsuario()==0){
				emailNegocioTipoUsuario.setIdEmailNegocioTipoUsuario((Long)entidadService.saveAndGetId(emailNegocioTipoUsuario));
			}
			emailNegocioSave.setEmail(email);
			emailNegocioSave.setEmailNegocioTipoUsuario(emailNegocioTipoUsuario);
			emailNegocioSave.setIdNegocio(idNegocioParam);
			if(idEmailNegocioParam.equals("0")){
				//Validar que no exista un registro igual
//				emailNegocioSave =  emailNegocioService.obtenerObjetoEmailNegocio(idNegocioParam, email, emailNegocioTipoUsuario);
			}
			emailNegocioSave.setEstatus(Long.valueOf(estatusEmailNegocioParam));
		}else{
			emailNegocioSave = entidadService.findById(EmailNegocio.class, Long.valueOf(idEmailNegocioParam));
			emailNegocioSave.setEstatus(Long.valueOf(estatusEmailNegocioParam));
		}
		//Seteamos el id del objeto una ves guardado.
		emailNegocioSave.setIdEmailNegocio((Long)entidadService.saveAndGetId(emailNegocioSave));*/
		respuestaAccion = "OK|" + emailNegocioSave.getIdEmailNegocio();
		
		
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-15
	 */
	@Action(value = "obtenerListaEmailNegocioConfig", results = { @Result(name = SUCCESS, location = OBTENEREMAILNEGOCIOCONFIG) })
	public String obtenerListaEmailNegocioConfig() {
		String methodName = "obtenerListaEmailNegocioConfig():: ";
		LogDeMidasWeb.log(methodName + "Inicializa. ", Level.INFO, null);
		LogDeMidasWeb.log(methodName + "   idNegocio: " + idNegocioParam, Level.INFO, null);		
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("idNegocio", new Long(idNegocioParam));
		parametros.put("estatus", new Long(0));
		emailNegocioAlertasList	= entidadService.findByProperties(EmailNegocioAlertas.class, parametros);
		emailNegocioList = entidadService.findByProperties(EmailNegocio.class, parametros);
		for(int i = 0; i < emailNegocioAlertasList.size(); i++){
			parametros = new HashMap<String,Object>();
			parametros.put("alertasEmailNegocio", emailNegocioAlertasList.get(i));
			parametros.put("estatus", new Long(0));
			emailNegocioConfigList.addAll(entidadService.findByProperties(EmailNegocioConfig.class, parametros));
		}
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-18
	 */
	@Action(value = "guardarEmailNegocioConfig", results = { @Result(name = SUCCESS, location = OBTENERRESPUESTAOPERACION) })
	public String guardarEmailNegocioConfig() {
		String methodName = "guardarEmailNegocioConfig():: ";
		LogDeMidasWeb.log(methodName + "Inicializa. ", Level.INFO, null);
		LogDeMidasWeb.log(methodName + "Parametros: ", Level.INFO, null);
		LogDeMidasWeb.log(methodName + "                   idNegocioParam: " + idNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "             idAlertaEmailNegocio: " + idAlertaEmailNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "        idEmailNegocioConfigParam: " + idEmailNegocioConfigParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "           catAlertasSapAmisParam: " + catAlertasSapAmisParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "              idEmailNegocioParam: " + idEmailNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "   estatusEmailNegocioConfigParam: " + estatusEmailNegocioConfigParam, Level.INFO, null);
		EmailNegocioConfig emailNegocioConfigSave = new EmailNegocioConfig();
		// Seteamos los atributos del objeto EmailNegocio
		if(estatusEmailNegocioConfigParam.equals("0")){
			if(!idEmailNegocioConfigParam.equals("0")){
				emailNegocioConfigSave.setIdEmailNegocioConfig(Long.valueOf(idEmailNegocioConfigParam));
			}
			emailNegocioConfigSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
			emailNegocioAlertas = getEmailNegocioAlertasService().findById(new Long(idAlertaEmailNegocioParam));
			emailNegocio = getEmailNegocioService().findById(Long.valueOf(idEmailNegocioParam));
			emailNegocioConfigSave.setEmailNegocio(emailNegocio);
			emailNegocioConfigSave.setAlertasEmailNegocio(emailNegocioAlertas);
//			
//			catAlertasSapAmis = sapAmisService.obtenerObjetoCatAlertasSapAmis(catAlertasSapAmisParam);
////			EmailNegocioAlertas = emailNegocioService.obtenerObjetoEmailNegocioAlertas(idNegocioParam, catAlertasSapAmis);
//			emailNegocio 		= entidadService.findById(EmailNegocio.class, Long.valueOf(idEmailNegocioParam));
//			if(idEmailNegocioConfigParam.equals("0")){
////				emailNegocioConfigSave 	=  emailNegocioService.obtenerObjetoEmailNegocioConfig(EmailNegocioAlertas, emailNegocio);
//			} else {
//				emailNegocioConfigSave.setIdEmailNegocioConfig(Long.valueOf(idEmailNegocioConfigParam));
//			}
////			emailNegocioConfigSave.setEmailNegocioAlertas(EmailNegocioAlertas);
//			emailNegocioConfigSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
			emailNegocioConfigSave = getEmailNegocioConfigService().completeObject(emailNegocioConfigSave);
		}else{
			emailNegocioConfigSave = getEmailNegocioConfigService().findById(Long.valueOf(idEmailNegocioConfigParam));
			emailNegocioConfigSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
		}
		//Seteamos el id del objeto una ves guardado.
		LogDeMidasWeb.log(methodName + "emailNegocioConfigSave.getIdEmailNegocioConfig(): " + emailNegocioConfigSave.getIdEmailNegocioConfig(), Level.INFO, null);
		LogDeMidasWeb.log(methodName + "emailNegocioConfigSave.getValue(): " + emailNegocioConfigSave.getValue(), Level.INFO, null);
		LogDeMidasWeb.log(methodName + "emailNegocioConfigSave.getEstatus(): " + emailNegocioConfigSave.getEstatus(), Level.INFO, null);
		emailNegocioConfigSave = getEmailNegocioConfigService().saveObject(emailNegocioConfigSave);
		this.respuestaAccion = "OK|" + emailNegocioConfigSave.getIdEmailNegocioConfig();
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-15
	 */
	@Action(value = "obtenerListaAlertasSAPAMIS", results = { @Result(name = SUCCESS, location = OBTENERSAPAMISALERTAS) })
	public String obtenerListaAlertasSAPAMIS() {
		String methodName = "obtenerListaAlertasSAPAMIS():: ";
		LogDeMidasWeb.log(methodName + "Se obtienen todas las alertas disponibles" , Level.INFO, null);
		catAlertasSapAmisList 	= entidadService.findAll(CatAlertasSapAmis.class);
		LogDeMidasWeb.log(methodName + "Se obtienen las configuraciones actuales de ALertas y Correos" , Level.INFO, null);
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("idNegocio", idNegocioParam);
		parametros.put("estatus", 0);
		emailNegocioAlertasList = entidadService.findByProperties(EmailNegocioAlertas.class, parametros);
		return SUCCESS;
	}

	/**
	 * @author Eduardo.Chavez
	 * 2015-10-18
	 */
	@Action(value = "guardarEmailNegocioAlertas", results = { @Result(name = SUCCESS, location = GUARDARALERTAEMAILNEGOCIO) })
	public String guardarEmailNegocioAlertas() {
		String methodName = "guardarEmailNegocioAlertas():: ";
		LogDeMidasWeb.log(methodName + "Inicializa. ", Level.INFO, null);
		LogDeMidasWeb.log(methodName + "Parametros: ", Level.INFO, null);
		LogDeMidasWeb.log(methodName + "                   idNegocioParam: " + idNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "        idAlertaEmailNegocioParam: " + idAlertaEmailNegocioParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "           catAlertasSapAmisParam: " + catAlertasSapAmisParam, Level.INFO, null);
		LogDeMidasWeb.log(methodName + "   estatusEmailNegocioConfigParam: " + estatusEmailNegocioConfigParam, Level.INFO, null);
		CatAlertasSapAmis catAlertasSapAmisSave = catAlertasSapAmisService.findByDesc(catAlertasSapAmisParam);
		
		EmailNegocioAlertas emailNegocioAlertasSave = new EmailNegocioAlertas();
		emailNegocioAlertasSave.setIdEmailNegocioAlertas(new Long(idAlertaEmailNegocioParam));
		emailNegocioAlertasSave.setIdNegocio(idNegocioParam);
		emailNegocioAlertasSave.setCatAlertasSapAmis(catAlertasSapAmisSave);
		if(emailNegocioAlertasSave.getIdEmailNegocioAlertas() < 1){
			emailNegocioAlertasSave = getEmailNegocioAlertasService().completeObject(emailNegocioAlertasSave);	
		}
		emailNegocioAlertasSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
		LogDeMidasWeb.log(methodName + "getIdEmailNegocioAlertas: " +  emailNegocioAlertasSave.getIdEmailNegocioAlertas(), Level.INFO, null);
		LogDeMidasWeb.log(methodName + "getValue(): " +  emailNegocioAlertasSave.getValue(), Level.INFO, null);
		emailNegocioAlertasSave = getEmailNegocioAlertasService().saveObject(emailNegocioAlertasSave);
		LogDeMidasWeb.log(methodName + "getIdEmailNegocioAlertas: " +  emailNegocioAlertasSave.getIdEmailNegocioAlertas(), Level.INFO, null);
		LogDeMidasWeb.log(methodName + "getValue(): " +  emailNegocioAlertasSave.getValue(), Level.INFO, null);
		// Seteamos los atributos del objeto EmailNegocio
//		if(estatusEmailNegocioConfigParam.equals("0")){
//			EmailNegocioAlertasSave.setIdEmailNegocioAlertas(Long.valueOf(idAlertaEmailNegocioParam));
//			catAlertasSapAmis = sapAmisService.obtenerObjetoCatAlertasSapAmis(catAlertasSapAmisParam);
//			if(idAlertaEmailNegocioParam.equals("0")){
////				EmailNegocioAlertasSave = emailNegocioService.obtenerObjetoEmailNegocioAlertas(idNegocioParam, catAlertasSapAmis);
//			}
//			EmailNegocioAlertasSave.setIdNegocio(idNegocioParam);
//			EmailNegocioAlertasSave.setCatAlertasSapAmis(catAlertasSapAmis);
//			EmailNegocioAlertasSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
//			EmailNegocioAlertas	= entidadService.findById(EmailNegocioAlertas.class, Long.valueOf(idAlertaEmailNegocioParam));
//			EmailNegocioAlertasSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
//		}else{
//			EmailNegocioAlertasSave = entidadService.findById(EmailNegocioAlertas.class, Long.valueOf(idAlertaEmailNegocioParam));
//			EmailNegocioAlertasSave.setEstatus(Long.valueOf(estatusEmailNegocioConfigParam));
//		}
//		//Seteamos el id del objeto una ves guardado.
//		EmailNegocioAlertasSave.setIdEmailNegocioAlertas((Long)entidadService.saveAndGetId(EmailNegocioAlertasSave));
		return SUCCESS;
	}

	
	/**
	 * @author Luis Ibarra
	 * 24/Nov/2015
	 */
	@Action(value = "cargarCorreosEnviados", results = { @Result(name = SUCCESS, location = OBTENERCORREOSENVIADOS) })
	public String cargarCorreosEnviados() {
		String methodName = "cargarCorreosEnviados():: ";
		LogDeMidasWeb.log(methodName + "idNegocio= " + idNegocioParam, Level.INFO, null);
		emailNegocioLogList = entidadService.findByProperty(EmailNegocioLog.class, "idNegocio", idNegocioParam);
		LogDeMidasWeb.log(methodName + "Encontrados=" + emailNegocioLogList.size(), Level.INFO, null);
		
		return SUCCESS;
	}
	
	
	/**
	 * @author Luis Ibarra
	 * 24/Nov/2015
	 */
	@Action(value = "nuevoEmailNegocio", results = { @Result(name = SUCCESS, location = OBTENERRESPUESTAOPERACION) })
	public String nuevoEmailNegocio() {
		String methodName = "nuevoEmailNegocio():: ";
		LogDeMidasWeb.log(methodName + "idNegocio= " + idNegocioParam, Level.INFO, null);
		emailNegocioLogList = entidadService.findByProperty(EmailNegocioLog.class, "idNegocio", idNegocioParam);
		return SUCCESS;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public SapAmisService getSapAmisService() {
		return sapAmisService;
	}

	@Autowired
	@Qualifier("sapAmisServiceEJB")
	public void setSapAmisService(SapAmisService sapAmisService) {
		this.sapAmisService = sapAmisService;
	}

	@Override
	public void prepare() throws Exception {
		idNegocioParam = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
	}

	public EmailNegocioService getEmailNegocioService() {
		return emailNegocioService;
	}

	@Autowired
	@Qualifier("emailNegocioServiceEJB")
	public void setEmailNegocioService(EmailNegocioService emailNegocioService) {
		this.emailNegocioService = emailNegocioService;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public List<EmailNegocio> getEmailNegocioList() {
		return emailNegocioList;
	}

	public void setEmailNegocioList(List<EmailNegocio> emailNegocioList) {
		this.emailNegocioList = emailNegocioList;
	}

	public List<EmailNegocioTipoUsuario> getEmailNegocioTipoUsuarioList() {
		return emailNegocioTipoUsuarioList;
	}

	public void setEmailNegocioTipoUsuarioList(List<EmailNegocioTipoUsuario> emailNegocioTipoUsuarioList) {
		this.emailNegocioTipoUsuarioList = emailNegocioTipoUsuarioList;
	}

	public List<EmailNegocioAlertas> getEmailNegocioAlertasList() {
		return emailNegocioAlertasList;
	}

	public void setEmailNegocioAlertasList(List<EmailNegocioAlertas> emailNegocioAlertasList) {
		this.emailNegocioAlertasList = emailNegocioAlertasList;
	}

	public EmailNegocio getEmailNegocio() {
		return emailNegocio;
	}

	public void setEmailNegocio(EmailNegocio emailNegocio) {
		this.emailNegocio = emailNegocio;
	}	
	
	public EmailNegocioAlertas getEmailNegocioAlertas() {
		return emailNegocioAlertas;
	}

	public void setEmailNegocioAlertas(EmailNegocioAlertas emailNegocioAlertas) {
		this.emailNegocioAlertas = emailNegocioAlertas;
	}

	public List<EmailNegocioConfig> getEmailNegocioConfigList() {
		return emailNegocioConfigList;
	}

	public void setEmailNegocioConfigList(
			List<EmailNegocioConfig> emailNegocioConfigList) {
		this.emailNegocioConfigList = emailNegocioConfigList;
	}

	public String getIdAlertaEmailNegocioParam() {
		return idAlertaEmailNegocioParam;
	}

	public void setIdAlertaEmailNegocioParam(String idAlertaEmailNegocioParam) {
		this.idAlertaEmailNegocioParam = idAlertaEmailNegocioParam;
	}

	public EmailNegocioTipoUsuario getEmailNegocioTipoUsuario() {
		return emailNegocioTipoUsuario;
	}

	public void setEmailNegocioTipoUsuario(EmailNegocioTipoUsuario emailNegocioTipoUsuario) {
		this.emailNegocioTipoUsuario = emailNegocioTipoUsuario;
	}
	public EntidadService getEntidadService() {
		return entidadService;
	}
	
	public String getIdEmailNegocioParam() {
		return idEmailNegocioParam;
	}

	public void setIdEmailNegocioParam(String idEmailNegocioParam) {
		this.idEmailNegocioParam = idEmailNegocioParam;
	}

	public String getEmailParam() {
		return emailParam;
	}

	public void setEmailParam(String emailParam) {
		this.emailParam = emailParam;
	}

	public String getEstatusEmailNegocioParam() {
		return estatusEmailNegocioParam;
	}

	public void setEstatusEmailNegocioParam(String estatusEmailNegocioParam) {
		this.estatusEmailNegocioParam = estatusEmailNegocioParam;
	}

	public String getIdEmailNegocioConfigParam() {
		return idEmailNegocioConfigParam;
	}

	public void setIdEmailNegocioConfigParam(String idEmailNegocioConfigParam) {
		this.idEmailNegocioConfigParam = idEmailNegocioConfigParam;
	}

	public String getEstatusEmailNegocioConfigParam() {
		return estatusEmailNegocioConfigParam;
	}

	public void setEstatusEmailNegocioConfigParam(String estatusEmailNegocioConfigParam) {
		this.estatusEmailNegocioConfigParam = estatusEmailNegocioConfigParam;
	}

	public String getEmailTipoUsuarioParam() {
		return tipoUsuarioParam;
	}

	public void setEmailTipoUsuarioParam(String tipoUsuarioParam) {
		this.tipoUsuarioParam = tipoUsuarioParam;
	}
	public String getTipoUsuarioParam() {
		return tipoUsuarioParam;
	}

	public void setTipoUsuarioParam(String tipoUsuarioParam) {
		this.tipoUsuarioParam = tipoUsuarioParam;
	}

	public String getSistemaParam() {
		return sistemaParam;
	}

	public void setSistemaParam(String sistemaParam) {
		this.sistemaParam = sistemaParam;
	}
	
	public String getAlertaParam() {
		return alertaParam;
	}

	public void setAlertaParam(String alertaParam) {
		this.alertaParam = alertaParam;
	}
	public CatAlertasSapAmis getCatAlertasSapAmis() {
		return catAlertasSapAmis;
	}

	public void setCatAlertasSapAmis(CatAlertasSapAmis catAlertasSapAmis) {
		this.catAlertasSapAmis = catAlertasSapAmis;
	}

	public String getCatAlertasSapAmisParam() {
		return catAlertasSapAmisParam;
	}

	public void setCatAlertasSapAmisParam(String catAlertasSapAmisParam) {
		this.catAlertasSapAmisParam = catAlertasSapAmisParam;
	}
	
	public List<CatAlertasSapAmis> getCatAlertasSapAmisList() {
		return catAlertasSapAmisList;
	}

	public void setCatAlertasSapAmisList(List<CatAlertasSapAmis> catAlertasSapAmisList) {
		this.catAlertasSapAmisList = catAlertasSapAmisList;
	}

	public List<EmailNegocioLog> getEmailNegocioLogList() {
		return emailNegocioLogList;
	}

	public void setEmailNegocioLogList(List<EmailNegocioLog> emailNegocioLogList) {
		this.emailNegocioLogList = emailNegocioLogList;
	}

	public String getRespuestaAccion() {
		return respuestaAccion;
	}

	public void setRespuestaAccion(String respuestaAccion) {
		this.respuestaAccion = respuestaAccion;
	}

	public EmailService getEmailService() {
		return emailService;
	}

	@Autowired
	@Qualifier("emailServiceEJB")
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	public EmailNegocioTipoUsuarioService getEmailNegocioTipoUsuarioService() {
		return emailNegocioTipoUsuarioService;
	}

	@Autowired
	@Qualifier("emailNegocioTipoUsuarioServiceEJB")
	public void setEmailNegocioTipoUsuarioService(EmailNegocioTipoUsuarioService emailNegocioTipoUsuarioService) {
		this.emailNegocioTipoUsuarioService = emailNegocioTipoUsuarioService;
	}

	public EmailNegocioAlertasService getEmailNegocioAlertasService() {
		return emailNegocioAlertasService;
	}

	@Autowired
	@Qualifier("emailNegocioAlertasServiceEJB")
	public void setEmailNegocioAlertasService(EmailNegocioAlertasService emailNegocioAlertasService) {
		this.emailNegocioAlertasService = emailNegocioAlertasService;
	}

//	public CatAlertasSapAmisService getCatAlertasSapAmisService() {
//		return catAlertasSapAmisService;
//	}

	@Autowired
	@Qualifier("catAlertasSapAmisServiceEJB")
	public void setCatAlertasSapAmisService(CatAlertasSapAmisService catAlertasSapAmisService) {
		this.catAlertasSapAmisService = catAlertasSapAmisService;
	}

	public EmailNegocioConfigService getEmailNegocioConfigService() {
		return emailNegocioConfigService;
	}

	@Autowired
	@Qualifier("emailNegocioConfigServiceEJB")
	public void setEmailNegocioConfigService(EmailNegocioConfigService emailNegocioConfigService) {
		this.emailNegocioConfigService = emailNegocioConfigService;
	}
}