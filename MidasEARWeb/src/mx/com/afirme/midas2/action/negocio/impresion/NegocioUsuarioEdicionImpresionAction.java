package mx.com.afirme.midas2.action.negocio.impresion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.impresion.NegocioUsuarioEdicionImpresion;
import mx.com.afirme.midas2.service.negocio.impresion.NegocioUsuarioEdicionImpresionService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Namespace(value = "/negocio/impresion")
public class NegocioUsuarioEdicionImpresionAction extends BaseAction implements Preparable{
	
	private static final long serialVersionUID = -2880492834256058129L;

	private Long idToNegocio;
	
	private Negocio negocio = new Negocio();
	
	private NegocioUsuarioEdicionImpresion negocioUsuarioImpresion =  new NegocioUsuarioEdicionImpresion();

	private List<NegocioUsuarioEdicionImpresion> usuariosAsociados = new ArrayList<NegocioUsuarioEdicionImpresion>();
	
	private List<NegocioUsuarioEdicionImpresion> usuariosDisponibles = new ArrayList<NegocioUsuarioEdicionImpresion>();
	
	@Autowired
	@Qualifier("negocioUsuarioEdicionImpresionServiceEJB")
	private NegocioUsuarioEdicionImpresionService negocioUsuarioEdicionImpresionService;
	

	@Override
	public void prepare() throws Exception {
		idToNegocio = (Long)ServletActionContext.getContext().getSession().get("idNegocio");
	}
	
	/**
	 * Método que se encarga de mostrar la pantalla dentro del dhtmlxTab del
	 * negocio.
	 */
	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = "/jsp/negocio/impresion/negocioUsuarioEdicionImpresion.jsp"))
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	/**
	 * Método que lista los usuarios en el dhtmlxGrid de
	 * Disponibles.
	 */
	@Action(value = "obtenerUsuariosDisponibles", results = @Result(name = SUCCESS, location = "/jsp/negocio/impresion/negocioUsuarioEdicionImpresionDisponibleGrid.jsp"))
	public String obtenerUsuariosDisponibles() {
		
		usuariosDisponibles = negocioUsuarioEdicionImpresionService.obtenerUsuariosDisponibles(idToNegocio);

		return SUCCESS;
	}
	
	/**
	 * Método que lista los usuarios en el dhtmlxGrid de
	 * Asociados.
	 */
	@Action(value = "obtenerUsuariosAsociados", results = @Result(name = SUCCESS, location = "/jsp/negocio/impresion/negocioUsuarioEdicionImpresionAsociadoGrid.jsp"))
	public String obtenerUsuariosAsociados() {
		
		usuariosAsociados = negocioUsuarioEdicionImpresionService.obtenerUsuariosAsociados(idToNegocio);

		return SUCCESS;
	}
	
	/**
	 * Método que utiliza el dhtmlxProcessor para la asociación del usuario.
	 * Invoca el metodo de relacionar de negocioUsuarioImpresionEdicionService
	 */
	@Action(value = "relacionarUsuario", results = @Result(name = SUCCESS, location = "/jsp/negocio/impresion/negocioUsuarioEdicionImpresion.jsp"))
	public String relacionarUsuario() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		String nombreUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		negocio.setIdToNegocio(idToNegocio);
		negocioUsuarioImpresion.setFechaCreacion(new Date());
		negocioUsuarioImpresion.setNegocio(negocio);
		negocioUsuarioImpresion.setCodigoUsuarioCreacion(nombreUsuario);
		negocioUsuarioEdicionImpresionService.relacionar(accion, negocioUsuarioImpresion);
		return SUCCESS;
	}
	
	/**
	 * Action encargado de asignar permisos a todos los usuarios de la lista.
	 */
	@Action(value = "asociarTodas", results = @Result(name = SUCCESS, location = "/jsp/negocio/impresion/negocioUsuarioEdicionImpresion.jsp"))
	public String asociarTodas() {
		negocioUsuarioEdicionImpresionService.asociarTodas(idToNegocio);
		return SUCCESS;
	}

	/**
	 * Action encargado de retirar todos los permisos del negocio.
	 */
	@Action(value = "desasociarTodas", results = @Result(name = SUCCESS, location = "/jsp/negocio/impresion/negocioUsuarioEdicionImpresion.jsp"))
	public String desasociarTodas() {
		negocioUsuarioEdicionImpresionService.desasociarTodas(idToNegocio);
		return SUCCESS;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public NegocioUsuarioEdicionImpresion getNegocioUsuarioImpresion() {
		return negocioUsuarioImpresion;
	}

	public void setNegocioUsuarioImpresion(
			NegocioUsuarioEdicionImpresion negocioUsuarioImpresion) {
		this.negocioUsuarioImpresion = negocioUsuarioImpresion;
	}

	public List<NegocioUsuarioEdicionImpresion> getUsuariosAsociados() {
		return usuariosAsociados;
	}

	public void setUsuariosAsociados(
			List<NegocioUsuarioEdicionImpresion> usuariosAsociados) {
		this.usuariosAsociados = usuariosAsociados;
	}

	public List<NegocioUsuarioEdicionImpresion> getUsuariosDisponibles() {
		return usuariosDisponibles;
	}

	public void setUsuariosDisponibles(
			List<NegocioUsuarioEdicionImpresion> usuariosDisponibles) {
		this.usuariosDisponibles = usuariosDisponibles;
	}
	
}
