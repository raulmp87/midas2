package mx.com.afirme.midas2.action.calculos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoComisionesView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.calculos.CalculoComisionesService;
import mx.com.afirme.midas2.service.calculos.DetalleCalculoComisionesService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
/**
 * Controller para el manejo de calculos del pago de comisiones del agente.
 * @author vmhersil
 *
 */
@Namespace("/fuerzaventa/calculos/comisiones")
@Component
@Scope("prototype")
public class CalculoComisionesAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6876473800544484769L;
	private String accion=SUCCESS;
	private CalculoComisionesService calculoComisionService;
	private DetalleCalculoComisionesService detalleCalculoComisionesService;
	private ConfigComisiones configuracion;
	private List<AgenteView> listaAgentes=new ArrayList<AgenteView>();
	private List<ValorCatalogoAgentes>modoEjecucion = new ArrayList<ValorCatalogoAgentes>();
	private CalculoComisiones calculo;
	private CalculoComisiones filtroCalculoComisiones;
	private List<CalculoComisiones> listaCalculos=new ArrayList<CalculoComisiones>();
	private List<DetalleCalculoComisiones> listaDetalleCalculos=new ArrayList<DetalleCalculoComisiones>();
	private static final String GRID_LISTAR_DETALLE_COMISIONES="/jsp/calculos/calculoComisiones/calculoPagoComisionesGrid.jsp";
	private static final String DETALLE_CALCULO_COMISIONES="/jsp/calculos/calculoComisiones/calculoPagoComisiones.jsp";
	private static final String LISTADO_CALCULO_COMISIONES="/jsp/calculos/calculoComisiones/listaPreviewPagoComisiones.jsp";
	private static final String GRID_LISTADO_CALCULO_COMISIONES="/jsp/calculos/calculoComisiones/previewPagoComisionesGrid.jsp";
	private static final String IMPORTE_PAGO_COMISIONES="/jsp/calculos/calculoComisiones/comparacionComisiones.jsp";
	private static final String GRID_DIFERENCIAS_MIZAR="/jsp/calculos/calculoComisiones/comparacionMizar.jsp";
	private static final String GRID_DIFERENCIAS_MIDAS="/jsp/calculos/calculoComisiones/comparacionMidas.jsp";
	private Long idCalculoVerDetalle;
	private InputStream reporteInputStream;
	private String contentType;
	private String fileName;
	private List<CalculoComisiones> comparacionMidas=new ArrayList<CalculoComisiones>();
	private List<CalculoComisiones> comparacionMizar=new ArrayList<CalculoComisiones>();
	private List<DetalleCalculoComisiones> comparacionDatosMizar=new ArrayList<DetalleCalculoComisiones>();
	private List<DetalleCalculoComisiones> comparacionDatosMidas=new ArrayList<DetalleCalculoComisiones>();
	private List<CalculoComisionesView> listaCalculosPreview = new ArrayList<CalculoComisionesView>();
	private String moduloOrigen;
	private List<CalculoBonoEjecucionesView> monitorEjecucionesBonoList; //Se reusan objetos
	private String gridMonitor = "COMIS";
	
	/**
	 * ================================================================
	 * Variables del Pago de comisiones Pagadas
	 * ================================================================
	 */
	private static final String LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO="/jsp/calculos/calculoComisiones/listaPreviewPagoComisionesPendientesPago.jsp";
	private static final String GRID_LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO="/jsp/calculos/calculoComisiones/previewPagoComisionesPendientesPagoGrid.jsp";
	private static final String DETALLE_CALCULO_COMISIONES_PAGADAS="/jsp/calculos/calculoComisiones/pagoComisionesPendientes.jsp";
	
	@Override
	public void prepare() throws Exception {}

	@Override
	public String guardar() {
		return accion;
	}

	@Override
	public String eliminar() {
		return accion;
	}

	@Override
	public String listar() {
		accion=listarFiltrado();
		return accion;
	}
	
	/**
	 * Action para renderear el grid de comisiones
	 */
	@Override
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location=GRID_LISTADO_CALCULO_COMISIONES),
		@Result(name=INPUT,location=GRID_LISTADO_CALCULO_COMISIONES),
		@Result(name=ERROR,location=GRID_LISTADO_CALCULO_COMISIONES)
	})
	public String listarFiltrado() {
		listaCalculos=calculoComisionService.findByFilters(filtroCalculoComisiones);
		return accion;
	}

	@Override
	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location=DETALLE_CALCULO_COMISIONES),
		@Result(name=INPUT,location=LISTADO_CALCULO_COMISIONES),
		@Result(name=ERROR,location=LISTADO_CALCULO_COMISIONES)
	})
	public String verDetalle() {
		try {
			calculo=calculoComisionService.loadById(idCalculoVerDetalle);
		} catch (MidasException e) {
			onError(e);
		}
		return accion;
	}
	
	@Action(value="verDetalleCalculo", results={
			@Result(name=SUCCESS,location=GRID_LISTAR_DETALLE_COMISIONES),
			@Result(name=SUCCESS,location=GRID_LISTAR_DETALLE_COMISIONES)
	})
	public String verDetalleCalculo(){
		try {
			listaDetalleCalculos = detalleCalculoComisionesService.listarDetalleDeCalculo(calculo);
			/**se comenta esta bloque de codigo ya que no se debe de enviar correo al momento de consultar el detalle del calculo 
			 *jmmp 04/03/2014 
			calculoComisionService.mailThreadMethodSupport(calculo.getId(),
					null, null, null, GenericMailService.T_GENERAL,
					"enviarCorreoGeneracionPreview");*/
		} catch (MidasException e) {
			e.printStackTrace();
		}
		return accion;
	}
	/**
	 * 
	 * @return
	 */
	@Action(value="obtenerAgentesPorConfiguracion",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","listaAgentes\\.*,listaAgentes.*"})
	})
	public String obtenerAgentesPorConfiguracion(){
		try {
			listaAgentes=calculoComisionService.obtenerAgentes(configuracion);
		} catch (Exception e) {
			onError(e);
		}
		return accion;
	}
	
	public void prepareGenerarPreviewCalculo(){
		calculo=new CalculoComisiones();
	}
	/**
	 * Action para generar el preview del calculo
	 * @return
	 */
	@Action(value="generarPreviewCalculo",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","listadoPreviewComisiones","namespace","/fuerzaventa/calculos/comisiones",
				"tipoAccion","${tipoAccion}","configuracion.id","${configuracion.id}","idRegistro","${idRegistro}", 
				"idTipoOperacion","${idTipoOperacion}"}),
		@Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/configuracionPagoComisiones",
				"tipoAccion","${tipoAccion}","configComisiones.id","${configuracion.id}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
				"idRegistro","${idRegistro}", "idTipoOperacion","${idTipoOperacion}"})
	})
	public String generarPreviewCalculo(){
		try {
			Long idEjecucion = null;
			Long idConfig=(configuracion!=null && configuracion.getId()!=null)?configuracion.getId():null;
			List<AgenteView> listaAgentesDelCalculo=calculoComisionService.cargarAgentesPorConfiguracion(idConfig);
//			List<AgenteView> listaAgentesDelCalculo=null;
			String queryListaAgentes=calculoComisionService.cargarAgentesPorConfiguracionQuery(idConfig);
			//Long idCalculoTemporal=calculoComisionService.getNextIdCalculoTemporal();
			//calculoComisionService.guardarAgentesEnTemporal(listaAgentesDelCalculo, idCalculoTemporal);
			
			//Obtener Id de Calculo Temporal
			//Guardar listado de agentes en tabla temporal.
			idEjecucion =  calculoComisionService.generarCalculo(configuracion,listaAgentesDelCalculo,null,queryListaAgentes);
			this.setMensaje("Se ha iniciado correctamente el calculo de comisiones con id ejecucion: " + idEjecucion );
   		    //TODO: Mover esta logica a la BD, renombrar el parametro para indicar que es id de ejecucion no de calculo.
   		    /*
   		    if(calculoComisionService.eliminarComisionesCero(idCalculo)==0){
				throw new MidasException(" No existen comisiones pendientes de pago. Favor de validar la configuración realizada.");
			}else{
				calculoComisionService.enviarCorreoGeneracionPreview(idCalculo, GenericMailService.P_PAGO_COMISIONES, GenericMailService.M_GENERACION_DE_PREVIEW, null, GenericMailService.T_GENERAL);
			}*/
			
		} catch (MidasException e) {
			onError(e);
		}
		return result;
	}
	
	/**
	 * listadoPreviewComisiones
	 * @return
	 */
	
	public void prepareListadoPreviewComisiones(){
		try{
			modoEjecucion = cargarCatalogo("Modos de Ejecucion de Comisiones");
		}
		catch(Exception e){
			
		}
	}
	
	@Action(value="listadoPreviewComisiones", results={
			@Result(name=SUCCESS, location=LISTADO_CALCULO_COMISIONES)
	})
	public String listadoPreviewComisiones(){		
		return SUCCESS;
	}
	
	@Action(value="listarPreviewPagoComisiones", results={
			@Result(name=SUCCESS, location=GRID_LISTADO_CALCULO_COMISIONES)
	})
	public String listarPreviewPagoComisiones(){
//		filtroCalculoComisiones.setConfiguracionComisiones(configuracion);
		listaCalculosPreview=calculoComisionService.findByFiltersView(filtroCalculoComisiones);
		return SUCCESS;
	}
	/**
	 * Action para autorizar un calculo
	 * @return
	 */
	@Action(value="autorizarPreview",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/calculos/comisiones",
					"tipoAccion","${tipoAccion}","idCalculoVerDetalle","${calculo.id}","moduloOrigen","${moduloOrigen}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT, type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/calculos/comisiones",
					"tipoAccion","${tipoAccion}","idCalculoVerDetalle","${calculo.id}","moduloOrigen","${moduloOrigen}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
	})
	public String autorizarPreview(){
		try {
			calculoComisionService.autorizarPreview(calculo);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			result=SUCCESS;
		} catch (Exception e) {
			onError(e);
		}
		return result;
	}
	/**
	 * Action para cancelar un preview, solo si el estatus del calculo es Pendiente o Pendiente-Recalcular,
	 * de lo contrario se lanza la excepcion de que no se puede cancelar el calculo por regla de negocio
	 * @return
	 */
	@Action(value="cancelarPreview",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","listadoPreviewComisiones","namespace","/fuerzaventa/calculos/comisiones",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tipoAccion","${tipoAccion}"}
		),
		@Result(name=INPUT,type="redirectAction",
				params={"actionName","listadoPreviewComisiones","namespace","/fuerzaventa/calculos/comisiones",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tipoAccion","${tipoAccion}"}
		),
		@Result(name=ERROR,type="redirectAction",
				params={"actionName","listadoPreviewComisiones","namespace","/fuerzaventa/calculos/comisiones",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tipoAccion","${tipoAccion}"}
		)
	})
	public String cancelarPreview(){
		try {
			calculoComisionService.cancelarCalculoPreview(calculo);
			setMensaje("El preview ha sido cancelado exitosamente");
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			result=SUCCESS;
		} catch (Exception e) {
			onError(e);
		}
		return result;
	}
	
	@Action(value="generarReporte",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","reporteInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})})	
	public String generarReporte(){
		try {
			TransporteImpresionDTO reporte = calculoComisionService.generarReporteComisiones(calculo);
			reporteInputStream=new ByteArrayInputStream(reporte.getByteArray());
			contentType = "application/xls";
			fileName="Reporte Calculo Comisiones "+UtileriasWeb.getFechaString(new Date())+".xls";
		} catch (Exception e) {
			onError(e);
		}		
		return result;
	}
	
	@Action(value="verComparacionImpComis", results={
			@Result(name=SUCCESS,location=IMPORTE_PAGO_COMISIONES),
			@Result(name=INPUT,location=IMPORTE_PAGO_COMISIONES)
	})
	public String verComparacionImpComis(){
		try {
//			calculoComisionService.generarDatosComparativosMizarEnSeycos(calculo);
			calculo=calculoComisionService.loadById(calculo);
			comparacionDatosMidas=detalleCalculoComisionesService.listarDiferenciasMidasMizar(calculo.getId());
//			comparacionDatosMidas=detalleCalculoComisionesService.listarDetalleCalculoMidasDiferencia(calculo.getId());
//			comparacionDatosMizar=detalleCalculoComisionesService.listarDetalleCalculoMizarDiferencia(calculo.getId());
		} catch (MidasException e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="gridComparacionMidas", results={
			@Result(name=SUCCESS,location=GRID_DIFERENCIAS_MIDAS),
			@Result(name=INPUT,location=GRID_DIFERENCIAS_MIDAS)
	})
	public String gridComparacionMidas(){
//		for(int i=1;i<=6;i++){
//			CalculoComisiones calculoDummy = new CalculoComisiones();
//			calculoDummy.setId(Long.valueOf((long)i));
//			calculoDummy.setFechaCalculoString("agente"+i);
//			calculoDummy.setImporteTotal(i+3.5*1%9);
//			if(i%2==0){
//				calculoDummy.setImporteTotalString("red");	
//			}
//			else{
//				calculoDummy.setImporteTotalString("white");
//			}
//			comparacionMidas.add(calculoDummy);
//		}		
		try {
			comparacionDatosMidas=detalleCalculoComisionesService.listarDetalleCalculoMidasDiferencia(calculo.getId());
		} catch (MidasException e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="gridComparacionMizar", results={
			@Result(name=SUCCESS,location=GRID_DIFERENCIAS_MIZAR),
			@Result(name=INPUT,location=GRID_DIFERENCIAS_MIZAR)
	})
	public String gridComparacionMizar(){
		try {
//			comparacionDatosMidas=detalleCalculoComisionesService.listarDetalleDeCalculo(calculo);
			comparacionDatosMizar=detalleCalculoComisionesService.listarDetalleCalculoMizarDiferencia(calculo.getId());
		} catch (MidasException e) {
			onError(e);
		}
//		for(int i=1;i<=6;i++){
//			CalculoComisiones calculoDummy = new CalculoComisiones();
//			calculoDummy.setId(Long.valueOf((long)i));
//			calculoDummy.setFechaCalculoString("agente"+i);
//			calculoDummy.setImporteTotal(i+3.5*1%9);
//			if(i%2==0){
//				calculoDummy.setImporteTotalString("red");	
//			}
//			else{
//				calculoDummy.setImporteTotalString("white");
//			}
//			comparacionMizar.add(calculoDummy);
//		}		
		return SUCCESS;
	}
	
	@Action(value="recalcularComisiones",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/calculos/comisiones",
					"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","idCalculoVerDetalle","${calculo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/calculos/comisiones",
					"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","idCalculoVerDetalle","${calculo.id}","mensaje"
					,"${mensaje}","tipoMensaje","${tipoMensaje}"})					
		})
	public String recalcularComisiones(){
		try {
//			List<AgenteView> listaAgentesDelCalculo = calculoComisionService.cargarAgentesPorConfiguracion(configuracion.getId());
			List<AgenteView> listaAgentesDelCalculo = null;
			Long idCalculo=(calculo!=null)?calculo.getId():null;
			calculo=calculoComisionService.loadById(idCalculo);
			Long idConfiguracion=(calculo!=null && calculo.getConfiguracionComisiones()!=null)?calculo.getConfiguracionComisiones().getId():null;
//			Long idCalculoTemporal=calculoComisionService.getNextIdCalculoTemporal();
//			calculoComisionService.guardarAgentesEnTemporal(listaAgentesDelCalculo, idCalculoTemporal);
			String queryAgentes=calculoComisionService.cargarAgentesPorConfiguracionQuery(idConfiguracion);
			calculoComisionService.recalcularCalculoComisiones(idConfiguracion, listaAgentesDelCalculo, calculo.getId(),null,queryAgentes);
		} catch (MidasException e) {
			onError(e);
		}
		return result;
	}

	/**
	 * ================================================================
	 * Actions de Comisiones Pagadas Pendientes
	 * ================================================================
	 */
	/**
	 * Action que muestra la pantalla de calculo de comisiones pendientes de Pago.
	 * @return
	 */
	@Action(value="mostrarCalculosPendientesDePago",results={
		@Result(name=SUCCESS,location=LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO),
		@Result(name=INPUT,location=LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO),
		@Result(name=ERROR,location=LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO)
	})
	public String mostrarCalculosPendientesDePago(){
		return result;
	}
	/**
	 * Action para cargar el grid de Calculos Pendientes 
	 * @return
	 */
	@Action(value="listarCalculosPendientesDePagoGrid",results={
		@Result(name=SUCCESS,location=GRID_LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO),
		@Result(name=ERROR,location=GRID_LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO),
		@Result(name=INPUT,location=GRID_LISTADO_CALCULO_COMISIONES_PENDIENTES_PAGO)
	})
	public String listarCalculosPendientesDePagoGrid(){
		try {
			listaCalculos=calculoComisionService.obtenerCalculosPendientesPorPagar(filtroCalculoComisiones);
		} catch (MidasException e) {
			onError(e);
		}
		return result;
	}
	/**
	 * Muestra el detalle del calculo de pago de comisiones pendientes 
	 * @return
	 */
	@Action(value="mostrarDetallePagoComisionesPendientes",results={
		@Result(name=SUCCESS,location=DETALLE_CALCULO_COMISIONES_PAGADAS),
		@Result(name=INPUT,location=DETALLE_CALCULO_COMISIONES_PAGADAS),
		@Result(name=ERROR,location=DETALLE_CALCULO_COMISIONES_PAGADAS)
	})
	public String mostrarDetallePagoComisionesPendientes(){
		Long idCalculo=(calculo!=null)?calculo.getId():null;
		try {
			calculo=calculoComisionService.loadById(idCalculo);
		} catch (MidasException e) {
			onError(e);
		}
		return result;
	}
	/**
	 * Action para cargar el grid de detalle de calculo, es igual al action de verDetalleCalculo
	 * NOTA: Se separa para manejar actions separados por la logica del negocio, tecnicamente podria usarle el
	 * mismo action.
	 * @return
	 */
	@Action(value="verDetallesDelCalculoPendientePagoGrid",results={
		@Result(name=SUCCESS,location=GRID_LISTAR_DETALLE_COMISIONES),
		@Result(name=INPUT,location=GRID_LISTAR_DETALLE_COMISIONES),
		@Result(name=ERROR,location=GRID_LISTAR_DETALLE_COMISIONES)
	})
	public String verDetallesDelCalculoPendientePagoGrid(){
		verDetalleCalculo();
		return result;
	}
	
	@Action(value="monitorCalculoComisiones",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/monitorCalculoBono.jsp")
		})
		public String monitorCalculoComisiones(){	
		
			return SUCCESS;
		}
	
	@Action(value="listarMonitorComisionesGrid",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/monitorCalculoBonoGrid.jsp")
		})
	public String listarMonitorComisionesGrid(){
		
		try {
			monitorEjecucionesBonoList = calculoComisionService.listaCalculoComisionesMonitorEjecucion();
		} catch (MidasException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * ===========================================================
	 * Setters && getters
	 * ===========================================================
	 */
	public ConfigComisiones getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfigComisiones configuracion) {
		this.configuracion = configuracion;
	}
	
	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public CalculoComisiones getCalculo() {
		return calculo;
	}

	public void setCalculo(CalculoComisiones calculo) {
		this.calculo = calculo;
	}

	public CalculoComisiones getFiltroCalculoComisiones() {
		return filtroCalculoComisiones;
	}

	public void setFiltroCalculoComisiones(CalculoComisiones filtroCalculoComisiones) {
		this.filtroCalculoComisiones = filtroCalculoComisiones;
	}

	public List<CalculoComisiones> getListaCalculos() {
		return listaCalculos;
	}

	public void setListaCalculos(List<CalculoComisiones> listaCalculos) {
		this.listaCalculos = listaCalculos;
	}

	public Long getIdCalculoVerDetalle() {
		return idCalculoVerDetalle;
	}

	public void setIdCalculoVerDetalle(Long idCalculoVerDetalle) {
		this.idCalculoVerDetalle = idCalculoVerDetalle;
	}

	public List<ValorCatalogoAgentes> getModoEjecucion() {
		return modoEjecucion;
	}

	public void setModoEjecucion(List<ValorCatalogoAgentes> modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	
	public List<DetalleCalculoComisiones> getListaDetalleCalculos() {
		return listaDetalleCalculos;
	}

	public void setListaDetalleCalculos(
			List<DetalleCalculoComisiones> listaDetalleCalculos) {
		this.listaDetalleCalculos = listaDetalleCalculos;
	}

	public InputStream getReporteInputStream() {
		return reporteInputStream;
	}

	public void setReporteInputStream(InputStream reporteInputStream) {
		this.reporteInputStream = reporteInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<CalculoComisiones> getComparacionMidas() {
		return comparacionMidas;
	}

	public void setComparacionMidas(List<CalculoComisiones> comparacionMidas) {
		this.comparacionMidas = comparacionMidas;
	}

	public List<CalculoComisiones> getComparacionMizar() {
		return comparacionMizar;
	}

	public void setComparacionMizar(List<CalculoComisiones> comparacionMizar) {
		this.comparacionMizar = comparacionMizar;
	}

	public List<DetalleCalculoComisiones> getComparacionDatosMizar() {
		return comparacionDatosMizar;
	}

	public void setComparacionDatosMizar(
			List<DetalleCalculoComisiones> comparacionDatosMizar) {
		this.comparacionDatosMizar = comparacionDatosMizar;
	}

	public List<DetalleCalculoComisiones> getComparacionDatosMidas() {
		return comparacionDatosMidas;
	}

	public void setComparacionDatosMidas(
			List<DetalleCalculoComisiones> comparacionDatosMidas) {
		this.comparacionDatosMidas = comparacionDatosMidas;
	}
	
	

	public List<CalculoComisionesView> getListaCalculosPreview() {
		return listaCalculosPreview;
	}

	public void setListaCalculosPreview(
			List<CalculoComisionesView> listaCalculosPreview) {
		this.listaCalculosPreview = listaCalculosPreview;
	}

	@Autowired
	@Qualifier("detalleCalculoComisionesServiceEJB")
	public void setDetalleCalculoComisionesService(
			DetalleCalculoComisionesService detalleCalculoComisionesService) {
		this.detalleCalculoComisionesService = detalleCalculoComisionesService;
	}

	@Autowired
	@Qualifier("calculoComisionesServiceEJB")
	public void setCalculoComisionService(
			CalculoComisionesService calculoComisionService) {
		this.calculoComisionService = calculoComisionService;
	}
	
	public String getModuloOrigen() {
		return moduloOrigen;
	}

	public void setModuloOrigen(String moduloOrigen) {
		this.moduloOrigen = moduloOrigen;
	}

	public String getGridMonitor() {
		return gridMonitor;
	}

	public void setGridMonitor(String gridMonitor) {
		this.gridMonitor = gridMonitor;
	}

	public List<CalculoBonoEjecucionesView> getMonitorEjecucionesBonoList() {
		return monitorEjecucionesBonoList;
	}

	public void setMonitorEjecucionesBonoList(
			List<CalculoBonoEjecucionesView> monitorEjecucionesBonoList) {
		this.monitorEjecucionesBonoList = monitorEjecucionesBonoList;
	}
	
}
