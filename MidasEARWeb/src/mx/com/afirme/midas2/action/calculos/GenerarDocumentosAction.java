package mx.com.afirme.midas2.action.calculos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReporteAgenteBaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfiguracionAgenteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.GuiaHonorariosAgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.DocumentoAgenteService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;
import net.sf.jasperreports.engine.JRException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/calculos/generarDocumentos")
public class GenerarDocumentosAction extends ReporteAgenteBaseAction implements
		Preparable {

	private static final long serialVersionUID = 2614710200578346979L;
	private static final String GENERAR_DOCS = "/jsp/calculos/generarDocumentos.jsp";
	private static final String RECIBO_HONORARIOS = "RECIBO DE HONORARIOS";
	private static final String SUCCESS_HONORARIOS = "SUCCESS_HONORARIOS";
	private List<AgenteView> listaAgentes;
	private List<Agente> agenteList;
	private Date fechaInicial;
	private Date fechaFinal;
	private List<ValorCatalogoAgentes> tipoDocumentos;
	private ConfigBonosService configBonosService;
	private String tipoDocumento;
	private Boolean chkTodosAgentes;
	private Boolean chkDetalle;
	private Boolean chkSegAfirmeCom;
	private Boolean chkGuiaLectura;
	private Boolean chkMostrarColAdicionales;
	private String textoSegAfirmeCom;
	private Long idAgenteSeleccionado;
	private String formatoDeSalida = "Formato de Salida";
	private String tipoSalidaArchivo;

	private String idsSeleccionados;
	private InputStream reportePruebas;
	
	private BigDecimal solicitudChequeId;
	
	private Long idAgente = 0l;

	private ConfiguracionAgenteDTO configuracionAgente = new ConfiguracionAgenteDTO();

	private List<GuiaHonorariosAgenteView> guiasHonorariosAgente;
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	private AgenteMidasService agenteMidasService;
	
	@Autowired
	@Qualifier("documentoAgenteServiceEJB")
	private DocumentoAgenteService documentoAgenteService;

	public String getTipoSalidaArchivo() {
		return tipoSalidaArchivo;
	}

	public void setTipoSalidaArchivo(String tipoSalidaArchivo) {
		this.tipoSalidaArchivo = tipoSalidaArchivo;
	}

	public InputStream getReportePruebas() {
		return reportePruebas;
	}

	public void setReportePruebas(InputStream reportePruebas) {
		this.reportePruebas = reportePruebas;
	}

	public String getFormatoDeSalida() {
		return formatoDeSalida;
	}

	public void setFormatoDeSalida(String formatoDeSalida) {
		this.formatoDeSalida = formatoDeSalida;
	}

	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	/**
	 * Action de pruebas borrar cuando ya nadie lo utilice xD
	 * 
	 * @return
	 */
	@Action(value = "exportarToPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToPdf() {
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
					.imprimirReporteEstadoDeCuentaAgente(8l, 80080l, 201210l,
							true, true, true, true,
							"esto es una prueba de afirme comunica", true);

			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			setContentType("application/pdf");
			if (getTipoReporte() == null || getTipoReporte() == 1) {
				setFileName("reportePruebas.pdf");
			} else {
				setFileName("reportePruebas.pdf");
			}
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "mostrarGenerarDocs", results = { @Result(name = SUCCESS, location = GENERAR_DOCS) })
	public String mostrarGenerarDocs() throws Exception {
		final CentroOperacion filtro = new CentroOperacion();
		// setCentroOperacionList(centroOperacionService.findByFilters(filtro));
		setCentroOperacionList(this.entidadService
				.findAll(CentroOperacion.class));
		this.setTipoPromotoria(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipos de Promotoria"));
		this.setTipoAgente(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Clasificacion de Agente"));//Tipo de Agente
		this.setTipoDocumentos(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipos Doc Generar Documentos"));
		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {

		if (getIdCentroOperacion() != null) {
			setCentroOperacionesSeleccionados(new LinkedList<CentroOperacion>());
			getCentroOperacionesSeleccionados().add(
					new CentroOperacion(getIdCentroOperacion().longValue()));
		}
		if (getIdEjecutivo() != null) {
			setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
			getEjecutivosSeleccionados().add(
					new Ejecutivo(getIdEjecutivo().longValue()));
		}

		if (getIdGerencia() != null) {
			setGerenciasSeleccionadas(new LinkedList<Gerencia>());
			getGerenciasSeleccionadas().add(
					new Gerencia(getIdGerencia().longValue()));
		}
		if (getIdPromotoria() != null) {
			setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
			getPromotoriasSeleccionadas().add(
					new Promotoria(getIdPromotoria().longValue()));
		}
	}
	
	@Action(value = "enviarDocumentos", results = { 
		@Result(name = SUCCESS, location = "/jsp/calculos/generarDocumentosGrid.jsp"),
		@Result(name = SUCCESS_HONORARIOS, location = "/jsp/calculos/generarDocumentosParaHonorariosGrid.jsp")
	})
	public String enviarDocumentos() throws JRException {

		List<Long> ids = new ArrayList<Long>();
		
		idsSeleccionados = idsSeleccionados.replaceAll(", ", "");
		
		
		if (!idsSeleccionados.isEmpty()) {
			for (String idAgenteStr : idsSeleccionados.split(",")) {
				ids.add(Long.valueOf(idAgenteStr));
			}

			Map<String, Object> genericParams = new LinkedHashMap<String, Object>();
			String[] nombreRoles = {"Rol_M2_AdministradorAgente","Rol_M2_CoordinadorAgente"}; 
			boolean tieneRole=usuarioService.tieneRolUsuarioActual(nombreRoles);
			
			genericParams.put("tieneRole", tieneRole);
			genericParams.put("tipoDocumento", this.getTipoDocumento());
			genericParams.put("anio", getAnio());
			genericParams.put("mes", getMes());
			genericParams.put("detalle",getChkDetalle() == null ? Boolean.FALSE : getChkDetalle());
			genericParams.put("guiaLectura",getChkGuiaLectura() == null ? Boolean.FALSE: getChkGuiaLectura());
			genericParams.put("MostrarColAdicionales",getChkMostrarColAdicionales() == null ? Boolean.FALSE: getChkMostrarColAdicionales());
			genericParams.put("segAfirmeComunica",getChkSegAfirmeCom() == null ? Boolean.FALSE: getChkSegAfirmeCom());
			genericParams.put("afirmeComunicaText", getTextoSegAfirmeCom());
			
			if (tipoDocumento.equals(RECIBO_HONORARIOS)) {
				configBonosService.enviarDocPorCorreoAgentes(ids, genericParams, "pdf");
				return SUCCESS_HONORARIOS;
			}
			configBonosService.mailThreadMethodSupport(ids,genericParams, "enviarDocPorCorreoAgentes", (tipoSalidaArchivo != null) ? tipoSalidaArchivo : "pdf");
		}

		return SUCCESS;
	}
	
	@Action(value = "listarDocumentos", results = { 
			@Result(name = SUCCESS, location = "/jsp/calculos/generarDocumentosGrid.jsp"),
			@Result(name = SUCCESS_HONORARIOS, location = "/jsp/calculos/generarDocumentosParaHonorariosGrid.jsp")
	})
	public String listarDocumentos() {
		if (tipoDocumento != null && !tipoDocumento.isEmpty()) {
			try {
				if (tipoDocumento.equals(RECIBO_HONORARIOS)) {
									
					configuracionAgente.setCentrosOperacion(getCentroOperacionesSeleccionados());
					configuracionAgente.setEjecutivos(getEjecutivosSeleccionados());
					configuracionAgente.setGerencias(getGerenciasSeleccionadas());
					configuracionAgente.setPromotorias(getPromotoriasSeleccionadas());
					configuracionAgente.setSituaciones(null);
					configuracionAgente.setTiposAgente(getTipoAgentesSeleccionados());
					configuracionAgente.setTiposPromotoria(getTipoPromotoriasSeleccioadas());
					configuracionAgente.setAgentesEspecificos(chkTodosAgentes == null ? agenteList : null);
					
					String anioMes =  String.format("%s%02d", getAnio(), Integer.valueOf(getMes()));
					
					guiasHonorariosAgente = documentoAgenteService.obtenerGuiasHonorarios(configuracionAgente, anioMes);
					return SUCCESS_HONORARIOS; 
				} else {
					this.listaAgentes = configBonosService
							.obtenerAgentesPorConfiguracionCapturada(
									getCentroOperacionesSeleccionados(),
									getEjecutivosSeleccionados(),
									getGerenciasSeleccionadas(),
									getPromotoriasSeleccionadas(), null,
									getTipoAgentesSeleccionados(),
									getTipoPromotoriasSeleccioadas(),
									chkTodosAgentes == null ? agenteList : null,
									null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return SUCCESS;
	}

	@Action(value = "verDocumento", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String verDocumento() {
		try {
			setMes(String.format("%02d", Integer.valueOf(getMes())));
			tipoSalidaArchivo = (tipoSalidaArchivo != null) ? tipoSalidaArchivo: "pdf";
			TransporteImpresionDTO transporte = configBonosService.obtenerDocumentoAgente(
																		idAgenteSeleccionado == null ? 0L : idAgenteSeleccionado, 
																		this.getTipoDocumento(), 
																		getAnio(), 
																		getMes(),
																		chkGuiaLectura == null ? Boolean.FALSE : chkGuiaLectura,
																		chkDetalle == null ? Boolean.FALSE : chkDetalle,
																		chkMostrarColAdicionales == null ? Boolean.FALSE: chkMostrarColAdicionales,
																		chkSegAfirmeCom == null ? Boolean.FALSE : chkSegAfirmeCom, 
																		getTextoSegAfirmeCom(),
																		tipoSalidaArchivo,
																		solicitudChequeId
																	);

			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return INPUT;
			}

			if (TransporteImpresionDTO.DOC_ESTADO_CUENTA.equalsIgnoreCase(this.getTipoDocumento())) {
				setContentType("application/pdf");
				setFileName("EstadoDeCuentaAgente.pdf");

			} else if (TransporteImpresionDTO.DOC_DETALLE_PRIMAS.equalsIgnoreCase(this.getTipoDocumento())) {
				if (tipoSalidaArchivo.equals(TipoSalidaReportes.TO_EXCEL.getValue())) {
					setContentType("application/xls");
					setFileName("DetallePrimasAgente.xls");
				} else {
					setContentType("application/pdf");
					setFileName("DetallePrimasAgente.pdf");
				}

			} else if (TransporteImpresionDTO.DOC_RECIBO_HONORARIOS.equalsIgnoreCase(this.getTipoDocumento())) {
				setContentType("application/pdf");
				setFileName("ReciboDeHonorarios.pdf");
			}

		} catch (Exception error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<ValorCatalogoAgentes> getTipoDocumentos() {
		return tipoDocumentos;
	}

	public void setTipoDocumentos(List<ValorCatalogoAgentes> tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Boolean getChkTodosAgentes() {
		return chkTodosAgentes;
	}

	public void setChkTodosAgentes(Boolean chkTodosAgentes) {
		this.chkTodosAgentes = chkTodosAgentes;
	}

	public Boolean getChkDetalle() {
		return chkDetalle;
	}

	public void setChkDetalle(Boolean chkDetalle) {
		this.chkDetalle = chkDetalle;
	}

	public Boolean getChkSegAfirmeCom() {
		return chkSegAfirmeCom;
	}

	public void setChkSegAfirmeCom(Boolean chkSegAfirmeCom) {
		this.chkSegAfirmeCom = chkSegAfirmeCom;
	}

	public Boolean getChkGuiaLectura() {
		return chkGuiaLectura;
	}

	public void setChkGuiaLectura(Boolean chkGuiaLectura) {
		this.chkGuiaLectura = chkGuiaLectura;
	}

	public Boolean getChkMostrarColAdicionales() {
		return chkMostrarColAdicionales;
	}

	public void setChkMostrarColAdicionales(Boolean chkMostrarColAdicionales) {
		this.chkMostrarColAdicionales = chkMostrarColAdicionales;
	}

	public String getTextoSegAfirmeCom() {
		return textoSegAfirmeCom;
	}

	public void setTextoSegAfirmeCom(String textoSegAfirmeCom) {
		this.textoSegAfirmeCom = textoSegAfirmeCom;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}

	public Long getIdAgenteSeleccionado() {
		return idAgenteSeleccionado;
	}

	public void setIdAgenteSeleccionado(Long idAgenteSeleccionado) {
		this.idAgenteSeleccionado = idAgenteSeleccionado;
	}

	public ConfiguracionAgenteDTO getConfiguracionAgente() {
		return configuracionAgente;
	}

	public void setConfiguracionAgente(ConfiguracionAgenteDTO configuracionAgente) {
		this.configuracionAgente = configuracionAgente;
	}

	public List<GuiaHonorariosAgenteView> getGuiasHonorariosAgente() {
		return guiasHonorariosAgente;
	}

	public void setGuiasHonorariosAgente(
			List<GuiaHonorariosAgenteView> guiasHonorariosAgente) {
		this.guiasHonorariosAgente = guiasHonorariosAgente;
	}

	public BigDecimal getSolicitudChequeId() {
		return solicitudChequeId;
	}

	public void setSolicitudChequeId(BigDecimal solicitudChequeId) {
		this.solicitudChequeId = solicitudChequeId;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	
}
