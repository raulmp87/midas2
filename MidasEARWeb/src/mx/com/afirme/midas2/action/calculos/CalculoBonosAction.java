package mx.com.afirme.midas2.action.calculos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.DetalleCalculoBono;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.bonos.ConfigBonosDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.PreviewCalculoBonoView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.bonos.ProgramacionBonoService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/calculos/bonos")
@Component
@Scope("prototype")
public class CalculoBonosAction extends CatalogoHistoricoAction implements Preparable{

	private static final long serialVersionUID = -713394603975473453L;
	
	private CalculoBonosService calculoBonosService;
	private ConfigBonosService configBonosService;
	private ProgramacionBonoService progBonoService;
	private ConfigBonos configuracionBono;
	private ProgramacionBono programacionBono;
	private CalculoBono calculoBono;
	private CalculoBono filtroCalculoBono;
	private DetalleCalculoBono detalleCalculoBono;
	private List<PreviewCalculoBonoView> calculoBonoList = new ArrayList<PreviewCalculoBonoView>();
	private List<DetalleCalculoBono> detalleCalculoBonoList = new ArrayList<DetalleCalculoBono>();
	private List<ConfigBonosDTO> listaDescripcionBono = new ArrayList<ConfigBonosDTO>();
	private List<ValorCatalogoAgentes>modoEjecucion = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes>tipoBonos = new ArrayList<ValorCatalogoAgentes>();
	private String reporte;
	
	private InputStream inputStream;
	private String contentType;
	private String fileName;
	private Boolean isDirecto;
	
	private static final String PREVIEW_BONOS="/jsp/calculos/calculoBonos/previewCalculoBonos.jsp";
	private static final String PREVIEW_BONOS_GRID="/jsp/calculos/calculoBonos/previewCalculoBonosGrid.jsp";
	private static final String DETALLE_CALCULO_BONOS="/jsp/calculos/calculoBonos/detalleCalculoBonos.jsp";
	private static final String DETALLE_CALCULO_BONOS_GRID="/jsp/calculos/calculoBonos/detalleCalculoBonosGrid.jsp";
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}

	public void prepareVerPreviewBonos(){
		modoEjecucion = cargarCatalogo("Modos de Ejecucion de Comisiones");
		listaDescripcionBono = configBonosService.findByFiltersView(null);
//		tipoBonos = cargarCatalogo("Modos de Ejecucion de Comisiones");
		tipoBonos = cargarCatalogo("Tipos de Bono");
	}
	
	@Action(value="verPreviewBonos", results={
			@Result(name=SUCCESS, location=PREVIEW_BONOS)
	})
	public String verPreviewBonos(){	
		try {
//			listaAgentes=calculoBonosService.obtenerAgentesPorConfiguracion(configBonos);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="previewBonosGrid", results={
			@Result(name=SUCCESS, location=PREVIEW_BONOS_GRID)
	})
	public String previewBonosGrid(){
		calculoBonoList=calculoBonosService.findByFiltersCalculoView(filtroCalculoBono);
		return SUCCESS;
	}
	
	@Action(value="verDetalleCalculoBonos", results={
			@Result(name=SUCCESS, location=DETALLE_CALCULO_BONOS)
	})
	public String verDetalleCalculoBonos(){	
		calculoBono=calculoBonosService.loadByIdCalculo(detalleCalculoBono.getCalculoBono().getId());
		// NOTA: Se identificó que el envío de correos no debe ir aquí, confirmado con el equipo de Agentes 
		// TODO Adicionalmente se detectó un problema con este código ya que no libera los recursos y termina por acabarse la memoria.	
		/*calculoBonosService.mailThreadMethodSupport(calculoBono.getId(), null,
				null, null, GenericMailService.T_GENERAL,
				"enviarCorreoGeneracionPreview");*/
		return SUCCESS;
	}
	
	@Action(value="detalleCalculoBonosGrid", results={
			@Result(name=SUCCESS, location=DETALLE_CALCULO_BONOS_GRID)
	})
	public String detalleCalculoBonosGrid(){
		calculoBono=calculoBonosService.loadByIdCalculo(detalleCalculoBono.getCalculoBono().getId());
		detalleCalculoBonoList=calculoBonosService.findByFiltersDetalleCalculo(detalleCalculoBono);
		return SUCCESS;
	}
	
	@Action(value="autorizarPagoBonos", results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalleCalculoBonos","namespace","/fuerzaventa/calculos/bonos",
					"tipoAccion","${tipoAccion}","detalleCalculoBono.calculoBono.id","${calculoBono.id}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalleCalculoBonos","namespace","/fuerzaventa/calculos/bonos",
					"tipoAccion","${tipoAccion}","detalleCalculoBono.calculoBono.id","${calculoBono.id}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
	})
	public String autorizarPagoBonos(){
			try {
				calculoBonosService.autorizarPreview(calculoBono);
				setMensaje(MENSAJE_EXITO);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			} catch (Exception e) {
				onError(e);
			}
			
		return result;
	}
	
	@Action(value="recalcularBonos", results={
			@Result(name=SUCCESS, location=PREVIEW_BONOS)
	})
	public String recalcularBonos(){	
		try {
			progBonoService.recalcularBono(calculoBono.getId());			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(value="eliminaCalculoBono",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalleCalculoBonos","namespace","/fuerzaventa/calculos/bonos",
					"tipoAccion","${tipoAccion}","detalleCalculoBono.calculoBono.id","${calculoBono.id}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalleCalculoBonos","namespace","/fuerzaventa/calculos/bonos",
					"tipoAccion","${tipoAccion}","detalleCalculoBono.calculoBono.id","${calculoBono.id}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
	})
	public String eliminaCaluloBono(){
		try {
			CalculoBono calculo = calculoBonosService.eliminaCalculoBonos(calculoBono);
			if(calculo!=null){
				setMensaje(MENSAJE_EXITO);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			}else{				
				setMensaje("EL calculo no puede ser eliminado ya que su estatus es AUTORIZADO ");
				setTipoMensaje(TIPO_MENSAJE_ERROR);	
				tipoAccion="3";
				return INPUT;
			}
			
		} catch (MidasException e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
			tipoAccion="3";
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value ="generarDetalleExcel", results = {
			@Result(name= SUCCESS, type="stream" , params={ 
					"contentType","${contentType}",
					"inputName","inputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name= INPUT, location="/jsp/error.jsp")	})
	public String generarDetalleExcel()
	{	
		System.out.println(reporte);
		contentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		if(reporte.equals("reporteAgrupado")){
			TransporteImpresionDTO respuesta = calculoBonosService.getDetalleBonosExcel(calculoBono);		
					
			inputStream = new ByteArrayInputStream(respuesta.getByteArray());
			fileName = " Detalle_Calculo_Bono_" + calculoBono.getId() + ".xlsx";	
		}else if(reporte.equals("reporteNivelcobertura")){
			TransporteImpresionDTO respuesta = calculoBonosService.getDetalleBonosNivelCoberturaExcel(calculoBono);		
			
			inputStream = new ByteArrayInputStream(respuesta.getByteArray());
			fileName = " Detalle_Calculo_Bono_Nivel_Cobertura" + calculoBono.getId() + ".xlsx";	
		}else if(reporte.equals("reporteNivelAmis")){			
			TransporteImpresionDTO respuesta = calculoBonosService.getDetalleBonosNivelAmisExcel(calculoBono);
			
			inputStream = new ByteArrayInputStream(respuesta.getByteArray());
			fileName = " Detalle_Calculo_Bono_Nivel_Amis" + calculoBono.getId() + ".xlsx";
		}
		else{
			TransporteImpresionDTO respuesta = calculoBonosService.getDetalleBonosNivelRamoSubramoExcel(calculoBono);		
			
			inputStream = new ByteArrayInputStream(respuesta.getByteArray());
			fileName = " Detalle_Calculo_Bono_Nivel_RamoSubramo" + calculoBono.getId() + ".xlsx";
		}
		return SUCCESS;
	}
	
	@Action(value="rehabilitarDetalleBono")
	public String rehabilitarDetalleBono() {
		
		calculoBonosService.rehabilitarDetalleBono(detalleCalculoBono);
				
		return NONE;
	}
	
	public ConfigBonos getConfiguracionBono() {
		return configuracionBono;
	}
	
	public void setConfiguracionBono(ConfigBonos configuracionBono) {
		this.configuracionBono = configuracionBono;
	}

	public List<ValorCatalogoAgentes> getModoEjecucion() {
		return modoEjecucion;
	}

	public void setModoEjecucion(List<ValorCatalogoAgentes> modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}

	public List<ConfigBonosDTO> getListaDescripcionBono() {
		return listaDescripcionBono;
	}

	public void setListaDescripcionBono(List<ConfigBonosDTO> listaDescripcionBono) {
		this.listaDescripcionBono = listaDescripcionBono;
	}

	public List<ValorCatalogoAgentes> getTipoBonos() {
		return tipoBonos;
	}

	public void setTipoBonos(List<ValorCatalogoAgentes> tipoBonos) {
		this.tipoBonos = tipoBonos;
	}		
	
	public ProgramacionBono getProgramacionBono() {
		return programacionBono;
	}

	public void setProgramacionBono(ProgramacionBono programacionBono) {
		this.programacionBono = programacionBono;
	}

	public CalculoBono getCalculoBono() {
		return calculoBono;
	}

	public void setCalculoBono(CalculoBono calculoBono) {
		this.calculoBono = calculoBono;
	}

	public DetalleCalculoBono getDetalleCalculoBono() {
		return detalleCalculoBono;
	}

	public void setDetalleCalculoBono(DetalleCalculoBono detalleCalculoBono) {
		this.detalleCalculoBono = detalleCalculoBono;
	}

	public List<PreviewCalculoBonoView> getCalculoBonoList() {
		return calculoBonoList;
	}

	public void setCalculoBonoList(List<PreviewCalculoBonoView> calculoBonoList) {
		this.calculoBonoList = calculoBonoList;
	}

	public List<DetalleCalculoBono> getDetalleCalculoBonoList() {
		return detalleCalculoBonoList;
	}

	public void setDetalleCalculoBonoList(
			List<DetalleCalculoBono> detalleCalculoBonoList) {
		this.detalleCalculoBonoList = detalleCalculoBonoList;
	}

	public ConfigBonosService getConfigBonosService() {
		return configBonosService;
	}
	
	public CalculoBono getFiltroCalculoBono() {
		return filtroCalculoBono;
	}

	public void setFiltroCalculoBono(CalculoBono filtroCalculoBono) {
		this.filtroCalculoBono = filtroCalculoBono;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	@Autowired
	@Qualifier("calculoBonosServiceEJB")
	public void setCalculoBonosService(CalculoBonosService calculoBonosService) {
		this.calculoBonosService = calculoBonosService;
	}	
	
	@Autowired
	@Qualifier("programacionBonoServiceEJB")
	public void setProgBonoService(ProgramacionBonoService progBonoService) {
		this.progBonoService = progBonoService;
	}

	public String getReporte() {
		return reporte;
	}

	public void setReporte(String reporte) {
		this.reporte = reporte;
	}

	public Boolean getIsDirecto() {
		return isDirecto;
	}

	public void setIsDirecto(Boolean isDirecto) {
		this.isDirecto = isDirecto;
	}
	
}
