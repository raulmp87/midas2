package mx.com.afirme.midas2.action.cobranza.recibos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir.FiltroReciboReexpedicionDTO;
import mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir.FormaPagoSAT;
import mx.com.afirme.midas2.service.cobranza.recibos.reexpedir.ReexpedirComprobanteService;

@SuppressWarnings("serial")
@Component
@Scope("prototype")
@Namespace("/cobranza/recibos/reexpedicion")
public class ReexpedirComprobanteAction extends BaseAction {
	private static final String MENSAJEERROR = "/jsp/cobranza/recibos/reexpedir/MensajeReexpedir.jsp";
	private static final String NAMESPACE="/cobranza/recibos/reexpedicion";
	
	private FiltroReciboReexpedicionDTO filtroRecibo = new FiltroReciboReexpedicionDTO();
	private List<FiltroReciboReexpedicionDTO> recibos;
	private List<FormaPagoSAT> formasPagoSAT = new ArrayList<FormaPagoSAT>(1);
	private String idMetodoPago;
	private Long numeroCuenta;
	private String listaRecibos;
	private String llaveFiscal;
	
	private ReexpedirComprobanteService reexpedirComprobanteService;
	
	private String mensaje = "";
	private byte[] reciboFiscal   = null;
	private String contentType;
	private InputStream inputStream;
	private String fileName;
	
	private String tipoDocumento;
	
	@Action (value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirComprobantes.jsp")
	})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	@Action (value = "listarRecibosFiltrado", results = { 
			@Result(name = SUCCESS, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirComprobantesGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirComprobantesGrid.jsp")
	})
	public String listarRecibosFiltrado() {
		if(super.getCount() == null){
			super.setCount(20);
		}
		if(super.getPosStart() == null){
			super.setPosStart(0);
		}
		if(super.getTotalCount() == null){
			super.setTotalCount(0L);
		}
		
		if(super.getOrderBy() == null){
			super.setOrderBy("1");
			super.setDirect("desc");
		}
		
		try {
			recibos = reexpedirComprobanteService.listarRecibosFiltrado(filtroRecibo);
			
			if (!recibos.isEmpty()) {
				super.setTotalCount(Long.valueOf(recibos.size()));
			}
			
			filtroRecibo.setRowsStart(super.getPosStart());
			filtroRecibo.setRowsCount(super.getCount());
			filtroRecibo.setTotalRowsCount(super.getTotalCount());
			
			recibos = reexpedirComprobanteService.listarRecibosFiltrado(filtroRecibo);
			
		} catch (Exception e) {
			LOG.error("Error al buscar los recibos... ... "+e);
			return INPUT;
		}
		
		return SUCCESS;		
	}
	
	@Action (value = "modalReexpedirDatosComplementarios", results = { 
			@Result(name = SUCCESS, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirDatosComplementarios.jsp"),
			@Result(name = INPUT, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirDatosComplementarios.jsp")
	})
	public String modalReexpedirDatosComplementarios(){
		try {
			formasPagoSAT = reexpedirComprobanteService.getFormasPagoSAT();
		} catch (Exception e) {
			LOG.error("Error al botener los metodos de pago... "+e);
			return INPUT;
		} 
		
		return SUCCESS;
	}
	
	@Action (value = "reexpedirComprobantes", results = { 
			@Result(name = SUCCESS, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirDatosComplementarios.jsp"),
			@Result(name = INPUT, location = "/jsp/cobranza/recibos/reexpedir/ReexpedirDatosComplementarios.jsp")
	})
	public String reexpedirComprobantes(){
		try {
			if (numeroCuenta == null) {
				numeroCuenta = 0L;
			}
			reexpedirComprobanteService.reexpedirComprobantes(idMetodoPago, BigDecimal.valueOf(numeroCuenta), listaRecibos, filtroRecibo);
		} catch (SystemException e) {
			LOG.error("Error al reexpedir el/los comprobantes... "+e);
			return INPUT;
		}
		
		return SUCCESS;
	}
	
	@Action(value = "descargarComprobante", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = MENSAJEERROR, params = {
							"mensaje", "${mensaje}" }) })
	public String descargarComprobante(){
		contentType="application/pdf";
		
		try {
			reciboFiscal = reexpedirComprobanteService.descargarComprobante(llaveFiscal, tipoDocumento);
		} catch (SystemException e) {
			mensaje="No se pudo obtener el "+tipoDocumento+" del Recibo "+llaveFiscal;
			LOG.error("Error al descargar el "+tipoDocumento+" del comprobante "+llaveFiscal+" ... "+e);
			return INPUT;
		}
		
		if(reciboFiscal!=null){
			inputStream = new ByteArrayInputStream(reciboFiscal);
			fileName="Recibo_"+llaveFiscal+"."+tipoDocumento;
		}else{
			mensaje="No se pudo obtener el PDF del Recibo "+llaveFiscal;
			LOG.trace("No se pudo obtener el PDF del Recibo "+llaveFiscal);
			return INPUT;
		}
		
		return SUCCESS;
	}
	
	public ReexpedirComprobanteService getReexpedirComprobanteService() {
		return reexpedirComprobanteService;
	}

	@Autowired
	@Qualifier("reexpedirComprobanteServiceEJB")
	public void setReexpedirComprobanteService(ReexpedirComprobanteService reexpedirComprobanteService) {
		this.reexpedirComprobanteService = reexpedirComprobanteService;
	}

	public FiltroReciboReexpedicionDTO getFiltroRecibo() {
		return filtroRecibo;
	}

	public void setFiltroRecibo(FiltroReciboReexpedicionDTO filtroRecibo) {
		this.filtroRecibo = filtroRecibo;
	}

	@Override
	public String getMensaje() {
		return mensaje;
	}

	@Override
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<FiltroReciboReexpedicionDTO> getRecibos() {
		return recibos;
	}

	public void setRecibos(List<FiltroReciboReexpedicionDTO> recibos) {
		this.recibos = recibos;
	}

	public String getIdMetodoPago() {
		return idMetodoPago;
	}

	public void setIdMetodoPago(String idMetodoPago) {
		this.idMetodoPago = idMetodoPago;
	}

	public Long getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(Long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getListaRecibos() {
		return listaRecibos;
	}

	public void setListaRecibos(String listaRecibos) {
		this.listaRecibos = listaRecibos;
	}

	public String getLlaveFiscal() {
		return llaveFiscal;
	}

	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}

	public byte[] getReciboFiscal() {
		return reciboFiscal;
	}

	public void setReciboFiscal(byte[] reciboFiscal) {
		this.reciboFiscal = reciboFiscal;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public List<FormaPagoSAT> getFormasPagoSAT() {
		return formasPagoSAT;
	}

	public void setFormasPagoSAT(List<FormaPagoSAT> formasPagoSAT) {
		this.formasPagoSAT = formasPagoSAT;
	}
}
