package mx.com.afirme.midas2.action.cobranza.reportes;

import java.util.List;



import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cobranza.reportes.CalendarioGonherDTO;
import mx.com.afirme.midas2.domain.cobranza.reportes.CargoAutoDomiTC;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.cobranza.reportes.ReportesCobranzaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CargoAutoDomiTCAction extends BaseAction  implements Preparable{
	private static final long serialVersionUID = 1L;
	public static final Logger LOG = Logger.getLogger(CargoAutoDomiTCAction.class);
	
	public TransporteImpresionDTO transporte = null;
	public CalendarioGonherDTO calendarioGonherDTO = null;
	
	@Override
	public void prepare() throws Exception {
			
	}
	
	public String init(){
		return SUCCESS;
	}
	
	
	public String mostrar(){
		calendarioGonherDTO = reportesCobranzaService.getFechaSiguienteProgramacion();
		return SUCCESS;
	}
	
	public String saveConfrimProg(){
		reportesCobranzaService.saveConfrimProgramacion(calendarioGonherDTO);
		return SUCCESS;
	}
	

	public String exportExcel(){
		List<CargoAutoDomiTC> list = reportesCobranzaService.getReporteCargoAutoDomiTC();
		ExcelExporter exporter = new ExcelExporter( CargoAutoDomiTC.class );			
		transporte =  exporter.exportXLS(list, "Reporte Previo Carga Automatica Domi TC");
		return SUCCESS;
	}
	
	@Autowired
	private ReportesCobranzaService reportesCobranzaService;

	public void setReportesCobranzaService(
			ReportesCobranzaService reportesCobranzaService) {
		this.reportesCobranzaService = reportesCobranzaService;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public CalendarioGonherDTO getCalendarioGonherDTO() {
		return calendarioGonherDTO;
	}

	public void setCalendarioGonherDTO(CalendarioGonherDTO calendarioGonherDTO) {
		this.calendarioGonherDTO = calendarioGonherDTO;
	}	
}