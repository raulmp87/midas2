package mx.com.afirme.midas2.action.cobranza.recibos;

 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cobranza.FiltroRecibos;
import mx.com.afirme.midas2.service.cobranza.NotificaFacturaReciboService;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 */
@Component
@Scope("prototype")
@Namespace("/cobranza/recibos")

/**
 * muestras para el grid de : agenteGrid.jsp - agenteCatalogo.jsp - agenteHeader.jsp - agente.js 
 */

public class NotificaFacturaReciboAction extends BaseAction implements Preparable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private final String RECIBOS= "/jsp/cobranza/administraRecibos.jsp";
	

	@EJB
	private NotificaFacturaReciboService notificaFacturaRecibo;
	
	
	@Action(value="mostrarContenedor",results={
			@Result( name=SUCCESS, location=RECIBOS)
		})
	public String mostrarContenedor() {
		// TODO Auto-generated method stub
		//String variable;		variable=null;
		return SUCCESS;
	}
	
		
	@Action(value="notificaFacturaPorUsuario",results={
			@Result(name=SUCCESS,location=RECIBOS)
	})
	public ActionForward notificaFacturaPorUsuario(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println(request.getParameter("pPoliza"));
		//NotificaFacturaReciboService procesaRecibo = new NotificaFacturaReciboService();
		
		FiltroRecibos filtro = new FiltroRecibos();
		

		
		filtro.setPoliza   ( request.getParameter("pPoliza") );
		filtro.setAsegurado( 0 );
		filtro.setAgente   ( 0 );
		filtro.setFiltro   ( 0 );
		
		 notificaFacturaRecibo.procesaEnvioFacturaRecibo(filtro);		
		
		 	return null;
	}
	
	//Notificacion Masiva de Forma Manual, desde jsp de Notificaciones. 
	@Action(value="notificaFacturaMasivo",results={
			@Result(name=SUCCESS,location=RECIBOS)
	})
	public String notificaFacturaMasivo( ) {
		
		
		FiltroRecibos filtro = new FiltroRecibos();
		
		
		
		filtro.setPoliza    ("0");
		filtro.setAsegurado (0);
		filtro.setAgente    (0);
		filtro.setFiltro    (1);
	
		
		LogDeMidasWeb.getLogger().log(Level.INFO, "Inicio del Proceso de Notificacion: " + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date()));
		notificaFacturaRecibo.guardalognotif("Inicio del Proceso de Notificacion - Manual");
		
		notificaFacturaRecibo.procesaEnvioFacturaRecibo(filtro);		
		

		LogDeMidasWeb.getLogger().log(Level.INFO, "Fin del Proceso de Notificacion: " + new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date()));
		notificaFacturaRecibo.guardalognotif("Fin del Proceso de Notificacion - Manual");
		
		return SUCCESS;
	}



	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	
}
