package mx.com.afirme.midas2.action.cobranza.catalogos;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.cobranza.catalogos.RelacionGrupoAgenteDTO;
import mx.com.afirme.midas2.service.cobranza.catalogos.CatalogoCobranzaService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class DomiXPolizaAction extends BaseAction  implements Preparable{

	private static final long serialVersionUID = 1L;
	public static final Logger LOG = Logger.getLogger(DomiXPolizaAction.class);
	
	public List<RelacionGrupoAgenteDTO> listaRelacionGrupoAgenteDTO;
	public RelacionGrupoAgenteDTO relacionGrupoAgente = new RelacionGrupoAgenteDTO(); 
	public String numeroPoliza;

	@Override
	public void prepare() throws Exception {
		
	}
	
	public String init(){
		return SUCCESS;
	}
	
	public String mostrar(){
		listaRelacionGrupoAgenteDTO = new ArrayList<RelacionGrupoAgenteDTO>(1);
		try{
			listaRelacionGrupoAgenteDTO = catalogoCobranzaService.getListaRelacionGrupoAgente(relacionGrupoAgente);
			setMensajeExito();
		}catch(Exception e){
			listaRelacionGrupoAgenteDTO = new ArrayList<RelacionGrupoAgenteDTO>(1);
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al obtener la lista de Pólizas Configuradas");
		}
				
		return SUCCESS;
	}
	
	public String modifica(){
		try{
			catalogoCobranzaService.save(relacionGrupoAgente);
			setMensajeExito();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al agregar o editar el registro");
		}
				
		return SUCCESS;
	}
	
	public String baja(){
		try{
			catalogoCobranzaService.baja(relacionGrupoAgente);
			setMensajeExito();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al eliminar el registro");
		}
		return SUCCESS;
	}
	
	@Autowired
	private CatalogoCobranzaService catalogoCobranzaService;

	public void setCatalogoCobranzaService(
			CatalogoCobranzaService catalogoCobranzaService) {
		this.catalogoCobranzaService = catalogoCobranzaService;
	}

	public List<RelacionGrupoAgenteDTO> getListaRelacionGrupoAgenteDTO() {
		return listaRelacionGrupoAgenteDTO;
	}

	public void setListaRelacionGrupoAgenteDTO(
			List<RelacionGrupoAgenteDTO> listaRelacionGrupoAgenteDTO) {
		this.listaRelacionGrupoAgenteDTO = listaRelacionGrupoAgenteDTO;
	}

	public RelacionGrupoAgenteDTO getRelacionGrupoAgente() {
		return relacionGrupoAgente;
	}

	public void setRelacionGrupoAgente(RelacionGrupoAgenteDTO relacionGrupoAgente) {
		this.relacionGrupoAgente = relacionGrupoAgente;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
}