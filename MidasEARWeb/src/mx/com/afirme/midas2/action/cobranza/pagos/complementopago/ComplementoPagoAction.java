package mx.com.afirme.midas2.action.cobranza.pagos.complementopago;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.cobranza.recibos.consolidar.ReciboConsolidadoAction;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.ComplementoPago;
import mx.com.afirme.midas2.service.complementopago.ComplementoPagoService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.util.MidasException;

import com.opensymphony.xwork2.Preparable;



@Component
@Scope("prototype")
@Namespace("/cobranza/pagos/complementopago")
public class ComplementoPagoAction extends BaseAction implements Preparable {

	private static final String NAMESPACE="/cobranza/pagos/complementopago";
	private static final String COMPLEMENTOPAGOGRID = "/jsp/cobranza/pagos/complementopago/complementoPagoGrid.jsp";
	private static final String MONITORCOMPLEMENTOPAGOGRID = "/jsp/cobranza/pagos/complementopago/monitor/MonitorCPGrid.jsp";
	private static final String MENSAJECOMPLEMENTO = "/jsp/cobranza/pagos/complementopago/mensajeComplemento.jsp";
	private static final String INICIOCOMPLEMENTOPAGO = "/jsp/cobranza/pagos/complementopago/InicioComplementoPago.jsp";
	private static final String INICIOMONITORCOMPLEMENTOPAGO = "/jsp/cobranza/pagos/complementopago/monitor/InicioMonitorCP.jsp";
	private static final Logger loggerCP = Logger.getLogger(ComplementoPagoAction.class);
	private String contentType;
	private String fileName;
	private String prefijoComplemento = "CPDOC_";
	private String folioFiscal;
	private ComplementoPago filtroComplementoPago;
	private String mensaje;
	private InputStream inputStream;
	private ComplementoPagoService complementoPagoService;
	private ClientePolizasService clientePolizasService;
	private List<ComplementoPago> listComplementoPago;
	private ImpresionRecibosService impresionRecibosService;


	@Action(value = "exportarPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType","${contentType}",
					"namespace", NAMESPACE,
					"inputName","inputStream",
					"contentDisposition","attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = MENSAJECOMPLEMENTO, params = {
							"mensaje", "${mensaje}" }) })
	public String exportarPDF()    throws MidasException{
			loggerCP.trace("Entro exportarPDF()");
			contentType="application/pdf";
			fileName= prefijoComplemento+folioFiscal+".pdf";
			byte[] complementoPagoPDF = null;
			try {
				complementoPagoPDF = impresionRecibosService.getReciboFiscal(folioFiscal,"pdf");
			} catch (Exception e) {
				loggerCP.trace("Error al exportar PDF "+e.getMessage());
			}
				if(complementoPagoPDF!=null){
						inputStream = new ByteArrayInputStream(complementoPagoPDF);
						fileName=prefijoComplemento+folioFiscal+".pdf";

				}else{
					loggerCP.trace("Error al exportar PDF");
					mensaje="No se pudo obtener el XML del Recibo "+folioFiscal;
					return INPUT;
				}
				return SUCCESS;
	}

	@Action(value = "exportarXML", results = {
			@Result(name = SUCCESS, type = "stream", params = {
				"contentType","${contentType}",
			  "namespace", NAMESPACE, "inputName",
				"inputStream", "contentDisposition",
				"attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = MENSAJECOMPLEMENTO,
					params = {
							"mensaje", "${mensaje}" }
						 )
					 }
				 )
	public String exportarXML()   throws MidasException{
		loggerCP.trace("Entro exportarXML()");
		contentType="text/xml ";
		fileName= prefijoComplemento+folioFiscal+".xml";
		byte[] complementoPagoXML   = null;
			try {
				complementoPagoXML = clientePolizasService.getReciboFiscalXML(folioFiscal);
			} catch (Exception e) {
				
				loggerCP.trace("Error al exportar XML "+e.getMessage());
			}

			if(complementoPagoXML!=null){
				inputStream = new ByteArrayInputStream(complementoPagoXML);
				fileName=prefijoComplemento+folioFiscal+".xml";
			}else{
				mensaje="No se pudo obtener el XML del Recibo "+folioFiscal;
				return INPUT;
			}

		return SUCCESS;
	}

	@Action(value = "execService", results = {
			@Result(name = SUCCESS, location = INICIOCOMPLEMENTOPAGO)
		})
	public void execService(){
		complementoPagoService.timbraComplementoPago();
	}

	@Action(value = "execNotificacion", results = {
			@Result(name = SUCCESS, location = INICIOCOMPLEMENTOPAGO)
		})
	public void execNotificacion(){
		complementoPagoService.notificaComplementoPago();
	}
	
	public void prepare() throws MidasException {
		loggerCP.trace(">> Prepare");
	}


	@Action(value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = INICIOCOMPLEMENTOPAGO)
		})
		public String mostrarContenedor(){
			return SUCCESS;
		}

		@Action(value = "mostrarContenedorMonitor", results = {
				@Result(name = SUCCESS, location = INICIOMONITORCOMPLEMENTOPAGO)
			})
		public String mostrarContenedorMonitor(){
			return SUCCESS;
		}

	@Action(value = "lista", results = {
        @Result(name = SUCCESS, location = COMPLEMENTOPAGOGRID),
				@Result(name = INPUT, location = COMPLEMENTOPAGOGRID)
    })
    public String listar() {
			try {
				listComplementoPago = complementoPagoService.findByFilters(filtroComplementoPago);
			} catch (Exception e) {
				return INPUT;
			}
        return SUCCESS;
    }
	
	@Action(value = "listarErroresCP", results = {
	        @Result(name = SUCCESS, location = MONITORCOMPLEMENTOPAGOGRID),
					@Result(name = INPUT, location = MONITORCOMPLEMENTOPAGOGRID)
	    })
	    public String listarErroresCP() {
				try {
					listComplementoPago = complementoPagoService.listarErrores(filtroComplementoPago);
				} catch (Exception e) {
					return INPUT;
				}
	        return SUCCESS;
	    }
	
	

	@Autowired
	@Qualifier("complementoPagoServiceEJB")
	public void setComplementoPagoService(ComplementoPagoService complementoPagoService) {
		this.complementoPagoService = complementoPagoService;
	}

	@EJB
	public void setImpresionRecibosService(ImpresionRecibosService impresionRecibosService) {
		this.impresionRecibosService = impresionRecibosService;
	}

	public ComplementoPago getFiltroComplementoPago() {
		return filtroComplementoPago;
	}


	public void setFiltroComplementoPago(ComplementoPago filtroComplementoPago) {
		this.filtroComplementoPago = filtroComplementoPago;
	}


	public List<ComplementoPago> getListComplementoPago() {
		return listComplementoPago;
	}


	public void setListComplementoPago(List<ComplementoPago> listComplementoPago) {
		this.listComplementoPago = listComplementoPago;
	}


	public String getFolioFiscal() {
		return folioFiscal;
	}


	public void setFolioFiscal(String folioFiscal) {
		this.folioFiscal = folioFiscal;
	}


	public InputStream getInputStream() {
		return inputStream;
	}


	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public ClientePolizasService getClientePolizasService() {
		return clientePolizasService;
	}

	@Autowired
	@Qualifier("clientePolizasServiceEJB")
	public void setClientePolizasService(ClientePolizasService clientePolizasService) {
		this.clientePolizasService = clientePolizasService;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}
