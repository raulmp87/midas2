package mx.com.afirme.midas2.action.cobranza.pagos;

import java.io.PrintWriter; 
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSAException;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSAMessage;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSAMessage.Type;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransaction;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransactionResponse;
import mx.com.afirme.midas2.domain.cobranza.pagos.RespuestaPagoDTO;
import mx.com.afirme.midas2.service.cobranza.pagos.CreditCardPaymentService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.util.ServletContextAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/cobranza/pagos")
public class CreditCardPaymentAction extends ActionSupport implements
		Preparable, ServletContextAware {
	
	public static final Logger LOG = Logger.getLogger(CreditCardPaymentAction.class);
	private CreditCardPaymentService creditCardPaymentService;
	private static final long serialVersionUID = -8428509543552329387L;
	private static final String URL = "/jsp/cobranza/payment/pagosPortalWeb.jsp";
	private static final String FRAME = "/jsp/cobranza/payment/iframe.jsp";
	private List<ReciboDTO> pendingReceipts;
	private List<ReciboDTO> paidReceipts;
	private PolizaDTO policy;
	private PROSAMessage message;
	private String insurancePolicy;
	private String subsection;
	private String destinatario;
	private String siglasRFC;
	private String fechaRFC;
	private String homoClaveRFC;
	private String UrlDestino;
	private Double tipoCambio;
	
	private RespuestaPagoDTO respuestaMail;
	private static final int monedaDls = 840;
	private static final String flotillaUbicacion = "UB";
	private static final int numTarjeta = 4;
	
	@Autowired
	@Qualifier("creditCardPaymentServiceEJB")
	public void setCreditCardPaymentService(
			CreditCardPaymentService creditCardPaymentService) {
		this.creditCardPaymentService = creditCardPaymentService;
	}
	
	//private TipoCambioFacadeRemote tipoCambioService;

	@Action(value = "init", results = { @Result(name = SUCCESS, location = FRAME) })
	public String init() {
		return SUCCESS;
	}

	@Action(value = "start", results = { @Result(name = SUCCESS, location = URL) })
	public String start() {
		return SUCCESS;
	}
	
	public PolizaDTO getPolicy() {
		return policy;
	}

	public void setPolicy(PolizaDTO policy) {
		this.policy = policy;
	}

	public List<ReciboDTO> getPendingReceipts() {
		return pendingReceipts;
	}

	public void setPendingReceipts(List<ReciboDTO> pendingReceipts) {
		this.pendingReceipts = pendingReceipts;
	}

	public List<ReciboDTO> getPaidReceipts() {
		return paidReceipts;
	}

	public void setPaidReceipts(List<ReciboDTO> paidReceipts) {
		this.paidReceipts = paidReceipts;
	}

	public String getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(String insurancyPolicy) {
		this.insurancePolicy = insurancyPolicy;
	}

	public PROSAMessage getMessage() {
		return message;
	}

	public void setMessage(PROSAMessage message) {
		this.message = message;
	}

	public String getSubsection() {
		return subsection;
	}

	public void setSubsection(String subsection) {
		this.subsection = subsection;
	}

	public CreditCardPaymentService getCreditCardPaymentService() {
		return creditCardPaymentService;
	}

	public RespuestaPagoDTO getRespuestaMail() {
		return respuestaMail;
	}

	public void setRespuestaMail(RespuestaPagoDTO respuestaMail) {
		this.respuestaMail = respuestaMail;
	}
	
	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getSiglasRFC() {
		return siglasRFC;
	}

	public void setSiglasRFC(String siglasRFC) {
		this.siglasRFC = siglasRFC;
	}

	public String getFechaRFC() {
		return fechaRFC;
	}

	public void setFechaRFC(String fechaRFC) {
		this.fechaRFC = fechaRFC;
	}

	public String getHomoClaveRFC() {
		return homoClaveRFC;
	}

	public void setHomoClaveRFC(String homoClaveRFC) {
		this.homoClaveRFC = homoClaveRFC;
	}
	
	public Double getTipoCambio() {
		return tipoCambio;
	}
	
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
	public void setUrlDestino(String Url){
		this.UrlDestino = Url;
	}
	
	public String getUrlDestino(){
		return this.UrlDestino;
	}

	@Action(value = "findReceipts", results = {
			@Result(name = SUCCESS, location=URL),
			@Result(name = INPUT, location = URL)})
	public String findReceipts(){
		try {
			String validpoliza = creditCardPaymentService.validPoliza(this.insurancePolicy, this.subsection, this.siglasRFC, this.fechaRFC);
			if (StringUtils.isBlank(validpoliza)) {			
				List<ReciboDTO> receipts = creditCardPaymentService.findReceipts(this.insurancePolicy, this.subsection, this.siglasRFC, this.fechaRFC, this.homoClaveRFC);
				
				if (receipts.size() > 0) {				
					int idMoneda =  receipts.get(0).getIdMoneda().intValue();
					
					if (idMoneda == monedaDls ) {
						try {
							this.tipoCambio = creditCardPaymentService.obtieneTipoCambioAlDia(new Date(), "SEGASIS");						
						} catch (Exception ex) {
							LOG.error("No se pudo consultar el tipo de cambio al día, " + new Date().getTime(), ex);
							setMessage(new PROSAMessage(Type.DANGER, "No se pudo consultar el tipo de cambio al día."));
						}
						setMessage(new PROSAMessage(Type.WARNING, "La póliza que intenta pagar esta en dólares, se aplicará el tipo de cambio al día. " + this.tipoCambio));
					}
					
					if (receipts.get(0).getGeneracionRecibo().equals(flotillaUbicacion)) {
						if (this.subsection == null) {
							setMessage(new PROSAMessage(Type.WARNING, "La póliza que intenta consultar es flotilla por ubicación, debe seleccionar un inciso para poder realizar su pago."));
							return INPUT;
						}
					}
					
					policy = creditCardPaymentService.findPolicy(this.insurancePolicy);
						
					for (ReciboDTO receipt : receipts) {
						if (receipt.getSituacion().equals("PAGADO")) {
							paidReceipts.add(receipt);
						} else {
							pendingReceipts.add(receipt);
						}
					}							 
					
				} else {
					setMessage(new PROSAMessage(Type.DANGER, "Su búsqueda no obtuvo resultados. Favor de validar los datos introducidos e intente de nuevo."));
				}				
			} else {
				setMessage(new PROSAMessage(Type.DANGER, "La póliza "+this.insurancePolicy+" se encuentra cancelada. Favor de validarlo con su agente."));
			}
		} catch (PROSAException e) {
			LOG.error(e.getMessage(), e);
			setMessage(new PROSAMessage(Type.DANGER, "Su búsqueda no obtuvo resultados. Favor de validar los datos introducidos e intente de nuevo."));
			return INPUT;
		}		
		return SUCCESS;
	}

	@Action(value = "startTransaction", results = {
			@Result(name = SUCCESS, location = URL) })
	public String startTransacion() {
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();
		LOG.info("Entrando al metodo startTransacion.....");
		try {
			String[] selectedReceipts = request.getParameterValues("selectedReceipts");
			setSiglasRFC(request.getParameter("siglasRFC"));
			setFechaRFC(request.getParameter("fechaRFC"));
			PROSATransaction transaction = creditCardPaymentService.requestTransaction(this.insurancePolicy, this.subsection, selectedReceipts);
			StringBuilder recepits = new StringBuilder();
			StringBuilder url = new StringBuilder();
			url.append(transaction.getURL());			
			for (String idRecibo : selectedReceipts) {				
				recepits.append(idRecibo+",");
			}			
			url.append("&receipts=" + recepits.substring(0,recepits.length()-1));
			url.append("&siglasRFC=" + this.siglasRFC);
			url.append("&fechaRFC=" + this.fechaRFC);
			
			PrintWriter out = response.getWriter();
			out.print("function openWindows(iURL) { if (window.showModalDialog) { var retValue = showModalDialog(iURL, null, 'dialogWidth:900px; dialogHeight:500px; dialogLeft:300px;'); } else { var modal = window.open(iURL, null, 'width=900px,height=500,left=300,modal=yes,alwaysRaised=yes', null); } }; openWindows(" + url.toString() + "); ");
			
			this.UrlDestino = url.toString();
			//response.sendRedirect(url.toString());	
		} catch (PROSAException e) {
			setMessage(new PROSAMessage(Type.DANGER, e.getMessage()));
		} catch (Exception e) {
			LOG.error("Ocurrio un error inesperado.  ",e);
			setMessage(new PROSAMessage(Type.DANGER, PROSAException.UNKNOWN_ERROR));
		}
		LOG.info("Saliendo del metodo startTransacion SUCCESS....");
		return SUCCESS;		
	}
	
	public PROSATransactionResponse PaymentResponse(HttpServletRequest request){
		PROSATransactionResponse transactionResponse = new PROSATransactionResponse();	
		Map<String, String[]> map = request.getParameterMap();
	    Set set = map.entrySet();
	    Iterator it = set.iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, String[]> entry = (Entry<String, String[]>) it.next();
	        String paramName = entry.getKey();
	        LOG.info("ParamName = " + paramName);
			
			if (paramName.equals("receipts")){
				String[] paramValues = entry.getValue();
				if(paramValues[0].length() > 0){
					String [] selectedReceipts = paramValues[0].split(",");
					transactionResponse.setRecibos(selectedReceipts);
				}
			}
			else if (paramName.equals("EM_Auth")){
				String[] paramValues = entry.getValue();
				transactionResponse.setAuth(paramValues[0]);
			}
			else if (paramName.equals("EM_Digest")){
				String[] paramValues = entry.getValue();
				transactionResponse.setDigest(paramValues[0]);
			}
			else if (paramName.equals("EM_Merchant")){
				String[] paramValues = entry.getValue();
				transactionResponse.setMerchant(paramValues[0]);
			}
			else if (paramName.equals("EM_OrderID")){
				String[] paramValues = entry.getValue();
				transactionResponse.setOrderId(paramValues[0]);
			}
			else if (paramName.equals("EM_RefNum")){
				String[] paramValues = entry.getValue();
				transactionResponse.setRefNum(paramValues[0]);
			}
			else if (paramName.equals("EM_Response")){
				String[] paramValues = entry.getValue();
				transactionResponse.setResponse(paramValues[0]);
			}
			else if (paramName.equals("EM_Store")){
				String[] paramValues = entry.getValue();
				transactionResponse.setStore(paramValues[0]);
			}
			else if (paramName.equals("EM_Term")){
				String[] paramValues = entry.getValue();
				transactionResponse.setTerm(paramValues[0]);
			}
			else if (paramName.equals("EM_Total")){
				String[] paramValues = entry.getValue();
				transactionResponse.setTotal(paramValues[0]);
			}
			else if (paramName.equals("insurancePolicy")){
				String[] paramValues = entry.getValue();
				transactionResponse.setPolicy(paramValues[0]);
			}
			else if (paramName.equals("subsection")){
				String[] paramValues = entry.getValue();
				transactionResponse.setSubsection(paramValues[0]);
			}
			else if (paramName.equals("cc_number")){
				String ccNumber = null;
				String[] paramValues = entry.getValue();
				ccNumber = paramValues[0];
				if(ccNumber.length() > 0){
					StringBuilder sb = new StringBuilder();
					sb.append("************");
					sb.append(ccNumber.substring(ccNumber.length()-numTarjeta));
					ccNumber = sb.toString();
					transactionResponse.setCc_number(ccNumber);
				}
			}
			else if (paramName.equals("cc_name")){
				String[] paramValues = entry.getValue();
				transactionResponse.setCc_name(paramValues[0] != null ? paramValues[0] : " ");
			}
	    }
		return transactionResponse;
	}

		@Action(value = "payReceipts", results = {
				@Result(name = SUCCESS, location = URL)})
		public String payReceipts() {
			try{
			HttpServletRequest request = ServletActionContext.getRequest();
			RespuestaPagoDTO respuestaPagoDTO = new RespuestaPagoDTO();
								
			try {
				PROSATransactionResponse transactionResponse = PaymentResponse(request) ;
				String [] selectedReceipts = transactionResponse.getRecibos(); 
				respuestaPagoDTO = creditCardPaymentService.payReceipts(transactionResponse, selectedReceipts);
				if (respuestaPagoDTO.isPagado()) {
					setMessage(new PROSAMessage(Type.SUCCESS, "Gracias por su pago. en breve recibirá sus comprobantes por correo electrónico."));
					try{
						if (!StringUtils.isBlank(respuestaPagoDTO.getReciboLlaveFiscal())) {
							List<ReciboDTO> recibosPagados = creditCardPaymentService.buscaRecibosById(this.insurancePolicy, null, selectedReceipts);
							for (ReciboDTO reciboPagado : recibosPagados) {							
								String llaveFiscal = reciboPagado.getSerieFiscal() +" "+reciboPagado.getFolioFiscal();							
								LOG.info("Entrando al metodo Enviar Recibo por Email....");
								creditCardPaymentService.envioPDFyXML(llaveFiscal, respuestaPagoDTO.getCorreos(), respuestaPagoDTO.getPoliza());
								LOG.info("Saliendo del metodo Enviar Recibo por Email....");
							}												
						} else {
							LOG.info("No se pudo enviar el Recibo por Correo");
						}
					} catch (Exception ex){
						LOG.error("Ocurrio un error al intentar enviar el Recibo por Correo.",ex);
						setMessage(new PROSAMessage(Type.WARNING, "Ocurrio un error al intentar enviar el recibo por correo."));
					}				
					findReceipts();
				}
			} catch (PROSAException e) {
				LOG.error("Error al aplicar el pago de los recibos ", e);
				setMessage(new PROSAMessage(Type.DANGER, e.getMessage()));			
			} catch(Exception e) {
				LOG.error("Error al aplicar el pago de los recibos ", e);
				setMessage(new PROSAMessage(Type.DANGER, PROSAException.UNKNOWN_ERROR));			
			}
			LOG.info("Saliendo del metodo payReceipts SUCCESS....");
			return SUCCESS;
			}
			catch(Exception e){
				LOG.info("Error al procesar respuesta");
				return SUCCESS;	
			}
		}
	
	@Override
	public void setServletContext(ServletContext arg0) {

	}

	@Override
	public void prepare() throws Exception {
		paidReceipts = new ArrayList<ReciboDTO>();
		pendingReceipts = new ArrayList<ReciboDTO>();
		policy = new PolizaDTO();
	}	
}