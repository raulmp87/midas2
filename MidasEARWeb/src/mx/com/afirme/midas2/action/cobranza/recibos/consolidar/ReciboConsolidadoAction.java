package mx.com.afirme.midas2.action.cobranza.recibos.consolidar;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDN;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.Consolidado;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RecibosConsolidar;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RelRecibosConsolidado;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.recibos.consolidar.ConsolidarRecibosService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/cobranza/recibos/consolidado")
public class ReciboConsolidadoAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1905122041950251207L;
	private static final String REFCARGOCONS = "REF_CARGO_CONS";
	private static final Logger loggerConsolidado = Logger.getLogger(ReciboConsolidadoAction.class);
	private static final String[] COLUMNAS_GRID_CONSOLIDADOS= {REFCARGOCONS,REFCARGOCONS,"POLIZA","FOLIO_FISCAL",REFCARGOCONS,"FH_CONSOLIDADO","FH_CORTE","FH_LIMITE","FH_CREACION","IMP_CARGO_CONS","ESTATUS","USUARIO_CREACION",REFCARGOCONS,REFCARGOCONS};
	private static final String NAMESPACE="/cobranza/recibos/consolidado";
	private static final String RECIBOSACONSOLIDARGRID = "/jsp/cobranza/recibos/consolidar/recibosAConsolidarGrid.jsp";
	private static final String RECIBOSYACONSOLIDADOSGRID = "/jsp/cobranza/recibos/consolidar/recibosConsolidadosGrid.jsp";
	private static final String RECIBOSACONSOLIDAR = "/jsp/cobranza/recibos/consolidar/previoReciboConsolidado.jsp";
	private static final String MENSAJECONSOLIDADO = "/jsp/cobranza/recibos/consolidar/mensajeConsolidar.jsp";
	private static final String ENVIARCORREO = "/jsp/cobranza/recibos/consolidar/enviarCorreo.jsp";
	private List<RecibosConsolidar> listRecibosAConsolidarGrid;
	private List<RecibosConsolidar> listRecibosExcluidosGrid;
	private List<Consolidado> lisConsolidadoGrid;
	private ConsolidarRecibosService operacionesConsolidarRecibosService;
	private String fechaFinEjecucionManual ="";
	private String fechaConsolidado ="";
	private String numPolizaConsolidar="";
	private String idRecibosConsolidar="";
	private String mensaje="";
	private String contentType;
	private ImpresionesService impresionesService;
	private InputStream inputStream;
	private String fileName;
	private String numFolio="";
	private String correo="";
	private String filterForm="";
	private Integer centroEmision;
	private Integer numRenovPol;
	private Long numTarjeta;
	private String folioFiscal="";
	private BigDecimal folio;
	private Object data;
	private BigDecimal idMedioPago;
	private List<MedioPagoDTO> medioPagoDTOs = new ArrayList<MedioPagoDTO>(1);	

	@Autowired
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	protected EntidadService entidadService;
	private ClientePolizasService clientePolizasService;
	private ImpresionRecibosService impresionRecibosService;	
	private TransporteImpresionDTO transporteExcel;
	private Boolean isDirecto;
	private String formatoDeFecha = "dd/MM/yyyy";
	private String preFijoRecibo = "Recibo_";

	
	private String consolidarRecibosSeleccionados = "<< consolidarRecibosSeleccionados()";
	@Action(value = "exportarExcelConsolidado", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = RECIBOSACONSOLIDARGRID,
							params={"fechaFinEjecucionManual","${fechaFinEjecucionManual}","numPolizaConsolidar","${numPolizaConsolidar}","fechaConsolidado","${fechaConsolidado}"})
	})
	public String exportarExcelConsolidado()   throws MidasException{
		loggerConsolidado.trace(">> exportarExcelConsolidado()");
		contentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		List<Consolidado> listConsolidados;
		Consolidado consolidado	=new Consolidado();
		consolidado.setFoliofiscal("");
		consolidado.setTotalCount(new Long("0"));
		consolidado.setPosStart(new Integer("0"));
		consolidado.setPosStart(new Integer("0"));
		consolidado.setCount(new Integer("0"));
		consolidado.setOrderByAttribute("");
		consolidado.setFilterForm("");
		consolidado.setReporteExcel("S");
		listConsolidados = operacionesConsolidarRecibosService.getlistRecibosConsolidados(consolidado);
		loggerConsolidado.info("ObtenerListadoConsolidados size = {"+ listConsolidados.size()+ "}");
		this.setLisConsolidadoGrid(listConsolidados);
		TransporteImpresionDTO respuesta =impresionesService.getExcel(listConsolidados, 
				"midas2.cobranza.recibos.consolidados.plantilla",ConstantesReporte.TIPO_XLSX);
		inputStream = new ByteArrayInputStream(respuesta.getByteArray());
		fileName = " Reporte Recibos Consolidados.xlsx";
		loggerConsolidado.trace("<< exportarExcelConsolidado()");
		return SUCCESS;
	}
	@Action(value = "exportarExcel", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = RECIBOSACONSOLIDARGRID,                                                                                 
							params={"fechaFinEjecucionManual","${fechaFinEjecucionManual}","numPolizaConsolidar","${numPolizaConsolidar}","fechaConsolidado","${fechaConsolidado}","centroEmision","${centroEmision}","numRenovPol","${numRenovPol}"})
	})
	public String exportarExcel()   throws MidasException{
		loggerConsolidado.trace(">> exportarExcel()");
		contentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		List<RecibosConsolidar> listRecibosAConsolidar;
		RecibosConsolidar reciboConsolidar = new RecibosConsolidar();
		loggerConsolidado.trace("Recibos a Consolidar: Parametros de busqueda :: Fecha Fin Ejecucion Manual = {"+fechaFinEjecucionManual+"} con el Num Poliza Consolidar: {"+numPolizaConsolidar+"}");
		DateFormat sdf = new SimpleDateFormat(formatoDeFecha);
		
		try {
			Date date = sdf.parse(fechaFinEjecucionManual);
			reciboConsolidar.setFechaCorte(date);
			
			Date dateConsolidado = sdf.parse(fechaConsolidado);
			reciboConsolidar.setFechaConsolidado(dateConsolidado);	
		} catch (Exception e) {
			loggerConsolidado.error("Metodo exportarExcel	Error en las fechas 	",e);
		  throw new MidasException("No se pudo exportar a Excel; Existe un error al obtener las fechas");
		}

		
		Long integerNumPolizaConsolidar =Long.parseLong(numPolizaConsolidar);
		reciboConsolidar.setNumPolizaConsolidar( integerNumPolizaConsolidar);
		reciboConsolidar.setCentroEmision(centroEmision);
		reciboConsolidar.setNumRenovPol(numRenovPol);
		listRecibosAConsolidar = operacionesConsolidarRecibosService.getlistRecibosAConsolidar(reciboConsolidar);
		loggerConsolidado.trace("ObtenerListadoRecibos size = {"+ listRecibosAConsolidar.size()+ "}");
		this.setListRecibosAConsolidarGrid(listRecibosAConsolidar);
		TransporteImpresionDTO respuesta =impresionesService.getExcel(listRecibosAConsolidar, 
				"midas2.cobranza.recibos.consolidar.plantilla",ConstantesReporte.TIPO_XLSX);
		inputStream = new ByteArrayInputStream(respuesta.getByteArray());
		fileName = " Reporte Recibos a Consolidar - Poliza "+numPolizaConsolidar+" " + ".xlsx";
		loggerConsolidado.trace("<< exportarExcel()");
		return SUCCESS;
	}
	@Action(value = "exportarPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = MENSAJECONSOLIDADO, params = {
							"mensaje", "${mensaje}" }) })
	public String exportarPDF()    throws MidasException{
		loggerConsolidado.trace(">> exportaPDF()");
		
			contentType="application/pdf";
			fileName=preFijoRecibo+folioFiscal+".pdf";
			byte[] reciboFiscalPDF   = null;
			try {
				reciboFiscalPDF = impresionRecibosService.getReciboFiscal(folioFiscal);
			} catch (Exception e) {
				loggerConsolidado.error("Error al exportar PDF "+e.getMessage(), e);
			}	
				if(reciboFiscalPDF!=null){
						inputStream = new ByteArrayInputStream(reciboFiscalPDF);
						fileName=preFijoRecibo+folioFiscal+".pdf";
				}else{
						mensaje="No se pudo obtener el PDF del Recibo "+folioFiscal;
						loggerConsolidado.trace("<< exportaPDF()");
						return INPUT;
				}
		
				loggerConsolidado.trace("<< exportaPDF()");
		
				return SUCCESS;
	}
	
	@Action(value = "exportarXML", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
					@Result(name = INPUT, location = MENSAJECONSOLIDADO, params = {
							"mensaje", "${mensaje}" }) })
	public String exportarXML()   throws MidasException{
		loggerConsolidado.trace(">> exportarXML()");
		
		contentType="text/xml ";
		fileName=preFijoRecibo+folioFiscal+".pdf";
		byte[] reciboFiscalXML   = null;
			try {
				reciboFiscalXML = clientePolizasService.getReciboFiscalXML(folioFiscal);
			} catch (Exception e) {
				loggerConsolidado.error("Error al exportar XML "+e.getMessage(), e);
			}
		
			if(reciboFiscalXML!=null){
				inputStream = new ByteArrayInputStream(reciboFiscalXML);
				fileName=preFijoRecibo+folioFiscal+".xml";
			}else{
				mensaje="No se pudo obtener el XML del Recibo "+folioFiscal;
				loggerConsolidado.trace("<< exportarXML()");
				return INPUT;
			}
			loggerConsolidado.trace("<< exportarXML()");
		return SUCCESS;
	}

	@Action (value = "recibosConsolidados", results = { 
			@Result(name = SUCCESS, location = RECIBOSACONSOLIDAR) 
	})
	public String recibosConsolidados(){
		MedioPagoDN medioPagoDN      = MedioPagoDN.getInstancia();
		 try {
			medioPagoDTOs = medioPagoDN.listarTodos();
		} catch (Exception e) {
			loggerConsolidado.error("Error al exportar PDF "+e.getMessage(), e);
		} 
		return SUCCESS;		
	} 
	
	@Action(value="capturarInformacionCorreo", results = 
	{@Result(name=SUCCESS, location = ENVIARCORREO,
			params={"numFolio","${numFolio}"})
			})

	public String capturarInformacionCorreo() {
		loggerConsolidado.trace(">> capturarInformacionCorreo()");
		loggerConsolidado.trace("<< capturarInformacionCorreo()");
		return SUCCESS;
	}
	
	@Action(value="mandarCorreoReciboConsolidado", results = 
	{@Result(name=SUCCESS,type="json",
			params={"noCache","true", "ignoreHierarchy", "false", "includeProperties","mensaje"})
			})

	public String mandarCorreoReciboConsolidado() {
		loggerConsolidado.trace(">> mandarCorreoReciboConsolidado()");
		EnvioCaratulaParameter envioCaratulaParameter = new EnvioCaratulaParameter();
		StringBuilder mensajeMail = new StringBuilder();
		mensajeMail.append("Se han consolidado recibos en uno solo. \r\n").append(
				"Favor de revisar los archivos adjuntos. \r\n");

		StringBuilder title = new StringBuilder();
		title.append("Recibo Consolidado: " + "CON " + numFolio);

		StringBuilder subject = new StringBuilder();
		subject.append("Recibos Consolidados en uno solo.");

		StringBuilder greeting = new StringBuilder();
		greeting.append("Estimado(a): "
				+ usuarioService.getUsuarioActual().getNombreCompleto().trim());

		envioCaratulaParameter.setPolizaID(numFolio);
		envioCaratulaParameter.setMailService(mailService);
		envioCaratulaParameter.setEntidadService(entidadService);
		envioCaratulaParameter
				.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
		envioCaratulaParameter.setLlaveFiscal("CON " + numFolio);
		envioCaratulaParameter.setMessage(mensajeMail.toString());
		envioCaratulaParameter.setTitle(title.toString());
		envioCaratulaParameter.setSubject(subject.toString());
		envioCaratulaParameter.setGreeting(greeting.toString());
		envioCaratulaParameter.setCorreo(correo);
		clientePolizasService.envioPDFyXML(envioCaratulaParameter);

		mensaje="Se guardo consolidado.";		
		loggerConsolidado.trace("<< mandarCorreoReciboConsolidado()");
		return SUCCESS;
	}
	@Action(value = "llenarRecibosYaConsolidados", results = {			 
			@Result(name = SUCCESS, location = RECIBOSYACONSOLIDADOSGRID,
			params={}),
			@Result(name = INPUT, location = RECIBOSYACONSOLIDADOSGRID,
			params={})
		})
			public String llenarRecibosYaConsolidados() throws MidasException {
		loggerConsolidado.trace(">> llenarRecibosYaConsolidados()");
				
				List<Consolidado> listConsolidados;
				Consolidado consolidado	=new Consolidado();
				if(super.getCount() == null){
					super.setCount(20);
				}
				if(super.getPosStart() == null){
					super.setPosStart(0);
				}
				if(super.getTotalCount() == null){
					super.setTotalCount(0L);
				}
				
				if(super.getOrderBy() == null){
					super.setOrderBy("1");
					super.setDirect("desc");
				}
				consolidado.setPosStart(super.getPosStart());
				consolidado.setCount(super.getCount());
				consolidado.setOrderByAttribute(decodeOrderBy());
				consolidado.setTotalCount(super.getTotalCount());
				consolidado.setFilterForm(filterForm);
				consolidado.setReporteExcel("");
				listConsolidados = operacionesConsolidarRecibosService.getlistRecibosConsolidados(consolidado);
				loggerConsolidado.info("obtenerListadoConsolidados size = {"+ listConsolidados.size()+ "}");
				if(!listConsolidados.isEmpty()){
					super.setTotalCount((listConsolidados.get(0)).getTotalCount());		
				}else{
					super.setTotalCount(0L);	
				}
				this.setLisConsolidadoGrid(listConsolidados);
				
				loggerConsolidado.info("<< llenarRecibosYaConsolidados()");
				return SUCCESS;
			}	
	
	private String decodeOrderBy(){
		String order = null;
		if(!StringUtil.isEmpty(super.getOrderBy())){
			order = COLUMNAS_GRID_CONSOLIDADOS[Integer.valueOf(super.getOrderBy())];
			if(!StringUtil.isEmpty(super.getDirect())){
				order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
			}
		}		
		return order;
	}
	
	
	
	@Action(value = "llenarListadoReciboConsolidado", results = {			 
	@Result(name = SUCCESS, location = RECIBOSACONSOLIDARGRID,
	params={"fechaFinEjecucionManual","${fechaFinEjecucionManual}","numPolizaConsolidar","${numPolizaConsolidar}",
			"fechaConsolidado","${fechaConsolidado}","centroEmision","${centroEmision}","numRenovPol","${numRenovPol}"}),
	@Result(name = INPUT, location = RECIBOSACONSOLIDARGRID,
	params={"fechaFinEjecucionManual","${fechaFinEjecucionManual}","numPolizaConsolidar","${numPolizaConsolidar}",
			"fechaConsolidado","${fechaConsolidado}","centroEmision","${centroEmision}","centroEmision","${numRenovPol}"})
})
	public String llenarListadoReciboConsolidado() throws MidasException {
		loggerConsolidado.trace(">> llenarListadoReciboConsolidado()");
		
		List<RecibosConsolidar> listRecibosAConsolidar;
		loggerConsolidado.info("Parametros de busqueda :: Fecha Fin Ejecucion Manual = {"+fechaFinEjecucionManual+"} con el Num Poliza Consolidar: {"+numPolizaConsolidar+"}" +
				" con Centro de Emision: {"+centroEmision+"}" +
				" con el Num de Renovacion de Poliza: {"+numRenovPol+"}" +
		"");
		RecibosConsolidar reciboConsolidar = new RecibosConsolidar();
		DateFormat sdf = new SimpleDateFormat(formatoDeFecha);
		
		try {
			Date date = sdf.parse(fechaFinEjecucionManual);
			reciboConsolidar.setFechaCorte(date);
			
			Date dateConsolidado = sdf.parse(fechaConsolidado);
			reciboConsolidar.setFechaConsolidado(dateConsolidado);
		} catch (Exception e) {
			loggerConsolidado.error("Metodo llenarListadoReciboConsolidado Error en las fechas 	",e);
		  throw new MidasException("No se puede obtener Recibos Consolidados; Error en las fechas");
		}

		
		
		Long integerNumPolizaConsolidar =Long.parseLong(numPolizaConsolidar);
		reciboConsolidar.setNumPolizaConsolidar( integerNumPolizaConsolidar);
		reciboConsolidar.setNumRenovPol(numRenovPol);
		reciboConsolidar.setCentroEmision(centroEmision);
		listRecibosAConsolidar = operacionesConsolidarRecibosService.getlistRecibosAConsolidar(reciboConsolidar);
		loggerConsolidado.info("obtenerListadoRecibos size = {"+ listRecibosAConsolidar.size()+ "}");
		this.setListRecibosAConsolidarGrid(listRecibosAConsolidar);
		this.setListRecibosExcluidosGrid(new ArrayList<RecibosConsolidar>());
		
		loggerConsolidado.trace("<< llenarListadoReciboConsolidado()");
		return SUCCESS;
	}
	@Action(value = "cancelarRecibosSeleccionados", results = {			
			@Result(name = INPUT,type="json",
					params={"noCache","true", "ignoreHierarchy", "false", "includeProperties","mensaje"})
	})
	public String cancelarRecibosSeleccionados() throws MidasException {
		loggerConsolidado.trace(">> cancelarRecibosSeleccionados()");
		
		loggerConsolidado.trace("Parametros de consolidacion :: idRecibosConsolidar = {"+idRecibosConsolidar+"}");
		List<Consolidado> listRecibosConsolidados;
		RecibosConsolidar reciboConsolidar = new RecibosConsolidar();
		reciboConsolidar.setIdRecibosConsolidar(idRecibosConsolidar);
		reciboConsolidar.setNombreUsuario(usuarioService.getUsuarioActual().getNombreUsuario().trim());
		listRecibosConsolidados = operacionesConsolidarRecibosService.cancelarRecibosSeleccionados(reciboConsolidar);
		this.setLisConsolidadoGrid(listRecibosConsolidados);
		mensaje="Se cancelaron correctamente los recibos.";
		loggerConsolidado.trace("<< cancelarRecibosSeleccionados()");
		return INPUT;
		
	}
	
	
	@Action(value = "consolidarRecibosSeleccionados", results = {			
			@Result(name = SUCCESS, type="redirectAction",
			params={"actionName","mandarCorreoReciboConsolidado","numFolio","${numFolio}","idRecibosConsolidar","${idRecibosConsolidar}","fechaFinEjecucionManual","${fechaFinEjecucionManual}","numPolizaConsolidar","${numPolizaConsolidar}","fechaConsolidado","${fechaConsolidado}","idMedioPago","${idMedioPago}","numTarjeta","${numTarjeta}"}),
			@Result(name = INPUT,type="json",
					params={"noCache","true", "ignoreHierarchy", "false", "includeProperties","mensaje"})
	})
	public String consolidarRecibosSeleccionados() throws MidasException {
		loggerConsolidado.trace(">> consolidarRecibosSeleccionados()");
		
		loggerConsolidado.info("Parametros de consolidacion :: idRecibosConsolidar = {"+idRecibosConsolidar+"} con la fechaFinEjecucionManual: {"+fechaFinEjecucionManual+"} con el numPolizaConsolidar: {"+numPolizaConsolidar+"}"+"} con el idMedioPago: {"+idMedioPago+"}" + " con el numTarjeta: {"+numTarjeta+"}");
		List<RelRecibosConsolidado> listRecibosConsolidados;
		RecibosConsolidar reciboConsolidar;
		DateFormat sdf = new SimpleDateFormat(formatoDeFecha);
		Date date;
		Date dateFechaConsolidado;
		try {
			 date = sdf.parse(fechaFinEjecucionManual);
			 dateFechaConsolidado = sdf.parse(fechaConsolidado);
		} catch (Exception e) {
			loggerConsolidado.error("Metodo consolidarRecibosSeleccionados Error en las fechas 	",e);
		  throw new MidasException("No se puede consolidar recibos; Error al obtener las fechas");
		}
		reciboConsolidar = new RecibosConsolidar();
		reciboConsolidar.setFechaCorte(date);
		reciboConsolidar.setFechaConsolidado(dateFechaConsolidado);
		
		Long integerNumPolizaConsolidar =Long.parseLong(numPolizaConsolidar);
		reciboConsolidar.setNumPolizaConsolidar( integerNumPolizaConsolidar);
		reciboConsolidar.setIdRecibosConsolidar(idRecibosConsolidar);
		reciboConsolidar.setNombreUsuario(usuarioService.getUsuarioActual().getNombreCompleto().trim());
		listRecibosConsolidados = operacionesConsolidarRecibosService.consolidarRecibosSeleccionados(reciboConsolidar);
		RelRecibosConsolidado  recibosConsolidar = listRecibosConsolidados.get(0);
		
		if(recibosConsolidar.getTieneError().booleanValue()){
			mensaje=recibosConsolidar.getDescripcionError();
			
			loggerConsolidado.trace(consolidarRecibosSeleccionados);
			return INPUT;
		}else{
			folio = new BigDecimal(recibosConsolidar.getReferenciacargoconsolidado());
			numFolio=""+folio;
			Consolidado consolidado= new Consolidado();
			try {
				
				consolidado.setFoliofiscal(folio.toString());
				consolidado.setIdMedioPago(idMedioPago);
				consolidado.setNumTarjeta(new Long(numTarjeta));
				correrCFDConsolidado(consolidado);
			} catch (Exception e) {
				loggerConsolidado.error("Se genera error al consolidar", e);
				mensaje=e.getMessage();
				loggerConsolidado.trace(consolidarRecibosSeleccionados);
				return INPUT;
			}
			
			
			loggerConsolidado.trace(consolidarRecibosSeleccionados);
			return SUCCESS;
		}
		
	}

	public String correrCFDConsolidado(Consolidado consolidado){
		loggerConsolidado.trace(">> llenarListadoReciboConsolidado()");
		
		data = operacionesConsolidarRecibosService.correrCFDConsolidado(consolidado);
		
		loggerConsolidado.trace("<< llenarListadoReciboConsolidado()");
		return SUCCESS;
	}
	
	/**
	 * this method is responsible to allow the action to prepare itself
	 */
	@Override
	public void prepare() throws MidasException {
		loggerConsolidado.trace(">> prepare()");
		loggerConsolidado.trace("<< prepare()");
	}
	 
	public List<RecibosConsolidar> getListRecibosAConsolidarGrid() {
		return listRecibosAConsolidarGrid;
	}
	public void setListRecibosAConsolidarGrid(
			List<RecibosConsolidar> listRecibosAConsolidarGrid) {
		this.listRecibosAConsolidarGrid = listRecibosAConsolidarGrid;
	}
	
	
	/**
	 * @return the listRecibosExcluidosGrid
	 */
	public List<RecibosConsolidar> getListRecibosExcluidosGrid() {
		return listRecibosExcluidosGrid;
	}
	/**
	 * @param listRecibosExcluidosGrid the listRecibosExcluidosGrid to set
	 */
	public void setListRecibosExcluidosGrid(
			List<RecibosConsolidar> listRecibosExcluidosGrid) {
		this.listRecibosExcluidosGrid = listRecibosExcluidosGrid;
	}
	/**
	 * @return the lisConsolidadoGrid
	 */
	public List<Consolidado> getLisConsolidadoGrid() {
		return lisConsolidadoGrid;
	}

	/**
	 * @param lisConsolidadoGrid the lisConsolidadoGrid to set
	 */
	public void setLisConsolidadoGrid(List<Consolidado> lisConsolidadoGrid) {
		this.lisConsolidadoGrid = lisConsolidadoGrid;
	}

	/**
	 * @return the numFolio
	 */
	public String getNumFolio() {
		return numFolio;
	}


	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}


	/**
	 * @return the filterForm
	 */
	public String getFilterForm() {
		return filterForm;
	}
	/**
	 * @param filterForm the filterForm to set
	 */
	public void setFilterForm(String filterForm) {
		this.filterForm = filterForm;
	}
	
	
	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}
	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	/**
	 * @return the centroEmision
	 */
	public Integer getCentroEmision() {
		return centroEmision;
	}
	/**
	 * @param centroEmision the centroEmision to set
	 */
	public void setCentroEmision(Integer centroEmision) {
		this.centroEmision = centroEmision;
	}
	/**
	 * @return the numRenovPol
	 */
	public Integer getNumRenovPol() {
		return numRenovPol;
	}
	/**
	 * @param numRenovPol the numRenovPol to set
	 */
	public void setNumRenovPol(Integer numRenovPol) {
		this.numRenovPol = numRenovPol;
	}
	/**
	 * @return the operacionesConsolidarRecibosService
	 */
	public ConsolidarRecibosService getOperacionesConsolidarRecibosService() {
		return operacionesConsolidarRecibosService;
	}
	/**
	 * @param operacionesConsolidarRecibosService the operacionesConsolidarRecibosService to set
	 */
	@Autowired
	@Qualifier("operacionesConsolidarRecibosServiceEJB")
	public void setOperacionesConsolidarRecibosService(
			ConsolidarRecibosService operacionesConsolidarRecibosService) {
		this.operacionesConsolidarRecibosService = operacionesConsolidarRecibosService;
	}
	
	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
    public void setGeneraPlantillaReporteBitemporalService(GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService){
    	this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService; 
    }
	
	
	public ClientePolizasService getClientePolizasService() {
		return clientePolizasService;
	}
	@EJB
	public void setImpresionRecibosService(
			ImpresionRecibosService impresionRecibosService) {
		this.impresionRecibosService = impresionRecibosService;
	}
	@Autowired
	@Qualifier("clientePolizasServiceEJB")
	public void setClientePolizasService(ClientePolizasService clientePolizasService) {
		this.clientePolizasService = clientePolizasService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService){
		this.entidadService = entidadService;
	}

	/**
	 * @return the fechaFinEjecucionManual
	 */
	public String getFechaFinEjecucionManual() {
		return fechaFinEjecucionManual;
	}

	/**
	 * @param fechaFinEjecucionManual the fechaFinEjecucionManual to set
	 */
	public void setFechaFinEjecucionManual(String fechaFinEjecucionManual) {
		this.fechaFinEjecucionManual = fechaFinEjecucionManual;
	}

	/**
	 * @return the numPolizaConsolidar
	 */
	public String getNumPolizaConsolidar() {
		return numPolizaConsolidar;
	}

	/**
	 * @param numPolizaConsolidar the numPolizaConsolidar to set
	 */
	public void setNumPolizaConsolidar(String numPolizaConsolidar) {
		this.numPolizaConsolidar = numPolizaConsolidar;
	}

	/**
	 * @return the idRecibosConsolidar
	 */
	public String getIdRecibosConsolidar() {
		return idRecibosConsolidar;
	}

	/**
	 * @param idRecibosConsolidar the idRecibosConsolidar to set
	 */
	public void setIdRecibosConsolidar(String idRecibosConsolidar) {
		this.idRecibosConsolidar = idRecibosConsolidar;
	}


	@Override
	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}


	@Override
	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return the fechaConsolidado
	 */
	public String getFechaConsolidado() {
		return fechaConsolidado;
	}

	/**
	 * @param fechaConsolidado the fechaConsolidado to set
	 */
	public void setFechaConsolidado(String fechaConsolidado) {
		this.fechaConsolidado = fechaConsolidado;
	}
	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}
	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Boolean getIsDirecto() {
		return isDirecto;
	}
	public void setIsDirecto(Boolean isDirecto) {
		this.isDirecto = isDirecto;
	}

	/**
	 * @return the numTarjeta
	 */
	public Long getNumTarjeta() {
		return numTarjeta;
	}
	/**
	 * @param numTarjeta the numTarjeta to set
	 */
	public void setNumTarjeta(Long numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	/**
	 * @return the medioPagoDTOs
	 */
	public List<MedioPagoDTO> getMedioPagoDTOs() {
		return medioPagoDTOs;
	}
	/**
	 * @param medioPagoDTOs the medioPagoDTOs to set
	 */
	public void setMedioPagoDTOs(List<MedioPagoDTO> medioPagoDTOs) {
		this.medioPagoDTOs = medioPagoDTOs;
	}
	/**
	 * @return the folioFiscal
	 */
	public String getFolioFiscal() {
		return folioFiscal;
	}
	/**
	 * @param folioFiscal the folioFiscal to set
	 */
	public void setFolioFiscal(String folioFiscal) {
		this.folioFiscal = folioFiscal;
	}

	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}
	@Autowired
		@Qualifier("impresionesServiceEJB")
		public void setImpresionesService(ImpresionesService impresionesService) {
			this.impresionesService = impresionesService;
		}
}
