package mx.com.afirme.midas2.action.notificacionCierreMes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.notificacionCierreMes.NotificacionCM;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.service.NotificacionesCierreMes.NotificacionCierreMesService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/notificacionCierreMes")
public class NotificacionCierreMesAction extends BaseAction implements
		Preparable {

	private static final Logger LOG = Logger
			.getLogger(NotificacionCierreMesAction.class);
	/**
	 * Variables name (to be used in jsp with Languaje)
	 */
	private static final long serialVersionUID = 1L;
	private final String FAIL = "/jsp/notificacionCierreMes/notificacionMes.jsp";
	private final String JSP_FORM = "/jsp/notificacionCierreMes/notificacionMes.jsp";
	private final String JSP_LIST = "/jsp/notificacionCierreMes/listarNotificacionCierreMes.jsp";
	/**
	 * Main entity service
	 */
	private ParametroGlobalService parametroGlobalService;

	private EntidadService entidadService;
	private Usuario usuario;
	private String fechaActual;
	private String fechSys;

	private NotificacionCM notificaObj = new NotificacionCM();
	private List<NotificacionCM> notificaList = new ArrayList<NotificacionCM>();

	private ParametroGlobal paraGlo = new ParametroGlobal();
	private NotificacionCierreMesService notificacionCierreMesService;

	private String fechaEjecucion;
	private String fechaNueva;
	private boolean active;

	@Autowired
	private Environment env;

	/**
	 * Default constructor
	 */
	public NotificacionCierreMesAction() {
		super();
		LOG.info("NotificacionCierreMesAction load.");
	}

	private Usuario getUsuarioSesion() {
		HttpServletRequest request = ServletActionContext.getRequest();
		SistemaContext sistemaContext = WebApplicationContextUtils
				.getWebApplicationContext(request.getServletContext()).getBean(
						SistemaContext.class);
		return (Usuario) request.getSession().getAttribute(
				sistemaContext.getUsuarioAccesoMidas());
	}

	/**
	 * EJB
	 * 
	 * @param entidadService
	 */
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	public void setParametroGlobalService(
			ParametroGlobalService parametroGlobalService) {
		this.parametroGlobalService = parametroGlobalService;
	}

	@Autowired
	@Qualifier("notificacionCierreMesServiceEJB")
	public void setNotificacionCierreMesService(
			NotificacionCierreMesService notificacionCierreMesService) {
		this.notificacionCierreMesService = notificacionCierreMesService;
	}

	/**
	 * Regresa los datos de la Base de datos para el llenado de la pantalla
	 * inicial de configuracion.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "mostrarAdminNotificacion", results = {
			@Result(name = SUCCESS, location = JSP_FORM),
			@Result(name = FAIL, location = FAIL) })
	public String mostrarAdminNotificacion() throws Exception {
		ParametroGlobal pGlobal = new ParametroGlobal();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaActual = sdf.format(Calendar.getInstance().getTime());
		usuario = this.getUsuarioSesion();
		String fechaProx = "";
		LOG.info("ID User : " + usuario.getId() + "Nombre User : "
				+ usuario.getNombreCompleto() + " - " + fechaActual);

		try {
			pGlobal = parametroGlobalService.obtenerPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.NOTIFICACION_CIERRE_MES);
			pGlobal.getValue();
			fechaEjecucion = pGlobal.getValue();

			String fechaBD = fechaEjecucion;
			String[] dato1 = fechaBD.split("/");
			String day = dato1[0];

			fechaEjecucion = notificacionCierreMesService.convertFecha(day);
			active = pGlobal.isActivo();

		} catch (Exception e) {
			e.printStackTrace();
			return FAIL;
		}
		return SUCCESS;
	}

	/**
	 * Save Guarda Los datos de configuracion de la pantalla Notificacion Cierre
	 * Mes
	 * 
	 * @return
	 */
	@Action(value = "guardarDatos", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarAdminNotificacion", "namespace",
					"/fuerzaventa/notificacionCierreMes", "mensaje",
					"${mensaje}", "tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, location = FAIL) })
	public String guardarDatos() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			fechaActual = sdf.format(Calendar.getInstance().getTime());

			Date ahora = new Date();
			SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
			LOG.info(formateador.format(ahora));

			String fecha = formateador.format(ahora);
			Date dt = null;
			if ((fecha != null) && (!"".equals(fecha))) {
				dt = formateador.parse(fecha);
			}

			ParametroGlobal pGlobal = new ParametroGlobal();
			pGlobal = parametroGlobalService.obtenerPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.NOTIFICACION_CIERRE_MES);
			pGlobal.setActivo(active);
			if (fechaNueva != null && fechaNueva.equals("")) {
				fechaNueva = pGlobal.getValor();
			}
			pGlobal.setValor(fechaNueva);
			notificacionCierreMesService.saveNotificacionMes(pGlobal);
			setMensajeExito();

		} catch (Exception e) {
			e.printStackTrace();
			setMensajeError("Ocurrio un Error");
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * Get & Set Class NotificacionCierreMesAction
	 * 
	 * @return
	 */

	public NotificacionCM getNotificaObj() {
		return notificaObj;
	}

	public void setNotificaObj(NotificacionCM notificaObj) {
		this.notificaObj = notificaObj;
	}

	public List<NotificacionCM> getNotificaList() {
		return notificaList;
	}

	public void setNotificaList(List<NotificacionCM> notificaList) {
		this.notificaList = notificaList;
	}

	public String getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(String fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getFechaNueva() {
		return fechaNueva;
	}

	public void setFechaNueva(String fechaNueva) {
		this.fechaNueva = fechaNueva;
	}

}
