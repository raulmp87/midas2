/**
 * 
 */
package mx.com.afirme.midas2.action.envioxml;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.PersistenceException;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.js.util.StringUtil;
import com.opensymphony.xwork2.Preparable;

/**
 * @author jreyes
 *
 */
@Namespace("/envioxml")
@Component
@Scope("prototype")
public class EnvioFacturaAction extends BaseAction  implements Preparable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 379769784366423701L;

	/** Log de EnvioFacturaServiceImpl */
	private static Logger log = Logger.getLogger(EnvioFacturaAction.class);
	
	private EnvioFactura envioFactura;
	private Date fechaFinal;
	private Date fechaInicial;
	private boolean hideCargar;
	private String fileName;
	private List<EnvioFactura> xmls = new LinkedList<EnvioFactura>();	

	private Usuario usuario;
	private EnvioFacturaService envioFacturaService;

	private final String RECEPCION = "/WEB-INF/jsp/envioxml/recepcion.jsp";
	private final String MOSTRAR = "/WEB-INF/jsp/envioxml/mostrar.jsp";
	private final String LISTAR = "/WEB-INF/jsp/envioxml/listar.jsp";
	private final String LISTARDET = "/WEB-INF/jsp/envioxml/listardet.jsp";
	
	private final String D_SINVID = "LIQVID";
	private final String D_SINAUT = "LIQAUT";
	private final String D_LIQSP = "LIQSP";
	private final String D_REASEGU = "REASEGU";
	
	private final String FN_M2_SN_SIN_VIDA = "FN_M2_SN_Siniestros_Vida";
	private final String FN_M2_SN_LIQ_AUTOS = "FN_M2_SN_Liquidacion_Autos";
	private final String FN_M2_SN_LIQ_SERV_PUBLICO = "FN_M2_SN_Liquidacion_Servicio_Publico";
	private final String ROL_OP_REASEGURO = "FN_M2_SN_Rol_Op_Reaseguro_Aut";
	
	public String origenEnvio = "";
			
	@Action(value="mostrar",results={@Result(name=SUCCESS,location=MOSTRAR)})
	public String mostrar() {
		if(envioFactura!=null && envioFactura.getOrigenEnvio()!=null && envioFactura.getIdOrigenEnvio()==null){
			
			if(envioFactura.getOrigenEnvio().equalsIgnoreCase(D_REASEGU) || envioFactura.getOrigenEnvio().equalsIgnoreCase(D_SINVID) || 
					envioFactura.getOrigenEnvio().equalsIgnoreCase(D_SINAUT) || envioFactura.getOrigenEnvio().equalsIgnoreCase(D_LIQSP)) {				
				envioFactura.setIdOrigenEnvio(new Long(0));
				/* Validación en No Cheque / TR */
				log.error("Error en número de Cheque/TR");
				this.setMensajeError("No se ingresó número de Cheque/TR");
			} else {
				long idOrigenEnvio = envioFacturaService.getIncrementedProperty(envioFactura);
				envioFactura.setIdOrigenEnvio(idOrigenEnvio);
			}
			
		}
		return SUCCESS;
	}
	
	@Action(value="listar",results={@Result(name=SUCCESS,location=LISTAR)})
	public String listar() {
		
		try {
			xmls = envioFacturaService.findByOperation(envioFactura, fechaFinal, fechaInicial);
		} catch ( Exception e) {
			log.error("Error al obtener la lista de envios : ", e);
		}
		
		return SUCCESS;
	}
	
	@Action(value="listardet",results={@Result(name=SUCCESS,location=LISTARDET)})
	public String listardet() {
		
		try {
			envioFactura.setRespuestas( envioFacturaService.findByIdEnvio(envioFactura.getIdEnvio() ));
		} catch ( Exception e) {
			log.error("Error al obtener la respuesta del envio : ", e);
		}
		
		return SUCCESS;
	}
	
	@Action(value="cargar",results={@Result(name=SUCCESS,location=MOSTRAR)})
	public String cargar () {
		boolean isFileXML = false;
		String extension = null;
		if(envioFactura!=null && envioFactura.getFacturaXml()!=null && fileName!=null){
			
			extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
			
			if (extension != null
					&& ("xml".equalsIgnoreCase(extension.trim()) 
							|| "zip".equalsIgnoreCase(extension.trim()))) {
				isFileXML = true;
			}
		}
		if(isFileXML){
			
			try {
				/* Asignar departamento */
				if(envioFactura.getOrigenEnvio().equalsIgnoreCase(D_SINVID)) {
					envioFactura.setIdDepartamento("0005");
				} else if(envioFactura.getOrigenEnvio().equalsIgnoreCase(D_SINAUT)) {
					envioFactura.setIdDepartamento("0003");
				} else if(envioFactura.getOrigenEnvio().equalsIgnoreCase(D_LIQSP)) {
					envioFactura.setIdDepartamento("0006");
				} else if(envioFactura.getOrigenEnvio().equalsIgnoreCase(D_REASEGU)) {
					envioFactura.setIdDepartamento("0008");
				}
				
				/* Validación en No Cheque / TR */
				if(envioFactura.getIdOrigenEnvio() == 0) {
					log.error("Error en número de Cheque/TR");
					this.setMensajeError("No se ingresó número de Cheque/TR");
				} else {
					envioFacturaService.saveEnvio(envioFactura, extension);
				}
				
							
			} catch (SystemException e) {
				this.setMensajeError(e.getMessage());
			} catch (PersistenceException e) {
				log.error("Error al cargar factura xml : ", e);
				this.setMensajeError("Error al guardar los datos de la factura");
			} catch (Exception e) {
				log.error("Ocurrio un error al subir la factura xml : ", e);
				this.setMensajeError("Ocurrio un error al subir la factura xml : ");
			}
		}else{
			this.setMensajeError("Revise el archivo, solo se aceptan XML");
		}
		
		return SUCCESS;
	}
	
	@Action(value="recepcion",results={@Result(name=SUCCESS,location=RECEPCION)})
	public String recepcion() {

		usuario = usuarioService.getUsuarioActual();
		
		if(usuarioService.tienePermisoUsuarioActual(FN_M2_SN_SIN_VIDA)) {
			setOrigenEnvio(FN_M2_SN_SIN_VIDA);			
		} else if(usuarioService.tienePermisoUsuarioActual(FN_M2_SN_LIQ_AUTOS)) {
			setOrigenEnvio(FN_M2_SN_LIQ_AUTOS);			
		} else if(usuarioService.tienePermisoUsuarioActual(FN_M2_SN_LIQ_SERV_PUBLICO)) {
			setOrigenEnvio(FN_M2_SN_LIQ_SERV_PUBLICO);			
		} else if(usuarioService.tienePermisoUsuarioActual(ROL_OP_REASEGURO)) {
			setOrigenEnvio(ROL_OP_REASEGURO);
		}
		
		return SUCCESS;
	}	
	
	/* ************ Getters & setters ************ */
	/**
	 * @return el hideCargar
	 */
	public boolean getHideCargar() {
		return hideCargar;
	}

	/**
	 * @param hideCargar el hideCargar a establecer
	 */
	public void setHideCargar(boolean hideCargar) {
		this.hideCargar = hideCargar;
	}

	/**
	 * @return el fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName el fileName a establecer
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return el xmls
	 */
	public List<EnvioFactura> getXmls() {
		return xmls;
	}

	/**
	 * @param xmls el xmls a establecer
	 */
	public void setXmls(List<EnvioFactura> xmls) {
		this.xmls = xmls;
	}

	/**
	 * @return el envioFactura
	 */
	public EnvioFactura getEnvioFactura() {
		return envioFactura;
	}

	/**
	 * @param envioFactura el envioFactura a establecer
	 */
	public void setEnvioFactura(EnvioFactura envioFactura) {
		this.envioFactura = envioFactura;
	}

	/**
	 * @return el envioFacturaService
	 */
	public EnvioFacturaService getEnvioFacturaService() {
		return envioFacturaService;
	}

	/**
	 * @return el fechaFinal
	 */
	public Date getFechaFinal() {
		return fechaFinal;
	}

	/**
	 * @param fechaFinal el fechaFinal a establecer
	 */
	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	/**
	 * @return el fechaInicial
	 */
	public Date getFechaInicial() {
		return fechaInicial;
	}

	/**
	 * @param fechaInicial el fechaInicial a establecer
	 */
	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	
	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getOrigenEnvio() {
		return origenEnvio;
	}

	public void setOrigenEnvio(String origenEnvio) {
		this.origenEnvio = origenEnvio;
	}

	/**
	 * @param envioFacturaService el envioFacturaService a establecer
	 */
	@Autowired
	@Qualifier("envioFacturaEJB")
	public void setEnvioFacturaService(EnvioFacturaService envioFacturaService) {
		this.envioFacturaService = envioFacturaService;
	}

	@Override
	public void prepare() throws Exception {}
}
