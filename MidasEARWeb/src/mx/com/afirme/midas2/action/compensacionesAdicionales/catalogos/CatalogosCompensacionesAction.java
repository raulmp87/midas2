package mx.com.afirme.midas2.action.compensacionesAdicionales.catalogos;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;
import mx.com.afirme.midas2.domain.compensaciones.Proveedor;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/compensacionesAdicionales/catalogos")
public class CatalogosCompensacionesAction extends BaseAction{
	private static final long serialVersionUID = 4315317481915127212L;
	
	private CatalogoCompensacionesService catalogoCompensacionesService;
	private ListadoService listadoService;	
	private String descripcionBusquedaAgente;
	private String idGerencias;
	private List<AgenteView> agentesList;
	private List<DatosSeycos> listaDatosSeycos;
	private List<Proveedor> listaProveedores;
	private List<Promotoria> listaPromotoria;
	private List<Negocio> listaNegocios ;
	public List<CaEntidadPersona> entidadPersonas;
	private String catalogo;
	private int numeroCatalogo ;
	private int totalEncontrados ;
	private Long tipoEntidad;
	private int soloDescripcion = 0;
	
	@Action(value="ventanaCatalogo",
			results={@Result( name=SUCCESS, location="/jsp/compensacionesAdicionales/catalogos/seleccionarAgente.jsp")})
	public String ventanaAgentes(){
		return SUCCESS;
	}
	@Action(value="buscarCatalogo",
			results={@Result( name=SUCCESS, location="/jsp/compensacionesAdicionales/catalogos/resultadoBusquedaAgente.jsp"),
			         @Result( name=INPUT, location="/jsp/compensacionesAdicionales/catalogos/resultadoBusquedaAgente.jsp")})
	public String buscarAgente(){
			if(descripcionBusquedaAgente != null){
				if (catalogo.trim().compareTo("AGENTE") == 0 ) {
					agentesList = listadoService.listaAgentesPorDescripcionLightWeightAutorizados(descripcionBusquedaAgente.trim().toUpperCase());
					numeroCatalogo = 1;
					totalEncontrados = agentesList.size();
				}
				if (catalogo.trim().compareTo("PROVEEDOR") == 0 ) {
				    listaProveedores = catalogoCompensacionesService.listarProveedores(descripcionBusquedaAgente);
				    numeroCatalogo = 2;
				    totalEncontrados = listaProveedores.size();
				}
				if (catalogo.trim().compareTo("NEGOCIO") == 0 ) {
					listaNegocios = catalogoCompensacionesService.listarNegocios(descripcionBusquedaAgente);
					numeroCatalogo = 3;
					totalEncontrados = listaNegocios.size();
				}
				if (catalogo.trim().compareTo("ORDEN_PAGOS") == 0 ) {
					entidadPersonas = catalogoCompensacionesService.listarAgentesProveedores(descripcionBusquedaAgente,tipoEntidad);
					numeroCatalogo = 4;				
					LOG.info("Tipo Persona Selecionada "+ tipoEntidad);
					if(tipoEntidad == CaTipoEntidad.TIPO_CONTRATANTE){
						soloDescripcion=1;					
					}							
					totalEncontrados = entidadPersonas.size();					
				 }
				if (catalogo.trim().compareTo("AGENTE_SEYCOS") == 0 ) {
					listaDatosSeycos = listadoService.listarAgenteSeycos(descripcionBusquedaAgente, idGerencias);
					numeroCatalogo = 5;
					totalEncontrados = listaDatosSeycos.size();
				}
				if (catalogo.trim().compareTo("PROMOTOR") == 0 ) {
					setListaPromotoria(catalogoCompensacionesService.listarPromotores(descripcionBusquedaAgente));
					numeroCatalogo = 6;
					totalEncontrados = getListaPromotoria().size();
				}
			}else{
			agentesList = new ArrayList<AgenteView>();
			}	
		return INPUT;
	}	
	
	
	
	public String getDescripcionBusquedaAgente() {
		return descripcionBusquedaAgente;
	}

	public void setDescripcionBusquedaAgente(String descripcionBusquedaAgente) {
		this.descripcionBusquedaAgente = descripcionBusquedaAgente;
	}
	
	public String getIdGerencias() {
		return idGerencias;
	}

	public void setIdGerencias(String idGerencias) {
		this.idGerencias = idGerencias;
	}

	public List<AgenteView> getAgentesList() {
		return agentesList;
	}

	public void setAgentesList(List<AgenteView> agentesList) {
		this.agentesList = agentesList;
	}

	@Autowired
	@Qualifier("catalogoCompensacionesServiceEJB")
	public void setCatalogoCompensacionesService(
			CatalogoCompensacionesService catalogoCompensacionesService) {
		this.catalogoCompensacionesService = catalogoCompensacionesService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	
	public List<Proveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<Proveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public List<Negocio> getListaNegocios() {
		return listaNegocios;
	}

	public void setListaNegocios(List<Negocio> listaNegocios) {
		this.listaNegocios = listaNegocios;
	}

	public String getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(String catalogo) {
		this.catalogo = catalogo;
	}

	public int getNumeroCatalogo() {
		return numeroCatalogo;
	}

	public void setNumeroCatalogo(int numeroCatalogo) {
		this.numeroCatalogo = numeroCatalogo;
	}

	public int getTotalEncontrados() {
		return totalEncontrados;
	}

	public void setTotalEncontrados(int totalEncontrados) {
		this.totalEncontrados = totalEncontrados;
	}
	
	public List<CaEntidadPersona> getEntidadPersonas() {
		return entidadPersonas;
	}

	public void setEntidadPersonas(List<CaEntidadPersona> entidadPersonas) {
		this.entidadPersonas = entidadPersonas;
	}

	public Long getTipoEntidad() {
		return tipoEntidad;
	}

	public void setTipoEntidad(Long tipoEntidad) {
		this.tipoEntidad = tipoEntidad;
	}
	public int getSoloDescripcion() {
		return soloDescripcion;
	}
	public void setSoloDescripcion(int soloDescripcion) {
		this.soloDescripcion = soloDescripcion;
	}
	public List<DatosSeycos> getListaDatosSeycos() {
		return listaDatosSeycos;
	}
	public void setListaDatosSeycos(List<DatosSeycos> listaDatosSeycos) {
		this.listaDatosSeycos = listaDatosSeycos;
	}
	public void setListaPromotoria(List<Promotoria> listaPromotoria) {
		this.listaPromotoria = listaPromotoria;
	}
	public List<Promotoria> getListaPromotoria() {
		return listaPromotoria;
	}
	
}
