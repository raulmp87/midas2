package mx.com.afirme.midas2.action.compensacionesAdicionales.banca;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionBanca;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;
import mx.com.afirme.midas2.dto.compensaciones.CalculosBancaView;
import mx.com.afirme.midas2.dto.compensaciones.ConfiguracionContraprestacionBanca;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.compensaciones.CaBancaPrimPagCPService;
import mx.com.afirme.midas2.service.compensaciones.CaBancaPrimPagService;
import mx.com.afirme.midas2.service.compensaciones.CaBancaSiniMesService;
import mx.com.afirme.midas2.service.compensaciones.CaBitacoraService;
import mx.com.afirme.midas2.service.compensaciones.CaConfiguracionBancaService;
import mx.com.afirme.midas2.service.compensaciones.CaRamoService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;
import java.io.File;
import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Qualifier;

@Component
@Scope("prototype")
@Namespace("/compensacionesAdicionales/banca")
public class CompensacionAdicionalBancaAction extends BaseAction {

	private static final long serialVersionUID = 4147253641997019560L;

	@Autowired
	private CaRamoService caRamoService;
	@Autowired
	private CaConfiguracionBancaService caConfiguracionBancaService;
	@Autowired
	@Qualifier(value="caBancaPrimPagCPServiceEJB")
	private CaBancaPrimPagCPService caBancaPrimPagCPService;
	@Autowired
	@Qualifier(value="caBancaPrimPagServiceEJB")
	private CaBancaPrimPagService caBancaPrimPagService;
	@Autowired
	@Qualifier(value="caBancaSiniMesServiceEJB")
	private CaBancaSiniMesService caBancaSiniMesService;
	
	@Autowired private CaBitacoraService bitacoraService;
	
	private List<CaRamo> listLineasVenta;
	private List<RamoSeycos> listRamos;
	private List<GerenciaSeycos> listGerencias;
	private List<EstadoDTO> listEstados;
	private List<CaConfiguracionBanca> listCaConfiguracionBanca;
	private List<CalculosBancaView> listResultsCalculosBanca;
	private List<CaBitacora> listcaBitacora;
	private CaBancaPrimPagCP caBancaConfiguracionActual;
	
	private Long idCp;
	private BigDecimal costonetosinaut;
	private BigDecimal costonetosindan;
	private BigDecimal primasretendevenaut;
	private BigDecimal primasretendevendan;
	private File primasPagadas;
	private File siniMes;
	private Double anio;
	private Double mes;
	private int valor;
	private int anioInt;
	private int mesInt;
	
	private ConfiguracionContraprestacionBanca configuracionContraprestacionBanca;
	private InputStream dataStream;
	private Date fechaCalculoCompensacion;
	private Date fechaId;
	
	@Action(value = "mostrarConfiguracionInicio", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/contenedorConfiguracionBanca.jsp")})
	public String mostrarConfiguracionInicio() {		
		LOG.info(">> mostrarConfiguracionInicio()");
		try{			
			this.caConfiguracionBancaService.cargarConfiguradorContraprestacion(configuracionContraprestacionBanca);
			this.listLineasVenta = this.filterLineasNegocio(this.caRamoService.findAll());
			this.listRamos = this.caConfiguracionBancaService.findRamosSeycos();
			this.listGerencias = this.caConfiguracionBancaService.findGerenciasSeycos();
			this.listEstados = this.caConfiguracionBancaService.findEstadosDTO();			
		}catch(Exception e){
			LOG.error("Información del Error", e);			
		}
		LOG.info("<< mostrarConfiguracionInicio()");
		return SUCCESS;
	}
	
	
	@Action(value = "mostrarCalculosCompensacionesBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/calculosCompensacionesBanca.jsp")})
	public String mostrarCalculosCompensacionesBanca() {		
		LOG.info(">> mostrarCalculosCompensacionesBanca()");
		try{			
			this.listLineasVenta = this.filterLineasNegocio(this.caRamoService.findAll());
			this.listRamos = this.caConfiguracionBancaService.findRamosSeycos();
			this.listGerencias = this.caConfiguracionBancaService.findGerenciasSeycos();		
		}catch(Exception e){
			LOG.error("Información del Error", e);			
		}
		LOG.info("<< mostrarCalculosCompensacionesBanca()");
		return SUCCESS;
	}
	
	@Action(value = "cargarGridEstadosActivos", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/gridBancaEstadosActivos.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/banca/gridBancaEstadosActivos.jsp") })
	public String cargarGridEstadosActivos(){
		LOG.info(">> cargarGridEstadosActivos()");
		try{
			this.caConfiguracionBancaService.cargarEstadosActivos(configuracionContraprestacionBanca);
		}catch(Exception e){
			LOG.error("Información del Error", e);
		}
		LOG.info("<< cargarGridEstadosActivos()");
		return SUCCESS;
	}
	
	@Action(value="mostrarCargarExcelCompensacionesBanca", results = {
			@Result(name = SUCCESS, location  = "/jsp/compensacionesAdicionales/banca/cargarExcelCompensacionesAdicionales.jsp")
			})
		public String mostrarCargarExcelCompensacionesBanca(){
			try{
				LOG.info("Entrando >>>>");
				Calendar now = Calendar.getInstance();
				anioInt = now.get(Calendar.YEAR);
				mesInt = now.get(Calendar.MONTH);											 
				caBancaConfiguracionActual = caBancaPrimPagCPService.findConfiguracionActual(anioInt, mesInt);
				if(caBancaConfiguracionActual != null){
					caBancaConfiguracionActual.setValor("1");
					DateFormat formatter  = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					String fecha = formatter .format(caBancaConfiguracionActual.getModificacion());
					Date modificacion = formatter.parse(fecha);
					caBancaConfiguracionActual.setModificacion(modificacion);
				}
				else{
					caBancaConfiguracionActual = new CaBancaPrimPagCP();
					caBancaConfiguracionActual.setValor("2");
					LOG.info(caBancaConfiguracionActual.getValor());
				}
				LOG.info("Imprimiendo el usuario actual : "+ super.getUsuarioActual().getNombre());
			LOG.info("Saliendo <<<<");
			}catch(Exception e){
				LOG.error("Informacion del Error" ,e);
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="verHistorialConfiguracion" , results ={
		@Result(name = SUCCESS, location ="/jsp/compensacionesAdicionales/historicoConfiguracion.jsp"),
		@Result(name = INPUT, location ="/jsp/compensacionesAdicionales/historicoConfiguracion.jsp" )
	})
	public String verHistorialConfiguracion(){
		LOG.info(">> verHistorialConfiguracion()");
		LOG.info("<< verHistorialConfiguracion()");
		return SUCCESS;
	}
	
	@Action(value="listarBitacoraConfiguracion" ,results = {
			@Result(name = SUCCESS, location ="/jsp/compensacionesAdicionales/historicoGridBitacoraConfiguracion.jsp")
	})
	public String listarBitacoraConfiguracion(){
		LOG.info("listarBitacoraConfiguracion() >>");
		try{
			listcaBitacora = bitacoraService.obtenerBitacoraPorConfiguracionCalc(idCp);
			LOG.info("Imprimiendo el ID CONFIGURACION" + idCp);
		}catch(RuntimeException re){
			LOG.error("Informacion del Error : ",re);
		}
		LOG.info("listarBitacoraConfiguracion() <<");
		return SUCCESS;
	}
	
	@Action(value="guardarCPExcel" , results ={
			@Result(name = SUCCESS ,location = "/jsp/compensacionesAdicionales/mensajesCompensaciones.jsp"),
			@Result(name = INPUT ,  location = "/jsp/compensacionesAdicionales/mensajesCompensaciones.jsp")
	})
	public String guardarCPExcel(){
		LOG.info(">> guardarCPExcel()");
		try{
			DateFormat formatter  = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			String fecha = formatter .format(date);
			Date modificacion = formatter.parse(fecha);
			
			CaBancaPrimPagCP caBancaPrimPagCP = new CaBancaPrimPagCP();
			caBancaPrimPagCP.setCostonetosinaut(costonetosinaut);
			caBancaPrimPagCP.setCostonetosindan(costonetosindan);
			caBancaPrimPagCP.setPrimasretendevenaut(primasretendevenaut);
			caBancaPrimPagCP.setPrimasretendevendan(primasretendevendan);
			caBancaPrimPagCP.setAnio(anio);
			caBancaPrimPagCP.setMes(mes);
			caBancaPrimPagCP.setModificacion(modificacion);
			caBancaPrimPagCP.setUsuario(super.getUsuarioActual().getNombre());
			
			
			this.caBancaPrimPagService.procesarArchivos(caBancaPrimPagCP,this.primasPagadas, this.siniMes, mes);
			LOG.info(">> cargarExcelPrimasPagadas()");
			
			if(this.primasPagadas != null && this.siniMes !=null){
				Long idRetorno = caBancaPrimPagCP.getId();
				if(idRetorno != null){
					super.setMensaje("Se ha cargardo el Archivo");
				}
				else{
					super.setMensaje("No se ha podido cargar el Archivo");
				}
			}
			else{
				super.setMensaje("Ocurrio un error al cargar el Archivo");
			}
			
		}catch(Exception e){
			LOG.error("Informacion del Error", e);
			super.setMensaje("Ocurrio un error al guardar los datos[1]");
			return INPUT;
		}
		finally{
			if(this.primasPagadas !=null && this.siniMes !=null){
				this.primasPagadas.deleteOnExit();
			}
			LOG.info("File :" + this.primasPagadas);
		}
		super.setMensaje("Se han guardado los datos[1]");
		LOG.info("<< guardarCPExcel()");
		return SUCCESS;
	}
	
	@Action(value="guardarConfiguracion", results={
					@Result(name = SUCCESS, type = "stream", params = {"contentType", "text/plain","inputName", "dataStream"}),
					@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/mensajesCompensaciones.jsp")})
	public String guardarConfiguracion(){
		LOG.info(">> guardarConfiguracion()");
		try{
			this.configuracionContraprestacionBanca.getCaConfiguracionBanca().setUsuario(super.getUsuarioActual().getNombre());
			this.caConfiguracionBancaService.guardarConfiguracion(configuracionContraprestacionBanca);					
			Map<String, String> mapResult = new HashMap<String, String>();
			mapResult.put("id", configuracionContraprestacionBanca.getCaConfiguracionBanca().getId().toString());
			mapResult.put("usuario", configuracionContraprestacionBanca.getCaConfiguracionBanca().getUsuario());			
			mapResult.put("fecha", new SimpleDateFormat("dd/MM/yyyy").format(configuracionContraprestacionBanca.getCaConfiguracionBanca().getFechaModificacion()));
			this.setDataStream(new ByteArrayInputStream(new Gson().toJson(mapResult).getBytes()));
		}catch(Exception e){
			LOG.error("Información del Error", e);
			super.setMensaje("Ocurrio un error al guardar la configuracion");
			return INPUT;
		}
		super.setMensaje("Se ha guardado la configuracion");
		LOG.info("<< guardarConfiguracion()");
		return SUCCESS;
	}
	
	@Action(value = "buscarConfiguracionesContraprestacionBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/gridConfiguracionesBanca.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/banca/gridConfiguracionesBanca.jsp") })
	public String buscarConfiguracionesContraprestacionBanca(){
		LOG.info(">> buscarConfiguracionesContraprestacionBanca()");
		try{
			this.listCaConfiguracionBanca = this.caConfiguracionBancaService.filterCalculosCaConfiguracionesBanca(configuracionContraprestacionBanca);
		}catch(Exception e){
			LOG.error("Información del Error", e);
		}
		LOG.info("<< buscarConfiguracionesContraprestacionBanca()");
		return SUCCESS;
	}
	
	
	@Action(value = "listConfiguracionesBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/listConfiguracionesBanca.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/banca/listConfiguracionesBanca.jsp") })
	public String listConfiguracionesBanca(){
		LOG.info(">> listConfiguracionesBanca()");
		LOG.info("<< listConfiguracionesBanca()");
		return SUCCESS;
	}
	
	
	@Action(value = "gridConfiguracionesBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/gridConfiguracionesBanca.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/banca/gridConfiguracionesBanca.jsp") })
	public String gridConfiguracionesBanca(){
		LOG.info(">> gridConfiguracionesBanca()");
		try{
			this.listCaConfiguracionBanca = this.caConfiguracionBancaService.findAll();
		}catch(Exception e){
			LOG.error("Información del Error", e);
		}
		LOG.info("<< gridConfiguracionesBanca()");
		return SUCCESS;
	}
	
	
	private List<CaRamo> filterLineasNegocio(List<CaRamo> listRamos){
		if(listRamos != null){
			Iterator<CaRamo> iteratorRamo = listRamos.iterator();		
			while(iteratorRamo.hasNext()){
				CaRamo caRamo = iteratorRamo.next();
				if(RAMO.BANCA.getValor().equals(caRamo.getValor().longValue())){
					iteratorRamo.remove();
				}			
			}	
		}				
		return listRamos;
	}

	@Action(value = "ejecutarCalculosBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/gridResultsCalculosBanca.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/banca/gridResultsCalculosBanca.jsp") })
	public String ejecutarCalculosBanca(){
		LOG.info(">> ejecutarCalculosBanca()");
		try{	
			/*String string = fechaId.toString();
			String[] parts = string.split(" ");
			String p1 = parts[1];//month
			String p2 = parts[2];//day
			String p3 = parts[5];//year
			String auxMes = null;
			if(p1.equals("Jun")){
				auxMes = "06";
			}
			String fullDate = p2+"/"+auxMes+"/"+p3;
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaIdDate = format.parse(fullDate);*/
			this.listResultsCalculosBanca = this.caConfiguracionBancaService.ejecutarCalculosBanca(configuracionContraprestacionBanca, listCaConfiguracionBanca, fechaId);
			LOG.info("Fecha Id : ", fechaId.toString());
			//this.listResultsCalculosBanca = this.caConfiguracionBancaService.ejecutarCalculosBanca(configuracionContraprestacionBanca, listCaConfiguracionBanca, fechaCalculoCompensacion);
		}catch(Exception e){
			LOG.error("Información del Error", e);
			return INPUT;
		}
		LOG.info("<< ejecutarCalculosBanca()");
		return SUCCESS;
	}
	
	@Action(value = "provisionarCalculosBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/mensajesCompensaciones.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/mensajesCompensaciones.jsp") })
	public String provisionarCalculosBanca(){
		LOG.info(">> provisionarCalculosBanca()");
		try{
			this.caConfiguracionBancaService.procesarListaConfiguracionBanca(listResultsCalculosBanca,fechaCalculoCompensacion);
			//boolean result = this.caConfiguracionBancaService.provisionarBanca(configuracionContraprestacionBanca, listCaConfiguracionBanca, fechaCalculoCompensacion);
			if(listResultsCalculosBanca != null){
				super.setMensaje("Se ha provisionado");
			}else{
				super.setMensaje("Ocurrio un error al provisionar");
			}
		}catch(Exception e){
			LOG.error("Información del Error", e);
			super.setMensaje("Ocurrio un error al provisionar");
			return INPUT;
		}
		LOG.info("<< provisionarCalculosBanca()");
		return SUCCESS;
	}
	
	@Action(value = "exportarExcelResultadosCalculos", results = { 
			@Result(name = SUCCESS, type = "stream", params = { "contentType", "application/xls", "contentDisposition", "attachment;filename=\"resultadosCalculos.xls\"", "inputName", "dataStream" }), 
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/mensajesCompensaciones.jsp")})
	public String exportarExcelResultadosCalculos(){
		LOG.info(">> exportarExcelResultadosCalculos()");
		try{
			TransporteImpresionDTO transporte = this.caConfiguracionBancaService.exportarExcelCalculos(listResultsCalculosBanca);
			this.setDataStream(new ByteArrayInputStream(transporte.getByteArray()));
		}catch(Exception e){
			LOG.error("Información del Error", e);
			super.setMensaje("Ocurrio un error al exportar el archivo excel");
			return INPUT;
		}
		LOG.info("<< exportarExcelResultadosCalculos()");
		return SUCCESS;
	}
	
	@Action(value = "historicoConfiguracionBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/banca/historicoConfiguracionBanca.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/banca/historicoConfiguracionBanca.jsp") })
	public String historicoConfiguracionBanca(){
		LOG.info(">> historicoConfiguracionBanca()");
		LOG.info("<< historicoConfiguracionBanca()");
		return SUCCESS;
	}
	
	@Action(value = "gridHistoricoConfiguracionBanca", results = {
			@Result(name = SUCCESS, location = "/jsp/compensacionesAdicionales/historicoGridBitacora.jsp"),
			@Result(name = INPUT, location = "/jsp/compensacionesAdicionales/historicoGridBitacora.jsp") })
	public String gridHistoricoConfiguracionBanca(){
		LOG.info(">> gridHistoricoConfiguracionBanca()");
		try{
			this.listcaBitacora = this.caConfiguracionBancaService.getListHistoricoConfiguracion(configuracionContraprestacionBanca);
		}catch(Exception e){
			this.listcaBitacora = new ArrayList<CaBitacora>();
			LOG.error("Información del Error", e);
		}
		LOG.info("<< gridHistoricoConfiguracionBanca()");
		return SUCCESS;
	}
	
	public void setListLineasVenta(List<CaRamo> listLineasVenta) {
		this.listLineasVenta = listLineasVenta;
	}

	public List<CaRamo> getListLineasVenta() {
		return listLineasVenta;
	}

	public void setListRamos(List<RamoSeycos> listRamos) {
		this.listRamos = listRamos;
	}

	public List<RamoSeycos> getListRamos() {
		return listRamos;
	}

	public void setListGerencias(List<GerenciaSeycos> listGerencias) {
		this.listGerencias = listGerencias;
	}

	public List<GerenciaSeycos> getListGerencias() {
		return listGerencias;
	}

	public void setListEstados(List<EstadoDTO> listEstados) {
		this.listEstados = listEstados;
	}

	public List<EstadoDTO> getListEstados() {
		return listEstados;
	}

	public void setConfiguracionContraprestacionBanca(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca) {
		this.configuracionContraprestacionBanca = configuracionContraprestacionBanca;
	}

	public ConfiguracionContraprestacionBanca getConfiguracionContraprestacionBanca() {
		return configuracionContraprestacionBanca;
	}

	public void setDataStream(InputStream dataStream) {
		this.dataStream = dataStream;
	}

	public InputStream getDataStream() {
		return dataStream;
	}


	public void setFechaCalculoCompensacion(Date fechaCalculoCompensacion) {
		this.fechaCalculoCompensacion = fechaCalculoCompensacion;
	}


	public Date getFechaCalculoCompensacion() {
		return fechaCalculoCompensacion;
	}


	public void setListCaConfiguracionBanca(List<CaConfiguracionBanca> listCaConfiguracionBanca) {
		this.listCaConfiguracionBanca = listCaConfiguracionBanca;
	}


	public List<CaConfiguracionBanca> getListCaConfiguracionBanca() {
		return listCaConfiguracionBanca;
	}


	public void setListResultsCalculosBanca(List<CalculosBancaView> listResultsCalculosBanca) {
		this.listResultsCalculosBanca = listResultsCalculosBanca;
	}


	public List<CalculosBancaView> getListResultsCalculosBanca() {
		return listResultsCalculosBanca;
	}


	public void setListcaBitacora(List<CaBitacora> listcaBitacora){
		this.listcaBitacora = listcaBitacora;
	}


	public List<CaBitacora> getListcaBitacora() {
		return listcaBitacora;
	}


	public BigDecimal getCostonetosinaut() {
		return costonetosinaut;
	}


	public void setCostonetosinaut(BigDecimal costonetosinaut) {
		this.costonetosinaut = costonetosinaut;
	}


	public BigDecimal getCostonetosindan() {
		return costonetosindan;
	}


	public void setCostonetosindan(BigDecimal costonetosindan) {
		this.costonetosindan = costonetosindan;
	}


	public BigDecimal getPrimasretendevenaut() {
		return primasretendevenaut;
	}


	public void setPrimasretendevenaut(BigDecimal primasretendevenaut) {
		this.primasretendevenaut = primasretendevenaut;
	}


	public BigDecimal getPrimasretendevendan() {
		return primasretendevendan;
	}


	public void setPrimasretendevendan(BigDecimal primasretendevendan) {
		this.primasretendevendan = primasretendevendan;
	}


	public File getPrimasPagadas() {
		return primasPagadas;
	}


	public void setPrimasPagadas(File primasPagadas) {
		this.primasPagadas = primasPagadas;
	}


	public File getSiniMes() {
		return siniMes;
	}


	public void setSiniMes(File siniMes) {
		this.siniMes = siniMes;
	}


	public Double getMes() {
		return mes;
	}


	public void setMes(Double mes) {
		this.mes = mes;
	}
	
	public Double getAnio() {
		return anio;
	}


	public void setAnio(Double anio) {
		this.anio = anio;
	}


	public CaBancaPrimPagCP getCaBancaConfiguracionActual() {
		return caBancaConfiguracionActual;
	}


	public void setCaBancaConfiguracionActual(
			CaBancaPrimPagCP caBancaConfiguracionActual) {
		this.caBancaConfiguracionActual = caBancaConfiguracionActual;
	}


	public int getValor() {
		return valor;
	}


	public void setValor(int valor) {
		this.valor = valor;
	}


	public int getAnioInt() {
		return anioInt;
	}


	public void setAnioInt(int anioInt) {
		this.anioInt = anioInt;
	}


	public int getMesInt() {
		return mesInt;
	}


	public void setMesInt(int mesInt) {
		this.mesInt = mesInt;
	}


	public CaBitacoraService getBitacoraService() {
		return bitacoraService;
	}


	public void setBitacoraService(CaBitacoraService bitacoraService) {
		this.bitacoraService = bitacoraService;
	}


	public Long getIdCp() {
		return idCp;
	}


	public void setIdCp(Long idCp) {
		this.idCp = idCp;
	}


	public Date getFechaId() {
		return fechaId;
	}


	public void setFechaId(Date fechaId) {
		this.fechaId = fechaId;
	}
}
