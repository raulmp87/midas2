package mx.com.afirme.midas2.action.compensacionesAdicionales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionCompensacionLista;
import mx.com.afirme.midas2.domain.compensaciones.CaEstatus;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.dto.compensaciones.FiltroCompensacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.compensaciones.CaCompensacionService;
import mx.com.afirme.midas2.service.compensaciones.CaConfiguracionBancaService;
import mx.com.afirme.midas2.service.compensaciones.CaEstatusService;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.CompensacionesAdicionalesService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/compensacionesAdicionales")
public class FiltrosCompensacionesAction extends CatalogoHistoricoAction{	
	
	private static final long serialVersionUID = -7630725849360784735L;
	@Autowired
	private CaConfiguracionBancaService caConfiguracionBancaService;
	private CatalogoCompensacionesService catalogoCompensacionesService;
	private CompensacionesAdicionalesService compensacionesAdicionalesService;
	private CaEstatusService estatusService;
	private FiltroCompensacion filtroCompensacion;
	private List<CaCompensacion> listaCompensaciones;
	private List<CaConfiguracionCompensacionLista> listaEntidadesPersona;
	private List<CaParametros> listaParametros;
	private List<CaEstatus> listaEstatus;
	private List<CaRamo> listaRamos;
	private List<GerenciaSeycos> listGerencias;
	private EntidadService entidadService;
	
	@Autowired CaCompensacionService caCompensacionService;
	private Long idCompensacionEliminar;
	
	@Action(value="eliminarCompensacion",
			results={@Result( name=SUCCESS, location="/jsp/compensacionesAdicionales/compensaciones.jsp")})
	public String eliminarCompensacion(){
		CaCompensacion ca = caCompensacionService.findById(idCompensacionEliminar);
		ca.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICOSI);
		ca.setFechaModificacion(new Date());
		ca.setUsuario(System.getProperty("user.name"));
		caCompensacionService.update(ca);		
		listaEstatus = estatusService.findAll();		
		listaRamos = catalogoCompensacionesService.listarRamos();
		
		return SUCCESS;
	}
	
	@Action(value="compensacionesInicio",
			results={@Result( name=SUCCESS, location="/jsp/compensacionesAdicionales/compensaciones.jsp")})
	public String mostrarInicio(){
		try{
			listaEstatus = estatusService.findAll();
			this.filtroEstatus();
			this.setListGerencias(this.caConfiguracionBancaService.findGerenciasSeycos());
			this.listaRamos = this.catalogoCompensacionesService.listarRamos();
		}catch(Exception ex){
			
		}
		return SUCCESS;
	}
	
	@Action(value="listaPaginadoCompensaciones",
			results={@Result(name=SUCCESS, location="/jsp/suscripcion/solicitud/paginadoGrid.jsp")})
	public String paginarCompensaciones(){
		if (filtroCompensacion == null){
			filtroCompensacion = new FiltroCompensacion();
		}
		if(getTotalCount() == null){
			setTotalCount(compensacionesAdicionalesService.totalCompensaciones(filtroCompensacion));
			setListadoEnSession(null);
		}
		setPosPaginado();
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value="listaCompensaciones",
			results={@Result(name=SUCCESS, location="/jsp/compensacionesAdicionales/listaCompensaciones.jsp")})
	public String listarCompensaciones(){
		if (!listadoDeCache()){
			try{
				if (filtroCompensacion == null){
					filtroCompensacion = new FiltroCompensacion();
				}
				try{
					filtroCompensacion.setPageNumber(getPosStart());
				}catch(Exception ex2){
					filtroCompensacion.setPageNumber(0);
				}
				filtroCompensacion.setPageSize(REGISTROS_A_MOSTRAR);
				listaCompensaciones = compensacionesAdicionalesService.buscarCompensaciones(filtroCompensacion);
			}catch(Exception ex){
				listaCompensaciones = new ArrayList<CaCompensacion>(1);
			}
		}else{
			listaCompensaciones = (List<CaCompensacion>) getListadoPaginado();
			setListadoEnSession(listaCompensaciones);
		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value="listarEntidadesPersona",
			results={@Result(name=SUCCESS, location="/jsp/compensacionesAdicionales/entidadesPersonaGrid.jsp")})
	public String listarEntidadesPersona(){
		if (!listadoDeCache()){
			try{
				if (filtroCompensacion == null){
					filtroCompensacion = new FiltroCompensacion();
				}
				try{
				filtroCompensacion.setPageNumber(getPosStart());
				}catch(Exception ex2){
					filtroCompensacion.setPageNumber(0);
				}
				filtroCompensacion.setPageSize(REGISTROS_A_MOSTRAR);
				listaEntidadesPersona = compensacionesAdicionalesService.buscarEntidadesPersona(filtroCompensacion);
			}catch(Exception ex){
				listaEntidadesPersona = new ArrayList<CaConfiguracionCompensacionLista>(1);
				//e.printStackTrace();
			}
		}else{
			listaEntidadesPersona = (List<CaConfiguracionCompensacionLista>) getListadoPaginado();
			setListadoEnSession(listaEntidadesPersona);
		}
		
		
		for (CaConfiguracionCompensacionLista entidadPersona : listaEntidadesPersona) {
			CotizacionDTO cotizacion= new CotizacionDTO();
			entidadPersona.setSinCompensacion("0");
				BigDecimal idCotizacion=entidadPersona.getCaCompensacion().getCotizacionId();
				 if(idCotizacion != null  ){
					 cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
					 entidadPersona.setSinCompensacion(cotizacion.getSinCompensacion()== null || !cotizacion.getSinCompensacion()?"0":"1");
				 }
		}
		
	

		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(value="listarParametros",
			results={@Result(name=SUCCESS, location="/jsp/compensacionesAdicionales/parametrosGrid.jsp")})
	public String listarParametros(){
		if (!listadoDeCache()){
			try{
				try{
					filtroCompensacion.setPageNumber(getPosStart());
				}catch(Exception ex2){
					filtroCompensacion.setPageNumber(0);
				}
				filtroCompensacion.setPageSize(REGISTROS_A_MOSTRAR);
				listaParametros = compensacionesAdicionalesService.buscarParametrosGenerales(filtroCompensacion);
			}catch(Exception ex){
				listaParametros = new ArrayList<CaParametros>(1);
			}
		}else{
			listaParametros = (List<CaParametros>) getListadoPaginado();
			setListadoEnSession(listaCompensaciones);
		}
		return SUCCESS;
	}
	
	@Action(value="filtrosVida",
			results={@Result( name=SUCCESS, location="/jsp/compensacionesAdicionales/compensaciones.jsp")})
	public String mostrarFiltrosVida(){
		
		return SUCCESS;
	}
	
	
	@Autowired
	@Qualifier("catalogoCompensacionesServiceEJB")
	public void setCatalogoCompensacionesService(
			CatalogoCompensacionesService catalogoCompensacionesService) {
		this.catalogoCompensacionesService = catalogoCompensacionesService;
	}
	
	@Autowired
	@Qualifier("compensacionesAdicionalesServiceEJB")
	public void setCompensacionesAdicionalesService(
			CompensacionesAdicionalesService compensacionesAdicionalesService) {
		this.compensacionesAdicionalesService = compensacionesAdicionalesService;
	}
	
	@Autowired
	@Qualifier("EstatuscaServiceEJB")
	public void setEstatusService(CaEstatusService estatusService) {
		this.estatusService = estatusService;
	}

	public FiltroCompensacion getFiltroCompensacion() {
		return filtroCompensacion;
	}

	public void setFiltroCompensacion(FiltroCompensacion filtroCompensacion) {
		this.filtroCompensacion = filtroCompensacion;
	}

	public List<CaCompensacion> getListaCompensaciones() {
		return listaCompensaciones;
	}

	public void setListaCompensaciones(List<CaCompensacion> listaCompensaciones) {
		this.listaCompensaciones = listaCompensaciones;
	}

	
	public List<CaConfiguracionCompensacionLista> getListaEntidadesPersona() {
		return listaEntidadesPersona;
	}

	public void setListaEntidadesPersona(
			List<CaConfiguracionCompensacionLista> listaEntidadesPersona) {
		this.listaEntidadesPersona = listaEntidadesPersona;
	}

	public List<CaParametros> getListaParametros() {
		return listaParametros;
	}

	public void setListaParametros(List<CaParametros> listaParametros) {
		this.listaParametros = listaParametros;
	}

	public List<CaEstatus> getListaEstatus() {
		return listaEstatus;
	}

	public void setListaEstatus(List<CaEstatus> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	public List<CaRamo> getListaRamos() {
		return listaRamos;
	}

	public void setListaRamos(List<CaRamo> listaRamos) {
		this.listaRamos = listaRamos;
	}
	
	public Long getIdCompensacionEliminar() {
		return idCompensacionEliminar;
	}
	public void setIdCompensacionEliminar(Long idCompensacionEliminar) {
		this.idCompensacionEliminar = idCompensacionEliminar;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String guardar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
	
	private void filtroEstatus(){
		Iterator<CaEstatus> iteratorEstatus = listaEstatus.iterator();		
		while (iteratorEstatus.hasNext()) {
			CaEstatus caEstatus = (CaEstatus) iteratorEstatus.next();
			if(!ConstantesCompensacionesAdicionales.ESTATUS_APROBADO.equals(caEstatus.getId())
					&& !ConstantesCompensacionesAdicionales.ESTATUS_PENDIENTE.equals(caEstatus.getId())){
				iteratorEstatus.remove();
			}
		}
	}

	public void setListGerencias(List<GerenciaSeycos> listGerencias) {
		this.listGerencias = listGerencias;
	}

	public List<GerenciaSeycos> getListGerencias() {
		return listGerencias;
	}
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
