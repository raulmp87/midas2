package mx.com.afirme.midas2.action.compensacionesAdicionales.ordenPagos;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.DatosDetallePagosPorRecibo;
import mx.com.afirme.midas2.domain.compensaciones.LiquidacionCompensaciones;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagos;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoDTO;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoFiltroDTO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.LiquidacionCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosDetalleService;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosService;
import mx.com.afirme.midas2.service.fuerzaventa.EnvioFacturaAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/compensacionesAdicionales/ordenPago")
@Component
@Scope("prototype")
public class OrdenPagosAction extends CatalogoHistoricoAction implements
		Preparable {
	private static final long serialVersionUID = 4315317481915127212L;
    private static final String NAMESPACE = "/compensacionesAdicionales/ordenPago";
    private static final String LOCATION_MOSTRAR_ORDENES_PAGO_JSP = "/jsp/compensacionesAdicionales/ordenPagos/ordenesPago/mostrar.jsp";
	private static final String LOCATION_MOSTRAR_JSP = "/jsp/compensacionesAdicionales/ordenPagos/contenedor.jsp";
	private static final String LOCATION_GRID_JSP = "/jsp/compensacionesAdicionales/ordenPagos/ordenPagosGrid.jsp";
	private static final String LOCATION_MOSTRARORDENPAGOS_JSP = "/jsp/compensacionesAdicionales/ordenPagos/mostrar.jsp";
	private static final String LOCATION_DETALLE_JSP = "/jsp/compensacionesAdicionales/ordenPagos/mostrarDetalle.jsp";
	private static final String LOCATION_GRID_DETALLE_JSP = "/jsp/compensacionesAdicionales/ordenPagos/ordenPagosGridDetalle.jsp";
	private static final String LOCATION_GRID_OP_JSP = "/jsp/compensacionesAdicionales/ordenPagos/agregarOrdenPagos/ordenPagGrid.jsp";
	private static final String LOCATION_MOSTRAR_PEND_JSP = "/jsp/compensacionesAdicionales/ordenPagos/ordenpagosAutorizar/mostrarOrdenPagoPendiente.jsp";
	private static final String LOCATION_GRID_PEND_OP_JSP = "/jsp/compensacionesAdicionales/ordenPagos/envioFacturas/ordenPagGrid.jsp";
	private static final String LOCATION_MOSTRAR_OP_JSP = "/jsp/compensacionesAdicionales/ordenPagos/agregarOrdenPagos/mostrar.jsp";
	private static final String LOCATION_DETALLE_OP_JSP = "/jsp/compensacionesAdicionales/ordenPagos/agregarOrdenPagos/detalle.jsp";
	private static final String MOSTRAR_FACTURA_JSP = "/jsp/compensacionesAdicionales/ordenPagos/envioFacturas/envioFacturaCompensaciones.jsp";
	private final String MOSTRAR_FACTURA_PROV_JSP = "/jsp/compensacionesAdicionales/ordenPagos/envioFacturas/facturaCompensacionesProveedores.jsp";
	private static final String LOCATION_GRIDTODOSOP_JSP = "/jsp/compensacionesAdicionales/ordenPagos/ordenesPago/ordenesPagosGrid.jsp";
	//private final String LOCATION_GRID_OP_JSP = "/jsp/catalogos/fuerzaventa/envioFacturaAgentes/envioFacturaCompensaciones/ordenPagoGrid.jsp";
	private static final String USUARIO = "MIDAS";
	private static final String ESTATUSTRAMITE = "TRAMITE";
	private static final String ESTATUSPENDIENTEPAGAR = "PENDIENTEXPAGAR";
	private boolean ramoAuto;
	private boolean ramoVida;
	private boolean ramoDanios;
	private boolean ramoTodos = true;
	private Long agente;
	private Long promotor;
	private Long proveedor;
	private String contratante;
	private Long gerencia;
	private Long negocio;
	private Date fechaInicio;
	private Date fechaFinal;
	private boolean agentesTodos = true;
	private boolean promotoresTodos = true;
	private boolean proveedoresTodos =true;
	private boolean negociosTodos=true;
	private boolean gerenciaTodos=true;
	private boolean contratanteTodos=true;
	private final Long TIPOENTIDAD_TODOS=0L;
	private String ramo;
	private Long estatusFactura;
	private Long estatusOrdenPago;
	
	private boolean cons_porPrima;
	private boolean cons_porBS;
	private boolean cons_porCumpMeta;
	private boolean cons_porDerPoliza;
	private boolean cons_porUtilidad;
	private boolean cons_Todos =true;
	
	private Long CONS_POR_PRIMA = 1L;
	private Long CONS_POR_BS = 2L;
	private Long CONS_POR_DERPOLIZA = 3L;
	private Long CONS_POR_UTILIDAD = 6L;
	//private static  Long CONS_POR_CUMP_META= 6L;	
	private String AUTO_IDENTIFICADOR = "A";
    private String VIDA_IDENTIFICADOR = "V";
    private String DANIOS_IDENTIFICADOR = "D";
    private static String idParametro;
  	private ConfigBonosService configBonosService;
	private Boolean chkTodosAgentes;
	private Boolean chkDetalle;
	private Boolean chkSegAfirmeCom;
	private Boolean chkGuiaLectura;
	private Boolean chkMostrarColAdicionales;
	private String textoSegAfirmeCom;
	private Long idAgenteSeleccionado;
	private String formatoDeSalida = "Formato de Salida";
	private String mes;
	private String anio;
	private String contentType;
	private String fileName;
	private String folio;
	private Long idLiquidacion;
	private Long idRecibo;
	private Long identificadorBenId;
	private boolean enProceso = false;
	private  OrdenesPagoDTO ordenesPagoDTO;
	private OrdenPagosDetalle ordenPagosDetalle;
	private List<OrdenPagosDetalle> listaOrdenPagos_Detalle = new ArrayList<OrdenPagosDetalle>();
	private OrdenPagos ordenPagos;
	private List<OrdenPagos> lista = new ArrayList<OrdenPagos>();
	private List<LiquidacionCompensaciones> listaLiquidacion = new ArrayList<LiquidacionCompensaciones>();
	private List<OrdenesPagoDTO> listaOrdenPagos = new ArrayList<OrdenesPagoDTO>();
	private List<OrdenesPagoDTO> listaTodosOrdenPagos = new ArrayList<OrdenesPagoDTO>();
	private List<DatosDetallePagosPorRecibo>  listaDetallePagos;
	private List<OrdenesPagoDTO> listaOrdenesPago = new ArrayList<OrdenesPagoDTO>();
	private List<CaRamo> ramoLista = new ArrayList<CaRamo>(); 
	private OrdenesPagoFiltroDTO filtros;
	private  String tabActiva;
	private Boolean esExcluir;
	private String idsAgregarOp;
	/** Log de OrdenPagosAction */
	private static final Logger LOGGER = Logger.getLogger(OrdenPagosAction.class);
	private Agente agente1 = new Agente();
	private Long idAgente;
	private String numeroCedula;
	private String tipoAgente;
	private String nombreAgente;
	private String ejecutivo;
	private File facturaXml;
	private HttpServletResponse response;
	private InputStream inputStream;
	private UsuarioService usuarioService;
	private Boolean isProveedor;
	private Boolean isAgente;
	private Long idProveedor;
	private PrestadorServicio prestadorServicio;
	private List<Map<String, Object>> listaPrestadoresServicio;
	private PrestadorDeServicioService prestadorDeServicioService;
	private Long idPrestadorServicio;
	private String rfc;
	private String vencimientoCedula;
	private String centroOperacion;
	private String gerenciaAgente;
	private String estatus;
	@Autowired
	private CatalogoCompensacionesService catalogoCompensacionesService;
	
	private String tipoSalidaArchivo;
	@Autowired
	private  OrdenPagosService ordenPagosService;	
	@Autowired
	private OrdenPagosDetalleService ordenPagosDetalleService;
	
	@Autowired
	private LiquidacionCompensacionesService liquidacionCompensacionesService;
	private ImpresionesService impresionesService;
	
	private EnvioFacturaAgenteService envioFacturaService;
	private PromotoriaJPAService promotoriaService;

	@Autowired
	GenerarPlantillaReporte generarPlantillaReporte;
	
	private  OrdenesPagoDTO ordenesTodosPagoDTO;
	@Override
	public void prepare() throws Exception {
	}
	///pantalla 1
	@Action(value = "mostrarOrdenesPago", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRAR_ORDENES_PAGO_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRAR_ORDENES_PAGO_JSP) })
	public String mostrarOrdenesPago() {
		filtros = this.liquidacionCompensacionesService.obtenerFiltrosOrdenPagos(1l);
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedorOrdenPagos", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRAR_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRAR_JSP) })
	public String mostrarContenedorOrdenPagos() {
		return SUCCESS;
	}

	@Action(value = "mostrarOrdenPagos", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRARORDENPAGOS_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRARORDENPAGOS_JSP) })
	public String mostrarOrdenPagos() {
		return SUCCESS;
	}

	@Action(value = "mostrarPagosOrden", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRAR_OP_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRAR_OP_JSP) })
	public String mostrarPagosOrden() {
		this.setTabActiva("orden_pagos");
		return SUCCESS;
	}
	
	@Action(value = "mostrarPagosOrdenPediente", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRAR_PEND_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRAR_PEND_JSP) })
	public String mostrarPagosOrdenPediente() {
		return SUCCESS;
	}

	@Action(value = "listar", results = {
			@Result(name = SUCCESS, location = LOCATION_GRID_JSP),
			@Result(name = INPUT, location = LOCATION_GRID_JSP) })
	@Override
	public String listar() {
		lista = ordenPagosService.findAll();
		return SUCCESS;
	}
	@Action(value = "listarOP", results = {
			@Result(name = SUCCESS, location = LOCATION_GRID_OP_JSP),
			@Result(name = INPUT, location = LOCATION_GRID_OP_JSP) })
	public String listarOP() {
		listaLiquidacion = liquidacionCompensacionesService.obtenerOrdenPagoSoliFactura(ESTATUSTRAMITE); 
		return SUCCESS;
	}
		
//	@Action(value = "listaTodosOrdenesPagos", results = {
//			@Result(name = SUCCESS, location = LOCATION_GRID_OP_JSP),
//			@Result(name = INPUT, location = LOCATION_GRID_OP_JSP) })
//	public String listaTodosOrdenesPagos() {
//		listaOrdenesPago = liquidacionCompensacionesService.buscarLiquidacionXEstatus(datosOrdenPagosTodos());
//		LOG.info("listaTodosOrdenesPagos  Lista>>>"+listaLiquidacion.size());
//		return SUCCESS;
//	}	
	
	///lista pendientes por validar XML
	@Action(value = "listarPendientesOrdenPago", results = {
			@Result(name = SUCCESS, location = LOCATION_GRID_PEND_OP_JSP),
			@Result(name = INPUT, location = LOCATION_GRID_PEND_OP_JSP) })
	public String listarPendientesOrdenPago() {
		Usuario usuario = usuarioService.getUsuarioActual();
		isProveedor = usuarioService.tienePermisoUsuarioActual("FN_M2_CA_ProveedorCompensaciones");
		isAgente= usuarioService.tieneRolUsuarioActual(sistemaContext.getRolAgenteAutos());
		Long idBeneficiario=(isProveedor==false && isAgente==false)?this.idAgente:null;
		if (isProveedor){
			try{
			    listaPrestadoresServicio = prestadorDeServicioService.obtenerPrestadoresPorUsuario(usuario.getNombreUsuario());
				if(listaPrestadoresServicio.size() == 1){ 
					idPrestadorServicio = Long.valueOf(listaPrestadoresServicio.get(0).get("id").toString());
				}
				if(idPrestadorServicio != null){
					setPrestadorServicio(prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue()));
					idBeneficiario=Long.valueOf(this.prestadorServicio.getId());
				}
			}catch(Exception e){
				LOG.error("Error al cargar los datos del Proveedor", e);
			}
		}
		if (isAgente){
			try{
			setAgente1(envioFacturaService.getDatosAgente(usuario.getNombreUsuario(), usuario.getPersonaId(),null));
			idBeneficiario=Long.valueOf(this.agente1.getIdAgente());
			}catch(Exception e){
				LOG.error("Error al cargar los datos del Agente", e);
			}
		}
		if(idBeneficiario != null){
			if(estatusOrdenPago!= null && estatusOrdenPago!=0){
				listaLiquidacion = liquidacionCompensacionesService.obtenerLiquidacionesPorFiltro("TRAMITE-FACTURA",idBeneficiario);
			}else{
				listaLiquidacion = liquidacionCompensacionesService.obtenerLiquidacionesPorFiltro(idBeneficiario);
			}
		}else{
			listaLiquidacion.clear();
		}
		return SUCCESS;
	}
	
	
	//Listar Filtrado OrdenPagos
	public OrdenesPagoDTO getDatosOrdenPagos() {
		ordenesPagoDTO = new OrdenesPagoDTO();	
		StringBuilder selectCheck=new StringBuilder();
		Usuario usuario = new Usuario();
		usuario.setNombre("MIDAS");
		ordenesPagoDTO.setUsuario(usuario.getNombre());
		
		if(ramoTodos){
		selectCheck.append(ramoTodos == true? AUTO_IDENTIFICADOR+","+VIDA_IDENTIFICADOR+","+DANIOS_IDENTIFICADOR:"");
		ordenesPagoDTO.setRamo(selectCheck.toString());
		LOGGER.debug("Ramo Todos :::"+ ordenesPagoDTO.getRamo()+"");

		}		
		else{				    
			    selectCheck.append(ramoAuto == true? AUTO_IDENTIFICADOR:"");
			    LOGGER.debug("Ramo Autos :" + ramoAuto);	    
			   
			    selectCheck.append(ramoAuto == true && ramoVida == true? ",":"");
				selectCheck.append(ramoVida == true?""+VIDA_IDENTIFICADOR:"");
				LOGGER.debug("Ramo Vida :" + ramoVida);
				
				selectCheck.append((ramoVida == true||ramoAuto==true)&&ramoDanios == true? ",":"");
				selectCheck.append(ramoDanios == true?""+DANIOS_IDENTIFICADOR:"");
				LOGGER.debug("Ramo Daños :" + ramoDanios);
				
			    ordenesPagoDTO.setRamo(selectCheck.toString());
				LOGGER.debug("Retornando Ramos :" + ordenesPagoDTO.getRamo());				
			    }	
			
           if (agente != null){              
                ordenesPagoDTO.setAgente(agente.toString().trim());
                LOGGER.debug(">>Agente"+ ordenesPagoDTO.getAgente()); 
                } 
           
           if(agentesTodos == true){
        	    LOGGER.debug("Dentro del if de Agentes :" +  agentesTodos); 
                ordenesPagoDTO.setAgente(TIPOENTIDAD_TODOS.toString()); 
                LOGGER.debug("Agentes despues del Set "+ ordenesPagoDTO.getAgente());  
                }    
           
            if (promotor!=null) {
            	ordenesPagoDTO.setPromotor(promotor.toString().trim());
    			LOGGER.debug(">>Agente"+ ordenesPagoDTO.getPromotor());
		    	}
            if(promotoresTodos == true){
          	    LOGGER.debug("Dentro del if de Promotores :" +  promotoresTodos); 
                ordenesPagoDTO.setPromotor(TIPOENTIDAD_TODOS.toString()); 
                LOGGER.debug("Promotor despues del Set "+ ordenesPagoDTO.getPromotor());  
                } 
            
            if(proveedor != null){
            	ordenesPagoDTO.setProveedor(proveedor.toString().trim());  
            	LOGGER.debug(">>Poveedor"+ ordenesPagoDTO.getProveedor());
            }
           if(proveedoresTodos == true){
                LOGGER.debug("Dentro del if de Proveedores :" +  proveedoresTodos); 
                ordenesPagoDTO.setProveedor(TIPOENTIDAD_TODOS.toString()); 
                LOGGER.debug("Proveedor despues del Set "+ ordenesPagoDTO.getProveedor());  
                }  
           
           if(contratante != null){
                ordenesPagoDTO.setContratante(contratante.trim()); 
                LOGGER.debug(">>contratante"+ ordenesPagoDTO.getContratante());
                }  
           if(contratanteTodos == true){
           	    LOGGER.debug("Dentro del if de Contratantes :" +  contratanteTodos); 
           	    ordenesPagoDTO.setContratante(TIPOENTIDAD_TODOS.toString()); 
                LOGGER.debug("contratante despues del Set "+ ordenesPagoDTO.getContratante());  
                }
           if(gerencia != null){
               ordenesPagoDTO.setGerencia(gerencia.toString()); 
               LOGGER.debug(">>gerencia"+ ordenesPagoDTO.getGerencia());
               }  
          if(gerenciaTodos == true){
          	    LOGGER.debug("Dentro del if de GerenciaTodos :" +  gerenciaTodos); 
          	    ordenesPagoDTO.setGerencia(TIPOENTIDAD_TODOS.toString()); 
               LOGGER.debug("gerencia despues del Set "+ ordenesPagoDTO.getGerencia());  
               }           
		   if (negocio != null) {
			    ordenesPagoDTO.setNegocio(negocio.toString());
		        LOGGER.debug(">>Negocio" + ordenesPagoDTO.getNegocio());
		        }
		   if(negociosTodos == true){
	            LOGGER.debug("Dentro del if de Negocios :" +  negociosTodos); 
	            ordenesPagoDTO.setNegocio(TIPOENTIDAD_TODOS.toString()); 
	            LOGGER.debug("Proveedor despues del Set "+ ordenesPagoDTO.getNegocio());  
	     		}	
           StringBuilder selectConcepto = new StringBuilder();
           if(cons_Todos){
               selectConcepto.append(cons_Todos == true? CONS_POR_PRIMA + "," + CONS_POR_BS + "," + CONS_POR_DERPOLIZA + "," + CONS_POR_UTILIDAD :"");
               ordenesPagoDTO.setConcepto(selectConcepto.toString());
               LOGGER.debug("Todos los Conseptos : " + ordenesPagoDTO.getConcepto());
	         }
	    	else{
    		    selectConcepto.append(cons_porPrima == true? CONS_POR_PRIMA:"");
    		    LOGGER.debug("Concepto Por prima : " + cons_porPrima);
    			
    		    selectConcepto.append(cons_porPrima ==true && cons_porBS ==true ? ",":"" );
    		    selectConcepto.append(cons_porBS == true? ""+CONS_POR_BS:"");
    		
    		    selectConcepto.append((cons_porPrima == true||cons_porBS==true) &&cons_porDerPoliza ==true ? ",":"");
    		    selectConcepto.append(cons_porDerPoliza == true?""+CONS_POR_DERPOLIZA:"");
    			
    		    selectConcepto.append((cons_porPrima == true||cons_porBS==true || cons_porDerPoliza == true)&&cons_porUtilidad == true? ",":"");
    		    selectConcepto.append(cons_porUtilidad == true?""+CONS_POR_UTILIDAD:"");
    			
    		    ordenesPagoDTO.setConcepto(selectConcepto.toString());
    		    LOGGER.debug("Conseptos Seleccionados : " + cons_porCumpMeta);
    			} 
         
        if(fechaInicio != null){        	
        	ordenesPagoDTO.setFechaInicio(fechaInicio);        	
        	LOGGER.debug("Salida Fecha Inicial : " + getFechaInicio()); 
            }
        
        if(fechaFinal != null){       	
        	ordenesPagoDTO.setFechaFin(fechaFinal);         	
        	LOGGER.debug("Salida Fecha Final : " + getFechaFinal()); 
            }
        
      return ordenesPagoDTO;
	 }	

	@Action(value = "listarFiltrado", results = {
		@Result(name = SUCCESS, location = LOCATION_GRID_JSP),
		@Result(name = INPUT, location = LOCATION_GRID_JSP) })
	@Override
	public String listarFiltrado() {		
		LOGGER.debug(">>listarFiltrado()>>"+ tipoAccion);		
		try {
			if(tipoAccion.equals("5")){				
				listaOrdenPagos = liquidacionCompensacionesService.agruparOrden(getDatosOrdenPagos());
			}else{
				listaOrdenPagos = liquidacionCompensacionesService.obtenerOrdenPagoProceso(USUARIO);
			}
			LOGGER.debug("GENERADO LISTA FILTRADO"+ listaOrdenPagos.size());
		} catch (Exception e) {
			onError(e);
			return INPUT;
		}
		return SUCCESS;		
	}
	
	@Action(value = "listarFiltradoEstatus", results = {
			@Result(name = SUCCESS, location = LOCATION_GRID_OP_JSP),
			@Result(name = INPUT, location = LOCATION_GRID_OP_JSP) })
			
		public String listarFiltradoEstatus() {		
			LOGGER.debug(">>listarFiltrado()>>"+ tipoAccion);		
			try {
				listaLiquidacion = liquidacionCompensacionesService.obtenerOrdenPagoSoliFactura(ESTATUSPENDIENTEPAGAR);
				LOGGER.debug("GENERADO LISTA FILTRADO"+ listaOrdenPagos.size());
			}catch (Exception e) {
				onError(e);
				return INPUT;
			}
			return SUCCESS;		
		}
	
	@Action(value = "listarTodosOrdenesPago", results = {
			@Result(name = SUCCESS, location = LOCATION_GRIDTODOSOP_JSP),
			@Result(name = INPUT, location = LOCATION_GRIDTODOSOP_JSP) })
		
		public String listarTodosOrdenesPago() {		
			LOGGER.debug(">>listarTodosOrdenesPago()");
			try {
				listaOrdenPagos = liquidacionCompensacionesService.buscarLiquidacionXEstatus(datosOrdenPagosTodos());					
				LOGGER.debug("listarTodosOrdenesPago() "+ listaOrdenPagos.size());
			} catch (Exception e) {
				//onError(e);
				LOGGER.error(">>listarTodosOrdenesPago() ",e);
				return INPUT;
			}
			return SUCCESS;		
		}
	@Action(value = "listarFiltradoOrdenesPago", results = {
			@Result(name = SUCCESS, location = LOCATION_GRID_JSP),
			@Result(name = INPUT, location = LOCATION_GRID_JSP) })
		
		public String listarFiltradoOrdenesPago() {		
			LOGGER.debug(">>listarFiltradoOrdenesPago()");
			try {
				//listaOrdenesPago = ordenPagosDetalleService.///liquidacionCompensacionesService.agruparOrden(getDatosOrdenPagos());
				LOGGER.debug("Lista Cargado>>"+ listaOrdenPagos.size());
			} catch (Exception e) {
				onError(e);
				return INPUT;
			}
			return SUCCESS;
		}
	// Actions OrdenPagos_Detalle	
	@Action(value = "verDetalle", results = { 
		@Result(name = SUCCESS, location = LOCATION_DETALLE_JSP),
		@Result(name = INPUT, location = LOCATION_DETALLE_JSP)})
	@Override
	public String verDetalle() {
		return SUCCESS;
	}
		
	@Action(value = "generarExcelDetallePagos", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String generarExcelDetallePagos() {	
		contentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		listaDetallePagos = liquidacionCompensacionesService.buscarOrdenesporLiquidacion(idLiquidacion);
		TransporteImpresionDTO respuesta =impresionesService.getExcel(listaDetallePagos, 
				"midas.impresion.compensacionadicionales.reportedetallepagos",ConstantesReporte.TIPO_XLSX);		
		setInputStream(new ByteArrayInputStream(respuesta.getByteArray()));
		fileName = " DetallePagos-" + new Date().toString() + ".xlsx";
		return SUCCESS;
	}
	
	@Action(value = "listarFiltradoDetalle", results = { 
		@Result(name = SUCCESS, location = LOCATION_GRID_DETALLE_JSP),
		@Result(name = INPUT, location = LOCATION_GRID_DETALLE_JSP)})
		
	public String listarFiltradoDetalle() {
		try {	
	     idLiquidacion = ordenesPagoDTO.getId();
		 LOGGER.debug("listarFiltradoDetalle()  idLiquidacion=>"+idLiquidacion);
		 listaOrdenPagos_Detalle = ordenPagosDetalleService.obtenerOrdenPagosDetalleProcesoPorId(idLiquidacion);
		 LOGGER.debug("GENERADO LISTA FILTRADO"+ listaOrdenPagos_Detalle.size());
		
		 } catch (Exception e) {
			 onError(e);
			 return INPUT;
		 }
		 return SUCCESS;	 
	 }

	public void validateGuardar() {
		
	}

	@Action(value = "actualizarEstatusDetalle", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"listaOrdenPagos_Detalle","${listaOrdenPagos_Detalle}", 
					"ordenesPagoDTO.id","${ordenesPagoDTO.id}" }),
	@Result(name = INPUT, location = LOCATION_DETALLE_JSP) })
	public String actualizarEstatusDetalle() {
		try {
			LOGGER.debug("idLiquidacion>"+idLiquidacion);
			LOGGER.debug("idRecibo>"+idRecibo);
			if (idLiquidacion!= null && idRecibo!=null) {
				setIdTipoOperacion(20L);
				ordenesPagoDTO=new OrdenesPagoDTO();
			ordenesPagoDTO.setId(idLiquidacion);
				liquidacionCompensacionesService.excluirOrdenPago(idLiquidacion, idRecibo);
				onSuccess();
				setTipoAccion("4");
			} else {
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("No se ha encontrado un folio");
				setTipoAccion("3");
			}
		} catch (Exception e) {
			setMensajeError(e.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}
	//////////eliminar Liquidacion///////////////
	@Action(value = "eliminarLiquidacion", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedorOrdenPagos", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}","tabActiva", "${tabActiva}",
					"tipoMensaje", "${tipoMensaje}",
					"listaOrdenPagos_Detalle","${listaOrdenPagos_Detalle}", 
					"ordenesPagoDTO.id","${ordenesPagoDTO.id}" }),
	@Result(name = INPUT, type = "redirectAction", params = {
			"actionName", "mostrarContenedorOrdenPagos", "namespace", NAMESPACE,
			"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}","tabActiva", "${tabActiva}",
			"tipoMensaje", "${tipoMensaje}",
			"listaOrdenPagos_Detalle","${listaOrdenPagos_Detalle}", 
			"ordenesPagoDTO.id","${ordenesPagoDTO.id}" }) })
	public String eliminarLiquidacion() {
		try {
			LOGGER.debug("idLiquidacion>"+idLiquidacion);
			if (idLiquidacion!= null ) {
				liquidacionCompensacionesService.eliminarLiquidacion(idLiquidacion);				
			} else {
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("No se ha encontrado un idLiquidacion");
				setTipoAccion("3");
			}
			this.setTabActiva("orden_pagos");
			onSuccess();
		} catch (Exception e) {
			setMensajeError(e.getMessage());
			this.setTabActiva("orden_pagos");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "verDetalleOP", results = { @Result(name = SUCCESS, location = LOCATION_DETALLE_OP_JSP) })
	public String verDetalleOP() {
		OrdenPagos ordenPagos = new OrdenPagos();
		 if (ordenesPagoDTO != null) {
			 ordenPagos.setId(ordenesPagoDTO.getId());
			 
			 if (ordenPagos.getId() != null) {
				 ordenPagos = ordenPagosService.findById(ordenPagos.getId());
			 }
		 }
		return SUCCESS;
	}
	
	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedorOrdenPagos", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarContenedorOrdenPagos", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}"}) })
	@Override
	public String guardar() {
		LOGGER.debug("getIdsAgregarOp()>>"+getIdsAgregarOp());
		
		try {
			onSuccess();
			String respuesta = liquidacionCompensacionesService.generarOrdenPagos(getIdsAgregarOp());
			
			if(respuesta != null){
				String[] polizas = respuesta.split(":");
				String mensaje = polizas[0]; //Se obtienen los id's de las pólizas
				String cantidad = polizas[1]; //Se obtiene la cantidad de pólizas con PROVEEDOR X
				setMensaje("Se encontraron " + cantidad + " pólizas configuradas con PROVEEDOR X, no es posible generar la orden de pago.");  //las cuales se enlistan a continuación: \n " + mensaje); 
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
			
			tipoAccion = "4";
			setIdTipoOperacion(20L);
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return result;
	}
	
	@Action(value = "cotabilizarCheques", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarPagosOrdenPediente", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarPagosOrdenPediente", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}"}) })
	public String cotabilizarCheques() {
		LOGGER.debug("getIdsAgregarOp()>>"+getIdsAgregarOp());
		try {
			onSuccess();
			liquidacionCompensacionesService.contabilizarCheques(getIdsAgregarOp());
			//tipoAccion = "4";
			setIdTipoOperacion(20L);
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return result;
	}
	@Action(value = "enviarReportePorCorreo", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarOrdenPagos", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
					
	public String enviarReportePorCorreo() {
		LOGGER.debug(idParametro);
		tipoSalidaArchivo = "pdf";
		try {
			String[] idParametro = getIdParametro().split(",");
			int i;
			for(i=0;i<idParametro.length;i++){
				Long idLiquidacion = Long.valueOf(idParametro[i]);
				ordenPagosService.enviarReportePorCorreo(idLiquidacion);
				onSuccess();
				tipoAccion = "4";
				setIdTipoOperacion(20L);
			}	
		} catch (Exception e) {
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}
	

	@Action(value = "mostrarEnvioFacturaCa", results = {
	      @Result(name = SUCCESS, location = MOSTRAR_FACTURA_JSP),
	      @Result(name = INPUT, location = MOSTRAR_FACTURA_JSP) })
	  public String mostrarEnvioFacturaCa(){
	    Usuario usuario = usuarioService.getUsuarioActual();
	    isProveedor = usuarioService.tienePermisoUsuarioActual("FN_M2_CA_ProveedorCompensaciones");
	    isAgente= usuarioService.tieneRolUsuarioActual(sistemaContext.getRolAgenteAutos());
	    Long idBeneficiario=(isProveedor==false && isAgente==false)?this.idAgente:null;
	      if(isProveedor){
	        try{
		        listaPrestadoresServicio = prestadorDeServicioService.obtenerPrestadoresPorUsuario(usuario.getNombreUsuario());
				if(listaPrestadoresServicio.size() == 1){ // un solo registro{
					idPrestadorServicio = Long.valueOf(listaPrestadoresServicio.get(0).get("id").toString());
				}
				if(idPrestadorServicio != null){
					setPrestadorServicio(prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue()));
					nombreAgente = prestadorServicio.getPersonaMidas().getNombre();
		        	rfc=prestadorServicio.getPersonaMidas().getRfc();
		        	this.idAgente=Long.valueOf(this.prestadorServicio.getId());
				}		
	        }catch(Exception e){
	          LOG.error("Error al cargar los datos del Proveedor", e);
	        }
	      }
	      if (isAgente){
	        try{
	          Agente agt = envioFacturaService.getDatosAgente(usuario.getNombreUsuario(), usuario.getPersonaId(),null);
	          this.setAgente1(agt);
	          this.iniciaValores();
	        }catch(Exception e){
	          LOG.error("Error al cargar los datos del Agente", e);
	        }
	      }        
	      if(!isProveedor && !isAgente && idBeneficiario != null){
	        try{
	          Agente agt = envioFacturaService.getDatosAgente(null, null, idBeneficiario);
	          if(agt !=null){
	            this.setAgente1(agt);
	            this.iniciaValores();            
	          }else{
	        	  listaPrestadoresServicio = prestadorDeServicioService.obtenerPrestadoresPorUsuario(usuario.getNombreUsuario());
					if(listaPrestadoresServicio.size() == 1){ // un solo registro{
						idPrestadorServicio = Long.valueOf(listaPrestadoresServicio.get(0).get("id").toString());
					}
					if(idPrestadorServicio != null){
						setPrestadorServicio(prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue()));
						nombreAgente = prestadorServicio.getPersonaMidas().getNombre();
			        	rfc=prestadorServicio.getPersonaMidas().getRfc();
			        	this.idAgente=Long.valueOf(this.prestadorServicio.getId());
					}		
	          }
	        }catch(Exception e){
	          LOG.error("Error al cargar los datos del Agente", e);
	        }
	      }
	    
	    this.tabActiva = ConstantesCompensacionesAdicionales.TAB_COMPENSACION;
	    return SUCCESS;
		}
	@Action(value = "mostrarFacturaProveedores", results = {
			@Result(name = SUCCESS, location = MOSTRAR_FACTURA_PROV_JSP),
			@Result(name = INPUT, location = MOSTRAR_FACTURA_PROV_JSP) })
	public String mostrarFacturaProveedores() {
		return SUCCESS;
	}
	
	@Action(value = "listarOrdenPagoFactura", results = {
			@Result(name = SUCCESS, location = LOCATION_GRID_OP_JSP),
			@Result(name = INPUT, location = LOCATION_GRID_OP_JSP) })
	public String listarOrdenPagoFactura() {
		lista = ordenPagosService.obtenerOrdenPagosGeneradoPorId(null);
		return SUCCESS;
	}
	
	@Action(value="subirArchivoXml", 
			results={@Result(name=SUCCESS,type="redirectAction",
						params={"actionName","mostrarEnvioFacturaCa","namespace","/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa","idAgente","${idAgente}"}),					 
				@Result(name=INPUT,type="redirectAction",
						params={"actionName","mostrarEnvioFacturaCa","namespace","/compensacionesAdicionales/ordenPago/mostrarEnvioFacturaCa","idAgente","${idAgente}"})
	})
	
	public String subirArchivoXml() {
		EnvioFacturaAgente envio=null;
		if(idAgente !=null && idLiquidacion != null){
			try{
				if(facturaXml != null){
					Usuario usuario = usuarioService.getUsuarioActual();
					String usuarioName =usuario.getNombreUsuario();
					envio = liquidacionCompensacionesService.persistEnvioFacturaCa(idAgente, idLiquidacion, facturaXml,usuarioName);
					generaRespuesta("Archivo cargado con exito, su factura fue "+ envio.getStatus().getValor()+"."+envio.getMessagesFactura()+"");
				}else{
					generaRespuesta("Error al cargar su Factura, Revise el archivo enviado.");
				}
			}catch (FileNotFoundException e) {
				LOG.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, Revise el archivo enviado."+(envio!=null?envio.getMessagesFactura():""));
			}catch(PersistenceException e){
				LOG.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, Revise tus datos Fiscales."+(envio!=null?envio.getMessagesFactura():""));
			}catch(ConnectException e){
				LOG.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, No se puede conectar con el validador, pruebe dentro de un momento."+(envio!=null?envio.getMessagesFactura():""));
			}catch(Exception e){
				LOG.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, pruebe dentro de un momento."+(envio!=null?envio.getMessagesFactura():""));
			}			
		}
		return SUCCESS;
	}
	
	public String generaRespuesta(String mensaje){
		try {
			setResponse(ServletActionContext.getResponse());
			getResponse().setContentType("text/html");
        	PrintWriter out = getResponse().getWriter();
        	out.write(mensaje);
        	out.flush();
        	out.close();
        } catch (IOException ioe) {
        	LOG.error(" --generaRespuesta() ", ioe);
        }

        return com.opensymphony.xwork2.Action.NONE;
	}
	
	private OrdenesPagoDTO datosOrdenPagosTodos() {		 
		ordenesPagoDTO = new OrdenesPagoDTO();
		try{
			if (ramo != null){
				ordenesPagoDTO.setRamo(ramo.toString().trim());
				LOGGER.debug(">>Ramo" + ordenesPagoDTO.getRamo());
			}
			if (negocio != null) {
				ordenesPagoDTO.setNegocio(negocio.toString());
		        LOGGER.debug(">>Negocio" + ordenesPagoDTO.getNegocio());
		        }
			if(contratante != null){
				ordenesPagoDTO.setContratante(contratante.trim()); 
				ordenesPagoDTO.setNombreContratante(contratante.trim());
                LOGGER.debug(">>Contratante"+ ordenesPagoDTO.getContratante());
                LOGGER.debug(">>NombreContratante"+ ordenesPagoDTO.getNombreContratante());
            }
           if (agente != null){              
        	   ordenesPagoDTO.setAgente(agente.toString().trim());
                LOGGER.debug(">>Agente"+ ordenesPagoDTO.getAgente()); 
                }    
           if(proveedor != null){
        	   ordenesPagoDTO.setProveedor(proveedor.toString().trim());  
	           	LOGGER.debug(">>Poveedor"+ ordenesPagoDTO.getProveedor());
	        }
            if (promotor!=null) {
            	ordenesPagoDTO.setPromotor(promotor.toString().trim());
    			LOGGER.debug(">>promotor"+ ordenesPagoDTO.getPromotor());
		    	}             
           if(gerencia != null){
        	   ordenesPagoDTO.setGerencia(gerencia.toString()); 
               LOGGER.debug(">>gerencia"+ ordenesPagoDTO.getGerencia());
               }  		   
           if(estatusFactura != null){
        	   ordenesPagoDTO.setEstatusFactura(estatusFactura.toString().trim()); 
               LOGGER.debug(">>estatusFactura"+ ordenesPagoDTO.getEstatusFactura());
               }
           if(estatusOrdenPago != null){
        	   ordenesPagoDTO.setEstatusOrdenpago(estatusOrdenPago.toString().trim()); 
               LOGGER.debug(">>estatusOrdenPago"+ ordenesPagoDTO.getEstatusOrdenpago());
               }
           if (!folio.trim().equals("") ){
				ordenesPagoDTO.setId(new Long(folio.toString()));
				LOGGER.debug(">>Folio" + ordenesPagoDTO.getId());
			}
           
		}catch(Exception e){
			LOG.error(" error getDatosOrdenPagosTodos() ", e);
		}
      return ordenesPagoDTO;      
	 }	
	/*
	 * obtenemos Reporte de Autorizacion de Pagos
	 * 
	 *  */
	
	@Action(value="reporteAutorizarOP",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","inputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})})	
	public String reporteAutorizarOrdenPagos(){
		try {
			TransporteImpresionDTO reporte = generarPlantillaReporte.imprimirReporteAutorizacionOrdenpago(getIdsAgregarOp());
			inputStream=new ByteArrayInputStream(reporte.getByteArray());
			contentType = "application/xls";
			fileName="Reporte Autorizar_Pago "+UtileriasWeb.getFechaString(new Date())+".xls";
		} catch (Exception e) {
			onError(e);
		}		
		return result;
	}	
		
	@Action(value = "guardarSiniestralidad", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"listaOrdenPagos_Detalle","${listaOrdenPagos_Detalle}", 
					"ordenesPagoDTO.id","${ordenesPagoDTO.id}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarContenedorOrdenPagos", "namespace", NAMESPACE,
					"tipoAccion", "${tipoAccion}", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"listaOrdenPagos_Detalle","${listaOrdenPagos_Detalle}", 
					"ordenesPagoDTO.id","${ordenesPagoDTO.id}"}) })
	public String guardarSiniestralidad() {
		LOGGER.debug("guardarSiniestralidad()>>"+getIdsAgregarOp());
		 StringBuilder mensaje = new StringBuilder();
		 boolean msj=false;
		 String valor="";
		try {
			String[] parts = getIdsAgregarOp().split(";");
			for( String val:parts){
				valor=ordenPagosDetalleService.guardarSiniestralidad(val);
				if(valor!="1")
					mensaje.append(valor+"\n");
				else
					msj=true;
			}
			if(msj)
				mensaje.append("Ocurrio un Error"+"\n");
			ordenesPagoDTO=new OrdenesPagoDTO();
			ordenesPagoDTO.setId(idLiquidacion);
			listaOrdenPagos_Detalle = ordenPagosDetalleService.obtenerOrdenPagosDetalleProcesoPorId(idLiquidacion);
			setMensaje(mensaje.toString());
			setTipoMensaje("4");
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return SUCCESS;
	}
	
	public String getTipoAccion() {
		return tipoAccion;
	}

	public String getTabActiva() {	
		return tabActiva;
	}
	
	public void setTabActiva(String tabActiva) {	
		this.tabActiva=tabActiva;
	}

	public OrdenPagosService getOrdenPagosService() {
		return ordenPagosService;
	}

	public void setOrdenPagosService(OrdenPagosService ordenPagosService) {
		this.ordenPagosService = ordenPagosService;
	}

	public OrdenPagosDetalleService getOrdenPagosDetalleService() {
		return ordenPagosDetalleService;
	}

	public void setOrdenPagosDetalleService(
			OrdenPagosDetalleService ordenPagosDetalleService) {
		this.ordenPagosDetalleService = ordenPagosDetalleService;
	}

	public List<OrdenPagos> getLista() {
		return lista;
	}

	public  void setLista(List<OrdenPagos> lista) {
		this.lista = lista;
	}

	public boolean isRamoAuto() {
		return ramoAuto;
	}

	public  void setRamoAuto(boolean ramoAuto) {
		this.ramoAuto = ramoAuto;
	}

	public  Long getAgente() {
		return agente;
	}

	public  void setAgente(Long agente) {
		this.agente = agente;
	}
	
	public boolean isRamoVida() {
		return ramoVida;
	}

	public  void setRamoVida(boolean ramoVida) {
		this.ramoVida = ramoVida;
	}

	public  boolean isRamoDanios() {
		return ramoDanios;
	}

	public  void setRamoDanios(boolean ramoDanios) {
		this.ramoDanios = ramoDanios;
	}
		
	public  Long getPromotor() {
		return promotor;
	}

	public  void setPromotor(Long promotor) {
		this.promotor = promotor;
	}

	public Long getProveedor() {
		return proveedor;
	}

	public  void setProveedor(Long proveedor) {
		this.proveedor = proveedor;
	}

	public  String getContratante() {
		return contratante;
	}

	public  void setContratante(String contratante) {
		this.contratante = contratante;
	}

	public  Long getGerencia() {
		return gerencia;
	}

	public  void setGerencia(Long gerencia) {
		this.gerencia = gerencia;
	}

	public  Long getNegocio() {
		return negocio;
	}

	public  void setNegocio(Long negocio) {
		this.negocio = negocio;
	}

	public  Date getFechaInicio() {
		return fechaInicio;
	}

	public  void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public  Date getFechaFinal() {
		return fechaFinal;
	}

	public  void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public  boolean isCons_porPrima() {
		return cons_porPrima;
	}

	public  void setCons_porPrima(boolean cons_porPrima) {
		this.cons_porPrima = cons_porPrima;
	}

	public boolean isCons_porBS() {
		return cons_porBS;
	}

	public  void setCons_porBS(boolean cons_porBS) {
		this.cons_porBS = cons_porBS;
	}

	public  boolean isCons_porCumpMeta() {
		return cons_porCumpMeta;
	}

	public  void setCons_porCumpMeta(boolean cons_porCumpMeta) {
		this.cons_porCumpMeta = cons_porCumpMeta;
	}

	public  boolean isCons_porDerPoliza() {
		return cons_porDerPoliza;
	}

	public  void setCons_porDerPoliza(boolean cons_porDerPoliza) {
		this.cons_porDerPoliza = cons_porDerPoliza;
	}

	public  boolean isCons_porUtilidad() {
		return cons_porUtilidad;
	}

	public  void setCons_porUtilidad(boolean cons_porUtilidad) {
		this.cons_porUtilidad = cons_porUtilidad;
	}

	public  boolean isCons_Todos() {
		return cons_Todos;
	}

	public  void setCons_Todos(boolean cons_Todos) {
		this.cons_Todos = cons_Todos;
	}

	public List<OrdenPagosDetalle> getListaOrdenPagos_Detalle() {
		return listaOrdenPagos_Detalle;
	}

	public void setListaOrdenPagos_Detalle(
			List<OrdenPagosDetalle> listaOrdenPagos_Detalle) {
		this.listaOrdenPagos_Detalle = listaOrdenPagos_Detalle;
	}

	public String getTipoSalidaArchivo() {
		return tipoSalidaArchivo;
	}

	public void setTipoSalidaArchivo(String tipoSalidaArchivo) {
		this.tipoSalidaArchivo = tipoSalidaArchivo;
	}

	public OrdenesPagoDTO getOrdenesPagoDTO() {
		return ordenesPagoDTO;
	}

	public void setOrdenesPagoDTO(OrdenesPagoDTO ordenesPagoDTO) {
		this.ordenesPagoDTO = ordenesPagoDTO;
	}

	public ConfigBonosService getConfigBonosService() {
		return configBonosService;
	}

	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	public Boolean getChkTodosAgentes() {
		return chkTodosAgentes;
	}

	public void setChkTodosAgentes(Boolean chkTodosAgentes) {
		this.chkTodosAgentes = chkTodosAgentes;
	}

	public Boolean getChkDetalle() {
		return chkDetalle;
	}

	public void setChkDetalle(Boolean chkDetalle) {
		this.chkDetalle = chkDetalle;
	}

	public Boolean getChkSegAfirmeCom() {
		return chkSegAfirmeCom;
	}

	public void setChkSegAfirmeCom(Boolean chkSegAfirmeCom) {
		this.chkSegAfirmeCom = chkSegAfirmeCom;
	}

	public Boolean getChkGuiaLectura() {
		return chkGuiaLectura;
	}

	public void setChkGuiaLectura(Boolean chkGuiaLectura) {
		this.chkGuiaLectura = chkGuiaLectura;
	}

	public Boolean getChkMostrarColAdicionales() {
		return chkMostrarColAdicionales;
	}

	public void setChkMostrarColAdicionales(Boolean chkMostrarColAdicionales) {
		this.chkMostrarColAdicionales = chkMostrarColAdicionales;
	}

	public String getTextoSegAfirmeCom() {
		return textoSegAfirmeCom;
	}

	public void setTextoSegAfirmeCom(String textoSegAfirmeCom) {
		this.textoSegAfirmeCom = textoSegAfirmeCom;
	}

	public Long getIdAgenteSeleccionado() {
		return idAgenteSeleccionado;
	}

	public void setIdAgenteSeleccionado(Long idAgenteSeleccionado) {
		this.idAgenteSeleccionado = idAgenteSeleccionado;
	}

	public String getFormatoDeSalida() {
		return formatoDeSalida;
	}

	public void setFormatoDeSalida(String formatoDeSalida) {
		this.formatoDeSalida = formatoDeSalida;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMes() {
		return mes;
	}

	public String getAnio() {
		return anio;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public OrdenPagos getOrdenPagos() {
		return ordenPagos;
	}

	public void setOrdenPagos(OrdenPagos ordenPagos) {
		this.ordenPagos = ordenPagos;
	}
	

	@Override
	public String eliminar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	public void setEsExcluir(Boolean esExcluir) {
		this.esExcluir = esExcluir;
	}

	public Boolean getEsExcluir() {
		return esExcluir;
	}

	public  Boolean getAgentesTodos() {
		return agentesTodos;
	}

	public  void setAgentesTodos(Boolean agentesTodos) {
		this.agentesTodos = agentesTodos;
	}

	public  Boolean getPromotoresTodos() {
		return promotoresTodos;
	}

	public  void setPromotoresTodos(Boolean promotoresTodos) {
		this.promotoresTodos = promotoresTodos;
	}

	public  Boolean getProveedoresTodos() {
		return proveedoresTodos;
	}

	public  void setProveedoresTodos(Boolean proveedoresTodos) {
		this.proveedoresTodos = proveedoresTodos;
	}

	public  Boolean getNegociosTodos() {
		return negociosTodos;
	}

	public  void setNegociosTodos(Boolean negociosTodos) {
		this.negociosTodos = negociosTodos;
	}

	public  boolean isContratanteTodos() {
		return contratanteTodos;
	}

	public  void setContratanteTodos(boolean contratanteTodos) {
		this.contratanteTodos = contratanteTodos;
	}

	public static String getIdParametro() {
		return idParametro;
	}

	public static void setIdParametro(String idParametro) {
		OrdenPagosAction.idParametro = idParametro;
	}

	public Long getIdRecibo() {
		return idRecibo;
	}

	public void setIdRecibo(Long idRecibo) {
		this.idRecibo = idRecibo;
	}

	public  OrdenPagosDetalle getOrdenPagosDetalle() {
		return ordenPagosDetalle;
	}

	public  void setOrdenPagosDetalle(OrdenPagosDetalle ordenPagosDetalle) {
		this.ordenPagosDetalle = ordenPagosDetalle;
	}

	public  boolean isRamoTodos() {
		return ramoTodos;
	}

	public  void setRamoTodos(boolean ramoTodos) {
		this.ramoTodos = ramoTodos;
	}
	
	public String getIdsAgregarOp() {
		return idsAgregarOp;
	}

	public void setIdsAgregarOp(String idsAgregarOp) {
		this.idsAgregarOp = idsAgregarOp;
	}

	public Long getIdentificadorBenId() {
		return identificadorBenId;
	}

	public void setIdentificadorBenId(Long identificadorBenId) {
		this.identificadorBenId = identificadorBenId;
	}

	public  List<OrdenesPagoDTO> getListaOrdenPagos() {
		return listaOrdenPagos;
	}

	public  void setListaOrdenPagos(List<OrdenesPagoDTO> listaOrdenPagos) {
		this.listaOrdenPagos = listaOrdenPagos;
	}

	public Long getIdLiquidacion() {
		return idLiquidacion;
	}

	public void setIdLiquidacion(Long idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}

	public  boolean isGerenciaTodos() {
		return gerenciaTodos;
	}

	public  void setGerenciaTodos(boolean gerenciaTodos) {
		this.gerenciaTodos = gerenciaTodos;
	}

	public  List<LiquidacionCompensaciones> getListaLiquidacion() {
		return listaLiquidacion;
	}

	public  void setListaLiquidacion(
			List<LiquidacionCompensaciones> listaLiquidacion) {
		this.listaLiquidacion = listaLiquidacion;
	}

	public  boolean isEnProceso() {
		return enProceso;
	}

	public  void setEnProceso(boolean enProceso) {
		this.enProceso = enProceso;
	}
	public  List<OrdenesPagoDTO> getListaOrdenesPago() {
		return listaOrdenesPago;
	}
	public  void setListaOrdenesPago(List<OrdenesPagoDTO> listaOrdenesPago) {
		this.listaOrdenesPago = listaOrdenesPago;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public File getFacturaXml() {
		return facturaXml;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public void setFacturaXml(File facturaXml) {
		this.facturaXml = facturaXml;
	}
	
	public  void setListaDetallePagos(List<DatosDetallePagosPorRecibo> listaDetallePagos) {
		this.listaDetallePagos = listaDetallePagos;
	}
	public  List<DatosDetallePagosPorRecibo> getListaDetallePagos() {
		return listaDetallePagos;
	}
	public List<CaRamo> getRamoLista() {
		return ramoLista;
	}
	public void setRamoLista(List<CaRamo> ramoLista) {
		this.ramoLista = ramoLista;
	}
	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}
	
	@Autowired
	@Qualifier("envioFacturaAgenteEJB")
	public void setEnvioFacturaService(EnvioFacturaAgenteService envioFacturaService) {
		this.envioFacturaService = envioFacturaService;
	}
	
	@Autowired
	@Qualifier("promotoriaJPAServiceEJB")
	public void setPromotoriaService(PromotoriaJPAService promotoriaService) {
		this.promotoriaService = promotoriaService;
	}
	
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public String getRamo() {
		return ramo;
	}
	public  void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public Long getEstatusFactura() {
		return estatusFactura;
	}
	public  void setEstatusFactura(Long estatusFactura) {
		this.estatusFactura = estatusFactura;
	}
	public  Long getEstatusOrdenPago() {
		return estatusOrdenPago;
	}
	public  void setEstatusOrdenPago(Long estatusOrdenPago) {
		this.estatusOrdenPago = estatusOrdenPago;
	}

	public OrdenesPagoDTO getOrdenesTodosPagoDTO() {
		return ordenesTodosPagoDTO;
	}
	public void setOrdenesTodosPagoDTO(OrdenesPagoDTO ordenesTodosPagoDTO) {
		this.ordenesTodosPagoDTO = ordenesTodosPagoDTO;
	}
	public List<OrdenesPagoDTO> getListaTodosOrdenPagos() {
		return listaTodosOrdenPagos;
	}
	public void setListaTodosOrdenPagos(List<OrdenesPagoDTO> listaTodosOrdenPagos) {
		this.listaTodosOrdenPagos = listaTodosOrdenPagos;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public OrdenesPagoFiltroDTO getFiltros() {
		return filtros;
	}
	public void setFiltros(OrdenesPagoFiltroDTO filtros) {
		this.filtros = filtros;
	}
	
	public String getNumeroCedula() {
		return numeroCedula;
	}
	public void setNumeroCedula(String numeroCedula) {
		this.numeroCedula = numeroCedula;
	}
	public String getTipoAgente() {
		return tipoAgente;
	}
	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}
	public String getNombreAgente() {
		return nombreAgente;
	}
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}
	public String getEjecutivo() {
		return ejecutivo;
	}
	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	public Agente getAgente1() {
		return agente1;
	}
	public void setAgente1(Agente agente1) {
		this.agente1 = agente1;
	}
	public Boolean getIsProveedor() {
		return isProveedor;
	}
	public void setIsProveedor(Boolean isProveedor) {
		this.isProveedor = isProveedor;
	}
	public Boolean getIsAgente() {
		return isAgente;
	}
	public void setIsAgente(Boolean isAgente) {
		this.isAgente = isAgente;
	}
	public Long getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}
	public PrestadorServicio getPrestadorServicio() {
		return prestadorServicio;
	}
	public void setPrestadorServicio(PrestadorServicio prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}
	public String getRfc(){
		return this.rfc;
	}
	public void setRfc(String rfc){
		this.rfc=rfc;
	}
	public String getVencimientoCedula(){
		return this.vencimientoCedula;
	}
	public void setVencimientoCedula(String vencimientoCedula){
		this.vencimientoCedula=vencimientoCedula;
	}
	public String getCentroOperacion(){
		return this.centroOperacion;
	}
	public void setCentroOperacion(String centroOperacion){
		this.centroOperacion=centroOperacion;
	}
	public String getGerenciaAgente(){
		return this.gerenciaAgente;
	}
	public void setGerenciaAgente(String gerenciaAgente){
		this.gerenciaAgente=gerenciaAgente;
	}
	public String getEstatus(){
		return this.estatus;
	}
	public void setEstatus(String estatus){
		this.estatus=estatus;
	}
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	public void setPrestadorDeServicioService(
			PrestadorDeServicioService prestadorDeServicioService) {
		this.prestadorDeServicioService = prestadorDeServicioService;
	}
	
	public void iniciaValores(){
		idAgente = agente1.getIdAgente();
		numeroCedula = agente1.getNumeroCedula();
		tipoAgente = agente1.getTipoAgente();
		nombreAgente = agente1.getPersona().getNombreCompleto();
		ejecutivo = agente1.getPromotoria().getEjecutivo().getPersonaResponsable().getNombreCompleto();
		rfc=agente1.getPersona().getRfc();
		vencimientoCedula=agente1.getVencimientoCedulaString();
		centroOperacion = agente1.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().getDescripcion();
		gerenciaAgente = agente1.getPromotoria().getEjecutivo().getGerencia().getDescripcion();
		estatus = agente1.getTipoSituacion().getValor();
	}
	
	}
