package mx.com.afirme.midas2.action.compensacionesAdicionales.complementoPago;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
//import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
//import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
//import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
//import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
//import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
//import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
//import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
//import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.RelacionFacturaOrdenCompraDTO;
//import mx.com.afirme.midas2.service.ListadoService;
//import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.fuerzaventa.EnvioFacturaAgenteService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
//import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
//import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
//import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/compensacionesAdicionales/complementoPagoCompensaciones")
public class ComplementoPagoCompensacionesAction extends BaseAction {

	private static final long serialVersionUID = 1L;
	 
	private EnvioFacturaAgenteService envioFacturaAgenteService;
	public UsuarioService usuarioService;
	
	private Long idAgente;
	private String numeroCedula;
	private String vencimientoCedula;
	private String tipoAgente;
	private String nombreAgente;
	private String centroOperacion;
	private String gerencia;
	private String estatus;
	private String rfc;
	private String ejecutivo;
	private Boolean isAgente;
	private Boolean isAdministrador;

	private final String ENVIOCOMPLEMENTOCONTENEDOR = "/jsp/compensacionesAdicionales/complementoPago/mostrarEnvioComplementoPago.jsp";
	private final String GRIDCARGACOMPLEMENTOS = "/jsp/compensacionesAdicionales/complementoPago/complementoPagoCargaComplGrid.jsp";
	private final String GRIDFACTURAS = "/jsp/compensacionesAdicionales/complementoPago/complementoPagoFacturaGrid.jsp";
	
	@Action(value="mostrarEnvioComplementoPago",
			results={@Result( name=SUCCESS, location=ENVIOCOMPLEMENTOCONTENEDOR)
	})
	public String mostrarEnvioComplementoPago(){
		Usuario usuario = usuarioService.getUsuarioActual();
		isAgente = usuarioService.tieneRolUsuarioActual(sistemaContext.getRolAgenteAutos());	
		isAdministrador = usuarioService.tienePermisoUsuarioActual("FN_M2_Carga_Facturas_Administrador");
		
		try{
			if(isAdministrador && idAgente != null){
				iniciaValores(envioFacturaAgenteService.getDatosAgente(null, null,idAgente));
			}else if (isAgente){
				iniciaValores(envioFacturaAgenteService.getDatosAgente(usuario.getNombreUsuario(), usuario.getPersonaId(),null));
			}
		}catch (Exception e) {
			//addActionError("Error al iniciar Datos de Agente");
			this.setMensajeError("Error al iniciar Datos de Agente");
			//log.error(" --init() ", e);
		}
		return SUCCESS;
	}
	
	public void iniciaValores(Agente agente){
		idAgente = agente.getIdAgente();
		numeroCedula = agente.getNumeroCedula();
		vencimientoCedula = agente.getVencimientoCedulaString();
		tipoAgente = agente.getTipoAgente();
		nombreAgente = agente.getPersona().getNombreCompleto();
		centroOperacion = agente.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().getDescripcion();
		gerencia = agente.getPromotoria().getEjecutivo().getGerencia().getDescripcion();
		estatus = agente.getTipoSituacion().getValor();
		rfc = agente.getPersona().getRfc();
		ejecutivo = agente.getPromotoria().getEjecutivo().getPersonaResponsable().getNombreCompleto();	
	}

	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Autowired
	@Qualifier("envioFacturaAgenteEJB")
	public void setEnvioFacturaAgenteService(EnvioFacturaAgenteService envioFacturaAgenteService) {
		this.envioFacturaAgenteService = envioFacturaAgenteService;
	}
	
	//Datos de agente
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNumeroCedula() {
		return numeroCedula;
	}

	public void setNumeroCedula(String numeroCedula) {
		this.numeroCedula = numeroCedula;
	}

	public String getVencimientoCedula() {
		return vencimientoCedula;
	}

	public void setVencimientoCedula(String vencimientoCedula) {
		this.vencimientoCedula = vencimientoCedula;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Boolean getIsAgente() {
		return isAgente;
	}

	public void setAgente(boolean isAgente) {
		this.isAgente = isAgente;
	}

	public Boolean getIsAdministrador() {
		return isAdministrador;
	}

	public void setAdministrador(boolean isAdministrador) {
		this.isAdministrador = isAdministrador;
	}
}
