/**
 * 
 * Powered by AddMotions. 
 * Monterrey, MÃ©xico
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.action.compensacionesAdicionales;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaLineaNegocio;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaRangos;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCondicionCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoProvision;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoRango;
import mx.com.afirme.midas2.domain.compensaciones.Proveedor;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.ProductoVidaDTO;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosViewDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.compensaciones.CaBaseCalculoService;
import mx.com.afirme.midas2.service.compensaciones.CaBitacoraService;
import mx.com.afirme.midas2.service.compensaciones.CaCompensacionService;
import mx.com.afirme.midas2.service.compensaciones.CaEntidadPersonaService;
import mx.com.afirme.midas2.service.compensaciones.CaLineaNegocioService;
import mx.com.afirme.midas2.service.compensaciones.CaParametrosService;
import mx.com.afirme.midas2.service.compensaciones.CaRamoService;
import mx.com.afirme.midas2.service.compensaciones.CaRangosService;
import mx.com.afirme.midas2.service.compensaciones.CaSubramosDaniosService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoAutorizacionService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoCompensacionService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoCondicionCalculoService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoContratoService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoMonedaService;
import mx.com.afirme.midas2.service.compensaciones.CaTipoProvisionService;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosDetalleService;
import mx.com.afirme.midas2.service.compensaciones.negociovida.NegocioVidaService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class CompensacionesAdicionalesAction extends BaseAction {

	private static final Logger LOG = Logger.getLogger(CompensacionesAdicionalesAction.class);
	
	private static final long serialVersionUID = 1L;

	@Autowired private CaBaseCalculoService baseCalculocaService;
	@Autowired private CaTipoMonedaService monedacaService;	
	@Autowired private CaTipoContratoService tipoContratocaService;
	@Autowired private CaCompensacionService caCompensacionService;
	@Autowired private CaEntidadPersonaService entidadPersonacaService;
	@Autowired private CaParametrosService parametrosGralescaService;
	@Autowired private CaTipoCompensacionService tipoCompensacioncaService;
	@Autowired private CaTipoCondicionCalculoService tipoCondicionesCalccaService;
	@Autowired private CaTipoProvisionService tipoProvisioncaService;
	@Autowired private CaTipoAutorizacionService tipoAutorizacioncaService;
	@Autowired private CaRamoService ramocaService;
	@Autowired private CaLineaNegocioService lineaNegociocaService;
	@Autowired private CaRangosService rangoscaService;	
	@Autowired private CatalogoCompensacionesService catalogoCompensacionesService;
	@Autowired private NegocioService negocioService;
	@Autowired private NegocioSeccionService negocioSeccionService;	
	@Autowired private AgenteMidasService agenteMidasService;
	@Autowired private CaBitacoraService bitacoraService;
	@Autowired private CaSubramosDaniosService caSubramosDaniosService;
	@Autowired private OrdenPagosDetalleService ordenPagosDetalleService;
	@Autowired private NegocioVidaService negocioVidaService;
	private EntidadService entidadService;
	private List<CaBaseCalculo> listBaseCalculoca;
	private List<CaTipoMoneda> listTipoMonedaca;
	private List<CaTipoContrato> listTipoContratoca;
	private List<CaTipoCondicionCalculo> listCondicionesCalccas;
	private List<CaTipoProvision> listTipoProvisionca;
	private List<CaTipoAutorizacion> listTipoAutorizacionca;
	private List<CaTipoAutorizacion> listTipoAutorizacioncaByCompensacion;
	private List<CaLineaNegocio> lineasInicial;
	private List<CaRangos> listRangoscas;
	private List<CaTipoRango> listTipoRangocas;
    private List<CaBitacora> listcaBitacora; 
    private List<CompensacionesDTO> listRecibos;
        
	private Long idNegocio;
	private Long idCotizacion;
	private Long idCotizacionEndoso;
	private Long idNegocioVida;
	private Long idConfiguracionBanca;
	private Long idPoliza;
	private BigDecimal topeMaximo;
	private int idRecibo;
	private String ramo;
	private boolean contraprestacionB;
	private CompensacionesDTO compensacionesDTO;
	private int esContra;
	private String idAgenteAgregar;
	private Long idCompensacionActual;
	private Long idTipoCompensacion;
	private CaEntidadPersona caEntidadPersonaCargar;
	private String idProveedorCargar;
	private int compensacionNueva;
	private Long entidadPersonaId;
	private String dataGson;
	private InputStream dataStream;
	private String tabActiva;
	private File fileUpload;
	private String invocacionConfigurador;
	private String tipoAccion;
	private String vidaIndiv; 
	private String formaPagoBanca;
	private List<ProductoVidaDTO> productosVida= new ArrayList<ProductoVidaDTO>();
	private int aplicaComision;	
	public String verRangosSiniestralidadContra(){
		LOG.debug(">> verRangosSiniestralidadContra()");
		String result = verRangosSiniestralidad();
		LOG.debug("<< verRangosSiniestralidadContra()");
		return result;
	}
	
	public String verRangosSiniestralidad(){
		LOG.debug(">> verRangosSiniestralidad()");
		CaCompensacion caCompensacion = null;

		RAMO caRamo = RAMO.findByType(this.ramo);

		switch(caRamo){

			case AUTOS:{					
				caCompensacion = caCompensacionService.findByRamoAndFieldAndContraprestacion(caRamo.getValor(),  ConstantesCompensacionesAdicionales.NEGOCIO_ID, this.idNegocio, contraprestacionB);
				break;
			}

			case DANOS:{
				caCompensacion = caCompensacionService.findByRamoAndFieldAndContraprestacion(caRamo.getValor(),  ConstantesCompensacionesAdicionales.COTIZACION_ID, this.idCotizacion, contraprestacionB);
				break;
			}

			case VIDA:{
				caCompensacion = caCompensacionService.findByRamoAndFieldAndContraprestacion(caRamo.getValor(),  ConstantesCompensacionesAdicionales.NEGOCIOVIDA_ID, this.idNegocioVida, contraprestacionB);
				break;
			}
			
			case BANCA:{
				caCompensacion = caCompensacionService.findByRamoAndFieldAndContraprestacion(caRamo.getValor(),  ConstantesCompensacionesAdicionales.CONFIGURACIONBANCA_ID, this.idConfiguracionBanca, contraprestacionB);
				break;
			}
		}
		
		if(caCompensacion != null){
			
			CaParametros pg = null;
			CaEntidadPersona entidadPersonaParametros = null;
	
			if(contraprestacionB && entidadPersonaId != null){										
				entidadPersonaParametros = entidadPersonacaService.findById(entidadPersonaId);
			}else if(entidadPersonaId != null){
				entidadPersonaParametros = entidadPersonacaService.findById(entidadPersonaId);
			}
			
			if(entidadPersonaParametros != null){
				
				CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.PORBAJASINIESTRALIDAD));
				
				if(new Long(ConstantesCompensacionesAdicionales.PORCUMPLIMIENTODEMETA).equals(idTipoCompensacion)){
					caTipoCompensacion = tipoCompensacioncaService.findById(idTipoCompensacion);
				}
				
				pg = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion, caCompensacion, entidadPersonaParametros);		
				if(pg != null){
					listRangoscas = rangoscaService.findAllbyParametrosGralesId(pg.getId());		
				}
			}
		}
		LOG.debug("<< verRangosSiniestralidad()");
		return SUCCESS;
	}
	
	public String verLineasNegocioContra() {
		return verLineasNegocio();
	}
	
	public String verLineasNegocio() {
		LOG.debug(">> verLineasNegocio()");
		lineasInicial = lineaNegociocaService.findByNegocioIdandContraprestacion(idNegocio,contraprestacionB);
		List<SeccionDTO> seccionDTOs;
		
		RAMO caRamo = RAMO.findByType(this.ramo);
		
		if(caRamo != null){			
						
			switch (caRamo) {
			
				case AUTOS:{
				
					if(idNegocio != null){						
						lineasInicial = lineaNegociocaService.findByNegocioIdandContraprestacion(idNegocio,contraprestacionB);										
						if(lineasInicial == null || lineasInicial.isEmpty()){							
							lineasInicial = new ArrayList<CaLineaNegocio>();			
							seccionDTOs = negocioSeccionService.listarSeccionNegocioByIdNegocio(new BigDecimal(idNegocio));		
							
							for(SeccionDTO dto : seccionDTOs){
								CaLineaNegocio lineaInicialAux = new CaLineaNegocio();				
								lineaInicialAux.setValorId(dto.getCodigo());
								lineaInicialAux.setValorPorcentaje("");
								lineaInicialAux.setValorDescripcion(dto.getDescripcion());
								lineasInicial.add(lineaInicialAux);
							}
						
						}else{
							
							HashMap<String,CaLineaNegocio> lineasActualesMapa = new HashMap<String, CaLineaNegocio>();							
							for(CaLineaNegocio objL : lineasInicial){
								lineasActualesMapa.put(objL.getValorId(), objL);
							}							
							seccionDTOs = negocioSeccionService.listarSeccionNegocioByIdNegocio(new BigDecimal(idNegocio));
							
							for(SeccionDTO objDto : seccionDTOs){
								if(!lineasActualesMapa.containsKey(objDto.getCodigo())){
									CaLineaNegocio lineaNueva = new CaLineaNegocio();
									lineaNueva.setValorId(objDto.getCodigo());
									lineaNueva.setValorDescripcion(objDto.getDescripcion());
									lineaNueva.setValorPorcentaje("");										
									lineasActualesMapa.put(lineaNueva.getValorId(), lineaNueva);
									lineasInicial.add(lineaNueva);					
								}
							}
							
							lineasActualesMapa = new HashMap<String, CaLineaNegocio>();
							for(CaLineaNegocio objL : lineasInicial){
								lineasActualesMapa.put(objL.getValorId(), objL);
							}
							
							List<CaLineaNegocio> lineasAuxiliar = new ArrayList<CaLineaNegocio>();			
							for(SeccionDTO objDto : seccionDTOs){
								if(lineasActualesMapa.containsKey(objDto.getCodigo()))
									lineasAuxiliar.add(lineasActualesMapa.get(objDto.getCodigo()));
							}
							
							if(!lineasAuxiliar.isEmpty()){
								lineasInicial = lineasAuxiliar;
							}
						}
					}
										 
					break;
				}

			}
		}
		LOG.debug("<< verLineasNegocio()");				
		return SUCCESS;
	}

	/**
	 * Metodo de Incializacion de Configuracion 
	 * de Compensaciones Adicionales
	 * @return
	 */
	public String init() {
		LOG.debug(">> init()");
		compensacionesDTO = new CompensacionesDTO();
		List<CaCompensacion> listCompensacion = null;
		CaCompensacion caCompensacion = null;		
		
		RAMO caRamo = RAMO.findByType(ramo != null ? ramo : "");
		
		switch (caRamo) {
		
			case AUTOS:{					
				 if (idNegocio != null && idNegocio > 0) {							
					compensacionesDTO.setIdNegocio(idNegocio);
					compensacionesDTO.setRamo(ramocaService.findById(caRamo.getValor()));
					Negocio negocio = negocioService.findByClave(idNegocio.longValue());
					if (negocio != null){
						compensacionesDTO.setNombreNegocio(negocio.getDescripcionNegocio());
					}	
					listCompensacion = caCompensacionService.listCompensaciones(caRamo.getValor(), ConstantesCompensacionesAdicionales.NEGOCIO_ID, this.idNegocio);
					compensacionNueva = 1;									
					if(!listCompensacion.isEmpty()){
						compensacionNueva = 0;						
						caCompensacion = this.getCompensacionActiva(listCompensacion);												
					}				
				}
				break;
			}
			
			case DANOS:{
				this.definirTipoAccion();
				if(this.idCotizacion != null && this.idCotizacion > 0){
					
					compensacionesDTO.setCotizacionId(this.idCotizacion);
					compensacionesDTO.setRamo(ramocaService.findById(caRamo.getValor()));
					listCompensacion = caCompensacionService.listCompensaciones(caRamo.getValor(), ConstantesCompensacionesAdicionales.COTIZACION_ID, this.idCotizacion);	
					compensacionNueva = 1;									
					if(!listCompensacion.isEmpty()){
						compensacionNueva = 0;
						caCompensacion = this.getCompensacionActiva(listCompensacion);
						if(caCompensacion!=null){
							listRecibos=caCompensacionService.getlistRecibosPendientesPagar(this.idPoliza);
							if(caCompensacion.getIdReciboModificacion()>0){
								compensacionesDTO.setIdRecibo(caCompensacion.getIdReciboModificacion());
							}
						}						
					}						
					this.procesarIdToPolizaByCotizacion();
				}
				break;
			}
			
			case VIDA:{
				if(this.idNegocioVida != null){
					compensacionesDTO.setIdNegocioVida(this.idNegocioVida);
					compensacionesDTO.setVidaIndiv(this.vidaIndiv);
					compensacionesDTO.setRamo(ramocaService.findById(caRamo.getValor()));
					listCompensacion = caCompensacionService.listCompensaciones(caRamo.getValor(), ConstantesCompensacionesAdicionales.NEGOCIOVIDA_ID, this.idNegocioVida);	
					compensacionNueva = 1;									
					if(!listCompensacion.isEmpty()){
						compensacionNueva = 0;
						caCompensacion = this.getCompensacionActiva(listCompensacion);
						topeMaximo=(caCompensacion.getTopeMaximo()!=null)?caCompensacion.getTopeMaximo():BigDecimal.ZERO;
						compensacionesDTO.setTopeMaximo(topeMaximo);
						compensacionesDTO.setFormaPagoBanca(caCompensacion.getFormaPagoBanca());
						compensacionesDTO.setAplicaComision(caCompensacion.getAplicaComision()!=null ? 1 : 0);
						compensacionesDTO.setFechaModificacionVida(caCompensacion.getFechaModificacionVida());
						this.negocioVidaService.fechaModificacionVida(listCompensacion, compensacionesDTO);
					}									
				}				
				break;
			}
			
			case BANCA:{
				if(this.idConfiguracionBanca != null){
					compensacionesDTO.setIdConfiguracionBanca(this.idConfiguracionBanca);
					compensacionesDTO.setRamo(ramocaService.findById(caRamo.getValor()));
					listCompensacion = caCompensacionService.listCompensaciones(caRamo.getValor(), ConstantesCompensacionesAdicionales.CONFIGURACIONBANCA_ID, this.idConfiguracionBanca);	
					compensacionNueva = 1;									
					if(!listCompensacion.isEmpty()){
						compensacionNueva = 0;
						caCompensacion = this.getCompensacionActiva(listCompensacion);			
					}									
				}				
				break;
			}
			
			default:
				if(idCompensacionActual != null && idCompensacionActual > 0){
					caCompensacion = caCompensacionService.findById(idCompensacionActual);
					compensacionNueva = 1;
					if(caCompensacion != null){
						compensacionNueva = 0;	
						if(caCompensacion.getContraprestacion()){
							tabActiva = ConstantesCompensacionesAdicionales.TAB_CONTRAPRESTACION;
						}else{
							tabActiva = ConstantesCompensacionesAdicionales.TAB_COMPENSACION;
						}
						compensacionesDTO = new CompensacionesDTO();
						compensacionesDTO.setRamo(ramocaService.findById(caCompensacion.getCaRamo().getId()));
						compensacionesDTO.setIdNegocio(caCompensacion.getNegocioId());
						compensacionesDTO.setCompensacionId(caCompensacion.getId());
						topeMaximo=(caCompensacion.getTopeMaximo()!=null)?caCompensacion.getTopeMaximo():BigDecimal.ZERO;
						compensacionesDTO.setTopeMaximo(topeMaximo);
						compensacionesDTO.setFormaPagoBanca(caCompensacion.getFormaPagoBanca());
						idNegocio = caCompensacion.getNegocioId();
						if(idNegocio != null){
							Negocio negocio = negocioService.findByClave(idNegocio.longValue());
							compensacionesDTO.setNombreNegocio(negocio.getDescripcionNegocio());
						}
						if(RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getId())){
							aplicaComision=caCompensacion.getAplicaComision() ? 1 : 0;
							compensacionesDTO.setAplicaComision(aplicaComision);
						}
						idNegocioVida = caCompensacion.getNegocioVidaId();
						idCotizacion  = caCompensacion.getCotizacionId() != null ? caCompensacion.getCotizacionId().longValue() : null;		
						idConfiguracionBanca = caCompensacion.getConfiguracionBancaId();
						
						/**
						 * Validaciones por ramo
						 */
						if(RAMO.DANOS.getValor().equals(caCompensacion.getCaRamo().getId())){
							tipoAccion = "2";
							compensacionesDTO.setCotizacionId(idCotizacion);
							this.procesarIdToPolizaByCotizacion();
							if(caCompensacion!=null){
								listRecibos=caCompensacionService.getlistRecibosPendientesPagar(this.idPoliza);
								if(caCompensacion.getIdReciboModificacion()>0){
					            	compensacionesDTO.setIdRecibo(caCompensacion.getIdReciboModificacion());
					        	}							
							}					
						}
					}
				}
				break;
		}	
		/**
		 * Permisos
		 */
		this.buscarPermisosAutorizaciones();
		LOG.debug("<< init()");				
		return SUCCESS;
	}
	
	/**
	 * 
	 * @param listCompensacion
	 * @return
	 */
	private CaCompensacion getCompensacionActiva(List<CaCompensacion> listCompensacion){
		
		CaCompensacion caCompensacion = null;		
		
		if(ConstantesCompensacionesAdicionales.TAB_COMPENSACION.equals(tabActiva) || ConstantesCompensacionesAdicionales.TAB_CONTRAPRESTACION.equals(tabActiva)){
			
			for(CaCompensacion ca : listCompensacion){
				if(ca.getContraprestacion() && ConstantesCompensacionesAdicionales.TAB_CONTRAPRESTACION.equals(tabActiva)){
					caCompensacion = ca;
					compensacionesDTO.setCompensacionId(caCompensacion.getId());
					break;
				}else if(ConstantesCompensacionesAdicionales.TAB_COMPENSACION.equals(tabActiva)){
					caCompensacion = ca;
					compensacionesDTO.setCompensacionId(caCompensacion.getId());
					break;
				}
			}
		
		}
		
		if(caCompensacion == null){
			
			for(CaCompensacion ca : listCompensacion){
				if(!ca.getContraprestacion()){
					caCompensacion = ca;
					tabActiva = ConstantesCompensacionesAdicionales.TAB_COMPENSACION;
					compensacionesDTO.setCompensacionId(caCompensacion.getId());
					break;
				}
			}
			
			if(caCompensacion == null){
				for(CaCompensacion ca : listCompensacion){
					if(ca.getContraprestacion()){
						caCompensacion = ca;
						tabActiva = ConstantesCompensacionesAdicionales.TAB_CONTRAPRESTACION;
						compensacionesDTO.setCompensacionId(caCompensacion.getId());
						break;
					}
				}
			}
			
		}
		
		return caCompensacion;
	}
	
	
	public void cargarListas(RAMO caRamo, boolean contraprestacion) {
		LOG.debug(">> cargarListas()");
		switch(caRamo){
		
		case AUTOS:{						
				this.listBaseCalculoca = baseCalculocaService.findByProperties(ConstantesCompensacionesAdicionales.PROPIEDADVALOR,ConstantesCompensacionesAdicionales.PRIMANETA,
						ConstantesCompensacionesAdicionales.PRIMANETARECARGO,ConstantesCompensacionesAdicionales.PRIMATOTAL);		
			break;
		}
		
		case DANOS:{
				this.listBaseCalculoca = baseCalculocaService.findAll();
			break;
		}
		
		case VIDA:{
				this.listBaseCalculoca = baseCalculocaService.findByProperties(ConstantesCompensacionesAdicionales.PROPIEDADVALOR,ConstantesCompensacionesAdicionales.PRIMANETA, ConstantesCompensacionesAdicionales.PRIMARIESGO);					
			break;
		}
		
		case BANCA:{
				this.listBaseCalculoca = baseCalculocaService.findAll();					
			break;
		}
		
	}
	
		this.listTipoMonedaca = monedacaService.findAll();
		this.listCondicionesCalccas = tipoCondicionesCalccaService.findAll();
		this.listTipoContratoca = tipoContratocaService.findAll();	
		this.listTipoProvisionca = tipoProvisioncaService.findAll();
		this.listTipoAutorizacionca = tipoAutorizacioncaService.findAll();
		this.listTipoAutorizacioncaByCompensacion = new ArrayList<CaTipoAutorizacion>();
		
		if(!contraprestacion){			
			Iterator<CaTipoAutorizacion>  iterator  = this.listTipoAutorizacionca.iterator();			
			while (iterator.hasNext()) {
				CaTipoAutorizacion tipoAutorizacion =  iterator.next();				
				if(tipoAutorizacion.getId().equals(ConstantesCompensacionesAdicionales.AUTORIZACION_JURIDICO)){
					iterator.remove();
				}			
			}
		}
			
	  LOG.debug("<< cargarListas()");
	}
	
	/**
	 * Metodo utilizado para la funcionalidad del 
	 * TAB Contraprestaciones
	 * @return
	 */
	public String verContraprestacionConf() {		
		LOG.debug(">> verContraprestacionConf()");
		RAMO caRamo = RAMO.findByType(this.ramo);		
		esContra = contraprestacionB ? 1 : 0;		
		cargarListas(caRamo, contraprestacionB);
		
		compensacionesDTO = compensacionesDTO == null ? new CompensacionesDTO() : compensacionesDTO;				
	
		switch(caRamo){
		
			case AUTOS:{				
				if (idNegocio != null) {
					compensacionesDTO.setIdNegocio(idNegocio);
					compensacionesDTO.setContraprestacion(true);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);					
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);	
					if(idPoliza != null){
						compensacionesDTO.setPolizaId(new BigDecimal(idPoliza));
					}
				}		
				break;
			}
			
			case DANOS: {				
				if (idCotizacion != null) {
					compensacionesDTO.setCotizacionId(idCotizacion);
					compensacionesDTO.setContraprestacion(true);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);									
				}									
				break;
			}
			
			case VIDA: {
				if (this.idNegocioVida != null){				
					compensacionesDTO.setIdNegocioVida(idNegocioVida);
					compensacionesDTO.setTopeMaximo(topeMaximo);
					compensacionesDTO.setAplicaComision(aplicaComision);
					compensacionesDTO.setFormaPagoBanca(formaPagoBanca);
					compensacionesDTO.setContraprestacion(true);
					compensacionesDTO.setVidaIndiv(this.vidaIndiv);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);
				}
				break;
			}
			
			case BANCA: {
				if (this.idConfiguracionBanca != null){				
					compensacionesDTO.setIdConfiguracionBanca(idConfiguracionBanca);
					compensacionesDTO.setContraprestacion(true);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);
				}
				break;
			}	
		}
		LOG.debug("<< verContraprestacionConf()");
		return SUCCESS;
	}
	
	/**
	 * Metodo utilizado para la funcionalidad del 
	 * TAB Compensaciones
	 * @return
	 */
	
	public String verCompensacionConf() {
		LOG.debug(">> verCompensacionConf()");
		
		RAMO caRamo = RAMO.findByType(this.ramo);			
		esContra = contraprestacionB ? 1 : 0;		
		cargarListas(caRamo, contraprestacionB);		
		
		compensacionesDTO = compensacionesDTO == null ? new CompensacionesDTO() : compensacionesDTO;
		
		switch(caRamo){
		
			case AUTOS:{				
				if (this.idNegocio != null){				
					compensacionesDTO.setIdNegocio(idNegocio);
					compensacionesDTO.setContraprestacion(false);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);		
				}		
				break;
			}
			
			case DANOS:{
				if (this.idCotizacion != null){				
					compensacionesDTO.setCotizacionId(idCotizacion);
					compensacionesDTO.setContraprestacion(false);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);							
				}										
				break;
			}
			
			case VIDA:{
				if (this.idNegocioVida != null){				
					compensacionesDTO.setIdNegocioVida(idNegocioVida);
					compensacionesDTO.setTopeMaximo(topeMaximo);
					compensacionesDTO.setVidaIndiv(this.vidaIndiv);
					compensacionesDTO.setContraprestacion(false);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO,caRamo);	
				}	
				break;
			}
			
			case BANCA: {
				if (this.idConfiguracionBanca != null){				
					compensacionesDTO.setIdConfiguracionBanca(idConfiguracionBanca);
					compensacionesDTO.setContraprestacion(false);
					compensacionesDTO.setCaEntidadPersona(caEntidadPersonaCargar);
					caCompensacionService.findConfiguracion(compensacionesDTO, caRamo);
				}
				break;
			}
			
		}		
		LOG.debug("<< verCompensacionConf()");
		return SUCCESS;
	}
	
	public String cargarProveedor(){
		LOG.debug(">> cargarProveedor()");
		
		caEntidadPersonaCargar = entidadPersonacaService.findById(entidadPersonaId); 		
		verContraprestacionConf();
		
		LOG.debug("<< cargarProveedor()");
		return SUCCESS;
	}
	
	public String cargarAgente(){
		LOG.debug(">> cargarAgente()");
		
		caEntidadPersonaCargar = entidadPersonacaService.findById(entidadPersonaId);
		verCompensacionConf();
		
		LOG.debug("<< cargarAgente()");
		return SUCCESS;
	}
	
	public String agregarAgente(){
	
		LOG.debug(">> agregarAgente()");
	
		String claveProveedor = idAgenteAgregar.trim();		

		if(compensacionesDTO == null){
			compensacionesDTO = new CompensacionesDTO();
		}
		
		if(contraprestacionB){			
			LOG.debug(" -- Buscando Proveedor ");			
			CaEntidadPersona entidadPersona = entidadPersonacaService.findByCompensacionAndTipoEntidadAndClave(idCompensacionActual, Long.valueOf(claveProveedor), ConstantesCompensacionesAdicionales.PROVEEDOR);

			if(entidadPersona == null){
				compensacionesDTO.setListEntidadPersonaca(new ArrayList<CaEntidadPersona>());
				List<Proveedor> listProveedores = catalogoCompensacionesService.listarProveedoresId(claveProveedor);
				
				try {
					if(listProveedores != null && !listProveedores.isEmpty()){	
						LOG.debug(" --Proveedor Encontrado ");
						compensacionesDTO.setClaveProveedorContra(claveProveedor);
						compensacionesDTO.setNombreProveedorContra(listProveedores.get(0).getNombre());	
					}
				}catch (Exception e) {
					LOG.error("debugrmaciÃ³n del Error", e);
				}
			}
		
		}else{
			LOG.debug(" -- Buscando Agente ");					
			CaEntidadPersona entidadPersona = null;
			try {
				if(RAMO.VIDA.getTipo().equals(this.ramo)){
					DatosSeycos agenteSeycosId = compensacionesDTO.getDatosSeycos();					
					if(agenteSeycosId != null){
						compensacionesDTO.setClaveAgenteGral(agenteSeycosId.getIdAgente());
						compensacionesDTO.setNombreAgenteGral(agenteSeycosId.getNombreCompleto());
						entidadPersona = entidadPersonacaService.findByCompensacionAndTipoEntidadAndClave(idCompensacionActual, agenteSeycosId, ConstantesCompensacionesAdicionales.AGENTE);
						if(entidadPersona == null){
							List<DatosSeycos> listDatosSeycos = negocioVidaService.findAgenteSeycosConPromotor(agenteSeycosId);					
							if(!listDatosSeycos.isEmpty()){
								LOG.debug(" --AgenteSeycosId Encontrado ");
								DatosSeycos agenteSeycos = listDatosSeycos.get(0);						
								compensacionesDTO.setListEntidadPersonaca(new ArrayList<CaEntidadPersona>());		
								if(agenteSeycos.getIdSupervisoria() != null && Long.valueOf(agenteSeycos.getIdSupervisoria().trim()) > 0){
									compensacionesDTO.setClavePromotorGral(agenteSeycos.getIdSupervisoria());
									compensacionesDTO.setNombrePromotorGral(agenteSeycos.getNombreSupervisoria());
								}else{
									compensacionesDTO.setClavePromotorGral("");
									compensacionesDTO.setNombrePromotorGral("");
								}
							}
						}
					}else{
						compensacionesDTO.getDatosSeycos().setIdAgente(null);
						compensacionesDTO.getDatosSeycos().setIdEmpresa(null);
						compensacionesDTO.getDatosSeycos().setFsit(null);
					}
				}else{			
					entidadPersona = entidadPersonacaService.findByCompensacionAndTipoEntidadAndClave(idCompensacionActual,Long.valueOf(claveProveedor), ConstantesCompensacionesAdicionales.AGENTE);
					if(entidadPersona == null){
						compensacionesDTO.setListEntidadPersonaca(new ArrayList<CaEntidadPersona>());						
						Agente agente = new Agente();
						agente.setIdAgente(Long.valueOf(claveProveedor));					
						agente = agenteMidasService.findByClaveAgente(agente);					
						if(agente != null){
							LOG.debug(" --Agente Encontrado ");
							compensacionesDTO.setClaveAgenteGral(agente.getIdAgente().toString());
							String nombre = new StringBuilder(agente.getPersona().getNombre()).append(" ").append(agente.getPersona().getApellidoPaterno()).append(" ").append(agente.getPersona().getApellidoMaterno()).toString();
							compensacionesDTO.setNombreAgenteGral(nombre);
							Persona pReponsable = agente.getPromotoria().getPersonaResponsable();
							if(pReponsable != null && pReponsable.getIdPersona() != null){
								compensacionesDTO.setClavePromotorGral(agente.getPromotoria().getId().toString());
								compensacionesDTO.setNombrePromotorGral(agente.getPromotoria().getDescripcion());
							}
						}
					}
				}
			} catch (Exception e) {
				LOG.error("InformaciÃ³n del Error", e);
			}
		}
		
		RAMO caRamo = RAMO.findByType(this.ramo);
		CaRamo ramoca = ramocaService.findById(caRamo.getValor());
		compensacionesDTO.setRamo(ramoca);		
		compensacionesDTO.setVidaIndiv(this.vidaIndiv);
		this.cargarListas(caRamo, contraprestacionB);
		
		LOG.debug("<< agregarAgente()");
		return SUCCESS;		
	}

	/**
	 * Guardado de las Compensaciones Adicionales
	 * Compensacion y Contrapresacion
	 * @return
	 */
	public String guardarCompensacion() {
		LOG.debug(">> guardarCompensacion()");
		try{
			caCompensacionService.guardarCompensacion(this.getListCompensacionesDTO(dataGson));	
			}catch (Exception e) {
			LOG.error("debugrmaciÃ³n del Error", e);
		}		
		LOG.debug("<< guardarCompensacion()");
		return null;
	}
	
	public String autorizarCompensacion(){		
		LOG.debug(">> autorizarCompensacion()");
		Long id = null;
		boolean permisos = false;
		
		RAMO caRamo = RAMO.findByType(this.ramo);
	
		switch (caRamo) {
			
			case AUTOS:
				 id = this.idNegocio;
				break;
		
			case DANOS:
				id = this.idCotizacion;
				break;
				
			case VIDA: 
				id = this.idNegocioVida;
				break;
			
			case BANCA: 
				id = this.idConfiguracionBanca;
				break;
		}		
		
		this.buscarPermisosAutorizaciones();
		
		/**
		 * Valida los Permisos del Usuario,
		 * dependiendo de si es contraprestacion.
		 */
		permisos = contraprestacionB ? compensacionesDTO.isFnTecnico() || compensacionesDTO.isFnAgente() || compensacionesDTO.isFnJuridico() : compensacionesDTO.isFnTecnico() || compensacionesDTO.isFnAgente();
					
		if(permisos){
			try{
				caCompensacionService.autorizarCompensacion(this.ramo, compensacionesDTO.getCompensacionId(), id, compensacionesDTO.getTipoAutorizacionca().getId(), contraprestacionB);			
			}catch(Exception ex){
				LOG.error(ex.getMessage());
			}
		}
		LOG.debug("<< autorizarCompensacion()");
		return null;
	}
	
	
	public String verHistorialCompensacion(){
		LOG.debug(">> verHistorialCompensacion()");
		LOG.debug("<< verHistorialCompensacion()");
		return SUCCESS;
	}
	
	public String listarFiltradoDetalle(){
		LOG.debug(">> listarFiltradoDetalle()");
		try {		
			listcaBitacora = bitacoraService.obtenerBitacoraPorCompensacionId(compensacionesDTO.getCompensacionId());
		} catch (RuntimeException re) {
			LOG.error("InformaciÃ³n del Error", re);
		}	
		LOG.debug("<< listarFiltradoDetalle()");
		return SUCCESS;
	}
	
	/**
	 * @param gsonList
	 * @return
	 */
	private List<CompensacionesDTO> getListCompensacionesDTO(String gsonList){
		LOG.debug(">> getListCompensacionesDTO()");
		List<CompensacionesDTO> list;		
		try{
			Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
			list = gson.fromJson(gsonList, new TypeToken<List<CompensacionesDTO>>(){}.getType());
		}catch(Exception ex){			
			list = new ArrayList<CompensacionesDTO>();
			LOG.error("InformaciÃ³n del Error", ex);
		}		
		LOG.debug("<< getListCompensacionesDTO()");
		return list;
	}
	
	/**
	 * Provisiona una Compensacion
	 * @return
	 */
	public String provisionar(){
		LOG.debug(">> provisionar()");
		
		int result = 0;
		try{
			procesarIdToPolizaByCotizacion();
			result = caCompensacionService.provisionarPorUtilidad(idPoliza, entidadPersonaId, compensacionesDTO.getMontoUtilidadesUtilidad(),compensacionesDTO.getMontoDeLasUtilidades());
			switch (result) {
			
			   case 0:  				   
				   super.setMensaje("Se ha provisionado correctamente");				
				   break;
               
			    case 1:			    	
			    	super.setMensaje("Ocurrio un error al provisionar");			    	
			    	break;
				
                case 2:
                    super.setMensaje("Monto de utilidad excedido");
                    break;
                    
                case 3:
                    super.setMensaje("Monto de utilidad menor a monto enviado a pago");                    
	                break;

                case 4:
                    super.setMensaje("Monto de utilidad provisionado con anterioridad");                    
	                break;	                
			}
			
		}catch (Exception e) {
			LOG.error("debugrmaciÃ³n del Error", e);
			super.setMensaje("Ocurrio un error al provisionar");
		}
		
		LOG.debug("<< provisionar()");
		return SUCCESS;
	}
	
	/**
	 * Ver Listado de Ramos y SubRamos de Danios
	 * @return
	 */	
	public String verSubRamosDanios(){
		LOG.debug(">> verSubRamosDanios()");
		try{
			SubRamosDaniosViewDTO subRamosDaniosViewDTO = new SubRamosDaniosViewDTO();	
			subRamosDaniosViewDTO.setIdToCotizacion(new BigDecimal(idCotizacion));
			subRamosDaniosViewDTO.setIdCompensacion(new BigDecimal(idCompensacionActual));
			subRamosDaniosViewDTO.setIdEntidadPer(new BigDecimal(entidadPersonaId));
			subRamosDaniosViewDTO.setIdCotizacionEndoso(new BigDecimal(idCotizacionEndoso));
			subRamosDaniosViewDTO.setIdRecibo(idRecibo);
			this.compensacionesDTO = new CompensacionesDTO();
			caSubramosDaniosService.findSubRamos(subRamosDaniosViewDTO,compensacionesDTO);
		}catch(Exception e){
			LOG.error("InformaciÃ³n del Error", e);
		}
		LOG.debug("<< verSubRamosDanios()");
		return SUCCESS;
	}
	
	public String calcularProvisionDanios(){
         LOG.debug(">> calcularProvisionDanios()");		
		try{			
			/**
			 * Al ejecutar los calculos se tiene que guardar primero los nuevos registros de subramos
			 */
			CaEntidadPersona caEntidadPersona = this.entidadPersonacaService.findById(entidadPersonaId);
			
			if(caEntidadPersona != null){
				
				CaTipoCompensacion caTipoCompensacion = tipoCompensacioncaService.findById(new Long(ConstantesCompensacionesAdicionales.PORPRIMA));				
				CaParametros caParametros = parametrosGralescaService.findByTipocompeidCompeIdPersoId(caTipoCompensacion, new CaCompensacion(idCompensacionActual), caEntidadPersona);				
				caSubramosDaniosService.guardarListadoSubRamosDanios(compensacionesDTO.getListSubRamosDaniosView(), idCotizacion, idCotizacionEndoso, caParametros);				
				/**
				 * Se envia a calcular la provision
				 */
				this.enviarAProvision();
			}		
			
		}catch(Exception e){
			LOG.error("InformaciÃ³n del Error", e);
		}
		LOG.debug("<< calcularProvisionDanios()");
		return SUCCESS;
	}
	
	private void enviarAProvision(){
		SubRamosDaniosViewDTO subRamosDaniosViewDTO = new SubRamosDaniosViewDTO();
	   	subRamosDaniosViewDTO.setIdToCotizacion(new BigDecimal(idCotizacion));	
    	subRamosDaniosViewDTO.setIdCotizacionEndoso(new BigDecimal(idCotizacionEndoso));	
		subRamosDaniosViewDTO.setIdToPoliza(new BigDecimal(idPoliza));
		subRamosDaniosViewDTO.setIdEntidadPer(new BigDecimal(entidadPersonaId));
	    subRamosDaniosViewDTO.setIdCompensacion(new BigDecimal(idCompensacionActual));		    
	    subRamosDaniosViewDTO.setIdRecibo(idRecibo);
		compensacionesDTO = new CompensacionesDTO();
		caSubramosDaniosService.calcularProvisionDanios(subRamosDaniosViewDTO, compensacionesDTO);
    	}
	
	public String solicitarPagoUtilidad(){
		LOG.debug(">> solicitarPagoUtilidad()");
		boolean result = true;
		try{
			
			result = caCompensacionService.solicitarPagoPorUtilidad(idPoliza, entidadPersonaId, compensacionesDTO.getMontoDeLasUtilidades());
			
			if(result){
				super.setMensaje("Se ha generado la orden de pago");
			}else{
				super.setMensaje("Ocurrio un error al generar la orden de pago");
			}	
			
		}catch (Exception e) {
			LOG.error("InformaciÃ³n del Error", e);
			super.setMensaje("Ocurrio un error al generar la orden de pago");
		}
		
		LOG.debug("<< solicitarPagoUtilidad()");
		return SUCCESS;
	}
	
	public String consultarPagoUtilidad(){		
		LOG.debug(">> consultarPagoUtilidad()");
		try{			
			BigDecimal pago = ordenPagosDetalleService.consultarPagoPorUtilidad(idCompensacionActual, entidadPersonaId);			
			this.dataStream = new ByteArrayInputStream(pago.toString().getBytes());
		}catch(Exception e){
			LOG.error("InformaciÃ³n del Error", e);
		}		
		LOG.debug("<< consultarPagoUtilidad()");		
		return SUCCESS;
	}
	
	public String consultarProvision(){		
		LOG.debug(">> consultarProvision()");		
		try{
			BigDecimal provision = ordenPagosDetalleService.consultarProvisionPorUtilidad(idCompensacionActual, entidadPersonaId);			
			this.dataStream = new ByteArrayInputStream(provision.toString().getBytes());
		}catch(Exception e){
			LOG.error("InformaciÃ³n del Error", e);			
		}
		LOG.debug("<< consultarProvision()");		
		return SUCCESS;
	}
	
	private void buscarPermisosAutorizaciones(){
		LOG.debug(">> buscarPermisosAutorizaciones()");
		compensacionesDTO.setFnTecnico(usuarioService.tienePermisoUsuarioActual(ConstantesCompensacionesAdicionales.FN_AUTORIZA_TECNICO));
		compensacionesDTO.setFnAgente(usuarioService.tienePermisoUsuarioActual(ConstantesCompensacionesAdicionales.FN_AUTORIZA_AGENTE));
		compensacionesDTO.setFnJuridico(usuarioService.tienePermisoUsuarioActual(ConstantesCompensacionesAdicionales.FN_AUTORIZA_JURIDICO));
		LOG.debug("<< buscarPermisosAutorizaciones()");
	}
	
	public String cargarPresupuestoAnual(){
		LOG.debug(">> cargarPresupuestoAnual()");
		try{
			if(this.fileUpload != null && this.idConfiguracionBanca != null){
				boolean result = this.caCompensacionService.cargarExcel(this.fileUpload, this.idConfiguracionBanca);
				if (result==true){
					super.setMensaje("Se ha cargado el Archivo");
				}
				else{
					super.setMensaje("No se ha podido cargar el Archivo");
				}
			}else{
				super.setMensaje("Ocurrio un error no se ha podido cargar el archivo");
			}
		}catch (Exception e) {
			LOG.error("InformaciÃ³n del Error", e);
			super.setMensaje("Ocurrio un error no se ha podido cargar el archivo");			
		}finally{
			if(this.fileUpload != null){
				this.fileUpload.deleteOnExit();
			}
			LOG.debug("File:"+this.fileUpload);
		}
		LOG.debug("<< cargarPresupuestoAnual()");
		return SUCCESS;
	}
		
	private void definirTipoAccion(){		
	/**
	 * 
	 * 1.- No se puede modificar 
	 * (No se modifica cuando la poliza ya se halla emitido desde Midas I, solo podran autorizar)
	 * 
	 * 2.- Se permite modificar 
	 * (Es modificable por completo cuando este en cotizacion o en edicion desde Midas II cuando la poliza ya este emitida)
	 * 
	 * 3.- Se permite modifcar solo subRamos 
	 * (Se modifican los subramos cuando halla un endoso y se crearan nuevos registros con el numero de endoso)
	 * 
	 */		
		if(this.tipoAccion == null){
			
			boolean endoso0 = this.esEndoso0(this.idCotizacion);
			
			if(!endoso0 && idCotizacion != null)
				this.idCotizacionEndoso = idCotizacion; 
			
			if(endoso0 && this.idCotizacion != null && this.idCotizacion > 0 && ( this.idPoliza == null || this.idPoliza.equals(0L) ) ){
				this.tipoAccion = "2";
			}else if(!endoso0 || (this.idCotizacionEndoso != null && this.idCotizacionEndoso > 0)){
				this.tipoAccion = "3";
				this.idCotizacion = this.caSubramosDaniosService.findCotizacionForIdCotizacionEndoso(idCotizacionEndoso);
			}else if(this.idPoliza != null && this.idPoliza > 0){
				this.tipoAccion = "1";
			}
		}		
	}
	
	private void procesarIdToPolizaByCotizacion() {
		PolizaDTO poliza = new PolizaDTO();
		try {
			this.compensacionesDTO.setPolizaId(this.idPoliza != null ? new BigDecimal(this.idPoliza) : null);
			if(this.idCotizacion != null || this.idCotizacion > 0){
				poliza = entidadService.findByProperty(PolizaDTO.class,"cotizacionDTO.idToCotizacion", this.idCotizacion).get(0);		
				LOG.debug("getIdToPolizaByCotizacion() estatus de cotizacion {"+poliza.getCotizacionDTO().getClaveEstatus()+"}");
				this.setIdPoliza(poliza.getIdToPoliza().longValue());
				this.compensacionesDTO.setPolizaId(poliza.getIdToPoliza());
			}
		} catch (Exception e) {
			LOG.error("getIdToPolizaByCotizacion()", e);
		}
	}
	public String verProductosVida() {
		this.productosVida = caCompensacionService.getProductosVida();				
		return SUCCESS;
	}
	
	/**
	 * M&eacute;todo que evalua si la cotizaci&oacute;n es endoso 0
	 * o en su defecto es un endoso consecutivo
	 * 
	 * @param idCotizacion Identificador de TO cotizacion
	 * 
	 * @return [true] es endoso 0 | [false] es endoso consecutivo
	 */
	private boolean esEndoso0(Long idCotizacion){
		return caSubramosDaniosService.findCotizacionForIdCotizacionEndoso(idCotizacion) == null;
	}
	
	public List<CaBaseCalculo> getListBaseCalculoca() {
		return listBaseCalculoca;
	}

	public void setListBaseCalculoca(List<CaBaseCalculo> listBaseCalculoca) {
		this.listBaseCalculoca = listBaseCalculoca;
	}

	public List<CaTipoMoneda> getListTipoMonedaca() {
		return listTipoMonedaca;
	}

	public void setListTipoMonedaca(List<CaTipoMoneda> listTipoMonedaca) {
		this.listTipoMonedaca = listTipoMonedaca;
	}

	public List<CaTipoContrato> getListTipoContratoca() {
		return listTipoContratoca;
	}

	public String listarRamos() {
		return SUCCESS;
	}

	public CompensacionesDTO getCompensacionesDTO() {
		return compensacionesDTO;
	}

	public void setCompensacionesDTO(CompensacionesDTO compensacionesDTO) {
		this.compensacionesDTO = compensacionesDTO;
	}

	public List<CaTipoProvision> getListTipoProvisionca() {
		return listTipoProvisionca;
	}

	public void setListTipoProvisionca(List<CaTipoProvision> listTipoProvisionca) {
		this.listTipoProvisionca = listTipoProvisionca;
	}

	public List<CaTipoAutorizacion> getListTipoAutorizacionca() {
		return listTipoAutorizacionca;
	}

	public void setListTipoAutorizacionca(List<CaTipoAutorizacion> listTipoAutorizacionca) {
		this.listTipoAutorizacionca = listTipoAutorizacionca;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public List<CaTipoCondicionCalculo> getListCondicionesCalccas() {
		return listCondicionesCalccas;
	}
	
	public List<CaLineaNegocio> getLineasInicial() {
		return lineasInicial;
	}

	public void setLineasInicial(List<CaLineaNegocio> lineasInicial) {
		this.lineasInicial = lineasInicial;
	}
	
	public List<CaRangos> getListRangoscas() {
		return listRangoscas;
	}
	
	public void setListRangoscas(List<CaRangos> listRangoscas) {
		this.listRangoscas = listRangoscas;
	}
	public List<CaTipoRango> getListTipoRangocas() {
		return listTipoRangocas;
	}
	public void setListTipoRangocas(List<CaTipoRango> listTipoRangocas) {
		this.listTipoRangocas = listTipoRangocas;
	}
	public boolean isContraprestacionB() {
		return contraprestacionB;
	}
	public void setContraprestacionB(boolean contraprestacionB) {
		this.contraprestacionB = contraprestacionB;
	}
	public int getEsContra() {
		return esContra;
	}
	public void setEsContra(int esContra) {
		this.esContra = esContra;
	}
	public void setIdAgenteAgregar(String idAgenteAgregar){
		this.idAgenteAgregar = idAgenteAgregar;
	}
	public String getIdAgenteAgregar(){
		return idAgenteAgregar;		
	}
	public Long getIdCompensacionActual() {
		return idCompensacionActual;
	}
	public void setIdCompensacionActual(Long idCompensacionActual) {
		this.idCompensacionActual = idCompensacionActual;
	}
	public CaEntidadPersona getCaEntidadPersonaCargar() {
		return caEntidadPersonaCargar;
	}
	public void setCaEntidadPersonaCargar(CaEntidadPersona caEntidadPersonaCargar) {
		this.caEntidadPersonaCargar = caEntidadPersonaCargar;
	}
	public String getIdProveedorCargar() {
		return idProveedorCargar;
	}
	public void setIdProveedorCargar(String idProveedorCargar) {
		this.idProveedorCargar = idProveedorCargar;
	}
	public List<CaTipoAutorizacion> getListTipoAutorizacioncaByCompensacion() {
		return listTipoAutorizacioncaByCompensacion;
	}
	public void setListTipoAutorizacioncaByCompensacion(List<CaTipoAutorizacion> listTipoAutorizacioncaByCompensacion) {
		this.listTipoAutorizacioncaByCompensacion = listTipoAutorizacioncaByCompensacion;
	}
	public int getCompensacionNueva() {
		return compensacionNueva;
	}
	public void setCompensacionNueva(int compensacionNueva) {
		this.compensacionNueva = compensacionNueva;
	}
	public Long getEntidadPersonaId() {
		return entidadPersonaId;
	}
	public void setEntidadPersonaId(Long entidadPersonaId) {
		this.entidadPersonaId = entidadPersonaId;
	}
	public void setIdCotizacion(Long idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public Long getIdCotizacion() {
		return idCotizacion;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getRamo() {
		return ramo;
	}

	public void setDataGson(String dataGson) {
		this.dataGson = dataGson;
	}

	public String getDataGson() {
		return dataGson;
	}

	public void setIdPoliza(Long idPoliza) {
		this.idPoliza = idPoliza;
	}

	public Long getIdPoliza() {
		return idPoliza;
	}

	public CaBitacoraService getBitacoraService() {
		return bitacoraService;
	}

	public void setBitacoraService(CaBitacoraService bitacoraService) {
		this.bitacoraService = bitacoraService;
	}

	public List<CaBitacora> getListcaBitacora() {
		return listcaBitacora;
	}

	public void setListcaBitacora(List<CaBitacora> listcaBitacora) {
		this.listcaBitacora = listcaBitacora;
	}

	public void setDataStream(InputStream dataStream) {
		this.dataStream = dataStream;
	}

	public InputStream getDataStream() {
		return dataStream;
	}

	public void setIdNegocioVida(Long idNegocioVida) {
		this.idNegocioVida = idNegocioVida;
	}

	public Long getIdNegocioVida() {
		return idNegocioVida;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setIdConfiguracionBanca(Long idConfiguracionBanca) {
		this.idConfiguracionBanca = idConfiguracionBanca;
	}
	
	public void setIdConfiguracionBanca(String idConfiguracionBanca) {
		this.idConfiguracionBanca = Long.valueOf(idConfiguracionBanca);
	}

	public Long getIdConfiguracionBanca() {
		return idConfiguracionBanca;
	}

	public void setIdTipoCompensacion(Long idTipoCompensacion) {
		this.idTipoCompensacion = idTipoCompensacion;
	}

	public Long getIdTipoCompensacion() {
		return idTipoCompensacion;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setInvocacionConfigurador(String invocacionConfigurador) {
		this.invocacionConfigurador = invocacionConfigurador;
	}

	public String getInvocacionConfigurador() {
		return invocacionConfigurador;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public void setIdCotizacionEndoso(Long idCotizacionEndoso) {
		this.idCotizacionEndoso = idCotizacionEndoso;
	}

	public Long getIdCotizacionEndoso() {
		return idCotizacionEndoso;
	}
	
	public String getVidaIndiv() {
		return vidaIndiv;
	}
	
	public void setVidaIndiv(String vidaIndiv) {
		this.vidaIndiv = vidaIndiv;
	}	
	
	public List<CompensacionesDTO> getListRecibos() {
		return listRecibos;
	}

	public void setListRecibos(List<CompensacionesDTO> listRecibos) {
		this.listRecibos = listRecibos;
	}
	public int getIdRecibo() {
		return this.idRecibo;
	}

	public void setIdRecibo(int idRecibo) {
		this.idRecibo = idRecibo;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public String getFormaPagoBanca() {
		return formaPagoBanca;
	}

	public void setFormaPagoBanca(String formaPagoBanca) {
		this.formaPagoBanca = formaPagoBanca;
	}
	
	public int getAplicaComision() {
		return aplicaComision;
	}

	public void setAplicaComision(int aplicaComision) {
		this.aplicaComision = aplicaComision;
	}
	
	public List<ProductoVidaDTO> getProductosVida() {
		return productosVida;
	}

	public void setProductosVida(List<ProductoVidaDTO>  productosVida) {
		this.productosVida = productosVida;
	}
	
}
