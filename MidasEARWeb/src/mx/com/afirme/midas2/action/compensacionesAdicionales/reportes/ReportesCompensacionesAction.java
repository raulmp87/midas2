package mx.com.afirme.midas2.action.compensacionesAdicionales.reportes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO.DatosReporteCompParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO.DatosReporteEstadoCuentaParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO.DatosRepDerPolParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO.DatosReportePagDevDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra.DatosReportePagoCompContraParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.ReporteFiltrosDTO;
import mx.com.afirme.midas2.dto.compensaciones.FiltroCompensacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.compensaciones.CatalogoCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.LiquidacionCompensacionesService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("prototype")
@Namespace("/compensacionesAdicionales/reportes")
public class ReportesCompensacionesAction extends CatalogoHistoricoAction{	
	
	private static final long serialVersionUID = 768012833222881479L;
	
	//JSP's Reportes 
	//private static final String LOCATION_MOSTRAR__CONTENEDOR_R_JSP = "/jsp/compensacionesAdicionales/ordenPagos/reportes/contenedorReportes.jsp";
	private static final String LOCATION_MOSTRAR_REPORTES_JSP = "/jsp/compensacionesAdicionales/reportes/reporteOrdenPagos/mostrarReportes.jsp";
	//private static final String LOCATION_MOSTRAR_REPORTES_JSP = "/jsp/compensacionesAdicionales/reportes/reporteOrdenPagos/mostrarReporteDerechoPoliza.jsp";
	///variables Reporte Compensaciones y reporte Derecho Poliza	////
	private Short tipoReporte;
	private Short tipoArchivo;
	private String ramo;
	private Long agenteDev;
	private Long promotorDev;
	private Long gerenciaDev;
	private Long proveedorDev;
	private String agente;
	private String promotor;
	private String proveedor;
	private String gerencia;
	private Long idNegocio;
	private String nombreNegocio;
	private Date fechaInicio;
	private Date fechaFin;
	private String contratante;
	private Long negocio;
	private String noPoliza;
	private String resumenDetalle;
	private Long anioMes;
	private Long anio;
	private Long mes;
	private String nomAgente;
	private String nomPromotor;
	private String nomProveedor;
	private Long cveAgente;
	private Long cvePromotor;
	private Long cveProveedor;
	private Long claveNombreAgente;
	private Long claveNombreProveedor;
	private Long claveNombreGerencia;
	private Long claveNombrePromotor;
	private Date fechaInicial;
	private Date fechaFinal;
	private String ramoo;
	private Long poliza;
    private Long cotizacion;
    private Long idCompensacion;
    private Long name_reporte;
    private Long REP_PAGADASXDEVENGAR = 1L;
	private CatalogoCompensacionesService catalogoCompensacionesService;
	private FiltroCompensacion filtroCompensacion;
	
	@Autowired
	private LiquidacionCompensacionesService liquidacionCompensacionesService;
	private GenerarPlantillaReporte generarPlantillaReporte;
	//private static CaReportesDTO caReportesDTO;
	
	//---------------------------------------------------//
		
	private List<CaRamo> ramoLista;
	///generar Excel///
	private InputStream reportesCaInputStream;
	private InputStream reportesEcInputStream;
	private InputStream reportesPCCInputStream;
	private String contentType;
	private String fileName;	

	@Action(value = "mostrarReportes", results = {
		      @Result(name = SUCCESS, location = LOCATION_MOSTRAR_REPORTES_JSP),
		      @Result(name = INPUT, location = LOCATION_MOSTRAR_REPORTES_JSP) })
		  public String mostrarReportes() {
		  ramoLista = catalogoCompensacionesService.listarRamos();
		    return SUCCESS;
		  }
	
	
	@Action(value ="mostrarReportePagadasDevengarCa", results ={
			@Result(name= SUCCESS, location="/jsp/compensacionesAdicionales/reportes/reportePagadasDevengar/mostrarReportePagadasDevengar.jsp")
	})
	public String mostrarReportePagadasDevengarCa(){
		if (filtroCompensacion == null){
			filtroCompensacion = new FiltroCompensacion();
		}
		ramoLista = catalogoCompensacionesService.listarRamos();	
		
		return SUCCESS;
	}
	
	
	@Action(value ="mostrarReporteCompensacionesAdicionales", results ={
			@Result(name= SUCCESS, location="/jsp/compensacionesAdicionales/reportes/reporteCompensaciones/mostrarReporteCompensacionesAdicionales.jsp")
	})
	public String mostrarReporteCompensacionesAdicionales(){
		if (filtroCompensacion == null){
			filtroCompensacion = new FiltroCompensacion();
		}
		
		ramoLista = catalogoCompensacionesService.listarRamos();	
		
		return SUCCESS;
	}
	@Action(value ="generarReporteCompensaciones", results ={
			@Result(name= SUCCESS,type="stream" , params={ 
					"contentType","${contentType}",
					"inputName","reportesCaInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name= INPUT, location="/jsp/error.jsp")
	})
	public String generarReporteCompensaciones(){					
			//se crean los parametros de entrada para el reporte.
			DatosReporteCompParametrosDTO parametros = new DatosReporteCompParametrosDTO(tipoReporte, agente, promotor, proveedor, gerencia, ramo, idNegocio, nombreNegocio, fechaInicio, fechaFinal);
			TransporteImpresionDTO transporteImpresionDTO =  impresionesService.imprimirReportesCompensacionesAdicionales(parametros, getLocale());
			
			reportesCaInputStream= new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType = "application/xls";
			fileName = "Reporte de Compensaciones Adicionales.xls";	
	
			return SUCCESS;
		}
	
	@Action(value ="mostrarReporteDerechoPoliza", results ={
			@Result(name= SUCCESS, location="/jsp/compensacionesAdicionales/reportes/reporteCompensacionDerPol/mostrarReporteDerechoPoliza.jsp")
	})
	public String mostrarReporteDerechoPoliza(){
		//ramo = obtenerRamo(ramo);
		if (filtroCompensacion == null){
			filtroCompensacion = new FiltroCompensacion();
		}
		ramoLista = catalogoCompensacionesService.listarRamos();		
		return SUCCESS;
	}	
	/**/
	@Action(value="generarReporteEstadoCuenta", results = {
			@Result(name=SUCCESS, type="stream", params={
					"contentType","${contentType}",
					"inputName","reportesEcInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name=INPUT, location="/jsp/error.jsp")
	})

		public String generarReporteEstadoCuenta(){
		
		
			DatosReporteEstadoCuentaParametrosDTO parametros = new DatosReporteEstadoCuentaParametrosDTO(anioMes, cveAgente, cvePromotor, cveProveedor);
			//DatosReporteEstadoCuentaParametrosDTO parametros = new DatosReporteEstadoCuentaParametrosDTO(new Long(201604), new Long(90358), null, null);

			TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirReportesEstadoCuenta(parametros,getLocale());
			
			reportesEcInputStream = new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType="application/pdf";
			fileName="Reporte Estado de Cuenta.pdf";

			return SUCCESS;	
		}
	/**/

	@Action(value="generarReportePagoCompContra", results = {
			@Result(name=SUCCESS, type="stream", params={
					"contentType","${contentType}",
					"inputName","reportesPCCInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name=INPUT, location="/jsp/error.jsp")
	})

		public String generarReportePagoCompContra(){
		DatosReportePagoCompContraParametrosDTO parametros = new DatosReportePagoCompContraParametrosDTO(fechaInicio,fechaFinal, claveNombreAgente, claveNombrePromotor, claveNombreProveedor, claveNombreGerencia, ramo, poliza);
			//new DatosReportePagoCompContraParametrosDTO(fechaInicio,fechaFinal, claveNombreAgente, claveNombrePromotor, claveNombreProveedor, claveNombreGerencia, ramo, poliza);
		LOG.info( "FECHAS PRUEBA ---> "+ fechaInicio + " <->" + fechaFinal);
		TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirReportesPagoCompContra(parametros,getLocale());  
			/*
			 * fechaInicio
			 * fechaFinal
			 * */
			
			reportesPCCInputStream = new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType="application/xls";			
		
			if(name_reporte == REP_PAGADASXDEVENGAR){				
				fileName="reporte compensaciones pagadas y por devengar_"+UtileriasWeb.getFechaString(new Date())+".xls";				
			}else{				
				fileName="reporte_pago_compensaciones_contraprestaciones_"+UtileriasWeb.getFechaString(new Date())+".xls";				
			}


			return SUCCESS;	
		}
	
	@Action(value ="generarReporteDerechoPoliza", results ={
		      @Result(name= SUCCESS,type="stream" , params={ 
		          "contentType","${contentType}",
		          "inputName","reportesCaInputStream",
		          "contentDisposition","attachment;filename=\"${fileName}\""
		      }),
		      @Result(name= INPUT, location="/jsp/error.jsp")
		  })
		  
		  public String generarReporteDerechoPoliza(){
		    LOG.info("ENTRANDO generarReporteDerechoPoliza() ");
		    //se crean los parametros de entrada para el reporte.
		    DatosRepDerPolParametrosDTO parametros = new DatosRepDerPolParametrosDTO(tipoReporte, agente, promotor, ramo, idNegocio, nombreNegocio, fechaInicio, fechaFin);//this.getDatosReporteCompParametrosDTO();
		    TransporteImpresionDTO transporteImpresionDTO =  impresionesService.imprimirReportesDerechoPoliza(parametros, getLocale());
		    LOG.info("Antes de Generar el Array de Bytes");
		    reportesCaInputStream= new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
		    
		    contentType = "application/xls";
		    LOG.info("Termina de Generar el Array de Bytes");
		    fileName = "Reporte Compensaciones por Derecho de Poliza.xls";  
		    
		    return SUCCESS;
		  }
	
		@Action(value ="generarReportePagadasDevengar", results ={
			@Result(name= SUCCESS,type="stream" , params={ 
					"contentType","${contentType}",
					"inputName","reportesCaInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name= INPUT, location="/jsp/error.jsp")
	})
		
		public String generarReportePagadasDevengar(){	
			//ramo = obtenerRamo(ramo);
			DatosReportePagDevDTO parametros = new DatosReportePagDevDTO(tipoReporte, tipoArchivo, ramo, agenteDev, promotorDev, proveedorDev, gerenciaDev, idNegocio, nombreNegocio, anioMes, noPoliza );
			TransporteImpresionDTO transporteImpresionDTO =  impresionesService.imprimirReportesPagadasDevengar(parametros, getLocale());
			
			reportesCaInputStream= new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType = "application/xls";
			fileName = "Reporte Compensaciones Pagadas y por Devengar.xls";	
			
			return SUCCESS;
		}
		
		public String obtenerRamo(String Ramo) {
			String resultRamo = "";
			if(ramo.equals('1')){
				resultRamo = "A";
			}else if(ramo.equals('2')){
				resultRamo = "D";
			}else if(ramo.equals('3')){
				resultRamo = "V";
			}else if(ramo.equals('4')){
				resultRamo = "B";
	 }
			return resultRamo;
	}
		
		

		@Action(value ="mostrarReporteBancaSeguros", results ={
				@Result(name= SUCCESS, location="/jsp/compensacionesAdicionales/reportes/reporteBancaSeguros/mostrarBancaSeguros.jsp")
		})
		
		public String mostrarReporteBancaSeguros(){
			return SUCCESS;
		}
		
		@Action(value ="generarReporteBancaSeguros", results={
				@Result(name=SUCCESS, type="stream", params={
						"contentType","${contentType}",
						"inputName", "reportesCaInputStream",
						"contentDisposition","attachment; filename=\"${fileName}\""
				}),
				@Result(name=INPUT, location="/jsp/error.jsp")
		})
		
		public String generarReporteBancaSeguros(){
			//DatosReporteBancaSegurosDTO parametros = new (anioMes, gerencia, Sistema.TIPO_XLS);
			Map<String,Object> parametros = new HashMap<String,Object>();
			parametros.put("anio",anio);
			parametros.put("mes",mes);
			parametros.put("gerencia",gerencia);
			//DatosReporteBancaSegurosDTO dtaBancaSegurosDTO = new DatosReporteBancaSegurosDTO();
			TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirReporteBancaSeguros(parametros);
			reportesCaInputStream = new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
		contentType="application/xls";
		fileName="Reporte_Banca_Seguros.xls";
		return SUCCESS;
		}
		
		
		@Action(value ="mostrarReportePagoSaldos", results ={
				@Result(name= SUCCESS, location="/jsp/compensacionesAdicionales/reportes/reportePagosSaldosComAdi/mostrarPagosSaldosCom.jsp")
		})
		
		public String mostrarReportePagosSaldos (){
			return SUCCESS;
		}

		@Action(value ="generarReportePagosSaldos", results={
				@Result(name=SUCCESS, type="stream", params={
						"contentType","${contentType}",
						"inputName", "reportesCaInputStream",
						"contentDisposition","attachment; filename=\"${fileName}\""
				}),
				@Result(name=INPUT, location="/jsp/error.jsp")
		})
		
		public String generarReportePagosSaldos(){
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("IdCompensacion",idCompensacion);
		parametros.put("cotizacion",cotizacion);
		parametros.put("poliza",poliza);
		parametros.put("agente",agente);
		parametros.put("promotor",promotor);
		parametros.put("proveedor",proveedor);
		parametros.put("fechaInicio",fechaInicio);
		parametros.put("fechaFinal",fechaFinal);
		TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirReportePagosSaldos(parametros);
		reportesCaInputStream =  new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
		contentType="application/xls";
		fileName="Reporte_Pagos_Saldos";
			return SUCCESS;
		}
		
			
		@Action(value="generarReciboHonorarios", results = {
				@Result(name=SUCCESS, type="stream", params={
						"contentType","${contentType}",
						"inputName","reportesEcInputStream",
						"contentDisposition","attachment;filename=\"${fileName}\""
				}),
				@Result(name=INPUT, location="/jsp/error.jsp")
		})

			public String generarReciboHonorarios(){
			
			  ReporteFiltrosDTO reporteFiltrosDTO = new ReporteFiltrosDTO();
		            reporteFiltrosDTO.setAnioMes(anioMes);
		            LOG.info("AnioMes"+reporteFiltrosDTO.getAnioMes());
		            reporteFiltrosDTO.setCveAgente(cveAgente);
		            LOG.info("agente = "+reporteFiltrosDTO.getCveAgente());
		            reporteFiltrosDTO.setCvePromotor(cvePromotor);
		            LOG.info("promotor = "+reporteFiltrosDTO.getCvePromotor());
		            reporteFiltrosDTO.setCveProveedor(cveProveedor);
		            LOG.info("proveedor ="+reporteFiltrosDTO.getCveProveedor());		            
				   TransporteImpresionDTO transporteImpresionDTO = liquidacionCompensacionesService.obtenerReciboHonorarios(reporteFiltrosDTO);
						
					reportesEcInputStream = new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
					contentType="application/pdf";
					fileName="Recibo de Honorarios.pdf"; 

				return SUCCESS;	
			}
		
		@Action(value="generarReporteDetallePrima",results={
				@Result(name=SUCCESS,type="stream",
						params={"contentType","${contentType}","inputName","reportesCaInputStream",
						"contentDisposition","attachment;filename=\"${fileName}\""})})	
		public String generarReporteDetallePrima(){
			try {
				  ReporteFiltrosDTO reporteFiltrosDTO = new ReporteFiltrosDTO();
		            reporteFiltrosDTO.setAnioMes(anioMes);
		            LOG.info("AnioMes"+reporteFiltrosDTO.getAnioMes());
		            reporteFiltrosDTO.setCveAgente(cveAgente);
		            LOG.info("agente = "+reporteFiltrosDTO.getCveAgente());
		            reporteFiltrosDTO.setCvePromotor(cvePromotor);
		            LOG.info("promotor = "+reporteFiltrosDTO.getCvePromotor());
		            reporteFiltrosDTO.setCveProveedor(cveProveedor);
		            LOG.info("proveedor ="+reporteFiltrosDTO.getCveProveedor());
			  	    TransporteImpresionDTO reporte = generarPlantillaReporte.imprimirReporteDetallePrimasComp(reporteFiltrosDTO);
			  	    reportesCaInputStream=new ByteArrayInputStream(reporte.getByteArray());
					contentType = "application/xls";
					fileName="Reporte Autorizar_Pago "+UtileriasWeb.getFechaString(new Date())+".xls";
			} catch (Exception e) {
				onError(e);
			}		
			return SUCCESS;
		}	
		
		
	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	private ImpresionesService impresionesService;
	
	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}
	public List<CaRamo> getRamoLista() {
		return ramoLista;
	}


	public void setRamoLista(List<CaRamo> ramoLista) {
		this.ramoLista = ramoLista;
	}
	
	@Autowired
	@Qualifier("catalogoCompensacionesServiceEJB")
	public void setCatalogoCompensacionesService(
			CatalogoCompensacionesService catalogoCompensacionesService) {
		this.catalogoCompensacionesService = catalogoCompensacionesService;
	}
	
	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporteService(
			GenerarPlantillaReporte generarPlantillaReporteService) {
		this.generarPlantillaReporte = generarPlantillaReporteService;
	}


	public FiltroCompensacion getFiltroCompensacion() {
		return filtroCompensacion;
	}


	public ImpresionesService getImpresionesService() {
		return impresionesService;
	}


	public void setFiltroCompensacion(FiltroCompensacion filtroCompensacion) {
		this.filtroCompensacion = filtroCompensacion;
	}


	public InputStream getReportesCaInputStream() {
		return reportesCaInputStream;
	}


	public void setReportesCaInputStream(InputStream reportesCaInputStream) {
		this.reportesCaInputStream = reportesCaInputStream;
	}
	public Long getNegocio() {
		return negocio;
	}

	public void setNegocio(Long negocio) {
		this.negocio = negocio;
	}
	public Long getIdNegocio() {
		return idNegocio;
	}

	public String getContratante() {
		return contratante;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public  void setContratante(String contratante) {
		this.contratante = contratante;
	}
	public Short getTipoReporte() {
		return tipoReporte;
	}
	public void setTipoReporte(Short tipoReporte) {
		this.tipoReporte = tipoReporte;
	}


	public Date getFechaFin() {
		return fechaFin;
	}
	public  void setRamo(String ramo) {
		this.ramo = ramo;
	}
	public String getRamo() {
		return ramo;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public  Date getFechaInicio() {
		return fechaInicio;
	}

	public  void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public  String getAgente() {
		return agente;
	}

	public  String getPromotor() {
		return promotor;
	}

	public  String getProveedor() {
		return proveedor;
	}

	public  String getGerencia() {
		return gerencia;
	}

	public  void setAgente(String agente) {
		this.agente = agente;
	}

	public  void setPromotor(String promotor) {
		this.promotor = promotor;
	}

	public  void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public  void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}


	public String getNomAgente() {
		return nomAgente;
	}


	public void setNomAgente(String nomAgente) {
		this.nomAgente = nomAgente;
	}


	public String getNomPromotor() {
		return nomPromotor;
	}


	public void setNomPromotor(String nomPromotor) {
		this.nomPromotor = nomPromotor;
	}


	public String getNomProveedor() {
		return nomProveedor;
	}


	public void setNomProveedor(String nomProveedor) {
		this.nomProveedor = nomProveedor;
	}


	public Long getCveAgente() {
		return cveAgente;
	}


	public void setCveAgente(Long cveAgente) {
		this.cveAgente = cveAgente;
	}


	public Long getCvePromotor() {
		return cvePromotor;
	}


	public void setCvePromotor(Long cvePromotor) {
		this.cvePromotor = cvePromotor;
	}


	public Long getCveProveedor() {
		return cveProveedor;
	}


	public void setCveProveedor(Long cveProveedor) {
		this.cveProveedor = cveProveedor;
	}


	public InputStream getReportesEcInputStream() {
		return reportesEcInputStream;
	}


	public void setReportesEcInputStream(InputStream reportesEcInputStream) {
		this.reportesEcInputStream = reportesEcInputStream;
	}

	public Long getClaveNombreAgente() {
		return claveNombreAgente;
	}


	public void setClaveNombreAgente(Long claveNombreAgente) {
		this.claveNombreAgente = claveNombreAgente;
	}


	public Long getClaveNombreProveedor() {
		return claveNombreProveedor;
	}


	public void setClaveNombreProveedor(Long claveNombreProveedor) {
		this.claveNombreProveedor = claveNombreProveedor;
	}


	public Date getFechaInicial() {
		return fechaInicial;
	}


	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}


	public Date getFechaFinal() {
		return fechaFinal;
	}


	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}


	public String getRamoo() {
		return ramoo;
	}


	public void setRamoo(String ramoo) {
		this.ramoo = ramoo;
	}


	public Long getPoliza() {
		return poliza;
	}


	public void setPoliza(Long poliza) {
		this.poliza = poliza;
	}


	public InputStream getReportesPCCInputStream() {
		return reportesPCCInputStream;
	}


	public void setReportesPCCInputStream(InputStream reportesPCCInputStream) {
		this.reportesPCCInputStream = reportesPCCInputStream;
	}


	public Long getClaveNombreGerencia() {
		return claveNombreGerencia;
	}


	public void setClaveNombreGerencia(Long claveNombreGerencia) {
		this.claveNombreGerencia = claveNombreGerencia;
	}


	public Long getClaveNombrePromotor() {
		return claveNombrePromotor;
	}


	public void setClaveNombrePromotor(Long claveNombrePromotor) {
		this.claveNombrePromotor = claveNombrePromotor;
	}


	/*public static Long getAnioMes() {
		return anioMes;
	}


	public static void setAnioMes(Long anioMes) {
		ReportesCompensacionesAction.anioMes = anioMes;
	}*/


	
	

	public String getNoPoliza() {
		return noPoliza;
	}


	public void setNoPoliza(String noPoliza) {
		this.noPoliza = noPoliza;
	}


	public String getResumenDetalle() {
		return resumenDetalle;
	}


	public void setResumenDetalle(String resumenDetalle) {
		this.resumenDetalle = resumenDetalle;
	}
	

	public Short getTipoArchivo() {
		return tipoArchivo;
	}


	public void setTipoArchivo(Short tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}


	public Long getAnioMes() {
		return anioMes;
	}


	public void setAnioMes(Long anioMes) {
		this.anioMes = anioMes;
	}


	public String getNombreNegocio() {
		return nombreNegocio;
	}


	public  void setNombreNegocio(String nombreNegocio) {
		this.nombreNegocio = nombreNegocio;
	}


	public Long getAgenteDev() {
		return agenteDev;
	}


	public void setAgenteDev(Long agenteDev) {
		this.agenteDev = agenteDev;
	}


	public Long getPromotorDev() {
		return promotorDev;
	}


	public void setPromotorDev(Long promotorDev) {
		this.promotorDev = promotorDev;
	}


	public Long getProveedorDev() {
		return proveedorDev;
	}


	public void setProveedorDev(Long proveedorDev) {
		this.proveedorDev = proveedorDev;
	}


	public Long getGerenciaDev() {
		return gerenciaDev;
	}


	public void setGerenciaDev(Long gerenciaDev) {
		this.gerenciaDev = gerenciaDev;
	}


	public Long getAnio() {
		return anio;
	}


	public void setAnio(Long anio) {
		this.anio = anio;
	}


	public Long getMes() {
		return mes;
	}


	public void setMes(Long mes) {
		this.mes = mes;
	}


	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}

	public Long getName_reporte() {
		return name_reporte;
	}


	public void setName_reporte(Long name_reporte) {
		this.name_reporte = name_reporte;
	}   
	
	
}
