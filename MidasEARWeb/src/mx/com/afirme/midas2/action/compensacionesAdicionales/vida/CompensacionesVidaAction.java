package mx.com.afirme.midas2.action.compensacionesAdicionales.vida;

import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNotNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVida;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaAgente;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaGerencia;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaProducto;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaRamo;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaSeccion;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVidaSubRamo;
import mx.com.afirme.midas2.domain.emision.ppct.AgenteSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.LineaNegocioSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.ProductoSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.SubRamoSeycos;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.compensaciones.CaCompensacionService;
import mx.com.afirme.midas2.service.compensaciones.negociovida.NegocioVidaService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/compensacionesAdicionales/vida")

@InterceptorRefs({
    @InterceptorRef("timer"),
    @InterceptorRef("midas2Stack")
})
public class CompensacionesVidaAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6966558576899239126L;
	private static final Logger LOG = LoggerFactory.getLogger(CompensacionesVidaAction.class);
	
	private static final String LOCATION_MOSTRARCONTENEDORVIDA_JSP = "/jsp/compensacionesAdicionales/vida/contenedorNegocioVida.jsp";
	private static final String CONFIGURACION_NEGOCIO_VIDA 		   = "/jsp/compensacionesAdicionales/vida/configuracionNegocioVida.jsp";
	private static final String CONFIGURACION_NEGOCIO_VIDA_GRID    = "/jsp/compensacionesAdicionales/vida/configuracionNegocioVidaGrid.jsp";
	private static final String CONFIGURACION_NEGOCIO_VIDA_MOSTRAR    = "/jsp/compensacionesAdicionales/vida/mostrar.jsp";
	//private final String NAMESPACE="/compensacionesAdicionales/vida";
	
	private Long id;
	private String descripcion;
	private List<NegocioVida> listaNegociosVida;
	@Autowired 
	private CaCompensacionService caCompensacionService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	private EntidadService entidadService;
	@Autowired
	private NegocioVidaService negocioVidaService;
	private NegocioVida configuracionNegocioVida;
	private InputStream dataStream;
	
	@Action(value="mostrar",
			results={@Result( name=SUCCESS, location=CONFIGURACION_NEGOCIO_VIDA_MOSTRAR),
			         @Result( name=INPUT, location=CONFIGURACION_NEGOCIO_VIDA_MOSTRAR)})
	public String mostrar(){		
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=CONFIGURACION_NEGOCIO_VIDA_GRID),
			@Result(name=INPUT,location=CONFIGURACION_NEGOCIO_VIDA_GRID)
		})	
		public String listarFiltrado() {
			LOG.info("descripcion>>"+descripcion);
			LOG.info("id>>"+id);
			try{				 
				listaNegociosVida=negocioVidaService.findByFilters(new NegocioVida(id,descripcion));
			}catch(Exception e){
				return INPUT;
			}
			return SUCCESS;
		}

	@Action(value="mostrarConfiguracionInicio",
			results={@Result( name=SUCCESS, location="/jsp/compensacionesAdicionales/vida/configuracion.jsp"),
			         @Result( name=INPUT, location="/jsp/compensacionesAdicionales/vida/configuracion.jsp")})
	public String mostrarConfiguracionInicio(){
		
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedorNegocioVida", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRARCONTENEDORVIDA_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRARCONTENEDORVIDA_JSP) })
	public String mostrarContenedorNegocioVida() {
		return SUCCESS;
	}
	
	@Action(value = "verDetalleNegocioVida", results = {
			@Result(name = SUCCESS, location = CONFIGURACION_NEGOCIO_VIDA),
			@Result(name = INPUT, location = CONFIGURACION_NEGOCIO_VIDA) })
	public String verDetalleNegocioVida() {
		inicializarListas();
		//ConfigBonosDTO filtroConfigBono=new ConfigBonosDTO();
		return SUCCESS;
	}	
	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location=CONFIGURACION_NEGOCIO_VIDA)
		})
	public String verDetalle() {
		try {
			configuracionNegocioVida=entidadService.findById(NegocioVida.class, id);
				inicializarListas();				
		} catch (Exception e) {
			LOG.error("-- verDetalle()",e);
		}
		return SUCCESS;
	}
	
	private List<GenericaAgentesView> listaProductosSeycos = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> listaProductos = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> listaRamos  = new ArrayList<GenericaAgentesView>();
	
	private List<NegocioVidaAgente> listaAgentes = new ArrayList<NegocioVidaAgente>();
	private List<GenericaAgentesView> lista_agentes = new ArrayList<GenericaAgentesView>();
	
	private List<NegocioVidaGerencia> listaGerencia= new ArrayList<NegocioVidaGerencia>();
	
	private List<GenericaAgentesView> listaGerenciaSeycos = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> listaSubRamos = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> listaLineaNegocio = new ArrayList<GenericaAgentesView>();
	
	private List<NegocioVidaProducto> listaProductosSeleccionados = new ArrayList<NegocioVidaProducto>();
	private List<NegocioVidaRamo>     listaRamosSeleccionados = new ArrayList<NegocioVidaRamo>();
	private List<NegocioVidaSubRamo>  listaSubRamosSeleccionados = new ArrayList<NegocioVidaSubRamo>();
	private List<NegocioVidaGerencia> listaGerenciaSeleccionadas = new ArrayList<NegocioVidaGerencia>();
	private List<NegocioVidaSeccion>  listaLineaNegocioSeleccionados = new ArrayList<NegocioVidaSeccion>();	
	
	private List<Long> productoLong = new ArrayList<Long>();
	private List<Long> gerenciaLong = new ArrayList<Long>();
	private List<Long> agenteLong = new ArrayList<Long>();
	private List<Long> ramoLong = new ArrayList<Long>();
	private List<Long> subRamoLong = new ArrayList<Long>();
	private List<Long> lineaNegocioLong = new ArrayList<Long>();
		
	private ConfigBonosService configBonosService;  
	private ConfigBonos configuracionBono;
	private void inicializarListas(){
		try {
			if(configuracionNegocioVida!=null && configuracionNegocioVida.getId()!=null){
				cargarCombosChecados();
			}
			else{
				//listaProductosSeycos = listadoService.listaProductoSeycos("1");
				listaGerenciaSeycos = listadoService.listaGerenciaSeycos();
				listaRamos = negocioVidaService.getRamosPorProductosView("0");
			}
		} catch (Exception e) {
			LOG.error("-- inicializarListas()", e);
		}
	}
	
	public void prepareGuardar() {
		guardarConfiguracionNegocioSeycos();
	}
	
	@Action(value="guardarConfiguracionNegocioSeycos",
			results={
				@Result(name = "success",type = "stream", 
						params = {"contentType", "text/plain","inputName", "dataStream"})
			})
	public String guardarConfiguracionNegocioSeycos(){
		Usuario usuario = null;
		
		usuario = usuarioService.getUsuarioActual();
		LOG.info(">> Entrando guardarConfiguracionNegocioSeycos()");
		try {			
			 cargaListas();
			 configuracionNegocioVida.setBajalogica(Short.valueOf("1"));
			 configuracionNegocioVida.setUsuario(usuario.getNombre());
			 configuracionNegocioVida.setFechacreacion(new Date());
			 configuracionNegocioVida.setCodigo("0");
			 configuracionNegocioVida = negocioVidaService.saveConfiguration(configuracionNegocioVida);
			 this.dataStream = new ByteArrayInputStream(configuracionNegocioVida.getId().toString().getBytes());
		} catch (Exception e) {		
			inicializarListas();
			LOG.error("--error guardarConfiguracionNegocioSeycos()>>", e);
		}
		return SUCCESS;
	}
	

	@Action(value="findIdsCompensaciones", results={
	                @Result(name = "success",type = "stream", params = {"contentType", "text/plain","inputName", "dataStream"})
	            })
	    public String findIdsCompensaciones(){            
	        LOG.info(">> Entrando findIdsCompensaciones()");
	        try { 
	        	if(configuracionNegocioVida !=  null && configuracionNegocioVida.getId() != null && configuracionNegocioVida.getId() > 0){	                        
				 List<CaCompensacion> listCompensaciones = caCompensacionService.listCompensaciones(RAMO.VIDA.getValor(), ConstantesCompensacionesAdicionales.NEGOCIOVIDA_ID, configuracionNegocioVida.getId());
				 Map<String, Long> mapResult = new HashMap<String, Long>();				                 
			     for(CaCompensacion caCompensacion: listCompensaciones){
			         mapResult.put(caCompensacion.getContraprestacion() ? ConstantesCompensacionesAdicionales.TAB_CONTRAPRESTACION : ConstantesCompensacionesAdicionales.TAB_COMPENSACION, caCompensacion.getId());
			     }
				 this.dataStream = new ByteArrayInputStream(new Gson().toJson(mapResult).getBytes());
	        	}	        	
	        } catch (Exception e) {     
	            inicializarListas();
	            LOG.error("--error findIdsCompensaciones()>>", e);
	        }
	        return SUCCESS;
	    }
	
	private void cargaListas() throws Exception {
		LOG.info(">> Entrando cargaListas()");
			if(configuracionNegocioVida != null){			
				configuracionNegocioVida.setNegocioVidaProductos(obtenerProductoSeleccionados());
				configuracionNegocioVida.setNegocioVidaRamos(obtenerRamoSeleccionados());
				configuracionNegocioVida.setNegocioVidaSubRamos(obtenerSubRamosSeycosSeleccionadas());
				configuracionNegocioVida.setNegocioVidaSeccions(obtenerLineaNegocioSeleccionados());
				configuracionNegocioVida.setNegocioVidaGerencias(obtenerGerenciasSeycosSeleccionadas());
				configuracionNegocioVida.setNegocioVidaAgentes(obtenerAgenteSeycosSeleccionadas());
			}
		}
	
	private void cargarCombosChecados(){
		try {
			listaRamos=negocioVidaService.getRamosXConfView(id);
			listaSubRamos=negocioVidaService.getSubRamosXConfView(id);
			listaProductosSeycos=negocioVidaService.getProductoXConfView(id);
			listaLineaNegocio=negocioVidaService.getLineasNegXConfView(id);
			listaGerenciaSeycos=negocioVidaService.getGerenciaXConfView(id);
			lista_agentes = negocioVidaService.getAgenteXConfView(id);
		 }catch (Exception e){
			LOG.error("cargarCombosChecados()", e);
		}
	}

	private List<NegocioVidaProducto> obtenerProductoSeleccionados(){
		LOG.info(">>>Entrando obtenerProductoSeleccionados()");
		List<NegocioVidaProducto> listaProductosSeleccionados = new ArrayList<NegocioVidaProducto>();
		List<ProductoSeycos> listaProductoSeycos;		
		NegocioVidaProducto negocioVidaProducto;
		try {
		if(!isEmptyList(productoLong)){
			for(Long idProducto : productoLong){
				if(isNotNull(idProducto)){
					listaProductoSeycos = negocioVidaService.getProductoVida(idProducto);
					for(ProductoSeycos objProducto : listaProductoSeycos){
						negocioVidaProducto = new NegocioVidaProducto();
						negocioVidaProducto.setProducto(objProducto);
						listaProductosSeleccionados.add(negocioVidaProducto);
					}			
				}
			}
		}	
		}catch (Exception e) {
			LOG.error("-- error obtenerGerenciasSeycosSeleccionadas()", e);
		}
		return listaProductosSeleccionados;
	}

	private List<NegocioVidaRamo> obtenerRamoSeleccionados(){
		LOG.info(">>>Entrando obtenerRamoSeleccionados()");
		List<NegocioVidaRamo> listaRamosSeleccionados = new ArrayList<NegocioVidaRamo>();
		List<RamoSeycos> listaRamoSeycos;
		NegocioVidaRamo negocioVidaRamos;
		try {
		if(!isEmptyList(ramoLong)){
			for(Long idRamo : ramoLong){
				if(isNotNull(idRamo)){
					listaRamoSeycos = negocioVidaService.getRamoVida(idRamo);
					for(RamoSeycos objRamo : listaRamoSeycos){
						negocioVidaRamos = new NegocioVidaRamo();
						negocioVidaRamos.setRamo(objRamo);
						listaRamosSeleccionados.add(negocioVidaRamos);
					}			
				}
			}
		}	
		}catch (Exception e) {
			LOG.error("-- error obtenerRamoSeleccionados()", e);
		}
		return listaRamosSeleccionados;
	}
	
	private List<NegocioVidaSubRamo> obtenerSubRamosSeycosSeleccionadas(){
		LOG.info(">>>Entrando obtenerSubRamosSeycosSeleccionadas()");
		List<NegocioVidaSubRamo> listaSubRamosSeleccionados = new ArrayList<NegocioVidaSubRamo>();
		List<SubRamoSeycos> listaSubRamoSeycos;
		NegocioVidaSubRamo negocioVidaSubRamo;
		try {
		if(!isEmptyList(subRamoLong)){
			for(Long idSubRamo : subRamoLong){
				if(isNotNull(idSubRamo)){
					listaSubRamoSeycos = negocioVidaService.getSubRamoVida(idSubRamo);
					for(SubRamoSeycos objAgente : listaSubRamoSeycos){
						negocioVidaSubRamo = new NegocioVidaSubRamo();
						negocioVidaSubRamo.setSubRamo(objAgente);
						listaSubRamosSeleccionados.add(negocioVidaSubRamo);
					}			
				}
			}
		}
		
		}catch (Exception e) {
			LOG.error("-- error obtenerSubRamosSeycosSeleccionadas()", e);
		}
		return listaSubRamosSeleccionados;
	}
	
	private List<NegocioVidaSeccion> obtenerLineaNegocioSeleccionados(){
		LOG.info(">>>Entrando obtenerLineaNegocioSeleccionados()");
		List<NegocioVidaSeccion> listaLineaNegocioSeleccionados = new ArrayList<NegocioVidaSeccion>();
		List<LineaNegocioSeycos> listaSeccionSeycos;
		NegocioVidaSeccion negocioSeccion;
		try {
		if(!isEmptyList(lineaNegocioLong)){
			for(Long idLinea : lineaNegocioLong){
				if(isNotNull(idLinea)){
					listaSeccionSeycos = negocioVidaService.getLineaNegocioVida(idLinea);
					for(LineaNegocioSeycos objSeccion : listaSeccionSeycos){
						negocioSeccion = new NegocioVidaSeccion();
						negocioSeccion.setSeccion(objSeccion);
						listaLineaNegocioSeleccionados.add(negocioSeccion);
					}			
				}
			}
		}	
		}catch (Exception e) {
			LOG.error("-- error obtenerLineaNegocioSeleccionados()", e);
		}
		return listaLineaNegocioSeleccionados;
	}
	
	private List<NegocioVidaGerencia> obtenerGerenciasSeycosSeleccionadas(){
		LOG.info(">>>Entrando obtenerGerenciasSeycosSeleccionadas()");
		List<NegocioVidaGerencia> listaGerencia= new ArrayList<NegocioVidaGerencia>();
		List<GerenciaSeycos> listagerenciaSeycos;
		NegocioVidaGerencia negocioVidaGerencia;
		try {
		if(!isEmptyList(gerenciaLong)){
			for(Long idGerencia : gerenciaLong){
				if(isNotNull(idGerencia)){
					listagerenciaSeycos = negocioVidaService.getGerenciaVida(idGerencia);
					for(GerenciaSeycos objGerencia : listagerenciaSeycos){
						negocioVidaGerencia = new NegocioVidaGerencia();
						negocioVidaGerencia.setGerencia(objGerencia);
						negocioVidaGerencia.setIdEmpresa(objGerencia.getIdEmpresa());
						negocioVidaGerencia.setIdGerencia(objGerencia.getIdGerencia());
						negocioVidaGerencia.setFSit(objGerencia.getFSit());
						listaGerencia.add(negocioVidaGerencia);
					}			
				}
			}
		}
		
		}catch (Exception e) {
			LOG.error("-- error obtenerGerenciasSeycosSeleccionadas()", e);
		}
		return listaGerencia;
	}


	private List<NegocioVidaAgente> obtenerAgenteSeycosSeleccionadas(){
		LOG.info(">>>Entrando obtenerAgenteSeycosSeleccionadas()");
		List<NegocioVidaAgente> listaAgentes = new ArrayList<NegocioVidaAgente>();
		List<AgenteSeycos> listaAgenteSeycos;
		NegocioVidaAgente negocioVidaAgente;
		try {
		if(!isEmptyList(agenteLong)){
			for(Long idAgente : agenteLong){
				if(isNotNull(idAgente)){
					listaAgenteSeycos = negocioVidaService.getAgenteVida(idAgente);
					for(AgenteSeycos objAgentes : listaAgenteSeycos){
						negocioVidaAgente = new NegocioVidaAgente();
						negocioVidaAgente.setAgenteSeycos(objAgentes);
						negocioVidaAgente.setIdEmpresa(objAgentes.getIdEmpresa());
						negocioVidaAgente.setIdAgente(objAgentes.getIdAgente());
						negocioVidaAgente.setFSit(objAgentes.getFSit());
						listaAgentes.add(negocioVidaAgente);
					}			
				}
			}
		}
		
		}catch (Exception e) {
			LOG.error("-- error obtenerEgentesSeycosSeleccionadas()", e);
		}
		return listaAgentes;
	}
	
	
	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}	
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public List<GenericaAgentesView> getListaProductosSeycos() {
		return listaProductosSeycos;
	}

	public List<GenericaAgentesView> getListaProductos() {
		return listaProductos;
	}
	
	public List<GenericaAgentesView> getListaGerenciaSeycos() {
		return listaGerenciaSeycos;
	}

	public List<GenericaAgentesView> getListaSubRamos() {
		return listaSubRamos;
	}

	public List<GenericaAgentesView> getListaLineaNegocio() {
		return listaLineaNegocio;
	}

	public ConfigBonosService getConfigBonosService() {
		return configBonosService;
	}

	public ConfigBonos getConfiguracionBono() {
		return configuracionBono;
	}

	public void setListaProductosSeycos(
			List<GenericaAgentesView> listaProductosSeycos) {
		this.listaProductosSeycos = listaProductosSeycos;
	}

	public void setListaProductos(List<GenericaAgentesView> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public void setListaGerenciaSeycos(List<GenericaAgentesView> listaGerenciaSeycos) {
		this.listaGerenciaSeycos = listaGerenciaSeycos;
	}

	public void setListaSubRamos(List<GenericaAgentesView> listaSubRamos) {
		this.listaSubRamos = listaSubRamos;
	}

	public void setListaLineaNegocio(List<GenericaAgentesView> listaLineaNegocio) {
		this.listaLineaNegocio = listaLineaNegocio;
	}

	public void setConfiguracionBono(ConfigBonos configuracionBono) {
		this.configuracionBono = configuracionBono;
	}

	public NegocioVidaService getNegocioVidaService() {
		return negocioVidaService;
	}

	public void setNegocioVidaService(NegocioVidaService negocioVidaService) {
		this.negocioVidaService = negocioVidaService;
	}

	public NegocioVida getConfiguracionNegocioVida() {
		return configuracionNegocioVida;
	}

	public void setConfiguracionNegocioVida(NegocioVida configuracionNegocioVida) {
		this.configuracionNegocioVida = configuracionNegocioVida;
	}

	public List<NegocioVidaRamo> getListaRamosSeleccionados() {
		return listaRamosSeleccionados;
	}

	public List<NegocioVidaSubRamo> getListaSubRamosSeleccionados() {
		return listaSubRamosSeleccionados;
	}

	public void setListaRamosSeleccionados(
			List<NegocioVidaRamo> listaRamosSeleccionados) {
		this.listaRamosSeleccionados = listaRamosSeleccionados;
	}

	public void setListaSubRamosSeleccionados(
			List<NegocioVidaSubRamo> listaSubRamosSeleccionados) {
		this.listaSubRamosSeleccionados = listaSubRamosSeleccionados;
	}

	public List<NegocioVidaGerencia> getListaGerenciaSeleccionadas() {
		return listaGerenciaSeleccionadas;
	}

	public List<NegocioVidaSeccion> getListaLineaNegocioSeleccionados() {
		return listaLineaNegocioSeleccionados;
	}

	public void setListaGerenciaSeleccionadas(
			List<NegocioVidaGerencia> listaGerenciaSeleccionadas) {
		this.listaGerenciaSeleccionadas = listaGerenciaSeleccionadas;
	}

	public void setListaLineaNegocioSeleccionados(
			List<NegocioVidaSeccion> listaLineaNegocioSeleccionados) {
		this.listaLineaNegocioSeleccionados = listaLineaNegocioSeleccionados;
	}

	public void setListaProductosSeleccionados(
			List<NegocioVidaProducto> listaProductosSeleccionados) {
		this.listaProductosSeleccionados = listaProductosSeleccionados;
	}

	public List<NegocioVidaAgente> getListaAgentes() {
		return listaAgentes;
	}

	public List<NegocioVidaProducto> getListaProductosSeleccionados() {
		return listaProductosSeleccionados;
	}

	public void setListaAgentes(List<NegocioVidaAgente> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Long> getGerenciaLong() {
		return gerenciaLong;
	}

	public void setGerenciaLong(List<Long> gerenciaLong) {
		this.gerenciaLong = gerenciaLong;
	}

	public List<GenericaAgentesView> getLista_agentes() {
		return lista_agentes;
	}

	public void setLista_agentes(List<GenericaAgentesView> lista_agentes) {
		this.lista_agentes = lista_agentes;
	}

	public List<NegocioVidaGerencia> getListaGerencia() {
		return listaGerencia;
	}

	public void setListaGerencia(List<NegocioVidaGerencia> listaGerencia) {
		this.listaGerencia = listaGerencia;
	}

	public List<Long> getRamoLong() {
		return ramoLong;
	}

	public List<Long> getLineaNegocioLong() {
		return lineaNegocioLong;
	}

	public void setRamoLong(List<Long> ramoLong) {
		this.ramoLong = ramoLong;
	}

	public void setLineaNegocioLong(List<Long> lineaNegocioLong) {
		this.lineaNegocioLong = lineaNegocioLong;
	}

	public List<Long> getProductoLong() {
		return productoLong;
	}

	public void setProductoLong(List<Long> productoLong) {
		this.productoLong = productoLong;
	}

	public List<Long> getSubRamoLong() {
		return subRamoLong;
	}

	public void setSubRamoLong(List<Long> subRamoLong) {
		this.subRamoLong = subRamoLong;
	}

	public void setDataStream(InputStream dataStream) {
		this.dataStream = dataStream;
	}

	public InputStream getDataStream() {
		return dataStream;
	}

	public List<GenericaAgentesView> getListaRamos() {
		return listaRamos;
	}

	public void setListaRamos(List<GenericaAgentesView> listaRamos) {
		this.listaRamos = listaRamos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<NegocioVida> getListaNegociosVida() {
		return listaNegociosVida;
	}

	public void setListaNegociosVida(List<NegocioVida> listaNegociosVida) {
		this.listaNegociosVida = listaNegociosVida;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String guardar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String listar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	public List<Long> getAgenteLong() {
		return agenteLong;
	}

	public void setAgenteLong(List<Long> agenteLong) {
		this.agenteLong = agenteLong;
	}

}