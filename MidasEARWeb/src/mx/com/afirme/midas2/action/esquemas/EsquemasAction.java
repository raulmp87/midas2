package mx.com.afirme.midas2.action.esquemas;

import java.math.BigDecimal;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/esquemas/cargaArchivos")
public class EsquemasAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 6132998255096489739L;
	public static final String EXITOSO = "Carga Finalizada.";
	public static final String ERROR = "Error al guardar el archivo. Favor revisarlo!";
	
	private BigDecimal idToControlArchivo;

	private String fechaCorte;
	
	private String tipoArchivo;
	
	private String mensaje;
	
	@SuppressWarnings("unused")
	private EntidadService entidadService;
	
	@Override
	public void prepare() {
		
	}
	
	@Action
	(value = "mostrar", results = { 
			@Result(name = SUCCESS, location = "/jsp/vida/archivoCompVida.jsp") })
	public String mostrarGeneracion() {	
		
		
		return SUCCESS;
	}
		
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}
	
	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}
	
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
