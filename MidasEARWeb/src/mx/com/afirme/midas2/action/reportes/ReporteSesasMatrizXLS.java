package mx.com.afirme.midas2.action.reportes;
import java.util.List;

import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.creadorxls.MidasDocumentoXLS;
import mx.com.afirme.midas.sistema.creadorxls.MidasHojaXLS;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;
import mx.com.afirme.midas2.service.reportes.GeneracionSesasService;

import org.apache.log4j.Logger;

public class ReporteSesasMatrizXLS extends MidasDocumentoXLS  {
	private static final Logger LOG = Logger
	.getLogger(ReporteSesasMatrizXLS.class);


private GeneracionSesasService generacionSesasService;
public ReporteSesasMatrizXLS(){
ServiceLocator serviceLocator = ServiceLocator.getInstance();

try {
	generacionSesasService = serviceLocator.getEJB(GeneracionSesasService.class);
	
} catch (SystemException e) {
	LOG.error("Error en reporte Sesas Action: ", e);
}
}

@SuppressWarnings({ "rawtypes", "unchecked" })
public byte[] obtenerReporte(Integer idGeneracion)throws Exception{
MidasHojaXLS hojaMatriz = generaHojaMatriz();
hojaMatriz.iniciarProcesamientoArchivoXLS();

List listaRegistros = generacionSesasService.generarMatriz( idGeneracion); 
hojaMatriz.insertarFilasArchivoXLS(listaRegistros);
return finalizarProcesamientoArchivoXLS();
}


private MidasHojaXLS generaHojaMatriz() {
String [] atributosDTO = {
		"id_tipo_arch",
		"nombre_campo",
		"id_validacion",
		"descripcion",
		"midas_ind_err",
		"midas_ind_war",
		"midas_flo_err",
		"midas_flo_war",
		"midas_tot_err",
		"midas_tot_war",
		"seycos_ind_err",
		"seycos_ind_war",
		"seycos_flo_err",
		"seycos_flo_war",
		"seycos_tot_err",
		"seycos_tot_war"
};

String[] nombreColumnas = {
		"TIPO",
		"CAMPO",
		"ID VALIDACION",
		"DESCRIPCION",
		"MIDAS_IND_ERR",
		"MIDAS_IND_WAR",
		"MIDAS_FLO_ERR",
		"MIDAS_FLO_WAR",
		"MIDAS_TOT_ERR",
		"MIDAS_TOT_WAR",
		"SEYCOS_IND_ERR",
		"SEYCOS_IND_WAR",
		"SEYCOS_FLO_ERR",
		"SEYCOS_FLO_WAR",
		"SEYCOS_TOT_ERR",
		"SEYCOS_TOT_WAR"
};

MidasHojaXLS hojaMatriz = new MidasHojaXLS("Matriz de validacion", nombreColumnas, atributosDTO, this,MidasXLSCreator.FORMATO_XLSX);
return hojaMatriz;
}

}
