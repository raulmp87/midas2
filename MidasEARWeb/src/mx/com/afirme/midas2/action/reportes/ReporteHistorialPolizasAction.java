package mx.com.afirme.midas2.action.reportes;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialPolizaDTO;
import mx.com.afirme.midas2.service.reportes.historial.poliza.ReporteHistorialPolizaService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * Clase que atiende las peticiones del usuario
 * 
 * @author AFIRME
 * 
 * @since 28072016
 * 
 * @version 1.0
 *
 */
@Namespace("/reporte/historialPoliza")
@Component
@Scope("prototype")
public class ReporteHistorialPolizasAction extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = 1L;
	
	private TransporteImpresionDTO transporteExcel;
	private String resultJson;
	
	@Autowired
	private ReporteHistorialPolizaService histPoliza;
	
	private static final Logger log = LoggerFactory.getLogger(ReporteHistorialPolizasAction.class);
	
	public ReporteHistorialPolizasAction() {
	}

	@Override
	public void prepare() throws Exception {
	}
	
	@Action(value = "generarReporteHistorialPoliza", results = {@Result(name = SUCCESS, type="stream", 
			params={ "contentType","${transporteExcel.contentType}", "inputName","transporteExcel.genericInputStream", "contentDisposition", "attachment;filename=\"${transporteExcel.fileName}\""}),
			@Result(name = INPUT, type ="json",  params={"noCache","true", "ignoreHierarchy", "false", "includeProperties", "resultJson"})})
	public String generarReporteHistorialPoliza(){
		String result = SUCCESS;
		List<ReporteHistorialPolizaDTO> polizas = new ArrayList<ReporteHistorialPolizaDTO>();
		Agente agente = usuarioService.getAgenteUsuarioActual();
		
		try{
			polizas = histPoliza.consultarHistorialPolizasPorAgente(agente.getId());
			transporteExcel = histPoliza.generarReporte(polizas, "Rep_Historial_Poliza");
		} catch(Exception err){
			resultJson = "No se pudo Generar el Excel";
			result = INPUT;
			log.error("No se pudo Generar el Excel", err);
		}
		
		return result;
	}

	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public String getResultJson() {
		return resultJson;
	}

	public void setResultJson(String resultJson) {
		this.resultJson = resultJson;
	}

	public ReporteHistorialPolizaService getHistPoliza() {
		return histPoliza;
	}

	public void setHistPoliza(ReporteHistorialPolizaService histPoliza) {
		this.histPoliza = histPoliza;
	}
}
