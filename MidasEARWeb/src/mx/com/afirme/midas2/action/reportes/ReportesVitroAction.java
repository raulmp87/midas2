package mx.com.afirme.midas2.action.reportes;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.apache.struts2.convention.annotation.Action;

import com.afirme.nomina.model.*;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportes.vitro.ReportesVitroDTO;
import mx.com.afirme.midas2.service.reportes.vitro.ReportesVitroService;

/**
 * Clase que contiene las llamadas a los metodos necesarios para generar el reporte de vitro.
 * 
 * @author Afirme
 * 
 * @since 08052016
 */
@Namespace("/reportes/vitro")
@Component
@Scope("prototype")
public class ReportesVitroAction extends BaseAction{

	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = LoggerFactory.getLogger(ReportesVitroAction.class);
	
	@Autowired
	private ReportesVitroService vitroReportService;
	
	private List<Dpngrupo> gruposVitro;
	private List<Date> cobros;
	private List<Negocio> negocios;
	private List<Dpncliente> clientes;
	private List<ReportesVitroDTO> polizasVitro;
	
	private TransporteImpresionDTO transporteExcel;
	
	private Date fechaCorte;
	
	private String nombreGrupo;
	private String fileName;
	private String fecha;
	
	private Long idGrupo;
	private Long idNegocio;
	private Long idFormaPago;
	private Long idCliente;
	
	private static final String PANTALLA_REPORTES_VITRO = "/jsp/reportes/vitro/reportesVitro.jsp";

	@Action(value = "init", results = { @Result(name = SUCCESS, location = PANTALLA_REPORTES_VITRO) })
	public String init(){

		negocios = vitroReportService.consultarNegocios();
		
		if(negocios.isEmpty()){
			nombreGrupo = "El agente no puede vender este negocio";
		}
		
		return SUCCESS;
	}
	
	@Action(value = "buscarListaFechasCobro", results = {@Result(name = SUCCESS, type="json", params={"noCache","true", "ignoreHierarchy", "false", 
			"includeProperties", "^cobros.*"})})
	public String buscarListaFechasCobros(){
		cobros = vitroReportService.consultarCalendarioCobro(idGrupo);
		return SUCCESS;
	}
	
	@Action(value = "buscarClientes", results = {@Result(name = SUCCESS, type="json", params={"noCache","true", "ignoreHierarchy", "false", 
			"includeProperties", "^clientes.*"})})
	public String consultarClientes(){
		clientes = vitroReportService.consultarCliente(idNegocio.toString());
		return SUCCESS;
	}
	
	@Action(value = "buscarGruposCliente", results = {@Result(name = SUCCESS, type="json", params={"noCache","true", "ignoreHierarchy", "false", 
			"includeProperties", "^gruposVitro.*"})})
	public String consultarGruposCliente(){
		gruposVitro = vitroReportService.consultarGruposVitro();
		return SUCCESS;
	}
	
	@Action(value = "generarReporte", results={@Result( name=SUCCESS, type="stream", params={ "contentType","${transporteExcel.contentType}", "inputName","transporteExcel.genericInputStream", "contentDisposition", "attachment;filename=\"${transporteExcel.fileName}\""})})
	public String generarReporte(){
		if(nombreGrupo.trim().equals(ReportesVitroService.GRUPO_NOMINA_SEMANAL)){
			polizasVitro = vitroReportService.consultarPolizasVitroSemanal(fechaCorte, idNegocio, idFormaPago);
			if(StringUtils.isNotBlank(fecha))
				fileName = ReportesVitroService.NOMBRE_REPORTE_VITRO + "_W" + fecha.replace('/', '.');
			else
				fileName = ReportesVitroService.NOMBRE_REPORTE_VITRO + "_W";
		}else{
			polizasVitro = vitroReportService.consultarPolizasVitroQuincenal(fechaCorte, idNegocio, idFormaPago);
			if(StringUtils.isNotBlank(fecha))
				fileName = ReportesVitroService.NOMBRE_REPORTE_VITRO + "_Q" + fecha.replace('/', '.');
			else
				fileName = ReportesVitroService.NOMBRE_REPORTE_VITRO + "_Q";
		}
		try{
			transporteExcel = vitroReportService.generarReporteVitro(polizasVitro, fileName);
		} catch(Exception err){
			LOG.error("No se pudo Generar el Excel", err);
		}
		
		return SUCCESS;
	}

	public ReportesVitroService getVitroReportService() {
		return vitroReportService;
	}

	public void setVitroReportService(ReportesVitroService vitroReportService) {
		this.vitroReportService = vitroReportService;
	}

	public List<Dpngrupo> getGruposVitro() {
		return gruposVitro;
	}

	public void setGruposVitro(List<Dpngrupo> gruposVitro) {
		this.gruposVitro = gruposVitro;
	}

	public List<Date> getCobros() {
		return cobros;
	}

	public void setCobros(List<Date> cobros) {
		this.cobros = cobros;
	}

	public List<Negocio> getNegocios() {
		return negocios;
	}

	public void setNegocios(List<Negocio> negocios) {
		this.negocios = negocios;
	}

	public List<Dpncliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Dpncliente> clientes) {
		this.clientes = clientes;
	}

	public List<ReportesVitroDTO> getPolizasVitro() {
		return polizasVitro;
	}

	public void setPolizasVitro(List<ReportesVitroDTO> polizasVitro) {
		this.polizasVitro = polizasVitro;
	}

	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getNombreGrupo() {
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo) {
		this.nombreGrupo = nombreGrupo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getIdGrupo() {
		return idGrupo;
	}

	public void setIdGrupo(Long idGrupo) {
		this.idGrupo = idGrupo;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public Long getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(Long idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
