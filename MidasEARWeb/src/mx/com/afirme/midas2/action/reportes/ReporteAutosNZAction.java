package mx.com.afirme.midas2.action.reportes;

import java.io.InputStream;
import java.util.Date;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReportMethods;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReporteAgenteBaseAction;
import mx.com.afirme.midas2.service.reportes.GeneracionReporteAutosNZService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ReporteAutosNZAction extends ReporteAgenteBaseAction implements Preparable,ReportMethods{
	
	private static final long serialVersionUID = 8298539106849639669L;
	private GeneracionReporteAutosNZService generacionReporteAutosNZService;
	private Date fechaInicial;
	private Date fechaFinal;
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	
	public String listar(){		
		return SUCCESS;
	}
	
	/**
	 * Metodo para generar informacion de siniestros autos,
	 * de Netezza 
	 * 	
	 * @return 
	 * @throws Exception
	 */
	public String generarReporte() {
		
		String mensaje = "";
		String usuario = "";
		
		try {
			usuario = usuarioService.getUsuarioActual().getNombreUsuario();
			int respuesta = generacionReporteAutosNZService.generarReporte(fechaInicial, fechaFinal, usuario);
			if ( respuesta == 1 ){
				mensaje = "Se programo la tarea nocturna.";
			}
			else{
				mensaje = "No se ha podido programar la tarea nocturna.";
				throw new Exception();
			}
			
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}
	
	
	public String generarInformacion() {
		
		String mensaje = "";
		InputStream inputStream;
		try {
			inputStream = generacionReporteAutosNZService.exportarReporte(fechaInicial, fechaFinal);
			
			plantillaInputStream = inputStream;
			contentType = "application/txt"; 
			fileName = "Reporte de Emisión del "+""+generacionReporteAutosNZService.getFechasReporte(2)+".txt";
		
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String mostrarFiltros() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToPDF() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToExcel() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	

	public GeneracionReporteAutosNZService getGeneracionReporteAutosNZService() {
		return generacionReporteAutosNZService;
	}
	@Autowired
	@Qualifier("generacionReporteAutosNZService")
	public void setGeneracionReporteAutosNZService(
			GeneracionReporteAutosNZService generacionReportesNZService) {
		this.generacionReporteAutosNZService = generacionReportesNZService;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
