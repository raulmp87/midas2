package mx.com.afirme.midas2.action.reportes;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.reportes.movil.cliente.ReporteClienteMovilService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

//@Namespace("/reportes/clienteMovil")
@Component
@Scope("prototype")
public class ReporteClienteMovilAction extends BaseAction implements Preparable{

private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = LoggerFactory.getLogger(ReporteClienteMovilAction.class);
	
	private TransporteImpresionDTO transporte;
	private String tipoPoliza;
	private List<NegocioSeguros> tipoPolizaLista = new ArrayList<NegocioSeguros>(1);
	
	@Override
	public void prepare() throws Exception {
			
	}

	public String init(){
		tipoPolizaLista = reportesClienteMovilService.getTipoPolizas();
		//negocios = vitroReportService.consultarNegocios();
		return SUCCESS;
	}
	public String exportExcel(){
		LOG.info("Tipo Poliza : " + getTipoPoliza());
		List<ClientePolizas> list = reportesClienteMovilService.getReportePolizasClienteMovil(tipoPoliza);
		ExcelExporter exporter = new ExcelExporter( ClientePolizas.class );			
		transporte =  exporter.exportXLS(list, "Reporte_APP_Movil_Afirme_Seguros");
		return SUCCESS;
	}
	
	@Autowired
	private ReporteClienteMovilService reportesClienteMovilService;

	public void setReportesClienteMovilService(
			ReporteClienteMovilService reportesClienteMovilService) {
		this.reportesClienteMovilService = reportesClienteMovilService;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	public List<NegocioSeguros> getTipoPolizaLista() {
		return tipoPolizaLista;
	}
	public void setTipoPolizaLista(List<NegocioSeguros> tipoPolizaLista) {
		this.tipoPolizaLista = tipoPolizaLista;
	}
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}

}
