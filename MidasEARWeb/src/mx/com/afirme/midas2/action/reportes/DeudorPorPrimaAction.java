package mx.com.afirme.midas2.action.reportes;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.reportes.ReporteService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones.GeneraPlantillaReporteSolicitudService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class DeudorPorPrimaAction extends BaseAction implements Preparable {

 
	private Date fechaReporte;
	private String nivel;
	private BigDecimal rango;
	private String fechaUltimaGeneracion;
	private Integer ejecutandoDXP;
	
	private InputStream solicitudInputStream;
	private String     fileName;
	private String      contentType;
	
	private GeneraPlantillaReporteSolicitudService generaPlantillaReporteSolicitudService;
	private ReporteService reporteService;
	 
	@Autowired
	@Qualifier("generaPlantillaReporteSolicitudServiceEJB")
	public void setGeneraPlantillaReporteSolicitudService(
			GeneraPlantillaReporteSolicitudService generaPlantillaReporteSolicitudService) {
		this.generaPlantillaReporteSolicitudService = generaPlantillaReporteSolicitudService;
	}
	
	@Autowired
	@Qualifier("reporteServiceEJB")
	public void setReporteService(ReporteService reporteService) {
		this.reporteService = reporteService;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Override
	public void prepare() throws Exception {
		if(fechaReporte == null){
			fechaReporte = new Date();
		}
		if(nivel == null){
			nivel = "R";
		}
		if(rango == null){
			rango = new BigDecimal(30);
		}
	}
	
	public String mostrar(){
		try{
			
			ejecutandoDXP = reporteService.getEjecutandoGeneracionDXP();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(ejecutandoDXP == null || ejecutandoDXP.intValue() == 0){
			try{
				Date fechaSP = reporteService.getUltimaFechaGeneracionDporP();
				if(fechaSP != null)
				{
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
					fechaUltimaGeneracion = sdf.format(fechaSP);					
				}else
				{
					List<String> mensajes = new ArrayList<String>();
					mensajes.add("No existen datos del DXP generados anteriormente, debe crear una nueva versión.");
					this.setMensajeListaPersonalizado("", mensajes, TIPO_MENSAJE_INFORMACION);
				}				
			
			}catch(Exception e){
				e.printStackTrace();
			}		
		}
		
		return SUCCESS;
	}
	
	 public String descargaExcel(){
		 
			try{
				//String urlString = ServletActionContext.getRequest().getRequestURL().toString();				
			   // TransporteImpresionDTO transporte = generaPlantillaReporteSolicitudService.impresionDeudorPrima(nivel, rango);
				InputStream i = generaPlantillaReporteSolicitudService.impresionDeudorPrima(nivel, new BigDecimal(0));
				setSolicitudInputStream(i);
			    //setSolicitudInputStream(new ByteArrayInputStream(transporte.getByteArray()));
				//setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			    //setContentType("application/vnd.openxml");
			    setContentType("application/octet-stream");
				//setFileName("DXP_"+nivel+".xlsx");		
			    Date fechaSP = reporteService.getUltimaFechaGeneracionDporP();
			    
			    setFileName("DXP_"+nivel+fechaSP.toString()+".txt");
			   
			}catch(Exception e){
				e.printStackTrace();
				setMensaje("Error al imprimir favor de intentarlo mas tarde");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return ERROR;
			}
			return SUCCESS;
	 }
	 
	 public String generaDXP(){
		 reporteService.generaDXP(fechaReporte);
		 try {
			    TimeUnit.SECONDS.sleep(2);
		} catch (Exception e) {
			   e.printStackTrace();
		}		 
		 return SUCCESS;
	 }
	

	public void setSolicitudInputStream(InputStream solicitudInputStream) {
		this.solicitudInputStream = solicitudInputStream;
	}

	public InputStream getSolicitudInputStream() {
		return solicitudInputStream;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFechaReporte(Date fechaReporte) {
		this.fechaReporte = fechaReporte;
	}

	public Date getFechaReporte() {
		return fechaReporte;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getNivel() {
		return nivel;
	}

	public void setFechaUltimaGeneracion(String fechaUltimaGeneracion) {
		this.fechaUltimaGeneracion = fechaUltimaGeneracion;
	}

	public String getFechaUltimaGeneracion() {
		return fechaUltimaGeneracion;
	}

	public void setEjecutandoDXP(Integer ejecutandoDXP) {
		this.ejecutandoDXP = ejecutandoDXP;
	}

	public Integer getEjecutandoDXP() {
		return ejecutandoDXP;
	}

	public void setRango(BigDecimal rango) {
		this.rango = rango;
	}

	public BigDecimal getRango() {
		return rango;
	}

	
}