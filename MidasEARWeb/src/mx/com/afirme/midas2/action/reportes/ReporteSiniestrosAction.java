package mx.com.afirme.midas2.action.reportes;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;


import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReportMethods;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReporteAgenteBaseAction;
import mx.com.afirme.midas2.service.reportes.GeneracionSiniestrosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ReporteSiniestrosAction extends ReporteAgenteBaseAction implements Preparable,ReportMethods{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8298539106849639669L;
	private GeneracionSiniestrosService generacionReportesNZService;
	private Date fechaInicial;
	private Date fechaFinal;
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	
	public String listar(){		
		return SUCCESS;
	}
	
	
	public String generarInformacion() {
		
		String mensaje = "";
		InputStream inputStream;
		try {
			inputStream = generacionReportesNZService.exportarReporte(fechaInicial, fechaFinal);
			
			plantillaInputStream = inputStream;
			contentType = "application/txt";
			fileName = "Reporte de Siniestros del "+getFechaStr(fechaInicial)+" al "+getFechaStr(fechaFinal)+".txt";
			
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String mostrarFiltros() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToPDF() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToExcel() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	

	public GeneracionSiniestrosService getGeneracionReportesNZService() {
		return generacionReportesNZService;
	}
	@Autowired
	@Qualifier("generacionReportesNZService")
	public void setGeneracionReportesNZService(
			GeneracionSiniestrosService generacionReportesNZService) {
		this.generacionReportesNZService = generacionReportesNZService;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFechaStr(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		return sdf.format(date);
	}
	
}
