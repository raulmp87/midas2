package mx.com.afirme.midas2.action.reportes;

import java.util.logging.Level;

import mx.com.afirme.midas.danios.reportes.MidasReporteBase;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasArchivosDTO;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.ReporteSesasArchivosId;


public class ReporteSesas extends MidasReporteBase{
	

	private FileManagerService fileManagerService;
	
	public ReporteSesas(){
		try {
			fileManagerService = ServiceLocator.getInstance().getEJB(
					FileManagerService.class);

		} catch (Exception e) {
			LogDeMidasWeb.log("Error en reporte Sesas Action: ",Level.SEVERE, e);
		}	
	}
	
		public void generarArchivosSesas() throws Exception {		
		try {
			String idGeneracionStr = null;
			
		//	idGeneracionStr = reporteSesasService.revisarPendientes();
			
			
			if  (idGeneracionStr!=null && idGeneracionStr.length()!=0) {
			
			int idGeneracion = Integer.parseInt(idGeneracionStr);
			int idArchivo;
			ReporteSesasArchivosId idArchivoSesas = new ReporteSesasArchivosId();

			String lista = null;
			byte[] arregloBytes = null;

			GeneracionSesasArchivosDTO reporteSesasArchivosDTO = new GeneracionSesasArchivosDTO();
			
				for (int i = 0; i < 6; i++) {
					ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
					controlArchivoDTO.setClaveTipo("0");
					idArchivo = i + 1;
					idArchivoSesas.setidArchivo(idArchivo);
					idArchivoSesas.setidGeneracion(idGeneracion);
				//	reporteSesasArchivosDTO = reporteSesasService.findById(idArchivoSesas);
					//reporteSesasService.getFileName
					//lista = reporteSesasService.getReporte("idArchivo", idGeneracion,"");
					lista="listado";
					arregloBytes = lista.getBytes();
					controlArchivoDTO.setNombreArchivoOriginal(reporteSesasArchivosDTO.getNomArchivo());
					ControlArchivoDN controlArchivoDN = ControlArchivoDN.getInstancia();
					controlArchivoDTO = controlArchivoDN.agregar(controlArchivoDTO);

					String IdControlArchivo = controlArchivoDTO.getIdToControlArchivo().toString();
					fileManagerService.uploadFile(IdControlArchivo + ".txt",IdControlArchivo + ".txt",arregloBytes);

					reporteSesasArchivosDTO.setNomArchivoFrtmx(IdControlArchivo);
				//	reporteSesasService.update(reporteSesasArchivosDTO);

				}
			}
			
	} catch (Exception ex) {
		LogDeMidasWeb.log("Excepcion encontrada mientras se ejecutaba el metodo ReporteSesasAction.llamarGenReportes la tarea Reportes Sesas. Instancia : " 
    			, Level.SEVERE, ex);
	}
	}	
	

}
