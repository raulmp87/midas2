package mx.com.afirme.midas2.action.reportes;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.reportes.ReporteEmisionView;
import mx.com.afirme.midas2.service.reportes.ReporteEmisionRestService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

public class EmisionRestAction extends BaseAction {

	private static final long serialVersionUID = 1L;

	private final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");

	@Autowired
	@Qualifier("reporteEmisionRestServiceEJB")
	private ReporteEmisionRestService reporteEmisionRest;
	@Autowired
	@Qualifier("usuarioServiceEJB")
	private UsuarioService usuarioService;

	private InputStream fileInputStream;
	private String token;
	private String initDate;
	private String endDate;
	private String claveAgente;

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getInitDate() {
		return initDate;
	}

	public void setInitDate(String initDate) {
		this.initDate = initDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public ReporteEmisionRestService getReporteEmisionRest() {
		return reporteEmisionRest;
	}

	public void setReporteEmisionRest(ReporteEmisionRestService reporteEmisionRest) {
		this.reporteEmisionRest = reporteEmisionRest;
	}

	public String getClaveAgente() {
		return claveAgente;
	}

	public void setClaveAgente(String claveAgente) {
		this.claveAgente = claveAgente;
	}

	public String execute() throws Exception {
		validate();

		final ReporteEmisionView filtro = new ReporteEmisionView();
		filtro.setClaveAgente(claveAgente);
		filtro.setFechaInicial(initDate);
		filtro.setFechaFinal(endDate);
		fileInputStream = reporteEmisionRest.imprimirReporteEmision(filtro);
		return SUCCESS;
	}
	
	public void validate() {

		if (StringUtils.isBlank(token) || !usuarioService.isTokenActive(token)) {
			addActionError("Invalid Token");
		}

		if (StringUtils.isBlank(initDate)) {
			addActionError("initDate is mandatory.");
		}

		if (StringUtils.isBlank(endDate)) {
			addActionError("endDate is mandatory.");
		}

		if (StringUtils.isBlank(claveAgente)) {
			addActionError("claveAgente is mandatory.");
		}

		if (hasErrors()) {
			throw new RuntimeException();
		}

		Date initD;
		Date endD;
		try {
			initD = SDF.parse(initDate);
			endD = SDF.parse(endDate);
		} catch (Exception e) {
			addActionError("Invalid date format");
			throw new RuntimeException();
		}

		long diff = endD.getTime() - initD.getTime();
		if (diff < 0) {
			addActionError("endDate must be grater than initDate");
		}
		if (TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) > 31) {
			addActionError("Range exceeded");
		}

		if (hasErrors()) {
			throw new RuntimeException();
		}

	}

}
