package mx.com.afirme.midas2.action.reportes;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialCobranzaDTO;
import mx.com.afirme.midas2.service.reportes.historial.cobranza.ReporteHistorialCobranzaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/reporte/historialCobranza")
@Component
@Scope("prototype")
public class ReporteHistorialCobranzaAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(ReporteHistorialCobranzaAction.class);

	@Override
	public void prepare() throws Exception {		
	}
	
	@Autowired
	public UsuarioService usuarioService;
	
	private ReporteHistorialCobranzaService historialService;	
	@Autowired
	@Qualifier("reporteHistorialCobranzaServiceEJB")
	public void setHistorialService(ReporteHistorialCobranzaService historialService) {
		this.historialService = historialService;
	}
	
	private TransporteImpresionDTO transporteExcel;
	
	@Action(value = "generarReporteHistorialCobranza", results = {@Result(name = SUCCESS, type="stream", 
			params={ "contentType","${transporteExcel.contentType}", "inputName","transporteExcel.genericInputStream", "contentDisposition", "attachment;filename=\"${transporteExcel.fileName}\""})})
	public String generarReporteHistorialCobranza(){
		List<ReporteHistorialCobranzaDTO> lista = new ArrayList<ReporteHistorialCobranzaDTO>();
		Agente agente = usuarioService.getAgenteUsuarioActual();
		Long idAgente = (agente != null && agente.getId() != null && agente.getId() > 0L) ? agente.getId() : 0L;
		
		lista = historialService.consultarHistCobranzaPorAgente(idAgente);
		try{
			transporteExcel = historialService.generarReporteCobranza(lista, "Rep_Historial_Cobranza");
		} catch(Exception err){
			log.error("No se pudo Generar el Excel", err);
		}

		return SUCCESS;
	}

	/**
	 * @return el transporteExcel
	 */
	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	/**
	 * @param transporteExcel el transporteExcel a establecer
	 */
	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
}
