package mx.com.afirme.midas2.action.reportes;

import java.util.LinkedList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReporteAgenteBaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/reportesAgentes")
public class ReportesAgentesAction extends ReporteAgenteBaseAction {
	
	private static final String GENERAR_REPORTES = "/jsp/reportesAgentes/generarReportesAgentes.jsp";
	
	private List<ValorCatalogoAgentes> tipoDocumentos;
	
	private boolean isPromotor;
	private Long idRegAgente;
	
	private static final long serialVersionUID = -2051134651226948628L;

	@Action(value = "mostrarReportesAgentes", results = { @Result(name = SUCCESS, location = GENERAR_REPORTES) })
	public String mostrarReportesAgentes() throws Exception{
		setCentroOperacionList(entidadService
				.findAll(CentroOperacion.class));
		setTipoPromotoria(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipos de Promotoria"));
		setTipoAgente(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipo de Agente"));
		setTipoDocumentos(valorCatalogoAgentesService
                .obtenerElementosPorCatalogo("Tipos Doc Generar Documentos"));

		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
				
		Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
		
		if (agenteUsuarioActual != null) {
			
			setIdPromotoria(agenteUsuarioActual.getPromotoria().getIdPromotoria());
			
			idRegAgente = agenteUsuarioActual.getId();
			
		}	
				
		isPromotor = usuarioService.tieneRolUsuarioActual("Rol_M2_Promotor");
				
		if (isPromotor) {
			
			idRegAgente = null;
			
		} 
			
		return SUCCESS;
		
	}

	/**
	 * @return the tipoDocumentos
	 */
	public List<ValorCatalogoAgentes> getTipoDocumentos() {
		return tipoDocumentos;
	}

	/**
	 * @param tipoDocumentos the tipoDocumentos to set
	 */
	public void setTipoDocumentos(List<ValorCatalogoAgentes> tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}

	public Boolean getIsPromotor() {
		return isPromotor;
	}

	public void setIsPromotor(Boolean isPromotor) {
		this.isPromotor = isPromotor;
	}

	/**
	 * @return the idRegAgente
	 */
	public Long getIdRegAgente() {
		return idRegAgente;
	}

	/**
	 * @param idRegAgente the idRegAgente to set
	 */
	public void setIdRegAgente(Long idRegAgente) {
		this.idRegAgente = idRegAgente;
	}
}
