package mx.com.afirme.midas2.action.reportes;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList; 
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReportMethods;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReporteAgenteBaseAction;
import mx.com.afirme.midas2.domain.reportes.reporteReservas.GeneracionVigorDTO;
import mx.com.afirme.midas2.service.reportes.GeneracionVigorService;
import mx.com.afirme.midas2.util.MidasException; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ReporteVigorAction extends ReporteAgenteBaseAction implements Preparable,ReportMethods{
	
	private static final long serialVersionUID = 8298539106849639669L;
	private GeneracionVigorService generacionVigorService;
	private Date fechaInicial;
	private Date fechaFinal;
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	private List<GeneracionVigorDTO> listTareasVigor = new ArrayList<GeneracionVigorDTO>();
	private BigDecimal option;
	private String resultado;
	
	/**
	 * Metodo para programar Tarea ejecucion
	 * de Reporte Vigor
	 * 	
	 * @return 
	 * @throws Exception
	 */
	public String programarTarea() {
		
		String mensaje = "";
		
		try {
			
			int respuesta = generacionVigorService.programarTarea(fechaInicial, fechaFinal, option.toString()+"");
			if ( respuesta == 1 ){
				mensaje = "Se programo la tarea nocturna.";
			}
			else{
				mensaje = "No se ha podido programar la tarea nocturna.";
				throw new Exception();
			}
			
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}
	
	/**
	 * Busca la ultima tarea y en caso de no ser existosa busca ademas
	 * la ultima exitosa.
	 * @return
	 */
	public String buscarTareasVigor(){
		
		String mensaje = "";
		try {
			 
			listTareasVigor = generacionVigorService.getTareasVigor(option);
			if ( listTareasVigor.size() == 0 ){
				mensaje = "No existen tareas programadas.";
			}
			
		} catch (MidasException e) {
			
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		
		super.setMensaje(mensaje);
		
		return SUCCESS;
	}
	
	/**
	 * Metodo para generar informacion de siniestros autos,
	 * de Netezza 
	 * 	
	 * @return 
	 * @throws Exception
	 */
	public String generarReporte() {
		
		String mensaje = "";
		String usuario = "";
		
		try {
			usuario = usuarioService.getUsuarioActual().getNombreUsuario();
			int respuesta = generacionVigorService.generarReporte(fechaInicial, fechaFinal, usuario);
			if ( respuesta == 1 ){
				mensaje = "Se programo la tarea nocturna.";
				resultado = mensaje;
			}
			else{
				mensaje = "No se ha podido programar la tarea nocturna.";
				resultado = mensaje + "\n" + "Hay un proceso en ejecucion.";
				throw new Exception();
			}
			
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}
	
	
	public String generarInformacion() {
		
		String mensaje = "";
		InputStream inputStream;
		try {
			inputStream = generacionVigorService.exportarReporte(fechaInicial, fechaFinal);
			if (inputStream == null)
				throw new Exception();
			
			plantillaInputStream = inputStream;
			contentType = "application/txt";
			fileName = "Reporte de Vigor Autos al "+generacionVigorService.getFechasReporte(1)+".txt";
			
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Override
	public String mostrarFiltros() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToPDF() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToExcel() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public GeneracionVigorService getGeneracionVigorService() {
		return generacionVigorService;
	}
	
	@Autowired
	@Qualifier("generacionVigorService")
	public void setGeneracionVigorService(
			GeneracionVigorService generacionSiniestroService) {
		this.generacionVigorService = generacionSiniestroService;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public List<GeneracionVigorDTO> getListTareasVigor() {
		return listTareasVigor;
	}

	public void setListTareasVigor(List<GeneracionVigorDTO> listTareasVigor) {
		this.listTareasVigor = listTareasVigor;
	}

	public BigDecimal getOption() {
		return option;
	}

	public void setOption(BigDecimal option) {
		this.option = option;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String listar(){		
		return SUCCESS;
	}
}
