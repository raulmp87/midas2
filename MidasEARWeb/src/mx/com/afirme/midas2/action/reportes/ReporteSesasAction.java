package mx.com.afirme.midas2.action.reportes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReportMethods;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente.ReporteAgenteBaseAction;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasDTO;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.ReporteSesasMatrizDTO;
import mx.com.afirme.midas2.service.reportes.GeneracionSesasService;
import mx.com.afirme.midas2.util.MidasException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ReporteSesasAction  extends ReporteAgenteBaseAction implements Preparable, ReportMethods {

	private static final long serialVersionUID = 970896486043200898L;
	private Date fechaInicial;
	private Date fechaFinal;
	private String modo;
	private String numArchivo;
	private String listArchivos;
	private String idGeneracion;
	private GeneracionSesasService generacionSesasService;
	private InputStream plantillaInputStream;
	private InputStream plantillaMatrizSesas;
	private String contentType;
	private String fileName;
	private String claveNeg;
	
	
	public String getClaveNeg() {
		return claveNeg;
	}

	public void setClaveNeg(String claveNeg) {
		this.claveNeg = claveNeg;
	}

	private List<GeneracionSesasDTO> listTareasSesas = new ArrayList<GeneracionSesasDTO>();
	@Autowired
	@Qualifier("generacionSesasServiceEJB")
	public void setGeneracionSesasService(
			GeneracionSesasService generacionSesasService) {
		this.generacionSesasService = generacionSesasService;
	}

	public String listar(){		
		
		if (this.getClaveNeg().equals("V"))
		{
			return "success2";	
			
		}
		else
		{
		    return SUCCESS;
		}
	}
	
	@Override
	public String mostrarFiltros() {
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public String exportarToExcel() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}
	
	
	/**
	 * Busca la ultima tarea y en caso de no ser existosa busca ademas
	 * la ultima exitosa.
	 * @return
	 */           
	public String generaArchivosSesas(){	
		generacionSesasService.generarArchivosSesas(this.getClaveNeg());
		return SUCCESS;
	}
	
	/**
	 * Busca la ultima tarea y en caso de no ser existosa busca ademas
	 * la ultima exitosa.
	 * @return
	 */
	public String buscarTareasSesas(){
		
		
		String mensaje = "";
		try {
			
			listTareasSesas = generacionSesasService.getTareasSesas(this.getClaveNeg());
			if ( listTareasSesas.size() == 0 ){
				mensaje = "No existen tareas programadas.";
			}
			
		} catch (MidasException e) {
			
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		
		super.setMensaje(mensaje);
		if(this.getClaveNeg().equalsIgnoreCase("V"))
			return "success2";	
		else
			return SUCCESS;
		
	}
	
	/**
	 * Metodo para inicializar la tarea nocturna, en el dia actual
	 * y la hora definida en la tabla:
	 * TOPARAMETROGENERAL donde idtogrupoparametrogeneral = 13 
	 * y descripcionparametrogeneral = 'Hora de inicio del Job Genera Sesas' 
	 * @return 
	 * @throws Exception
	 */
	public String generarInformacion() {
		
		String mensaje = "";
		String usuario = "";
		String claveNeg = "";
		
		
		claveNeg = this.getClaveNeg();
		
		try {
			usuario = usuarioService.getUsuarioActual().getNombreUsuario();
			int respuesta = generacionSesasService.iniciarJob(fechaInicial, fechaFinal, usuario, claveNeg);
			if ( respuesta == 1 ){
				mensaje = "Se programo la tarea nocturna.";
			}
			else{
				if ( respuesta == 2 ){
					mensaje = "Ya existe una tarea programada.";
				}
				else{
					mensaje = "No se ha podido programar la tarea nocturna.";
				}
				
			}
			
		} catch (Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		super.setMensaje(mensaje);
		return SUCCESS;
		
	}
	
	public String generarReporte(){
		
		try {			
			InputStream plantillaBytes = generacionSesasService.generarReporte( listArchivos, idGeneracion,claveNeg );
			if(plantillaBytes == null){
				return ERROR;
			}
			plantillaInputStream = plantillaBytes;
			contentType = "application/zip";
			if (claveNeg.equals("V"))
			{
				setFileName("ReporteSesasVida_"+ modo + ".zip");
			}
			else if (claveNeg.equals("A"))
			{
				setFileName("ReporteSesasAutos_"+ modo + ".zip");
			}
			return SUCCESS;
			
		} catch (MidasException e) {
			e.printStackTrace();
		}
		
		return ERROR;
	}
	
public String generarReporteVida(){
	int idArchivo=listArchivos!=null?Integer.parseInt(listArchivos.trim()):0;
	int idGeneracion=this.idGeneracion!=null?Integer.parseInt(this.idGeneracion.trim()):0;
	InputStream inputStream;
	try {
		inputStream = generacionSesasService.generarReporteVida(idArchivo);
		if (inputStream == null)
			new Exception();
		
		plantillaInputStream = inputStream;
		contentType = "application/txt";
		fileName = generacionSesasService.getNombreReporteVida(idArchivo, idGeneracion);
		return SUCCESS;			

	} catch (Exception e) {
		e.printStackTrace();

	}
	return ERROR;


	}
	
	public String generarMatriz(){
		
		try {
			int intIdGeneracion = Integer.parseInt(idGeneracion);
			ReporteSesasMatrizXLS MatrizXLS = new ReporteSesasMatrizXLS();
			
			byte[] reportesSp = MatrizXLS.obtenerReporte(intIdGeneracion);
			plantillaInputStream= new ByteArrayInputStream(reportesSp);
			if(reportesSp == null){
				return ERROR;
			}
			contentType = "application/xls";
			fileName="Matriz sesas.xls";
			//this.writeBytes(response, reportesSp, Sistema.TIPO_XLS,"Matriz");
			return SUCCESS;
			
		} catch (Exception e) {
			LOG.error("Error en reporte Sesas Action: ", e);
		} 
		
		
		return ERROR;
	}
	
	public String generarReportePas(){
		
		try {
			
			InputStream plantillaBytes = generacionSesasService.generarReporte( modo, numArchivo, claveNeg );
			if(plantillaBytes == null){
				return ERROR;
			}

			plantillaInputStream = plantillaBytes;
			contentType = "application/zip";
			setFileName("ReporteSesas_"+ modo + " Archivo_" + numArchivo+ ".zip");	
			return SUCCESS;
			
		} catch (MidasException e) {
			e.printStackTrace();
		}
		
		return ERROR;
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getModo() {
		return modo;
	}

	public void setModo(String modo) {
		this.modo = modo;
	}
	public String getListArchivos() {
		return listArchivos;
	}

	public void setListArchivos(String listArchivos) {
		this.listArchivos = listArchivos;
	}

	public String getIdGeneracion() {
		return idGeneracion;
	}

	public void setIdGeneracion(String idGeneracion) {
		this.idGeneracion = idGeneracion;
	}
	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<GeneracionSesasDTO> getListTareasSesas() {
		return listTareasSesas;
	}

	public void setListTareasSesas(List<GeneracionSesasDTO> listTareasSesas) {
		this.listTareasSesas = listTareasSesas;
	}

	public String getNumArchivo() {
		return numArchivo;
	}

	public void setNumArchivo(String numArchivo) {
		this.numArchivo = numArchivo;
	}
}
