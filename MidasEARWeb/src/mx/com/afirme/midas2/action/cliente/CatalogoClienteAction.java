package mx.com.afirme.midas2.action.cliente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilFacadeRemote;
import mx.com.afirme.midas.catalogos.giro.CatalogoGiroDTO;
import mx.com.afirme.midas.catalogos.giro.CatalogoGiroFacadeRemote;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.catalogos.sector.SectorFacadeRemote;
import mx.com.afirme.midas.catalogos.tipodomiciliacion.TipoDomiciliacionDTO;
import mx.com.afirme.midas.catalogos.tipodomiciliacion.TipoDomiciliacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.ClientSystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.OcupacionCNSFDTO;
import mx.com.afirme.midas2.clientesapi.exception.RegisterClientException;
import mx.com.afirme.midas2.clientesapi.exception.ValidateClientException;
import mx.com.afirme.midas2.service.negocio.cliente.NegocioClienteService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;



import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;




import com.asm.ejb.ASMRemote;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;

@Namespace("/catalogoCliente")
@Component
@Scope("prototype")
public class CatalogoClienteAction extends BaseAction implements Preparable{
	private static final long serialVersionUID = 1L;
	private ClienteUnicoDTO cliente;
	private ClienteGenericoDTO clienteDom;
	private ClienteFacadeRemote clienteFacade;
	private ClientesApiService  clienteRest;
	private EstadoFacadeRemote estadoFacade;
	private CiudadFacadeRemote ciudadFacade;
	private ColoniaFacadeRemote coloniaFacade;
	private PaisFacadeRemote paisFacade;
	private SectorFacadeRemote sectorFacade;
	private CatalogoGiroFacadeRemote catalogoGiroFacade;
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	private EstadoCivilFacadeRemote estadoCivilFacade;
	private TipoDomiciliacionFacadeRemote tipoDomiciliacionFacade;
	private final String VER_DETALLE_JSP="/jsp/cliente/catalogoClientes/detalleCliente.jsp";
	private final String VER_DETALLE_JSP_PF="/jsp/cliente/catalogoClientes/detalleClienteTabsPersonaFisica.jsp";
	private final String VER_DETALLE_JSP_PM="/jsp/cliente/catalogoClientes/detalleClienteTabsPersonaMoral.jsp";
	private final String MOSTRAR_CATALOGO_JSP="/jsp/cliente/catalogoClientes/listadoClientes.jsp";
	private final String CLIENTE_GRID="/jsp/cliente/catalogoClientes/clientesGrid.jsp";
	private final String CLIENTES_DUPLICADOS="/jsp/cliente/catalogoClientes/listadoClientesDuplicados.jsp";
	private final String CLIENTES_DUPLICADOS_AGENTES="/jsp/suscripcion/cotizacion/auto/agente/include/listadoClientesDuplicados.jsp";
	private final String SUCCESS_AGENTES="/jsp/suscripcion/cotizacion/auto/agente/include/guardadoFin.jsp";
	
	/***Componentes************************************/
	private String tipoAccion;
	private String idField;
	private Long idLineaNegocio;
	private List<PaisDTO> paises=new ArrayList<PaisDTO>();
	private List<EstadoDTO> estados=new ArrayList<EstadoDTO>();
	private List<EstadoDTO> estadosNacimiento=new ArrayList<EstadoDTO>();
	private List<EstadoDTO> estadosDir=new ArrayList<EstadoDTO>();
	private List<EstadoDTO> estadosFiscal=new ArrayList<EstadoDTO>();
	
	private List<CiudadDTO> ciudades=new ArrayList<CiudadDTO>();
	private List<CiudadDTO> ciudadesNac=new ArrayList<CiudadDTO>();
	
	private List<CiudadDTO> ciudadesDir=new ArrayList<CiudadDTO>();
	private List<CiudadDTO> ciudadesFiscal=new ArrayList<CiudadDTO>();
	private List<ColoniaDTO> colonias=new ArrayList<ColoniaDTO>();
	private List<ColoniaDTO> coloniasDir=new ArrayList<ColoniaDTO>();
	private List<ColoniaDTO> coloniasFiscal=new ArrayList<ColoniaDTO>();
	private List<SectorDTO> sectores=new ArrayList<SectorDTO>();
	private Integer tipoPersona;
	private List<OcupacionCNSFDTO>ocupacionesCNSF= new  ArrayList<OcupacionCNSFDTO>();
	private List<CatalogoGiroDTO> giros=new ArrayList<CatalogoGiroDTO>();
	private List<BancoEmisorDTO> bancos=new ArrayList<BancoEmisorDTO>(1);
	private List<EstadoCivilDTO> estadosCiviles=new ArrayList<EstadoCivilDTO>();
	private List<String> camposRequeridos=new ArrayList<String>();
	private List<ClienteGenericoDTO> mediosPagoCliente=new ArrayList<ClienteGenericoDTO>();
	private List<ClienteUnicoDTO> listaClientes=new ArrayList<ClienteUnicoDTO>();
	private ClienteUnicoDTO filtroCliente;
	private List<TipoDomiciliacionDTO> mediosPagoDomiciliacion=new ArrayList<TipoDomiciliacionDTO>();
	private String divCarga;
	private String tabActiva;
	private Integer moduloOrigen;
	private Long idAgente;
	private String fechaActual;
	//Regreso Negocio
	private Short tipoRegreso=0;
	private Long idNegocio;
	private NegocioClienteService negocioClienteService;
	private BigDecimal idToCotizacion;
	private CotizacionService cotizacionService;
	//
	private BigDecimal idToPoliza;
	private Date validoEn;	
    private Date recordFrom;
    private Long validoEnMillis;
    private Long recordFromMillis;
    private Short claveTipoEndoso;
	private Long tipoOperac ;
	private Long tipoAccionAgente;
	
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private boolean correoObligatorio;
	
	private String primaJSON;

	
	public void prepare() throws Exception {
		if(tipoAccion!=null && tipoAccion.equals("1") && (tabActiva ==null || tabActiva.equals(""))){
			cliente=new ClienteUnicoDTO();
		}
	}
	@Action(value="guardarCliente",results={
		@Result(name=SUCCESS,type="redirectAction",params={"actionName","loadById","namespace","/catalogoCliente","cliente.idCliente","${cliente.idCliente}","idLineaNegocio","${idLineaNegocio}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","divCarga","${divCarga}","idAgente","${idAgente}","moduloOrigen","${moduloOrigen}","tipoAccionAgente","${tipoAccionAgente}","tipoAccion","${tipoAccion}"}),
		//@Result(name=INPUT,type="redirectAction",params={"actionName","mostrarDetalleCliente","namespace","/catalogoCliente","cliente.idCliente","${cliente.idCliente}","idLineaNegocio","${idLineaNegocio}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
		@Result(name=INPUT,type="redirectAction",params={"actionName","loadById","namespace","/catalogoCliente","cliente.idCliente","${cliente.idCliente}","idLineaNegocio","${idLineaNegocio}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","divCarga","${divCarga}","idAgente","${idAgente}","moduloOrigen","${moduloOrigen}","tipoAccionAgente","${tipoAccionAgente}","tipoAccion","${tipoAccion}"}),
		@Result(name="fallasPersonaMoral",location=VER_DETALLE_JSP_PM),
		@Result(name="fallasPersonaFisica",location=VER_DETALLE_JSP_PF),
		@Result(name="unsuccessful",location=VER_DETALLE_JSP),
		@Result(name="regresoNegocio",type="redirectAction",params={"actionName","verDetalle","namespace","/negocio","claveNegocio","A","id","${idNegocio}","tabActiva","clientes","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
		@Result(name="regresoCotizacionComp",type="redirectAction",params={"actionName","iniciarComplementar","namespace","/suscripcion/cotizacion/auto/complementar","cotizacionId","${idToCotizacion}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
		@Result(name="changeCotizacion",type="redirectAction",params={"actionName","cambiarEstatusEnProceso","namespace","/suscripcion/cotizacion/auto","id","${idToCotizacion}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
		@Result(name="regresoCotizacionAgente",type="redirectAction",params={"actionName","mostrarContenedor","namespace","/suscripcion/cotizacion/auto/cotizadoragente","idToCotizacion","${idToCotizacion}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}", "tabActiva", "${tabActiva}"}),
		@Result(name="regresoCotizacionAgenteNew", location="/jsp/suscripcion/cotizacion/auto/agente/include/agregarCliente.jsp"),
		@Result(name="clientesDuplicados",location=CLIENTES_DUPLICADOS),
		@Result(name="clientesDuplicadosAgente",location=CLIENTES_DUPLICADOS_AGENTES),
		@Result(name="unsuccessfulAgentes",location="/jsp/suscripcion/cotizacion/auto/agente/include/agregarCliente.jsp"),
		@Result(name="successAgentes",location=SUCCESS_AGENTES)
		
		    
		
	},
		interceptorRefs={
			@InterceptorRef(value="defaultStack"),
			@InterceptorRef(value="store",params={"operationMode","STORE"})
		}
	)
	public String guardarCliente(){
		inicializarCampos();//Una vez que se guarde sin importar si se guardo correctamente o no, se vuelve a renderear los campos
		tabActiva=(tabActiva!=null && !tabActiva.trim().isEmpty())?tabActiva:"datosGenerales";
		divCarga=(divCarga!=null && !divCarga.trim().isEmpty() && (divCarga.split(",")!=null && divCarga.split(",").length>0 && !divCarga.split(",")[0].isEmpty()))?divCarga:null;
		Short regresoCotizacionAgenteNew= 5;
		//Si viene de Negocio
		Short regresoNegocio = 1;
		Short regresoCotizacionComp = 2;
		Short regresoCotizacionAgente = 4;
		try {
			if(cliente!=null){
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdClienteString())){
					cliente.setIdCliente(new BigDecimal(cliente.getIdClienteString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdToPersonaString())){
					cliente.setIdToPersona(new BigDecimal(cliente.getIdToPersonaString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getClaveTipoPersonaString())){
					cliente.setClaveTipoPersona(Short.parseShort(cliente.getClaveTipoPersonaString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdEstadoNacimientoString())){
					cliente.setIdEstadoNacimiento(new BigDecimal(cliente.getIdEstadoNacimientoString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdColoniaString())){
					cliente.setIdColonia(new BigDecimal(cliente.getIdColoniaString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdMunicipioString())){
					cliente.setIdMunicipio(new BigDecimal(cliente.getIdMunicipioString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdEstadoString())){
					cliente.setIdEstado(new BigDecimal(cliente.getIdEstadoString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getCodigoRFC())){
					cliente.setCodigoRFCFiscal(cliente.getCodigoRFC());
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getTelefono())){
					cliente.setTelefonoFiscal(cliente.getTelefono());
				}
			}
			
			boolean validaNegocio=false;
			if("consulta".equals(tipoAccion)){
				validaNegocio=true;
			}
			//CAMBIO se pone que siempre se valide el negocioaddClientDiv
			validaNegocio=true;
			
			//Cambio para cliente unico, se agrega llamado a cliente rest
			
			try{
				//se agrega caso para cotizador agentes, ya que no se captura el municipio
				if(cliente.getClaveCiudadNacimiento()==null){
						List<CiudadDTO> ciudades=ciudadFacade.findByProperty("stateId",cliente.getClaveEstadoNacimiento());
							if(ciudades!=null &&ciudades.size()>0){
								cliente.setClaveCiudadNacimiento(ciudades.get(0).getCityId());
								
							}
					
				}
				
			cliente=clienteRest.saveFullDataClienteUnico(cliente, null, validaNegocio);
			
			
			}
			
			catch (ValidateClientException e) {
				
				e.printStackTrace();
				
				super.setMensajeError(e.getMessage());
				if(tipoRegreso != null && tipoRegreso.equals(regresoCotizacionAgenteNew)){
					
					return "unsuccessfulAgentes";
				}
				
				return "unsuccessful";
			}
			catch (RegisterClientException e) {
				
				 ClienteUnicoDTO[] temp=
					
				new Gson().fromJson(e.getMessage(), ClienteUnicoDTO[].class);
				 listaClientes = Arrays.asList(temp);
				 
				 if(tipoRegreso != null && tipoRegreso.equals(regresoCotizacionAgenteNew)){
						
						return "clientesDuplicadosAgente";
					}
				return "clientesDuplicados";
			}
			
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			if(tipoRegreso != null && tipoRegreso.equals(regresoCotizacionAgenteNew)){
			
				super.setMensajeExito();
				return "successAgentes";
			}
			if(tipoRegreso != null && tipoRegreso.equals(regresoNegocio)){
				if(idNegocio != null){
					if(negocioClienteService.relacionarClienteNegocio(Long.valueOf(idNegocio), cliente.getIdCliente())){
						super.setMensajeExito();
						return "regresoNegocio";
					}else{
						super.setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
					}
				}
			}
			if(tipoRegreso != null && tipoRegreso.equals(regresoCotizacionComp)){
				if(idToCotizacion != null){
					if(cotizacionService.actualizarDatosContratanteCotizacion(idToCotizacion, cliente.getIdCliente(), this.getUsuarioActual().getNombreUsuario()) == null){
						setMensajeError("confirm:message:Favor de complementar el C\u00F3digo postal en los datos fiscales del cliente seleccionado, \uu00BFDesea hacerlo ahora\u003F");
						setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
						setNextFunction("complementarContratante("+cliente.getIdCliente()+","+idToCotizacion+")");
						this.tabActiva = "complementar";
						return "regresoCotizacionComp";
					}
					super.setMensajeExito();
					// Si el nuevo contratante tiene un iva valida si cambio
					try{
						boolean cambioIVA = cotizacionService.validaIVACotizacion(idToCotizacion, this.getUsuarioActual().getNombreUsuario());
						if(cambioIVA){		
							super.setMensajeExitoPersonalizado("IVA ajustado debido a domicilio fiscal del Contrante, favor de recalcular la cotizaci\u00f3n.");
							this.tabActiva = "detalle";
							return "regresoCotizacionComp";	
						}else{
							return "regresoCotizacionComp";
						}
					}catch(Exception ex){
						super.setMensajeError("EL CLIENTE SELECCIONADO NO CUENTA CON INFORMACION DEL DOMICILIO, POR LO CUAL EL SISTEMA TOMARA EL IVA DEL 16%.");
						super.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
						return "regresoCotizacionComp";
					}	
				}
			}
			
			if(tipoRegreso != null && tipoRegreso.equals(regresoCotizacionAgente)){
				if(idToCotizacion != null){
					if(cotizacionService.actualizarDatosContratanteCotizacion(idToCotizacion, cliente.getIdCliente(), this.getUsuarioActual().getNombreUsuario()) == null){
						setMensajeError("confirm:message:Favor de complementar el C\u00F3digo postal en los datos fiscales del cliente seleccionado, \uu00BFDesea hacerlo ahora\u003F");
						setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
						setNextFunction("complementarContratante("+cliente.getIdCliente()+","+idToCotizacion+")");
						return "regresoCotizacionAgente";
					}
					super.setMensajeExito();
					// Si el nuevo contratante tiene un iva valida si cambio
					try{
						boolean cambioIVA = cotizacionService.validaIVACotizacion(idToCotizacion, this.getUsuarioActual().getNombreUsuario());
						if(cambioIVA){		
							super.setMensajeExitoPersonalizado("IVA ajustado debido a domicilio fiscal del Contrante, favor de terminar la cotizaci\u00f3n.");
							return "regresoCotizacionAgente";	
						}else{
							return "regresoCotizacionAgente";
						}
					}catch(Exception ex){
						super.setMensajeError("EL CLIENTE SELECCIONADO NO CUENTA CON INFORMACION DEL DOMICILIO, POR LO CUAL EL SISTEMA TOMARA EL IVA DEL 16%.");
						super.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
						return "regresoCotizacionAgente";
					}	
				}
			}
			
		} 
		
		
		catch (Exception e) {
			BigDecimal id=cliente.getIdCliente();
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			if(e instanceof ClientSystemException){
				ClientSystemException ex=(ClientSystemException)e;
				if(ex!=null && ex.getErrorList()!=null && !ex.getErrorList().isEmpty()){
					setMensajeMapaErrorPersonalizado("Se ha producido un error!",ex.getErrorList(), MENSAJE_ERROR_GENERAL);
				}
				Integer idCliente=ex.getIdCliente();
				id=(idCliente!=null)?new BigDecimal(idCliente):null;
				cliente.setIdCliente(id);
			}else{
				e.printStackTrace();
			}
			if(tipoRegreso != null && tipoRegreso.equals(regresoCotizacionAgenteNew)){
				super.setMensajeExito();
				return "regresoCotizacionAgenteNew";
			}else{
				return INPUT;
			}
			
			
		}
		  setTipoAccion("4");
		return SUCCESS;
	}
	/**
	 * Accion para guardar los datos de un contacto en caso de un siniestro
	 * @return
	 */
	public String guardarContactoAvisoSiniestro(){
		String accion=SUCCESS;
		try {
			boolean validaNegocio=false;
			if("consulta".equals(tipoAccion)){
				validaNegocio=true;
			}
			clienteFacade.guardarDatosAvisoSiniestro(cliente,getUsuarioActual().getNombreUsuario(),validaNegocio);
			setMensaje(getText("midas.cliente.datosAvisoSiniestro.exito"));
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		} catch (Exception e) {
			if(e instanceof ClientSystemException){
				ClientSystemException ex=(ClientSystemException)e;
				if(ex!=null && ex.getErrorList()!=null && !ex.getErrorList().isEmpty()){
					setMensajeMapaErrorPersonalizado("Se ha producido un error!",ex.getErrorList(), MENSAJE_ERROR_GENERAL);
				}
			}else{
				setMensaje(getText("midas.cliente.datosAvisoSiniestro.error")+",casusado por:"+e.getMessage());
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				e.printStackTrace();
			}
		}
		return accion;
	}
	/**
	 * Accion para obtener los campos requeridos por Ajax-Json
	 * @return
	 */
	public String cargarCamposObligatorios(){
		try {
			camposRequeridos=clienteFacade.obtenerCamposRequeridos(cliente);
			//HttpServletResponse response=ServletActionContext.getResponse();
			//UtileriasWeb.sendJSON(response,camposRequeridos);
		} catch (Exception e) {
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * Accion para guardar los datos de un contacto en caso de un siniestro
	 * @return
	 */
	public String guardarDatosCobranza(){
		String accion=SUCCESS;
		Long idConducto = null;
		try {
			boolean validaNegocio=false;
			if("consulta".equals(tipoAccion)){
				validaNegocio=true;
			}
			validaNegocio=true;
			idConducto = clienteFacade.guardarDatosCobranza(cliente,getUsuarioActual().getNombreUsuario(),validaNegocio);
			if(idConducto != null){
				setMensaje(getText("midas.cliente.datosCobranza.exito"));
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			}
		} catch (Exception e) {
			if(e instanceof ClientSystemException){
				ClientSystemException ex=(ClientSystemException)e;
				if(ex!=null && ex.getErrorList()!=null && !ex.getErrorList().isEmpty()){
					setMensajeMapaErrorPersonalizado("Se ha producido un error!",ex.getErrorList(), MENSAJE_ERROR_GENERAL);
				}
			}else{
				setMensaje(getText("midas.cliente.datosCobranza.error")+",casusado por:"+e.getMessage());
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				e.printStackTrace();
			}
		}
		return accion;
	}
	/**
	 * Action para mostrar el detalle del cliente, se carga los datos del cliente en caso de que se haya elegido uno,
	 * sino, entonces es nuevo cliente.
	 * @return
	 */
	public String mostrarDetalleCliente(){
		
		inicializarCampos();
		if(cliente!=null && cliente.getIdCliente()!=null){
			try {//Se cambio implementacion para que que consuma servicio rest
				clienteDom=clienteFacade.loadById(cliente);
				
				cliente=clienteRest.findClienteUnicoById(cliente.getIdCliente().longValue());
				cliente.setIdDomicilio(clienteDom.getIdDomicilio());
				cliente.setIdRepresentante(clienteDom.getIdRepresentante());
				cliente.setNombreContacto(clienteDom.getNombreContacto());
				
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}
		return SUCCESS;
	}
	
	private void inicializarCampos(){
		setFechaActual(UtileriasWeb.getFechaString(new Date()));
		cargarPaises();
//		cargarEstados((cliente!=null)?cliente.getIdPaisString():null);
		if(cliente==null || (cliente!=null && (cliente.getClaveNacionalidad()==null || cliente.getClaveNacionalidad().isEmpty()))){
			if(cliente==null){
				cliente=new ClienteUnicoDTO();
				cliente.setEstadoCivil("S"); //Opcion default para clientes nuevos.
				cliente.setSexo("M");
			}
			cliente.setClaveNacionalidad("PAMEXI");
			cliente.setIdPaisConstitucion("PAMEXI");
			cliente.setIdPaisNacimiento("PAMEXI");
			
			cliente.setIdPaisString("PAMEXI");
			cliente.setTipoSituacionString("A");
			if((nombre != null && nombre.trim().length() >0) || (apellidoPaterno != null && apellidoPaterno.trim().length() > 0) || (apellidoMaterno != null && apellidoMaterno.trim().length() > 0)){
				cliente.setNombre(nombre);
				cliente.setApellidoPaterno(apellidoPaterno);
				cliente.setApellidoMaterno(apellidoMaterno);
			}
		}
		sectores=sectorFacade.findAll();
		estadosCiviles=estadoCivilFacade.findAll();
		giros=catalogoGiroFacade.findAll();
		bancos=bancoEmisorFacade.findAll();
		mediosPagoDomiciliacion=tipoDomiciliacionFacade.findAll();
		ocupacionesCNSF=clienteRest.getGiros();
		cargarEstadosNacimiento("PAMEXI");
		
		
	}
	
	private void cargarPaises(){
		paises=paisFacade.findAll();
	}
	
	private void cargarEstados(String countryId){
		countryId=(countryId!=null && !countryId.isEmpty())?countryId:"PAMEXI";
		estados=estadoFacade.findByProperty("countryId",countryId);
	}
	
	/**
	 * Establece la lista estadosNacimiento asociada a la  clave del país
	 * @param countryId identificador de pais seis caracteres por ejemplo PAMEXI
	 * */
	private void cargarEstadosNacimiento(String countryId){
		countryId=(countryId!=null && !countryId.isEmpty())?countryId:"PAMEXI";
		estadosNacimiento=estadoFacade.findByProperty("countryId",countryId);
	}
	/**
	 * Establece la lista estadosDir asociada a la  clave del país
	 * @param countryId identificador de pais seis caracteres por ejemplo PAMEXI
	 * */
	private void cargarEstadosDir(String countryId){
		countryId=(countryId!=null && !countryId.isEmpty())?countryId:"PAMEXI";
		estadosDir=estadoFacade.findByProperty("countryId",countryId);
	}
	/**
	 * Establece la lista estadosFiscal asociada a la clave del país
	 * @param countryId identificador de pais seis caracteres por ejemplo PAMEXI
	 * */
	private void cargarEstadosFiscal(String countryId){
		countryId=(countryId!=null && !countryId.isEmpty())?countryId:"PAMEXI";
		estadosFiscal=estadoFacade.findByProperty("countryId",countryId);
	}
	
	private void cargarCiudades(BigDecimal idEstado,String zipCode){
		if(idEstado!=null && !UtileriasWeb.esCadenaVacia(zipCode)){
			EstadoDTO estado=estadoFacade.findById(idEstado);
			if(estado!=null){
				ciudades=ciudadFacade.findByProperty("stateId",idEstado);
			}
		}
	}
	/**
	 * Establece la lista ciudadesNac asociada a la clave del estado
	 * @param idEstado de estado 
	 * */
	private void cargarCiudadesNacimiento(String idEstado){
		
		ciudadesNac=ciudadFacade.findByProperty("stateId",idEstado);
		
		
	}
	/**
	 * Establece la lista ciudadesDir asociada a la clave del estado
	 * @param idEstado de estado 
	 * */
	private void cargarCiudadesDir(String idEstado){
				ciudadesDir=ciudadFacade.findByProperty("stateId",idEstado);
			
		
	}
	/**
	 * Establece la lista ciudadesFiscal asociada a la clave del estado
	 * @param idEstado de estado 
	 * */
	private void cargarCiudadesFiscal(String idEstado){
		
				ciudadesFiscal=ciudadFacade.findByProperty("stateId",idEstado);
			
		
	}
	
	
	
	private void cargarColonias(BigDecimal idCiudad){
		if(idCiudad!=null){
			colonias=coloniaFacade.findByProperty("cityId", idCiudad);
		}
	}
	/**
	 * Establece la lista coloniasDir asociada a la clave del estado
	 * @param idCiudad de la ciudad (Municipio) 
	 * */
	private void cargarColoniasDir(String idCiudad){
		if(idCiudad!=null){
			coloniasDir=coloniaFacade.findByProperty("cityId", idCiudad);
		}
	}
	/**
	 * Establece la lista coloniasFiscal asociada a la clave del estado
	 * @param idCiudad de la ciudad (Municipio) 
	 * */
	private void cargarColoniasFiscal(String idCiudad){
		if(idCiudad!=null){
			coloniasFiscal=coloniaFacade.findByProperty("cityId", idCiudad);
		}
	}
	/**
	 * Action para hacer el switch de las pantallas de persona fisica a moral segun sea el caso
	 * @return
	 */
	@Action(value="switchTipoPersona",results={
		@Result(name="personaFisica",location="/jsp/cliente/catalogoClientes/detalleClienteTabsPersonaFisica.jsp"),
		@Result(name="personaMoral",location="/jsp/cliente/catalogoClientes/detalleClienteTabsPersonaMoral.jsp")
	})
	public String switchTipoPersona(){
		String accion="personaFisica";
		Short claveTipoPersona=null;
		if(tipoPersona==2){
			accion="personaMoral";
			claveTipoPersona=new Short("2");
		}else{
			claveTipoPersona=new Short("1");
		}
		if(cliente!=null){
			cliente.setClaveTipoPersona(claveTipoPersona);
		}
		inicializarCampos();
		cliente.setClaveNacionalidad("PAMEXI");
		cliente.setIdPaisConstitucion("PAMEXI");
		cliente.setIdPaisNacimiento("PAMEXI");
		return accion;
	}
	/**
	 * Action para consultar medios de pago.
	 * @return
	 */
	public String consultarMediosPago(){
		if(cliente!=null && cliente.getIdCliente()!=null){
			try {
				mediosPagoCliente=clienteFacade.loadMediosPagoPorCliente(cliente, null);
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}
		return SUCCESS;
	}
	
	@Action(value="loadById",results={
		@Result(name=SUCCESS,location=VER_DETALLE_JSP),
		@Result(name=INPUT,location=MOSTRAR_CATALOGO_JSP),
		@Result(name="SUCCES_AGENTE",location="/jsp/suscripcion/cotizacion/auto/agente/include/agregarCliente.jsp")
		},
		interceptorRefs={
			@InterceptorRef(value="defaultStack"),
			@InterceptorRef(value="store",params={"operationMode","RETRIEVE"})
		}
	)
	public String verDetalle(){
		inicializarCampos();
		if(cliente!=null && cliente.getIdCliente()!=null){
			try {
				ResponseEntity<Long> idClienteUnico= clienteRest.clienteUnificado(null,cliente.getIdCliente().longValue());
				cliente=clienteRest.findClienteUnicoById(idClienteUnico.getBody());
				
				clienteDom=clienteFacade.loadById(cliente);
				cliente.setIdDomicilio(clienteDom.getIdDomicilio());
				cliente.setIdRepresentante(clienteDom.getIdRepresentante());
				cliente.setNombreContacto(clienteDom.getNombreContacto());
				cliente.setNumeroTelefono(clienteDom.getNumeroTelefono());
				cliente.setCorreoElectronicoAviso(clienteDom.getCorreoElectronicoAviso());
				if(cliente.getIdPaisNacimiento()!=null){
					cargarEstadosNacimiento(cliente.getIdPaisNacimiento());
				} 
				if(cliente.getClaveEstadoNacimiento()!=null){
					
					cargarCiudadesNacimiento(cliente.getClaveEstadoNacimiento());
				}
				
				cargarEstadosDir(cliente.getIdPaisString());
				cargarEstadosFiscal(cliente.getIdPaisFiscal());
				cargarEstados(cliente.getIdPaisString());
				
				cargarCiudadesDir(cliente.getIdEstadoString());
				cargarCiudadesFiscal(cliente.getIdEstadoFiscal());
				cargarColoniasDir(cliente.getIdMunicipioString());
				cargarColoniasFiscal(cliente.getIdMunicipioFiscal());
				consultarMediosPago();
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}
//		if(idLineaNegocio==null){
//			//FIXME Para ambiente de pruebas.
//			idLineaNegocio=285l;
//		}
		Short regresoCotizacionAgenteNew= 5;
		if(tipoRegreso.equals(regresoCotizacionAgenteNew)){
			return "SUCCES_AGENTE";
		}
		return SUCCESS;
	}
	
	@Action(value="validateClientePrima",results={
			@Result(name=SUCCESS, type="json", params = {"includeProperties", "primaJSON"})
			}
		)
		public String validateClientePrima(){
			
			ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(cliente.getIdClienteUnico()));
						
			setPrimaJSON(entityPrima.getBody());
		
			return SUCCESS;
		}
	
	
	@Action(value="filtrarClientes",results={
		@Result(name=SUCCESS,location=CLIENTE_GRID),
		@Result(name=INPUT,location=CLIENTE_GRID)
	})
	public String filtrarClientes(){
		try {
			listaClientes=clienteRest.getListClientesByFilter(filtroCliente);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	private void ajustaFiltroClientes(){
		if(filtroCliente != null){
			if(filtroCliente.getNombre() != null){
				String valor = UtileriasWeb.parseEncodingISO(filtroCliente.getNombre());
				filtroCliente.setNombre(valor);
			}
			if(filtroCliente.getApellidoPaterno() != null){
				String valor = UtileriasWeb.parseEncodingISO(filtroCliente.getApellidoPaterno());
				filtroCliente.setApellidoPaterno(valor);
			}
			if(filtroCliente.getApellidoMaterno() != null){
				String valor = UtileriasWeb.parseEncodingISO(filtroCliente.getApellidoMaterno());
				filtroCliente.setApellidoMaterno(valor);
			}
			if(filtroCliente.getRazonSocial() != null){
				String valor = UtileriasWeb.parseEncodingISO(filtroCliente.getRazonSocial());
				filtroCliente.setRazonSocial(valor);
			}
		}
	}
	
	public String mostrarDatosGenerales(){
		return SUCCESS;
	}
	
	public String mostrarDatosContacto(){
		return SUCCESS;
	}
	
	public String mostrarDatosCobranza(){
		return SUCCESS;
	}
	
	public String mostrarDatosFiscales(){
		return SUCCESS;
	}
	
	public String mostrarAvisoSiniestro(){
		return SUCCESS;
	}
	
	public String mostrarCanalVentas(){
		return SUCCESS;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
	
	@Autowired
	@Qualifier("paisFacadeRemoteEJB")
	public void setPaisFacade(PaisFacadeRemote paisFacade) {
		this.paisFacade = paisFacade;
	}
	
	@Autowired
	@Qualifier("estadoFacadeRemoteEJB")
	public void setEstadoFacade(EstadoFacadeRemote estadoFacade) {
		this.estadoFacade = estadoFacade;
	}
	
	@Autowired
	@Qualifier("ciudadFacadeRemoteEJB")
	public void setCiudadFacade(CiudadFacadeRemote ciudadFacade) {
		this.ciudadFacade = ciudadFacade;
	}
	
	@Autowired
	@Qualifier("coloniaFacadeRemoteEJB")
	public void setColoniaFacade(ColoniaFacadeRemote coloniaFacade) {
		this.coloniaFacade = coloniaFacade;
	}
	
	@Autowired
	@Qualifier("sectorFacadeRemoteEJB")
	public void setSectorFacade(SectorFacadeRemote sectorFacade){
		this.sectorFacade=sectorFacade;
	}
	
	@Autowired
	@Qualifier("catalogoGiroFacadeRemoteEJB")
	public void setCatalogoGiroFacade(CatalogoGiroFacadeRemote catalogoGiroFacade) {
		this.catalogoGiroFacade = catalogoGiroFacade;
	}
	
	@Autowired
	@Qualifier("bancoEmisorFacadeRemoteEJB")
	public void setBancoEmisorFacade(BancoEmisorFacadeRemote bancoEmisorFacade) {
		this.bancoEmisorFacade = bancoEmisorFacade;
	}
	
	@Autowired
	@Qualifier("estadoCivilFacadeRemoteEJB")
	public void setEstadoCivilFacade(EstadoCivilFacadeRemote estadoCivilFacade) {
		this.estadoCivilFacade = estadoCivilFacade;
	}
	
	@Autowired
	@Qualifier("tipoDomiciliacionFacadeRemoteEJB")
	public void setTipoDomiciliacionFacadeRemote(TipoDomiciliacionFacadeRemote tipoDomiciliacionFacade){
		this.tipoDomiciliacionFacade=tipoDomiciliacionFacade;
	}
	
	@Autowired
	@Qualifier("negocioClienteEJB")
	public void setNegocioClienteService(NegocioClienteService negocioClienteService) {
		this.negocioClienteService = negocioClienteService;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	public ClienteUnicoDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteUnicoDTO cliente) {
		this.cliente = cliente;
	}
	public List<EstadoDTO> getEstados() {
		return estados;
	}
	public void setEstados(List<EstadoDTO> estados) {
		this.estados = estados;
	}
	public List<CiudadDTO> getCiudades() {
		return ciudades;
	}
	public void setCiudades(List<CiudadDTO> ciudades) {
		this.ciudades = ciudades;
	}
	public List<ColoniaDTO> getColonias() {
		return colonias;
	}
	public void setColonias(List<ColoniaDTO> colonias) {
		this.colonias = colonias;
	}
	public List<PaisDTO> getPaises() {
		return paises;
	}
	public void setPaises(List<PaisDTO> paises) {
		this.paises = paises;
	}
	public Integer getTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(Integer tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public List<SectorDTO> getSectores() {
		return sectores;
	}
	public void setSectores(List<SectorDTO> sectores) {
		this.sectores = sectores;
	}
	public List<CatalogoGiroDTO> getGiros() {
		return giros;
	}
	
	public List<OcupacionCNSFDTO> getOcupacionesCNSF(){
		
		return ocupacionesCNSF;
	}
	public void setOcupacionesCNSF(List<OcupacionCNSFDTO> ocupacionesCNSF){
		
		 this.ocupacionesCNSF=ocupacionesCNSF;
	}
	
	public void setGiros(List<CatalogoGiroDTO> giros) {
		this.giros = giros;
	}
	public List<BancoEmisorDTO> getBancos() {
		return bancos;
	}
	public void setBancos(List<BancoEmisorDTO> bancos) {
		this.bancos = bancos;
	}
	public List<EstadoCivilDTO> getEstadosCiviles() {
		return estadosCiviles;
	}
	public void setEstadosCiviles(List<EstadoCivilDTO> estadosCiviles) {
		this.estadosCiviles = estadosCiviles;
	}
	public List<String> getCamposRequeridos() {
		return camposRequeridos;
	}
	public void setCamposRequeridos(List<String> camposRequeridos) {
		this.camposRequeridos = camposRequeridos;
	}
	public List<ClienteGenericoDTO> getMediosPagoCliente() {
		return mediosPagoCliente;
	}
	public void setMediosPagoCliente(List<ClienteGenericoDTO> mediosPagoCliente) {
		this.mediosPagoCliente = mediosPagoCliente;
	}
	public List<TipoDomiciliacionDTO> getMediosPagoDomiciliacion() {
		return mediosPagoDomiciliacion;
	}
	public void setMediosPagoDomiciliacion(
			List<TipoDomiciliacionDTO> mediosPagoDomiciliacion) {
		this.mediosPagoDomiciliacion = mediosPagoDomiciliacion;
	}
	public List<ClienteUnicoDTO> getListaClientes() {
		return listaClientes;
	}
	public void setListaClientes(List<ClienteUnicoDTO> listaClientes) {
		this.listaClientes = listaClientes;
	}
	public ClienteUnicoDTO getFiltroCliente() {
		return filtroCliente;
	}
	public void setFiltroCliente(ClienteUnicoDTO filtroCliente) {
		this.filtroCliente = filtroCliente;
	}
	public String getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public String getIdField() {
		return idField;
	}
	public void setIdField(String idField) {
		this.idField = idField;
	}
	public Long getIdLineaNegocio() {
		return idLineaNegocio;
	}
	public void setIdLineaNegocio(Long idLineaNegocio) {
		this.idLineaNegocio = idLineaNegocio;
	}
	public String getDivCarga() {
		return divCarga;
	}
	public void setDivCarga(String divCarga) {
		this.divCarga = divCarga;
	}
	public String getTabActiva() {
		return tabActiva;
	}
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	public Integer getModuloOrigen() {
		return moduloOrigen;
	}
	public void setModuloOrigen(Integer moduloOrigen) {
		this.moduloOrigen = moduloOrigen;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public void setTipoRegreso(Short tipoRegreso) {
		this.tipoRegreso = tipoRegreso;
	}
	public Short getTipoRegreso() {
		return tipoRegreso;
	}
	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}
	public Long getIdNegocio() {
		return idNegocio;
	}
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}
	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}
	public Date getValidoEn() {
		return validoEn;
	}
	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}
	public Date getRecordFrom() {
		return recordFrom;
	}
	public void setValidoEnMillis(Long validoEnMillis) {
		this.validoEnMillis = validoEnMillis;
	}
	public Long getValidoEnMillis() {
		return validoEnMillis;
	}
	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}
	public Long getRecordFromMillis() {
		return recordFromMillis;
	}
	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}
	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}
	public String getFechaActual() {
		return fechaActual;
	}
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
	public Long getTipoOperac() {
		return tipoOperac;
	}
	public void setTipoOperac(Long tipoOperac) {
		this.tipoOperac = tipoOperac;
	}
	public Long getTipoAccionAgente() {
		return tipoAccionAgente;
	}
	public void setTipoAccionAgente(Long tipoAccionAgente) {
		this.tipoAccionAgente = tipoAccionAgente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre.replace('_', ' ');
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno.replace('_', ' ');
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno.replace('_', ' ');
	}
	public boolean isCorreoObligatorio() {
		return correoObligatorio;
	}
	public void setCorreoObligatorio(boolean correoObligatorio) {
		this.correoObligatorio = correoObligatorio;
	}
	public List<EstadoDTO> getEstadosNacimiento() {
		return estadosNacimiento;
	}
	public void setEstadosNacimiento(List<EstadoDTO> estadosNacimiento) {
		this.estadosNacimiento = estadosNacimiento;
	}
	public List<EstadoDTO> getEstadosDir() {
		return estadosDir;
	}
	public void setEstadosDir(List<EstadoDTO> estadosDir) {
		this.estadosDir = estadosDir;
	}
	public List<CiudadDTO> getCiudadesDir() {
		return ciudadesDir;
	}
	public void setCiudadesDir(List<CiudadDTO> ciudadesDir) {
		this.ciudadesDir = ciudadesDir;
	}
	public List<CiudadDTO> getCiudadesFiscal() {
		return ciudadesFiscal;
	}
	public void setCiudadesFiscal(List<CiudadDTO> ciudadesFiscal) {
		this.ciudadesFiscal = ciudadesFiscal;
	}
	public List<EstadoDTO> getEstadosFiscal() {
		return estadosFiscal;
	}
	public void setEstadosFiscal(List<EstadoDTO> estadosFiscal) {
		this.estadosFiscal = estadosFiscal;
	}
	public List<ColoniaDTO> getColoniasDir() {
		return coloniasDir;
	}
	public void setColoniasDir(List<ColoniaDTO> coloniasDir) {
		this.coloniasDir = coloniasDir;
	}
	public List<ColoniaDTO> getColoniasFiscal() {
		return coloniasFiscal;
	}
	public void setColoniasFiscal(List<ColoniaDTO> coloniasFiscal) {
		this.coloniasFiscal = coloniasFiscal;
	}
	public List<CiudadDTO> getCiudadesNac() {
		return ciudadesNac;
	}
	public void setCiudadesNac(List<CiudadDTO> ciudadesNac) {
		this.ciudadesNac = ciudadesNac;
	}
	public String getPrimaJSON() {
		return primaJSON;
	}
	public void setPrimaJSON(String primaJSON) {
		this.primaJSON = primaJSON;
	}
	
	
	
}