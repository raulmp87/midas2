package mx.com.afirme.midas2.action.cliente.polizas.catalogo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;
import mx.com.afirme.midas2.domain.movil.cliente.InfoDescargaPoliza;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.poliza.PolizaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ConsultaMovilAction extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5491476867547842162L;
	//private final Logger LOG = Logger.getLogger(ConsultaMovilAction.class);
	@Autowired
	private ClientePolizasService polizasService;
	private String correo;
	private String nombre;
	private String usuario;
	private String idCorreo;
	private String gerenciaID;
	private Gerencia gerencia = new Gerencia();
	private List<CorreosCosultaMovil> lstCorreos = new ArrayList<CorreosCosultaMovil>();
	private List<InfoDescargaPoliza> lstDetalleDescarga = new ArrayList<InfoDescargaPoliza>();
	private List<CentroOperacionView> lstGerencia = new ArrayList<CentroOperacionView>();
	
	
	public String getGerenciaID() {
		return gerenciaID;
	}

	public void setGerenciaID(String gerenciaID) {
		this.gerenciaID = gerenciaID;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	public List<CentroOperacionView> getLstGerencia() {
		return lstGerencia;
	}

	public void setLstGerencia(List<CentroOperacionView> lstGerencia) {
		this.lstGerencia = lstGerencia;
	}

	public List<CorreosCosultaMovil> getLstCorreos() {
		return lstCorreos;
	}

	public List<InfoDescargaPoliza> getLstDetalleDescarga() {
		return lstDetalleDescarga;
	}

	public void setLstDetalleDescarga(List<InfoDescargaPoliza> lstDetalleDescarga) {
		this.lstDetalleDescarga = lstDetalleDescarga;
	}

	public void setLstCorreos(List<CorreosCosultaMovil> lstCorreos) {
		this.lstCorreos = lstCorreos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getIdCorreo() {
		return idCorreo;
	}

	public void setIdCorreo(String idCorreo) {
		this.idCorreo = idCorreo;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub		   
	}	
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ClientePolizasService getPolizasService() {
		return polizasService;
	}

	public void setPolizasService(ClientePolizasService polizasService) {
		this.polizasService = polizasService;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	//Sección de Correos
	public String mostrarCorreos(){
		lstGerencia = polizasService.getLstGerencias();
		return SUCCESS;
	}
	
	public String listarCorreos(){		
		lstCorreos = polizasService.getCorreosNotificacion();
		return SUCCESS;
	}
	
	public String guardarCorreo(){
		polizasService.guardarCorreo(correo, nombre, gerenciaID);
		
		lstCorreos = new ArrayList<CorreosCosultaMovil>();
		lstCorreos = polizasService.getCorreosNotificacion();
		
		lstGerencia = new ArrayList<CentroOperacionView>();
		lstGerencia = polizasService.getLstGerencias();
		return SUCCESS;
	}
	
	public String eliminarCorreo(){
		polizasService.eliminarCorreo(idCorreo);
		return SUCCESS;
	}
	
	// Seccion detallada de descargas
	public String mostrarDescargas(){
		return SUCCESS;
	}
	
	public String listarDescargas(){
		lstDetalleDescarga = polizasService.getRegistroDescargas();
		return SUCCESS;
	}
	
	public String verDetalle(){
		return SUCCESS;
	}
	
	public String bloquearUsuario(){
		polizasService.bloqueaUsuario(usuario);
		return SUCCESS;
	}
	
	public String desbloqueaUsuario(){
		polizasService.desbloqueaUsuario(usuario);
		return SUCCESS;
	}
	
	public String verDetalleDescargas(){		
		return SUCCESS;
	}
	
	public String listarDetalle(){
		lstDetalleDescarga = polizasService.getDetalleDescargas(usuario);
		return SUCCESS;
	}
}
