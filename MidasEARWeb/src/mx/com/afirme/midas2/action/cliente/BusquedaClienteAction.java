package mx.com.afirme.midas2.action.cliente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;
import mx.com.afirme.midas2.service.cliente.grupo.GrupoClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class BusquedaClienteAction extends BaseAction {
	private static final long serialVersionUID = 1L;
	public static final int BUSQUEDA_CLIENTE = 1;
	public static final int BUSQUEDA_GRUPO_CLIENTE = 2;
	public static final int BUSQUEDA_COTIZADOR_AGENTE = 3;

	private int tipoBusqueda; 
	private ClienteDTO cliente;
	private GrupoCliente grupoCliente = new GrupoCliente();
	private String tipoAccion;
	private String idField;
	private List<ClienteDTO> listaClientes;
	private List<GrupoCliente> listaGruposCliente;
	private ClienteFacadeRemote clienteFacade;
	private GrupoClienteService grupoClienteService;
	private String divCarga;
	private ClienteGenericoDTO filtroCliente;
	private Short tipoRegreso=0;
	private Long idNegocio;
	private BigDecimal idToCotizacion;
	private Short ocultarAgregar = 0;
	/**
	 * Action usado para mostrar la página principal de búsqueda, 
	 * ya sea para consultar un cliente o un grupo de clientes
	 * @return
	 */
	public String mostrarPantallaConsulta(){
		String result = null;
		if(tipoBusqueda == BUSQUEDA_CLIENTE){
			result = "busquedaCliente";
		}
		else if(tipoBusqueda == BUSQUEDA_GRUPO_CLIENTE){
			result = "busquedaGrupoCliente";
		}
		else if(tipoBusqueda == BUSQUEDA_COTIZADOR_AGENTE){
			result = "busquedaCotizadorAgente";
		}
		return result;
	}
	
	/**
	 * Action usado para consultar el listado de clientes en base a 
	 * los filtros del objeto cliente.
	 */
	public String filtrarClientes(){
		//consultar listado de clientes en base al filtro "cliente"
		
		try {
			//No consultar el listado si no se capturan datos
			if(cliente != null && ((cliente.getNombre() != null && !cliente.getNombre().equals("") ) ||
					(cliente.getApellidoPaterno() != null && !cliente.getApellidoPaterno().equals("") ) ||
					(cliente.getApellidoMaterno() != null && !cliente.getApellidoMaterno().equals("") ) )){
				listaClientes = clienteFacade.listarFiltrado(cliente, null
						//UtileriasWeb.obtieneNombreUsuario(ServletActionContext.getRequest())
						);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Regresar JSP con el listado en formato XML
		return SUCCESS;
	}
	
	/**
	 * Action usado para consultar el listado de grupos de cliente en base a 
	 * los filtros del objeto grupoCliente.
	 */
	public String filtrarGruposCliente(){
		//consultar listado de clientes en base al filtro "grupoCliente"
		listaGruposCliente = grupoClienteService.listarPorDescripcion(grupoCliente.getDescripcion(),grupoCliente.getClave());
		if(listaGruposCliente  != null && listaGruposCliente.isEmpty()){
			super.setMensajeError("No se encontraron grupos con los datos ingresados");
			listaGruposCliente = new ArrayList<GrupoCliente>();
		}
		
		//Regresar JSP con el listado en formato XML
		return SUCCESS;
	}

	public int getTipoBusqueda() {
		return tipoBusqueda;
	}

	public void setTipoBusqueda(int tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public GrupoCliente getGrupoCliente() {
		return grupoCliente;
	}

	public void setGrupoCliente(GrupoCliente grupoCliente) {
		this.grupoCliente = grupoCliente;
	}

	public List<ClienteDTO> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<ClienteDTO> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<GrupoCliente> getListaGruposCliente() {
		return listaGruposCliente;
	}

	public void setListaGruposCliente(List<GrupoCliente> listaGruposCliente) {
		this.listaGruposCliente = listaGruposCliente;
	}

	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	@Autowired
	@Qualifier("grupoClienteServiceEJB")
	public void setGrupoClienteService(GrupoClienteService grupoClienteService) {
		this.grupoClienteService = grupoClienteService;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setIdField(String idField) {
		this.idField = idField;
	}

	public String getIdField() {
		return idField;
	}

	public String getDivCarga() {
		return divCarga;
	}

	public void setDivCarga(String divCarga) {
		this.divCarga = divCarga;
	}

	public ClienteGenericoDTO getFiltroCliente() {
		return filtroCliente;
	}

	public void setFiltroCliente(ClienteGenericoDTO filtroCliente) {
		this.filtroCliente = filtroCliente;
	}

	public void setTipoRegreso(Short tipoRegreso) {
		this.tipoRegreso = tipoRegreso;
	}

	public Short getTipoRegreso() {
		return tipoRegreso;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setOcultarAgregar(Short ocultarAgregar) {
		this.ocultarAgregar = ocultarAgregar;
	}

	public Short getOcultarAgregar() {
		return ocultarAgregar;
	}
}
