package mx.com.afirme.midas2.action.cliente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.ClientSystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Namespace("/cliente/catalogoClienteJson")
@Component
@Scope("prototype")
public class CatalogoClienteJsonAction extends  BaseAction{
	private static final long serialVersionUID = 1L;
	private ClienteGenericoDTO cliente;
	private ClienteFacadeRemote clienteFacade;
	/***Components for view****************************************************************/
	private String divCarga;
	private List<ClienteGenericoDTO> mediosPagoCliente=new ArrayList<ClienteGenericoDTO>();
	private List<String> camposRequeridos=new ArrayList<String>();
	public String tipoAccion;
	
	/**
	 * Accion para obtener los campos requeridos por Ajax-Json
	 * @return
	 */
	@Action(value="cargarCamposObligatorios",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","camposRequeridos.*"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","tipoMensaje,mensaje"})
	})
	public String cargarCamposObligatorios(){
		try {
			camposRequeridos=clienteFacade.obtenerCamposRequeridos(cliente);
		} catch (Exception e) {
			onError("Error inesperado", e);
		}
		return SUCCESS;
	}
	
	/**
	 * Accion para consultar los medios de pago
	 * @return
	 */
	@Action(value="consultarMediosPagoPorCliente",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mediosPagoCliente.*,tipoMensaje,mensaje"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mediosPagoCliente.*,tipoMensaje,mensaje"})
	})
	public String consultarMediosPago(){
		//if(cliente!=null && cliente.getIdCliente()!=null){
			//TODO consultar lista de medios de pago
			mediosPagoCliente=new ArrayList<ClienteGenericoDTO>();
			ClienteGenericoDTO c1=new ClienteGenericoDTO();
			c1.setNombre("Victor");c1.setApellidoPaterno("Herrera");c1.setApellidoMaterno("Silva");
			c1.setIdCliente(new BigDecimal(1));
			ClienteGenericoDTO c2=new ClienteGenericoDTO();
			c2.setNombre("Julieta");c2.setApellidoPaterno("Rios");c2.setApellidoMaterno("Holguin");
			c2.setIdCliente(new BigDecimal(2));
			mediosPagoCliente.add(c1);
			mediosPagoCliente.add(c2);
		//}
		return SUCCESS;
	}
	
	/**
	 * Accion para guardar los datos de un contacto en caso de un siniestro
	 * @return
	 */
	@Action(value="guardarDatosCobranza",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","cliente,mensaje,tipoMensaje"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","tipoMensaje,mensaje"})
	})
	public String guardarDatosCobranza(){
		String accion=SUCCESS;
		Long idConducto = null;
		try {
			if(cliente!=null){
				if(!UtileriasWeb.esCadenaVacia(cliente.getIdClienteString())){
					cliente.setIdCliente(new BigDecimal(cliente.getIdClienteString()));
				}
				if(!UtileriasWeb.esCadenaVacia(cliente.getClaveTipoPersonaString())){
					cliente.setClaveTipoPersona(Short.parseShort(cliente.getClaveTipoPersonaString()));
				}
			}
			boolean validaNegocio=false;
			if("consulta".equals(tipoAccion)){
				validaNegocio=true;
			}
			idConducto = clienteFacade.guardarDatosCobranza(cliente,getUsuarioActual().getNombreUsuario(),validaNegocio); 
			if (idConducto != null){
				onSuccess(getText("midas.cliente.datosCobranza.exito"));
			}
			
		} catch (Exception e) {
			onError(getText("midas.cliente.datosCobranza.error"),e);
		}
		return accion;
	}
	
	/**
	 * Maneja el mensaje de exito cuando se concluye exitosamente un action
	 * @param msg
	 */
	private void onSuccess(String msg){
		setMensaje(msg);
		setTipoMensaje(TIPO_MENSAJE_EXITO);
	}
	/**
	 * Maneja los errores de un action
	 * @param message
	 * @param e
	 */
	private void onError(String message,Exception e){
		if(e instanceof ClientSystemException){
			ClientSystemException ex=(ClientSystemException)e;
			if(ex!=null && ex.getErrorList()!=null && !ex.getErrorList().isEmpty()){
				setMensajeMapaErrorPersonalizado("Se ha producido un error!",ex.getErrorList(), MENSAJE_ERROR_GENERAL);
			}
		}else{
			setMensaje(message+",casusado por:"+e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			e.printStackTrace();
		}
	}
	/**Sets and Gets********************************************************************************/
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}
	
	public List<ClienteGenericoDTO> getMediosPagoCliente() {
		return mediosPagoCliente;
	}

	public void setMediosPagoCliente(List<ClienteGenericoDTO> mediosPagoCliente) {
		this.mediosPagoCliente = mediosPagoCliente;
	}

	public ClienteGenericoDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteGenericoDTO cliente) {
		this.cliente = cliente;
	}

	public List<String> getCamposRequeridos() {
		return camposRequeridos;
	}

	public void setCamposRequeridos(List<String> camposRequeridos) {
		this.camposRequeridos = camposRequeridos;
	}

	public String getDivCarga() {
		return divCarga;
	}

	public void setDivCarga(String divCarga) {
		this.divCarga = divCarga;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	
}
