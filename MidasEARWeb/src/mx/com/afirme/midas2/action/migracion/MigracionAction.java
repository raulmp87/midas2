package mx.com.afirme.midas2.action.migracion;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.migracion.MigracionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class MigracionAction extends BaseAction {

	private MigracionService migracionService;
	
	@Autowired
	public void setMigracionService(MigracionService migracionService) {
		this.migracionService = migracionService;
	}
	
	public void migrarEndosos() {
		migracionService.migrarEndosos();
	}
	
	public void detenerMigracionEndosos() {
		migracionService.detenerMigracionEndosos();
	}
	
	public void migrarEndososEmisionDelegada() {
		migracionService.migrarEndososEmisionDelegada();
	}
	
	public void detenerMigracionEndososEmisionDelegada() {
		migracionService.detenerMigracionEndososEmisionDelegada();
	}
	
	public void migrarArchivosFortimax() {
		migracionService.migrarArchivosFortimax();
	}	
}
