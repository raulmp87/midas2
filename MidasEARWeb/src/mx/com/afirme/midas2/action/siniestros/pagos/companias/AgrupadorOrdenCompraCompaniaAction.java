package mx.com.afirme.midas2.action.siniestros.pagos.companias;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.RelacionFacturaOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/companias/recepcionFacturas")
public class AgrupadorOrdenCompraCompaniaAction extends BaseAction implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RelacionFacturaOrdenCompraDTO> listaFacturaOrdenCompra;
	private Map<String, Map<String, Object>> listaFacturasRelacionadas;
	private Map<String, String> mapOrdenFactura;
	private File zipFacturas;
	private Integer tipoUsuario;
	private Map<Long, String> listaOficinas;
	private Map<Long, String> listaFacturasCombo;
	private List<Map<String, Object>> listaPrestadoresServicio;
	private List<OrdenCompraProveedorDTO> listaOrdenesCompra;
	private List<MensajeValidacionFactura> listaValidacionesFactura;
	private List<DocumentoFiscal> listaFacturas;
	
	private Long idOficina;
	private Long idBatch;
	private Long idFactura;
	private Long idValidacionFactura;
	private Integer numFacturasCargadas;
	private List<EnvioValidacionFactura> listaFacturasCargadas;
	private String extensionArchivo;

	private ListadoService listadoService;
	private UsuarioService usuarioService;
	private OrdenCompraService ordenCompraService;
	private RecepcionFacturaService recepcionFacturaService;
	private PrestadorDeServicioService prestadorDeServicioService;
	private Oficina oficinaPrestadorServicio;

	private DocumentoFiscal factura;
	private Map<Long, String> companiasMap;
	private Integer companiaSeleccionada;
	private Map<Long, String> oficinasMap;
	private Long oficinaSeleccionada;
	private String nombreAgrupador;
	private String nombrePrestadorServicio;
	private Integer idPrestadorServicio;
	private String idsOrdenesCompra;
	private List<DocumentoFiscal> resultados;
	private OrdenCompraProveedorDTO filtroAgrupador;
	private String nombreCompletoCia;
	private String esError = "n";
	private Map<String, String> catTipoAgrupador;

	@Autowired
	@Qualifier("recepcionFacturaServiceEJB")
	public void setRecepcionFacturaService(
			RecepcionFacturaService recepcionFacturaService) {
		this.recepcionFacturaService = recepcionFacturaService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Autowired
	@Qualifier("ordenCompraServiceEJB")
	public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
		this.ordenCompraService = ordenCompraService;
	}

	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	public void setPrestadorDeServicioService(
			PrestadorDeServicioService prestadorDeServicioService) {
		this.prestadorDeServicioService = prestadorDeServicioService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	@Override
	public void prepare() throws Exception {

	}
	
	private void inicializaCombos(){
		companiasMap = this.listadoService.getMapPrestadorPorTipo("CIA");
		oficinasMap = this.listadoService.obtenerOficinasSiniestros();
		catTipoAgrupador = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AGRUP_FACT_SINIESTRO, Boolean.TRUE);	
	}

	public void prepareMostrarRegistro() {
		inicializaCombos();
	}

	@Action(value = "mostrarRegistro", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/companias/registro.jsp") })
	public String mostrarRegistro() {
		
		if(this.factura != null && this.factura.getId()!=null){
			this.factura = this.entidadService.findById(DocumentoFiscal.class, this.factura.getId());
			this.companiaSeleccionada = this.factura.getProveedor().getId();
			this.nombreCompletoCia = this.factura.getProveedor().getNombrePersona();
			this.marcaOficinaSeleccionadaEnRegistro(this.factura);
			 
		}
		
		return SUCCESS;
	}
	
	private void marcaOficinaSeleccionadaEnRegistro(DocumentoFiscal factura){
		
		this.oficinaSeleccionada =  null;
		for(OrdenCompra orden: factura.getOrdenesCompra()){
			EstimacionCoberturaReporteCabina pase = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, orden.getIdTercero());
			Long idOficina = pase.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getId();
			if(this.oficinaSeleccionada==null){
				this.oficinaSeleccionada = idOficina;
			}else if(this.oficinaSeleccionada != idOficina){
				break;
			}
		}
	}

	@Action(value = "buscarOrdenesCompra", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/companias/ordenesCompraGrid.jsp") })
	public String buscarOrdenesCompra() {
		if(this.factura==null){
			if (this.companiaSeleccionada == null) {
				this.listaOrdenesCompra = new ArrayList<OrdenCompraProveedorDTO>();
			}else{
				this.listaOrdenesCompra = this.ordenCompraService.obtenerOrdenesCompraParaAgruparParaCompania(
						this.companiaSeleccionada,this.oficinaSeleccionada, this.filtroAgrupador.getTipoAgrupador());
			}
		}else{
			this.factura = this.entidadService.findById(DocumentoFiscal.class, this.factura.getId());
			List<OrdenCompra> ordenes =  this.factura.getOrdenesCompra();
			this.listaOrdenesCompra = new ArrayList<OrdenCompraProveedorDTO>();
			for(OrdenCompra orden : ordenes){
				OrdenCompraProveedorDTO dto = this.ordenCompraService.convierteOrdenCompraAOrdenCompraProveedorDTO(orden);
				this.listaOrdenesCompra.add(dto);
			}
			this.companiaSeleccionada = this.factura.getProveedor().getId();
		}
		return SUCCESS;
	}
	
	
	
	public void prepareGuardar(){
		inicializaCombos();
	}
	
	@Action(value = "guardar", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/companias/registro.jsp") })
	public String guardar() {
		this.factura = this.recepcionFacturaService.guardaAgrupador(this.idPrestadorServicio, 
				this.idsOrdenesCompra);
		return SUCCESS;
	}
	
	
	
	public void prepareEliminar(){
		inicializaCombos();
	}
	
	@Action(value = "eliminar", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/pagos/companias/busquedaDeAgrupadores.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/pagos/companias/busquedaDeAgrupadores.jsp") 
	})
	public String eliminar() {
		
		try {
			this.recepcionFacturaService.eliminaAgrupador(this.factura.getId());
			this.setMensajeExito();
			return SUCCESS;
		}catch(Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(e.getMessage() != null ? 
					e.getMessage() : ""));
			return INPUT;
		}
		
		
		
	}
	
	public void prepareMostrarBusqueda(){
		this.inicializaCombos();
	}
	
	
	@Action(value = "mostrarBusqueda", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/companias/busquedaDeAgrupadores.jsp") })
	public String mostrarBusqueda() {
		return SUCCESS;
	}
	
	@Action(value = "buscarAgrupadores", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/companias/busquedaResultadosAgrupadoresGrid.jsp") })
	public String buscarAgrupadores() {
		if(this.filtroAgrupador==null){
			this.resultados = new ArrayList<DocumentoFiscal>();
		}else{
			this.resultados = this.recepcionFacturaService.buscarAgrupadores(this.filtroAgrupador);
		}
		return SUCCESS;
	}

	public Map<String, Map<String, Object>> getListaFacturasRelacionadas() {
		return listaFacturasRelacionadas;
	}

	public void setListaFacturasRelacionadas(
			Map<String, Map<String, Object>> listaFacturasRelacionadas) {
		this.listaFacturasRelacionadas = listaFacturasRelacionadas;
	}

	public Map<Long, String> getCompaniasMap() {
		return companiasMap;
	}

	public void setCompaniasMap(Map<Long, String> companiasMap) {
		this.companiasMap = companiasMap;
	}

	public Integer getCompaniaSeleccionada() {
		return companiaSeleccionada;
	}

	public void setCompaniaSeleccionada(Integer companiaSeleccionada) {
		this.companiaSeleccionada = companiaSeleccionada;
	}

	public Map<Long, String> getOficinasMap() {
		return oficinasMap;
	}

	public void setOficinasMap(Map<Long, String> oficinasMap) {
		this.oficinasMap = oficinasMap;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public OrdenCompraService getOrdenCompraService() {
		return ordenCompraService;
	}

	public RecepcionFacturaService getRecepcionFacturaService() {
		return recepcionFacturaService;
	}

	public PrestadorDeServicioService getPrestadorDeServicioService() {
		return prestadorDeServicioService;
	}

		public File getZipFacturas() {
		return zipFacturas;
	}

	public List<RelacionFacturaOrdenCompraDTO> getListaFacturaOrdenCompra() {
		return listaFacturaOrdenCompra;
	}

	public void setListaFacturaOrdenCompra(
			List<RelacionFacturaOrdenCompraDTO> listaFacturaOrdenCompra) {
		this.listaFacturaOrdenCompra = listaFacturaOrdenCompra;
	}

	public void setZipFacturas(File zipFacturas) {
		this.zipFacturas = zipFacturas;
	}

	public Map<Long, String> getListaOficinas() {
		return listaOficinas;
	}

	public Map<String, String> getMapOrdenFactura() {
		return mapOrdenFactura;
	}

	public void setMapOrdenFactura(Map<String, String> mapOrdenFactura) {
		this.mapOrdenFactura = mapOrdenFactura;
	}

	public void setListaOficinas(Map<Long, String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

	public List<Map<String, Object>> getListaPrestadoresServicio() {
		return listaPrestadoresServicio;
	}

	public void setListaPrestadoresServicio(
			List<Map<String, Object>> listaPrestadoresServicio) {
		this.listaPrestadoresServicio = listaPrestadoresServicio;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNombrePrestadorServicio() {
		return nombrePrestadorServicio;
	}

	public List<MensajeValidacionFactura> getListaValidacionesFactura() {
		return listaValidacionesFactura;
	}

	public void setListaValidacionesFactura(
			List<MensajeValidacionFactura> listaValidacionesFactura) {
		this.listaValidacionesFactura = listaValidacionesFactura;
	}

	public Oficina getOficinaPrestadorServicio() {
		return oficinaPrestadorServicio;
	}

	public void setOficinaPrestadorServicio(Oficina oficinaPrestadorServicio) {
		this.oficinaPrestadorServicio = oficinaPrestadorServicio;
	}

	public void setNombrePrestadorServicio(String nombrePrestadorServicio) {
		this.nombrePrestadorServicio = nombrePrestadorServicio;
	}

	public Integer getIdPrestadorServicio() {
		return idPrestadorServicio;
	}

	public void setIdPrestadorServicio(Integer idPrestadorServicio) {
		this.idPrestadorServicio = idPrestadorServicio;
	}

	public Long getIdOficina() {
		return idOficina;
	}

	public List<OrdenCompraProveedorDTO> getListaOrdenesCompra() {
		return listaOrdenesCompra;
	}

	public void setListaOrdenesCompra(
			List<OrdenCompraProveedorDTO> listaOrdenesCompra) {
		this.listaOrdenesCompra = listaOrdenesCompra;
	}

	public List<DocumentoFiscal> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(List<DocumentoFiscal> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public Long getIdBatch() {
		return idBatch;
	}

	public void setIdBatch(Long idBatch) {
		this.idBatch = idBatch;
	}

	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public Map<Long, String> getListaFacturasCombo() {
		return listaFacturasCombo;
	}

	public void setListaFacturasCombo(Map<Long, String> listaFacturasCombo) {
		this.listaFacturasCombo = listaFacturasCombo;
	}

	public Integer getNumFacturasCargadas() {
		return numFacturasCargadas;
	}

	public void setNumFacturasCargadas(Integer numFacturasCargadas) {
		this.numFacturasCargadas = numFacturasCargadas;
	}

	public List<EnvioValidacionFactura> getListaFacturasCargadas() {
		return listaFacturasCargadas;
	}

	public void setListaFacturasCargadas(
			List<EnvioValidacionFactura> listaFacturasCargadas) {
		this.listaFacturasCargadas = listaFacturasCargadas;
	}

	public Long getIdValidacionFactura() {
		return idValidacionFactura;
	}

	public void setIdValidacionFactura(Long idValidacionFactura) {
		this.idValidacionFactura = idValidacionFactura;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}

	public Long getOficinaSeleccionada() {
		return oficinaSeleccionada;
	}

	public void setOficinaSeleccionada(Long oficinaSeleccionada) {
		this.oficinaSeleccionada = oficinaSeleccionada;
	}

	public String getNombreAgrupador() {
		return nombreAgrupador;
	}

	public void setNombreAgrupador(String nombreAgrupador) {
		this.nombreAgrupador = nombreAgrupador;
	}

	public String getIdsOrdenesCompra() {
		return idsOrdenesCompra;
	}

	public void setIdsOrdenesCompra(String idsOrdenesCompra) {
		this.idsOrdenesCompra = idsOrdenesCompra;
	}

	public DocumentoFiscal getFactura() {
		return factura;
	}

	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public List<DocumentoFiscal> getResultados() {
		return resultados;
	}

	public void setResultados(List<DocumentoFiscal> resultados) {
		this.resultados = resultados;
	}

	public OrdenCompraProveedorDTO getFiltroAgrupador() {
		return filtroAgrupador;
	}

	public void setFiltroAgrupador(OrdenCompraProveedorDTO filtroAgrupador) {
		this.filtroAgrupador = filtroAgrupador;
	}

	public String getNombreCompletoCia() {
		return nombreCompletoCia;
	}

	public void setNombreCompletoCia(String nombreCompletoCia) {
		this.nombreCompletoCia = nombreCompletoCia;
	}

	public String getEsError() {
		return esError;
	}

	public void setEsError(String esError) {
		this.esError = esError;
	}

	public Map<String, String> getCatTipoAgrupador() {
		return catTipoAgrupador;
	}

	public void setCatTipoAgrupador(Map<String, String> catTipoAgrupador) {
		this.catTipoAgrupador = catTipoAgrupador;
	}
}
