/**
 * 
 */
package mx.com.afirme.midas2.action.siniestros.valuacion.ordencompra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraPermiso;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.AutorizacionesUsuarioDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.OrdenesCompraAutorizarDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.UsuarioPermisoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionService;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author admin
 *
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/valuacion/ordencompraautorizacion")
public class OrdenCompraAutorizacionAction extends BaseAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idReporteCabina;
	
	private Map<Long,String> conceptoPagoMap;
	
	private Map<String,String> coberturaMap;
	
	private Map<Long,String> oficinasMap;
	
	private Map<String,String> estautsMap;
	
	private Map<String,String> nivelMap;
	
	private Map<Long,String> lineaNegocioMap;  
	
	private Map<String,String> tipoOrdenCompraMap;
	
	private OrdenCompraPermiso ordenCompraPermiso;
	
	private List<AutorizacionesUsuarioDTO> resultadoPermisos;
	
	private List<UsuarioPermisoDTO> usuariosDisponibles;
	
	private List<UsuarioPermisoDTO> usuariosAsociados;
	
	private List<OrdenesCompraAutorizarDTO> ordenesCompraAutorizar;
	
	private UsuarioPermisoDTO usuario;
	
	private String tipoPago;
	
	private String coberturaCompuesta;
	
	private Long lineaNegocioId;
	
	private Long ordenCompraPermisoId;
	
	private Boolean soloConsulta;
	
	
	private short INT_ORDENCOMPRA						=1;
	//private short INT_GASTOAJUSTE						=2;
	
	private final String CONTENEDOR_ORDENCOMPRA_PERMISOS 	=	"/jsp/siniestros/valuacion/ordencompra/contenedorOrdenCompraPermisos.jsp";
	private final String LISTADO_ORDENCOMPRA_PERMISOS_GRID	=	"/jsp/siniestros/valuacion/ordencompra/listadoOrdenCompraPermisosGrid.jsp";
	private final String INCLUDE_CONCEPTO_PAGO 				=	"/jsp/siniestros/valuacion/ordencompra/include_comboConceptoPago.jsp";
	private final String INCLUDE_COBERTURAS 				=	"/jsp/siniestros/valuacion/ordencompra/include_comboCoberturas.jsp";
	private final String INCLUDE_EDICION_PERMISO			=	"/jsp/siniestros/valuacion/ordencompra/include_alta_permiso.jsp";
	private final String MOSTRAR_VENTANA_RELACION_USUARIOS	=	"/jsp/siniestros/valuacion/ordencompra/relacionar_usuarios_window.jsp";
	private final String LISTADO_USUARIOS_DISPONIBLES_GRID	=	"/jsp/siniestros/valuacion/ordencompra/listadoUsuariosDisponiblesGrid.jsp";
	private final String LISTADO_USUARIOS_ASOCIADOS_GRID	=	"/jsp/siniestros/valuacion/ordencompra/listadoUsuariosAsociadosGrid.jsp";
	private final String DRAG_DROP_SUCCESS					=	"/jsp/relaciones/respuestaAsociacion.jsp";
	
	private final String CONTENEDOR_ORDENCOMPRA_POR_AUTORIZAR 	=	"/jsp/siniestros/valuacion/ordencompra/contenedorBandejaOrdenCompra.jsp";
	private final String LISTADO_ORDENCOMPRA_POR_AUTORIZAR 		=	"/jsp/siniestros/valuacion/ordencompra/listadoBandejaOrdenCompraGrid.jsp";
	
	
	
//	contenedorBandejaOrdenCompra.jsp
//	contenedorOrdenCompraPermisos.jsp
//	listadoOrdenCompraPermisos.jsp
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("ordenCompraAutorizacionServiceEJB")
	private OrdenCompraAutorizacionService ordenCompraAutorizacionService;


	
	@Override
	public void prepare() throws Exception {
		this.oficinasMap = listadoService.obtenerOficinasSiniestros();
		this.estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		this.nivelMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.NIVEL_DE_AUTORIZACION);
		this.tipoOrdenCompraMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		this.lineaNegocioMap = listadoService.obtenerMapSecciones();
		this.coberturaMap = new HashMap<String,String>();
		this.conceptoPagoMap = new HashMap<Long,String>();
	}
	
	
	
	
	@Action(value="mostrarPermisos",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA_PERMISOS),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA_PERMISOS)
			})
	public String mostrarPermisos(){
		if(!StringUtils.isEmpty (this.getMensaje())){
			super.setMensaje(this.getMensaje());
		}
		return SUCCESS;		
	}
	
	@Action(value="nuevoPermiso",results={
			@Result(name=SUCCESS,location=INCLUDE_EDICION_PERMISO),
			@Result(name=INPUT,location=INCLUDE_EDICION_PERMISO)
			})
	public String nuevoPermiso(){
		this.ordenCompraPermiso = new OrdenCompraPermiso();
		return SUCCESS;		
	}
	
	
	@Action(value="cargarConceptoPago",results={
			@Result(name=SUCCESS,location=INCLUDE_CONCEPTO_PAGO),
			@Result(name=INPUT,location=INCLUDE_CONCEPTO_PAGO)
			})
	public String cargarConceptoPago(){
		if(tipoPago.equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)){
			String[] reg = null;
			Pattern p = Pattern.compile("\\|");				
			reg = p.split(coberturaCompuesta);
			this.conceptoPagoMap = listadoService.listarConceptosPorCoberturaSeccion(new Long(reg[0]),lineaNegocioId, reg[1],INT_ORDENCOMPRA);
		}else if(tipoPago.equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)){
				this.conceptoPagoMap = listadoService.obtenerMapConceptosGastoAjuste();
		}else if (tipoPago.equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE)){
				this.conceptoPagoMap = listadoService.obtenerMapConceptosReembolsoGastoAjuste();
		}
	
		return SUCCESS;		
	}
	
	
	@Action(value="cargarCoberturas",results={
			@Result(name=SUCCESS,location=INCLUDE_COBERTURAS),
			@Result(name=INPUT,location=INCLUDE_COBERTURAS)
			})
	public String cargarCoberturas(){
		
		List<CoberturaSeccionSiniestroDTO> lCoberturasSiniestro 	= this.estimacionCoberturaSiniestroService.obtenerCoberturasPorSeccion(this.lineaNegocioId);
		String claveSubcalculo 										= "n";
		Long idCobertura 											= 0l;
		coberturaMap 												= new HashMap<String,String>();

		
		for( CoberturaSeccionSiniestroDTO cobertura : lCoberturasSiniestro ){
			
			claveSubcalculo = ( (null== cobertura.getClaveSubCalculo() || cobertura.getClaveSubCalculo().isEmpty() )? OrdenCompraService.CVENULA: cobertura.getClaveSubCalculo() );
			idCobertura     = new Long ( cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().toString()) ;
			
			coberturaMap.put(idCobertura+ OrdenCompraService.SEPARADOR +claveSubcalculo, cobertura.getNombreCobertura());
		}
		
		return SUCCESS;		
	}
	
	@Action(value="guardarPermiso",results={
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA_PERMISOS),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarPermisos", "namespace",
					"/siniestros/valuacion/ordencompraautorizacion",
					"mensaje","${mensaje}"}) 
			})	
	public String guardarPermiso(){
		ordenCompraAutorizacionService.guardarPermiso(ordenCompraPermiso);
		ordenCompraPermiso = new OrdenCompraPermiso();
		setMensajeExito();
		return SUCCESS;
	}
	
	
	@Action(value="mostrarEditarPermiso",results={
			@Result(name=SUCCESS,location=INCLUDE_EDICION_PERMISO),
			@Result(name=INPUT,location=INCLUDE_EDICION_PERMISO)
			})
	public String mostrarEditarPermiso(){
		ordenCompraPermiso = ordenCompraAutorizacionService.obtenerPermisoById(ordenCompraPermisoId);
		return SUCCESS;
	}
	
	
	@Action(value="eliminarPermiso",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA_PERMISOS),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA_PERMISOS)
			})
	public String eliminarPermiso(){
		
		try {
			ordenCompraAutorizacionService.eliminarPermiso(ordenCompraPermisoId);
			setMensajeExitoPersonalizado("Se ha eliminado el permiso");
		} catch (Exception e) {
			setMensaje("No se puede eliminar el permiso, por que tiene usuarios relacionados a este");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_INFORMACION);
		}
		
		return SUCCESS;
	}


	@Action(value="listadoPermisoUsuario",results={
			@Result(name=SUCCESS,location=LISTADO_ORDENCOMPRA_PERMISOS_GRID),
			@Result(name=INPUT,location=LISTADO_ORDENCOMPRA_PERMISOS_GRID)
			})
	public String  listadoPermisoUsuario(){
		resultadoPermisos = ordenCompraAutorizacionService.obtenerListaAutorizaciones();
		return SUCCESS;
	}
	
	
	
	@Action(value="mostrarRelacionarUsuarios",results={
			@Result(name=SUCCESS,location=MOSTRAR_VENTANA_RELACION_USUARIOS),
			@Result(name=INPUT,location=MOSTRAR_VENTANA_RELACION_USUARIOS)
			})
	public String mostrarRelacionarUsuarios(){
		return SUCCESS;		
	}
	
	
	@Action
	(value = "obtenerUsuariosRelacionados", results = { 
			@Result(name = SUCCESS, location = LISTADO_USUARIOS_ASOCIADOS_GRID),
			@Result(name = INPUT, location = LISTADO_USUARIOS_ASOCIADOS_GRID)
	})
	public String obtenerUsuariosRelacionados(){
		usuariosAsociados =  ordenCompraAutorizacionService.obtenerUsuariosAsociados(ordenCompraPermisoId);
		return SUCCESS;
	}
	
	
	@Action
	(value = "obtenerUsuariosDisponibles", results = { 
			@Result(name = SUCCESS, location = LISTADO_USUARIOS_DISPONIBLES_GRID),
			@Result(name = INPUT, location = LISTADO_USUARIOS_DISPONIBLES_GRID)
	})
	public String obtenerUsuariosDisponibles(){
		usuariosDisponibles =  ordenCompraAutorizacionService.obtenerUsuariosDisponibles( ordenCompraPermisoId );
		return SUCCESS;
	}
	
	
	@Action
	(value = "accionRelacionarUsuarios", results = { 
			@Result(name = SUCCESS, location = DRAG_DROP_SUCCESS),
			@Result(name = INPUT, location = CONTENEDOR_ORDENCOMPRA_PERMISOS)
	})
	public String accionRelacionarUsuarios(){
		String accion 							= ServletActionContext.getRequest().getParameter("!nativeeditor_status");	
		
		if (accion.equals(ROW_INSERTED)) {
			ordenCompraAutorizacionService.guardarRelacionUsuario(usuario);
			
		} else if (accion.equals(ROW_DELETED)) {
			ordenCompraAutorizacionService.eliminarRelacionUsuario(usuario.getIdUsuarioPermiso());
		}
		
		return SUCCESS;
	}
	
	
	
	@Action(value="mostrarListadoOrdenesPorAutorizar",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA_POR_AUTORIZAR),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA_POR_AUTORIZAR)
			})
	public String  mostrarListadoOrdenesPorAutorizar(){
		return SUCCESS;
	}
	
	
	@Action(value="listadoOrdenesPorAutorizar",results={
			@Result(name=SUCCESS,location=LISTADO_ORDENCOMPRA_POR_AUTORIZAR),
			@Result(name=INPUT,location=LISTADO_ORDENCOMPRA_POR_AUTORIZAR)
			})
	public String  listadoOrdenesPorAutorizar(){
		//llamada original
		//this.ordenesCompraAutorizar = ordenCompraAutorizacionService.obtenerListaOrdenesCompraPorAutorizar();
		this.ordenesCompraAutorizar = ordenCompraAutorizacionService.obtenerListaOrdenesCompraPorAutorizarSP();
		return SUCCESS;
	}
	


	/**
	 * @return the idReporteCabina
	 */
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}




	/**
	 * @param idReporteCabina the idReporteCabina to set
	 */
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}




	/**
	 * @return the conceptoPagoMap
	 */
	public Map<Long, String> getConceptoPagoMap() {
		return conceptoPagoMap;
	}




	/**
	 * @param conceptoPagoMap the conceptoPagoMap to set
	 */
	public void setConceptoPagoMap(Map<Long, String> conceptoPagoMap) {
		this.conceptoPagoMap = conceptoPagoMap;
	}




	/**
	 * @return the coberturaMap
	 */
	public Map<String, String> getCoberturaMap() {
		return coberturaMap;
	}




	/**
	 * @param coberturaMap the coberturaMap to set
	 */
	public void setCoberturaMap(Map<String, String> coberturaMap) {
		this.coberturaMap = coberturaMap;
	}




	/**
	 * @return the oficinasMap
	 */
	public Map<Long, String> getOficinasMap() {
		return oficinasMap;
	}




	/**
	 * @param oficinasMap the oficinasMap to set
	 */
	public void setOficinasMap(Map<Long, String> oficinasMap) {
		this.oficinasMap = oficinasMap;
	}




	/**
	 * @return the estautsMap
	 */
	public Map<String, String> getEstautsMap() {
		return estautsMap;
	}




	/**
	 * @param estautsMap the estautsMap to set
	 */
	public void setEstautsMap(Map<String, String> estautsMap) {
		this.estautsMap = estautsMap;
	}




	/**
	 * @return the nivelMap
	 */
	public Map<String, String> getNivelMap() {
		return nivelMap;
	}




	/**
	 * @param nivelMap the nivelMap to set
	 */
	public void setNivelMap(Map<String, String> nivelMap) {
		this.nivelMap = nivelMap;
	}




	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}




	/**
	 * @param listadoService the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}




	/**
	 * @return the ordenCompraPermiso
	 */
	public OrdenCompraPermiso getOrdenCompraPermiso() {
		return ordenCompraPermiso;
	}




	/**
	 * @param ordenCompraPermiso the ordenCompraPermiso to set
	 */
	public void setOrdenCompraPermiso(OrdenCompraPermiso ordenCompraPermiso) {
		this.ordenCompraPermiso = ordenCompraPermiso;
	}




	/**
	 * @return the tipoOrdenCompraMap
	 */
	public Map<String, String> getTipoOrdenCompraMap() {
		return tipoOrdenCompraMap;
	}




	/**
	 * @param tipoOrdenCompraMap the tipoOrdenCompraMap to set
	 */
	public void setTipoOrdenCompraMap(Map<String, String> tipoOrdenCompraMap) {
		this.tipoOrdenCompraMap = tipoOrdenCompraMap;
	}




	/**
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}




	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}




	/**
	 * @return the coberturaCompuesta
	 */
	public String getCoberturaCompuesta() {
		return coberturaCompuesta;
	}




	/**
	 * @param coberturaCompuesta the coberturaCompuesta to set
	 */
	public void setCoberturaCompuesta(String coberturaCompuesta) {
		this.coberturaCompuesta = coberturaCompuesta;
	}




	/**
	 * @return the lineaNegocioMap
	 */
	public Map<Long, String> getLineaNegocioMap() {
		return lineaNegocioMap;
	}




	/**
	 * @param lineaNegocioMap the lineaNegocioMap to set
	 */
	public void setLineaNegocioMap(Map<Long, String> lineaNegocioMap) {
		this.lineaNegocioMap = lineaNegocioMap;
	}




	/**
	 * @return the lineaNegocioId
	 */
	public Long getLineaNegocioId() {
		return lineaNegocioId;
	}




	/**
	 * @param lineaNegocioId the lineaNegocioId to set
	 */
	public void setLineaNegocioId(Long lineaNegocioId) {
		this.lineaNegocioId = lineaNegocioId;
	}




	/**
	 * @return the resultadoPermisos
	 */
	public List<AutorizacionesUsuarioDTO> getResultadoPermisos() {
		return resultadoPermisos;
	}




	/**
	 * @param resultadoPermisos the resultadoPermisos to set
	 */
	public void setResultadoPermisos(
			List<AutorizacionesUsuarioDTO> resultadoPermisos) {
		this.resultadoPermisos = resultadoPermisos;
	}




	




	/**
	 * @return the usuariosDisponibles
	 */
	public List<UsuarioPermisoDTO> getUsuariosDisponibles() {
		return usuariosDisponibles;
	}




	/**
	 * @param usuariosDisponibles the usuariosDisponibles to set
	 */
	public void setUsuariosDisponibles(List<UsuarioPermisoDTO> usuariosDisponibles) {
		this.usuariosDisponibles = usuariosDisponibles;
	}




	/**
	 * @return the usuariosAsociados
	 */
	public List<UsuarioPermisoDTO> getUsuariosAsociados() {
		return usuariosAsociados;
	}




	/**
	 * @param usuariosAsociados the usuariosAsociados to set
	 */
	public void setUsuariosAsociados(List<UsuarioPermisoDTO> usuariosAsociados) {
		this.usuariosAsociados = usuariosAsociados;
	}


	/**
	 * @return the ordenCompraPermisoId
	 */
	public Long getOrdenCompraPermisoId() {
		return ordenCompraPermisoId;
	}




	/**
	 * @param ordenCompraPermisoId the ordenCompraPermisoId to set
	 */
	public void setOrdenCompraPermisoId(Long ordenCompraPermisoId) {
		this.ordenCompraPermisoId = ordenCompraPermisoId;
	}




	/**
	 * @return the usuario
	 */
	public UsuarioPermisoDTO getUsuario() {
		return usuario;
	}




	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(UsuarioPermisoDTO usuario) {
		this.usuario = usuario;
	}




	/**
	 * @return the ordenesCompraAutorizar
	 */
	public List<OrdenesCompraAutorizarDTO> getOrdenesCompraAutorizar() {
		return ordenesCompraAutorizar;
	}




	/**
	 * @param ordenesCompraAutorizar the ordenesCompraAutorizar to set
	 */
	public void setOrdenesCompraAutorizar(
			List<OrdenesCompraAutorizarDTO> ordenesCompraAutorizar) {
		this.ordenesCompraAutorizar = ordenesCompraAutorizar;
	}




	/**
	 * @return the soloConsulta
	 */
	public Boolean getSoloConsulta() {
		return soloConsulta;
	}




	/**
	 * @param soloConsulta the soloConsulta to set
	 */
	public void setSoloConsulta(Boolean soloConsulta) {
		this.soloConsulta = soloConsulta;
	}
	
	
	
	
	

}
