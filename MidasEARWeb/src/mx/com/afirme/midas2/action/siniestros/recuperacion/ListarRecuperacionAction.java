package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.util.ExcelExporter;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/listado/recuperaciones")

public class ListarRecuperacionAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2658477756130705926L;

	@Autowired
	@Qualifier("recuperacionServiceEJB")
	private RecuperacionService recuperacionService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	private RecuperacionDTO        filtroRecuperacionDto;
	private Map<String,String>     lstEstatusRecuperacion;
	private Map<String,String>     lstMedioRecuperacion;
	private Map<Long,String>       lstOficinas;
	private Map<String,String>     lstOrigenRecuperacion;
	private Map<String,String>     lstPtDocumentada;
	private Map<String,String>     lstTipoRecuperacion;
	private Map<String,String>     lstTipoServicio;
	private List<RecuperacionDTO>  listadoRecuperacionDto;
	private TransporteImpresionDTO transporte;
	private String 				   tipoRecuperacion;
	
	// TODO: SE DEBE DE CREAR EL CATALOGO EN BD
	private Map<String,String>     lstComplemento;
	private Map<String,String>     lstEstatusIngreso;
	private Map<String,String>     lstEstatusCarta;
	private Map<String,String>     lstIndemnizado;
	private Map<String,String>     lstEstatusEnSubasta;
	
	private static final String CONTENEDOR_BUSCAR_RECUPERACIONES = "/jsp/siniestros/recuperacion/listadoRecuperaciones.jsp";
	private static final String CONTENEDOR_BUSCAR_RECUPERACIONES_GRID = "/jsp/siniestros/recuperacion/listadoRecuperacionesGrid.jsp";
	private static final String CONTENEDOR_NUEVA_RECUPERACION= "/jsp/siniestros/recuperacion/tipoRecuperacion.jsp";
	
	
	public void prepareMostrarContenedor(){
		this.llenarListados();
	}
	
	@Action(value="mostrarContenedor", results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSCAR_RECUPERACIONES)
	})
	public String mostrarContenedor(){
		if(!StringUtils.isEmpty (this.getMensaje())){
			super.setMensaje(this.getMensaje());
		}
		return SUCCESS;
	}
	
	@Action(value="buscarRecuperaciones", results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSCAR_RECUPERACIONES_GRID),
			@Result(name = INPUT,location=CONTENEDOR_BUSCAR_RECUPERACIONES_GRID) 	
	})
	public String buscarRecuperaciones(){
		
		if( filtroRecuperacionDto != null ){
			// HACES BUSQUEDA MIDAS.PKGSIN_RECUPERACIONES
			listadoRecuperacionDto = recuperacionService.buscarRecuperaciones(filtroRecuperacionDto);
		}	
		
		return SUCCESS;
		
		
	}
	
	@Action(value="definirRecuperacion", results={
			@Result(name=SUCCESS,location=CONTENEDOR_NUEVA_RECUPERACION)
	})
	public String definirRecuperacion(){
		this.lstTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECUPERACION);
		// REMUEVE SALVAMENTO, DEDUCIBLE, CRUCERO, JURIDICO, VIALES Y ESPECIALES.
		this.lstTipoRecuperacion.remove("SVM");
		this.lstTipoRecuperacion.remove("DED");
		this.lstTipoRecuperacion.remove("CRU");
		this.lstTipoRecuperacion.remove("JUR");
		this.lstTipoRecuperacion.remove("VIA");
		this.lstTipoRecuperacion.remove("ESP");
		this.lstTipoRecuperacion.remove("SIP");
		return SUCCESS;
	}
	
	@Action(value="exportarRecuperaciones",
			results={@Result(name=SUCCESS,
					  type="stream",
					  params={"contentType","${transporte.contentType}",
						  "inputName","transporte.genericInputStream",
						  "contentDisposition","attachment;filename=\"${transporte.fileName}\""})}) 
	public String exportarRecuperaciones(){
		
		listadoRecuperacionDto = recuperacionService.buscarRecuperaciones(filtroRecuperacionDto);
		ExcelExporter exporter = new ExcelExporter( RecuperacionDTO.class );
		transporte = exporter.exportXLS(listadoRecuperacionDto, "Busqueda Recuperaciones");
		
		return SUCCESS;
	}
	
	
	private void llenarListados(){
		this.lstEstatusRecuperacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_RECUPERACION);
		this.lstMedioRecuperacion   = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MEDIO_RECUPERACION);
		this.lstOficinas            = listadoService.obtenerOficinasSiniestros();
		this.lstOrigenRecuperacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ORIGEN_RECUPERACION);
		this.lstPtDocumentada       = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.RESPUESTA_SIMPLE);
		this.lstTipoRecuperacion    = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECUPERACION);
		this.lstTipoServicio        = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_SERVICIO_POLIZA);
		this.lstEstatusCarta        = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_CARTA_CIA);
		this.lstIndemnizado         = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.RESPUESTA_SIMPLE);
	}
	
	@Override
	public void prepare() throws Exception {
		
		
	}


	public RecuperacionService getRecuperacionService() {
		return recuperacionService;
	}

	public void setRecuperacionService(RecuperacionService recuperacionService) {
		this.recuperacionService = recuperacionService;
	}

	public Map<String, String> getLstEstatusRecuperacion() {
		return lstEstatusRecuperacion;
	}

	public void setLstEstatusRecuperacion(Map<String, String> lstEstatusRecuperacion) {
		this.lstEstatusRecuperacion = lstEstatusRecuperacion;
	}

	public Map<String, String> getLstMedioRecuperacion() {
		return lstMedioRecuperacion;
	}

	public void setLstMedioRecuperacion(Map<String, String> lstMedioRecuperacion) {
		this.lstMedioRecuperacion = lstMedioRecuperacion;
	}

	public Map<Long, String> getLstOficinas() {
		return lstOficinas;
	}

	public void setLstOficinas(Map<Long, String> lstOficinas) {
		this.lstOficinas = lstOficinas;
	}

	public Map<String, String> getLstOrigenRecuperacion() {
		return lstOrigenRecuperacion;
	}

	public void setLstOrigenRecuperacion(Map<String, String> lstOrigenRecuperacion) {
		this.lstOrigenRecuperacion = lstOrigenRecuperacion;
	}

	public Map<String, String> getLstPtDocumentada() {
		return lstPtDocumentada;
	}

	public void setLstPtDocumentada(Map<String, String> lstPtDocumentada) {
		this.lstPtDocumentada = lstPtDocumentada;
	}

	public Map<String, String> getLstTipoRecuperacion() {
		return lstTipoRecuperacion;
	}

	public void setLstTipoRecuperacion(Map<String, String> lstTipoRecuperacion) {
		this.lstTipoRecuperacion = lstTipoRecuperacion;
	}

	public Map<String, String> getLstTipoServicio() {
		return lstTipoServicio;
	}

	public void setLstTipoServicio(Map<String, String> lstTipoServicio) {
		this.lstTipoServicio = lstTipoServicio;
	}
	
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	public RecuperacionDTO getFiltroRecuperacionDto() {
		return filtroRecuperacionDto;
	}

	public void setFiltroRecuperacionDto(RecuperacionDTO filtroRecuperacionDto) {
		this.filtroRecuperacionDto = filtroRecuperacionDto;
	}

	public List<RecuperacionDTO> getListadoRecuperacionDto() {
		return listadoRecuperacionDto;
	}

	public void setListadoRecuperacionDto(
			List<RecuperacionDTO> listadoRecuperacionDto) {
		this.listadoRecuperacionDto = listadoRecuperacionDto;
	}

	public Map<String, String> getLstComplemento() {
		return lstComplemento;
	}

	public void setLstComplemento(Map<String, String> lstComplemento) {
		this.lstComplemento = lstComplemento;
	}

	public Map<String, String> getLstEstatusIngreso() {
		return lstEstatusIngreso;
	}

	public void setLstEstatusIngreso(Map<String, String> lstEstatusIngreso) {
		this.lstEstatusIngreso = lstEstatusIngreso;
	}

	public Map<String, String> getLstEstatusCarta() {
		return lstEstatusCarta;
	}

	public void setLstEstatusCarta(Map<String, String> lstEstatusCarta) {
		this.lstEstatusCarta = lstEstatusCarta;
	}

	public Map<String, String> getLstIndemnizado() {
		return lstIndemnizado;
	}

	public void setLstIndemnizado(Map<String, String> lstIndemnizado) {
		this.lstIndemnizado = lstIndemnizado;
	}

	public Map<String, String> getLstEstatusEnSubasta() {
		return lstEstatusEnSubasta;
	}

	public void setLstEstatusEnSubasta(Map<String, String> lstEstatusEnSubasta) {
		this.lstEstatusEnSubasta = lstEstatusEnSubasta;
	}


}
