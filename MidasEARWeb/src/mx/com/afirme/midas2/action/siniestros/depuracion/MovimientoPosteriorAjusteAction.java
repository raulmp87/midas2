package mx.com.afirme.midas2.action.siniestros.depuracion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.MovimientoPosteriorAjusteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.depuracion.DepuracionReservaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/depuracion")
public class MovimientoPosteriorAjusteAction extends BaseAction implements Preparable  {

	private static final String	LOCATION_MOVIMIENTOPOSTERIORAJUSTE_JSP	= "/jsp/siniestros/depuracion/movimientoPosteriorAjuste.jsp";

	private static final long serialVersionUID = -8065074024636894085L;
	
	private MovimientoPosteriorAjusteDTO detalleMovimiento;
	private MovimientoPosteriorAjusteDTO filtroMovimientos;
	private Map<String,String> listCoberturas;
	private List<ConfiguracionDepuracionReserva> lstConceptos;
	private List<MovimientoPosteriorAjusteDTO> lstMovimientos;
	private Map<Long,String> lstOficinas;
	private Map<String,String> lstTerminoSiniestro;
	private Long idMovimiento;
	
	private TransporteImpresionDTO transporte;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("autorizacionReservaServiceEJB")
	private AutorizacionReservaService autorizacionReservaService;	

	@Autowired
	@Qualifier("depuracionReservaServiceEJB")
	private DepuracionReservaService depuracionReservaService;
	
	/**
	 * Llenar los listados de la siguiente manera:
	 * 
	 * lstOficinas: Invocar al método obtenerOficinasSiniestros de ListadoService.
	 * 
	 * lstCoberturas = autorizacionReservaService.obtenerCoberturasPorSeccion, enviar
	 * el idToSeccion null
	 * 
	 * lstTerminoSiniestro = listadoService.obtenerCatalogoValorFijo con catalogo
	 * TERMINO_AJUSTESINIESTRO
	 * 
	 * lstConceptos = Invocar al método obtenerConfiguracionesActivas de
	 * DepuracionReservaService
	 */
	@Override
	public void prepare() {
	}
	
	private void init(){
		this.lstOficinas = this.listadoService.obtenerOficinasSiniestros();
		this.listCoberturas = this.listadoService.obtenerCoberturaPorSeccionConDescripcion(null);
		this.lstTerminoSiniestro = this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		this.lstConceptos = this.depuracionReservaService.obtenerConfiguracionesActivas();
	}
	
	
	public void prepareMostrarContenedor(){
		this.init();
	}
	
	@Action(value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = LOCATION_MOVIMIENTOPOSTERIORAJUSTE_JSP),
			@Result(name = INPUT, location = LOCATION_MOVIMIENTOPOSTERIORAJUSTE_JSP) })
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	/**
	 * Invocar al método buscarMovimientosPosteriores de DepuracionReservaService
	 */
	@Action(value = "buscarMovimientos", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/movimientoPosteriorAjusteGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_MOVIMIENTOPOSTERIORAJUSTE_JSP) })
	public String buscarMovimientos(){
		if(this.filtroMovimientos!=null){
			this.lstMovimientos = this.depuracionReservaService.buscarMovimientosPosteriores(this.filtroMovimientos);
		}else{
			this.lstMovimientos = new ArrayList<MovimientoPosteriorAjusteDTO>();
		}
		return SUCCESS;
	}

	/**
	 * Invocar al método obtenerDetalleMovimientoPosterior de DepuracionReservaService
	 * y asignarlo a la variable detalleMovimiento
	 */
	@Action(value = "verDetalleMovimiento", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/detalleMovimientoPosterior.jsp"),
			@Result(name = INPUT, location = LOCATION_MOVIMIENTOPOSTERIORAJUSTE_JSP) })
	public String verDetalleMovimiento(){
		this.detalleMovimiento = this.depuracionReservaService.obtenerDetalleMovimientoPosterior(idMovimiento);
		return SUCCESS;
	}
	
	@Action(value="exportarResultados",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})

	public String exportarAExcel(){
		this.lstMovimientos = this.depuracionReservaService.buscarMovimientosPosteriores(this.filtroMovimientos);
		
		ExcelExporter exporter = new ExcelExporter(MovimientoPosteriorAjusteDTO.class);
		transporte = exporter.exportXLS(lstMovimientos, "Lista Depuracion");
		
		return SUCCESS;
		
	}

	public MovimientoPosteriorAjusteDTO getDetalleMovimiento() {
		return detalleMovimiento;
	}

	public void setDetalleMovimiento(MovimientoPosteriorAjusteDTO detalleMovimiento) {
		this.detalleMovimiento = detalleMovimiento;
	}

	public MovimientoPosteriorAjusteDTO getFiltroMovimientos() {
		return filtroMovimientos;
	}

	public void setFiltroMovimientos(MovimientoPosteriorAjusteDTO filtroMovimientos) {
		this.filtroMovimientos = filtroMovimientos;
	}	

	public Map<String, String> getListCoberturas() {
		return listCoberturas;
	}

	public void setListCoberturas(Map<String, String> listCoberturas) {
		this.listCoberturas = listCoberturas;
	}

	public List<ConfiguracionDepuracionReserva> getLstConceptos() {
		return lstConceptos;
	}

	public void setLstConceptos(List<ConfiguracionDepuracionReserva> lstConceptos) {
		this.lstConceptos = lstConceptos;
	}

	public List<MovimientoPosteriorAjusteDTO> getLstMovimientos() {
		return lstMovimientos;
	}

	public void setLstMovimientos(List<MovimientoPosteriorAjusteDTO> lstMovimientos) {
		this.lstMovimientos = lstMovimientos;
	}

	public Map<Long, String> getLstOficinas() {
		return lstOficinas;
	}

	public void setLstOficinas(Map<Long, String> lstOficinas) {
		this.lstOficinas = lstOficinas;
	}

	public Map<String, String> getLstTerminoSiniestro() {
		return lstTerminoSiniestro;
	}

	public void setLstTerminoSiniestro(Map<String, String> lstTerminoSiniestro) {
		this.lstTerminoSiniestro = lstTerminoSiniestro;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public AutorizacionReservaService getAutorizacionReservaService() {
		return autorizacionReservaService;
	}

	public void setAutorizacionReservaService(
			AutorizacionReservaService autorizacionReservaService) {
		this.autorizacionReservaService = autorizacionReservaService;
	}

	public DepuracionReservaService getDepuracionReservaService() {
		return depuracionReservaService;
	}

	public void setDepuracionReservaService(
			DepuracionReservaService depuracionReservaService) {
		this.depuracionReservaService = depuracionReservaService;
	}

	public Long getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(Long idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
}