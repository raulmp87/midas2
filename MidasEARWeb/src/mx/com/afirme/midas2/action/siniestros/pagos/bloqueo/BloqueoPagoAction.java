package mx.com.afirme.midas2.action.siniestros.pagos.bloqueo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.BloqueoPago;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService.BloqueoPagoFiltro;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/bloqueo")
public class BloqueoPagoAction  extends BaseAction implements Preparable {
	
	private static final long serialVersionUID = -3985521813954475066L;

	private static final String SUCCESS_BLOQUEOS = "SUCCESS_BLOQUEOS";

	private static final String SUCCESS_PAGOS = "SUCCESS_PAGOS";
	
	private static final String TIPO_BLOQUEO_PROVEEDOR = "PROVEEDOR";
	
	private BloqueoPago bloqueoPago;
	private List<BloqueoPago> listadoBloqueos;
	private Map<Integer,String> proovedoresMap;
	private Map<String,String> tiposDeBloqueoMap;
	private Map<Integer,String> situacionesActualesMap;
	private Map<String,String> tipoProveedorMap;
	private Map<Long,String> proveedorMap;
	private BloqueoPagoFiltro filtroBloqueos;
	private Integer proveedorId;
	private String tipoPrestador;
	
	private Boolean esDeOrdenesDePago;
	
	private Long idPagoBloqueado;
	private Boolean esModoConsulta;
	private Long idPago;

	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("bloqueOrdenPagoEJB")
	private BloqueoPagoService bloqueoPagoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	private UsuarioService usuarioService;
	
	
	@Override
	public void prepare(){
		this.tiposDeBloqueoMap = this.bloqueoPagoService.obtenerTipoDeBloqueoDescripcion();
		this.situacionesActualesMap = this.bloqueoPagoService.obtenerEstatusBloqueoDescripcion();
		this.tipoProveedorMap=  listadoService.getMapTipoPrestador();
		this.proveedorMap = new LinkedHashMap<Long, String>();
	}
	
	@Action(value = "mostrarContenedor", results = { 
		@Result(name = SUCCESS, location = "/jsp/siniestros/pagos/bloqueo/contenedorListadoBloqueoPagos.jsp") })
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	@Action(value = "buscarBloqueos", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/pagos/bloqueo/listadoPagosBloqueosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/pagos/bloqueo/contenedorListadoBloqueoPagos.jsp")})
	public String buscarBloqueos() {
		if(filtroBloqueos==null){
			this.listadoBloqueos = new ArrayList<BloqueoPago>();
		}else{
			this.listadoBloqueos = this.bloqueoPagoService.buscarBloqueos(filtroBloqueos);
		}
		return SUCCESS;
	}
	
	
	@Action(value = "mostrarContenedorDetalleBloqueo", results = { 
			 @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/bloqueo/contenedorBloqueoPago.jsp") })
	public String mostrarContenedorDetalleBloqueo() {
		if(this.idPagoBloqueado != null){
			this.bloqueoPago = this.entidadService.findById(BloqueoPago.class, this.idPagoBloqueado);
			if(this.bloqueoPago.getProveedor()!=null){
				this.proveedorId = this.bloqueoPago.getProveedor().getId();
				this.tipoPrestador = this.bloqueoPago.getTipoPrestador();
				this.proveedorMap = this.listadoService.getMapPrestadorPorTipo(this.tipoPrestador);
			}
			if(!this.esModoConsulta){
				if(this.bloqueoPago.getEstatus()){
					this.bloqueoPago.setFechaDesbloqueo(new Date());
					this.bloqueoPago.setUsuarioDesbloqueo(usuarioService.getUsuarioActual().getNombreUsuario());
				}else{
					this.bloqueoPago.setFechaBloqueo(new Date());
					this.bloqueoPago.setUsuarioBloqueo(usuarioService.getUsuarioActual().getNombreUsuario());
				}
				this.bloqueoPago.setEstatus(!this.bloqueoPago.getEstatus());
			}
		}else{
			this.bloqueoPago = new BloqueoPago();
			this.bloqueoPago.setEstatus(Boolean.TRUE);
			this.bloqueoPago.setUsuarioBloqueo(usuarioService.getUsuarioActual().getNombreUsuario());
			this.bloqueoPago.setFechaBloqueo(new Date());
		}
		return SUCCESS;
	}
	
	
	@Action(value = "bloquearDesdePagos", results = { 
			 @Result(name = SUCCESS, location = "/jsp/siniestros/pagos/bloqueo/contenedorBloqueoPago.jsp") })
	public String bloquearDesdePagos() {
		this.bloqueoPago = this.findBloqueoPagoPorPagoId(this.idPago);
		if(this.bloqueoPago==null){
			this.bloqueoPago = new BloqueoPago();
		}
		OrdenPagoSiniestro ordenPago = this.entidadService.findById(OrdenPagoSiniestro.class, this.idPago);
		this.bloqueoPago.setOrdenPago(ordenPago);
		if(this.bloqueoPago.getId()!=null){
			if(this.bloqueoPago.getProveedor()!=null){
				this.proveedorId = this.bloqueoPago.getProveedor().getId();
				this.tipoPrestador = this.bloqueoPago.getTipoPrestador();
				this.proveedorMap = this.listadoService.getMapPrestadorPorTipo(this.tipoPrestador);
			}
			if(this.bloqueoPago.getEstatus()){
				this.bloqueoPago.setFechaDesbloqueo(new Date());
				this.bloqueoPago.setUsuarioDesbloqueo(usuarioService.getUsuarioActual().getNombreUsuario());
			}else{
				this.bloqueoPago.setFechaBloqueo(new Date());
				this.bloqueoPago.setUsuarioBloqueo(usuarioService.getUsuarioActual().getNombreUsuario());
			}
			this.bloqueoPago.setEstatus(!this.bloqueoPago.getEstatus());
		}else{
			this.bloqueoPago.setEstatus(Boolean.TRUE);
			this.bloqueoPago.setTipo("PAGO");
			this.bloqueoPago.setUsuarioBloqueo(usuarioService.getUsuarioActual().getNombreUsuario());
			this.bloqueoPago.setFechaBloqueo(new Date());
		}
		this.esModoConsulta = false;
		return SUCCESS;
	}
	
	@Action(value = "guardarPagoBloqueado", results = { 
			@Result(name = SUCCESS_BLOQUEOS, location = "/jsp/siniestros/pagos/bloqueo/contenedorListadoBloqueoPagos.jsp"),
			@Result(name=SUCCESS_PAGOS, type="redirectAction", params={
				"actionName","mostrar",
				"namespace","/siniestros/pagos/pagosSinestro"}),
			@Result(name = INPUT, location = "/jsp/siniestros/pagos/bloqueo/contenedorBloqueoPago.jsp")
	})
	public String guardarPagoBloqueado() {
		try {
			if (bloqueoPago.getTipo().equals(TIPO_BLOQUEO_PROVEEDOR)){
				this.asignaPrestador();
			}
			this.bloqueoPagoService.guardarBloqueo(bloqueoPago);
			super.setMensajeExito();
			if(esDeOrdenesDePago != null && esDeOrdenesDePago){
				return SUCCESS_PAGOS;
			}else{
				return SUCCESS_BLOQUEOS;
			}
			
		} catch (MidasException e) {
			super.setMensajeError(e.getLocalizedMessage());
			return INPUT;
		}
		
	}
	
	private BloqueoPago findBloqueoPagoPorPagoId(Long pagoId){
		BloqueoPago bloqueo = null;
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("ordenPago.id", pagoId );
		params.put("tipo", "PAGO" );
		params.put("estatus", Boolean.TRUE );
		List<BloqueoPago> bloqueos = this.entidadService.findByProperties(BloqueoPago.class, params);
		if(bloqueos.size()>0){
			bloqueo = bloqueos.get(0);
		}
		return bloqueo;
	}
	
	private void asignaPrestador(){
		if(this.proveedorId!=null){
			PrestadorServicio proveedor = this.entidadService.findById(PrestadorServicio.class, this.proveedorId);
			this.bloqueoPago.setProveedor(proveedor);
			this.bloqueoPago.setTipoPrestador(this.tipoPrestador);
		}
	}
	
	public BloqueoPago getBloqueoPago() {
		return bloqueoPago;
	}

	public void setBloqueoPago(BloqueoPago bloqueoPago) {
		this.bloqueoPago = bloqueoPago;
	}

	public List<BloqueoPago> getListadoBloqueos() {
		return listadoBloqueos;
	}

	public void setListadoBloqueos(List<BloqueoPago> listadoBloqueos) {
		this.listadoBloqueos = listadoBloqueos;
	}

	

	public Map<Integer, String> getProovedoresMap() {
		return proovedoresMap;
	}

	public void setProovedoresMap(Map<Integer, String> proovedoresMap) {
		this.proovedoresMap = proovedoresMap;
	}

	public Map<String, String> getTiposDeBloqueoMap() {
		return tiposDeBloqueoMap;
	}

	public void setTiposDeBloqueoMap(Map<String, String> tiposDeBloqueoMap) {
		this.tiposDeBloqueoMap = tiposDeBloqueoMap;
	}

	public Map<Integer, String> getSituacionesActualesMap() {
		return situacionesActualesMap;
	}

	public void setSituacionesActualesMap(
			Map<Integer, String> situacionesActualesMap) {
		this.situacionesActualesMap = situacionesActualesMap;
	}

	public BloqueoPagoFiltro getFiltroBloqueos() {
		return filtroBloqueos;
	}

	public void setFiltroBloqueos(BloqueoPagoFiltro filtroBloqueos) {
		this.filtroBloqueos = filtroBloqueos;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public BloqueoPagoService getBloqueoPagoService() {
		return bloqueoPagoService;
	}

	public void setBloqueoPagoService(BloqueoPagoService bloqueoPagoService) {
		this.bloqueoPagoService = bloqueoPagoService;
	}

	public Integer getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public Long getIdPagoBloqueado() {
		return idPagoBloqueado;
	}

	public void setIdPagoBloqueado(Long idPagoBloqueado) {
		this.idPagoBloqueado = idPagoBloqueado;
	}

	public Boolean getEsModoConsulta() {
		return esModoConsulta;
	}

	public void setEsModoConsulta(Boolean esModoConsulta) {
		this.esModoConsulta = esModoConsulta;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public Map<String, String> getTipoProveedorMap() {
		return tipoProveedorMap;
	}

	public void setTipoProveedorMap(Map<String, String> tipoProveedorMap) {
		this.tipoProveedorMap = tipoProveedorMap;
	}

	public Map<Long, String> getProveedorMap() {
		return proveedorMap;
	}

	public void setProveedorMap(Map<Long, String> proveedorMap) {
		this.proveedorMap = proveedorMap;
	}

	public String getTipoPrestador() {
		return tipoPrestador;
	}

	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}

	public Long getIdPago() {
		return idPago;
	}

	public void setIdPago(Long idPago) {
		this.idPago = idPago;
	}

	public Boolean getEsDeOrdenesDePago() {
		return esDeOrdenesDePago;
	}

	public void setEsDeOrdenesDePago(Boolean esDeOrdenesDePago) {
		this.esDeOrdenesDePago = esDeOrdenesDePago;
	}
	
}
