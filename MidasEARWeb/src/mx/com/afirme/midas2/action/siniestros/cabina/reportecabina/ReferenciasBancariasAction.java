package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionDTO;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reportecabina/referenciasbancarias")
public class ReferenciasBancariasAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = -2658477756130705926L;
	
	private ReporteCabina entidad;
	
	private List<RecuperacionDTO> listadoRecuperacion;
	
	
	@Autowired
	@Qualifier("recuperacionServiceEJB")
	private transient RecuperacionService recuperacionService;
	
	
	/*GETTERS Y SETTERS*/
	public ReporteCabina getEntidad() {
		return entidad;
	}

	public void setEntidad(ReporteCabina entidad) {
		this.entidad = entidad;
	}

	public List<RecuperacionDTO> getListadoRecuperacion() {
		return listadoRecuperacion;
	}

	public void setListadoRecuperacion(List<RecuperacionDTO> listadoRecuperacion) {
		this.listadoRecuperacion = listadoRecuperacion;
	}
	
	
	/*METODOS*/
	
	@Override
	public void prepare() throws Exception {
		//Implementacion necesaria por implementar Preparable, en este caso no es necesario utilizarlo.
	}

	@Action(value="mostrarContenedor", results={
			@Result(name=SUCCESS,location="/jsp/siniestros/cabina/reportecabina/referenciasBancarias.jsp")
	})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	@Action(value="listarReferenciasBancarias", results={
			@Result(name=SUCCESS,location="/jsp/siniestros/cabina/reportecabina/referenciasBancariasGrid.jsp"),
			@Result(name = INPUT,location="/jsp/siniestros/cabina/reportecabina/referenciasBancariasGrid.jsp") 	
	})
	public String buscarReferenciasBancarias(){
		try {
			listadoRecuperacion = recuperacionService.buscarReferenciasBancarias(entidad.getId());	
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}
}
