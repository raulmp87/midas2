package mx.com.afirme.midas2.action.siniestros.recuperacion.ingresos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/listado/ingresos")
public class ListarIngresoAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -6275372472583187227L;
	
	private static final String	LOCATION_CONTENEDORLISTARINGRESO_JSP = "/jsp/siniestros/recuperacion/listadoIngresos.jsp";
	private static final String	LOCATION_CONTENEDORLISTARINGRESOGRID_JSP = "/jsp/siniestros/recuperacion/listadoIngresosGrid.jsp";
	
	
	private ListarIngresoDTO filtroIngreso;
	private List<ListarIngresoDTO> ingresos;
	private Map<String,String> listaEstatus;
	private Map<String,String> listaMedioRecuperacion;
	private Map<Long,String> listaOficinas;
	private Map<String,String> listaTipoRecuperacion;
	
	private TransporteImpresionDTO transporte;
	
	@Autowired
	@Qualifier("ingresoServiceEJB")
	private IngresoService ingresoService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Override
	public void prepare() throws Exception {
	}
	
	/**
	 * carga los listados necesarios para presentar la pantalla de busqueda
	 */
	private void obtenerListados(){
		listaEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_INGRESO);
		listaMedioRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MEDIO_RECUPERACION);
		listaOficinas = listadoService.obtenerOficinasSiniestros();
		listaTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECUPERACION);
	}
	
	public void prepareMostrarContenedor(){
		obtenerListados();
	}
	
	@Action(value = "mostrarContenedor", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTARINGRESO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTARINGRESO_JSP)})
	public String mostrarContenedor(){
		if(!StringUtils.isEmpty (this.getMensaje())){
			super.setMensaje(this.getMensaje());
		}
		return SUCCESS;
	}
	
	@Action(value = "buscarIngresos", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTARINGRESOGRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTARINGRESOGRID_JSP)})
	public String buscarIngresos(){
		ingresos = ingresoService.buscarIngresos(filtroIngreso, null);
		return SUCCESS;
	}
	
	@Action(value="exportarIngresos",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarIngresos(){
		ingresos = ingresoService.buscarIngresos(filtroIngreso, null);
		ExcelExporter exporter = new ExcelExporter(ListarIngresoDTO.class);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy",new Locale("es","MX"));
		transporte = exporter.exportXLS(ingresos, "Ingresos "+sdf.format(new Date()));
		
		return SUCCESS;
	}	

	/**
	 * @return the filtroIngreso
	 */
	public ListarIngresoDTO getFiltroIngreso() {
		return filtroIngreso;
	}

	/**
	 * @param filtroIngreso the filtroIngreso to set
	 */
	public void setFiltroIngreso(ListarIngresoDTO filtroIngreso) {
		this.filtroIngreso = filtroIngreso;
	}

	/**
	 * @return the ingresos
	 */
	public List<ListarIngresoDTO> getIngresos() {
		return ingresos;
	}

	/**
	 * @param ingresos the ingresos to set
	 */
	public void setIngresos(List<ListarIngresoDTO> ingresos) {
		this.ingresos = ingresos;
	}

	/**
	 * @return the listaEstatus
	 */
	public Map<String, String> getListaEstatus() {
		return listaEstatus;
	}

	/**
	 * @param listaEstatus the listaEstatus to set
	 */
	public void setListaEstatus(Map<String, String> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	/**
	 * @return the listaMedioRecuperacion
	 */
	public Map<String, String> getListaMedioRecuperacion() {
		return listaMedioRecuperacion;
	}

	/**
	 * @param listaMedioRecuperacion the listaMedioRecuperacion to set
	 */
	public void setListaMedioRecuperacion(Map<String, String> listaMedioRecuperacion) {
		this.listaMedioRecuperacion = listaMedioRecuperacion;
	}

	/**
	 * @return the listaOficinas
	 */
	public Map<Long, String> getListaOficinas() {
		return listaOficinas;
	}

	/**
	 * @param listaOficinas the listaOficinas to set
	 */
	public void setListaOficinas(Map<Long, String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

	/**
	 * @return the listaTipoRecuperacion
	 */
	public Map<String, String> getListaTipoRecuperacion() {
		return listaTipoRecuperacion;
	}

	/**
	 * @param listaTipoRecuperacion the listaTipoRecuperacion to set
	 */
	public void setListaTipoRecuperacion(Map<String, String> listaTipoRecuperacion) {
		this.listaTipoRecuperacion = listaTipoRecuperacion;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

}