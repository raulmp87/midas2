/**
 * 
 */
package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina.CausaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina.TerminoAjuste;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoPaseAtencion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author simavera
 * 
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/estimacioncobertura")
public class EstimacionCoberturaSiniestroAction extends BaseAction implements Preparable {



	private static final long serialVersionUID = -6582239631455419064L;

	private Map<String,String> pasesAtencion;
	private Map<String,String> causasMovimiento;
	private Map<String,String> estatus;
	private Map<String,String> danos;
	private Map<String,String> tiposBien;
	private Map<String,String> tiposEstado;
	private Map<String,String> tiposAtencion;
	private Map<String,String> paises;
	private Map<String,String> estados;
	private Map<String,String> municipios;
	private Map<String,String> aplicaDeducible;
	private Map<String,String> hospitalDiferente;
	private Map<String,String> tieneCiaSeguros;
	private Map<String,String> ciaLlegadas;
	private Map<String,String> colores;
	private Map<String,String> ctgNoAplicaDeducible;
	private Map<String,String> motivosCorreoNoPropocionado;
	private Map<String,String> ctgTipoTransporte;
	private Map<String,String> tiposPersona;
	private Map<String,String> circunstancia;
	private Short soloConsulta;
	private Long terminoAjuste;
	private Long tipoSiniestro;
	private Long tipoResponsabilidad;
	private Long idCoberturaReporteCabina;
	private EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestro;
	private Long idEstimacionCoberturaReporte;
	private List<PaseAtencionSiniestroDTO> pasesAtencionTercero;
	private Long tipoCalculoCobertura;
	private String tipoEstimacion;
	private String namespace;
	private String methodName;
	private Long reporteCabinaId;
	private String tipoCalculo;
	private Integer idResponsableReparacion;
	private Integer idHospital;
	private Integer idMedico;
	private Integer idTaller;
	private int pasesAtencionInt;
	private Integer idCompaniaSegurosTercero;
	private String apDeducible;
	private Long idPaseDeAtencion;
	private String tieneCiaSegurosValor;
	private Boolean otroHospital;
	private String idEstatus;
	private String estatusHabilitado;
	private Long validOnMillis;
	private Long recordFromMillis;
	private String isListadoPases;
	private Boolean modoConsultaDeListado;
	private Boolean tieneAntiguedadELNoSerie;
	private BigDecimal porcentajeDeducible;
	private BigDecimal porcentajeDeducibleValorComercial;
	private String     motivoNoAplicaDeducibleSel;
	private String     reponseDeCESVI;
	private Boolean permiteCambiarDeducible;
	private Boolean permiteCapturarNoSiniestroTercero;
	private Boolean permiteCapturarCiaInfo;
	
	private SiniestroCabinaDTO siniestroDTO = new SiniestroCabinaDTO();
		
	private static final String MOSTRAR_ESTIMACION_DIRECTA 			= "ESD";
	private static final String MOSTRAR_DANIOS_MATERIALES 			= "DMA";
	private static final String MOSTRAR_GASTOS_MEDICOS 				= "GME";
	private static final String MOSTRAR_GASTOS_MEDICOS_CONDUCTOR 	= "GMC";
	private static final String MOSTRAR_RCPERSONA 					= "RCP";
	private static final String MOSTRAR_RCVEHICULO 					= "RCV";
	private static final String MOSTRAR_RCBIENES 					= "RCB";
	private static final String MOSTRAR_RCVIAJERO 					= "RCJ";	
	private static final String MOSTRAR_LISTA_PASES 				= "NOA";
	
	private static final String SOLO_REGISTRO 						= "SRE";
	
	
	//
	
	private static final String ESTATUS_ESTIMACION_PENDIENTE = "PEN";
	
	
	private static final int REQUIERE_AUTORIZACION_AJUSTE			= 1;
	private static final int REQUIERE_AUTORIZACION_ANTIGUEDAD		= 2;
	
	private Long idParametroAntiguedad;
	
	private int requiereAutorizacion;
	
	private String numeroSiniestroEstimacion;
	private String numeroSiniestroAfectacion;
	private BigDecimal estimacionNueva;


	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("siniestroCabinaServiceEJB")
	private SiniestroCabinaService siniestroService;
	
	@Autowired
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@Autowired
	@Qualifier("autorizacionReservaServiceEJB")
	private AutorizacionReservaService autorizacionReservaService;
	
	@Autowired
	@Qualifier("catSolicitudAutorizacionServiceEJB")
	private CatSolicitudAutorizacionService catSolicitudAutorizacionService;

	
	@Autowired
	@Qualifier("informacionVehicularServiceEJB")
	private InformacionVehicularService informacionVehicularService;
	
	
	@Override
	public void prepare(){			
		this.estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTIMACION_ESTATUS);
		this.danos = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.DANOS);
		this.tiposBien = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_BIEN);
		this.tiposEstado = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_ESTADO_PERSONA);
		this.tiposAtencion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_ATENCION);
		this.paises = listadoService.getMapPaises();
		this.estados = this.listadoService.getMapEstadosPorPaisMidas(null);
		this.municipios = new HashMap<String, String>();
		this.aplicaDeducible = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RESPUESTA_SIMPLE);
		this.hospitalDiferente = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RESPUESTA_SIMPLE);
		this.tieneCiaSeguros = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RESPUESTA_SIMPLE);
		this.ciaLlegadas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_LLEGADA);
		this.colores = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.COLOR);
		this.ctgNoAplicaDeducible = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVOS_NO_APLICAR_DEDUCIBLE);
		this.motivosCorreoNoPropocionado = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVOS_CORREO_NO_PROPORCIONADO);
		this.ctgTipoTransporte = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_TRANSPORTE);
		this.tiposPersona = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PERSONA_FISCAL);
		this.circunstancia = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CIRCUNSTANCIA);
		if(this.idCoberturaReporteCabina!=null){
			CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, this.idCoberturaReporteCabina);
			String codigoTipoResponsabilidad = coberturaReporteCabina.getIncisoReporteCabina().getAutoIncisoReporteCabina().getTipoResponsabilidad();
			AutoIncisoReporteCabina autoInciso = siniestroService.obtenerAutoIncisoByReporteCabina(coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId());
			this.pasesAtencion = listadoService.obtenerTipoDePasePorTipoEstimacion(this.tipoEstimacion, codigoTipoResponsabilidad, autoInciso.getCausaSiniestro(), autoInciso.getTerminoAjuste());
		}
		
		validaCausasDeMovimiento();
		
	}

	@Action(value = "mostrarEstimacion", results = {
			@Result(name = MOSTRAR_ESTIMACION_DIRECTA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionDirecta.jsp"),
			@Result(name = MOSTRAR_LISTA_PASES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/listadoPasesAtencion.jsp"),
			@Result(name = MOSTRAR_DANIOS_MATERIALES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionDanosMateriales.jsp")})
	/**
	 * Regresa la pantalla correspondiente al tipo de estimacion
	 */
	public String mostrarEstimacion() {
		String success 				= "";
		BigDecimal idMoneda,montoDeducible = BigDecimal.ZERO;
		String recibidaOrdenCia = null;
		Integer ciaSeguros = null;
		PrestadorServicio prestadorServicio = null;
		//TODO Falta validacion....
		if (this.pasesAtencionInt == 1) {
			success = MOSTRAR_LISTA_PASES;
			return success;
		}
		estimacionCoberturaSiniestro = estimacionCoberturaSiniestroService.obtenerDatosEstimacionCobertura( idCoberturaReporteCabina, tipoCalculo, tipoEstimacion );
		idEstimacionCoberturaReporte = estimacionCoberturaSiniestro.getEstimacionCoberturaReporte().getId();
		
		
		
		
		if (EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)) {
			success = MOSTRAR_ESTIMACION_DIRECTA;
//			success = MOSTRAR_DANIOS_MATERIALES;
			
			estimacionCoberturaSiniestro.getEstimacionGenerica().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionGenerica().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			idMoneda = estimacionCoberturaSiniestro.getEstimacionGenerica().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getCotizacionDTO().getIdMoneda();
			estimacionCoberturaSiniestro.setMoneda( estimacionCoberturaSiniestroService.descripcionMoneda(idMoneda ) );
			
			validaCausasDeMovimiento();
			
		
			String fuenteSumaAsegurada = estimacionCoberturaSiniestro.getEstimacionGenerica().getCoberturaReporteCabina().getCoberturaDTO().getClaveFuenteSumaAsegurada();
			//1 Comercial
			//2 Factura
			//3 Amparada
			//9 Convenido
			if (fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL) || 
					fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA) || 
					fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)) {

				if(idEstimacionCoberturaReporte == null){
					estimacionCoberturaSiniestro.getEstimacionGenerica().setSumaAseguradaObtenida(
							new BigDecimal(estimacionCoberturaSiniestro.getEstimacionGenerica().getCoberturaReporteCabina().getValorSumaAsegurada()));
				}else{
					//only for robos coverage...
					EstimacionCoberturaReporteCabina estimaSumaAsegurada = entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCoberturaReporte);
					estimacionCoberturaSiniestro.getEstimacionGenerica().setSumaAseguradaObtenida(estimaSumaAsegurada.getSumaAseguradaObtenida());
				}
			}
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)) {
			success = MOSTRAR_DANIOS_MATERIALES;
			recibidaOrdenCia = this.siniestroDTO.getRecibidaOrdenCia();
			
			
			if( this.siniestroDTO.getCiaSeguros() != null && !this.siniestroDTO.getCiaSeguros().equals("") ){
				ciaSeguros = Integer.valueOf( this.siniestroDTO.getCiaSeguros() );
			}			
			
			if(estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getTaller()!= null)
			{
				idTaller = estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getTaller().getId();
			}
			
			if(estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCompaniaSegurosTercero() != null)
			{
				idCompaniaSegurosTercero = estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCompaniaSegurosTercero().getId();
			}
			
			siniestroDTO = siniestroService.obtenerSiniestroCabina(
					estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId());
			estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			
			if(estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getId() == null){
				estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setPlacas( 
						estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPlaca() );
				estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			}
			if (estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getEstatus() == null || 
					estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getEstatus().equals("")) {
				estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			}
			
			this.siniestroDTO.setRecibidaOrdenCia( recibidaOrdenCia );
			
			if( ciaSeguros != null ){
				prestadorServicio = entidadService.findById( PrestadorServicio.class, ciaSeguros );
				estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setCompaniaSegurosTercero( prestadorServicio );
				this.idCompaniaSegurosTercero = prestadorServicio.getId();
			}
			validaCausasDeMovimiento();
			
			String fuenteSumaAsegurada = estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCoberturaReporteCabina().getCoberturaDTO().getClaveFuenteSumaAsegurada();
			if ((fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL) || 
					fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA) || 
					fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)) &&
						estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getSumaAseguradaObtenida() == null) {
				estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setSumaAseguradaObtenida(
						new BigDecimal(estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getCoberturaReporteCabina().getValorSumaAsegurada()));
			}
		}
		this.configuraAppDeducible();
		
		// COBERTURA CRISTALES
		this.configuraCoberturaCristales(estimacionCoberturaSiniestro.getEstimacionCoberturaReporte() );
		// # CALCULAR MONTO DEDUCIBLE
		montoDeducible = recuperacionDeducibleService.calcularDeducible(estimacionCoberturaSiniestro.getReporteCabina(),success);
		// PORCENTAJE DEDUCIBLE DMA
		this.porcentajeDeducibleValorComercial = recuperacionDeducibleService.obtenerPorcentajeDeducibleValorComercial();
		this.motivoNoAplicaDeducibleSel = estimacionCoberturaSiniestro.getMotivoNoAplicaDeducible();
		
		if( idEstimacionCoberturaReporte == null ){
			estimacionCoberturaSiniestro.setMontoDeducible( montoDeducible );
			estimacionCoberturaSiniestro.setMontoDeducibleCalculado(montoDeducible);
			
		}
		
		return success;
	}
	
	
	
	@Action(value = "crearNuevoPase", results = {
			@Result(name = MOSTRAR_GASTOS_MEDICOS, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicos.jsp"),
			@Result(name = MOSTRAR_GASTOS_MEDICOS_CONDUCTOR, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicosConductor.jsp"),
			@Result(name = MOSTRAR_RCBIENES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCBienes.jsp"),
			@Result(name = MOSTRAR_RCPERSONA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCPersonas.jsp"),
			@Result(name = MOSTRAR_RCVEHICULO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCVehiculo.jsp"),
			@Result(name = MOSTRAR_RCVIAJERO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCViajero.jsp") })
	/**
	 * Regresa la pantalla correspondiente al tipo de estimacion
	 */
	public String crearNuevoPase() {
		Iterator<Entry<String,String>> pasesAtencionIterator = this.pasesAtencion.entrySet().iterator();
		Map.Entry<String, String> paseAtencionEntry = null;
		Map<String,String> pasesAtencionFiltrado = new HashMap<String, String>();
		BigDecimal montoDeducible = BigDecimal.ZERO;
		
		String success = "";
		estimacionCoberturaSiniestro = estimacionCoberturaSiniestroService.obtenerDatosEstimacionCoberturaNueva( idCoberturaReporteCabina, tipoEstimacion );
		if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setTipoPaseAtencion(TipoPaseAtencion.GASTOS_MEDICOS_OCUPANTES.getValue());
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			success = MOSTRAR_GASTOS_MEDICOS;
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			
			if(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			if(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getOtroHospital()!= null &&  !"".equals(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getOtroHospital())){
				otroHospital = true;
			}else{
				otroHospital = false;
			}
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.GASTOS_MEDICOS_TERCEROS)) {
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			
			success = MOSTRAR_GASTOS_MEDICOS_CONDUCTOR;
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)) {
			this.estimacionCoberturaSiniestro.getEstimacionBienes().setTipoEstimacion( this.tipoEstimacion );
			this.estimacionCoberturaSiniestro.getEstimacionBienes().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			estimacionCoberturaSiniestro.getEstimacionBienes().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			PaisMidas pais = new PaisMidas();
			pais.setId("PAMEXI");
			this.estimacionCoberturaSiniestro.getEstimacionBienes().setPais( pais );

			if (this.estimacionCoberturaSiniestro.getEstimacionBienes().getResponsableReparacion() != null) {
				this.idResponsableReparacion = this.estimacionCoberturaSiniestro.getEstimacionBienes().getResponsableReparacion().getId();
			}

			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.PAGO_DANIOS, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.OBRA_CIVIL)) {			
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			estimacionCoberturaSiniestro.getEstimacionBienes().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			
			success = MOSTRAR_RCBIENES;
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionPersonas().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionPersonas().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			estimacionCoberturaSiniestro.getEstimacionPersonas().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			this.actualizaSiniestroTercero();
			if(estimacionCoberturaSiniestro.getEstimacionPersonas().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionPersonas().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.GASTOS_MEDICOS_TERCEROS)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			estimacionCoberturaSiniestro.getEstimacionPersonas().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			this.permiteCapturarCiaInfo = (estimacionCoberturaSiniestro.getRecibidaOrdenCia().intValue() == 1 && "AFCT".equals(estimacionCoberturaSiniestro.getResponsabilidad()) )?Boolean.TRUE:Boolean.FALSE;
			success = MOSTRAR_RCPERSONA;
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)) {			
			estimacionCoberturaSiniestro.getEstimacionVehiculos().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionVehiculos().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			estimacionCoberturaSiniestro.getEstimacionVehiculos().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			
			this.actualizaSiniestroTercero();
			if(estimacionCoberturaSiniestro.getEstimacionVehiculos().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionVehiculos().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
							TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.PASE_ATENCION_TALLER, TipoPaseAtencion.PAGO_DANIOS, TipoPaseAtencion.SOLO_REG_SIPAC, TipoPaseAtencion.SIPAC)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			estimacionCoberturaSiniestro.getEstimacionVehiculos().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			this.permiteCapturarCiaInfo = (estimacionCoberturaSiniestro.getRecibidaOrdenCia().intValue() == 1 && "AFCT".equals(estimacionCoberturaSiniestro.getResponsabilidad()) )?Boolean.TRUE:Boolean.FALSE;
						
			AutoIncisoReporteCabina autoInciso = estimacionCoberturaSiniestro.getEstimacionCoberturaReporte()
					.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();					
							
			if(EnumUtil.equalsValue(autoInciso.getTerminoAjuste(), TerminoAjuste.SE_ENTREGO_SIPAC, 
							TerminoAjuste.SE_ENTREGO_NA_SIPAC_REEMBOLSOCIA, TerminoAjuste.SE_ENTREGO_NA_SIPAC, 
							TerminoAjuste.SE_ENTREGO_NA_SIPAC_TERCERO, TerminoAjuste.SE_ENTREGO_NA_SIPAC_REEMBOLSOCIA_TERCERO, 
							TerminoAjuste.SE_ENTREGO_SIPAC_OC_TERCERO, TerminoAjuste.SE_ENTREGO_SIPAC_TERCERO,
							TerminoAjuste.SE_ENTREGO_SIPAC_REEMBOLSOCIA)){				
				this.permiteCapturarNoSiniestroTercero = Boolean.TRUE;
			} else {
				this.permiteCapturarNoSiniestroTercero = Boolean.FALSE;
			}
			
			success = MOSTRAR_RCVEHICULO;
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionViajero().setTipoEstimacion(this.tipoEstimacion);
			estimacionCoberturaSiniestro.getEstimacionViajero().getCoberturaReporteCabina().setClaveTipoCalculo(tipoCalculo);
			estimacionCoberturaSiniestro.getEstimacionViajero().setNumeroSiniestroTercero(estimacionCoberturaSiniestro.getNumeroSiniestroCia());
			
			if(estimacionCoberturaSiniestro.getEstimacionViajero().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionViajero().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.GASTOS_MEDICOS_TERCEROS)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			estimacionCoberturaSiniestro.getEstimacionViajero().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			
			success = MOSTRAR_RCVIAJERO;
		}
		
		this.idCompaniaSegurosTercero = estimacionCoberturaSiniestro.getCompaniaOrdenId();
		
		//this.aplicaSoloRegistroParaRC();
		this.validaCausasDeMovimiento();
		
		// COBERTURA CRISTALES
		this.configuraCoberturaCristales(estimacionCoberturaSiniestro.getEstimacionCoberturaReporte() );
		
		// # CALCULAR MONTO DEDUCIBLE
		montoDeducible = recuperacionDeducibleService.calcularDeducible(estimacionCoberturaSiniestro.getReporteCabina(),success);
		estimacionCoberturaSiniestro.setMontoDeducible( montoDeducible );
		estimacionCoberturaSiniestro.setMontoDeducibleCalculado(montoDeducible);
		
		return success;
	}
	
	
	@Action(value = "mostrarPase", results = {
			@Result(name = MOSTRAR_GASTOS_MEDICOS, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicos.jsp"),
			@Result(name = MOSTRAR_GASTOS_MEDICOS_CONDUCTOR, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicosConductor.jsp"),
			@Result(name = MOSTRAR_RCBIENES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCBienes.jsp"),
			@Result(name = MOSTRAR_RCPERSONA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCPersonas.jsp"),
			@Result(name = MOSTRAR_RCVEHICULO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCVehiculo.jsp"),
			@Result(name = MOSTRAR_RCVIAJERO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCViajero.jsp"),
			@Result(name = MOSTRAR_LISTA_PASES, location = "/jsp/siniestros/cabina/pasesatencion/listadoPaseAtencion.jsp") })
	public String mostrarPase() {
		if(this.modoConsultaDeListado!= null && this.modoConsultaDeListado){
			this.pasesAtencion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);
		}
		Iterator<Entry<String,String>> pasesAtencionIterator = this.pasesAtencion.entrySet().iterator();
		Map.Entry<String, String> paseAtencionEntry = null;
		Map<String,String> pasesAtencionFiltrado = new HashMap<String, String>();
		String success = "";
		
		EstimacionCoberturaReporteCabina estimacion  = entidadService.findById(EstimacionCoberturaReporteCabina.class, this.idPaseDeAtencion);
		estimacionCoberturaSiniestro = this.estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
		this.idEstimacionCoberturaReporte = estimacionCoberturaSiniestro.getEstimacionCoberturaReporte().getId();
		this.permiteCapturarCiaInfo = (estimacionCoberturaSiniestro.getRecibidaOrdenCia().intValue() == 1 && estimacionCoberturaSiniestro.getResponsabilidad().equals("AFCT") )?Boolean.TRUE:Boolean.FALSE;
		
		if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getCompaniaSegurosTercero()!= null) {
				this.idCompaniaSegurosTercero = this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getCompaniaSegurosTercero().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getHospital() != null) {
				this.idHospital = this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getHospital().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getOtroHospital() != null && !this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getOtroHospital().equals("")) {
				this.otroHospital = true;
			}else{
				this.otroHospital = false;
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getMedico() != null) {
				this.idMedico = this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().getMedico().getId();
			}
			//estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setEstatus( ESTATUS_ESTIMACION_PENDIENTE );
			success = MOSTRAR_GASTOS_MEDICOS;
		} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getCompaniaSegurosTercero()!= null) {
				this.idCompaniaSegurosTercero = this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getCompaniaSegurosTercero().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getHospital() != null) {
				this.idHospital = this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getHospital().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getMedico() != null) {
				this.idMedico = this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getMedico().getId();
			}
			
			if(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			if(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getOtroHospital()!= null &&  !"".equals(estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().getOtroHospital())){
				otroHospital = false;
			}else{
				otroHospital = false;
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.GASTOS_MEDICOS_TERCEROS)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			
			success = MOSTRAR_GASTOS_MEDICOS_CONDUCTOR;
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)) {
			this.estimacionCoberturaSiniestro.getEstimacionBienes().setTipoEstimacion( this.tipoEstimacion );
			this.estimacionCoberturaSiniestro.getEstimacionBienes().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			PaisMidas pais = new PaisMidas();
			pais.setId("PAMEXI");
			this.estimacionCoberturaSiniestro.getEstimacionBienes().setPais( pais );

			if (this.estimacionCoberturaSiniestro.getEstimacionBienes().getResponsableReparacion() != null) {
				this.idResponsableReparacion = this.estimacionCoberturaSiniestro.getEstimacionBienes().getResponsableReparacion().getId();
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.PAGO_DANIOS, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.OBRA_CIVIL)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			
			success = MOSTRAR_RCBIENES;
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionPersonas().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionPersonas().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			this.actualizaSiniestroTercero();
			if(estimacionCoberturaSiniestro.getEstimacionPersonas().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionPersonas().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionPersonas().getCompaniaSegurosTercero()!= null) {
				this.idCompaniaSegurosTercero = this.estimacionCoberturaSiniestro.getEstimacionPersonas().getCompaniaSegurosTercero().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionPersonas().getHospital() != null) {
				this.idHospital = this.estimacionCoberturaSiniestro.getEstimacionPersonas().getHospital().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionPersonas().getOtroHospital() != null) {
				this.otroHospital = Boolean.TRUE;
			}else{
				this.otroHospital = Boolean.FALSE;
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionPersonas().getMedico() != null) {
				this.idMedico = this.estimacionCoberturaSiniestro.getEstimacionPersonas().getMedico().getId();
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.GASTOS_MEDICOS_TERCEROS)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			
			success = MOSTRAR_RCPERSONA;
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionVehiculos().setTipoEstimacion( this.tipoEstimacion );
			estimacionCoberturaSiniestro.getEstimacionVehiculos().getCoberturaReporteCabina().setClaveTipoCalculo( tipoCalculo );
			this.actualizaSiniestroTercero();
			if(estimacionCoberturaSiniestro.getEstimacionVehiculos().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionVehiculos().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionVehiculos().getCompaniaSegurosTercero() != null) {
				this.idCompaniaSegurosTercero = this.estimacionCoberturaSiniestro.getEstimacionVehiculos().getCompaniaSegurosTercero().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionVehiculos().getTaller() != null) {
				this.idTaller = this.estimacionCoberturaSiniestro.getEstimacionVehiculos().getTaller().getId();
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
							TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.PASE_ATENCION_TALLER, TipoPaseAtencion.PAGO_DANIOS, TipoPaseAtencion.SIPAC, TipoPaseAtencion.SOLO_REG_SIPAC)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			
			AutoIncisoReporteCabina autoInciso = estimacionCoberturaSiniestro.getEstimacionCoberturaReporte()
					.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();					
							
			if(EnumUtil.equalsValue(autoInciso.getTerminoAjuste(), TerminoAjuste.SE_ENTREGO_SIPAC, 
							TerminoAjuste.SE_ENTREGO_NA_SIPAC_REEMBOLSOCIA, TerminoAjuste.SE_ENTREGO_NA_SIPAC, 
							TerminoAjuste.SE_ENTREGO_NA_SIPAC_TERCERO, TerminoAjuste.SE_ENTREGO_NA_SIPAC_REEMBOLSOCIA_TERCERO, 
							TerminoAjuste.SE_ENTREGO_SIPAC_OC_TERCERO, TerminoAjuste.SE_ENTREGO_SIPAC_TERCERO,
							TerminoAjuste.SE_ENTREGO_SIPAC_REEMBOLSOCIA)){				
				this.permiteCapturarNoSiniestroTercero = Boolean.TRUE;
			} else {
				this.permiteCapturarNoSiniestroTercero = Boolean.FALSE;
			}
			
			success = MOSTRAR_RCVEHICULO;
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)) {
			estimacionCoberturaSiniestro.getEstimacionViajero().setTipoEstimacion(this.tipoEstimacion);
			estimacionCoberturaSiniestro.getEstimacionViajero().getCoberturaReporteCabina().setClaveTipoCalculo(tipoCalculo);
			
			if (this.estimacionCoberturaSiniestro.getEstimacionViajero().getCompaniaSegurosTercero()!= null) {
				this.idCompaniaSegurosTercero = this.estimacionCoberturaSiniestro.getEstimacionViajero().getCompaniaSegurosTercero().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionViajero().getHospital() != null) {
				this.idHospital = this.estimacionCoberturaSiniestro.getEstimacionViajero().getHospital().getId();
			}
			
			if (this.estimacionCoberturaSiniestro.getEstimacionViajero().getMedico() != null) {
				this.idMedico = this.estimacionCoberturaSiniestro.getEstimacionViajero().getMedico().getId();
			}
			
			if(estimacionCoberturaSiniestro.getEstimacionViajero().getTieneCompaniaSeguros() != null){
				if(estimacionCoberturaSiniestro.getEstimacionViajero().getTieneCompaniaSeguros()){
					tieneCiaSegurosValor = "S";
				}else{
					tieneCiaSegurosValor = "N";
				}
			}
			
			if (SystemCommonUtils.isValid(this.estimacionCoberturaSiniestro.getEstimacionViajero().getOtroHospital())) {
				this.otroHospital = Boolean.TRUE;
			}else{
				this.otroHospital = Boolean.FALSE;
			}
			
			while(pasesAtencionIterator.hasNext()){
				paseAtencionEntry = pasesAtencionIterator.next();
				if (EnumUtil.equalsValue(paseAtencionEntry.getKey(), TipoPaseAtencion.REEMBOLSO_A_CIA, 
										TipoPaseAtencion.SOLO_REGISTRO, TipoPaseAtencion.GASTOS_MEDICOS_TERCEROS)) {				
					pasesAtencionFiltrado.put( paseAtencionEntry.getKey(), paseAtencionEntry.getValue() );
				}
			}
			this.pasesAtencion = pasesAtencionFiltrado;
			
			success = MOSTRAR_RCVIAJERO;
		}
		
		this.idCompaniaSegurosTercero = estimacionCoberturaSiniestro.getCompaniaOrdenId();
		this.motivoNoAplicaDeducibleSel = estimacionCoberturaSiniestro.getMotivoNoAplicaDeducible();
		
		this.configuraAppDeducible();
		this.validaCausasDeMovimiento();
		return success;
	}
	
	private void actualizaSiniestroTercero(){
		Long idPase = null;
		if(estimacionCoberturaSiniestro.getEstimacionVehiculos()!=null){
			idPase = estimacionCoberturaSiniestro.getEstimacionVehiculos().getId();
		}else if(estimacionCoberturaSiniestro.getEstimacionPersonas()!=null){
			idPase = estimacionCoberturaSiniestro.getEstimacionPersonas().getId();
		}
		 
		if(idPase!=null){
			EstimacionCoberturaReporteCabina pase = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idPase);
			this.numeroSiniestroEstimacion = pase.getNumeroSiniestroTercero();
		}
		if(this.idCoberturaReporteCabina!=null){
			CoberturaReporteCabina cobertura = this.entidadService.findById(CoberturaReporteCabina.class, this.idCoberturaReporteCabina);
			this.numeroSiniestroAfectacion = cobertura.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia();
		}
	}
	
	private void configuraCoberturaCristales(EstimacionCoberturaReporteCabina estimacion){

		// DEDUCIBLE COBERTURA CRISTALES - 1 EQUIVALE A PORCENTAJE DEDUCIBLE
		if( estimacion.getCoberturaReporteCabina().getClaveTipoDeducible() != null &&  estimacion.getCoberturaReporteCabina().getClaveTipoDeducible() == 1 ){
			if(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina()
					.getAutoIncisoReporteCabina().getCausaSiniestro().equalsIgnoreCase(CausaSiniestro.CRISTALES.getValue()))
			{
				this.porcentajeDeducible = this.recuperacionDeducibleService.obtenerPorcentajeDeducible();
			}
		}
	}

	/**
	 * Obtiene el listado de los Pases de Atencion para una Cobertura de tercero
	 */
	@Action(value = "obtenerListadoPases", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/listadoPasesAtencionGrid.jsp") })
	public String obtenerListadoPases() {
		this.pasesAtencionTercero = estimacionCoberturaSiniestroService.obtenerListadoPases( this.idCoberturaReporteCabina, this.tipoEstimacion );
		return SUCCESS;
	}

	/**
	 * Guarda la estimacion
	 */
	@Action(value = "guardar", results = { 
			@Result(name = "SUCCESS_DIRECTO", type = "redirectAction", params = {
				"actionName", "${methodName}",
				"mensaje", "${mensaje}", "mensajeListaPersonalizado",
				"${mensajeListaPersonalizado}", "tipoMensaje", "${tipoMensaje}",
				"idCoberturaReporteCabina", "${idCoberturaReporteCabina}",
				"tipoCalculo", "${tipoCalculo}", "tipoEstimacion",
				"${tipoEstimacion}", "reporteCabinaId", "${reporteCabinaId}","soloConsulta", "${soloConsulta}",
				"estatusHabilitado","${estatusHabilitado}",
				"isListadoPases","${isListadoPases}"}),
			@Result(name = "SUCCESS_PASEATENCION", type = "redirectAction", params = {
				"actionName", "${methodName}",
				"mensaje", "${mensaje}", "mensajeListaPersonalizado",
				"${mensajeListaPersonalizado}", "tipoMensaje", "${tipoMensaje}",
				"idCoberturaReporteCabina", "${idCoberturaReporteCabina}",
				"tipoCalculo", "${tipoCalculo}", "tipoEstimacion",
				"${tipoEstimacion}", 
				"reporteCabinaId", "${reporteCabinaId}","idPaseDeAtencion", "${idPaseDeAtencion}","soloConsulta", "${soloConsulta}",
				"estatusHabilitado","${estatusHabilitado}",
				"isListadoPases","${isListadoPases}"}),
			@Result(name = MOSTRAR_GASTOS_MEDICOS, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicos.jsp"),
			@Result(name = MOSTRAR_GASTOS_MEDICOS_CONDUCTOR, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicosConductor.jsp"),
			@Result(name = MOSTRAR_RCBIENES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCBienes.jsp"),
			@Result(name = MOSTRAR_RCPERSONA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCPersonas.jsp"),
			@Result(name = MOSTRAR_RCVEHICULO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCVehiculo.jsp"),
			@Result(name = MOSTRAR_RCVIAJERO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCViajero.jsp"),
			@Result(name = MOSTRAR_ESTIMACION_DIRECTA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionDirecta.jsp"),
			@Result(name = MOSTRAR_LISTA_PASES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/listadoPasesAtencion.jsp"),
			@Result(name = MOSTRAR_DANIOS_MATERIALES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionDanosMateriales.jsp"),
			@Result(name = INPUT, location = "/jsp/errorImpresiones.jsp")})
	public String guardar() {
		
		//Proceso de validacion de permisos para alterar la estimacion del pase
		String success = this.tipoEstimacion;
		
		if(tipoEstimacion != null){
			EstimacionCoberturaReporteCabina estimacion = this.estimacionCoberturaSiniestroService.selectEstimacionCoberturaReporteCabina(this.estimacionCoberturaSiniestro, tipoEstimacion);
			BigDecimal estimacionNueva= this.estimacionCoberturaSiniestro.getDatosEstimacion().getEstimacionNueva(); 
			BigDecimal estimacionActual = this.estimacionCoberturaSiniestro.getDatosEstimacion().getEstimacionActual();
			
			String tipoPaseAtencion = estimacion.getTipoPaseAtencion();
			String tipoEstimacion = estimacion.getTipoEstimacion();
			if(tipoPaseAtencion!=null){
				if(MOSTRAR_RCVEHICULO.equals(tipoEstimacion)){
					this.numeroSiniestroEstimacion = estimacionCoberturaSiniestro.getEstimacionVehiculos().getNumeroSiniestroTercero();
				}else if(MOSTRAR_RCPERSONA.equals(tipoEstimacion)){
					this.numeroSiniestroEstimacion = estimacionCoberturaSiniestro.getEstimacionPersonas().getNumeroSiniestroTercero();
				}
			}
			
			if(estimacion.getId()!=null){
				if(estimacionNueva!=null && estimacionActual!= null && estimacionNueva.compareTo(estimacionActual)!=0 ){
					this.idParametroAntiguedad = this.autorizacionReservaService.validarParametrosAntiguedad(estimacion.getId(), this.estimacionCoberturaSiniestro.getDatosEstimacion().getEstimacionNueva());
					if(this.idParametroAntiguedad==null){
						String mensaje = autorizacionReservaService.validarParametros(estimacion.getId(),this.estimacionCoberturaSiniestro.getDatosEstimacion().getEstimacionNueva());
						if(mensaje!=null){
							this.requiereAutorizacion = REQUIERE_AUTORIZACION_AJUSTE;
							super.setMensaje(mensaje);
							super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_INFORMACION);
						}else{
							success = this.guardaEstimacion();
						}
					}else{
						
						// # NOTIFICACION DE SINIESTRO ANTIGUO
						this.estimacionCoberturaSiniestroService.notificaSolicitudSiniestroAntiguo(this.estimacionCoberturaSiniestro, this.reporteCabinaId);
						this.requiereAutorizacion = REQUIERE_AUTORIZACION_ANTIGUEDAD;
						super.setMensaje(MensajeDTO.MENSAJE_REQUIERE_AUT_RESERVA);
						super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_INFORMACION);
					}
				}else{
					success = this.guardaEstimacion();
				}
			}else{
				success = this.guardaEstimacion();
			}
		}else{
			super.setMensaje(MensajeDTO.MENSAJE_ERROR_GUARDAR_ESTIMACION);
			super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			success = INPUT;
		}
		return success;

	}
	
	
	private String guardaEstimacion(){
		List<String> errores 	= null;
		String success 			= "";
		try {
			this.guardarPrestadoresServicio();					
			this.estimacionCoberturaSiniestro.setAplicaDeducible(StringUtil.isEmpty(apDeducible) || apDeducible.equals("N") ? Boolean.FALSE : Boolean.TRUE);
			errores = estimacionCoberturaSiniestroService.guardarEstimacionCobertura( this.estimacionCoberturaSiniestro, this.idCoberturaReporteCabina, this.tipoEstimacion );
			if (!errores.isEmpty()) {
				super.setMensajeListaPersonalizado("Campos Requeridos", errores, BaseAction.TIPO_MENSAJE_ERROR);
				success = this.tipoEstimacion;
			} else {
				setMensaje(MensajeDTO.MENSAJE_GUARDAR_ESTIMACION);
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			}
		} catch (Exception e) {
			super.setMensaje(MensajeDTO.MENSAJE_ERROR_GUARDAR_ESTIMACION);
			super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			success = this.tipoEstimacion;
		}
				
		
		if ( errores != null && errores.isEmpty() ) {
			if (EnumUtil.equalsValue(this.tipoEstimacion, TipoEstimacion.RC_VIAJERO, TipoEstimacion.GASTOS_MEDICOS, 
					TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, TipoEstimacion.RC_BIENES,  TipoEstimacion.RC_PERSONA, TipoEstimacion.RC_VEHICULO)) {
				this.idPaseDeAtencion = getIdEstimacion(this.tipoEstimacion, this.estimacionCoberturaSiniestro);
				success = "SUCCESS_PASEATENCION";
				methodName = "mostrarPase";
			}else{
				success = "SUCCESS_DIRECTO";
				methodName = "mostrarEstimacion";
			}
		}
		
		if( this.isListadoPases != null && isListadoPases.equals("1") ){
			isListadoPases = "2";
		}
		return success;
	}
	
	@Action(value = "findEstimacionNueva", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estimacionNueva"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estimacionNueva,mensaje"})
		})
	public String findEstimacionNueva(){	
		try{
			this.estimacionCoberturaSiniestroService.selectEstimacionCoberturaReporteCabina(
					this.estimacionCoberturaSiniestro, this.tipoEstimacion);
			this.estimacionNueva = this.estimacionCoberturaSiniestro.getDatosEstimacion()!=null?this.estimacionCoberturaSiniestro.getDatosEstimacion().getEstimacionNueva():null;
			return SUCCESS;
		}catch(Exception ex){
			setMensajeError("Error al obtener datos de la estimacion");
			return INPUT;
		}
	}
	
	@Action(value = "autorizarReserva", results = {
			@Result(name = MOSTRAR_GASTOS_MEDICOS, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicos.jsp"),
			@Result(name = MOSTRAR_GASTOS_MEDICOS_CONDUCTOR, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionGastosMedicosConductor.jsp"),
			@Result(name = MOSTRAR_RCBIENES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCBienes.jsp"),
			@Result(name = MOSTRAR_RCPERSONA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCPersonas.jsp"),
			@Result(name = MOSTRAR_RCVEHICULO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCVehiculo.jsp"),
			@Result(name = MOSTRAR_RCVIAJERO, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionRCViajero.jsp"),
			@Result(name = MOSTRAR_ESTIMACION_DIRECTA, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionDirecta.jsp"),
			@Result(name = MOSTRAR_LISTA_PASES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/listadoPasesAtencion.jsp"),
			@Result(name = MOSTRAR_DANIOS_MATERIALES, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/estimacionDanosMateriales.jsp")})
	public String autorizarReserva(){
		
		String success = this.tipoEstimacion;
		EstimacionCoberturaReporteCabina estimacion = this.estimacionCoberturaSiniestroService.selectEstimacionCoberturaReporteCabina(this.estimacionCoberturaSiniestro, tipoEstimacion);
		Long idEstimacion = estimacion.getId();
		BigDecimal estimacionNueva = this.estimacionCoberturaSiniestro.getDatosEstimacion().getEstimacionNueva();
		String causaMovimiento = this.estimacionCoberturaSiniestro.getDatosEstimacion().getCausaMovimiento();
		if(this.requiereAutorizacion== REQUIERE_AUTORIZACION_AJUSTE){
			this.catSolicitudAutorizacionService.solicitarAutorizacionReserva(idEstimacion,estimacionNueva, causaMovimiento );
		}else if(this.requiereAutorizacion==REQUIERE_AUTORIZACION_ANTIGUEDAD){
			this.catSolicitudAutorizacionService.solicitarAutorizacionReservaAntiguedad(idEstimacion, estimacionNueva, this.idParametroAntiguedad, causaMovimiento);
		}
		this.requiereAutorizacion = 0;
		super.setMensaje("Autorizacion Enviada");
		super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		return success;
	}
	
	@Action(value="estaNumeroDeSerieinvolucradoEnSiniestrosAntiguos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^tieneAntiguedadELNoSerie"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^tieneAntiguedadELNoSerie"})
		})
	public String estaNumeroDeSerieinvolucradoEnSiniestrosAntiguos(){
		String noSerie = this.estimacionCoberturaSiniestro.getEstimacionVehiculos().getNumeroSerie();
		this.tieneAntiguedadELNoSerie = this.estimacionCoberturaSiniestroService.tieneNumeroDeSerieSiniestrosAnteriores(this.idEstimacionCoberturaReporte, noSerie);
	return SUCCESS;
	}
	
	@Action(value="permiteCambiarDeducible",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^permiteCambiarDeducible"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^permiteCambiarDeducible"})
		})
	public String permiteCambiarDeducible(){
		permiteCambiarDeducible = estimacionCoberturaSiniestroService.permiteCambiarDeducible(this.idEstimacionCoberturaReporte);
		return SUCCESS;
	}
	
	
	private Long getIdEstimacion(String tipoEstimacion , EstimacionCoberturaSiniestroDTO dto){
		Long id = 0l;
		
		if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)) {
			id = dto.getEstimacionBienes().getId();
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)){
			id = dto.getEstimacionGastosMedicos().getId();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)){
			id = dto.getEstimacionPersonas().getId();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)){
			id = dto.getEstimacionViajero().getId();
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)){
			id = dto.getEstimacionGastosMedicosConductor().getId();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)){
			id = dto.getEstimacionVehiculos().getId();
		}
		
		return id;
	}
	
	private void guardarPrestadoresServicio() {
		
		if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)) {
			if(idCompaniaSegurosTercero != null){
				PrestadorServicio pestadorSeguroTercero = entidadService.findById(PrestadorServicio.class, this.idCompaniaSegurosTercero);
				this.estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setCompaniaSegurosTercero(pestadorSeguroTercero);
			}
			if(idTaller != null){
				PrestadorServicio pestadorServicioTaller = entidadService.findById(PrestadorServicio.class, this.idTaller);
				this.estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setTaller(pestadorServicioTaller);
			}

			if(estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getEstado().getId() == null || "".equals(estimacionCoberturaSiniestro.getEstimacionDanosMateriales().getEstado().getId())){
				estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setEstado(null);
			}
			estimacionCoberturaSiniestro.getEstimacionDanosMateriales().setEstatus( this.idEstatus );
		
		} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)) {
			if( this.idHospital != null ){
				PrestadorServicio prestadorHospital = entidadService.findById(PrestadorServicio.class, this.idHospital);
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setHospital( prestadorHospital );
			}
			
			if( this.idMedico != null ){
				PrestadorServicio prestadorMedico = entidadService.findById(PrestadorServicio.class, this.idMedico);
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setMedico( prestadorMedico );
			}
			
			if( this.idCompaniaSegurosTercero != null ){
				PrestadorServicio pestadorSeguroTercero = entidadService.findById(PrestadorServicio.class, this.idCompaniaSegurosTercero);
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setCompaniaSegurosTercero( pestadorSeguroTercero );
			}
			estimacionCoberturaSiniestro.getEstimacionGastosMedicos().setEstatus( this.idEstatus );
			
		} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)) {
			if( this.idHospital != null ){
				PrestadorServicio prestadorHospital = entidadService.findById(PrestadorServicio.class, this.idHospital);
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setHospital( prestadorHospital );
			}
			if( this.idMedico != null ){
				PrestadorServicio prestadorMedico =  entidadService.findById(PrestadorServicio.class, this.idMedico);
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setMedico( prestadorMedico );
			}
			if("S".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setTieneCompaniaSeguros(true);
			}else if("N".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setTieneCompaniaSeguros(false);
			}
			if( this.idCompaniaSegurosTercero != null ){
				PrestadorServicio pestadorSeguroTercero = entidadService.findById(PrestadorServicio.class, this.idCompaniaSegurosTercero);
				this.estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setCompaniaSegurosTercero( pestadorSeguroTercero );
			}
			estimacionCoberturaSiniestro.getEstimacionGastosMedicosConductor().setEstatus( this.idEstatus );
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)) {
			if( this.idResponsableReparacion != null ){
				PrestadorServicio prestadorResponsableReparacion = this.entidadService.findById(PrestadorServicio.class, this.idResponsableReparacion);
				this.estimacionCoberturaSiniestro.getEstimacionBienes().setResponsableReparacion( prestadorResponsableReparacion );
			}
			
			if(estimacionCoberturaSiniestro.getEstimacionBienes().getEstado().getId() == null || "".equals(estimacionCoberturaSiniestro.getEstimacionBienes().getEstado().getId())){
				estimacionCoberturaSiniestro.getEstimacionBienes().setEstado(null);
			}
			if(estimacionCoberturaSiniestro.getEstimacionBienes().getMunicipio().getId() == null || "".equals(estimacionCoberturaSiniestro.getEstimacionBienes().getMunicipio().getId())){
				estimacionCoberturaSiniestro.getEstimacionBienes().setMunicipio(null);
			}
			estimacionCoberturaSiniestro.getEstimacionBienes().setEstatus( this.idEstatus );
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)) {
			if( this.idHospital != null ){
				PrestadorServicio prestadorHospital = entidadService.findById(PrestadorServicio.class, this.idHospital);
				this.estimacionCoberturaSiniestro.getEstimacionPersonas().setHospital( prestadorHospital );
			}
	
			if( this.idMedico != null ){
				PrestadorServicio prestadorMedico = this.entidadService.findById(PrestadorServicio.class, this.idMedico );
				this.estimacionCoberturaSiniestro.getEstimacionPersonas().setMedico( prestadorMedico );
			}
			
			if("S".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionPersonas().setTieneCompaniaSeguros(true);
			}else if("N".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionPersonas().setTieneCompaniaSeguros(false);
			}
			
			if( this.idCompaniaSegurosTercero != null ){
				PrestadorServicio pestadorSeguroTercero = entidadService.findById(PrestadorServicio.class, this.idCompaniaSegurosTercero);
				this.estimacionCoberturaSiniestro.getEstimacionPersonas().setCompaniaSegurosTercero( pestadorSeguroTercero );
			}
			estimacionCoberturaSiniestro.getEstimacionPersonas().setEstatus( this.idEstatus );
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)) {
			if(idCompaniaSegurosTercero != null){
				PrestadorServicio pestadorSeguroTercero = entidadService.findById(PrestadorServicio.class, this.idCompaniaSegurosTercero);
				this.estimacionCoberturaSiniestro.getEstimacionVehiculos().setCompaniaSegurosTercero(pestadorSeguroTercero);
			}
			if(idTaller != null){
				PrestadorServicio pestadorServicioTaller = entidadService.findById(PrestadorServicio.class, this.idTaller);
				this.estimacionCoberturaSiniestro.getEstimacionVehiculos().setTaller(pestadorServicioTaller);
			}
			if("S".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionVehiculos().setTieneCompaniaSeguros(true);
			}else if("N".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionVehiculos().setTieneCompaniaSeguros(false);
			}
			if(estimacionCoberturaSiniestro.getEstimacionVehiculos().getEstado().getId() == null || "".equals(estimacionCoberturaSiniestro.getEstimacionVehiculos().getEstado().getId())){
				estimacionCoberturaSiniestro.getEstimacionVehiculos().setEstado(null);
			}
			estimacionCoberturaSiniestro.getEstimacionVehiculos().setEstatus( this.idEstatus );
		} else if (EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)) {
			if(this.idCompaniaSegurosTercero != null){
				PrestadorServicio pestadorSeguroTercero = entidadService.findById(PrestadorServicio.class, this.idCompaniaSegurosTercero);
				this.estimacionCoberturaSiniestro.getEstimacionViajero().setCompaniaSegurosTercero(pestadorSeguroTercero );
			}
			
			if( this.idHospital != null){
				PrestadorServicio prestadorHospital = entidadService.findById(PrestadorServicio.class, this.idHospital);
				this.estimacionCoberturaSiniestro.getEstimacionViajero().setHospital(prestadorHospital);
			}

			if( this.idMedico != null){
				PrestadorServicio prestadorMedico = entidadService.findById(PrestadorServicio.class, this.idMedico);
				this.estimacionCoberturaSiniestro.getEstimacionViajero().setMedico(prestadorMedico);
			}
			
			if("S".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionViajero().setTieneCompaniaSeguros(true);
			}else if("N".equalsIgnoreCase(tieneCiaSegurosValor)){
				this.estimacionCoberturaSiniestro.getEstimacionViajero().setTieneCompaniaSeguros(false);
			}
			
			estimacionCoberturaSiniestro.getEstimacionViajero().setEstatus( this.idEstatus );
		}		
	}
	
	private void validaCausasDeMovimiento(){
		this.causasMovimiento = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);
		
		if (this.idEstimacionCoberturaReporte == null) {
				String value = this.causasMovimiento.get(CausaMovimiento.APERTURA_RESERVA.toString());
				this.causasMovimiento.clear();
				this.causasMovimiento.put(CausaMovimiento.APERTURA_RESERVA.toString(), value);
				
		} else {
			this.causasMovimiento.remove(CausaMovimiento.APERTURA_RESERVA.toString());
		}
	}
	
	private void configuraAppDeducible(){
		EstimacionCoberturaReporteCabina estimacion = this.estimacionCoberturaSiniestroService.selectEstimacionCoberturaReporteCabina(estimacionCoberturaSiniestro, this.tipoEstimacion);
		if(estimacion.getAplicaDeducible()!=null){
			if(estimacionCoberturaSiniestro.getEstimacionCoberturaReporte().getAplicaDeducible()){
				apDeducible = "S";
			}else{
				apDeducible = "N";
			}
		}
	}
	
	
	@Action(value="obtieneCompaniaSiniestro",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^permiteCambiarDeducible"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^permiteCambiarDeducible"})
		})
	public String obtieneCompaniaSiniestro(){
		permiteCambiarDeducible = estimacionCoberturaSiniestroService.permiteCambiarDeducible(this.idEstimacionCoberturaReporte);
		return SUCCESS;
	}
	

	@Action(value="validarCESVI",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^reponseDeCESVI"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^reponseDeCESVI"})
		})
	public String validarCESVI(){
		String noSerie = this.estimacionCoberturaSiniestro.getEstimacionVehiculos().getNumeroSerie();
		this.reponseDeCESVI ="";
		Respuesta respuestaCesvi;
			try {
				 respuestaCesvi = informacionVehicularService.obtenerInformacionVehiculo(noSerie);
				 this.reponseDeCESVI=respuestaCesvi.getObservacionesSesa();
			} catch (Exception e) {
				this.reponseDeCESVI="Se genera el siguiente error al consultar servicio de CESVI: "+e.getMessage();
				e.printStackTrace();
			}
			
			
			
	return SUCCESS;
	}

	public InformacionVehicularService getInformacionVehicularService() {
		return informacionVehicularService;
	}

	public void setInformacionVehicularService(
			InformacionVehicularService informacionVehicularService) {
		this.informacionVehicularService = informacionVehicularService;
	}
	
	/**
	 * @return the pasesAtencion
	 */
	public Map<String, String> getPasesAtencion() {
		return pasesAtencion;
	}

	/**
	 * @param pasesAtencion
	 *            the pasesAtencion to set
	 */
	public void setPasesAtencion(Map<String, String> pasesAtencion) {
		this.pasesAtencion = pasesAtencion;
	}

	/**
	 * @return the causasMovimiento
	 */
	public Map<String, String> getCausasMovimiento() {
		return causasMovimiento;
	}

	/**
	 * @param causasMovimiento
	 *            the causasMovimiento to set
	 */
	public void setCausasMovimiento(Map<String, String> causasMovimiento) {
		this.causasMovimiento = causasMovimiento;
	}

	/**
	 * @return the estatus
	 */
	public Map<String, String> getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus
	 *            the estatus to set
	 */
	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the danos
	 */
	public Map<String, String> getDanos() {
		return danos;
	}

	/**
	 * @param danos
	 *            the danos to set
	 */
	public void setDanos(Map<String, String> danos) {
		this.danos = danos;
	}

	/**
	 * @return the tiposBien
	 */
	public Map<String, String> getTiposBien() {
		return tiposBien;
	}

	/**
	 * @param tiposBien
	 *            the tiposBien to set
	 */
	public void setTiposBien(Map<String, String> tiposBien) {
		this.tiposBien = tiposBien;
	}

	/**
	 * @return the tiposEstado
	 */
	public Map<String, String> getTiposEstado() {
		return tiposEstado;
	}

	/**
	 * @param tiposEstado
	 *            the tiposEstado to set
	 */
	public void setTiposEstado(Map<String, String> tiposEstado) {
		this.tiposEstado = tiposEstado;
	}

	/**
	 * @return the tiposAtencion
	 */
	public Map<String, String> getTiposAtencion() {
		return tiposAtencion;
	}

	/**
	 * @param tiposAtencion
	 *            the tiposAtencion to set
	 */
	public void setTiposAtencion(Map<String, String> tiposAtencion) {
		this.tiposAtencion = tiposAtencion;
	}

	/**
	 * @return the paises
	 */
	public Map<String, String> getPaises() {
		return paises;
	}

	/**
	 * @param paises
	 *            the paises to set
	 */
	public void setPaises(Map<String, String> paises) {
		this.paises = paises;
	}

	/**
	 * @return the estados
	 */
	public Map<String, String> getEstados() {
		return estados;
	}

	/**
	 * @param estados
	 *            the estados to set
	 */
	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	/**
	 * @return the municipios
	 */
	public Map<String, String> getMunicipios() {
		return municipios;
	}

	/**
	 * @param municipios
	 *            the municipios to set
	 */
	public void setMunicipios(Map<String, String> municipios) {
		this.municipios = municipios;
	}

	/**
	 * @return the soloConsulta
	 */
	public Short getSoloConsulta() {
		return soloConsulta;
	}

	/**
	 * @param soloConsulta
	 *            the soloConsulta to set
	 */
	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	/**
	 * @return the terminoAjuste
	 */
	public Long getTerminoAjuste() {
		return terminoAjuste;
	}

	/**
	 * @param terminoAjuste
	 *            the terminoAjuste to set
	 */
	public void setTerminoAjuste(Long terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	/**
	 * @return the tipoSiniestro
	 */
	public Long getTipoSiniestro() {
		return tipoSiniestro;
	}

	/**
	 * @param tipoSiniestro
	 *            the tipoSiniestro to set
	 */
	public void setTipoSiniestro(Long tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	/**
	 * @return the tipoResponsabilidad
	 */
	public Long getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}

	/**
	 * @param tipoResponsabilidad
	 *            the tipoResponsabilidad to set
	 */
	public void setTipoResponsabilidad(Long tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}

	/**
	 * @return the idCoberturaReporteCabina
	 */
	public Long getIdCoberturaReporteCabina() {
		return idCoberturaReporteCabina;
	}

	/**
	 * @param idCoberturaReporteCabina
	 *            the idCoberturaReporteCabina to set
	 */
	public void setIdCoberturaReporteCabina(Long idCoberturaReporteCabina) {
		this.idCoberturaReporteCabina = idCoberturaReporteCabina;
	}

	/**
	 * @return the estimacionCoberturaSiniestro
	 */
	public EstimacionCoberturaSiniestroDTO getEstimacionCoberturaSiniestro() {
		return estimacionCoberturaSiniestro;
	}

	/**
	 * @param estimacionCoberturaSiniestro
	 *            the estimacionCoberturaSiniestro to set
	 */
	public void setEstimacionCoberturaSiniestro(
			EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestro) {
		this.estimacionCoberturaSiniestro = estimacionCoberturaSiniestro;
	}

	/**
	 * @return the idEstimacionCoberturaReporte
	 */
	public Long getIdEstimacionCoberturaReporte() {
		return idEstimacionCoberturaReporte;
	}

	/**
	 * @param idEstimacionCoberturaReporte
	 *            the idEstimacionCoberturaReporte to set
	 */
	public void setIdEstimacionCoberturaReporte(
			Long idEstimacionCoberturaReporte) {
		this.idEstimacionCoberturaReporte = idEstimacionCoberturaReporte;
	}

	/**
	 * @return the pasesAtencionTercero
	 */
	public List<PaseAtencionSiniestroDTO> getPasesAtencionTercero() {
		return pasesAtencionTercero;
	}

	/**
	 * @param pasesAtencionTercero
	 *            the pasesAtencionTercero to set
	 */
	public void setPasesAtencionTercero(
			List<PaseAtencionSiniestroDTO> pasesAtencionTercero) {
		this.pasesAtencionTercero = pasesAtencionTercero;
	}

	/**
	 * @return the tipoCalculoCobertura
	 */
	public Long getTipoCalculoCobertura() {
		return tipoCalculoCobertura;
	}

	/**
	 * @param tipoCalculoCobertura
	 *            the tipoCalculoCobertura to set
	 */
	public void setTipoCalculoCobertura(Long tipoCalculoCobertura) {
		this.tipoCalculoCobertura = tipoCalculoCobertura;
	}

	/**
	 * @return the tipoEstimacion
	 */
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	/**
	 * @param tipoEstimacion
	 *            the tipoEstimacion to set
	 */
	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace
	 *            the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName
	 *            the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @param reporteCabinaId
	 *            the reporteCabinaId to set
	 */
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	/**
	 * @return the reporteCabinaId
	 */
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	/**
	 * @param aplicaDeducible
	 *            the aplicaDeducible to set
	 */
	public void setAplicaDeducible(Map<String, String> aplicaDeducible) {
		this.aplicaDeducible = aplicaDeducible;
	}

	/**
	 * @return the aplicaDeducible
	 */
	public Map<String, String> getAplicaDeducible() {
		return aplicaDeducible;
	}

	/**
	 * @param hospitalDiferente
	 *            the hospitalDiferente to set
	 */
	public void setHospitalDiferente(Map<String, String> hospitalDiferente) {
		this.hospitalDiferente = hospitalDiferente;
	}

	/**
	 * @return the hospitalDiferente
	 */
	public Map<String, String> getHospitalDiferente() {
		return hospitalDiferente;
	}

	public String getTipoCalculo() {
		return tipoCalculo;
	}

	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}

	/**
	 * @return the idResponsableReparacion
	 */
	public Integer getIdResponsableReparacion() {
		return idResponsableReparacion;
	}

	/**
	 * @param idResponsableReparacion
	 *            the idResponsableReparacion to set
	 */
	public void setIdResponsableReparacion(Integer idResponsableReparacion) {
		this.idResponsableReparacion = idResponsableReparacion;
	}

	/**
	 * @return the idHospital
	 */
	public Integer getIdHospital() {
		return idHospital;
	}

	/**
	 * @param idHospital
	 *            the idHospital to set
	 */
	public void setIdHospital(Integer idHospital) {
		this.idHospital = idHospital;
	}

	/**
	 * @return the idMedico
	 */
	public Integer getIdMedico() {
		return idMedico;
	}

	/**
	 * @param idMedico
	 *            the idMedico to set
	 */
	public void setIdMedico(Integer idMedico) {
		this.idMedico = idMedico;
	}

	/**
	 * @return the idTaller
	 */
	public Integer getIdTaller() {
		return idTaller;
	}

	/**
	 * @param idTaller
	 *            the idTaller to set
	 */
	public void setIdTaller(Integer idTaller) {
		this.idTaller = idTaller;
	}

	/**
	 * @return the idCompaniaSegurosTercero
	 */
	public Integer getIdCompaniaSegurosTercero() {
		return idCompaniaSegurosTercero;
	}

	/**
	 * @param idCompaniaSegurosTercero the idCompaniaSegurosTercero to set
	 */
	public void setIdCompaniaSegurosTercero(Integer idCompaniaSegurosTercero) {
		this.idCompaniaSegurosTercero = idCompaniaSegurosTercero;
	}

	/**
	 * @return the pasesAtencionInt
	 */
	public int getPasesAtencionInt() {
		return pasesAtencionInt;
	}

	/**
	 * @param pasesAtencionInt
	 *            the pasesAtencionInt to set
	 */
	public void setPasesAtencionInt(int pasesAtencionInt) {
		this.pasesAtencionInt = pasesAtencionInt;
	}

	public String getApDeducible() {
		return apDeducible;
	}

	public void setApDeducible(String apDeducible) {
		this.apDeducible = apDeducible;
	}

	public Long getIdPaseDeAtencion() {
		return idPaseDeAtencion;
	}

	public void setIdPaseDeAtencion(Long idPaseDeAtencion) {
		this.idPaseDeAtencion = idPaseDeAtencion;
	}		

	public Map<String, String> getCiaLlegadas() {
		return ciaLlegadas;
	}

	public void setCiaLlegadas(Map<String, String> ciaLlegadas) {
		this.ciaLlegadas = ciaLlegadas;
	}

	public Map<String, String> getTieneCiaSeguros() {
		return tieneCiaSeguros;
	}

	public void setTieneCiaSeguros(Map<String, String> tieneCiaSeguros) {
		this.tieneCiaSeguros = tieneCiaSeguros;
	}

	public Map<String, String> getColores() {
		return colores;
	}

	public void setColores(Map<String, String> colores) {
		this.colores = colores;
	}

	public String getTieneCiaSegurosValor() {
		return tieneCiaSegurosValor;
	}

	public void setTieneCiaSegurosValor(String tieneCiaSegurosValor) {
		this.tieneCiaSegurosValor = tieneCiaSegurosValor;
	}

	/**
	 * @return the otroHospital
	 */
	public Boolean getOtroHospital() {
		return otroHospital;
	}

	/**
	 * @param otroHospital the otroHospital to set
	 */
	public void setOtroHospital(Boolean otroHospital) {
		this.otroHospital = otroHospital;
	}
	
	public SiniestroCabinaDTO getSiniestroDTO() {
		return siniestroDTO;
	}

	public void setSiniestroDTO(SiniestroCabinaDTO siniestroDTO) {
		this.siniestroDTO = siniestroDTO;
	}

	/**
	 * @return the idEstatus
	 */
	public String getIdEstatus() {
		return idEstatus;
	}

	/**
	 * @param idEstatus the idEstatus to set
	 */
	public void setIdEstatus(String idEstatus) {
		this.idEstatus = idEstatus;
	}

	/**
	 * @return the estatusHabilitado
	 */
	public String getEstatusHabilitado() {
		return estatusHabilitado;
	}

	/**
	 * @param estatusHabilitado the estatusHabilitado to set
	 */
	public void setEstatusHabilitado(String estatusHabilitado) {
		this.estatusHabilitado = estatusHabilitado;
	}

	/**
	 * @return the validOnMillis
	 */
	public Long getValidOnMillis() {
		return validOnMillis;
	}

	/**
	 * @param validOnMillis the validOnMillis to set
	 */
	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	/**
	 * @return the recordFromMillis
	 */
	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	/**
	 * @param recordFromMillis the recordFromMillis to set
	 */
	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	/**
	 * @param isListadoPases the isListadoPases to set
	 */
	public void setIsListadoPases(String isListadoPases) {
		this.isListadoPases = isListadoPases;
	}

	/**
	 * @return the isListadoPases
	 */
	public String getIsListadoPases() {
		return isListadoPases;
	}



	public Boolean getModoConsultaDeListado() {
		return modoConsultaDeListado;
	}



	public void setModoConsultaDeListado(Boolean modoConsultaDeListado) {
		this.modoConsultaDeListado = modoConsultaDeListado;
	}

	public Boolean getTieneAntiguedadELNoSerie() {
		return tieneAntiguedadELNoSerie;
	}

	public void setTieneAntiguedadELNoSerie(Boolean tieneAntiguedadELNoSerie) {
		this.tieneAntiguedadELNoSerie = tieneAntiguedadELNoSerie;
	}

	public Map<String, String> getCtgNoAplicaDeducible() {
		return ctgNoAplicaDeducible;
	}

	public void setCtgNoAplicaDeducible(Map<String, String> ctgNoAplicaDeducible) {
		this.ctgNoAplicaDeducible = ctgNoAplicaDeducible;
	}

	public BigDecimal getPorcentajeDeducible() {
		return porcentajeDeducible;
	}

	public void setPorcentajeDeducible(BigDecimal porcentajeDeducible) {
		this.porcentajeDeducible = porcentajeDeducible;
	}

	public BigDecimal getPorcentajeDeducibleValorComercial() {
		return porcentajeDeducibleValorComercial;
	}

	public void setPorcentajeDeducibleValorComercial(
			BigDecimal porcentajeDeducibleValorComercial) {
		this.porcentajeDeducibleValorComercial = porcentajeDeducibleValorComercial;
	}

	public String getMotivoNoAplicaDeducibleSel() {
		return motivoNoAplicaDeducibleSel;
	}

	public void setMotivoNoAplicaDeducibleSel(String motivoNoAplicaDeducibleSel) {
		this.motivoNoAplicaDeducibleSel = motivoNoAplicaDeducibleSel;
	}
	public String getReponseDeCESVI() {
		return reponseDeCESVI;
	}
	public void setReponseDeCESVI(String reponseDeCESVI) {
		this.reponseDeCESVI = reponseDeCESVI;
	}
	public Boolean getPermiteCambiarDeducible() {
		return permiteCambiarDeducible;
	}

	public void setPermiteCambiarDeducible(Boolean permiteCambiarDeducible) {
		this.permiteCambiarDeducible = permiteCambiarDeducible;
	}

	public Map<String, String> getMotivosCorreoNoPropocionado() {
		return motivosCorreoNoPropocionado;
	}

	public void setMotivosCorreoNoPropocionado(
			Map<String, String> motivosCorreoNoPropocionado) {
		this.motivosCorreoNoPropocionado = motivosCorreoNoPropocionado;
	}

	public Long getIdParametroAntiguedad() {
		return idParametroAntiguedad;
	}

	public void setIdParametroAntiguedad(Long idParametroAntiguedad) {
		this.idParametroAntiguedad = idParametroAntiguedad;
	}

	public AutorizacionReservaService getAutorizacionReservaService() {
		return autorizacionReservaService;
	}

	public void setAutorizacionReservaService(
			AutorizacionReservaService autorizacionReservaService) {
		this.autorizacionReservaService = autorizacionReservaService;
	}

	public int getRequiereAutorizacion() {
		return requiereAutorizacion;
	}

	public void setRequiereAutorizacion(int requiereAutorizacion) {
		this.requiereAutorizacion = requiereAutorizacion;
	}

	public CatSolicitudAutorizacionService getCatSolicitudAutorizacionService() {
		return catSolicitudAutorizacionService;
	}

	public void setCatSolicitudAutorizacionService(
			CatSolicitudAutorizacionService catSolicitudAutorizacionService) {
		this.catSolicitudAutorizacionService = catSolicitudAutorizacionService;
	}
	
	public Boolean getPermiteCapturarNoSiniestroTercero() {
		return permiteCapturarNoSiniestroTercero;
	}

	public void setPermiteCapturarNoSiniestroTercero(
			Boolean permiteCapturarNoSiniestroTercero) {
		this.permiteCapturarNoSiniestroTercero = permiteCapturarNoSiniestroTercero;
	}

	public Boolean getPermiteCapturarCiaInfo() {
		return permiteCapturarCiaInfo;
	}

	public void setPermiteCapturarCiaInfo(Boolean permiteCapturarCiaInfo) {
		this.permiteCapturarCiaInfo = permiteCapturarCiaInfo;
	}

	public String getNumeroSiniestroEstimacion() {
		return numeroSiniestroEstimacion;
	}

	public void setNumeroSiniestroEstimacion(String numeroSiniestroEstimacion) {
		this.numeroSiniestroEstimacion = numeroSiniestroEstimacion;
	}

	public String getNumeroSiniestroAfectacion() {
		return numeroSiniestroAfectacion;
	}

	public void setNumeroSiniestroAfectacion(String numeroSiniestroAfectacion) {
		this.numeroSiniestroAfectacion = numeroSiniestroAfectacion;
	}
	
	public Map<String, String> getCtgTipoTransporte() {
		return ctgTipoTransporte;
	}

	public void setCtgTipoTransporte(Map<String, String> ctgTipoTransporte) {
		this.ctgTipoTransporte = ctgTipoTransporte;
	}	
	
	public Map<String, String> getTiposPersona() {
		return tiposPersona;
	}
	
	public void setTiposPersona(Map<String, String> tiposPersona) {
		this.tiposPersona = tiposPersona;
	}
	
	public Map<String, String> getCircunstancia() {
		return circunstancia;
	}
	
	public void setCircunstancia(Map<String, String> circunstancia) {
		this.circunstancia = circunstancia;
	}
	
	public BigDecimal getEstimacionNueva() {
		return estimacionNueva;
	}

	public void setEstimacionNueva(BigDecimal estimacionNueva) {
		this.estimacionNueva = estimacionNueva;
	}
}

