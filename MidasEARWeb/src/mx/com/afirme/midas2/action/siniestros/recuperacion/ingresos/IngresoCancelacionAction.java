package mx.com.afirme.midas2.action.siniestros.recuperacion.ingresos;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/ingresoCancelacion")

public class IngresoCancelacionAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean cancelarElIngresoAplicado;
	private String  comentarioCancelacionPorDevolucion;
	private int     aplicarTodosIngresos;
	private String  cuentaCancelacionPorDevolucion;
	private Long    ingresoId;
	private String  idIngresosConcatPorAplicar;
	private String  noCuentaAcredora;
	private String  ingresosPendientesConcat;
	private String  motivoCancelacionPorDevolucion;
	private String  motivoCancelacionPendientePorAplicar;
	private Map<String,String>  motivosDeCancelacion;
	private Boolean regresaAIngresoPendientePorAplicar;
	private int     guardarCancelacionPendientePorAplicar;
	private int     tipoCancelacionIngresoAplicado;
	private Boolean soloConsulta;
	private List<ListarIngresoDTO> listaIngresos;
	private ListarIngresoDTO filtroIngresosPendientes;
	private Map<String,String>  listaEstatus;
	private Map<String,String>  listaMedioRecuperacion;
	private Map<Long,String>  listaOficinas;
	private Map<String,String>  listaTipoRecuperacion;
	//private List  listadoRecuperacionDto;
	private int   subTipoCancelacion;
	private Integer tipoDeCancelacion = 0;
	private String tipoDeRecuperacion = "";

	private static final String mensajeError = "NO SE PUDO CANCELAR EL INGRESO SELECCIONADO";
	private static final String CONTENEDOR_MODAL_CANCELAR_INGRESO_PENDIENTE_APLICAR = "/jsp/siniestros/recuperacion/ingresos/cancelacion/cancelacionPendientePorAplicar.jsp";
	private static final String LOCATION_CONTENEDORLISTARINGRESO_JSP = "/jsp/siniestros/recuperacion/listadoIngresos.jsp";
	private static final String CONTENEDOR_CANCELACION_INGRESO_APLICADO_JSP = "/jsp/siniestros/recuperacion/ingresos/cancelacion/cancelacionIngresosAplicados.jsp";
	private static final String LOCATION_CONTENEDORINGRESOSPENDIENTESGRID_JSP = "/jsp/siniestros/recuperacion/ingresos/ingresosPendientesPorCancelarGrid.jsp";

	@Autowired
	@Qualifier("ingresoServiceEJB")
	private IngresoService ingresoService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

	public void prepareMostrarVentanaCancelacionPendientePorAplicar(){
		this.motivosDeCancelacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO);
	}

	@Action(value="mostrarVentanaCancelacionPendientePorAplicar", results={
			@Result(name=SUCCESS,location=CONTENEDOR_MODAL_CANCELAR_INGRESO_PENDIENTE_APLICAR),
			@Result(name = INPUT,location= LOCATION_CONTENEDORLISTARINGRESO_JSP)
	})
	public String mostrarVentanaCancelacionPendientePorAplicar(){

		try{ 
			if( guardarCancelacionPendientePorAplicar == 1 ){
				this.ingresoService.cancelarIngresoPendienteRecuperacionRegistrada(ingresoId,motivoCancelacionPendientePorAplicar);
				setMensajeExito();
			}
		}catch(Exception e){
			if (e instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)e).getMessageClean());
			}
			return INPUT;
		}

		return SUCCESS; 
	}

	/**
	 * Cargar el listado de tipos de cancelación
	 * Direcciona a la pantalla modal donde se selecciona el tipo de cancelación
	 */
	@Action(value="mostrarVentanaCancelacionIngresoAplicado", results={
			@Result(name=SUCCESS,location=CONTENEDOR_CANCELACION_INGRESO_APLICADO_JSP),
			@Result(name = INPUT,location= CONTENEDOR_CANCELACION_INGRESO_APLICADO_JSP)
	})
	public String mostrarVentanaCancelacionIngresoAplicado(){
		Ingreso ingreso = this.ingresoService.obtenerIngreso(this.ingresoId) ;
		this.tipoDeRecuperacion = ingreso.getRecuperacion().getTipo();
		return SUCCESS;
	}


	public void prepareCancelacionPorDevolucion()  {
		inicializaCatalogoListadoIngresos();
	}

	/**
	 * Invocar ingresoService.cancelacionPorDevolucion
	 * Si regresa null enviar el mensaje de exito.
	 * En caso contrario devolver el error y mostrar el mensaje devuelto por el
	 * servicio
	 * Regresa a la pantalla del listado de Ingresos
	 */
	@Action(value="cancelacionPorDevolucion", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDORLISTARINGRESO_JSP),
			@Result(name = INPUT,location= LOCATION_CONTENEDORLISTARINGRESO_JSP)
	})
	public String cancelacionPorDevolucion(){
		
		String respuesta = this.ingresoService.cancelacionAcuenta(ingresoId, motivoCancelacionPorDevolucion, noCuentaAcredora, comentarioCancelacionPorDevolucion, aplicarTodosIngresos,1,0 );
		
		try {
			if( StringUtil.isEmpty(respuesta) ) {
				super.setMensajeExito();
				return SUCCESS;
			}else {
				super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(respuesta));
				return INPUT;
			}
		
		}catch(Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		
	}


	public void prepareCancelarACuentaAcreedora(){
		this.inicializaCatalogoListadoIngresos();
	} 
	/**
	 * validar  tipoCancelacionACuentaAcreedora
	 * 
	 * si es igual a CANCELACION_A_CUENTA_ACREEDORA_REGRESAR
	 * invocar ingresoService.cancelacionAcuentaYRegresarAPendiente
	 * 
	 * si es igual a CANCELACION_A_CUENTA_ACREEDORA_CANCELAR
	 * invocar ingresoService.cancelacionAcuentaAcreedora
	 */
	@Action(value="cancelarACuentaAcreedora", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDORLISTARINGRESO_JSP),
			@Result(name = INPUT,location= LOCATION_CONTENEDORLISTARINGRESO_JSP)
	}) 
	public String cancelarACuentaAcreedora(){

		String respuesta = this.ingresoService.cancelacionAcuenta(ingresoId, motivoCancelacionPorDevolucion, noCuentaAcredora, comentarioCancelacionPorDevolucion, aplicarTodosIngresos,2,tipoCancelacionIngresoAplicado);
		
		try {
			if( StringUtil.isEmpty(respuesta) ) {
				super.setMensajeExito();
				return SUCCESS;
			}else {
				super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(respuesta));
				return INPUT;
			}
		
		}catch(Exception e) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	} 


	public void prepareCancelarPorReversaDeIngreso(){
		this.inicializaCatalogoListadoIngresos();
	} 
	/**
	 *si es igual a CANCELACION_TIPO_REGRESAR
	 *invocar ingresoService.cancelarReversaYRegresar
	 *si es igual a CANCELACION_TIPO_CANCELAR
	 *invocar ingresoService.cancelarReversaYCancel
	 */
	@Action(value="cancelarPorReversaDeIngreso", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDORLISTARINGRESO_JSP),
			@Result(name = INPUT,location= LOCATION_CONTENEDORLISTARINGRESO_JSP)
	}) 
	public String cancelarPorReversaDeIngreso(){

		List<String> mensajes = null;

		try{

			mensajes = this.ingresoService.cancelacionDeReversa(ingresoId, motivoCancelacionPorDevolucion, noCuentaAcredora, comentarioCancelacionPorDevolucion, idIngresosConcatPorAplicar,aplicarTodosIngresos,tipoCancelacionIngresoAplicado);
			if( !mensajes.isEmpty() ){
				super.setMensajeListaPersonalizado("Error al cancelar el ingreso", mensajes, TIPO_MENSAJE_ERROR);
				return INPUT;
			}
			super.setMensajeExito();
			return SUCCESS; 
		}catch(Exception e){
			super.setMensajeListaPersonalizado("Error al cancelar el ingreso", mensajes, TIPO_MENSAJE_ERROR);
			return INPUT;
		}


	} 


	@Action(value = "buscarIngresosPendientes", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORINGRESOSPENDIENTESGRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORINGRESOSPENDIENTESGRID_JSP)})
			public String buscarIngresosPendientes(){

		filtroIngresosPendientes.setEstatus(Ingreso.EstatusIngreso.PENDIENTE.getValue());
		listaIngresos = ingresoService.buscarIngresos(filtroIngresosPendientes, filtroIngresosPendientes.getIngresosConcat());
		return SUCCESS;
	} 


	private void inicializaCatalogoListadoIngresos(){
		listaEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_INGRESO);
		listaMedioRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MEDIO_RECUPERACION);
		listaOficinas = listadoService.obtenerOficinasSiniestros();
		listaTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECUPERACION);
	}

	public Boolean getCancelarElIngresoAplicado() {
		return cancelarElIngresoAplicado;
	}

	public void setCancelarElIngresoAplicado(Boolean cancelarElIngresoAplicado) {
		this.cancelarElIngresoAplicado = cancelarElIngresoAplicado;
	}

	public String getComentarioCancelacionPorDevolucion() {
		return comentarioCancelacionPorDevolucion;
	}

	public void setComentarioCancelacionPorDevolucion(
			String comentarioCancelacionPorDevolucion) {
		this.comentarioCancelacionPorDevolucion = comentarioCancelacionPorDevolucion;
	}

	public String getCuentaCancelacionPorDevolucion() {
		return cuentaCancelacionPorDevolucion;
	}

	public void setCuentaCancelacionPorDevolucion(
			String cuentaCancelacionPorDevolucion) {
		this.cuentaCancelacionPorDevolucion = cuentaCancelacionPorDevolucion;
	}

	public String getIngresosPendientesConcat() {
		return ingresosPendientesConcat;
	}

	public void setIngresosPendientesConcat(String ingresosPendientesConcat) {
		this.ingresosPendientesConcat = ingresosPendientesConcat;
	}

	public String getMotivoCancelacionPorDevolucion() {
		return motivoCancelacionPorDevolucion;
	}

	public void setMotivoCancelacionPorDevolucion(
			String motivoCancelacionPorDevolucion) {
		this.motivoCancelacionPorDevolucion = motivoCancelacionPorDevolucion;
	}

	public Map<String,String> getMotivosDeCancelacion() {
		return motivosDeCancelacion;
	}

	public void setMotivosDeCancelacion(Map<String,String> motivosDeCancelacion) {
		this.motivosDeCancelacion = motivosDeCancelacion;
	}

	public Boolean getRegresaAIngresoPendientePorAplicar() {
		return regresaAIngresoPendientePorAplicar;
	}

	public void setRegresaAIngresoPendientePorAplicar(
			Boolean regresaAIngresoPendientePorAplicar) {
		this.regresaAIngresoPendientePorAplicar = regresaAIngresoPendientePorAplicar;
	}

	public String getMotivoCancelacionPendientePorAplicar() {
		return motivoCancelacionPendientePorAplicar;
	}

	public void setMotivoCancelacionPendientePorAplicar(
			String motivoCancelacionPendientePorAplicar) {
		this.motivoCancelacionPendientePorAplicar = motivoCancelacionPendientePorAplicar;
	}

	public int getGuardarCancelacionPendientePorAplicar() {
		return guardarCancelacionPendientePorAplicar;
	}

	public void setGuardarCancelacionPendientePorAplicar(
			int guardarCancelacionPendientePorAplicar) {
		this.guardarCancelacionPendientePorAplicar = guardarCancelacionPendientePorAplicar;
	}

	public String getNoCuentaAcredora() {
		return noCuentaAcredora;
	}

	public void setNoCuentaAcredora(String noCuentaAcredora) {
		this.noCuentaAcredora = noCuentaAcredora;
	}

	public int getAplicarTodosIngresos() {
		return aplicarTodosIngresos;
	}

	public void setAplicarTodosIngresos(int aplicarTodosIngresos) {
		this.aplicarTodosIngresos = aplicarTodosIngresos;
	}

	public int getTipoCancelacionIngresoAplicado() {
		return tipoCancelacionIngresoAplicado;
	}

	public void setTipoCancelacionIngresoAplicado(int tipoCancelacionIngresoAplicado) {
		this.tipoCancelacionIngresoAplicado = tipoCancelacionIngresoAplicado;
	}

	public Long getIngresoId() {
		return ingresoId;
	}

	public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}

	public String getIdIngresosConcatPorAplicar() {
		return idIngresosConcatPorAplicar;
	}

	public void setIdIngresosConcatPorAplicar(String idIngresosConcatPorAplicar) {
		this.idIngresosConcatPorAplicar = idIngresosConcatPorAplicar;
	}

	public Boolean getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Boolean soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public List<ListarIngresoDTO> getListaIngresos() {
		return listaIngresos;
	}

	public void setListaIngresos(List<ListarIngresoDTO> listaIngresos) {
		this.listaIngresos = listaIngresos;
	}

	public ListarIngresoDTO getFiltroIngresosPendientes() {
		return filtroIngresosPendientes;
	}

	public void setFiltroIngresosPendientes(
			ListarIngresoDTO filtroIngresosPendientes) {
		this.filtroIngresosPendientes = filtroIngresosPendientes;
	}

	public IngresoService getIngresoService() {
		return ingresoService;
	}

	public void setIngresoService(IngresoService ingresoService) {
		this.ingresoService = ingresoService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/*public List getListadoRecuperacionDto() {
		return listadoRecuperacionDto;
	}

	public void setListadoRecuperacionDto(
			List listadoRecuperacionDto) {
		this.listadoRecuperacionDto = listadoRecuperacionDto;
	}*/

	public Map<String,String> getListaEstatus() {
		return listaEstatus;
	}

	public void setListaEstatus(Map<String,String> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	public Map<String,String> getListaMedioRecuperacion() {
		return listaMedioRecuperacion;
	}

	public void setListaMedioRecuperacion(Map<String,String> listaMedioRecuperacion) {
		this.listaMedioRecuperacion = listaMedioRecuperacion;
	}

	public Map<Long,String> getListaOficinas() {
		return listaOficinas;
	}

	public void setListaOficinas(Map<Long,String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

	public Map<String,String> getListaTipoRecuperacion() {
		return listaTipoRecuperacion;
	}

	public void setListaTipoRecuperacion(Map<String,String> listaTipoRecuperacion) {
		this.listaTipoRecuperacion = listaTipoRecuperacion;
	}

	public int getSubTipoCancelacion() {
		return subTipoCancelacion;
	}
	public void setSubTipoCancelacion(int subTipoCancelacion) {
		this.subTipoCancelacion = subTipoCancelacion;
	}

	public Integer getTipoDeCancelacion() {
		return tipoDeCancelacion;
	}

	public void setTipoDeCancelacion(Integer tipoDeCancelacion) {
		this.tipoDeCancelacion = tipoDeCancelacion;
	}

	public String getTipoDeRecuperacion() {
		return tipoDeRecuperacion;
	}

	public void setTipoDeRecuperacion(String tipoDeRecuperacion) {
		this.tipoDeRecuperacion = tipoDeRecuperacion;
	}

}
