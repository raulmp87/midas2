package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.opensymphony.xwork2.Preparable;

public class RecuperacionAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 3030500654103121988L;
	
	protected static final String PAIS_DEFAULT = "PAMEXI"; 

	protected Map<String, String> lstMedioRecuperacion = new LinkedHashMap<String, String>();;

	protected Map<String, String> lstTipoOC;

	protected Map<String, String> lstCoberturas;

	protected Map<String, String> lstEstatus;
	
	protected Map<Long,String> lstPases;

	protected Long recuperacionId;

	protected String motivo;

	protected Boolean soloConsulta = false;
	
	protected Boolean editarDatosGenericos;

	protected Boolean esNuevoRegistro = false;	
	
	protected String tipoRecuperacionDesc;
	
	protected String medioRecuperaDesc;
	
	protected String estatusRecuperacionDesc;
	
	protected String noSiniestro;
	
	protected String noReporteSiniestro;	
	
	
	protected String pantallaSeleccion;// Se utiliza para la utilizacion de tab, para nombrar sobre que pestaña  (JSP) se esta trabajando
	
	protected String coberturasSeleccionadas;
	
	protected String pasesSeleccionadas;
	
	protected boolean esAccionCancelar = false;
	
	protected String mensajePantalla;//Se utiliza para enviar mensajes en pantalla mediante Json 
	
	protected static final String separator = ",";
	
	protected String tieneVentaActiva;
	
	protected Boolean esEdicionVentaRecuperacion;

	/**
	 * Esta atributo puede recibir tres valores:
	 * 
	 * R:Consultar
	 * U:Editar
	 * C:Cancelar
	 */
	protected String tipoMostrar;	
	protected String TIPO_CONSULTAR="R";
	protected String TIPO_EDITAR="U";
	protected String TIPO_CANCELAR="C";
	
	
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	protected CatalogoGrupoValorService catalogoGrupoValorService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	protected ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")	
	protected EntidadService entidadService;
	
	@Autowired
	@Qualifier("recuperacionServiceEJB")
	protected RecuperacionService recuperacionService;
	
	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	protected ParametroGlobalService parametroGlobalService ; 

	protected void setPropiedadesGenerales(Recuperacion recuperacion){
		if (recuperacion != null && recuperacion.getId() != null){			
			//set coberturas
			this.lstCoberturas=this.recuperacionService.obtenerCoberturasRecuperacion(recuperacionId);
			
			
			//set pases de atencion
			this.lstPases=this.recuperacionService.obtenerPasesRecuperacion(recuperacionId);
			
			//set estatus
			this.estatusRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, recuperacion.getEstatus());
						
			//set tipo recuperacion
			if (null!=recuperacion.getTipo() ){
				this.tipoRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION,recuperacion.getTipo() );
			}
			
			//setear medio
			lstMedioRecuperacion=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION, recuperacion.getMedio());
			
			//setear orden compra			
			lstTipoOC=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.TIPO_ORDENCOMPRA, recuperacion.getTipoOrdenCompra());
			
			if(noReporteSiniestro == null){
				noReporteSiniestro = recuperacion.getReporteCabina().getNumeroReporte();
			}
			if(noSiniestro == null && recuperacion.getReporteCabina().getSiniestroCabina() != null){
				noSiniestro = recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();
			}	
			
			if(null==editarDatosGenericos){
				editarDatosGenericos=false;
			}
			StringBuilder stringBuilder = new StringBuilder();
			Iterator<String> iteratorCoberturas = lstCoberturas.keySet().iterator();
		    while (iteratorCoberturas.hasNext()) {		 
		    	stringBuilder.append(iteratorCoberturas.next());
		        if (iteratorCoberturas.hasNext()) {
		        	stringBuilder.append(separator);
		        }
		    }
		    coberturasSeleccionadas=stringBuilder.toString();
		    stringBuilder = new StringBuilder();
		    Iterator<Long> iteratorPases = lstPases.keySet().iterator();
		    while (iteratorPases.hasNext()) {		 
		    	stringBuilder.append(iteratorPases.next());
		        if (iteratorPases.hasNext()) {
		        	stringBuilder.append(separator);
		        }
		    }
		    pasesSeleccionadas = stringBuilder.toString();
		
		}
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}

	public void setLstMedioRecuperacion(Map<String, String> lstMedioRecuperacion) {
		this.lstMedioRecuperacion = lstMedioRecuperacion;
	}

	public Map<String, String> getLstMedioRecuperacion() {
		return lstMedioRecuperacion;
	}

	public Map<String, String> getLstTipoOC() {
		return lstTipoOC;
	}

	public void setLstTipoOC(Map<String, String> lstTipoOC) {
		this.lstTipoOC = lstTipoOC;
	}

	public Map<String, String> getLstCoberturas() {
		return lstCoberturas;
	}

	public void setLstCoberturas(Map<String, String> lstCoberturas) {
		this.lstCoberturas = lstCoberturas;
	}

	public Map<String, String> getLstEstatus() {
		return lstEstatus;
	}

	public void setLstEstatus(Map<String, String> lstEstatus) {
		this.lstEstatus = lstEstatus;
	}

	public Long getRecuperacionId() {
		return recuperacionId;
	}

	public void setRecuperacionId(Long recuperacionId) {
		this.recuperacionId = recuperacionId;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Boolean getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Boolean soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Boolean getEsNuevoRegistro() {
		return esNuevoRegistro;
	}

	public void setEsNuevoRegistro(Boolean esNuevoRegistro) {
		this.esNuevoRegistro = esNuevoRegistro;
	}

	public String getTipoRecuperacionDesc() {
		return tipoRecuperacionDesc;
	}

	public void setTipoRecuperacionDesc(String tipoRecuperacionDesc) {
		this.tipoRecuperacionDesc = tipoRecuperacionDesc;
	}

	public String getMedioRecuperaDesc() {
		return medioRecuperaDesc;
	}

	public void setMedioRecuperaDesc(String medioRecuperaDesc) {
		this.medioRecuperaDesc = medioRecuperaDesc;
	}

	public String getEstatusRecuperacionDesc() {
		return estatusRecuperacionDesc;
	}

	public void setEstatusRecuperacionDesc(String estatusRecuperacionDesc) {
		this.estatusRecuperacionDesc = estatusRecuperacionDesc;
	}

	public String getNoSiniestro() {
		return noSiniestro;
	}

	public void setNoSiniestro(String noSiniestro) {
		this.noSiniestro = noSiniestro;
	}

	public String getNoReporteSiniestro() {
		return noReporteSiniestro;
	}

	public void setNoReporteSiniestro(String noReporteSiniestro) {
		this.noReporteSiniestro = noReporteSiniestro;
	}

	public Boolean getEsAccionCancelar() {
		return esAccionCancelar;
	}

	public void setEsAccionCancelar(Boolean esAccionCancelar) {
		this.esAccionCancelar = esAccionCancelar;
	}

	public Map<Long, String> getLstPases() {
		return lstPases;
	}

	public void setLstPases(Map<Long, String> lstPases) {
		this.lstPases = lstPases;
	}

	public String getPantallaSeleccion() {
		return pantallaSeleccion;
	}

	public void setPantallaSeleccion(String pantallaSeleccion) {
		this.pantallaSeleccion = pantallaSeleccion;
	}

	public String getCoberturasSeleccionadas() {
		return coberturasSeleccionadas;
	}

	public void setCoberturasSeleccionadas(String coberturasSeleccionadas) {
		this.coberturasSeleccionadas = coberturasSeleccionadas;
	}

	public String getPasesSeleccionadas() {
		return pasesSeleccionadas;
	}

	public void setPasesSeleccionadas(String pasesSeleccionadas) {
		this.pasesSeleccionadas = pasesSeleccionadas;
	}

	public String getMensajePantalla() {
		return mensajePantalla;
	}

	public void setMensajePantalla(String mensajePantalla) {
		this.mensajePantalla = mensajePantalla;
	}

	public String getTipoMostrar() {
		return tipoMostrar;
	}

	public void setTipoMostrar(String tipoMostrar) {
		this.tipoMostrar = tipoMostrar;
	}

	public CatalogoGrupoValorService getCatalogoGrupoValorService() {
		return catalogoGrupoValorService;
	}

	public void setCatalogoGrupoValorService(
			CatalogoGrupoValorService catalogoGrupoValorService) {
		this.catalogoGrupoValorService = catalogoGrupoValorService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public RecuperacionService getRecuperacionService() {
		return recuperacionService;
	}

	public void setRecuperacionService(RecuperacionService recuperacionService) {
		this.recuperacionService = recuperacionService;
	}

	public void setEsAccionCancelar(boolean esAccionCancelar) {
		this.esAccionCancelar = esAccionCancelar;
	}

	public Boolean getEditarDatosGenericos() {
		return editarDatosGenericos;
	}

	public void setEditarDatosGenericos(Boolean editarDatosGenericos) {
		this.editarDatosGenericos = editarDatosGenericos;
	}

	public String getTieneVentaActiva() {
		return tieneVentaActiva;
	}

	public void setTieneVentaActiva(String tieneVentaActiva) {
		this.tieneVentaActiva = tieneVentaActiva;
	}

	public Boolean getEsEdicionVentaRecuperacion() {
		return esEdicionVentaRecuperacion;
	}

	public void setEsEdicionVentaRecuperacion(Boolean esEdicionVentaRecuperacion) {
		this.esEdicionVentaRecuperacion = esEdicionVentaRecuperacion;
	}
	

}
