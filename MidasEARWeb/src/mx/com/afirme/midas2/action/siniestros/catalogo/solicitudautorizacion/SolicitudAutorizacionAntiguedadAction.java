package mx.com.afirme.midas2.action.siniestros.catalogo.solicitudautorizacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionAjusteAntiguedad;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogos.solicitudautorizacion.SolicitudAutorizacionAntiguedadDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/autorizacion/solicitudAutorizacionAntiguedad")
public class SolicitudAutorizacionAntiguedadAction extends BaseAction implements Preparable {

	private static final String	LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDADGRID_JSP	= "/jsp/siniestros/catalogo/solicitudautorizacion/solAutAntiguedadGrid.jsp";
	private static final String	LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDAD_JSP	= "/jsp/siniestros/catalogo/solicitudautorizacion/solAutAntiguedad.jsp";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int esBusqueda;
	private SolicitudAutorizacionAntiguedadDTO filtroSolicitud;
	private Map<String,String> lstCondicion;
	private Map<String,String> lstEstatus;
	private Map<Long,String> lstOficina;
	private Map<String,String> lstTipoAjuste;
	private Map<String,String> lstTipoSiniestro;
	private Map<String,String> lstCoberturas;
	private List<SolicitudAutorizacionAjusteAntiguedad>  solicitudes;
	private String lstAutorizar;
	private String lstRechazar;
	
	private TransporteImpresionDTO transporte;
	
	@Autowired
	@Qualifier("catSolicitudAutorizacionServiceEJB")
	private CatSolicitudAutorizacionService catSolicitudAutorizacionService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("autorizacionReservaServiceEJB")
	private AutorizacionReservaService autorizacionReservaService;
	
	
	
	
	public void prepare(){
		this.lstOficina = this.listadoService.obtenerOficinasSiniestros();
		this.lstCoberturas = this.listadoService.obtenerCoberturaPorSeccionConDescripcion(null);
		this.lstTipoAjuste = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AJUSTE);
		this.lstTipoSiniestro = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSA_SINIESTRO);
		this.lstCondicion = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CRITERIO_COMPARACION);
		this.lstEstatus = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_SOLICITUD_ANTIGUEDAD);
	}

	@Action(value = "buscarSolicitudes", results = {
			@Result(name = SUCCESS, location = LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDADGRID_JSP),
			@Result(name = INPUT, location = LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDAD_JSP) })
	public String buscarSolicitudes(){
		if(filtroSolicitud!=null){
			this.solicitudes = this.catSolicitudAutorizacionService.buscarAutorizacionesAntiguedad(filtroSolicitud);
		}else{
			this.solicitudes = new ArrayList<SolicitudAutorizacionAjusteAntiguedad>();
		}
		this.esBusqueda = 1;
		return SUCCESS;
	}

	public void prepareCambiarEstatus(){
		this.filtroSolicitud = new SolicitudAutorizacionAntiguedadDTO();
	}
	
	@Action(value = "cambiarEstatus", results = {
			@Result(name = SUCCESS, location = LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDADGRID_JSP),
			@Result(name = INPUT, location = LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDAD_JSP) })
	public String cambiarEstatus(){
		this.catSolicitudAutorizacionService.autorizarSiniestroAntiguo(this.lstAutorizar);
		this.catSolicitudAutorizacionService.rechazarSiniestroAntiguo(this.lstRechazar);
		this.solicitudes = this.catSolicitudAutorizacionService.buscarAutorizacionesAntiguedad(filtroSolicitud);
		return SUCCESS;
	}

	
	@Action(value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDAD_JSP),
			@Result(name = INPUT, location = LOCATION_SOLICITUDAUTORIZACIONSOLAUTANTIGUEDAD_JSP) })
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	
	@Action(value="exportarResultados",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})

	public String exportarAExcel(){
		this.solicitudes = this.catSolicitudAutorizacionService.buscarAutorizacionesAntiguedad(filtroSolicitud);
		
		ExcelExporter exporter = new ExcelExporter(SolicitudAutorizacionAjusteAntiguedad.class);
		transporte = exporter.exportXLS(solicitudes, "Listado de Solicitudes");
		
		return SUCCESS;
	}

	
	public int getEsBusqueda() {
		return esBusqueda;
	}

	public void setEsBusqueda(int esBusqueda) {
		this.esBusqueda = esBusqueda;
	}

	

	public SolicitudAutorizacionAntiguedadDTO getFiltroSolicitud() {
		return filtroSolicitud;
	}

	public void setFiltroSolicitud(
			SolicitudAutorizacionAntiguedadDTO filtroSolicitud) {
		this.filtroSolicitud = filtroSolicitud;
	}

	public Map<String, String> getLstCoberturas() {
		return lstCoberturas;
	}

	public void setLstCoberturas(Map<String, String> lstCoberturas) {
		this.lstCoberturas = lstCoberturas;
	}

	public Map<String, String> getLstCondicion() {
		return lstCondicion;
	}

	public void setLstCondicion(Map<String, String> lstCondicion) {
		this.lstCondicion = lstCondicion;
	}

	public Map<String, String> getLstEstatus() {
		return lstEstatus;
	}

	public void setLstEstatus(Map<String, String> lstEstatus) {
		this.lstEstatus = lstEstatus;
	}

	public Map<Long, String> getLstOficina() {
		return lstOficina;
	}

	public void setLstOficina(Map<Long, String> lstOficina) {
		this.lstOficina = lstOficina;
	}

	public String getLstRechazar() {
		return lstRechazar;
	}

	public void setLstRechazar(String lstRechazar) {
		this.lstRechazar = lstRechazar;
	}

	public String getLstAutorizar() {
		return lstAutorizar;
	}

	public void setLstAutorizar(String lstAutorizar) {
		this.lstAutorizar = lstAutorizar;
	}

	public Map<String, String> getLstTipoAjuste() {
		return lstTipoAjuste;
	}

	public void setLstTipoAjuste(Map<String, String> lstTipoAjuste) {
		this.lstTipoAjuste = lstTipoAjuste;
	}

	public Map<String, String> getLstTipoSiniestro() {
		return lstTipoSiniestro;
	}

	public void setLstTipoSiniestro(Map<String, String> lstTipoSiniestro) {
		this.lstTipoSiniestro = lstTipoSiniestro;
	}

	public List<SolicitudAutorizacionAjusteAntiguedad> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(
			List<SolicitudAutorizacionAjusteAntiguedad> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public CatSolicitudAutorizacionService getCatSolicitudAutorizacionService() {
		return catSolicitudAutorizacionService;
	}

	public void setCatSolicitudAutorizacionService(
			CatSolicitudAutorizacionService catSolicitudAutorizacionService) {
		this.catSolicitudAutorizacionService = catSolicitudAutorizacionService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public AutorizacionReservaService getAutorizacionReservaService() {
		return autorizacionReservaService;
	}

	public void setAutorizacionReservaService(
			AutorizacionReservaService autorizacionReservaService) {
		this.autorizacionReservaService = autorizacionReservaService;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	
	
	
	
}
