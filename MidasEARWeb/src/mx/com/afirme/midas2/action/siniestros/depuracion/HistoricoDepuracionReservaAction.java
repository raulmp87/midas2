package mx.com.afirme.midas2.action.siniestros.depuracion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReservaDetalle;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.depuracion.DepuracionReservaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author simavera 
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/depuracion/reserva/historico")
public class HistoricoDepuracionReservaAction extends BaseAction implements Preparable{
	
	private static final String	LOCATION_RESERVASDEPURADAS_JSP	= "/jsp/siniestros/depuracion/reservasDepuradas.jsp";
	private static final String	LOCATION_RESERVASDEPURAR_JSP	= "/jsp/siniestros/depuracion/reservasDepurar.jsp";
	private static final String DOCUMENTO_DEPURADA = "DEPURADA";
	private static final String DOCUMENTO_DEPURAR = "DEPURAR";
	

	private static final long serialVersionUID = -7192526168841536262L;
	private List<DepuracionReserva> depuracionesList;
	private Long depuracionReservaId;
	private List<DepuracionReservaDetalle> reservasList;
	private String idsDepuracionDetalle;
	private Long idConfigDepuracionReserva;
	private Map<String,String> listEstatus;
	private TransporteImpresionDTO transporte;
	private List<Map<String, Object>> reservasDepurar; 
	private DepuracionReserva depuracionReserva;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("depuracionReservaServiceEJB")
	private DepuracionReservaService depuracionReservaService;

	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	public HistoricoDepuracionReservaAction(){

	}
	
	@Override
	public void prepare(){
		this.listEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_DEPURACION);
	}

	/**
	 * Ejecuta la depuracion de Reservas
	 */
	@Action (value = "ejecutarDepuracion", results = { 
			@Result(name = SUCCESS, location = LOCATION_RESERVASDEPURADAS_JSP),
			@Result(name = INPUT, location = LOCATION_RESERVASDEPURAR_JSP)})
	public String ejecutarDepuracion(){
		try{			 
			 if (idsDepuracionDetalle != null && !idsDepuracionDetalle.equals("")) {
				 depuracionReserva = depuracionReservaService.ejecutarDepuracionSP(idsDepuracionDetalle);
				 idConfigDepuracionReserva = depuracionReserva.getConfiguracion().getId();
				 depuracionReservaId = depuracionReserva.getId();
				 super.setMensajeExito();
				 return SUCCESS;
			 } else {
				 super.setMensaje("Se debe seleccionar al menos un registro a depurar");
				 return INPUT;
			 }
			
			
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}

	/**
	 * muestra la Pantalla de Configuracion de Depuraciones
	 */
	@Action (value = "mostrarGenerarReservasADepurar", results = { 
			@Result(name = SUCCESS, location = LOCATION_RESERVASDEPURAR_JSP),
			@Result(name = INPUT, location = LOCATION_RESERVASDEPURAR_JSP)})
	public String mostrarGenerarReservasADepurar(){
		try{
			depuracionReserva = depuracionReservaService.generarDepuracion(idConfigDepuracionReserva, true);
			this.depuracionReservaId = depuracionReserva.getId(); 
			return SUCCESS;
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}

	/**
	 * muestra la pantalla de Historico de depuraciones
	 */
	@Action(value = "mostrarHistorico", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/historicoDepuracion.jsp") })
	public String mostrarHistorico(){
		return SUCCESS;
	}
	
	/**
	 * Muestra la pantalla de Histórico de Depuración de Reserva
	 */
	@Action(value = "mostrarListadoHistorico", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/historicoDepuracionGrid.jsp") })
	public String mostrarListadoHistorico(){
		this.depuracionesList = depuracionReservaService.obtenerHistoricoConfiguracion( this.idConfigDepuracionReserva );
		return SUCCESS;
	}

	/**
	 * Este metdo se invoca desde el Historico de depuraciones
	 * Invocar al metodo generarReservas de DepuracionReservaService
	 */
	@Action(value = "mostrarReservasParaDepurar", results = { @Result(name = SUCCESS, location = LOCATION_RESERVASDEPURAR_JSP) })
	public String mostrarReservasADepurar(){
		return SUCCESS;
	}
	
	/**
	 * Este metdo se invoca desde el Historico de depuraciones
	 * Invocar al metodo generarReservas de DepuracionReservaService
	 */
	@Action(value = "mostrarListadoReservasParaDepurar", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/reservasDepurarGrid.jsp") })
	public String mostrarListadoReservasADepurar(){
		reservasDepurar = new ArrayList<Map<String, Object>>();
		Map<String, Object> map;
		DepuracionReserva depuracion = new DepuracionReserva();
		depuracion.setId(depuracionReservaId);
		reservasList = depuracionReservaService.generarReservasSP(idConfigDepuracionReserva, depuracion, true);
		//reservasList = new ArrayList<DepuracionReservaDetalle>();
		for(DepuracionReservaDetalle reserva: reservasList){
			map = new LinkedHashMap<String, Object>();
			map.put("id", reserva.getId());
			map.put("estatus", reserva.getEstatus());
			map.put("numero", reserva.getNumero());
			map.put("numeroSiniestro", reserva.getNumeroSiniestro());
			map.put("poliza", reserva.getNumeroPolizaFormateada());
			map.put("numeroInciso", reserva.getNumeroInciso());
			map.put("folioPase", reserva.getFolio());
			map.put("nomCobertura", reserva.getNombreCobertura());
			map.put("nombreAsegurado", reserva.getNombreAsegurado() );
			map.put("terminoAjuste", reserva.getTerminoAjusteDesc());
			map.put("nombreConfiguracion", reserva.getNombreConfiguracion());
			map.put("montoReserva", reserva.getReservaActual());
			map.put("montoReservaDepurar", reserva.getReservaPorDepurar());
			map.put("porcentaje",  reserva.getDepuratedPercent());
			reservasDepurar.add(map);
		}
		return SUCCESS;
	}
	
	/**
	 * muestra la pantalla de reservas depuradas
	 */
	 @Action(value = "mostrarReservasDepuradas", results = { @Result(name = SUCCESS, location = LOCATION_RESERVASDEPURADAS_JSP) })
	public String mostrarReservasDepuradas(){
		depuracionReserva = depuracionReservaService.obtenerDepuracion(depuracionReservaId);
		idConfigDepuracionReserva = depuracionReserva.getConfiguracion().getId();
		return SUCCESS;
	}
	 
    /**
	 * muestra el listado de reservas depuradas
	 */
	 @Action(value = "mostrarListadoReservasDepuradas", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/reservasDepuradasGrid.jsp") })
	public String mostrarListadoReservasDepuradas(){
		  if(this.idsDepuracionDetalle.isEmpty()){
			 this.reservasList = depuracionReservaService.getListDepuratedReservsByIdDepuracionReserva(depuracionReservaId, null);
		  }else{
			  this.reservasList = depuracionReservaService.obtenerReservasDepuradasById( this.idsDepuracionDetalle );
		  }
		 
		  
		 return SUCCESS;
	}
	 
	 @Action(value="exportarResultadosReservaDepurada",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarAExcelReservaDepurada(){	
		this.reservasList = depuracionReservaService.exportarSoloDepuradosToExcel(this.idsDepuracionDetalle);
		ExcelExporter exporter = new ExcelExporter( DepuracionReservaDetalle.class, DOCUMENTO_DEPURADA);
		transporte = exporter.exportXLS(reservasList, "Reservas_Depuradas: " + sdf.format(new Date()));
	
		return SUCCESS;
		
	}

	 @Action(value="exportarResultadosReservaDepurar",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarAExcelReservaDepurar(){
		
		this.reservasList = depuracionReservaService.obtenerReservasExportacionSP(depuracionReservaId);
		ExcelExporter exporter = new ExcelExporter( DepuracionReservaDetalle.class, DOCUMENTO_DEPURAR );
		transporte = exporter.exportXLS(reservasList, "Reservas a Depurar: " + sdf.format(new Date()));
		
		return SUCCESS;
		
	}
	
	/**
	 * @return the depuracionesList
	 */
	public List<DepuracionReserva> getDepuracionesList() {
		return depuracionesList;
	}

	/**
	 * @param depuracionesList the depuracionesList to set
	 */
	public void setDepuracionesList(List<DepuracionReserva> depuracionesList) {
		this.depuracionesList = depuracionesList;
	}

	/**
	 * @return the depuracionReservaId
	 */
	public Long getDepuracionReservaId() {
		return depuracionReservaId;
	}

	/**
	 * @param depuracionReservaId the depuracionReservaId to set
	 */
	public void setDepuracionReservaId(Long depuracionReservaId) {
		this.depuracionReservaId = depuracionReservaId;
	}

	/**
	 * @return the reservasList
	 */
	public List<DepuracionReservaDetalle> getReservasList() {
		return reservasList;
	}

	/**
	 * @param reservasList the reservasList to set
	 */
	public void setReservasList(List<DepuracionReservaDetalle> reservasList) {
		this.reservasList = reservasList;
	}

	/**
	 * @return the idsDepuracionDetalle
	 */
	public String getIdsDepuracionDetalle() {
		return idsDepuracionDetalle;
	}

	/**
	 * @param idsDepuracionDetalle the idsDepuracionDetalle to set
	 */
	public void setIdsDepuracionDetalle(String idsDepuracionDetalle) {
		this.idsDepuracionDetalle = idsDepuracionDetalle;
	}

	/**
	 * @return the idConfigDepuracionReserva
	 */
	public Long getIdConfigDepuracionReserva() {
		return idConfigDepuracionReserva;
	}

	/**
	 * @param idConfiguracion the idConfiguracion to set
	 */
	public void setIdConfigDepuracionReserva(Long idConfigDepuracionReserva) {
		this.idConfigDepuracionReserva = idConfigDepuracionReserva;
	}

	/**
	 * @return the listEstatus
	 */
	public Map<String, String> getListEstatus() {
		return listEstatus;
	}

	/**
	 * @param listEstatus the listEstatus to set
	 */
	public void setListEstatus(Map<String, String> listEstatus) {
		this.listEstatus = listEstatus;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}

	/**
	 * @param listadoService the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the depuracionReservaService
	 */
	public DepuracionReservaService getDepuracionReservaService() {
		return depuracionReservaService;
	}

	/**
	 * @param depuracionReservaService the depuracionReservaService to set
	 */
	public void setDepuracionReservaService(
			DepuracionReservaService depuracionReservaService) {
		this.depuracionReservaService = depuracionReservaService;
	}

	public List<Map<String, Object>> getReservasDepurar() {
		return reservasDepurar;
	}

	public void setReservasDepurar(List<Map<String, Object>> reservasDepurar) {
		this.reservasDepurar = reservasDepurar;
	}

	public DepuracionReserva getDepuracionReserva() {
		return depuracionReserva;
	}

	public void setDepuracionReserva(DepuracionReserva depuracionReserva) {
		this.depuracionReserva = depuracionReserva;
	}
	
	

}
