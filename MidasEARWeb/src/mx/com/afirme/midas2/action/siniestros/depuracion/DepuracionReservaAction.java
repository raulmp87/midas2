package mx.com.afirme.midas2.action.siniestros.depuracion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.DepuracionReservaDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.SeccionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService;
import mx.com.afirme.midas2.service.siniestros.depuracion.DepuracionReservaService;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/depuracion/reserva")
public class DepuracionReservaAction extends BaseAction implements Preparable  {

	private static final String	LOCATION_CONFIGURACIONDEPURACION_JSP	= "/jsp/siniestros/depuracion/configuracionDepuracion.jsp";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1761674786244094929L;
	
	private static final Logger log = Logger.getLogger(DepuracionReservaAction.class);
	
	private String accionCoberturas;
	private String accionTerminos;
	private static final String ASOCIAR_COBERTURA = "AC";
	private static final String ASOCIAR_SECCION = "AS";
//	private static final String ASOCIAR_TERMINO = "AT";
	private static final String ASOCIAR_TODAS_COBERTURAS = "AST";
//	private static final String ASOCIAR_TODOS_TERMINOS = "ATT";
	private static final String ELIMINAR_COBERTURA = "EC";
	private static final String ELIMINAR_SECCION = "ES";
//	private static final String ELIMINAR_TERMINO = "ET";
	private static final String ELIMINAR_TODAS_COBERTURAS = "ETC";
//	private static final String ELIMINAR_TODOS_TERMINOS = "ETT";
	private static final String ESTATUS_PROCESO_PENDIENTE = "2";
	private static final String ESTATUS_PROCESO_ACTIVA = "1";
	private static final String ESTATUS_PROCESO_INACTIVA = "0";
	private List<SeccionSiniestroDTO> coberturasAsociadas = new ArrayList<SeccionSiniestroDTO>();
	private List<SeccionSiniestroDTO> coberturasDisponibles = new ArrayList<SeccionSiniestroDTO>();
	private List<CoberturaSeccionSiniestroDTO> coberturasList = new ArrayList<CoberturaSeccionSiniestroDTO>();
	private List<ConfiguracionDepuracionReserva> configuraciones;
	private Map<String,String> condicionRangoList;
	private Map<String,String> criterioAntiguedadList;
	private Map<String,String> estatusList;
	private Map<Long,String> negocioList;
	private Map<Long,String> oficinaList;
	private Map<String,String> terminoAjusteAsociados;
	private Map<String,String> terminoAjusteDisponibles;
	private Map<String,String> tipoAtencionList;
	private Map<String,String> tipoPerdidaList;
	private Map<String,String> tipoProgramacionLst;
	
	private static final String programacionPorDia = "PD";
	private static final String programacionPorFecha = "PF";
	private static final String programacionDosDiasAntes = "DD";
	
	private Map<String,String> respuestaSimple;
	private int soloConsulta;
	private int vista;
	private DepuracionReservaDTO filtroDepuracion;
	private Long idConfiguracion;
	private String terminoAjuste;
	private BigDecimal idToSeccion;
	private BigDecimal idToCobertura;
	private String estatusProceso;
	private String claveSubCalculo;
	private String aplicaPago;
	private String aplicaPresupuesto;
	private String estatusAnterior;
	private Boolean noEditable;
	private List<Map<String, Object>> configuracionesMap;
	private Map<String, String> tipoServicio;
	private String cobertura;
	
	private ConfiguracionDepuracionReserva configuracion;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	private NegocioService negocioService;
	
	@Autowired
	@Qualifier("depuracionReservaServiceEJB")
	private DepuracionReservaService depuracionReservaService;
	
	@Autowired
	@Qualifier("catalogoSiniestroOficinaServiceEJB")
	private CatalogoSiniestroOficinaService oficinaService;

	@Override
	public void prepare(){
		if(vista == 0){
			coberturasList = depuracionReservaService.obtenerCoberturasPorSeccion();
		}else{
			tipoProgramacionLst = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PROGRAMACION);
			condicionRangoList = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CRITERIO_COMPARACION);
			criterioAntiguedadList = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CRITERIO_ANTIGUEDAD);
			tipoAtencionList = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_DE_ATENCION);
			tipoPerdidaList = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PERDIDA);
			respuestaSimple = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RESPUESTA_SIMPLE);
		}
		estatusList = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_PROCESO);
		oficinaList = listadoService.obtenerOficinasSiniestros();
		tipoServicio = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_SERVICIO);
		negocioList = new LinkedHashMap<Long, String>();
		Negocio negocio = new Negocio();
		negocio.setClaveEstatus((short)1);
		List<Negocio> negocios = negocioService.findByFilters(negocio);
		for(Negocio neg : negocios){
			negocioList.put(neg.getIdToNegocio(), neg.getDescripcionNegocio());		
		}
	}
	
	@Action(value = "mostrarBusquedaConfiguracion", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/busquedaConfigDepuracion.jsp"))
	public String mostrarBusquedaConfiguracion(){
		return SUCCESS;
	}
	
	@Action(value = "mostrarConfiguracion", results = @Result(name = SUCCESS, location = LOCATION_CONFIGURACIONDEPURACION_JSP))
	public String mostrarConfiguracion(){
		if(configuracion != null && configuracion.getId() != null){
			configuracion = depuracionReservaService.cargarConfiguracionDepuracion(configuracion.getId());
			configuracion=setPropiedades(false, configuracion);
			if(configuracion.getAplicaPresupuesto() !=  null && configuracion.getAplicaPresupuesto()){
				if(configuracion.getAplicaPresupuesto()){
					aplicaPresupuesto = "S";
				}else{
					aplicaPresupuesto = "N";
				}
			}
			if(configuracion.getEstatus() != null){
				if(configuracion.getEstatus() == 2){
					estatusProceso = ESTATUS_PROCESO_PENDIENTE;
					noEditable = false;
				}else if(configuracion.getEstatus() == 0){
					estatusProceso = ESTATUS_PROCESO_INACTIVA;
					noEditable = true;
					estatusList.remove("2");
				}else{
					estatusProceso = ESTATUS_PROCESO_ACTIVA;
					noEditable = true;
					estatusList.remove("2");
				}
			}
			estatusAnterior = estatusProceso;
		}else{
			estatusList.remove("0");
			estatusList.remove("1");
			estatusProceso = ESTATUS_PROCESO_PENDIENTE;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarDepuracion", results = {
			@Result(name = SUCCESS, location = LOCATION_CONFIGURACIONDEPURACION_JSP),
			@Result(name = INPUT, location = LOCATION_CONFIGURACIONDEPURACION_JSP)})
	public String guardarDepuracion(){
		try{
			Long oficinaId = (configuracion.getOficina()!=null)?configuracion.getOficina().getId():null;
			String mensajeErrorOficina = this.oficinaService.validaOficinaConTipoDeServicio(oficinaId,configuracion.getTipoServicio());
			if(mensajeErrorOficina==null){
				
				if(ESTATUS_PROCESO_PENDIENTE.equals(estatusProceso)){
					configuracion.setEstatus(2);
					if("S".equalsIgnoreCase(aplicaPresupuesto)){
						configuracion.setAplicaPresupuesto(true);
					}else if("N".equalsIgnoreCase(aplicaPresupuesto)){
						configuracion.setAplicaPresupuesto(false);
					}
				}else if(ESTATUS_PROCESO_INACTIVA.equals(estatusProceso)){
					if(noEditable){
						configuracion = depuracionReservaService.cargarConfiguracionDepuracion(configuracion.getId());
					}
					configuracion.setEstatus(0);
					configuracion.setFechaInactivo(new Date());
					noEditable = true;
					estatusList.remove("2");
				}else{
					if(noEditable){
						configuracion = depuracionReservaService.cargarConfiguracionDepuracion(configuracion.getId());
					}
					configuracion.setEstatus(1);
					configuracion.setFechaActivo(new Date());
					noEditable = true;
					estatusList.remove("2");
				}
				configuracion=setPropiedades(true, configuracion);
				depuracionReservaService.guardarConfiguracionDepuracion(configuracion);
				configuracion=setPropiedades(false, configuracion);
				estatusAnterior = estatusProceso;
				super.setMensajeExito();
				return SUCCESS;
			}else{
				super.setMensajeError(mensajeErrorOficina);
				return INPUT;
			}
		}catch (Exception ex){
			log.error(ex);
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "siniestroDTO");
			}	
			return INPUT;

		}
	}
	
	private ConfiguracionDepuracionReserva setPropiedades(boolean deVistaABD, ConfiguracionDepuracionReserva configuracion){
		//setear combo pagos si traen valores desde la pagina
		if(deVistaABD){
		if(configuracion.getConfigPagos()!=null){
			if("S".equalsIgnoreCase(aplicaPago)){
				configuracion.setAplicaPago(true);
			}else if("N".equalsIgnoreCase(aplicaPago)){
				configuracion.setAplicaPago(false);
			}
		}else{
			configuracion.setAplicaPago(null);
		}
		}
		else{
		//setear combo pagos si traen valores desde la bd
		if(configuracion.getAplicaPago() != null){
			if(configuracion.getAplicaPago()){
				aplicaPago = "S";
			}else{
				aplicaPago = "N";
			}
		}
		}
		return configuracion;
	}
	
	@Action
	(value = "asociarCobertura", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/coberturasDepuracionAsociadas.jsp") })
	public String asociarCobertura(){
		if (ASOCIAR_SECCION.equalsIgnoreCase(accionCoberturas)) {
			depuracionReservaService.asociarSeccion(configuracion.getId(), idToSeccion);
		}else if(ASOCIAR_TODAS_COBERTURAS.equalsIgnoreCase(accionCoberturas)){
			depuracionReservaService.asociarTodasCoberturas(configuracion.getId());
		}else if(ASOCIAR_COBERTURA.equalsIgnoreCase(accionCoberturas)){
			depuracionReservaService.asociarCobertura(configuracion.getId(), idToSeccion, idToCobertura, claveSubCalculo);
		}	
		coberturasAsociadas = depuracionReservaService.obtenerCoberturasAsociadas(configuracion.getId());
		
		return SUCCESS;
	}
	
	@Action
	(value = "eliminarCobertura", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/coberturasDepuracionDisponibles.jsp") })
	public String eliminarCobertura(){
			
		if (ELIMINAR_SECCION.equalsIgnoreCase(accionCoberturas)) {
			depuracionReservaService.eliminarSeccion(configuracion.getId(), idToSeccion);
		}else if (ELIMINAR_TODAS_COBERTURAS.equalsIgnoreCase(accionCoberturas)){
			depuracionReservaService.eliminarTodasCoberturas(configuracion.getId());
		}else if (ELIMINAR_COBERTURA.equalsIgnoreCase(accionCoberturas)){
			depuracionReservaService.eliminarCobertura(configuracion.getId(), idToCobertura);
		}		
		
		coberturasDisponibles = depuracionReservaService.obtenerCoberturasDisponibles(configuracion.getId(), idToSeccion);
		
		return SUCCESS;
	}
	
	@Action
	(value = "asociarTodasCoberturas", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONFIGURACIONDEPURACION_JSP) })
	public String asociarTodasCoberturas(){
		depuracionReservaService.asociarTodasCoberturas(configuracion.getId());
		return SUCCESS;
	}
	
	@Action
	(value = "eliminarTodasCoberturas", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONFIGURACIONDEPURACION_JSP) })
	public String eliminarTodasCoberturas(){
		depuracionReservaService.eliminarTodasCoberturas(configuracion.getId());
		return SUCCESS;
	}
	
	@Action(value = "obtenerCoberturasAsociadas", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/coberturasDepuracionAsociadas.jsp"))
	public String obtenerCoberturasAsociadas(){
		coberturasAsociadas = depuracionReservaService.obtenerCoberturasAsociadas(configuracion.getId());
		return SUCCESS;
	}
	
	@Action(value = "obtenerCoberturasDisponibles", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/coberturasDepuracionDisponibles.jsp"))
	public String obtenerCoberturasDisponibles(){
		coberturasDisponibles = depuracionReservaService.obtenerCoberturasDisponibles(configuracion.getId(), idToSeccion);
		return SUCCESS;
	}
	
	@Action
	(value = "asociarTodosTerminos", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONFIGURACIONDEPURACION_JSP) })
	public String asociarTodosTerminos(){
		depuracionReservaService.asociarTodosTerminos(configuracion.getId());
		terminoAjusteAsociados = depuracionReservaService.obtenerTerminosAsociados(configuracion.getId());
		return SUCCESS;
	}
	
	@Action
	(value = "eliminarTodosTermino", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONFIGURACIONDEPURACION_JSP) })
	public String eliminarTodosTermino(){
		depuracionReservaService.eliminarTodosTerminos(configuracion.getId());
		terminoAjusteDisponibles = depuracionReservaService.obtenerTerminosDisponibles(configuracion.getId());
		return SUCCESS;
	}
	
	@Action
	(value = "relacionarTerminos" )
	public void relacionarTerminos(){
		try{
			String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
			if (accion.equals(ROW_INSERTED)) {
				depuracionReservaService.asociarTermino(configuracion.getId(), terminoAjuste);
			}else if (accion.equals(ROW_DELETED)) {
				depuracionReservaService.eliminarTermino(configuracion.getId(), terminoAjuste);
			}
		}catch(Exception e){
			log.equals("Error al relacioarTerminos "+e);
			super.setMensajeError(getText("midas.negocio.error"));
		}
		
	}
	
	@Action(value = "obtenerTerminosAsociados", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/terminosDepuracionAsociados.jsp"))
	public String obtenerTerminosAsociados(){
		terminoAjusteAsociados = depuracionReservaService.obtenerTerminosAsociados(configuracion.getId());
		return SUCCESS;
	}
	
	@Action(value = "obtenerTerminosDisponibles", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/terminosDepuracionDisponibles.jsp"))
	public String obtenerTerminosDisponibles(){
		terminoAjusteDisponibles = depuracionReservaService.obtenerTerminosDisponibles(configuracion.getId());
		return SUCCESS;
	}
	
	@Action(value = "buscarConfiguraciones", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/busquedaConfigDepuracionGrid.jsp"))
	public String buscarConfiguraciones(){
		String servicio = "";
		configuracionesMap = new ArrayList<Map<String,Object>>();
		if(this.filtroDepuracion!=null){
			configuraciones = depuracionReservaService.buscarConfiguraciones(filtroDepuracion);
		}else{
			configuraciones = new ArrayList<ConfiguracionDepuracionReserva>();
		}
		Map<String, Object> map;
		for(ConfiguracionDepuracionReserva conf:configuraciones){
			map = new LinkedHashMap<String, Object>();
			map.put("id", conf.getId());
			map.put("nombre", conf.getNombre());
			map.put("estatus", estatusList.get(String.valueOf(conf.getEstatus())));
			if(conf.getTipoServicio()!=null){
				servicio = tipoServicio.get(conf.getTipoServicio());
			}
			map.put("servicio", servicio);
			map.put("oficina", conf.getOficina() != null ? oficinaList.get(conf.getOficina().getId()) : null);
			map.put("fechaActivo", conf.getFechaActivo());
			map.put("fechaInactivo", conf.getFechaInactivo());
			configuracionesMap.add(map);
		}
		return SUCCESS;
	}
	
	@Action(value = "generaDepuracion", results = @Result(name = SUCCESS, location = "/jsp/siniestros/depuracion/busquedaConfigDepuracion.jsp"))
	public String generaDepuracion(){
		depuracionReservaService.generarInformacionDepuracion();
		return SUCCESS;
	}

	public String getAccionCoberturas() {
		return accionCoberturas;
	}

	public void setAccionCoberturas(String accionCoberturas) {
		this.accionCoberturas = accionCoberturas;
	}

	public String getAccionTerminos() {
		return accionTerminos;
	}

	public void setAccionTerminos(String accionTerminos) {
		this.accionTerminos = accionTerminos;
	}

	public List<SeccionSiniestroDTO> getCoberturasAsociadas() {
		return coberturasAsociadas;
	}

	public void setCoberturasAsociadas(List<SeccionSiniestroDTO> coberturasAsociadas) {
		this.coberturasAsociadas = coberturasAsociadas;
	}

	public List<SeccionSiniestroDTO> getCoberturasDisponibles() {
		return coberturasDisponibles;
	}

	public void setCoberturasDisponibles(
			List<SeccionSiniestroDTO> coberturasDisponibles) {
		this.coberturasDisponibles = coberturasDisponibles;
	}

	public List<CoberturaSeccionSiniestroDTO> getCoberturasList() {
		return coberturasList;
	}

	public void setCoberturasList(List<CoberturaSeccionSiniestroDTO> coberturasList) {
		this.coberturasList = coberturasList;
	}

	public List<ConfiguracionDepuracionReserva> getConfiguraciones() {
		return configuraciones;
	}

	public void setConfiguraciones(
			List<ConfiguracionDepuracionReserva> configuraciones) {
		this.configuraciones = configuraciones;
	}

	public Map<String, String> getCondicionRangoList() {
		return condicionRangoList;
	}

	public void setCondicionRangoList(Map<String, String> condicionRangoList) {
		this.condicionRangoList = condicionRangoList;
	}

	public Map<String, String> getCriterioAntiguedadList() {
		return criterioAntiguedadList;
	}

	public void setCriterioAntiguedadList(Map<String, String> criterioAntiguedadList) {
		this.criterioAntiguedadList = criterioAntiguedadList;
	}

	public Map<String, String> getEstatusList() {
		return estatusList;
	}

	public void setEstatusList(Map<String, String> estatusList) {
		this.estatusList = estatusList;
	}

	public Map<Long, String> getNegocioList() {
		return negocioList;
	}

	public void setNegocioList(Map<Long, String> negocioList) {
		this.negocioList = negocioList;
	}

	public Map<Long, String> getOficinaList() {
		return oficinaList;
	}

	public void setOficinaList(Map<Long, String> oficinaList) {
		this.oficinaList = oficinaList;
	}

	public Map<String, String> getTerminoAjusteAsociados() {
		return terminoAjusteAsociados;
	}

	public void setTerminoAjusteAsociados(Map<String, String> terminoAjusteAsociados) {
		this.terminoAjusteAsociados = terminoAjusteAsociados;
	}

	public Map<String, String> getTerminoAjusteDisponibles() {
		return terminoAjusteDisponibles;
	}

	public void setTerminoAjusteDisponibles(
			Map<String, String> terminoAjusteDisponibles) {
		this.terminoAjusteDisponibles = terminoAjusteDisponibles;
	}

	public Map<String, String> getTipoAtencionList() {
		return tipoAtencionList;
	}

	public void setTipoAtencionList(Map<String, String> tipoAtencionList) {
		this.tipoAtencionList = tipoAtencionList;
	}

	public Map<String, String> getTipoPerdidaList() {
		return tipoPerdidaList;
	}

	public void setTipoPerdidaList(Map<String, String> tipoPerdidaList) {
		this.tipoPerdidaList = tipoPerdidaList;
	}

	public Map<String, String> getTipoProgramacionLst() {
		return tipoProgramacionLst;
	}

	public void setTipoProgramacionLst(Map<String, String> tipoProgramacionLst) {
		this.tipoProgramacionLst = tipoProgramacionLst;
	}

	public int getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(int soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public int getVista() {
		return vista;
	}

	public void setVista(int vista) {
		this.vista = vista;
	}

	public DepuracionReservaDTO getFiltroDepuracion() {
		return filtroDepuracion;
	}

	public void setFiltroDepuracion(DepuracionReservaDTO filtroDepuracion) {
		this.filtroDepuracion = filtroDepuracion;
	}

	public Long getIdConfiguracion() {
		return idConfiguracion;
	}

	public void setIdConfiguracion(Long idConfiguracion) {
		this.idConfiguracion = idConfiguracion;
	}

	public ConfiguracionDepuracionReserva getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfiguracionDepuracionReserva configuracion) {
		this.configuracion = configuracion;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public String getEstatusProceso() {
		return estatusProceso;
	}

	public void setEstatusProceso(String estatusProceso) {
		this.estatusProceso = estatusProceso;
	}

	public BigDecimal getIdToCobertura() {
		return idToCobertura;
	}

	public void setIdToCobertura(BigDecimal idToCobertura) {
		this.idToCobertura = idToCobertura;
	}

	public String getClaveSubCalculo() {
		return claveSubCalculo;
	}

	public void setClaveSubCalculo(String claveSubCalculo) {
		this.claveSubCalculo = claveSubCalculo;
	}

	public Map<String, String> getRespuestaSimple() {
		return respuestaSimple;
	}

	public void setRespuestaSimple(Map<String, String> respuestaSimple) {
		this.respuestaSimple = respuestaSimple;
	}

	public String getAplicaPago() {
		return aplicaPago;
	}

	public void setAplicaPago(String aplicaPago) {
		this.aplicaPago = aplicaPago;
	}

	public String getAplicaPresupuesto() {
		return aplicaPresupuesto;
	}

	public void setAplicaPresupuesto(String aplicaPresupuesto) {
		this.aplicaPresupuesto = aplicaPresupuesto;
	}

	public String getEstatusAnterior() {
		return estatusAnterior;
	}

	public void setEstatusAnterior(String estatusAnterior) {
		this.estatusAnterior = estatusAnterior;
	}

	public Boolean getNoEditable() {
		return noEditable;
	}

	public void setNoEditable(Boolean noEditable) {
		this.noEditable = noEditable;
	}

	public List<Map<String, Object>> getConfiguracionesMap() {
		return configuracionesMap;
	}

	public void setConfiguracionesMap(List<Map<String, Object>> configuracionesMap) {
		this.configuracionesMap = configuracionesMap;
	}

	public String getCobertura() {
		return cobertura;
	}

	public void setCobertura(String cobertura) {
		this.cobertura = cobertura;
	}

	public Map<String, String> getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(Map<String, String> tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public CatalogoSiniestroOficinaService getOficinaService() {
		return oficinaService;
	}

	public void setOficinaService(CatalogoSiniestroOficinaService oficinaService) {
		this.oficinaService = oficinaService;
	}

	public String getProgramacionPorDia() {
		return programacionPorDia;
	}

	public String getProgramacionPorFecha() {
		return programacionPorFecha;
	}

	public String getProgramacionDosDiasAntes() {
		return programacionDosDiasAntes;
	}
	
}
