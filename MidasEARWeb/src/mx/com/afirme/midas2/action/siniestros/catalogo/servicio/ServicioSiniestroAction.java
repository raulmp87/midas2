package mx.com.afirme.midas2.action.siniestros.catalogo.servicio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogos.ServicioSiniestrosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService.ServicioSiniestroFiltro;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;



@Component
@Scope("prototype")
@Namespace("/siniestros/catalogo/servicio/servicioSiniestro")
public class ServicioSiniestroAction extends BaseAction implements Preparable {
	
	private    static final String	LOCATION_LISTADOSERVICIOSINIESTROGRID_JSP	    = "/jsp/siniestros/catalogo/servicio/listadoServicioSiniestroGrid.jsp";
	private    static final String	LOCATION_CONTENEDORALTASERVICIOSINIESTRO_JSP	= "/jsp/siniestros/catalogo/servicio/contenedorAltaServicioSiniestro.jsp";
	private    static final String	LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP	    = "/jsp/siniestros/catalogo/servicio/contenedorServicioSiniestro.jsp";
	private    static final int     CANTIDAD_ENCABEZADOS = 5;
	private    static final int     CANTIDAD_CAMPOS	  = 9;
	private    static final	String  CONTENEDOR_ASIGNAR = "contenedorAsignar";
	
	// # INDICARA 2-LEGALES 1-AJUSTADORES
	private    short     tipoServicioOperacion;     			      						
	// # INDICA 0-BUSQUEDA 1-ALTA 2-MODIFICACION
	private    short     tipoOperacion;
	// # INDICA LA LEYENDA DE TEXTO POR SERVICIO
	private    String[]  leyendaServicioCabeza = new String[CANTIDAD_ENCABEZADOS];
	// # INDICA LOS TEXTOS QUE VAN EN LA PARTE LATERAL IZQUIERDA DE LOS INPUT EN LA VISTA
	private    String[]  leyendaServicioCampo  = new String[CANTIDAD_CAMPOS];
	private	   Map<String,String>   valoresEstatus 	   = null;
	private	   Map<String,String>   valoresEstatusAsignacion 	   = null;
	private	   Map<String,String>   valoresTipoDisponibilidad 	   = null;
	private	   Map<String,String>   valoresTipoServicio   = null;
	private	   Map<Long  ,String>   valoresOficinas       = null;
	private    boolean   readOnly              = false;
	private    Long      idServicioSiniestro;
	// # INDICA SI EL COMBO ESTARA ACTIVO PARA AJUSTADORES
	private    boolean   codigoServicio        = false;
	// # INDICA SI ACTIVA EL CAMPO NO. PRESTADOR DE SERVICIO
	private    boolean   prestadorServicio     = true;
	private    Long      idPersonaMidas;
	// # 1-ALTA 0-MODIFICAR
	private    short     accion                = 0;
	
	private List<ServicioSiniestro> 	serviciosSiniestrosGrid;
	private TransporteImpresionDTO      transporteExcel;
	private ServicioSiniestrosDTO   	servicioSiniestroDto;
	private ServicioSiniestroFiltro 	servicioSiniestroFiltro = new ServicioSiniestroFiltro();
	private boolean modoAsignar;
	private Map<String,String>   estados   = null;
	private Map<String,String>   municipios   = null;
	private Long reporteCabinaId;
	
	private Map<String, String> tiposPersona;
	private String tipoPersona; 

	// # SERVICES
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("servicioSiniestroServiceEJB")
	private ServicioSiniestroService servicioSiniestroService;
	
	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	private ParametroGlobalService parametroGlobalService;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void prepare(){
		
		// # CARGA EL CATALOGO GENERAL DEPENDIENDO EL TIPO DE SERVICIO
		this.cargaCatalogoGeneral();
		// # MUESTRA LA LEYENDA QUE INDICARA QUE TIPO DE SERVICIO SE MUESTRA
		this.generaLeyendasPrincipalesContenedor();
		// # MUESTRA LA LEYENDA DEL LADO IZQUIERDO DE LOS INPUT
		this.generaLeyedasInputContenedor(); 
		// # ACTIVA COMBO PARA AJUSTADORES
		codigoServicio    = this.tipoServicioOperacion == 1;
		// # ACTIVA CAMPO PARA ALTA DE NO. PRESTADOR
		prestadorServicio = this.tipoServicioOperacion == 2;
		this.estados = this.listadoService.getMapEstadosPorPaisMidas(null);
		this.municipios = new HashMap<String, String>();
		
		this.tiposPersona = listadoService
		.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PERSONA);
	}
	

	/**
	 * Mostrar pantalla principal
	 */
	@Action(value="mostrar", results={
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP),
			@Result(name = INPUT, location= LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP)
	})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	/**
	 * Mostrar pantalla principal
	 */
	@Action(value="mostrarContenedorDeAsignacion", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/servicio/contenedorBusquedaAsignacionServicioSiniestro.jsp")
	})
	public String mostrarContenedorParaAsignar(){
		return SUCCESS;
	}
	
	
	public void prepareVerDetalle(){
		// # OBTENER DATOS
		servicioSiniestroDto =  this.servicioSiniestroService.obtenerDatosSiniestro(this.idServicioSiniestro, this.idPersonaMidas,this.tipoOperacion);
		
		// # MARCAR COMO SOLO LECTURA
		this.readOnly = true;
		
	}
	
	@Action(value="verDetalle", results={
			@Result(name = SUCCESS, location= LOCATION_CONTENEDORALTASERVICIOSINIESTRO_JSP),
			@Result(name = INPUT, location= LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP)
	})
	public String verDetalle(){
		
		return SUCCESS;
	}
	
	
	/**
	 * Mostrar pantalla principal
	 */
	@Action(value="altaServicio", results={
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORALTASERVICIOSINIESTRO_JSP),
			@Result(name = INPUT, location= LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP)
	})
	public String altaServicio(){
		
		return SUCCESS;
	}

	
	/**
	 * Realiza la alta de un servicio especial
	 */
	@Action(
			value="salvarServicio",
			results = {
					@Result(name= SUCCESS, location = LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP ),
					@Result(name = INPUT, location= LOCATION_CONTENEDORALTASERVICIOSINIESTRO_JSP)
			}
	)
	public String salvarServicio(){
		
		try{
			if ( this.validarPaises(servicioSiniestroDto) ){
				
				List<String> mensajes = this.validateInput();
				if(!mensajes.isEmpty()){
					super.setMensajeListaPersonalizado("Campos Requeridos", mensajes, MENSAJE_ERROR_VALDACION);
					return INPUT;
				}
				this.servicioSiniestroService.salvarServicioSiniestro(servicioSiniestroDto, this.tipoServicioOperacion);
				super.setMensaje(this.getText("midas.negocio.exito"));
				
			}else{
				super.setMensaje(this.getText("midas.servicio.siniestros.error.domicilio"));
				return INPUT;
			}
			
		}catch (Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError("ERROR");
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "servicioSiniestroDto");
			}	
			return INPUT;

		}
		return SUCCESS;
		
	}
	
	private List<String> validateInput(){
		List<String> mensajes = new ArrayList<String>();
		if(this.servicioSiniestroDto.getTipoPersona().equals("PF")){
			this.validateInputPersonaFisica(mensajes);
		}else{
			this.validateInputPersonaMoral(mensajes);
		}
		return mensajes;
	}
	
	private void validateInputPersonaFisica(List<String> mensajes){
		validNotNullField(mensajes, "Nombre(s)", this.servicioSiniestroDto.getNombrePersona());
		validNotNullField(mensajes, "Apellido Paterno", this.servicioSiniestroDto.getApellidoPaterno());
		validNotNullField(mensajes, "Apellido Materno", this.servicioSiniestroDto.getApellidoMaterno());
	}
	
	private void validateInputPersonaMoral(List<String> mensajes){
		validNotNullField(mensajes, "Nombre de la Empresa", this.servicioSiniestroDto.getNombreDeLaEmpresa());
		validNotNullField(mensajes, "Administrador", this.servicioSiniestroDto.getAdministrador());
	}
	
	private void validNotNullField(List<String> mensajes, String fieldName, Object obj){
		if(obj==null){
			mensajes.add(fieldName);
		}else{
			if(obj instanceof String && ((String)obj).equals("")){
				mensajes.add(fieldName);
			}
		}
	}
	
	@Action(
			value="actualizarServicio",
			results = {
					@Result(name= SUCCESS, location = LOCATION_CONTENEDORSERVICIOSINIESTRO_JSP ),
					@Result(name = INPUT, location= LOCATION_CONTENEDORALTASERVICIOSINIESTRO_JSP)
			}
	)
	public String actualizarServicio(){
		
		try{
			
			if ( this.validarPaises(servicioSiniestroDto) ){
				this.servicioSiniestroService.actualizaServicioSiniestro(servicioSiniestroDto, this.tipoServicioOperacion);
				super.setMensaje(this.getText("midas.negocio.exito"));
			
			}else{
				super.setMensaje(this.getText("midas.servicio.siniestros.error.domicilio"));
				return INPUT;
			}
		}catch (Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError("ERROR");
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "servicioSiniestroDto");
			}	
			return INPUT;
		}
		return SUCCESS;
		
	}	
	
	
	public void prepareListarCarga(){
		servicioSiniestroFiltro.setTipoServicio(this.tipoServicioOperacion);
		serviciosSiniestrosGrid = servicioSiniestroService.buscarServicioSiniestro(servicioSiniestroFiltro);		
	}
	
	/**
	 * Método para listar las cargas masivas
	 */
	@Action(value="listarCarga", results={
			@Result(name = SUCCESS, location = LOCATION_LISTADOSERVICIOSINIESTROGRID_JSP),
			@Result(name = INPUT, location = LOCATION_LISTADOSERVICIOSINIESTROGRID_JSP)
	})
	public String listarCarga(){
		return SUCCESS;
	}
	
	public void prepareListarCargaAsignacion(){
		servicioSiniestroFiltro.setTipoServicio(this.tipoServicioOperacion);
		serviciosSiniestrosGrid = servicioSiniestroService.buscarServicioSiniestro(servicioSiniestroFiltro);
		//serviciosSiniestrosGrid = this.entidadService.findByProperty(ServicioSiniestro.class,"servicioSiniestroId",this.tipoServicioOperacion );
		
		// # ORDENAR REGISTROS GRID
		//this.ordenaRegistrosGrid();
	}
	
	
	/**
	 * Método para listar las cargas masivas
	 */
	@Action(value="listarCargaAsignacion", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/servicio/listadoAsignacionServicioSiniestroGrid.jsp")
	})
	public String listarCargaAsignacion(){
		return SUCCESS;
	}
	
	/***
	 * Busca datos del servicio
	 * @return
	 */
	@Action(value="busquedaGeneral", results={
			@Result(name=SUCCESS, location = LOCATION_LISTADOSERVICIOSINIESTROGRID_JSP),
			@Result(name=INPUT, location = LOCATION_LISTADOSERVICIOSINIESTROGRID_JSP),
			@Result(name=CONTENEDOR_ASIGNAR, location = "/jsp/siniestros/catalogo/servicio/listadoAsignacionServicioSiniestroGrid.jsp")
	})
	public String busquedaGeneral(){
		// # SETEA EL TIPO DE BUSQUEDA AJUSTADOR(1) - ABOGADO(2)
		servicioSiniestroFiltro.setTipoServicio(tipoServicioOperacion);
		servicioSiniestroFiltro.setNombrePrestador(StringUtil.decodeUri(servicioSiniestroFiltro.getNombrePrestador()));
		servicioSiniestroFiltro.setNombreAbogadoAjustador(StringUtil.decodeUri(servicioSiniestroFiltro.getNombreAbogadoAjustador()));
		if(modoAsignar){
			ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, reporteCabinaId);
			servicioSiniestroFiltro.setEstatus(1);
			servicioSiniestroFiltro.setOficinaId(reporteCabina.getOficina().getId());
			this.serviciosSiniestrosGrid = this.servicioSiniestroService.buscarAjustadoresDisponibles(servicioSiniestroFiltro);
			return CONTENEDOR_ASIGNAR;
		}else{
			this.serviciosSiniestrosGrid = this.servicioSiniestroService.buscarServicioSiniestro( servicioSiniestroFiltro );
			this.ordenaRegistrosGrid();
			return SUCCESS;
		}
	}	
	
	
	private void ordenaRegistrosGrid(){
		
		if(serviciosSiniestrosGrid != null && !serviciosSiniestrosGrid.isEmpty()){
			Collections.sort(serviciosSiniestrosGrid, 
					new Comparator<ServicioSiniestro>() {				
						public int compare(ServicioSiniestro n1, ServicioSiniestro n2){
							return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
						}
					});
		}
	}
	
	
	/***
	 * Exportar excel
	 * @return
	 */
	@Action(value="exportarExcel",
			results={@Result(
					name=SUCCESS,
					type="stream",
					params={
							"contentType","${transporteExcel.contentType}",
							"inputName","transporteExcel.genericInputStream",
							"contentDisposition",
							"attachment;filename=\"${transporteExcel.fileName}\""
						}
				)
			}
	)
	public String exportarExcel(){
		
		servicioSiniestroFiltro.setTipoServicio(tipoServicioOperacion);
		servicioSiniestroFiltro.setNombrePrestador(StringUtil.decodeUri(servicioSiniestroFiltro.getNombrePrestador()));
		servicioSiniestroFiltro.setNombreAbogadoAjustador(StringUtil.decodeUri(servicioSiniestroFiltro.getNombreAbogadoAjustador()));
		this.serviciosSiniestrosGrid = this.servicioSiniestroService.buscarServicioSiniestro( servicioSiniestroFiltro );
	
		ExcelExporter exporter = new ExcelExporter(ServicioSiniestro.class, Integer.valueOf(tipoServicioOperacion) == 1 ? "AJUSTADOR" : "ABOGADO");
		this.transporteExcel = exporter.exportXLS(serviciosSiniestrosGrid, "Listado_"+((this.tipoServicioOperacion == 1) ? "Ajustadores":"Abogados"));
		
		return SUCCESS;
	}
	
	private void generaLeyedasInputContenedor(){
		
		String postFijo = "";
		switch (this.getTipoServicioOperacion()){
		case 1:
			postFijo = " Ajustador";
		break;
		case 2:
			postFijo = " Abogado ";
		break;
		}

		int i = 0;
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.no")+postFijo;
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.noprestador");
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.tipo") + " de " + postFijo;
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.noprestador");
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.nombre")+postFijo;
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.oficina");
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.estatus");
		leyendaServicioCampo[i++] = getText("midas.servicio.siniestros.certificacion");
		leyendaServicioCampo[i]   = getText("midas.servicio.siniestros.nombreprestador");
		
	}
	
	
	/***
	 * Carga en la entidad el catalogo a mostrar dependiendo del servicio
	 */
	private void cargaCatalogoGeneral(){
		
		valoresTipoServicio = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.TIPO_SOLICITUD ); 
		valoresEstatus      = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.ESTATUS );
		valoresOficinas     = this.listadoService.obtenerOficinasSiniestros();
		if(modoAsignar){
			valoresEstatusAsignacion  = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.ESTATUS_ASIGNACION_AJUSTADOR );
			valoresTipoDisponibilidad = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.DISPONIBILIDAD_ASIGNACION_AJUSTADOR );
		}
		
	}
	
	/***
	 * Almacena el arreglo las 2 leyendas que se muestran arriba de cada tabla dependiendo el tipo de servicio
	 * @return
	 */
	private void generaLeyendasPrincipalesContenedor(){
		int i = 0;		
		int maxNumReportes = Integer.parseInt(parametroGlobalService.obtenerValorPorIdParametro(
				ParametroGlobalService.AP_MIDAS, ParametroGlobalService.MAX_REPORTES_AJUSTADOR));		
		switch (this.getTipoServicioOperacion()){
			case 1:
				leyendaServicioCabeza[i++] = getText("midas.servicio.siniestros.busqueda.ajustadores").concat( 
				(modoAsignar ? " ".concat(getText("midas.servicio.siniestros.busqueda.maxreportes", new String[]{String.valueOf(maxNumReportes)})) : ""));
				leyendaServicioCabeza[i++] = getText("midas.servicio.siniestros.listado.ajustadores");
				leyendaServicioCabeza[i]   = getText("midas.servicio.siniestros.datos.ajustadores");
				
			break;
			case 2:
				leyendaServicioCabeza[i++] = getText("midas.servicio.siniestros.busqueda.legales");
				leyendaServicioCabeza[i++] = getText("midas.servicio.siniestros.listado.legales");
				leyendaServicioCabeza[i]   = getText("midas.servicio.siniestros.datos.legales");
				
			break;	
		}
		
	}
	
	
	private boolean validarPaises(ServicioSiniestrosDTO servicioSiniestroDto){
		boolean valid = true;
		if(!SystemCommonUtils.isValid(servicioSiniestroDto.getIdPaisName())){
			valid = false;
		}else if (!SystemCommonUtils.isValid(servicioSiniestroDto.getIdEstadoName())){
			valid = false;
		}else if (!SystemCommonUtils.isValid(servicioSiniestroDto.getIdCiudadName())){
			valid = false;
		}else if (!(SystemCommonUtils.isValid(servicioSiniestroDto.getIdColoniaName()) || 
					SystemCommonUtils.isValid(servicioSiniestroDto.getNuevaColoniaName()))){
			valid = false;
		}else if (!SystemCommonUtils.isValid(servicioSiniestroDto.getCpName())){
			valid = false;
		}else if (!SystemCommonUtils.isValid(servicioSiniestroDto.getCalleName())){
			valid = false;
		}else if (!SystemCommonUtils.isValid(servicioSiniestroDto.getNumeroName())){
			valid = false;
		}
		return valid;
	}
	
	
	
	public short getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(short tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String[] getLeyendaServicioCabeza() {
		return leyendaServicioCabeza;
	}
		
	public void setLeyendaServicioCabeza(String[] leyendaServicioCabeza) {
		this.leyendaServicioCabeza = leyendaServicioCabeza;
	}

	public String[] getLeyendaServicioCampo() {
		return leyendaServicioCampo;
	}

	public void setLeyendaServicioCampo(String[] leyendaServicioCampo) {
		this.leyendaServicioCampo = leyendaServicioCampo;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getValoresEstatus() {
		return valoresEstatus;
	}

	public void setValoresEstatus(Map<String, String> valoresEstatus) {
		this.valoresEstatus = valoresEstatus;
	}

	public Map<String, String> getValoresTipoServicio() {
		return valoresTipoServicio;
	}

	public void setValoresTipoServicio(Map<String, String> valoresTipoServicio) {
		this.valoresTipoServicio = valoresTipoServicio;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public List<ServicioSiniestro> getServiciosSiniestrosGrid() {
		return serviciosSiniestrosGrid;
	}

	public void setServiciosSiniestrosGrid(
			List<ServicioSiniestro> serviciosSiniestrosGrid) {
		this.serviciosSiniestrosGrid = serviciosSiniestrosGrid;
	}

	public short getTipoServicioOperacion() {
		return tipoServicioOperacion;
	}

	public void setTipoServicioOperacion(short tipoServicioOperacion) {
		this.tipoServicioOperacion = tipoServicioOperacion;
	}

	public ServicioSiniestrosDTO getServicioSiniestroDto() {
		return servicioSiniestroDto;
	}

	public void setServicioSiniestroDto(ServicioSiniestrosDTO servicioSiniestroDto) {
		this.servicioSiniestroDto = servicioSiniestroDto;
	}

	public void setServicioSiniestroService(
			ServicioSiniestroService servicioSiniestroService) {
		this.servicioSiniestroService = servicioSiniestroService;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public Long getIdServicioSiniestro() {
		return idServicioSiniestro;
	}

	public void setIdServicioSiniestro(Long idServicioSiniestro) {
		this.idServicioSiniestro = idServicioSiniestro;
	}

	public boolean isCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(boolean codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public Long getIdPersonaMidas() {
		return idPersonaMidas;
	}

	public void setIdPersonaMidas(Long idPersonaMidas) {
		this.idPersonaMidas = idPersonaMidas;
	}

	public Map<Long, String> getValoresOficinas() {
		return valoresOficinas;
	}

	public void setValoresOficinas(Map<Long, String> valoresOficinas) {
		this.valoresOficinas = valoresOficinas;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public boolean isPrestadorServicio() {
		return prestadorServicio;
	}

	public void setPrestadorServicio(boolean prestadorServicio) {
		this.prestadorServicio = prestadorServicio;
	}

	public ServicioSiniestroFiltro getServicioSiniestroFiltro() {
		return servicioSiniestroFiltro;
	}

	public void setServicioSiniestroFiltro(
			ServicioSiniestroFiltro servicioSiniestroFiltro) {
		this.servicioSiniestroFiltro = servicioSiniestroFiltro;
	}


	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public short getAccion() {
		return accion;
	}


	public void setAccion(short accion) {
		this.accion = accion;
	}


	public boolean isModoAsignar() {
		return modoAsignar;
	}

	public void setModoAsignar(boolean modoAsignar) {
		this.modoAsignar = modoAsignar;
	}


	public Map<String, String> getEstados() {
		return estados;
	}


	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}


	public Map<String, String> getMunicipios() {
		return municipios;
	}


	public void setMunicipios(Map<String, String> municipios) {
		this.municipios = municipios;
	}


	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}


	public Map<String, String> getValoresEstatusAsignacion() {
		return valoresEstatusAsignacion;
	}

	public void setValoresEstatusAsignacion(Map<String, String> valoresEstatusAsignacion) {
		this.valoresEstatusAsignacion = valoresEstatusAsignacion;
	}

	public Map<String, String> getValoresTipoDisponibilidad() {
		return valoresTipoDisponibilidad;
	}

	public void setValoresTipoDisponibilidad(Map<String, String> valoresTipoDisponibilidad) {
		this.valoresTipoDisponibilidad = valoresTipoDisponibilidad;
	}


	public void setTiposPersona(Map<String, String> tiposPersona) {
		this.tiposPersona = tiposPersona;
	}


	public Map<String, String> getTiposPersona() {
		return tiposPersona;
	}


	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}


	public String getTipoPersona() {
		return tipoPersona;
	}
	
}
