package mx.com.afirme.midas2.action.siniestros.recuperacion;



import java.util.LinkedHashMap;

import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSipac;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSipacService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/recuperacionSipac")
public class RecuperacionSipacAction extends RecuperacionAction implements Preparable {
	
	private RecuperacionSipac recuperacion;

	@Autowired
	@Qualifier("recuperacionSipacServiceEJB")
	private RecuperacionSipacService recuperacionSipacService;
	
	@Autowired
	@Qualifier("recuperacionServiceEJB")
	private RecuperacionService recuperacionService;
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/sipac/contenedorRecuperacionSIPAC.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/recuperacion/sipac/contenedorRecuperacionSIPAC.jsp")
			})
	public String mostrarContenedor()
	{	
		if(this.recuperacion.getId() !=null){
			  
			recuperacion = entidadService.findById(RecuperacionSipac.class, this.recuperacion.getId());			
			esNuevoRegistro=false;
			CatValorFijo catMed = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION,recuperacion.getMedio() );
			lstMedioRecuperacion = new LinkedHashMap<String, String>();
			lstMedioRecuperacion.put(catMed.getCodigo(), catMed.getValue());			
			noReporteSiniestro=recuperacion.getReporteCabina().getNumeroReporte();
			
			if(recuperacion.getReporteCabina().getSiniestroCabina() != null)
			{
				noSiniestro = recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();				
			}
			
			lstCoberturas=recuperacionService.obtenerCoberturasRecuperacion(recuperacion.getId());
			lstPases=recuperacionService.obtenerPasesRecuperacion(recuperacion.getId());
			CatValorFijo cat = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, recuperacion.getEstatus());
			estatusRecuperacionDesc=cat.getDescripcion();
			cat = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION,recuperacion.getTipo() );
			tipoRecuperacionDesc=cat.getDescripcion();			
		}
		
		return SUCCESS;
	}
	
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/sipac/informacionRecuperacionSIPAC.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/recuperacion/sipac/informacionRecuperacionSIPAC.jsp")
			})
	public String mostrar(){
		
		if(this.recuperacion.getId()!=null){
			
			recuperacion = recuperacionSipacService.obtener(this.recuperacion.getId());
			
			if(tipoMostrar.equalsIgnoreCase("C") && recuperacion.getCodigoUsuarioCancelacion() != null)
			{
				recuperacion.setCodigoUsuarioCancelacion(this.getUsuarioActual().getNombreUsuario());		
			}
			
		}				
		return SUCCESS;
	}

	public RecuperacionSipac getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionSipac recuperacion) {
		this.recuperacion = recuperacion;
	}	
}
