package mx.com.afirme.midas2.action.siniestros.incentivos;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivos;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivosNotificacion;
import mx.com.afirme.midas2.domain.siniestros.incentivos.IncentivoAjustador.ConceptoIncentivo;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.siniestros.incentivos.ConfiguracionIncentivosService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace(value = "/siniestros/incentivoAjustador")
public class IncentivosAjustadorAction  extends BaseAction implements Preparable  {
	
	private static final long serialVersionUID = 1L;

	private List<ConfiguracionIncentivos> configuraciones;
	private ConfiguracionIncentivos configuracion;
	private List<IncentivoAjustadorDTO> incentivos;
	private String incentivosConcat;
	private List<IncentivoAjustadorDTO> incentivosDiaInhabil;
	private List<IncentivoAjustadorDTO> incentivosRecEfectivo;
	private List<IncentivoAjustadorDTO> incentivosRecCompania;
	private List<IncentivoAjustadorDTO> incentivosRecSipac;
	private List<IncentivoAjustadorDTO> incentivosRechazo;
	private List<IncentivoAjustadorDTO> totalesIncentivos;
	
	private Map<Long,String> oficinasActivas;
	private Map<String,String> condicionesEfectivo;
	
	private TransporteImpresionDTO transporte;
	private static final String	EXT_PDF	= ".pdf";
	private static final String	TIPO_PDF	= "application/pdf";
	
	private Boolean esConsulta;
	private String methodName;
	private String namespace;
	
	private Long configuracionId;
	
	private String destinatariosStr;
	
	private List<ConfiguracionIncentivosNotificacion> historicosEnvio;
	
	private BigDecimal montoTotal;
	
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService grupoValorService;
	
	
	
	
	@Autowired
	@Qualifier("configuracionIncentivosServiceEJB")
	private ConfiguracionIncentivosService incentivosService;
	
	
	
	final String CONTENEDOR_BUSQUEDA_INCENTIVOS=  "/jsp/siniestros/incentivos/contenedorListadoConfiguracionesAutorizacion.jsp";
	final String CONTENEDOR_BUSQUEDA_INCENTIVOS_GRID =  "/jsp/siniestros/incentivos/contenedorListadoConfiguracionesAutorizacionGrid.jsp";
	final String CONTENEDOR_BUSQUEDA_NOTIFICACIONES =  "/jsp/siniestros/incentivos/envioNotificacionesConfiguracion.jsp";
	final String CONTENEDOR_BUSQUEDA_NOTIFICACIONES_GRID =  "/jsp/siniestros/incentivos/historicoEnviosGrid.jsp";
	final String CONTENEDOR_CONFIGURACION_INCENTIVOS = "/jsp/siniestros/incentivos/contenedorConfiguracionIncentivos.jsp";
	final String CONTENEDOR_CONFIGURACION_INCENTIVOS_GRID = "/jsp/siniestros/incentivos/listadoPercepcionesAjustadoresGrid.jsp";
	final String CONTENEDOR_DETALLE_PERCEPCION = "/jsp/siniestros/incentivos/contenedorDetallePercepcion.jsp";
	final String CONTENEDOR_DETALLE_PERCEPCION_GRID ="/jsp/siniestros/incentivos/listadoDetallePercepcionGrid.jsp";
	final String CONTENEDOR_DESGLOSE_PERCEPCIONES = "/jsp/siniestros/incentivos/contenedorDesglosePercepciones.jsp";
	final String PERCEPCIONES_HORARIOINHABIL_GRID = "/jsp/siniestros/incentivos/incentivoHorarioInhabilGrid.jsp";
	final String PERCEPCIONES_RECEFECTIVO_GRID = "/jsp/siniestros/incentivos/incentivoRecEfectivoGrid.jsp";
	final String PERCEPCIONES_RECCOMPANIA_GRID = "/jsp/siniestros/incentivos/incentivoRecCompaniaGrid.jsp";
	final String PERCEPCIONES_RECSIPAC_GRID = "/jsp/siniestros/incentivos/incentivoRecSipacGrid.jsp";
	final String PERCEPCIONES_RECHAZO_GRID = "/jsp/siniestros/incentivos/incentivoRechazoGrid.jsp";
	final String TOTALES_PERCEPCIONES_GRID = "/jsp/siniestros/incentivos/totalesIncentivosGrid.jsp";
	

	
	@Override
	public void prepare() throws Exception {		
	}
	
	public void prepareMostrarContenedorIncentivosAjustador(){
		oficinasActivas = listadoService.obtenerOficinasSiniestros();
		CatValorFijo valorMayor = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.CRITERIO_COMPARACION, "MAY");
        CatValorFijo valorMayorIgual = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.CRITERIO_COMPARACION, "MAQ");
        condicionesEfectivo = new HashMap<String,String>();
        condicionesEfectivo.put(valorMayor.getCodigo(), valorMayor.getDescripcion());
        condicionesEfectivo.put(valorMayorIgual.getCodigo(), valorMayorIgual.getDescripcion());
	}

	
	@Action(value = "mostrarContenedorIncentivosAutorizacion", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_INCENTIVOS) 
	})
	public String mostrarContenedorIncentivosAutorizacion(){
		return SUCCESS;
	}

	
	@Action(value = "mostrarContenedorIncentivosAutorizacionGrid", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_INCENTIVOS_GRID),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_INCENTIVOS_GRID) 
	})
	public String mostrarContenedorIncentivosAutorizacionGrid(){
		this.configuraciones = this.incentivosService.obtenerConfiguracionesParaBandejaAutorizacion();
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedorNotificaciones", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_NOTIFICACIONES),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_INCENTIVOS) 
	})
	public String mostrarContenedorNotificaciones(){
		return SUCCESS;
	}
	
	
	@Action(value = "enviaNotificacion", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_NOTIFICACIONES),
			@Result(name = INPUT  , location = CONTENEDOR_CONFIGURACION_INCENTIVOS) 
	})
	public String enviaNotificacion(){
		this.incentivosService.enviaNotificacion(this.configuracionId, this.destinatariosStr);
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "mostrarEnvioHistorico", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_NOTIFICACIONES_GRID),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_NOTIFICACIONES) 
	})
	public String mostrarEnvioHistorico(){
		this.historicosEnvio = this.incentivosService.obtenerHistoricoNotificaciones(this.configuracionId);
		return SUCCESS;
	}
	
	
	@Action(value = "autoriza", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_INCENTIVOS) 
	})
	public String autoriza(){
		this.incentivosService.autorizaConfiguracion(this.configuracionId);
		return SUCCESS;
	}
	
	@Action(value = "rechaza", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_INCENTIVOS) 
	})
	public String rechaza(){
		this.incentivosService.rechazaConfiguracion(this.configuracionId);
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedorIncentivosAjustador", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_CONFIGURACION_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_CONFIGURACION_INCENTIVOS) 
	})
	public String mostrarContenedorIncentivosAjustador(){
		return SUCCESS;
	}
	
	@Action(value = "buscarPercepcionesAjustadores", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_CONFIGURACION_INCENTIVOS_GRID),
			@Result(name = INPUT, location = CONTENEDOR_CONFIGURACION_INCENTIVOS)})
	public String buscarPercepcionesAjustadores(){
		configuraciones = incentivosService.obtenerConfiguracionesParaListadoPercepciones();
		return SUCCESS;
	}
	
	@Action(value = "imprimirPercepcionIncentivos", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirPercepcionIncentivos(){
		transporte = incentivosService.imprimirPercepcionAjustadores(configuracion.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "PercepcionIncentivos_"+ configuracion.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value = "guardarConfiguracionIncentivos", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
	                 "configuracion.id", "${configuracion.id}"}),
             @Result(name = INPUT, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}"})}) 
	public String guardarConfiguracionIncentivos(){
		try{
			configuracion = incentivosService.guardarConfiguracionIncentivos(configuracion);
			incentivos = incentivosService.obtenerIncentivoPreliminar(configuracion.getId());
			System.out.println("guardarConfiguracionIncentivos.action:  GUARDA LA CONFIGURACION DE INCENTIVOS, OBTIENE LOS INCENTIVOS PRELIMINARES (INCENTIVOS.SIZE): " + 
					((incentivos != null )? incentivos.size() : "null" ) );
			if(incentivos == null || incentivos.isEmpty()){
				incentivosService.eliminarConfiguracion(configuracion.getId());
				setMensaje("No se encontraron Reportes de Siniestros en base a la Configuraci\u00F3n dada");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				methodName = "mostrarContenedorIncentivosAjustador";
				namespace = "/siniestros/incentivoAjustador";
				System.out.println("LA BUSQUEDA DE INCENTIVOS VINO VACIA");
				return INPUT;
			}
			setMensaje("Acci\u00F3n realizada correctamente");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			methodName = "generarConfiguracionAjustadores";
			namespace = "/siniestros/incentivoAjustador";
			System.out.println("SE GUARDO CORRECTAMENTE LA CONFIGURACION DE INCENTIVOS");
			return SUCCESS;
		}catch(Exception ex){
			setMensaje("Ocurri\u00F3 un error inesperado");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			methodName = "mostrarContenedorIncentivosAjustador";
			namespace = "/siniestros/incentivoAjustador";
			System.out.println("EXCEPCION AL GUARDAR LA CONFIGURACION DE INCENTIVOS");
			return INPUT;
		}
	}
	
	@Action(value = "generarConfiguracionAjustadores", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_DETALLE_PERCEPCION),
			@Result(name = INPUT  , location = CONTENEDOR_DETALLE_PERCEPCION) 
	})
	public String generarConfiguracionAjustadores(){
		configuracion = incentivosService.obtenerConfiguracionIncentivos(configuracion.getId());
		return SUCCESS;
	}
	
	@Action(value = "buscarDetallesPercepcion", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_DETALLE_PERCEPCION_GRID),
			@Result(name = INPUT  , location = CONTENEDOR_DETALLE_PERCEPCION_GRID) 
	})
	public String buscarDetallesPercepcion(){
		incentivos = incentivosService.obtenerIncentivoPreliminar(configuracion.getId());
		System.out.println("BUSCAR DETALLES DE PERCEPCION: " + ((incentivos != null )? incentivos.size() : "null" ));
		
		return SUCCESS;
	}
	
	@Action(value = "generarPercepcionAjustadores", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_DETALLE_PERCEPCION),
			@Result(name = INPUT  , location = CONTENEDOR_DETALLE_PERCEPCION) 
	})
	public String generarPercepcionAjustadores(){
		return SUCCESS;
	}

	
	@Action(value = "preRegistroPercepciones", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
	                 "configuracionId", "${configuracionId}"}),
             @Result(name = INPUT, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}"})}) 
	public String preRegistroPercepciones(){
		try{
			incentivosService.preRegistroIncentivos(configuracionId, incentivosConcat);
			methodName = "mostrarContenedorDesglosePercepciones";
			namespace = "/siniestros/incentivoAjustador";
			return SUCCESS;
		}catch(Exception ex){
			setMensaje("Ocurri\u00F3 un error inesperado");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			methodName = "generarConfiguracionAjustadores";
			namespace = "/siniestros/incentivoAjustador";
			return INPUT;
		}
	}
	
	@Action(value = "mostrarContenedorDesglosePercepciones", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_DESGLOSE_PERCEPCIONES),
			@Result(name = INPUT  , location = CONTENEDOR_DESGLOSE_PERCEPCIONES) 
	})
	public String mostrarContenedorDesglosePercepciones(){
		configuracion = incentivosService.obtenerConfiguracionIncentivos(configuracionId);
		
		totalesIncentivos = incentivosService.obtenerIncentivosAjustador(configuracionId);
		montoTotal = BigDecimal.ZERO;
		
		for (IncentivoAjustadorDTO incentivo : totalesIncentivos) {
			montoTotal = montoTotal.add(incentivo.getMontoTotal());
		}
		return SUCCESS;
	}
	
	@Action(value = "obtenerIncentivosHorarioInhabil", results = {
			@Result(name = SUCCESS, location = PERCEPCIONES_HORARIOINHABIL_GRID),
			@Result(name = INPUT  , location = PERCEPCIONES_HORARIOINHABIL_GRID) 
	})
	public String obtenerIncentivosHorarioInhabil(){
		incentivosDiaInhabil = incentivosService.obtenerIncentivos(configuracionId, ConceptoIncentivo.INHABIL);
		return SUCCESS;
	}
	
	@Action(value = "obtenerIncentivosRecuperacionEfectivo", results = {
			@Result(name = SUCCESS, location = PERCEPCIONES_RECEFECTIVO_GRID),
			@Result(name = INPUT  , location = PERCEPCIONES_RECEFECTIVO_GRID) 
	})
	public String obtenerIncentivosRecuperacionEfectivo(){
		incentivosRecEfectivo = incentivosService.obtenerIncentivos(configuracionId, ConceptoIncentivo.EFECTIVO);
		return SUCCESS;
	}
	
	@Action(value = "obtenerIncentivosRecuperacionCia", results = {
			@Result(name = SUCCESS, location = PERCEPCIONES_RECCOMPANIA_GRID),
			@Result(name = INPUT  , location = PERCEPCIONES_RECCOMPANIA_GRID) 
	})
	public String obtenerIncentivosRecuperacionCia(){
		incentivosRecCompania = incentivosService.obtenerIncentivos(configuracionId, ConceptoIncentivo.COMPANIA);
		return SUCCESS;
	}
	
	@Action(value = "obtenerIncentivosRecuperacionSipac", results = {
			@Result(name = SUCCESS, location = PERCEPCIONES_RECSIPAC_GRID),
			@Result(name = INPUT  , location = PERCEPCIONES_RECSIPAC_GRID) 
	})
	public String obtenerIncentivosRecuperacionSipac(){
		incentivosRecSipac = incentivosService.obtenerIncentivos(configuracionId, ConceptoIncentivo.SIPAC);
		return SUCCESS;
	}
	
	@Action(value = "obtenerIncentivosRechazo", results = {
			@Result(name = SUCCESS, location = PERCEPCIONES_RECHAZO_GRID),
			@Result(name = INPUT  , location = PERCEPCIONES_RECHAZO_GRID) 
	})
	public String obtenerIncentivosRechazo(){
		incentivosRechazo = incentivosService.obtenerIncentivos(configuracionId, ConceptoIncentivo.RECHAZO);
		return SUCCESS;
	}
	
	@Action(value = "obtenerTotalesIncentivos", results = {
			@Result(name = SUCCESS, location = TOTALES_PERCEPCIONES_GRID),
			@Result(name = INPUT  , location = TOTALES_PERCEPCIONES_GRID) 
	})
	public String obtenerTotalesIncentivos(){
		totalesIncentivos = incentivosService.obtenerIncentivosAjustador(configuracionId);
		
		return SUCCESS;
	}
	
	
	@Action(value = "registrarPercepcion", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
	                 "configuracionId", "${configuracionId}"}),
             @Result(name = INPUT, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}"})}) 
	public String registrarPercepcion(){
		try{
			configuracion = incentivosService.registrarPercepcion(configuracionId);
			methodName = "mostrarContenedorDesglosePercepciones";
			namespace = "/siniestros/incentivoAjustador";
			setMensajeExitoPersonalizado("Se ha generado exitosamente un nuevo registro de Percepciones de Ajustadores con el Folio #" + configuracion.getFolio());
			return SUCCESS;
		}catch(Exception ex){
			setMensaje("Ocurri\u00F3 un error inesperado");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			methodName = "generarConfiguracionAjustadores";
			namespace = "/siniestros/incentivoAjustador";
			return INPUT;
		}
	}
	
	@Action(value = "cancelarConfiguracionIncentivos", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_CONFIGURACION_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_CONFIGURACION_INCENTIVOS) 
	})
	public String cancelarConfiguracionIncentivos(){
		incentivosService.cancelarConfiguracion(configuracion.getId());
		prepareMostrarContenedorIncentivosAjustador();
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "eliminarConfiguracion", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_CONFIGURACION_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_CONFIGURACION_INCENTIVOS) 
	})
	public String eliminarConfiguracion(){
		incentivosService.eliminarConfiguracion(configuracionId);
		prepareMostrarContenedorIncentivosAjustador();
		return SUCCESS;
	}
	
	@Action(value = "enviaAutorizacion", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_INCENTIVOS),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_INCENTIVOS) 
	})
	public String enviaAutorizacion(){
		this.incentivosService.enviaAutorizacion(this.configuracionId);
		return SUCCESS;
	}

	public List<ConfiguracionIncentivos> getConfiguraciones() {
		return configuraciones;
	}


	public void setConfiguraciones(List<ConfiguracionIncentivos> configuraciones) {
		this.configuraciones = configuraciones;
	}
	
	public ConfiguracionIncentivosService getIncentivosService() {
		return incentivosService;
	}


	public void setIncentivosService(
			ConfiguracionIncentivosService incentivosService) {
		this.incentivosService = incentivosService;
	}


	public Long getConfiguracionId() {
		return configuracionId;
	}


	public void setConfiguracionId(Long configuracionId) {
		this.configuracionId = configuracionId;
	}


	public List<ConfiguracionIncentivosNotificacion> getHistoricosEnvio() {
		return historicosEnvio;
	}


	public void setHistoricosEnvio(
			List<ConfiguracionIncentivosNotificacion> historicosEnvio) {
		this.historicosEnvio = historicosEnvio;
	}


	public String getDestinatariosStr() {
		return destinatariosStr;
	}


	public void setDestinatariosStr(String destinatariosStr) {
		this.destinatariosStr = destinatariosStr;
	}
	
	/**
	 * @return the configuracion
	 */
	public ConfiguracionIncentivos getConfiguracion() {
		return configuracion;
	}


	/**
	 * @param configuracion the configuracion to set
	 */
	public void setConfiguracion(ConfiguracionIncentivos configuracion) {
		this.configuracion = configuracion;
	}


	/**
	 * @return the oficinasActivas
	 */
	public Map<Long, String> getOficinasActivas() {
		return oficinasActivas;
	}


	/**
	 * @param oficinasActivas the oficinasActivas to set
	 */
	public void setOficinasActivas(Map<Long, String> oficinasActivas) {
		this.oficinasActivas = oficinasActivas;
	}

	/**
	 * @return the condicionesEfectivo
	 */
	public Map<String, String> getCondicionesEfectivo() {
		return condicionesEfectivo;
	}

	/**
	 * @param condicionesEfectivo the condicionesEfectivo to set
	 */
	public void setCondicionesEfectivo(Map<String, String> condicionesEfectivo) {
		this.condicionesEfectivo = condicionesEfectivo;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the incentivos
	 */
	public List<IncentivoAjustadorDTO> getIncentivos() {
		return incentivos;
	}

	/**
	 * @param incentivos the incentivos to set
	 */
	public void setIncentivos(List<IncentivoAjustadorDTO> incentivos) {
		this.incentivos = incentivos;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the esConsulta
	 */
	public Boolean getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Boolean esConsulta) {
		this.esConsulta = esConsulta;
	}

	/**
	 * @return the incentivosConcat
	 */
	public String getIncentivosConcat() {
		return incentivosConcat;
	}

	/**
	 * @param incentivosConcat the incentivosConcat to set
	 */
	public void setIncentivosConcat(String incentivosConcat) {
		this.incentivosConcat = incentivosConcat;
	}

	public List<IncentivoAjustadorDTO> getIncentivosDiaInhabil() {
		return incentivosDiaInhabil;
	}

	public void setIncentivosDiaInhabil(
			List<IncentivoAjustadorDTO> incentivosDiaInhabil) {
		this.incentivosDiaInhabil = incentivosDiaInhabil;
	}

	public List<IncentivoAjustadorDTO> getIncentivosRecEfectivo() {
		return incentivosRecEfectivo;
	}

	public void setIncentivosRecEfectivo(
			List<IncentivoAjustadorDTO> incentivosRecEfectivo) {
		this.incentivosRecEfectivo = incentivosRecEfectivo;
	}

	public List<IncentivoAjustadorDTO> getIncentivosRecCompania() {
		return incentivosRecCompania;
	}

	public void setIncentivosRecCompania(
			List<IncentivoAjustadorDTO> incentivosRecCompania) {
		this.incentivosRecCompania = incentivosRecCompania;
	}

	public List<IncentivoAjustadorDTO> getIncentivosRecSipac() {
		return incentivosRecSipac;
	}

	public void setIncentivosRecSipac(List<IncentivoAjustadorDTO> incentivosRecSipac) {
		this.incentivosRecSipac = incentivosRecSipac;
	}

	public List<IncentivoAjustadorDTO> getIncentivosRechazo() {
		return incentivosRechazo;
	}

	public void setIncentivosRechazo(List<IncentivoAjustadorDTO> incentivosRechazo) {
		this.incentivosRechazo = incentivosRechazo;
	}

	public List<IncentivoAjustadorDTO> getTotalesIncentivos() {
		return totalesIncentivos;
	}

	public void setTotalesIncentivos(List<IncentivoAjustadorDTO> totalesIncentivos) {
		this.totalesIncentivos = totalesIncentivos;
	}

	public BigDecimal getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(BigDecimal montoTotal) {
		this.montoTotal = montoTotal;
	}
	
	
	

}