package mx.com.afirme.midas2.action.siniestros.catalogo.pieza;

import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService.ValuacionReporteFiltro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.catalogo.pieza.PiezaValuacionService;
import mx.com.afirme.midas2.service.siniestros.catalogo.pieza.PiezaValuacionService.PiezaValuacionFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/catalogo/pieza/piezaValuacion")

public class PiezaValuacionAction extends BaseAction implements Preparable {

	private static final String	LOCATION_PIEZAVALUACIONCATALOGO_JSP	= "/jsp/siniestros/catalogo/pieza/piezaValuacionCatalogo.jsp";
	private static final long        serialVersionUID     = 5316354006705323654L;
	// accion # 1-ALTA 0-MODIFICAR 2-VER DETALLE READ ONLY
	private short     				 accion               = 1;			  
	private TransporteImpresionDTO   transporteExcel;
	private List<PiezaValuacion>     piezaValuacionGrid;
	private PiezaValuacionFiltro     piezaValuacionFiltro;
	private PiezaValuacion           piezaValuacion;
	private Map<String,String>       ctgEstatus 	       = null;
	private Map<String,String>       ctgSeccionAuto 	   = null;
	private Long                     piezaValuacionId      ;
	
//	VARIABLES PARA REGRESAR A LA PAGINA DE VALUACIONREPORTE AL CERRAR
	private Boolean 				 valuacionEsPantallaPrevia;
	private Long 					 idValuacionReporte;
	private String					 methodName;
	private String					 namespace;
	private ValuacionReporteFiltro 	 valuacionReporteFiltro;
	private Integer 				 esConsulta;
	
	@Autowired
	@Qualifier("piezaValuacionServiceEJB")
	private PiezaValuacionService    piezaValuacionService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Override
	public void prepare(){
		
		// # CATALOGO DE ESTATUS
		ctgEstatus     = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.ESTATUS );
		ctgSeccionAuto = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.SECCION_AUTOMOVIL );
		
	}
	
	@Action(value="listarCarga", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/pieza/piezaValuacionGrid.jsp"),
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/pieza/piezaValuacionGrid.jsp")
		})
	public String listarCarga(){
		this.piezaValuacionGrid = this.piezaValuacionService.listar(piezaValuacionFiltro);
		return SUCCESS;
	}
	
	@Action(value="mostrar", results={
		@Result(name = SUCCESS, location = LOCATION_PIEZAVALUACIONCATALOGO_JSP),
		@Result(name = INPUT, location = LOCATION_PIEZAVALUACIONCATALOGO_JSP)
	})
	public String mostrar(){
		return SUCCESS;
	}
	
	
	@Action(value="busqueda", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/pieza/piezaValuacionGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_PIEZAVALUACIONCATALOGO_JSP)
	})
	public String buscar(){
		piezaValuacionFiltro.setDescripcion(StringUtil.decodeUri(piezaValuacionFiltro.getDescripcion()));
		this.piezaValuacionGrid = this.piezaValuacionService.listar(piezaValuacionFiltro);
		if( this.piezaValuacionGrid.isEmpty() ){
			super.setMensajeError("No se encontraron resultados");
		}
		return SUCCESS;
	}
	
	
	@Action(value="mostrarAlta", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/pieza/piezaValuacionDetalle.jsp")
	})
	public String alta(){
		return SUCCESS;
	}
	
	@Action(value="verDetalle", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/pieza/piezaValuacionDetalle.jsp")
	})
	public String verDetalle(){
		
		this.piezaValuacion = this.piezaValuacionService.obtener(this.piezaValuacionId);
		return SUCCESS;
	}
	
	@Action(value="salvarActualizar", results={
			@Result(name = SUCCESS, location = LOCATION_PIEZAVALUACIONCATALOGO_JSP),
			@Result(name = "SUCCESS_VALUACION", type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "valuacionReporteFiltro.ajustador", "${valuacionReporteFiltro.ajustador}",
	                 "valuacionReporteFiltro.estatus", "${valuacionReporteFiltro.estatus}",
	                 "valuacionReporteFiltro.fechaFinal", "${valuacionReporteFiltro.fechaFinal}",
	                 "valuacionReporteFiltro.fechaInicial", "${valuacionReporteFiltro.fechaInicial}",
	                 "valuacionReporteFiltro.numeroReporte", "${valuacionReporteFiltro.numeroReporte}",
	                 "valuacionReporteFiltro.numeroSerie", "${valuacionReporteFiltro.numeroSerie}",
	                 "valuacionReporteFiltro.numeroValuacion", "${valuacionReporteFiltro.numeroValuacion}",
	                 "valuacionReporteFiltro.valuador", "${valuacionReporteFiltro.valuador}",
	                 "esConsulta", "${esConsulta}",
	                 "idValuacionReporte", "${idValuacionReporte}" }),
			@Result(name = INPUT, location = "/jsp/siniestros/catalogo/pieza/piezaValuacionDetalle.jsp")
	})
	public String salvarActualizar(){
		String success = "";
		try {
			this.piezaValuacionService.guardar(piezaValuacion);
			setMensajeExito();
		}catch (Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "piezaValuacion");
			}
			
			return INPUT;
		}
		
		if(valuacionEsPantallaPrevia){
			success = "SUCCESS_VALUACION";
			methodName = "obtener";
			namespace = "/siniestros/valuacion";
		}else{
			success = SUCCESS;
		}
		return success;
	}	
	
	
	/***
	 * Exportar excel
	 * @return
	 */
	@Action(value="exportarExcel",
			results={@Result(
					name=SUCCESS,
					type="stream",
					params={
							"contentType","${transporteExcel.contentType}",
							"inputName","transporteExcel.genericInputStream",
							"contentDisposition",
							"attachment;filename=\"${transporteExcel.fileName}\""
						}
				)
			}
	)
	public String exportarExcel(){
		piezaValuacionFiltro.setDescripcion(StringUtil.decodeUri(piezaValuacionFiltro.getDescripcion()));
		this.piezaValuacionGrid = this.piezaValuacionService.listar(piezaValuacionFiltro);
		
		ExcelExporter exporter = new ExcelExporter(PiezaValuacion.class);
		this.transporteExcel = exporter.exportXLS(piezaValuacionGrid, "Listado de Piezas");
				
		return SUCCESS;

	}

	public short getAccion() {
		return accion;
	}

	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public List<PiezaValuacion> getPiezaValuacionGrid() {
		return piezaValuacionGrid;
	}

	public PiezaValuacionFiltro getPiezaValuacionFiltro() {
		return piezaValuacionFiltro;
	}

	public PiezaValuacion getPiezaValuacion() {
		return piezaValuacion;
	}

	public Map<String, String> getCtgEstatus() {
		return ctgEstatus;
	}

	public Map<String, String> getCtgSeccionAuto() {
		return ctgSeccionAuto;
	}

	public PiezaValuacionService getPiezaValuacionService() {
		return piezaValuacionService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setAccion(short accion) {
		this.accion = accion;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public void setPiezaValuacionGrid(List<PiezaValuacion> piezaValuacionGrid) {
		this.piezaValuacionGrid = piezaValuacionGrid;
	}

	public void setPiezaValuacionFiltro(PiezaValuacionFiltro piezaValuacionFiltro) {
		this.piezaValuacionFiltro = piezaValuacionFiltro;
	}

	public void setPiezaValuacion(PiezaValuacion piezaValuacion) {
		this.piezaValuacion = piezaValuacion;
	}

	public void setCtgEstatus(Map<String, String> ctgEstatus) {
		this.ctgEstatus = ctgEstatus;
	}

	public void setCtgSeccionAuto(Map<String, String> ctgSeccionAuto) {
		this.ctgSeccionAuto = ctgSeccionAuto;
	}

	public void setPiezaValuacionService(PiezaValuacionService piezaValuacionService) {
		this.piezaValuacionService = piezaValuacionService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Long getPiezaValuacionId() {
		return piezaValuacionId;
	}

	public void setPiezaValuacionId(Long piezaValuacionId) {
		this.piezaValuacionId = piezaValuacionId;
	}

	/**
	 * @return the valuacionEsPantallaPrevia
	 */
	public Boolean getValuacionEsPantallaPrevia() {
		return valuacionEsPantallaPrevia;
	}

	/**
	 * @param valuacionEsPantallaPrevia the valuacionEsPantallaPrevia to set
	 */
	public void setValuacionEsPantallaPrevia(Boolean valuacionEsPantallaPrevia) {
		this.valuacionEsPantallaPrevia = valuacionEsPantallaPrevia;
	}

	/**
	 * @return the idValuacionReporte
	 */
	public Long getIdValuacionReporte() {
		return idValuacionReporte;
	}

	/**
	 * @param idValuacionReporte the idValuacionReporte to set
	 */
	public void setIdValuacionReporte(Long idValuacionReporte) {
		this.idValuacionReporte = idValuacionReporte;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the valuacionReporteFiltro
	 */
	public ValuacionReporteFiltro getValuacionReporteFiltro() {
		return valuacionReporteFiltro;
	}

	/**
	 * @param valuacionReporteFiltro the valuacionReporteFiltro to set
	 */
	public void setValuacionReporteFiltro(
			ValuacionReporteFiltro valuacionReporteFiltro) {
		this.valuacionReporteFiltro = valuacionReporteFiltro;
	}

	/**
	 * @return the esConsulta
	 */
	public Integer getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Integer esConsulta) {
		this.esConsulta = esConsulta;
	}

}
