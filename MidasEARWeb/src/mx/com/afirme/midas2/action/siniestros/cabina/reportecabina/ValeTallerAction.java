package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ValeTallerDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;



/**
 * @author simavera
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/valeTaller")
public class ValeTallerAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = -124910831918996671L;
	private Long idReporteCabina;
	private ValeTallerDTO valeTaller;
	private Map<String,String> listServicio;
	private Map<String,String> listUbicacion;
	private Long idGrua;
	private List<PaseAtencionSiniestroDTO> listPases;
	private TransporteImpresionDTO transporte;
	private String urlRedirect;
	private Short soloConsulta;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("gastoAjusteServiceEJB")
	private GastoAjusteService gastoAjusteService;

	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	private PrestadorDeServicioService prestadorServicioService;	

	@Autowired
	@Qualifier("siniestroCabinaServiceEJB")
	private SiniestroCabinaService siniestroService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	public ValeTallerAction(){

	}
	/**
	 * Inicializar los valores
	 * 
	 *Inicializar los valores
	 *listServicio: listadoService.obtenerCatalogoValorFijo con TIPO_SERVICIO_GRUA
	 *Invocar reporteCabinaService.obtenerDatosReporte y actualizar valeTaller con los datos de numeroPoliza, numeroInciso, numeroSiniestro, nombreAjustador y claveAjustador
	 *Invocar prestadorDeServicioService.buscarPrestador y actualizar valeTaller con los datos nombreGrua,
     *listPases: estimacionCoberturaSiniestroService.obtenerListadoPases con tipoEstimacion = RCV 
     **/
	public void prepare(){
		DatosReporteSiniestroDTO datosReporte = null;
		PrestadorServicio prestador = null;
		ReporteCabina reporteCabina = null;
		valeTaller = new ValeTallerDTO();
		
		listServicio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_SERVICIO_GRUA);
		listUbicacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.UBICACION_SERVICIO_GRUA);
		datosReporte = reporteCabinaService.obtenerDatosReporte( this.idReporteCabina );
		valeTaller.setNumeroPoliza( datosReporte.getNumeroPoliza() );
		valeTaller.setNumeroInciso( datosReporte.getNumeroInciso() );
		valeTaller.setNumeroSiniestro( datosReporte.getNumeroSiniestro());
		DateTime fecha = TimeUtils.now();
		valeTaller.setDia(String.valueOf(fecha.getDayOfMonth()));
		valeTaller.setMes(String.valueOf(fecha.getMonthOfYear()));
		valeTaller.setAnio(String.valueOf(fecha.getYear()));
		valeTaller.setHora(String.valueOf(fecha.getHourOfDay()) + ':' +  String.format("%02d", fecha.getMinuteOfHour()));
		valeTaller.setAmPm(new SimpleDateFormat("aa").format(fecha.toDate()));
		
		reporteCabina = entidadService.findById( ReporteCabina.class, this.idReporteCabina );
		
		if (reporteCabina.getAjustador() != null) {
			valeTaller.setNombreAjustador( reporteCabina.getAjustador().getNombrePersona() );
			valeTaller.setClaveAjustador( reporteCabina.getAjustador().getCodigoUsuarioCreacion() );
		}	
		
		if (idGrua != null) {
			prestador = prestadorServicioService.buscarPrestador( this.idGrua.intValue() );
		}		
		
		if (prestador != null && prestador.getPersonaMidas() != null) {
			valeTaller.setNombreGrua( prestador.getPersonaMidas().getNombre() );
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("claveTipoCalculo", ClaveTipoCalculo.RESPONSABILIDAD_CIVIL.getValue());
		params.put("incisoReporteCabina.id", reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getId());
		List<CoberturaReporteCabina> lstCoberturas = entidadService.findByProperties(CoberturaReporteCabina.class, params);
		if (lstCoberturas != null && !lstCoberturas.isEmpty()) {
			listPases = estimacionCoberturaSiniestroService.obtenerListadoPases(lstCoberturas.get(0).getId(), TipoEstimacion.RC_VEHICULO.getValue());
		}
		
		
	}
	
	/**
	 * Inicia pantalla principal
	 */
	@Action(value = "mostrarVale", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/valeTaller.jsp") })
	public String mostrarVale(){
		
		return SUCCESS;
	}
	
	/**
	 * invocar al mÃ©todo obtenerAutoIncisoByReporteCabina de SiniestroCabinaService, 
	 * con el objeto llenar los valores 
	 * modelo, 
	 * marcaVehiculo, 
	 * tipoVehiculoDesc, 
	 * numeroPlaca y 
	 * color de la variable valeTaller
	 * 
	 */
	@Action(value = "obtenerDatosInciso", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/valeTaller.jsp") })
	public String obtenerDatosInciso(){
		AutoIncisoReporteCabina autoIncisoReporte = null;
		
		autoIncisoReporte = siniestroService.obtenerAutoIncisoByReporteCabina( this.idReporteCabina );
		
		valeTaller.setNombreAsegurado(autoIncisoReporte.getIncisoReporteCabina().getNombreAsegurado());
		valeTaller.setModelo(Integer.valueOf((autoIncisoReporte.getModeloVehiculo())));
		valeTaller.setMarcaVehiculo( autoIncisoReporte.getDescripcionFinal());
		valeTaller.setNumeroPlacas( autoIncisoReporte.getPlaca() );
		valeTaller.setColor( autoIncisoReporte.getDescColor() );
		
		return SUCCESS;
	}
	
	
	@Action(value = "obtenerDatosPase", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/valeTaller.jsp") })
	public String obtenerDatosPase(){
		
		EstimacionCoberturaReporteCabina estimacion  = entidadService.findById(EstimacionCoberturaReporteCabina.class, this.valeTaller.getIdTercero());
		EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestro = this.estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
		valeTaller.setModelo(estimacionCoberturaSiniestro.getEstimacionVehiculos().getModeloVehiculo().intValue());
		valeTaller.setMarcaVehiculo(estimacionCoberturaSiniestro.getEstimacionVehiculos().getMarca() + '-' + estimacionCoberturaSiniestro.getEstimacionVehiculos().getEstiloVehiculo());
		valeTaller.setTipoVehiculoDesc(estimacionCoberturaSiniestro.getEstimacionVehiculos().getEstiloVehiculo());
		valeTaller.setNumeroPlacas(estimacionCoberturaSiniestro.getEstimacionVehiculos().getPlacas());
		valeTaller.setNombreAsegurado(estimacionCoberturaSiniestro.getEstimacionCoberturaReporte().getNombreAfectado());
		valeTaller.setColor(estimacionCoberturaSiniestro.getEstimacionVehiculos().getDescColor());

		return SUCCESS;
	}


	/**
	 * Invocar al metodo imprimirValeGrua de GastoAjusteService
	 */
	@Action(value = "imprimirVale", results = { 
			@Result(name = SUCCESS, 
					type = "stream", 
					params = { 
						"contentType","${transporte.contentType}", 
						"contentDisposition", "attachment;filename=\"${transporte.fileName}\"", 
						"inputName","transporte.genericInputStream" 
					}),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" })})
	public String imprimirVale(){
		try{
			transporte = gastoAjusteService.imprimirValeTaller( this.valeTaller );
			
			if (transporte.getByteArray() == null) {
				setMensaje("No se encontró información para los criterios de búsqueda");
				return INPUT;
			} else {
				transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
				transporte.setContentType("application/pdf");
				String fileName = "ValeTaller_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
				transporte.setFileName(fileName);	
			}
			
			
			
		}catch(Exception e){

		}
		return SUCCESS;
	}
	
	/**
	 * @return the idReporteCabina
	 */
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}
	
	/**
	 * @param idReporteCabina the idReporteCabina to set
	 */
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}
	
	/**
	 * @return the valeTaller
	 */
	public ValeTallerDTO getValeTaller() {
		return valeTaller;
	}
	
	/**
	 * @param valeTaller the valeTaller to set
	 */
	public void setValeTaller(ValeTallerDTO valeTaller) {
		this.valeTaller = valeTaller;
	}
	
	/**
	 * @return the listServicio
	 */
	public Map<String, String> getListServicio() {
		return listServicio;
	}
	
	/**
	 * @param listServicio the listServicio to set
	 */
	public void setListServicio(Map<String, String> listServicio) {
		this.listServicio = listServicio;
	}
	
	/**
	 * @return the listUbicacion
	 */
	public Map<String, String> getListUbicacion() {
		return listUbicacion;
	}
	/**
	 * @param listUbicacion the listUbicacion to set
	 */
	public void setListUbicacion(Map<String, String> listUbicacion) {
		this.listUbicacion = listUbicacion;
	}
	/**
	 * @return the idGrua
	 */
	public Long getIdGrua() {
		return idGrua;
	}
	
	/**
	 * @param idGrua the idGrua to set
	 */
	public void setIdGrua(Long idGrua) {
		this.idGrua = idGrua;
	}
	
	/**
	 * @return the listPases
	 */
	public List<PaseAtencionSiniestroDTO> getListPases() {
		return listPases;
	}
	
	/**
	 * @param listPases the listPases to set
	 */
	public void setListPases(List<PaseAtencionSiniestroDTO> listPases) {
		this.listPases = listPases;
	}
	
	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}
	
	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}
	
	/**
	 * @param listadoService the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	/**
	 * @return the gastoAjusteService
	 */
	public GastoAjusteService getGastoAjusteService() {
		return gastoAjusteService;
	}
	/**
	 * @param gastoAjusteService the gastoAjusteService to set
	 */
	public void setGastoAjusteService(GastoAjusteService gastoAjusteService) {
		this.gastoAjusteService = gastoAjusteService;
	}
	/**
	 * @return the estimacionCoberturaSiniestroService
	 */
	public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
		return estimacionCoberturaSiniestroService;
	}
	
	/**
	 * @param estimacionCoberturaSiniestroService the estimacionCoberturaSiniestroService to set
	 */
	public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}
	
	/**
	 * @return the reporteCabinaService
	 */
	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}
	
	/**
	 * @param reporteCabinaService the reporteCabinaService to set
	 */
	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}
	
	/**
	 * @return the prestadorServicioService
	 */
	public PrestadorDeServicioService getPrestadorServicioService() {
		return prestadorServicioService;
	}
	
	/**
	 * @param prestadorServicioService the prestadorServicioService to set
	 */
	public void setPrestadorServicioService(
			PrestadorDeServicioService prestadorServicioService) {
		this.prestadorServicioService = prestadorServicioService;
	}
	
	/**
	 * @return the siniestroService
	 */
	public SiniestroCabinaService getSiniestroService() {
		return siniestroService;
	}
	/**
	 * @param siniestroService the siniestroService to set
	 */
	public void setSiniestroService(SiniestroCabinaService siniestroService) {
		this.siniestroService = siniestroService;
	}
	/**
	 * @return the entidadService
	 */
	public EntidadService getEntidadService() {
		return entidadService;
	}
	
	/**
	 * @param entidadService the entidadService to set
	 */
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	/**
	 * @return the urlRedirect
	 */
	public String getUrlRedirect() {
		return urlRedirect;
	}
	/**
	 * @param urlRedirect the urlRedirect to set
	 */
	public void setUrlRedirect(String urlRedirect) {
		this.urlRedirect = urlRedirect;
	}
	
	public Short getSoloConsulta() {
		return soloConsulta;
	}
	
	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}	

}