package mx.com.afirme.midas2.action.siniestros.indemnizacion.perdidatotal;

import java.io.File;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.cobranza.recibos.consolidar.ReciboConsolidadoAction;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class PerdidaTotalCFDIAction extends BaseAction implements Preparable{
	Long id;
	private static final Logger LOG = Logger.getLogger(PerdidaTotalCFDIAction.class);
	private static final long serialVersionUID = 3121156965146250377L;
	File file;
	private EntidadService entidadService;
	private Object model;
	private Object data;
	String responseString;
	String xmlString= "";
	private TransporteImpresionDTO transporte;
	
	@Autowired
	@Qualifier("cartaSiniestroServiceEJB")
	private CartaSiniestroService cartaSiniestroService;
	@Autowired
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;

	private static final Logger logger = Logger.getLogger(PerdidaTotalCFDIAction.class);
	String numReporte= "";
	String correo= "";
	String idValuador= "";
	String subject= "";
	String messageContent= "";
	String from= "";
	String greeting= "";
	String idIndemnizacion= "";
	/**
	 * /rest/perdidaTotalCFDI/validarXML.action
	 * @return json String
	 */
	@Autowired
	@Qualifier("perdidaTotalServiceEJB")
	private PerdidaTotalService perdidaTotalService;
		

	public String enviarCorreoIndemnizacionCFDI() {
		logger.info(">> enviarCorreoIndemnizacionCFDI()");
		logger.info("numReporte ->"+ numReporte +"<-");	
		logger.info("correo ->"+ correo +"<-");	
		logger.info("idValuador ->"+ idValuador +"<-");	
		logger.info("subject ->"+ subject +"<-");	
		logger.info("messageContent ->"+ messageContent +"<-");	
		logger.info("from ->"+from  +"<-");	
		logger.info("greeting ->"+greeting  +"<-");

		EnvioCaratulaParameter envioCaratulaParameter = new EnvioCaratulaParameter();

		StringBuilder mensajeMail2 = new StringBuilder();
		mensajeMail2.append(messageContent);
		StringBuilder title2 = new StringBuilder();
		title2.append(subject);
		StringBuilder subject2 = new StringBuilder();
		subject2.append("");
		StringBuilder greeting2 = new StringBuilder();
		greeting2.append(greeting);

			envioCaratulaParameter.setPolizaID("0");
			envioCaratulaParameter.setMailService(mailService);
			envioCaratulaParameter.setEntidadService(entidadService);
			envioCaratulaParameter
			.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
			envioCaratulaParameter.setLlaveFiscal(" ");
			envioCaratulaParameter.setMessage(mensajeMail2.toString());
			envioCaratulaParameter.setTitle(title2.toString());
			envioCaratulaParameter.setSubject(subject2.toString());
			envioCaratulaParameter.setGreeting(greeting2.toString());
			envioCaratulaParameter.setCorreo(correo);

			String responseString ="";
			try {
	
					responseString =cartaSiniestroService.enviarCorreoCartaNotificacionPerdidaTotal(envioCaratulaParameter,new Long( idIndemnizacion),new Integer( idValuador));

				} catch (Exception e) {
					responseString="No es posible enviar el correo: Error:"+e.getMessage();
					logger.error("Error al mandar llamar el envio del correo "+e.getMessage());
					e.printStackTrace();
										}
				logger.info("<< enviarCorreoIndemnizacionCFDI()");
					return responseString;
					}
	
	
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the entidadService
	 */
	public EntidadService getEntidadService() {
		return entidadService;
	}
	/**
	 * @param entidadService the entidadService to set
	 */
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService){
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
    public void setGeneraPlantillaReporteBitemporalService(GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService){
    	this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService; 
    }
	/**
	 * @return the model
	 */
	public Object getModel() {
		return model;
	}
	/**
	 * @param model the model to set
	 */
	public void setModel(Object model) {
		this.model = model;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}


	/**
	 * @return the responseString
	 */
	public String getResponseString() {
		return responseString;
	}


	/**
	 * @param responseString the responseString to set
	 */
	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}


	/**
	 * @return the perdidaTotalService
	 */
	public PerdidaTotalService getPerdidaTotalService() {
		return perdidaTotalService;
	}


	/**
	 * @param perdidaTotalService the perdidaTotalService to set
	 */
	public void setPerdidaTotalService(PerdidaTotalService perdidaTotalService) {
		this.perdidaTotalService = perdidaTotalService;
	}


	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}

	/**
	 * @param xmlString the xmlString to set
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	/**
	 * @return the numReporte
	 */
	public String getNumReporte() {
		return numReporte;
	}

	/**
	 * @param numReporte the numReporte to set
	 */
	public void setNumReporte(String numReporte) {
		this.numReporte = numReporte;
	}

	/**
	 * @return the correo
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * @param correo the correo to set
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}

	/**
	 * @return the idValuador
	 */
	public String getIdValuador() {
		return idValuador;
	}

	/**
	 * @param idValuador the idValuador to set
	 */
	public void setIdValuador(String idValuador) {
		this.idValuador = idValuador;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the messageContent
	 */
	public String getMessageContent() {
		return messageContent;
	}

	/**
	 * @param messageContent the messageContent to set
	 */
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the greeting
	 */
	public String getGreeting() {
		return greeting;
	}

	/**
	 * @param greeting the greeting to set
	 */
	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	/**
	 * @return the idIndemnizacion
	 */
	public String getIdIndemnizacion() {
		return idIndemnizacion;
	}

	/**
	 * @param idIndemnizacion the idIndemnizacion to set
	 */
	public void setIdIndemnizacion(String idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}

	@Override
	public void prepare(){
		
	}
}
