package mx.com.afirme.midas2.action.siniestros.catalogo.oficina;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.OficinaEstado;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService.OficinaFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.JpaUtil;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/siniestros/catalogo/oficina")
public class CatOficinaAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Variables de diseño
	private Boolean consulta = false;
	private Oficina entidad;
	private Map<String, String> estatus;
	private OficinaFiltro filtroOficina;
	private ListadoService listadoService;
	private List<Oficina> listaOficinas = new ArrayList<Oficina>();
	private CatalogoSiniestroService catalogoSiniestroService;
	private CatalogoSiniestroOficinaService catalogoSiniestroOficinaService;
	private String paisId;
	
	private EntidadService entidadService;
	//Servicio para obtener la lista de estados apartir de un pais
	private EstadoFacadeRemote estadoFacadeRemote;
	
	private Map<String, String> paises = new LinkedHashMap<String, String>();
	private List<EstadoMidas> estadoListDispobibles = new ArrayList<EstadoMidas>();
	private List<EstadoMidas> estadoListAsociados = new ArrayList<EstadoMidas>();
	private EstadoMidas estadoTransaccion;
	
	private Boolean mostrarDragEstados = false;
	private TransporteImpresionDTO transporte;
	
	private static final String MOSTARCONTENEDOR ="/jsp/siniestros/catalogo/oficina/contenedorOficina.jsp";
	private static final String MOSTARCONTENEDORGRID ="/jsp/siniestros/catalogo/oficina/contenedorOficinaGrid.jsp";
	private static final String MOSTARLISTADO ="/jsp/siniestros/catalogo/oficina/contenedorListadoOficina.jsp";
	private static final String ESTADOSDISPONIBLESGRID ="/jsp/siniestros/catalogo/oficina/estadosDisponiblesGrid.jsp";
	private static final String ESTADOSASOCIADOSGRID ="/jsp/siniestros/catalogo/oficina/estadosAsociadosGrid.jsp";
	private static final int ESTATUS_ACTIVO = 1;
	private static final int ESTATUS_INACTIVO = 0;
	
	private Map<String, String> tipoServicio;

	@Override
	public void prepare(){
		estatus =  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		paises = listadoService.getMapPaisesMidas();
		tipoServicio = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_SERVICIO);
	}
	
	
	
	@Action(value = "exportarListaOficinas", results = {
			@Result(name = SUCCESS, 
					type = "stream", 
					params = { 
						"contentType","${transporte.contentType}", 
						"contentDisposition", "attachment;filename=\"${transporte.fileName}\"", 
						"inputName","transporte.genericInputStream" 
					}),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
					
	public String exportarListaOficinas(){
		filtroOficina.setNombreOficina(StringUtil.decodeUri(filtroOficina.getNombreOficina()));
		filtroOficina.setClaveOficina(StringUtil.decodeUri(filtroOficina.getClaveOficina()));	
		listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina, "id".concat("-").concat(JpaUtil.DESC));
		
		ExcelExporter exporter = new ExcelExporter(Oficina.class);
		transporte = exporter.exportXLS(listaOficinas, "Oficinas Siniestros");
		
		return SUCCESS;
	}
	
	
	//Accion que cargar la primera pantalla del Catalogo de oficina
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDOR),
			@Result(name=INPUT,location=MOSTARCONTENEDOR)
			})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	@Action(value="mostrarListado",results={
			@Result(name=SUCCESS,location=MOSTARLISTADO),
			@Result(name=INPUT,location=MOSTARLISTADO)
			})
	public String mostrarListado(){
		
		if(consulta){
			filtroOficina = new OficinaFiltro();
			filtroOficina.setId(Integer.valueOf(entidad.getId().toString()));
			listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina, "id".concat("-").concat(JpaUtil.DESC));
			entidad = listaOficinas.get(0);	
			entidad = catalogoSiniestroOficinaService.obtenerWsEnpoint(entidad);
		}else{
			if(entidad!= null){
				filtroOficina = new OficinaFiltro();
				filtroOficina.setId(Integer.valueOf(entidad.getId().toString()));
				listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina, "id".concat("-").concat(JpaUtil.DESC));
				entidad = listaOficinas.get(0);	
				entidad = catalogoSiniestroOficinaService.obtenerWsEnpoint(entidad);
			}else{
				entidad = new Oficina();
				entidad.setEstatus(ESTATUS_INACTIVO);
			}
		}
		return SUCCESS;
	}
	
	@Action(value="buscar",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDORGRID),
			@Result(name=INPUT,location=MOSTARCONTENEDORGRID)
			})
	public String buscar(){
		filtroOficina.setClaveOficina(StringUtil.decodeUri(filtroOficina.getClaveOficina()));
		filtroOficina.setNombreOficina(StringUtil.decodeUri(filtroOficina.getNombreOficina()));
		listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOficina, "id".concat("-").concat(JpaUtil.DESC));
		return SUCCESS;
	}
	
	@Action(value="salvarCatalogo",results={
			@Result(name=SUCCESS, location=MOSTARLISTADO),
			@Result(name=INPUT,location=MOSTARLISTADO)
			})
	public String salvarCatalogo(){		
		if(entidad == null || entidad.getId() == null){
			List<Oficina> oficina = entidadService.findByProperty(Oficina.class, "claveOficina", entidad.getClaveOficina());
			if(oficina.size() == 0){
				entidad = catalogoSiniestroOficinaService.salvarOficina(entidad);
				super.setMensajeExito();
			}else{
				super.setMensajeError("El código de oficina ya ha sido registrado anteriormente. Intente con otro.");
			}
		}else{
			Oficina oficina = entidadService.findById(Oficina.class, entidad.getId());
			boolean tieneEstados = !oficina.getOficinaEstados().isEmpty();
			if(entidad.getEstatus() == ESTATUS_INACTIVO || (entidad.getEstatus() == ESTATUS_ACTIVO && tieneEstados)){
				entidad = catalogoSiniestroOficinaService.actualizarDatosOficina(entidad, entidad.getId());
				entidad = catalogoSiniestroOficinaService.obtenerWsEnpoint(entidad);
				super.setMensajeExito();
			}else{
				entidad.setEstatus(ESTATUS_INACTIVO);
				super.setMensajeError("No es posible guardar la oficina con estatus Activo sin antes asignar estados.");
			}
		}
		return SUCCESS;
	}
	
	@Action(value="gridCatalogoOficina",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDORGRID),
			@Result(name=INPUT,location=MOSTARCONTENEDORGRID)
			})
	public String gridCatalogoOficina(){
		listaOficinas = catalogoSiniestroService.buscar(Oficina.class, filtroOficina, "id".concat("-").concat(JpaUtil.DESC));
		return SUCCESS;
	}
	
	@Action(value="loadGridEstadosAsociados",results={
			@Result(name=SUCCESS,location=ESTADOSASOCIADOSGRID),
			@Result(name=INPUT,location=ESTADOSASOCIADOSGRID)
			})
	public String loadGridEstadosAsociados(){
		
		OficinaFiltro filtroOf = new OficinaFiltro();
		if(entidad.getId() != null){
			filtroOf.setId(Integer.valueOf(entidad.getId().toString()));
			listaOficinas =  catalogoSiniestroService.buscar(Oficina.class, filtroOf,  "nombreOficina");
			entidad = listaOficinas.get(0);
			ArrayList<EstadoMidas> listaEstados = new ArrayList<EstadoMidas>();
			
			for(OficinaEstado relOficeEstado:entidad.getOficinaEstados()){
				listaEstados.add(relOficeEstado.getEstado());
			}
			
			for(EstadoMidas estado : listaEstados){
				estadoListAsociados.add(estado);
			}
			
			
			Collections.sort(estadoListAsociados, new Comparator<EstadoMidas>() {
				@Override
				public int compare(EstadoMidas o1, EstadoMidas o2) {
					return o1.getDescripcion().compareTo(o2.getDescripcion());
				}
			});
			
			
		}

		return SUCCESS;
	}
	
	@Action(value="loadGridEstadosDisponiblesPais",results={
			@Result(name=SUCCESS,location=ESTADOSDISPONIBLESGRID),
			@Result(name=INPUT,location=ESTADOSDISPONIBLESGRID)
			})
	public String loadGridEstadosDisponiblesPais(){
		if((paisId != null) && (entidad != null ) ){
			estadoListDispobibles = catalogoSiniestroOficinaService.estadosDisponiblesPorPais(paisId, entidad.getId());
		}
		return SUCCESS;
	}
	
	@Action(value="relacionarEstadoOficina",results={
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp"),
			@Result(name=INPUT,location="/jsp/relaciones/respuestaAsociacion.jsp")
			})
	public String relacionarEstadoOficina(){
		
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		estadoTransaccion = entidadService.findById(EstadoMidas.class, estadoTransaccion.getId());
		PaisMidas paisTrasferido = entidadService.findById(PaisMidas.class, estadoTransaccion.getPais().getId());
		estadoTransaccion.setPais(paisTrasferido);
		filtroOficina = new OficinaFiltro();
		
		if (accion.equals(ROW_INSERTED)) {
			entidad = catalogoSiniestroOficinaService.relacionarOficina(entidad.getId(), estadoTransaccion);
		} else if (accion.equals(ROW_DELETED)) {
			Oficina oficina = entidadService.findById(Oficina.class, entidad.getId());
			int estados = oficina.getOficinaEstados().size();
			if(estados > 1 || oficina.getEstatus().equals(ESTATUS_INACTIVO)){
				entidad = (Oficina) catalogoSiniestroOficinaService.eliminarRelacion(entidad.getId(), estadoTransaccion.getId());
			}else{
				super.setMensajeError("No es posible quitar todos los estados relacionados si la oficina se encuentra activa.");	
			}
		}
		return SUCCESS;
	}
	
	@Action(value="relacionTodosEstados",results={
			@Result(name=SUCCESS,location=MOSTARLISTADO),
			@Result(name=INPUT,location=MOSTARLISTADO)
			})
	public String relacionTodosEstados(){
		
		estadoListDispobibles = catalogoSiniestroOficinaService.estadosDisponiblesPorPais(paisId, entidad.getId());
		entidad = catalogoSiniestroOficinaService.relacionarTodosEstados(entidad.getId(), estadoListDispobibles);
		
		return SUCCESS;
	}
	
	@Action(value="eliminarTodosEstadosRelacionados",results={
			@Result(name=SUCCESS,location=MOSTARLISTADO),
			@Result(name=INPUT,location=MOSTARLISTADO)
			})
	public String eliminarTodosEstadosRelacionados(){
		if(entidad.getEstatus().equals(ESTATUS_INACTIVO)){
			entidad = catalogoSiniestroOficinaService.eliminarRelacionTodosEstados(entidad.getId());			
		}else{
			super.setMensajeError("No es posible quitar todos los estados relacionados si la oficina se encuentra activa.");
		}
		return SUCCESS;
	}
	

	public Boolean getConsulta() {
		return consulta;
	}

	public void setConsulta(Boolean consulta) {
		this.consulta = consulta;
	}

	public Oficina getEntidad() {
		return entidad;
	}

	public void setEntidad(Oficina entidad) {
		this.entidad = entidad;
	}

	public Map<String, String> getEstatus() {
		return estatus;
	}

	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}

	public OficinaFiltro getFiltroOficina() {
		return filtroOficina;
	}

	public void setFiltroOficina(OficinaFiltro filtroOficina) {
		this.filtroOficina = filtroOficina;
	}

	public CatalogoSiniestroService getCatalogoSiniestroService() {
		return catalogoSiniestroService;
	}
	
	@Autowired
	@Qualifier("catalogoSiniestroServiceEJB")
	public void setCatalogoSiniestroService(
			CatalogoSiniestroService catalogoSiniestroService) {
		this.catalogoSiniestroService = catalogoSiniestroService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public List<Oficina> getListaOficinas() {
		return listaOficinas;
	}

	public void setListaOficinas(List<Oficina> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

	public EstadoFacadeRemote getEstadoFacadeRemote() {
		return estadoFacadeRemote;
	}
	
	@Autowired
	@Qualifier("estadoFacadeRemoteEJB")
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}

	public Map<String, String> getPaises() {
		return paises;
	}

	public void setPaises(Map<String, String> paises) {
		this.paises = paises;
	}

	public List<EstadoMidas> getEstadoListDispobibles() {
		return estadoListDispobibles;
	}

	public void setEstadoListDispobibles(List<EstadoMidas> estadoListDispobibles) {
		this.estadoListDispobibles = estadoListDispobibles;
	}

	public List<EstadoMidas> getEstadoListAsociados() {
		return estadoListAsociados;
	}

	public void setEstadoListAsociados(List<EstadoMidas> estadoListAsociados) {
		this.estadoListAsociados = estadoListAsociados;
	}

	public EstadoMidas getEstadoTransaccion() {
		return estadoTransaccion;
	}

	public void setEstadoTransaccion(EstadoMidas estadoTransaccion) {
		this.estadoTransaccion = estadoTransaccion;
	}

	public CatalogoSiniestroOficinaService getCatalogoSiniestroOficinaService() {
		return catalogoSiniestroOficinaService;
	}
	
	@Autowired
	@Qualifier("catalogoSiniestroOficinaServiceEJB")
	public void setCatalogoSiniestroOficinaService(
			CatalogoSiniestroOficinaService catalogoSiniestroOficinaService) {
		this.catalogoSiniestroOficinaService = catalogoSiniestroOficinaService;
	}

	public String getPaisId() {
		return paisId;
	}

	public void setPaisId(String paisId) {
		this.paisId = paisId;
	}

	public Boolean isMostrarDragEstados() {
		return mostrarDragEstados;
	}

	public void setMostrarDragEstados(Boolean mostrarDragEstados) {
		this.mostrarDragEstados = mostrarDragEstados;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}



	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}



	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}



	public Map<String, String> getTipoServicio() {
		return tipoServicio;
	}



	public void setTipoServicio(Map<String, String> tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

}
