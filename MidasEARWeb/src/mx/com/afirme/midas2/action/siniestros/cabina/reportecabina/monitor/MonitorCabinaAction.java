package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina.monitor;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.monitor.ReporteMonitorDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.monitor.MonitorCabinaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/monitor")
public class MonitorCabinaAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 8646993185176700221L;

	private Long reporteCabinaId;

	private Boolean paginacionEnabled = false;

	private List<ReporteMonitorDTO> reporteSiniestrosAsignacion;

	private List<ReporteMonitorDTO> reporteSiniestrosArribo;

	private List<ReporteMonitorDTO> reporteSiniestrosTermino;

	private Map<Long, String>	ctgOficinas;

	private Long			oficinaId;
	
	public Map<Long, String> getCtgOficinas() {
		if (ctgOficinas == null)
			ctgOficinas = listadoService.obtenerOficinasSiniestros();
		return ctgOficinas;
	}

	public void setCtgOficinas(Map<Long, String> ctgOficinas) {
		this.ctgOficinas = ctgOficinas;
	}
	
	private TransporteImpresionDTO transporte;

	// HACKME: obterner de cookies
	private Boolean alertasActivas = true;

	@Autowired
	@Qualifier("monitorCabinaServiceEJB")
	private MonitorCabinaService monitorCabinaService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService			listadoService;

	@Override
	public void prepare(){
	}

	@Action(value = "mostrarMonitor", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/monitor/monitorCabina.jsp"))
	public String mostrarMonitor() {
		return SUCCESS;
	}

	@Action(value = "listarReportesAsignacion", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/monitor/listadoAsignacionMonitorGrid.jsp") })
	public String listarReportesAsignacion() {
		return SUCCESS;
	}

	@Action(value = "listarReportesArribo", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/monitor/listadoArriboMonitorGrid.jsp") })
	public String listarReportesArribo() {
		return SUCCESS;
	}

	@Action(value = "listarReportesTermino", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/monitor/listadoTerminoMonitorGrid.jsp") })
	public String listarReportesTermino() {
		return SUCCESS;
	}

	@Action(value = "exportarExcel", results = { @Result(name = SUCCESS, type = "stream", params = {
			"contentType", "${transporte.contentType}", "inputName",
			"transporte.genericInputStream", "contentDisposition",
			"attachment;filename=\"${transporte.fileName}\"" }) })
	public String exportarExcel() {
		
		List<ReporteMonitorDTO> reportes = monitorCabinaService.getReporteSiniestros();

		ExcelExporter exporter = new ExcelExporter(ReporteMonitorDTO.class);
		transporte = exporter.exportXLS(reportes, "Reportes_Monitor");

		return SUCCESS;
	}

	public List<ReporteMonitorDTO> getReporteSiniestrosAsignacion() {
		if (reporteSiniestrosAsignacion == null) {
//			if (reporteCabinaId == null) {
				reporteSiniestrosAsignacion = monitorCabinaService
						.getReporteSiniestrosAsignacion();
//			} else {
//				reporteSiniestrosAsignacion = new ArrayList<ReporteMonitorDTO>();
//				ReporteCabina reporteCabina = reporteCabinaService
//						.buscarReporte(reporteCabinaId);
//				ReporteMonitorDTO reporteMonitorDTO = monitorCabinaService
//						.generarReporteMonitorDTO(reporteCabina);
//				reporteSiniestrosAsignacion.add(reporteMonitorDTO);
//			}
		}
		return reporteSiniestrosAsignacion;
	}

	public List<ReporteMonitorDTO> getReporteSiniestrosArribo() {
		if (reporteSiniestrosArribo == null) {
//			if (reporteCabinaId == null) {
				reporteSiniestrosArribo = monitorCabinaService
						.getReporteSiniestrosArribo();
//			} else {
//				reporteSiniestrosArribo = new ArrayList<ReporteMonitorDTO>();
//				ReporteCabina reporteCabina = reporteCabinaService
//						.buscarReporte(reporteCabinaId);
//				ReporteMonitorDTO reporteMonitorDTO = monitorCabinaService
//						.generarReporteMonitorDTO(reporteCabina);
//				reporteSiniestrosArribo.add(reporteMonitorDTO);
//			}
		}
		return reporteSiniestrosArribo;
	}

	public List<ReporteMonitorDTO> getReporteSiniestrosTermino() {
		if (reporteSiniestrosTermino == null) {
//			if (reporteCabinaId == null) {
				reporteSiniestrosTermino = monitorCabinaService
						.getReporteSiniestrosTermino();
//			} else {
//				reporteSiniestrosTermino = new ArrayList<ReporteMonitorDTO>();
//				ReporteCabina reporteCabina = reporteCabinaService
//						.buscarReporte(reporteCabinaId);
//				ReporteMonitorDTO reporteMonitorDTO = monitorCabinaService
//						.generarReporteMonitorDTO(reporteCabina);
//				reporteSiniestrosTermino.add(reporteMonitorDTO);
//			}
		}
		return reporteSiniestrosTermino;
	}

	public Boolean getPaginationEnabled() {
		return paginacionEnabled;
	}

	public Boolean getAlertasActivas() {
		return alertasActivas;
	}

	public void setAlertasActivas(Boolean alertasActivas) {
		this.alertasActivas = alertasActivas;
	}

	public void setId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	public Long getId() {
		return reporteCabinaId;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}

	public Long getOficinaId() {
		return oficinaId;
	}

}
