package mx.com.afirme.midas2.action.siniestros.valuacion;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReporte;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReportePieza;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgs;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ValuacionHGSEditDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ValuacionReporteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService.ValuacionReporteFiltro;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value="/siniestros/valuacion")
public class ValuacionReporteAction extends BaseAction implements Preparable{
	
	private static final String	LOCATION_CONTENEDORVALUACION_JSP	= "/jsp/siniestros/valuacion/contenedorValuacion.jsp";
	private static final String	LOCATION_VALUACIONREPORTEGRID_JSP	= "/jsp/siniestros/valuacion/valuacionReporteGrid.jsp";
	private static final String	LOCATION_VALUACIONREPORTEDETALLETOTALES_JSP	= "/jsp/siniestros/valuacion/valuacionReporteDetalleTotales.jsp";
	private static final String	LOCATION_VALUACIONREPORTEDETALLE_JSP	= "/jsp/siniestros/valuacion/valuacionReporteDetalle.jsp";
	private static final String	LOCATION_VALUACIONREPORTECATALOGO_JSP	= "/jsp/siniestros/valuacion/valuacionReporteCatalogo.jsp";
	private static final long serialVersionUID = -2070426499963518775L;
	private static final String MENSAJE_ERROR_BUSQUEDA = "No se encontraron resultados";
	private static final String SUCCESS_HGS = "SUCCESS_HGS";
	private static final String INPUT_HGS = "INPUT_HGS";
	private static final Integer TIPO_VALUACION_HGS = 2;
	private static final Integer EN_PROCESO = 1;
	private static final Integer TERMINADO = 2;
	
	private Map<String, String> ajustadores;
	private Map<String, String> valuadores;
	private Map<String, String> estatus;
	private Map<String, String> seccionesAuto;
	private Map<String, String> piezasPorSeccion;
	private Map<String, String> tiposAfectacion;
	private Map<String, String> listaTiposProcesoValuacion;
	private Map<String, String> listaEstatusValuacion;
	
	private List<ValuacionReporteDTO> valuaciones;
	private List<ValuacionReportePieza> piezas;
	
	private Long idValuacionReporte;
	private ValuacionReporte entidad;
	private String entidadEstatusDesc;
	//private IncisoSiniestroDTO incisoSiniestro;
	private AutoIncisoReporteCabina autoIncisoReporteCabina;
	private String incisoMarcaTipo;
	private String color;
	private ValuacionReportePieza piezaValuacionReporte;
	private String stringListaPiezasEliminar;
	private Boolean eliminarPiezas;
	
	private Double nuevoValorCelda;
	private Integer columna;
	private Long idPiezaValuacion;
	private String origen;
	
	private ValuacionReporteFiltro valuacionReporteFiltro;
	private Integer esConsulta;
	
	private ValuacionReporteDTO valuacionDTO;
	private Long idToReporte;
	private String namespace;
	private String methodName;
	private Boolean terminarReporte;
	private Boolean mostrarTablaVacia;
	private Integer tipoValuacion;
	private Long idCoberturaReporteCabina;
	private Long idEstimacionCoberturaReporteCabina;
	private Short soloConsulta;
	private String tipoCalculo;
	private String tipoEstimacion;
	private int pasesAtencionInt;
	private ValuacionHgs valuacionHgs;
	private ValuacionHGSEditDTO datosValuacionHgs;
	private TransporteImpresionDTO transporte;
	private static final String	EXT_PDF	= ".pdf";
	private static final String	TIPO_PDF	= "application/pdf";
	
	//datos valuacion nueva
	private Short tipoProceso;
	private Short estatusValuacion;
	private Long idValuacionHGS;
	private Date fechaIngresoTaller;
	private Date fechaTerminacion;
	private Long idValuador;
	private Date fechaReingresoReparacion;
	private Date ultimoSurtidoRefacciones;
	private String nombreValuador;
	private BigDecimal montoRefacciones;
	private BigDecimal montoManoObra;
	private BigDecimal montoTotalReparacion;
	private PrestadorServicioFiltro filtroCatalogo;
	private List<String> listaValuadores;
	private List<Map<String, Object>> listaValuadoresNombres;
	private List<PrestadorServicioRegistro> listaComboValuadores;
	private String nombreValuadorHgs;
	private Long idValuadorHgs;

	public SiniestroCabinaService getSiniestroCabinaService() {
		return siniestroCabinaService;
	}

	public void setSiniestroCabinaService(
			SiniestroCabinaService siniestroCabinaService) {
		this.siniestroCabinaService = siniestroCabinaService;
	}

	
	
	
	@Autowired
	@Qualifier("valuacionReporteServiceEJB")
	private ValuacionReporteService valuacionReporteService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("siniestroCabinaServiceEJB")
	private SiniestroCabinaService siniestroCabinaService;
	
	@Autowired
	@Qualifier("hgsServiceEJB")
	private HgsService hgsService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionService;


	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Override
	public void prepare(){
		
	}
	
	public void prepareMostrar(){
		ajustadores = listadoService.obtenerAjustadoresValuacionCrucero();
		valuadores = listadoService.obtenerValuadores();
		estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO);
	}
	
	public void prepareListar(){
		ajustadores = listadoService.obtenerAjustadoresValuacionCrucero();
		valuadores = listadoService.obtenerValuadores();
		estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO);
	}
	
	public void prepareObtener(){
		
		if(tipoValuacion != null && tipoValuacion.equals(TIPO_VALUACION_HGS))
		{
			listaTiposProcesoValuacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PROCESO_VALUACION);
			listaEstatusValuacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION);		
		
		}else
		{
			entidad = valuacionReporteService.obtener(idValuacionReporte);
			estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO);
			entidadEstatusDesc = estatus.get(entidad.getEstatus().toString());
			this.seccionesAuto = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.SECCION_AUTOMOVIL);
			
			if(piezaValuacionReporte != null && piezaValuacionReporte.getSeccionAutoId() != null){
				piezasPorSeccion = listadoService.obtenerPiezasPorSeccionAutomovil(piezaValuacionReporte.getSeccionAutoId().toString());
			}else{
				piezasPorSeccion = new LinkedHashMap<String, String>();
			}
			tiposAfectacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AFECTACION);
		}
	}
	
	public void prepareActualizarPieza(){
//		prepareObtener();
//		prepararInformacionAuto();
	}
	
	public void prepareRelacionarPieza(){
		prepareObtener();
//		prepararInformacionAuto();
	}
	
	public void prepareGuardar(){
		prepareObtener();
		prepararInformacionAuto();
	}
	
	public void prepareEnviarSolicitud(){
	}
	
	private void prepararInformacionAuto(){
		if(entidad != null && entidad.getReporte().getPoliza() !=null){
				
				autoIncisoReporteCabina = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(entidad.getReporte().getId());
				this.incisoMarcaTipo = ((autoIncisoReporteCabina.getDescMarca() != null)? autoIncisoReporteCabina.getDescMarca(): "") + 
					" - " + 
					((autoIncisoReporteCabina.getDescripcionFinal() != null)? autoIncisoReporteCabina.getDescripcionFinal(): "");
				
				/*Date fechaReporteSiniestro 			= entidad.getReporte().getFechaOcurrido();
				this.incisoSiniestro 					= new IncisoSiniestroDTO();
				
				incisoSiniestro.setFechaReporteSiniestro(fechaReporteSiniestro);
				incisoSiniestro.setNumeroPoliza( entidad.getReporte().getPoliza().getNumeroPolizaFormateada());
			
				incisoSiniestro = polizaSiniestroService.obtenerIncisoAsignadoReporte(incisoSiniestro, entidad.getReporte().getId(), fechaReporteSiniestro, fechaReporteSiniestro);
				incisoSiniestro.setFechaReporteSiniestro(fechaReporteSiniestro);
				incisoSiniestro = polizaSiniestroService.buscarDetalleInciso(incisoSiniestro, fechaReporteSiniestro, fechaReporteSiniestro);
		
				
				
				this.incisoMarcaTipo = autoIncisoReporte.getDescMarca() + " " + autoIncisoReporte.getDescTipoUso();
				Map<String, String> colores = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.COLOR);
				String colorId = entidad.getReporte().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getColor(); 
				color = colores.get(colorId);
				Short modelo = entidad.getReporte().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo();
				incisoSiniestro.setModelo(modelo.toString());*/	
				
		}
	}
	
	@Action(value = "mostrar", results = {
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTECATALOGO_JSP) ,
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTECATALOGO_JSP)})
	public String mostrar(){
		return SUCCESS;
	}
	
	@Action(value = "actualizarPieza", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/valuacion/valuacionReportePiezasGrid.jsp") ,
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTEDETALLE_JSP)})
	public String actualizarPieza(){
		entidad = valuacionReporteService.obtener(idValuacionReporte);
		this.piezas = valuacionReporteService.cargarDescripcionCatalogos(entidad.getPiezas());
		return SUCCESS;
	}
	
	@Action(value = "imprimirValuacion", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirValuacion(){
		transporte = valuacionReporteService.imprimirValuacion(idValuacionReporte);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "ValuacionReporte_"+ idValuacionReporte + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		
		return SUCCESS;
	}
	
	@Action(value = "actualizarTotales", results = {
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTEDETALLETOTALES_JSP) ,
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTEDETALLETOTALES_JSP)})
	public String actualizarTotales(){
		
		entidad = valuacionReporteService.actualizarTotales(idPiezaValuacion, columna, nuevoValorCelda, origen);
		this.piezas = valuacionReporteService.cargarDescripcionCatalogos(entidad.getPiezas());
		
		return SUCCESS;
	}	
	
	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTEDETALLE_JSP) ,
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTEDETALLE_JSP)})
	public String guardar(){
		if(entidad != null){
			if(terminarReporte != null && terminarReporte){
				entidad.setFechaValuacion(new Date());
				entidad.setEstatus(TERMINADO);
				esConsulta=1;
				List<String> mensajes = valuacionReporteService.terminarValuacionReporte(entidad);
				if(!mensajes.isEmpty()){
				esConsulta = 0;
					entidad.setEstatus(EN_PROCESO);
					setMensajeError(mensajes.get(0));
					return INPUT;
				}else{
					super.setMensajeExito();
				}
			}else{
				valuacionReporteService.guardar(entidad);
			}
		}
		entidad = valuacionReporteService.obtener(idValuacionReporte);
		entidadEstatusDesc = estatus.get(entidad.getEstatus().toString());
		return SUCCESS;
	}
	
	@Action(value = "listar", results = { 
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTEGRID_JSP),
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTECATALOGO_JSP)
			})
	public String listar() {
		if(mostrarTablaVacia){
			valuaciones = new ArrayList<ValuacionReporteDTO>();
			return SUCCESS;
		}else{
			valuaciones = valuacionReporteService.listar(valuacionReporteFiltro);
			if(valuaciones == null || valuaciones.isEmpty()){
				setMensajeError(MENSAJE_ERROR_BUSQUEDA);
			}
			return SUCCESS;
		}

	} 
	
	@Action(value = "guardarDatosValuacionManual", results = {
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTEDETALLE_JSP) ,
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTECATALOGO_JSP),
			@Result(name = SUCCESS_HGS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje"}) ,
			@Result(name = INPUT_HGS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje"}
			)})
	public String guardarDatosValuacionManual(){
		
		listaComboValuadores = valuacionReporteService.obtenerListadoValuadores();
		listaTiposProcesoValuacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PROCESO_VALUACION);
		listaEstatusValuacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION);
		
		String successVal = SUCCESS;	 	
		
		if(tipoValuacion != null && tipoValuacion.equals(TIPO_VALUACION_HGS)){
			successVal = SUCCESS_HGS;
			
			try{
				List<ValuacionHgs> listaValuaciones = hgsService.consultaValuacion(idEstimacionCoberturaReporteCabina);
				//la valuacion no existe, es nueva.
				if (listaValuaciones.isEmpty() || listaValuaciones == null ){
					
					if(valuacionHgs.getValuacionHgs() != null){
						List<ValuacionHgs> lValuacionHgs = entidadService.findByProperty(ValuacionHgs.class,"valuacionHgs", valuacionHgs.getValuacionHgs());
						if(lValuacionHgs != null
								&& !lValuacionHgs.isEmpty()){
							setMensajeError("El numero de valuacion hgs ya existe");
							prepareMostrar();
							return INPUT_HGS;
						}
					}
					
					listaValuaciones = hgsService.guardarValuacionNuevaHgs(idEstimacionCoberturaReporteCabina, 
							valuacionHgs.getClaveTipoProceso(), valuacionHgs.getClaveEstatusVal(), valuacionHgs.getValuacionHgs(), 
							valuacionHgs.getFechaIngresoTaller(), valuacionHgs.getFechaTerminacion(), valuacionHgs.getValuadorId(), 
							valuacionHgs.getFechaReingresoTaller(), valuacionHgs.getFechaUltimoSurtido(), valuacionHgs.getValuadorNombre(), 
							valuacionHgs.getImRefacciones(), valuacionHgs.getImpNanoObra(), valuacionHgs.getImpSubTotalValuacion());
					
					valuacionHgs = listaValuaciones.get(0);
					
					setMensajeExito();	
				}else{
					//la valuacion si existe
					
					listaValuaciones = hgsService.guardarValuacionExistenteHgs(idEstimacionCoberturaReporteCabina, 
							valuacionHgs.getClaveTipoProceso(), valuacionHgs.getClaveEstatusVal(), valuacionHgs.getValuacionHgs(), 
							valuacionHgs.getFechaIngresoTaller(), valuacionHgs.getFechaTerminacion(), valuacionHgs.getValuadorId(), 
							valuacionHgs.getFechaReingresoTaller(), valuacionHgs.getFechaUltimoSurtido(), valuacionHgs.getValuadorNombre(), 
							valuacionHgs.getImRefacciones(), valuacionHgs.getImpNanoObra(), valuacionHgs.getImpSubTotalValuacion());
					
					valuacionHgs = listaValuaciones.get(0);
					
					setMensajeExito();
				}
			}catch(Exception ex){
				listaComboValuadores = valuacionReporteService.obtenerListadoValuadores();
				listaTiposProcesoValuacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PROCESO_VALUACION);
				listaEstatusValuacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION);
				setMensajeError("Error al guardar la valuacion de HGS");
				prepareMostrar();
				return INPUT_HGS;
			}
			
		}
		
		
		
		return successVal;
	}
	
	@Action(value = "obtener", results = {
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTEDETALLE_JSP) ,
			@Result(name = SUCCESS_HGS, location = LOCATION_CONTENEDORVALUACION_JSP),
			@Result(name = INPUT_HGS, location = LOCATION_CONTENEDORVALUACION_JSP),			
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTECATALOGO_JSP)})
	public String obtener(){  
		String successVal = SUCCESS;	 	
		
		if(tipoValuacion != null && tipoValuacion.equals(TIPO_VALUACION_HGS))   
		{
			successVal = SUCCESS_HGS;
			try{
				List<ValuacionHgs> listaValuaciones = hgsService.consultaValuacion(idEstimacionCoberturaReporteCabina);	
				if (listaValuaciones.isEmpty() || listaValuaciones == null ){
					
					listaComboValuadores = valuacionReporteService.obtenerListadoValuadores();
					valuacionHgs = null;
					
					
				}else{
					listaComboValuadores = valuacionReporteService.obtenerListadoValuadores();
					valuacionHgs = listaValuaciones.get(0);
				}
				
				
				/*if(!listaValuaciones.isEmpty())   
				{
					valuacionHgs = listaValuaciones.get(0);							
				}
				else
				{					
					this.setMensaje(getText("midas.siniestros.valuacion.contenedorValuacion.mensajeInformativo"));
					this.setNextFunction("cerrarVentanaModal('DetValuacionHGS')");
					this.setTipoMensaje(TIPO_MENSAJE_INFORMACION);
					return INPUT_HGS;
				}*/				
			}catch(Exception ex){
				setMensajeError("Error al consultar valuacion HGS en tabla de Midas");
				prepareMostrar();
				return INPUT_HGS;
			}				
			
		}else
		{
			try{
				
				prepararInformacionAuto();
				
			}catch(Exception ex){
				setMensajeError("Error al recuperar la informacion del auto");
				prepareMostrar();
				return INPUT;
			}							
		}
		
		return successVal;
	}
	
	@Action(value = "relacionarPieza", results = {
			@Result(name = SUCCESS, location = LOCATION_VALUACIONREPORTEDETALLE_JSP) ,
			@Result(name = INPUT, location = LOCATION_VALUACIONREPORTEDETALLE_JSP)})
	public String relacionarPieza(){
		try{
			if(eliminarPiezas){
				if(stringListaPiezasEliminar.isEmpty()){
					setMensajeError("Debe seleccionar al menos un elemento de la lista.");
					return INPUT;
				}
				String[] piezasId = stringListaPiezasEliminar.split(",");
				for(String piezaId: piezasId){
					valuacionReporteService.eliminarPieza(Long.parseLong(piezaId));
				}
			}else{
				if(piezaValuacionReporte != null){
					valuacionReporteService.agregarPieza(entidad, piezaValuacionReporte);
				}
			}
		}catch (Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "piezaValuacionReporte");
			}
			
			return INPUT;
		}
		entidad = valuacionReporteService.obtener(idValuacionReporte);
		entidadEstatusDesc = estatus.get(entidad.getEstatus().toString());
		return SUCCESS;
	}
	
	/**
	 * @return the ajustadores
	 */
	public Map<String, String> getAjustadores() {
		return ajustadores;
	}
	/**
	 * @param ajustadores the ajustadores to set
	 */
	public void setAjustadores(Map<String, String> ajustadores) {
		this.ajustadores = ajustadores;
	}
	/**
	 * @return the consulta
	 */

	/**
	 * @return the entidad
	 */
	public ValuacionReporte getEntidad() {
		return entidad;
	}
	/**
	 * @return the esConsulta
	 */
	public Integer getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Integer esConsulta) {
		this.esConsulta = esConsulta;
	}

	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(ValuacionReporte entidad) {
		this.entidad = entidad;
	}
	/**
	 * @return the piezas
	 */
	public List<ValuacionReportePieza> getPiezas() {
		return piezas;
	}
	/**
	 * @param piezas the piezas to set
	 */
	public void setPiezas(List<ValuacionReportePieza> piezas) {
		this.piezas = piezas;
	}
	/**
	 * @return the valuacionReporteFiltro
	 */
	public ValuacionReporteFiltro getValuacionReporteFiltro() {
		return valuacionReporteFiltro;
	}
	/**
	 * @param valuacionReporteFiltro the valuacionReporteFiltro to set
	 */
	public void setValuacionReporteFiltro(
			ValuacionReporteFiltro valuacionReporteFiltro) {
		this.valuacionReporteFiltro = valuacionReporteFiltro;
	}
	/**
	 * @return the valuadores
	 */
	public Map<String, String> getValuadores() {
		return valuadores;
	}
	/**
	 * @param valuadores the valuadores to set
	 */
	public void setValuadores(Map<String, String> valuadores) {
		this.valuadores = valuadores;
	}

	/**
	 * @return the status
	 */
	public Map<String, String> getEstatus() {
		return estatus;
	}

	/**
	 * @param status the status to set
	 */
	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the valuaciones
	 */
	public List<ValuacionReporteDTO> getValuaciones() {
		return valuaciones;
	}

	/**
	 * @param valuaciones the valuaciones to set
	 */
	public void setValuaciones(List<ValuacionReporteDTO> valuaciones) {
		this.valuaciones = valuaciones;
	}

	/**
	 * @return the valuacionSiniestroService
	 */
	public ValuacionReporteService getValuacionReporteService() {
		return valuacionReporteService;
	}

	/**
	 * @param valuacionSiniestroService the valuacionSiniestroService to set
	 */
	public void setValuacionReporteService(
			ValuacionReporteService valuacionReporteService) {
		this.valuacionReporteService = valuacionReporteService;
	}

	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}

	/**
	 * @param listadoService the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the idValuacionReporte
	 */
	public Long getIdValuacionReporte() {
		return idValuacionReporte;
	}

	/**
	 * @param idValuacionReporte the idValuacionReporte to set
	 */
	public void setIdValuacionReporte(Long idValuacionReporte) {
		this.idValuacionReporte = idValuacionReporte;
	}

	/**
	 * @return the entidadEstatusDesc
	 */
	public String getEntidadEstatusDesc() {
		return entidadEstatusDesc;
	}

	/**
	 * @param entidadEstatusDesc the entidadEstatusDesc to set
	 */
	public void setEntidadEstatusDesc(String entidadEstatusDesc) {
		this.entidadEstatusDesc = entidadEstatusDesc;
	}


	/**
	 * @return the incisoSiniestro
	 */
	/*public IncisoSiniestroDTO getIncisoSiniestro() {
		return incisoSiniestro;
	}*/

	/**
	 * @param incisoSiniestro the incisoSiniestro to set
	 */
	/*public void setIncisoSiniestro(IncisoSiniestroDTO incisoSiniestro) {
		this.incisoSiniestro = incisoSiniestro;
	}*/
	
	

	/**
	 * @return the incisoMarcaTipo
	 */
	public String getIncisoMarcaTipo() {
		return incisoMarcaTipo;
	}

	public AutoIncisoReporteCabina getAutoIncisoReporteCabina() {
		return autoIncisoReporteCabina;
	}

	public void setAutoIncisoReporteCabina(
			AutoIncisoReporteCabina autoIncisoReporteCabina) {
		this.autoIncisoReporteCabina = autoIncisoReporteCabina;
	}

	/**
	 * @param incisoMarcaTipo the incisoMarcaTipo to set
	 */
	public void setIncisoMarcaTipo(String incisoMarcaTipo) {
		this.incisoMarcaTipo = incisoMarcaTipo;
	}

	/**
	 * @return the seccionesAuto
	 */
	public Map<String, String> getSeccionesAuto() {
		return seccionesAuto;
	}

	/**
	 * @param seccionesAuto the seccionesAuto to set
	 */
	public void setSeccionesAuto(Map<String, String> seccionesAuto) {
		this.seccionesAuto = seccionesAuto;
	}

	/**
	 * @return the piezasPorSeccion
	 */
	public Map<String, String> getPiezasPorSeccion() {
		return piezasPorSeccion;
	}

	/**
	 * @param piezasPorSeccion the piezasPorSeccion to set
	 */
	public void setPiezasPorSeccion(Map<String, String> piezasPorSeccion) {
		this.piezasPorSeccion = piezasPorSeccion;
	}

	/**
	 * @return the tiposAfectacion
	 */
	public Map<String, String> getTiposAfectacion() {
		return tiposAfectacion;
	}

	/**
	 * @param tiposAfectacion the tiposAfectacion to set
	 */
	public void setTiposAfectacion(Map<String, String> tiposAfectacion) {
		this.tiposAfectacion = tiposAfectacion;
	}

	/**
	 * @return the piezaValuacion
	 */
	public ValuacionReportePieza getPiezaValuacionReporte() {
		return piezaValuacionReporte;
	}

	/**
	 * @param piezaValuacion the piezaValuacion to set
	 */
	public void setPiezaValuacionReporte(ValuacionReportePieza piezaValuacionReporte) {
		this.piezaValuacionReporte = piezaValuacionReporte;
	}

	/**
	 * @return the stringListaPiezasEliminar
	 */
	public String getStringListaPiezasEliminar() {
		return stringListaPiezasEliminar;
	}

	/**
	 * @param stringListaPiezasEliminar the stringListaPiezasEliminar to set
	 */
	public void setStringListaPiezasEliminar(String stringListaPiezasEliminar) {
		this.stringListaPiezasEliminar = stringListaPiezasEliminar;
	}

	/**
	 * @return the eliminarPiezas
	 */
	public Boolean getEliminarPiezas() {
		return eliminarPiezas;
	}

	/**
	 * @param eliminarPiezas the eliminarPiezas to set
	 */
	public void setEliminarPiezas(Boolean eliminarPiezas) {
		this.eliminarPiezas = eliminarPiezas;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the nuevoValorCelda
	 */
	public Double getNuevoValorCelda() {
		return nuevoValorCelda;
	}

	/**
	 * @param nuevoValorCelda the nuevoValorCelda to set
	 */
	public void setNuevoValorCelda(Double nuevoValorCelda) {
		this.nuevoValorCelda = nuevoValorCelda;
	}

	/**
	 * @return the columna
	 */
	public Integer getColumna() {
		return columna;
	}

	/**
	 * @param columna the columna to set
	 */
	public void setColumna(Integer columna) {
		this.columna = columna;
	}

	/**
	 * @return the idPiezaValuacion
	 */
	public Long getIdPiezaValuacion() {
		return idPiezaValuacion;
	}

	/**
	 * @param idPiezaValuacion the idPiezaValuacion to set
	 */
	public void setIdPiezaValuacion(Long idPiezaValuacion) {
		this.idPiezaValuacion = idPiezaValuacion;
	}

	/**
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the valuacionDTO
	 */
	public ValuacionReporteDTO getValuacionDTO() {
		return valuacionDTO;
	}

	/**
	 * @param valuacionDTO the valuacionDTO to set
	 */
	public void setValuacionDTO(ValuacionReporteDTO valuacionDTO) {
		this.valuacionDTO = valuacionDTO;
	}

	/**
	 * @return the idToReporte
	 */
	public Long getIdToReporte() {
		return idToReporte;
	}

	/**
	 * @param idToReporte the idToReporte to set
	 */
	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the terminarReporte
	 */
	public Boolean getTerminarReporte() {
		return terminarReporte;
	}

	/**
	 * @param terminarReporte the terminarReporte to set
	 */
	public void setTerminarReporte(Boolean terminarReporte) {
		this.terminarReporte = terminarReporte;
	}

	/**
	 * @return the mostrarTablaVacia
	 */
	public Boolean getMostrarTablaVacia() {
		return mostrarTablaVacia;
	}

	/**
	 * @param mostrarTablaVacia the mostrarTablaVacia to set
	 */
	public void setMostrarTablaVacia(Boolean mostrarTablaVacia) {
		this.mostrarTablaVacia = mostrarTablaVacia;
	}

	public Integer getTipoValuacion() {
		return tipoValuacion;
	}

	public void setTipoValuacion(Integer tipoValuacion) {
		this.tipoValuacion = tipoValuacion;
	}

	public Long getIdEstimacionCoberturaReporteCabina() {
		return idEstimacionCoberturaReporteCabina;
	}

	public void setIdEstimacionCoberturaReporteCabina(
			Long idEstimacionCoberturaReporteCabina) {
		this.idEstimacionCoberturaReporteCabina = idEstimacionCoberturaReporteCabina;
	}

	public ValuacionHgs getValuacionHgs() {
		return valuacionHgs;
	}

	public void setValuacionHgs(ValuacionHgs valuacionHgs) {
		this.valuacionHgs = valuacionHgs;
	}

	public HgsService getHgsService() {
		return hgsService;
	}

	public void setHgsService(HgsService hgsService) {
		this.hgsService = hgsService;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public String getTipoCalculo() {
		return tipoCalculo;
	}

	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}

	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	public int getPasesAtencionInt() {
		return pasesAtencionInt;
	}

	public void setPasesAtencionInt(int pasesAtencionInt) {
		this.pasesAtencionInt = pasesAtencionInt;
	}

	public Long getIdCoberturaReporteCabina() {
		return idCoberturaReporteCabina;
	}

	public void setIdCoberturaReporteCabina(Long idCoberturaReporteCabina) {
		this.idCoberturaReporteCabina = idCoberturaReporteCabina;
	}

	public Map<String, String> getListaTiposProcesoValuacion() {
		return listaTiposProcesoValuacion;
	}

	public void setListaTiposProcesoValuacion(
			Map<String, String> listaTiposProcesoValuacion) {
		this.listaTiposProcesoValuacion = listaTiposProcesoValuacion;
	}

	public Map<String, String> getListaEstatusValuacion() {
		return listaEstatusValuacion;
	}

	public void setListaEstatusValuacion(Map<String, String> listaEstatusValuacion) {
		this.listaEstatusValuacion = listaEstatusValuacion;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	public EstimacionCoberturaSiniestroService getEstimacionService() {
		return estimacionService;
	}

	public void setEstimacionService(
			EstimacionCoberturaSiniestroService estimacionService) {
		this.estimacionService = estimacionService;
	}
	
	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}
	

	public Short getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(Short tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public Short getEstatusValuacion() {
		return estatusValuacion;
	}

	public void setEstatusValuacion(Short estatusValuacion) {
		this.estatusValuacion = estatusValuacion;
	}

	public Long getIdValuacionHGS() {
		return idValuacionHGS;
	}

	public void setIdValuacionHGS(Long idValuacionHGS) {
		this.idValuacionHGS = idValuacionHGS;
	}

	public Date getFechaIngresoTaller() {
		return fechaIngresoTaller;
	}

	public void setFechaIngresoTaller(Date fechaIngresoTaller) {
		this.fechaIngresoTaller = fechaIngresoTaller;
	}

	public Date getFechaTerminacion() {
		return fechaTerminacion;
	}

	public void setFechaTerminacion(Date fechaTerminacion) {
		this.fechaTerminacion = fechaTerminacion;
	}

	public Long getIdValuador() {
		return idValuador;
	}

	public void setIdValuador(Long idValuador) {
		this.idValuador = idValuador;
	}

	public Date getFechaReingresoReparacion() {
		return fechaReingresoReparacion;
	}

	public void setFechaReingresoReparacion(Date fechaReingresoReparacion) {
		this.fechaReingresoReparacion = fechaReingresoReparacion;
	}

	public Date getUltimoSurtidoRefacciones() {
		return ultimoSurtidoRefacciones;
	}

	public void setUltimoSurtidoRefacciones(Date ultimoSurtidoRefacciones) {
		this.ultimoSurtidoRefacciones = ultimoSurtidoRefacciones;
	}

	public String getNombreValuador() {
		return nombreValuador;
	}

	public void setNombreValuador(String nombreValuador) {
		this.nombreValuador = nombreValuador;
	}

	public BigDecimal getMontoRefacciones() {
		return montoRefacciones;
	}

	public void setMontoRefacciones(BigDecimal montoRefacciones) {
		this.montoRefacciones = montoRefacciones;
	}

	public BigDecimal getMontoManoObra() {
		return montoManoObra;
	}

	public void setMontoManoObra(BigDecimal montoManoObra) {
		this.montoManoObra = montoManoObra;
	}

	public BigDecimal getMontoTotalReparacion() {
		return montoTotalReparacion;
	}

	public void setMontoTotalReparacion(BigDecimal montoTotalReparacion) {
		this.montoTotalReparacion = montoTotalReparacion;
	}
	
	public ValuacionHGSEditDTO getDatosValuacionHgs() {
		return datosValuacionHgs;
	}

	public void setDatosValuacionHgs(ValuacionHGSEditDTO datosValuacionHgs) {
		this.datosValuacionHgs = datosValuacionHgs;
	}
	
	public PrestadorServicioFiltro getFiltroCatalogo() {
		return filtroCatalogo;
	}

	public void setFiltroCatalogo(PrestadorServicioFiltro filtroCatalogo) {
		this.filtroCatalogo = filtroCatalogo;
	}

	public List<String> getListaValuadores() {
		return listaValuadores;
	}

	public void setListaValuadores(List<String> listaValuadores) {
		this.listaValuadores = listaValuadores;
	}

	public List<Map<String, Object>> getListaValuadoresNombres() {
		return listaValuadoresNombres;
	}

	public void setListaValuadoresNombres(
			List<Map<String, Object>> listaValuadoresNombres) {
		this.listaValuadoresNombres = listaValuadoresNombres;
	}

	public String getNombreValuadorHgs() {
		return nombreValuadorHgs;
	}

	public void setNombreValuadorHgs(String nombreValuadorHgs) {
		this.nombreValuadorHgs = nombreValuadorHgs;
	}
	
	public Long getIdValuadorHgs() {
		return idValuadorHgs;
	}

	public void setIdValuadorHgs(Long idValuadorHgs) {
		this.idValuadorHgs = idValuadorHgs;
	}

	public List<PrestadorServicioRegistro> getListaComboValuadores() {
		return listaComboValuadores;
	}

	public void setListaComboValuadores(
			List<PrestadorServicioRegistro> listaComboValuadores) {
		this.listaComboValuadores = listaComboValuadores;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}