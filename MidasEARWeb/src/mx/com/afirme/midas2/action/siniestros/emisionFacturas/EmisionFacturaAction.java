package mx.com.afirme.midas2.action.siniestros.emisionFacturas;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFacturaHistorico;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.dto.emisionFactura.FacturaResultadoDTO;
import mx.com.afirme.midas2.dto.emisionFactura.FiltroFacturaDTO;
import mx.com.afirme.midas2.dto.emisionFactura.IngresoFacturableDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.emisionFacturas.EmisionFacturaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
//@Namespace("/siniestros/recuperacion/ingresos")
@Namespace("/siniestros/emision/factura")
public class EmisionFacturaAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 2584828363828115245L;
	
	private static final String	LOCATION_CONTENEDORFACTURACION_JSP = "/jsp/siniestros/recuperacion/ingresos/contenedorEdicionInformacionFactura.jsp";
	private static final String	LOCATION_CONTENEDORFACTURACION_GRID_JSP = "/jsp/siniestros/recuperacion/ingresos/listadoIngresosFacturablesGrid.jsp";
	private static final String LOCATION_CONTENEDOR_BUSCAR_FACTURAS = "/jsp/siniestros/emisionFacturas/contenedorBusquedaEmisionFacturas.jsp";
	private static final String LOCATION_CONTENEDOR_DETALLE_FACTURA = "/jsp/siniestros/emisionFacturas/contenedorDetalleFactura.jsp";
	private static final String LOCATION_CONTENEDOR_BUSCAR_FACTURAS_GRID = "/jsp/siniestros/emisionFacturas/contenedorBusquedaEmisionFacturasGrid.jsp";
	private static final String LOCATION_CONTENEDOR_CANC_FACTURA = "/jsp/siniestros/emisionFacturas/contenedorCancelacionFacturaPopUp.jsp";
	private static final String LOCATION_CONTENEDOR_HISTORIAL_FACTURAS_GRID = "/jsp/siniestros/emisionFacturas/contenedorHistorialCancelacionGrid.jsp";
	private static final String ORIGEN_BUSQUEDA_INGRESOS = "OBI";
	private static final String ORIGEN_BUSQUEDA_FACTURA  = "OBF";
	private static final String ORIGEN_BUSQUEDA_INGRESOS_ERROR = "OBI";
	private static final String ORIGEN_BUSQUEDA_FACTURA_ERROR  = "OBF";
	
	private static final String PAIS_DEFAULT = "PAMEXI"; 
	
	@Autowired
	@Qualifier("emisionFacturaServiceEJB")
	private EmisionFacturaService emisionFacturaService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;	
	
	private List<IngresoFacturableDTO> listaIngresosFacturables;
	private String filtroIngresos;
	private Map<String,String> listaTiposPersona = null;
	private Map<String,String> listaMetodosPago = null;
	private Map<String,String> estados   = null;
	private Map<String,String> municipios   = null;
	private Map<String,String> listaEstatusRecuperacion;
	private Map<String,String> listaEstatusFactura;
	private Map<String,String> listaEstatusIngreso;
	private Map<String,String> listaMotivosCancelacion;
	private Map<Long,String>   listaOficinas;
	private Map<String, String> colonias = new LinkedHashMap<String, String>();
	//private String 
	
	private Long ingresoIdSelected;
	private EmisionFactura emisionFactura = new EmisionFactura();
	private AutoIncisoReporteCabina autoInciso = new AutoIncisoReporteCabina();
	private List<String> errores = new ArrayList<String>();
	private String estadoName;
	private String listaIngresosAFacturarStr;
	private FiltroFacturaDTO filtroEmisionFactura = new FiltroFacturaDTO();
	private List<FacturaResultadoDTO> lstEmisionFactura = new ArrayList<FacturaResultadoDTO>();
	private TransporteImpresionDTO transporte;
	List<EmisionFacturaHistorico> lstEmisionFacturaHistorico = new ArrayList<EmisionFacturaHistorico>();

	private String nombreRazonSocial;
	private String rfc;
	private String calle;
	private String telefono;
	private String email;
	private String importe;
	private String iva;
	private String totalFactura;
	private String concepto;
	private String estado;
	private String ciudad;
	private String colonia;
	private String cp;
	private String descripcionFacturacion;
	private String ingresoId;
	private String metodoPago;
	private String observacionesFacturacion;
	private String tipoPersona;
	private String digitos;
	private String emisionFacturaId;
	private List<String> listaErroresValidacion;
	private String folioFactura;
	private String destinatariosStr;
	private String observacionesReenvio;
	private Long   emisionId;
	private String motivoCancelacionCtg;
	private String motivoCancelacionTxt;
	private boolean modoConsultar;
	private String tipoRecuperacion;
	private String pantallOrigen = "OBI"; // EL VALOR INDICARA A DONDE REDIRECCIONAR CUANDO SE GENERE LA FACTURA, POR DEFAUL SE CARGA LA DE INGRESOS
	

	@Override
	public void prepare() throws Exception {
		this.listaTiposPersona = emisionFacturaService.getListaTiposPersona();
		this.estados = this.listadoService.getMapEstadosPorPaisMidas(null);
		this.municipios = new HashMap<String, String>();
		this.listaMetodosPago = emisionFacturaService.getListaMetodosPago();
	}
	
	@Action(value = "mostrarIngresosFacturables", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORFACTURACION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORFACTURACION_JSP)})
	public String mostrarIngresosFacturables(){
		return SUCCESS;
	}
	
	@Action(value = "obtenerIngresosFacturables", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORFACTURACION_GRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORFACTURACION_GRID_JSP)})
	public String obtenerIngresosFacturables(){
		listaIngresosFacturables = emisionFacturaService.obtenerIngresosFacturables(filtroIngresos);
		return SUCCESS;
	}

	@Action(value = "obtenerInformacionAsegurado", results = { 
			@Result(name = SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^emisionFactura.*, estadoName",
					"excludeProperties",
					"^emisionFactura\\.ingreso,^emisionFactura\\.siniestroCabina,^emisionFactura\\.lstEmisionFacturaHistorico"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","ingresoIdSelected"})})
	public String obtenerInformacionAsegurado(){
		setEmisionFactura((emisionFacturaService.obtenerInformacionAsegurado(ingresoIdSelected)));
		if(getEmisionFactura().getTipoPersona().equals(EmisionFactura.TipoPersona.ASEGURADO.getValue())){
			setEstadoName(estados.get(getEmisionFactura().getEstado()));
		}
		return SUCCESS;
	}
	
	@Action(value = "obtenerInformacionProveedor", results = { 
			@Result(name = SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^emisionFactura.*, estadoName",
					"excludeProperties",
					"^emisionFactura\\.ingreso,^emisionFactura\\.siniestroCabina,^emisionFactura\\.lstEmisionFacturaHistorico"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","ingresoIdSelected"})})
	public String obtenerInformacionProveedor(){
		setEmisionFactura((emisionFacturaService.obtenerInformacionProveedor(ingresoIdSelected)));
		return SUCCESS;
	}
	
	@Action(value = "obtenerInformacionFactura", results = { 
			@Result(name = SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^emisionFactura.*",
					"excludeProperties",
					"^emisionFactura\\.ingreso,^emisionFactura\\.siniestroCabina,^emisionFactura\\.lstEmisionFacturaHistorico"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","ingresoIdSelected, emisionFacturaId"})})
	public String obtenerInformacionFactura(){
		
		this.emisionFactura = emisionFacturaService.obtenerInformacionFactura(ingresoIdSelected);
		if( this.emisionFactura != null){
			this.tipoRecuperacion = this.emisionFactura.getIngreso().getRecuperacion().getTipo();
		}
		
		return SUCCESS;
	}
	
	@Action(value = "obtenerEmisionFactura", results = { 
			@Result(name = SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^emisionFactura.*","excludeProperties","^emisionFactura\\.ingreso,^emisionFactura\\.siniestroCabina,^emisionFactura\\.lstEmisionFacturaHistorico"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","ingresoIdSelected"})})
	public String obtenerEmisionFactura(){
		this.emisionFactura = emisionFacturaService.obtenerEmisionFacturaByIngresoId(ingresoIdSelected);
		if( this.emisionFactura != null){
			this.tipoRecuperacion = this.emisionFactura.getIngreso().getRecuperacion().getTipo();
		}
		
		return SUCCESS;
	}
	
	@Action(value = "obtenerEmisionFacturaByEmision", results = { 
			@Result(name = SUCCESS, type="json",params=
					{"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^emisionFactura\\.id,^emisionFactura\\.nombreRazonSocial,^emisionFactura\\.id,^emisionFactura\\.rfc,^emisionFactura\\.calle,^emisionFactura\\.cp,^emisionFactura\\.telefono,^emisionFactura\\.email,^emisionFactura\\.estadoMidas\\.id,^emisionFactura\\.ciudadMidas\\.id,^emisionFactura\\.coloniaMidas\\.id",
					"excludeProperties",
					"^emisionFactura\\.ingreso,^emisionFactura\\.siniestroCabina,^emisionFactura\\.lstEmisionFacturaHistorico"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","emisionId"})})
	public String obtenerEmisionFacturaByEmision(){
		setEmisionFactura(emisionFacturaService.obtenerEmisionFactura(emisionId));
		return SUCCESS;
	}
	
	@Action(value = "guardarInformacionFactura", results = { 
			@Result(name = SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","errores.*, emisionFacturaId"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^emisionFactura.*"})})
	public String guardarInformacionFactura(){
		if(emisionFacturaId == null || emisionFacturaId.equals("")){
			emisionFactura = new EmisionFactura();
		}
		else{
			emisionFactura = emisionFacturaService.obtenerEmisionFactura(Long.valueOf(emisionFacturaId));
		}
		emisionFactura.setNombreRazonSocial(nombreRazonSocial);
		emisionFactura.setRfc(rfc);
		emisionFactura.setCalle(calle);
		emisionFactura.setTelefono(telefono);
		emisionFactura.setEmail(email);
		emisionFactura.setEstadoMidas(this.entidadService.findById(EstadoMidas.class, estado));
		emisionFactura.setCiudadMidas(this.entidadService.findById(CiudadMidas.class, ciudad));
		emisionFactura.setColoniaMidas(this.entidadService.findById(ColoniaMidas.class, colonia));
		emisionFactura.setCp(cp);
		emisionFactura.setConcepto(concepto);
		emisionFactura.setDescripcionFacturacion(descripcionFacturacion);
		emisionFactura.setMetodoPago(metodoPago);
		emisionFactura.setIngreso(emisionFacturaService.getIngresoById(Long.valueOf(ingresoId)));
		//emisionFactura.setIngresoId(Long.valueOf(ingresoId));
		emisionFactura.setObservacionesFacturacion(observacionesFacturacion);
		emisionFactura.setEstatus("TRAMITE");
		emisionFactura.setTipoPersona(tipoPersona);
		if(digitos != null && !digitos.trim().equals(""))
			emisionFactura.setDigitos(Long.valueOf(digitos));
		if(importe != null && !importe.trim().equals(""))
			emisionFactura.setImporte(new BigDecimal(importe));
		if(iva != null && !iva.trim().equals("")){
			emisionFactura.setIva(new BigDecimal(iva));
		}else{
			emisionFactura.setIva(new BigDecimal("0.0"));
		}
		if(totalFactura != null && !totalFactura.trim().equals(""))
			emisionFactura.setTotalFactura(new BigDecimal(totalFactura));
		emisionFactura.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		errores = emisionFacturaService.guardarInformacionFactura(emisionFactura);
		if(errores.size() == 1 && errores.get(0).startsWith("ID")){
			String id = errores.get(0).substring(3, errores.get(0).length());
			emisionFactura.setId(Long.valueOf(id));
			emisionFacturaId = String.valueOf(emisionFactura.getId());
		}else{
			if(errores.size() == 0){
				System.out.println("NO SE OBTUVO EL ID");
			}else{
				System.out.println("CON ERRORES");
			}
		}
		return SUCCESS;
	}
	
@Action(value="generarFacturaElectronica",results = { 
			@Result(name = ORIGEN_BUSQUEDA_INGRESOS, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/emision/factura", 			
					"mensaje", "${mensaje}",		
					"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = ORIGEN_BUSQUEDA_FACTURA, type = "redirectAction", params = { 
					"actionName", "mostrarDetalleFactura", 
					"namespace", "/siniestros/emision/factura", 			
					"emisionId", "${emisionId}"}),
			@Result(name = ORIGEN_BUSQUEDA_INGRESOS_ERROR, type = "redirectAction", params = { 
					"actionName", "mostrarIngresosFacturables", 
					"namespace", "/siniestros/emision/factura",	
					"filtroIngresos", "${listaIngresosAFacturarStr}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = ORIGEN_BUSQUEDA_FACTURA_ERROR, type = "redirectAction", params = { 
					"actionName", "mostrarDetalleFactura", 
					"namespace", "/siniestros/emision/factura",	
					"emisionId", "${emisionId}"})
	})
	public String generarFacturaElectronica(){
		String success = "",input="";
		try{
			emisionFacturaService.generarFacturaElectronica(listaIngresosAFacturarStr);	
			this.setMensajeExito();
			
			if( this.pantallOrigen.equals(this.ORIGEN_BUSQUEDA_FACTURA) ){
				success = this.ORIGEN_BUSQUEDA_FACTURA;
				this.emisionId = new Long(listaIngresosAFacturarStr);
			}else{
				success = this.ORIGEN_BUSQUEDA_INGRESOS;
			}
			
			return success;
		}catch(Exception e){
			
			super.setMensajeError("Error al generar la factura");
			
			if( this.pantallOrigen.equals(this.ORIGEN_BUSQUEDA_FACTURA) ){
				input = this.ORIGEN_BUSQUEDA_FACTURA_ERROR;
				this.emisionId = new Long(listaIngresosAFacturarStr);
			}else{
				input = this.ORIGEN_BUSQUEDA_INGRESOS_ERROR;
			}
			
			return input;
		}
		
		
	}

	@Action(value="mostrarDetalleError",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/ingresos/detalleMensajeEmisionFactura.jsp")
	})	
	public String obtenerDetalleDeError() {		
		
		listaErroresValidacion = emisionFacturaService.obtenerDetalleError(folioFactura);	
				
		return SUCCESS;
	}
	
	@Action(value="mostrarReenvioFactura",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/emisionFacturas/contenedorReenvioFactura.jsp")
	})	
	public String mostrarReenvioFactura() {		
						
		return SUCCESS;
	}
	
	@Action(value="reenviarFactura",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","emisionFactura.id,tipoMensaje, mensaje"})				
	})
	public String reenviarFactura(){
		
		try{
			emisionFacturaService.enviarFactura(emisionFactura.getId(),destinatariosStr,observacionesReenvio);
			this.setMensajeExito();
		}catch(Exception e)
		{
			this.setMensajeError("Ha ocurrido un error al intentar enviar la Factura.");		
			LOG.error(e.getMessage());
		}
		
		return SUCCESS;
	}
	
	@Action(value = "mostrarPantallaPDF", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachtment;filename=\"${transporte.fileName}\""})
	})
	public String mostrarPantallaPDF(){
		   
		emisionFactura = entidadService.findById(EmisionFactura.class, emisionFactura.getId());
		
		transporte = emisionFacturaService.obtenerPdf(emisionFactura.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");	
		
		String fileName = "Factura_"+ emisionFactura.getFolioFactura() + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
		transporte.setFileName(fileName);
		
		return SUCCESS;
	}
	
	public void prepareContenedorBusquedaEmisionFactura(){
		
		listaEstatusRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECUPERACION);
		listaEstatusFactura      = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.EMISION_FACTURA_ESTATUS);
		listaOficinas            = listadoService.obtenerOficinasSiniestros();
		listaEstatusIngreso      = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_INGRESO);
		
	}
	
	@Action(value="contenedorBusquedaEmisionFactura", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDOR_BUSCAR_FACTURAS),
			@Result(name = INPUT,location=LOCATION_CONTENEDOR_BUSCAR_FACTURAS)
	})		
	public String contenedorBusquedaEmisionFactura(){
		
		return SUCCESS;
	}
	
	
	@Action(value="buscarEmisionFacturasGrid", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDOR_BUSCAR_FACTURAS_GRID),
			@Result(name = INPUT,location=LOCATION_CONTENEDOR_BUSCAR_FACTURAS_GRID)
	})		
	public String buscarFacturasGrid(){
	
		lstEmisionFactura = this.emisionFacturaService.buscarFacturas(filtroEmisionFactura);
		
		return SUCCESS;
	}
	
	@Action(value="exportarExcelEmisionFacturas",
			results={@Result(name=SUCCESS,
					  type="stream",
					  params={"contentType","${transporte.contentType}",
						  "inputName","transporte.genericInputStream",
						  "contentDisposition","attachment;filename=\"${transporte.fileName}\""})}) 
	public String exportarExcelEmisionFacturas(){	
		
		lstEmisionFactura = this.emisionFacturaService.buscarFacturas(filtroEmisionFactura);
		
		if( lstEmisionFactura != null ){
			ExcelExporter exporter = new ExcelExporter( FacturaResultadoDTO.class );
			transporte = exporter.exportXLS(lstEmisionFactura, "Emisión de Facturas");

		}
		return SUCCESS;	
	}	
	
	
	public void prepareMostrarDetalleFactura(){
		this.listaMetodosPago = emisionFacturaService.getListaMetodosPago();
		estados = listadoService.getMapEstadosPorPaisMidas(PAIS_DEFAULT);
	}
	
	@Action(value="mostrarDetalleFactura", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDOR_DETALLE_FACTURA),
			@Result(name = INPUT,location=LOCATION_CONTENEDOR_DETALLE_FACTURA)
	})		
	public String mostrarDetalleFactura(){
	
		this.emisionFactura = this.emisionFacturaService.obtenerEmisionFactura(this.emisionId);
		if(this.emisionFactura != null && this.emisionFactura.getCiudadMidas() != null && this.emisionFactura.getCiudadMidas().getEstado() != null){
			municipios = listadoService.getMapMunicipiosPorEstadoMidas( this.emisionFactura.getEstadoMidas().getId()	 );
			colonias   = listadoService.getMapColoniasPorCiudadMidas(this.emisionFactura.getCiudadMidas().getId());
		}
		
		return SUCCESS;
	}
	
	
	@Action(value="cancelarFactura", results={
			@Result(name=SUCCESS,type = "redirectAction", params = { 
					"actionName", "mostrarDetalleFactura", 
					"namespace", "/siniestros/emision/factura", 			
					"emisionId", "${emisionId}"}),
			@Result(name = INPUT,type = "redirectAction", params = { 
					"actionName", "mostrarDetalleFactura", 
					"namespace", "/siniestros/emision/factura", 			
					"emisionId", "${emisionId}"})
	})	
	public String cancelarFactura(){
	
		try{
			this.emisionFacturaService.cancelaFactura(this.emisionId, motivoCancelacionCtg,motivoCancelacionTxt);
			super.setMensajeExito();
			return SUCCESS;
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(ex.getMessage() != null ? 
					ex.getMessage() : ""));
			return INPUT;
		}
	}
	
	
	public void prepareMuestraCancelarFacturaMotivos(){
		this.listaMotivosCancelacion = this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.EMISION_FACTURA_CANCELACION );
	}
	
	@Action(value="muestraCancelarFacturaMotivos", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDOR_CANC_FACTURA),
			@Result(name = INPUT,location=LOCATION_CONTENEDOR_CANC_FACTURA)
	})		
	public String muestraCancelarFacturaMotivos(){
		return SUCCESS;
	}
	
	@Action(value="modificarFacturaEmision", results={
		@Result(name = SUCCESS, type = "redirectAction", params = { 
				"actionName", "mostrarDetalleFactura", 
				"namespace", "/siniestros/emision/factura", 			
				"emisionId", "${emisionId}"}),
		@Result(name = INPUT, type = "redirectAction", params = { 
				"actionName", "mostrarDetalleFactura", 
				"namespace", "/siniestros/emision/factura", 			
				"emisionId", "${emisionId}"})
	})	
	public String modificarFacturaEmision(){
		
		try{
			this.emisionFactura.setId(this.emisionId);
			this.emisionFacturaService.actualizarDatosEmisionFactura(this.emisionFactura);
			super.setMensajeExito();
			return SUCCESS;
		}catch(Exception e){
			super.setMensajeError("Error al modificar la factura");
			return INPUT;
		}
		
	}
	
	
	
	@Action(value="listarMovimientosHistoricos", results={
			@Result(name=SUCCESS,location=LOCATION_CONTENEDOR_HISTORIAL_FACTURAS_GRID),
			@Result(name = INPUT,location=LOCATION_CONTENEDOR_HISTORIAL_FACTURAS_GRID)
	})	
	public String listarMovimientosHistoricos(){
		
		this.lstEmisionFacturaHistorico =  this.emisionFacturaService.obtenerHistoricoEmisionFactura(this.emisionId);
		
		return SUCCESS;
	}
	
	
	@Action(value = "obtenerDatosVehiculoSalvamento", results = { 
			@Result(name = SUCCESS, type="json",params=
					{"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^autoInciso\\.descColor,^emisionFactura\\.descEstilo,^autoInciso\\.descMarca,^autoInciso\\.descTipoUso,^autoInciso\\.numeroSerie,^autoInciso\\.numeroMotor,^autoInciso\\.modeloVehiculo",
					"excludeProperties",
					"^autoInciso\\.incisoReporteCabina"}),
			@Result(name = INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","emisionId"})})
	public String obtenerDatosVehiculoSalvamento(){
		
		this.autoInciso = this.emisionFacturaService.obtenerDatosVehiculoSalvamentoByIngreso(ingresoIdSelected);
		
		return SUCCESS;
	}
	
	public void setListaIngresosFacturables(List<IngresoFacturableDTO> listaIngresosFacturables) {
		this.listaIngresosFacturables = listaIngresosFacturables;
	}

	public List<IngresoFacturableDTO> getListaIngresosFacturables() {
		return listaIngresosFacturables;
	}

	/**
	 * @param filtroIngresos the filtroIngresos to set
	 */
	public void setFiltroIngresos(String filtroIngresos) {
		this.filtroIngresos = filtroIngresos;
	}

	/**
	 * @return the filtroIngresos
	 */
	public String getFiltroIngresos() {
		return filtroIngresos;
	}

	/**
	 * @param listaTiposPersona the listaTiposPersona to set
	 */
	public void setListaTiposPersona(Map<String,String> listaTiposPersona) {
		this.listaTiposPersona = listaTiposPersona;
	}

	/**
	 * @return the listaTiposPersona
	 */
	public Map<String,String> getListaTiposPersona() {
		return listaTiposPersona;
	}

	/**
	 * @param estados the estados to set
	 */
	public void setEstados(Map<String,String> estados) {
		this.estados = estados;
	}

	/**
	 * @return the estados
	 */
	public Map<String,String> getEstados() {
		return estados;
	}

	/**
	 * @param municipios the municipios to set
	 */
	public void setMunicipios(Map<String,String> municipios) {
		this.municipios = municipios;
	}

	/**
	 * @return the municipios
	 */
	public Map<String,String> getMunicipios() {
		return municipios;
	}

	/**
	 * @param ingresoIdSelected the ingresoIdSelected to set
	 */
	public void setIngresoIdSelected(Long ingresoIdSelected) {
		this.ingresoIdSelected = ingresoIdSelected;
	}

	/**
	 * @return the ingresoIdSelected
	 */
	public Long getIngresoIdSelected() {
		return ingresoIdSelected;
	}

	/**
	 * @param estadoName the estadoName to set
	 */
	public void setEstadoName(String estadoName) {
		this.estadoName = estadoName;
	}

	/**
	 * @return the estadoName
	 */
	public String getEstadoName() {
		return estadoName;
	}

	/**
	 * @param listaMetodosPago the listaMetodosPago to set
	 */
	public void setListaMetodosPago(Map<String,String> listaMetodosPago) {
		this.listaMetodosPago = listaMetodosPago;
	}

	/**
	 * @return the listaMetodosPago
	 */
	public Map<String,String> getListaMetodosPago() {
		return listaMetodosPago;
	}

	/**
	 * @param errores the errores to set
	 */
	public void setErrores(List<String> errores) {
		this.errores = errores;
	}

	/**
	 * @return the errores
	 */
	public List<String> getErrores() {
		return errores;
	}
	/**
	 * @param nombreRazonSocial the nombreRazonSocial to set
	 */
	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}
	/**
	 * @return the nombreRazonSocial
	 */
	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String getRfc() {
		return rfc;
	}
	
	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	public String getCalle() {
		return calle;
	}

	
	public void setEmisionFactura(EmisionFactura emisionFactura) {
		this.emisionFactura = emisionFactura;
	}

	public EmisionFactura getEmisionFactura() {
		return emisionFactura;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstado() {
		return estado;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getColonia() {
		return colonia;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getCp() {
		return cp;
	}

	public void setDescripcionFacturacion(String descripcionFacturacion) {
		this.descripcionFacturacion = descripcionFacturacion;
	}

	public String getDescripcionFacturacion() {
		return descripcionFacturacion;
	}

	public void setIngresoId(String ingresoId) {
		this.ingresoId = ingresoId;
	}

	public String getIngresoId() {
		return ingresoId;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getImporte() {
		return importe;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getIva() {
		return iva;
	}

	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}

	public String getMetodoPago() {
		return metodoPago;
	}

	public void setObservacionesFacturacion(String observacionesFacturacion) {
		this.observacionesFacturacion = observacionesFacturacion;
	}

	public String getObservacionesFacturacion() {
		return observacionesFacturacion;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTotalFactura(String totalFactura) {
		this.totalFactura = totalFactura;
	}

	public String getTotalFactura() {
		return totalFactura;
	}

	/**
	 * @param digitos the digitos to set
	 */
	public void setDigitos(String digitos) {
		this.digitos = digitos;
	}

	/**
	 * @return the digitos
	 */
	public String getDigitos() {
		return digitos;
	}

	/**
	 * @param emisionFacturaId the emisionFacturaId to set
	 */
	public void setEmisionFacturaId(String emisionFacturaId) {
		this.emisionFacturaId = emisionFacturaId;
	}

	/**
	 * @return the emisionFacturaId
	 */
	public String getEmisionFacturaId() {
		return emisionFacturaId;
	}

	public FiltroFacturaDTO getFiltroEmisionFactura() {
		return filtroEmisionFactura;
	}

	public void setFiltroEmisionFactura(FiltroFacturaDTO filtroEmisionFactura) {
		this.filtroEmisionFactura = filtroEmisionFactura;
	}

	public Map<String, String> getListaEstatusIngreso() {
		return listaEstatusIngreso;
	}

	public void setListaEstatusIngreso(Map<String, String> listaEstatusIngreso) {
		this.listaEstatusIngreso = listaEstatusIngreso;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getListaEstatusFactura() {
		return listaEstatusFactura;
	}

	public void setListaEstatusFactura(Map<String, String> listaEstatusFactura) {
		this.listaEstatusFactura = listaEstatusFactura;
	}

	public Map<Long, String> getListaOficinas() {
		return listaOficinas;
	}

	public void setListaOficinas(Map<Long, String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

	public List<FacturaResultadoDTO> getLstEmisionFactura() {
		return lstEmisionFactura;
	}

	public void setLstEmisionFactura(List<FacturaResultadoDTO> lstEmisionFactura) {
		this.lstEmisionFactura = lstEmisionFactura;
	}

	public Map<String, String> getListaEstatusRecuperacion() {
		return listaEstatusRecuperacion;
	}

	public void setListaEstatusRecuperacion(
			Map<String, String> listaEstatusRecuperacion) {
		this.listaEstatusRecuperacion = listaEstatusRecuperacion;
	}

	public String getListaIngresosAFacturarStr() {
		return listaIngresosAFacturarStr;
	}

	public void setListaIngresosAFacturarStr(String listaIngresosAFacturarStr) {
		this.listaIngresosAFacturarStr = listaIngresosAFacturarStr;
	}

	public List<String> getListaErroresValidacion() {
		return listaErroresValidacion;
	}

	public void setListaErroresValidacion(List<String> listaErroresValidacion) {
		this.listaErroresValidacion = listaErroresValidacion;
	}

	public String getFolioFactura() {
		return folioFactura;
	}

	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}
	
		public String getDestinatariosStr() {
		return destinatariosStr;
	}

	public void setDestinatariosStr(String destinatariosStr) {
		this.destinatariosStr = destinatariosStr;
	}

	public String getObservacionesReenvio() {
		return observacionesReenvio;
	}

	public void setObservacionesReenvio(String observacionesReenvio) {
		this.observacionesReenvio = observacionesReenvio;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public Long getEmisionId() {
		return emisionId;
	}

	public void setEmisionId(Long emisionId) {
		this.emisionId = emisionId;
	}

	public Map<String, String> getListaMotivosCancelacion() {
		return listaMotivosCancelacion;
	}

	public void setListaMotivosCancelacion(
			Map<String, String> listaMotivosCancelacion) {
		this.listaMotivosCancelacion = listaMotivosCancelacion;
	}

	public String getMotivoCancelacionCtg() {
		return motivoCancelacionCtg;
	}

	public void setMotivoCancelacionCtg(String motivoCancelacionCtg) {
		this.motivoCancelacionCtg = motivoCancelacionCtg;
	}

	public String getMotivoCancelacionTxt() {
		return motivoCancelacionTxt;
	}

	public void setMotivoCancelacionTxt(String motivoCancelacionTxt) {
		this.motivoCancelacionTxt = motivoCancelacionTxt;
	}

	public boolean isModoConsultar() {
		return modoConsultar;
	}

	public void setModoConsultar(boolean modoConsultar) {
		this.modoConsultar = modoConsultar;
	}

	public Map<String, String> getColonias() {
		return colonias;
	}

	public void setColonias(Map<String, String> colonias) {
		this.colonias = colonias;
	}

	public List<EmisionFacturaHistorico> getLstEmisionFacturaHistorico() {
		return lstEmisionFacturaHistorico;
	}

	public void setLstEmisionFacturaHistorico(
			List<EmisionFacturaHistorico> lstEmisionFacturaHistorico) {
		this.lstEmisionFacturaHistorico = lstEmisionFacturaHistorico;
	}

	public String getTipoRecuperacion() {
		return tipoRecuperacion;
	}

	public void setTipoRecuperacion(String tipoRecuperacion) {
		this.tipoRecuperacion = tipoRecuperacion;
	}

	public AutoIncisoReporteCabina getAutoInciso() {
		return autoInciso;
	}

	public void setAutoInciso(AutoIncisoReporteCabina autoInciso) {
		this.autoInciso = autoInciso;
	}

	public String getPantallOrigen() {
		return pantallOrigen;
	}

	public void setPantallOrigen(String pantallOrigen) {
		this.pantallOrigen = pantallOrigen;
	}




}