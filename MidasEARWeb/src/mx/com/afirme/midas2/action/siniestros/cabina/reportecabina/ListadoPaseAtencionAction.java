package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionExportacionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/reportecabina")
public class ListadoPaseAtencionAction extends BaseAction implements Preparable {

	/**
	 * @author eftregue 
	 */
	private static final long serialVersionUID = 1L;
	
	private PaseAtencionSiniestroDTO paseAtencion;
	private Long idEstimacionCobertura;
	private List<PaseAtencionSiniestroDTO> pasesAtencion = new ArrayList<PaseAtencionSiniestroDTO>();
	private List<PaseAtencionExportacionDTO> pasesAtencionExportacion = new ArrayList<PaseAtencionExportacionDTO>();
	private Map<String,String> tipoPases = new HashMap<String, String>();
	private Map<String,String> tiposPaseAtencion = new HashMap<String, String>();
	private TransporteImpresionDTO transporte = new TransporteImpresionDTO();
	private Boolean isFilterEmpty = false;
	private String isListadoPases;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	@Override
	public void prepare(){
		this.tipoPases = listadoService.obtenerTiposPaseAtencion();
		this.tiposPaseAtencion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);
	}
	
	@Action(value = "mostrarListadoPasesAtencion", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/pasesatencion/listadoPaseAtencion.jsp"))
	public String mostrarListadoPasesAtencion(){
		isListadoPases = "1";
		return SUCCESS;
	}
	
	@Action(value = "buscarPasesDeAtencion", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/pasesatencion/listadoPaseAtencionGrid.jsp"))
	public String buscarPasesDeAtencion(){
		if( paseAtencion != null ){
			pasesAtencion = estimacionCoberturaSiniestroService.buscarPasesAtencion( paseAtencion );
		}else{
			pasesAtencion = new ArrayList<PaseAtencionSiniestroDTO>();
		}
		return SUCCESS;
	}
	
	@Action(value="imprimirPaseAtencion",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirPaseAtencion(){
		
		transporte = estimacionCoberturaSiniestroService.imprimirPaseAtencion( idEstimacionCobertura );
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");
		EstimacionCoberturaReporteCabina estimacion = estimacionCoberturaSiniestroService.obtenerEstimacionCoberturaReporteCabina( idEstimacionCobertura );

		String tipoCalculo = estimacion.getCoberturaReporteCabina().getClaveTipoCalculo();
		String tipoEstimacion = estimacion.getTipoEstimacion();
		
		String fileName = tipoCalculo+"_"+"_"+tipoEstimacion+estimacion.getFolio()+"_"+".pdf";
		transporte.setFileName(fileName);
		
		return SUCCESS;
	}

	@Action(value="exportarPaseAtencion",
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Listado Pases Atencion", "xls"})},
			results={@Result(name=SUCCESS,
					type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition",
					"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarPaseAtencion(){
		pasesAtencionExportacion = estimacionCoberturaSiniestroService.obtenerPaseAtencionExportacion( paseAtencion );
		ExcelExporter exporter = new ExcelExporter( PaseAtencionExportacionDTO.class );
		
		transporte = exporter.exportXLS(pasesAtencionExportacion, "Listado Pases Atencion");
				
		return SUCCESS;
	}
	
	
	@Action(value = "mostrarPasesDeAtencionDeReporte", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/pasesatencion/listadoPaseAtencionDeReporte.jsp"))
	public String mostrarPasesDeAtencionDeReporte(){
		return SUCCESS;
	}
	
	
/*	----------------------------------------------------------------------------------------------------
-------------------------                 Sets & Gets              ------------------------------------- 
------------------------------------------------------------------------------------------------------- */
	
	public PaseAtencionSiniestroDTO getPaseAtencion() {
		return paseAtencion;
	}

	public void setPaseAtencion(PaseAtencionSiniestroDTO paseAtencion) {
		this.paseAtencion = paseAtencion;
	}

	public List<PaseAtencionSiniestroDTO> getPasesAtencion() {
		return pasesAtencion;
	}

	public void setPasesAtencion(List<PaseAtencionSiniestroDTO> pasesAtencion) {
		this.pasesAtencion = pasesAtencion;
	}

	public Map<String, String> getTipoPases() {
		return tipoPases;
	}

	public void setTipoPases(Map<String, String> tipoPases) {
		this.tipoPases = tipoPases;
	}

	/**
	 * @return the tiposPaseAtencion
	 */
	public Map<String, String> getTiposPaseAtencion() {
		return tiposPaseAtencion;
	}

	/**
	 * @param tiposPaseAtencion the tiposPaseAtencion to set
	 */
	public void setTiposPaseAtencion(Map<String, String> tiposPaseAtencion) {
		this.tiposPaseAtencion = tiposPaseAtencion;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
		return estimacionCoberturaSiniestroService;
	}

	public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}

	public List<PaseAtencionExportacionDTO> getPasesAtencionExportacion() {
		return pasesAtencionExportacion;
	}

	public void setPasesAtencionExportacion(
			List<PaseAtencionExportacionDTO> pasesAtencionExportacion) {
		this.pasesAtencionExportacion = pasesAtencionExportacion;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	public Long getIdEstimacionCobertura() {
		return idEstimacionCobertura;
	}

	public void setIdEstimacionCobertura(Long idEstimacionCobertura) {
		this.idEstimacionCobertura = idEstimacionCobertura;
	}

	public Boolean getIsFilterEmpty() {
		return isFilterEmpty;
	}

	public void setIsFilterEmpty(Boolean isFilterEmpty) {
		this.isFilterEmpty = isFilterEmpty;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the isListadoPases
	 */
	public String getIsListadoPases() {
		return isListadoPases;
	}

	/**
	 * @param isListadoPases the isListadoPases to set
	 */
	public void setIsListadoPases(String isListadoPases) {
		this.isListadoPases = isListadoPases;
	}

}
