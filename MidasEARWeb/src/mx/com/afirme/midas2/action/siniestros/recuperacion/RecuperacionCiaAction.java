package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania.EstatusCartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ComplementoCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.OrdenCompraCartaCia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.SeguimientoRecuperacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionRelacionSiniestroDTO;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:43 p.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/recuperacionCia")
public class RecuperacionCiaAction  extends RecuperacionAction{
	private static final long serialVersionUID = 8691256138263690064L;
	private final String 	CONTENEDOR_RECUPERACION_CIA	 =	"/jsp/siniestros/recuperacion/compania/contenedorRecuperacionCia.jsp";
	private final String 	DETALLE_RECUPERACION_CIA	 =	"/jsp/siniestros/recuperacion/compania/detalleRecuperacionCia.jsp";
	private final String 	ORDEN_COMPRA_GRID	 =	"/jsp/siniestros/recuperacion/compania/ordenCompraCiaGrid.jsp";
	private final String 	SEGUIMIENTOS_GRID	 =	"/jsp/siniestros/recuperacion/compania/seguimientoRecuperacionCiaGrid.jsp";
	private final String 	TIPO_PASE_CIA	 =	"/jsp/siniestros/recuperacion/compania/tipoPaseAtnCia.jsp";
	private final String 	IMPRESION_REDOC_CIA	 =	"/jsp/siniestros/recuperacion/compania/impresionRedoc.jsp";
	private final String 	ENTREGA_CIA	 =	"/jsp/siniestros/recuperacion/compania/entregaCopiaCia.jsp";
	private final String 	ALERTA_CIA_GRID	 ="/jsp/siniestros/recuperacion/compania/alertaCIAGrid.jsp";
	private final String 	ALERTA_CIA= "/jsp/siniestros/recuperacion/compania/contenedorAlertaCIA.jsp";
	private final String 	CANCELACION_CIA = "/jsp/siniestros/recuperacion/compania/cancelacionRecuperacionCIa.jsp";
	private final String 	SOLICITAR_CANCELA_CIA = "/jsp/siniestros/recuperacion/compania/solicitarCancelacionCIA.jsp";
	private final String 	REDOCUMENTACIONCIA = "/jsp/siniestros/recuperacion/compania/redocumentacionCia.jsp";

	private final String 	REDOCUMENTACIONCIA_GRID = "/jsp/siniestros/recuperacion/compania/cartasRedocumentadasGrid.jsp";
	
	/**TODO CREAR EL JSP complementarRecuperacion.jsp**/
	private final String 	COMPLEMENTAR_RECUPERACION ="/jsp/siniestros/recuperacion/compania/complementarRecuperacion.jsp";
	
	
	private final String ROL_ANALISTARECUPERACIONES= "Rol_M2_Analista_Recuperaciones_Plaza";
	@Autowired
	@Qualifier("recuperacionCiaServiceEJB")
	private RecuperacionCiaService recuperacionCiaService;
	private RecuperacionRelacionSiniestroDTO infoAfectacion = new  RecuperacionRelacionSiniestroDTO();
	private RecuperacionRelacionSiniestroDTO infoImportes = new  RecuperacionRelacionSiniestroDTO();
	private CartaCompania cartaCia=new CartaCompania();
	private CartaCompania cartaRedocumentacion=new CartaCompania();
	private CartaCompania cartaCiaActiva=new CartaCompania();
	private CartaCompania primeraCarta=new CartaCompania();
	private List<CartaCompania> lstCartaCompania;

	private String comentarioSeguimiento;
	private RecuperacionRelacionSiniestroDTO datosEstimacion = new  RecuperacionRelacionSiniestroDTO();
	private Integer porcentajeParticipacion;
	private String estatusCarta;
	private Date fechaAcuse;
	private Map<String,String> lstTipoRedocumentacion= new LinkedHashMap<String, String>();

	private Map<String,String> lstCausaRechazo= new LinkedHashMap<String, String>();
	private Map<String,String> lstTipoPaseCia= new LinkedHashMap<String, String>();
	private String tipoPaseCia;
	private Map<String,String> lstCausasNoElaboracion= new LinkedHashMap<String, String>();
	private Map<String,String> lstCausas =new LinkedHashMap<String, String>();
	private Map<String,String> lstEstatusCarta =new LinkedHashMap<String, String>();
	private Map<String,String> lstEstatusCartaRedoc =new LinkedHashMap<String, String>();
	
	private Map<String,String> lstMotivoExclusion =new LinkedHashMap<String, String>();
	private Map<String,String> lstMotivosCanc =new LinkedHashMap<String, String>();
	private Map<Long, String> lstCiaDeSeguros =new LinkedHashMap<Long, String>();
	private List<OrdenCompraCartaCia> lstOrdenesCompra;
	private List<SeguimientoRecuperacion> lstSeguimientos;
	private Map<String,String> lstTipoRechazo;
	private Map<String,String> lstTipoRechazoRedocumentacion;
	private String ordenesCiaConcat="";
	private RecuperacionCompania recuperacion = new RecuperacionCompania();
	private List<RecuperacionCompania> recuperacionesVencer;
	private String tipoRedocumentacion;
	private Long prestadorServicioId;
	private String methodName;
	private String termioAjuste;
	private String url;
	private Long idCobertura; 
	private Long idPaseAtencion;
	boolean tienePermisoSolCancela;
	private String currentUser;
	private String userName;
	private String creationDateTxt;
	boolean verRedocumentacion = false; 
	ComplementoCompania complementoCompania = new ComplementoCompania();
	boolean tieneComplemento =false;
	private TransporteImpresionDTO transporte;
	private Boolean impresionPreparada;
	private String nombreCia;
	//para cartas migradas
	private String validaExclusionRechazo = "n";
	private String codeUserCartasMigradas;
	//Reserva
	private BigDecimal reservaDisponible;
	
	public String getCodeUserCartasMigradas() {
		return codeUserCartasMigradas;
	}

	public void setCodeUserCartasMigradas(String codeUserCartasMigradas) {
		this.codeUserCartasMigradas = codeUserCartasMigradas;
	}

	public Date getFechaAcuseCartasMigradas() {
		return fechaAcuseCartasMigradas;
	}

	public void setFechaAcuseCartasMigradas(Date fechaAcuseCartasMigradas) {
		this.fechaAcuseCartasMigradas = fechaAcuseCartasMigradas;
	}

	private Date fechaAcuseCartasMigradas;
	
	
	
	//REDOCUMENTACION 
	private BigDecimal montoRegistradoRedoc;
	private BigDecimal nuevoMontoRecuperarRedoc;
	
	@Autowired
	@Qualifier("recuperacionProveedorServiceEJB")
	private RecuperacionProveedorService recuperacionProveedorService;
	
	@Autowired
	@Qualifier("movimientoSiniestroServiceEJB")
	private MovimientoSiniestroService movimientoSiniestroService;
	
	/**
	 * Invocar al m�todo <b><i>recuperacionCiaService.
	 * guardarRecuperacionCiaManual</i></b>
	 */
      @Action(value="validarGuardarRecuperacion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})				
		})
	public String validarGuardarRecuperacion(){
		    	if(null!=this.prestadorServicioId){
		  			PrestadorServicio compania=this.entidadService.findById(PrestadorServicio.class,new Integer (prestadorServicioId.toString()));
		  			recuperacion.setCompania(compania);
		  		}
		     //TODO AGREGAR EL CAMPO idPaseAtencion a la validación
    		  String msj=(this.recuperacionCiaService.validarRecuperacionCia(this.recuperacion,this.idPaseAtencion));
    		  if(!StringUtil.isEmpty(msj)){
    			  this.setMensajeError(msj);
    		  }else{
    			  this.setMensajeExito();
    		  }		
    		   return SUCCESS;		
		}
      
	@Action(value = "guardarRecuperacion", results = { 
			
            @Result(name = INPUT, location = CONTENEDOR_RECUPERACION_CIA) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/recuperacionCia",
					"soloConsulta","${soloConsulta}",
					"esNuevoRegistro","false",
					"recuperacionId", "${recuperacion.id}",
					"tipoMostrar","${tipoMostrar}",
					"editarDatosGenericos","${editarDatosGenericos}",
					"mensaje","${mensaje}"}) 					
			})	
	public String guardarRecuperacion(){
		if(null!=this.prestadorServicioId){
			PrestadorServicio compania=this.entidadService.findById(PrestadorServicio.class,new Integer (prestadorServicioId.toString()));
			recuperacion.setCompania(compania);
		}    
		//this.mensajePantalla=recuperacionCiaService.guardarRecuperacionCiaManual(recuperacion, cartaCia, this.coberturasSeleccionadas, this.pasesSeleccionadas, ordenesCiaConcat);
		
		// GUARDA MONTOS Y PROVISION SOLAMENTE, ESTO POR QUE AL METER DENTRO DEL METODO INICIAL LA TRANSACCION MARCA ERROR
//		RecuperacionCompania recuperacionCia = this.recuperacionCiaService.guardarCiaYProvisiona(recuperacion.getId(),recuperacion.getMontoGAGrua(), ordenesCiaConcat,this.pasesSeleccionadas,this.coberturasSeleccionadas);
//		
//		//this.mensajePantalla=recuperacionCiaService.guardarRecuperacionCiaManualProvision(recuperacion, cartaCia, this.coberturasSeleccionadas, this.pasesSeleccionadas, ordenesCiaConcat);
//		this.mensajePantalla = this.recuperacionCiaService.guardarRecuperacionCiaManualProvision(recuperacion, cartaCia, this.coberturasSeleccionadas, this.pasesSeleccionadas, ordenesCiaConcat);
		this.mensajePantalla=recuperacionCiaService.guardarCiaYProvisiona(recuperacion, cartaCia, this.coberturasSeleccionadas, this.pasesSeleccionadas, ordenesCiaConcat);
		//this.mensajePantalla = "";
		
		if(!StringUtils.isEmpty(this.mensajePantalla)){
			setMensaje(mensajePantalla);
			return INPUT;
		}else{
			setMensaje("Recuperacion No."+recuperacion.getNumero()+" Guardada Exitosamente");
			if(recuperacion.getId()!=null){
				this.tipoMostrar=TIPO_EDITAR;
			}
			
		}
		return SUCCESS;
	}
	/**
	 * Mostrar contenedor principal de la pantalla de Rcuperacion de Compa�ias
	 * 
	 * <b>///EDITAR RECUPERACION A COMPA�IA////</b>
	 * Si la variable recuperacionId no es nulo, entonces se debe invocar al m�todo
	 * <b><i>recuperacionCiaService.obtenerRecuperacionCia</i></b> para asignar a la
	 * variable <b>recuperacion</b>, <b><i>estimacionCoberturaSiniestroService.
	 * obtenerDatosEstimacion</i></b>  para asignar a la variable
	 * <b>datosEstimacion</b>, <b><i>recuperacionCiaService.obtenerCartaOriginal
	 * </i></b>para asignar a la variable <b>cartaCia</b>
	 */
	@Action(value = "mostrarContenedor", results = { 
			@Result(name = SUCCESS, location = CONTENEDOR_RECUPERACION_CIA) ,			
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/recuperaciones",
					"mensaje","${mensaje}"}) })			
	public String mostrarContenedor(){ 
		recuperacion = recuperacionCiaService.obtenerRecuperacionCia(recuperacionId);	
		if (null!=recuperacionId ){				
			setPropiedadesGenerales(recuperacion);
			recuperacion.setMontoPiezas((recuperacion.getMontoPiezas()==null)?BigDecimal.ZERO:recuperacion.getMontoPiezas());
			this.cartaCia=this.recuperacionCiaService.obtenerCartaCompania(recuperacionId);		
			if(EnumUtil.equalsValue(EstatusCartaCompania.PENDIENTE_ELABORACION, cartaCia.getEstatus())  
					|| 	EnumUtil.equalsValue(EstatusCartaCompania.REGISTRADO, cartaCia.getEstatus())){
				this.editarDatosGenericos = Boolean.TRUE;
			}else{
				this.editarDatosGenericos = Boolean.FALSE;
			}
			if(this.soloConsulta ||  this.tipoMostrar.equalsIgnoreCase(TIPO_CONSULTAR)  ){
				this.editarDatosGenericos = Boolean.FALSE;
			}
			
			this.datosEstimacion=recuperacionCiaService.obtenerDatosRelacionSiniestro(recuperacionId);
			this.idCobertura=datosEstimacion.getIdCoberturaRepCabina();
			this.idPaseAtencion=datosEstimacion.getIdEstimacionCoberturaRepCabina();
			
			// OBTENER MONTO DE PIEZAS
//			recuperacion.setMontoPiezas(this.datosEstimacion.getMontoPiezas());
			
			lstOrdenesCompra= this.recuperacionCiaService.obtenerOrdenesCompraCia(recuperacionId, Boolean.FALSE);
			ordenesCiaConcat=this.concatenaListaOrdenesCia(lstOrdenesCompra);
			prepareMostrarRedocumentacion();
			
			/**TODO Se ejecuta obtenerTieneComplemento si es true debe mostrar la pantalla de acciones si es false continuar **/
			this.complementoCompania= this.recuperacionCiaService.obtenerTieneComplemento(recuperacionId);
			if(null!=complementoCompania && null!=complementoCompania.getId()){
				this.tieneComplemento=true;
			}else{
				this.tieneComplemento=false;
			}
			reservaDisponible = movimientoSiniestroService.obtenerReservaAfectacion(idPaseAtencion, Boolean.TRUE);
		}else{
			recuperacion=this.recuperacionCiaService.inicializaRecuperacionCompania(recuperacion);
		}	   
		llenarListadosCompania();
		if(this.tipoMostrar.equalsIgnoreCase(TIPO_CANCELAR) || this.tipoMostrar.equalsIgnoreCase(TIPO_CONSULTAR)){
			this.prepareMostrarCancelacion();
		}
		
		return SUCCESS;
	}
	
	public String concatenaListaOrdenesCia(List<OrdenCompraCartaCia> lstOrdenesCompra){
		String concat = "";
		if(null!= lstOrdenesCompra && ! lstOrdenesCompra.isEmpty()){
			Iterator<OrdenCompraCartaCia> iteratorPases = lstOrdenesCompra.iterator();
		    while (iteratorPases.hasNext()) {		
		    	OrdenCompraCartaCia oc  = iteratorPases.next();
		    	concat +=oc.getOrdenCompra().getId();
		        if (iteratorPases.hasNext()) {
		        	concat += separator;
		        }
		    }
		}
		return concat;
	}
	
	@Action(value="mostrarContenedorCompanias",results={
			@Result(name=SUCCESS,location=DETALLE_RECUPERACION_CIA),
			@Result(name=INPUT,location=DETALLE_RECUPERACION_CIA)
			})
	public String mostrarContenedorCompanias(){
		reservaDisponible = movimientoSiniestroService.obtenerReservaAfectacion(idPaseAtencion, Boolean.TRUE);
		return SUCCESS;
	}
	public void llenarListadosCompania(){
			this.lstCiaDeSeguros.put(new Long(0), getText("midas.general.seleccione"));
			this.lstCiaDeSeguros.putAll(this.listadoService.getMapCiaDeSeguros());
			this.lstEstatusCarta.put("", getText("midas.general.seleccione"));  
			this.lstEstatusCarta=this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_CARTA_CIA);  
			this.lstCausasNoElaboracion.put("", getText("midas.general.seleccione")); 
			this.lstCausasNoElaboracion.putAll(this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CAUSA_NO_ELABORACION));
			if(null!=recuperacion && null!=recuperacion.getId()){
				if(null!= recuperacion.getCompania() && null!= recuperacion.getCompania().getId())
					this.prestadorServicioId= new Long( recuperacion.getCompania().getId());				
				if(this.soloConsulta || this.tipoMostrar.equalsIgnoreCase(this.TIPO_CONSULTAR)){
					if(null!=this.recuperacion.getCompania() && null!=this.recuperacion.getCompania().getId()){
						this.lstCiaDeSeguros.clear();
						this.lstCiaDeSeguros.put(new Long(this.recuperacion.getCompania().getId()), this.recuperacion.getCompania().getPersonaMidas().getNombre());
					}
					this.lstEstatusCarta=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, this.cartaCia.getEstatus());
					this.lstCausasNoElaboracion.clear();
					if(null!=this.cartaCia && !StringUtil.isEmpty(this.cartaCia.getCausasNoElaboracion())){
						this.lstCausasNoElaboracion=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.CAUSA_NO_ELABORACION, this.cartaCia.getCausasNoElaboracion());
					}
				}else if(this.tipoMostrar.equalsIgnoreCase("U")){
					this.lstEstatusCarta=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, cartaCia.getEstatus());
				}
			}else{
				this.estatusRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
				this.tipoRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION, Recuperacion.TipoRecuperacion.COMPANIA.toString());
				this.lstTipoOC=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.TIPO_ORDENCOMPRA, Recuperacion.TipoOrdenCompra.AFECTACION.toString());
				lstMedioRecuperacion=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION, Recuperacion.MedioRecuperacion.REEMBOLSO.toString() );
				this.cartaCia.setEstatus( CartaCompania.EstatusCartaCompania.REGISTRADO.codigo.toString());
				this.lstEstatusCarta=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, cartaCia.getEstatus());
			}
	}
	
	/**
	 * Invocar al m�todo <b><i>estimacionCoberturaSiniestroService.
	 * obtenerDatosEstimacion</i></b> y asignar el resultado a la variable
	 * <b>infoAfectacion</b>
	 */
	@Action (value = "obtenerDatosPase", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^infoAfectacion.*,reservaDisponible"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^infoAfectacion.*,reservaDisponible"})
      })
	public String obtenerDatosPase(){
		Long idCobertura =null;
		String cveSubTipoCalculo =null;
		Long idPase=null;
		if(!StringUtil.isEmpty(this.coberturasSeleccionadas)){			
			coberturasSeleccionadas= coberturasSeleccionadas.trim();
			String[] coberturaLst=  coberturasSeleccionadas.split("\\|"); 
			idCobertura = new Long (coberturaLst[0]);
			cveSubTipoCalculo =coberturaLst[1];				
		}
		if(!StringUtil.isEmpty(this.pasesSeleccionadas)){			
			pasesSeleccionadas= pasesSeleccionadas.trim();
			String[] paseslst=  pasesSeleccionadas.split("\\|"); 
			idPase = new Long (paseslst[0]);
		}
		this.infoAfectacion= this.recuperacionCiaService.obtenerDatosRelacionSiniestro(idCobertura,cveSubTipoCalculo,idPase);
		recuperacion.setImporteInicial(infoAfectacion.getMontoInicial());
		recuperacion.setPorcentajeParticipacion(infoAfectacion.getPorcParticipacion());
		recuperacion.setSiniestroCia(infoAfectacion.getNoSiniestrosCia());
		recuperacion.setPolizaCia(infoAfectacion.getNumPolizaCia());
		prestadorServicioId = infoAfectacion.getCompaniaId();
		nombreCia = infoAfectacion.getNombreCompania();
		reservaDisponible = movimientoSiniestroService.obtenerReservaAfectacion(idPase, Boolean.TRUE);
		return SUCCESS;
	}
	@Action (value = "calcularImportes", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^infoImportes.*"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^infoImportes.*"})
      })
	public String calcularImportes(){
		obtenerIdcoberturaPase();
		if( (null==this.recuperacion || null== this.recuperacion.getId())  || this.editarDatosGenericos  ){
			if(!StringUtil.isEmpty(ordenesCiaConcat)){
				infoImportes= this.recuperacionCiaService.calculaImportes(ordenesCiaConcat, porcentajeParticipacion, this.recuperacion.getMontoGAGrua(), idPaseAtencion,idCobertura);
			}
		}
		
		return SUCCESS;
	}
	/**
	 * 
	 * Si la recuperacionId es diferente de nulo, Invocar al m�todo
	 * <b><i>recuperacionCiaService.obtenerOrdenesCompraCia</i></b> y asignarlo a la
	 * variable <b>lstOrdenesCompra;</b>
	 * de lo contrario Invocar al m�todo r<b><i>ecuperacionCiaService.
	 * obtenerOCEstimacion</i></b> y asignar a la variable <b>lstOdenesCompra</b>
	 */
	@Action(value="obtenerOrdenesCompraCia",results={
			@Result(name=SUCCESS,location=ORDEN_COMPRA_GRID),
			@Result(name=INPUT,location=ORDEN_COMPRA_GRID)
		})
	public String obtenerOrdenesCompraCia(){
		
		if (recuperacionId != null && !editarDatosGenericos ) {
			lstOrdenesCompra= this.recuperacionCiaService.obtenerOrdenesCompraCia(recuperacionId, Boolean.FALSE);
		} else {
			obtenerIdcoberturaPase();
			//TODO Agregar el ID de la recuperacion para saber que ordenes le corresponden a la recuiperacion
			lstOrdenesCompra= this.recuperacionCiaService.obtenerOCestimacion(recuperacionId,recuperacion.getReporteCabina().getId(), 
					idCobertura, idPaseAtencion, this.ordenesCiaConcat, this.porcentajeParticipacion, Boolean.FALSE);
		}
		
		return SUCCESS;
	}
	public void obtenerIdcoberturaPase(){
		idCobertura =null;
		idPaseAtencion=null;
		if(!StringUtil.isEmpty(this.coberturasSeleccionadas)){			
			coberturasSeleccionadas= coberturasSeleccionadas.trim();
			String[] coberturaLst=  coberturasSeleccionadas.split("\\|"); 
			idCobertura = new Long (coberturaLst[0]);				
		}
		if(!StringUtil.isEmpty(this.pasesSeleccionadas)){			
			pasesSeleccionadas= pasesSeleccionadas.trim();
			String[] paseslst=  pasesSeleccionadas.split("\\,"); 
			if(null!=paseslst && paseslst.length>0 )
			idPaseAtencion = new Long (paseslst[0]);
		}
	}
	/**
	 * <b>SE DEFINE EN DISE�O EDITAR RECUPERACION A COMPA�IAS</b>
	 * 
	 * Invocar al m�todo r<b><i>ecuperacionCiaService.elaborarCarta</i></b>
	 */
		@Action(value = "elaborarCarta", results = { 		
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/recuperaciones",
					"mensaje","${mensaje}"}) ,	
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/recuperacion/recuperacionCia",
					"soloConsulta","${soloConsulta}",
					"esNuevoRegistro","${esNuevoRegistro}",
					"recuperacionId","${recuperacionId}",
					"tipoMostrar","${tipoMostrar}",
					"editarDatosGenericos","${editarDatosGenericos}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"})
			})  
			public String elaborarCarta(){			
			if(null!=this.prestadorServicioId){				
				PrestadorServicio compania=this.entidadService.findById(PrestadorServicio.class,new Integer (prestadorServicioId.toString()));
				recuperacion.setCompania(compania);
			}    
			//this.mensajePantalla=recuperacionCiaService.guardarRecuperacionCiaManual(recuperacion,this.cartaCia, this.coberturasSeleccionadas, this.pasesSeleccionadas, ordenesCiaConcat);
			this.mensajePantalla=recuperacionCiaService.guardarCiaYProvisiona(recuperacion,this.cartaCia, this.coberturasSeleccionadas, this.pasesSeleccionadas, ordenesCiaConcat);
			if(!StringUtils.isEmpty(this.mensajePantalla)){
				setMensajeError(mensajePantalla);
				return INPUT;
			}else{
				String mensaje=this.recuperacionCiaService.elaborarCarta(recuperacionId);
				if (StringUtil.isEmpty(mensaje)){
					setMensajeExito();
					editarDatosGenericos=false;
					return INPUT;
				}else {
				setMensajeError(mensaje);
				return INPUT;
				}
			}	
		}
		/**
		 * - Actualiza la Fecha de impresión con la Fecha actual del Sistema solo en caso de ser la primera impresión.
			- Actualiza el Pase de Atención Compañía según con el dato capturado en la ventana de impresión (Original o Copia).
			- Actualiza el Estatus de la Carta como Impreso 
		 */
		@Action(value="imprimeCarta", results={
				@Result(name=INPUT,location=TIPO_PASE_CIA),
				@Result(name = SUCCESS, type = "redirectAction", params = { 
						"actionName", "mostrarContenedor", 
						"namespace", "/siniestros/recuperacion/recuperacionCia",
						"soloConsulta","${soloConsulta}",
						"esNuevoRegistro","${esNuevoRegistro}",
						"recuperacionId","${recuperacionId}",
						"tipoMostrar","${tipoMostrar}",
						"editarDatosGenericos","${editarDatosGenericos}",
						"mensaje", "${mensaje}",	 	
						"tipoMensaje", "${tipoMensaje}"})
		})
		public String imprimeCarta(){
			this.lstTipoPaseCia.put("", getText("midas.general.seleccione"));
			this.lstTipoPaseCia.putAll(listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.PASE_ATENCION_CIA));
			Map<String, Object> params = new HashMap<String, Object>();	
			if( tipoPaseCia != null && tipoPaseCia != "" ){	
				params.put("estatus", CartaCompania.EstatusCartaCompania.IMPRESO.codigo);
				params.put("recuperacion.id", recuperacionId);
				List<CartaCompania>  lstCartaCompania=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params,"folio desc");
				if(null!=lstCartaCompania && !lstCartaCompania.isEmpty()){
					this.cartaCia=lstCartaCompania.get(0);
				}else{
					this.cartaCia=this.recuperacionCiaService.obtenerCartaCompania(recuperacionId);
					cartaCia.setFechaImpresion(new Date());
				}
				cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.IMPRESO.toString());
				recuperacion = recuperacionCiaService.obtenerRecuperacionCia(recuperacionId);
				recuperacion.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
				recuperacion.setFechaModificacion(new Date());			
				recuperacion.setPaseAtencionCia(tipoPaseCia);				
				this.entidadService.save(recuperacion);
				cartaCia.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
				cartaCia.setFechaModificacion(new Date());	
				this.entidadService.save(cartaCia);
				
				this.soloConsulta=false;
				this.esNuevoRegistro=false;
				this.tipoMostrar=this.TIPO_EDITAR;
				this.editarDatosGenericos=false;
				setMensajeExito();
				return SUCCESS;			
			}
			return INPUT;
		}
	
		/**
		 * <b>SE DEFINE EN DISE�O EDITAR RECUPERACION A COMPA�IAS</b>
		 * Invocar al m�todo r<b><i>ecuperacionCiaService.entregaCarta</i></b>
		 */
		@Action(value="entregaCarta", results={
				@Result(name=INPUT,location=ENTREGA_CIA),
				@Result(name = SUCCESS, type = "redirectAction", params = { 
						"actionName", "mostrarContenedor", 
						"namespace", "/siniestros/recuperacion/recuperacionCia",
						"soloConsulta","${soloConsulta}",
						"esNuevoRegistro","${esNuevoRegistro}",
						"recuperacionId","${recuperacionId}",
						"tipoMostrar","${tipoMostrar}",
						"editarDatosGenericos","${editarDatosGenericos}",
						"mensaje", "${mensaje}",	 	
						"fechaAcuseCartasMigradas", "${fechaAcuseCartasMigradas}",
						"codeUserCartasMigradas", "${codeUserCartasMigradas}",
						"tipoMensaje", "${tipoMensaje}"})
		})
		public String entregaCarta(){
			if( fechaAcuse != null ){
				String msj=this.recuperacionCiaService.entregarCartas(this.recuperacionId.toString(), fechaAcuse);
				this.soloConsulta=false;
				this.esNuevoRegistro=false;
				this.tipoMostrar=this.TIPO_EDITAR;
				this.editarDatosGenericos=false;
				if (StringUtil.isEmpty(msj)){
					setMensajeExito();
					return SUCCESS;	
				}else{
					setMensajeError(msj);
					return SUCCESS;	
				}
			}else {
				return INPUT;
			}
			
		}
		
		@Action(value = "mostrarAlerta", 
				results = { 
				@Result(name = SUCCESS, location =ALERTA_CIA),
				@Result(name = INPUT, location = ALERTA_CIA)
				})
				public String mostrarAlerta(){
					return SUCCESS;
				}		
		
		@Action(value="buscarAlertas",results={
				@Result(name=SUCCESS,location=ALERTA_CIA_GRID),
				@Result(name=INPUT,location=ALERTA_CIA_GRID)
				})
		public String buscarAlertas(){
			recuperacionesVencer = this.recuperacionCiaService.obtenerRecuperacionesVencer();
			return SUCCESS;
		}
		//---------------CANCELACION-------------------------------
		/**
		 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
		 * Mostrar contenido pesta�a Cancelaci�n
		 */
		@Action(value = "mostrarCancelacion", 
				results = { 
				@Result(name = SUCCESS, location =CANCELACION_CIA),
				@Result(name = INPUT, location = CANCELACION_CIA)
				})
		public String mostrarCancelacion(){
			return SUCCESS;
		}
		/**
		 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
		 * Invocar al m�todo <b><i>recuperacionCiaService.cancelarRecuperacion</i></b>
		 */
		@Action(value = "cancelarRecuperacion", 
				results = { 
				@Result(name = SUCCESS, location =CANCELACION_CIA),
				@Result(name = INPUT, type = "redirectAction", params = { 
						"actionName", "mostrarContenedor", 
						"namespace", "/siniestros/recuperacion/recuperacionCia",
						"soloConsulta","${soloConsulta}",
						"esNuevoRegistro","${esNuevoRegistro}",
						"recuperacionId","${recuperacionId}",
						"tipoMostrar","${tipoMostrar}",
						"editarDatosGenericos","${editarDatosGenericos}",
						"mensaje", "${mensaje}",	 	
						"tipoMensaje", "${tipoMensaje}"})
				})
		public String cancelarRecuperacion(){			
			this.recuperacionCiaService.cancelarRecuperacion(this.recuperacionId);
			this.soloConsulta=false;
			this.esNuevoRegistro=false;
			this.tipoMostrar=this.TIPO_CANCELAR;
			this.editarDatosGenericos=false;
			this.recuperacionId=recuperacionId;
			setMensajeExito();
			return INPUT;
		}
		/**
		 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
		 * Invocar al m�todo <b><i>recuperacionCiaService.solicitarCancelacion</i></b>
		 */@Action(value = "solicitarCancelacion", 
				results = { 
				@Result(name = SUCCESS, location =SOLICITAR_CANCELA_CIA),
				@Result(name = INPUT, type = "redirectAction", params = { 
						"actionName", "mostrarContenedor", 
						"namespace", "/siniestros/recuperacion/recuperacionCia",
						"soloConsulta","${soloConsulta}",
						"esNuevoRegistro","${esNuevoRegistro}",
						"recuperacionId","${recuperacionId}",
						"tipoMostrar","${tipoMostrar}",
						"editarDatosGenericos","${editarDatosGenericos}",
						"mensaje", "${mensaje}",	 	
						"tipoMensaje", "${tipoMensaje}"})
				})
				public String solicitarCancelacion(){			 		
			 		this.recuperacionCiaService.solicitarCancelacion(this.recuperacionId,this.recuperacion.getMotivoCancelacion() );
			 		this.soloConsulta=true;
					this.esNuevoRegistro=false;
					this.tipoMostrar=this.TIPO_CANCELAR;
					this.editarDatosGenericos=false;
					this.recuperacionId=recuperacionId;
					setMensajeExito();
					return INPUT;
				}
		@Action(value = "mostrarSolCancela", 
				results = { 
				@Result(name = SUCCESS, location =SOLICITAR_CANCELA_CIA),
				@Result(name = INPUT, location = CONTENEDOR_RECUPERACION_CIA)
				})
				public String mostrarSolCancela(){
					this.tienePermisoSolCancela= true;
					recuperacion = recuperacionCiaService.obtenerRecuperacionCia(recuperacionId);
					this.lstMotivosCanc.put("", getText("midas.general.seleccione"));
					this.lstMotivosCanc.putAll( this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO));
					return SUCCESS;
				}
		
		
		/**
		 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
		 * 
		 * Invocar al m�todo <b><i>recuperacionCiaService.obtenerCartasRedoc</i></b> y
		 * asignar a la variable <b>lstCartasRedoc</b>
		 */
		@Action(value="obtenerCartasRedoc",results={
				@Result(name=SUCCESS,location=REDOCUMENTACIONCIA_GRID),
				@Result(name=INPUT,location=REDOCUMENTACIONCIA_GRID)
			})
		public String obtenerCartasRedoc(){
			this.lstCartaCompania= this.recuperacionCiaService.obtenerCartasRedoc(this.recuperacionId);
			return SUCCESS;
		}
		

//----------------------------------------------
	@Action(value = "guardarRedocumentacion", results = { 
	            @Result(name = INPUT, location = CONTENEDOR_RECUPERACION_CIA) ,	
	            @Result(name = SUCCESS, type = "redirectAction", params = { 
						"actionName", "mostrarContenedor", 
						"namespace", "/siniestros/recuperacion/recuperacionCia",
						"soloConsulta","${soloConsulta}",
						"esNuevoRegistro","${esNuevoRegistro}",
						"recuperacionId","${recuperacionId}",
						"tipoMostrar","${tipoMostrar}",
						"editarDatosGenericos","${editarDatosGenericos}",
						"mensaje", "${mensaje}",	 	
						"tipoMensaje", "${tipoMensaje}",
						"fechaAcuseCartasMigradas", "${fechaAcuseCartasMigradas}"})					
				})	
		public String guardarRedocumentacion(){
		//this.recuperacionCiaService.generarRedocumentacion(this.cartaRedocumentacion, this.recuperacionId);
		
			if (fechaAcuseCartasMigradas == null){//si facusecartasmigradas viene vacío se invoca proceso normal
				this.recuperacionCiaService.generarRedocumentacionProvision(this.cartaRedocumentacion,this.recuperacionId, validaExclusionRechazo);
				super.setMensajeExito();
			}else{//si				
				this.cartaRedocumentacion.setFechaAcuseCartasMigradas(fechaAcuseCartasMigradas);
				this.recuperacionCiaService.generarRedocumentacionProvision(this.cartaRedocumentacion,this.recuperacionId, validaExclusionRechazo);
				super.setMensajeExito();
			}
			return SUCCESS;
		}
		
		
	@Action(value = "capturaFechaAcuseCartasMigradas", results={
			@Result(name=INPUT, location=ENTREGA_CIA),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "guardarRedocumentacion", 
					"namespace", "/siniestros/recuperacion/recuperacionCia",
					"soloConsulta","${soloConsulta}",
					"esNuevoRegistro","${esNuevoRegistro}",
					"recuperacionId","${recuperacionId}",
					"tipoMostrar","${tipoMostrar}",
					"editarDatosGenericos","${editarDatosGenericos}",
					"mensaje", "${mensaje}",	 
					"codeUserCartasMigradas", "${codeUserCartasMigradas}",
					"tipoMensaje", "${tipoMensaje}"})
	})
	
	public String capturaFechaAcuseCartasMigradas(){
		
		if(fechaAcuseCartasMigradas != null){
			this.soloConsulta=false;
			this.esNuevoRegistro=false;
			this.tipoMostrar=this.TIPO_EDITAR;
			this.editarDatosGenericos=false;
			return SUCCESS;
		}else {
			return INPUT;
		}
		
	}
	
	@Action(value = "elaborarCartaRedoc", results = { 		
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/recuperaciones",
					"mensaje","${mensaje}"}) ,	
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/recuperacion/recuperacionCia",
					"soloConsulta","${soloConsulta}",
					"esNuevoRegistro","${esNuevoRegistro}",
					"recuperacionId","${recuperacionId}",
					"tipoMostrar","${tipoMostrar}",
					"editarDatosGenericos","${editarDatosGenericos}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"})
			})  
			public String elaborarCartaRedoc(){	
//				this.recuperacionCiaService.generarRedocumentacionProvision(this.cartaRedocumentacion, this.recuperacionId,validaExclusionRechazo);
				String mensaje=this.recuperacionCiaService.elaborarCarta(recuperacionId);
				if (StringUtil.isEmpty(mensaje)){
					setMensajeExito();
					editarDatosGenericos=false;
					return INPUT;
				}else {
				setMensajeError(mensaje);
				return INPUT;
				}
		}
		
	@Action(value="mostrarImprimeCartaRedoc",results={
			@Result(name=SUCCESS,location=IMPRESION_REDOC_CIA),
			@Result(name=INPUT,location=IMPRESION_REDOC_CIA)
			})
	public String mostrarImprimeCartaRedoc(){
		return SUCCESS;
	}
	
	@Action(value = "imprimeCartaRedoc", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimeCartaRedoc(){
			CartaCompania  cartaCia=this.recuperacionCiaService.obtenerCartaActiva(this.recuperacionId);
			cartaCia.setFechaImpresion(new Date());
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.IMPRESO.toString());			
			cartaCia.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCia.setFechaModificacion(new Date());	
			cartaCia = this.entidadService.save(cartaCia);
			this.soloConsulta=false;
			this.esNuevoRegistro=false;
			this.tipoMostrar=this.TIPO_EDITAR;
			this.editarDatosGenericos=false;
			
			transporte = recuperacionCiaService.imprimirCarta(cartaCia.getId());
			transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
			transporte.setContentType("application/pdf");
			String fileName = "CartaCia_"+ cartaCia.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
			transporte.setFileName(fileName);
			
			setMensajeExito();
			return SUCCESS;			
	}
		
	
	@Action(value="entregaCartaRedoc", results={
			@Result(name=INPUT,location=ENTREGA_CIA),
			@Result(name = SUCCESS, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/recuperacion/recuperacionCia",
					"soloConsulta","${soloConsulta}",
					"esNuevoRegistro","${esNuevoRegistro}",
					"recuperacionId","${recuperacionId}",
					"tipoMostrar","${tipoMostrar}",
					"editarDatosGenericos","${editarDatosGenericos}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"})
	})
	public String entregaCartaRedoc(){
		if( fechaAcuse != null ){
			String msj=this.recuperacionCiaService.entregaCarta(this.recuperacionId, fechaAcuse);
			this.soloConsulta=false;
			this.esNuevoRegistro=false;
			this.tipoMostrar=this.TIPO_EDITAR;
			this.editarDatosGenericos=false;
			if (StringUtil.isEmpty(msj)){
				setMensajeExito();
				return SUCCESS;	
			}else{
				setMensajeError(msj);
				return SUCCESS;	
			}
		}else {
			return INPUT;
		}
		
	}
		
	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * Invocar al m�todo <b><i>recuperacionCiaService.generarRedocumentacion</i></b>
	 */
	public String generarRedocumentacion(){
		return "";
	}

	
	/**
	 * <b>SE DEFINE EN DISE�O DE REGISTRAR SEGUIMIENTO DE RECUPERACI�N DE
	 * COMPA��A</b>
	 * 
	 * Invocar al m�todo<b><i> recuperacionCiaService.guardarSeguimiento</i></b>
	 */
	@Action(value = "guardarSeguimiento", 
			results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","seguimientoId"}),
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","recuperacionId,currentUser,comentarioSeguimiento"})
			})
	public String guardarSeguimiento(){
		recuperacionCiaService.guardarSeguimiento(this.recuperacionId, this.comentarioSeguimiento);
		return SUCCESS;
	}

	
	

	/**
	 * <b>SE AGREGA EN DISE�O REGISTRAR REDOCUMENTACION</b>
	 * 
	 * 
	 * Invocar al m�todo<b><i> recuperacionCiaService.obtenerCartaActiva</i></b> y si
	 * la carta tiene estatus diferente a ENTREGADA asignar a la variable
	 * <b>cartaCia</b>
	 */
	public String mostrarRedocumentacion(){
		return "";
	}

	/**
	 * <b>SE DEFINE EN DISE�O DE REGISTRAR SEGUIMIENTO DE RECUPERACI�N DE
	 * COMPA��A</b>
	 * 
	 * Mostrar pesta�a del contenedor de Seguimiento de Recuperaci�n de Compa��a
	 */
	@Action(value = "mostrarSeguimiento", 
			results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","currentUser,userName,creationDateTxt"}),
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","recuperacionId"})
			})
	public String mostrarSeguimiento(){
		setCurrentUser(usuarioService.getUsuarioActual().getNombreUsuario());
		setUserName(usuarioService.getUsuarioActual().getNombreCompleto());		
		setCreationDateTxt(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		lstSeguimientos = new ArrayList<SeguimientoRecuperacion>();
		return SUCCESS;
	}

	

	

	/**
	 * <b>SE AGREGA EN DISE�O DE MOSTRAR RECUPERACIONES CIA POR VENCER</b>
	 * 
	 * Invocar al m�todo <b><i>recuperacionCiaService.
	 * obtenerRecuperacionesVence</i></b>r y asignar a la variable
	 * <b>recuperacionesVencer</b>
	 */
	public String obtenerRecuperacionesVencer(){
		return "";
	}

	/**
	 * <b>SE DEFINE EN DISE�O DE REGISTRAR SEGUIMIENTO DE RECUPERACI�N DE
	 * COMPA��A</b>
	 * 
	 * Invocar al m�todo<b><i> recuperacionCiaService.obtenerSeguimientos </i></b>y
	 * asignar a la variable lstSeguimiento
	 */
	@Action(value="obtenerSeguimientos",results={
			@Result(name=SUCCESS,location=SEGUIMIENTOS_GRID),
			@Result(name=INPUT,location=SEGUIMIENTOS_GRID)
		})
	public String obtenerSeguimientos(){
		if(null == this.recuperacionId ){                              
			lstSeguimientos = new ArrayList<SeguimientoRecuperacion>();
		}else{
			lstSeguimientos = this.recuperacionCiaService.obtenerSeguimientos(this.recuperacionId);
		}	
		return SUCCESS;
	}
	
	/**
	 * <b>SE DEFINE EN DISE�O DE CANCELAR RECUPERACION COMPA�IA</b>
	 * 
	 * Invocar m�todo obtenerListados
	 */
	public void prepareMostrarCancelacion(){
		if (null!= this.recuperacion  && null!= this.recuperacionId &&  !StringUtil.isEmpty(recuperacion.getMotivoCancelacion())){
			this.lstMotivosCanc= this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO, recuperacion.getMotivoCancelacion());
		}
	}

	/**
	 * Llenar listados de tipoRedocumentacion, lstCausaRechazo, 
	 * 
	 * <b>lstCausaRechazo</b>: obtener el listado mediante listadoService.
	 * obtenerCatalogoValorFijo enviando el codigo CAUSA_RECHAZO_CARTA
	 * 
	 * <b>lstTipoRechazo</b>:  obtener el listado mediante listadoService.
	 * obtenerCatalogoValorFijo enviando el codigo TIPO_RECHAZO_CARTA
	 * 
	 * <b>lstMotivoExclusion</b>:  obtener el listado mediante listadoService.
	 * obtenerCatalogoValorFijo enviando el codigo MOTIVO_EXCLUSION_CARTA
	 * 
	 * 
	 * <b>tipoRedocumentacion</b>:  obtener el listado mediante listadoService.
	 * obtenerCatalogoValorFijo enviando el codigo TIPO_REDOCUMENTACION
	 */
	public void prepareMostrarRedocumentacion(){
		Map<String, Object> params = new HashMap<String, Object>();
		lstTipoRechazoRedocumentacion= new LinkedHashMap<String, String>();
		this.cartaCiaActiva=this.recuperacionCiaService.obtenerCartaActiva(this.recuperacionId);
		cartaCiaActiva.setMontoARecuperar(this.recuperacion.getImporte());
		params.put("recuperacion.id", recuperacionId);
		List<CartaCompania>  listaCartas=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params, "folio desc");		
		if(null!= listaCartas  && !listaCartas.isEmpty()  && listaCartas.size()>1 ){
			CartaCompania penultimaCarta  = listaCartas.get(1);
			if(null!=penultimaCarta.getMontoARecuperar())
				cartaCiaActiva.setMontoARecuperar(penultimaCarta.getMontoARecuperar());
		}
		params.clear();
		params.put("recuperacion.id", recuperacionId);
		params.put("folio", 0);
		List<CartaCompania> lst=this.entidadService.findByProperties(CartaCompania.class, params);
		if(null!=lst && !lst.isEmpty()){
			this.primeraCarta=lst.get(0);
			if(null!=primeraCarta.getFechaAcuse()){
				this.verRedocumentacion=true;
			}
		
		}
		Long folio= cartaCiaActiva.getFolio();
		if(  (cartaCiaActiva.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo) ||  cartaCiaActiva.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.POR_REDOCUMENTAR.codigo)  
				||  cartaCiaActiva.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.REGISTRADO.codigo) ||  cartaCiaActiva.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.RECHAZADO.codigo))   ){
			if( cartaCiaActiva.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.REGISTRADO.codigo) && folio !=0){
				this.cartaRedocumentacion=this.recuperacionCiaService.obtenerCartaActiva(this.recuperacionId);
				if(null!= listaCartas  && !listaCartas.isEmpty()  && listaCartas.size()>1 ){
					CartaCompania penultimaCarta  = listaCartas.get(1);
					if(null!=penultimaCarta.getMontoARecuperar())
						cartaCiaActiva.setMontoARecuperar(penultimaCarta.getMontoARecuperar());
				}
			}else{
				this.cartaRedocumentacion= new CartaCompania();
				this.cartaRedocumentacion.setFechaCreacion(null);
				cartaCiaActiva.setMontoARecuperar(this.recuperacion.getImporte());
				if(folio!=0){
		          this.cartaRedocumentacion.setFechaCreacion(cartaCiaActiva.getFechaCreacion());
		          this.cartaRedocumentacion.setCodigoUsuarioCreacion(cartaCiaActiva.getCodigoUsuarioCreacion());
		        }
			}
			this.lstTipoRedocumentacion.put("", getText("midas.general.seleccione"));
			lstTipoRedocumentacion.putAll(this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_REDOCUMENTACION));
			this.lstCausaRechazo.put("", getText("midas.general.seleccione"));
			this.lstCausaRechazo.putAll(this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_RECHAZO_CARTACIA));//
			this.lstTipoRechazoRedocumentacion.put("",  getText("midas.general.seleccione"));
			this.lstTipoRechazoRedocumentacion.putAll( this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECHAZO_RED_CIA));
			lstMotivoExclusion.put("",  getText("midas.general.seleccione"));
			this.lstMotivoExclusion.putAll( this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_EXCUSION_CARTACIA));//

		}else{
			this.cartaRedocumentacion=this.recuperacionCiaService.obtenerCartaActiva(this.recuperacionId);				
			lstTipoRedocumentacion=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.TIPO_REDOCUMENTACION, cartaRedocumentacion.getTipoRedocumentacion());
			this.lstCausaRechazo=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MOTIVO_RECHAZO_CARTACIA, cartaRedocumentacion.getCausaRechazo());
			this.lstTipoRechazoRedocumentacion=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.TIPO_RECHAZO_RED_CIA, cartaRedocumentacion.getTipoRechazo());
			lstMotivoExclusion=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MOTIVO_EXCUSION_CARTACIA, cartaRedocumentacion.getMotivoExclusion());
		}		
		this.lstEstatusCartaRedoc=this.listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, this.cartaCiaActiva.getEstatus());
		
	}
	
	@Action(value="mostrarRegistrarComplemento",results={
			@Result(name=SUCCESS,location=COMPLEMENTAR_RECUPERACION)
			})
	public String mostrarRegistrarComplemento(){
		return SUCCESS;
	}
	
	@Action(value = "imprimirCarta", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirCarta(){
		this.cartaCia=this.recuperacionCiaService.obtenerCartaCompania(recuperacionId);
		cartaCia.setFechaImpresion(new Date());
		transporte = recuperacionCiaService.imprimirCarta(cartaCia.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");
		String fileName = "CartaCia_"+ cartaCia.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value="prepararImpresion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^impresionPreparada"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^impresionPreparada"})
		})
	public String prepararImpresion(){
		try{
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("estatus", CartaCompania.EstatusCartaCompania.IMPRESO.codigo);
			params.put("recuperacion.id", recuperacionId);
			List<CartaCompania>  lstCartaCompania=this.entidadService.findByPropertiesWithOrder(CartaCompania.class, params,"folio desc");
			if(null!=lstCartaCompania && !lstCartaCompania.isEmpty()){
				this.cartaCia=lstCartaCompania.get(0);
			}else{
				this.cartaCia=this.recuperacionCiaService.obtenerCartaCompania(recuperacionId);
				cartaCia.setFechaImpresion(new Date());
			}
			cartaCia.setEstatus(CartaCompania.EstatusCartaCompania.IMPRESO.toString());
			recuperacion = recuperacionCiaService.obtenerRecuperacionCia(recuperacionId);
			recuperacion.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaModificacion(new Date());			
			recuperacion.setPaseAtencionCia(tipoPaseCia);				
			this.entidadService.save(recuperacion);
			cartaCia.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			cartaCia.setFechaModificacion(new Date());	
			this.entidadService.save(cartaCia);
			
			impresionPreparada = true;
		}catch(Exception ex){
			impresionPreparada = false;
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "reimprimirCarta", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String reimprimirCarta(){
		transporte = recuperacionCiaService.imprimirCarta(cartaCia.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");
		String fileName = "CartaCia_"+ cartaCia.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	//TODO ACTION COMPLEMENTO 

	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Invocar al método recuperacionCiaService.cancelarComplemento
	 */
	@Action(value="cancelarComplemento",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","recuperacionId,tipoMensaje,mensaje"})				
		})
	public String cancelarComplemento(){
		
		recuperacionCiaService.cancelarComplemento(recuperacionId);
		
		return SUCCESS;
	}
	
	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Invocar al método recuperacionCiaService.complementarCarta
	 */
	@Action(value="complementarCarta",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","recuperacionId,tipoMensaje,mensaje"})				
		})
	public String complementarCarta(){
		
		RecuperacionCompania recuperacionNueva = recuperacionCiaService.complementarCarta(recuperacionId);
		
		setMensaje(getText("midas.siniestros.recuperacion.compania.registro.complemento.complementarCarta.exito", 
				new String[]{String.valueOf(recuperacionNueva.getNumero())}));
		
		return SUCCESS;
	}
	

	/**
	 * <b>SE AGREGA EN DISEÑO REGISTRAR COMPLEMENTO</b>
	 * 
	 * Invocar al método recuperacionCiaService.regeneraRecuperacion
	 */
	@Action(value="regeneraRecuperacion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","recuperacionId,tipoMensaje,mensaje"})				
		})
	public String regeneraRecuperacion(){
		
		RecuperacionCompania recuperacionNueva = recuperacionCiaService.regeneraRecuperacion(recuperacionId);		
		
		setMensaje(getText("midas.siniestros.recuperacion.compania.registro.complemento.regeneraRecuperacion.exito", 
				new String[]{String.valueOf(recuperacionNueva.getNumero())}));
		
		return SUCCESS;
	}
	
	public RecuperacionRelacionSiniestroDTO getInfoAfectacion() {
		return infoAfectacion;
	}
	public void setInfoAfectacion(RecuperacionRelacionSiniestroDTO infoAfectacion) {
		this.infoAfectacion = infoAfectacion;
	}
	public RecuperacionRelacionSiniestroDTO getInfoImportes() {
		return infoImportes;
	}
	public void setInfoImportes(RecuperacionRelacionSiniestroDTO infoImportes) {
		this.infoImportes = infoImportes;
	}
	public CartaCompania getCartaCia() {
		return cartaCia;
	}
	public void setCartaCia(CartaCompania cartaCia) {
		this.cartaCia = cartaCia;
	}
	public String getComentarioSeguimiento() {
		return comentarioSeguimiento;
	}
	public void setComentarioSeguimiento(String comentarioSeguimiento) {
		this.comentarioSeguimiento = comentarioSeguimiento;
	}
	public RecuperacionRelacionSiniestroDTO getDatosEstimacion() {
		return datosEstimacion;
	}
	public void setDatosEstimacion(RecuperacionRelacionSiniestroDTO datosEstimacion) {
		this.datosEstimacion = datosEstimacion;
	}
	public Integer getPorcentajeParticipacion() {
		return porcentajeParticipacion;
	}
	public void setPorcentajeParticipacion(Integer porcentajeParticipacion) {
		this.porcentajeParticipacion = porcentajeParticipacion;
	}
	public String getEstatusCarta() {
		return estatusCarta;
	}
	public void setEstatusCarta(String estatusCarta) {
		this.estatusCarta = estatusCarta;
	}
	public Date getFechaAcuse() {
		return fechaAcuse;
	}
	public void setFechaAcuse(Date fechaAcuse) {
		this.fechaAcuse = fechaAcuse;
	}
	public Map<String, String> getLstCausaRechazo() {
		return lstCausaRechazo;
	}
	public void setLstCausaRechazo(Map<String, String> lstCausaRechazo) {
		this.lstCausaRechazo = lstCausaRechazo;
	}
	public Map<String, String> getLstCausasNoElaboracion() {
		return lstCausasNoElaboracion;
	}
	public void setLstCausasNoElaboracion(Map<String, String> lstCausasNoElaboracion) {
		this.lstCausasNoElaboracion = lstCausasNoElaboracion;
	}
	public Map<String, String> getLstCausas() {
		return lstCausas;
	}
	public void setLstCausas(Map<String, String> lstCausas) {
		this.lstCausas = lstCausas;
	}
	public Map<String, String> getLstEstatusCarta() {
		return lstEstatusCarta;
	}
	public void setLstEstatusCarta(Map<String, String> lstEstatusCarta) {
		this.lstEstatusCarta = lstEstatusCarta;
	}
	public Map<String, String> getLstMotivoExclusion() {
		return lstMotivoExclusion;
	}
	public void setLstMotivoExclusion(Map<String, String> lstMotivoExclusion) {
		this.lstMotivoExclusion = lstMotivoExclusion;
	}
	public Map<String, String> getLstMotivosCanc() {
		return lstMotivosCanc;
	}
	public void setLstMotivosCanc(Map<String, String> lstMotivosCanc) {
		this.lstMotivosCanc = lstMotivosCanc;
	}
	public Map<Long, String> getLstCiaDeSeguros() {
		return lstCiaDeSeguros;
	}
	public void setLstCiaDeSeguros(Map<Long, String> lstCiaDeSeguros) {
		this.lstCiaDeSeguros = lstCiaDeSeguros;
	}
	public List<OrdenCompraCartaCia> getLstOrdenesCompra() {
		return lstOrdenesCompra;
	}
	public void setLstOrdenesCompra(List<OrdenCompraCartaCia> lstOrdenesCompra) {
		this.lstOrdenesCompra = lstOrdenesCompra;
	}
	public List<SeguimientoRecuperacion> getLstSeguimientos() {
		return lstSeguimientos;
	}
	public void setLstSeguimientos(List<SeguimientoRecuperacion> lstSeguimientos) {
		this.lstSeguimientos = lstSeguimientos;
	}
	public Map<String, String> getLstTipoRechazo() {
		return lstTipoRechazo;
	}
	public void setLstTipoRechazo(Map<String, String> lstTipoRechazo) {
		this.lstTipoRechazo = lstTipoRechazo;
	}
	public String getOrdenesCiaConcat() {
		return ordenesCiaConcat;
	}
	public void setOrdenesCiaConcat(String ordenesCiaConcat) {
		this.ordenesCiaConcat = ordenesCiaConcat;
	}
	public RecuperacionCompania getRecuperacion() {
		return recuperacion;
	}
	public void setRecuperacion(RecuperacionCompania recuperacion) {
		this.recuperacion = recuperacion;
	}
	public List<RecuperacionCompania> getRecuperacionesVencer() {
		return recuperacionesVencer;
	}
	public void setRecuperacionesVencer(
			List<RecuperacionCompania> recuperacionesVencer) {
		this.recuperacionesVencer = recuperacionesVencer;
	}
	public String getTipoRedocumentacion() {
		return tipoRedocumentacion;
	}
	public void setTipoRedocumentacion(String tipoRedocumentacion) {
		this.tipoRedocumentacion = tipoRedocumentacion;
	}
	public Long getPrestadorServicioId() {
		return prestadorServicioId;
	}
	public void setPrestadorServicioId(Long prestadorServicioId) {
		this.prestadorServicioId = prestadorServicioId;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getTermioAjuste() {
		return termioAjuste;
	}
	public void setTermioAjuste(String termioAjuste) {
		this.termioAjuste = termioAjuste;
	}

	public Long getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}

	public Long getIdPaseAtencion() {
		return idPaseAtencion;
	}

	public void setIdPaseAtencion(Long idPaseAtencion) {
		this.idPaseAtencion = idPaseAtencion;
	}

	public Map<String, String> getLstTipoPaseCia() {
		return lstTipoPaseCia;
	}

	public void setLstTipoPaseCia(Map<String, String> lstTipoPaseCia) {
		this.lstTipoPaseCia = lstTipoPaseCia;
	}

	public String getTipoPaseCia() {
		return tipoPaseCia;
	}

	public void setTipoPaseCia(String tipoPaseCia) {
		this.tipoPaseCia = tipoPaseCia;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public boolean isTienePermisoSolCancela() {
		return tienePermisoSolCancela;
	}

	public void setTienePermisoSolCancela(boolean tienePermisoSolCancela) {
		this.tienePermisoSolCancela = tienePermisoSolCancela;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCreationDateTxt(String creationDateTxt) {
		this.creationDateTxt = creationDateTxt;
	}

	public String getCreationDateTxt() {
		return creationDateTxt;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public CartaCompania getCartaRedocumentacion() {
		return cartaRedocumentacion;
	}

	public void setCartaRedocumentacion(CartaCompania cartaRedocumentacion) {
		this.cartaRedocumentacion = cartaRedocumentacion;
	}

	public Map<String, String> getLstTipoRedocumentacion() {
		return lstTipoRedocumentacion;
	}

	public void setLstTipoRedocumentacion(Map<String, String> lstTipoRedocumentacion) {
		this.lstTipoRedocumentacion = lstTipoRedocumentacion;
	}

	public Map<String, String> getLstTipoRechazoRedocumentacion() {
		return lstTipoRechazoRedocumentacion;
	}

	public void setLstTipoRechazoRedocumentacion(
			Map<String, String> lstTipoRechazoRedocumentacion) {
		this.lstTipoRechazoRedocumentacion = lstTipoRechazoRedocumentacion;
	}

	public BigDecimal getMontoRegistradoRedoc() {
		return montoRegistradoRedoc;
	}

	public void setMontoRegistradoRedoc(BigDecimal montoRegistradoRedoc) {
		this.montoRegistradoRedoc = montoRegistradoRedoc;
	}

	public BigDecimal getNuevoMontoRecuperarRedoc() {
		return nuevoMontoRecuperarRedoc;
	}

	public void setNuevoMontoRecuperarRedoc(BigDecimal nuevoMontoRecuperarRedoc) {
		this.nuevoMontoRecuperarRedoc = nuevoMontoRecuperarRedoc;
	}

	public CartaCompania getCartaCiaActiva() {
		return cartaCiaActiva;
	}

	public void setCartaCiaActiva(CartaCompania cartaCiaActiva) {
		this.cartaCiaActiva = cartaCiaActiva;
	}

	public Map<String, String> getLstEstatusCartaRedoc() {
		return lstEstatusCartaRedoc;
	}

	public void setLstEstatusCartaRedoc(Map<String, String> lstEstatusCartaRedoc) {
		this.lstEstatusCartaRedoc = lstEstatusCartaRedoc;
	}

	public CartaCompania getPrimeraCarta() {
		return primeraCarta;
	}

	public void setPrimeraCarta(CartaCompania primeraCarta) {
		this.primeraCarta = primeraCarta;
	}

	public List<CartaCompania> getLstCartaCompania() {
		return lstCartaCompania;
	}

	public void setLstCartaCompania(List<CartaCompania> lstCartaCompania) {
		this.lstCartaCompania = lstCartaCompania;
	}

	public boolean isVerRedocumentacion() {
		return verRedocumentacion;
	}

	public void setVerRedocumentacion(boolean verRedocumentacion) {
		this.verRedocumentacion = verRedocumentacion;
	}

	public ComplementoCompania getComplementoCompania() {
		return complementoCompania;
	}

	public void setComplementoCompania(ComplementoCompania complementoCompania) {
		this.complementoCompania = complementoCompania;
	}

	public boolean isTieneComplemento() {
		return tieneComplemento;
	}

	public void setTieneComplemento(boolean tieneComplemento) {
		this.tieneComplemento = tieneComplemento;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the impresionPreparada
	 */
	public Boolean getImpresionPreparada() {
		return impresionPreparada;
	}

	/**
	 * @param impresionPreparada the impresionPreparada to set
	 */
	public void setImpresionPreparada(Boolean impresionPreparada) {
		this.impresionPreparada = impresionPreparada;
	}

	public String getNombreCia() {
		return nombreCia;
	}

	public void setNombreCia(String nombreCia) {
		this.nombreCia = nombreCia;
	}

	public String getValidaExclusionRechazo() {
		return validaExclusionRechazo;
	}

	public void setValidaExclusionRechazo(String validaExclusionRechazo) {
		this.validaExclusionRechazo = validaExclusionRechazo;
	}

	public BigDecimal getReservaDisponible() {
		return reservaDisponible;
	}

	public void setReservaDisponible(BigDecimal reservaDisponible) {
		this.reservaDisponible = reservaDisponible;
	}

	
}