package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania.EstatusCartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.MedioRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionCiaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/listado/recuperacion/compania")
public class ListarRecuperacionCiaAction extends BaseAction implements Preparable {
	
	private static final long			serialVersionUID				= -2658477756130705926L;
	private static final String			NAMESPACE						= "/siniestros/recuperacion/listado/recuperacion/compania";
	private static final String			ACTION_ASIGNAR_REFERENCIA		= "asignarReferencia";
	private static final String			ACTION_ELIMINAR_REFERENCIA		= "eliminarReferencia";	
	private static final String			ACTION_MOSTRAR_CONTENEDOR		= "mostrarContenedor";
	private static final String			ACTION_ENTREGAR_CARTAS			= "entregarCartas";
	private static final String			ACTION_REASIGNAR_CARTAS			= "reasignarCartas";
	private static final String			ACTION_REGISTRARENVIO_CARTAS	= "registrarEnvioCartas";
	private static final String			ACTION_ELIMINARENVIO_CARTAS		= "eliminarEnvioCartas";
	private static final String			ACTION_REGISTRARRECEP_CARTAS	= "registrarRecepcionCartas";
	private static final String			ACTION_ELIMINARRECEP_CARTAS		= "eliminarRecepcionCartas";
	private static final String			ACTION_BUSCAR					= "buscar";
	private static final String			LOCATION_JSP					= "/jsp/siniestros/recuperacion/compania/";
	private static final String			LOCATION_CONTENEDORBUSQUEDA_JSP	= LOCATION_JSP + "contenedorListarRecuperacionCia.jsp";
	private static final String			LOCATION_RESULTADOBUSQUEDA_GRID	= LOCATION_JSP + "listarRecuperacionCiaGrid.jsp";	

	public static final Short			MODULO_PROGRAMACION_INGRESO     = 1;
	public static final Short			MODULO_RECEPCION_CARTA          = 2;
	public static final Short			MODULO_REASIGNACION_OFICINA     = 3;
	public static final Short			MODULO_ENVIORECEPCION_CARTA     = 4;

	@Autowired
	@Qualifier("recuperacionCiaServiceEJB")
	private RecuperacionCiaService		recuperacionCiaService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService				listadoService;
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService   grupoValorService;

	private Map<String, String>			lstTipoRecuperacion;
	private Map<String, String>			lstMedioRecuperacion;
	private Map<String, String>			lstEstatusRecuperacion;
	private Map<String, String>			lstTipoServicio;
	private Map<Long, String>			lstOficinas;
	private Map<String, String>			lstEstatusCarta;
	private String						referenciaBancaria;
	private String						recuperacionesConcat;
	private RecuperacionCiaDTO			recuperacionCiaFiltro;
	private List<RecuperacionCiaDTO>	recuperaciones;
	private Short						tipoModulo;
	private Date						fechaAcuse;
	private Long						oficinaReasignacionId;
	private Long						oficinaRecepcion;
	private Short						tipoEnvioRecepcion;
	private TransporteImpresionDTO      transporteExcel;
	private String						personaRecepcion;
	private String						fechaEnvioRecepcion;
	
	
	@Override
	public void prepare(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaEnvioRecepcion = sdf.format(new Date());
		llenarListados();
	}

	/**
	 * Actions
	 * 
	 * 
	 * ********************************************************************************************************/
	@Action(value = ACTION_REGISTRARENVIO_CARTAS, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String registrarEnvioCartas(){
		String respuesta = recuperacionCiaService.registrarEnvioCarta(recuperacionesConcat, oficinaRecepcion);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se registró correctamente la información en las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}
	
	@Action(value = ACTION_ELIMINARENVIO_CARTAS, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String eliminarEnvioCartas(){
		String respuesta = recuperacionCiaService.eliminarEnvioCarta(recuperacionesConcat);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se registró correctamente la información en las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}
	
	@Action(value = ACTION_REGISTRARRECEP_CARTAS, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String registrarRecepcionCartas(){
		String respuesta = recuperacionCiaService.registrarRecepcionCarta(recuperacionesConcat, personaRecepcion);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se registró correctamente la información en las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}
	
	@Action(value = ACTION_ELIMINARRECEP_CARTAS, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String eliminarRecepcionCartas(){
		String respuesta = recuperacionCiaService.eliminarRecepcionCarta(recuperacionesConcat);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se registró correctamente la información en las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}
	
	@Action(value = ACTION_ENTREGAR_CARTAS, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String entregarCartas(){
		String respuesta = recuperacionCiaService.entregarCartas(recuperacionesConcat, fechaAcuse);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se registró correctamente la información en las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}
	
	@Action(value = ACTION_REASIGNAR_CARTAS, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String reasignarCartas(){
		String respuesta = recuperacionCiaService.reasignarCartas(recuperacionesConcat, oficinaReasignacionId);
		
		if(!respuesta.equals("")){
			setMensajeError(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se reasignaron Recuperaciones seleccionadas a la Oficina indicada");
		}
		
		return SUCCESS;
	}	
	
	@Action(value = ACTION_ASIGNAR_REFERENCIA, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String asignarReferencia(){
		String respuesta = recuperacionCiaService.asignarReferenciaBancaria(recuperacionesConcat, referenciaBancaria);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se asignó correctamente la Referencia Bancaria a las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}

	@Action(value = ACTION_ELIMINAR_REFERENCIA, results = { 
			@Result(name = SUCCESS, 
					type = "redirectAction", 
					params = {"actionName", ACTION_MOSTRAR_CONTENEDOR, 
							  "namespace",	NAMESPACE,
							  "mensaje","${mensaje}",
							  "tipoMensaje","${tipoMensaje}",
							  "tipoModulo","${tipoModulo}"})})
	public String eliminarReferencia(){
		String respuesta = recuperacionCiaService.eliminarReferenciaBancaria(recuperacionesConcat);
		if(SystemCommonUtils.isNotNull(respuesta)){
			setMensajeExitoPersonalizado(respuesta);
		}else{
			setMensajeExitoPersonalizado("Se eliminó correctamente la Referencia Bancaria de las Recuperaciones seleccionadas");
		}
		return SUCCESS;
	}

	@Action(value = ACTION_BUSCAR, results = { 
			@Result(name = SUCCESS, location = LOCATION_RESULTADOBUSQUEDA_GRID)})
	public String buscar(){
		if(SystemCommonUtils.isNotNull(recuperacionCiaFiltro)){
			recuperacionCiaFiltro.setTipoModulo(tipoModulo);
			recuperaciones = recuperacionCiaService.buscarRecuperacionesCia(recuperacionCiaFiltro);
		}
		return SUCCESS;
	}
	
	@Action(value = ACTION_MOSTRAR_CONTENEDOR, results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORBUSQUEDA_JSP)})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	@Action(value="exportarExcel",
			results={@Result(name=SUCCESS,	type="stream",	params={"contentType","${transporteExcel.contentType}",
																	"inputName","transporteExcel.genericInputStream",
																	"contentDisposition", "attachment;filename=\"${transporteExcel.fileName}\""
						}
				)
			}
	)
	public String exportarExcel(){
		recuperaciones = recuperacionCiaService.buscarRecuperacionesCia(recuperacionCiaFiltro);
		String documento = null;
		switch(tipoModulo){
			case 1:
				documento = RecuperacionCiaDTO.EXPORTABLE_LISTADO_REC_CIA_PROGRAMAR;
				break;
			case 2:
				documento = RecuperacionCiaDTO.EXPORTABLE_LISTADO_REC_CIA_ENTREGACARTAS;
				break;
			case 3:
				documento = RecuperacionCiaDTO.EXPORTABLE_LISTADO_REC_CIA_REELABCARTAS;
				break;
			case 4:
				documento = RecuperacionCiaDTO.EXPORTABLE_LISTADO_REC_CIA_ENVRECCARTAS;
				break;
		}
		ExcelExporter exporter = new ExcelExporter(RecuperacionCiaDTO.class, documento);
		this.transporteExcel = exporter.exportXLS(recuperaciones, documento);
		
		return SUCCESS;
	}
	
	/**
	 * Métodos Privados
	 * 
	 * 
	 * ********************************************************************************************************/
	
	private void llenarListados(){
		
		lstTipoServicio = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_SERVICIO_POLIZA); 
		lstOficinas = listadoService.obtenerOficinasSiniestros();		
		
		this.lstTipoRecuperacion = new LinkedHashMap<String, String>();
		CatValorFijo valorTipo = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION, TipoRecuperacion.COMPANIA.getValue());
		this.lstTipoRecuperacion.put(valorTipo.getCodigo(), valorTipo.getDescripcion());
		
		this.lstMedioRecuperacion = new LinkedHashMap<String, String>();
		CatValorFijo valorMedio = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION, MedioRecuperacion.REEMBOLSO.toString());
		this.lstMedioRecuperacion.put(valorMedio.getCodigo(), valorMedio.getDescripcion());
		
		this.lstEstatusRecuperacion = new LinkedHashMap<String, String>();
		this.lstEstatusCarta = new LinkedHashMap<String, String>();
		
		CatValorFijo valorEstatusRegistrado = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, EstatusRecuperacion.REGISTRADO.getValue());
		CatValorFijo valorEstatusPendiente = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, EstatusRecuperacion.PENDIENTE.getValue());
		CatValorFijo valorEstatusPendienteElab = grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, EstatusCartaCompania.PENDIENTE_ELABORACION.getValue());
		CatValorFijo valorEstatusEntregado= grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, EstatusCartaCompania.ENTREGADO.getValue());
		CatValorFijo valorEstatusRedocumentar= grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, EstatusCartaCompania.POR_REDOCUMENTAR.getValue());
		CatValorFijo valorEstatusImpreso= grupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, EstatusCartaCompania.IMPRESO.getValue());
		
		if (this.tipoModulo != null) {
			if (tipoModulo == MODULO_PROGRAMACION_INGRESO) {
				lstEstatusRecuperacion.put(valorEstatusPendiente.getCodigo(), valorEstatusPendiente.getDescripcion());
				lstEstatusCarta.put(valorEstatusEntregado.getCodigo(), valorEstatusEntregado.getDescripcion());
				lstEstatusCarta.put(valorEstatusRedocumentar.getCodigo(), valorEstatusRedocumentar.getDescripcion());
			}
			
			if (tipoModulo == MODULO_RECEPCION_CARTA) {
				
				lstEstatusRecuperacion.put(valorEstatusRegistrado.getCodigo(), valorEstatusRegistrado.getDescripcion());
				lstEstatusCarta.put(valorEstatusImpreso.getCodigo(), valorEstatusImpreso.getDescripcion());
			}
			
			if (tipoModulo == MODULO_REASIGNACION_OFICINA) {
				
				lstEstatusRecuperacion.put(valorEstatusRegistrado.getCodigo(), valorEstatusRegistrado.getDescripcion());
				lstEstatusCarta.put(valorEstatusPendienteElab.getCodigo(), valorEstatusPendienteElab.getDescripcion());
			}
			if (tipoModulo == MODULO_ENVIORECEPCION_CARTA) {
				
				lstEstatusRecuperacion.put(valorEstatusRegistrado.getCodigo(), valorEstatusRegistrado.getDescripcion());
				lstEstatusCarta.put(valorEstatusImpreso.getCodigo(), valorEstatusImpreso.getDescripcion());
			}				
		} else {
			lstEstatusRecuperacion.put(valorEstatusRegistrado.getCodigo(), valorEstatusRegistrado.getDescripcion());
			lstEstatusCarta.put(valorEstatusPendienteElab.getCodigo(), valorEstatusPendienteElab.getDescripcion());
		}
	}
	
	/**
	 * gets y sets
	 * 
	 * 
	 * ********************************************************************************************************/
	public Map<String, String> getLstTipoRecuperacion() {
		return lstTipoRecuperacion;
	}

	public void setLstTipoRecuperacion(Map<String, String> lstTipoRecuperacion) {
		this.lstTipoRecuperacion = lstTipoRecuperacion;
	}

	public Map<String, String> getLstMedioRecuperacion() {
		return lstMedioRecuperacion;
	}

	public void setLstMedioRecuperacion(Map<String, String> lstMedioRecuperacion) {
		this.lstMedioRecuperacion = lstMedioRecuperacion;
	}

	public Map<String, String> getLstEstatusRecuperacion() {
		return lstEstatusRecuperacion;
	}

	public void setLstEstatusRecuperacion(Map<String, String> lstEstatusRecuperacion) {
		this.lstEstatusRecuperacion = lstEstatusRecuperacion;
	}

	public Map<String, String> getLstTipoServicio() {
		return lstTipoServicio;
	}

	public void setLstTipoServicio(Map<String, String> lstTipoServicio) {
		this.lstTipoServicio = lstTipoServicio;
	}

	public Map<Long, String> getLstOficinas() {
		return lstOficinas;
	}

	public void setLstOficinas(Map<Long, String> lstOficinas) {
		this.lstOficinas = lstOficinas;
	}

	public Map<String, String> getLstEstatusCarta() {
		return lstEstatusCarta;
	}

	public void setLstEstatusCarta(Map<String, String> lstEstatusCarta) {
		this.lstEstatusCarta = lstEstatusCarta;
	}

	public String getReferenciaBancaria() {
		return referenciaBancaria;
	}

	public void setReferenciaBancaria(String referenciaBancaria) {
		this.referenciaBancaria = referenciaBancaria;
	}

	public String getRecuperacionesConcat() {
		return recuperacionesConcat;
	}

	public void setRecuperacionesConcat(String recuperacionesConcat) {
		this.recuperacionesConcat = recuperacionesConcat;
	}

	public RecuperacionCiaDTO getRecuperacionCiaFiltro() {
		return recuperacionCiaFiltro;
	}

	public void setRecuperacionCiaFiltro(RecuperacionCiaDTO recuperacionCiaFiltro) {
		this.recuperacionCiaFiltro = recuperacionCiaFiltro;
	}

	public List<RecuperacionCiaDTO> getRecuperaciones() {
		return recuperaciones;
	}

	public void setRecuperaciones(List<RecuperacionCiaDTO> recuperaciones) {
		this.recuperaciones = recuperaciones;
	}

	public Short getTipoModulo() {
		return tipoModulo;
	}

	public void setTipoModulo(Short tipoModulo) {
		this.tipoModulo = tipoModulo;
	}

	public Date getFechaAcuse() {
		return fechaAcuse;
	}

	public void setFechaAcuse(Date fechaAcuse) {
		this.fechaAcuse = fechaAcuse;
	}

	public Long getOficinaReasignacionId() {
		return oficinaReasignacionId;
	}

	public void setOficinaReasignacionId(Long oficinaReasignacionId) {
		this.oficinaReasignacionId = oficinaReasignacionId;
	}

	public Long getOficinaRecepcion() {
		return oficinaRecepcion;
	}

	public void setOficinaRecepcion(Long oficinaRecepcion) {
		this.oficinaRecepcion = oficinaRecepcion;
	}

	public Short getTipoEnvioRecepcion() {
		return tipoEnvioRecepcion;
	}

	public void setTipoEnvioRecepcion(Short tipoEnvioRecepcion) {
		this.tipoEnvioRecepcion = tipoEnvioRecepcion;
	}

	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public String getPersonaRecepcion() {
		return personaRecepcion;
	}

	public void setPersonaRecepcion(String personaRecepcion) {
		this.personaRecepcion = personaRecepcion;
	}

	public String getFechaEnvioRecepcion() {
		return fechaEnvioRecepcion;
	}

	public void setFechaEnvioRecepcion(String fechaEnvioRecepcion) {
		this.fechaEnvioRecepcion = fechaEnvioRecepcion;
	}

}
