package mx.com.afirme.midas2.action.siniestros.recuperacion.ingresos;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.DepositoBancarioDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.ImportesIngresosDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoAcreedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoManualDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/ingresos")
public class IngresoAction extends BaseAction implements Preparable{
	private static final long serialVersionUID = 2584828363828115245L;	
	private static final String	LOCATION_CONTENEDORINGRESOS_JSP = "/jsp/siniestros/recuperacion/ingresos/contenedorIngresos.jsp";
	private static final String	LOCATION_CONTENEDORFACTURACION_JSP = "/jsp/siniestros/recuperacion/ingresos/contenedorIngresosFacturacion.jsp";
	private static final String	LOCATION_CONTENEDORINGRESOSPENDIENTESGRID_JSP = "/jsp/siniestros/recuperacion/ingresos/ingresosPendientesGrid.jsp";
	private static final String	LOCATION_CONTENEDORDEPOSITOSBANCARIOS_JSP = "/jsp/siniestros/recuperacion/ingresos/contenedorDepositos.jsp";
	private static final String	LOCATION_CONTENEDORDEPOSITOSBANCARIOSGRID_JSP = "/jsp/siniestros/recuperacion/ingresos/depositosBancariosGrid.jsp";
	private static final String	LOCATION_CONTENEDORMOVSACREEDORES_JSP = "/jsp/siniestros/recuperacion/ingresos/contenedorMovsAcreedores.jsp";
	private static final String	LOCATION_CONTENEDORMOVSMANUALES_JSP = "/jsp/siniestros/recuperacion/ingresos/contenedorMovtoManual.jsp";
	private static final String	LOCATION_CONTENEDORMOVSACREEDORESGRID_JSP = "/jsp/siniestros/recuperacion/ingresos/movsAcreedoresGrid.jsp";
	private static final String	LOCATION_CONTENEDORMOVSMANUALESGRID_JSP = "/jsp/siniestros/recuperacion/ingresos/movtosManualesGrid.jsp";
	private static final String	LOCATION_ALTAMOVTOMANUAL_JSP = "/jsp/siniestros/recuperacion/ingresos/altaMovtoManual.jsp";
	private String mensajeAplicacion ;
	private ListarIngresoDTO filtroIngresosPendientes;
	private ListarIngresoDTO ingresosPendDto;
	private DepositoBancarioDTO filtroDepositos;
	private MovimientoAcreedorDTO filtroMovAcreedor;
	private MovimientoManualDTO filtroMovManual;
	private Ingreso ingreso;
	private Long ingresoId;
	private Map<String,String> listaCuentasDepositos;
	private List<DepositoBancarioDTO> listaDepositos;
	private List<ListarIngresoDTO> listaIngresos;
	private List<MovimientoAcreedorDTO> listaMovAcreedores;
	private List<MovimientoManualDTO> listaMovManuales;
	private Map<String,String> listaMotivosCancelacion;
	private Map<Long, String> bancos;
	private Map<String,String> listaMedioRecuperacion;
	private Map<Long,String> listaOficinas;
	private Map<String,String> listaReferenciasIdentif = new HashMap<String,String>();;
	private Map<String,String> listaTipoRecuperacion;
	private Map<String,String> listaMotivoCancelacion;
	private Map<Long,String> listaCuentaAcredora;
	private Map<Long,String> listaCuentaManual;
	private Boolean soloConsulta;
	private Boolean consultaSinFiltro;
	private Integer tipoDeCancelacion = 0;
	private Boolean ingresosIndentificadosCheck;
	private ImportesIngresosDTO importesDTO;
	private Long idToReporte;
	private int variacion ;
	private Map<String,String> opcionesCancelarIngreso;
	public static final int MIDAS_APP_ID = 5;
	public static final String PARAMETRO = "VARIACION_APL_INGRESO_MANUAL";
	public static final String PANTALLA_ORIGEN_INGRESOS_PENDIENTES = "PENDIENTE";
	private String origenRecuperacionDeducible = "";
	
	@Autowired
	@Qualifier("ingresoServiceEJB")
	private IngresoService ingresoService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	private ParametroGlobalService parametroGlobalService ; 
	
	@Autowired
	@Qualifier("recuperacionCiaServiceEJB")
	private RecuperacionCiaService recuperacionCiaService;
	
	
	/**
	 * carga los listados necesarios para presentar la pantalla de busqueda
	 */
	private void obtenerListados(){
		listaCuentaAcredora = listadoService.getMapCuentasAcreedorasSiniestros();
		listaMedioRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MEDIO_RECUPERACION);
		listaOficinas = listadoService.obtenerOficinasSiniestros();
		listaTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECUPERACION);	
		listaMotivoCancelacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO);
		List<String> referencias = recuperacionCiaService.obtenerReferenciasIdentificadas();
		
		for (String referencia: referencias) {
			listaReferenciasIdentif.put(referencia, referencia);
		}
		
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);	
		this.variacion=1;
		if (null!=value && StringUtils.isNumeric(value)){
			variacion= Integer.parseInt(value);
		}
	}
	
	private void obtenerListadosDepositosBancarios(){
		bancos = listadoService.getMapBancosMidas();		
	}
	
	private void obtenerListadosMovsAcreedores(){
		listaTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECUPERACION);
		listaMotivosCancelacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO);
		listaCuentaAcredora = listadoService.getMapCuentasAcreedorasSiniestros();
	}
	
	@Override
	public void prepare() throws Exception {
	}
	
	public void prepareMostrarContenedor(){
		this.obtenerListados();
		//VALIDAR SI EL INGRESO TIENE RECUPERACION DE DEDUCIBLE Y SU ORIGEN
		this.origenRecuperacionDeducible = this.ingresoService.obtenerOrigenDedudicleRecuperacionIngreso( this.ingresoId );
	}
	
	public void prepareMostrarDepositosBancarios(){
		this.obtenerListadosDepositosBancarios();
	}
	
	public void prepareMostrarMovsAcreedores(){
		this.obtenerListadosMovsAcreedores();
	}
	
	public void prepareMostrarMovsManuales(){
		listaCuentaManual = listadoService.getMapCuentasManualesSiniestros();
	}
	
	@Action(value = "mostrarContenedor", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORINGRESOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORINGRESOS_JSP)})
	public String mostrarContenedor(){
		
		opcionesCancelarIngreso = new HashMap<String,String>();
		
		if(null!=this.ingresoId){
			List<ListarIngresoDTO> ingresos=this.ingresoService.buscarIngresos(this.ingresoId.toString());
			if(!ingresos.isEmpty()){
				this.ingresosPendDto=ingresos.get(0);
				if(this.soloConsulta){
					this.filtroIngresosPendientes=ingresos.get(0);
					filtroIngresosPendientes.setMontoIniIngreso(filtroIngresosPendientes.getMontoIniRecuperacion());
					filtroIngresosPendientes.setMontoFinIngreso(filtroIngresosPendientes.getMontoFinalRecuperado());
					//Buscando el reporte para la consulta de historico de movimientos
					Ingreso ingreso = ingresoService.obtenerIngreso(ingresoId); 
					if(ingreso != null
							&& ingreso.getRecuperacion() != null
							&& ingreso.getRecuperacion().getReporteCabina() != null){
						
						
						opcionesCancelarIngreso.put("1","Aplicar a Todos");
						opcionesCancelarIngreso.put("2","Ingreso Seleccionado(No.Ingreso = "+ingreso.getNumero()+")");
						
						idToReporte = ingreso.getRecuperacion().getReporteCabina().getId();
					}
				}
			}else{
				ingresosPendDto=new ListarIngresoDTO();
				ingresosPendDto.setEstatusDesc(Ingreso.EstatusIngreso.PENDIENTE.toString());
				
			}
		}else {
			ingresosPendDto=new ListarIngresoDTO();
			ingresosPendDto.setEstatusDesc(Ingreso.EstatusIngreso.PENDIENTE.toString());
		}
		
		return SUCCESS;
	}
	
	@Action(value = "mostrarFacturacion", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORFACTURACION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORFACTURACION_JSP)})
	public String mostrarFacturacion(){
		return SUCCESS;
	}
	
	@Action(value = "buscarIngresosPendientes", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORINGRESOSPENDIENTESGRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORINGRESOSPENDIENTESGRID_JSP)})
	public String buscarIngresosPendientes(){
		if(this.soloConsulta && null!= this.ingresoId){
			listaIngresos =this.ingresoService.obtenerIngresosAplicacion(this.ingresoId);
		}else{
			if( null!= this.ingresoId){
				listaIngresos = ingresoService.buscarIngresos(ingresoId.toString());
			}else {
				filtroIngresosPendientes.setEstatus(Ingreso.EstatusIngreso.PENDIENTE.getValue());
				filtroIngresosPendientes.setPantallaOrigen(PANTALLA_ORIGEN_INGRESOS_PENDIENTES);
				listaIngresos = ingresoService.buscarIngresos(filtroIngresosPendientes, filtroIngresosPendientes.getIngresosConcat());
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarDepositosBancarios", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORDEPOSITOSBANCARIOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORDEPOSITOSBANCARIOS_JSP)})
	public String mostrarDepositosBancarios(){
		return SUCCESS;
	}
	
	@Action(value = "buscarDepositosBancarios", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORDEPOSITOSBANCARIOSGRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORDEPOSITOSBANCARIOSGRID_JSP)})
	public String buscarDepositosBancarios(){
		if(this.soloConsulta && null!= this.ingresoId){
			listaDepositos= this.ingresoService.obtenerDepositosAplicacion(ingresoId);
		}else{
			if(null== this.consultaSinFiltro){
				this.consultaSinFiltro=Boolean.FALSE;
			}			
			listaDepositos = ingresoService.buscarDepositosBancarios(filtroDepositos, filtroDepositos.getDepositosConcat(), consultaSinFiltro);
		}
		return SUCCESS;
	}

    @Action(value = "aplicarIngreso", results = { 
			@Result(name = INPUT, location = LOCATION_CONTENEDORINGRESOS_JSP) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/ingresos",
					"mensaje","${mensaje}"}) })	
	public String aplicarIngreso(){
		String mensaje;
		try {
			mensaje = this.ingresoService.aplicarIngreso(filtroIngresosPendientes.getIngresosConcat(), 
					filtroDepositos.getDepositosConcat(), filtroMovAcreedor.getMovAcreedoresConcat(), filtroMovManual.getMovManualConcat());
		} catch (MidasException e) {
			setMensajeError(e.getMessage());
			return SUCCESS;
		}
		if(!StringUtils.isEmpty(mensaje)){
			setMensaje(mensaje);
			return INPUT;
		}else {
			setMensaje("Los ingresos han sido aplicados correctamente ");

			return SUCCESS;
			
		}
		
	}
	@Action (value = "calcularImportes", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^importesDTO.*"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^importesDTO.*"})
      })
	public String calcularImportes(){
		if(null!=soloConsulta && this.soloConsulta){
			//this.importesDTO=this.calculaImportesSoloConsulta(this.ingresoId);
			this.importesDTO= this.ingresoService.calculaImportes(this.ingresoId.toString(), null,null,null,true);

		}else {
			this.importesDTO= this.ingresoService.calculaImportes(filtroIngresosPendientes.getIngresosConcat(), filtroDepositos.getDepositosConcat(),
					filtroMovAcreedor.getMovAcreedoresConcat(), filtroMovManual.getMovManualConcat(), false);
		}
		return SUCCESS;
	}
	
	
	@Action (value = "validaAplicacionIngresos", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeAplicacion"})
      })
	public String validaAplicacionIngresos(){
		try {
			this.mensajeAplicacion= this.ingresoService.validaAplicarIngreso(filtroIngresosPendientes.getIngresosConcat(), 
					filtroDepositos.getDepositosConcat(), filtroMovAcreedor.getMovAcreedoresConcat(), filtroMovManual.getMovManualConcat());
		} catch (Exception e) {
			this.mensajeAplicacion=e.getMessage();
		}
		return SUCCESS;
	}
	

	@Action(value = "mostrarMovsAcreedores", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOVSACREEDORES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOVSACREEDORES_JSP)})
	public String mostrarMovsAcreedores(){
		return SUCCESS;
	}
	
	@Action(value = "mostrarMovsManuales", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOVSMANUALES_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOVSMANUALES_JSP)})
	public String mostrarMovsManuales(){
		return SUCCESS;
	}
	
	@Action(value = "buscarMovsAcreedores", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOVSACREEDORESGRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOVSACREEDORESGRID_JSP)})
	public String buscarMovsAcreedores(){
		if(this.soloConsulta && null!= this.ingresoId){
			listaMovAcreedores= this.ingresoService.obtenerMovtoAcreedorAplicacion(ingresoId);
		}else{
			if(null== this.consultaSinFiltro){
				this.consultaSinFiltro=Boolean.FALSE;
			}			
			listaMovAcreedores = ingresoService.buscarMovimientosAcreedores(filtroMovAcreedor, filtroMovAcreedor.getMovAcreedoresConcat(), consultaSinFiltro);
		}
		return SUCCESS;
	}
	
	@Action(value = "buscarMovsManuales", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOVSMANUALESGRID_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOVSMANUALESGRID_JSP)})
	public String buscarMovsManuales(){
		if( this.soloConsulta && null != this.ingresoId) {
			listaMovManuales= ingresoService.obtenerMovtoManualAplicacion(ingresoId);
		} else {
			if(null== this.consultaSinFiltro){
				this.consultaSinFiltro=Boolean.FALSE;
			}			
			listaMovManuales = ingresoService.buscarMovimientosManuales(filtroMovManual, filtroMovManual.getMovManualConcat(), consultaSinFiltro);
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarRegistrarMovtoManual", results = {
			@Result(name = SUCCESS, location = LOCATION_ALTAMOVTOMANUAL_JSP) ,
			@Result(name = INPUT, location = LOCATION_ALTAMOVTOMANUAL_JSP)})
	public String mostrarRegistrarMovtoManual(){
		listaCuentaManual = listadoService.getMapCuentasManualesSiniestros();
		filtroMovManual = new MovimientoManualDTO();
		filtroMovManual.setUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
		filtroMovManual.setFechaRegistro(new SimpleDateFormat("dd/MM/yyyy",new Locale("es")).format(new Date()));
		return SUCCESS;
	}
	
	@Action(value = "registrarMovtoManual", results = {
			@Result(name = SUCCESS, location = LOCATION_ALTAMOVTOMANUAL_JSP) ,
			@Result(name = INPUT, location = LOCATION_ALTAMOVTOMANUAL_JSP)})
	public String registrarMovtoManual(){
		try{
			ingresoService.registrarMovtoManual(filtroMovManual.getIdCuentaContable() , 
					filtroMovManual.getImporte(), filtroMovManual.getCausaMovimiento());
			listaCuentaManual = listadoService.getMapCuentasManualesSiniestros();
		}catch(Exception ex){
			setMensajeError("No se pudo registrar el movimiento");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "eliminarMovtoManual", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOVSMANUALES_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOVSMANUALES_JSP)})
	public String eliminarMovtoManual(){
		try{
			ingresoService.eliminarMovtoManual(filtroMovManual.getIdMovimiento());
		}catch(Exception ex){
			setMensajeError("No se pudo eliminar el movimiento");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	//--------
	private Long idAplicacion ; 
	@Action(value = "contabilizaAplicacion", results = { 
			@Result(name = INPUT, location = LOCATION_CONTENEDORINGRESOS_JSP) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/ingresos",
					"mensaje","${mensaje}"}) })	
	public String contabilizaAplicacion(){
		try {
			this.ingresoService.contabilizaAplicacionIngreso(this.idAplicacion);
		} catch (Exception e) {
			setMensajeError(e.getMessage());
			return SUCCESS;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	
	@Action(value = "aplicarIngresoAutomatico", results = { 
			@Result(name = INPUT, location = LOCATION_CONTENEDORINGRESOS_JSP) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/ingresos",
					"mensaje","${mensaje}"}) })	
	public String aplicarIngresoAutomatico(){
		try {
			this.ingresoService.aplicarIngresoAutomatico();
		} catch (Exception e) {
			setMensajeError(e.getMessage());
			return SUCCESS;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	
	//sendRequestJQ(null,'/MidasWeb/siniestros/recuperacion/ingresos/contabilizaAplicacion.action?idAplicacion=1',targetWorkArea,null);
	/**
	 * @return the filtroIngresosPendientes
	 */
	public ListarIngresoDTO getFiltroIngresosPendientes() {
		return filtroIngresosPendientes;
	}

	/**
	 * @param filtroIngresosPendientes the filtroIngresosPendientes to set
	 */
	public void setFiltroIngresosPendientes(
			ListarIngresoDTO filtroIngresosPendientes) {
		this.filtroIngresosPendientes = filtroIngresosPendientes;
	}

	/**
	 * @return the ingreso
	 */
	public Ingreso getIngreso() {
		return ingreso;
	}

	/**
	 * @param ingreso the ingreso to set
	 */
	public void setIngreso(Ingreso ingreso) {
		this.ingreso = ingreso;
	}

	/**
	 * @return the ingresoId
	 */
	public Long getIngresoId() {
		return ingresoId;
	}

	/**
	 * @param ingresoId the ingresoId to set
	 */
	public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}

	/**
	 * @return the listaCuentasDepositos
	 */
	public Map<String, String> getListaCuentasDepositos() {
		return listaCuentasDepositos;
	}

	/**
	 * @param listaCuentasDepositos the listaCuentasDepositos to set
	 */
	public void setListaCuentasDepositos(Map<String, String> listaCuentasDepositos) {
		this.listaCuentasDepositos = listaCuentasDepositos;
	}

	/**
	 * @return the listaIngresos
	 */
	public List<ListarIngresoDTO> getListaIngresos() {
		return listaIngresos;
	}

	/**
	 * @param listaIngresos the listaIngresos to set
	 */
	public void setListaIngresos(List<ListarIngresoDTO> listaIngresos) {
		this.listaIngresos = listaIngresos;
	}

	/**
	 * @return the listaMedioRecuperacion
	 */
	public Map<String, String> getListaMedioRecuperacion() {
		return listaMedioRecuperacion;
	}

	/**
	 * @param listaMedioRecuperacion the listaMedioRecuperacion to set
	 */
	public void setListaMedioRecuperacion(Map<String, String> listaMedioRecuperacion) {
		this.listaMedioRecuperacion = listaMedioRecuperacion;
	}

	/**
	 * @return the listaOficinas
	 */
	public Map<Long, String> getListaOficinas() {
		return listaOficinas;
	}

	/**
	 * @param listaOficinas the listaOficinas to set
	 */
	public void setListaOficinas(Map<Long, String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}

	public Map<String, String> getListaReferenciasIdentif() {
		return listaReferenciasIdentif;
	}

	public void setListaReferenciasIdentif(Map<String, String> listaReferenciasIdentif) {
		this.listaReferenciasIdentif = listaReferenciasIdentif;
	}

	/**
	 * @return the listaTipoRecuperacion
	 */
	public Map<String, String> getListaTipoRecuperacion() {
		return listaTipoRecuperacion;
	}

	/**
	 * @param listaTipoRecuperacion the listaTipoRecuperacion to set
	 */
	public void setListaTipoRecuperacion(Map<String, String> listaTipoRecuperacion) {
		this.listaTipoRecuperacion = listaTipoRecuperacion;
	}

	/**
	 * @return the soloConsulta
	 */
	public Boolean getSoloConsulta() {
		return soloConsulta;
	}

	/**
	 * @param soloConsulta the soloConsulta to set
	 */
	public void setSoloConsulta(Boolean soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	/**
	 * @return the tipoDeCancelacion
	 */
	public Integer getTipoDeCancelacion() {
		return tipoDeCancelacion;
	}

	/**
	 * @param tipoDeCancelacion the tipoDeCancelacion to set
	 */
	public void setTipoDeCancelacion(Integer tipoDeCancelacion) {
		this.tipoDeCancelacion = tipoDeCancelacion;
	}

	/**
	 * @return the ingresosIndentificadosCheck
	 */
	public Boolean getIngresosIndentificadosCheck() {
		return ingresosIndentificadosCheck;
	}

	/**
	 * @param ingresosIndentificadosCheck the ingresosIndentificadosCheck to set
	 */
	public void setIngresosIndentificadosCheck(Boolean ingresosIndentificadosCheck) {
		this.ingresosIndentificadosCheck = ingresosIndentificadosCheck;
	}
	
	public ListarIngresoDTO getIngresosPendDto() {
		return ingresosPendDto;
	}

	public void setIngresosPendDto(ListarIngresoDTO ingresosPendDto) {
		this.ingresosPendDto = ingresosPendDto;
	}

	public ImportesIngresosDTO getImportesDTO() {
		return importesDTO;
	}

	public void setImportesDTO(ImportesIngresosDTO importesDTO) {
		this.importesDTO = importesDTO;
	}

	/**
	 * @return the filtroDepositos
	 */
	public DepositoBancarioDTO getFiltroDepositos() {
		return filtroDepositos;
	}

	/**
	 * @param filtroDepositos the filtroDepositos to set
	 */
	public void setFiltroDepositos(DepositoBancarioDTO filtroDepositos) {
		this.filtroDepositos = filtroDepositos;
	}

	/**
	 * @return the listaDepositos
	 */
	public List<DepositoBancarioDTO> getListaDepositos() {
		return listaDepositos;
	}

	/**
	 * @param listaDepositos the listaDepositos to set
	 */
	public void setListaDepositos(List<DepositoBancarioDTO> listaDepositos) {
		this.listaDepositos = listaDepositos;
	}
	
	public Map<Long, String> getBancos() {
		return bancos;
	}

	public void setBancos(Map<Long, String> bancos) {
		this.bancos = bancos;
	}

	/**
	 * @return the filtroMovAcreedor
	 */
	public MovimientoAcreedorDTO getFiltroMovAcreedor() {
		return filtroMovAcreedor;
	}

	/**
	 * @param filtroMovAcreedor the filtroMovAcreedor to set
	 */
	public void setFiltroMovAcreedor(MovimientoAcreedorDTO filtroMovAcreedor) {
		this.filtroMovAcreedor = filtroMovAcreedor;
	}
	
	public int getVariacion() {
		return variacion;
	}

	public void setVariacion(int variacion) {
		this.variacion = variacion;
	}

	public Map<String, String> getListaMotivoCancelacion() {
		return listaMotivoCancelacion;
	}

	public void setListaMotivoCancelacion(Map<String, String> listaMotivoCancelacion) {
		this.listaMotivoCancelacion = listaMotivoCancelacion;
	}

	
	public Map<Long, String> getListaCuentaAcredora() {
		return listaCuentaAcredora;
	}

	public void setListaCuentaAcredora(Map<Long, String> listaCuentaAcredora) {
		this.listaCuentaAcredora = listaCuentaAcredora;
	}

	/**
	 * @return the listaMovAcreedores
	 */
	public List<MovimientoAcreedorDTO> getListaMovAcreedores() {
		return listaMovAcreedores;
	}

	/**
	 * @param listaMovAcreedores the listaMovAcreedores to set
	 */
	public void setListaMovAcreedores(List<MovimientoAcreedorDTO> listaMovAcreedores) {
		this.listaMovAcreedores = listaMovAcreedores;
	}

	/**
	 * @return the listaMotivosCancelacion
	 */
	public Map<String, String> getListaMotivosCancelacion() {
		return listaMotivosCancelacion;
	}

	/**
	 * @param listaMotivosCancelacion the listaMotivosCancelacion to set
	 */
	public void setListaMotivosCancelacion(
			Map<String, String> listaMotivosCancelacion) {
		this.listaMotivosCancelacion = listaMotivosCancelacion;
	}

	public Boolean getConsultaSinFiltro() {
		return consultaSinFiltro;
	}

	public void setConsultaSinFiltro(Boolean consultaSinFiltro) {
		this.consultaSinFiltro = consultaSinFiltro;
	}

	public String getMensajeAplicacion() {
		return mensajeAplicacion;
	}

	public void setMensajeAplicacion(String mensajeAplicacion) {
		this.mensajeAplicacion = mensajeAplicacion;
	}

	/**
	 * @return the idToReporte
	 */
	public Long getIdToReporte() {
		return idToReporte;
	}

	/**
	 * @param idToReporte the idToReporte to set
	 */
	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}

	public Long getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(Long idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public Map<String, String> getOpcionesCancelarIngreso() {
		return opcionesCancelarIngreso;
	}

	public void setOpcionesCancelarIngreso(
			Map<String, String> opcionesCancelarIngreso) {
		this.opcionesCancelarIngreso = opcionesCancelarIngreso;
	}

	/**
	 * @return the listaCuentaManual
	 */
	public Map<Long, String> getListaCuentaManual() {
		return listaCuentaManual;
	}

	/**
	 * @param listaCuentaManual the listaCuentaManual to set
	 */
	public void setListaCuentaManual(Map<Long, String> listaCuentaManual) {
		this.listaCuentaManual = listaCuentaManual;
	}

	/**
	 * @return the filtroMovManual
	 */
	public MovimientoManualDTO getFiltroMovManual() {
		return filtroMovManual;
	}

	/**
	 * @param filtroMovManual the filtroMovManual to set
	 */
	public void setFiltroMovManual(MovimientoManualDTO filtroMovManual) {
		this.filtroMovManual = filtroMovManual;
	}

	/**
	 * @return the listaMovManuales
	 */
	public List<MovimientoManualDTO> getListaMovManuales() {
		return listaMovManuales;
	}

	/**
	 * @param listaMovManuales the listaMovManuales to set
	 */
	public void setListaMovManuales(List<MovimientoManualDTO> listaMovManuales) {
		this.listaMovManuales = listaMovManuales;
	}

	public String getOrigenRecuperacionDeducible() {
		return origenRecuperacionDeducible;
	}

	public void setOrigenRecuperacionDeducible(String origenRecuperacionDeducible) {
		this.origenRecuperacionDeducible = origenRecuperacionDeducible;
	}
	
}