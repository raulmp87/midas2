package mx.com.afirme.midas2.action.siniestros.pagos.notasCredito;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.notasDeCredito.ConjuntoOrdenCompraNota;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.BusquedaNotasCreditoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.FacturaSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.OrdenCompraRegistro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.pagos.notasDeCredito.NotasCreditoService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/notasCredito")
public class NotaCreditoAction  extends BaseAction implements Preparable{

	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;
		
	private Boolean soloLectura;
	private Boolean esCancelacion;
	private File archivoNotasCredito;
	private String extensionArchivo;
	private PrestadorServicio proveedor;
	private Long idBatch;
	private DocumentoFiscal factura;
	private String seleccionNotasCreditoConcat;
	private String seleccionOrdenesAsociarConcat;
	private String seleccionOrdenesCancelarConcat;
	private Map<String,String> listaEstatusNotasCredito;
	private ConjuntoOrdenCompraNota conjuntoOrdenCompraNota;   
	private List<EnvioValidacionFactura> listaNotasCreditoCargadas;
	private List<FacturaSiniestroRegistro> listaFacturas;
	private List<NotasCreditoRegistro> listaNotasCredito; 
	private List<OrdenCompraRegistro> listaOrdenesCompra;
	private BusquedaNotasCreditoDTO filtroNotasCredito;
	private List<MensajeValidacionFactura> listaValidacionesNotaCredito;
	private Long idValidacionNotaCredito;
	private Integer numNotasCargadas;
	
	private NotasCreditoService notasCreditoService; 
	private ListadoService listadoService;
	private EntidadService entidadService;
	private UsuarioService usuarioService;
	private PrestadorDeServicioService prestadorDeServicioService;
	private RecepcionFacturaService recepcionFacturaService;
		
	@Autowired
	@Qualifier("notasCreditoServiceEJB")
	public void setNotasCreditoService(NotasCreditoService notasCreditoService) {
		this.notasCreditoService = notasCreditoService;
	}	

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	public void setPrestadorDeServicioService(
			PrestadorDeServicioService prestadorDeServicioService) {
		this.prestadorDeServicioService = prestadorDeServicioService;
	}
	
	@Autowired
	@Qualifier("recepcionFacturaServiceEJB")
	public void setRecepcionFacturaService(
			RecepcionFacturaService recepcionFacturaService) {
		this.recepcionFacturaService = recepcionFacturaService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}	
	
	public void prepareMostrarRecepcionNotasCredito()
	{
		
	}
	
	@Action(value="mostrarRecepcionNotasCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/contenedorOrdenesCompraNotasCredito.jsp")
	})	
	public String mostrarRecepcionNotasCredito() {
		
		     	
		return SUCCESS;		
	}
	
	@Action(value="mostrarBusquedaFacturas",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/contenedorListadoFacturas.jsp")
	})	
	public String mostrarBusquedaFacturas() {
		
		return SUCCESS;		
	}
	 
	@Action(value="buscarFacturasParaNotasCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/listadoFacturasParaNotasCreditoGrid.jsp")
	})	
	public String buscarFacturasParaNotasCredito() {
		
		listaFacturas = notasCreditoService.obtenerFacturasParaCrearNotasDeCredito();
		
		return SUCCESS;		
	}
	
	@Action(value="listarOrdenesCompraPorFactura",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/listadoAsociacionOrdenesCompraGrid.jsp")
	})	
	public String listarOrdenesCompraPorFactura() {
		
		if(conjuntoOrdenCompraNota != null && conjuntoOrdenCompraNota.getId() != null)
		{
			listaOrdenesCompra = notasCreditoService.obtenerOrdenesDeCompraPorConjunto(conjuntoOrdenCompraNota.getId());
		}
		else if(factura != null && factura.getId() != null)
		{
			listaOrdenesCompra = notasCreditoService.obtenerOrdenesDeCompraPorFactura(factura.getId());			
		}		
		
		return SUCCESS;		
	}	
	
	@Action(value="listarNotasCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/listadoAsociacionNotasCreditoGrid.jsp")
	})	
	public String listarNotasCredito() {	
		
		if(conjuntoOrdenCompraNota != null && conjuntoOrdenCompraNota.getId() != null) //Consultar/Eliminar 
		{
			listaNotasCreditoCargadas = notasCreditoService.obtenerEnvioValidacionFacturaPorConjunto(conjuntoOrdenCompraNota.getId());
		} 
		else if(idBatch != null)
		{			
			listaNotasCreditoCargadas = recepcionFacturaService.obtenerFacturasProcesadas(idBatch);			
		}
		
		return SUCCESS;
	}
	
	@Action(value="cargarArchivoNotasCredito",	
			results = {@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","idBatch,tipoMensaje,mensaje,numNotasCargadas"})
	})	//Nota: Se agrega parametro contentType = text/plain  para prevenir que internet explorer intente descargar un archivo con la respuesta json. 
	public String cargarArchivoNotasCredito() {			
		
		listaNotasCreditoCargadas = notasCreditoService.procesarArchivoNotasDeCredito(proveedor.getId(), 
				archivoNotasCredito,extensionArchivo);
		
		numNotasCargadas = listaNotasCreditoCargadas.size();
		idBatch = listaNotasCreditoCargadas.size() > 0 ?listaNotasCreditoCargadas.get(0).getIdBatch():0L;
		
		Boolean existeFacturaErronea = notasCreditoService.existenFacturasEstatus(listaNotasCreditoCargadas, 
				DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		if(existeFacturaErronea == null 
				|| existeFacturaErronea){
			
			this.setMensajeError("Ha ocurrido un error con el procesamiento de una o más Notas de Crédito.");
			
		}else
		{
			this.setMensajeExito();
		}
		
		return SUCCESS;		
	}
	
	
	@Action(value="validarRegistrarNotaCredito",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarRecepcionNotasCredito", 
			"namespace", "/siniestros/pagos/notasCredito", 		
			"mensaje", "${mensaje}",
			"factura.id", "${factura.id}",
			"factura.numeroFactura", "${factura.numeroFactura}",
			"idBatch", "${idBatch}",
			"seleccionNotasCreditoConcat", "${seleccionNotasCreditoConcat}",
			"seleccionOrdenesAsociarConcat", "${seleccionOrdenesAsociarConcat}",
			"seleccionOrdenesCancelarConcat", "${seleccionOrdenesCancelarConcat}",
			"proveedor.id", "${proveedor.id}",	
			"proveedor.personaMidas.nombre", "${proveedor.personaMidas.nombre}",
			"tipoMensaje", "${tipoMensaje}"})
			})	
	public String validarRegistrarNotaCredito() {	
		
		notasCreditoService.registrarNotasDeCredito(idBatch, factura.getId(), 
				seleccionOrdenesAsociarConcat,seleccionOrdenesCancelarConcat, 
				seleccionNotasCreditoConcat);
		
		this.setMensajeExito();
		
		return SUCCESS;
	}
	
	@Action(value="verDetalleValidacionNotaCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/recepcionNotaCreditoResultadoValidacion.jsp")
	})	
	public String verDetalleValidacionNotaCredito() {
		
		listaValidacionesNotaCredito = recepcionFacturaService.obtenerResultadosValidacion(idValidacionNotaCredito);
						
		return SUCCESS;
	}
	
	public void prepareMostrarBusquedaNotasCredito()
	{		
		listaEstatusNotasCredito = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_FACTURA); 		
	}
	
	@Action(value="mostrarBusquedaNotasCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/busquedaNotasCredito.jsp")
	})	
	public String mostrarBusquedaNotasCredito() {		
						
		return SUCCESS;   
	}
	
	@Action(value="buscarNotasCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/notasCredito/listadoBusquedaNotasCreditoGrid.jsp")
	})	
	public String buscarNotasCredito() {
		
		listaNotasCredito = notasCreditoService.buscarNotasCeCredito(filtroNotasCredito);				
		
		return SUCCESS;
	}
	
	@Action(value="cancelarNotaCredito",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaNotasCredito", 
			"namespace", "/siniestros/pagos/notasCredito", 		
			"mensaje", "${mensaje}",					
			"tipoMensaje", "${tipoMensaje}"})
			})	
	public String cancelarNotaCredito() {  
		
		notasCreditoService.cancelarNotaDeCredito(seleccionNotasCreditoConcat);
		this.setMensajeExito();
						
		return SUCCESS;
	}

	public Boolean getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(Boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

	public File getArchivoNotasCredito() {
		return archivoNotasCredito;
	}

	public void setArchivoNotasCredito(File archivoNotasCredito) {
		this.archivoNotasCredito = archivoNotasCredito;
	}

	public PrestadorServicio getProveedor() {
		return proveedor;
	}

	public void setProveedor(PrestadorServicio proveedor) {
		this.proveedor = proveedor;
	}

	public Long getIdBatch() {
		return idBatch;
	}

	public void setIdBatch(Long idBatch) {
		this.idBatch = idBatch;
	}

	public DocumentoFiscal getFactura() {
		return factura;
	}

	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}

	public String getSeleccionNotasCreditoConcat() {
		return seleccionNotasCreditoConcat;
	}

	public void setSeleccionNotasCreditoConcat(String seleccionNotasCreditoConcat) {
		this.seleccionNotasCreditoConcat = seleccionNotasCreditoConcat;
	}

	public Map<String, String> getListaEstatusNotasCredito() {
		return listaEstatusNotasCredito;
	}

	public void setListaEstatusNotasCredito(
			Map<String, String> listaEstatusNotasCredito) {
		this.listaEstatusNotasCredito = listaEstatusNotasCredito;
	}

	public ConjuntoOrdenCompraNota getConjuntoOrdenCompraNota() {
		return conjuntoOrdenCompraNota;
	}

	public void setConjuntoOrdenCompraNota(
			ConjuntoOrdenCompraNota conjuntoOrdenCompraNota) {
		this.conjuntoOrdenCompraNota = conjuntoOrdenCompraNota;
	}

	public List<EnvioValidacionFactura> getListaNotasCreditoCargadas() {
		return listaNotasCreditoCargadas;
	}

	public void setListaNotasCreditoCargadas(
			List<EnvioValidacionFactura> listaNotasCreditoCargadas) {
		this.listaNotasCreditoCargadas = listaNotasCreditoCargadas;
	}

	public List<FacturaSiniestroRegistro> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(List<FacturaSiniestroRegistro> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public List<NotasCreditoRegistro> getListaNotasCredito() {
		return listaNotasCredito;
	}

	public void setListaNotasCredito(List<NotasCreditoRegistro> listaNotasCredito) {
		this.listaNotasCredito = listaNotasCredito;
	}

	public List<OrdenCompraRegistro> getListaOrdenesCompra() {
		return listaOrdenesCompra;
	}

	public void setListaOrdenesCompra(List<OrdenCompraRegistro> listaOrdenesCompra) {
		this.listaOrdenesCompra = listaOrdenesCompra;
	}

	public BusquedaNotasCreditoDTO getFiltroNotasCredito() {
		return filtroNotasCredito;
	}

	public void setFiltroNotasCredito(BusquedaNotasCreditoDTO filtroNotasCredito) {
		this.filtroNotasCredito = filtroNotasCredito;
	}

	public Integer getNumNotasCargadas() {
		return numNotasCargadas;
	}

	public void setNumNotasCargadas(Integer numNotasCargadas) {
		this.numNotasCargadas = numNotasCargadas;
	}

	public String getSeleccionOrdenesAsociarConcat() {
		return seleccionOrdenesAsociarConcat;
	}

	public void setSeleccionOrdenesAsociarConcat(
			String seleccionOrdenesAsociarConcat) {
		this.seleccionOrdenesAsociarConcat = seleccionOrdenesAsociarConcat;
	}

	public String getSeleccionOrdenesCancelarConcat() {
		return seleccionOrdenesCancelarConcat;
	}

	public void setSeleccionOrdenesCancelarConcat(
			String seleccionOrdenesCancelarConcat) {
		this.seleccionOrdenesCancelarConcat = seleccionOrdenesCancelarConcat;
	}

	public List<MensajeValidacionFactura> getListaValidacionesNotaCredito() {
		return listaValidacionesNotaCredito;
	}

	public void setListaValidacionesNotaCredito(
			List<MensajeValidacionFactura> listaValidacionesNotaCredito) {
		this.listaValidacionesNotaCredito = listaValidacionesNotaCredito;
	}

	public Long getIdValidacionNotaCredito() {
		return idValidacionNotaCredito;
	}

	public void setIdValidacionNotaCredito(Long idValidacionNotaCredito) {
		this.idValidacionNotaCredito = idValidacionNotaCredito;
	}

	public Boolean getEsCancelacion() {
		return esCancelacion;
	}

	public void setEsCancelacion(Boolean esCancelacion) {
		this.esCancelacion = esCancelacion;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}	

}
