package mx.com.afirme.midas2.action.siniestros.pagos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.pagos.ConceptosOrdenPagosDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.DetalleOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
/**
 * Action que maneja todas la peticiones relacionadas con el m�dulo de pagos.
 * @author usuario
 * @version 1.0
 * @created 22-sep-2014 05:36:39 p.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/pagosSinestro")

public class PagosSinestroAction  extends BaseAction implements Preparable {	
	private static final long serialVersionUID = -904741320121443575L;
	private static final String LISTADOORDENPAGO =	"/jsp/siniestros/pagos/listadoOrdenesPago.jsp";
	private static final String CONTENEDOR_ORDENPAGO =	"/jsp/siniestros/pagos/contenedorOrdenPago.jsp";
	private static final String CONTENEDOR_TOTALES =	"/jsp/siniestros/pagos/totalesOrdenPago";
	private static final String LISTADETALLEORDENPAGO =	"/jsp/siniestros/pagos/listadoDetalleOrdenPago.jsp";
	private static final String CONTENEDOR_ORDENPAGODETALLE="/jsp/siniestros/pagos/contenedorOrdenPagoDetalle.jsp";
	private static final String CONTENEDOR_CANCELARORDENPAGO="/jsp/siniestros/pagos/cancelarOrdenPago.jsp";
	private static final String ACCION_CONSULTAR="consultar";
	private static final String ACCION_NUEVA="nueva";
	private static final String  ESTATUSINICIAL="Tramite";	
	private static final String ESTATUS_LIBERADA="1";	
	private Long idOrdenCompra; 
	private Long idOrdenPago;
	private Long idReporteCabina;
	private Long idCobertura;
	private  boolean soloLectura;

	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("conceptoAjusteServiceEJB")
	private ConceptoAjusteService conceptoAjusteService;
	
	@Autowired
	@Qualifier("ordenCompraServiceEJB")
	private OrdenCompraService ordenCompraService;
	
	@Autowired
	@Qualifier("pagosSiniestroServiceEJB")
	private PagosSiniestroService pagosSiniestroService;
	
	@Autowired
	@Qualifier("bloqueOrdenPagoEJB")
	private BloqueoPagoService bloqueoPagoService;
	
	/**
	 * Listado de detalles de ordenes de pago
	 */
	private List<DesglosePagoConceptoDTO> desglosePagoConceptoDTO;
	/**
	 * bandera para saber si se entra a la pantalla de Orden de pago como
	 * captura/edicion/consulta
	 */
	private String modoPantalla;
	private OrdenPagoSiniestro ordenPago;
	private DetalleOrdenCompra detalleOrdenPago;
 	/**
	 * DTO  para transporte de la informaci�n del footer de la pantalla de orden de
	 * pago, donde se muestran los totales.
	 */
	private DesglosePagoConceptoDTO totales;
	private List<DetalleOrdenCompraDTO> listaDetalleDTO;
	private DesglosePagoConceptoDTO deglosePorConcepto;
	private ConceptosOrdenPagosDTO conceptosOrdenPagosDTO;	
	private List<OrdenPagosDTO> listadoOrdenPagos;
	private OrdenPagosDTO filtroOrden;	
	private Map<Long,String> oficinas;
	private Map<String,String> terminoAjusteMap;	
	private Map<String,String> motivoCancelacionMap;
	private Map<Long,String> proveedorMap;
	private Map<String,String> pagoaMap;
	private Map<String,String> tipoPagoMap;
	private Map<String,String> tipoOrdenPagoMap;
	private Map<String,String> estautsMap;
	private Map<String,String> tipoProveedorMap;
	private Map<Long,String> conceptoPagoMap;	
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENPAGO),
			@Result(name=INPUT,location=CONTENEDOR_ORDENPAGO)
			})
	public String mostrar(){	
		this.filtroOrden= new OrdenPagosDTO();
		return SUCCESS;
	}
	
	
	/**
	 * Cambia el estatus de la orden de pago a autorizado
	 */
	@Action(value="autorizarOrdenPago",results={
			@Result(name=SUCCESS,location=LISTADOORDENPAGO),
			@Result(name=INPUT,location=LISTADOORDENPAGO)
			})
	public String autorizarOrdenPago(){		
		return SUCCESS;
	}	
	/**
	 * cancela una orden depago, se cambia el estatus a cancelado.
	 */
	@Action(value="cancelarOrdenPago",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENPAGO),
			@Result(name=INPUT,location=CONTENEDOR_ORDENPAGO)
			})
	public String cancelarOrdenPago(){		
		try {
			this.pagosSiniestroService.cancelarOrdenPago(filtroOrden.getId());
		} catch (Exception e) {
			this.setMensajeError(e.getMessage());
			return INPUT;
		}
		OrdenPagoSiniestro ordenPagoAux = this.entidadService.findById(OrdenPagoSiniestro.class,filtroOrden.getId());
		ordenPagoAux.setComentarios(filtroOrden.getComentarios());
		ordenPagoAux.setMotivoCancelacion(filtroOrden.getMotivoCancelacion());
		this.entidadService.save(ordenPagoAux);
		if(filtroOrden.isSolicitarCancelarOrdenCompra()){
			this.pagosSiniestroService.solicitarCancelacionOrdenCompra(ordenPagoAux.getId());
		}
		this.filtroOrden=new OrdenPagosDTO();
		super.setMensajeExitoPersonalizado( getText("midas.servicio.siniestros.pagos.msjCancelada")  );
		return SUCCESS;
	}

	
	@Action(value="guardarDetalleOrdenPago",results={
			@Result(name=SUCCESS,location=LISTADETALLEORDENPAGO),
			@Result(name=INPUT,location=LISTADETALLEORDENPAGO)
			})
	@Deprecated
	public String guardarDetalleOrdenPago(){
		OrdenPagoSiniestro ordenDto = new OrdenPagoSiniestro();		
		ordenDto.setOrdenCompra(entidadService.findById(OrdenCompra.class, this.filtroOrden.getNumOrdenCompra()));
		ordenDto.setId(this.filtroOrden.getId());
		desglosePagoConceptoDTO =this.pagosSiniestroService.getDetalleOrdenPago(ordenDto.getId(), false);
		if(!desglosePagoConceptoDTO.isEmpty()){
			DesglosePagoConceptoDTO totalesAux= desglosePagoConceptoDTO.get(0);			
			deglosePorConcepto.setTotalesPagados(totalesAux.getTotalesPagados());
			deglosePorConcepto.setTotalesPorPagar(totalesAux.getTotalesPorPagar());
			deglosePorConcepto.setReserva(totalesAux.getReserva());
			deglosePorConcepto.setReservaDisponible(totalesAux.getReservaDisponible());
			deglosePorConcepto.setDeduciblesTotal(totalesAux.getDeduciblesTotal());
			deglosePorConcepto.setDescuentosTotal(totalesAux.getDescuentosTotal());
		}
		return SUCCESS;	
	}

	/**
	 * recibe las peticiones de guardado de una orden de pago.
	 * 
	 * @param ordenPago
	 */
	@Action(value="guardarOrdenPago",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENPAGO),
			@Result(name=INPUT,location=CONTENEDOR_ORDENPAGODETALLE)
			})
	public String guardarOrdenPago(){
		if(null==filtroOrden.getNumOrdenCompra()){
			super.setMensajeError(getText("midas.servicio.siniestros.pagos.noRefOrdenCompra"));
			return INPUT;
		}
		if(null==this.filtroOrden.getId()){
			super.setMensajeError(("midas.servicio.siniestros.pagos.noRefOrdenPago"));
			return INPUT;
		}
		OrdenPagoSiniestro ordenPagos = this.entidadService.findById(OrdenPagoSiniestro.class,filtroOrden.getId());
		OrdenCompra oc =this.ordenCompraService.obtenerOrdenCompra(filtroOrden.getNumOrdenCompra()) ;
		//*Obtener nuevo desglose guardado
		OrdenPagoSiniestro ordenDto = new OrdenPagoSiniestro();
		ordenDto.setOrdenCompra(oc);
		ordenDto.setId(this.filtroOrden.getId());
		this.deglosePorConcepto=new DesglosePagoConceptoDTO();
		desglosePagoConceptoDTO =this.pagosSiniestroService.getDetalleOrdenPago(ordenDto.getId(), false);
		if(!desglosePagoConceptoDTO.isEmpty()){
			DesglosePagoConceptoDTO totalesAux= desglosePagoConceptoDTO.get(0);
			deglosePorConcepto.setTotalesPagados(totalesAux.getTotalesPagados());
			deglosePorConcepto.setTotalesPorPagar(totalesAux.getTotalesPorPagar());
			deglosePorConcepto.setReserva(totalesAux.getReserva());
			deglosePorConcepto.setReservaDisponible(totalesAux.getReservaDisponible());
			deglosePorConcepto.setDeduciblesTotal(totalesAux.getDeduciblesTotal());
			deglosePorConcepto.setDescuentosTotal(totalesAux.getDescuentosTotal());
		}
		ordenPagos.setEstatus(ESTATUS_LIBERADA);
		ordenPagos.setEstatusSeycos(filtroOrden.getEstatusSeycos());
		ordenPagos.setFechaActualizacionSeycos(filtroOrden.getFechaActualizacionSeycos());
		if(null!=filtroOrden.getFechaPago())
			ordenPago.setFechaPago(filtroOrden.getFechaPago());
		ordenPagos.setIdOrdenPagoSeycos(filtroOrden.getIdOrdenPagoSeycos());
		ordenPagos.setOrdenCompra(oc);
		ordenPagos.setComentarios("");
		ordenPagos.setId(filtroOrden.getId());
		ordenPagos.setOrigenPago(filtroOrden.getOrigenPago());
		ordenPagos.setFactura(filtroOrden.getNumFactura());
		BigDecimal total = new BigDecimal("0.0");
		if (null!=this.deglosePorConcepto && null!=this.deglosePorConcepto.getTotalesPorPagar()){
			total=this.deglosePorConcepto.getTotalesPorPagar();
		}		
		if(null!=oc.getIdBeneficiario()){
			Integer proveedorId = Integer.parseInt( oc.getIdBeneficiario().toString());
			if (bloqueoPagoService.estaElProovedorBloqueado(proveedorId)){
				super.setMensajeError(getText("midas.servicio.siniestros.pagos.msjProvedorBloqueado"));
				deglosePorConcepto=new DesglosePagoConceptoDTO();
				this.agignarMontos();
				return INPUT;
			}
		}
			ImportesOrdenCompraDTO importes= this.ordenCompraService.calcularImportes(oc.getId(),null,false);
			if(importes.getTotal().compareTo(total)!=0){
				super.setMensajeError(getText("midas.servicio.siniestros.pagos.msjMontoOrdenPagoDifOrdenCompra"));
				deglosePorConcepto=new DesglosePagoConceptoDTO();
				this.agignarMontos();
				return INPUT;
			}
		this.pagosSiniestroService.guardarOrdenPago(ordenPagos);
		ordenPagos=this.pagosSiniestroService.autorizarOrdenPago(ordenPagos.getId());
	    oc= this.entidadService.findById(OrdenCompra.class, filtroOrden.getNumOrdenCompra());
	    if(!StringUtil.isEmpty(filtroOrden.getBeneficiario())){
			oc.setNomBeneficiario(filtroOrden.getBeneficiario());
		}
	    this.entidadService.save(oc);
		this.filtroOrden=new OrdenPagosDTO();
		super.setMensajeExitoPersonalizado(getText("midas.servicio.siniestros.pagos.msjAutorizada"));
		return SUCCESS;
	}
	
	
	private void agignarMontos (){
		OrdenPagoSiniestro ordenDto = new OrdenPagoSiniestro();
		ordenDto.setOrdenCompra(entidadService.findById(OrdenCompra.class, filtroOrden.getNumOrdenCompra()));
		ordenDto.setId(this.filtroOrden.getId());
		this.deglosePorConcepto=new DesglosePagoConceptoDTO();
		desglosePagoConceptoDTO =this.pagosSiniestroService.getDetalleOrdenPago(ordenDto.getId(), false);
		if(!desglosePagoConceptoDTO.isEmpty()){
			DesglosePagoConceptoDTO totalesAux= desglosePagoConceptoDTO.get(0);
			deglosePorConcepto.setTotalesPagados(totalesAux.getTotalesPagados());
			deglosePorConcepto.setTotalesPorPagar(totalesAux.getTotalesPorPagar());
			deglosePorConcepto.setReserva(totalesAux.getReserva());
			deglosePorConcepto.setReservaDisponible(totalesAux.getReservaDisponible());
			deglosePorConcepto.setDeduciblesTotal(totalesAux.getDeduciblesTotal());
			deglosePorConcepto.setDescuentosTotal(totalesAux.getDescuentosTotal());
		}
	}

	/**
	 * Muestra la pantalla de cancelaci�n de orden de pago
	 */
	@Action(value="mostrarCancelarOrdenPago",results={
			@Result(name=SUCCESS,location=CONTENEDOR_CANCELARORDENPAGO),
			@Result(name=INPUT,location=CONTENEDOR_CANCELARORDENPAGO)
			})
	public String mostrarCancelarOrdenPago(){
		OrdenPagosDTO filtro= new OrdenPagosDTO();
		if(null!=this.idOrdenCompra){
			filtro.setNumOrdenCompra(this.idOrdenCompra);
		}
		this.soloLectura=false;		
		if(null!=this.idOrdenPago){
			filtro.setId(this.idOrdenPago);
			/*validar Pre Registro*/
			OrdenPagoSiniestro preOrden =this.entidadService.findById(OrdenPagoSiniestro.class, this.idOrdenPago);
			if(null!=preOrden && StringUtil.isEmpty(preOrden.getEstatus())){
				filtro.setId(null);
			}
		}
		List<OrdenPagosDTO> list=this.pagosSiniestroService.buscarOrdenesPago(filtro,false);
		if(!list.isEmpty()){
			this.filtroOrden=list.get(0);
			if(null==this.filtroOrden.getFechaCacelacion()){
				this.filtroOrden.setFechaCacelacion(new Date());
			}
			if(StringUtil.isEmpty(this.filtroOrden.getCancealadaPor()) ){
					

				this.filtroOrden.setCancealadaPor(usuarioService.getUsuarioActual().getNombreCompleto());
				}
		}else{
			this.filtroOrden= new OrdenPagosDTO();
			}
		this.filtroOrden.setSolicitarCancelarOrdenCompra(false);
		OrdenPagoSiniestro ordenDto = new OrdenPagoSiniestro();
		ordenDto.setOrdenCompra(entidadService.findById(OrdenCompra.class, filtroOrden.getNumOrdenCompra()));
		ordenDto.setId(this.filtroOrden.getId());
		return SUCCESS;
	}
	
	/**
	 * muestra lista vacia 
	 */
	@Action(value="mostrarListaVacia",results={
			@Result(name=SUCCESS,location=LISTADOORDENPAGO),
			@Result(name=INPUT,location=LISTADOORDENPAGO)
			})
	public String mostrarListaVacia(){
		listadoOrdenPagos = new ArrayList<OrdenPagosDTO>();
		return SUCCESS;
	}

	/**
	 * muestra la pantalla de listado de ordenes de pago, debe traer todas las ordenes
	 * de compras autorizadas 
	 */
	@Action(value="mostrarListado",results={
			@Result(name=SUCCESS,location=LISTADOORDENPAGO),
			@Result(name=INPUT,location=LISTADOORDENPAGO)
			})
	public String mostrarListado(){
		this.listadoOrdenPagos=	this.pagosSiniestroService.buscarOrdenesPago(this.filtroOrden,false);
		//this.listadoOrdenPagos=	null;
	  return SUCCESS;
	}

	/**
	 * Muestra la pantalla principal de captura de la orden de pago en modos
	 * captura/edicion/consulta
	 */
	@Action(value="mostrarOrdenPago",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENPAGODETALLE),
			@Result(name=INPUT,location=CONTENEDOR_ORDENPAGO)
			})
	public String mostrarOrdenPago(){
		filtroOrden = pagosSiniestroService.obtenerOrdenPago(idOrdenCompra);		
		this.soloLectura=false;
		if(this.modoPantalla.equalsIgnoreCase(this.ACCION_CONSULTAR)){
			this.soloLectura=true;
		}else if(this.modoPantalla.equalsIgnoreCase(this.ACCION_NUEVA) ){
			filtroOrden.setEstatus(ESTATUSINICIAL);
		}
		OrdenCompra ordenCompra= this.entidadService.findById(OrdenCompra.class, idOrdenCompra);
		if(null!=ordenCompra){
			if(null!=ordenCompra.getReporteCabina()){
				this.idReporteCabina=ordenCompra.getReporteCabina().getId();

			}
			if(null!=ordenCompra.getCoberturaReporteCabina()){
				this.idCobertura=ordenCompra.getCoberturaReporteCabina().getId();
			}
		}
		OrdenPagoSiniestro ordenDto = new OrdenPagoSiniestro();
		ordenDto.setOrdenCompra(entidadService.findById(OrdenCompra.class, idOrdenCompra));
		ordenDto.setId(this.filtroOrden.getId());
		this.deglosePorConcepto=new DesglosePagoConceptoDTO();
		desglosePagoConceptoDTO =this.pagosSiniestroService.getDetalleOrdenPago(ordenDto.getId(), false);
		if(!desglosePagoConceptoDTO.isEmpty()){
			deglosePorConcepto=desglosePagoConceptoDTO.get(0);			
		}
		return SUCCESS;
	}
	/**
	 * obtene la informaci�n de un detalle espec�fico para ser editado en pantalla
	 */
	@Action (value = "obtenerDetalleOrdenPago", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^importesDTO.*"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^importesDTO.*"})
      })	
	public String obtenerDetalleOrdenPago(){
		return SUCCESS;
	}
	/**
	 * obtiene los totales y los responde v�a ajax para actualizar cada movimiento de
	 * los detalles.
	 */
	@Action(value="obtenerTotales",results={
			@Result(name=SUCCESS,location=CONTENEDOR_TOTALES),
			@Result(name=INPUT,location=CONTENEDOR_TOTALES)
			})
	public String obtenerTotales(){
		return SUCCESS;
	}
	
	@Action(value="generacionAutomaticaOrdenPago",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENPAGO),
			@Result(name=INPUT,location=CONTENEDOR_ORDENPAGO)
			})
	public String generacionAutomaticaOrdenPago(){
		if(null!=idOrdenCompra){
			this.pagosSiniestroService.generacionAutomaticaOrdenPago(this.idOrdenCompra);
		}		
		return SUCCESS;
	}
	
	
	/**
	 * consulta los conceptos o detalleOrdenCompra relacionados a una orden de compra
	 * y los muestra en un grid listadoOrdenCompraConceptos.jsp
	 * 
	 * @param idOrdenCompra    identificador de la orden de compra para buscar sus
	 * conceptos relacionados
	 */
	@Action(value="buscarDetalleCompra",results={
			@Result(name=SUCCESS,location=LISTADETALLEORDENPAGO),
			@Result(name=INPUT,location=LISTADETALLEORDENPAGO)
			})
	public String buscarDetalleCompra(){	
		OrdenPagoSiniestro ordenDto = new OrdenPagoSiniestro();
		ordenDto.setOrdenCompra(entidadService.findById(OrdenCompra.class, idOrdenCompra));
		ordenDto.setId(this.filtroOrden.getId());
		desglosePagoConceptoDTO =this.pagosSiniestroService.getDetalleOrdenPago(ordenDto.getId(), false);
		return SUCCESS;	
	}
	@Override
	public void prepare(){
		this.tipoPagoMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA);
		this.estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_ORDEN_PAGO);
		this.tipoOrdenPagoMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		this.tipoProveedorMap=  listadoService.getMapTipoPrestador();
		this.conceptoPagoMap=listadoService.getMapConceptosAjuste();
		this.oficinas = listadoService.obtenerOficinasSiniestros();		
		this.proveedorMap=new LinkedHashMap<Long, String>();
		this.terminoAjusteMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		this.motivoCancelacionMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);
		this.pagoaMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_AFECTADO);
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ConceptoAjusteService getConceptoAjusteService() {
		return conceptoAjusteService;
	}

	public void setConceptoAjusteService(ConceptoAjusteService conceptoAjusteService) {
		this.conceptoAjusteService = conceptoAjusteService;
	}

	public OrdenCompraService getOrdenCompraService() {
		return ordenCompraService;
	}

	public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
		this.ordenCompraService = ordenCompraService;
	}

	public PagosSiniestroService getPagosSiniestroService() {
		return pagosSiniestroService;
	}

	public void setPagosSiniestroService(PagosSiniestroService pagosSiniestroService) {
		this.pagosSiniestroService = pagosSiniestroService;
	}
	public List<DesglosePagoConceptoDTO> getDesglosePagoConceptoDTO() {
		return desglosePagoConceptoDTO;
	}

	public void setDesglosePagoConceptoDTO(
			List<DesglosePagoConceptoDTO> desglosePagoConceptoDTO) {
		this.desglosePagoConceptoDTO = desglosePagoConceptoDTO;
	}

	public String getModoPantalla() {
		return modoPantalla;
	}

	public void setModoPantalla(String modoPantalla) {
		this.modoPantalla = modoPantalla;
	}
	public OrdenPagoSiniestro getOrdenPago() {
		return ordenPago;
	}
	public void setOrdenPago(OrdenPagoSiniestro ordenPago) {
		this.ordenPago = ordenPago;
	}
	public DesglosePagoConceptoDTO getTotales() {
		return totales;
	}

	public void setTotales(DesglosePagoConceptoDTO totales) {
		this.totales = totales;
	}

	public DesglosePagoConceptoDTO getDeglosePorConcepto() {
		return deglosePorConcepto;
	}

	public void setDeglosePorConcepto(DesglosePagoConceptoDTO deglosePorConcepto) {
		this.deglosePorConcepto = deglosePorConcepto;
	}

	public ConceptosOrdenPagosDTO getConceptosOrdenPagosDTO() {
		return conceptosOrdenPagosDTO;
	}

	public void setConceptosOrdenPagosDTO(
			ConceptosOrdenPagosDTO conceptosOrdenPagosDTO) {
		this.conceptosOrdenPagosDTO = conceptosOrdenPagosDTO;
	}

	public List<OrdenPagosDTO> getListadoOrdenPagos() {
		return listadoOrdenPagos;
	}

	public void setListadoOrdenPagos(List<OrdenPagosDTO> listadoOrdenPagos) {
		this.listadoOrdenPagos = listadoOrdenPagos;
	}
	public OrdenPagosDTO getFiltroOrden() {
		return filtroOrden;
	}
	public void setFiltroOrden(OrdenPagosDTO filtroOrden) {
		this.filtroOrden = filtroOrden;
	}

	public Map<String, String> getTipoPagoMap() {
		return tipoPagoMap;
	}

	public void setTipoPagoMap(Map<String, String> tipoPagoMap) {
		this.tipoPagoMap = tipoPagoMap;
	}

	public Map<String, String> getTipoOrdenPagoMap() {
		return tipoOrdenPagoMap;
	}

	public void setTipoOrdenPagoMap(Map<String, String> tipoOrdenPagoMap) {
		this.tipoOrdenPagoMap = tipoOrdenPagoMap;
	}

	public Map<String, String> getEstautsMap() {
		return estautsMap;
	}

	public void setEstautsMap(Map<String, String> estautsMap) {
		this.estautsMap = estautsMap;
	}

	public Map<String, String> getTipoProveedorMap() {
		return tipoProveedorMap;
	}

	public void setTipoProveedorMap(Map<String, String> tipoProveedorMap) {
		this.tipoProveedorMap = tipoProveedorMap;
	}

	public Map<Long, String> getConceptoPagoMap() {
		return conceptoPagoMap;
	}

	public void setConceptoPagoMap(Map<Long, String> conceptoPagoMap) {
		this.conceptoPagoMap = conceptoPagoMap;
	}

	public String getLISTADOORDENPAGO() {
		return LISTADOORDENPAGO;
	}

	public String getCONTENEDOR_ORDENPAGODETALLE() {
		return CONTENEDOR_ORDENPAGODETALLE;
	}

	public String getCONTENEDOR_TOTALES() {
		return CONTENEDOR_TOTALES;
	}

	public String getLISTADETALLEORDENPAGO() {
		return LISTADETALLEORDENPAGO;
	}


	public Map<Long, String> getProveedorMap() {
		return proveedorMap;
	}


	public void setProveedorMap(Map<Long, String> proveedorMap) {
		this.proveedorMap = proveedorMap;
	}


	public Map<String, String> getTerminoAjusteMap() {
		return terminoAjusteMap;
	}


	public void setTerminoAjusteMap(Map<String, String> terminoAjusteMap) {
		this.terminoAjusteMap = terminoAjusteMap;
	}
	
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}


	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}


	public Long getIdOrdenPago() {
		return idOrdenPago;
	}


	public void setIdOrdenPago(Long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}


	public Long getIdReporteCabina() {
		return idReporteCabina;
	}


	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}


	public String getCONTENEDOR_ORDENPAGO() {
		return CONTENEDOR_ORDENPAGO;
	}


	public boolean getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(boolean soloLectura) {
		this.soloLectura = soloLectura;
	}


	public List<DetalleOrdenCompraDTO> getListaDetalleDTO() {
		return listaDetalleDTO;
	}


	public void setListaDetalleDTO(List<DetalleOrdenCompraDTO> listaDetalleDTO) {
		this.listaDetalleDTO = listaDetalleDTO;
	}


	public Map<String, String> getMotivoCancelacionMap() {
		return motivoCancelacionMap;
	}


	public void setMotivoCancelacionMap(Map<String, String> motivoCancelacionMap) {
		this.motivoCancelacionMap = motivoCancelacionMap;
	}


	public Map<String, String> getPagoaMap() {
		return pagoaMap;
	}


	public void setPagoaMap(Map<String, String> pagoaMap) {
		this.pagoaMap = pagoaMap;
	}


	public Long getIdCobertura() {
		return idCobertura;
	}


	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}


	public BloqueoPagoService getBloqueoPagoService() {
		return bloqueoPagoService;
	}


	public void setBloqueoPagoService(BloqueoPagoService bloqueoPagoService) {
		this.bloqueoPagoService = bloqueoPagoService;
	}


	public DetalleOrdenCompra getDetalleOrdenPago() {
		return detalleOrdenPago;
	}


	public void setDetalleOrdenPago(DetalleOrdenCompra detalleOrdenPago) {
		this.detalleOrdenPago = detalleOrdenPago;
	}
}