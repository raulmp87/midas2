package mx.com.afirme.midas2.action.siniestros.pagos.facturas;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.RelacionFacturaOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/facturas/recepcionFacturas")
public class RecepcionFacturaAction extends BaseAction implements Preparable{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<RelacionFacturaOrdenCompraDTO> listaFacturaOrdenCompra;
	private Map<String,Map<String, Object>> listaFacturasRelacionadas;
	private Map<String,String> mapOrdenFactura; 
	private File zipFacturas; 
	private Integer tipoUsuario;
	private Map<Long,String> listaOficinas;
	private Map<Long,String> listaFacturasCombo;
	private List<Map<String, Object>> listaPrestadoresServicio;
	private List<OrdenCompraProveedorDTO> listaOrdenesCompra;
	private List<MensajeValidacionFactura> listaValidacionesFactura;
	private List<DocumentoFiscal> listaFacturas;
	private String nombrePrestadorServicio;
	private Integer idPrestadorServicio;
	private Long idOficina;
	private Long idBatch;
	private Long idFactura;
	private Long idValidacionFactura;
	private Integer numFacturasCargadas;
	private List<EnvioValidacionFactura> listaFacturasCargadas;
	private String extensionArchivo;
		
	private ListadoService listadoService;
	private UsuarioService usuarioService;
	private OrdenCompraService ordenCompraService;
	private RecepcionFacturaService recepcionFacturaService;
	private PrestadorDeServicioService prestadorDeServicioService;
	private Oficina oficinaPrestadorServicio;
	
	@Autowired
	@Qualifier("recepcionFacturaServiceEJB")
	public void setRecepcionFacturaService(
			RecepcionFacturaService recepcionFacturaService) {
		this.recepcionFacturaService = recepcionFacturaService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Autowired
	@Qualifier("ordenCompraServiceEJB")
	public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
		this.ordenCompraService = ordenCompraService;
	}
	
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	public void setPrestadorDeServicioService(
			PrestadorDeServicioService prestadorDeServicioService) {
		this.prestadorDeServicioService = prestadorDeServicioService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void prepareMostrarRecepcionFacturas()
	{
		Usuario usuario = usuarioService.getUsuarioActual();
		tipoUsuario = usuario.getTipoUsuario();

		if(numFacturasCargadas== null)
		{
			numFacturasCargadas = 0;
		}
		
		listaPrestadoresServicio = prestadorDeServicioService.obtenerPrestadoresPorUsuario(usuario.getNombreUsuario());
		
		if(listaPrestadoresServicio.size() == 1) // un solo registro
		{
			idPrestadorServicio = Integer.valueOf(listaPrestadoresServicio.get(0).get("id").toString());
		}
		
		if(idPrestadorServicio != null)
		{
			//obtener el proveedor para mostrar su descripcion y oficina
			PrestadorServicio prestador = prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue());
			nombrePrestadorServicio = prestador.getPersonaMidas().getNombre();
			oficinaPrestadorServicio = prestador.getOficina();
		}				
	}
	
	@Action(value="mostrarRecepcionFacturas",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/facturas/recepcion/contenedorRecepcionFacturaOrdenCompra.jsp")
	})	
	public String mostrarRecepcionFacturas() {
		
		return SUCCESS;
	}
	
	@Action(value="listarOrdenesDeCompra",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/facturas/recepcion/recepcionFacturaOrdenCompraGrid.jsp")
	})	
	public String listarOrdenesDeCompra() {		  

		if(idPrestadorServicio != null)
		{			
			listaOrdenesCompra = ordenCompraService.obtenerOrdenesCompraProveedor(idPrestadorServicio);						
		}			
		
		if(idBatch != null)
		{
			listaFacturasCombo = recepcionFacturaService.obtenerFacturasPorBatchId(idBatch);								
		}		
		
		return SUCCESS;
	}	
	
	@Action(value="listarFacturas",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/facturas/recepcion/recepcionFacturaCargaFacturaGrid.jsp")
	})	
	public String listarFacturas() {	
		
		if(idBatch != null)
		{			
			listaFacturasCargadas = recepcionFacturaService.obtenerFacturasProcesadas(idBatch);			
		}		
		
		return SUCCESS;
	}
	
	@Action(value="verDetalleValidacionFactura",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/facturas/recepcion/recepcionFacturaResultadoValidacion.jsp")
	})	
	public String verDetalleValidacionFactura() {		
		
		listaValidacionesFactura = recepcionFacturaService.obtenerResultadosValidacion(idValidacionFactura);		
				
		return SUCCESS;
	}
	
	@Action(value="validarRegistrarFactura",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarRecepcionFacturas", 
			"namespace", "/siniestros/pagos/facturas/recepcionFacturas", 		
			"mensaje", "${mensaje}",
			"idBatch", "${idBatch}",
			"idPrestadorServicio","${idPrestadorServicio}",
			"tipoMensaje", "${tipoMensaje}"})
			})	
	public String validarRegistrarFactura() {		
		
		listaFacturasRelacionadas = new HashMap<String,Map<String, Object>>();
		
		listaFacturasCargadas = recepcionFacturaService.validarFacturasSiniestroAction(listaFacturaOrdenCompra, idBatch, idPrestadorServicio);		
		
		return SUCCESS;
	}	
	
	//results = {@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","idBatch,tipoMensaje,mensaje,numFacturasCargadas"})
	@Action(value="cargarArchivoFacturas",results={
			@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","idBatch,tipoMensaje,mensaje,numFacturasCargadas"}),
			@Result(name=INPUT,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","idBatch,tipoMensaje,mensaje,numFacturasCargadas"})
	})	//Nota: Se agrega parametro contentType = text/plain  para prevenir que internet explorer intente descargar un archivo con la respuesta json. 
	public String cargarArchivoFacturas() {		
		
		if( this.recepcionFacturaService.isValidoXmlSolo() && extensionArchivo.equals("xml") ){
			super.setMensajeError(super.getText("midas.siniestros.pagos.facturas.recepcionFacturas.solo.xml.error"));
			return INPUT;
		}else{
			listaFacturasCargadas = recepcionFacturaService.procesarFacturasZip(idPrestadorServicio, zipFacturas,extensionArchivo);		
			numFacturasCargadas = listaFacturasCargadas.size();
			idBatch = listaFacturasCargadas.size() >0 ?listaFacturasCargadas.get(0).getIdBatch():0L;

			this.setMensajeExito();
			
			return SUCCESS;
		}
		
	}
	
	@Action(value="obtenerOficina",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^oficinaPrestadorServicio\\.id,^oficinaPrestadorServicio\\.nombreOficina"})			
		})		
	public String obtenerOficina(){
		
		oficinaPrestadorServicio = prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue()).getOficina();
		
		return SUCCESS;
	}

	public File getZipFacturas() {
		return zipFacturas;
	}

	public List<RelacionFacturaOrdenCompraDTO> getListaFacturaOrdenCompra() {
		return listaFacturaOrdenCompra;
	}

	public void setListaFacturaOrdenCompra(
			List<RelacionFacturaOrdenCompraDTO> listaFacturaOrdenCompra) {
		this.listaFacturaOrdenCompra = listaFacturaOrdenCompra;
	}

	public void setZipFacturas(File zipFacturas) {
		this.zipFacturas = zipFacturas;
	}	

	public Map<Long, String> getListaOficinas() {
		return listaOficinas;
	}

	public Map<String, String> getMapOrdenFactura() {
		return mapOrdenFactura;
	}	

	public void setMapOrdenFactura(Map<String, String> mapOrdenFactura) {
		this.mapOrdenFactura = mapOrdenFactura;
	}

	public void setListaOficinas(Map<Long, String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}	

	public List<Map<String, Object>> getListaPrestadoresServicio() {
		return listaPrestadoresServicio;
	}

	public void setListaPrestadoresServicio(
			List<Map<String, Object>> listaPrestadoresServicio) {
		this.listaPrestadoresServicio = listaPrestadoresServicio;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNombrePrestadorServicio() {
		return nombrePrestadorServicio;
	}

	public List<MensajeValidacionFactura> getListaValidacionesFactura() {
		return listaValidacionesFactura;
	}

	public void setListaValidacionesFactura(
			List<MensajeValidacionFactura> listaValidacionesFactura) {
		this.listaValidacionesFactura = listaValidacionesFactura;
	}

	public Oficina getOficinaPrestadorServicio() {
		return oficinaPrestadorServicio;
	}

	public void setOficinaPrestadorServicio(Oficina oficinaPrestadorServicio) {
		this.oficinaPrestadorServicio = oficinaPrestadorServicio;
	}

	public void setNombrePrestadorServicio(String nombrePrestadorServicio) {
		this.nombrePrestadorServicio = nombrePrestadorServicio;
	}

	public Integer getIdPrestadorServicio() {
		return idPrestadorServicio;
	}

	public void setIdPrestadorServicio(Integer idPrestadorServicio) {
		this.idPrestadorServicio = idPrestadorServicio;
	}

	public Long getIdOficina() {
		return idOficina;
	}

	public List<OrdenCompraProveedorDTO> getListaOrdenesCompra() {
		return listaOrdenesCompra;
	}

	public void setListaOrdenesCompra(List<OrdenCompraProveedorDTO> listaOrdenesCompra) {
		this.listaOrdenesCompra = listaOrdenesCompra;
	}

	public List<DocumentoFiscal> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(List<DocumentoFiscal> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public Long getIdBatch() {
		return idBatch;
	}

	public void setIdBatch(Long idBatch) {
		this.idBatch = idBatch;
	}

	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}	

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}
	
	public Map<Long, String> getListaFacturasCombo() {
		return listaFacturasCombo;
	}

	public void setListaFacturasCombo(Map<Long, String> listaFacturasCombo) {
		this.listaFacturasCombo = listaFacturasCombo;
	}

	public Integer getNumFacturasCargadas() {
		return numFacturasCargadas;
	}

	public void setNumFacturasCargadas(Integer numFacturasCargadas) {
		this.numFacturasCargadas = numFacturasCargadas;
	}

	public List<EnvioValidacionFactura> getListaFacturasCargadas() {
		return listaFacturasCargadas;
	}

	public void setListaFacturasCargadas(
			List<EnvioValidacionFactura> listaFacturasCargadas) {
		this.listaFacturasCargadas = listaFacturasCargadas;
	}

	public Long getIdValidacionFactura() {
		return idValidacionFactura;
	}

	public void setIdValidacionFactura(Long idValidacionFactura) {
		this.idValidacionFactura = idValidacionFactura;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}
		
}
