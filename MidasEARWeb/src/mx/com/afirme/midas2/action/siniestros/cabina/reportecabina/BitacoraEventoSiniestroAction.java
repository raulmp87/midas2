package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.BitacoraEventoSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
/**
 * mx.com.afirme.midas2.action.siniestros.cabina.reportecabina
 * @author Armando Garcia
 * @version 1.0
 * @updated 24-jun-2014 02:06:15 p.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/bitacoraEvento")
public class BitacoraEventoSiniestroAction extends BaseAction implements Preparable{
	private static final long serialVersionUID = -5064991411387048766L;
	private BitacoraEventoSiniestroDTO filtroEvento;
	private String numeroReporte;
	private Long reporteCabinaId;
	private Short soloConsulta;
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	private Map<String, String> tiposEvento;
	private Map<String, String> tiposModulo;
	private TransporteImpresionDTO transporte;
	private List<BitacoraEventoSiniestro> listaEventos = new ArrayList<BitacoraEventoSiniestro>();
	private List<BitacoraEventoSiniestroDTO> bitacoraEventos = new ArrayList<BitacoraEventoSiniestroDTO>();	
	private static final String MOSTARCONTENEDOR ="/jsp/siniestros/cabina/reportecabina/bitacoraEventoSiniestro.jsp";
	private static final String MOSTARCONTENEDORGRID ="/jsp/siniestros/cabina/reportecabina/bitacoraEventoSiniestroGrid.jsp";	
	/**
	 * M�todo que invoca al m�todo buscarEventos de ReporteCabinaService en base a un
	 * filtro de b�squeda
	 * <b>VER DIAGRAMA DE CLASES EN CARPETA REPORTE DE CABINA</b>
	 */
	@Action(value="buscarEventos",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDORGRID),
			@Result(name=INPUT,location=MOSTARCONTENEDORGRID)
			})
	public String buscarEventos(){		
		bitacoraEventos = reporteCabinaService.buscarEventos(filtroEvento, reporteCabinaId);
		return SUCCESS;
	}
		
	
	/**
	 * M�todo que realiza la exportaci�n a excel, leer lineamiento de exportaci�n
	 */
	@Action(value="exportar",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})
		public String exportar() {		
		
			bitacoraEventos = reporteCabinaService.buscarEventos(filtroEvento, reporteCabinaId);
						
			ExcelExporter exporter = new ExcelExporter(BitacoraEventoSiniestroDTO.class);
			transporte = exporter.exportXLS(bitacoraEventos, "Listado de Eventos");
			
			return SUCCESS;
		}

	/**
	 * M�todo para mostrar la pantalla principal de bit�cora
	 */
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDOR),
			@Result(name=INPUT,location=MOSTARCONTENEDOR)
			})
	public String mostrarBitacora(){
		ReporteCabina reporte=reporteCabinaService.buscarReporte(reporteCabinaId);
		numeroReporte = reporte.getNumeroReporte();
		return SUCCESS;
	}

	/**
	 * Inicializar cat�logo de tiposModulo por medio de listadoService
	 */
	@Override
	public void prepare(){	
		tiposModulo = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MODULO); 
		tiposEvento=new LinkedHashMap<String, String>();
		tiposEvento.put("","Seleccione..");
	}
	
	public String getNumeroReporte() {
		return numeroReporte;
	}

	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}

	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}
	
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getTiposEvento() {
		return tiposEvento;
	}

	public void setTiposEvento(Map<String, String> tiposEvento) {
		this.tiposEvento = tiposEvento;
	}

	public Map<String, String> getTiposModulo() {
		return tiposModulo;
	}

	public void setTiposModulo(Map<String, String> tiposModulo) {
		this.tiposModulo = tiposModulo;
	}

	public BitacoraEventoSiniestroDTO getFiltroEvento() {
		return filtroEvento;
	}

	public void setFiltroEvento(BitacoraEventoSiniestroDTO filtroEvento) {
		this.filtroEvento = filtroEvento;
	}

	public List<BitacoraEventoSiniestroDTO> getBitacoraEventos() {
		return bitacoraEventos;
	}

	public void setBitacoraEventos(List<BitacoraEventoSiniestroDTO> bitacoraEventos) {
		this.bitacoraEventos = bitacoraEventos;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public List<BitacoraEventoSiniestro> getListaEventos() {
		return listaEventos;
	}

	public void setListaEventos(List<BitacoraEventoSiniestro> listaEventos) {
		this.listaEventos = listaEventos;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}
		
	
}