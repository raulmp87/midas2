package mx.com.afirme.midas2.action.siniestros.autorizacion;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroAutorizacionReserva;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
/**
 * @author simavera
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/catalogos/configuradorParametros")
public class ParametroAutReservaAction extends BaseAction implements Preparable {

	private static final String	LOCATION_PARAMETROAUTRESERVA_JSP	= "/jsp/siniestros/autorizacion/parametroAutReserva.jsp";

	private static final long serialVersionUID = -6929526575342515440L;
	
	private ParametroAutorizacionReserva parametro;
	private Long idParametro;
	private Long idToSeccion;
	private List<ParametroAutorizacionReserva> parametros;
	private List<SeccionDTO> seccionList;
	private Map<BigDecimal,String> coberturaList;
	private Map<Long,String> oficinaList;
	private Map<String,String> estatusList;
	private Map<String,String> criterioList;
	private String namespace;
	private String methodName;
	private Boolean rangoReserva;
	private Boolean rangoDeducible;
	private Boolean rangoPorcentajeDeducible;	
	private String estatus;	

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("seccionEJB")
	private SeccionFacadeRemote seccionFacade;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("autorizacionReservaServiceEJB")
	private AutorizacionReservaService autorizacionReservaService;
	
	public ParametroAutReservaAction(){

	}

	/**
	 * Se inicializan los listados
	 */
	public void prepare(){

		seccionList = seccionFacade.listarVigentesAutosUsables();
		oficinaList = listadoService.obtenerOficinasSiniestros();
		criterioList = listadoService.obtenerCatalogoValorFijo( CatGrupoFijo.TIPO_CATALOGO.CRITERIO_COMPARACION ); 
		estatusList = listadoService.obtenerCatalogoValorFijo( CatGrupoFijo.TIPO_CATALOGO.ESTATUS );
		coberturaList = new LinkedHashMap<BigDecimal, String>();
	}
	
	/**
	 * Mostrar pantalla principal
	 */
	@Action(value = "mostrarConfigurador", results = { @Result(name = SUCCESS, location = LOCATION_PARAMETROAUTRESERVA_JSP) })
	public String mostrarConfigurador(){	
		this.estatus ="true";
		return SUCCESS;
	}

	/**
	 * Invocar al metodo listarParametros de AutorizacionReservaService
	 */
	@Action(value = "listarParametros", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/autorizacion/parametroAutReservaGrid.jsp") })
	public String listarParametros(){
		this.parametros = autorizacionReservaService.listarParametros();
		return SUCCESS;
	}

	/**
	 * Invocar al metodo obtenerParametro de AutorizacionReservaService
	 */
	@Action(value = "buscar", results = { @Result(name = SUCCESS, location = LOCATION_PARAMETROAUTRESERVA_JSP) })
	public String mostrarParametro(){
		this.parametro = autorizacionReservaService.obtenerParametro( this.idParametro );

		if( parametro.getMontoReservaFinal() != null ){
			this.rangoReserva = true;
		}
		
		if( parametro.getMontoDeducibleFinal() != null ){
			this.rangoDeducible = true;
		}
		
		if( parametro.getMontoPorcentajeFinal() != null ){
			this.rangoPorcentajeDeducible = true;
		}
		
		this.estatus = parametro.getEstatus().toString();
		
		return SUCCESS;
	}
	
	/**
	 * Invocar al metodo guardarParametro de AutorizacionReservaService
	 */
	@Action(value = "guardar", results = {
			@Result(name = INPUT, location = LOCATION_PARAMETROAUTRESERVA_JSP),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName","${methodName}",
					"namespace","${namespace}",
					"idParametro", "${idParametro}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) })
	public String guardarParametro(){
		try {
			if( this.estatus.equals("1") || this.estatus.equals("true")){
				this.parametro.setEstatus( true );
			}else{
				this.parametro.setEstatus( false );
			}
			
			this.autorizacionReservaService.guardarParametro( this.parametro );
			super.setMensajeExito();
		} catch (Exception ex) {	
			return INPUT;
		}
		namespace = "/siniestros/catalogos/configuradorParametros";
        methodName = "mostrarConfigurador";
		return SUCCESS;
	}

	/**
	 * Invocar al metodo eliminarParametro de AutorizacionReservaService
	 */
	@Action(value = "eliminar", results = {
			@Result(name = INPUT, location = LOCATION_PARAMETROAUTRESERVA_JSP),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName","${methodName}",
					"namespace","${namespace}",
					"idParametro", "${idParametro}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) })
	public String eliminarParametro() {
		try {
			this.autorizacionReservaService.eliminarParamstatic( idParametro );
			super.setMensajeExito();
		} catch (Exception ex) {	
			return INPUT;
		}
		namespace = "/siniestros/catalogos/configuradorParametros";
        methodName = "mostrarConfigurador";
		return SUCCESS;
	}

	/**
	 * Invocar al metodo obtenerCoberturasPorSeccion de AutorizacionReservaService
	 */
	@Action(value = "obtenerCoberturaPorSeccion", results = { @Result(name = SUCCESS, location = LOCATION_PARAMETROAUTRESERVA_JSP) })
	public String obtenerCoberturaPorSeccion(){
		return SUCCESS;
	}
	
	/**
	 * @return the parametro
	 */
	public ParametroAutorizacionReserva getParametro() {
		return parametro;
	}

	/**
	 * @param parametro the parametro to set
	 */
	public void setParametro(ParametroAutorizacionReserva parametro) {
		this.parametro = parametro;
	}

	/**
	 * @return the idParametro
	 */
	public Long getIdParametro() {
		return idParametro;
	}

	/**
	 * @param idParametro the idParametro to set
	 */
	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	/**
	 * @return the idToSeccion
	 */
	public Long getIdToSeccion() {
		return idToSeccion;
	}

	/**
	 * @param idToSeccion the idToSeccion to set
	 */
	public void setIdToSeccion(Long idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	/**
	 * @return the parametros
	 */
	public List<ParametroAutorizacionReserva> getParametros() {
		return parametros;
	}

	/**
	 * @param parametros the parametros to set
	 */
	public void setParametros(List<ParametroAutorizacionReserva> parametros) {
		this.parametros = parametros;
	}

	/**
	 * @return the seccionList
	 */
	public List<SeccionDTO> getSeccionList() {
		return seccionList;
	}

	/**
	 * @param seccionList the seccionList to set
	 */
	public void setSeccionList(List<SeccionDTO> seccionList) {
		this.seccionList = seccionList;
	}

	/**
	 * @return the coberturaList
	 */
	public Map<BigDecimal,String> getCoberturaList() {
		return coberturaList;
	}

	/**
	 * @param coberturaList the coberturaList to set
	 */
	public void setCoberturaList(Map<BigDecimal,String> coberturaList) {
		this.coberturaList = coberturaList;
	}

	/**
	 * @return the oficinaList
	 */
	public Map<Long,String> getOficinaList() {
		return oficinaList;
	}

	/**
	 * @param oficinaList the oficinaList to set
	 */
	public void setOficinaList(Map<Long,String> oficinaList) {
		this.oficinaList = oficinaList;
	}

	/**
	 * @return the estatusList
	 */
	public Map<String, String> getEstatusList() {
		return estatusList;
	}

	/**
	 * @param estatusList the estatusList to set
	 */
	public void setEstatusList(Map<String, String> estatusList) {
		this.estatusList = estatusList;
	}

	/**
	 * @return the criterioList
	 */
	public Map<String, String> getCriterioList() {
		return criterioList;
	}

	/**
	 * @param criterioList the criterioList to set
	 */
	public void setCriterioList(Map<String, String> criterioList) {
		this.criterioList = criterioList;
	}
	
	/**
	 * @return the seccionFacade
	 */
	public SeccionFacadeRemote getSeccionFacade() {
		return seccionFacade;
	}

	/**
	 * @param seccionFacade the seccionFacade to set
	 */
	public void setSeccionFacade(SeccionFacadeRemote seccionFacade) {
		this.seccionFacade = seccionFacade;
	}

	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}

	/**
	 * @param listadoService
	 *            the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the entidadService
	 */
	public EntidadService getEntidadService() {
		return entidadService;
	}
	
	/**
	 * @param entidadService the entidadService to set
	 */
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the rangoReserva
	 */
	public Boolean getRangoReserva() {
		return rangoReserva;
	}

	/**
	 * @param rangoReserva the rangoReserva to set
	 */
	public void setRangoReserva(Boolean rangoReserva) {
		this.rangoReserva = rangoReserva;
	}

	/**
	 * @return the rangoDeducible
	 */
	public Boolean getRangoDeducible() {
		return rangoDeducible;
	}

	/**
	 * @param rangoDeducible the rangoDeducible to set
	 */
	public void setRangoDeducible(Boolean rangoDeducible) {
		this.rangoDeducible = rangoDeducible;
	}

	/**
	 * @return the rangoPorcentajeDeducible
	 */
	public Boolean getRangoPorcentajeDeducible() {
		return rangoPorcentajeDeducible;
	}

	/**
	 * @param rangoPorcentajeDeducible the rangoPorcentajeDeducible to set
	 */
	public void setRangoPorcentajeDeducible(Boolean rangoPorcentajeDeducible) {
		this.rangoPorcentajeDeducible = rangoPorcentajeDeducible;
	}

	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
}