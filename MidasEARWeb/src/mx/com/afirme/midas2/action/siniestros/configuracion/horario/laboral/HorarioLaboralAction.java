package mx.com.afirme.midas2.action.siniestros.configuracion.horario.laboral;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.laboral.HorarioLaboralService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.laboral.HorarioLaboralService.HorarioLaboralFiltro;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/configuracion/horario/laboral/horarioLaboral")
public class HorarioLaboralAction extends BaseAction implements Preparable {

	private static final long		serialVersionUID	= 4104147946791965757L;
	private static final String		jSPsFolder			= "/jsp/siniestros/configuracion/horario/laboral/";
	private static final String		jspContenedor		= jSPsFolder
																+ "horarioLaboralCatalogo.jsp";
	private static final String		jspGrid				= jSPsFolder
																+ "horarioLaboralGrid.jsp";
	private static final String		jspDetalle			= jSPsFolder
																+ "horarioLaboralDetalle.jsp";
	// # 1-ALTA 0-MODIFICAR 2-VER DETALLE READ ONLY
	private short					accion				= 1;
	private short					cargaInicial		= 1;
	private List<HorarioLaboral>	horarioLaboralGrid;
	private HorarioLaboralFiltro	horarioLaboralFiltro;
	private HorarioLaboral			horario;
	private Map<String, String>		ctgEstatus			= null;
	private Map<String, String>		ctgHorarios			= null;
	private Long					horarioId;

	@Autowired
	@Qualifier("horarioLaboralServiceEJB")
	private HorarioLaboralService	horarioLaboralService;

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService			entidadService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService			listadoService;

	@Override
	public void prepare(){

	}

	@Action(value = "listarCarga", results = { @Result(name = SUCCESS, location = jspGrid) })
	public String listarCarga() {
		this.horarioLaboralGrid = this.entidadService
				.findAll(HorarioLaboral.class);
		return SUCCESS;
	}

	@Action(value = "mostrar", results = {
			@Result(name = SUCCESS, location = jspContenedor),
			@Result(name = INPUT, location = jspContenedor) })
	public String mostrar() {
		return SUCCESS;
	}

	@Action(value = "busqueda", results = {
			@Result(name = SUCCESS, location = jspGrid),
			@Result(name = INPUT, location = jspGrid) })
	public String buscar() {
		this.horarioLaboralGrid = this.horarioLaboralService
				.listar(horarioLaboralFiltro);
		return SUCCESS;
	}

	@Action(value = "mostrarAlta", results = { @Result(name = SUCCESS, location = jspContenedor) })
	public String alta() {
		return SUCCESS;
	}

	@Action(value = "verDetalle", results = { @Result(name = SUCCESS, location = jspDetalle) })
	public String verDetalle() {
		this.horario = this.horarioLaboralService.obtener(this.horarioId);
		// System.out.println(this.horarioLaboral.getFechaCreacion());
		return SUCCESS;
	}

	@Action(value = "salvarActualizar", results = {
			@Result(name = INPUT, location = jspDetalle),
			@Result(name = SUCCESS, location = jspDetalle) })
	public String salvarActualizar() {
		try {
			this.horarioLaboralService.guardar(horario);
		} catch (NegocioEJBExeption ex) {
			String[] mensajesArray = Arrays.copyOf(ex.getValues(),
					ex.getValues().length, String[].class);
			List<String> mensajes = Arrays.asList(mensajesArray);
			if (mensajes.size() > 0) {
				super.setMensajeListaPersonalizado("Mensajes de Validación",
						mensajes, TIPO_MENSAJE_INFORMACION);
			}
			return INPUT;
		} catch (Exception ex) {
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException) ex
						.getCause();
				addErrors(
						constraintViolationException.getConstraintViolations(),
						"horarioLaboral");
			}
			return INPUT;
		}
		horario = null;
		accion = 1;
		return SUCCESS;
	}

	public short getAccion() {
		return accion;
	}

	public void setAccion(short accion) {
		this.accion = accion;
	}

	public short getCargaInicial() {
		return cargaInicial;
	}

	public void setCargaInicial(short cargaInicial) {
		this.cargaInicial = cargaInicial;
	}

	public List<HorarioLaboral> getHorarioLaboralGrid() {
		if (HorarioLaboral.ctgHorarios.size() == 0)
			HorarioLaboral.ctgHorarios = (ctgHorarios != null) ? ctgHorarios
					: this.listadoService
							.obtenerCatalogoValorFijo(TIPO_CATALOGO.HORA_LABORAL);
		return horarioLaboralGrid;
	}

	public void setHorarioLaboralGrid(List<HorarioLaboral> horarioLaboralGrid) {
		this.horarioLaboralGrid = horarioLaboralGrid;
	}

	public HorarioLaboralFiltro getHorarioLaboralFiltro() {
		return horarioLaboralFiltro;
	}

	public void setHorarioLaboralFiltro(
			HorarioLaboralFiltro horarioLaboralFiltro) {
		this.horarioLaboralFiltro = horarioLaboralFiltro;
	}

	public HorarioLaboral getHorarioLaboral() {
		return horario;
	}

	public String getEstatusPorDefault() {
		// Constraint del CDU {14307D31-BB5D-4ae3-97F5-C83B0B02DE25} / Analisis
		// MIDAS II.Use Case Model.MODULO SINIESTROS AUTOS.CABINA.Configurador
		// de Horarios de Ajustadores.CDU Definir Horarios Laborales : Estatus
		// para Nuevos - Se debe mostrar por default la opción "Activo" en el
		// Estatus del horario, siempre y cuando sea un nuevo horario.
		if (accion == 1) {
			return "1";
		} else {
			return horario.getEstatus().toString();
		}
	}

	public void setHorarioLaboral(HorarioLaboral horarioLaboral) {
		this.horario = horarioLaboral;
	}

	public Map<String, String> getCtgEstatus() {
		if (ctgEstatus == null)
			ctgEstatus = this.listadoService
					.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		return ctgEstatus;
	}

	public Long getHorarioId() {
		return horarioId;
	}

	public void setHorarioId(Long horarioId) {
		this.horarioId = horarioId;
	}

	public HorarioLaboralService getHorarioLaboralService() {
		return horarioLaboralService;
	}

	public void setHorarioLaboralService(
			HorarioLaboralService horarioLaboralService) {
		this.horarioLaboralService = horarioLaboralService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getCtgHorarios() {
		if (ctgHorarios == null)
			ctgHorarios = this.listadoService
					.obtenerCatalogoValorFijo(HorarioLaboral.ctgHorariosTipo);
		return ctgHorarios;
	}

}
