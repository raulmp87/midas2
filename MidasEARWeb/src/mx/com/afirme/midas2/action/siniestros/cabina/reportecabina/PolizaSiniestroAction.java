/**
 * 
 */
package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.cliente.DireccionCliente;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/poliza")
public class PolizaSiniestroAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = -5064991411387048766L;

	private Long idToPoliza;
	private Long incisoContinuityId;
	private Long idReporte;
	private Date validOn;
	private Date recordFrom;
	private String numeroInciso;
	private String condicionesEspeciales;
	private IncisoReporteCabina incisoReporte;
	private List<IncisoSiniestroDTO> listIncisos;
	private IncisoSiniestroDTO filtroBusqueda;
	private IncisoSiniestroDTO detalleInciso;
	private List<BitemporalCoberturaSeccion> biCoberturaSeccionList;
	private List<CondicionEspecialDTO> condicionesEspecialesList;
	private List<ControlDinamicoRiesgoDTO> controles;
	private Long idCobertura;
	private int incisosAsociadosReporte;
	private int incisoVigenteAutorizado;
	private BigDecimal idCliente;
	private ClienteGenericoDTO filtroCliente;
	private Map<String, String> tiposPersona;
	private List<IncisoSiniestroDTO> incisos;
	private Map<Short, String> listTipoServicio;
	private Boolean busquedaRealizada;
	private Date fechaReporteSiniestro;
	private List<DireccionCliente> listDireccionesCliente;
	private DireccionCliente direccionCliente;
	private String estatusPoliza;
	private Short codigoEstatusPoliza;
	private String autorizadorSelected;
	private String numeroPolizaFormateado;
	private Integer estatusSolicitud;
	private ToProrroga prorroga;
	private String namespace;
	private String methodName;
	private Long validOnMillis;
	private Long recordFromMillis;
	private String horaOcurrido;
	private Long fechaReporteSiniestroMillis;  
	private Integer tipoValidacion;
	private Long idToSolicitud;
	private Long validOnMillisInciso;
	private Long recordFromMillisInciso;
	private Date validOnInciso;
	private Date recordFromInciso;
	private Short soloConsulta;
	private String estatusVigenciaInciso;
	private boolean busquedaPolizas;
	private String condicionEspecialId;
	private String detalleCondicion;
	private String sumaStr;


	@Autowired
	@Qualifier("polizaSiniestroServiceEJB")
	private PolizaSiniestroService polizaSiniestroService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Autowired
	@Qualifier("configuracionDatoIncisoBitemporalServiceEJB")
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService;

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

		
	@Override
	public void prepare(){
		busquedaRealizada = Boolean.FALSE;

	}

	@Action(value = "mostrarBusqueda", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/busquedaPolizaSiniestro.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenedorReporteCabina.jsp") })
	public String mostrarBusqueda() {
		busquedaPolizas = true;
		validateInfoDate();
		return SUCCESS;
	}
	
	@Action(value = "mostrarBusquedaPolizas", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/busquedaPolizas.jsp")})
	public String mostrarBusquedaPolizas() {
		 fechaReporteSiniestro = new Date();
		 fechaReporteSiniestroMillis = fechaReporteSiniestro.getTime();
		 filtroBusqueda = new IncisoSiniestroDTO();
		 busquedaPolizas = false;
		return SUCCESS;
	}

	
	@Action(value = "buscarPoliza", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/incisosPolizaSiniestroGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/incisosPolizaSiniestroGrid.jsp") })
	public String buscarPoliza() {

		if (filtroBusqueda != null) {

			try {
				validateInfoDate();
				filtroBusqueda.setFechaReporteSiniestro(fechaReporteSiniestro);
				filtroBusqueda.setNombreContratante(StringUtil.decodeUri(filtroBusqueda.getNombreContratante()));
				this.incisos = polizaSiniestroService.buscarPolizaFiltro(filtroBusqueda, fechaReporteSiniestro, fechaReporteSiniestro);
				busquedaRealizada = Boolean.TRUE;
				incisoContinuityId = null;
				idToSolicitud = null;

			} catch (Exception e) {
				return INPUT;
			}
		}

		return SUCCESS;
	}

	@Action(value = "mostrarDetalleInciso", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/consultaPolizaSiniestro.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/consultaPolizaSiniestro.jsp") })
	public String mostrarDetalleInciso() {

		IncisoSiniestroDTO incisoSiniestroDTO = initDataToFindInciso();
		this.validOn = new Date(this.validOnMillis);
		this.recordFrom = new Date(this.recordFromMillis);
		validateInfoDate();		
		
		this.detalleInciso = polizaSiniestroService.buscarDetalleInciso(incisoSiniestroDTO, validOn, recordFrom, fechaReporteSiniestro);

		this.idToPoliza = this.detalleInciso.getIdToPoliza().longValue();
		this.estatusSolicitud = this.polizaSiniestroService.validaSiTieneAutorizacionEnEsperaYEstatus(this.idToPoliza,this.detalleInciso.getDescEstatus(), idReporte);
		this.validOn = this.detalleInciso.getValidOn();
		this.validOnMillis = this.validOn.getTime();
		this.recordFrom = this.detalleInciso.getRecordFrom();
		this.recordFromMillis = this.recordFrom.getTime();
		
		incisoVigenteAutorizado = polizaSiniestroService.incisoVigenteAutorizado(this.incisoContinuityId,this.idToPoliza, 
				validOn, recordFrom, fechaReporteSiniestro, detalleInciso.getEstatus(), idReporte);
		
		this.condicionesEspecialesList = polizaSiniestroService.buscarCondicionesEspeciales(this.incisoContinuityId,
				this.idReporte, this.fechaReporteSiniestro, this.validOn, this.recordFrom);
		
		return SUCCESS;
	}
	
	
	@Action(value = "mostrarDetalleCondicion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/condicionEspecialDetalle.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/condicionEspecialDetalle.jsp") })
	public String mostrarDetalleCondicion() {
		CondicionEspecial condicion =  entidadService.findById(CondicionEspecial.class, Long.valueOf(condicionEspecialId));
		if(condicion != null){
			detalleCondicion = condicion.getDescripcion();
		}		
		return SUCCESS;
	}

	/**
	 * Obtiene el listado de las Coberturas asociadas a un Inciso de una Poliza
	 */
	@Action(value = "mostrarCoberturasInciso", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/coberturasIncisoGrid.jsp") })
	public String mostrarCoberturasInciso() {
		this.validOn = new Date( this.validOnMillis );
		this.recordFrom = new Date( this.recordFromMillis );
		validateInfoDate();
		
		Map<String, Object> mapCoberturas = polizaSiniestroService
				.obtenerCoberturasIncisoMap(this.incisoContinuityId,
						new DateTime( this.validOnMillis ),
						new DateTime( this.recordFromMillis ), fechaReporteSiniestro);
		
		this.biCoberturaSeccionList = (List<BitemporalCoberturaSeccion>) mapCoberturas.get("biCoberturaSeccionList");
		boolean imprimirUMA = (Boolean) mapCoberturas.get("imprimirUMA");
		
		if (imprimirUMA){
			this.setSumaStr(getText("midas.siniestros.cabina.reportecabina.afectacionInciso.UMA"));
		}else{
			this.setSumaStr(getText("midas.siniestros.cabina.reportecabina.afectacionInciso.DSMGVDF"));
		}
		
		return SUCCESS;
	}

	@Action(value = "obtenerDatosRiesgo", results = { @Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/controlDinamicoRiesgoDetalle.jsp") })
	public String obtenerDatosRiesgo() {
		validateInfoDate();
		controles = polizaSiniestroService.obtenerDatosRiesgo(incisoContinuityId, detalleInciso.getIdToCotizacion().longValue(), 
				new Date(validOnMillis), new Date(recordFromMillis), fechaReporteSiniestro, idCobertura);

		return SUCCESS;
	}

	/**
	 * Muestra Condiciones Especiales asociadas a un Inciso de una Poliza
	 * 
	 * @return
	 */
	@Action(value = "mostrarCondicionesEspecialesInciso", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/condicionEspecialReporte.jsp") })
	public String mostrarCondicionesEspeciales() {
		validateInfoDate();
		this.incisosAsociadosReporte = polizaSiniestroService.incisoAsociadoReporteCabina(this.incisoContinuityId,this.idToPoliza, this.idReporte, this.fechaReporteSiniestro);
		return SUCCESS;
	}

	/**
	 * Obtiene el listado de las Condiciones Especiales asociadas a un Inciso de
	 * una Poliza
	 * 
	 * @return
	 */
	@Action(value = "mostrarCondicionesEspecialesIncisoGrid", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/condicionEspecialReporteGrid.jsp") })
	public String mostrarCondicionesEspecialesGrid() {
		validateInfoDate();
		if( validOnMillis != null ){
			this.validOn = new Date(this.validOnMillis);
		}
		if( recordFromMillis != null ){
			this.recordFrom = new Date(this.recordFromMillis);
		}
		this.condicionesEspecialesList = polizaSiniestroService.buscarCondicionesEspeciales(this.incisoContinuityId,
				this.idReporte, this.fechaReporteSiniestro, this.validOn, this.recordFrom);
		return SUCCESS;
	}

	/**
	 * Obtiene el listado de las Condiciones Especiales asociadas a un Inciso de
	 * una Poliza
	 * 
	 * @return
	 */
	@Action(value = "asociarCondicionesEspecialesInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/condicionEspecialReporte.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/condicionEspecialReporte.jsp") })
	public String asociarCondicionesEspeciales() {
		try {
			polizaSiniestroService.setAutoInciso(null);
			switch(tipoValidacion){
				case 0 : 	polizaSiniestroService.asignarCondicionesEspeciales(this.idReporte,this.condicionesEspeciales, 
								PolizaSiniestroService.TIPO_VALIDACION_CONDICION.VALIDAR_REPORTE);
							break;
				case 2 : 	polizaSiniestroService.asignarCondicionesEspeciales(this.idReporte,this.condicionesEspeciales, 
								PolizaSiniestroService.TIPO_VALIDACION_CONDICION.VALIDAR_SINIESTRO);
							break;
			}
			List<String> mensajes = polizaSiniestroService.getMensajesEvaluacion();
			if(mensajes.size() > 0){
				super.setMensajeListaPersonalizado(
						"Validaciones",  
						mensajes, BaseAction.TIPO_MENSAJE_INFORMACION);
			}else{
				super.setMensajeExito();
			}

		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}

	
	
	
	
	
	/**
	 * Asigna un Inciso de una Poliza a un Reporte Cabina
	 * 
	 * @return
	 */
	//@Action(value = "asignarIncisoReporte", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/consultaPolizaSiniestro.jsp") })
	@Action(value="asignarIncisoReporte",results={								 
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","${methodName}",
					"namespace","${namespace}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"incisoContinuityId","${incisoContinuityId}",
					"idReporte","${idReporte}",
					"filtroBusqueda.servicioPublico","${filtroBusqueda.servicioPublico}",
					"filtroBusqueda.autoInciso.numeroSerie","${filtroBusqueda.autoInciso.numeroSerie}",
					"filtroBusqueda.autoInciso.placa","${filtroBusqueda.autoInciso.placa}",
					"filtroBusqueda.autoInciso.nombreAsegurado","${filtroBusqueda.autoInciso.nombreAsegurado}",
					"filtroBusqueda.servicioParticular","${filtroBusqueda.servicioParticular}",
					"filtroBusqueda.numeroPoliza","${filtroBusqueda.numeroPoliza}",
					"filtroBusqueda.autoInciso.numeroMotor","${filtroBusqueda.autoInciso.numeroMotor}",
					"filtroBusqueda.fechaIniVigencia","${filtroBusqueda.fechaIniVigencia}",
					"filtroBusqueda.fechaFinVigencia","${filtroBusqueda.fechaFinVigencia}",
					"validOnMillis","${validOnMillis}",
					"recordFromMillis","${recordFromMillis}",
					"fechaReporteSiniestroMillis","${fechaReporteSiniestroMillis}",
					"fechaReporteSiniestro","${fechaReporteSiniestro}"})
	})	
	public String asignarInciso() {
		
		if( !polizaSiniestroService.validacionTieneCoberturasAfectadas(idReporte)){
			try {
				this.validOn = new Date( this.validOnMillis );
				this.recordFrom = new Date( this.recordFromMillis );
				this.fechaReporteSiniestro = new Date(this.fechaReporteSiniestroMillis);
				
				polizaSiniestroService.asignarInciso(this.idReporte,this.idToPoliza, incisoContinuityId, this.validOn,this.recordFrom, this.fechaReporteSiniestro);
				setMensaje(MensajeDTO.MENSAJE_ASIGNAR_INCISO);
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			} catch (Exception e) {
				setMensaje(MensajeDTO.MENSAJE_ERROR_ASIGNAR_INCISO);
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			}
		}else{
			setMensaje(MensajeDTO.MENSAJE_ERROR_COBERTURAS_AFECTADAS);
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
		}
		namespace = "/siniestros/cabina/reporteCabina/poliza";
        methodName = "mostrarDetalleInciso";
		return SUCCESS;
	}

	/**
	 * Muestra los datos del cliente
	 * 
	 * @return
	 */
	@Action(value = "mostrarDatosCliente", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/contenedorDatosAsegurado.jsp"), 
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenedorDatosAsegurado.jsp")})
	public String mostrarDatosCliente() {

		try {
			this.tiposPersona = listadoService
					.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PERSONA);
			filtroCliente = polizaSiniestroService
					.buscarDatosCliente(this.idCliente);
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}

	/**
	 * Muestra las direcciones del cliente ya sea personal o moral
	 * 
	 * @return
	 */
	@Action(value = "buscarDireccionesCliente", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/direccionesAseguradoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/direccionesAseguradoGrid.jsp")})
	public String buscarDireccionesCliente() {

		try {
			this.listDireccionesCliente = polizaSiniestroService
					.buscarDireccionesCliente(this.idCliente);
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "mostrarDatosRiesgo", results = { @Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/controlDinamicoRiesgoDetalle.jsp") })
	public String mostrarDatosRiesgo() {

		controles = polizaSiniestroService.obtenerDatosRiesgo(incisoContinuityId, detalleInciso.getIdToCotizacion().longValue(), 
				new Date(validOnMillis), new Date(recordFromMillis), fechaReporteSiniestro);

		return SUCCESS;
	}

	public String mostrarVentanaDatosRiesgo() {
		return SUCCESS;
	}

	/*
	 * Como parametro recive el incisoContinuityId , pero tiene el valor de
	 * idToSolicitudDataEnTramite
	 */
	@Action(value = "mostrarDatosVehiculo", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/datosVehiculoSeleccionado.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/datosVehiculoSeleccionado.jsp") })
	public String mostrarDatosVehiculo() {

		if (idToSolicitud != null) {
			this.detalleInciso = polizaSiniestroService.buscarSolicitudPolizaById(BigDecimal.valueOf(this.idToSolicitud));
		} else {
			IncisoSiniestroDTO incisoSiniestroDTO = initDataToFindInciso();
			validateInfoDate();
			this.detalleInciso = polizaSiniestroService.buscarInciso(incisoSiniestroDTO, new Date(validOnMillis),new Date(recordFromMillis));
			this.estatusVigenciaInciso= polizaSiniestroService.obtenerSituacionVigenciaInciso(
					this.detalleInciso.getValidOn(), fechaReporteSiniestro, 
					this.detalleInciso.getNumeroSerie(), this.detalleInciso.getIdToPoliza());
			idToSolicitud = null;
		}

		return SUCCESS;
	}

	@Action(value = "mostrarSolicitarAutorizacion", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/solicitarAutorizacion.jsp") })
	public String mostrarSolicitarAutorizacion() {
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				BigDecimal.valueOf(idToPoliza));
		this.numeroPolizaFormateado = poliza.getNumeroPolizaFormateada();
		return SUCCESS;
	}

	@Action(value = "enviarSolicitudAutorizacion", results = {
			@Result(name = SUCCESS, params={"estatusSolicitud","${estatusSolicitud}"} , location = "/jsp/siniestros/cabina/reportecabina/botonAutorizarPoliza.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/botonAutorizarPoliza.jsp") })
	public String enviarsolicitudAutorizacion() {

		try {
			this.polizaSiniestroService.enviarSolicitudAutorizacionVigencia(this.idToPoliza, new BigDecimal(this.numeroInciso),this.idReporte, this.codigoEstatusPoliza);
			setMensajeExito();
			estatusSolicitud = this.polizaSiniestroService.validaSiTieneAutorizacionEnEsperaYEstatus(this.idToPoliza,"", idReporte);
		} catch (Exception e) {
			super.setMensajeError(getText("midas.solicitarAutorizacionVigencia.mensajeError"));
		}
		return SUCCESS;
	}
		
	
	

	/**
	 * Muestra los datos de Prorroga
	 * 
	 * @return
	 */
	@Action(value = "mostrarProrroga", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/contenedorProrroga.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenedorProrroga.jsp")})
	public String mostrarProrroga() {
		PolizaDTO poliza = null;
		try {
			poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(
					idToPoliza));

			if (poliza != null
					&& poliza.getCotizacionDTO().getIdToCotizacion() != null) {
				this.prorroga = polizaSiniestroService.obtenerProrroga(poliza
						.getCotizacionDTO().getIdToCotizacion().longValue(),
						this.incisoContinuityId, this.validOn);
			}

		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	private IncisoSiniestroDTO initDataToFindInciso(){
		
		IncisoSiniestroDTO incisoSiniestroDTO = new IncisoSiniestroDTO();

		incisoSiniestroDTO.setIncisoContinuityId(this.incisoContinuityId);
		if (fechaReporteSiniestroMillis != null) {
			incisoSiniestroDTO.setFechaReporteSiniestro(new Date(fechaReporteSiniestroMillis));
		} else {
			incisoSiniestroDTO.setFechaReporteSiniestro(fechaReporteSiniestro);
		}
		
		incisoSiniestroDTO.setValidOnMillis(validOnMillisInciso);
		incisoSiniestroDTO.setRecordFromMillis(recordFromMillisInciso);
		
		this.validOn = new Date( this.validOnMillis );
		this.recordFrom = new Date( this.recordFromMillis );
		
//		this.validOnInciso = new Date( this.validOnMillisInciso );
//		this.recordFromInciso = new Date( this.recordFromMillisInciso );
		
		return incisoSiniestroDTO;
	}
	
	public void validateInfoDate(){
		SimpleDateFormat fechaHora	= new SimpleDateFormat("dd/M/yyyy H:mm");
		SimpleDateFormat fecha 		= new SimpleDateFormat("dd/M/yyyy");
		String fechaCompleta  		= "";
		
		try {
			if( this.fechaReporteSiniestroMillis != null){
				this.fechaReporteSiniestro = new Date(this.fechaReporteSiniestroMillis );
				if(this.detalleInciso != null){
					this.detalleInciso.setFechaReporteSiniestro(this.fechaReporteSiniestro);
				}
			}
			else if(this.fechaReporteSiniestro != null && SystemCommonUtils.isValid(this.horaOcurrido)){
					fechaCompleta = fecha.format( this.fechaReporteSiniestro) + " " + this.horaOcurrido;
					this.fechaReporteSiniestro = fechaHora.parse(fechaCompleta) ;
					this.fechaReporteSiniestroMillis = this.fechaReporteSiniestro.getTime();
					if(this.detalleInciso != null){
						this.detalleInciso.setFechaReporteSiniestro(this.fechaReporteSiniestro);
					}
					
				}
			} catch (ParseException e) {

			}
	}
	
	
	
	
	@Action(value = "consultaDetalleInciso", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/consultaPolizaSiniestro.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenedorReporteCabina.jsp") })
	public String consultaDetalleInciso() {

		IncisoSiniestroDTO incisoSiniestroDTO = initDataToFindInciso();
		this.validOn = new Date(this.validOnMillis);
		this.recordFrom = new Date(this.recordFromMillis);
		
		this.detalleInciso = polizaSiniestroService.buscarDetalleInciso(incisoSiniestroDTO, validOn,recordFrom, fechaReporteSiniestro);

		this.idToPoliza = this.detalleInciso.getIdToPoliza().longValue();
		this.estatusSolicitud = this.polizaSiniestroService.validaSiTieneAutorizacionEnEsperaYEstatus(this.idToPoliza,this.detalleInciso.getDescEstatus(), idReporte);
		this.validOn = this.detalleInciso.getValidOn();
		this.validOnMillis = this.validOn.getTime();
		this.recordFrom = this.detalleInciso.getRecordFrom();
		this.recordFromMillis = this.recordFrom.getTime();
		
		incisoVigenteAutorizado = polizaSiniestroService.incisoVigenteAutorizado(this.incisoContinuityId,this.idToPoliza, 
				validOn, recordFrom, fechaReporteSiniestro, detalleInciso.getEstatus(), idReporte);
		

		return SUCCESS;
	}
	

	/**
	 * ********************************************************************************************************************************
	 * **************************************************** GET & SET *************************************************************
	 * ********************************************************************************************************************************
	 */

	/**
	 * @return the polizaSiniestroService
	 */
	public PolizaSiniestroService getPolizaSiniestroService() {
		return polizaSiniestroService;
	}

	/**
	 * @param polizaSiniestroService
	 *            the polizaSiniestroService to set
	 */
	public void setPolizaSiniestroService(
			PolizaSiniestroService polizaSiniestroService) {
		this.polizaSiniestroService = polizaSiniestroService;
	}

	/**
	 * @return the idToPoliza
	 */
	public Long getIdToPoliza() {
		return idToPoliza;
	}

	/**
	 * @param idToPoliza
	 *            the idToPoliza to set
	 */
	public void setIdToPoliza(Long idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	/**
	 * @return the incisoContinuityId
	 */
	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	/**
	 * @param incisoContinuityId
	 *            the incisoContinuityId to set
	 */
	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}

	/**
	 * @param idReporte
	 *            the idReporte to set
	 */
	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}

	/**
	 * @return the idReporte
	 */
	public Long getIdReporte() {
		return idReporte;
	}

	/**
	 * @return the incisoReporte
	 */
	public IncisoReporteCabina getIncisoReporte() {
		return incisoReporte;
	}

	/**
	 * @param incisoReporte
	 *            the incisoReporte to set
	 */
	public void setIncisoReporte(IncisoReporteCabina incisoReporte) {
		this.incisoReporte = incisoReporte;
	}

	/**
	 * @return the validOn
	 */
	public Date getValidOn() {
		return validOn;
	}

	/**
	 * @param validOn
	 *            the validOn to set
	 */
	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}

	/**
	 * @return the recordFrom
	 */
	public Date getRecordFrom() {
		return recordFrom;
	}

	/**
	 * @param recordFrom
	 *            the recordFrom to set
	 */
	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	/**
	 * @param numeroInciso
	 *            the numeroInciso to set
	 */
	public void setNumeroInciso(String numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	/**
	 * @param condicionesEspeciales
	 *            the condicionesEspeciales to set
	 */
	public void setCondicionesEspeciales(String condicionesEspeciales) {
		this.condicionesEspeciales = condicionesEspeciales;
	}

	/**
	 * @return the condicionesEspeciales
	 */
	public String getCondicionesEspeciales() {
		return condicionesEspeciales;
	}

	/**
	 * @return the listIncisos
	 */
	public List<IncisoSiniestroDTO> getListIncisos() {
		return listIncisos;
	}

	/**
	 * @param listIncisos
	 *            the listIncisos to set
	 */
	public void setListIncisos(List<IncisoSiniestroDTO> listIncisos) {
		this.listIncisos = listIncisos;
	}

	/**
	 * @return the filtroBusqueda
	 */
	public IncisoSiniestroDTO getFiltroBusqueda() {
		return filtroBusqueda;
	}

	/**
	 * @param filtroBusqueda
	 *            the filtroBusqueda to set
	 */
	public void setFiltroBusqueda(IncisoSiniestroDTO filtroBusqueda) {
		this.filtroBusqueda = filtroBusqueda;
	}

	/**
	 * @return the detalleInciso
	 */
	public IncisoSiniestroDTO getDetalleInciso() {
		return detalleInciso;
	}

	/**
	 * @param detalleInciso
	 *            the detalleInciso to set
	 */
	public void setDetalleInciso(IncisoSiniestroDTO detalleInciso) {
		this.detalleInciso = detalleInciso;
	}

	/**
	 * @return the numeroInciso
	 */
	public String getNumeroInciso() {
		return numeroInciso;
	}

	/**
	 * @param biCoberturaSeccionList
	 *            the biCoberturaSeccionList to set
	 */
	public void setBiCoberturaSeccionList(
			List<BitemporalCoberturaSeccion> biCoberturaSeccionList) {
		this.biCoberturaSeccionList = biCoberturaSeccionList;
	}

	/**
	 * @return the biCoberturaSeccionList
	 */
	public List<BitemporalCoberturaSeccion> getBiCoberturaSeccionList() {
		return biCoberturaSeccionList;
	}

	/**
	 * @return the condicionesEspecialesList
	 */
	public List<CondicionEspecialDTO> getCondicionesEspecialesList() {
		return condicionesEspecialesList;
	}

	/**
	 * @param condicionesEspecialesList
	 *            the condicionesEspecialesList to set
	 */
	public void setCondicionesEspecialesList(
			List<CondicionEspecialDTO> condicionesEspecialesList) {
		this.condicionesEspecialesList = condicionesEspecialesList;
	}

	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}

	public void setControles(List<ControlDinamicoRiesgoDTO> controles) {
		this.controles = controles;
	}

	public Long getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}

	/**
	 * @return the incisoVigenteAutorizado
	 */
	public int getIncisoVigenteAutorizado() {
		return incisoVigenteAutorizado;
	}

	/**
	 * @param incisosAsociadosReporte
	 *            the incisosAsociadosReporte to set
	 */
	public void setIncisosAsociadosReporte(int incisosAsociadosReporte) {
		this.incisosAsociadosReporte = incisosAsociadosReporte;
	}

	/**
	 * @return the incisosAsociadosReporte
	 */
	public int getIncisosAsociadosReporte() {
		return incisosAsociadosReporte;
	}

	/**
	 * @param incisoVigenteAutorizado
	 *            the incisoVigenteAutorizado to set
	 */
	public void setIncisoVigenteAutorizado(int incisoVigenteAutorizado) {
		this.incisoVigenteAutorizado = incisoVigenteAutorizado;
	}

	/**
	 * @param filtroCliente
	 *            the filtroCliente to set
	 */
	public void setFiltroCliente(ClienteGenericoDTO filtroCliente) {
		this.filtroCliente = filtroCliente;
	}

	/**
	 * @return the filtroCliente
	 */
	public ClienteGenericoDTO getFiltroCliente() {
		return filtroCliente;
	}

	/**
	 * @return the idCliente
	 */
	public BigDecimal getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente
	 *            the idCliente to set
	 */
	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public List<IncisoSiniestroDTO> getIncisos() {
		return incisos;
	}

	public void setIncisos(List<IncisoSiniestroDTO> incisos) {
		this.incisos = incisos;
	}

	public Map<Short, String> getListTipoServicio() {
		return listTipoServicio;
	}

	public void setListTipoServicio(Map<Short, String> listTipoServicio) {
		this.listTipoServicio = listTipoServicio;
	}

	public ConfiguracionDatoIncisoBitemporalService getConfiguracionDatoIncisoService() {
		return configuracionDatoIncisoService;
	}

	public void setConfiguracionDatoIncisoService(
			ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}

	public Date getFechaReporteSiniestro() {
		return fechaReporteSiniestro;
	}

	public void setFechaReporteSiniestro(Date fechaReporteSiniestro) {
		this.fechaReporteSiniestro = fechaReporteSiniestro;
	}

	

	public Boolean getBusquedaRealizada() {
		return busquedaRealizada;
	}

	public void setBusquedaRealizada(Boolean busquedaRealizada) {
		this.busquedaRealizada = busquedaRealizada;
	}

	public Boolean getIncisosEmpty() {
		Boolean result = Boolean.TRUE;

		if (this.incisos != null && this.incisos.size() > 0) {
			result = Boolean.FALSE;
		}

		return result;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the tiposPersona
	 */
	public Map<String, String> getTiposPersona() {
		return tiposPersona;
	}

	/**
	 * @param tiposPersona
	 *            the tiposPersona to set
	 */
	public void setTiposPersona(Map<String, String> tiposPersona) {
		this.tiposPersona = tiposPersona;
	}

	/**
	 * 
	 * @return the listDireccionesCliente
	 */
	public List<DireccionCliente> getListDireccionesCliente() {
		return listDireccionesCliente;
	}

	/**
	 * @param listDireccionesCliente
	 *            the listDireccionesCliente to set
	 */
	public void setListDireccionesCliente(
			List<DireccionCliente> listDireccionesCliente) {
		this.listDireccionesCliente = listDireccionesCliente;
	}

	/**
	 * @return the direccionCliente
	 */
	public DireccionCliente getDireccionCliente() {
		return direccionCliente;
	}

	/**
	 * @param direccionCliente
	 *            the direccionCliente to set
	 */
	public void setDireccionCliente(DireccionCliente direccionCliente) {
		this.direccionCliente = direccionCliente;
	}

	public String getEstatusPoliza() {
		return estatusPoliza;
	}

	public void setEstatusPoliza(String estatusPoliza) {
		this.estatusPoliza = estatusPoliza;
	}

	public String getAutorizadorSelected() {
		return autorizadorSelected;
	}

	public void setAutorizadorSelected(String autorizadorSelected) {
		this.autorizadorSelected = autorizadorSelected;
	}

	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}

	public Integer getEstatusSolicitud() {
		return estatusSolicitud;
	}

	public void setEstatusSolicitud(Integer estatusSolicitud) {
		this.estatusSolicitud = estatusSolicitud;
	}

	/**
	 * @param prorroga
	 *            the prorroga to set
	 */
	public void setProrroga(ToProrroga prorroga) {
		this.prorroga = prorroga;
	}

	/**
	 * @return the prorroga
	 */
	public ToProrroga getProrroga() {
		return prorroga;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}



	public Long getValidOnMillis() {
		return validOnMillis;
	}



	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}



	public Long getRecordFromMillis() {
		return recordFromMillis;
	}



	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}



	public String getHoraOcurrido() {
		return horaOcurrido;
	}



	public void setHoraOcurrido(String horaOcurrido) {
		this.horaOcurrido = horaOcurrido;
	}



	public Long getFechaReporteSiniestroMillis() {
		return fechaReporteSiniestroMillis;
	}



	public void setFechaReporteSiniestroMillis(Long fechaReporteSiniestroMillis) {
		this.fechaReporteSiniestroMillis = fechaReporteSiniestroMillis;
	} 



	public Integer getTipoValidacion() {
		return tipoValidacion;
	}



	public void setTipoValidacion(Integer tipoValidacion) {
		this.tipoValidacion = tipoValidacion;
	}



	public Long getValidOnMillisInciso() {
		return validOnMillisInciso;
	}



	public void setValidOnMillisInciso(Long validOnMillisInciso) {
		this.validOnMillisInciso = validOnMillisInciso;
	}



	public Long getRecordFromMillisInciso() {
		return recordFromMillisInciso;
	}



	public void setRecordFromMillisInciso(Long recordFromMillisInciso) {
		this.recordFromMillisInciso = recordFromMillisInciso;
	}



	public Date getValidOnInciso() {
		return validOnInciso;
	}



	public void setValidOnInciso(Date validOnInciso) {
		this.validOnInciso = validOnInciso;
	}



	public Date getRecordFromInciso() {
		return recordFromInciso;
	}



	public void setRecordFromInciso(Date recordFromInciso) {
		this.recordFromInciso = recordFromInciso;
	}



	public Long getIdToSolicitud() {
		return idToSolicitud;
	}



	public void setIdToSolicitud(Long idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public String getEstatusVigenciaInciso() {
		return estatusVigenciaInciso;
	}

	public void setEstatusVigenciaInciso(String estatusVigenciaInciso) {
		this.estatusVigenciaInciso = estatusVigenciaInciso;
	}

	public Short getCodigoEstatusPoliza() {
		return codigoEstatusPoliza;
	}

	public void setCodigoEstatusPoliza(Short codigoEstatusPoliza) {
		this.codigoEstatusPoliza = codigoEstatusPoliza;
	}
	public boolean isBusquedaPolizas() {
		return busquedaPolizas;
	}

	public void setBusquedaPolizas(boolean busquedaPolizas) {
		this.busquedaPolizas = busquedaPolizas;
	}

	public String getCondicionEspecialId() {
		return condicionEspecialId;
	}

	public void setCondicionEspecialId(String condicionEspecialId) {
		this.condicionEspecialId = condicionEspecialId;
	}

	public String getDetalleCondicion() {
		return detalleCondicion;
	}

	public void setDetalleCondicion(String detalleCondicion) {
		this.detalleCondicion = detalleCondicion;
	}

	public String getSumaStr() {
		return sumaStr;
	}

	public void setSumaStr(String sumaStr) {
		this.sumaStr = sumaStr;
	}
}
	