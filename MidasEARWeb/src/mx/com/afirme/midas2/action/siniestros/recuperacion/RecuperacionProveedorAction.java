package mx.com.afirme.midas2.action.siniestros.recuperacion;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.FolioVentaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.DatosRecuperacionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:43 p.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/recuperacionProveedor")
public class RecuperacionProveedorAction extends RecuperacionAction{
	private final String 	CONTENEDOR_RECUPERACION_PRV	 =	"/jsp/siniestros/recuperacion/contenedorRecuperacionProveedor.jsp";
	private final String 	DEVOLUCION_RECUPERACION_PRV	 =	"/jsp/siniestros/recuperacion/devolucionRecuperacionProveedor.jsp";
	private final String 	DEVOLUCION_VENTA_PRV	 =	"/jsp/siniestros/recuperacion/contenedorVentaProveedor.jsp";
	private final String 	REFERENCIA_BANCARIA_DEVOLUCION_GRID	 =	"/jsp/siniestros/recuperacion/referenciaBancariaDevolucionGrid.jsp";
	private final String 	REFERENCIA_BANCARIA_VENTA_GRID	 =	"/jsp/siniestros/recuperacion/referenciaBancariaVentaGrid.jsp";	
	private final String	VENTANA_CANCELAR_RECUPERACION_PRV = "/jsp/siniestros/recuperacion/ventanaCancelacionProveedor.jsp";
	private final String	CONTENEDOR_CANCELAR_RECUPERACION_PRV = "/jsp/siniestros/recuperacion/contenedorCancelacionProveedor.jsp";
	//Coordinador Cobro Compañías y Salvamento  
	//Gerente Operaciones Indemnización Autos
	private final String ROL_COBRO_CIASALVAMENTO= "Rol_M2_Coordinador_Cobro_Companias_Salvamentos";
	private final String ROL_GERENTEINDM = "Rol_M2_Gerente_Operaciones_Indemnizacion";
	final String PANTALLA_DEVOLUCION = "DEVOLUCION"; 
	final String PANTALLA_VENTA = "VENTA"; 
	final String PANTALLA_CANCELA= "CANCELACION";
	private static final long serialVersionUID = -7901430645823762699L;	
	@Autowired
	@Qualifier("recuperacionProveedorServiceEJB")
	private RecuperacionProveedorService recuperacionProveedorService;
	private Map<String,String>  estados;
	private Map<String,String>  estatusVenta;
	private List<FolioVentaRecuperacion> lstFoliosVenta;
	private Map<String,String> lstMotivo =new LinkedHashMap<String, String>();;
	private Map<String,String>  municipios;
	private RecuperacionProveedor recuperacion = new RecuperacionProveedor() ;
	private DatosRecuperacionDTO datosRecuperacion;
	private boolean tienePermisoCapturaDatosVenta;
	private List<ReferenciaBancariaDTO> lstReferenciasDevolucion;
	private List<ReferenciaBancariaDTO> lstReferenciasVenta;	/**
	 * <b>SE AGREGA EN DESARROLLO</b>
	 */
	@Action(value="mostrarVentanaCancelar",results={
			@Result(name=SUCCESS,location=VENTANA_CANCELAR_RECUPERACION_PRV),
			@Result(name=INPUT,location=VENTANA_CANCELAR_RECUPERACION_PRV)
			})
	public String mostrarVentanaCancelar(){
		return SUCCESS;
	}
	
	/**
	 * <b>SE AGREGA EN DESARROLLO</b>
	 */
	@Action(value="mostrarPestanaCancelar",results={
			@Result(name=SUCCESS,location=CONTENEDOR_CANCELAR_RECUPERACION_PRV),
			@Result(name=INPUT,location=CONTENEDOR_CANCELAR_RECUPERACION_PRV)
			})
	public String mostrarPestanaCancelar(){
		return SUCCESS;
	}
	
	/**
	 * <b>SE AGREGA EN DISENO CANCELAR RECUPERACION PROVEEDOR</b>
	 */
	@Action(value="cancelar",results={
			@Result(name=SUCCESS,location=VENTANA_CANCELAR_RECUPERACION_PRV),
			@Result(name=INPUT,location=VENTANA_CANCELAR_RECUPERACION_PRV)
			})
	public String cancelar(){
		try{
			recuperacionProveedorService.cancelarRecuperacionProveedor(recuperacion.getId(), recuperacion.getMotivoCancelacion());
			recuperacion = entidadService.findById(RecuperacionProveedor.class, recuperacion.getId());
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)ex).getMessageClean());
			}
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	@Action(value="buscarCuentasDevolucion",results={
			@Result(name=SUCCESS,location=REFERENCIA_BANCARIA_DEVOLUCION_GRID),
			@Result(name=INPUT,location=REFERENCIA_BANCARIA_DEVOLUCION_GRID)
			})
	public String buscarCuentasDevolucion(){	
		
		//this.lstReferenciasDevolucion= new ArrayList<ReferenciaBancariaDTO>();
		if(null==this.recuperacionId){
			this.lstReferenciasDevolucion=this.recuperacionProveedorService.obtenerCuentasBancarias();
		}else{
			this.lstReferenciasDevolucion=this.recuperacionProveedorService.obtenerReferenciasBancaria(this.recuperacionId, Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
			if(null==lstReferenciasDevolucion || lstReferenciasDevolucion.isEmpty()){
				this.lstReferenciasDevolucion=this.recuperacionProveedorService.obtenerCuentasBancarias();
			}
		}
			return SUCCESS;	
		
	}
	@Action(value="buscarCuentasVenta",results={
			@Result(name=SUCCESS,location=REFERENCIA_BANCARIA_VENTA_GRID),
			@Result(name=INPUT,location=REFERENCIA_BANCARIA_VENTA_GRID)
			})
	public String buscarCuentasVenta(){	
		
		this.lstReferenciasVenta= new ArrayList<ReferenciaBancariaDTO>();
		if(null==this.recuperacionId){
			this.lstReferenciasVenta=this.recuperacionProveedorService.obtenerCuentasBancarias();
		}else{
			this.lstReferenciasVenta=this.recuperacionProveedorService.obtenerReferenciasBancaria(this.recuperacionId, Recuperacion.MedioRecuperacion.VENTA.toString());
			if(null==lstReferenciasVenta || lstReferenciasVenta.isEmpty() ){
				this.lstReferenciasVenta=this.recuperacionProveedorService.obtenerCuentasBancarias();
			}
		}
			return SUCCESS;	
		
	}
	
	/**
	 * Metodo que guarda la informacion de la creacion de una Recuperacion de
	 * Proveedor.
	 */
				
	@Action(value = "guardar", results = { 
			@Result(name = INPUT, location = CONTENEDOR_RECUPERACION_PRV) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/recuperaciones",
					"mensaje","${mensaje}"}) })					
	public String guardar(){
		
		
		try {
			this.recuperacionProveedorService.guardarRecuperacionProveedor(recuperacion, this.coberturasSeleccionadas, this.pasesSeleccionadas, null);
				
			if(null!=recuperacion && null!=  recuperacion.getId()){
				this.recuperacionId=recuperacion.getId();
				this.soloConsulta=Boolean.TRUE;
				this.esNuevoRegistro=false;
				this.recuperacion=this.recuperacionProveedorService.obtenerRecuperacionProveedor(recuperacion.getId());
			}
			setMensaje("Recuperacion No."+recuperacion.getNumero()+" Guardada Exitosamente");
			 
		} catch (Exception e) {
			setMensajeError(e.getMessage() );
		}
		prepareMostrarContenedor();
		return SUCCESS;
		
	}

	/**
	 * Metodo que guarda la informacion de la Venta-
	 */

	@Action(value = "guardarVenta", results = { 
			@Result(name = INPUT, location = CONTENEDOR_RECUPERACION_PRV) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/recuperaciones",
					"mensaje","${mensaje}"}) })	
	public String guardarVenta(){
		String mensaje =this.recuperacionProveedorService.guardarVentaRecuperacionProveedor(this.recuperacionId, this.recuperacion, null);
		super.setMensajeExitoPersonalizado(mensaje);
		
		return SUCCESS;
	}

	
	/**
	 * Mostrar el jsp contenedor de Recuperacion a Proveedores
	 */	
	@Action(value = "mostrarContenedor", results = { 
			@Result(name = SUCCESS, location = CONTENEDOR_RECUPERACION_PRV) ,			
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/siniestros/recuperacion/listado/recuperaciones",
					"mensaje","${mensaje}"}) })			
	public String mostrarContenedor(){
		prepareMostrarContenedor();
		prepareDatosRecuperacionGenerico();
		return SUCCESS;
	}
	 
	
	@Action(value="mostrarContenedorDevolucionPRV",results={
			@Result(name=SUCCESS,location=DEVOLUCION_RECUPERACION_PRV),
			@Result(name=INPUT,location=DEVOLUCION_RECUPERACION_PRV)
			})
	public String mostrarContenedorDevolucionPRV(){		
			prepareDatosRecuperacionPrestaador();			
		return SUCCESS; 
	}
	
	/**
	 * Mostrar la seccion del tab de Venta
	 * */

	@Action(value="mostrarContenedorVentaPRV",results={
			@Result(name=SUCCESS,location=DEVOLUCION_VENTA_PRV),
			@Result(name=INPUT,location=DEVOLUCION_VENTA_PRV)
			})
	public String mostrarContenedorVentaPRV(){
		
		if(this.soloConsulta || StringUtil.isEmpty(this.pantallaSeleccion) ||  !pantallaSeleccion.equalsIgnoreCase(PANTALLA_VENTA) ){
			this.recuperacion=this.recuperacionProveedorService.obtenerRecuperacionProveedor(this.recuperacionId);
		}
		if( StringUtil.isEmpty(this.pantallaSeleccion)){
			this.pantallaSeleccion=	PANTALLA_VENTA;
		}
		prepareMostrarContenidoVenta();
		return SUCCESS; 
	}
	/**
	 * Invocar al metodo llenarListados
	 */
	
	public void prepareMostrarContenedor(){		
		this.lstCoberturas  =new LinkedHashMap<String, String>();
		if(null!=this.recuperacionId){
			RecuperacionProveedor recupera=this.recuperacionProveedorService.obtenerRecuperacionProveedor(recuperacionId);
			this.lstTipoOC= listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.TIPO_ORDENCOMPRA, recupera.getTipoOrdenCompra());			
			//Inicia medio Recuperacion
			this.esNuevoRegistro=false;
			this.lstMedioRecuperacion.clear();
			lstMedioRecuperacion=listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION,recupera.getMedio() );
		}else{			
			this.lstTipoOC=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDENCOMPRA);
			lstMedioRecuperacion=listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION,Recuperacion.MedioRecuperacion.REEMBOLSO.toString() );
		}
	}	
	public void prepareDatosRecuperacionPrestaador(){		
		//Obtener datos Orden Compra. y Reporte		
		if (null!=recuperacionId){
			if(null==datosRecuperacion){
				this.datosRecuperacion=this.recuperacionProveedorService.obtenerDatosRecuperacionProveedor(recuperacionId);
			}
			if(null==recuperacion  || null==recuperacion.getMotivoDevolucion()){
				this.recuperacion=this.recuperacionProveedorService.obtenerRecuperacionProveedor(recuperacionId);
			}			
			this.lstMotivo=listadoService.obtenerCatalogoPorCodigo(TIPO_CATALOGO.MOTIVO_DEVOLUCION, recuperacion.getMotivoDevolucion());
		}else{
			if(null==datosRecuperacion){
				this.datosRecuperacion=new DatosRecuperacionDTO();
			}
			this.lstMotivo.put("","Seleccione..");
			this.lstMotivo.putAll( listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_DEVOLUCION));
		}
	}
	
	public void prepareDatosRecuperacionGenerico(){
		//Obtener datos Orden Compra. y Reporte
		if (null!=recuperacionId){
			this.recuperacion = this.recuperacionProveedorService.obtenerRecuperacionProveedor(this.recuperacionId);
			this.noReporteSiniestro=recuperacion.getReporteCabina().getNumeroReporte();
			this.noSiniestro=recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();
			this.lstCoberturas=this.recuperacionProveedorService.obtenerCoberturasRecuperacion(recuperacionId);
			this.lstPases=this.recuperacionProveedorService.obtenerPasesRecuperacion(recuperacionId);
			this.estatusRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, recuperacion.getEstatus());
			if (null!=recuperacion.getTipo() ){
				tipoRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION,recuperacion.getTipo() );
			}else{
				tipoRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION, Recuperacion.TipoRecuperacion.PROVEEDOR.name());

			}	
		}else{
			this.recuperacion = new RecuperacionProveedor();
			this.recuperacion.setEstatus(Recuperacion.EstatusRecuperacion.PENDIENTE.toString());
			this.recuperacion.setMedio(Recuperacion.MedioRecuperacion.REEMBOLSO.toString());
			this.recuperacion.setOrigen(Recuperacion.OrigenRecuperacion.MANUAL.toString());
			this.recuperacion.setFechaCreacion(new Date());
			this.recuperacion.setTipo(Recuperacion.TipoRecuperacion.PROVEEDOR.toString());
			this.recuperacion.setActivo(Boolean.TRUE); 
			this.recuperacion.setEsRefaccion(Boolean.FALSE);
			this.estatusRecuperacionDesc=catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, Recuperacion.EstatusRecuperacion.REGISTRADO.name());
			this.tipoRecuperacionDesc=this.catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION, Recuperacion.TipoRecuperacion.PROVEEDOR.name());
		}
	}

	public void prepareMostrarContenidoVenta(){
		this.estatusVenta= new LinkedHashMap<String, String>();
		this.estados  = this.listadoService.getMapEstadosMX();  
		this.municipios =new LinkedHashMap<String, String>();
		 if (    (usuarioService.tieneRolUsuarioActual(ROL_COBRO_CIASALVAMENTO )   || usuarioService.tieneRolUsuarioActual(ROL_GERENTEINDM )  )  
				 	 ){
				this.tienePermisoCapturaDatosVenta = true;
			}
		 if(this.soloConsulta){
			 this.lstFoliosVenta=this.recuperacionProveedorService.obtenerFoliosVenta(recuperacionId);	 
			 this.estados.clear();
			 this.municipios.clear();
			 if(null!=recuperacion && null != recuperacion.getEstadoVenta() && null != recuperacion.getEstadoVenta().getId()){
				 this.estados.put(this.recuperacion.getEstadoVenta().getId(), this.recuperacion.getEstadoVenta().getDescripcion());
			 }
			 
			 if(null!=recuperacion && null != recuperacion.getCiudadVenta() && null != recuperacion.getCiudadVenta().getId()){
				 this.municipios.put(this.recuperacion.getCiudadVenta().getId(), this.recuperacion.getCiudadVenta().getDescripcion());
			 }
			 CatValorFijo cat=this.catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_VENTA_RECUPERACION, recuperacion.getEstatusPreventa());
			 if(null!=cat){
				 this.estatusVenta.clear();
				 this.estatusVenta.put(cat.getCodigo(), cat.getDescripcion());
			 }
		 }else{
			 this.lstFoliosVenta=this.recuperacionProveedorService.generarFolioVenta(this.recuperacionId);		
			 if(null!=recuperacion && null != recuperacion.getEstadoVenta() && null != recuperacion.getEstadoVenta().getId()){
				 this.municipios=this.listadoService.getMapMunicipiosPorEstado(recuperacion.getEstadoVenta().getId());
			 }
				this.estatusVenta=listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_VENTA_RECUPERACION);
		 }
	}
	
	public void prepareMostrarPestanaCancelacion(){
		recuperacion = entidadService.findById(RecuperacionProveedor.class, recuperacion.getId());
	}

	public RecuperacionProveedorService getRecuperacionProveedorService() {
		return recuperacionProveedorService;
	}

	public void setRecuperacionProveedorService(
			RecuperacionProveedorService recuperacionProveedorService) {
		this.recuperacionProveedorService = recuperacionProveedorService;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public Map<String, String> getEstatusVenta() {
		return estatusVenta;
	}

	public void setEstatusVenta(Map<String, String> estatusVenta) {
		this.estatusVenta = estatusVenta;
	}

	public List<FolioVentaRecuperacion> getLstFoliosVenta() {
		return lstFoliosVenta;
	}

	public void setLstFoliosVenta(List<FolioVentaRecuperacion> lstFoliosVenta) {
		this.lstFoliosVenta = lstFoliosVenta;
	}

	public Map<String, String> getLstMotivo() {
		return lstMotivo;
	}

	public void setLstMotivo(Map<String, String> lstMotivo) {
		this.lstMotivo = lstMotivo;
	}

	public Map<String, String> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(Map<String, String> municipios) {
		this.municipios = municipios;
	}

	public RecuperacionProveedor getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionProveedor recuperacion) {
		this.recuperacion = recuperacion;
	}

	public DatosRecuperacionDTO getDatosRecuperacion() {
		return datosRecuperacion;
	}

	public void setDatosRecuperacion(DatosRecuperacionDTO datosRecuperacion) {
		this.datosRecuperacion = datosRecuperacion;
	}

	public boolean isTienePermisoCapturaDatosVenta() {
		return tienePermisoCapturaDatosVenta;
	}

	public void setTienePermisoCapturaDatosVenta(
			boolean tienePermisoCapturaDatosVenta) {
		this.tienePermisoCapturaDatosVenta = tienePermisoCapturaDatosVenta;
	}

	public List<ReferenciaBancariaDTO> getLstReferenciasDevolucion() {
		return lstReferenciasDevolucion;
	}

	public void setLstReferenciasDevolucion(
			List<ReferenciaBancariaDTO> lstReferenciasDevolucion) {
		this.lstReferenciasDevolucion = lstReferenciasDevolucion;
	}

	public List<ReferenciaBancariaDTO> getLstReferenciasVenta() {
		return lstReferenciasVenta;
	}

	public void setLstReferenciasVenta(
			List<ReferenciaBancariaDTO> lstReferenciasVenta) {
		this.lstReferenciasVenta = lstReferenciasVenta;
	}
	
	
	
}