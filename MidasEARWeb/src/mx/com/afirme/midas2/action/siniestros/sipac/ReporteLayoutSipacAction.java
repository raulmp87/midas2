package mx.com.afirme.midas2.action.siniestros.sipac;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.sipac.LayoutSipac;
import mx.com.afirme.midas2.domain.siniestros.sipac.LoteLayoutSipac;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;

import javax.ejb.EJB;

import mx.com.afirme.midas2.service.siniestros.sipac.ReporteLayoutSipacService;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.Preparable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/siniestros/sipac")
public class ReporteLayoutSipacAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 804097850963734358L;
	
	private static final Logger LOG = LoggerFactory.getLogger(ReporteLayoutSipacAction.class);

	private ReporteLayoutSipacService layoutService; 
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	private String fileName;
	private LoteLayoutSipac lote;
	private LayoutSipac layoutSipac;
	private Long idLote;
	private Date fechaInicial;
	private Date fechaFinal;
	private InputStream plantillaInputSipac;
	private InputStream plantillaDescargaLote;
	private String contentType;
	private static final String NAMESPACE = "/siniestros/sipac";
	private static final String NOMBRE_REPORTE_LAYOUT_SIPAC = "Reporte_Sipac_";
	private List<LoteLayoutSipac> listLoteLayoutSipac = new ArrayList<LoteLayoutSipac>();
	private List<LoteLayoutSipac> listLotesPendientes = new ArrayList<LoteLayoutSipac>();
	private List<LayoutSipac> listLayoutSipac = new ArrayList<LayoutSipac>();
	
	@Action(value="mostrarLayoutSipac",results={
			@Result(name= SUCCESS, location="/jsp/siniestros/sipac/reporteLayoutSipac.jsp")
	})
	public String mostrarLayoutSipac(){
		return SUCCESS;
	}

	@Action(value="aceptarLoteSipac", results={
			@Result(name= SUCCESS, location="/jsp/siniestros/sipac/loteSipacGrid.jsp")
	})
	public String aceptarLoteSipac(){
		try{
			lote = layoutService.updateLote(idLote, "2");
		}catch(Exception e){
			LOG.error("Error al aceptar Lote " + e);
		}
		return SUCCESS;
	}
	
	@Action(value="cancelarLoteSipac", results={
			@Result(name= SUCCESS, location="/jsp/siniestros/sipac/loteSipacGrid.jsp")
	})
	public String cancelarLoteSipac(){
		try{
			lote = layoutService.updateLote(idLote, "1");
		}catch(Exception e){
			LOG.error("Error al cancelar Lote " + e);
		}
		return SUCCESS;
	}
	
	@Action(value="subirLoteSipac", results={
			@Result(name= SUCCESS, location="/jsp/siniestros/sipac/consultarLote.jsp")
	})
	public String subirLoteSipac(){
		return SUCCESS;
	}
	
	@Action(value="descargarLoteSipac", results={
			@Result(name= SUCCESS,type = ACTION_PARAM_STREAM, params = { 
					ACTION_PARAM_CONTENT_TYPE,"${contentType}",
					"namespace", NAMESPACE,
					ACTION_PARAM_INPUT_NAME,"plantillaDescargaLote",
					"mensaje","${mensaje}",
					"contentDisposition","attachment;filename=\"${fileName}\"" }),
			@Result(name = INPUT, location="/jsp/siniestros/sipac/messageSipac.jsp", params = {
					"mensaje", "${mensaje}"
			})
	})
	public String descargarLoteSipac(){
		try{
			InputStream layoutSipacLote = layoutService.generarTxtByLote(idLote);
			plantillaDescargaLote = layoutSipacLote;
			contentType = "application/txt";
			fileName = NOMBRE_REPORTE_LAYOUT_SIPAC + "Lote: " + idLote +".txt";
		}catch(Exception e){
			setMensaje("Error: Ocurrio un error al descargar el Lote");
			LOG.error("Error al descargar Lote " + e);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "generarLayoutSipac", results = {
			@Result(name = SUCCESS, type = ACTION_PARAM_STREAM, params = { 
				ACTION_PARAM_CONTENT_TYPE,"${contentType}",
				"namespace", NAMESPACE,
				ACTION_PARAM_INPUT_NAME,"plantillaInputSipac",
				"mensaje", "${mensaje}", 
				"contentDisposition","attachment;filename=\"${fileName}\""
			}), 
			@Result(name = INPUT, location="/jsp/siniestros/sipac/messageSipac.jsp", params = {
					"mensaje", "${mensaje}"
			})
	})
	public String generarLayoutSipac(){
			try{
				listLotesPendientes = layoutService.obtenerLotesPendientes();
			
				if(listLotesPendientes.isEmpty()){
					
					listLayoutSipac = layoutService.buscarReportesPend(fechaInicial, fechaFinal);
					
					if(!listLayoutSipac.isEmpty()){
						
						lote = layoutService.saveLotes(fechaInicial, fechaFinal);
						
						plantillaInputSipac = layoutService.generarArchivoTxt(listLayoutSipac, lote);
						contentType = "application/txt";
						fileName = NOMBRE_REPORTE_LAYOUT_SIPAC +UtileriasWeb.getFechaStr(fechaInicial)+"_a_"+UtileriasWeb.getFechaStr(fechaFinal)+".txt";
						setMensajeExito();
					}else{
						setMensaje("Error: No existen reportes Pendientes entre " + fechaInicial + " y " + fechaFinal + "; No se creara un nuevo Lote.");
						return INPUT;
					}
				}else{
					setMensaje("Error: Existen Lotes Pendientes; Favor de Cancelar o Completar Lote Pendiente");
					return INPUT;
				}
			}catch(Exception e){
				LOG.error("No se pudo Generar el Archivo", e);
			}
			return SUCCESS;
	}
	
	@Action(value="filtrarLotes", results={
		@Result(name= SUCCESS, location="/jsp/siniestros/sipac/loteSipacGrid.jsp"),
		@Result(name= INPUT, location="/jsp/siniestros/sipac/loteSipacGrid.jsp")
	})
	public String filtrarLotes() {
		
		try {
			listLoteLayoutSipac = layoutService.obtenerLotesSipac();
			for(LoteLayoutSipac element: listLoteLayoutSipac){
				if(element.getEstatus() != null){
					element.setEstatus(catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_LOTESIPAC, String.valueOf(element.getEstatus())).getDescripcion());
				}
			}
		} catch (Exception e) {
			e.getMessage();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "listarLote", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/sipac/informacionLoteGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/sipac/informacionLoteGrid.jsp") })
	public String listar() {
		try {			
			listLayoutSipac = layoutService.findLayoutByIdLote(idLote);
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, type = ACTION_PARAM_STREAM, params = { 
				ACTION_PARAM_CONTENT_TYPE,"${contentType}",
				"namespace", NAMESPACE,
				ACTION_PARAM_INPUT_NAME,"plantillaInputSipac",
				"mensaje", "${mensaje}", 
				"contentDisposition","attachment;filename=\"${fileName}\""
			}), 
			@Result(name = INPUT, location="/jsp/siniestros/sipac/messageSipac.jsp", params = {
					"mensaje", "${mensaje}"
			})
	})
	public String guardar() {			
		try {
			layoutService.save(idLote,layoutSipac);			
			contentType = "application/xml";
			String grId = ServletActionContext.getRequest().getParameter("gr_id");
			plantillaInputSipac = new ByteArrayInputStream(
					("<data><action type=\"" + "update" + "\" sid=\"" + grId + 
							"\" tid=\"" + grId + "\" /></data>").getBytes("UTF-8"));
			setMensajeExito();
			return SUCCESS;
		} catch (UnsupportedEncodingException e) {
			setMensaje("Error: No fue posible actualizar el registro");
			return INPUT;
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	public void prepare() throws Exception {
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	@Autowired
	@Qualifier("reporteLayoutSipacServiceEJB")
	public void setLayoutService(ReporteLayoutSipacService layoutService) {
		this.layoutService = layoutService;
	}
	
	public ReporteLayoutSipacService getLayoutService() {
		return layoutService;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public InputStream getPlantillaInputSipac() {
		return plantillaInputSipac;
	}

	public void setPlantillaInputSipac(InputStream plantillaInputSipac) {
		this.plantillaInputSipac = plantillaInputSipac;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public LoteLayoutSipac getLote() {
		return lote;
	}

	public void setLote(LoteLayoutSipac lote) {
		this.lote = lote;
	}

	public List<LoteLayoutSipac> getListLoteLayoutSipac() {
		return listLoteLayoutSipac;
	}

	public void setListLoteLayoutSipac(List<LoteLayoutSipac> listLoteLayoutSipac) {
		this.listLoteLayoutSipac = listLoteLayoutSipac;
	}

	public List<LoteLayoutSipac> getListLotesPendientes() {
		return listLotesPendientes;
	}

	public void setListLotesPendientes(List<LoteLayoutSipac> listLotesPendientes) {
		this.listLotesPendientes = listLotesPendientes;
	}
	
	public Long getIdLote() {
		return idLote;
	}

	public void setIdLote(Long idLote) {
		this.idLote = idLote;
	}

	public List<LayoutSipac> getListLayoutSipac() {
		return listLayoutSipac;
	}

	public void setListLayoutSipac(List<LayoutSipac> listLayoutSipac) {
		this.listLayoutSipac = listLayoutSipac;
	}

	public InputStream getPlantillaDescargaLote() {
		return plantillaDescargaLote;
	}

	public void setPlantillaDescargaLote(InputStream plantillaDescargaLote) {
		this.plantillaDescargaLote = plantillaDescargaLote;
	}

	public LayoutSipac getLayoutSipac() {
		return layoutSipac;
	}

	public void setLayoutSipac(LayoutSipac layoutSipac) {
		this.layoutSipac = layoutSipac;
	}
}
