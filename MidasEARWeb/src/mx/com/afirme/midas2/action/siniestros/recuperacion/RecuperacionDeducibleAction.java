package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.LinkedHashMap;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/recuperacionDeducible")
public class RecuperacionDeducibleAction extends RecuperacionAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Logger LOG = Logger.getLogger(RecuperacionDeducibleAction.class);
	
	private Long idRecuperacion;
	
	private RecuperacionDeducible recuperacion;
	
	private TransporteImpresionDTO transporte;
	private static final String	EXT_PDF	= ".pdf";
	private static final String	TIPO_PDF	= "application/pdf";
	
	@Autowired
	@Qualifier("recuperacionDeducibleServiceEJB")
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@Autowired
	@Qualifier("recuperacionServiceEJB")
	private RecuperacionService recuperacionService;
	
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/deducible/contenedorRecuperacionDeducible.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/recuperacion/deducible/contenedorRecuperacionDeducible.jsp")
			})
	public String mostrarContenedor()
	{	
		if(this.recuperacion.getId() != null){
			  
			recuperacion = entidadService.findById(RecuperacionDeducible.class, recuperacion.getId());
			CatValorFijo cat = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDENCOMPRA, recuperacion.getTipoOrdenCompra());
			lstTipoOC = new LinkedHashMap<String, String>();
			lstTipoOC.put(cat.getCodigo(), cat.getValue());
			esNuevoRegistro=false;
			CatValorFijo catMed = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.MEDIO_RECUPERACION,recuperacion.getMedio() );
			lstMedioRecuperacion = new LinkedHashMap<String, String>();
			lstMedioRecuperacion.put(catMed.getCodigo(), catMed.getValue());			
			noReporteSiniestro=recuperacion.getReporteCabina().getNumeroReporte();
			
			if(recuperacion.getReporteCabina().getSiniestroCabina() != null)
			{
				noSiniestro = recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();				
			}
			
			lstCoberturas=recuperacionService.obtenerCoberturasRecuperacion(recuperacion.getId());
			lstPases=recuperacionService.obtenerPasesRecuperacion(recuperacion.getId());
			cat = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_RECUPERACION, recuperacion.getEstatus());
			estatusRecuperacionDesc=cat.getDescripcion();
			cat = catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_RECUPERACION,recuperacion.getTipo() );
			tipoRecuperacionDesc=cat.getDescripcion();			
		}
		
		return SUCCESS;
	}
	

	public void prepareMostrar(){

	}
	
	/**
	 * Mostrará el detalle de la Recuperación.
	 * 
	 * Invocará el servicio RecuperacionDeducibleService.obtener(Long recuperacionId).
	 * 
	 * 
	 * Este action se llamará al invocar la edición, consulta y cancelación de la
	 * recuperación.
	 * 
	 * Se deberá asegurar que el parámetro tipoMostrar se reciba, y se envíe de nuevo
	 * a la vista, ya que dependiendo del valor se mostrarán y ocultarán campos en el
	 * jsp, así como hacerlos solo lectura o editables.
	 * 
	 * Los valores posibles del atributo tipoMostrar son:
	 * R:Consultar
	 * U:Editar
	 * C:Cancelar
	 */
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/deducible/informacionRecuperacionDeducible.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/recuperacion/deducible/informacionRecuperacionDeducible.jsp")
			})
	public String mostrar(){
		
		recuperacion = recuperacionDeducibleService.obtener(recuperacion.getId());
		
		if(tipoMostrar.equalsIgnoreCase("C") && recuperacion.getCodigoUsuarioCancelacion() != null)
		{
			recuperacion.setCodigoUsuarioCancelacion(this.getUsuarioActual().getNombreUsuario());		
		}
				
		return SUCCESS;
	}

	@Action(value="obtenerCoberturas",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/deducible/informacionRecuperacionDeducible.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/recuperacion/deducible/informacionRecuperacionDeducible.jsp")
			})
	public String obtenerCoberturas(){		
		return SUCCESS;
	}

	/*@Action(value="obtenerPases",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/recuperacion/deducible/informacionRecuperacionDeducible.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/recuperacion/deducible/informacionRecuperacionDeducible.jsp")
			})
	public String obtenerPases(){
		return SUCCESS;
	}	*/
	
	public void prepareGuardar(){
		
		recuperacion = entidadService.findById(RecuperacionDeducible.class, recuperacion.getId());
	}	
	
	/**
	 * Este action invocará diferentes servicios dependiendo del atributo tipoMostrar.
	 * 
	 * 
	 * Se invocará el servicio
	 * <i>RecuperacionDeducibleService.guardar(RecuperacionDeducible
	 * recuperacionDeducible) </i>
	 * 
	 * si se selecciona el botón de guardar.
	 * 
	 * Se invocará el servicio
	 * 
	 * <i>RecuperacionDeducibleService.cancelar(Long recuperacionId) </i>
	 * 
	 * si se selecciona el botón de cancelar.
	 */
	@Action(value="guardar",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarContenedor", 
			"namespace", "/siniestros/recuperacion/listado/recuperaciones", 		
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/recuperacion/recuperacionDeducible", 		
					"mensaje", "${mensaje}",
					"recuperacion.id","${recuperacion.id}",
					"tipoMostrar","${tipoMostrar}",
					"tipoMensaje", "${tipoMensaje}"})
	})
	public String guardar(){
		
		recuperacionDeducibleService.guardar(recuperacion);
		this.setMensajeExito();
		
		return SUCCESS;
	}
	
	@Action(value="cancelar",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarContenedor", 
			"namespace", "/siniestros/recuperacion/listado/recuperaciones", 		
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/recuperacion/recuperacionDeducible", 		
					"mensaje", "${mensaje}",
					"recuperacion.id","${recuperacion.id}",
					"tipoMostrar","${tipoMostrar}",
					"tipoMensaje", "${tipoMensaje}"})
	})
	public String cancelar(){
		try{
			recuperacionDeducibleService.cancelar(recuperacion.getId(), recuperacion.getMotivoCancelacion());
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex instanceof NegocioEJBExeption){
				super.setMensajeError(((NegocioEJBExeption)ex).getMessageClean());
			}
			if(ex.getCause() instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)ex.getCause()).getMessageClean());
			}
			return INPUT;
		}
		this.setMensajeExito();
		
		return SUCCESS;
	}
	
		
	@Action(value="generarMovimientoIngreso",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarContenedor", 
			"namespace", "/siniestros/recuperacion/listado/recuperaciones", 		
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarContenedor", 
					"namespace", "/siniestros/recuperacion/recuperacionDeducible", 		
					"mensaje", "${mensaje}",
					"recuperacion.id","${recuperacion.id}",
					"tipoMostrar","${tipoMostrar}",
					"tipoMensaje", "${tipoMensaje}"})
	})
	public String generarMovimientoIngreso(){
		
		try {
			recuperacion = entidadService.findById(RecuperacionDeducible.class, recuperacion.getId());
			recuperacionService.generarIngreso(recuperacion);
			
			this.setMensajeExito();
			
		}catch(Exception e)
		{
			this.setMensajeError("Ha ocurrido un error al intentar generar el movimiento de ingreso.");			
			LOG.error("Ha ocurrido un error al intentar generar el movimiento de ingreso.", e);
		}
		  
		
		return SUCCESS;
	}

	@Action(value = "imprimirInstructivoDeposito", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirInstructivoDeposito(){
		transporte = recuperacionService.imprimirInstructivoDepositoRecuperacion(recuperacion.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "InstructivoDeposito_"+ recuperacionId + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}

	public Long getIdRecuperacion() {
		return idRecuperacion;
	}

	public void setIdRecuperacion(Long idRecuperacion) {
		this.idRecuperacion = idRecuperacion;
	}

	public RecuperacionDeducible getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionDeducible recuperacion) {
		this.recuperacion = recuperacion;
	}


	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}


	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}


}
