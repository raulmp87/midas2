package mx.com.afirme.midas2.action.siniestros.indemnizacion;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteRoboSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestroEndoso;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestroFactura;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.InfoContratoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaBajaPlacasDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.FormatoFechasPT;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value="/siniestros/indemnizacion/cartas")
public class CartaSiniestroAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -2907425778740760311L;
	private static final String	LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP	= "/jsp/siniestros/indemnizacion/contenedorEntregaDocumentos.jsp";
	private static final String	LOCATION_CONTENEDORFORMATOFECHASPT_JSP	= "/jsp/siniestros/indemnizacion/contenedorFormatoFechasPT.jsp";
	private static final String	LOCATION_CONTENEDORBAJAPLACAS_JSP	= "/jsp/siniestros/indemnizacion/contenedorBajaPlacas.jsp";

	private static final String	EXT_PDF	= ".pdf";
	private static final String	TIPO_PDF	= "application/pdf";

	//BAJA DE PLACAS
	private CartaSiniestro cartaSiniestro;
	private CartaSiniestro entidad;
	private IncisoSiniestroDTO incisoSiniestro;
	private CartaBajaPlacasDTO bajaPlacasDTO;
	private TransporteImpresionDTO transporte;
	private Boolean informacionCompleta;
	private Map<String,String> listaEstados;
	
	public static final String BAJA_PLACAS_PERDIDA_TOTAL = "BPPT";
	public static final String BAJA_PLACAS_ROBO_TOTAL = "BPRT";
	
	//NOTIFICACION DE PERDIDA TOTAL
	private Long reporteCabinaId;
	private Integer valuadorId;
	
	//DETERMINACION PERDIDA TOTAL
	private Long 	idIndemnizacion;
	private Date 	fechaDeterminacion;
	private String 	tipoSiniestroDesc;
	private String	tipoSiniestro;
	private IndemnizacionSiniestro indemnizacion;
	private DeterminacionInformacionSiniestroDTO informacionSiniestro;
	
	//CARTA FINIQUITO
	private Boolean esFiniquitoAsegurado;

	//FORMATO FECHAS PERDIDA TOTAL
	private FormatoFechasPT fechasPerdidaTotal;
	private Long idOrdenCompra;
	private Short consulta;
	private PerdidaTotalFiltro 	filtroPT;
	
	//ENTREGA DE DOCUMENTOS
	private Long siniestroId;
	private String tipoCarta;
	private String nombreEndoso;
	private List<CartaSiniestroEndoso> endosos;
	private Boolean esEliminarEndoso;
	private String stringListaEndososEliminar;
	private CartaSiniestroFactura factura;
	private List<CartaSiniestroFactura> facturas;
	private final long MODO_EJECUCION_GUARDAR = 0;
	private final long MODO_EJECUCION_IMPRIMIR = 1;
	
	private Boolean esCoberturaAsegurado;
	
	//INFORMACION CONTRATO
	private InfoContratoSiniestro infoContrato;
	private Integer esConsulta;
	
	private static final int MIDAS_APP_ID = 5;
	private static final String PARAMETRO_GLOBAL_APODERADO = "APODERADO_LEGAL_FINIQUITO";

	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("cartaSiniestroServiceEJB")
	private CartaSiniestroService cartaSiniestroService;
	
	@Autowired
	@Qualifier("perdidaTotalServiceEJB")
	private PerdidaTotalService perdidaTotalService;
	
	@Autowired
	@Qualifier("siniestroCabinaServiceEJB")
	private SiniestroCabinaService siniestroCabinaService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	private ParametroGlobalService parametroGlobalService;

	@Override
	public void prepare(){
		
	}
	
	public void prepareMostrarBajaPlacas(){
		cargarCartaSiniestro();
	}
	
	public void prepareImprimirBajaPlacas(){
		cargarEntidadCartaSiniestro();
		prepararInformacionAuto();
	}
	
	public void prepareGuardarBajaPlacas(){
		cargarEntidadCartaSiniestro();
		bajaPlacasDTO = cartaSiniestroService.obtenerDatosBajaPlacas(cartaSiniestro.getIndemnizacion().getId(), cartaSiniestro.getTipo(), cartaSiniestro);
	}
	
	private void cargarCartaSiniestro(){
		if(cartaSiniestro != null && 
				cartaSiniestro.getIndemnizacion()!= null && 
				cartaSiniestro.getIndemnizacion().getId() != null &&
				cartaSiniestro.getTipo() != null){
			Map<String,Object> params = new HashMap<String,Object>();
			params.put( "indemnizacion.id",  cartaSiniestro.getIndemnizacion().getId());
			params.put( "tipo", cartaSiniestro.getTipo());
			List<CartaSiniestro> resultados = entidadService.findByProperties(CartaSiniestro.class, params);
			if(resultados != null && !resultados.isEmpty()){
				cartaSiniestro = resultados.get(0);
				entidad = resultados.get(0);
			}
		}
	}
	
	private void cargarEntidadCartaSiniestro(){
		if(cartaSiniestro != null && 
				cartaSiniestro.getIndemnizacion()!= null && 
				cartaSiniestro.getIndemnizacion().getId() != null &&
				cartaSiniestro.getTipo() != null){
			Map<String,Object> params = new HashMap<String,Object>();
			params.put( "indemnizacion.id",  cartaSiniestro.getIndemnizacion().getId());
			params.put( "tipo", cartaSiniestro.getTipo());
			List<CartaSiniestro> resultados = entidadService.findByProperties(CartaSiniestro.class, params);
			if(resultados != null && !resultados.isEmpty()){
				entidad = resultados.get(0);
			}
			
		}
	}
	
	private void prepararInformacionAuto(){
		if(tienePoliza()){
				bajaPlacasDTO = new CartaBajaPlacasDTO();
				bajaPlacasDTO.setTipoCarta(entidad.getTipo());
				esCoberturaAsegurado = cartaSiniestroService.esCoberturaAsegurado(entidad.getIndemnizacion().getId());
				if(esCoberturaAsegurado){
					AutoIncisoReporteCabina autoIncisoReporteCabina = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(entidad.getIndemnizacion().getSiniestro().getReporteCabina().getId());
					bajaPlacasDTO.setMarcaDesc(autoIncisoReporteCabina.getDescMarca());
					bajaPlacasDTO.setDescTipoUso(autoIncisoReporteCabina.getDescripcionFinal());
					bajaPlacasDTO.setModelo(autoIncisoReporteCabina.getModeloVehiculo() != null ? autoIncisoReporteCabina.getModeloVehiculo().toString() : null);
					bajaPlacasDTO.setNumeroSerie(autoIncisoReporteCabina.getNumeroSerie());
					bajaPlacasDTO.setPlaca(autoIncisoReporteCabina.getPlaca());
				}else{
					EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,entidad.getIndemnizacion().getOrdenCompra().getIdTercero() );
					TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
					if(tercero != null){
						bajaPlacasDTO.setModelo((tercero.getModeloVehiculo() != null)? tercero.getModeloVehiculo().toString() : null);
						bajaPlacasDTO.setNumeroSerie(( !StringUtil.isEmpty(  tercero.getNumeroSerie()))? tercero.getNumeroSerie() : null);
						bajaPlacasDTO.setDescTipoUso(((tercero.getEstiloVehiculo() != null)? tercero.getEstiloVehiculo(): null));
						bajaPlacasDTO.setMarcaDesc(((tercero.getMarca() != null)? tercero.getMarca(): null));
						bajaPlacasDTO.setPlaca(((tercero.getPlacas() != null)? tercero.getPlacas(): null));
					}
				}
				bajaPlacasDTO.setFechaSiniestro(entidad.getIndemnizacion().getSiniestro().getReporteCabina().getFechaOcurrido());
				Usuario usuarioActual = usuarioService.getUsuarioActual();
				bajaPlacasDTO.setNombreUsuario(usuarioActual.getNombreCompleto());
				bajaPlacasDTO.setPuestoUsuario(perdidaTotalService.obtenerPuestoUsuarioActualIndemnizacion());
				bajaPlacasDTO.setDescripcionSol(entidad.getDescripcionSol());
				bajaPlacasDTO.setDescripcionSolComp(entidad.getDescSolComplementaria());
				bajaPlacasDTO.setEstado(listadoService.getMapEstadosPorPaisMidas(null).get(entidad.getEstadoTenencia()));
				bajaPlacasDTO.setPersonaAQuienSeDirige(entidad.getPersonaAQuienSeDirige());
				if(entidad.getTipo().compareTo(BAJA_PLACAS_ROBO_TOTAL) == 0){
					ReporteSiniestroRoboDTO filtroRobo = new ReporteSiniestroRoboDTO();
					filtroRobo.setServParticular(false); filtroRobo.setServPublico(false);
					filtroRobo.setClaveOficina(entidad.getIndemnizacion().getSiniestro().getReporteCabina().getClaveOficina());
					filtroRobo.setConsecutivoReporte(entidad.getIndemnizacion().getSiniestro().getReporteCabina().getConsecutivoReporte());
					filtroRobo.setAnioReporte(entidad.getIndemnizacion().getSiniestro().getReporteCabina().getAnioReporte());
					List<ReporteSiniestroRoboDTO> resultados = reporteCabinaService.buscarReportesRobo(filtroRobo);
					if(resultados != null && resultados.size() > 0 ){
						Long idReporteRobo = resultados.get(0).getReporteRoboId();
						ReporteRoboSiniestro reporteRobo = entidadService.findById(ReporteRoboSiniestro.class, idReporteRobo);
						bajaPlacasDTO.setAgenteMinisterio(reporteRobo.getNombreAgenteMinisterioPublico());
						bajaPlacasDTO.setNumeroAveriguacionPrevia(reporteRobo.getNumeroAveriguacionDen());
					}
				}
		}
	}
	
	@Action(value = "mostrarBajaPlacas", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORBAJAPLACAS_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORBAJAPLACAS_JSP)})
	public String mostrarBajaPlacas(){
		try{
			cargarEntidadCartaSiniestro();
			prepararInformacionAuto();
			List<String> validacionBajaPlacas = cartaSiniestroService.validarBajaPlacas(bajaPlacasDTO, entidad.getTipo());
			if(validacionBajaPlacas != null && validacionBajaPlacas.isEmpty()){
				informacionCompleta = Boolean.TRUE;
			}
		}catch(Exception ex){
			informacionCompleta = Boolean.FALSE;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarBajaPlacas", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORBAJAPLACAS_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORBAJAPLACAS_JSP)})
	public String guardarBajaPlacas(){
		try{
			List<String> validacionBajaPlacas = cartaSiniestroService.validarBajaPlacas(bajaPlacasDTO, cartaSiniestro.getTipo());
			if(validacionBajaPlacas != null && validacionBajaPlacas.isEmpty()){
				informacionCompleta = Boolean.TRUE;
			}else{
				informacionCompleta = Boolean.FALSE;
				setMensajeListaPersonalizado("No se pudo guardar. Faltan datos del Siniestro o de la baja de placas:", obtenerMensajesError(validacionBajaPlacas), "10");
				return INPUT;
			}
			if(entidad != null &&
					entidad.getId() != null ){
				entidad.setDescripcionSol(cartaSiniestro.getDescripcionSol());
				entidad.setDescSolComplementaria(cartaSiniestro.getDescSolComplementaria());
				entidad.setEstadoTenencia(cartaSiniestro.getEstadoTenencia());
				entidad.setPersonaAQuienSeDirige(cartaSiniestro.getPersonaAQuienSeDirige());
				entidad.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				entidad.setFechaModificacion(new Date());
				entidadService.save(entidad);
				cartaSiniestro = entidad;
			}else{
				cartaSiniestro.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
				cartaSiniestro.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				entidadService.save(cartaSiniestro);
			}
			try{
				cargarEntidadCartaSiniestro();
				prepararInformacionAuto();
				validacionBajaPlacas = cartaSiniestroService.validarBajaPlacas(bajaPlacasDTO, cartaSiniestro.getTipo());
				if(validacionBajaPlacas != null && validacionBajaPlacas.isEmpty()){
					informacionCompleta = Boolean.TRUE;
				}else{
					informacionCompleta = Boolean.FALSE;
				}
			}catch(Exception ex){
				informacionCompleta = Boolean.FALSE;
			}
		}catch(Exception ex){
			setMensajeError("No se pudieron guardar los cambios");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "imprimirBajaPlacas", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirBajaPlacas(){
		try{
			List<String> validacionBajaPlacas = cartaSiniestroService.validarBajaPlacas(bajaPlacasDTO, entidad.getTipo());
			if(validacionBajaPlacas != null && validacionBajaPlacas.isEmpty()){
				informacionCompleta = Boolean.TRUE;
				transporte = cartaSiniestroService.imprimirCartaBajaPlacas(bajaPlacasDTO);
				transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
				transporte.setContentType(TIPO_PDF);
				
				String fileName = entidad.getTipo() + "_" + entidad.getIndemnizacion().getSiniestro().getId() + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
				transporte.setFileName(fileName);
			}else{
				informacionCompleta = Boolean.FALSE;
				setMensajeListaPersonalizado("Faltan datos del Siniestro o de la baja de placas", obtenerMensajesError(validacionBajaPlacas), "10");
				return INPUT;
			}
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		return SUCCESS;
	}
	
	/**
	 * Obtiene la descripcion del mensaje de error del properties para mostrarlo en la pantalla
	 * @param mensajesErrorProperties
	 * @return
	 */
	private List<String> obtenerMensajesError(List<String> mensajesErrorProperties){
		List<String> mensajesError = new ArrayList<String>();
		for(String mensajeProperties: mensajesErrorProperties){
			mensajesError.add(getText(mensajeProperties));
		}
		return mensajesError;
	}
	
	@Action(value = "imprimirNotificacionPerdidaTotal", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM, 
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirNotificacionPerdidaTotal(){
		transporte = cartaSiniestroService.imprimirCartaNotificacionPerdidaTotal(idIndemnizacion, valuadorId);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "NotificacionPerdidaTotal_"+ idIndemnizacion + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value = "imprimirDeterminacionPT", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirDeterminacionPT(){
		transporte = cartaSiniestroService.imprimirDeterminacionPT(idIndemnizacion, fechaDeterminacion, tipoSiniestro, tipoSiniestroDesc);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "DeterminacionIndemnizacion_"+ idIndemnizacion + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}

	
	/**
	 * carga las fechas de Perdidas Totales 
	 */
	@Action(value = "mostrarFechasPerdidaTotal", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORFORMATOFECHASPT_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORFORMATOFECHASPT_JSP)})
	public String mostrarFechasPT(){
		try{
			fechasPerdidaTotal = cartaSiniestroService.obtenerFechasPT( this.idOrdenCompra );
			//se inserta la prima pendiente de pago hasta la fecha de la determinacionPerdidaTotal. No importa si los recibos fueron pagados posteriormente
			this.indemnizacion = entidadService.findById(IndemnizacionSiniestro.class,fechasPerdidaTotal.getIndemnizacion().getId());
			fechasPerdidaTotal.setImportePrimaPend(indemnizacion.getPrimasPendientes());
			if(null!=fechasPerdidaTotal  && null!=fechasPerdidaTotal.getIndemnizacion())
				this.esFiniquitoAsegurado = cartaSiniestroService.esCoberturaAsegurado(fechasPerdidaTotal.getIndemnizacion().getId());

		}catch(Exception ex){
			return INPUT;
		}
		return SUCCESS;
	}
	
	/**
	 * Guarda la informacion capturada en la pantalla de formato fechas PT, en la seccion de primas pendientes
	 */
	@Action(value = "guardarPrimasPendientes", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORFORMATOFECHASPT_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORFORMATOFECHASPT_JSP)})
	public String guardarPrimasPendientes(){
		try{
			cartaSiniestroService.guardarPrimasPendientes( this.fechasPerdidaTotal );
		}catch(Exception ex){
			ex.getMessage();
			setMensajeError("Error al guardar el Formato de Fechas");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "imprimirFormatoFechasPT", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirFormatoFechasPT(){
		try{
				transporte = cartaSiniestroService.imprimirFormatoFechasPT( this.idOrdenCompra );
				transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
				transporte.setContentType(TIPO_PDF);
				
				String fileName =  "Formato_Fechas_Perdida_Total" + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
				transporte.setFileName(fileName);
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarCartaFiniquito", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorCartaFiniquito.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorCartaFiniquito.jsp")})
	public String mostrarCartaFiniquito(){
		try{
			cartaSiniestro = cartaSiniestroService.obtenerCartaFiniquito(idIndemnizacion);
			esFiniquitoAsegurado = cartaSiniestroService.esCoberturaAsegurado(idIndemnizacion);
			if(esFiniquitoAsegurado == null){
				setMensajeError("No se pudo determinar si es finiquito de asegurado o de tercero");
				return INPUT;
			}
		}catch(Exception ex){
			setMensajeError("No se pudo recuperar la informacion de la carta de finiquito");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "imprimirCartaFiniquito", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirCartaFiniquito(){
		transporte = cartaSiniestroService.imprimirCartaFiniquito(idIndemnizacion);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "CartaFiniquito_"+ idIndemnizacion + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value = "mostrarEntregaDocumentos", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP)})
	public String mostrarEntregaDocumentos(){
		try{
			cartaSiniestro = cartaSiniestroService.obtenerCartaEntregaDocumentos(idIndemnizacion);
			List<String> validacionEntregaDocumentos = cartaSiniestroService.validarEntregaDocumentos(cartaSiniestro, tipoCarta, this.MODO_EJECUCION_IMPRIMIR);
			if(cartaSiniestro == null || !validacionEntregaDocumentos.isEmpty()){
				informacionCompleta = Boolean.FALSE;
			}else{
				informacionCompleta = Boolean.TRUE;
			}
		}catch(Exception ex){
			setMensajeError("No se pudo recuperar la informacion de la carta de entrega de documentos");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarEntregaDocumentos", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP)})
	public String guardarEntregaDocumentos(){
		try{
			List<String> validacionEntregaDocumentos = cartaSiniestroService.validarEntregaDocumentos(cartaSiniestro, tipoCarta, this.MODO_EJECUCION_GUARDAR);
			if(!validacionEntregaDocumentos.isEmpty()){
				setMensajeListaPersonalizado("No se pudo guardar. Faltan datos de la entrega de documentos:", obtenerMensajesError(validacionEntregaDocumentos), "10");
					if(cartaSiniestro.getId() != null){
						cartaSiniestro = cartaSiniestroService.obtenerCartaEntregaDocumentos(idIndemnizacion);
						validacionEntregaDocumentos = cartaSiniestroService.validarEntregaDocumentos(cartaSiniestro, tipoCarta, this.MODO_EJECUCION_IMPRIMIR);
						if(cartaSiniestro == null || !validacionEntregaDocumentos.isEmpty()){
							informacionCompleta = Boolean.FALSE;
						}else{
							informacionCompleta = Boolean.TRUE;
						}
					}
					return INPUT;
			}else{
				informacionCompleta = Boolean.TRUE;
				cartaSiniestroService.guardarEntregaDocumentos(cartaSiniestro, idIndemnizacion);
				cartaSiniestro = cartaSiniestroService.obtenerCartaEntregaDocumentos(idIndemnizacion);
			}
		}catch(Exception ex){
			setMensajeError("No se pudieron guardar los cambios");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}

	@Action(value = "imprimirEntregaDocumentos", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""}),
			@Result(name = INPUT, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP)})
	public String imprimirEntregaDocumentos(){
		List<String> validacionEntregaDocumentos = cartaSiniestroService.validarEntregaDocumentos(cartaSiniestro, tipoCarta, this.MODO_EJECUCION_IMPRIMIR);
		if(!informacionCompleta && !validacionEntregaDocumentos.isEmpty()){
			setMensajeListaPersonalizado("No se pudo imprimir. Faltan datos de la entrega de documentos:", obtenerMensajesError(validacionEntregaDocumentos), "10");
			informacionCompleta = Boolean.FALSE;
			return INPUT;
		}else{
			informacionCompleta = Boolean.TRUE;
			transporte = cartaSiniestroService.imprimirCartaEntregaDocumentos(cartaSiniestro, tipoCarta, idIndemnizacion);
			transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
			transporte.setContentType(TIPO_PDF);
			
			String fileName = "CartaEntregaDoctos_"+ cartaSiniestro.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
			transporte.setFileName(fileName);
			setMensajeExito();
			return SUCCESS;
		}
	}
	
	@Action(value = "listarFacturas", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/facturasEntregaDocumentosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/facturasEntregaDocumentosGrid.jsp")
			})
	public String listarFacturas() {
		if(cartaSiniestro != null && cartaSiniestro.getId() != null){
			facturas = cartaSiniestroService.listarFacturas(cartaSiniestro.getId());
		}
		if(facturas == null){
			facturas = new ArrayList<CartaSiniestroFactura>();
		}
		return SUCCESS;
	}
	
	@Action(value = "listarEndosos", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/endososEntregaDocumentosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/endososEntregaDocumentosGrid.jsp")
			})
	public String listarEndosos() {
		if(factura != null && factura.getId() != null){
			endosos = cartaSiniestroService.listarEndosos(factura.getId());
		}
		if(endosos == null){
			endosos = new ArrayList<CartaSiniestroEndoso>();
		}
		return SUCCESS;
	}
	
	@Action(value = "relacionarFactura", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP)
			})
	public String relacionarFactura() {
		List<String> validacionEntregaDocumentos = null;
		try{
			if(esEliminarEndoso){
				if(stringListaEndososEliminar == null || stringListaEndososEliminar.isEmpty()){
					setMensajeError("Debe seleccionar al menos un elemento de la lista para eliminar.");
					return INPUT;
				}
				String[] endososId = stringListaEndososEliminar.split(",");
				for(String endosoId: endososId){
					cartaSiniestroService.eliminarFactura(Long.parseLong(endosoId));
				}
			}else{
				cartaSiniestroService.agregarFactura(cartaSiniestro.getId(), factura);
			}
			if(cartaSiniestro.getId() != null){
				cartaSiniestro = cartaSiniestroService.obtenerCartaEntregaDocumentos(idIndemnizacion);
				validacionEntregaDocumentos = cartaSiniestroService.validarEntregaDocumentos(cartaSiniestro, tipoCarta, this.MODO_EJECUCION_IMPRIMIR);
				if(cartaSiniestro == null || !validacionEntregaDocumentos.isEmpty()){
					informacionCompleta = Boolean.FALSE;
				}else{
					informacionCompleta = Boolean.TRUE;
				}
			}
			facturas = cartaSiniestroService.listarFacturas(cartaSiniestro.getId());
			return SUCCESS;
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			return INPUT;
		}
	}
	
	@Action(value = "relacionarEndoso", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORENTREGADOCUMENTOS_JSP)
			})
	public String relacionarEndoso() {
		try{
			if(esEliminarEndoso){
				if(stringListaEndososEliminar == null || stringListaEndososEliminar.isEmpty()){
					setMensajeError("Debe seleccionar al menos un elemento de la lista para eliminar.");
					return INPUT;
				}
				String[] endososId = stringListaEndososEliminar.split(",");
				for(String endosoId: endososId){
					cartaSiniestroService.eliminarEndoso(Long.parseLong(endosoId));
				}
			}else{
				cartaSiniestroService.agregarEndosos(factura.getId(), nombreEndoso);
			}
			endosos = cartaSiniestroService.listarEndosos(factura.getId());
			facturas = cartaSiniestroService.listarFacturas(factura.getId());
			return SUCCESS;
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			return INPUT;
		}
	}
	
	@Action(value = "imprimirCartaPTS", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirCartaPTS(){
		transporte = cartaSiniestroService.imprimirCartaPTSNoDocumentada(idIndemnizacion);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "CartaPTS_"+ idIndemnizacion + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value = "mostrarInfoContrato", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp")})
	public String mostrarInfoContrato(){
		try{
			infoContrato = cartaSiniestroService.buscarInfoContrato(idIndemnizacion);
			if(infoContrato == null){infoContrato = new InfoContratoSiniestro();}
			if(infoContrato.getApoderadoAfirme() == null){
				infoContrato.setApoderadoAfirme(parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID, PARAMETRO_GLOBAL_APODERADO));
			}
		}catch(Exception ex){
			setMensajeError("No se pudo recuperar la informaci\u00F3N del contrato");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarInfoContrato", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp")})
	public String guardarInfoContrato(){//TODO
		try{
			infoContrato = cartaSiniestroService.guardarInfoContrato(infoContrato, idIndemnizacion);
		}catch(Exception ex){
			setMensajeError("No se pudo guardar la informacion del contrato");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "imprimirInfoContrato", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirInfoContrato(){
		try{
			infoContrato = cartaSiniestroService.guardarInfoContrato(infoContrato, idIndemnizacion);
		}catch(Exception ex){
			setMensajeError("No se pudo guardar la informacion del contrato");
			return INPUT;
		}
		transporte = cartaSiniestroService.imprimirContratoIndemnizacion(idIndemnizacion, infoContrato);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "ContratoSalvamento_"+ idIndemnizacion + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	private boolean tienePoliza(){
		try{
			return entidad.getIndemnizacion().getSiniestro().getReporteCabina().getPoliza() != null;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * @return the cartaSiniestro
	 */
	public CartaSiniestro getCartaSiniestro() {
		return cartaSiniestro;
	}

	/**
	 * @param cartaSiniestro the cartaSiniestro to set
	 */
	public void setCartaSiniestro(CartaSiniestro cartaSiniestro) {
		this.cartaSiniestro = cartaSiniestro;
	}

	/**
	 * @return the entidad
	 */
	public CartaSiniestro getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(CartaSiniestro entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the incisoSiniestro
	 */
	public IncisoSiniestroDTO getIncisoSiniestro() {
		return incisoSiniestro;
	}

	/**
	 * @param incisoSiniestro the incisoSiniestro to set
	 */
	public void setIncisoSiniestro(IncisoSiniestroDTO incisoSiniestro) {
		this.incisoSiniestro = incisoSiniestro;
	}

	/**
	 * @return the bajaPlacasDTO
	 */
	public CartaBajaPlacasDTO getBajaPlacasDTO() {
		return bajaPlacasDTO;
	}

	/**
	 * @param bajaPlacasDTO the bajaPlacasDTO to set
	 */
	public void setBajaPlacasDTO(CartaBajaPlacasDTO bajaPlacasDTO) {
		this.bajaPlacasDTO = bajaPlacasDTO;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the informacionCompleta
	 */
	public Boolean getInformacionCompleta() {
		return informacionCompleta;
	}

	/**
	 * @param informacionCompleta the informacionCompleta to set
	 */
	public void setInformacionCompleta(Boolean informacionCompleta) {
		this.informacionCompleta = informacionCompleta;
	}

	/**
	 * @return the reporteCabinaId
	 */
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	/**
	 * @param reporteCabinaId the reporteCabinaId to set
	 */
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	/**
	 * @return the valuadorId
	 */
	public Integer getValuadorId() {
		return valuadorId;
	}

	/**
	 * @param valuadorId the valuadorId to set
	 */
	public void setValuadorId(Integer valuadorId) {
		this.valuadorId = valuadorId;
	}
	
	/**
	 * @return the idIndemnizacion
	 */
	public Long getIdIndemnizacion() {
		return idIndemnizacion;
	}

	/**
	 * @param idIndemnizacion the idIndemnizacion to set
	 */
	public void setIdIndemnizacion(Long idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}

	/**
	 * @return the fechaDeterminacion
	 */
	public Date getFechaDeterminacion() {
		return fechaDeterminacion;
	}

	/**
	 * @param fechaDeterminacion the fechaDeterminacion to set
	 */
	public void setFechaDeterminacion(Date fechaDeterminacion) {
		this.fechaDeterminacion = fechaDeterminacion;
	}

	/**
	 * @return the tipoSiniestroDesc
	 */
	public String getTipoSiniestroDesc() {
		return tipoSiniestroDesc;
	}

	/**
	 * @param tipoSiniestroDesc the tipoSiniestroDesc to set
	 */
	public void setTipoSiniestroDesc(String tipoSiniestroDesc) {
		this.tipoSiniestroDesc = tipoSiniestroDesc;
	}

	/**
	 * @return the tipoSiniestro
	 */
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}

	/**
	 * @param tipoSiniestro the tipoSiniestro to set
	 */
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	/**
	 * @return the esFiniquitoAsegurado
	 */
	public Boolean getEsFiniquitoAsegurado() {
		return esFiniquitoAsegurado;
	}

	/**
	 * @param esFiniquitoAsegurado the esFiniquitoAsegurado to set
	 */
	public void setEsFiniquitoAsegurado(Boolean esFiniquitoAsegurado) {
		this.esFiniquitoAsegurado = esFiniquitoAsegurado;
	}
	
	/**
	 * @return the fechasPerdidaTotal
	 */
	public FormatoFechasPT getFechasPerdidaTotal() {
		return fechasPerdidaTotal;
	}

	/**
	 * @param fechasPerdidaTotal the fechasPerdidaTotal to set
	 */
	public void setFechasPerdidaTotal(FormatoFechasPT fechasPerdidaTotal) {
		this.fechasPerdidaTotal = fechasPerdidaTotal;
	}

	/**
	 * @return the idOrdenCompra
	 */
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	/**
	 * @param idOrdenCompra the idOrdenCompra to set
	 */
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	/**
	 * @return the consulta
	 */
	public Short getConsulta() {
		return consulta;
	}

	/**
	 * @param consulta the consulta to set
	 */
	public void setConsulta(Short consulta) {
		this.consulta = consulta;
	}

	/**
	 * @return the siniestroId
	 */
	public Long getSiniestroId() {
		return siniestroId;
	}

	/**
	 * @param siniestroId the siniestroId to set
	 */
	public void setSiniestroId(Long siniestroId) {
		this.siniestroId = siniestroId;
	}

	/**
	 * @return the tipoCarta
	 */
	public String getTipoCarta() {
		return tipoCarta;
	}

	/**
	 * @param tipoCarta the tipoCarta to set
	 */
	public void setTipoCarta(String tipoCarta) {
		this.tipoCarta = tipoCarta;
	}

	/**
	 * @return the nombreEndoso
	 */
	public String getNombreEndoso() {
		return nombreEndoso;
	}

	/**
	 * @param nombreEndoso the nombreEndoso to set
	 */
	public void setNombreEndoso(String nombreEndoso) {
		this.nombreEndoso = nombreEndoso;
	}

	/**
	 * @return the endosos
	 */
	public List<CartaSiniestroEndoso> getEndosos() {
		return endosos;
	}

	/**
	 * @param endosos the endosos to set
	 */
	public void setEndosos(List<CartaSiniestroEndoso> endosos) {
		this.endosos = endosos;
	}

	/**
	 * @return the esEliminarEndoso
	 */
	public Boolean getEsEliminarEndoso() {
		return esEliminarEndoso;
	}

	/**
	 * @param esEliminarEndoso the esEliminarEndoso to set
	 */
	public void setEsEliminarEndoso(Boolean esEliminarEndoso) {
		this.esEliminarEndoso = esEliminarEndoso;
	}

	/**
	 * @return the stringListaEndososEliminar
	 */
	public String getStringListaEndososEliminar() {
		return stringListaEndososEliminar;
	}

	/**
	 * @param stringListaEndososEliminar the stringListaEndososEliminar to set
	 */
	public void setStringListaEndososEliminar(String stringListaEndososEliminar) {
		this.stringListaEndososEliminar = stringListaEndososEliminar;
	}

	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}

	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	/**
	 * @return the informacionSiniestro
	 */
	public DeterminacionInformacionSiniestroDTO getInformacionSiniestro() {
		return informacionSiniestro;
	}

	/**
	 * @param informacionSiniestro the informacionSiniestro to set
	 */
	public void setInformacionSiniestro(
			DeterminacionInformacionSiniestroDTO informacionSiniestro) {
		this.informacionSiniestro = informacionSiniestro;
	}

	/**
	 * @return the filtroPT
	 */
	public PerdidaTotalFiltro getFiltroPT() {
		return filtroPT;
	}

	/**
	 * @param filtroPT the filtroPT to set
	 */
	public void setFiltroPT(PerdidaTotalFiltro filtroPT) {
		this.filtroPT = filtroPT;
	}

	/**
	 * @return the esCoberturaAsegurado
	 */
	public Boolean getEsCoberturaAsegurado() {
		return esCoberturaAsegurado;
	}

	/**
	 * @param esCoberturaAsegurado the esCoberturaAsegurado to set
	 */
	public void setEsCoberturaAsegurado(Boolean esCoberturaAsegurado) {
		this.esCoberturaAsegurado = esCoberturaAsegurado;
	}

	/**
	 * @return the listaEstados
	 */
	public Map<String, String> getListaEstados() {
		if(listaEstados == null || listaEstados.isEmpty()){
			this.listaEstados = listadoService.getMapEstadosPorPaisMidas(null);
		}
		return listaEstados;
	}

	/**
	 * @param listaEstados the listaEstados to set
	 */
	public void setListaEstados(Map<String, String> listaEstados) {
		this.listaEstados = listaEstados;
	}

	/**
	 * @return the factura
	 */
	public CartaSiniestroFactura getFactura() {
		return factura;
	}

	/**
	 * @param factura the factura to set
	 */
	public void setFactura(CartaSiniestroFactura factura) {
		this.factura = factura;
	}

	/**
	 * @return the facturas
	 */
	public List<CartaSiniestroFactura> getFacturas() {
		return facturas;
	}

	/**
	 * @param facturas the facturas to set
	 */
	public void setFacturas(List<CartaSiniestroFactura> facturas) {
		this.facturas = facturas;
	}

	/**
	 * @return the infoContrato
	 */
	public InfoContratoSiniestro getInfoContrato() {
		return infoContrato;
	}

	/**
	 * @param infoContrato the infoContrato to set
	 */
	public void setInfoContrato(InfoContratoSiniestro infoContrato) {
		this.infoContrato = infoContrato;
	}

	/**
	 * @return the esConsulta
	 */
	public Integer getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Integer esConsulta) {
		this.esConsulta = esConsulta;
	}
	
}