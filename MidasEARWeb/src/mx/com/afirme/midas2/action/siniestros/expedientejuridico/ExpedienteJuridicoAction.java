package mx.com.afirme.midas2.action.siniestros.expedientejuridico;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ComentarioJuridico;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoDTO;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoExportableDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.expedientejuridico.ExpedienteJuridicoService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * Ejecutar
 * 
 * this.expedientesJuridicos= expedienteJuridicoService.buscarExpedientes
 * @author Israel
 * @version 1.0
 * @created 06-ago.-2015 10:30:35 a. m.
 */
@Component
@Scope("prototype")
@Namespace(value = "/siniestros/expedientejuridico")
public class ExpedienteJuridicoAction extends BaseAction implements Preparable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	final String CONTENEDOR_EXPEDIENTE_JURIDICO = "/jsp/siniestros/expedientejuridico/contenedorExpedienteJuridico.jsp";
	final String COMENTARIOS_EXPEDIENTE_JURIDICO_GRID = "/jsp/siniestros/expedientejuridico/comentariosExpedienteJuridicoGrid.jsp";
	final String CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO =  "/jsp/siniestros/expedientejuridico/contenedorBusquedaExpedienteJuridico.jsp";
	final String CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO_GRID =  "/jsp/siniestros/expedientejuridico/listadoExpedienteJuridicoGrid.jsp";
	
	private ExpedienteJuridicoDTO expedienteJuridicoDTO;
	private List<ExpedienteJuridico> expedientesJuridicos;
	private List<ComentarioJuridico> comentarios;
	
	private ExpedienteJuridico expedienteJuridico;
	private String comentarioAfirme;
	private String comentarioProveedor;
	
	private Boolean esConsulta;
	private Boolean sePuedeBuscarSiniestro;
	
	@Autowired
	@Qualifier("expedienteJuridicoEJB")
	public ExpedienteJuridicoService expedienteJuridicoService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	private TransporteImpresionDTO transporte;
	
	Map<String,String> estatusJuridicoMap;
	Map<String,String> motivoTurnoMap;
	Map<String,String> estatusVehiculoMap;
	Map<String,String> tipoConclusionMap;
	Map<Long,String>   abogadosMap;
	private Map<String,String> tipoAbogado;



	@Action(value = "buscarExpedienteJuridico", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO_GRID),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO_GRID) 
	})
	public String buscar(){
		this.expedientesJuridicos = this.expedienteJuridicoService.buscarExpedientes(expedienteJuridicoDTO);
		return SUCCESS;
	}

	@Action(value="exportarExcel",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarExcel(){
		this.expedientesJuridicos = this.expedienteJuridicoService.buscarExpedientes(expedienteJuridicoDTO);
		List<ExpedienteJuridicoExportableDTO> dtoList = this.expedienteJuridicoService.crearExpedientesExportables(expedientesJuridicos);
		ExcelExporter exporter = new ExcelExporter(ExpedienteJuridicoExportableDTO.class);
		transporte = exporter.exportXLS(dtoList, "Expedientes Juridicos");
		return SUCCESS;
	}
	
	public void prepareMostrarContenedorExpedienteJuridico(){
		this.estatusJuridicoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_JURIDICO);
		this.motivoTurnoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_MOTIVO_TURNO);
		this.estatusVehiculoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_ESTATUS_VEHICULO);
		this.tipoConclusionMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_TIPO_CONCLUSION);
		this.abogadosMap = listadoService.obtenerAbogados();
		this.sePuedeBuscarSiniestro = (this.expedienteJuridico!=null && this.expedienteJuridico.getId()!=null)?Boolean.FALSE:Boolean.TRUE;
	}

	
	@Action(value = "mostrarContenedorExpedienteJuridico", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_EXPEDIENTE_JURIDICO),
			@Result(name = INPUT, location = CONTENEDOR_EXPEDIENTE_JURIDICO) })
	public String mostrarContenedorExpedienteJuridico(){
		if(this.expedienteJuridico != null && this.expedienteJuridico.getId()!=null){
			List<ExpedienteJuridico> expJuridicoList = this.entidadService.findByProperty(ExpedienteJuridico.class, "id", this.expedienteJuridico.getId());
			if(expJuridicoList!=null && expJuridicoList.size()>0){
				this.expedienteJuridico  = expJuridicoList.get(0);
				ServicioSiniestro abogado = this.expedienteJuridico.getAbogado();
				if(abogado!=null){
					if(abogado.getAmbitoPrestadorServicio().intValue() == ServicioSiniestro.Ambito.EXTERNO.getValue().intValue()){
						abogado.setAmbitoDesc(ServicioSiniestro.Ambito.EXTERNO.toString());
					}else if(abogado.getAmbitoPrestadorServicio().intValue() == ServicioSiniestro.Ambito.INTERNO.getValue().intValue()){
						abogado.setAmbitoDesc(ServicioSiniestro.Ambito.INTERNO.toString());
					}
				}
			}
		}
		return SUCCESS;
	}
	
	
	public void prepareGuardarExpedienteJuridico(){
		this.estatusJuridicoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_JURIDICO);
		this.motivoTurnoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_MOTIVO_TURNO);
		this.estatusVehiculoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_ESTATUS_VEHICULO);
		this.tipoConclusionMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_TIPO_CONCLUSION);
		this.abogadosMap = listadoService.obtenerAbogados();
		this.sePuedeBuscarSiniestro = (this.expedienteJuridico!=null && this.expedienteJuridico.getId()!=null)?Boolean.FALSE:Boolean.TRUE;
	}
	@Action(value = "guardarExpedienteJuridico", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_EXPEDIENTE_JURIDICO),
			@Result(name = INPUT, location = CONTENEDOR_EXPEDIENTE_JURIDICO) })
	public String guardarExpedienteJuridico(){
		this.expedienteJuridicoService.guardar(expedienteJuridico, comentarioAfirme, comentarioProveedor);
		this.comentarioAfirme= null;
		this.comentarioProveedor= null;
		super.setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "obtenerComentarios", results = {
			@Result(name = SUCCESS, location = COMENTARIOS_EXPEDIENTE_JURIDICO_GRID),
			@Result(name = INPUT, location = COMENTARIOS_EXPEDIENTE_JURIDICO_GRID) })
	public String obtenerComentarios(){
		this.comentarios = this.expedienteJuridicoService.obtenerComentarios(this.expedienteJuridico.getId());
		return SUCCESS;
	}

	@Action(value = "mostrarListadoJuridico", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO_GRID),
			@Result(name = INPUT  , location = CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO_GRID) 
	})
	public String mostrarListado(){
		return SUCCESS;
	}

	public void prepareMostrarContenedorBusquedaExpedienteJuridico(){
		this.estatusJuridicoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_JURIDICO);
		this.motivoTurnoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_MOTIVO_TURNO);
		this.estatusVehiculoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_ESTATUS_VEHICULO);
		this.tipoConclusionMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.JURIDICO_TIPO_CONCLUSION);
		this.tipoAbogado = this.listadoService.obtenerCatalogoValorFijo( CatGrupoFijo.TIPO_CATALOGO.TIPO_SOLICITUD );
		this.abogadosMap = listadoService.obtenerAbogados();
	}
	@Action(value = "mostrarContenedorBusquedaExpedienteJuridico", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO),
			@Result(name = INPUT, location = CONTENEDOR_BUSQUEDA_EXPEDIENTE_JURIDICO) })
	public String mostrarContenedorBusquedaExpedienteJuridico(){
		return SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		
		 
	}

	public ExpedienteJuridicoDTO getExpedienteJuridicoDTO() {
		return expedienteJuridicoDTO;
	}

	public void setExpedienteJuridicoDTO(ExpedienteJuridicoDTO expedienteJuridicoDTO) {
		this.expedienteJuridicoDTO = expedienteJuridicoDTO;
	}

	public List<ExpedienteJuridico> getExpedientesJuridicos() {
		return expedientesJuridicos;
	}

	public void setExpedientesJuridicos(
			List<ExpedienteJuridico> expedientesJuridicos) {
		this.expedientesJuridicos = expedientesJuridicos;
	}

	public ExpedienteJuridicoService getExpedienteJuridicoService() {
		return expedienteJuridicoService;
	}

	public void setExpedienteJuridicoService(
			ExpedienteJuridicoService expedienteJuridicoService) {
		this.expedienteJuridicoService = expedienteJuridicoService;
	}

	public ExpedienteJuridico getExpedienteJuridico() {
		return expedienteJuridico;
	}

	public void setExpedienteJuridico(ExpedienteJuridico expedienteJuridico) {
		this.expedienteJuridico = expedienteJuridico;
	}

	public String getComentarioAfirme() {
		return comentarioAfirme;
	}

	public void setComentarioAfirme(String comentarioAfirme) {
		this.comentarioAfirme = comentarioAfirme;
	}

	public String getComentarioProveedor() {
		return comentarioProveedor;
	}

	public void setComentarioProveedor(String comentarioProveedor) {
		this.comentarioProveedor = comentarioProveedor;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getEstatusJuridicoMap() {
		return estatusJuridicoMap;
	}

	public void setEstatusJuridicoMap(Map<String, String> estatusJuridicoMap) {
		this.estatusJuridicoMap = estatusJuridicoMap;
	}

	public Map<String, String> getMotivoTurnoMap() {
		return motivoTurnoMap;
	}

	public void setMotivoTurnoMap(Map<String, String> motivoTurnoMap) {
		this.motivoTurnoMap = motivoTurnoMap;
	}

	public Map<String, String> getEstatusVehiculoMap() {
		return estatusVehiculoMap;
	}

	public void setEstatusVehiculoMap(Map<String, String> estatusVehiculoMap) {
		this.estatusVehiculoMap = estatusVehiculoMap;
	}

	public Map<String, String> getTipoConclusionMap() {
		return tipoConclusionMap;
	}

	public void setTipoConclusionMap(Map<String, String> tipoConclusionMap) {
		this.tipoConclusionMap = tipoConclusionMap;
	}

	public Map<Long, String> getAbogadosMap() {
		return abogadosMap;
	}

	public void setAbogadosMap(Map<Long, String> abogadosMap) {
		this.abogadosMap = abogadosMap;
	}

	public List<ComentarioJuridico> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<ComentarioJuridico> comentarios) {
		this.comentarios = comentarios;
	}

	public Boolean getEsConsulta() {
		return esConsulta;
	}

	public void setEsConsulta(Boolean esConsulta) {
		this.esConsulta = esConsulta;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public Boolean getSePuedeBuscarSiniestro() {
		return sePuedeBuscarSiniestro;
	}

	public void setSePuedeBuscarSiniestro(Boolean sePuedeBuscarSiniestro) {
		this.sePuedeBuscarSiniestro = sePuedeBuscarSiniestro;
	}

	public Map<String, String> getTipoAbogado() {
		return tipoAbogado;
	}

	public void setTipoAbogado(Map<String, String> tipoAbogado) {
		this.tipoAbogado = tipoAbogado;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	
	
}