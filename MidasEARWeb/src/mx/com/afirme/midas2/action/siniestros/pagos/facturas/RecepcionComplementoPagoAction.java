package mx.com.afirme.midas2.action.siniestros.pagos.facturas;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/facturas/recepcionComplementoPago")
public class RecepcionComplementoPagoAction extends BaseAction implements Preparable{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(RecepcionComplementoPagoAction.class);
	 
	private Integer tipoUsuario;
	private Map<Long,String> listaOficinas;
	private List<Map<String, Object>> listaPrestadoresServicio;
	private String nombrePrestadorServicio;
	private Integer idPrestadorServicio;
	private Long idOficina;
	private Integer numComplementosCargados;
	private PrestadorDeServicioService prestadorDeServicioService;
	private Oficina oficinaPrestadorServicio;
	public UsuarioService usuarioService;


	
		
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	public void setPrestadorDeServicioService(
			PrestadorDeServicioService prestadorDeServicioService) {
		this.prestadorDeServicioService = prestadorDeServicioService;
	}

	public void prepareMostrarRecepcionComplementoPago()
	{
		Usuario usuario = usuarioService.getUsuarioActual();
		tipoUsuario = usuario.getTipoUsuario();

		if(numComplementosCargados== null)
		{
			numComplementosCargados = 0;
		}
		
		listaPrestadoresServicio = prestadorDeServicioService.obtenerPrestadoresPorUsuario(usuario.getNombreUsuario());
		
		if(listaPrestadoresServicio.size() == 1) // un solo registro
		{
			idPrestadorServicio = Integer.valueOf(listaPrestadoresServicio.get(0).get("id").toString());
		}
		
		if(idPrestadorServicio != null)
		{
			PrestadorServicio prestador = prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue());
			nombrePrestadorServicio = prestador.getPersonaMidas().getNombre();
			oficinaPrestadorServicio = prestador.getOficina();
		}	
	}
	
	@Action(value="mostrarRecepcionComplementoPago",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/pagos/facturas/recepcion/ContenedorRecepcionComplementoPago.jsp")
	})	
	public String mostrarRecepcionComplementoPago() {
		
		return SUCCESS;
	}

	@Action(value="obtenerOficina",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^oficinaPrestadorServicio\\.id,^oficinaPrestadorServicio\\.nombreOficina"})			
		})		
	public String obtenerOficina(){
		
		oficinaPrestadorServicio = prestadorDeServicioService.buscarPrestador(idPrestadorServicio.intValue()).getOficina();
		
		return SUCCESS;
	}

	public Map<Long, String> getListaOficinas() {
		return listaOficinas;
	}

	
	public void setListaOficinas(Map<Long, String> listaOficinas) {
		this.listaOficinas = listaOficinas;
	}	

	public List<Map<String, Object>> getListaPrestadoresServicio() {
		return listaPrestadoresServicio;
	}

	public void setListaPrestadoresServicio(
			List<Map<String, Object>> listaPrestadoresServicio) {
		this.listaPrestadoresServicio = listaPrestadoresServicio;
	}

	public Integer getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Integer tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNombrePrestadorServicio() {
		return nombrePrestadorServicio;
	}

	public Oficina getOficinaPrestadorServicio() {
		return oficinaPrestadorServicio;
	}

	public void setOficinaPrestadorServicio(Oficina oficinaPrestadorServicio) {
		this.oficinaPrestadorServicio = oficinaPrestadorServicio;
	}

	public void setNombrePrestadorServicio(String nombrePrestadorServicio) {
		this.nombrePrestadorServicio = nombrePrestadorServicio;
	}

	public Integer getIdPrestadorServicio() {
		return idPrestadorServicio;
	}

	public void setIdPrestadorServicio(Integer idPrestadorServicio) {
		this.idPrestadorServicio = idPrestadorServicio;
	}

	public Long getIdOficina() {
		return idOficina;
	}

	public void setIdOficina(Long idOficina) {
		this.idOficina = idOficina;
	}
		
	public Integer getNumComplementosCargados() {
		return numComplementosCargados;
	}

	public void setNumComplementosCargados(Integer numComplementosCargados) {
		this.numComplementosCargados = numComplementosCargados;
	}

}