package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina.EstatusVigenciaInciso;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita.CitaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService.FechaHoraDTO;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService.TIPO_FECHA_HORA;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.cometd.bayeux.server.BayeuxServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/reportecabina")
public class ReporteCabinaAction  extends BaseAction implements Preparable {
	
	private static final String	LOCATION_CONTENEDORLUGARREPORTECABINA_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorLugarReporteCabina.jsp";
	private static final String	LOCATION_CONTENEDORTEXTOLIBRE_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorTextoLibre.jsp";
	private static final String	LOCATION_CONTENEDORCITAREPORTECABINA_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorCitaReporteCabina.jsp";
	private static final String	LOCATION_CONTENEDORREPORTECABINA_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorReporteCabina.jsp";
	private static final String	VENTANA_ORIGEN_REPORTE_CABINA	= "ventanaOrigenReporteCabina";

	private static final long serialVersionUID = -1363327055383126637L;
	public static final Logger log = Logger.getLogger(ReporteCabinaAction.class);
	
//	INFORMACION DEL ReporteCabina
	private Long idToReporte;
	private ReporteCabina entidad = new ReporteCabina();
	private CitaReporteCabina cita;
	private String informacionAdicional;
	private String declaracionTexto;
	private String valueTextoLibre;
	private String numeroSiniestro;
	private String atendidoPor;
	private String nsOficina;
	private String nsConsecutivoR;
	private String nsAnio;
	private String latitud;
	private String longitud;
	private Boolean isConfirmarUbicacion;
	
//	INFORMACION REQUERIDA PARA CREAR REPORTE
	private String nombreReporta;
	private String telLadaReporta;
	private String telefonoReporta;
	
//	INFORMACION DEL LugarSiniestroMidas
	private String tipoLugar;
	private String tituloVentana;
	private LugarSiniestroMidas lugarAtencionOcurrido;
	private LugarSiniestroMidas lugarAtencion;
	private Double latitudLugar;
	private Double longitudLugar;
	private Boolean coordenadasConfirmadas = Boolean.FALSE;
	
//	LISTAS PARA LOS COMBOS DE LugarSiniestroMidas
	private Map<String, String> zonas;
	private Map<String,String> tipoCarreteras;
	private Map<Long,String> oficinasActivas;
	private String estatusReporte;
	
//	INFORMACION BUSQUEDA POLIZA INCISO
	private Long incisoContinuityId;
	private IncisoSiniestroDTO detalleInciso;
	
//	INFORMACION PARA EJECUTAR SOLICITUD AUTORIZACION VIGENCIA EN LA PANTALLA DE SINIESTRO CABINA
	private SiniestroCabinaDTO siniestroDTO;
	private Long validOnMillis;
	private Long recordFromMillis;
	private Short soloConsulta;
	
//	HORAS 
	private String horaReporte;
	private String horaAsignacion;
	private String horaContacto;
	private String horaTerminacion;
	private FechaHoraDTO fechaHoraDTO = new FechaHoraDTO();
	private String tipoFechaHora;
	private String isEditable;
	

	private TransporteImpresionDTO transporte;
	
	// Canales de Cometd
//	private static final String oficinaPrefijoChannel = "/oficina/";
	private static final String reporteCanceladoChannel = "/reporte-cabina/reporte-cancelado/";
	private static final String reporteCitaCreadaChannel = "/reporte-cabina/cita-creada/";
	private static final String reporteCreadoChannel = "/reporte-cabina/reporte-creado/";
	private static final String reporteAsignadoChannel = "/reporte-cabina/ajustador-asignado/";
	private static final String reporteContactoChannel = "/reporte-cabina/ajustador-contacto/";
	private static final String reporteTerminoChannel = "/reporte-cabina/ajustador-termino/";

	//validaciones
	private boolean validacionFechaHoraOcurrido;
	private String mensajeParaCrearPasesDeAtencion;
	
	private String causaSiniestro;
	private String tipoRegistro;
	private String tipoResonsabilidad; 
	private String terminoDeAjuste; 
	private String ventanaOrigen;
	private String estatusVigenciaInciso;
	
	private boolean despliegaMensajeExito;
	
	
	private Boolean vieneBandejaSolicitudAntiguedad;
	
	// VARIABLES DE EXPEDIENTE JURIDICO
	private ExpedienteJuridico expedienteJuridico;
	private Boolean vieneExpedienteJuridico;
	private Boolean modoConsultaExpediente;
	
	
	private Boolean noEditableParaRol = Boolean.FALSE;
	

	private int estatusPendienteReporte;
	
	//TODO remove. Es para Dev y QA
	private String version = "v12.0.54";
	
	private Short incisoAutorizado;

	public static enum EnumTipoFechaCambio {
		CREACION, ASIGNACION, ARRIBO, TERMINO
	};
	
//	MOTIVO CANCELACION QUE PERMITE AUTORIZACION
	public static final String MOTIVO_FALTA_PAGO = "CANCELADA POR FALTA DE PAGO";
//		"CANCELADA A PETICION"; //BORRAR, PARA PRUEBAS
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	
	@Autowired
	@Qualifier("polizaSiniestroServiceEJB")
	private PolizaSiniestroService polizaSiniestroService;
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@Autowired
	@Qualifier("siniestroCabinaServiceEJB")
	private SiniestroCabinaService siniestroService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	
	public static final String MENSAJE_ERROR_COBERTURAS_AFECTADAS 	= "No se puede cancelar el reporte,ya se afectaron las coberturas";
	public static final String MENSAJE_REPORTE_CANCELADO 			= "Se cancelo el reporte";
	
	
	@Override
	public void prepare(){
		this.oficinasActivas = listadoService.obtenerOficinasSiniestros();		
	}
	
	private void llenarAtendidoPor(){
		if( idToReporte == null && entidad.getId() == null ){
			Usuario usuario = usuarioService.getUsuarioActual();	
			this.atendidoPor = usuario.getNombre().concat(!StringUtil.isEmpty(usuario.getApellidoPaterno()) ? ' ' + usuario.getApellidoPaterno() : " ").concat(
					!StringUtil.isEmpty(usuario.getApellidoMaterno()) ? ' ' + usuario.getApellidoMaterno() : " ");
			/*entidad.setFechaHoraReporte(new Date());
			DateFormat sdf = new SimpleDateFormat("HH:mm");
			this.horaReporte =sdf.format( entidad.getFechaHoraReporte() );*/
		}		
	}
	
	public void prepareMostrarContenedor() {		
		llenarAtendidoPor();		
		if (ventanaOrigen != null && !ventanaOrigen.equals("")) {
			ServletActionContext.getContext().getSession().put(VENTANA_ORIGEN_REPORTE_CABINA, ventanaOrigen);
		}
	}
	
	public void prepareMostrarBuscarReporte() {		
		llenarAtendidoPor();		
		if (ventanaOrigen != null && !ventanaOrigen.equals("")) {
			ServletActionContext.getContext().getSession().put(VENTANA_ORIGEN_REPORTE_CABINA, ventanaOrigen);
		}
	}
	
	public void prepareMostrarCita(){
		if(idToReporte != null){
			entidad = reporteCabinaService.buscarReporte(idToReporte);
			if(entidad != null){
				if(entidad.getCitaReporteCabina() == null){
					cita = new CitaReporteCabina();
					cita.setAsegurado(entidad.getPersonaConductor());
					cita.setLadaTelefono(entidad.getLadaTelefono());
					cita.setTelefono(entidad.getTelefono());
				}else{
					cita = entidad.getCitaReporteCabina();
					if(cita.getAsegurado() == null || "".equals(cita.getAsegurado())){
						cita.setAsegurado(entidad.getPersonaConductor());
					}
					if(cita.getTelefono() == null || "".equals(cita.getTelefono())){
						cita.setTelefono(entidad.getTelefono());
					}
					if(cita.getLadaTelefono() == null || "".equals(cita.getLadaTelefono())){
						cita.setLadaTelefono(entidad.getLadaTelefono());
					}
				}
			}
		}
	}
	
	public void prepareMostrarLugar(){
		zonas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ZONA);
		tipoCarreteras = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_CARRETERA);
	}
	
	public void prepareGuardarLugar(){
		zonas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ZONA);
		tipoCarreteras = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_CARRETERA);
	}
	
	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP))
	public String mostrarContenedor(){	
		
		
		if( this.idToReporte != null ){
			entidad = reporteCabinaService.buscarReporte(idToReporte);
			if( !reporteCabinaService.validarGuardadoFechaOcurrido (entidad)){			
				validacionFechaHoraOcurrido = Boolean.TRUE;
			}
			if(!StringUtil.isEmpty(entidad.getAtendidoPor())){
				this.atendidoPor = entidad.getAtendidoPor();	
			}			
			setInfoHora();			
			this.estatusReporte = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_REPORTE_CABINA).get(entidad.getEstatus().toString());			
		}
		return SUCCESS;
	}
	
	public String mostrarAseguradoContratante(){
		return null;
	}
	
	public String mostrarBitacora(){
		return null;
	}
	
	public String mostrarBuscarAjustador(){
		return null;
	}
	
	public String mostrarBuscarPoliza(){
		return null;
	}

	@Action(value = "mostrarBuscarReporte", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP))
	public String mostrarBuscarReporte(){		
		Usuario usuario = usuarioService.getUsuarioActual();
		
		for (Rol rol : usuario.getRoles()) {
			System.out.println("Rol: " + rol.getDescripcion());
			//if(rol.getDescripcion().equals("Rol_M2_Supervisor_Cabina_Editor"))
			if(rol.getDescripcion().equals("Rol_Subdirector_Rea"))
			{
				setIsEditable("SI");
				break;
			}else
			{
				setIsEditable("NO");
			}
		}
		
		/*
		if( SystemCommonUtils.isValid(nsOficina) && 
				SystemCommonUtils.isValid(nsConsecutivoR) && 
				SystemCommonUtils.isValid(nsAnio)){
			if( this.expedienteJuridico != null){
				entidad = reporteCabinaService.buscarReportePorSiniestro(nsOficina, nsConsecutivoR, nsAnio );
			}else{
				entidad = reporteCabinaService.buscarReporte( nsOficina, nsConsecutivoR, nsAnio );
				//idToReporte = entidad.getId();
			}
			
		}else {
			if( this.idToReporte != null ){
			entidad = reporteCabinaService.buscarReporte(idToReporte);
			}	
		}
		
		if( entidad != null ){
			setInfoHora();
			setIdToReporte(entidad.getId());
			this.estatusReporte = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_REPORTE_CABINA).get(entidad.getEstatus().toString());			
			if(!StringUtil.isEmpty(entidad.getAtendidoPor())){
				this.atendidoPor = entidad.getAtendidoPor();	
			}			
			if(entidad.getPoliza() !=null){				
				Date fechaReporteSiniestro 			= entidad.getFechaHoraOcurrido();
				this.detalleInciso 					= new IncisoSiniestroDTO();
				
				detalleInciso.setFechaReporteSiniestro( fechaReporteSiniestro );
				detalleInciso.setNumeroPoliza( entidad.getPoliza().getNumeroPolizaFormateada());
				detalleInciso.setIdToPoliza(entidad.getPoliza().getIdToPoliza());
			
				detalleInciso = polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(entidad.getId());
				estatusVigenciaInciso = polizaSiniestroService.obtenerSituacionVigenciaInciso(entidad.getId());
				
				if (detalleInciso.getEstatus() == EstatusVigenciaInciso.NOVIGENTE.ordinal()) {
					  incisoAutorizado = polizaSiniestroService.estatusIncisoAutorizacion(entidad.getPoliza().getIdToPoliza().longValue(),
							  entidad.getId(), detalleInciso.getNumeroInciso(), null);
				}
				
				//SPV CABIN Validacion lectura una vez convertido a siniestro
				if(reporteCabinaService.noEsEditableParaElRol(entidad)){
					noEditableParaRol = Boolean.TRUE;
				}
			
			}else if(SystemCommonUtils.isValid(entidad.getIdToSolicitud())){
				detalleInciso = polizaSiniestroService.obtenerCartaCoberturaAsignadoReporte(idToReporte, entidad.getIdToSolicitud());	
			}
			this.lugarAtencion = reporteCabinaService.mostrarLugarSiniestro(entidad.getId(), TipoLugar.AT );
			
			return SUCCESS;
		}else{
			setIdToReporte(null);
			return SUCCESS;
		}*/
		
		setIdToReporte(null);
		return SUCCESS;
	}
	

	
	@Action (value = "mostrarCita", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORCITAREPORTECABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORCITAREPORTECABINA_JSP)})
	public String mostrarCita(){
		return SUCCESS;	
	}
	
	@Action(value = "mostrarDeclaracion", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORTEXTOLIBRE_JSP))
	public String mostrarDeclaracion(){
		return SUCCESS;
	}
	
	public String mostrarGastosAjuste(){
		return null;
	}
	
	public String mostrarHistorico(){
		return null;
	}
	
	@Action(value = "mostrarInfoAdicional", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORTEXTOLIBRE_JSP))
	public String mostrarInfoAdicional(){
		return SUCCESS;
	}
	
	@Action (value = "salvarCita", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORCITAREPORTECABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORCITAREPORTECABINA_JSP)})
	public String salvarCita(){
		try{
			entidad = reporteCabinaService.buscarReporte(idToReporte);
			CitaReporteCabina citaRep = entidad.getCitaReporteCabina();
			if(citaRep != null){
				citaRep.setAsegurado(cita.getAsegurado());
				citaRep.setCelular(cita.getCelular());
				citaRep.setFechaCita(cita.getFechaCita());
				citaRep.setFechaTermino(cita.getFechaTermino());
				citaRep.setHoraCita(cita.getHoraCita());
				citaRep.setHoraTermino(cita.getHoraTermino());
				citaRep.setLadaCelular(cita.getLadaCelular());
				citaRep.setLadaTelefono(cita.getLadaTelefono());
				citaRep.setTelefono(cita.getTelefono());
			}else{
				citaRep = cita;
			}
			reporteCabinaService.salvarCita(citaRep, idToReporte);			
			ExternalEventBroadcaster broadcaster = obtenerBayeuxBroadcaster();
			broadcaster.onExternalEvent("{\"id\":" + entidad.getId() + "}",
						reporteCitaCreadaChannel + entidad.getId());
			
			super.setMensajeExito();
			return SUCCESS;
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	
	@Action(value = "mostrarLugar", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLUGARREPORTECABINA_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORLUGARREPORTECABINA_JSP)})
	public String mostrarLugar(){
		
		if(idToReporte != null && idToReporte.longValue() != 0l){
			lugarAtencionOcurrido = reporteCabinaService.mostrarLugarSiniestro(idToReporte, tipoLugar.equals(TipoLugar.AT.toString())? TipoLugar.AT : TipoLugar.OC);
			if( lugarAtencionOcurrido != null && lugarAtencionOcurrido.getCoordenadas() != null ){
				lugarAtencion = lugarAtencionOcurrido;
				this.latitud = lugarAtencionOcurrido.getCoordenadas().getLatitud().toString();
				this.longitud = lugarAtencionOcurrido.getCoordenadas().getLongitud().toString();
				this.coordenadasConfirmadas = Boolean.TRUE;
			}
		}
		
		return SUCCESS;
	}
	
	@Action(value = "guardarLugar", results = { 
			@Result(name = INPUT, location = LOCATION_CONTENEDORLUGARREPORTECABINA_JSP) ,			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarLugar", "namespace",
					"/siniestros/cabina/reportecabina",
					"idToReporte", "${idToReporte}",
					"tipoLugar", "${tipoLugar}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) })		
	public String guardarLugar(){
		try{
			String mensajeValidarDireccion = lugarAtencionOcurrido.validarDireccion();
			if(SystemCommonUtils.isNotNull(lugarAtencionOcurrido.getColonia()) &&
					!SystemCommonUtils.isValid(lugarAtencionOcurrido.getColonia().getId())){
						lugarAtencionOcurrido.setColonia(null);
			}
			if(!mensajeValidarDireccion.isEmpty()){
				setMensajeError(mensajeValidarDireccion);
				return INPUT;
			}
			
			if( isConfirmarUbicacion ){
				Coordenadas coordenadas = new Coordenadas();
				coordenadas.setLatitud(new Double(latitud));
				coordenadas.setLongitud(new Double(longitud));
				lugarAtencionOcurrido.setCoordenadas(coordenadas);
			}

			idToReporte = reporteCabinaService.guardarLugarSiniestro(
					idToReporte, nombreReporta, telLadaReporta, telefonoReporta, lugarAtencionOcurrido, 
						tipoLugar.equals(TipoLugar.AT.toString())? TipoLugar.AT : TipoLugar.OC);
			
			this.lugarAtencion = reporteCabinaService.mostrarLugarSiniestro(idToReporte, TipoLugar.AT );
			
			setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex.getCause()instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(),"lugarAtencionOcurrido");
			}	
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "salvarReporte", results = {
		@Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP),
		@Result(name = INPUT, location = LOCATION_CONTENEDORREPORTECABINA_JSP)})	
	public String salvarReporte(){
		ReporteCabina reporteAnterior = null;
		validacionFechaHoraOcurrido = Boolean.FALSE;
		EnumTipoFechaCambio cambioFecha = null;		
		
		// variables para identificar el tipo de cambio a realizar en las fechas de
		// CREACION, ASIGNACION, ARRIBO, TERMINO para el uso del monitor
		// (el monitor usa cometd, el cual es usable solo en action)
				
		// Si entidad viene sin id, se asume que es CREACION
		if (entidad.getId() == null) {
			cambioFecha = EnumTipoFechaCambio.CREACION;
		} else {
			// Sino obtener fechas anteriores de la base de datos
			reporteAnterior = reporteCabinaService.buscarReporte(entidad.getId());
			entidad.setLugarAtencion(reporteAnterior.getLugarAtencion());
			entidad.setLugarOcurrido(reporteAnterior.getLugarOcurrido());
			entidad.setCitaReporteCabina(reporteAnterior.getCitaReporteCabina());
			entidad.setSiniestroCabina(reporteAnterior.getSiniestroCabina());
			entidad.setSeccionReporteCabina(reporteAnterior.getSeccionReporteCabina());
			entidad.setPoliza(reporteAnterior.getPoliza());
			entidad.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario().toString());
			entidad.setFechaHoraReporte(reporteAnterior.getFechaHoraReporte());
			entidad.setFechaHoraAsignacion(reporteAnterior.getFechaHoraAsignacion());
			entidad.setFechaHoraContacto(reporteAnterior.getFechaHoraContacto());
			entidad.setFechaHoraTerminacion(reporteAnterior.getFechaHoraTerminacion());
			entidad.setFechaModificacion(new Date());
			MonedaDTO moneda = reporteAnterior.getMoneda();
			if(moneda== null){
				moneda = this.entidadService.findById(MonedaDTO.class, Short.valueOf((short)MonedaDTO.MONEDA_PESOS));
			}
			entidad.setMoneda(moneda);
			entidad.setAgenteId(reporteAnterior.getAgenteId());
			entidad.setNombreAgente(reporteAnterior.getNombreAgente());
			entidad.setPersonaContratanteId(reporteAnterior.getPersonaContratanteId());
			if(((reporteAnterior.getAjustador() == null || reporteAnterior.getAjustador().getId() == null) && 
					entidad.getAjustador().getId() != null) || (reporteAnterior.getAjustador() != entidad.getAjustador())) {
				cambioFecha = EnumTipoFechaCambio.ASIGNACION;
			}
			
			//SPV CABIN Validacion lectura una vez convertido a siniestro
			if(reporteCabinaService.noEsEditableParaElRol(reporteAnterior)){
				super.setMensajeError(getText("midas.siniestros.cabina.reportecabina.error.noeditableporrol"));							
				noEditableParaRol = Boolean.TRUE;
				return INPUT;
			}
		}

		//Validar caso hora de ocurrido invalida
		if(!StringUtil.isEmpty(entidad.getHoraOcurrido()) && !entidad.getHoraOcurrido().contains(":")){
			super.setMensajeError(getText("midas.siniestros.cabina.reportecabina.error.horaOcurridoInvalida"));			
			return INPUT;			
		}
		
		if( !reporteCabinaService.validarGuardadoFechaOcurrido (entidad)){
			super.setMensajeError(getText("midas.siniestros.cabina.reportecabina.error.fechaHoraOcurrido"));
			validacionFechaHoraOcurrido = Boolean.TRUE;
			return INPUT;			
		}
		
		// # VALIDA SI LA FECHA ES MAYOR O IGUAL A LA DEL SISTEMA, SI ES MAYOR O IGUAL MARCA ERROR
		if( entidad.getFechaOcurrido() != null){			
			Date temFechaOcurrido = this.convertirFecha(entidad.getFechaOcurrido(), entidad.getHoraOcurrido());
			boolean fechaOcurridoInvalida = false;
			if( reporteAnterior != null && temFechaOcurrido.compareTo(reporteAnterior.getFechaCreacion()) > 0){
				fechaOcurridoInvalida = true;
			}else if(reporteAnterior == null && temFechaOcurrido.compareTo(new Date()) > 0){
				fechaOcurridoInvalida = true;
			}
			if(fechaOcurridoInvalida){
				super.setMensajeError(getText("midas.siniestros.cabina.reportecabina.error.fechaHoraOcurridoMayor")); 
				validacionFechaHoraOcurrido = Boolean.TRUE;
				return INPUT;
			}			
		}
		
		
	
		// FUNCION PRINCIPAL DE SALVAR REPORTE
		reporteCabinaService.salvarReporte(entidad);
		entidad = reporteCabinaService.buscarReporte(entidad.getId());
		
		//formatear fechas y horas de seguimiento
		setInfoHora();
		
		// Si se determino el cambio de fecha: enviar cometd event
		enviarAMonitor(cambioFecha);

		if( entidad.getId() == null){
			validacionFechaHoraOcurrido = Boolean.FALSE;		
		}
		
		CatValorFijo valorEstatus =   catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_REPORTE_CABINA, String.valueOf(entidad.getEstatus()));   
		this.estatusReporte = valorEstatus.getDescripcion();
		
		if(isDespliegaMensajeExito()) 
		{	
			 super.setMensajeExito();			
		}
				
		return SUCCESS;
	}
	
	private Date convertirFecha(Date fechaOcurrido, String hora){
		SimpleDateFormat sdfYYYY = new SimpleDateFormat("yyyy/MM/dd");
		SimpleDateFormat sdfYYYYHH = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date retVal = fechaOcurrido;
		try{
			String fecha = sdfYYYY.format(fechaOcurrido)+" "+hora+":00";
			
			retVal = sdfYYYYHH.parse(fecha);
			
		}catch(Exception e){
			log.error("Reporte Cabiba - Error al parsear la fecha de ocurrido");
		}
		
		return retVal;
	}
	
	private ExternalEventBroadcaster obtenerBayeuxBroadcaster(){
		// Prepara broadcaster de Cometd
		ServletContext sc = ServletActionContext.getServletContext();
		BayeuxServer bayeux = (BayeuxServer) sc.getAttribute(BayeuxServer.ATTRIBUTE);
		return new ExternalEventBroadcaster(bayeux);
	}
	
	
	private void enviarAMonitor(EnumTipoFechaCambio cambioFecha){		
		// Si se determino el cambio de fecha: enviar cometd event	
		if (cambioFecha != null) {			
			try{
				ExternalEventBroadcaster broadcaster = obtenerBayeuxBroadcaster();
				if(broadcaster != null){
					switch (cambioFecha) {
						case TERMINO:
							broadcaster.onExternalEvent("{\"id\":" + entidad.getId() + "}",
									reporteTerminoChannel + entidad.getId());
							break;
						case ARRIBO:
							broadcaster.onExternalEvent("{\"id\":" + entidad.getId() + "}",
									reporteContactoChannel + entidad.getId());
							break;
						case ASIGNACION:
							broadcaster.onExternalEvent("{\"id\":" + entidad.getId() + "}",
									reporteAsignadoChannel + entidad.getId());
							break;
						case CREACION:
							broadcaster.onExternalEvent("{\"id\":" + entidad.getId() + "}",
									reporteCreadoChannel + entidad.getId());
							break;
					}
				}
			}catch(Exception ex){
				log.error(ex);
			}
		}
	}

	@Action(value = "cancelarReporte", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP))
	public String cancelarReporte(){

		siniestroService.cancelarSiniestro(idToReporte);
		
		this.estatusReporte = catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_REPORTE_CABINA, EstatusReporteCabina.CANCELADO.getValue().toString());
		entidad.setEstatus(EstatusReporteCabina.CANCELADO.getValue());
		
		ExternalEventBroadcaster broadcaster = obtenerBayeuxBroadcaster();
		broadcaster.onExternalEvent("{\"id\":" + entidad.getId() + "}",
					reporteCanceladoChannel + entidad.getId());
		
		setMensaje( MENSAJE_REPORTE_CANCELADO );
		setTipoMensaje(MensajeDTO.TIPO_MENSAJE_INFORMACION);

		return SUCCESS;
	}
	
	/***
	 * Se agrega método que vuelve a buscar los datos del reporte ya que al marcar error pierde algunos valores
	 */
	private void recuperarDatosFechaHoraReporte(){
		
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
		
		try{
			
			this.entidad = reporteCabinaService.buscarReporte(idToReporte);
			
			if( this.entidad != null){
				
				if( this.entidad.getFechaHoraReporte() != null ){
					this.horaReporte = sdf.format( entidad.getFechaHoraReporte() );
				}
				
				if( this.entidad.getFechaHoraAsignacion() != null ){
					this.horaAsignacion = sdf.format( entidad.getFechaHoraAsignacion() );
				}
				
				if( this.entidad.getFechaHoraContacto() != null ){
					this.horaContacto = sdf.format( entidad.getFechaHoraContacto() );
				}
				
				if( this.entidad.getFechaHoraTerminacion() != null ){
					this.horaTerminacion = sdf.format( entidad.getFechaHoraTerminacion() );
				}
				
			}
		}catch(Exception e){
			log.error("Error al recuperarDatosFechaHoraReporte :"+e);
		}
		
	}

	@Action(value = "salvarInformacionAdicional", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORTEXTOLIBRE_JSP))
	public String salvarInformacionAdicional(){
		reporteCabinaService.salvarInformacionAdicional(idToReporte, informacionAdicional);
		return SUCCESS;
	}
	
	@Action(value = "salvarDeclaracionSiniestro", results = @Result(name = SUCCESS, location = LOCATION_CONTENEDORTEXTOLIBRE_JSP))
	public String salvarDeclaracionSiniestro(){
		reporteCabinaService.salvarDeclaracionSiniestro(idToReporte, declaracionTexto);
		return SUCCESS;
	}
	
	/**
	 * Manda a exportar en un documento PDF la información de la condición especial
	 * @return
	 */
	@Action(value="imprimirReporteCabina",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirReporteCabina(){
		try{
			transporte = reporteCabinaService.imprimirReporteCabina(idToReporte);
			transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
			transporte.setContentType("application/pdf");
			ReporteCabina reporteCabina = reporteCabinaService.buscarReporte(idToReporte);

			String fileName = 
				"Reporte " + reporteCabina.getNumeroReporte() + "_"	+ UtileriasWeb.getFechaString(new Date()) + ".pdf";
			transporte.setFileName(fileName);
		}
		catch(Exception e){

		}
		return SUCCESS;
	}
	
	@Action(value = "generarFechaHoraContacto", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^fechaHoraDTO.*"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^fechaHoraDTO.*"})
		})
	public String generarFechaHoraContacto(){	
		fechaHoraDTO = reporteCabinaService.generarFechaHora(idToReporte, TIPO_FECHA_HORA.CONTACTO,isEditable,convertirFecha(entidad.getFechaHoraContacto(),horaContacto));
		if(fechaHoraDTO != null && StringUtil.isEmpty(fechaHoraDTO.getDescripcion()) && !StringUtil.isEmpty(fechaHoraDTO.getHora()) &&
				!StringUtil.isEmpty(fechaHoraDTO.getFecha())){
			entidad.setId(idToReporte);
			enviarAMonitor(EnumTipoFechaCambio.ARRIBO);
		}
		return SUCCESS;
	}
	
	
	@Action(value = "validarExistenciaFechaHora", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^fechaHoraDTO.*"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^fechaHoraDTO.*"})
		})
	public String validarExistenciaFechaHora(){
		TIPO_FECHA_HORA tipo = null;
		if(!StringUtil.isEmpty(tipoFechaHora) && tipoFechaHora.equals("OCURRIDO")){		
				tipo = TIPO_FECHA_HORA.OCURRIDO;
		}
		if(tipo != null){
			fechaHoraDTO = reporteCabinaService.validarExistenciaFechaHora(idToReporte, tipo);
		}
		return SUCCESS;
	}
	
	@Action(value = "generarFechaHoraTermino", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^fechaHoraDTO.*"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^fechaHoraDTO.*"})
		})
	public String generarFechaHoraTermino(){	
		fechaHoraDTO = reporteCabinaService.generarFechaHora(idToReporte, TIPO_FECHA_HORA.TERMINACION, isEditable,convertirFecha(entidad.getFechaHoraTerminacion(),horaTerminacion));
		if(fechaHoraDTO != null && StringUtil.isEmpty(fechaHoraDTO.getDescripcion()) && !StringUtil.isEmpty(fechaHoraDTO.getHora()) &&
				!StringUtil.isEmpty(fechaHoraDTO.getFecha())){
			entidad.setId(idToReporte);
			enviarAMonitor(EnumTipoFechaCambio.TERMINO);
		}
		return SUCCESS;
	}
	
	public void validateInfoDate(){
		DateFormat fechaHora	= new SimpleDateFormat("dd/MM/yyyy HH:mm");
		DateFormat fecha 		= new SimpleDateFormat("dd/MM/yyyy");
		String fechaCompleta  		= "";
		
		try {
				if(entidad.getFechaHoraReporte() != null && SystemCommonUtils.isValid(this.horaReporte)){
					fechaCompleta = fecha.format( entidad.getFechaHoraReporte()) + " " + this.horaReporte;
					entidad.setFechaHoraReporte( fechaHora.parse(fechaCompleta) );	
				}
				if( entidad.getFechaHoraAsignacion() != null && SystemCommonUtils.isValid(this.horaAsignacion)){
					fechaCompleta = fecha.format( entidad.getFechaHoraAsignacion()) + " " + this.horaAsignacion;
					entidad.setFechaHoraAsignacion( fechaHora.parse(fechaCompleta) );	
				}
				
				if( entidad.getFechaHoraContacto() != null && SystemCommonUtils.isValid(this.horaContacto)){
					fechaCompleta = fecha.format( entidad.getFechaHoraContacto()) + " " + this.horaContacto;
					entidad.setFechaHoraContacto( fechaHora.parse(fechaCompleta) );	
				}
				
				if( entidad.getFechaHoraTerminacion() != null && SystemCommonUtils.isValid(this.horaTerminacion)){
					fechaCompleta = fecha.format( entidad.getFechaHoraTerminacion()) + " " + this.horaTerminacion;
					entidad.setFechaHoraTerminacion( fechaHora.parse(fechaCompleta) );	
				}
			
			} catch (ParseException e) {

			}
	}
	
	@Action(value = "cerrarVentanaReporte", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORREPORTECABINA_JSP),	
			@Result(name = "LISTADO", type = "redirectAction", params = {
					"actionName", "mostrarListadoReportes", 
					"namespace",  "/siniestros/cabina/reportecabina"}),
			@Result(name = "MONITOR", type = "redirectAction", params = {
					"actionName", "mostrarMonitor", 
					"namespace",  "/siniestros/cabina/monitor"}) })
	public String cerrarVentanaReporte(){
		String result = SUCCESS;	
		if (ServletActionContext.getContext().getSession().get(VENTANA_ORIGEN_REPORTE_CABINA) != null) {
			ventanaOrigen = (String) ServletActionContext.getContext().getSession().get(VENTANA_ORIGEN_REPORTE_CABINA);
			
			if (ventanaOrigen != null ) {
				if (ventanaOrigen.equals("LR") || ventanaOrigen.equals("MN")) {
					result = "LISTADO";
				} else if (ventanaOrigen.equals("MR")) {
					result = "MONITOR";
				} 				
			}
		}		
		return result;
	}
	
	@Action(value = "validarPendientesReporte", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estatusPendienteReporte,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estatusPendienteReporte,mensaje"})
		})	
	public String validarPendientesReporte(){
		try{
			siniestroService.validarCambioEstatus(idToReporte);	
			estatusPendienteReporte = 0;
		}catch (NegocioEJBExeption ex){
			log.error(ex.getMessageClean());
			if (ex.getErrorCode().equals("NE_O")) {
				super.setMensajeError(ex.getMessageClean());
				estatusPendienteReporte = 1;
			} else if (ex.getErrorCode().equals("NE_R")) {
				super.setMensajeError(ex.getMessageClean());
				estatusPendienteReporte = 2;
			}
		}
		return SUCCESS;
	}
	
	@Action (value = "reaperturar", results = { 
			@Result(name = SUCCESS,  type = "redirectAction", params = {
					"actionName", "mostrarBuscarReporte", "namespace","/siniestros/cabina/reportecabina",
					"idToReporte", "${idToReporte}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name = INPUT, location = LOCATION_CONTENEDORREPORTECABINA_JSP) })
	public String reaperturarReporte() {	
					
		siniestroService.reaperturarReporte(idToReporte);		
		setMensaje("Se ha reaperturado el Reporte");
		setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		return SUCCESS;
		
	}
	
	@Action(value = "validarVigenciaPoliza", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^detalleInciso.*,incisoAutorizado"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^detalleInciso.*,incisoAutorizado,mensaje"})
		})
	public String validarVigenciaPoliza(){	
		//CARGANDO LA INFORMACION DE LA VIGENCIA DE LA POLIZA DEL REPORTE
		try{
		detalleInciso = polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(siniestroDTO.getReporteCabinaId());
		if (detalleInciso.getEstatus() == EstatusVigenciaInciso.NOVIGENTE.ordinal()
				|| (detalleInciso.getEstatus() == EstatusVigenciaInciso.CANCELADO.ordinal()
						&& detalleInciso.getMotivo().equals(MOTIVO_FALTA_PAGO ))  ) {
			ReporteCabina entidad = reporteCabinaService.buscarReporte(siniestroDTO.getReporteCabinaId());
			incisoAutorizado = polizaSiniestroService.estatusIncisoAutorizacion(entidad.getPoliza().getIdToPoliza().longValue(),
			  entidad.getId(), detalleInciso.getNumeroInciso(), null);
		}
		return SUCCESS;
		}catch(Exception ex){
			setMensajeError("Error al validar la vigencia de la pliza");
			detalleInciso = null;
			incisoAutorizado = null;
			return INPUT;
		}
	}
	
	@Action (value = "solicitarAutorizacionVigencia", results = { 
			@Result(name = SUCCESS,  type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace","/siniestros/cabina/siniestrocabina",
					"siniestroDTO.reporteCabinaId", "${siniestroDTO.reporteCabinaId}",
					"soloConsulta", "${soloConsulta}",
					"validOnMillis", "${validOnMillis}",
					"recordFromMillis", "${recordFromMillis}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}),
			@Result(name = INPUT, location = LOCATION_CONTENEDORREPORTECABINA_JSP) })
	public String solicitarAutorizacionVigencia() {	
		
		ReporteCabina reporte = reporteCabinaService.buscarReporte(entidad.getId());
		
		if (reporte != null) {
			
			polizaSiniestroService.enviarSolicitudAutorizacionVigencia(reporte.getPoliza().getIdToPoliza().longValue(),
					new BigDecimal(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso()), reporte.getId(), (short) 1);		
			setMensaje("Se enviado solicitud de autorización por vigencia");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		}
					
		
		return SUCCESS;
		
	}
	

	/**
	 * ********************************************************************************************************************************
	 * **************************************************** GET & SET ***************************************************************
	 * ********************************************************************************************************************************
	 */

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the entidad
	 */
	public ReporteCabina getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(ReporteCabina entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the zonas
	 */
	public Map<String, String> getZonas() {
		return zonas;
	}

	/**
	 * @param zona the zona to set
	 */
	public void setZonas(Map<String, String> zonas) {
		this.zonas = zonas;
	}

	public String getInformacionAdicional() {
		return informacionAdicional;
	}

	public void setInformacionAdicional(String informacionAdicional) {
		this.informacionAdicional = informacionAdicional;
	}

	public String getDeclaracionTexto() {
		return declaracionTexto;
	}

	public void setDeclaracionTexto(String declaracionTexto) {
		this.declaracionTexto = declaracionTexto;
	}

	public String getValueTextoLibre() {
		return valueTextoLibre;
	}

	public void setValueTextoLibre(String valueTextoLibre) {
		this.valueTextoLibre = valueTextoLibre;
	}

	/**
	 * @return the tipoCarretera
	 */
	public Map<String, String> getTipoCarreteras() {
		return tipoCarreteras;
	}

	/**
	 * @param tipoCarretera the tipoCarretera to set
	 */
	public void setTipoCarreteras(Map<String, String> tipoCarreteras) {
		this.tipoCarreteras = tipoCarreteras;
	}

	/**
	 * @return the tituloVentana
	 */
	public String getTituloVentana() {
		return tituloVentana;
	}

	/**
	 * @param tituloVentana the tituloVentana to set
	 */
	public void setTituloVentana(String tituloVentana) {
		this.tituloVentana = tituloVentana;
	}

	/**
	 * @param tipoLugar the tipoLugar to set
	 */
	public void setTipoLugar(String tipoLugar) {
		this.tipoLugar = tipoLugar;
	}

	/**
	 * @return the tipoLugar
	 */
	public String getTipoLugar() {
		return tipoLugar;
	}

	/**
	 * @return the lugarAtencionOcurrido
	 */
	public LugarSiniestroMidas getLugarAtencionOcurrido() {
		return lugarAtencionOcurrido;
	}

	/**
	 * @param lugarAtencionOcurrido the lugarAtencionOcurrido to set
	 */
	public void setLugarAtencionOcurrido(LugarSiniestroMidas lugarAtencionOcurrido) {
		this.lugarAtencionOcurrido = lugarAtencionOcurrido;
	}

	/**
	 * @return the nombreReporta
	 */
	public String getNombreReporta() {
		return nombreReporta;
	}

	/**
	 * @param nombreReporta the nombreReporta to set
	 */
	public void setNombreReporta(String nombreReporta) {
		this.nombreReporta = nombreReporta;
	}

	/**
	 * @return the telLadaReporta
	 */
	public String getTelLadaReporta() {
		return telLadaReporta;
	}

	/**
	 * @param telLadaReporta the telLadaReporta to set
	 */
	public void setTelLadaReporta(String telLadaReporta) {
		this.telLadaReporta = telLadaReporta;
	}

	/**
	 * @return the telefonoReporta
	 */
	public String getTelefonoReporta() {
		return telefonoReporta;
	}

	/**
	 * @param telefonoReporta the telefonoReporta to set
	 */
	public void setTelefonoReporta(String telefonoReporta) {
		this.telefonoReporta = telefonoReporta;
	}

	public Long getIdToReporte() {
		return idToReporte;
	}

	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}

	public CitaReporteCabina getCita() {
		return cita;
	}

	public void setCita(CitaReporteCabina cita) {
		this.cita = cita;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public Map<Long, String> getOficinasActivas() {
		return oficinasActivas;
	}

	public void setOficinasActivas(Map<Long, String> oficinasActivas) {
		this.oficinasActivas = oficinasActivas;
	}

	public String getAtendidoPor() {
		return atendidoPor;
	}

	public void setAtendidoPor(String atendidoPor) {
		this.atendidoPor = atendidoPor;
	}

	public String getNsOficina() {
		return nsOficina;
	}

	public void setNsOficina(String nsOficina) {
		this.nsOficina = nsOficina;
	}

	public String getNsConsecutivoR() {
		return nsConsecutivoR;
	}

	public void setNsConsecutivoR(String nsConsecutivoR) {
		this.nsConsecutivoR = nsConsecutivoR;
	}

	public String getNsAnio() {
		return nsAnio;
	}

	public void setNsAnio(String nsAnio) {
		this.nsAnio = nsAnio;
	}

	public String getEstatusReporte() {
		return estatusReporte;
	}

	public void setEstatusReporte(String estatusReporte) {
		this.estatusReporte = estatusReporte;
	}

	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}

	public IncisoSiniestroDTO getDetalleInciso() {
		return detalleInciso;
	}

	public void setDetalleInciso(IncisoSiniestroDTO detalleInciso) {
		this.detalleInciso = detalleInciso;
	}
	
	

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public Boolean getIsConfirmarUbicacion() {
		return isConfirmarUbicacion;
	}

	public void setIsConfirmarUbicacion(Boolean isConfirmarUbicacion) {
		this.isConfirmarUbicacion = isConfirmarUbicacion;
	}

	public String getHoraReporte() {
		return horaReporte;
	}

	public void setHoraReporte(String horaReporte) {
		this.horaReporte = horaReporte;
	}

	public String getHoraAsignacion() {
		return horaAsignacion;
	}
	
	public String getIsEditable() {
		return isEditable;
	}

	public void setIsEditable(String isEditable) {
		this.isEditable = isEditable;
	}

	public void setHoraAsignacion(String horaAsignacion) {
		this.horaAsignacion = horaAsignacion;
	}

	public String getHoraContacto() {
		return horaContacto;
	}

	public void setHoraContacto(String horaContacto) {
		this.horaContacto = horaContacto;
	}

	public String getHoraTerminacion() {
		return horaTerminacion;
	}

	public void setHoraTerminacion(String horaTerminacion) {
		this.horaTerminacion = horaTerminacion;
	}	

	
	private void setInfoHora(){
		DateFormat sdf = new SimpleDateFormat("HH:mm");
		
		if(entidad.getFechaHoraReporte() != null){
			this.horaReporte = sdf.format( entidad.getFechaHoraReporte() );
		}
		
		if(entidad.getFechaHoraAsignacion() != null){
			this.horaAsignacion = sdf.format( entidad.getFechaHoraAsignacion() );
		}
		
		if(entidad.getFechaHoraContacto() != null){
			this.horaContacto = sdf.format( entidad.getFechaHoraContacto() );
		}
		
		if(entidad.getFechaHoraTerminacion() != null){
			this.horaTerminacion = sdf.format( entidad.getFechaHoraTerminacion() );
		}
	}

	public LugarSiniestroMidas getLugarAtencion() {
		return lugarAtencion;
	}

	public void setLugarAtencion(LugarSiniestroMidas lugarAtencion) {
		this.lugarAtencion = lugarAtencion;
	}

	public Double getLatitudLugar() {
		return latitudLugar;
	}

	public void setLatitudLugar(Double latitudLugar) {
		this.latitudLugar = latitudLugar;
	}

	public Double getLongitudLugar() {
		return longitudLugar;
	}

	public void setLongitudLugar(Double longitudLugar) {
		this.longitudLugar = longitudLugar;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
	/**
	 * @return the validacionFechaHoraOcurrido
	 */
	public boolean isValidacionFechaHoraOcurrido() {
		return validacionFechaHoraOcurrido;
	}

	/**
	 * @param validacionFechaHoraOcurrido the validacionFechaHoraOcurrido to set
	 */
	public void setValidacionFechaHoraOcurrido(boolean validacionFechaHoraOcurrido) {
		this.validacionFechaHoraOcurrido = validacionFechaHoraOcurrido;
	}

	public FechaHoraDTO getFechaHoraDTO() {
		return fechaHoraDTO;
	}

	public void setFechaHoraDTO(FechaHoraDTO fechaHoraDTO) {
		this.fechaHoraDTO = fechaHoraDTO;
	}

	public String getTipoFechaHora() {
		return tipoFechaHora;
	}

	public void setTipoFechaHora(String tipoFechaHora) {
		this.tipoFechaHora = tipoFechaHora;
	}

	public Boolean getVieneBandejaSolicitudAntiguedad() {
		return vieneBandejaSolicitudAntiguedad;
	}

	public void setVieneBandejaSolicitudAntiguedad(
			Boolean vieneBandejaSolicitudAntiguedad) {
		this.vieneBandejaSolicitudAntiguedad = vieneBandejaSolicitudAntiguedad;
	}

	

	public String getMensajeParaCrearPasesDeAtencion() {
		return mensajeParaCrearPasesDeAtencion;
	}

	public void setMensajeParaCrearPasesDeAtencion(
			String mensajeParaCrearPasesDeAtencion) {
		this.mensajeParaCrearPasesDeAtencion = mensajeParaCrearPasesDeAtencion;
	}

	public String getCausaSiniestro() {
		return causaSiniestro;
	}

	public void setCausaSiniestro(String causaSiniestro) {
		this.causaSiniestro = causaSiniestro;
	}
	
	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getTipoResonsabilidad() {
		return tipoResonsabilidad;
	}

	public void setTipoResonsabilidad(String tipoResonsabilidad) {
		this.tipoResonsabilidad = tipoResonsabilidad;
	}

	public String getTerminoDeAjuste() {
		return terminoDeAjuste;
	}

	public void setTerminoDeAjuste(String terminoDeAjuste) {
		this.terminoDeAjuste = terminoDeAjuste;
	}

	public String getVentanaOrigen() {
		return ventanaOrigen;
	}

	public void setVentanaOrigen(String ventanaOrigen) {
		this.ventanaOrigen = ventanaOrigen;
	}

	public String getEstatusVigenciaInciso() {
		return estatusVigenciaInciso;
	}

	public void setEstatusVigenciaInciso(String estatusVigenciaInciso) {
		this.estatusVigenciaInciso = estatusVigenciaInciso;
	}

	public boolean isDespliegaMensajeExito() {
		return despliegaMensajeExito;
	}

	public void setDespliegaMensajeExito(boolean despliegaMensajeExito) {
		this.despliegaMensajeExito = despliegaMensajeExito;
	}

	public Boolean getNoEditableParaRol() {
		return noEditableParaRol;
	}

	public void setNoEditableParaRol(Boolean noEditableParaRol) {
		this.noEditableParaRol = noEditableParaRol;
	}

	public Boolean getVieneExpedienteJuridico() {
		return vieneExpedienteJuridico;
	}

	public void setVieneExpedienteJuridico(Boolean vieneExpedienteJuridico) {
		this.vieneExpedienteJuridico = vieneExpedienteJuridico;
	}

	public ExpedienteJuridico getExpedienteJuridico() {
		return expedienteJuridico;
	}

	public void setExpedienteJuridico(ExpedienteJuridico expedienteJuridico) {
		this.expedienteJuridico = expedienteJuridico;
	}

	public Boolean getModoConsultaExpediente() {
		return modoConsultaExpediente;
	}

	public void setModoConsultaExpediente(Boolean modoConsultaExpediente) {
		this.modoConsultaExpediente = modoConsultaExpediente;
	}

	public int getEstatusPendienteReporte() {
		return estatusPendienteReporte;
	}

	public void setEstatusPendienteReporte(int estatusPendienteReporte) {
		this.estatusPendienteReporte = estatusPendienteReporte;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getCoordenadasConfirmadas() {
		return coordenadasConfirmadas;
	}

	public void setCoordenadasConfirmadas(Boolean coordenadasConfirmadas) {
		this.coordenadasConfirmadas = coordenadasConfirmadas;
	}

	public Short getIncisoAutorizado() {
		return incisoAutorizado;
	}

	public void setIncisoAutorizado(Short incisoAutorizado) {
		this.incisoAutorizado = incisoAutorizado;
	}

	public SiniestroCabinaDTO getSiniestroDTO() {
		return siniestroDTO;
	}

	public void setSiniestroDTO(SiniestroCabinaDTO siniestroDTO) {
		this.siniestroDTO = siniestroDTO;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}
	
	
}
