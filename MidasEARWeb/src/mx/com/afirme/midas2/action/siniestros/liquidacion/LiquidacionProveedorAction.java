package mx.com.afirme.midas2.action.siniestros.liquidacion;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.InformacionBancaria;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.CentroOperacionLiquidacionView;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro.OrigenLiquidacion;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionElementoDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.DetalleOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService.LiquidacionSiniestroFiltro;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService;
import mx.com.afirme.midas2.service.siniestros.pagos.notasDeCredito.NotasCreditoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


/**
 * Action que maneja todas la peticiones relacionadas con el m�dulo de pagos.
 * @author usuario
 * @version 1.0
 * @created 22-sep-2014 05:36:39 p.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/liquidacion/liquidacionSiniestro")

public class LiquidacionProveedorAction  extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -904741320121443575L;

	private final String LISTADOORDENESPAGO =	"/jsp/siniestros/liquidacion/listadoOrdenesPagoLiquidar.jsp";
	private final String LISTADOORDENESPAGODETALLE =	"/jsp/siniestros/liquidacion/listadoOrdenesPagoLiquidarDetalle.jsp";
	
	private final String CONTENEDOR_DETALLE_FACTURA =	"/jsp/siniestros/liquidacion/detalleOrdenCompra.jsp";
	private final String CONTENEDOR_DETALLE_NOTA_CREDITO =	"/jsp/siniestros/liquidacion/contenedorDetalleNotaCredito.jsp";
	
	
	private final String LISTADOORDENEASOCIADAS =	"/jsp/siniestros/liquidacion/listadoOrdenesPagoAsociadas.jsp";
	private final String LISTADOLIQUIDACIONES =	"/jsp/siniestros/liquidacion/listadoLiquidaciones.jsp";

	private final String CONTENEDOR_LIQDETALLE="/jsp/siniestros/liquidacion/contenedorLiquidacionDetalle.jsp";
	private final String CONTENEDOR_LIQUIDACION="/jsp/siniestros/liquidacion/contenedorLiquidacion.jsp";

	private final String CONTENEDOR_BUSQUEDA_LIQUIDACION_AUT_PROVEEDOR="/jsp/siniestros/liquidacion/contenedorLiquidacionEgresoAutorizacion.jsp";     // JCV
	private final String CONTENEDOR_BUSQUEDA_LIQUIDACION_PROVEEDOR="/jsp/siniestros/liquidacion/contenedorLiquidacionEgresoProveedor.jsp"; // JCV
	private final String CONTENEDOR_BUSQUEDA_AUT_GRID="/jsp/siniestros/liquidacion/contenedorLiquidacionAutorizacionGrid.jsp"; // JCV
	private final String CONTENEDOR_BUSQUEDA_GRID="/jsp/siniestros/liquidacion/contenedorLiquidacionGrid.jsp"; // JCV
	private final String CONTENEDOR_LIQUIDACIONES_INFO_BANCARIA="/jsp/siniestros/liquidacion/contenedorLiquidacionInfoBancaria.jsp"; // JCV
	private final String CONTENEDOR_LIQUIDACIONES_INFO_BANCARIA_GRID="/jsp/siniestros/liquidacion/contenedorLiquidacionInfoBancariaGrid.jsp"; // JCV
	private final String CONTENEDOR_CANCELACION_LIQUIDACION="/jsp/siniestros/liquidacion/contenedorCancelarLiquidacion.jsp"; // JCV	
	
	private final String CONTENEDOR_ORDENES_COMPRA_LIQUIDACION="/jsp/siniestros/liquidacion/listadoOrdenesCompraGrid.jsp"; 
	private final String CONTENEDOR_NOTAS_CREDITO_ASOCIADAS="/jsp/siniestros/liquidacion/listadoNotasCreditoAsociadasHeaderGrid.jsp";
	private final String CONTENEDOR_NOTAS_CREDITO_PENDIENTES="/jsp/siniestros/liquidacion/listadoNotasCreditoPendientesHeaderGrid.jsp";
	private final String CONTENEDOR_CIFRAS_RECUPERACIONES="/jsp/siniestros/liquidacion/listadoCifrasRecuperacionesGrid.jsp";
	
	
	
	
	
	
	private Long idPrestador; // JCV
	private Long idOrdenCompra; 
	private Long idOrdenPago;
	private Long idLiqSiniestro;
	private String estatusLiquidacion;
	
	private List<DocumentoFiscal> listaFacturasOrdenesPago;
	private List<FacturaLiquidacionDTO> listaOrdenesPago;
	private List<NotasCreditoRegistro> listaNotasCredito;
	private List<Map<String, Object>> listaPrestadoresServicio;	
	private List<DocumentoFiscal> listaFacturasOrdenesPagoAsociadas;
	
	private List<LiquidacionElementoDTO> listaElementosDisponibles;
	private List<LiquidacionElementoDTO> listaElementosAsociados;
	private String tipoElemento;
	
	private List<CentroOperacionLiquidacionView> listaCentrosOperacion;
	private String nombrePrestadorServicio;
	List<OrdenPagoSiniestro> ordenesPagoSeleccionadas;
	
	private Long idFactura;

	private Boolean soloLectura;
	private Boolean proveedorTieneInfoBancaria;
	
	//IMPRESION DE LIQUIDACION
	private Boolean esPreview;
	private String contentDispositionConfig;
	private Boolean esImprimible;
	private String claveNegocio;
	private String pantallaOrigen;	
	private static enum CONTENT_DISPOSITION_CONFIGURATION{
		attachment, inline;
	}
	
	//PARAMETROS DE AUTORIZACION
	private Boolean usuarioAutorizador;
	
	//CANCELACION DE LIQUIDACION
	private Short tipoCancelacion;
	private Short envioCancelacion;
	
	//IMPRESION DE REPORTE DE LIQUIDACION
	private TransporteImpresionDTO transporte;
	
	/**
	 * Listado de detalles de ordenes de pago
	 */
	private List<DesglosePagoConceptoDTO> desglosePagoConceptoDTO;
	/**
	 * bandera para saber si se entra a la pantalla de Orden de pago como
	 * captura/edicion/consulta
	 */
	private String modoPantalla;
	private OrdenPagoSiniestro ordenPago;
	private LiquidacionSiniestro liquidacionSiniestro;
	private DetalleOrdenCompra detalleOrdenPago;
	/**
	 * DTO  para transporte de la informaci�n del footer de la pantalla de orden de
	 * pago, donde se muestran los totales.
	 * 
	 * 
	 * 
	 */
	private DesglosePagoConceptoDTO totales;
	 List<DetalleOrdenCompraDTO> listaDetalleDTO;
	
	List<OrdenPagosDTO> listadoOrdenPagos;
	List<OrdenPagosDTO> listadoLiquidacionesAsociadas;
	
	LiquidacionSiniestroFiltro filtroLiquidaciones;
	List<LiquidacionSiniestroRegistro> listadoLiquidaciones;

	DocumentoFiscal factura;
	
	private Map<Long,String> oficinas;
	private Map<String,String> terminoAjusteMap;
	private Map<String,String> tipoOrdenCompraMap;
	
	private Map<String,String> motivoCancelacionMap;

	private Map<String,String> pagoaMap;
	private Map<String,String> tipoPagoMap;

	private Map<String,String> tipoOrdenPagoMap;
	private Map<String,String> estautsMap;
	private Map<String,String> tipoProveedorMap;
	private Map<Long,String> conceptoPagoMap;


	// JCV
	private Map<String,String> ctgLiquidaciones;
	private Map<String,String> ctgTipoOperaciones;
	private Map<String,String> ctgEstatusLiquidaciones;
	private Map<String,String> ctgCriterioComparacion;	
	private List<InformacionBancaria> listInfoBancaria;
	private PrestadorServicio    prestadorDeServicio;	
	private Map<String, String>  estatus;
	private String               proveedorEsCompania; // s ES COMPAÑIA, n NO ES COMPAÑIA
	
	
	private String facturasConcat;
	
	
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("conceptoAjusteServiceEJB")
	private ConceptoAjusteService conceptoAjusteService;
	
	@Autowired
	@Qualifier("ordenCompraServiceEJB")
	private OrdenCompraService ordenCompraService;
	
	@Autowired
	@Qualifier("pagosSiniestroServiceEJB")
	private PagosSiniestroService pagosSiniestroService;
	
	@Autowired
	@Qualifier("bloqueOrdenPagoEJB")
	private BloqueoPagoService bloqueoPagoService;
	
	@Autowired
	@Qualifier("liquidacionSiniestroServiceEJB")
	private LiquidacionSiniestroService liquidacionSiniestroService;
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService catalogoGrupoValorService;
	

	@Autowired
	@Qualifier("notasCreditoServiceEJB")
	private NotasCreditoService notaCreditoService;
		
	// JCV
	private void cargaCatalogos(){
		ctgLiquidaciones        = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO); // Cheque y Transferencia Bancaria
		ctgTipoOperaciones      = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_OPERACION); // no aplica,otros,servicios profesionales
		
		ctgEstatusLiquidaciones = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_LIQUIDACION_SINIESTRO); //Aplicada, Liberada con Solicitud,Liberada sin Solicitud
		ctgEstatusLiquidaciones.remove(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE_POR_MIZAR.getValue()); //No aplica este estatus para este tipo de liquidacion.
		
		ctgCriterioComparacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CRITERIO_COMPARACION); // mayor,menor,igual
		oficinas                = listadoService.obtenerOficinasSiniestros();
	}
	
	private Map<String,String> listaTiposLiquidacion;
	public List<DesglosePagoConceptoDTO> ordenesCompraDesglose;
	
	// JCV 
	/**
	 * Busqueda principal, se carga pantalla al ingresar desde menu
	 */
	@Action(value="mostrarBusquedaAutorizacionLiquidacionProveedor",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_AUT_PROVEEDOR),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_AUT_PROVEEDOR)
			})
	public String mostrarBusquedaAutorizacionLiquidacionProveedor(){	
		this.cargaCatalogos();
		return SUCCESS;	
	}
	
	
	// JCV 
	/**
	 * Busqueda principal, se carga pantalla al ingresar desde menu
	 */
	@Action(value="mostrarBusquedaLiquidacionProveedor",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_PROVEEDOR),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_PROVEEDOR)
			})
	public String mostrarBusquedaLiquidacionProveedor(){	
		this.cargaCatalogos();
		return SUCCESS;	
	}			

	@Action(value="asociarOrdenPago",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento,facturasConcat," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String asociarOrdenPago(){

		try{			
			liquidacionSiniestro = liquidacionSiniestroService.procesarFacturaSeleccionadaSP(liquidacionSiniestro.getFacturasConcat(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO);
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}

	@Action(value="desasociarOrdenPago",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje,facturasConcat," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String desasociarOrdenPago(){

		try{
			liquidacionSiniestro = liquidacionSiniestroService.procesarFacturaSeleccionadaSP(liquidacionSiniestro.getFacturasConcat(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO);
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}
	
	@Action(value="recalcularLiquidacion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String recalcularLiquidacion(){

		try{
			
			/*
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			liquidacionSiniestroService.recalcularMontosLiquidacion(liquidacionSiniestro);
			liquidacionSiniestro = entidadService.save(liquidacionSiniestro);
			*/
			
			//liquidacionSiniestroService.recalcularMontosLiquidacionSP(liquidacionSiniestro.getId());
			if(liquidacionSiniestro != null &&liquidacionSiniestro.getId()!= null  ){
				List<LiquidacionSiniestro> liquidaciones = entidadService.findByProperty(LiquidacionSiniestro.class, "id", liquidacionSiniestro.getId());
				liquidacionSiniestro = liquidaciones.get(0);
			}
			//liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}
	
	@Action(value="reexpedir",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaLiquidacionProveedor", 
			"namespace", "/siniestros/liquidacion/liquidacionSiniestro", 
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name=INPUT,location=CONTENEDOR_LIQDETALLE)
			})			
	public String reexpedir(){	
		
		try{
			this.liquidacionSiniestroService.reexpedir(liquidacionSiniestro.getId());
			this.setMensajeExito();
			
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return INPUT;			
		}			

		return SUCCESS;
	}	
	
	/**
	 * muestra lista vacia 
	 */
	@Action(value="mostrarListaVacia",results={
			@Result(name=SUCCESS,location=LISTADOLIQUIDACIONES),
			@Result(name=INPUT,location=LISTADOLIQUIDACIONES)
			})
	public String mostrarListaVacia(){
		this.listadoLiquidaciones=	 new ArrayList<LiquidacionSiniestroRegistro>();
		return SUCCESS;
	}
	
	
	/**
	 * muestra la pantalla de listado de ordenes de pago, debe traer todas las ordenes
	 * de compras autorizadas 
	 */
	@Action(value="mostrarOrdenesPagoAsociadas",results={
			@Result(name=SUCCESS,location=LISTADOORDENESPAGO),
			@Result(name=INPUT,location=LISTADOORDENESPAGO)
			})
	public String mostrarOrdenesPagoAsociadas(){		
		
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{
			listaFacturasOrdenesPago = liquidacionSiniestroService.
				buscarFacturasLiquidacion(liquidacionSiniestro.getId());			
		}		
		
		return SUCCESS;
	}
	
	
	@Action(value="mostrarOrdenesDeCompra",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENES_COMPRA_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_ORDENES_COMPRA_LIQUIDACION)
			})
	public String mostrarOrdenesDeCompra(){		
		
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{
			 this.ordenesCompraDesglose= liquidacionSiniestroService.obtenerPagosPorLiquidacion(liquidacionSiniestro.getId());			
		}		
		
		return SUCCESS;
	}
	
	
	@Action(value="mostrarCifrasRecuperaciones",results={
			@Result(name=SUCCESS,location=CONTENEDOR_CIFRAS_RECUPERACIONES),
			@Result(name=INPUT,location=CONTENEDOR_CIFRAS_RECUPERACIONES)
			})
	public String mostrarCifrasRecuperaciones(){		
		
		
		this.listaNotasCredito = new ArrayList<NotasCreditoRegistro>();
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{
			this.listaNotasCredito = this.notaCreditoService.obtenerRecuperacionesNotasDeCreditoPorLiquidacion(liquidacionSiniestro.getId());
		}
		return SUCCESS;
	}
	
	
	
	
	/**
	 * muestra la pantalla de listado de ordenes de pago, debe traer todas las ordenes
	 * de compras autorizadas 
	 */
	@Action(value="mostrarListaLiquidaciones",results={
			@Result(name=SUCCESS,location=LISTADOLIQUIDACIONES),
			@Result(name=INPUT,location=LISTADOLIQUIDACIONES)
			})
	public String mostrarListaLiquidaciones(){
		if(null!=this.filtroLiquidaciones){
			filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue());
			this.listadoLiquidaciones=this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);

		}else{
			this.listadoLiquidaciones=	 new ArrayList<LiquidacionSiniestroRegistro>();
		}
	  return SUCCESS;
	}
	
	
	

	/**
	 * muestra la pantalla de listado de ordenes de pago, debe traer todas las ordenes
	 * de compras autorizadas 
	 */
	@Action(value="mostrarListadoOrdenPago",results={
			@Result(name=SUCCESS,location=LISTADOORDENESPAGO),
			@Result(name=INPUT,location=LISTADOORDENESPAGO)
			})
	public String mostrarListadoOrdenPago(){		
		
		listaFacturasOrdenesPago = pagosSiniestroService.
			buscarFacturasLiquidacion(liquidacionSiniestro.getProveedor().getId() != null ?
					Long.valueOf(liquidacionSiniestro.getProveedor().getId().toString()):null);
		
	  return SUCCESS;
	}	
	
	@Action(value="verDetalleFactura",results={
			@Result(name=SUCCESS,location=LISTADOORDENESPAGODETALLE),
			@Result(name=INPUT,location=LISTADOORDENESPAGODETALLE)
			})
	public String verDetalleFactura()
	{  
		//proveedorEsCompania = "s";
		listaOrdenesPago = pagosSiniestroService.buscarOrdenesPagoFactura(idFactura);
		
		// SE USA EN EL GRID LA BANDERA isFacturaCero PARA MOSTRAR O NO LA CABECERA siniestroTercero
		if( !listaOrdenesPago.isEmpty() ) {
			if( listaOrdenesPago.get(0).getNumeroFactura().equals("0") ) {
				proveedorEsCompania = "s";
			}else {
				proveedorEsCompania = "n";
			}
		}
		
		return SUCCESS;
	}
	
	@Action(value="verDetalleFacturaOrdenCompra",results={
			@Result(name=SUCCESS,location=CONTENEDOR_DETALLE_FACTURA),
			@Result(name=INPUT,location=CONTENEDOR_DETALLE_FACTURA)
			})
	public String verDetalleFacturaOrdenCompra()
	{  
		return SUCCESS;
	}
	
	@Action(value="verCOntenedorDetalleNotaDeCredito",results={
			@Result(name=SUCCESS,location=CONTENEDOR_DETALLE_NOTA_CREDITO),
			@Result(name=INPUT,location=CONTENEDOR_DETALLE_NOTA_CREDITO)
			})
	public String verCOntenedorDetalleNotaDeCredito()
	{  
		return SUCCESS;
	}
	
	@Action(value="verDetalleNotaCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/listadoNotasCreditoAsociadasGrid.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/listadoNotasCreditoAsociadasGrid.jsp")
			})
	public String verDetalleNotaCredito()
	{  
		if(tipoElemento.equalsIgnoreCase("Factura"))
		{
			listaNotasCredito = notaCreditoService.obtenerNotasDeCreditoPorFactura(idFactura);	
		}else if(tipoElemento.equalsIgnoreCase("Recuperacion"))
		{
			listaNotasCredito = notaCreditoService.obtenerNotasDeCreditoPorRecuperacion(idFactura);	
		}
		return SUCCESS;
	}
	
	@Action(value="verContenedorDetalleNotaCredito",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/listadoDetalleNotaCredito.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/listadoDetalleNotaCredito.jsp")
			})
	public String verContenedorDetalleNotaCredito()
	{  
		return SUCCESS;
	}
	
	@Action(value="verContenedorNotaCredito",results={
			@Result(name=SUCCESS,location=CONTENEDOR_NOTAS_CREDITO_ASOCIADAS),
			@Result(name=INPUT,location=CONTENEDOR_NOTAS_CREDITO_ASOCIADAS)
			})
	public String verContenedorNotaCredito()
	{  
		
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null){
			listaNotasCredito = notaCreditoService.obtenerNotasDeCreditoPorLiquidacion(liquidacionSiniestro.getId());
		}else{
			listaNotasCredito = new ArrayList<NotasCreditoRegistro>();
		}
		return SUCCESS;
	}
	
	
	@Action(value="verRecuperacionesPendientes",results={
			@Result(name=SUCCESS,location=CONTENEDOR_NOTAS_CREDITO_PENDIENTES),
			@Result(name=INPUT,location=CONTENEDOR_NOTAS_CREDITO_PENDIENTES)
			})
	public String verRecuperacionesPendientes()
	{  
		
		listaElementosDisponibles = new ArrayList<LiquidacionElementoDTO>();
		
		List<RecuperacionProveedor> recuperaciones = 
			liquidacionSiniestroService.obtenerRecuperacionesPendientesPorAsignar(liquidacionSiniestro.getProveedor().getId());

		listaElementosDisponibles = this.convierteRecuperacionesAElementoSiniestroDTOList(recuperaciones);
		return SUCCESS;
	}
	
	
	
	public void prepareMostrarLiquidacion()
	{	
		listaTiposLiquidacion = new LinkedHashMap<String, String>();	
		
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{  
			liquidacionSiniestro = entidadService.evictAndFindById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());//TODO remover despues de las pruebas
			 
			if(liquidacionSiniestro.getProveedor() != null)
			{
				nombrePrestadorServicio = liquidacionSiniestro.getProveedor().getId() + " - " +liquidacionSiniestro.getProveedor().getPersonaMidas().getNombre();
				listaTiposLiquidacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);				
			}
		}
		else
		{			
			liquidacionSiniestro = new LiquidacionSiniestro();
			liquidacionSiniestro.setOrigenLiquidacion(OrigenLiquidacion.PROVEEDOR.getValue());
			liquidacionSiniestro = liquidacionSiniestroService.guardarLiquidacion(liquidacionSiniestro, 
						LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE);			
			  
		}
		
		liquidacionSiniestro.setDescripcionEstatus(catalogoGrupoValorService.
				obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_LIQUIDACION_SINIESTRO,
						liquidacionSiniestro.getEstatus()).getDescripcion());       
		
		listaPrestadoresServicio = liquidacionSiniestroService.buscarProveedoresLiquidacion(); //TODO modificar para filtrar los proveedores correctos
		ctgTipoOperaciones = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_OPERACION);
		//Opcion 'No aplica' es solo para liq. beneficiarios  
		ctgTipoOperaciones.remove(LiquidacionSiniestro.TipoOperacion.NO_APLICA.getValue());	
		
		listaCentrosOperacion = entidadService.findByPropertiesWithOrder(CentroOperacionLiquidacionView.class, null, "nombre");
		
	}
	

	/**
	 * Muestra la pantalla principal de captura de la orden de pago en modos
	 * captura/edicion/consulta
	 */
	@Action(value="mostrarLiquidacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_LIQDETALLE),
			@Result(name=INPUT,location=CONTENEDOR_LIQDETALLE)
			})
	public String mostrarLiquidacion(){
		
		if(!liquidacionSiniestro.getEstatus().
				equalsIgnoreCase(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE.getValue())
				&& !liquidacionSiniestro.getEstatus().
				equalsIgnoreCase(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE_POR_RECHAZO.getValue()))
		{
			soloLectura = Boolean.TRUE;				
		}
		else if(soloLectura == null)//Si no viene desde JSP entonces el default es mostrar en solo lectura
		{
			soloLectura = Boolean.FALSE;			
		}		

		return SUCCESS;
	}	
	
	@Action(value="obtenerTipoLiquidacionProveedor",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaTiposLiquidacion.*,^liquidacionSiniestro\\.tipoLiquidacion,proveedorTieneInfoBancaria"})
	})
	public String obtenerTipoLiquidacionProveedor()
	{		  
		proveedorTieneInfoBancaria = Boolean.FALSE;
		PrestadorServicio proveedor = entidadService.findById(PrestadorServicio.class,liquidacionSiniestro.getProveedor().getId() != null ?
				liquidacionSiniestro.getProveedor().getId():-1);		
		
		listaTiposLiquidacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		
		if(proveedor.getInformacionBancaria() != null && !proveedor.getInformacionBancaria().isEmpty())
		{
			for(InformacionBancaria infoBancaria:proveedor.getInformacionBancaria())
			{
				if(infoBancaria.getNumeroCuenta()!= null)
				{
					proveedorTieneInfoBancaria =Boolean.TRUE;
					break;
				}						
			}					
		}

		if(!proveedorTieneInfoBancaria)
		{
			listaTiposLiquidacion.remove(LiquidacionSiniestro.TipoLiquidacion.TRANSFERENCIA_BANCARIA.getValue());			
		}		
	
		return SUCCESS;
	}	
	
	public String prepareLiquidacion(){

		return SUCCESS;
	}
	
	public void prepareGuardarLiquidacionAutomatico()
	{
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{  
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
		}		
	}	
	
	@Action(value="guardarLiquidacionAutomatico",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})				
		})
	public String guardarLiquidacionAutomatico(){
		
		try{
		liquidacionSiniestro = liquidacionSiniestroService.guardarLiquidacion(liquidacionSiniestro);
		}
		catch(Exception e)
		{
			this.setMensajeError("Error al intentar guardar la liquidacion");
			e.printStackTrace();
		}
		
		this.setMensajeExito();
		
		return SUCCESS;		
	}
	
	
	public void prepareGuardarLiquidacion()
	{
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{  
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
		}		
	}
	
	/**
	 * recibe las peticiones de guardado de una orden de pago.
	 * 
	 * @param ordenPago
	 */
	@Action(value="guardarLiquidacion",results={ 
			@Result(name=SUCCESS,location=CONTENEDOR_LIQDETALLE),
			@Result(name=INPUT,location=CONTENEDOR_LIQDETALLE)
			})
	public String guardarLiquidacion(){
		
		liquidacionSiniestroService.guardarLiquidacion(liquidacionSiniestro, 
				LiquidacionSiniestro.EstatusLiquidacionSiniestro.get(estatusLiquidacion));

		return SUCCESS;
	}
	
	
	//-----------------------------
	
	/**
	 * cancela una orden depago, se cambia el estatus a cancelado.
	 */
	/*	@Action(value="aceptarLiquidacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_LIQDETALLE)
			})
	public String aceptarLiquidacion(){		

		return SUCCESS;
	}
*/	
	

	/**
	 * cancela una orden depago, se cambia el estatus a cancelado.
	 */
	@Action(value="cancelarLiquidacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_LIQUIDACION)
			})
	public String cancelarLiquidacion(){
		return SUCCESS;
	}
	
	/**
	 * cancela una orden depago, se cambia el estatus a cancelado.
	 */
	@Action(value="eliminarLiquidacion",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaLiquidacionProveedor", 
			"namespace", "/siniestros/liquidacion/liquidacionSiniestro", 
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name=INPUT,location=CONTENEDOR_LIQDETALLE)
			})
	public String eliminarLiquidacion(){
		 
		try {
			this.liquidacionSiniestroService.eliminarLiquidacion(liquidacionSiniestro.getId());
		} catch (Exception e) {
			this.setMensajeError("Ha ocurrido un error al eliminar la liquidación.");
			e.printStackTrace();
		}
		
		this.setMensajeExito();
		
		return SUCCESS;
	}	
	
	@Action(value = "imprimirOrdenExpedicionCheque", results = {
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","${contentDispositionConfig};filename=\"${transporte.fileName}\""})})
	public String imprimirOrdenExpedicionCheque(){
		transporte = liquidacionSiniestroService.imprimirOrdenExpedicionCheque(liquidacionSiniestro.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");
		if(esPreview != null 
				&& esPreview == true){ //Tiene que estar en TRUE para que despliegue el PDF en pantalla en vez de intentar descargarlo
			contentDispositionConfig = CONTENT_DISPOSITION_CONFIGURATION.inline.toString();
		}else{
			contentDispositionConfig = CONTENT_DISPOSITION_CONFIGURATION.attachment.toString();
		}
		
		String fileName = "OrdenExpedicionCheque_"+ "id" + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value = "mostrarImprimirLiquidacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/liquidacion/contenedorLiquidacionImpresion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/liquidacion/contenedorLiquidacionImpresion.jsp")})
	public String mostrarImprimirLiquidacion(){
		if(liquidacionSiniestro != null
				&& liquidacionSiniestro.getId() != null){
			esImprimible = liquidacionSiniestroService.validarImpresionLiquidacion(liquidacionSiniestro.getId());
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			if(liquidacionSiniestro.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue()) == 0){
				usuarioAutorizador = liquidacionSiniestroService.validarUsuarioEsAutorizador(liquidacionSiniestro.getId());
			}else{
				usuarioAutorizador = Boolean.FALSE;
			}
		}else{
			setMensajeError("Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir");
			return INPUT;
		}
		return SUCCESS;
	}

	
	@Override
	public void prepare() throws Exception {
		this.tipoPagoMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA);
		oficinas = listadoService.obtenerOficinasSiniestros();
		this.estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_LIQUIDACION);
		this.tipoOrdenPagoMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		this.tipoProveedorMap=  listadoService.getMapTipoPrestador();
		this.conceptoPagoMap=listadoService.getMapConceptosAjuste();
		//this.proveedorMap=new LinkedHashMap<Long, String>();
		this.terminoAjusteMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		this.motivoCancelacionMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);
		this.pagoaMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ORIGEN_PAGO);
		this.tipoOrdenCompraMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
			


	}
	
	@Action(value="solicitarAutorizacion",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaLiquidacionProveedor", 
			"namespace", "/siniestros/liquidacion/liquidacionSiniestro", 		
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarLiquidacion", 
					"namespace", "/siniestros/liquidacion/liquidacionSiniestro",
					"liquidacionSiniestro.id","${liquidacionSiniestro.id}",
					"soloLectura","${soloLectura}",
					"pantallaOrigen","${pantallaOrigen}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"})
			})  
	public String solicitarAutorizacion(){	
		  
		try{
		liquidacionSiniestroService.solicitarAutorizacion(liquidacionSiniestro.getId());
		} catch (Exception e) {
			this.setMensajeError(e.getMessage());
			e.printStackTrace();
			return INPUT;
		}
		
		this.setMensajeExito();
			
		return SUCCESS;	
	}	
	
	// JCV
	/**
	 * Método que realiza la búsqeda por medio de un filtro
	 */
	@Action(value="buscarLiquidacionAutorizacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_AUT_GRID),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_AUT_GRID)
			})
	public String buscarLiquidacionAutorizacion(){	
		
		// TEMPORAL CARGA DE CATALOGOS
		this.cargaCatalogos();
		
		if( this.filtroLiquidaciones == null ){
			// PRECARGA - DATOS
			listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(ctgEstatusLiquidaciones,LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue());
		}else{
			filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue());
			listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);
		}
		
		return SUCCESS;	
	}
	
	// JCV
	/**
	 * Método que realiza la búsqeda por medio de un filtro
	 */
	@Action(value="buscarLiquidacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_GRID),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_GRID)
			})
	public String buscarLiquidacion(){	
		
		this.cargaCatalogos();
		
		if( this.filtroLiquidaciones != null ){
			filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue());
			listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);
		}
		
		if( listadoLiquidaciones.isEmpty() ){
			super.setMensajeError(getText("midas.liquidaciones.busqueda.busqueda.error"));
			return INPUT;
		}
		
		return SUCCESS;	
	}
	
	// JCV
	@Action(value="exportarBusquedaExcel",
			results={@Result(name=SUCCESS,
					  type="stream",
					  params={"contentType","${transporte.contentType}",
						  "inputName","transporte.genericInputStream",
						  "contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String exportarBusquedaExcel(){	
		
		//Obtenemos el listado a exportar
		filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue());
		listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);
		
		if( listadoLiquidaciones != null ){
			ExcelExporter exporter = new ExcelExporter( LiquidacionSiniestroRegistro.class,"PROVEEDOR" );
			transporte = exporter.exportXLS(listadoLiquidaciones, "Liquidaciones Proveedor");
		}
		
		return SUCCESS;	
	}
	
	// JCV 
	@Action(value="mostrarDatosBancariosLiquidaciones",results={
			@Result(name=SUCCESS,location=CONTENEDOR_LIQUIDACIONES_INFO_BANCARIA),
			@Result(name=INPUT,location=CONTENEDOR_LIQUIDACIONES_INFO_BANCARIA)
			})
	public String mostrarDatosBancariosLiquidaciones(){
		return SUCCESS;
	}
	
	// JCV 
	@Action(value="gridDatosBancariosLiquidaciones",results={
			@Result(name=SUCCESS,location=CONTENEDOR_LIQUIDACIONES_INFO_BANCARIA_GRID),
			@Result(name=INPUT,location=CONTENEDOR_LIQUIDACIONES_INFO_BANCARIA_GRID)
			})
	public String gridDatosBancariosLiquidaciones(){
		
		if( this.idPrestador != null ){
			this.prestadorDeServicio = entidadService.findById( PrestadorServicio.class, Integer.parseInt( this.idPrestador.toString() ) );
			
			if( this.prestadorDeServicio.getInformacionBancaria() != null ){
				listInfoBancaria = prestadorDeServicio.getInformacionBancaria();
				this.loadBankName  (listInfoBancaria);
				this.loadStatusName(listInfoBancaria);
			}else{
				listInfoBancaria=new ArrayList<InformacionBancaria>();
			}
		}
		
		return SUCCESS;
	}
	
	// JCV -- TOMADO DE CatPrestadorServicio
	private void loadBankName(List<InformacionBancaria> listInfoBancaria){
		for(InformacionBancaria informacionBancaria: listInfoBancaria){
			informacionBancaria.setBankName(this.findBankName(informacionBancaria.getBancoId()));
		}
	}
	
	// JCV -- TOMADO DE CatPrestadorServicio
	private void loadStatusName(List<InformacionBancaria> listInfoBancaria){
		this.estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS);
		
		for(InformacionBancaria informacionBancaria: listInfoBancaria){
			informacionBancaria.setStatusName(this.estatus.get(informacionBancaria.getEstatus().toString()));
		}
	}
	
	// JCV -- TOMADO DE CatPrestadorServicio
	private String findBankName(Integer bankId){
		BancoMidas banco = entidadService.findById(BancoMidas.class, bankId.longValue());
		if(banco != null){
			return banco.getNombre();
		}
		return "BANCO DESCONOCIDO";
	}


	/*AIGG */

	@Action(value = "autorizarLiquidacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/liquidacion/contenedorLiquidacionImpresion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/liquidacion/contenedorLiquidacionImpresion.jsp")})
	public String autorizarLiquidacion(){
		
			try {
				liquidacionSiniestroService.enviarSolicitudLiquidacion(this.idLiqSiniestro);
			} catch (Exception e) {
				
				super.setMensajeError(e.getMessage());
				return INPUT;
			}
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			if(liquidacionSiniestro.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue()) == 0){
				usuarioAutorizador = liquidacionSiniestroService.validarUsuarioEsAutorizador(liquidacionSiniestro.getId());
			}else{
				usuarioAutorizador = Boolean.FALSE;
			}
			super.setMensajeExitoPersonalizado(getText("Liquidacion Autorizada"));
			return SUCCESS;
	}
	

			
	@Action(value = "rechazarLiquidacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/liquidacion/contenedorLiquidacionImpresion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/liquidacion/contenedorLiquidacionImpresion.jsp")})		
	public String rechazarLiquidacion(){
			this.idLiqSiniestro=liquidacionSiniestro.getId();
			this.liquidacionSiniestro = this.entidadService.findById(LiquidacionSiniestro.class , this.idLiqSiniestro);
		
			if (null==liquidacionSiniestro){
				super.setMensajeError(getText("midas.liquidaciones.busqueda.noLiquidacionNoExiste"));
				return INPUT;
			}
			this.liquidacionSiniestroService.rechazarLiquidacion(this.idLiqSiniestro)  ;
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			if(liquidacionSiniestro.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue()) == 0){
				usuarioAutorizador = liquidacionSiniestroService.validarUsuarioEsAutorizador(liquidacionSiniestro.getId());
			}else{
				usuarioAutorizador = Boolean.FALSE;
			}
			super.setMensajeExitoPersonalizado(getText("midas.liquidaciones.liquidacionRechazada"));
			return SUCCESS;
	}	
	
	@Action(value="mostrarCancelarLiquidacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_CANCELACION_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_CANCELACION_LIQUIDACION)
			})
	public String mostrarCancelarLiquidacion(){
		
		liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, idLiqSiniestro);
		
		return SUCCESS;
	}
	
	@Action(value="cancelacionLiquidacion",
			results = { @Result(name=SUCCESS,location=CONTENEDOR_CANCELACION_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_CANCELACION_LIQUIDACION)
			})
	public String cancelacionLiquidacion(){
		
		String resultado = null;
		Short result = liquidacionSiniestroService.cancelarLiquidacion(idLiqSiniestro, tipoCancelacion);
		
		switch(result) {
			case 0 :
				setMensaje("Se ha realizado la Cancelaci\u00F3n de la Liquidaci\u00F3n");
				this.envioCancelacion=1;
				resultado = SUCCESS;
				break;
			case 1 :
				setMensaje("No existe la Solicitud de cheque para esta Liquidaci\u00F3n");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			case 2 :
				setMensaje("No ha sido posible realizar la Cancelaci\u00F3n ya que existe un cheque impreso");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			case 3 :
				setMensaje("No ha sido posible realizar la Cancelaci\u00F3n ya que la Liquidaci\u00F3n esta Aplicada");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			case 4 :
				setMensaje("No ha sido posible realizar la Cancelaci\u00F3n");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
		}
		
		return resultado;
	}
	
	@Action(value="obtenerRecuperacionesPendientesAsignar",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/listadoElementosLiquidacionGrid.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/listadoElementosLiquidacionGrid.jsp")
			})
	public String obtenerRecuperacionesPendientesAsignar(){
		
		listaElementosDisponibles = new ArrayList<LiquidacionElementoDTO>();
		
		List<RecuperacionProveedor> recuperaciones = 
			liquidacionSiniestroService.obtenerRecuperacionesPendientesPorAsignar(liquidacionSiniestro.getProveedor().getId());

		listaElementosDisponibles = this.convierteRecuperacionesAElementoSiniestroDTOList(recuperaciones);
	 
	  return SUCCESS;
	}
	
	
	@Action(value="obtenerRecuperacionesAsociadas",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/listadoElementosLiquidacionGrid.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/listadoElementosLiquidacionGrid.jsp")
			})
	public String obtenerRecuperacionesAsociadas(){
		
		listaElementosDisponibles = new ArrayList<LiquidacionElementoDTO>();
		
		List<RecuperacionProveedor> recuperaciones = 
			liquidacionSiniestroService.obtenerRecuperacionesAsociadas(liquidacionSiniestro.getId());

		listaElementosDisponibles = this.convierteRecuperacionesAElementoSiniestroDTOList(recuperaciones);
	 
	  return SUCCESS;
	}
	
	@Action(value="asociarRecuperacion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String asociarRecuperacion(){

		try{			
			liquidacionSiniestro = liquidacionSiniestroService.procesarRecuperacionSeleccionada(factura.getId(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO);
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}
	
	
	@Action(value="asociarRecuperaciones",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String asociarRecuperaciones(){

		try{			
			liquidacionSiniestro = liquidacionSiniestroService.procesarRecuperacionesSeleccionadas(liquidacionSiniestro.getFacturasConcat(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO);
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}
	
	@Action(value="desasociarRecuperacion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String desasociarRecuperacion(){

		try{			
			liquidacionSiniestro = liquidacionSiniestroService.procesarRecuperacionSeleccionada(factura.getId(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO);
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}
	
	@Action(value="desasociarRecuperaciones",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento",
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String desasociarRecuperaciones(){

		try{			
			liquidacionSiniestro = liquidacionSiniestroService.procesarRecuperacionesSeleccionadas(liquidacionSiniestro.getFacturasConcat(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO);
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}
		
		return SUCCESS;
	}
	
	private List<LiquidacionElementoDTO> convierteRecuperacionesAElementoSiniestroDTOList(List<RecuperacionProveedor> recuperaciones)
	{
		List<LiquidacionElementoDTO> resultado = new ArrayList<LiquidacionElementoDTO>();
		
		for(RecuperacionProveedor recuperacion: recuperaciones)
		{
			LiquidacionElementoDTO liquidacionElemento = 
				new LiquidacionElementoDTO(recuperacion.getId(), String.valueOf(recuperacion.getNumero()), 
						recuperacion.getMontoTotal(),"1", "REC", "Recuperacion");
			
			resultado.add(liquidacionElemento);
		}
		
		
		return resultado;
	}
	
	private List<LiquidacionElementoDTO> convierteDocFiscalAElementoSiniestroDTOList(List<DocumentoFiscal> facturas)
	{
		List<LiquidacionElementoDTO> resultado = new ArrayList<LiquidacionElementoDTO>();
		
		for(DocumentoFiscal factura: facturas)
		{
			LiquidacionElementoDTO liquidacionElemento = 
				new LiquidacionElementoDTO(factura.getId(), String.valueOf(factura.getNumeroFactura()), 
						factura.getMontoTotal(),factura.getTieneNotasCredito(), "FAC", "Factura");
			
			resultado.add(liquidacionElemento);
		}
		
		return resultado;		
	}
	
	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ConceptoAjusteService getConceptoAjusteService() {
		return conceptoAjusteService;
	}

	public void setConceptoAjusteService(ConceptoAjusteService conceptoAjusteService) {
		this.conceptoAjusteService = conceptoAjusteService;
	}

	public OrdenCompraService getOrdenCompraService() {
		return ordenCompraService;
	}

	public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
		this.ordenCompraService = ordenCompraService;
	}

	public PagosSiniestroService getPagosSiniestroService() {
		return pagosSiniestroService;
	}

	public void setPagosSiniestroService(PagosSiniestroService pagosSiniestroService) {
		this.pagosSiniestroService = pagosSiniestroService;
	}
	public List<DesglosePagoConceptoDTO> getDesglosePagoConceptoDTO() {
		return desglosePagoConceptoDTO;
	}

	public void setDesglosePagoConceptoDTO(
			List<DesglosePagoConceptoDTO> desglosePagoConceptoDTO) {
		this.desglosePagoConceptoDTO = desglosePagoConceptoDTO;
	}

	public String getModoPantalla() {
		return modoPantalla;
	}

	public void setModoPantalla(String modoPantalla) {
		this.modoPantalla = modoPantalla;
	}
	public OrdenPagoSiniestro getOrdenPago() {
		return ordenPago;
	}
	public void setOrdenPago(OrdenPagoSiniestro ordenPago) {
		this.ordenPago = ordenPago;
	}
	public DetalleOrdenCompra getDetalleOrdenPago() {
		return detalleOrdenPago;
	}
	public void setDetalleOrdenPago(DetalleOrdenCompra detalleOrdenPago) {
		this.detalleOrdenPago = detalleOrdenPago;
	}

	public DesglosePagoConceptoDTO getTotales() {
		return totales;
	}

	public void setTotales(DesglosePagoConceptoDTO totales) {
		this.totales = totales;
	}
	
	public List<OrdenPagosDTO> getListadoOrdenPagos() {
		return listadoOrdenPagos;
	}

	public void setListadoOrdenPagos(List<OrdenPagosDTO> listadoOrdenPagos) {
		this.listadoOrdenPagos = listadoOrdenPagos;
	}	

	public Map<String, String> getTipoPagoMap() {
		return tipoPagoMap;
	}

	public void setTipoPagoMap(Map<String, String> tipoPagoMap) {
		this.tipoPagoMap = tipoPagoMap;
	}

	public Map<String, String> getTipoOrdenPagoMap() {
		return tipoOrdenPagoMap;
	}

	public void setTipoOrdenPagoMap(Map<String, String> tipoOrdenPagoMap) {
		this.tipoOrdenPagoMap = tipoOrdenPagoMap;
	}

	public Map<String, String> getEstautsMap() {
		return estautsMap;
	}

	public void setEstautsMap(Map<String, String> estautsMap) {
		this.estautsMap = estautsMap;
	}

	public Map<String, String> getTipoProveedorMap() {
		return tipoProveedorMap;
	}

	public void setTipoProveedorMap(Map<String, String> tipoProveedorMap) {
		this.tipoProveedorMap = tipoProveedorMap;
	}

	public Map<Long, String> getConceptoPagoMap() {
		return conceptoPagoMap;
	}

	public void setConceptoPagoMap(Map<Long, String> conceptoPagoMap) {
		this.conceptoPagoMap = conceptoPagoMap;
	}

	public Map<String, String> getTerminoAjusteMap() {
		return terminoAjusteMap;
	}


	public void setTerminoAjusteMap(Map<String, String> terminoAjusteMap) {
		this.terminoAjusteMap = terminoAjusteMap;
	}
	
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}


	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}


	public Long getIdOrdenPago() {
		return idOrdenPago;
	}


	public void setIdOrdenPago(Long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}


	public Boolean getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(Boolean soloLectura) {
		this.soloLectura = soloLectura;
	}


	public List<DetalleOrdenCompraDTO> getListaDetalleDTO() {
		return listaDetalleDTO;
	}


	public void setListaDetalleDTO(List<DetalleOrdenCompraDTO> listaDetalleDTO) {
		this.listaDetalleDTO = listaDetalleDTO;
	}


	public Map<String, String> getMotivoCancelacionMap() {
		return motivoCancelacionMap;
	}


	public void setMotivoCancelacionMap(Map<String, String> motivoCancelacionMap) {
		this.motivoCancelacionMap = motivoCancelacionMap;
	}


	public Map<String, String> getPagoaMap() {
		return pagoaMap;
	}


	public void setPagoaMap(Map<String, String> pagoaMap) {
		this.pagoaMap = pagoaMap;
	}




	public BloqueoPagoService getBloqueoPagoService() {
		return bloqueoPagoService;
	}


	public void setBloqueoPagoService(BloqueoPagoService bloqueoPagoService) {
		this.bloqueoPagoService = bloqueoPagoService;
	}

	public Map<String, String> getTipoOrdenCompraMap() {
		return tipoOrdenCompraMap;
	}

	public void setTipoOrdenCompraMap(Map<String, String> tipoOrdenCompraMap) {
		this.tipoOrdenCompraMap = tipoOrdenCompraMap;
	}

	public Long getIdLiqSiniestro() {
		return idLiqSiniestro;
	}

	public void setIdLiqSiniestro(Long idLiqSiniestro) {
		this.idLiqSiniestro = idLiqSiniestro;
	}

	public LiquidacionSiniestroService getLiquidacionSiniestroService() {
		return liquidacionSiniestroService;
	}

	public void setLiquidacionSiniestroService(
			LiquidacionSiniestroService liquidacionSiniestroService) {
		this.liquidacionSiniestroService = liquidacionSiniestroService;
	}

	public LiquidacionSiniestro getLiquidacionSiniestro() {
		return liquidacionSiniestro;
	}

	public void setLiquidacionSiniestro(LiquidacionSiniestro liquidacionSiniestro) {
		this.liquidacionSiniestro = liquidacionSiniestro;
	}

	public String getEstatusLiquidacion() {
		return estatusLiquidacion;
	}

	public void setEstatusLiquidacion(String estatusLiquidacion) {
		this.estatusLiquidacion = estatusLiquidacion;
	}

	public List<OrdenPagosDTO> getListadoLiquidacionesAsociadas() {
		return listadoLiquidacionesAsociadas;
	}
	public void setListadoLiquidacionesAsociadas(
			List<OrdenPagosDTO> listadoLiquidacionesAsociadas) {
		this.listadoLiquidacionesAsociadas = listadoLiquidacionesAsociadas;
	}
	
	public List<LiquidacionSiniestroRegistro> getListadoLiquidaciones() {
		return listadoLiquidaciones;
	}
	public void setListadoLiquidaciones(
			List<LiquidacionSiniestroRegistro> listadoLiquidaciones) {
		this.listadoLiquidaciones = listadoLiquidaciones;
	}
	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}
	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	public Map<String, String> getCtgLiquidaciones() {
		return ctgLiquidaciones;
	}
	public void setCtgLiquidaciones(Map<String, String> ctgLiquidaciones) {
		this.ctgLiquidaciones = ctgLiquidaciones;
	}
	public Map<String, String> getCtgTipoOperaciones() {
		return ctgTipoOperaciones;
	}
	public void setCtgTipoOperaciones(Map<String, String> ctgTipoOperaciones) {
		this.ctgTipoOperaciones = ctgTipoOperaciones;
	}
	public Map<String, String> getCtgEstatusLiquidaciones() {
		return ctgEstatusLiquidaciones;
	}
	public void setCtgEstatusLiquidaciones(
			Map<String, String> ctgEstatusLiquidaciones) {
		this.ctgEstatusLiquidaciones = ctgEstatusLiquidaciones;
	}
	public Map<String, String> getCtgCriterioComparacion() {
		return ctgCriterioComparacion;
	}
	public void setCtgCriterioComparacion(Map<String, String> ctgCriterioComparacion) {
		this.ctgCriterioComparacion = ctgCriterioComparacion;
	}
	
	/**
	 * @return the esPreview
	 */
	public Boolean getEsPreview() {
		return esPreview;
	}
	/**
	 * @param esPreview the esPreview to set
	 */
	public void setEsPreview(Boolean esPreview) {
		this.esPreview = esPreview;
	}
	/**
	 * @return the claveNegocio
	 */
	public String getClaveNegocio() {
		return claveNegocio;
	}
	/**
	 * @param claveNegocio the claveNegocio to set
	 */
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	/**
	 * @return the contentDispositionConfig
	 */
	public String getContentDispositionConfig() {
		return contentDispositionConfig;
	}
	/**
	 * @param contentDispositionConfig the contentDispositionConfig to set
	 */
	public void setContentDispositionConfig(String contentDispositionConfig) {
		this.contentDispositionConfig = contentDispositionConfig;
	}
	/**
	 * @return the filtroLiquidaciones
	 */
	public LiquidacionSiniestroFiltro getFiltroLiquidaciones() {
		return filtroLiquidaciones;
	}
	/**
	 * @param filtroLiquidaciones the filtroLiquidaciones to set
	 */
	public void setFiltroLiquidaciones(
			LiquidacionSiniestroFiltro filtroLiquidaciones) {
		this.filtroLiquidaciones = filtroLiquidaciones;
	}
	
	/**
	 * @return the esImprimible
	 */
	public Boolean getEsImprimible() {
		return esImprimible;
	}
	/**
	 * @param esImprimible the esImprimible to set
	 */
	public void setEsImprimible(Boolean esImprimible) {
		this.esImprimible = esImprimible;
	}

	/**
	 * @return the pantallaOrigen
	 */
	public String getPantallaOrigen() {
		return pantallaOrigen;
	}

	/**
	 * @param pantallaOrigen the pantallaOrigen to set
	 */
	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}

	public Long getIdPrestador() {
		return idPrestador;
	}

	public void setIdPrestador(Long idPrestador) {
		this.idPrestador = idPrestador;
	}

	public PrestadorServicio getPrestadorDeServicio() {
		return prestadorDeServicio;
	}

	public void setPrestadorDeServicio(PrestadorServicio prestadorDeServicio) {
		this.prestadorDeServicio = prestadorDeServicio;
	}	

	public List<InformacionBancaria> getListInfoBancaria() {
		return listInfoBancaria;
	}

	public void setListInfoBancaria(List<InformacionBancaria> listInfoBancaria) {
		this.listInfoBancaria = listInfoBancaria;
	}
	
	public List<DocumentoFiscal> getListaFacturasOrdenesPago() {
		return listaFacturasOrdenesPago;
	}
	public void setListaFacturasOrdenesPago(
			List<DocumentoFiscal> listaFacturasOrdenesPago) {
		this.listaFacturasOrdenesPago = listaFacturasOrdenesPago;
	}
	
	public Long getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}
	public List<FacturaLiquidacionDTO> getListaOrdenesPago() {
		return listaOrdenesPago;
	}
	public void setListaOrdenesPago(List<FacturaLiquidacionDTO> listaOrdenesPago) {
		this.listaOrdenesPago = listaOrdenesPago;
	}
	public void setCatalogoGrupoValorService(
			CatalogoGrupoValorService catalogoGrupoValorService) {
		this.catalogoGrupoValorService = catalogoGrupoValorService;
	}
	public Map<String, String> getListaTiposLiquidacion() {
		return listaTiposLiquidacion;
	}
	public void setListaTiposLiquidacion(Map<String, String> listaTiposLiquidacion) {
		this.listaTiposLiquidacion = listaTiposLiquidacion;
	}
	public List<DocumentoFiscal> getListaFacturasOrdenesPagoAsociadas() {
		return listaFacturasOrdenesPagoAsociadas;
	}
	public void setListaFacturasOrdenesPagoAsociadas(
			List<DocumentoFiscal> listaFacturasOrdenesPagoAsociadas) {
		this.listaFacturasOrdenesPagoAsociadas = listaFacturasOrdenesPagoAsociadas;
	}
	
	public List<Map<String, Object>> getListaPrestadoresServicio() {
		return listaPrestadoresServicio;
	}
	public void setListaPrestadoresServicio(
			List<Map<String, Object>> listaPrestadoresServicio) {
		this.listaPrestadoresServicio = listaPrestadoresServicio;
	}
	public List<OrdenPagoSiniestro> getOrdenesPagoSeleccionadas() {
		return ordenesPagoSeleccionadas;
	}
	public void setOrdenesPagoSeleccionadas(
			List<OrdenPagoSiniestro> ordenesPagoSeleccionadas) {
		this.ordenesPagoSeleccionadas = ordenesPagoSeleccionadas;
	}

	public List<CentroOperacionLiquidacionView> getListaCentrosOperacion() {
		return listaCentrosOperacion;
	}

	public void setListaCentrosOperacion(
			List<CentroOperacionLiquidacionView> listaCentrosOperacion) {
		this.listaCentrosOperacion = listaCentrosOperacion;
	}

	public String getNombrePrestadorServicio() {
		return nombrePrestadorServicio;
	}

	public void setNombrePrestadorServicio(String nombrePrestadorServicio) {
		this.nombrePrestadorServicio = nombrePrestadorServicio;
	}


	public Boolean getProveedorTieneInfoBancaria() {
		return proveedorTieneInfoBancaria;
	}


	public void setProveedorTieneInfoBancaria(Boolean proveedorTieneInfoBancaria) {
		this.proveedorTieneInfoBancaria = proveedorTieneInfoBancaria;
	}


	public DocumentoFiscal getFactura() {
		return factura;
	}


	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}


	public Boolean getUsuarioAutorizador() {
		return usuarioAutorizador;
	}


	public void setUsuarioAutorizador(Boolean usuarioAutorizador) {
		this.usuarioAutorizador = usuarioAutorizador;
	}


	public Short getTipoCancelacion() {
		return tipoCancelacion;
	}


	public void setTipoCancelacion(Short tipoCancelacion) {
		this.tipoCancelacion = tipoCancelacion;
	}


	public Short getEnvioCancelacion() {
		return envioCancelacion;
	}


	public void setEnvioCancelacion(Short envioCancelacion) {
		this.envioCancelacion = envioCancelacion;
	}


	public List<NotasCreditoRegistro> getListaNotasCredito() {
		return listaNotasCredito;
	}


	public void setListaNotasCredito(List<NotasCreditoRegistro> listaNotasCredito) {
		this.listaNotasCredito = listaNotasCredito;
	}


	public List<LiquidacionElementoDTO> getListaElementosDisponibles() {
		return listaElementosDisponibles;
	}


	public void setListaElementosDisponibles(
			List<LiquidacionElementoDTO> listaElementosDisponibles) {
		this.listaElementosDisponibles = listaElementosDisponibles;
	}


	public List<LiquidacionElementoDTO> getListaElementosAsociados() {
		return listaElementosAsociados;
	}


	public void setListaElementosAsociados(
			List<LiquidacionElementoDTO> listaElementosAsociados) {
		this.listaElementosAsociados = listaElementosAsociados;
	}


	public String getTipoElemento() {
		return tipoElemento;
	}


	public void setTipoElemento(String tipoElemento) {
		this.tipoElemento = tipoElemento;
	}


	public String getProveedorEsCompania() {
		return proveedorEsCompania;
	}


	public void setProveedorEsCompania(String proveedorEsCompania) {
		this.proveedorEsCompania = proveedorEsCompania;
	}


	public String getFacturasConcat() {
		return facturasConcat;
	}


	public void setFacturasConcat(String facturasConcat) {
		this.facturasConcat = facturasConcat;
	}


	public List<DesglosePagoConceptoDTO> getOrdenesCompraDesglose() {
		return ordenesCompraDesglose;
	}


	public void setOrdenesCompraDesglose(
			List<DesglosePagoConceptoDTO> ordenesCompraDesglose) {
		this.ordenesCompraDesglose = ordenesCompraDesglose;
	}
	

}