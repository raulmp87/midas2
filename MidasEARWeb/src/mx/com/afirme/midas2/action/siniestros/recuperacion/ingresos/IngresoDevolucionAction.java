package mx.com.afirme.midas2.action.siniestros.recuperacion.ingresos;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos.IngresoDevolucionDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.IngresoDevolucionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService.IngresoDevolucionFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/ingreso/cancelacionDevolucion")
public class IngresoDevolucionAction extends BaseAction implements Preparable{

	private static final long			serialVersionUID								= 3693037754760876365L;
	private static final String			LOCATION_JSP = "/jsp/siniestros/recuperacion/ingresos/";
	private static final String			LOCATION_CONTENEDORLISTADOINGRESODEVOLUCION_JSP		= LOCATION_JSP + "contenedorListadoIngresoDevolucion.jsp";
	private static final String			LOCATION_INGRESODEVOLUCIONGRID_JSP					= LOCATION_JSP + "ingresoDevolucionGrid.jsp";
	private static final String			LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP	= LOCATION_JSP + "contenedorSolicitudChequeDevolucion.jsp";
	private static final String			LOCATION_CARGARFACTURAINGRESODEVOLUCIONGRID_JSP		= LOCATION_JSP + "cargaFacturaIngresoDevolucionGrid.jsp";	
	private static final String			flujo 												= "SOL";
	
	private Map<Long, String>			cuentaAcreedoraList;
	private Map<String, String>			motivoCancelacionList;
	private Map<String, String>			tipoDevolucionList;
	private Map<String, String>			estatusList;
	private Map<String, String>			tipoRecuperacionList;
	private Map<String, String>			solicitadoPorList;
	private Map<String, String>			autorizadoPorList;
	private Map<String, String>			formaPagoList;
	private List<IngresoDevolucionDTO>	ingresoDevolucionList;
	private Map<Long, String>			bancoList;

	private IngresoDevolucion			ingresoDevolucion;
	private IngresoDevolucionFiltro		ingresoDevolucionFiltro;
	private File						facturaXml;
	private String						tipoBusqueda;
	private Long						ingresoDevolucionId;
	private Boolean						consulta;
	private TransporteImpresionDTO 		transporte;
	private EnvioValidacionFactura 		facturaCargada;
	private List<EnvioValidacionFactura> 	facturasCargadas;
	
	@Autowired
	@Qualifier("ingresoDevolucionServiceEJB")
	private IngresoDevolucionService	ingresoDevolucionService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService				listadoService;

	public void prepare(){

	}

	public void prepareSolicitarCheque(){
		bancoList 				= listadoService.getMapBancosMidas();
		tipoDevolucionList    	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_DEVOLUCION_INGRESO);
		formaPagoList 			= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		cuentaAcreedoraList 	= listadoService.getMapCuentasAcreedorasSiniestros();
	}
	

	public void prepareMostrarBusqueda() {
		motivoCancelacionList 	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO);
		tipoDevolucionList    	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_DEVOLUCION_INGRESO);
		estatusList 			= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_DEVOLUCION_INGRESO);
		tipoRecuperacionList 	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECUPERACION);
		formaPagoList 			= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		cuentaAcreedoraList 	= listadoService.getMapCuentasAcreedorasSiniestros();
		solicitadoPorList 		= ingresoDevolucionService.obtenerUsuariosSolicitadores();
		autorizadoPorList 		= ingresoDevolucionService.obtenerUsuariosAutorizadores();
	}

	@Action(value = "buscar", results = { 
			@Result(name = SUCCESS, location = LOCATION_INGRESODEVOLUCIONGRID_JSP)})
	public String buscar(){
		if(SystemCommonUtils.isNotNull(ingresoDevolucionFiltro)){
			ingresoDevolucionFiltro.setTipoBusqueda(IngresoDevolucionFiltro.TipoBusqueda.SOLICITUD.toString());
			ingresoDevolucionList = ingresoDevolucionService.buscar(ingresoDevolucionFiltro);
		}
		return SUCCESS;
	}

	@Action(value = "mostrarBusqueda", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTADOINGRESODEVOLUCION_JSP)})
	public String mostrarBusqueda(){
		return SUCCESS;
	}

	@Action(value="exportar", results={@Result(name=SUCCESS, type="stream",
		    params={"contentType","${transporte.contentType}", "inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String exportar(){
		if(SystemCommonUtils.isNotNull(ingresoDevolucionFiltro)){
			ingresoDevolucionFiltro.setTipoBusqueda(IngresoDevolucionFiltro.TipoBusqueda.SOLICITUD.toString());
			ingresoDevolucionList = ingresoDevolucionService.buscar(ingresoDevolucionFiltro);
			ExcelExporter exporter = new ExcelExporter(IngresoDevolucionDTO.class);
			transporte = exporter.exportXLS(ingresoDevolucionList, "SolicitudesCheque");
		}
		
		return SUCCESS;
	}

	@Action(value = "imprimirSolicitudCheque", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTADOINGRESODEVOLUCION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTADOINGRESODEVOLUCION_JSP)})
	public String imprimirSolicitudCheque(){
		return SUCCESS;
	}

	@Action(value = "consultarSolicitudCheque", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTADOINGRESODEVOLUCION_JSP)})
	public String consultarSolicitudCheque(){
		prepareSolicitarCheque();
		if(SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			consulta = Boolean.TRUE;
			ingresoDevolucion = ingresoDevolucionService.obtenerSolicitudCheque(ingresoDevolucionId);
		}
		return SUCCESS;
	}


	@Action(value = "cancelarSolicitudCheque", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarBusqueda", "namespace",
					"/siniestros/recuperacion/ingreso/cancelacionDevolucion",
					"mensaje","${mensaje}",	 "tipoMensaje","${tipoMensaje}"}),	
			@Result(name = INPUT, type = "redirectAction", params = {
							"actionName", "mostrarBusqueda", "namespace",
							"/siniestros/recuperacion/ingreso/cancelacionDevolucion",
							"mensaje","${mensaje}",	"tipoMensaje","${tipoMensaje}"})
			})
	public String cancelarSolicitudCheque(){
		String result = INPUT;
		if(SystemCommonUtils.isNotNull(ingresoDevolucionId)){
			Short resp = ingresoDevolucionService.cancelar(ingresoDevolucionId);			
			
			switch(resp) {
				case 0 :
					setMensaje("Se ha realizado la Cancelaci\u00F3n de la Devoluci\u00F3n de Ingreso");
					result = SUCCESS;
					break;
				case -1 :
					setMensaje("No existe la Solicitud de cheque para esta Devoluci\u00F3n de Ingreso");
					setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
					result = INPUT;
					break;
				case -2 :
					setMensaje("No ha sido posible realizar la Cancelaci\u00F3n ya que existe un cheque impreso");
					setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
					result = INPUT;
					break;
				case 1 :
					setMensaje("No ha sido posible realizar la Cancelaci\u00F3n ya que la Devoluci\u00F3n de Ingreso esta Aplicada");
					setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
					result = INPUT;
					break;
				case 2 :
					setMensaje("No ha sido posible realizar la Cancelaci\u00F3n");
					setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
					result = INPUT;
					break;
			}
		}else{
			result = INPUT;
		}

		return result;
		
	}

	@Action(value = "solicitarCheque", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP)})
	public String solicitarCheque(){
		if(SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			consulta = Boolean.FALSE;
			ingresoDevolucion = ingresoDevolucionService.obtenerSolicitudCheque(ingresoDevolucionId);
		}
		return SUCCESS;
	}

	@Action(value = "enviarSolicitudCheque", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarBusqueda", "namespace",
					"/siniestros/recuperacion/ingreso/cancelacionDevolucion"}),	
			@Result(name = INPUT, location = LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP)})
	public String enviarSolicitudCheque(){
		if(SystemCommonUtils.isNotNull(ingresoDevolucion)){
			ingresoDevolucionService.enviarSolicitudAutorizacion(ingresoDevolucion);
		}
		return SUCCESS;
	}

	@Action(value = "cargarFactura", results = { 
			@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","idBatch,tipoMensaje,mensaje"}),
			@Result(name=INPUT,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","idBatch,tipoMensaje,mensaje"})
	})
	public String cargarFactura(){
		if(SystemCommonUtils.isNotNull(facturaXml)){
			facturaCargada = ingresoDevolucionService.cargarFactura(facturaXml);
			ingresoDevolucion = ingresoDevolucionService.obtenerSolicitudCheque(ingresoDevolucionId);
			ingresoDevolucion.setFactura(facturaCargada.getFactura());
			ingresoDevolucionService.guardar(ingresoDevolucion);
			facturasCargadas = new ArrayList<EnvioValidacionFactura>();
			facturasCargadas.add(facturaCargada);
			this.setMensajeExito();
			return SUCCESS;
		}else{
//			super.setMensajeError(super.getText("midas.siniestros.recuperacion.ingresodevolucion.noarchivo"));
			return INPUT;			
		}
	}
	
	@Action(value="listarFactura",results={
			@Result(name=SUCCESS,location = LOCATION_CARGARFACTURAINGRESODEVOLUCIONGRID_JSP)
	})	
	public String listarFactura() {	
		if(SystemCommonUtils.isEmptyList(facturasCargadas)){
			facturasCargadas = ingresoDevolucionService.obtenerFacturaCargada(ingresoDevolucionId);
		}
		return SUCCESS;
	}

	public Map<Long, String> getCuentaAcreedoraList() {
		return cuentaAcreedoraList;
	}

	public void setCuentaAcreedoraList(Map<Long, String> cuentaAcreedoraList) {
		this.cuentaAcreedoraList = cuentaAcreedoraList;
	}

	public Map<String, String> getMotivoCancelacionList() {
		return motivoCancelacionList;
	}

	public void setMotivoCancelacionList(Map<String, String> motivoCancelacionList) {
		this.motivoCancelacionList = motivoCancelacionList;
	}

	public Map<String, String> getTipoDevolucionList() {
		return tipoDevolucionList;
	}

	public void setTipoDevolucionList(Map<String, String> tipoDevolucionList) {
		this.tipoDevolucionList = tipoDevolucionList;
	}

	public Map<String, String> getEstatusList() {
		return estatusList;
	}

	public void setEstatusList(Map<String, String> estatusList) {
		this.estatusList = estatusList;
	}

	public Map<String, String> getTipoRecuperacionList() {
		return tipoRecuperacionList;
	}

	public void setTipoRecuperacionList(Map<String, String> tipoRecuperacionList) {
		this.tipoRecuperacionList = tipoRecuperacionList;
	}

	public Map<String, String> getSolicitadoPorList() {
		return solicitadoPorList;
	}

	public void setSolicitadoPorList(Map<String, String> solicitadoPorList) {
		this.solicitadoPorList = solicitadoPorList;
	}

	public Map<String, String> getAutorizadoPorList() {
		return autorizadoPorList;
	}

	public void setAutorizadoPorList(Map<String, String> autorizadoPorList) {
		this.autorizadoPorList = autorizadoPorList;
	}

	public Map<String, String> getFormaPagoList() {
		return formaPagoList;
	}

	public void setFormaPagoList(Map<String, String> formaPagoList) {
		this.formaPagoList = formaPagoList;
	}

	public List<IngresoDevolucionDTO> getIngresoDevolucionList() {
		return ingresoDevolucionList;
	}

	public void setIngresoDevolucionList(List<IngresoDevolucionDTO> ingresoDevolucionList) {
		this.ingresoDevolucionList = ingresoDevolucionList;
	}

	public Map<Long, String> getBancoList() {
		return bancoList;
	}

	public void setBancoList(Map<Long, String> bancoList) {
		this.bancoList = bancoList;
	}

	public IngresoDevolucion getIngresoDevolucion() {
		return ingresoDevolucion;
	}

	public void setIngresoDevolucion(IngresoDevolucion ingresoDevolucion) {
		this.ingresoDevolucion = ingresoDevolucion;
	}

	public IngresoDevolucionFiltro getIngresoDevolucionFiltro() {
		return ingresoDevolucionFiltro;
	}

	public void setIngresoDevolucionFiltro(IngresoDevolucionFiltro ingresoDevolucionFiltro) {
		this.ingresoDevolucionFiltro = ingresoDevolucionFiltro;
	}

	public File getFacturaXml() {
		return facturaXml;
	}

	public void setFacturaXml(File facturaXml) {
		this.facturaXml = facturaXml;
	}

	public String getTipoBusqueda() {
		return tipoBusqueda;
	}

	public void setTipoBusqueda(String tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}

	public IngresoDevolucionService getIngresoDevolucionService() {
		return ingresoDevolucionService;
	}

	public void setIngresoDevolucionService(IngresoDevolucionService ingresoDevolucionService) {
		this.ingresoDevolucionService = ingresoDevolucionService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Long getIngresoDevolucionId() {
		return ingresoDevolucionId;
	}

	public void setIngresoDevolucionId(Long ingresoDevolucionId) {
		this.ingresoDevolucionId = ingresoDevolucionId;
	}

	public Boolean getConsulta() {
		return consulta;
	}

	public void setConsulta(Boolean consulta) {
		this.consulta = consulta;
	}

	public static String getFlujo() {
		return flujo;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public EnvioValidacionFactura getFacturaCargada() {
		return facturaCargada;
	}

	public void setFacturaCargada(EnvioValidacionFactura facturaCargada) {
		this.facturaCargada = facturaCargada;
	}

	public List<EnvioValidacionFactura> getFacturasCargadas() {
		return facturasCargadas;
	}

	public void setFacturasCargadas(List<EnvioValidacionFactura> facturasCargadas) {
		this.facturasCargadas = facturasCargadas;
	}
	
	
}