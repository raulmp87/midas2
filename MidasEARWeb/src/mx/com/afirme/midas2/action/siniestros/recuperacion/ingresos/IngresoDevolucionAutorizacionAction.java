package mx.com.afirme.midas2.action.siniestros.recuperacion.ingresos;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.IngresoDevolucionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService.IngresoDevolucionFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion")
public class IngresoDevolucionAutorizacionAction extends BaseAction implements Preparable{

	private static final long			serialVersionUID									= -5734200750231104085L;

	private static final String			LOCATION_JSP										= "/jsp/siniestros/recuperacion/ingresos/";
	private static final String			LOCATION_CONTENEDORLISTADO_JSP						= LOCATION_JSP + "contenedorListadoIngresoDevolucion.jsp";
	private static final String			LOCATION_INGRESODEVOLUCIONGRID_JSP					= LOCATION_JSP + "ingresoDevolucionGrid.jsp";
	private static final String			LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP	= LOCATION_JSP + "contenedorSolicitudChequeDevolucion.jsp";
	private static final String			LOCATION_CONTENEDORIMPRESION_JSP					= LOCATION_JSP + "contenedorImpresionSolicitudCheque.jsp";;
	private static final String			flujo 												= "AUT";
	private Map<Long, String>			cuentaAcreedoraList;			
	private Map<String, String>			motivoCancelacionList;
	private Map<String, String>			tipoDevolucionList;
	private Map<String, String>			estatusList;
	private Map<String, String>			tipoRecuperacionList;
	private Map<String, String>			solicitadoPorList;
	private Map<String, String>			autorizadoPorList;
	private Map<String, String>			formaPagoList;
	private List<IngresoDevolucionDTO>	ingresoDevolucionList;
	private List<BancoDTO>				bancoList;
	private IngresoDevolucion			ingresoDevolucion;
	private Long						ingresoDevolucionId;
	private TransporteImpresionDTO		transporteImpresionDTO;
	private Boolean						esImprimible;
	private IngresoDevolucionFiltro		ingresoDevolucionFiltro;
	private TransporteImpresionDTO 		transporte;
	private Boolean 					esPreview;
	private String 						contentDispositionConfig;
	private String						observaciones;
	private String 						pantallaOrigen;
	
	private static enum CONTENT_DISPOSITION_CONFIGURATION{
		attachment, inline;
	}
	private static enum PANTALLA_ORIGEN_AUTORIZAR{
		LISTADO, IMPRESION;
	}

	@Autowired
	@Qualifier("ingresoDevolucionServiceEJB")
	private IngresoDevolucionService	ingresoDevolucionService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService				listadoService;
	
	public void prepare(){

	}


	public void prepareMostrarBusqueda(){
		motivoCancelacionList 	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVOS_CANCELACION_INGRESO);
		tipoDevolucionList    	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_DEVOLUCION_INGRESO);
		estatusList 			= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_DEVOLUCION_INGRESOAUT);
		tipoRecuperacionList 	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECUPERACION);
		formaPagoList 			= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		cuentaAcreedoraList 	= listadoService.getMapCuentasAcreedorasSiniestros();
		solicitadoPorList 		= ingresoDevolucionService.obtenerUsuariosSolicitadores();
		autorizadoPorList 		= ingresoDevolucionService.obtenerUsuariosAutorizadores();
	}

	@Action(value = "mostrarBusqueda", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTADO_JSP)})
	public String mostrarBusqueda(){
		
		return SUCCESS;
	}

	@Action(value = "buscar", results = { 
			@Result(name = SUCCESS, location = LOCATION_INGRESODEVOLUCIONGRID_JSP)})
	public String buscar(){
		if(SystemCommonUtils.isNotNull(ingresoDevolucionFiltro)){
			ingresoDevolucionFiltro.setTipoBusqueda(IngresoDevolucionFiltro.TipoBusqueda.AUTORIZACION.toString());
			ingresoDevolucionList = ingresoDevolucionService.buscar(ingresoDevolucionFiltro);
		}
		return SUCCESS;
	}

	@Action(value = "consultarSolicitudCheque", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSOLICITUDCHEQUEDEVOLUCION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTADO_JSP)})
	public String consultarSolicitudCheque(){
		if(SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			ingresoDevolucion = ingresoDevolucionService.obtenerSolicitudCheque(ingresoDevolucionId);
		}
		return SUCCESS;
	}

	@Action(value = "autorizarSolicitudCheque", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarBusqueda", "namespace",
					"/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"
					}),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "consultarImpresion", "namespace",
					"/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}",
					"ingresoDevolucionId", "${ingresoDevolucionId}"					
					})					
			})
	public String autorizarSolicitudCheque(){
		prepareMostrarBusqueda();
		if(SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			try {
				ingresoDevolucionService.autorizar(ingresoDevolucionId);
			} catch (Exception e) {
				super.setMensajeError(e.getMessage());
				if(StringUtil.isEmpty(this.pantallaOrigen) || this.pantallaOrigen.equalsIgnoreCase(PANTALLA_ORIGEN_AUTORIZAR.LISTADO.toString()) ){
					return SUCCESS;
				}else if (this.pantallaOrigen.equalsIgnoreCase(PANTALLA_ORIGEN_AUTORIZAR.IMPRESION.toString())){
					return INPUT;
				}else{
					return SUCCESS;
				}
			} 
		}
		super.setMensajeExito();
		return SUCCESS;
	}

	@Action(value = "rechazarSolicitudCheque", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarBusqueda", "namespace",
					"/siniestros/recuperacion/ingreso/cancelacionDevolucion/autorizacion"})})
	public String rechazarSolicitudCheque(){
		prepareMostrarBusqueda();
		if(SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			ingresoDevolucionService.rechazar(ingresoDevolucionId);
		}
		return SUCCESS;
	}

	@Action(value = "consultarImpresion", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORIMPRESION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORIMPRESION_JSP)})
	public String consultarImpresion(){
		esImprimible = Boolean.TRUE;
		if(!SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			setMensajeError("Debes proporcionar un n\u00FAmero de devoluci\u00F3n de ingreso v\u00E1lido para poder imprimir");
			return INPUT;
		}
		ingresoDevolucion = ingresoDevolucionService.obtenerSolicitudCheque(ingresoDevolucionId);
		observaciones = ingresoDevolucion.getObservaciones();
		return SUCCESS;
	}

	@Action(value = "imprimirSolicitudCheque", results = { 
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","${contentDispositionConfig};filename=\"${transporte.fileName}\""})})
	public String imprimirSolicitudCheque(){
		transporte = ingresoDevolucionService.obtenerImpresionCheque(ingresoDevolucionId);
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");
		if(esPreview != null 
				&& esPreview == true){ //Tiene que estar en TRUE para que despliegue el PDF en pantalla en vez de intentar descargarlo
			contentDispositionConfig = CONTENT_DISPOSITION_CONFIGURATION.inline.toString();
		}else{
			contentDispositionConfig = CONTENT_DISPOSITION_CONFIGURATION.attachment.toString();
		}
		
		String fileName = "SolicitudCheque_"+ ingresoDevolucionId + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value="exportar", results={@Result(name=SUCCESS, type="stream",
		    params={"contentType","${transporte.contentType}", "inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String exportar(){
		if(SystemCommonUtils.isNotNull(ingresoDevolucionFiltro)){
			ingresoDevolucionFiltro.setTipoBusqueda(IngresoDevolucionFiltro.TipoBusqueda.SOLICITUD.toString());
			ingresoDevolucionList = ingresoDevolucionService.buscar(ingresoDevolucionFiltro);
			ExcelExporter exporter = new ExcelExporter(IngresoDevolucionDTO.class);
			transporte = exporter.exportXLS(ingresoDevolucionList, "SolicitudesAutorizacionCheque");
		}
		
		return SUCCESS;
	}

	@Action(value = "guardarComentariosConsulta", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORIMPRESION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORIMPRESION_JSP)})
	public String guardarComentariosConsulta(){
		if(SystemCommonUtils.isNotNullNorZero(ingresoDevolucionId)){
			ingresoDevolucion = ingresoDevolucionService.obtenerSolicitudCheque(ingresoDevolucionId);
			ingresoDevolucion.setObservaciones(observaciones);
			ingresoDevolucion = ingresoDevolucionService.guardar(ingresoDevolucion);
			observaciones = ingresoDevolucion.getObservaciones();
			esImprimible = Boolean.TRUE;
			return SUCCESS;
		}
		setMensajeError("Debes proporcionar un n\u00FAmero de devoluci\u00F3n de ingreso v\u00E1lido para poder imprimir");
		return INPUT;
	}



	public Map<Long, String> getCuentaAcreedoraList() {
		return cuentaAcreedoraList;
	}


	public void setCuentaAcreedoraList(Map<Long, String> cuentaAcreedoraList) {
		this.cuentaAcreedoraList = cuentaAcreedoraList;
	}


	public Map<String, String> getMotivoCancelacionList() {
		return motivoCancelacionList;
	}

	public void setMotivoCancelacionList(Map<String, String> motivoCancelacionList) {
		this.motivoCancelacionList = motivoCancelacionList;
	}

	public Map<String, String> getTipoDevolucionList() {
		return tipoDevolucionList;
	}

	public void setTipoDevolucionList(Map<String, String> tipoDevolucionList) {
		this.tipoDevolucionList = tipoDevolucionList;
	}

	public Map<String, String> getEstatusList() {
		return estatusList;
	}

	public void setEstatusList(Map<String, String> estatusList) {
		this.estatusList = estatusList;
	}

	public Map<String, String> getTipoRecuperacionList() {
		return tipoRecuperacionList;
	}

	public void setTipoRecuperacionList(Map<String, String> tipoRecuperacionList) {
		this.tipoRecuperacionList = tipoRecuperacionList;
	}

	public Map<String, String> getSolicitadoPorList() {
		return solicitadoPorList;
	}

	public void setSolicitadoPorList(Map<String, String> solicitadoPorList) {
		this.solicitadoPorList = solicitadoPorList;
	}

	public Map<String, String> getAutorizadoPorList() {
		return autorizadoPorList;
	}

	public void setAutorizadoPorList(Map<String, String> autorizadoPorList) {
		this.autorizadoPorList = autorizadoPorList;
	}

	public Map<String, String> getFormaPagoList() {
		return formaPagoList;
	}

	public void setFormaPagoList(Map<String, String> formaPagoList) {
		this.formaPagoList = formaPagoList;
	}

	public List<IngresoDevolucionDTO> getIngresoDevolucionList() {
		return ingresoDevolucionList;
	}

	public void setIngresoDevolucionList(List<IngresoDevolucionDTO> ingresoDevolucionList) {
		this.ingresoDevolucionList = ingresoDevolucionList;
	}

	public List<BancoDTO> getBancoList() {
		return bancoList;
	}

	public void setBancoList(List<BancoDTO> bancoList) {
		this.bancoList = bancoList;
	}

	public IngresoDevolucion getIngresoDevolucion() {
		return ingresoDevolucion;
	}

	public void setIngresoDevolucion(IngresoDevolucion ingresoDevolucion) {
		this.ingresoDevolucion = ingresoDevolucion;
	}

	public Long getIngresoDevolucionId() {
		return ingresoDevolucionId;
	}

	public void setIngresoDevolucionId(Long ingresoDevolucionId) {
		this.ingresoDevolucionId = ingresoDevolucionId;
	}

	public TransporteImpresionDTO getTransporteImpresionDTO() {
		return transporteImpresionDTO;
	}

	public void setTransporteImpresionDTO(TransporteImpresionDTO transporteImpresionDTO) {
		this.transporteImpresionDTO = transporteImpresionDTO;
	}

	public Boolean isEsImprimible() {
		return esImprimible;
	}

	public void setEsImprimible(Boolean esImprimible) {
		this.esImprimible = esImprimible;
	}

	public IngresoDevolucionFiltro getIngresoDevolucionFiltro() {
		return ingresoDevolucionFiltro;
	}

	public void setIngresoDevolucionFiltro(IngresoDevolucionFiltro ingresoDevolucionFiltro) {
		this.ingresoDevolucionFiltro = ingresoDevolucionFiltro;
	}

	public IngresoDevolucionService getIngresoDevolucionService() {
		return ingresoDevolucionService;
	}

	public void setIngresoDevolucionService(IngresoDevolucionService ingresoDevolucionService) {
		this.ingresoDevolucionService = ingresoDevolucionService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public String getFlujo() {
		return flujo;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}


	/**
	 * @return the esPreview
	 */
	public Boolean getEsPreview() {
		return esPreview;
	}


	/**
	 * @param esPreview the esPreview to set
	 */
	public void setEsPreview(Boolean esPreview) {
		this.esPreview = esPreview;
	}


	/**
	 * @return the contentDispositionConfig
	 */
	public String getContentDispositionConfig() {
		return contentDispositionConfig;
	}


	/**
	 * @param contentDispositionConfig the contentDispositionConfig to set
	 */
	public void setContentDispositionConfig(String contentDispositionConfig) {
		this.contentDispositionConfig = contentDispositionConfig;
	}


	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}


	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	/**
	 * @return the esImprimible
	 */
	public Boolean getEsImprimible() {
		return esImprimible;
	}


	public String getPantallaOrigen() {
		return pantallaOrigen;
	}


	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}
	

}