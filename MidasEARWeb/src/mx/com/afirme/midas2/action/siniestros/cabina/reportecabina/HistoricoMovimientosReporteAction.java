package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/siniestros/cabina/reportecabina/historicomovimientos")
@Component
@Scope("prototype")
public class HistoricoMovimientosReporteAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 3644789207592083243L;
	private String pantallaOrigen;
	private String accion;
	private long idOrdenCompra;
	private long idOrdenPago;
	private Long ingresoId;
	private Integer tipoDeCancelacion;
	
	private MovimientoSiniestroDTO filtroMovimientos;
	private Long idToReporte;
	private List<MovimientoSiniestroDTO> movimientos;
	private Map<String,String> tipoMovimientos;
	private List<AfectacionCoberturaSiniestroDTO> coberturas;
	private Map<String,String> listaUsuarios;
	private Boolean mostrarSoloEncabezados;
	private Short soloConsulta;
	private Boolean esPantallaGA;
	
	private BigDecimal totalGastos;
	private BigDecimal totalRecuperacionGastos;
	private BigDecimal totalEstimado; 
	private BigDecimal totalPagado;
	private BigDecimal reservaPendiente;
	private BigDecimal totalIngresos;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCobSinService;
	
	@Autowired
	@Qualifier("movimientoSiniestroServiceEJB")
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;	
	
	@Override
	public void prepare(){
		coberturas = estimacionCobSinService.obtenerCoberturasAfectacion(idToReporte);
		tipoMovimientos = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_DE_MOVIMIENTO);
		listaUsuarios = movimientoSiniestroService.obtenerUsuariosMovSiniestros();
	}
	
	public void prepareMostrar(){
		if(filtroMovimientos == null){
			filtroMovimientos = new MovimientoSiniestroDTO();
		}
		if(StringUtil.isEmpty(filtroMovimientos.getNumeroSiniestro())){
			if(idToReporte != null){
				ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idToReporte);
				if(reporte != null
						&& reporte.getSiniestroCabina() != null ){
					filtroMovimientos.setNumeroSiniestro(reporte.getSiniestroCabina().getNumeroSiniestro());
				}
			}
		}
	}
	
	/**
	 * Muestra pantalla contenedora de las pestanias de la busqueda de historico de movimientos
	 * @return
	 */
	@Action(value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/contenedorMovimientosReporte.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenedorMovimientosReporte.jsp")})
	public String mostrarContenedor() {		
			return SUCCESS;
	}
	
	/**
	 * Muestra pestania de busqueda de historico de movimientos
	 * @return
	 */
	@Action(value = "mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/historicoMovimientosReporte.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/historicoMovimientosReporte.jsp")})
	public String mostrar() {			
			return SUCCESS;
	}
	
	/**
	 * Ejecuta la busqueda de los historicos de los movimientos
	 * @return
	 */
	@Action(value = "buscar", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/historicoMovimientosReporteGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/historicoMovimientosReporte.jsp")})
	public String buscar() {
		try{
			if(idToReporte != null){
				if(mostrarSoloEncabezados){
					movimientos = new ArrayList<MovimientoSiniestroDTO>();
					return SUCCESS;
				}else{
					filtroMovimientos.setIdToReporte(idToReporte);
					if(esPantallaGA != null 
							&& esPantallaGA){
						movimientos = movimientoSiniestroService.obtenerHistoricoMovimientosGA(filtroMovimientos);
						filtroMovimientos.setTipoDocumento(MovimientoCoberturaSiniestro.TipoDocumentoMovimiento.PAGO.toString());
						totalGastos = movimientoSiniestroService.obtenerImporteMovimientosGA(filtroMovimientos);
						filtroMovimientos.setTipoDocumento(MovimientoCoberturaSiniestro.TipoDocumentoMovimiento.INGRESO.toString());
						totalRecuperacionGastos = movimientoSiniestroService.obtenerImporteMovimientosGA(filtroMovimientos);
						if(totalGastos == null){ totalGastos = BigDecimal.ZERO;}
						if(totalRecuperacionGastos == null){ totalRecuperacionGastos = BigDecimal.ZERO;}
					}else{
						movimientos = movimientoSiniestroService.obtenerHistoricoMovimientos(filtroMovimientos);
						filtroMovimientos.setTipoDocumento(MovimientoCoberturaSiniestro.TipoDocumentoMovimiento.PAGO.toString());
						totalPagado = movimientoSiniestroService.obtenerImporteMovimientos(filtroMovimientos);
						filtroMovimientos.setTipoDocumento(MovimientoCoberturaSiniestro.TipoDocumentoMovimiento.ESTIMACION.toString());
						totalEstimado = movimientoSiniestroService.obtenerImporteMovimientos(filtroMovimientos);
						filtroMovimientos.setTipoDocumento(MovimientoCoberturaSiniestro.TipoDocumentoMovimiento.INGRESO.toString());
						totalIngresos = movimientoSiniestroService.obtenerImporteMovimientos(filtroMovimientos);
						if(totalPagado == null){ totalPagado = BigDecimal.ZERO;}
						if(totalEstimado == null){ totalEstimado = BigDecimal.ZERO;}
						if(totalIngresos == null){ totalIngresos = BigDecimal.ZERO;}
						reservaPendiente = totalEstimado.subtract(totalPagado);
					}
					return SUCCESS;
				}
			}else{
				setMensajeError(MENSAJE_ERROR_GENERAL);
				return INPUT;
			}
			
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	

	/**
	 * @return the filtroMovimientos
	 */
	public MovimientoSiniestroDTO getFiltroMovimientos() {
		return filtroMovimientos;
	}

	/**
	 * @param filtroMovimientos the filtroMovimientos to set
	 */
	public void setFiltroMovimientos(MovimientoSiniestroDTO filtroMovimientos) {
		this.filtroMovimientos = filtroMovimientos;
	}

	/**
	 * @return the movimientos
	 */
	public List<MovimientoSiniestroDTO> getMovimientos() {
		return movimientos;
	}

	/**
	 * @param movimientos the movimientos to set
	 */
	public void setMovimientos(List<MovimientoSiniestroDTO> movimientos) {
		this.movimientos = movimientos;
	}

	/**
	 * @return the tipoMovimientos
	 */
	public Map<String, String> getTipoMovimientos() {
		return tipoMovimientos;
	}

	/**
	 * @param tipoMovimientos the tipoMovimientos to set
	 */
	public void setTipoMovimientos(Map<String, String> tipoMovimientos) {
		this.tipoMovimientos = tipoMovimientos;
	}

	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}

	/**
	 * @param listadoService the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the estimacionCobSinService
	 */
	public EstimacionCoberturaSiniestroService getEstimacionCobSinService() {
		return estimacionCobSinService;
	}

	/**
	 * @param estimacionCobSinService the estimacionCobSinService to set
	 */
	public void setEstimacionCobSinService(
			EstimacionCoberturaSiniestroService estimacionCobSinService) {
		this.estimacionCobSinService = estimacionCobSinService;
	}

	/**
	 * @return the coberturas
	 */
	public List<AfectacionCoberturaSiniestroDTO> getCoberturas() {
		return coberturas;
	}

	/**
	 * @param coberturas the coberturas to set
	 */
	public void setCoberturas(List<AfectacionCoberturaSiniestroDTO> coberturas) {
		this.coberturas = coberturas;
	}

	/**
	 * @return the idToReporte
	 */
	public Long getIdToReporte() {
		return idToReporte;
	}

	/**
	 * @param idToReporte the idToReporte to set
	 */
	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}

	public String getPantallaOrigen() {
		return pantallaOrigen;
	}

	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	public void setIdOrdenCompra(long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	public long getIdOrdenPago() {
		return idOrdenPago;
	}

	public void setIdOrdenPago(long idOrdenPago) {
		this.idOrdenPago = idOrdenPago;
	}
	

	/**
	 * @return the mostrarSoloEncabezados
	 */
	public Boolean getMostrarSoloEncabezados() {
		return mostrarSoloEncabezados;
	}

	/**
	 * @param mostrarSoloEncabezados the mostrarSoloEncabezados to set
	 */
	public void setMostrarSoloEncabezados(Boolean mostrarSoloEncabezados) {
		this.mostrarSoloEncabezados = mostrarSoloEncabezados;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	/**
	 * @return the esPantallaGA
	 */
	public Boolean getEsPantallaGA() {
		return esPantallaGA;
	}

	/**
	 * @param esPantallaGA the esPantallaGA to set
	 */
	public void setEsPantallaGA(Boolean esPantallaGA) {
		this.esPantallaGA = esPantallaGA;
	}

	/**
	 * @return the listaUsuarios
	 */
	public Map<String, String> getListaUsuarios() {
		return listaUsuarios;
	}

	/**
	 * @param listaUsuarios the listaUsuarios to set
	 */
	public void setListaUsuarios(Map<String, String> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	/**
	 * @return the totalGastos
	 */
	public BigDecimal getTotalGastos() {
		return totalGastos;
	}

	/**
	 * @param totalGastos the totalGastos to set
	 */
	public void setTotalGastos(BigDecimal totalGastos) {
		this.totalGastos = totalGastos;
	}

	/**
	 * @return the totalRecuperacionGastos
	 */
	public BigDecimal getTotalRecuperacionGastos() {
		return totalRecuperacionGastos;
	}

	/**
	 * @param totalRecuperacionGastos the totalRecuperacionGastos to set
	 */
	public void setTotalRecuperacionGastos(BigDecimal totalRecuperacionGastos) {
		this.totalRecuperacionGastos = totalRecuperacionGastos;
	}

	/**
	 * @return the totalPagado
	 */
	public BigDecimal getTotalPagado() {
		return totalPagado;
	}

	/**
	 * @param totalPagado the totalPagado to set
	 */
	public void setTotalPagado(BigDecimal totalPagado) {
		this.totalPagado = totalPagado;
	}

	/**
	 * @return the reservaPendiente
	 */
	public BigDecimal getReservaPendiente() {
		return reservaPendiente;
	}

	/**
	 * @param reservaPendiente the reservaPendiente to set
	 */
	public void setReservaPendiente(BigDecimal reservaPendiente) {
		this.reservaPendiente = reservaPendiente;
	}

	/**
	 * @return the totalIngresos
	 */
	public BigDecimal getTotalIngresos() {
		return totalIngresos;
	}

	/**
	 * @param totalIngresos the totalIngresos to set
	 */
	public void setTotalIngresos(BigDecimal totalIngresos) {
		this.totalIngresos = totalIngresos;
	}

	/**
	 * @return the totalEstimado
	 */
	public BigDecimal getTotalEstimado() {
		return totalEstimado;
	}

	/**
	 * @param totalEstimado the totalEstimado to set
	 */
	public void setTotalEstimado(BigDecimal totalEstimado) {
		this.totalEstimado = totalEstimado;
	}

	/**
	 * @return the ingresoId
	 */
	public Long getIngresoId() {
		return ingresoId;
	}

	/**
	 * @param ingresoId the ingresoId to set
	 */
	public void setIngresoId(Long ingresoId) {
		this.ingresoId = ingresoId;
	}

	public Integer getTipoDeCancelacion() {
		return tipoDeCancelacion;
	}

	public void setTipoDeCancelacion(Integer tipoDeCancelacion) {
		this.tipoDeCancelacion = tipoDeCancelacion;
	}
	
}