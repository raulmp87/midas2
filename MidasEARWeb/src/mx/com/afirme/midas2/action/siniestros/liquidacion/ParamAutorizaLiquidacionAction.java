package mx.com.afirme.midas2.action.siniestros.liquidacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacion;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacionDetalle;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionRegistro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/siniestros/liquidacion/configuracion/parametros/liquidacionSiniestro")
public class ParamAutorizaLiquidacionAction extends BaseAction implements Preparable {
	
	
	
	private Long parametroId;
	private Integer parametroEstatus;
	
	
	private Boolean esParametroSoloLectura;
	
	private ParametroAutorizacionLiquidacion parametro;
	
	private String nombreusuarioActual;
	
	private String oficinasConcat;
	private String estadosConcat;
	private String proveedoresConcat;
	private String usuariosSolConcat;
	private String usuariosAutConcat;
	
	private Boolean esCondicionMontos;
	private Boolean esCondicionVigencia;
	private Boolean esRangoMontos;
	private Boolean esRangoVigencia;
	
	private Map<String,String> estatus;
	private Map<String,String> origenLiquidacion;
	private Map<String,String> tipoLiquidacion;
	private Map<String,String> tipoPago;
	private Map<String,String> estatusLiquidacion;
	private Map<String,String> criterioComparacion; 
	
	private Map<Long, String> oficinas;
	private Map<String,String> estados;
	private List<PrestadorServicioRegistro> listPrestadorServicio;
	private List<Usuario> usuarios;
	
	private ParamAutLiquidacionDTO filtroParametros;
	private List<ParamAutLiquidacionRegistro> resultados;
	
	
	private final String[] rolesParaUsuarios = {"Rol_M2_Gerente_Operaciones_Indemnizacion","Rol_M2_Gerente_Operaciones_Servicio_Publico",
												"Rol_M2_Coordinador_Pagos","Rol_M2_Coordinador_Cobro_Companias_Salvamentos","Rol_M2_Coordinador_Operaciones"};
	
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("liquidacionSiniestroServiceEJB")
	private LiquidacionSiniestroService liquidacionSiniestroService;
	
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	private PrestadorDeServicioService prestadorServicioService;
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	private UsuarioService usuarioService;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2840756580308507678L;
	private static final String CONTENEDOR_BUSQUEDA="/jsp/siniestros/liquidacion/parametros/paramAutorizaLiquidacion.jsp";
	private static final String CONTENEDOR_BUSQUEDA_GRID="/jsp/siniestros/liquidacion/parametros/paramAutorizaLiquidacionGrid.jsp";
	private static final String CONTENEDOR_ALTA="/jsp/siniestros/liquidacion/parametros/paramAltaAutorizacion.jsp";
	@Override
	public void prepare() throws Exception {
		
	}
	
	private void cargaInfoParaCrearParametro(){
		
		this.nombreusuarioActual = usuarioService.getUsuarioActual().getNombreCompleto();
		
		this.estatus = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		this.estatusLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_LIQUIDACION_SINIESTRO);
		this.origenLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ORIGEN_LIQUIDACION);
		this.tipoLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		this.tipoPago = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		this.criterioComparacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CRITERIO_COMPARACION);
		
		this.oficinas = listadoService.obtenerOficinasSiniestros();
		this.estados = this.listadoService.getMapEstadosPorPaisMidas(null);
		this.listPrestadorServicio = prestadorServicioService.buscar(new PrestadorServicioFiltro()); 
		this.usuarios =  usuarioService.buscarUsuariosPorNombreRol(rolesParaUsuarios);
		//TODO SOLO PARA PRUEBAS, ELIMINAR ANTES DE SUBIR
		if(this.usuarios==null){
			this.usuarios = new ArrayList<Usuario>();
			this.usuarios.add(usuarioService.getUsuarioActual());
		}
	}
	
	
	private void cargaInfoParaBusquedaDeParametros(){
		
		this.estatus = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		this.origenLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ORIGEN_LIQUIDACION);
		this.tipoLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		this.tipoPago = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		this.criterioComparacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CRITERIO_COMPARACION);
		this.usuarios =  usuarioService.buscarUsuariosPorNombreRol(rolesParaUsuarios);
		//TODO SOLO PARA PRUEBAS, ELIMINAR ANTES DE SUBIR
		if(this.usuarios==null){
			this.usuarios = new ArrayList<Usuario>();
			this.usuarios.add(usuarioService.getUsuarioActual());
		}
	}
	
	
	
	public void prepareMostrarContenedorBusqueda() throws Exception {
		this.cargaInfoParaBusquedaDeParametros();
	}
	
	@Action(value="mostrarContenedorBusqueda",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA)
			})
	public String mostrarContenedorBusqueda(){
		return SUCCESS;
	}
	
	public void prepareMostrarContenedorAlta() throws Exception {
		cargaInfoParaCrearParametro();
	}
	
	@Action(value="mostrarContenedorAlta",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ALTA),
			@Result(name=INPUT,location=CONTENEDOR_ALTA)
			})
	public String mostrarContenedorAlta(){
		return SUCCESS;
	}
	
	
	public void prepareConsultarParametro() throws Exception {
		cargaInfoParaCrearParametro();
	}
	
	@Action(value="consultarParametro",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ALTA),
			@Result(name=INPUT,location=CONTENEDOR_ALTA)
			})
	public String consultarParametro(){
		this.parametro = this.entidadService.findById(ParametroAutorizacionLiquidacion.class, this.parametroId);
		setConcatAttributes(this.parametro.getDetalles());
		this.parametroEstatus  = (this.parametro.getEstatus())?1:0;
		return SUCCESS;
	}
	
	private void setConcatAttributes(List<ParametroAutorizacionLiquidacionDetalle> detalles){
		if(detalles != null && detalles.size()>0){
			StringBuilder oficinasConcatBuilder = new StringBuilder();;
			StringBuilder estadosConcatBuilder = new StringBuilder();
			StringBuilder proveedoresConcatBuilder = new StringBuilder();
			StringBuilder usuariosSolConcatBuilder = new StringBuilder();
			StringBuilder usuariosAutConcatBuilder = new StringBuilder();
			
			for(ParametroAutorizacionLiquidacionDetalle detalle : detalles ){
				int value = Integer.valueOf(detalle.getTipoRelacion()).intValue(); 
				if(value == TipoParametroAutLiquidacion.ESTADO.getValue()){
					this.concatValue(estadosConcatBuilder, detalle.getIdRelacionado());
				}else if (value == TipoParametroAutLiquidacion.OFICINA.getValue()){
					this.concatValue(oficinasConcatBuilder, detalle.getIdRelacionado());
				}else if (value == TipoParametroAutLiquidacion.PROVEEDOR.getValue()){
					this.concatValue(proveedoresConcatBuilder, detalle.getIdRelacionado());
				}else if (value == TipoParametroAutLiquidacion.USUARIO_AUTORIZADOR.getValue()){
					this.concatValue(usuariosAutConcatBuilder, detalle.getIdRelacionado());
				}else if (value == TipoParametroAutLiquidacion.USUARIO_SOLICITADOR.getValue()){
					this.concatValue(usuariosSolConcatBuilder, detalle.getIdRelacionado());
				}
			}
			this.oficinasConcat = oficinasConcatBuilder.toString();
			this.estadosConcat = estadosConcatBuilder.toString();
			this.proveedoresConcat = proveedoresConcatBuilder.toString();
			this.usuariosSolConcat = usuariosSolConcatBuilder.toString();
			this.usuariosAutConcat = usuariosAutConcatBuilder.toString();
		}
	}
	
	private StringBuilder concatValue(StringBuilder str, Object value){
		if(str.length()==0){
			str.append(value);
		}else{
			str.append(","+value.toString().trim());
		}
		return str;
	}
	
	public void prepareGuardarParametro() throws Exception {
		cargaInfoParaCrearParametro();
	}
	
	@Action(value="guardarParametro",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ALTA),
			@Result(name=INPUT,location=CONTENEDOR_ALTA)
			})
	public String guardarParametro(){
		Boolean estatus = (this.parametroEstatus.intValue() == 1)?Boolean.TRUE:Boolean.FALSE;
		this.parametro.setEstatus(estatus);
		String mensajeError = this.liquidacionSiniestroService.guardarParametroAutorizacion(parametro, oficinasConcat, estadosConcat, proveedoresConcat, usuariosSolConcat, usuariosAutConcat);
		if(mensajeError == null){
			super.setMensajeExito();
		}else{
			super.setMensajeError(mensajeError);
			return INPUT;
		}
		parametro = entidadService.findById(ParametroAutorizacionLiquidacion.class, parametro.getId());
		return SUCCESS;
	}
	
	
	@Action(value="buscarParametros",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_GRID),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA)
			})
	public String buscarParametros(){
		if(this.filtroParametros==null){
			this.resultados = new ArrayList<ParamAutLiquidacionRegistro>();
		}else{
			this.resultados = this.liquidacionSiniestroService.buscarParametrosAutorizacion(this.filtroParametros);
		}
		
		return SUCCESS;
	}



	public ListadoService getListadoService() {
		return listadoService;
	}



	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}



	public EntidadService getEntidadService() {
		return entidadService;
	}



	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}



	public LiquidacionSiniestroService getLiquidacionSiniestroService() {
		return liquidacionSiniestroService;
	}



	public void setLiquidacionSiniestroService(
			LiquidacionSiniestroService liquidacionSiniestroService) {
		this.liquidacionSiniestroService = liquidacionSiniestroService;
	}



	public ParametroAutorizacionLiquidacion getParametro() {
		return parametro;
	}



	public void setParametro(ParametroAutorizacionLiquidacion parametro) {
		this.parametro = parametro;
	}



	public String getOficinasConcat() {
		return oficinasConcat;
	}



	public void setOficinasConcat(String oficinasConcat) {
		this.oficinasConcat = oficinasConcat;
	}



	public String getEstadosConcat() {
		return estadosConcat;
	}



	public void setEstadosConcat(String estadosConcat) {
		this.estadosConcat = estadosConcat;
	}



	public String getProveedoresConcat() {
		return proveedoresConcat;
	}



	public void setProveedoresConcat(String proveedoresConcat) {
		this.proveedoresConcat = proveedoresConcat;
	}



	public String getUsuariosSolConcat() {
		return usuariosSolConcat;
	}



	public void setUsuariosSolConcat(String usuariosSolConcat) {
		this.usuariosSolConcat = usuariosSolConcat;
	}



	public String getUsuariosAutConcat() {
		return usuariosAutConcat;
	}



	public void setUsuariosAutConcat(String usuariosAutConcat) {
		this.usuariosAutConcat = usuariosAutConcat;
	}

	public Map<String, String> getOrigenLiquidacion() {
		return origenLiquidacion;
	}

	public void setOrigenLiquidacion(Map<String, String> origenLiquidacion) {
		this.origenLiquidacion = origenLiquidacion;
	}

	public Map<String, String> getTipoLiquidacion() {
		return tipoLiquidacion;
	}

	public void setTipoLiquidacion(Map<String, String> tipoLiquidacion) {
		this.tipoLiquidacion = tipoLiquidacion;
	}

	public Map<String, String> getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Map<String, String> tipoPago) {
		this.tipoPago = tipoPago;
	}

	public Map<String, String> getEstatusLiquidacion() {
		return estatusLiquidacion;
	}

	public void setEstatusLiquidacion(Map<String, String> estatusLiquidacion) {
		this.estatusLiquidacion = estatusLiquidacion;
	}

	public Map<String, String> getCriterioComparacion() {
		return criterioComparacion;
	}

	public void setCriterioComparacion(Map<String, String> criterioComparacion) {
		this.criterioComparacion = criterioComparacion;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public List<PrestadorServicioRegistro> getListPrestadorServicio() {
		return listPrestadorServicio;
	}

	public void setListPrestadorServicio(
			List<PrestadorServicioRegistro> listPrestadorServicio) {
		this.listPrestadorServicio = listPrestadorServicio;
	}

	public PrestadorDeServicioService getPrestadorServicioService() {
		return prestadorServicioService;
	}

	public void setPrestadorServicioService(
			PrestadorDeServicioService prestadorServicioService) {
		this.prestadorServicioService = prestadorServicioService;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public String getNombreusuarioActual() {
		return nombreusuarioActual;
	}

	public void setNombreusuarioActual(String nombreusuarioActual) {
		this.nombreusuarioActual = nombreusuarioActual;
	}

	public Map<String, String> getEstatus() {
		return estatus;
	}

	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}

	public Boolean getEsCondicionMontos() {
		return esCondicionMontos;
	}

	public void setEsCondicionMontos(Boolean esCondicionMontos) {
		this.esCondicionMontos = esCondicionMontos;
	}

	public Boolean getEsCondicionVigencia() {
		return esCondicionVigencia;
	}

	public void setEsCondicionVigencia(Boolean esCondicionVigencia) {
		this.esCondicionVigencia = esCondicionVigencia;
	}

	public Boolean getEsRangoMontos() {
		return esRangoMontos;
	}

	public void setEsRangoMontos(Boolean esRangoMontos) {
		this.esRangoMontos = esRangoMontos;
	}

	public Boolean getEsRangoVigencia() {
		return esRangoVigencia;
	}

	public void setEsRangoVigencia(Boolean esRangoVigencia) {
		this.esRangoVigencia = esRangoVigencia;
	}

	public Boolean getEsParametroSoloLectura() {
		return esParametroSoloLectura;
	}

	public void setEsParametroSoloLectura(Boolean esParametroSoloLectura) {
		this.esParametroSoloLectura = esParametroSoloLectura;
	}

	public Long getParametroId() {
		return parametroId;
	}

	public void setParametroId(Long parametroId) {
		this.parametroId = parametroId;
	}

	public ParamAutLiquidacionDTO getFiltroParametros() {
		return filtroParametros;
	}

	public void setFiltroParametros(ParamAutLiquidacionDTO filtroParametros) {
		this.filtroParametros = filtroParametros;
	}

	public List<ParamAutLiquidacionRegistro> getResultados() {
		return resultados;
	}

	public void setResultados(List<ParamAutLiquidacionRegistro> resultados) {
		this.resultados = resultados;
	}

	public Integer getParametroEstatus() {
		return parametroEstatus;
	}

	public void setParametroEstatus(Integer parametroEstatus) {
		this.parametroEstatus = parametroEstatus;
	}
	
}
