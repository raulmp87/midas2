package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.io.File;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.pagos.notasDeCredito.NotasCreditoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/recuperacionProveedor/notasDeCredito")
public class RecuperacionNotasCreditoAction extends BaseAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String 	CONTENEDOR_NOTAS	 =	"/jsp/siniestros/recuperacion/notasDeCredito/contenedorRecuperacionNotasDeCredito.jsp";
	private final String 	LISTADO_NOTAS_GRID	 =	"/jsp/siniestros/recuperacion/notasDeCredito/listadoRecuperacionNotasDeCreditoGrid.jsp";
	private final String CONTENEDOR_MENSAJES_ERROR = "/jsp/siniestros/pagos/notasCredito/recepcionNotaCreditoResultadoValidacion.jsp";
	
	private File archivoNotasCredito;
	private Long batchId;
	private Boolean esEditable;
	private Boolean existeFacturaErronea;
	private String extensionArchivo;
	private Boolean soloLectura;
	private Long notaCreditoId;
	
	private Long idValidacionNotaCredito;
	private List<EnvioValidacionFactura> listaNotasCredito;
	private List<MensajeValidacionFactura>  listaValidacionesNotaCredito;
	private Integer idProveedor;
	private String folioFactura;
	private String nombreProveedor;
	private Integer numNotasCargadas;
	private RecuperacionProveedor recuperacion;
	private String notasCreditoSeleccionadasConcat;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("notasCreditoServiceEJB")
	private NotasCreditoService notasCreditoService; 
	
	@Autowired
	@Qualifier("recepcionFacturaServiceEJB")
	private RecepcionFacturaService recepcionFacturaService;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
	@Action(value = "cancelar", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_NOTAS),
			@Result(name = INPUT, location = CONTENEDOR_NOTAS) })
	public String cancelarNotaCredito() {
		this.inicializaContenedor();
		this.notasCreditoService.cancelarNotaCreditoRecuperacion(this.notaCreditoId);
		return SUCCESS;
	}
	

    @Action(value="cargarArchivo",	
			results = {@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","batchId,tipoMensaje,mensaje,numNotasCargadas,^recuperacion\\.id"})
	})
	public String cargarArchivoNotasCredito() {
		//***********************
		listaNotasCredito = notasCreditoService.procesarNotasCreditoRecuperacionProveedor(this.idProveedor, 
				archivoNotasCredito,extensionArchivo);
		
//		if(listaNotasCredito.size() > 0){
//			RecuperacionProveedor recuperacion = listaNotasCredito.get(0).getFactura().getRecuperacion();
//			Long idRecuperacion = recuperacion.getId();
//		}
		
		numNotasCargadas = listaNotasCredito.size();
		this.batchId = listaNotasCredito.size() > 0 ?listaNotasCredito.get(0).getIdBatch():0L;
		
		Boolean existeFacturaErronea = notasCreditoService.existenFacturasEstatus(listaNotasCredito, 
				DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		if(existeFacturaErronea == null 
				|| existeFacturaErronea){
			
			this.setMensajeError("Ha ocurrido un error con el procesamiento de una o más Notas de Crédito.");
			
		}else
		{
			this.setMensajeExito();
		}
		//******************************
		
		return SUCCESS;
	}
	
	@Action(value = "mostrarNotasPendientes", results = {
			@Result(name = SUCCESS, location = LISTADO_NOTAS_GRID),
			@Result(name = INPUT, location = CONTENEDOR_NOTAS) })
	public String mostrarNotasPendientes() {
		if(this.batchId == null){
			List<RecuperacionProveedor> recuperaciones =this.entidadService.findByProperty(RecuperacionProveedor.class, "id", this.recuperacion.getId());
			if(recuperaciones !=null &&recuperaciones.size()>0){
				this.recuperacion = recuperaciones.get(0);
				if(this.recuperacion.getNotasdeCredito()!=null && this.recuperacion.getNotasdeCredito().size()>0){
					this.listaNotasCredito = this.recepcionFacturaService.creaEnvioValidacion(this.recuperacion.getNotasdeCredito());
					this.recepcionFacturaService.agregaEstatusDescripcion(this.listaNotasCredito);
				}else{
					Integer idBeneficiario = this.recuperacion.getOrdenCompra().getIdBeneficiario();
				    this.listaNotasCredito = this.notasCreditoService.obtenerNotasCreditoPendientesPorProovedor(idBeneficiario);
				    numNotasCargadas = listaNotasCredito.size();
				    this.recepcionFacturaService.agregaEstatusDescripcion(this.listaNotasCredito);
				}
			} else{
				Integer idBeneficiario = this.recuperacion.getOrdenCompra().getIdBeneficiario();
			    this.listaNotasCredito = this.notasCreditoService.obtenerNotasCreditoPendientesPorProovedor(idBeneficiario);
			    numNotasCargadas = listaNotasCredito.size();
			    this.recepcionFacturaService.agregaEstatusDescripcion(this.listaNotasCredito);
			}
		}else{
			this.listaNotasCredito = this.entidadService.findByProperty(EnvioValidacionFactura.class, "idBatch", this.batchId);
			this.recepcionFacturaService.agregaEstatusDescripcion(this.listaNotasCredito);
		}
		return SUCCESS;
	}
	
	@Action(value = "consultar", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_NOTAS),
			@Result(name = INPUT, location = CONTENEDOR_NOTAS) })
	public String consultarNotasCreditoDeRecuperacion() {
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_NOTAS),
			@Result(name = INPUT, location = CONTENEDOR_NOTAS) })
	public String mostrarRegistroDeNotaDeCredito() {
	    this.inicializaContenedor();
		return SUCCESS;
	}
	
	
	private void inicializaContenedor(){
		this.recuperacion = this.entidadService.findById(RecuperacionProveedor.class, this.recuperacion.getId());
	    Integer idBeneficiario = this.recuperacion.getOrdenCompra().getIdBeneficiario();
	    PrestadorServicio proveedor = this.entidadService.findById(PrestadorServicio.class, idBeneficiario);
	    this.idProveedor = proveedor.getId();
	    this.nombreProveedor = proveedor.getPersonaMidas().getNombre();
	    this.folioFactura = this.recuperacion.getOrdenCompra().getFactura();
	    this.esEditable = (this.recuperacion.getNotasdeCredito()!=null && this.recuperacion.getNotasdeCredito().size()>0)?Boolean.FALSE:Boolean.TRUE;
	}
	
	@Action(value = "registrar", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_NOTAS),
			@Result(name = INPUT, location = CONTENEDOR_NOTAS) })
	public String validarRegistrarNotaCredito() {
		this.notasCreditoService.registrarNotasParaRecuperaciones(this.batchId, this.recuperacion.getId(), 
				notasCreditoSeleccionadasConcat, idProveedor);
		this.inicializaContenedor();
		this.setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "verMensajes", results = {
			@Result(name = SUCCESS, location = CONTENEDOR_MENSAJES_ERROR),
			@Result(name = INPUT, location = CONTENEDOR_NOTAS) })
	public String verMensajes() {
		this.listaValidacionesNotaCredito = recepcionFacturaService.obtenerResultadosValidacion(idValidacionNotaCredito);
		return SUCCESS;
	}
	
	
	public File getArchivoNotasCredito() {
		return archivoNotasCredito;
	}

	public void setArchivoNotasCredito(File archivoNotasCredito) {
		this.archivoNotasCredito = archivoNotasCredito;
	}

	public Long getBatchId() {
		return batchId;
	}

	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}

	public Boolean getEsEditable() {
		return esEditable;
	}

	public void setEsEditable(Boolean esEditable) {
		this.esEditable = esEditable;
	}

	public Boolean getExisteFacturaErronea() {
		return existeFacturaErronea;
	}

	public void setExisteFacturaErronea(Boolean existeFacturaErronea) {
		this.existeFacturaErronea = existeFacturaErronea;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}

	public String getFolioFactura() {
		return folioFactura;
	}

	public void setFolioFactura(String folioFactura) {
		this.folioFactura = folioFactura;
	}

	public Long getIdValidacionNotaCredito() {
		return idValidacionNotaCredito;
	}

	public void setIdValidacionNotaCredito(Long idValidacionNotaCredito) {
		this.idValidacionNotaCredito = idValidacionNotaCredito;
	}

	public List<EnvioValidacionFactura> getListaNotasCredito() {
		return listaNotasCredito;
	}


	public void setListaNotasCredito(List<EnvioValidacionFactura> listaNotasCredito) {
		this.listaNotasCredito = listaNotasCredito;
	}


	public List<MensajeValidacionFactura> getListaValidacionesNotaCredito() {
		return listaValidacionesNotaCredito;
	}

	public void setListaValidacionesNotaCredito(
			List<MensajeValidacionFactura> listaValidacionesNotaCredito) {
		this.listaValidacionesNotaCredito = listaValidacionesNotaCredito;
	}

	public String getNombreProveedor() {
		return nombreProveedor;
	}

	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public Integer getNumNotasCargadas() {
		return numNotasCargadas;
	}

	public void setNumNotasCargadas(Integer numNotasCargadas) {
		this.numNotasCargadas = numNotasCargadas;
	}

	public RecuperacionProveedor getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionProveedor recuperacion) {
		this.recuperacion = recuperacion;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public NotasCreditoService getNotasCreditoService() {
		return notasCreditoService;
	}

	public void setNotasCreditoService(NotasCreditoService notasCreditoService) {
		this.notasCreditoService = notasCreditoService;
	}

	public RecepcionFacturaService getRecepcionFacturaService() {
		return recepcionFacturaService;
	}

	public void setRecepcionFacturaService(
			RecepcionFacturaService recepcionFacturaService) {
		this.recepcionFacturaService = recepcionFacturaService;
	}


	public Integer getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}


	public Boolean getSoloLectura() {
		return soloLectura;
	}


	public void setSoloLectura(Boolean soloLectura) {
		this.soloLectura = soloLectura;
	}


	public String getNotasCreditoSeleccionadasConcat() {
		return notasCreditoSeleccionadasConcat;
	}


	public void setNotasCreditoSeleccionadasConcat(
			String notasCreditoSeleccionadasConcat) {
		this.notasCreditoSeleccionadasConcat = notasCreditoSeleccionadasConcat;
	}


	public Long getNotaCreditoId() {
		return notaCreditoId;
	}


	public void setNotaCreditoId(Long notaCreditoId) {
		this.notaCreditoId = notaCreditoId;
	}
	
}
