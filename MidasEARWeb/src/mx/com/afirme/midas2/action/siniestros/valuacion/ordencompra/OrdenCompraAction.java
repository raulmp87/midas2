package mx.com.afirme.midas2.action.siniestros.valuacion.ordencompra;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.OrdenCompraRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.BandejaOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ConceptoAjusteDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.DetalleOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImpresionOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.OrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.UtileriasService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


/**
 * Clase que maneja todas las peticiones del modulo de ordenes de compra
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:43:36 a.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/valuacion/ordencompra")


public class OrdenCompraAction extends BaseAction implements Preparable{
	
	private static final long serialVersionUID = 3372503354674128150L;
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("conceptoAjusteServiceEJB")
	private ConceptoAjusteService conceptoAjusteService;
	
	@Autowired
	@Qualifier("ordenCompraServiceEJB")
	private OrdenCompraService ordenCompraService;
	

	@Autowired
	@Qualifier("ordenCompraAutorizacionServiceEJB")
	private OrdenCompraAutorizacionService ordenCompraAutorizacionService;
	
	@Autowired
	@Qualifier("perdidaTotalServiceEJB")
	private PerdidaTotalService perdidaTotalService;
	
	
	@Autowired
	@Qualifier("utileriasServiceEJB")
	private UtileriasService utileriasService;
	
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("movimientoSiniestroServiceEJB")
	private MovimientoSiniestroService movimientoSiniestroService;
	
	
	private Long validOnMillis;
	private Long recordFromMillis;
	private TransporteImpresionDTO  transporte;
	private ImpresionOrdenCompraDTO impresionDTO;
	private final String TIPO_CONCEPTO_DANIOS = "DA";
	private final String ROL_ALTACONCEPTO_CEROS_CABINERO = "Rol_M2_Cabinero";
	private final String ROL_ALTACONCEPTO_CEROS_AJUSTADOR = "Rol_M2_Ajustador";
	private Map<Long, String> bancos;


	private String mensajePantalla  = "" ;
	private final String LISTADOORDENCOMPRA =	"/jsp/siniestros/valuacion/ordencompra/listadoOrdenCompra.jsp";
	private final String CONTENEDOR_ORDENCOMPRADETALLE =	"/jsp/siniestros/valuacion/ordencompra/contenedorOrdenCompraDetalle.jsp";
	private final String CONTENEDOR_ORDENCOMPRA =	"/jsp/siniestros/valuacion/ordencompra/contenedorOrdenCompra.jsp";
	private final String LISTACONCEPTOS =	"/jsp/siniestros/valuacion/ordencompra/listadoOrdenCompraConceptos.jsp";
	private final String LISTACONCEPTOSCONSULTA =	"/jsp/siniestros/valuacion/ordencompra/listadoConsultaOrdenCompraConceptos.jsp";
	private final String CONTENEDOR_ORDENCOMPRA_POR_IDEMIZACION 	=	"/jsp/siniestros/valuacion/ordencompra/contenedorOrdenCompraPorIndemizacion.jsp";
	private final String CONTENEDOR_DETALLEORDENCOMPRA_IDEMNIZACION 	=	"/jsp/siniestros/valuacion/ordencompra/contenedorOrdenCompraIndemnizacion.jsp";
	private final String BUSQUEDA_ASIGNACION_OC	="/jsp/siniestros/valuacion/ordencompra/contenedorBusquedaAsignacionOrdenCompra.jsp";
	private final String LISTADO_BUSQUEDA_ASIGNACION_OC	="/jsp/siniestros/valuacion/ordencompra/listadoBusquedaAsignacionOrdenCompra.jsp";
	private final String BANDEJA_ORDENCOMPRA	="/jsp/siniestros/valuacion/ordencompra/contenedorListadoOrdenCompra.jsp";
	private final String LISTADO_BANDEJA_ORDENCOMPRA	="/jsp/siniestros/valuacion/ordencompra/listadoBandejaOrdenCompra.jsp";
	
		
	private final String PANTALLA_ORIGEN_INDEMNIZACON	=	"INDEMNIZACION";
	private final String PANTALLA_ORIGEN_ORDENCOMPRA	=	"ORDEN_COMPRA";
	private static final String FN_NO_AFECTACION_RESERVA = "FN_M2_SN_No_Permite_Afectacion_Reserva";
	private String nombrePrestadorServicio ;
	
	private static final String[] COLUMNAS_GRID_ORDEN_COMPRA= {"compra.id", "numSiniestroCabina", "numReporteCabina", 
		"terminoAjusteSiniestro", "cobertura.descripcion", "totalOrdenCompra", "tipoPago", "tipoOrdenCompra", 
		"noAplica", "noAplica", "ordenPago.id",  "persona.nombre", "compra.nomBeneficiario", "compra.estatus"};		

	
	
	/**
	 * concepto o detalle capturado o para ser editado.
	 */
	private DetalleOrdenCompra detalleOrdenCompra;

	private String modificadoPor;
	private String creadoPor;
	
	/**
	 * lista de terceros afectados, debe tomarse de los pases de atencion
	 */
	private OrdenCompra ordenCompra;
	private OrdenCompraDTO filtroContenedor;
	private List<OrdenCompraDTO> listaOrdenCompra;
	private List<DetalleOrdenCompraDTO> listaDetalleDTO;
	 
	 
	 private String terminoAjuste;
	 private String codigoTerminoAjuste;
	 private String terminoSiniestro;
	 
	 private String formaPago;
	 
	 private String numReporte;
	 private String numSiniestro;
	 private Date fechaOcurrido;
	 
	private Long idOrdenCompra;
	private Long idOrdenCompraDetalle;
	private Long idEstimacionReporteCabina;
	private Long idIndemnizacion;
	private String tipoIndemnizacion;
	
	
	private String cveSubTipoCalculoCobertura;
	private Long idReporteCabina;
	private Long idPrestador;
	private Long idConcepto;
	private String origen;
	private String pantallaOrigen;
	private Long idDetalleOrdenSeleccionado;
	
	private String coberturaCompuesta;
	private Long idCoberturaReporteCabina;
	
	/*Datos solo para control de pantallas , no estan mapeados en base de datos */
	private String terceroAfectado;
	
	private String autorizadoPor;

	boolean soloLectura;
	boolean modoConsulta;

	boolean esAutorizada;
	private String mensajeMostrar;

	boolean altaCeros;

	boolean soloIndemnizacion;
	boolean crearIndemnizacion;
	boolean aplicaPagoDaños =true;
	
	private String methodName;
	private String namespace;
	private Boolean bandejaPorAutorizar;
	
	private PerdidaTotalFiltro 	filtroPT;
	
	
	private String tituloOrdenCompra;
	private String tituloOrdenCompraDetalle;
	private BigDecimal reservaDisponible;
	
	

	/**
	 * reporte cabina al que esta relacionadas las ordenes de compra
	 */
	private ReporteCabina reporteCabina;
	/**
	 * tipo de orden compra, ORDEN_COMPRA, GASTO_AJUSTE
	 */
	private String tipo;

	/**
	 * CATALOGOS 
	 * 
	 * */
	private Map<String,String> tipoPersona;
	private Map<String,String> reembolsoGastoAmap;
	
	private Map<String,String> tipoPagoMap;
	private Map<String,String> tipoOrdenCompraMap;
	private Map<String,String> tipoAfectado;
	private Map<String,String> coberturaMap;
	private Map<String,String> estautsMap;
	private Map<String,String> tipoProveedorMap; 
	private Map<Long,String> proveedorMap;
	private Map<Long,String> terceroAfectadoMap;
	private Map<Long,String> conceptoPagoMap;
	private Map<String,String> tipoIndemnizacionMap;
	private ImportesOrdenCompraDTO importesDTO;
	private ImportesOrdenCompraDTO importesPorConceptoDTO;
	private ConceptoAjuste conceptoAjuste;
	private ConceptoAjusteDTO conceptoAjusteDTO;
	
	
	
	/*
	 * BANDEJA ORDENES COMPRA
	 * 
	 * */
	private List<BandejaOrdenCompraDTO> listaBandejaOrdenCompra;
	private Map<Long,String> oficinas;
	private Map<String,String> terminoAjusteMap;
	BandejaOrdenCompraDTO filtroBandeja ;
	
	private List<OrdenCompraRecuperacionDTO> listaOrdenCompraRecuperacionDTO;
	private OrdenCompraRecuperacionDTO ordenCompraRecuperacionDTO;
	private OrdenCompraDTO filtroBusquedaRecuperacion;
	private Long idPaseAtencion;
	private String cadenaCoberturas;
	private String cadenaPaseAtencion;
	private String[] lstCoberturas;
	private String[] lstIdCoberturas;
	private  String[]lstIdPaseAtencion;
	private  String[]lstcoberturaEstimacion;
	private List<Long> coberturasId ;
	private List<Long>   pasesAtencionId;
	private String fieldName;
	
	/**
	 * action para procesar solicitudes de cancelaci�n de una �rden de compra
	 * 
	 * @param idOrdenCompra    identificador de la orden a ser cancelada.
	 */
	@Action(value="cancelarOrdenCompra",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA)
			})
	public String cancelarOrdenCompra(){
		 Long id=this.ordenCompra.getId();	
		 try {
			this.ordenCompraService.cancelarOrdenPago(id);
		} catch (Exception e) {
			super.setMensajeError(e.getMessage());
			return INPUT;
		}
		 super.setMensajeExitoPersonalizado("Se Cancelo la Orden de Compra No. "+ordenCompra.getId());
		 return SUCCESS;
		
	}
	/**
	 * metodo que procesa las peticiones para mostrar el detalle de una orden de
	 * compra o crear una nueva en caso que no reciba como parametro el numero de
	 * orden.
	 * 
	 * @param idReporteCabina    parametro opcional ya que no recibir el parametro se
	 * debe mostrar la pantalla de detalleOrdenCompra en blanco para ser capturada.
	 * @param idOrdenCompra    id del reporte de cabina al que esta relacionado la
	 * orden de compra.
	 */
	@Action(value="detalleOrdenCompra",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRADETALLE),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String detalleOrdenCompra(){
		this.prepareOrdenCompra();
		this.soloLectura=false;
		this.prepareDetalleOrdenCompra(this.idOrdenCompra);
		if(this.esAutorizada && !StringUtil.isEmpty(this.mensajeMostrar)){
			super.setMensaje(this.mensajeMostrar);
			
		}
		return SUCCESS;
	}
	@Action (value = "calcularImportes", results={
	                  @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^importesDTO.*"}),
	                  @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^importesDTO.*"})
	            })
	      public String calcularImportes(){
				this.importesDTO = this.ordenCompraService.calcularImportes(this.idOrdenCompra,null,false);
	            return SUCCESS;
	      }
	
	
	@Action (value = "validarCalculoImportes", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeMostrar,importesPorConceptoDTO.*"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeMostrar,importesPorConceptoDTO.*"})
      })
	public String validarCalculoImportes(){
			this.mensajeMostrar=null;
			this.importesPorConceptoDTO = this.ordenCompraService.calcularImportes(this.idOrdenCompra,null,true);
			boolean resultado =false;
			try {
				resultado =  this.ordenCompraService.validaImportesOrdenCompraContraConceptos(this.idOrdenCompra,null,null);
			} catch (Exception e) {
				this.mensajeMostrar = e.getMessage();
			}
	      return SUCCESS;
	}


	@Action (value = "validarCalculoImportesPorConcepto", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeMostrar"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeMostrar"})
      })
	public String validarCalculoImportesPorConcepto(){
			BigDecimal variacion = new BigDecimal (1);
			this.mensajeMostrar=null;
			boolean resultado =false;
			try {
				resultado =  this.ordenCompraService.validaImportesDetalleOrdenCompraContraConcepto(this.detalleOrdenCompra,variacion);
			} catch (Exception e) {
				this.mensajeMostrar = e.getMessage();
			}
	      return SUCCESS;
	}

	
	
	
	@Action (value = "cargaConcepto", results={
            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^conceptoAjusteDTO.*"}),
            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^conceptoAjusteDTO.*"})
      })
		public String cargaConcepto(){
		
				conceptoAjusteDTO= new ConceptoAjusteDTO();
				this.conceptoAjuste = this.entidadService.findById(ConceptoAjuste.class, this.idConcepto);
				conceptoAjusteDTO.setAplicaIsr(conceptoAjuste.getAplicaIsr());
				conceptoAjusteDTO.setAplicaIva(conceptoAjuste.getAplicaIva());
				conceptoAjusteDTO.setAplicaIvaRetenido(conceptoAjuste.getAplicaIvaRetenido());
				conceptoAjusteDTO.setCategoria(conceptoAjuste.getCategoria());
				conceptoAjusteDTO.setEstatus(conceptoAjuste.getEstatus());
				conceptoAjusteDTO.setFechaCambioEstatus(conceptoAjuste.getFechaCambioEstatus());
				conceptoAjusteDTO.setId(conceptoAjuste.getId());
				conceptoAjusteDTO.setNombre(conceptoAjuste.getNombre());
				conceptoAjusteDTO.setPorcIsr(conceptoAjuste.getPorcIsr());
				conceptoAjusteDTO.setPorcIva(conceptoAjuste.getPorcIva());
				conceptoAjusteDTO.setPorcIvaRetenido(conceptoAjuste.getPorcIvaRetenido());
				conceptoAjusteDTO.setTipoConcepto(conceptoAjuste.getTipoConcepto());
		      return SUCCESS;
		}
	
	
	
	
	
	
	/**
	 * metodo que procesa las peticiones para mostrar como solo lectura el detalle de una orden de
	 * compra o crear una nueva en caso que no reciba como parametro el numero de
	 * orden.
	 * 
	 * @param idReporteCabina    parametro opcional ya que no recibir el parametro se
	 * debe mostrar la pantalla de detalleOrdenCompra en blanco para ser capturada.
	 * @param idOrdenCompra    id del reporte de cabina al que esta relacionado la
	 * orden de compra.
	 */
	@Action(value="consultarDetalleOrdenCompra",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRADETALLE),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String consultarDetalleOrdenCompra(){
		this.prepareOrdenCompra();
		this.soloLectura=true;
		this.prepareDetalleOrdenCompra(this.idOrdenCompra);
		
		if (this.isEsAutorizada()){
			super.setMensaje(this.mensajeMostrar);
		}
		return SUCCESS;
	}

	/**
	 * recibe la peticion para eliminar un concepto
	 * 
	 * @param idDetalleOrdenCompra    identificador del concepto o detalle a eliminar
	 */
	@Action(value="eliminarConcepto",results={
			@Result(name=SUCCESS,location=LISTACONCEPTOS),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String eliminarConcepto(){
		 
		DetalleOrdenCompra  detaOrdenCompra =entidadService.findById(DetalleOrdenCompra.class,this.idOrdenCompraDetalle);
		ordenCompraService.eliminarConcepto(detaOrdenCompra.getId());		
		this.listaDetalleDTO=ordenCompraService.obtenerListaDetallesOrdenDTO(detaOrdenCompra.getOrdenCompra().getId(),null,false);
		this.prepareDetalleOrdenCompra(detaOrdenCompra.getOrdenCompra().getId());
		return SUCCESS;	
		
	}

	/**
	 * metodo que maneja las peticiones de guardado de una orden de compra.
	 * 
	 * @param ordeCompra    mapea la entidad de �rden compra a la pantalla
	 */
	@Action(value="guardar",results={
			@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String guardar(){
		this.prepareOrdenCompra();
		//OrdenCompra ordeCompra
		prepareDatosFormulario();
		if (null==ordenCompra.getId() && StringUtil.isEmpty(ordenCompra.getOrigen()) ){
			ordenCompra.setOrigen("MANUAL");
			ordenCompra.setEstatus( "Tramite");
		} 
		ReporteCabina reporteCabina =  entidadService.findById(ReporteCabina.class, this.idReporteCabina);
		ordenCompra.setReporteCabina(reporteCabina);
		ordenCompra.setIdCoberturaCompuesta(this.coberturaCompuesta);
		ordenCompra.setCveSubTipoCalculoCobertura(this.cveSubTipoCalculoCobertura);
		if(null!=this.idCoberturaReporteCabina ){
			CoberturaReporteCabina coberturaReporteCabina = entidadService.findById(CoberturaReporteCabina.class, this.idCoberturaReporteCabina);
			ordenCompra.setCoberturaReporteCabina(coberturaReporteCabina);
		}
		String msj  = this.ordenCompraService.validarCrearOrdenCompra(ordenCompra);
		if(!StringUtil.isEmpty(msj)){
			if(null!=ordenCompra.getId())
				this.prepareDetalleOrdenCompra(ordenCompra.getId());		
			super.setMensajeError(msj);
			return INPUT;	
		}
		try { 
			
			ordenCompraService.guardarOrdenCompra(ordenCompra);	
		
		}catch(Exception e){
			super.setMensajeError("Error al guardar Orden "+ e.getMessage());
			return SUCCESS;
		}
		try {
			this.prepareDetalleOrdenCompra(ordenCompra.getId());
		}catch(Exception e){
			super.setMensajeError("Error al Cargar Orden de Pago "+ e.getMessage());
			return SUCCESS;
		}
		super.setMensajeExitoPersonalizado(getText("midas.siniestros.valuacion.ordencompra.exito.guardar"));
		return INPUT;		
	}


	/**
	 * Recibe como par�metro un id de �rden de compra y genera un jasper con la
	 * impresi�n en PDF.
	 * 
	 * @param idOrdenCompra    par�metro del id de orden de compra a ser impreso.
	 */
	
	
	/**
	 * Manda a exportar en un documento PDF la información de la condición especial
	 * @return
	 */
	@Action(value = "imprimirOrdenCompra", results = {
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirOrdenCompra(){
		try{
			transporte = ordenCompraService.imprimirOrdenCompra(this.idOrdenCompra);
			transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
			transporte.setContentType("application/pdf");
			String fileName = "OrdenCompra"+ "_" +this.idOrdenCompra.toString() + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
			transporte.setFileName(fileName);
		} 
		catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	/**
	 * consulta los conceptos o detalleOrdenCompra relacionados a una orden de compra
	 * y los muestra en un grid listadoOrdenCompraConceptos.jsp
	 * 
	 * @param idOrdenCompra    identificador de la orden de compra para buscar sus
	 * conceptos relacionados
	 */
	@Action(value="buscarDetalleCompra",results={
			@Result(name=SUCCESS,location=LISTACONCEPTOS),
			@Result(name=INPUT,location=LISTACONCEPTOSCONSULTA)
			})
	public String buscarDetalleCompra(){	
		
		this.listaDetalleDTO=ordenCompraService.obtenerListaDetallesOrdenDTO(this.ordenCompra.getId(),null,false);
		try {
			if(this.soloLectura)
		
			return INPUT;	
		
		else
			return SUCCESS;	
		}catch (Exception e){
			return SUCCESS;	
		}
		
		
	}
	
	

	/**
	 * consulta los conceptos o detalleOrdenCompra relacionados a una orden de compra
	 * y los muestra en un grid listadoOrdenCompraConceptos.jsp
	 * 
	 * @param idOrdenCompra    identificador de la orden de compra para buscar sus
	 * conceptos relacionados
	 */
	@Action(value="agregarConcepto",results={
			@Result(name=SUCCESS,location=LISTACONCEPTOS),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String agregarConcepto(){	
		ConceptoAjuste conceptoAjuste = entidadService.findById(ConceptoAjuste.class, this.idConcepto);
		
		if (null!=this.idDetalleOrdenSeleccionado &&  this.idDetalleOrdenSeleccionado>0){
			//valida que el id exista
			DetalleOrdenCompra detalle = this.entidadService.findById(DetalleOrdenCompra.class, idDetalleOrdenSeleccionado);
			if (null!=detalle)
				ordenCompraService.eliminarConcepto(idDetalleOrdenSeleccionado);		
		}
		this.detalleOrdenCompra.setConceptoAjuste(conceptoAjuste);
		ordenCompraService.guardarConcepto(this.detalleOrdenCompra, this.ordenCompra.getId());
		this.listaDetalleDTO=ordenCompraService.obtenerListaDetallesOrdenDTO(this.ordenCompra.getId(),null,false);
		prepareDetalleOrdenCompra(this.ordenCompra.getId());	
		super.setMensaje("Gardd");
		return SUCCESS;	
	}
 
	/**
	 * action que direcciona a un jsp con un grid para listar las �rdenes de compra,
	 * al jsp listadoOrdenCompra.jsp
	 * 
	 * @param idReporteCabina    id del reporte de cabina del cual se consultar�n las
	 * �rdenes de compra a ser mostradas en la lista
	 */
	 @Action(value="listadoOrdenesCompra",results={
				@Result(name=SUCCESS,location=LISTADOORDENCOMPRA),
				@Result(name=INPUT,location=LISTADOORDENCOMPRA)
				})
	public String listadoOrdenesCompra(){
		 if (this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE) ||  this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE) ){
			 listaOrdenCompra = ordenCompraService.obtenerOrdenesCompraSiniestro(this.idReporteCabina,OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE.toString());
			 listaOrdenCompra.addAll( ordenCompraService.obtenerOrdenesCompraSiniestro(this.idReporteCabina,OrdenCompraService.TIPO_GASTOAJUSTE.toString()));
		 }else{
			 listaOrdenCompra = ordenCompraService.obtenerOrdenesCompraSiniestro(this.idReporteCabina,OrdenCompraService.TIPO_AFECTACION_RESERVA.toString());

		 }
		 
		 
		 return SUCCESS;	
	}


	 @Action(value="cambioEstatus",results={
				@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA),
				@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA)
				})
	public String cambioEstatus(){
		 this.prepareOrdenCompra();
		 String estatus = ordenCompra.getEstatus();
		 Long id=this.ordenCompra.getId();
		 this.ordenCompra=ordenCompraService.obtenerOrdenCompra(id);
		 this.ordenCompra.setEstatus(estatus);
		 if ( !ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_TRAMITE) && !ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_CANCELADA)){
				String est=ordenCompra.getEstatus();
				super.setMensajeError(getText("midas.siniestros.valuacion.ordencompra.error.estatus")+": "+est );
			 return INPUT;		
		}
		 ordenCompraService.guardarOrdenCompra(ordenCompra);
		 try {
				this.prepareDetalleOrdenCompra(id);
			}catch(Exception e){
				super.setMensajeError("Error al Cargar Orden de Pago "+ e.getMessage());
				return INPUT;
			}
			super.setMensajeExitoPersonalizado(getText("midas.siniestros.valuacion.ordencompra.extio.actualizar"));
			return SUCCESS;
	}
	 
		/**
		 * muestra el listado de �rdenes de compra relacionadas a un siniestro, este
		 * action se invoca desde un bot�n en el reporte cabina. Direcciona a el jsp
		 * contenedorOrdenCompra.jsp
		 * 
		 * @param idReporteCabina    id del reporte de cabina para consultar sus �rdenes
		 * de compra.
		 */  
	 @Action(value="mostrar",results={
				@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA),
				@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA)
				})
	public String mostrar(){
		 //SPV Cabina: validar si el reporte ya fue convertido a siniestro y evitar creacion de ordenes
		 if(usuarioService.tienePermisoUsuarioActual(FN_NO_AFECTACION_RESERVA)){
			 if(idReporteCabina != null){
				 ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);
				 if(reporte != null && reporte.getSiniestroCabina() != null && reporte.getSiniestroCabina().getId() != null){
					 modoConsulta = true;
				 }
			 }
		 }		 
		 return SUCCESS;	
	}
	 
	 
	 
	 
	 
	 @Action(value="mostrarOrdenCompraIndemnizacion",results={
				@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA_POR_IDEMIZACION),
				@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA_POR_IDEMIZACION)
				})
	public String mostrarOrdenCompraIndemnizacion(){
		 this.prepareOrdenCompra();
		 this.crearIndemnizacion=true;
		 this.tipoIndemnizacionMap = new LinkedHashMap<String, String>();
		 EstimacionCoberturaReporteCabina estimacion=this.entidadService.findById(EstimacionCoberturaReporteCabina.class, this.idEstimacionReporteCabina);
		 this.prepareOrdencompraIndemnizacion();
			
		 ReporteCabina reporte=null;
		 if(null==estimacion){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla= getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.sinestimacion");
			 return SUCCESS;	
		 }
		 try {
			  reporte =estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
			  
		 }catch(Exception e){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.gral");
			 return SUCCESS;
		 }
		 
		 if(!this.estimacionCoberturaSiniestroService.esSiniestrada(reporte.getId(),estimacion.getId() )){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.sinsiniestrar");
			 return SUCCESS;
		 }
		 
		 if(this.perdidaTotalService.existeIndemnizacionActiva( this.idEstimacionReporteCabina)){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.indemnizacionactiva");
			 return SUCCESS;
		 }	 
		 if(this.perdidaTotalService.existeIndemnizacionFinalizada( this.idEstimacionReporteCabina)){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.indemnizacionfinalizada");
			 return SUCCESS;
		 }
		 if(this.perdidaTotalService.existeIndemnizacionHGS( this.idEstimacionReporteCabina)){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.indemnizacionhgsactiva");
			 return SUCCESS;
		 }
		 if(this.perdidaTotalService.existeIndemnizacionFinalizadaHGS( this.idEstimacionReporteCabina)){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.indemnizacionhgsfin");
			 return SUCCESS;
		 }
		
		 return SUCCESS;	
	}
	 
	 @Action(value="generarOrdenCompraPorIndemnizacion",results={
				@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRA_POR_IDEMIZACION),
				@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA_POR_IDEMIZACION)
				})
	public String generarOrdenCompraPorIndemnizacion(){

		 ordenCompra.setNomBeneficiario(StringUtil.decodeUri(ordenCompra.getNomBeneficiario()));
			this.prepareOrdencompraIndemnizacion();

		 if(null ==this.idEstimacionReporteCabina){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla="";
			 super.setMensajeError(getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.sinidestimacion"));
			 return SUCCESS;	
		 } 
		 if( StringUtil.isEmpty(this.tipoIndemnizacion) ){
			 this.crearIndemnizacion=true;
			 this.mensajePantalla="";
			 super.setMensajeError(getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.selecciontipo"));
			 return SUCCESS;
		 }
		 if(null==this.ordenCompra || StringUtil.isEmpty(this.ordenCompra.getNomBeneficiario())){
			 this.crearIndemnizacion=true;
			 this.mensajePantalla="";
			 super.setMensajeError(getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.beneficiario"));
			 return SUCCESS;
		 }
			List<ConceptoAjuste> conceptos=this.entidadService.findByProperty(ConceptoAjuste.class, "tipoConcepto", tipoIndemnizacion);
			if(conceptos.isEmpty()){
				this.crearIndemnizacion=true;
				 this.mensajePantalla="";
				 super.setMensajeError(getText("midas.siniestros.valuacion.ordencompra.indemnizacion.error.sinconceptos"));
				 return SUCCESS;
			}
			String errorDeducible = ordenCompraService.validarAplicaDeducible(idEstimacionReporteCabina, null); 
			if(null != errorDeducible){
				this.crearIndemnizacion = true;
				this.mensajePantalla="";
				super.setMensajeError(errorDeducible);
				return SUCCESS;
			}
		 OrdenCompra orden=null;
		 try {
			 orden=this.ordenCompraService.generarOrdenCompraIndemnizacion(this.idEstimacionReporteCabina, this.tipoIndemnizacion,this.ordenCompra.getNomBeneficiario() );
		 }catch (Exception e){
			 this.crearIndemnizacion=false;
			 this.mensajePantalla="error "+ e.getMessage();
			 return SUCCESS;	
		 }
		 
		 if (null!=orden){
			 this.perdidaTotalService.generarIndemnizacion(orden);
			 this.crearIndemnizacion=false;
			 this.mensajePantalla=getText("midas.siniestros.valuacion.ordencompra.indemnizacion.exito.indemnnizacioniniciada");
		 }
		 return SUCCESS;	
	
	 }
	 
	 
	 
	 
	 
	 
	/**
	 * action que direcciona al jsp de detalleOrdenCompra pero en modo captura sin una
	 * orden previamente guardada. Se requiere validar antes de procesar una nueva
	 * orden que haya una reserva disponible.
	 * 
	 * @param idReporteCabina
	 */
	 @Action(value="nuevaOrdenCompra",results={
				@Result(name=SUCCESS,location=CONTENEDOR_ORDENCOMPRADETALLE),
				@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRA)
				})
	public String nuevaOrdenCompra(){
		 
		 if(!StringUtil.isEmpty(this.tipo)){
				if(this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE) ){
					 if(!this.estimacionCoberturaSiniestroService.esSiniestrada(this.idReporteCabina,null )){
						 super.setMensajeError("El reporte cabina no esta siniestrado.");
						 return INPUT;
					 }
				}
		}
		 
		
		 this.prepareOrdenCompra();
		 Usuario user=usuarioService.buscarUsuarioPorNombreUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
		 this.ordenCompra=new OrdenCompra();		 
		 this.ordenCompra.setTipo(this.tipo);
		 //si tipo es afectacion inciso se activa las coberturas
			if(null!=user){
				if(!StringUtil.isEmpty(user.getNombreCompleto())){
					this.creadoPor=user.getNombreCompleto();
				}
			}
			this.modificadoPor="";
		 this.setSoloLectura(false);
		 prepareDatosFormulario();
		 
		 return SUCCESS;
	}
	 
	 private void prepareDatosFormulario(){
		 if(null!=this.idReporteCabina){
				ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, this.idReporteCabina);
				if(null!=reporte){
					this.numReporte= reporte.getNumeroReporte();
					if(null!=reporte.getSiniestroCabina()){
						this.numSiniestro=reporte.getSiniestroCabina().getNumeroSiniestro();
					}
					this.fechaOcurrido = reporte.getFechaHoraOcurrido();
				}
			}
		 
		 
		 if(null!=this.idReporteCabina &&null!=this.ordenCompra&& this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)){
			 this.coberturaMap =listadoService.getCoberturasOrdenCompra(this.idReporteCabina, this.tipo);
			 if(null!=idCoberturaReporteCabina && !StringUtil.isEmpty(cveSubTipoCalculoCobertura))
				 this.terceroAfectadoMap=listadoService.getListasTercerosAfectadorPorCobertura(idCoberturaReporteCabina,this.cveSubTipoCalculoCobertura);

		 }else if(null!=this.idReporteCabina&&null!=this.ordenCompra && this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)){
			 this.ordenCompra.setTipoPago(OrdenCompraService.PAGO_A_PROVEEDOR);
			 this.coberturaMap =listadoService.getCoberturasOrdenCompra(this.idReporteCabina,this.tipo);
		 }else if(null!=this.idReporteCabina&&null!=this.ordenCompra && this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE)){
			 this.ordenCompra.setTipoPago(OrdenCompraService.PAGO_A_BENEFICIARIO);
			 this.coberturaMap =listadoService.getCoberturasOrdenCompra(this.idReporteCabina,this.tipo);
		 }
		 
		 if(null != ordenCompra 
				 && null != ordenCompra.getIdTercero()){
			if( ordenCompra.getTipoPago() != null
					&& ordenCompra.getTipoPago().equals(OrdenCompraService.PAGO_A_BENEFICIARIO)){
				EstimacionCoberturaReporteCabina estimacion = estimacionCoberturaSiniestroService.obtenerEstimacionCoberturaReporteCabina(ordenCompra.getIdTercero());
				ordenCompra.setAplicaDeducible(estimacion.getAplicaDeducible());
			}else{
				ordenCompra.setAplicaDeducible(false);
			}
		 }
		 
		 if (    (usuarioService.tieneRolUsuarioActual(ROL_ALTACONCEPTO_CEROS_CABINERO )   || usuarioService.tieneRolUsuarioActual(ROL_ALTACONCEPTO_CEROS_AJUSTADOR )  )  
				 	&& (this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)   ) ){
				altaCeros = true;
			}
			
	 }
	 
	 
	@Action(value="autorizarOrdenCompra",results={
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "${methodName}",
					"bandejaPorAutorizar","${bandejaPorAutorizar}",
					"esAutorizada","${esAutorizada}",
					"mensajeMostrar","${mensajeMostrar}",
					"idOrdenCompra", "${idOrdenCompra}" ,
					"origen", "${origen}" }),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String  autorizarOrdenCompra(){
		
		this.esAutorizada=true;
		this.mensajeMostrar=ordenCompraAutorizacionService.autorizarOrdenCompra(idOrdenCompra);
		if(this.soloLectura)
			methodName = "consultarDetalleOrdenCompra";
		else {
			
			List<OrdenCompraAutorizacion>  lstAutorizacion= this.entidadService.findByProperty(OrdenCompraAutorizacion.class, "orderCompra.id", idOrdenCompra);
			/*Se valida que la orden de compra tenga al menos una autorizacion, si es asi pasa a estado solo lectura*/
			if(lstAutorizacion.isEmpty()){
				methodName = "detalleOrdenCompra";
			}else{
				this.soloLectura=true;
				methodName = "consultarDetalleOrdenCompra";

			}
		}

		return SUCCESS;
	}
	
	
	
	

	
	@Action(value="rechazarOrdenCompra",results={
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "${methodName}",
					"bandejaPorAutorizar","${bandejaPorAutorizar}",
					"idOrdenCompra", "${idOrdenCompra}" }),
			@Result(name=INPUT,location=CONTENEDOR_ORDENCOMPRADETALLE)
			})
	public String  rechazarOrdenCompra(){
		ordenCompraAutorizacionService.rechazarOrdenCompra(idOrdenCompra);
		ordenCompraService.reiniciaIndemnizacion(idOrdenCompra);
		methodName = "consultarDetalleOrdenCompra";

		return SUCCESS;
	}

	
	/**
	 * carga los datos requeridos en pantalla antes de mostrar la pantalla de
	 * detalleOrdenCompra
	 */
	public void prepareDetalleOrdenCompra(Long idOrdenCompra){

		this.ordenCompra=ordenCompraService.obtenerOrdenCompra(idOrdenCompra);
		 if (usuarioService.tieneRolUsuarioActual(ROL_ALTACONCEPTO_CEROS_CABINERO)  && ( ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE) || ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE) ) ){
				altaCeros = true;
			}
		 
		if(null!=ordenCompra){
				if(null!=ordenCompra.getReporteCabina()){
					this.numReporte= ordenCompra.getReporteCabina().getNumeroReporte();
					if(null!=ordenCompra.getReporteCabina().getSiniestroCabina()){
					this.numSiniestro=ordenCompra.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();
					}
				}
			if( ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE) ){
				this.tituloOrdenCompra="Lista de Gastos de Ajuste";
				this.tituloOrdenCompraDetalle="Datos Generales del Gasto de Ajuste";
			}else if( ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA) ){
				this.tituloOrdenCompra="Lista de Ordenes de Compra";
				this.tituloOrdenCompraDetalle="Datos Generales de Ordenes de Compra";
			}else if( ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE) ){
				this.tituloOrdenCompra="Lista de Gastos de Ajuste";
				this.tituloOrdenCompraDetalle="Datos Generales del Reembolso de Gasto de Ajuste";
			}
		}
		this.tipo= ordenCompra.getTipo();
		this.idOrdenCompra =this.ordenCompra.getId();
		this.idReporteCabina= ordenCompra.getReporteCabina().getId();
		this.coberturaMap =listadoService.getCoberturasOrdenCompra(this.idReporteCabina,this.ordenCompra.getTipo());
		if(null!=ordenCompra.getCoberturaReporteCabina() && null!=ordenCompra.getCoberturaReporteCabina().getId() ){
			this.idCoberturaReporteCabina= ordenCompra.getCoberturaReporteCabina().getId();
			this.cveSubTipoCalculoCobertura=ordenCompra.getCveSubTipoCalculoCobertura();
			if(!StringUtils.isEmpty(ordenCompra.getIdCoberturaCompuesta())){
				this.coberturaCompuesta= ordenCompra.getIdCoberturaCompuesta();
				
			}
		}
		if( ! StringUtil.isEmpty( ordenCompra.getTipoProveedor())){
			this.proveedorMap=listadoService.getMapPrestadorPorTipo( ordenCompra.getTipoProveedor());
			
		}
		if(ordenCompra.getIdBeneficiario()!=null){
			Integer idPrestador = new Integer (ordenCompra.getIdBeneficiario().toString());
			PrestadorServicio prestador = this.entidadService.findById(PrestadorServicio.class, idPrestador);
			if(prestador!=null ){
				this.nombrePrestadorServicio=""+prestador.getId();
				if(!StringUtil.isEmpty(prestador.getNombrePersona())){
					this.nombrePrestadorServicio+=" - "+prestador.getNombrePersona();
					if(prestador.getOficina()!=null && !StringUtil.isEmpty(prestador.getOficina().getClaveOficina())){
						this.nombrePrestadorServicio+=" - "+prestador.getOficina().getClaveOficina();
					}
				}
				
			}
		}
		if(!StringUtils.isEmpty(ordenCompra.getCodigoUsuarioCreacion())){
			Usuario user=null;
			try{
				user=usuarioService.buscarUsuarioPorNombreUsuario(ordenCompra.getCodigoUsuarioCreacion());
			}catch (Exception e ){
				user=null;
			}			
			if(null!=user  && !StringUtil.isEmpty(user.getNombreCompleto())){
				this.creadoPor=user.getNombreCompleto();
			}else{
				this.creadoPor="";
			}
		}
		
		if(!StringUtils.isEmpty(ordenCompra.getCodigoUsuarioModificacion())){
			Usuario user=usuarioService.buscarUsuarioPorNombreUsuario(ordenCompra.getCodigoUsuarioModificacion());
			if(null!=user){
				if(!StringUtil.isEmpty(user.getNombreCompleto())){					
					this.modificadoPor=user.getNombreCompleto();
				}
			}
		}
		Long tipoPrestador= null;
		 List<TipoPrestadorServicio> lTipo= this.entidadService.findByProperty(TipoPrestadorServicio.class, "nombre", ordenCompra.getTipoProveedor());
		if (null!=lTipo && !lTipo.isEmpty()){
			tipoPrestador= lTipo.get(0).getId();
		}
				
		if( !StringUtil.isEmpty(ordenCompra.getTipo())){
			if(ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)){
				this.tipoProveedorMap=  listadoService.getMapTipoPrestadorAfectacionReserva();
				if(null!=idCoberturaReporteCabina){
					this.conceptoPagoMap = listadoService.listarConceptosPorCoberturaOrdenCompra(this.idCoberturaReporteCabina, this.cveSubTipoCalculoCobertura,ConceptoAjusteService.AFECTACION_RESERVA, null, false,null);
					this.terceroAfectadoMap=listadoService.getListasTercerosAfectadorPorCobertura(idCoberturaReporteCabina, this.cveSubTipoCalculoCobertura);
				}
			}else if(ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)){
				this.tipoProveedorMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PRESTADOR_GASTOAJUSTE);
				this.conceptoPagoMap = listadoService.listarConceptosPorCoberturaOrdenCompra(new Long(0),null,ConceptoAjusteService.GASTO_AJUSTE,null , false,tipoPrestador);
			}else if(ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE)){
				this.tipoProveedorMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PRESTADOR_GASTOAJUSTE);
				this.conceptoPagoMap = listadoService.listarConceptosPorCoberturaOrdenCompra(new Long(0),null,ConceptoAjusteService.REEMBOLSO_GASTO_AJUSTE,null , false,null);
			}

			
		}
		if(this.idOrdenCompra != null){
			OrdenCompra ordenCompra = this.entidadService.findById(OrdenCompra.class, this.idOrdenCompra);
			ReporteCabina reporte = ordenCompra.getReporteCabina();
			this.fechaOcurrido = reporte.getFechaHoraOcurrido();
			if(!ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)){
				// OBTENER RESERVA ACTUAL POR PASE DE ATENCION
				reservaDisponible = movimientoSiniestroService.obtenerReservaAfectacion(ordenCompra.getIdTercero(), Boolean.TRUE);
			}else{
				reservaDisponible = BigDecimal.ZERO; 
			}
		}
		
		
		
	}
	
	
	public void prepareDetalleOrdenCompraIndemnizacion (OrdenCompra ordenC){
		List<DetalleOrdenCompra> lstdetalleOrden = this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", ordenC.getId());
		if (!lstdetalleOrden.isEmpty()){
			DetalleOrdenCompra detalle = lstdetalleOrden.get(0);
			EstimacionCoberturaReporteCabina estimacion=this.entidadService.findById(EstimacionCoberturaReporteCabina.class, ordenC.getIdTercero());
			CoberturaReporteCabina cobertura = estimacion.getCoberturaReporteCabina();
			
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("cveTipoCalculoCobertura",cobertura.getClaveTipoCalculo());
			params.put("tipoEstimacion",estimacion.getTipoEstimacion());
			params.put("tipoConfiguracion",cobertura.getCoberturaDTO().getTipoConfiguracion());
			List<ConfiguracionCalculoCoberturaSiniestro> lista = this.entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, params);
			ConfiguracionCalculoCoberturaSiniestro conf = lista.get(0);	

			IndemnizacionSiniestro indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestro(ordenC.getId());
			if(indemnizacion != null && (indemnizacion.getEsPagoDanios() == null || !indemnizacion.getEsPagoDanios())){
				this.conceptoPagoMap = this.listadoService.listarConceptosPorCobertura(cobertura.getId(), conf.getCveSubTipoCalculoCobertura(), ConceptoAjusteService.AFECTACION_RESERVA , detalle.getConceptoAjuste().getTipoConcepto());
			}else{
				this.conceptoPagoMap = this.listadoService.listarConceptosPorCobertura(cobertura.getId(), conf.getCveSubTipoCalculoCobertura(), ConceptoAjusteService.AFECTACION_RESERVA , TIPO_CONCEPTO_DANIOS);
			}

		}else{
			this.conceptoPagoMap=new LinkedHashMap<Long, String>();
		}
	}
	
	public void consultarTerminosAjuste (){
		Long idReporte = null;
		if (null!=this.idReporteCabina){
			idReporte=this.idReporteCabina;
		}else if (null!= this.idOrdenCompra){
			OrdenCompra oc = this.entidadService.findById(OrdenCompra.class, this.idOrdenCompra);
			if (null!=oc){
				idReporte= oc.getReporteCabina().getId();
			}
			
		}
		
		
		if (null==idReporte)
			return;
		
		//Termino ajute  y siniestro
		if (!this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idReporte).isEmpty() )
		{
			AutoIncisoReporteCabina autoInciso=this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.seccionReporteCabina.reporteCabina.id",idReporte).get(0);
			if (! StringUtil.isEmpty(autoInciso.getTerminoAjuste())){
				this.codigoTerminoAjuste = autoInciso.getTerminoAjuste(); 
				CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO , autoInciso.getTerminoAjuste());
				if(null!=catalogo)
					this.terminoAjuste=(catalogo.getDescripcion().toUpperCase());
			}
			if (! StringUtil.isEmpty(autoInciso.getTerminoSiniestro())){
				CatValorFijo catalogo=	catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO , autoInciso.getTerminoSiniestro());
				if(null!=catalogo)
					this.terminoSiniestro=(catalogo.getDescripcion().toUpperCase());
			}
			
		}
		
		
		
		
	}
	
	
	@Action(value="consultarDetalleOrdenCompraIndemnizacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_DETALLEORDENCOMPRA_IDEMNIZACION),
			@Result(name=INPUT,location=CONTENEDOR_DETALLEORDENCOMPRA_IDEMNIZACION)
			})
	public String consultarDetalleOrdenCompraIndemnizacion(){
		this.prepareOrdenCompra();
		this.soloLectura=true;
		this.soloIndemnizacion=true;
		this.prepareDetalleOrdenCompra(this.idOrdenCompra);
		if(null==this.ordenCompra || this.ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA)){
			this.soloIndemnizacion=false;
		}
		if(null==this.ordenCompra || this.ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_INDEMNIZACION)){
			this.pantallaOrigen=this.PANTALLA_ORIGEN_INDEMNIZACON;
		}else{
			this.pantallaOrigen=this.PANTALLA_ORIGEN_ORDENCOMPRA;

		}
		
		prepareDetalleOrdenCompraIndemnizacion(this.ordenCompra);
		
		
		
		return SUCCESS;
	}
	
	
	private OrdenCompra setDatosIndemnizacion (OrdenCompra ordenCompra, Long idOrdenCompra){
		
		String curp=ordenCompra.getCurp();
		String rfc =ordenCompra.getRfc();
		String factura = ordenCompra.getFactura();
		String beneficiario = ordenCompra.getNomBeneficiario();
		String clabe = ordenCompra.getClabe();
		String correo = ordenCompra.getCorreo();
		String lada= ordenCompra.getLada(); 
		String telefono= ordenCompra.getTelefono(); 
		Integer banco = ordenCompra.getBancoId();
		String tipoPersona = ordenCompra.getTipoPersona();
		Boolean aplicaDeducible = ordenCompra.getAplicaDeducible();
		ordenCompra=this.ordenCompraService.obtenerOrdenCompra(idOrdenCompra);
		ordenCompra.setCurp(curp);
		ordenCompra.setFactura(factura);
		ordenCompra.setRfc(rfc);
		ordenCompra.setNomBeneficiario(beneficiario);
		ordenCompra.setBancoId(banco);
		ordenCompra.setClabe(clabe);
		ordenCompra.setLada(lada);
		ordenCompra.setTelefono(telefono);
		ordenCompra.setCorreo(correo);
		ordenCompra.setTipoPersona(tipoPersona);	
		ordenCompra.setAplicaDeducible(ordenCompra.getAplicaDeducible());
		return ordenCompra;
	}
	
	

	/**
	 * metodo que maneja las peticiones de guardado de una orden de compra. por indemnizacion
	 * 
	 * @param ordeCompra    mapea la entidad de �rden compra a la pantalla
	 */
	@Action(value="guardarOrdenCompraPorIndemnizacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_DETALLEORDENCOMPRA_IDEMNIZACION),
			@Result(name=INPUT,location=CONTENEDOR_DETALLEORDENCOMPRA_IDEMNIZACION)
			})
			public String guardarOrdenCompraPorIndemnizacion(){
		Long idOrdenCompraOrigen = ordenCompra.getId();
		List<DetalleOrdenCompra> lstDeta=this.ordenCompraService.obtenerDetallesOrdenCompra(idOrdenCompraOrigen,null);		
		OrdenCompra ordenCompraOrigen=this.ordenCompraService.obtenerOrdenCompra(idOrdenCompraOrigen);
		//Datos Capturables 	
		OrdenCompra ordenCompraCopy= this.setDatosIndemnizacion(ordenCompra,ordenCompra.getId());
		this.prepareOrdenCompra();		
		prepareDetalleOrdenCompra(ordenCompra.getId());
		prepareDetalleOrdenCompraIndemnizacion(ordenCompraOrigen);		
		this.ordenCompra=ordenCompraCopy;
		//si cambio tipo concepto debera de cambiar el concepto al detalle.
		ConceptoAjuste conceptoNew= null;
		if(null !=this.idConcepto){
			conceptoNew= this.entidadService.findById(ConceptoAjuste.class, idConcepto);
		}
		List<DetalleOrdenCompra>  detalleLst = new ArrayList<DetalleOrdenCompra>();
		if(ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_INDEMNIZACION)){
					
			try{
				ordenCompra.setIdOrdenCompraOrigen(idOrdenCompraOrigen);
				ordenCompra.setDetalleOrdenCompras(null);
				ordenCompra.setEstatus(OrdenCompraService.ESTATUS_TRAMITE);
				ordenCompra.setId(null);
				for (DetalleOrdenCompra detalle:    lstDeta){
					DetalleOrdenCompra detalleOrdenCompra= this.entidadService.findById(DetalleOrdenCompra.class, detalle.getId());
					if(null !=this.idConcepto){
						detalleOrdenCompra.setConceptoAjuste(conceptoNew);
					}else{
						ConceptoAjuste conceptoAjuste=detalleOrdenCompra.getConceptoAjuste();
						detalleOrdenCompra.setConceptoAjuste(conceptoAjuste);
					}
					if(null!=this.detalleOrdenCompra&&  !StringUtil.isEmpty(this.detalleOrdenCompra.getObservaciones())){
						detalle.setObservaciones(this.detalleOrdenCompra.getObservaciones());
					}
					detalleOrdenCompra.setId(null);
					detalleOrdenCompra.setOrdenCompra(ordenCompra);
					if( null!=this.detalleOrdenCompra && StringUtil.isEmpty(this.detalleOrdenCompra.getObservaciones())){
						detalleOrdenCompra.setObservaciones(this.detalleOrdenCompra.getObservaciones());
					}
					detalleLst.add(detalleOrdenCompra);
				}
				ordenCompra.setDetalleOrdenCompras(detalleLst);
				String msj = ordenCompraService.validarCrearOrdenCompra(ordenCompra);
				if(!StringUtil.isEmpty(msj)){
					this.ordenCompra = this.setDatosIndemnizacion(ordenCompra, idOrdenCompraOrigen);  
					super.setMensajeError(msj);
					return SUCCESS;
				}		
				ordenCompra.setIndemnizacionOrigen(null);
				Long idOrdenCompraGenerada = ordenCompraService.guardarOrdenCompra(ordenCompra);	
				ordenCompraOrigen.setEstatus(OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA);
				ordenCompraService.guardarOrdenCompra(ordenCompraOrigen);
				perdidaTotalService.cambiarEtapaIndemnizacionAOrdenCompraGenerada(idOrdenCompraOrigen, idOrdenCompraGenerada);
				this.soloIndemnizacion=false;	
			}catch(Exception e){
				if(null!= ordenCompra && null!=ordenCompra.getId()&& ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_TRAMITE)){
					this.entidadService.remove(ordenCompra);
				}
				super.setMensajeError("Error al guardar Orden "+ e.getMessage());
				return SUCCESS;
			}
			super.setMensajeExitoPersonalizado(getText("midas.siniestros.valuacion.ordencompra.exito.guardar"));
			return INPUT;	
			
		}else if(ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_TRAMITE)){
			this.soloIndemnizacion=true;
			try {
				for (DetalleOrdenCompra detalle:    lstDeta){
					DetalleOrdenCompra detalleOrdenCompra= this.entidadService.findById(DetalleOrdenCompra.class, detalle.getId());
					if(null !=this.idConcepto){
						detalleOrdenCompra.setConceptoAjuste(conceptoNew);
					}else{
						ConceptoAjuste conceptoAjuste=detalleOrdenCompra.getConceptoAjuste();
						detalleOrdenCompra.setConceptoAjuste(conceptoAjuste);
					}
					if(null!=this.detalleOrdenCompra&&  !StringUtil.isEmpty(this.detalleOrdenCompra.getObservaciones())){
						detalle.setObservaciones(this.detalleOrdenCompra.getObservaciones());
					}
					//detalleOrdenCompra.setId(null);
					detalleOrdenCompra.setOrdenCompra(ordenCompra);
					if( null!=this.detalleOrdenCompra && StringUtil.isEmpty(this.detalleOrdenCompra.getObservaciones())){
						detalleOrdenCompra.setObservaciones(this.detalleOrdenCompra.getObservaciones());
					}
					detalleLst.add(detalleOrdenCompra);
				}
				ordenCompra.setDetalleOrdenCompras(detalleLst);
				ordenCompraService.guardarOrdenCompra(ordenCompra);
			}catch(Exception e){
				super.setMensajeError("Error al guardar Orden de compra "+ e.getMessage());
				return SUCCESS;
			}
			try {
				this.prepareDetalleOrdenCompra(ordenCompra.getId());
			}catch(Exception e){
				super.setMensajeError("Error al Cargar Orden de Compra "+ e.getMessage());
				return SUCCESS;
			}
			super.setMensajeExitoPersonalizado(getText("midas.siniestros.valuacion.ordencompra.exito.guardar"));
			return INPUT;
		}else{
			this.soloIndemnizacion=false;
			return INPUT;
		}
	}
	/**
	 * carga los datos requeridos en pantalla antes de cargar la pantalla de captura
	 * de una orden de compra
	 */
	public void prepareOrdenCompra(){

		altaCeros = false;
		this.bancos =  listadoService.getMapBancosMidas();
		this.coberturaMap =new LinkedHashMap<String, String>();
		this.tipoProveedorMap =new LinkedHashMap<String, String>();
		this.tipoAfectado= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_AFECTADO);
		this.tipoOrdenCompraMap=  new LinkedHashMap<String, String>();
		this.estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_ORDEN_COMPRA);
		this.tipoPersona=  new LinkedHashMap<String, String>();
		this.reembolsoGastoAmap= new LinkedHashMap<String, String>();
		this.tipoPersona.put("","Seleccione..");
		this.reembolsoGastoAmap.put("","Seleccione..");
		Map<String, String> mapTipoPersona = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PERSONA);
		Map<String, String> mapGA = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.REEMBOLSO_GASTO_AJUSTE);
		this.tipoPersona.putAll(mapTipoPersona);
		this.reembolsoGastoAmap.putAll(mapGA);
		this.proveedorMap=new LinkedHashMap<Long, String>();
		this.terceroAfectadoMap=new LinkedHashMap<Long, String>();
		this.conceptoPagoMap=new LinkedHashMap<Long, String>();
		this.tipoIndemnizacionMap = new LinkedHashMap<String, String>();
		if(!StringUtil.isEmpty(this.tipo)){
			if(this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE) ){
				this.tipoProveedorMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PRESTADOR_GASTOAJUSTE);
				this.tituloOrdenCompra="Lista de Gastos de Ajuste";
				this.tituloOrdenCompraDetalle="Datos Generales del Gasto de Ajuste";
				this.tipoPagoMap=  new LinkedHashMap<String, String>();
				CatValorFijo cat =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA, OrdenCompraService.PAGO_A_PROVEEDOR);
				tipoPagoMap.put(cat.getCodigo(), cat.getDescripcion());	
				CatValorFijo catTipo =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, OrdenCompraService.TIPO_GASTOAJUSTE);
				tipoOrdenCompraMap.put(catTipo.getCodigo(), catTipo.getDescripcion());
			}else if (this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)) {
				this.tituloOrdenCompra="Lista de Ordenes de Compra";
				this.tituloOrdenCompraDetalle="Datos Generales de Ordenes de Compra";
				this.tipoProveedorMap=  listadoService.getMapTipoPrestadorAfectacionReserva();
				this.tipoPagoMap=new LinkedHashMap<String, String>();
				this.tipoPagoMap.put("","Seleccione..");
				Map<String, String> map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA);
				this.tipoPagoMap.putAll(map);
				CatValorFijo catTipo =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, OrdenCompraService.TIPO_AFECTACION_RESERVA);
				tipoOrdenCompraMap.put(catTipo.getCodigo(), catTipo.getDescripcion());	
			}else if(this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE) ){
				this.tipoProveedorMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PRESTADOR_GASTOAJUSTE);
				this.tituloOrdenCompra="Lista de Gastos de Ajuste";
				this.tituloOrdenCompraDetalle="Datos Generales del Reembolso Gasto de Ajuste";
				this.tipoPagoMap=  new LinkedHashMap<String, String>();
				CatValorFijo cat =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA, OrdenCompraService.PAGO_A_BENEFICIARIO);
				tipoPagoMap.put(cat.getCodigo(), cat.getDescripcion());	
				CatValorFijo catTipo =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE);
				tipoOrdenCompraMap.put(catTipo.getCodigo(), catTipo.getDescripcion());
			}
				
				
				
		}else{
			this.tituloOrdenCompra="Lista de Ordenes de Compra";
			this.tituloOrdenCompraDetalle="Datos Generales de Ordenes de Compra";
			this.tipoPagoMap=new LinkedHashMap<String, String>();
			this.tipoPagoMap.put("","Seleccione..");
			Map<String, String> map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA);
			this.tipoPagoMap.putAll(map);
			this.tipoOrdenCompraMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		}
		consultarTerminosAjuste();
		
	
	}

	public DetalleOrdenCompra getDetalleOrdenCompra() {
		return detalleOrdenCompra;
	}

	public void setDetalleOrdenCompra(DetalleOrdenCompra detalleOrdenCompra) {
		this.detalleOrdenCompra = detalleOrdenCompra;
	}


	

	public OrdenCompra getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(OrdenCompra ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public ReporteCabina getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteCabina reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public void prepare() throws Exception {
		if(!StringUtil.isEmpty(this.tipo)){
			if(this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE) ){
				this.tituloOrdenCompra="Lista de Gastos de Ajuste";
			}else if (this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_AFECTACION_RESERVA)) {
				this.tituloOrdenCompra="Lista de Ordenes de Compra";
			}else if(this.tipo.equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE) ){
				this.tituloOrdenCompra="Lista de Gastos de Ajuste";
				
			}
				
				
				
		}else{
			this.tituloOrdenCompra="Lista de Ordenes de Compra";
		}
		
	}
	
	
	public void prepareOrdencompraIndemnizacion()  {
		EstimacionCoberturaReporteCabina estimacion=this.entidadService.findById(EstimacionCoberturaReporteCabina.class, this.idEstimacionReporteCabina);
		 CoberturaReporteCabina cobertura = estimacion.getCoberturaReporteCabina();
		 if(null!=this.idReporteCabina){
				ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, this.idReporteCabina);
				if(null!=reporte){
					this.numReporte= reporte.getNumeroReporte();
					if(null!=reporte.getSiniestroCabina()){
					this.numSiniestro=reporte.getSiniestroCabina().getNumeroSiniestro();
					}
				}
			}
		 
		 tipoIndemnizacionMap=	this.perdidaTotalService.obtenerTiposSiniestroPerdidaTotal();
		 if(EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, cobertura.getClaveTipoCalculo())
				 && !tipoIndemnizacionMap.isEmpty() && tipoIndemnizacionMap.containsKey(OrdenCompraService.TIPO_INDEMNIZACION_ROBO)) {
			 String value = tipoIndemnizacionMap.get(OrdenCompraService.TIPO_INDEMNIZACION_ROBO);
			 tipoIndemnizacionMap.clear();
			 tipoIndemnizacionMap.put(OrdenCompraService.TIPO_INDEMNIZACION_ROBO, value);
		 }else{
			 if (!tipoIndemnizacionMap.isEmpty() && tipoIndemnizacionMap.containsKey(OrdenCompraService.TIPO_INDEMNIZACION_ROBO)){
					tipoIndemnizacionMap.remove(OrdenCompraService.TIPO_INDEMNIZACION_ROBO);
				}
			 /*Se elimina el concepto pago de daños del combo. por regla de negocio.*/
			 if (!tipoIndemnizacionMap.isEmpty() && tipoIndemnizacionMap.containsKey(OrdenCompraService.TIPO_INDEMNIZACION_PAGO_DANIOS)){
					tipoIndemnizacionMap.remove(OrdenCompraService.TIPO_INDEMNIZACION_PAGO_DANIOS);
				}
		 }
			
	}
	

	public Map<String, String> getTipoPagoMap() {
		return tipoPagoMap;
	}

	public void setTipoPagoMap(Map<String, String> tipoPagoMap) {
		this.tipoPagoMap = tipoPagoMap;
	}

	public Map<String, String> getCoberturaMap() {
		return coberturaMap;
	}

	public void setCoberturaMap(Map<String, String> coberturaMap) {
		this.coberturaMap = coberturaMap;
	}

	public Map<String, String> getEstautsMap() {
		return estautsMap;
	}

	public void setEstautsMap(Map<String, String> estautsMap) {
		this.estautsMap = estautsMap;
	}

	public Map<String, String> getTipoProveedorMap() {
		return tipoProveedorMap;
	}

	public void setTipoProveedorMap(Map<String, String> tipoProveedorMap) {
		this.tipoProveedorMap = tipoProveedorMap;
	}

	public Map<Long, String> getTerceroAfectadoMap() {
		return terceroAfectadoMap;
	}

	public void setTerceroAfectadoMap(Map<Long, String> terceroAfectadoMap) {
		this.terceroAfectadoMap = terceroAfectadoMap;
	}

	public Map<Long, String> getConceptoPagoMap() {
		return conceptoPagoMap;
	}

	public void setConceptoPagoMap(Map<Long, String> conceptoPagoMap) {
		this.conceptoPagoMap = conceptoPagoMap;
	}

	

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public OrdenCompraDTO getFiltroContenedor() {
		return filtroContenedor;
	}

	public void setFiltroContenedor(OrdenCompraDTO filtroContenedor) {
		this.filtroContenedor = filtroContenedor;
	}

	public List<OrdenCompraDTO> getListaOrdenCompra() {
		return listaOrdenCompra;
	}

	public void setListaOrdenCompra(List<OrdenCompraDTO> listaOrdenCompra) {
		this.listaOrdenCompra = listaOrdenCompra;
	}

	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	public Long getIdReporteCabina() {
		return idReporteCabina;
	}

	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}

	public boolean getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ConceptoAjusteService getConceptoAjusteService() {
		return conceptoAjusteService;
	}

	public void setConceptoAjusteService(ConceptoAjusteService conceptoAjusteService) {
		this.conceptoAjusteService = conceptoAjusteService;
	}

	public OrdenCompraService getOrdenCompraService() {
		return ordenCompraService;
	}

	public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
		this.ordenCompraService = ordenCompraService;
	}

	public Long getIdCoberturaReporteCabina() {
		return idCoberturaReporteCabina;
	}

	public void setIdCoberturaReporteCabina(Long idCoberturaReporteCabina) {
		this.idCoberturaReporteCabina = idCoberturaReporteCabina;
	}

	public String getTerceroAfectado() {
		return terceroAfectado;
	}

	public void setTerceroAfectado(String terceroAfectado) {
		this.terceroAfectado = terceroAfectado;
	}

	public String getAutorizadoPor() {
		return autorizadoPor;
	}

	public void setAutorizadoPor(String autorizadoPor) {
		this.autorizadoPor = autorizadoPor;
	}

	public Map<Long, String> getProveedorMap() {
		return proveedorMap;
	}

	public void setProveedorMap(Map<Long, String> proveedorMap) {
		this.proveedorMap = proveedorMap;
	}

	public Map<String, String> getTipoOrdenCompraMap() {
		return tipoOrdenCompraMap;
	}

	public void setTipoOrdenCompraMap(Map<String, String> tipoOrdenCompraMap) {
		this.tipoOrdenCompraMap = tipoOrdenCompraMap;
	}

	public Long getIdPrestador() {
		return idPrestador;
	}

	public void setIdPrestador(Long idPrestador) {
		this.idPrestador = idPrestador;
	}

	public Long getIdConcepto() {
		return idConcepto;
	}

	public void setIdConcepto(Long idConcepto) {
		this.idConcepto = idConcepto;
	}

	public List<DetalleOrdenCompraDTO> getListaDetalleDTO() {
		return listaDetalleDTO;
	}

	public void setListaDetalleDTO(List<DetalleOrdenCompraDTO> listaDetalleDTO) {
		this.listaDetalleDTO = listaDetalleDTO;
	}

	public ImportesOrdenCompraDTO getImportesDTO() {
		return importesDTO;
	}

	public void setImportesDTO(ImportesOrdenCompraDTO importesDTO) {
		this.importesDTO = importesDTO;
	}



	public String getCoberturaCompuesta() {
		return coberturaCompuesta;
	}

	public void setCoberturaCompuesta(String coberturaCompuesta) {
		this.coberturaCompuesta = coberturaCompuesta;
	}

	public String getCveSubTipoCalculoCobertura() {
		return cveSubTipoCalculoCobertura;
	}

	public void setCveSubTipoCalculoCobertura(String cveSubTipoCalculoCobertura) {
		this.cveSubTipoCalculoCobertura = cveSubTipoCalculoCobertura;
	}

	public ImpresionOrdenCompraDTO getImpresionDTO() {
		return impresionDTO;
	}

	public void setImpresionDTO(ImpresionOrdenCompraDTO impresionDTO) {
		this.impresionDTO = impresionDTO;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the bandejaPorAutorizar
	 */
	public Boolean getBandejaPorAutorizar() {
		return bandejaPorAutorizar;
	}

	/**
	 * @param bandejaPorAutorizar the bandejaPorAutorizar to set
	 */
	public void setBandejaPorAutorizar(Boolean bandejaPorAutorizar) {
		this.bandejaPorAutorizar = bandejaPorAutorizar;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public OrdenCompraAutorizacionService getOrdenCompraAutorizacionService() {
		return ordenCompraAutorizacionService;
	}

	public void setOrdenCompraAutorizacionService(
			OrdenCompraAutorizacionService ordenCompraAutorizacionService) {
		this.ordenCompraAutorizacionService = ordenCompraAutorizacionService;
	}

	public Long getIdEstimacionReporteCabina() {
		return idEstimacionReporteCabina;
	}

	public void setIdEstimacionReporteCabina(Long idEstimacionReporteCabina) {
		this.idEstimacionReporteCabina = idEstimacionReporteCabina;
	}

	public String getMensajePantalla() {
		return mensajePantalla;
	}

	public void setMensajePantalla(String mensajePantalla) {
		this.mensajePantalla = mensajePantalla;
	}

	public PerdidaTotalService getPerdidaTotalService() {
		return perdidaTotalService;
	}

	public void setPerdidaTotalService(PerdidaTotalService perdidaTotalService) {
		this.perdidaTotalService = perdidaTotalService;
	}

	public Map<String, String> getTipoIndemnizacionMap() {
		return tipoIndemnizacionMap;
	}

	public void setTipoIndemnizacionMap(Map<String, String> tipoIndemnizacionMap) {
		this.tipoIndemnizacionMap = tipoIndemnizacionMap;
	}

	public String getTipoIndemnizacion() {
		return tipoIndemnizacion;
	}

	public void setTipoIndemnizacion(String tipoIndemnizacion) {
		this.tipoIndemnizacion = tipoIndemnizacion;
	}

	public boolean isCrearIndemnizacion() {
		return crearIndemnizacion;
	}

	public void setCrearIndemnizacion(boolean crearIndemnizacion) {
		this.crearIndemnizacion = crearIndemnizacion;
	}

	public boolean isSoloIndemnizacion() {
		return soloIndemnizacion;
	}

	public void setSoloIndemnizacion(boolean soloIndemnizacion) {
		this.soloIndemnizacion = soloIndemnizacion;
	}

	public UtileriasService getUtileriasService() {
		return utileriasService;
	}

	public void setUtileriasService(UtileriasService utileriasService) {
		this.utileriasService = utileriasService;
	}

	public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
		return estimacionCoberturaSiniestroService;
	}

	public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}

	public String getPantallaOrigen() {
		return pantallaOrigen;
	}

	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}

	public boolean isAplicaPagoDaños() {
		return aplicaPagoDaños;
	}

	public void setAplicaPagoDaños(boolean aplicaPagoDaños) {
		this.aplicaPagoDaños = aplicaPagoDaños;
	}

	/**
	 * @return the filtroPT
	 */
	public PerdidaTotalFiltro getFiltroPT() {
		return filtroPT;
	}

	/**
	 * @param filtroPT the filtroPT to set
	 */
	public void setFiltroPT(PerdidaTotalFiltro filtroPT) {
		this.filtroPT = filtroPT;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public String getTituloOrdenCompra() {
		return tituloOrdenCompra;
	}

	public void setTituloOrdenCompra(String tituloOrdenCompra) {
		this.tituloOrdenCompra = tituloOrdenCompra;
	}

	public String getTituloOrdenCompraDetalle() {
		return tituloOrdenCompraDetalle;
	}

	public void setTituloOrdenCompraDetalle(String tituloOrdenCompraDetalle) {
		this.tituloOrdenCompraDetalle = tituloOrdenCompraDetalle;
	}

	public Long getIdDetalleOrdenSeleccionado() {
		return idDetalleOrdenSeleccionado;
	}

	public void setIdDetalleOrdenSeleccionado(Long idDetalleOrdenSeleccionado) {
		this.idDetalleOrdenSeleccionado = idDetalleOrdenSeleccionado;
	}

	public boolean isAltaCeros() {
		return altaCeros;
	}

	public void setAltaCeros(boolean altaCeros) {
		this.altaCeros = altaCeros;
	}

	public boolean isEsAutorizada() {
		return esAutorizada;
	}

	public void setEsAutorizada(boolean esAutorizada) {
		this.esAutorizada = esAutorizada;
	}

	public String getMensajeMostrar() {
		return mensajeMostrar;
	}

	public void setMensajeMostrar(String mensajeMostrar) {
		this.mensajeMostrar = mensajeMostrar;
	}

	public boolean isModoConsulta() {
		return modoConsulta;
	}

	public void setModoConsulta(boolean modoConsulta) {
		this.modoConsulta = modoConsulta;
	}

	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}

	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}

	public ConceptoAjusteDTO getConceptoAjusteDTO() {
		return conceptoAjusteDTO;
	}

	public void setConceptoAjusteDTO(ConceptoAjusteDTO conceptoAjusteDTO) {
		this.conceptoAjusteDTO = conceptoAjusteDTO;
	}

	public String getTerminoAjuste() {
		return terminoAjuste;
	}

	public void setTerminoAjuste(String terminoAjuste) {
		this.terminoAjuste = terminoAjuste;
	}

	public String getTerminoSiniestro() {
		return terminoSiniestro;
	}

	public void setTerminoSiniestro(String terminoSiniestro) {
		this.terminoSiniestro = terminoSiniestro;
	}

	public ImportesOrdenCompraDTO getImportesPorConceptoDTO() {
		return importesPorConceptoDTO;
	}

	public void setImportesPorConceptoDTO(
			ImportesOrdenCompraDTO importesPorConceptoDTO) {
		this.importesPorConceptoDTO = importesPorConceptoDTO;
	}

	public Map<String, String> getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(Map<String, String> tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public Map<String, String> getReembolsoGastoAmap() {
		return reembolsoGastoAmap;
	}

	public void setReembolsoGastoAmap(Map<String, String> reembolsoGastoAmap) {
		this.reembolsoGastoAmap = reembolsoGastoAmap;
	}

	public Map<String, String> getTipoAfectado() {
		return tipoAfectado;
	}

	public void setTipoAfectado(Map<String, String> tipoAfectado) {
		this.tipoAfectado = tipoAfectado;
	}

	public Map<Long, String> getBancos() {
		return bancos;
	}

	public void setBancos(Map<Long, String> bancos) {
		this.bancos = bancos;
	}

	public String getNumReporte() {
		return numReporte;
	}

	public void setNumReporte(String numReporte) {
		this.numReporte = numReporte;
	}

	public String getNumSiniestro() {
		return numSiniestro;
	}

	public void setNumSiniestro(String numSiniestro) {
		this.numSiniestro = numSiniestro;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

	public void setModificadoPor(String modificadoPor) {
		this.modificadoPor = modificadoPor;
	}

	public String getCreadoPor() {
		return creadoPor;
	}

	public void setCreadoPor(String creadoPor) {
		this.creadoPor = creadoPor;
	}
	
	@Action(value="mostrarBusquedaRecuperacion",results={
				@Result(name=SUCCESS,location=BUSQUEDA_ASIGNACION_OC),
				@Result(name=INPUT,location=BUSQUEDA_ASIGNACION_OC)
				})
	public String mostrarBusquedaRecuperacion(){
		this.tituloOrdenCompra="Busqueda de Ordenes de Compra";
		this.cadenaCoberturas.toString();
		this.cadenaPaseAtencion.toString();
		this.tipo.toString();
		this.idReporteCabina.toString();
		 return SUCCESS;	
	}
	

	@Action(value="busquedaOrdenCompraRecuperacion",results={
			@Result(name=SUCCESS,location=LISTADO_BUSQUEDA_ASIGNACION_OC),
			@Result(name=INPUT,location=LISTADO_BUSQUEDA_ASIGNACION_OC)
			})
	public String busquedaOrdenCompraRecuperacion(){
		coberturasId = new ArrayList<Long>();
		pasesAtencionId= new ArrayList<Long>();
		
		if(!StringUtil.isEmpty(cadenaPaseAtencion)){
			lstIdPaseAtencion =  cadenaPaseAtencion.split(",") ;
			for (String id :lstIdPaseAtencion ){
				Long idLong = new Long (id);
				pasesAtencionId.add(idLong);
			}
		}
		listaOrdenCompraRecuperacionDTO=this.ordenCompraService.obtenerOrdenCompraRecuperacionProveedor(this.idReporteCabina, cadenaCoberturas, pasesAtencionId, tipo,null);
		
		
		 
		 return SUCCESS;	
	}
	

	@Action(value="mostrarBandejaOrdenCompra",results={
			@Result(name=SUCCESS,location=BANDEJA_ORDENCOMPRA),
			@Result(name=INPUT,location=BANDEJA_ORDENCOMPRA)
			})
   public String mostrarBandejaOrdenCompra(){
		filtroBandeja=new  BandejaOrdenCompraDTO();
		this.estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_ORDEN_COMPRA);
		estautsMap.remove("Ind");estautsMap.remove("Ind_Final");estautsMap.remove("Ind_Cancel");
		tipoProveedorMap= listadoService.getMapTipoPrestador();
		this.proveedorMap=new LinkedHashMap<Long, String>();
		tipoOrdenCompraMap =listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		this.terminoAjusteMap=  listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		this.oficinas = listadoService.obtenerOficinasSiniestros();		
		this.tipoPagoMap=listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PAGOORDEN_COMPRA);

		return SUCCESS;	
	}
	

	 @Action(value="listadoBandejaOrdenCompra",results={
				@Result(name=SUCCESS,location=LISTADO_BANDEJA_ORDENCOMPRA),
				@Result(name=INPUT,location=LISTADO_BANDEJA_ORDENCOMPRA)
				})
	public String listadoBandejaOrdenCompra(){		 
		if(super.getCount() == null){
			super.setCount(REGISTROS_A_MOSTRAR); 
		}
					
		if(super.getPosStart() == null){
			super.setPosStart(0);
		}			
		filtroBandeja.setPosStart(super.getPosStart());
		filtroBandeja.setCount(super.getCount());
		filtroBandeja.setOrderByAttribute(decodeOrderBy());
		if(super.getPosStart().intValue() == 0){
			super.setTotalCount(ordenCompraService.contarOrdenesCompraBandeja(filtroBandeja));
		}		
		listaBandejaOrdenCompra=this.ordenCompraService.buscarOrdenesCompra(filtroBandeja);
		return SUCCESS;	
	 }

	 private String decodeOrderBy(){
			String order = null;
			if(!StringUtil.isEmpty(super.getOrderBy())){
				order = COLUMNAS_GRID_ORDEN_COMPRA[Integer.valueOf(super.getOrderBy())];
				if(!StringUtil.isEmpty(super.getDirect())){
					order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
				}
			}		
			return order;
		}
	
	
	public List<OrdenCompraRecuperacionDTO> getListaOrdenCompraRecuperacionDTO() {
		return listaOrdenCompraRecuperacionDTO;
	}

	public void setListaOrdenCompraRecuperacionDTO(
			List<OrdenCompraRecuperacionDTO> listaOrdenCompraRecuperacionDTO) {
		this.listaOrdenCompraRecuperacionDTO = listaOrdenCompraRecuperacionDTO;
	}

	public OrdenCompraRecuperacionDTO getOrdenCompraRecuperacionDTO() {
		return ordenCompraRecuperacionDTO;
	}

	public void setOrdenCompraRecuperacionDTO(
			OrdenCompraRecuperacionDTO ordenCompraRecuperacionDTO) {
		this.ordenCompraRecuperacionDTO = ordenCompraRecuperacionDTO;
	}

	public OrdenCompraDTO getFiltroBusquedaRecuperacion() {
		return filtroBusquedaRecuperacion;
	}

	public void setFiltroBusquedaRecuperacion(
			OrdenCompraDTO filtroBusquedaRecuperacion) {
		this.filtroBusquedaRecuperacion = filtroBusquedaRecuperacion;
	}

	public Long getIdPaseAtencion() {
		return idPaseAtencion;
	}

	public void setIdPaseAtencion(Long idPaseAtencion) {
		this.idPaseAtencion = idPaseAtencion;
	}

	public String getCadenaCoberturas() {
		return cadenaCoberturas;
	}

	public void setCadenaCoberturas(String cadenaCoberturas) {
		this.cadenaCoberturas = cadenaCoberturas;
	}

	public String getCadenaPaseAtencion() {
		return cadenaPaseAtencion;
	}

	public void setCadenaPaseAtencion(String cadenaPaseAtencion) {
		this.cadenaPaseAtencion = cadenaPaseAtencion;
	}

	public String[] getLstIdCoberturas() {
		return lstIdCoberturas;
	}

	public void setLstIdCoberturas(String[] lstIdCoberturas) {
		this.lstIdCoberturas = lstIdCoberturas;
	}

	public String[] getLstIdPaseAtencion() {
		return lstIdPaseAtencion;
	}

	public void setLstIdPaseAtencion(String[] lstIdPaseAtencion) {
		this.lstIdPaseAtencion = lstIdPaseAtencion;
	}

	public List<Long> getCoberturasId() {
		return coberturasId;
	}

	public void setCoberturasId(List<Long> coberturasId) {
		this.coberturasId = coberturasId;
	}

	public List<Long> getPasesAtencionId() {
		return pasesAtencionId;
	}

	public void setPasesAtencionId(List<Long> pasesAtencionId) {
		this.pasesAtencionId = pasesAtencionId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Long getIdIndemnizacion() {
		return idIndemnizacion;
	}

	public void setIdIndemnizacion(Long idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}

	public Date getFechaOcurrido() {
		return fechaOcurrido;
	}

	public void setFechaOcurrido(Date fechaOcurrido) {
		this.fechaOcurrido = fechaOcurrido;
	}
	public String getCodigoTerminoAjuste() {
		return codigoTerminoAjuste;
	}

	public void setCodigoTerminoAjuste(String codigoTerminoAjuste) {
		this.codigoTerminoAjuste = codigoTerminoAjuste;
	}
	public String getNombrePrestadorServicio() {
		return nombrePrestadorServicio;
	}

	public void setNombrePrestadorServicio(String nombrePrestadorServicio) {
		this.nombrePrestadorServicio = nombrePrestadorServicio;
	}
	

	public BandejaOrdenCompraDTO getFiltroBandeja() {
		return filtroBandeja;
	}

	public void setFiltroBandeja(BandejaOrdenCompraDTO filtroBandeja) {
		this.filtroBandeja = filtroBandeja;
	}

	public List<BandejaOrdenCompraDTO> getListaBandejaOrdenCompra() {
		return listaBandejaOrdenCompra;
	}

	public void setListaBandejaOrdenCompra(
			List<BandejaOrdenCompraDTO> listaBandejaOrdenCompra) {
		this.listaBandejaOrdenCompra = listaBandejaOrdenCompra;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public Map<String, String> getTerminoAjusteMap() {
		return terminoAjusteMap;
	}

	public void setTerminoAjusteMap(Map<String, String> terminoAjusteMap) {
		this.terminoAjusteMap = terminoAjusteMap;
	}

	public BigDecimal getReservaDisponible() {
		return reservaDisponible;
	}

	public void setReservaDisponible(BigDecimal reservaDisponible) {
		this.reservaDisponible = reservaDisponible;
	}

	public Long getIdOrdenCompraDetalle() {
		return idOrdenCompraDetalle;
	}

	public void setIdOrdenCompraDetalle(Long idOrdenCompraDetalle) {
		this.idOrdenCompraDetalle = idOrdenCompraDetalle;
		
	}
	

}