/**
 * 
 */
package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.math.BigDecimal;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SolicitudReporteCabinaDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;

/**
 * @author simavera
 *
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/cartaCobertura")
public class CartaCoberturaAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 839703147309187862L;

	private Long idToSolicitud;
	private Long idReporteCabina;
	private SolicitudDataEnTramiteDTO solicitud;
	private SolicitudReporteCabinaDTO solicitudReporteCabina;
	private String serie;
	
	@Autowired
    @Qualifier("polizaSiniestroServiceEJB")
	private PolizaSiniestroService polizaSiniestroService;
	
	@Override
	public void prepare(){
	}
	
	/**
	 * Pantalla Principal de Cartas Cobertura
	 * @return
	 */
	@Action(value = "mostrarCartaCobertura", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/cartaCoberturaDetalle.jsp") })
	public String mostrarCartaCobertura() {
		
		this.solicitudReporteCabina = polizaSiniestroService.buscarCartaCobertura( new BigDecimal(this.idToSolicitud), this.serie );
		return SUCCESS;
	}
	
	/**
	 * Pantalla Principal de Cartas Cobertura
	 * @return
	 */
	@Action(value = "asignarCartaCobertura", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/cartaCoberturaDetalle.jsp") })
	public String asignarCartaCobertura() {
		try{
			this.solicitudReporteCabina = polizaSiniestroService.buscarCartaCobertura( new BigDecimal(this.idToSolicitud), this.serie );
			polizaSiniestroService.asignarCartaCobertura( this.idReporteCabina, this.idToSolicitud.toString(), this.serie );
			setMensaje(MensajeDTO.MENSAJE_ASIGNAR_CARTA_COBERTURA);
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);	
		}catch (Exception ex) {
			setMensaje(MensajeDTO.MENSAJE_ERROR_ASIGNAR_CARTA_COBERTURA);
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
		}
		return SUCCESS;
	}
	

	/**
	 * @return the idToSolicitud
	 */
	public Long getIdToSolicitud() {
		return idToSolicitud;
	}

	/**
	 * @param idToSolicitud the idToSolicitud to set
	 */
	public void setIdToSolicitud(Long idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	/**
	 * @return the idReporteCabina
	 */
	public Long getIdReporteCabina() {
		return idReporteCabina;
	}

	/**
	 * @param idReporteCabina the idReporteCabina to set
	 */
	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}

	/**
	 * @return the solicitud
	 */
	public SolicitudDataEnTramiteDTO getSolicitud() {
		return solicitud;
	}

	/**
	 * @param solicitud the solicitud to set
	 */
	public void setSolicitud(SolicitudDataEnTramiteDTO solicitud) {
		this.solicitud = solicitud;
	}

	/**
	 * @return the solicitudReporteCabina
	 */
	public SolicitudReporteCabinaDTO getSolicitudReporteCabina() {
		return solicitudReporteCabina;
	}

	/**
	 * @param solicitudReporteCabina the solicitudReporteCabina to set
	 */
	public void setSolicitudReporteCabina(
			SolicitudReporteCabinaDTO solicitudReporteCabina) {
		this.solicitudReporteCabina = solicitudReporteCabina;
	}

	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}

	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}

	/**
	 * @return the polizaSiniestroService
	 */
	public PolizaSiniestroService getPolizaSiniestroService() {
		return polizaSiniestroService;
	}

	/**
	 * @param polizaSiniestroService the polizaSiniestroService to set
	 */
	public void setPolizaSiniestroService(
			PolizaSiniestroService polizaSiniestroService) {
		this.polizaSiniestroService = polizaSiniestroService;
	}
}
