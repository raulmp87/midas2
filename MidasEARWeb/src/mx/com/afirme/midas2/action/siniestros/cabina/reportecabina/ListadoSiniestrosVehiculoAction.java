package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroVehiculoDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.SiniestroService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/reportecabina")
public class ListadoSiniestrosVehiculoAction extends BaseAction implements Preparable {

/*
 * attrs
 */
	private static final long serialVersionUID = 1L;
	private String numeroSerie;
	private TransporteImpresionDTO transporteImpresion;
	private ReporteSiniestroVehiculoDTO reporteSiniestroDTO = new ReporteSiniestroVehiculoDTO();
	private List<ReporteSiniestroVehiculoDTO> reporteSiniestroList = new ArrayList<ReporteSiniestroVehiculoDTO>();
	
	@Autowired
	@Qualifier("siniestroServiceEJB")
	private SiniestroService siniestroService;
/* 
 * actions
 */
	@Override
	public void prepare(){
		
	}
	
	@Action(value = "mostrarListado", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/listadoSiniestrosVehiculo.jsp"))
	public String mostrarListado(){
		return SUCCESS;
	}
	
	@Action(value = "buscarSiniestro", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/listadoSiniestrosVehiculoGrid.jsp"))
	public String buscarSiniestro(){
		reporteSiniestroList = siniestroService.buscarSiniestro( numeroSerie );
		return SUCCESS;
	}
	
	@Action(value="exportarListado",
		       results={@Result(name=SUCCESS,
				type="stream",
				params={"contentType","${transporteImpresion.contentType}",
				"inputName","transporteImpresion.genericInputStream",
				"contentDisposition",
				"attachment;filename=\"${transporteImpresion.fileName}\""})})
	public String exportarListado(){
		reporteSiniestroList = siniestroService.buscarSiniestro( numeroSerie );
		ExcelExporter exporter = new ExcelExporter(ReporteSiniestroVehiculoDTO.class);
		transporteImpresion = exporter.exportXLS(reporteSiniestroList, "Listado Siniestros Vehiculo");
				
		return SUCCESS;
	}

/* 
 * getters&setters
 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public TransporteImpresionDTO getTransporteImpresion() {
		return transporteImpresion;
	}

	public void setTransporteImpresion(TransporteImpresionDTO transporteImpresion) {
		this.transporteImpresion = transporteImpresion;
	}

	public SiniestroService getSiniestroService() {
		return siniestroService;
	}

	public void setSiniestroService(SiniestroService siniestroService) {
		this.siniestroService = siniestroService;
	}

	public List<ReporteSiniestroVehiculoDTO> getReporteSiniestroList() {
		return reporteSiniestroList;
	}

	public void setReporteSiniestroList(
			List<ReporteSiniestroVehiculoDTO> reporteSiniestroList) {
		this.reporteSiniestroList = reporteSiniestroList;
	}

	public ReporteSiniestroVehiculoDTO getReporteSiniestroDTO() {
		return reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroVehiculoDTO reporteSiniestroDTO) {
		this.reporteSiniestroDTO = reporteSiniestroDTO;
	}
	
}