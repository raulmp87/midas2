package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/consulta")

public class EndosoPolizaSiniestroAction extends BaseAction {

	private static final long serialVersionUID = -6111114275898923567L;
	private List<CotizacionEndosoDTO> endososList = new ArrayList<CotizacionEndosoDTO>();
	private Long incisoContinuity;
	private List<String> usuarioNombre = new ArrayList<String>();	
	private Date fechaReporteSiniestro;
	private Long idReporte;
	private Short soloConsulta;
	
	/* Informaacion para regresar a la busqueda*/
	private IncisoSiniestroDTO filtroBusqueda;
	
	/* Datos para Consulta Inciso*/
	private Long validOnMillis;
	private Long recordFromMillis;
	private Long fechaReporteSiniestroMillis;
	
	
	@Autowired
	@Qualifier("cotizacionEndosoServiceEJB")
	private CotizacionEndosoService cotizacionEndosoService;	
	
	/**
	 * Mostrar pantalla principal
	 */
	@Action(value="mostrarEndoso", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/endosoPolizaSiniestro.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/cabina/reportecabina/endosoPolizaSiniestro.jsp")
	})
	public String mostrarEndoso(){
		return SUCCESS;
	}
	
	/**
	 * Método para listar las cargas masivas
	 */
	@Action(value="listarCarga", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/endosoPolizaSiniestroGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/endosoPolizaSiniestroGrid.jsp")
	})
	public String listaCarga() {	
		
		//Long keyInciso = new Long("677345");
		
		this.endososList = this.cotizacionEndosoService.obtenerEndosoInciso(incisoContinuity);
		
		return SUCCESS;		
	}	

	public List<CotizacionEndosoDTO> getEndososList() {
		return endososList;
	}

	public Long getIncisoContinuity() {
		return incisoContinuity;
	}

	public void setEndososList(List<CotizacionEndosoDTO> endososList) {
		this.endososList = endososList;
	}

	public void setIncisoContinuity(Long incisoContinuity) {
		this.incisoContinuity = incisoContinuity;
	}

	public List<String> getUsuarioNombre() {
		return usuarioNombre;
	}

	public void setUsuarioNombre(List<String> usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	public Date getFechaReporteSiniestro() {
		return fechaReporteSiniestro;
	}

	public void setFechaReporteSiniestro(Date fechaReporteSiniestro) {
		this.fechaReporteSiniestro = fechaReporteSiniestro;
	}

	public Long getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}

	public IncisoSiniestroDTO getFiltroBusqueda() {
		return filtroBusqueda;
	}

	public void setFiltroBusqueda(IncisoSiniestroDTO filtroBusqueda) {
		this.filtroBusqueda = filtroBusqueda;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Long getFechaReporteSiniestroMillis() {
		return fechaReporteSiniestroMillis;
	}

	public void setFechaReporteSiniestroMillis(Long fechaReporteSiniestroMillis) {
		this.fechaReporteSiniestroMillis = fechaReporteSiniestroMillis;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}	
	
	
	

}
