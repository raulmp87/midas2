package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReciboProgramaPagoDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Namespace("/siniestros/cabina/reportecabina/poliza/programapago")
@Component
@Scope("prototype")
public class ProgramasPagoAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -7852326877489642213L;

	private List<ProgramaPagoDTO> programasPagoList;
	private List<ReciboProgramaPagoDTO> recibosList;
	private IncisoSiniestroDTO incisoSiniestro;
	
	private Long incisoContinuityId;
	private Long idCotizacionSeycos;
	private Long numeroPrograma;
	private Date fechaReporteSiniestro;
	private Long idReporte;
	private Date validOn;
	private Date recordFrom;
	
	/* Informacion para regresar a la busqueda*/
	private IncisoSiniestroDTO filtroBusqueda;

	/* Datos para Consulta Inciso*/
	private Long validOnMillis;
	private Long recordFromMillis;
	private Long fechaReporteSiniestroMillis;
	private Short soloConsulta;
	
	@Autowired
    @Qualifier("polizaSiniestroServiceEJB")
	private PolizaSiniestroService polizaSiniestroService;
	
	@Override
	public void prepare(){
		
	}
	
	public void prepareMostrar(){
		if(incisoContinuityId != null && fechaReporteSiniestro != null){
			incisoSiniestro = initDataToFindInciso();
			incisoSiniestro = polizaSiniestroService.buscarDetalleInciso(incisoSiniestro, this.validOn, this.recordFrom, fechaReporteSiniestro);
		}
	}
	
	public void prepareObtenerProgramasPago(){
		if(incisoContinuityId != null && fechaReporteSiniestro != null){
			incisoSiniestro = initDataToFindInciso();
			incisoSiniestro = polizaSiniestroService.buscarDetalleInciso(incisoSiniestro, this.validOn, this.recordFrom, fechaReporteSiniestro);
		}
	}
	
	public void prepareObtenerRecibos(){
		
	}
	
	/**
	 * Muestra pantalla para mostrar los programas de pagos
	 * @return
	 */
	@Action(value = "mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/consultaProgramasPago.jsp") })
	public String mostrar() { 
		return SUCCESS;
	}
	
	/**
	 * Obtiene los programas de pago para el inciso correspondiente
	 * @return
	 */
	@Action(value = "obtenerProgramasPago", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/programasPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/consultaProgramasPago.jsp")
			})
	public String obtenerProgramasPago() {
		programasPagoList = polizaSiniestroService.obtenerProgramasPago(
				incisoSiniestro.getIdToPoliza().longValue(), incisoSiniestro.getNumeroInciso(), fechaReporteSiniestro);
			return SUCCESS;
	}
	
	/**
	 * Obtiene los programas de pago para el inciso correspondiente
	 * @return
	 */
	@Action(value = "obtenerRecibos", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/recibosProgramaGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/consultaProgramasPago.jsp")
			})
	public String obtenerRecibos() {
		recibosList = new ArrayList<ReciboProgramaPagoDTO>();
		recibosList = polizaSiniestroService.obtenerRecibosPrograma(
				idCotizacionSeycos, numeroPrograma, fechaReporteSiniestro);
		return SUCCESS;
	}

	/**
	 * @return the programasPagoList
	 */
	public List<ProgramaPagoDTO> getProgramasPagoList() {
		return programasPagoList;
	}

	/**
	 * @param programasPagoList the programasPagoList to set
	 */
	public void setProgramasPagoList(List<ProgramaPagoDTO> programasPagoList) {
		this.programasPagoList = programasPagoList;
	}

	/**
	 * @return the recibosList
	 */
	public List<ReciboProgramaPagoDTO> getRecibosList() {
		return recibosList;
	}

	/**
	 * @param recibosList the recibosList to set
	 */
	public void setRecibosList(List<ReciboProgramaPagoDTO> recibosList) {
		this.recibosList = recibosList;
	}

	/**
	 * @return the fechaReporteSiniestro
	 */
	public Date getFechaReporteSiniestro() {
		return fechaReporteSiniestro;
	}

	/**
	 * @param fechaReporteSiniestro the fechaReporteSiniestro to set
	 */
	public void setFechaReporteSiniestro(Date fechaReporteSiniestro) {
		this.fechaReporteSiniestro = fechaReporteSiniestro;
	}

	/**
	 * @return the incisoSiniestro
	 */
	public IncisoSiniestroDTO getIncisoSiniestro() {
		return incisoSiniestro;
	}

	/**
	 * @param incisoSiniestro the incisoSiniestro to set
	 */
	public void setIncisoSiniestro(IncisoSiniestroDTO incisoSiniestro) {
		this.incisoSiniestro = incisoSiniestro;
	}

	/**
	 * @return the incisoContinuityId
	 */
	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	/**
	 * @param incisoContinuityId the incisoContinuityId to set
	 */
	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}

	/**
	 * @return the idCotizacionSeycos
	 */
	public Long getIdCotizacionSeycos() {
		return idCotizacionSeycos;
	}

	/**
	 * @param idCotizacionSeycos the idCotizacionSeycos to set
	 */
	public void setIdCotizacionSeycos(Long idCotizacionSeycos) {
		this.idCotizacionSeycos = idCotizacionSeycos;
	}

	/**
	 * @return the numeroPrograma
	 */
	public Long getNumeroPrograma() {
		return numeroPrograma;
	}

	/**
	 * @param numeroPrograma the numeroPrograma to set
	 */
	public void setNumeroPrograma(Long numeroPrograma) {
		this.numeroPrograma = numeroPrograma;
	}

	/**
	 * @return the polizaSiniestroService
	 */
	public PolizaSiniestroService getPolizaSiniestroService() {
		return polizaSiniestroService;
	}

	/**
	 * @param polizaSiniestroService the polizaSiniestroService to set
	 */
	public void setPolizaSiniestroService(
			PolizaSiniestroService polizaSiniestroService) {
		this.polizaSiniestroService = polizaSiniestroService;
	}

	public Long getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}

	public IncisoSiniestroDTO getFiltroBusqueda() {
		return filtroBusqueda;
	}

	public void setFiltroBusqueda(IncisoSiniestroDTO filtroBusqueda) {
		this.filtroBusqueda = filtroBusqueda;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Date getValidOn() {
		return validOn;
	}

	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}
	
	
	private IncisoSiniestroDTO initDataToFindInciso(){
		
		IncisoSiniestroDTO incisoSiniestroDTO = new IncisoSiniestroDTO();

		incisoSiniestroDTO.setIncisoContinuityId(this.incisoContinuityId);
		incisoSiniestroDTO.setFechaReporteSiniestro(this.fechaReporteSiniestro);
		
		this.validOn = new Date( this.validOnMillis );
		this.recordFrom = new Date( this.recordFromMillis );
		
		return incisoSiniestroDTO;
	}

	public Long getFechaReporteSiniestroMillis() {
		return fechaReporteSiniestroMillis;
	}

	public void setFechaReporteSiniestroMillis(Long fechaReporteSiniestroMillis) {
		this.fechaReporteSiniestroMillis = fechaReporteSiniestroMillis;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}
	
	
	
}
