package mx.com.afirme.midas2.action.siniestros.reservas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoPaseAtencion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionGenerica;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCBienes;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCViajero;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;



@Component
@Scope("prototype")
@Namespace("/siniestros/reservas")
public class AjusteReservasAction extends BaseAction implements Preparable {

	private static final String	LOCATION_AJUSTERESERVA_JSP	= "/jsp/siniestros/reservas/ajusteReserva.jsp";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
//	private DatosEstimacionCoberturaDTO datosEstimacion;
	private EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestroDTO;
	private DatosReporteSiniestroDTO datosReporte;
	private Long idEstimacionCobertura;
	private Long idReporteCabina;
	private Long idReporte;
	private Long idParametroReserva;
	private List<PaseAtencionSiniestroDTO> listCoberturas;
	private Map<String,String> causasMovimientoMap ;
	//Datos Generales
	private String numeroSiniestro;
	private String numeroReporte;
	private String numeroPoliza;
	private Integer numeroInciso;
	private Integer requiereAutorizacion;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("catSolicitudAutorizacionServiceEJB")
	private CatSolicitudAutorizacionService catSolicitudAutorizacionService;
	
	@Autowired
	@Qualifier("autorizacionReservaServiceEJB")
	private AutorizacionReservaService autorizacionReservaService;
	
	/**
	 * Invocar el método obtenerDatosReporte de ReporteCabinaService para llenar el
	 * atributo datosReporte .Llenar el catálogo de causaMovimientoList por
	 * ListadoService
	 */
	public void prepare(){
		this.causasMovimientoMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);
		this.causasMovimientoMap.remove(CausaMovimiento.APERTURA_RESERVA.getValue());
		this.requiereAutorizacion = (this.requiereAutorizacion==null)?0:this.requiereAutorizacion; 
	}
	
	
	
	/**
	 * Invocar al método guardarEstimacionCobertura de
	 * EstimacionCoberturaSiniestroService.
	 */
	public String guardarEstimacion(){
		return "";
	}

	
	@Action(value = "mostrarAjusteDeReserva", results = {
			@Result(name = SUCCESS, location = LOCATION_AJUSTERESERVA_JSP),
			@Result(name = INPUT, location = "/jsp/siniestros/reportecabina/listadoReporteSiniestro.jsp") })
	public String mostrar(){
		this.estimacionCoberturaSiniestroDTO = new EstimacionCoberturaSiniestroDTO();
		return SUCCESS;
	}

	/**
	 * Invocar al método obtenerDesgloseCoberturas de
	 * EstimacionCoberturaSiniestroService. Si hay un error de validación mostrarlo en
	 * pantalla
	 */
	public String mostrarCoberturas(){
		return "";
	}

	/**
	 * Invocar al método obtenerMontosEstimacionCobertura y llenar el objeto
	 * datosEstimacion
	 */
	@Action(value = "mostrarDatosEstimacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/reservas/ajusteReservaGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_AJUSTERESERVA_JSP) })
	public String mostrarDatosEstimacion(){
		List<PaseAtencionSiniestroDTO>  lstCoberturas = this.estimacionCoberturaSiniestroService.obtenerDesgloseCoberturas(idReporteCabina ); 
		this.listCoberturas = filtraPorEstimacionesAfectables(lstCoberturas);
		return SUCCESS;
	}
	
	private List<PaseAtencionSiniestroDTO>  filtraPorEstimacionesAfectables(List<PaseAtencionSiniestroDTO> pasesDTO){
		List<PaseAtencionSiniestroDTO>  lstCoberturasFiltradas = new ArrayList<PaseAtencionSiniestroDTO>(); 
		if(pasesDTO!=null){
			for(PaseAtencionSiniestroDTO dto : pasesDTO){
				String esEditable = dto.getEstimacionCoberturaReporte().getEsEditable();
				String tipoPase = dto.getEstimacionCoberturaReporte().getTipoPaseAtencion(); 
				if(!EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REGISTRO, tipoPase) && esEditable!= null && esEditable.equals("EDT")){
					lstCoberturasFiltradas.add(dto);
				}
			}
		}
		return lstCoberturasFiltradas;
	}
	
	@Action(value = "mostrarInfoReservas", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/reservas/reservas.jsp"),
			@Result(name = INPUT, location = LOCATION_AJUSTERESERVA_JSP) })
	public String mostrarInformacionEnReservas(){
		EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, this.idEstimacionCobertura);
		estimacionCoberturaSiniestroDTO = this.estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
		return SUCCESS;
	}
	
	
	@Action(value = "guardarMovimiento", results = {
			@Result(name = SUCCESS, location = LOCATION_AJUSTERESERVA_JSP),
			@Result(name = INPUT, location = LOCATION_AJUSTERESERVA_JSP) })
	public String guardarMovimiento(){
		Long idEstimacion = this.estimacionCoberturaSiniestroDTO.getEstimacionCoberturaReporte().getId();
		String tipoEstimacion = this.estimacionCoberturaSiniestroDTO.getEstimacionCoberturaReporte().getTipoEstimacion();
		Long idCoberturaReporteCabina = this.estimacionCoberturaSiniestroDTO.getEstimacionCoberturaReporte().getCoberturaReporteCabina().getId();
		EstimacionCoberturaReporteCabina estimacion = this.colocaEstimacionEnDTO(idEstimacion, tipoEstimacion, this.estimacionCoberturaSiniestroDTO);
		this.idReporteCabina = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		
		List<String> errores 	= null;
		try {
			errores = estimacionCoberturaSiniestroService.validarEstimacionCobertura(this.estimacionCoberturaSiniestroDTO, idCoberturaReporteCabina, estimacion);
			if (!errores.isEmpty()) {
				super.setMensajeListaPersonalizado("Campos Requeridos", errores, BaseAction.TIPO_MENSAJE_ERROR);
			} else {
				this.idParametroReserva = this.autorizacionReservaService.validarParametrosAntiguedad(estimacion.getId(), this.estimacionCoberturaSiniestroDTO.getDatosEstimacion().getEstimacionNueva());
				if(this.idParametroReserva==null){
					String mensaje = autorizacionReservaService.validarParametros(idEstimacion,this.estimacionCoberturaSiniestroDTO.getDatosEstimacion().getEstimacionNueva());
					if(mensaje!=null){
						this.requiereAutorizacion = 1;
						super.setMensaje(mensaje);
						super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_INFORMACION);
					}else{
						estimacionCoberturaSiniestroService.guardarEstimacionCobertura( this.estimacionCoberturaSiniestroDTO, idCoberturaReporteCabina, tipoEstimacion );
						setMensaje(MensajeDTO.MENSAJE_GUARDAR_ESTIMACION);
						setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
						this.estimacionCoberturaSiniestroDTO.setDatosEstimacion(new  DatosEstimacionCoberturaDTO());
					}
				}else{
					
					// # NOTIFICACION DE SINIESTRO ANTIGUO
					this.estimacionCoberturaSiniestroService.notificaSolicitudSiniestroAntiguo(this.estimacionCoberturaSiniestroDTO, this.idReporteCabina);
					
					this.requiereAutorizacion = 2;
					super.setMensaje(MensajeDTO.MENSAJE_REQUIERE_AUT_RESERVA);
					super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_INFORMACION);
				}
			}
		} catch (Exception e) {
			setMensaje(MensajeDTO.MENSAJE_ERROR_GUARDAR_ESTIMACION);
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
		}
		
		return SUCCESS;
	}
	
	/**
	 * NO AUTORIZA la reserva, solo genera la solicitud
	 * @return
	 */
	@Action(value = "autorizarReserva", results = {
			@Result(name = SUCCESS, location = LOCATION_AJUSTERESERVA_JSP),
			@Result(name = INPUT, location = LOCATION_AJUSTERESERVA_JSP) })
	public String autorizarReserva(){
		Long idEstimacion = this.estimacionCoberturaSiniestroDTO.getEstimacionCoberturaReporte().getId();
		BigDecimal estimacionNueva = this.estimacionCoberturaSiniestroDTO.getDatosEstimacion().getEstimacionNueva();
		String causaMovimiento = this.estimacionCoberturaSiniestroDTO.getDatosEstimacion().getCausaMovimiento();
		if(this.requiereAutorizacion==1){
			this.catSolicitudAutorizacionService.solicitarAutorizacionReserva(idEstimacion,estimacionNueva, causaMovimiento );
		}else if(this.requiereAutorizacion==2){
			this.catSolicitudAutorizacionService.solicitarAutorizacionReservaAntiguedad(idEstimacion, estimacionNueva, this.idParametroReserva, causaMovimiento);
		}
		EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacion);
		this.idReporteCabina = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		this.estimacionCoberturaSiniestroDTO.setDatosEstimacion(new  DatosEstimacionCoberturaDTO());
		this.requiereAutorizacion = 0;
		super.setMensaje("Autorizacion Enviada");
		super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}
	
	private EstimacionCoberturaReporteCabina colocaEstimacionEnDTO(Long idEstimacion, String tipoEstimacion, EstimacionCoberturaSiniestroDTO dto){
		EstimacionCoberturaReporteCabina estimacion = null;
		if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.RC_BIENES)){
			dto.setEstimacionBienes(this.entidadService.findById(TerceroRCBienes.class,idEstimacion));
			estimacion = dto.getEstimacionBienes();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.DIRECTA)){
			dto.setEstimacionGenerica(this.entidadService.findById(EstimacionGenerica.class,idEstimacion));
			estimacion = dto.getEstimacionGenerica();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.GASTOS_MEDICOS)){
			dto.setEstimacionGastosMedicos(this.entidadService.findById(TerceroGastosMedicos.class,idEstimacion));
			estimacion = dto.getEstimacionGastosMedicos();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.RC_PERSONA)){
			dto.setEstimacionPersonas(this.entidadService.findById(TerceroRCPersonas.class,idEstimacion));
			estimacion = dto.getEstimacionPersonas();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.RC_VIAJERO)){
			dto.setEstimacionViajero(this.entidadService.findById(TerceroRCViajero.class,idEstimacion));
			estimacion = dto.getEstimacionViajero();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.GASTOS_MEDICOS_OCUPANTES)){
			dto.setEstimacionGastosMedicosConductor(this.entidadService.findById(TerceroGastosMedicosConductor.class,idEstimacion));
			estimacion = dto.getEstimacionGastosMedicosConductor();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.RC_VEHICULO)){
			dto.setEstimacionVehiculos(this.entidadService.findById(TerceroRCVehiculo.class,idEstimacion));
			estimacion = dto.getEstimacionVehiculos();
		}else if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.DANIOS_MATERIALES)){
			dto.setEstimacionDanosMateriales(this.entidadService.findById(TerceroDanosMateriales.class,idEstimacion));
			estimacion = dto.getEstimacionDanosMateriales();
		} 
		return estimacion;
	}

	

	public EstimacionCoberturaSiniestroDTO getEstimacionCoberturaSiniestroDTO() {
		return estimacionCoberturaSiniestroDTO;
	}



	public void setEstimacionCoberturaSiniestroDTO(
			EstimacionCoberturaSiniestroDTO estimacionCoberturaSiniestroDTO) {
		this.estimacionCoberturaSiniestroDTO = estimacionCoberturaSiniestroDTO;
	}



	public DatosReporteSiniestroDTO getDatosReporte() {
		return datosReporte;
	}

	public void setDatosReporte(DatosReporteSiniestroDTO datosReporte) {
		this.datosReporte = datosReporte;
	}

	public Long getIdEstimacionCobertura() {
		return idEstimacionCobertura;
	}

	public void setIdEstimacionCobertura(Long idEstimacionCobertura) {
		this.idEstimacionCobertura = idEstimacionCobertura;
	}

	public Long getIdReporteCabina() {
		return idReporteCabina;
	}

	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}

	public List<PaseAtencionSiniestroDTO> getListCoberturas() {
		return listCoberturas;
	}

	public void setListCoberturas(List<PaseAtencionSiniestroDTO> listCoberturas) {
		this.listCoberturas = listCoberturas;
	}

	public Map<String, String> getCausasMovimientoMap() {
		return causasMovimientoMap;
	}

	public void setCausasMovimientoMap(Map<String, String> causasMovimientoMap) {
		this.causasMovimientoMap = causasMovimientoMap;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

		public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}



	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}



	public String getNumeroReporte() {
		return numeroReporte;
	}



	public void setNumeroReporte(String numeroReporte) {
		this.numeroReporte = numeroReporte;
	}



	public String getNumeroPoliza() {
		return numeroPoliza;
	}



	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}



	public Integer getNumeroInciso() {
		return numeroInciso;
	}



	public void setNumeroInciso(Integer numeroInciso) {
		this.numeroInciso = numeroInciso;
	}



	public Long getIdReporte() {
		return idReporte;
	}

	public void setIdReporte(Long idReporte) {
		this.idReporte = idReporte;
	}


	public void setCatSolicitudAutorizacionService(
			CatSolicitudAutorizacionService catSolicitudAutorizacionService) {
		this.catSolicitudAutorizacionService = catSolicitudAutorizacionService;
	}



	public Integer getRequiereAutorizacion() {
		return requiereAutorizacion;
	}

	public void setRequiereAutorizacion(Integer requiereAutorizacion) {
		this.requiereAutorizacion = requiereAutorizacion;
	}



	public void setAutorizacionReservaService(
			AutorizacionReservaService autorizacionReservaService) {
		this.autorizacionReservaService = autorizacionReservaService;
	}



	public Long getIdParametroReserva() {
		return idParametroReserva;
	}



	public void setIdParametroReserva(Long idParametroReserva) {
		this.idParametroReserva = idParametroReserva;
	}

	
}