package mx.com.afirme.midas2.action.siniestros.catalogo.conceptos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoTipoPrestador;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ConceptoAjusteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.collections.map.MultiValueMap;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/catalogo/conceptos/conceptoAjusteAction")
public class ConceptoAjusteAction extends BaseAction implements Preparable {
	
	private static final short CATEGORIA_AFECTACIONRESERVA	= 1;
	private static final short CATEGORIA_GASTOAJUSTE	= 2;
	private static final short CATEGORIA_REEMBOLSO_GASTOAJUSTE	= 3;
	private static final String	LOCATION_CONCEPTOAJUSTE_JSP	= "/jsp/siniestros/catalogo/conceptos/conceptoAjuste.jsp";
	private static final Long CONCEPTO_FUE_EDITADO	= new Long(0);
	private static final Long CONCEPTO_YA_EXISTE = new Long(-2);
	private static final Long CONCEPTO_ERROR	= new Long(-1);
	
	private ConceptoAjuste        	conceptoAjuste;
	private List<ConceptoAjuste>  	conceptoAjusteGastosGrid;
	private List<ConceptoAjusteDTO> conceptoAjusteDtoGrid;
	private List<ConceptoAjuste>    conceptoAjusteAfectacionReservaGrid;
	private Map<String,String>    	ctgTipoCategoria                      = null;
	private Map<String,String>    	ctgEstatus                            = null;
	private Map<String,String>    	ctgTipoConcepto                       = null;
	private Map<Long,String>      	seccionDtoGrid                        = new LinkedHashMap<Long,String>();
	private List<CoberturaDTO>    	conceptosGrid;
	private List<TipoPrestadorServicio>    tipoPrestadorServicioGrid;

	// # ID CONCEPTO AJUSTE PARA ACTUALIZAR
	private Long 				  	keyNegocio;	                       
	// # ID CONCEPTO AJUSTE PARA ACTUALIZAR COBERTURAS
	private Long 				  	keyToCoberturasNegocio;	           
	// # STRING CON LAS COBERTURAS
	private String                	keyCoberturasLineasNegocio;        
	// # ID COMBO LINEA DE NEGOCIO
	private Long				  	keyLineaNegocio;                   
	// # NOMBRE DEL CONCEPTO SELECCIONADO
	private String                  nombreConcepto;                    

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("conceptoAjusteServiceEJB")
	private ConceptoAjusteService conceptoAjusteService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void prepare(){
		// # CARGA COMBOS
		this.ctgEstatus        = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.ESTATUS );
		this.ctgTipoCategoria  = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.TIPO_CONCEPTO );
		this.ctgTipoConcepto   = this.listadoService.obtenerCatalogoValorFijo( TIPO_CATALOGO.TIPO_CAT_CONCEPTOS );
		
	}
	

	//private void getComboSecciones(){
		//List<SeccionDTO> lSecciones = this.listadoService.getListarSeccionesVigentesAutosUsables();

		/*for( SeccionDTO lSeccion : lSecciones ){
			this.seccionDtoGrid.put( new Long( lSeccion.getIdToSeccion().toString() ), lSeccion.getDescripcion() );
		}*/

	//}
	
	public void prepareMostrar() {
		this.seccionDtoGrid = this.conceptoAjusteService.getListarSeccionesVigentesAutosUsablesSiniestros();
	}
	
	/**
	 * Metodo que direcciona al jsp principal conceptoAjuste.jsp
	 */
	@Action(value="mostrar", results={
			@Result(name = SUCCESS, location = LOCATION_CONCEPTOAJUSTE_JSP),
			@Result(name = INPUT, location= LOCATION_CONCEPTOAJUSTE_JSP)
	})
	public String mostrar(){
		return SUCCESS;
	}
	
	
	@Action(value="asociarCoberturas", results={
			@Result(name = SUCCESS, location = LOCATION_CONCEPTOAJUSTE_JSP),
			@Result(name = INPUT, location= LOCATION_CONCEPTOAJUSTE_JSP)
	})
	public String asociarCoberturas(){
		
		ArrayList<String> lListadoCoberturas = new ArrayList<String>();
		if( ! this.keyCoberturasLineasNegocio.isEmpty() ){
			
			// # LOS VALORES LLEGAN COMO 132-RCV , 34-N, 892-RCC 
			String[] keys = this.keyCoberturasLineasNegocio.split(",");
			for( int i = 0; i< keys.length; i++){
				lListadoCoberturas.add( keys[i] );
			}

			this.conceptoAjusteService.asociarCoberturas(keyToCoberturasNegocio,keyLineaNegocio, lListadoCoberturas);
			
		}else{
			return INPUT;
		}
		
		
		return SUCCESS;
	}
	
	/*Cambio Conseptos AIGG*/
	@Action(value="asociarTipoPrestador", results={
			@Result(name = SUCCESS, location = LOCATION_CONCEPTOAJUSTE_JSP),
			@Result(name = INPUT, location= LOCATION_CONCEPTOAJUSTE_JSP)
	})
	public String asociarTipoPrestador(){
		
		ArrayList<Long> lListadoPrestador= new ArrayList<Long>();
		if( ! this.keyCoberturasLineasNegocio.isEmpty() ){
			
			// # LOS VALORES LLEGAN COMO 132-RCV , 34-N, 892-RCC 
			String[] keys = this.keyCoberturasLineasNegocio.split(",");
			for( int i = 0; i< keys.length; i++){
				String id = keys[i];
				lListadoPrestador.add( new Long (id) );
			}
			
			this.conceptoAjusteService.asociarTipoPrestador(keyToCoberturasNegocio, lListadoPrestador);
			
		}else{
			return INPUT;
		}
		
		
		return SUCCESS;
	}
	
	

	/**
	 * Metodo de guardado de concepto, este se invoca en el botón de "Alta" de la
	 * pantalla a traves de entidadService.save
	 */
	@Action(value="guardar", results={
			@Result(name = SUCCESS, location = LOCATION_CONCEPTOAJUSTE_JSP),
			@Result(name = INPUT, location= LOCATION_CONCEPTOAJUSTE_JSP)
	})
	public String guardar(){
		
		try{ 
			
			// # EL BEAN VALIDATOR TIENE UNA VALIDACION ESPECIAL PARA PERMITIR SOLO 1 ó 2 COMO tipoConcepto, SI SE AGREGA UNO MAS AL CATÁLOGO SERA NECESARIO AGREGARLO AL REGEX
			
			if ( this.keyNegocio != null ){
				this.conceptoAjuste.setId(this.keyNegocio);
			}
			
			this.keyNegocio = this.conceptoAjusteService.crearCobertura(this.conceptoAjuste);
			
			if( this.keyNegocio.longValue() == CONCEPTO_ERROR.longValue() ){
				super.setMensajeError(this.getText("midas.siniestros.catalogo.conceptos.error.alta.clave.concepto"));
			}else if ( this.keyNegocio.longValue() == CONCEPTO_FUE_EDITADO.longValue() ){
				super.setMensaje(this.getText("midas.siniestros.catalogo.conceptos.error.edicion.concepto"));
			}else if ( this.keyNegocio.longValue() > 0 ){
				super.setMensaje(this.getText("midas.siniestros.catalogo.conceptos.alta.exitosa"));
			}else if ( this.keyNegocio.longValue() == CONCEPTO_YA_EXISTE.longValue() ){
				super.setMensaje(this.getText("Ya existe un concepto con ese nombre"));
			}
		
		}catch(Exception e){
			
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			if (e.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)e.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "conceptoAjuste");
			}
			
			return INPUT;
		}
		
		return SUCCESS;
	}

	/**
	 * Este método recibe el tipo de concepto y devuelve al jsp de
	 * listadoConceptoAjuste donde se mostraran en un grid los conceptos
	 * correspondientes al tipo seleccionado.
	 */
	@Action(value="listadoGastosAjuste", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/contedorAjusteGastosGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/contedorAjusteGastosGrid.jsp")
	})
	public String listadoGastosAjuste(){
		this.conceptoAjusteGastosGrid = this.conceptoAjusteService.listarConceptos(CATEGORIA_GASTOAJUSTE);
		this.ordenaConceptoAjusteGastosGrid();
		this.modificaCaracteres( this.conceptoAjusteGastosGrid );
		
		return SUCCESS;
	}
	
	/**
	 * Este método recibe el tipo de concepto y devuelve al jsp de
	 * listadoConceptoAjuste donde se mostraran en un grid los conceptos
	 * correspondientes al tipo seleccionado.
	 */
	@Action(value="listarAfectacionReserva", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/contedorAjusteAfectacionGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/contedorAjusteAfectacionGrid.jsp")
	})
	public String listarAfectacionReserva(){
		this.conceptoAjusteAfectacionReservaGrid  = this.conceptoAjusteService.listarConceptos(CATEGORIA_AFECTACIONRESERVA);
		this.ordenaConceptoAjusteAfectacionReservaGrid();
		this.modificaCaracteres( this.conceptoAjusteAfectacionReservaGrid );
		
		return SUCCESS;
	}
	
	
	@Action(value="listarReembolso", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/contedorReembolsoGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/contedorReembolsoGrid.jsp")
	})
	public String listarReembolso(){
		this.conceptoAjusteAfectacionReservaGrid  = this.conceptoAjusteService.listarConceptos(CATEGORIA_REEMBOLSO_GASTOAJUSTE);
		this.ordenaConceptoAjusteAfectacionReservaGrid();
		this.modificaCaracteres( this.conceptoAjusteAfectacionReservaGrid );
		
		return SUCCESS;
	}	
	
	private void modificaCaracteres( List<ConceptoAjuste> lConceptoAjuste){
		// CAMBIA CARACTERES
		
		for(ConceptoAjuste lConcepto : lConceptoAjuste ){
			lConcepto.setNombre(HtmlUtils.htmlEscape(lConcepto.getNombre()));
		}
	}
	
	private void ordenaConceptoAjusteGastosGrid(){
		
		if(conceptoAjusteGastosGrid != null && !conceptoAjusteGastosGrid.isEmpty()){
			Collections.sort(conceptoAjusteGastosGrid, 
					new Comparator<ConceptoAjuste>() {				
						public int compare(ConceptoAjuste n1, ConceptoAjuste n2){
							return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
						}
					});
		}
	}		
	
	private void ordenaConceptoAjusteAfectacionReservaGrid(){
		
		if(conceptoAjusteAfectacionReservaGrid != null && !conceptoAjusteAfectacionReservaGrid.isEmpty()){
			Collections.sort(conceptoAjusteAfectacionReservaGrid, 
					new Comparator<ConceptoAjuste>() {				
						public int compare(ConceptoAjuste n1, ConceptoAjuste n2){
							return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
						}
					});
		}
	}
	
	
	@Action(value="listarTipoPrestador", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/tipoPrestadorGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/tipoPrestadorGrid.jsp")
	}) 
	public String listarTipoPrestador(){
		Map<Long, String>  MTipoPrestadorServicio;
		List<TipoPrestadorServicio>    allTipoPrestador = new ArrayList<TipoPrestadorServicio>();
		List<ConceptoTipoPrestador>    tipoPrestadorAsociado =new ArrayList<ConceptoTipoPrestador>();
		// # OBTIENE TODAS LAS COBERTURAS DE LA SECCION
		this.tipoPrestadorServicioGrid = new ArrayList<TipoPrestadorServicio>();
		MTipoPrestadorServicio = this.listadoService.obtenerTipoPrestadorGastoAjuste();
		if(!MTipoPrestadorServicio.isEmpty()){
			for(Map.Entry<Long,String> map : MTipoPrestadorServicio.entrySet()){
				TipoPrestadorServicio tipoPrestador=this.entidadService.findById(TipoPrestadorServicio.class, map.getKey());
				tipoPrestador.setNombre("0");
				allTipoPrestador.add(tipoPrestador);
			}
		}		
		tipoPrestadorAsociado = this.conceptoAjusteService.obtenerTipoPrestadorAsociados(this.keyToCoberturasNegocio);
		
		for (TipoPrestadorServicio tipo:allTipoPrestador){
			
			//TipoPrestadorServicio tipoPrestadorServicio = tipo;
			
			for ( ConceptoTipoPrestador conceptoTipo : tipoPrestadorAsociado){
				if(tipo.getId()==conceptoTipo.getTipoPrestadorServicio().getId()){
					tipo.setNombre("1");
				} 
			}
			this.tipoPrestadorServicioGrid.add(tipo);

		}
		return SUCCESS;
	}
	
	@SuppressWarnings("rawtypes")
	@Action(value="listarCoberturas", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/coberturasGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/coberturasGrid.jsp")
	})
	public String listarCoberturas(){
		
		String claveSubcalculo = "n";
		Long coberturaId;
		List lMultiSelecionadas;
		List<CoberturaSeccionSiniestroDTO> lCoberturasSiniestro;
		
		// # OBTIENE TODAS LAS COBERTURAS DE LA SECCION
		lCoberturasSiniestro = this.estimacionCoberturaSiniestroService.obtenerCoberturasPorSeccion(this.keyLineaNegocio);

		// # FILTRA COBERTURAS ASOCIADAS A LA LINEA DE NEGOCIO Y CONCEPTO COBERTURA SELECCIONADAS POR EL USUARIO (SIN FILTRAR POR CLAVE_SUBCOVERTURA)
		MultiValueMap lConceptoCoberturaSeleccionadas = this.conceptoAjusteService.obtenerCobertuasAsociadas(this.keyToCoberturasNegocio, this.keyLineaNegocio);
		
		// # CREA ARRAYLIST PARA ALMACENAR LA INFORMACION
		this.conceptosGrid = new ArrayList<CoberturaDTO>();
		
		for ( CoberturaSeccionSiniestroDTO lCoberturaSeccion : lCoberturasSiniestro ){
			
			CoberturaDTO coberturaDto       = new CoberturaDTO();
			coberturaDto.setIdToCobertura   ( lCoberturaSeccion.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura() );
			// # OBTENER COBERTURA
			coberturaDto.setDescripcion     ( this.conceptoAjusteService.obtenerDatoLineaNegocio( lCoberturaSeccion.getNombreCobertura() )[0] );
			coberturaDto.setNombreComercial ( this.conceptoAjusteService.obtenerDatoLineaNegocio( lCoberturaSeccion.getNombreCobertura() )[1] );
			coberturaDto.setClaveTipoCalculo( ( StringUtil.isEmpty(lCoberturaSeccion.getClaveSubCalculo())? "n" : lCoberturaSeccion.getClaveSubCalculo() ) );
			coberturaDto.setClaveEstatus    ( (short) 0);
			
			claveSubcalculo = ( StringUtil.isEmpty(lCoberturaSeccion.getClaveSubCalculo()) ? "n" : lCoberturaSeccion.getClaveSubCalculo() );
			coberturaId     = new Long ( lCoberturaSeccion.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().toString()) ;
			
			Set entrySet = lConceptoCoberturaSeleccionadas.entrySet();
			Iterator it = entrySet.iterator();
			while (it.hasNext()) {
				
				Map.Entry mapEntry = (Map.Entry) it.next();
				lMultiSelecionadas = (List) lConceptoCoberturaSeleccionadas.get(mapEntry.getKey());
				for (int j = 0; j < lMultiSelecionadas.size(); j++) {
					if ( ( lMultiSelecionadas.get(j).toString().equals( coberturaId.toString() ) ) & ( claveSubcalculo.trim().equals(mapEntry.getKey().toString().trim()) ) ){
						coberturaDto.setClaveEstatus( (short) 1);
						break;
					}
				}
				
			}

			
			this.conceptosGrid.add(coberturaDto);
		}
		
		return SUCCESS;
	}
	
	
	@Action(value="mostrarCoberturasConceptoAsociadas", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/mostrarCoberturasConcepto.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/mostrarCoberturasConcepto.jsp")
	})
	public String mostrarCoberturasConceptoAsociadas(){
		return SUCCESS;
	}
	
	
	@Action(value="mostrarCoberturasConceptoAsociadasGrid", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/conceptos/mostrarCoberturasConceptoAsociadasGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/siniestros/catalogo/conceptos/mostrarCoberturasConceptoAsociadasGrid.jsp")
	})
	public String mostrarCoberturasConceptoGrid(){
		
		this.conceptoAjusteDtoGrid = this.conceptoAjusteService.obtenerCoberturasPorConcepto(this.keyToCoberturasNegocio);
		
		return SUCCESS;
	}	


	public ConceptoAjuste getConceptoAjuste() {
		return conceptoAjuste;
	}

	public void setConceptoAjuste(ConceptoAjuste conceptoAjuste) {
		this.conceptoAjuste = conceptoAjuste;
	}

	public Map<String, String> getCtgTipoConcepto() {
		return ctgTipoConcepto;
	}

	public void setCtgTipoConcepto(Map<String, String> ctgTipoConcepto) {
		this.ctgTipoConcepto = ctgTipoConcepto;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getCtgEstatus() {
		return ctgEstatus;
	}

	public void setCtgEstatus(Map<String, String> ctgEstatus) {
		this.ctgEstatus = ctgEstatus;
	}

	public ConceptoAjusteService getConceptoAjusteService() {
		return conceptoAjusteService;
	}

	public void setConceptoAjusteService(ConceptoAjusteService conceptoAjusteService) {
		this.conceptoAjusteService = conceptoAjusteService;
	}

	public List<ConceptoAjuste> getConceptoAjusteGastosGrid() {
		return conceptoAjusteGastosGrid;
	}

	public List<ConceptoAjuste> getConceptoAjusteAfectacionReservaGrid() {
		return conceptoAjusteAfectacionReservaGrid;
	}

	public void setConceptoAjusteGastosGrid(
			List<ConceptoAjuste> conceptoAjusteGastosGrid) {
		this.conceptoAjusteGastosGrid = conceptoAjusteGastosGrid;
	}

	public void setConceptoAjusteAfectacionReservaGrid(
			List<ConceptoAjuste> conceptoAjusteAfectacionReservaGrid) {
		this.conceptoAjusteAfectacionReservaGrid = conceptoAjusteAfectacionReservaGrid;
	}

	public Map<Long, String> getSeccionDtoGrid() {
		return seccionDtoGrid;
	}

	public void setSeccionDtoGrid(Map<Long, String> seccionDtoGrid) {
		this.seccionDtoGrid = seccionDtoGrid;
	}

	public Long getKeyNegocio() {
		return keyNegocio;
	}

	public void setKeyNegocio(Long keyNegocio) {
		this.keyNegocio = keyNegocio;
	}

	public List<CoberturaDTO> getConceptosGrid() {
		return conceptosGrid;
	}

	public void setConceptosGrid(List<CoberturaDTO> conceptosGrid) {
		this.conceptosGrid = conceptosGrid;
	}

	public String getKeyCoberturasLineasNegocio() {
		return keyCoberturasLineasNegocio;
	}

	public void setKeyCoberturasLineasNegocio(String keyCoberturasLineasNegocio) {
		this.keyCoberturasLineasNegocio = keyCoberturasLineasNegocio;
	}

	public Long getKeyToCoberturasNegocio() {
		return keyToCoberturasNegocio;
	}

	public void setKeyToCoberturasNegocio(Long keyToCoberturasNegocio) {
		this.keyToCoberturasNegocio = keyToCoberturasNegocio;
	}

	public Long getKeyLineaNegocio() {
		return keyLineaNegocio;
	}

	public void setKeyLineaNegocio(Long keyLineaNegocio) {
		this.keyLineaNegocio = keyLineaNegocio;
	}

	public Map<String, String> getCtgTipoCategoria() {
		return ctgTipoCategoria;
	}

	public void setCtgTipoCategoria(Map<String, String> ctgTipoCategoria) {
		this.ctgTipoCategoria = ctgTipoCategoria;
	}


	public List<TipoPrestadorServicio> getTipoPrestadorServicioGrid() {
		return tipoPrestadorServicioGrid;
	}


	public void setTipoPrestadorServicioGrid(
			List<TipoPrestadorServicio> tipoPrestadorServicioGrid) {
		this.tipoPrestadorServicioGrid = tipoPrestadorServicioGrid;
	}

	public List<ConceptoAjusteDTO> getConceptoAjusteDtoGrid() {
		return conceptoAjusteDtoGrid;
	}

	public void setConceptoAjusteDtoGrid(
			List<ConceptoAjusteDTO> conceptoAjusteDtoGrid) {
		this.conceptoAjusteDtoGrid = conceptoAjusteDtoGrid;
	}
	
	public String getNombreConcepto() {
		return nombreConcepto;
	}


	public void setNombreConcepto(String nombreConcepto) {
		this.nombreConcepto = nombreConcepto;
	}
}
