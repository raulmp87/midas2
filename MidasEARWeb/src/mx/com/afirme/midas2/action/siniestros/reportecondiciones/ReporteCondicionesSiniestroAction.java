package mx.com.afirme.midas2.action.siniestros.reportecondiciones;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.service.siniestros.reportecondiciones.ReporteCondicionesSiniestroService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/siniestros/reportecondiciones")
public class ReporteCondicionesSiniestroAction extends BaseAction {
	
	private Date fechaSiniestroIni;
	private Date fechaSiniestroFin;
	private String numeroPolizaMidas;
	private String numeroPolizaSeycos;
	private Short tipoPoliza;
	private Long idGerencia;
	private Long idCondicionEspecial;
	private List<GerenciaView>  lstGerencias;
	private List<CondicionEspecial> lstCondiciones;
	private TransporteImpresionDTO transporte;
	
	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	private CondicionEspecialService condicionEspecialService;
	
	@Autowired
	@Qualifier("reporteCondSiniestroServiceEJB")
	private ReporteCondicionesSiniestroService reporteCondicionesSiniestroService;
	
	@Autowired
	@Qualifier("gerenciaJPAServiceEJB")
	private GerenciaJPAService gerenciaJpaService;

	private static final long serialVersionUID = 7522973375143837441L;

	@Action(value = "mostrar", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/reportecondiciones/reporteCondicionesSiniestro.jsp") })
	public String mostrar() {		
		lstCondiciones = condicionEspecialService.obtenerCondiciones(null);
		lstGerencias = gerenciaJpaService.getList(true);
		
		return SUCCESS;
	}
	

	@Action(value = "generarReporte", results = {
			@Result(name = SUCCESS, 
					type = "stream", 
					params = { 
						"contentType","${transporte.contentType}", 
						"contentDisposition", "attachment;filename=\"${transporte.fileName}\"", 
						"inputName","transporte.genericInputStream" 
					}),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" })})
	public String generarReporte() {		
		transporte = new TransporteImpresionDTO();
		String numeroPoliza = null;
		if (numeroPolizaMidas != null && !numeroPolizaMidas.equals("")) {
			numeroPoliza = numeroPolizaMidas;
		}
		if (numeroPolizaSeycos != null && !numeroPolizaSeycos.equals("")) {
			numeroPoliza = numeroPolizaSeycos;
		}
		InputStream input = reporteCondicionesSiniestroService.generaReporte(numeroPoliza, tipoPoliza, idGerencia, idCondicionEspecial, fechaSiniestroIni, fechaSiniestroFin);
		
		if (input == null) {
			setMensaje("No se encontró información para los criterios de búsqueda");
			return INPUT;
		} else {
			transporte.setGenericInputStream(input);
			transporte.setContentType("application/vnd.ms-excel");
			transporte.setFileName("Reporte de Condiciones Especiales.xls");		
		}
		
		
		return SUCCESS;
	}


	public Date getFechaSiniestroIni() {
		return fechaSiniestroIni;
	}


	public void setFechaSiniestroIni(Date fechaSiniestroIni) {
		this.fechaSiniestroIni = fechaSiniestroIni;
	}


	public Date getFechaSiniestroFin() {
		return fechaSiniestroFin;
	}


	public void setFechaSiniestroFin(Date fechaSiniestroFin) {
		this.fechaSiniestroFin = fechaSiniestroFin;
	}


	public String getNumeroPolizaMidas() {
		return numeroPolizaMidas;
	}


	public void setNumeroPolizaMidas(String numeroPolizaMidas) {
		this.numeroPolizaMidas = numeroPolizaMidas;
	}


	public String getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}


	public void setNumeroPolizaSeycos(String numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}


	public Short getTipoPoliza() {
		return tipoPoliza;
	}


	public void setTipoPoliza(Short tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}


	public Long getIdGerencia() {
		return idGerencia;
	}


	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}


	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}


	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}


	public List<GerenciaView>  getLstGerencias() {
		return lstGerencias;
	}


	public void setLstGerencias(List<GerenciaView>  lstGerencias) {
		this.lstGerencias = lstGerencias;
	}


	public List<CondicionEspecial> getLstCondiciones() {
		return lstCondiciones;
	}


	public void setLstCondiciones(List<CondicionEspecial> lstCondiciones) {
		this.lstCondiciones = lstCondiciones;
	}


	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}


	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	
}
