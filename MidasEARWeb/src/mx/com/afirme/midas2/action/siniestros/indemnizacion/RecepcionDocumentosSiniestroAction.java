package mx.com.afirme.midas2.action.siniestros.indemnizacion;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaBajaPlacasDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.js.util.StringUtil;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value="/siniestros/indemnizacion/cartas/recepciondoctos")
public class RecepcionDocumentosSiniestroAction extends BaseAction implements Preparable{

	private static final String	LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP	= "/jsp/siniestros/indemnizacion/contenedorRecepcionDocumentosSiniestro.jsp";

	private static final long serialVersionUID = 7927194712484418862L;
	
	private Long siniestroId;
	private String tipoCarta;
	private String 	tipoSiniestro;
	private boolean mostrarCartaPT;


	private RecepcionDocumentosSiniestro recepcionDoctoSin;
	private CartaBajaPlacasDTO informacionAuto;
	private String fechaActual;
	private String numeroSiniestro;
	
	private Map<String, String> tiposPersona;
	private Map<String, String> formasPago;
	private Map<String, String> refrendosTenencias;
	private Map<Long, String> bancos;
	
	private Integer esConsulta;
	private Integer guardadoCorrecto;
	private PerdidaTotalFiltro 	filtroPT;
	private TransporteImpresionDTO transporte;
	
	private Long idIndemnizacion;
	
	@Autowired
	@Qualifier("cartaSiniestroServiceEJB")
	private CartaSiniestroService cartaSiniestroService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Override
	public void prepare(){
	}
	
	public void prepareMostrarRecepcionDoctos(){
		recepcionDoctoSin = cartaSiniestroService.mostrarRecepcionDeDocumentos(idIndemnizacion);
		if(StringUtil.isEmpty(recepcionDoctoSin.getBeneficiario())){
			recepcionDoctoSin.setBeneficiario(obtenerBeneficiarioOrdenCompra(idIndemnizacion));
		}
		informacionAuto = cartaSiniestroService.obtenerInformacionAutoRecepcionDocumentos(idIndemnizacion);
		if(recepcionDoctoSin != null){
              guardadoCorrecto = 1;
        }
		
		if(!tipoSiniestro.equalsIgnoreCase("RB")){
			recepcionDoctoSin.setDenunciaRobo("NA");
		}
		this.mostrarCartaPT=false;
		List<RecepcionDocumentosSiniestro> 	resultados= 
			entidadService.findByProperty(RecepcionDocumentosSiniestro.class, "indemnizacion.id", idIndemnizacion);
		CartaSiniestro	cartaSiniestro = cartaSiniestroService.obtenerCartaEntregaDocumentos(idIndemnizacion);
		if( null==cartaSiniestro && resultados.isEmpty()){
			this.mostrarCartaPT=true;
		}
	}
	
	private String obtenerBeneficiarioOrdenCompra(Long idIndemnizacion){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		if(indemnizacion.getOrdenCompra() != null 
				&& !StringUtil.isEmpty(indemnizacion.getOrdenCompra().getNomBeneficiario())){
			return indemnizacion.getOrdenCompra().getNomBeneficiario();
		}
		return null;
	}
	
	@Action(value = "mostrarRecepcionDoctos", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP)})
	public String mostrarRecepcionDoctos(){
		return SUCCESS;
	}
	
	@Action(value = "guardarRecepcionDoctos", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP)})
	public String guardarRecepcionDoctos(){
		try{
			if(!tipoSiniestro.equalsIgnoreCase("RB")){
				recepcionDoctoSin.setDenunciaRobo("NA");
			} 
			List<String> validacionRecepcionDoctos = 
				cartaSiniestroService.validarRecepcionDocumentos(recepcionDoctoSin, informacionAuto, numeroSiniestro, tipoCarta);
			if((recepcionDoctoSin.getEsPagoDanios() != null && recepcionDoctoSin.getEsPagoDanios()) || validacionRecepcionDoctos.size() == 0){
				cartaSiniestroService.guardarRecepcionDocumentos(recepcionDoctoSin, idIndemnizacion);
				guardadoCorrecto = 1;
			}else{
				guardadoCorrecto = 0;
				setMensajeListaPersonalizado("Los siguientes campos son obligatorios", obtenerMensajesError(validacionRecepcionDoctos), "10");
				return INPUT;
			}
		}catch(Exception ex){
			guardadoCorrecto = 0;
			setMensajeError("No se pudieron guardar los cambios");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "validarRecepcionDoctos", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDOR_RECEPCION_DOCUMENTOS_SINIESTRO_JSP)})
	public String validarRecepcionDoctos(){
		try{
			
		}catch(Exception ex){
			setMensajeError("No se pudieron guardar los cambios");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "imprimirRecepcionDoctos", results = {
			@Result(name=SUCCESS,type="stream",
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirRecepcionDoctos(){
		try{
				transporte = cartaSiniestroService.imprimirRecepcionDocumentosSiniestro(numeroSiniestro, recepcionDoctoSin, informacionAuto);
				transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
				transporte.setContentType("application/pdf");
				
				String fileName =  numeroSiniestro + "_" + "RecepcionDocumentos" + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
				transporte.setFileName(fileName);
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		return SUCCESS;
	}
	
	/**
	 * Obtiene la descripcion del mensaje de error del properties para mostrarlo en la pantalla
	 * @param mensajesErrorProperties
	 * @return
	 */
	private List<String> obtenerMensajesError(List<String> mensajesErrorProperties){
		List<String> mensajesError = new ArrayList<String>();
		for(String mensajeProperties: mensajesErrorProperties){
			mensajesError.add(getText(mensajeProperties));
		}
		return mensajesError;
	}

	/**
	 * @return the recepcionDoctoSin
	 */
	public RecepcionDocumentosSiniestro getRecepcionDoctoSin() {
		return recepcionDoctoSin;
	}

	/**
	 * @param recepcionDoctoSin the recepcionDoctoSin to set
	 */
	public void setRecepcionDoctoSin(RecepcionDocumentosSiniestro recepcionDoctoSin) {
		this.recepcionDoctoSin = recepcionDoctoSin;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	/**
	 * @return the incisoSiniestro
	 */
//	public IncisoSiniestroDTO getIncisoSiniestro() {
//		return incisoSiniestro;
//	}
//
//	/**
//	 * @param incisoSiniestro the incisoSiniestro to set
//	 */
//	public void setIncisoSiniestro(IncisoSiniestroDTO incisoSiniestro) {
//		this.incisoSiniestro = incisoSiniestro;
//	}
	
	/**
	 * @return the siniestroId
	 */
	public Long getSiniestroId() {
		return siniestroId;
	}

	/**
	 * @param siniestroId the siniestroId to set
	 */
	public void setSiniestroId(Long siniestroId) {
		this.siniestroId = siniestroId;
	}

	/**
	 * @return the tipoCarta
	 */
	public String getTipoCarta() {
		return tipoCarta;
	}

	/**
	 * @param tipoCarta the tipoCarta to set
	 */
	public void setTipoCarta(String tipoCarta) {
		this.tipoCarta = tipoCarta;
	}

	/**
	 * @return the fechaActual
	 */
	public String getFechaActual() {
		if(fechaActual == null || fechaActual.isEmpty()){
			fechaActual = new SimpleDateFormat("dd/MM/yyyy",new Locale("es")).format(new Date());
		}
		return fechaActual;
	}

	/**
	 * @param fechaActual the fechaActual to set
	 */
	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	/**
	 * @return the tiposPersona
	 */
	public Map<String, String> getTiposPersona() {
		if(tiposPersona == null || tiposPersona.isEmpty()){
			tiposPersona = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PERSONA);
		}
		return tiposPersona;
	}

	/**
	 * @param tiposPersona the tiposPersona to set
	 */
	public void setTiposPersona(Map<String, String> tiposPersona) {
		this.tiposPersona = tiposPersona;
	}

	/**
	 * @return the formasPago
	 */
	public Map<String, String> getFormasPago() {
		if(formasPago == null || formasPago.isEmpty()){
			formasPago = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.FORMA_PAGO);
		}
		return formasPago;
	}

	/**
	 * @param formasPago the formasPago to set
	 */
	public void setFormasPago(Map<String, String> formasPago) {
		this.formasPago = formasPago;
	}

	/**
	 * @return the refrendosTenencias
	 */
	public Map<String, String> getRefrendosTenencias() {
		if(refrendosTenencias == null || refrendosTenencias.isEmpty()){
			if(informacionAuto.getModelo() != null && 
					recepcionDoctoSin.getSiniestroCabina().getReporteCabina().getFechaOcurrido() != null){
				int modelo			 = Integer.parseInt(informacionAuto.getModelo());
				Date fechaOcurrido	 = recepcionDoctoSin.getSiniestroCabina().getReporteCabina().getFechaOcurrido();
				refrendosTenencias	 = cartaSiniestroService.obtenerListaRefrendosTenenciasParaRecepcionDoctos(modelo, fechaOcurrido);
			}else{
				refrendosTenencias	 = new HashMap<String, String>();
			}
		}
		return refrendosTenencias;
	}

	/**
	 * @param refrendosTenencias the refrendosTenencias to set
	 */
	public void setRefrendosTenencias(Map<String, String> refrendosTenencias) {
		this.refrendosTenencias = refrendosTenencias;
	}
	
	public String[] getTenenciasSeleccionadas(){
		if(recepcionDoctoSin != null && recepcionDoctoSin.getRefrendoTenencia() != null){
			return  recepcionDoctoSin.getRefrendoTenencia().split(", ");
		}else{
			return new String[0];
		}
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		if(recepcionDoctoSin != null && recepcionDoctoSin.getSiniestroCabina() != null && numeroSiniestro == null){
			numeroSiniestro = recepcionDoctoSin.getSiniestroCabina().getNumeroSiniestro();
		}
		return numeroSiniestro;
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	/**
	 * @return the esConsulta
	 */
	public Integer getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Integer esConsulta) {
		this.esConsulta = esConsulta;
	}

	/**
	 * @return the guardadoCorrecto
	 */
	public Integer getGuardadoCorrecto() {
		return guardadoCorrecto;
	}

	/**
	 * @param guardadoCorrecto the guardadoCorrecto to set
	 */
	public void setGuardadoCorrecto(Integer guardadoCorrecto) {
		this.guardadoCorrecto = guardadoCorrecto;
	}

	/**
	 * @return the filtroPT
	 */
	public PerdidaTotalFiltro getFiltroPT() {
		return filtroPT;
	}

	/**
	 * @param filtroPT the filtroPT to set
	 */
	public void setFiltroPT(PerdidaTotalFiltro filtroPT) {
		this.filtroPT = filtroPT;
	}

	/**
	 * @return the idIndemnizacion
	 */
	public Long getIdIndemnizacion() {
		return idIndemnizacion;
	}

	/**
	 * @param idIndemnizacion the idIndemnizacion to set
	 */
	public void setIdIndemnizacion(Long idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}

	/**
	 * @return the informacionAuto
	 */
	public CartaBajaPlacasDTO getInformacionAuto() {
		return informacionAuto;
	}

	/**
	 * @param informacionAuto the informacionAuto to set
	 */
	public void setInformacionAuto(CartaBajaPlacasDTO informacionAuto) {
		this.informacionAuto = informacionAuto;
	}

	/**
	 * @return the tipoSiniestro
	 */
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}

	/**
	 * @param tipoSiniestro the tipoSiniestro to set
	 */
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	/**
	 * @return the mostrarCartaPT
	 */
	public boolean isMostrarCartaPT() {
		return mostrarCartaPT;
	}

	/**
	 * @param mostrarCartaPT the mostrarCartaPT to set
	 */
	public void setMostrarCartaPT(boolean mostrarCartaPT) {
		this.mostrarCartaPT = mostrarCartaPT;
	}

	public Map<Long, String> getBancos() {
		bancos = listadoService.getMapBancosMidas();
		return bancos;
	}

	public void setBancos(Map<Long, String> bancos) {
		this.bancos = bancos;
	}

	

}