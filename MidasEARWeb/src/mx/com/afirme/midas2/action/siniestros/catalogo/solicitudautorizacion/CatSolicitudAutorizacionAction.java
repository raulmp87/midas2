/**
 * 
 */
package mx.com.afirme.midas2.action.siniestros.catalogo.solicitudautorizacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService.SolicitudAutorizacionFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author admin
 *
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion")
public class CatSolicitudAutorizacionAction  extends BaseAction implements Preparable{
	
	private static final String	LOCATION_CONTENEDORLISTADOSOLICITUDAUTORIZACION_JSP	= "/jsp/siniestros/catalogo/solicitudautorizacion/contenedorListadoSolicitudAutorizacion.jsp";
	private static final String	LOCATION_CONTENEDORCOMENTARIOS_JSP	= "/jsp/siniestros/catalogo/solicitudautorizacion/contenedorComentarios.jsp";

	private Map<String,String> autorizadores;
	private Map<Long,String> oficinasMap;
	private Map<String,String> estatusMap;
	private Map<String,String> tipoSolicitudMap;
	private SolicitudAutorizacion entidad;
	private SolicitudAutorizacionFiltro filtroBusqueda;
	private List<SolicitudAutorizacionDTO> resultados;
	private Long idSolicitudAutorizacion;
	private int estatus;
	private String tipoSolicitud;
	private String comentarios;
	private TransporteImpresionDTO transporte;
	private String nombreAccion; // MUESTRA EN LA VENTANA SI VA AUTORIZAR O RECHAZAR
	private Long idSolicitudPorTipoAutorizacion; // GUARDA EL NO. SOLICITUD QUE SE MUESTRA EN EL GRID 
	
	
	private CatSolicitudAutorizacionService catSolicitudAutorizacionService;
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	public void prepare(){
		this.oficinasMap = catSolicitudAutorizacionService.getOficinasMap();
		this.estatusMap = catSolicitudAutorizacionService.getEstatusMap();
		this.tipoSolicitudMap = catSolicitudAutorizacionService.getTipoSolicitudMap();
	}
	
	
	public void  prepareMostrarsolicitud() {
		
	}
	
	@Action(value = "mostrarContenedorComentarios", 
			results = { 
			@Result(name = SUCCESS, location =LOCATION_CONTENEDORCOMENTARIOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORCOMENTARIOS_JSP)
			})
			public String mostrarContenedorComentarios(){
				nombreAccion =  ( this.estatus == 1 ? "Autorizar" : "Rechazar" );
				nombreAccion += ( this.tipoSolicitud.equals("RES") ? " Ajuste de Reserva" : " Vigencia" );
				return SUCCESS;
			}	
	
	@Action(value="guardarComentarios", results={
			@Result(name=INPUT,location=LOCATION_CONTENEDORCOMENTARIOS_JSP),
			@Result(name = SUCCESS, type = "redirectAction", params = { 
					"actionName", "cambiarEstatus", 
					"namespace", "/siniestros/catalogo/solicitudautorizacion/catSolicitudAutorizacion",
					"idSolicitudAutorizacion","${idSolicitudAutorizacion}",
					"estatus","${estatus}",
					"tipoSolicitud","${tipoSolicitud}",
					"comentarios","${comentarios}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"})
	})
	public String guardarComentarios(){
		
		try {
			this.catSolicitudAutorizacionService.autorizaRechazaSolicitud(idSolicitudAutorizacion, estatus , tipoSolicitud,this.comentarios);
			return SUCCESS;
		}catch(Exception e) {
			return INPUT;
		}
		
	}	
	
	@Action
	(value = "mostrarBusquedaSolicitudesAutorizacion", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTADOSOLICITUDAUTORIZACION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTADOSOLICITUDAUTORIZACION_JSP)
	})
	public String mostrarBusquedaSolicitudesAutorizacion(){
		
		return SUCCESS;
	}
	

	
	@Action
	(value = "obtenerSolicitudesAutorizadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/solicitudautorizacion/listadoSolicitudAutorizacionGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTADOSOLICITUDAUTORIZACION_JSP)
	})
	public String obtenerSolicitudesAutorizadas(){
		if(filtroBusqueda == null){
			this.resultados = new ArrayList<SolicitudAutorizacionDTO>(); 
		}else{
			this.resultados = catSolicitudAutorizacionService.buscar(filtroBusqueda);
		}
		return SUCCESS;
	}

	
	@Action
	(value = "cambiarEstatus", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORLISTADOSOLICITUDAUTORIZACION_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORLISTADOSOLICITUDAUTORIZACION_JSP)
	})
	public String cambiarEstatus(){
		catSolicitudAutorizacionService.autorizacion(idSolicitudAutorizacion, estatus , tipoSolicitud);
		return SUCCESS;
	}
	
	
	@Action(value="exportarResultados",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarResultados(){
		this.resultados = catSolicitudAutorizacionService.buscar(filtroBusqueda);
		
		ExcelExporter exporter = new ExcelExporter(SolicitudAutorizacionDTO.class);
		transporte = exporter.exportXLS(resultados, "Solicitudes de Autorizaci\u00f3n");
		
		return SUCCESS;
		
	}
	
	
	/**
	 * ********************************************************************************************************************************
	 * **************************************************    GET & SET    *************************************************************
	 * ********************************************************************************************************************************
	 */

	public Map<String, String> getAutorizadores() {
		return autorizadores;
	}


	public void setAutorizadores(Map<String, String> autorizadores) {
		this.autorizadores = autorizadores;
	}


	public Map<Long, String> getOficinasMap() {
		return oficinasMap;
	}


	public void setOficinasMap(Map<Long, String> oficinasMap) {
		this.oficinasMap = oficinasMap;
	}


	public Map<String, String> getEstatusMap() {
		return estatusMap;
	}


	public void setEstatusMap(Map<String, String> estatusMap) {
		this.estatusMap = estatusMap;
	}


	public SolicitudAutorizacion getEntidad() {
		return entidad;
	}


	public void setEntidad(SolicitudAutorizacion entidad) {
		this.entidad = entidad;
	}




	public SolicitudAutorizacionFiltro getFiltroBusqueda() {
		return filtroBusqueda;
	}


	public void setFiltroBusqueda(SolicitudAutorizacionFiltro filtroBusqueda) {
		this.filtroBusqueda = filtroBusqueda;
	}


	public List<SolicitudAutorizacionDTO> getResultados() {
		return resultados;
	}


	public void setResultados(List<SolicitudAutorizacionDTO> resultados) {
		this.resultados = resultados;
	}


	public Long getIdSolicitudAutorizacion() {
		return idSolicitudAutorizacion;
	}


	public void setIdSolicitudAutorizacion(Long idSolicitudAutorizacion) {
		this.idSolicitudAutorizacion = idSolicitudAutorizacion;
	}


	public CatSolicitudAutorizacionService getCatSolicitudAutorizacionService() {
		return catSolicitudAutorizacionService;
	}


	@Autowired
	@Qualifier("catSolicitudAutorizacionServiceEJB")
	public void setCatSolicitudAutorizacionService(
			CatSolicitudAutorizacionService catSolicitudAutorizacionService) {
		this.catSolicitudAutorizacionService = catSolicitudAutorizacionService;
	}


	public int getEstatus() {
		return estatus;
	}


	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}


	public String getTipoSolicitud() {
		return tipoSolicitud;
	}


	public void setTipoSolicitud(String tipoSolicitud) {
		this.tipoSolicitud = tipoSolicitud;
	}


	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}


	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}


	public Map<String, String> getTipoSolicitudMap() {
		return tipoSolicitudMap;
	}


	public void setTipoSolicitudMap(Map<String, String> tipoSolicitudMap) {
		this.tipoSolicitudMap = tipoSolicitudMap;
	}


	public String getComentarios() {
		return comentarios;
	}


	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}


	public String getNombreAccion() {
		return nombreAccion;
	}


	public void setNombreAccion(String nombreAccion) {
		this.nombreAccion = nombreAccion;
	}


	public Long getIdSolicitudPorTipoAutorizacion() {
		return idSolicitudPorTipoAutorizacion;
	}


	public void setIdSolicitudPorTipoAutorizacion(
			Long idSolicitudPorTipoAutorizacion) {
		this.idSolicitudPorTipoAutorizacion = idSolicitudPorTipoAutorizacion;
	}

    


}
