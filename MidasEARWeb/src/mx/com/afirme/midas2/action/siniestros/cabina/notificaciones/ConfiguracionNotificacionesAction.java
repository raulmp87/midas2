package mx.com.afirme.midas2.action.siniestros.cabina.notificaciones;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.MovimientoProcesoCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/notificaciones")
public class ConfiguracionNotificacionesAction extends BaseAction implements Preparable {
	
	private static final String	LOCATION_CONTENEDORADJUNTOS_JSP	= "/jsp/siniestros/cabina/notificaciones/contenedorAdjuntos.jsp";
	private static final String	LOCATION_ARCHIVOSADJUNTOSGRID_JSP	= "/jsp/siniestros/cabina/notificaciones/archivosAdjuntosGrid.jsp";
	private static final String	LOCATION_NOTIFICACIONESGRID_JSP	= "/jsp/siniestros/cabina/notificaciones/notificacionesGrid.jsp";
	/**
	 * 
	 */
	private static final long serialVersionUID = 4793700710460067368L;
	public static final Logger log = Logger.getLogger(ConfiguracionNotificacionesAction.class);
	
	private ConfiguracionNotificacionCabina configuracionNotificacion = new ConfiguracionNotificacionCabina();
	private ConfiguracionNotificacionDTO configuracionNotificacionDTO = new ConfiguracionNotificacionDTO();
	private List<ConfiguracionNotificacionCabina> configuraciones;
	private DestinatarioConfigNotificacionCabina destinatarioNotificacion = new DestinatarioConfigNotificacionCabina();
	private Map<String, String> modosEnvio;
	private Map<String, String> tiposCorreoEnvio;
	private Map<String, String> tiposDestinatario;
	private Map<Long, String> procesos;
	private Map<Long, String> oficinas;
	private List<MovimientoProcesoCabina> movimientosCabina = new ArrayList<MovimientoProcesoCabina>(1);
	private List<Map<String, Object>> destinatariosList;
	private List<AdjuntoConfigNotificacionCabinaDTO> archivosAdjuntos;
	private String nombreArchivo;
	private AdjuntoConfigNotificacionCabina archivoAdjunto = new AdjuntoConfigNotificacionCabina();
	private File adjunto;
	private String adjuntoContentType;
	private String adjuntoFileName;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("configuracionNotificacionServiceEJB")
	private ConfiguracionNotificacionesService configuracionNotifiService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Override
	public void prepare(){
		modosEnvio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MODO_ENVIO);
		tiposDestinatario = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_DESTINATARIO, true);
		tiposCorreoEnvio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_CORREO);
		procesos = configuracionNotifiService.obtenerProcesos();
		oficinas = listadoService.obtenerOficinasSiniestros();
	}
	
	@Action(value = "asignarEstatusConfiguracion", results = { 
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp") })
	public String asignarEstatusConfiguracion(){
		//updated
		configuracionNotifiService.actualizarEstatusConfiguracion(configuracionNotificacion);
		return SUCCESS;
	}
	
	@Action(value="eliminarConfiguracion", results={
			@Result(name=SUCCESS,location=LOCATION_NOTIFICACIONESGRID_JSP)
	})
	public String eliminarConfiguracion(){
		configuracionNotificacion = configuracionNotifiService.obtenerConfiguracion(configuracionNotificacion.getId());
		configuracionNotifiService.eliminarConfiguracion(configuracionNotificacion);
		configuraciones = configuracionNotifiService.obtenerConfiguraciones();
		return SUCCESS;
	}
	
	@Action (value = "obtenerConfiguraciones", results = { 
			@Result(name = SUCCESS, location = LOCATION_NOTIFICACIONESGRID_JSP),
			@Result(name = INPUT, location = LOCATION_NOTIFICACIONESGRID_JSP)})
	public String obtenerConfiguraciones(){
		configuraciones = configuracionNotifiService.obtenerConfiguraciones();
		return SUCCESS;
	}
	
	@Action(value="busquedaGeneral", results={
			@Result(name = SUCCESS, location = LOCATION_NOTIFICACIONESGRID_JSP),
			@Result(name = INPUT, location = LOCATION_NOTIFICACIONESGRID_JSP)})
	public String busquedaGeneral(){
		configuraciones = configuracionNotifiService.obtenerConfiguracionesPorFiltros(configuracionNotificacionDTO);
		return SUCCESS;
	}
	
	@Action(value="cargarConfiguracion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionNotificacion.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionNotificacion.*"})
		})
	public String cargarConfiguracion() {
		try {			
			configuracionNotificacion = configuracionNotifiService.obtenerConfiguracion(configuracionNotificacion.getId());
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="cargaDestinatarios", results={
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/notificaciones/destinatariosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/notificaciones/destinatariosGrid.jsp")})
	public String cargaDestinatarios(){
		Map<String, Object> map;
		destinatariosList = new ArrayList<Map<String, Object>>();
		configuracionNotificacion = configuracionNotifiService.obtenerConfiguracion(configuracionNotificacion.getId());
		for(DestinatarioConfigNotificacionCabina des : configuracionNotificacion.getDestinatariosNotificacion()){
			map = new LinkedHashMap<String, Object>();
			map.put("id", des.getId());
			map.put("correo", des.getCorreo());
			map.put("nombre", des.getNombre());
			map.put("puesto", des.getPuesto());
			map.put("modoEnvio", des.getModoEnvio());
			map.put("modoEnvioDesc", modosEnvio.get(String.valueOf(des.getModoEnvio())));
			map.put("tipoDestinatario", des.getTipoDestinatario());
			map.put("tipoDestinatarioDesc", tiposDestinatario.get(String.valueOf(des.getTipoDestinatario())));
			map.put("correoEnvio", des.getCorreosEnvio());
			map.put("correoEnvioDesc", tiposCorreoEnvio.get(String.valueOf(des.getCorreosEnvio())));
			destinatariosList.add(map);
		}
		return SUCCESS;
	}
	
	
	@Action(value="guardarActualizarConfiguracion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje,configuracionNotificacion.*"})
		})
	public String guardarActualizarConfiguracion(){
		try {
			for(String oficinaId : configuracionNotificacionDTO.getOficinasId()){
				configuracionNotificacion.setOficina(entidadService.findById(Oficina.class, Long.valueOf(oficinaId)));
				configuracionNotificacion.setNotas(StringUtil.decodeUri(configuracionNotificacion.getNotas()));
				for(DestinatarioConfigNotificacionCabina destinatario : configuracionNotificacion.getDestinatariosNotificacion()){
					if(destinatario != null){
						destinatario.setPuesto(StringUtil.decodeUri(destinatario.getPuesto()));
						destinatario.setNombre(StringUtil.decodeUri(destinatario.getNombre()));
						destinatario.setCorreo(StringUtil.decodeUri(destinatario.getCorreo()));
					}
				}
				configuracionNotifiService.guardarActualizarConfiguracion(configuracionNotificacion);
			}
			super.setMensajeExito();
		} catch (Exception e) {
			log.error("Ocurrio un error al editar la configuracion ", e);
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/notificaciones/configuracionNotificaciones.jsp"))
	public String mostrarContenedor(){
		return SUCCESS;
	}

	public Map<String, String> getModosEnvio() {
		return modosEnvio;
	}

	public void setModosEnvio(Map<String, String> modosEnvio) {
		this.modosEnvio = modosEnvio;
	}

	public Map<String, String> getTiposCorreoEnvio() {
		return tiposCorreoEnvio;
	}

	public void setTiposCorreoEnvio(Map<String, String> tiposCorreoEnvio) {
		this.tiposCorreoEnvio = tiposCorreoEnvio;
	}

	public Map<String, String> getTiposDestinatario() {
		return tiposDestinatario;
	}

	public void setTiposDestinatario(Map<String, String> tiposDestinatario) {
		this.tiposDestinatario = tiposDestinatario;
	}

	public Map<Long, String> getProcesos() {
		return procesos;
	}

	public void setProcesos(Map<Long, String> procesos) {
		this.procesos = procesos;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public List<MovimientoProcesoCabina> getMovimientosCabina() {
		return movimientosCabina;
	}

	public void setMovimientosCabina(List<MovimientoProcesoCabina> movimientosCabina) {
		this.movimientosCabina = movimientosCabina;
	}

	public List<ConfiguracionNotificacionCabina> getConfiguraciones() {
		return configuraciones;
	}

	public void setConfiguraciones(
			List<ConfiguracionNotificacionCabina> configuraciones) {
		this.configuraciones = configuraciones;
	}

	public DestinatarioConfigNotificacionCabina getDestinatarioNotificacion() {
		return destinatarioNotificacion;
	}

	public void setDestinatarioNotificacion(
			DestinatarioConfigNotificacionCabina destinatarioNotificacion) {
		this.destinatarioNotificacion = destinatarioNotificacion;
	}

	public ConfiguracionNotificacionDTO getConfiguracionNotificacionDTO() {
		return configuracionNotificacionDTO;
	}

	public void setConfiguracionNotificacionDTO(
			ConfiguracionNotificacionDTO configuracionNotificacionDTO) {
		this.configuracionNotificacionDTO = configuracionNotificacionDTO;
	}

	public ConfiguracionNotificacionCabina getConfiguracionNotificacion() {
		return configuracionNotificacion;
	}

	public void setConfiguracionNotificacion(
			ConfiguracionNotificacionCabina configuracionNotificacion) {
		this.configuracionNotificacion = configuracionNotificacion;
	}

	public List<Map<String, Object>> getDestinatariosList() {
		return destinatariosList;
	}

	public void setDestinatariosList(List<Map<String, Object>> destinatariosList) {
		this.destinatariosList = destinatariosList;
	}
	
	@Action(value = "mostrarAdjuntos", 
			results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORADJUNTOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORADJUNTOS_JSP),
			@Result(name = ERROR, location = "/jsp/siniestros/cabina/notificaciones/errorFX.jsp")
			})
	public String mostrarAdjuntos(){
		return SUCCESS;
	}
	
	@Action (value = "obtenerAdjuntos", results = { 
			@Result(name = SUCCESS, location = LOCATION_ARCHIVOSADJUNTOSGRID_JSP),
			@Result(name = INPUT, location = LOCATION_ARCHIVOSADJUNTOSGRID_JSP)})
	public String obtenerAdjuntos(){
		configuracionNotificacion = configuracionNotifiService.obtenerConfiguracion(configuracionNotificacion.getId());
		archivosAdjuntos = configuracionNotifiService.obtenerAdjuntosSimpleList(configuracionNotificacion);
		
		return SUCCESS;
	}
	
	@Action(value = "agregarAdjunto", 
			results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORADJUNTOS_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORADJUNTOS_JSP),
			@Result(name = ERROR, location = "/jsp/siniestros/cabina/notificaciones/errorFX.jsp")
	}
	)
	public String agregarAdjunto(){
		
		if (this.adjunto!=null){
		AdjuntoConfigNotificacionCabina adjuntoSubir = new AdjuntoConfigNotificacionCabina();
		
				 byte[] b = new byte[(int) this.adjunto.length()];
				 FileInputStream fileInputStream = null;
		         try {
		        	 fileInputStream = new FileInputStream(this.adjunto);
		               fileInputStream.read(b);
		               adjuntoSubir.setArchivo(b);
		               adjuntoSubir.setDescripcion(nombreArchivo);
		               adjuntoSubir.setTipoArchivo(adjuntoContentType);
		          } catch (FileNotFoundException e) {
		        	  log.error("Adjuntar archivo: No se encuentra el archivo",e);
		          }
		          catch (IOException e1) {
		                  log.error("Adjuntar archivo: No se pudo leer el archivo",e1);
		          } finally {
		        	 IOUtils.closeQuietly(fileInputStream);
		         }

			adjuntoSubir.setConfNotificacionCabina(configuracionNotificacion);
			configuracionNotifiService.agregarAdjunto(configuracionNotificacion, adjuntoSubir);
		}
		return SUCCESS;
	}
	
	@Action(value="eliminarAdjunto", results={
			@Result(name=SUCCESS,location=LOCATION_ARCHIVOSADJUNTOSGRID_JSP),
			@Result(name = INPUT, location = LOCATION_ARCHIVOSADJUNTOSGRID_JSP)
	})
	public String eliminarAdjunto(){
		archivoAdjunto = configuracionNotifiService.obtenerAdjunto(archivoAdjunto.getId());
		configuracionNotificacion = configuracionNotifiService.obtenerConfiguracion(configuracionNotificacion.getId());
		configuracionNotifiService.borrarAdjunto(configuracionNotificacion, archivoAdjunto);
		archivosAdjuntos = configuracionNotifiService.obtenerAdjuntosSimpleList(configuracionNotificacion);
//		configuracionNotificacion.setAdjuntosNotificacion(archivosAdjuntos);
		return SUCCESS;
	}

	public List<AdjuntoConfigNotificacionCabinaDTO> getArchivosAdjuntos() {
		return archivosAdjuntos;
	}

	public void setArchivosAdjuntos(
			List<AdjuntoConfigNotificacionCabinaDTO> archivosAdjuntos) {
		this.archivosAdjuntos = archivosAdjuntos;
	}

	public File getAdjunto() {
		return adjunto;
	}

	public void setAdjunto(File adjunto) {
		this.adjunto = adjunto;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public AdjuntoConfigNotificacionCabina getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(AdjuntoConfigNotificacionCabina archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	public String getAdjuntoContentType() {
		return adjuntoContentType;
	}

	public void setAdjuntoContentType(String adjuntoContentType) {
		this.adjuntoContentType = adjuntoContentType;
	}

	public String getAdjuntoFileName() {
		return adjuntoFileName;
	}

	public void setAdjuntoFileName(String adjuntoFileName) {
		this.adjuntoFileName = adjuntoFileName;
	}
	
	

}
