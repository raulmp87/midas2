package mx.com.afirme.midas2.action.siniestros.liquidacion;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.InfoContratoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.CentroOperacionLiquidacionView;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro.EstatusLiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro.OrigenLiquidacion;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro.TipoOperacion;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionSiniestroRegistro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService.LiquidacionSiniestroFiltro;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/liquidacion/liquidacionIndemnizacion")

public class LiquidacionIndemnizacionAction  extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -523592611130617692L;

	private String origenBusquedaLiquidaciones; // BIB (BUSQUEDA INDEMNIZACION BENEFICIARIO) - BIA (BUSQUEDA INDEMNIZACION AUTORIZACION )
	
	private LiquidacionSiniestro liquidacionSiniestro;
	private Boolean soloLectura;
	private OrdenPagoSiniestro ordenPago;
	List<OrdenPagoSiniestro> listaOrdenesPago;
	List<OrdenPagoSiniestro> listaOrdenesPagoAsociadas;
	private List<FacturaLiquidacionDTO> listaOrdenesPagoSiniestro;
	private String estatusLiquidacion;
	private List<CentroOperacionLiquidacionView> listaCentrosOperacion;
	private Map<String,String> listaTiposLiquidacion;
	private List<Map<String, Object>> listaBeneficiarios;
	private BigDecimal primaPendientePagoConsulta;	
	private BigDecimal primaNoDevengadaConsulta;
	private String nombreBeneficiario;
	private Map<Long, String> listaBancos;
	
	private Map<String,String> ctgLiquidaciones;
	private Map<String,String> ctgTipoOperaciones;
	private Map<String,String> ctgEstatusLiquidaciones;
	private Map<String,String> ctgCriterioComparacion;	
	private Map<String,String> estatus;
	private Map<Long  ,String> oficinas;
	private List<LiquidacionSiniestroRegistro> listadoLiquidaciones;
	private LiquidacionSiniestroFiltro filtroLiquidaciones;
	private TransporteImpresionDTO transporte;
	private Short tipoCancelacion;
	private Short envioCancelacion;
	private Long idLiqSiniestro;
	
	private static final String CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO="/jsp/siniestros/liquidacion/contenedorLiquidacionInd.jsp";
	private static final String CONTENEDOR_BUSQUEDA_BENEFICIARIO_GRID="/jsp/siniestros/liquidacion/contenedorLiquidacionIndGrid.jsp";
	private static final String CONTENEDOR_CANCELACION_LIQUIDACION="/jsp/siniestros/liquidacion/contenedorCancelarLiquidacion.jsp";
	
	//IMPRESION DE LIQUIDACION
	private Boolean esPreview;
	private String contentDispositionConfig;
	private Boolean esImprimible;
	private String claveNegocio;
	private String pantallaOrigen;
	private Boolean usuarioAutorizador;
	private static enum CONTENT_DISPOSITION_CONFIGURATION{
		attachment, inline;
	}
	
	//INFORMACION CONTRATO
	private InfoContratoSiniestro infoContrato;
	private Boolean tieneIndemnizaciones;
	private static final int MIDAS_APP_ID = 5;
	private static final String PARAMETRO_GLOBAL_APODERADO = "APODERADO_LEGAL_FINIQUITO";
	private static final String	TIPO_PDF	= "application/pdf";
	private static final String	EXT_PDF	= ".pdf";
	
	
	@Autowired
	@Qualifier("liquidacionSiniestroServiceEJB")
	private LiquidacionSiniestroService liquidacionSiniestroService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("catalogoGrupoValorServiceEJB")
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@Autowired
	@Qualifier("pagosSiniestroServiceEJB")
	private PagosSiniestroService pagosSiniestroService;
	
	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	private ParametroGlobalService parametroGlobalService ;
	
	@Autowired
	@Qualifier("cartaSiniestroServiceEJB")
	private CartaSiniestroService cartaSiniestroService;
	
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void prepareMostrarLiquidacion()
	{	
		listaTiposLiquidacion = new LinkedHashMap<String, String>();  

		//Boolean tieneNumeroCuentaBancaria = Boolean.FALSE;
		
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{  
			liquidacionSiniestro = entidadService.evictAndFindById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());//TODO remover despues de las pruebas
			nombreBeneficiario = liquidacionSiniestro.getBeneficiario();
			liquidacionSiniestro.getOrdenesPago();
			
			listaTiposLiquidacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
			
			if(liquidacionSiniestro.getClabeBeneficiario() == null)
			{
				listaTiposLiquidacion.remove(LiquidacionSiniestro.TipoLiquidacion.TRANSFERENCIA_BANCARIA.getValue());
			}
		}
		else
		{   liquidacionSiniestro = new LiquidacionSiniestro();			
			liquidacionSiniestro.setOrigenLiquidacion(OrigenLiquidacion.BENEFICIARIO.getValue());
			liquidacionSiniestro.setTipoOperacion(TipoOperacion.NO_APLICA.getValue());//Para Beneficiarios esta opcion es por default
			liquidacionSiniestro.setTipoLiquidacion(LiquidacionSiniestro.TipoLiquidacion.CHEQUE.getValue());
			liquidacionSiniestro = liquidacionSiniestroService.guardarLiquidacion(liquidacionSiniestro, 
						LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE);
		}
		
		liquidacionSiniestro.setDescripcionEstatus(catalogoGrupoValorService.
				obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_LIQUIDACION_SINIESTRO,
						liquidacionSiniestro.getEstatus()).getDescripcion());    
		
		listaBeneficiarios = pagosSiniestroService.buscarBeneficiariosLiquidacion();
		
		ctgTipoOperaciones = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_OPERACION);		
		
		listaCentrosOperacion = entidadService.findAll(CentroOperacionLiquidacionView.class);
		
		listaBancos = listadoService.getMapBancosMidas();
	}
	
	/**
	 * Muestra la pantalla principal de captura de la orden de pago en modos
	 * captura/edicion/consulta
	 */
	@Action(value="mostrarLiquidacion",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/contenedorLiquidacionDetalleInd.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/contenedorLiquidacionDetalleInd.jsp")
			})
	public String mostrarLiquidacion(){
	
		try{		
			
			if(liquidacionSiniestro != null 
					&& !liquidacionSiniestro.getEstatus().equalsIgnoreCase(EstatusLiquidacionSiniestro.APLICADA.getValue()))
			{   
				primaPendientePagoConsulta = liquidacionSiniestroService.obtenerPrimaPendientePago(liquidacionSiniestro.getId());
				primaNoDevengadaConsulta = liquidacionSiniestroService.obtenerPrimaNoDevengada(liquidacionSiniestro.getId());
			}			
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return INPUT;			
		}
		
		if(!liquidacionSiniestro.getEstatus().
				equalsIgnoreCase(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE.getValue())
				&& !liquidacionSiniestro.getEstatus().
				equalsIgnoreCase(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE_POR_RECHAZO.getValue())
				&& !liquidacionSiniestro.getEstatus().
				equalsIgnoreCase(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE_POR_MIZAR.getValue()))
		{
			soloLectura = Boolean.TRUE;				
		}
		else if(soloLectura == null)//Si no viene desde JSP entonces el default es mostrar en solo lectura
		{
			soloLectura = Boolean.FALSE;			
		}		

		return SUCCESS;
	}
	
	@Action(value="asociarOrdenPago",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento,^liquidacionSiniestro\\.clabeBeneficiario," +
							"^liquidacionSiniestro\\.bancoBeneficiario,^liquidacionSiniestro\\.rfcBeneficiario,^liquidacionSiniestro\\.telefonoNumero," +
							"^liquidacionSiniestro\\.telefonoLada,^liquidacionSiniestro\\.correo,primaPendientePagoConsulta,primaNoDevengadaConsulta, " +
							"^liquidacionSiniestro\\.total", 
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String asociarOrdenPago(){

		try{		
			liquidacionSiniestro = liquidacionSiniestroService.procesarOrdenPagoSeleccionada(ordenPago.getId(), liquidacionSiniestro.getId(),
				LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO);
			
			primaPendientePagoConsulta = liquidacionSiniestroService.obtenerPrimaPendientePago(liquidacionSiniestro.getId());
			primaNoDevengadaConsulta = liquidacionSiniestroService.obtenerPrimaNoDevengada(liquidacionSiniestro.getId());
		
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}									
				
		return SUCCESS;
	}
	
	@Action(value="desasociarOrdenPago",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","^liquidacionSiniestro\\.subtotal,^liquidacionSiniestro\\.deducible,^liquidacionSiniestro\\.descuento," +
							"^liquidacionSiniestro\\.iva,^liquidacionSiniestro\\.ivaRet,^liquidacionSiniestro\\.isr,tipoMensaje,mensaje," +
							"^liquidacionSiniestro\\.netoPorPagar,^liquidacionSiniestro\\.importeSalvamento,^liquidacionSiniestro\\.clabeBeneficiario," +
							"^liquidacionSiniestro\\.bancoBeneficiario,^liquidacionSiniestro\\.rfcBeneficiario,^liquidacionSiniestro\\.telefonoNumero," +
							"^liquidacionSiniestro\\.telefonoLada,^liquidacionSiniestro\\.correo,primaPendientePagoConsulta,primaNoDevengadaConsulta, " +
							"^liquidacionSiniestro\\.total", 
					"excludeProperties","^liquidacionSiniestro\\.proveedor\\.*"})				
		})
	public String desasociarOrdenPago(){
		
		try{
			liquidacionSiniestro = liquidacionSiniestroService.procesarOrdenPagoSeleccionada(ordenPago.getId(), liquidacionSiniestro.getId(),
					LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO);
		
			primaPendientePagoConsulta = liquidacionSiniestroService.obtenerPrimaPendientePago(liquidacionSiniestro.getId());
			primaNoDevengadaConsulta = liquidacionSiniestroService.obtenerPrimaNoDevengada(liquidacionSiniestro.getId());
			
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return SUCCESS;			
		}		
		
		return SUCCESS;
	}
	
	@Action(value="reexpedir",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaLiquidacionIndemBeneficiario", 
			"namespace", "/siniestros/liquidacion/liquidacionIndemnizacion", 
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO)
			})			
	public String reexpedir(){	
		
		try{ 
			this.liquidacionSiniestroService.reexpedir(liquidacionSiniestro.getId());
			this.setMensajeExito();
			
		}catch(Exception e)
		{
			this.setMensajeError(e.getMessage());
			return INPUT;			
		}			

		return SUCCESS;
	}
	
	@Action(value="mostrarListadoOrdenPago",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/ordenPagoDispLiquidacionIndGrid.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/ordenPagoDispLiquidacionIndGrid.jsp")
			})
	public String mostrarListadoOrdenPago(){		
		
		listaOrdenesPago = pagosSiniestroService.buscarOrdenesPagoIndemnizacion(StringUtil.decodificadorUri(liquidacionSiniestro.getBeneficiario()));		
	 
	    return SUCCESS;
	}
	
	@Action(value="mostrarOrdenesPagoAsociadas",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/ordenPagoRelLiquidacionIndGrid.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/ordenPagoRelLiquidacionIndGrid.jsp")
			})
	public String mostrarOrdenesPagoAsociadas(){		
		
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{			
			listaOrdenesPagoAsociadas = 
				liquidacionSiniestroService.buscarOrdenesPagoIndemnizacion(liquidacionSiniestro.getId());
		}	 	
		
		return SUCCESS;
	}
	
	@Action(value="verDetalleFactura",results={
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/listadoOrdenesPagoLiquidarDetalle.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/listadoOrdenesPagoLiquidarDetalle.jsp")
			})
	public String verDetalleFactura()
	{  //Revisar si va a aplicar
		//listaOrdenesPagoSiniestro = pagosSiniestroService.buscarOrdenesPagoFactura(factura.getId());		
		
		return SUCCESS;
	}
	
	public void prepareGuardarLiquidacionAutomatico()
	{
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{  
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
		}		
	}	
	
	@Action(value="guardarLiquidacionAutomatico",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})				
		})
	public String guardarLiquidacionAutomatico(){
		
		try{
		liquidacionSiniestro = liquidacionSiniestroService.guardarLiquidacion(liquidacionSiniestro);
		}
		catch(Exception e)
		{
			this.setMensajeError("Error al intentar guardar la liquidacion");
			e.printStackTrace();
		}
		
		this.setMensajeExito();
		
		return SUCCESS;		
	}	
	
	public void prepareGuardarLiquidacion()
	{
		if(liquidacionSiniestro != null && liquidacionSiniestro.getId() != null)
		{  
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
		}		
	}	
	
	@Action(value="guardarLiquidacion",results={ 
			@Result(name=SUCCESS,location="/jsp/siniestros/liquidacion/contenedorLiquidacionDetalleInd.jsp"),
			@Result(name=INPUT,location="/jsp/siniestros/liquidacion/contenedorLiquidacionDetalleInd.jsp")
			})
	public String guardarLiquidacion(){		
		
		liquidacionSiniestroService.guardarLiquidacion(liquidacionSiniestro, 
				LiquidacionSiniestro.EstatusLiquidacionSiniestro.get(estatusLiquidacion));

		return SUCCESS;
	}
	
	private void cargaCatalogos(){
		ctgLiquidaciones        = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO); // Cheque y Transferencia Bancaria
		ctgTipoOperaciones      = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_OPERACION); // no aplica,otros,servicios profesionales
		ctgEstatusLiquidaciones = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_LIQUIDACION_SINIESTRO); //Aplicada, Liberada con Solicitud,Liberada sin Solicitud
		ctgCriterioComparacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CRITERIO_COMPARACION); // mayor,menor,igual
		oficinas = listadoService.obtenerOficinasSiniestros();
	}
	
	
	@Action(value="mostrarBusquedaLiquidacionIndemBeneficiario",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO)
			})
	public String mostrarBusquedaLiquidacionIndemBeneficiario(){	
		this.cargaCatalogos();
		return SUCCESS;	
	}
	
	/**
	 * Método que realiza la búsqeda por medio de un filtro
	 */
	@Action(value="buscarLiquidacionBeneficiario",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_BENEFICIARIO_GRID),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_BENEFICIARIO_GRID)
			})
	public String buscarLiquidacionBeneficiario(){	
		
		this.cargaCatalogos();
		
		if( this.filtroLiquidaciones != null ){
			this.filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue());
			listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);
		}
		
		if( listadoLiquidaciones.isEmpty() ){
			super.setMensajeError(getText("midas.liquidaciones.busqueda.busqueda.error"));
			return INPUT;
		}
		
		return SUCCESS;	
	}
	
	@Action(value="mostrarBusquedaAutorizacionLiquidacionBeneficiario",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO)
			})
	public String mostrarBusquedaAutorizacionLiquidacionBeneficiario(){	
		this.cargaCatalogos();
		return SUCCESS;	
	}
	
	/**
	 * Método que realiza la búsqeda por medio de un filtro
	 */
	@Action(value="buscarLiquidacionAutorizacionBeneficiario",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_BENEFICIARIO_GRID),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_BENEFICIARIO_GRID)
			})
	public String buscarLiquidacionAutorizacionBeneficiario(){	
		
		//CARGA DE CATALOGOS
		this.cargaCatalogos();
		
		if( this.filtroLiquidaciones == null ){
			listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(ctgEstatusLiquidaciones,LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue());
		}else{
			this.filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue());
			listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);
		}
		
		return SUCCESS;	
	}
	
	
	@Action(value="exportarBusquedaBeneficiarioExcel",
			results={@Result(name=SUCCESS,
					  type="stream",
					  params={"contentType","${transporte.contentType}",
						  "inputName","transporte.genericInputStream",
						  "contentDisposition","attachment;filename=\"${transporte.fileName}\""})}) 
	public String exportarBusquedaBeneficiarioExcel(){	
		
		this.filtroLiquidaciones.setOrigenLiquidacion(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue());
		listadoLiquidaciones = this.liquidacionSiniestroService.buscarLiquidacionDetalle(this.filtroLiquidaciones);
		if( listadoLiquidaciones != null ){
			//Creamos un nuevo objeto ExcelExporter con el tipo de class que se desea exportar
			ExcelExporter exporter = new ExcelExporter( LiquidacionSiniestroRegistro.class,"BENEFICIARIO" );
			transporte = exporter.exportXLS(listadoLiquidaciones, "Liquidaciones Beneficiario");
		}
		return SUCCESS;	
	}
	
	/**
	 * cancela una orden depago, se cambia el estatus a cancelado.
	 */
	@Action(value="eliminarLiquidacion",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaLiquidacionIndemBeneficiario", 
			"namespace", "/siniestros/liquidacion/liquidacionIndemnizacion", 
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_LIQUIDACION_INDEM_BENEFICIARIO)
			})
	public String eliminarLiquidacion(){
		 
		try {
			this.liquidacionSiniestroService.eliminarLiquidacion(liquidacionSiniestro.getId());
		} catch (Exception e) {
			this.setMensajeError("Ha ocurrido un error al eliminar la liquidación.");
			e.printStackTrace();
		}
		
		this.setMensajeExito();
		
		return SUCCESS;
	}
	
	@Action(value="solicitarAutorizacion",results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarBusquedaLiquidacionIndemBeneficiario", 
			"namespace", "/siniestros/liquidacion/liquidacionIndemnizacion", 
			"origenBusquedaLiquidaciones", "BIB",
			"mensaje", "${mensaje}",		
			"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "mostrarLiquidacion", 
					"namespace", "/siniestros/liquidacion/liquidacionIndemnizacion",
					"liquidacionSiniestro.id","${liquidacionSiniestro.id}",
					"soloLectura","${soloLectura}",
					"origenBusquedaLiquidaciones", "BIB",
					"pantallaOrigen","${pantallaOrigen}",
					"mensaje", "${mensaje}",	 	
					"tipoMensaje", "${tipoMensaje}"})
			})  
	public String solicitarAutorizacion(){	
		
		try{
		liquidacionSiniestroService.solicitarAutorizacion(liquidacionSiniestro.getId());
		} catch (Exception e) {
			this.setMensajeError(e.getMessage());
			e.printStackTrace();
			return INPUT;
		}
		
		this.setMensajeExito();
			
		return SUCCESS;	
	}
	
	@Action(value = "imprimirOrdenExpedicionChequeBeneficiario", results = {
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","${contentDispositionConfig};filename=\"${transporte.fileName}\""})})
	public String imprimirOrdenExpedicionChequeBeneficiario(){
		transporte = liquidacionSiniestroService.imprimirOrdenExpedicionCheque(liquidacionSiniestro.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType("application/pdf");
		if(esPreview != null 
				&& esPreview == true){ //Tiene que estar en TRUE para que despliegue el PDF en pantalla en vez de intentar descargarlo
			contentDispositionConfig = CONTENT_DISPOSITION_CONFIGURATION.inline.toString();
		}else{
			contentDispositionConfig = CONTENT_DISPOSITION_CONFIGURATION.attachment.toString();
		}
		
		String fileName = "OrdenExpedicionCheque_"+ "id" + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	@Action(value = "mostrarImprimirLiquidacionBeneficiario", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/liquidacion/impresionLiquidacionInd.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/liquidacion/impresionLiquidacionInd.jsp")})
	public String mostrarImprimirLiquidacionBeneficiario(){
		if(liquidacionSiniestro != null
				&& liquidacionSiniestro.getId() != null){
			esImprimible = liquidacionSiniestroService.validarImpresionLiquidacion(liquidacionSiniestro.getId());
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			tieneIndemnizaciones = liquidacionSiniestroService.tieneIndemnizaciones(liquidacionSiniestro.getId());
			if(liquidacionSiniestro.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue()) == 0){
				usuarioAutorizador = liquidacionSiniestroService.validarUsuarioEsAutorizador(liquidacionSiniestro.getId());
			}else{
				usuarioAutorizador = Boolean.FALSE;
			}
		}else{
			setMensajeError("Debes proporcionar un n\u00FAmero de liquidaci\u00F3n v\u00E1lido para poder imprimir");
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	
	/*AIGG */

	@Action(value = "autorizarLiquidacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/liquidacion/impresionLiquidacionInd.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/liquidacion/impresionLiquidacionInd.jsp")})
	public String autorizarLiquidacion(){
		
			try {
				liquidacionSiniestroService.enviarSolicitudLiquidacion(liquidacionSiniestro.getId());
			} catch (Exception e) {
				
				super.setMensajeError(e.getMessage());
				return INPUT;
			}
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			if(liquidacionSiniestro.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue()) == 0){
				usuarioAutorizador = liquidacionSiniestroService.validarUsuarioEsAutorizador(liquidacionSiniestro.getId());
			}else{
				usuarioAutorizador = Boolean.FALSE;
			}
			super.setMensajeExitoPersonalizado(getText("Liquidacion Autorizada"));
			return SUCCESS;
	}
	

			
	@Action(value = "rechazarLiquidacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/liquidacion/impresionLiquidacionInd.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/liquidacion/impresionLiquidacionInd.jsp")})		
	public String rechazarLiquidacion(){
			//this.idLiqSiniestro=liquidacionSiniestro.getId();
			this.liquidacionSiniestro = this.entidadService.findById(LiquidacionSiniestro.class ,liquidacionSiniestro.getId());
		
			if (null==liquidacionSiniestro){
				super.setMensajeError(getText("midas.liquidaciones.busqueda.noLiquidacionNoExiste"));
				return INPUT;
			}
			this.liquidacionSiniestroService.rechazarLiquidacion(liquidacionSiniestro.getId())  ;
			liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, liquidacionSiniestro.getId());
			if(liquidacionSiniestro.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue()) == 0){
				usuarioAutorizador = liquidacionSiniestroService.validarUsuarioEsAutorizador(liquidacionSiniestro.getId());
			}else{
				usuarioAutorizador = Boolean.FALSE;
			}
			super.setMensajeExitoPersonalizado(getText("midas.liquidaciones.liquidacionRechazada"));
			return SUCCESS;
	}	
	
	@Action(value = "mostrarInfoContrato", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp")})
	public String mostrarInfoContrato(){
		try{
			infoContrato = cartaSiniestroService.buscarInfoContratos(liquidacionSiniestro.getId());
			if(infoContrato == null){infoContrato = new InfoContratoSiniestro();}
			infoContrato.setApoderadoAfirme(parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID, PARAMETRO_GLOBAL_APODERADO));
		}catch(Exception ex){
			setMensajeError("No se pudo recuperar la informaci\u00F3N del contrato");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarInfoContrato", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorInfoContrato.jsp")})
	public String guardarInfoContrato(){
		try{
			infoContrato = cartaSiniestroService.guardarInfoContratos(infoContrato, liquidacionSiniestro.getId());
		}catch(Exception ex){
			setMensajeError("No se pudo guardar la informacion del contrato");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}

	@Action(value="mostrarCancelarLiquidacion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_CANCELACION_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_CANCELACION_LIQUIDACION)
			})
	public String mostrarCancelarLiquidacion(){
		this.idLiqSiniestro = liquidacionSiniestro.getId();
		return SUCCESS;
	}
	@Action(value = "imprimirInfoContrato", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirInfoContrato(){
		try{
			infoContrato = cartaSiniestroService.guardarInfoContratos(infoContrato, liquidacionSiniestro.getId());
		}catch(Exception ex){
			setMensajeError("No se pudo guardar la informacion del contrato");
			return INPUT;
		}
		transporte = cartaSiniestroService.imprimirContratoLiquidacion(liquidacionSiniestro.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "ContratoSalvamentoLiquidacion_"+ liquidacionSiniestro.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}

	
	@Action(value = "imprimirCartaFiniquito", results = {
			@Result(name=SUCCESS,type=ACTION_PARAM_STREAM,
					params={ACTION_PARAM_CONTENT_TYPE,"${transporte.contentType}",
					ACTION_PARAM_INPUT_NAME,"transporte.genericInputStream",
					ACTION_PARAM_CONTENT_DISPOSITION,"attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirCartaFiniquito(){
		transporte = cartaSiniestroService.imprimirCartaFiniquitoLiquidacion(liquidacionSiniestro.getId());
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		
		String fileName = "CartaFiniquitoLiquidacion_"+ liquidacionSiniestro.getId() + "_" + UtileriasWeb.getFechaString(new Date()) + EXT_PDF;
		transporte.setFileName(fileName);
		return SUCCESS;
	}
	
	
	@Action(value="cancelacionLiquidacion",
			results = { @Result(name=SUCCESS,location=CONTENEDOR_CANCELACION_LIQUIDACION),
			@Result(name=INPUT,location=CONTENEDOR_CANCELACION_LIQUIDACION)
			})
	public String cancelacionLiquidacion(){
		
		String resultado = INPUT;
		
		if (liquidacionSiniestro != null && liquidacionSiniestro.getId() != null) {
			
			Short result = liquidacionSiniestroService.cancelarLiquidacion(liquidacionSiniestro.getId() , tipoCancelacion);
			switch(result) {
			case 0 :
				setMensaje("Se ha realizado la Cancelaci\u00F3n de la Liquidaci\u00F3n");
				this.envioCancelacion=1;
				resultado = SUCCESS;
				break;
			case 1 :
				setMensaje("No existe la Solicitud de cheque para esta Liquidaci\u00F3n");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			case 2 :
				setMensaje("No ha sido posible realizar la Cancelaci\u00F3n ya que existe un cheque impreso");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			case 3 :
				setMensaje("No ha sido posible realizar la Cancelaci\u00F3n ya que la Liquidaci\u00F3n esta Aplicada");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			case 4 :
				setMensaje("No ha sido posible realizar la Cancelaci\u00F3n");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
				resultado = INPUT;
				break;
			}
		}
		return resultado;
	}
	
	@Action(value="obtenerTiposLiquidacionDisponibles",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaTiposLiquidacion.*,^liquidacionSiniestro\\.tipoLiquidacion"})
	})
	public String obtenerTiposLiquidacionDisponibles()
	{		  
		listaTiposLiquidacion  = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		
		liquidacionSiniestro = this.entidadService.findById(LiquidacionSiniestro.class,liquidacionSiniestro.getId());
		
		if(liquidacionSiniestro.getClabeBeneficiario() == null)
		{
			listaTiposLiquidacion.remove(LiquidacionSiniestro.TipoLiquidacion.TRANSFERENCIA_BANCARIA.getValue());
		}
	
		return SUCCESS;
	}
	
	public Map<String, String> getCtgLiquidaciones() {
		return ctgLiquidaciones;
	}


	public void setCtgLiquidaciones(Map<String, String> ctgLiquidaciones) {
		this.ctgLiquidaciones = ctgLiquidaciones;
	}


	public Map<String, String> getCtgTipoOperaciones() {
		return ctgTipoOperaciones;
	}


	public void setCtgTipoOperaciones(Map<String, String> ctgTipoOperaciones) {
		this.ctgTipoOperaciones = ctgTipoOperaciones;
	}


	public Map<String, String> getCtgEstatusLiquidaciones() {
		return ctgEstatusLiquidaciones;
	}


	public void setCtgEstatusLiquidaciones(
			Map<String, String> ctgEstatusLiquidaciones) {
		this.ctgEstatusLiquidaciones = ctgEstatusLiquidaciones;
	}


	public Map<String, String> getCtgCriterioComparacion() {
		return ctgCriterioComparacion;
	}


	public void setCtgCriterioComparacion(Map<String, String> ctgCriterioComparacion) {
		this.ctgCriterioComparacion = ctgCriterioComparacion;
	}


	public Map<String, String> getEstatus() {
		return estatus;
	}


	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}


	public Map<Long, String> getOficinas() {
		return oficinas;
	}


	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public LiquidacionSiniestroFiltro getFiltroLiquidaciones() {
		return filtroLiquidaciones;
	}

	public void setFiltroLiquidaciones(
			LiquidacionSiniestroFiltro filtroLiquidaciones) {
		this.filtroLiquidaciones = filtroLiquidaciones;
	}

	public String getOrigenBusquedaLiquidaciones() {
		return origenBusquedaLiquidaciones;
	}

	public void setOrigenBusquedaLiquidaciones(String origenBusquedaLiquidaciones) {
		this.origenBusquedaLiquidaciones = origenBusquedaLiquidaciones;
	}

	public List<LiquidacionSiniestroRegistro> getListadoLiquidaciones() {
		return listadoLiquidaciones;
	}

	public void setListadoLiquidaciones(
			List<LiquidacionSiniestroRegistro> listadoLiquidaciones) {
		this.listadoLiquidaciones = listadoLiquidaciones;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public LiquidacionSiniestro getLiquidacionSiniestro() {
		return liquidacionSiniestro;
	}

	public void setLiquidacionSiniestro(LiquidacionSiniestro liquidacionSiniestro) {
		this.liquidacionSiniestro = liquidacionSiniestro;
	}

	public Boolean getSoloLectura() {
		return soloLectura;
	}

	public void setSoloLectura(Boolean soloLectura) {
		this.soloLectura = soloLectura;
	}

	/*public DocumentoFiscal getFactura() {
		return factura;
	}

	public void setFactura(DocumentoFiscal factura) {
		this.factura = factura;
	}*/

	public List<OrdenPagoSiniestro> getListaOrdenesPago() {
		return listaOrdenesPago;
	}

	public void setListaOrdenesPago(List<OrdenPagoSiniestro> listaOrdenesPago) {
		this.listaOrdenesPago = listaOrdenesPago;
	}

	public List<OrdenPagoSiniestro> getListaOrdenesPagoAsociadas() {
		return listaOrdenesPagoAsociadas;
	}

	public void setListaOrdenesPagoAsociadas(
			List<OrdenPagoSiniestro> listaOrdenesPagoAsociadas) {
		this.listaOrdenesPagoAsociadas = listaOrdenesPagoAsociadas;
	}

	public List<FacturaLiquidacionDTO> getListaOrdenesPagoSiniestro() {
		return listaOrdenesPagoSiniestro;
	}

	public void setListaOrdenesPagoSiniestro(
			List<FacturaLiquidacionDTO> listaOrdenesPagoSiniestro) {
		this.listaOrdenesPagoSiniestro = listaOrdenesPagoSiniestro;
	}

	public String getEstatusLiquidacion() {
		return estatusLiquidacion;
	}

	public void setEstatusLiquidacion(String estatusLiquidacion) {
		this.estatusLiquidacion = estatusLiquidacion;
	}

	public List<CentroOperacionLiquidacionView> getListaCentrosOperacion() {
		return listaCentrosOperacion;
	}

	public void setListaCentrosOperacion(
			List<CentroOperacionLiquidacionView> listaCentrosOperacion) {
		this.listaCentrosOperacion = listaCentrosOperacion;
	}

	public Map<String, String> getListaTiposLiquidacion() {
		return listaTiposLiquidacion;
	}

	public void setListaTiposLiquidacion(Map<String, String> listaTiposLiquidacion) {
		this.listaTiposLiquidacion = listaTiposLiquidacion;
	}

	public List<Map<String, Object>> getListaBeneficiarios() {
		return listaBeneficiarios;
	}

	public void setListaBeneficiarios(List<Map<String, Object>> listaBeneficiarios) {
		this.listaBeneficiarios = listaBeneficiarios;
	}

	public BigDecimal getPrimaPendientePagoConsulta() {
		return primaPendientePagoConsulta;
	}

	public void setPrimaPendientePagoConsulta(BigDecimal primaPendientePagoConsulta) {
		this.primaPendientePagoConsulta = primaPendientePagoConsulta;
	}

	public BigDecimal getPrimaNoDevengadaConsulta() {
		return primaNoDevengadaConsulta;
	}

	public void setPrimaNoDevengadaConsulta(BigDecimal primaNoDevengadaConsulta) {
		this.primaNoDevengadaConsulta = primaNoDevengadaConsulta;
	}

	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}

	public void setNombreBeneficiario(String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}

	public OrdenPagoSiniestro getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPagoSiniestro ordenPago) {
		this.ordenPago = ordenPago;
	}

	public Map<Long, String> getListaBancos() {
		return listaBancos;
	}

	public void setListaBancos(Map<Long, String> listaBancos) {
		this.listaBancos = listaBancos;
	}

	/**
	 * @return the esPreview
	 */
	public Boolean getEsPreview() {
		return esPreview;
	}

	/**
	 * @param esPreview the esPreview to set
	 */
	public void setEsPreview(Boolean esPreview) {
		this.esPreview = esPreview;
	}

	/**
	 * @return the contentDispositionConfig
	 */
	public String getContentDispositionConfig() {
		return contentDispositionConfig;
	}

	/**
	 * @param contentDispositionConfig the contentDispositionConfig to set
	 */
	public void setContentDispositionConfig(String contentDispositionConfig) {
		this.contentDispositionConfig = contentDispositionConfig;
	}

	/**
	 * @return the esImprimible
	 */
	public Boolean getEsImprimible() {
		return esImprimible;
	}

	/**
	 * @param esImprimible the esImprimible to set
	 */
	public void setEsImprimible(Boolean esImprimible) {
		this.esImprimible = esImprimible;
	}

	/**
	 * @return the pantallaOrigen
	 */
	public String getPantallaOrigen() {
		return pantallaOrigen;
	}

	/**
	 * @param pantallaOrigen the pantallaOrigen to set
	 */
	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}

	/**
	 * @return the claveNegocio
	 */
	public String getClaveNegocio() {
		return claveNegocio;
	}

	/**
	 * @param claveNegocio the claveNegocio to set
	 */
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	/**
	 * @return the usuarioAutorizador
	 */
	public Boolean getUsuarioAutorizador() {
		return usuarioAutorizador;
	}

	/**
	 * @param usuarioAutorizador the usuarioAutorizador to set
	 */
	public void setUsuarioAutorizador(Boolean usuarioAutorizador) {
		this.usuarioAutorizador = usuarioAutorizador;
	}

	/**
	 * @return the infoContrato
	 */
	public InfoContratoSiniestro getInfoContrato() {
		return infoContrato;
	}

	/**
	 * @param infoContrato the infoContrato to set
	 */
	public void setInfoContrato(InfoContratoSiniestro infoContrato) {
		this.infoContrato = infoContrato;
	}

	/**
	 * @return the tieneIndemnizaciones
	 */
	public Boolean getTieneIndemnizaciones() {
		return tieneIndemnizaciones;
	}

	/**
	 * @param tieneIndemnizaciones the tieneIndemnizaciones to set
	 */
	public void setTieneIndemnizaciones(Boolean tieneIndemnizaciones) {
		this.tieneIndemnizaciones = tieneIndemnizaciones;
	}

	public Short getTipoCancelacion() {
		return tipoCancelacion;
	}

	public void setTipoCancelacion(Short tipoCancelacion) {
		this.tipoCancelacion = tipoCancelacion;
	}

	public Short getEnvioCancelacion() {
		return envioCancelacion;
	}

	public void setEnvioCancelacion(Short envioCancelacion) {
		this.envioCancelacion = envioCancelacion;
	}

	public Long getIdLiqSiniestro() {
		return idLiqSiniestro;
	}

	public void setIdLiqSiniestro(Long idLiqSiniestro) {
		this.idLiqSiniestro = idLiqSiniestro;
	}
	
	
	

}