package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoFacadeRemote;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.LlegadaCiaSeguros;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSapAmisSupervisionCampoService;
import mx.com.afirme.midas2.service.ocra.EnvioOcraService;
//import mx.com.afirme.midas2.service.operacionessapamis.siniestros.SapAmisSiniestrosService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.spv.ServicioPublicoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/siniestrocabina")
public class SiniestroCabinaAction<CatValorFijo>  extends BaseAction implements Preparable  {

	private static final String	LOCATION_LISTADOMOSTRARLLEGADACIAGRID_JSP	= "/jsp/siniestros/cabina/reportecabina/listadoMostrarLlegadaCiaGrid.jsp";
	private static final String	LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorMostrarLlegadaCia.jsp";
	private static final String	LOCATION_CONTENEDORSINIESTROCABINA_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorSiniestroCabina.jsp";
	private static final String LOCATION_REFERENCIA_BANCARIA_GRID	 =	"/jsp/siniestros/recuperacion/referenciaBancariaGrid.jsp";
	private static final String	LOCATION_CONTENEDORCOBERTURASRECHAZO_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorCoberturasRechazo.jsp";
	private static final String	LOCATION_CONTENEDORCOBERTURASRECHAZOGRID_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorCoberturasRechazoGrid.jsp";

	/**
	 * 
	 */
	private static final long serialVersionUID = -6699977229889054298L;
	
	public static final Logger log = Logger.getLogger(SiniestroCabinaAction.class);
	
	private Short tipoVista;
	public static final Short VISTA_INCISO_AFECTADO = 1;
	public static final Short VISTA_CIERRE_REPORTE = 2;
	
	private Map<String, String> catRespuestaSimple;
	private Map<String, String> catGenero;
	private Map<String, String> catTipoLicencia;
	private Map<String, String> catCausaSiniestro;
	private Map<String, String> catTipoResponsabilidad = new LinkedHashMap<String, String>();
	private Map<String, String> catTerminoAjusteSiniestro = new LinkedHashMap<String, String>();
	private Map<String, String> catTipoCorresponsabilidad;
	private Map<String, String> catTerminoSiniestro;
	private Map<String, String> catTipoPerdida;
	private Map<String, String> catColor;
	private Map<String, String> catOrigenSiniestro;
	private Map<String, String> catSapAmisSupervisionCampo;
	private Map<Long,String> ciaSeguros;
	private Map<String,String> motivosRechazo;
	private Boolean verAfectacionInciso;
	private int mostrarCoberturasAfectacion;
	private Short soloConsulta;
	private Boolean saltaPrepare;
	private Boolean mostrarBotonGenerarReferencia = false;
	
	/* Atributos para que cuando se regrese de un pase de atencion se pueda abrir el pop up de Listado de pases*/ 
	private Long idReporteCabina;
	private Long idCoberturaReporteCabina;
	private String tipoCalculo;
	private String tipoEstimacion;
	
	private Map<String, String> valuadores;
	private String codigoValuador;
	private String methodName;
	private String namespace;
	private Long idAjustadorReporte;
	private Long validOnMillis;
	private Long recordFromMillis;
	
	private SiniestroCabinaDTO siniestroDTO = new SiniestroCabinaDTO();
	private List<AfectacionCoberturaSiniestroDTO> listAfectacionCoberturaSiniestroDTO = new ArrayList<AfectacionCoberturaSiniestroDTO>();
	
	private List<ControlDinamicoRiesgoDTO> controles;
	private Long idCobertura;
	
	private LlegadaCiaSeguros llegadaCiaSeguros;
	private List<LlegadaCiaSeguros> llegadaCiaSegurosList;
	private Long idLlegadaCia;
	private Integer proveedorId;
	private List<BancoDTO>	bancoList;
	private List<ReferenciaBancariaDTO> lstReferenciasBancarias;
	
	private Map<String,String> mapCoberturasAfectables;
	private Long idCompaniaSiniestro;

	private String estatusValuacion;
	private Long valuacionId;
	private int estatusPendienteReporte;
	
	private Long idReporteRechazo;
	//monitor
	//private static final String reporteTerminoChannel = "/reporte-cabina/ajustador-termino/";
	
	private IncisoSiniestroDTO detalleInciso;
	private Short incisoAutorizado;
	
	
	// VARIABLES DE EXPEDIENTE JURIDICO
	private ExpedienteJuridico expedienteJuridico;
	private Boolean vieneExpedienteJuridico;
	private Boolean modoConsultaExpediente;
	
	
	//VARIABLES DE COMPAÑIA DE TERCERO
	private Boolean tieneCiaAsociada; 
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("siniestroCabinaServiceEJB")
	private SiniestroCabinaService siniestroService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@Autowired
	@Qualifier("polizaSiniestroServiceEJB")
	private PolizaSiniestroService polizaSiniestroService;

	@Autowired
	@Qualifier("valuacionReporteServiceEJB")
	private ValuacionReporteService valuacionReporteService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("hgsServiceEJB")
	private HgsService hgsService;
	
	@Autowired
	@Qualifier("ocraServiceEJB")
	private EnvioOcraService envioOcraService;
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	private UsuarioService usuarioService;
	
	@Autowired
	@Qualifier("bancoFacadeRemoteEJB")
	private BancoFacadeRemote bancoFacadeRemote;
	
	@Autowired
	@Qualifier("recuperacionServiceEJB")
	private RecuperacionService recuperacionService;
	
//	@Autowired
//	@Qualifier("sapAmisSiniestrosServiceEJB")
//	private SapAmisSiniestrosService sapAmisSiniestrosService;
	
	@Autowired
	@Qualifier("catSapAmisSupervisionCampoEJB")
	private CatSapAmisSupervisionCampoService catSapAmisSupervisionCampoService;

	@Autowired
	@Qualifier("servicioPublicoServiceEJB")
	private ServicioPublicoService servicioPublicoService;
	
	@Autowired
	@Qualifier("ingresoServiceEJB")
	private IngresoService ingresoService;
	
	


	@Override
	public void prepare(){
		catRespuestaSimple = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RESPUESTA_SIMPLE, Boolean.TRUE);
		catGenero = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.GENERO_PERSONA, Boolean.TRUE);
		catTipoLicencia = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_LICENCIA_CONDUCIR, Boolean.TRUE);
		catColor = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.COLOR, Boolean.TRUE);
		catCausaSiniestro = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSA_SINIESTRO, Boolean.TRUE);
		catSapAmisSupervisionCampo = catSapAmisSupervisionCampoService.obtenerCatalogo();
		//catTipoResponsabilidad = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RESPONSABILIDAD);
		
	}
	
	@Action (value = "mostrarContenedor", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String mostrarContenedor() {
		this.tieneCiaAsociada = this.tieneCiaSeguros();
		if(siniestroDTO.getReporteCabinaId() != null && siniestroDTO.getReporteCabinaId() > 0){
			siniestroDTO = siniestroService.obtenerSiniestroCabina(siniestroDTO.getReporteCabinaId());
		}
		return SUCCESS;
	}
	
	@Action (value = "guardar", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String guardar() {	
		try{
			siniestroService.guardarYValidaCias(siniestroDTO);
			siniestroDTO = siniestroService.obtenerSiniestroCabina(siniestroDTO.getReporteCabinaId());
			super.setMensajeExito();
		}catch (NegocioEJBExeption ex){
			String[] mensajesArray = Arrays.copyOf(ex.getValues(), ex.getValues().length, String[].class);
			List<String> mensajes = Arrays.asList(mensajesArray);
			if(mensajes.size() > 0){
				super.setMensajeListaPersonalizado("Mensajes de Validación", mensajes, TIPO_MENSAJE_INFORMACION);
			}
			return INPUT;

		}catch (Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			
			if (ex.getCause() instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(), "siniestroDTO");
			}	
			return INPUT;

		}
		return SUCCESS;
	}
	
	@Action (value = "convertirSiniestro", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String convertirSiniestro() {	
		try{
			// TODO Validar que tenga al menos una cobertura afectada
			log.info("CONVERTIR SINIESTRO. Entra a siniestroService.guardar");
			siniestroService.guardar(siniestroDTO);
			log.info("CONVERTIR SINIESTRO. Sale de siniestroService.guardar");
			
			log.info("CONVERTIR SINIESTRO. Entra a siniestroService.convertirSiniestro");
			SiniestroCabina siniestroCabina = siniestroService.convertirSiniestro(siniestroDTO);
			log.info("CONVERTIR SINIESTRO. sale de siniestroService.convertirSiniestro");
			setMensaje("Numero de Siniestro: " + siniestroCabina.getNumeroSiniestro());
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
//			sapAmisSiniestrosService.altaSiniestro(siniestroDTO);
			return SUCCESS;
		}catch (NegocioEJBExeption ex){
			log.error(ex.getMessageClean());
			super.setMensajeError(ex.getMessageClean());
			return INPUT;
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	
	
	@Action (value = "enviarHGSOCRA", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String enviarHGSOCRA() {	
		try{
			if ( this.hgsService.isPendientesHgs(siniestroDTO.getReporteCabinaId()) ){
				this.hgsService.insertarReporte( siniestroDTO.getReporteCabinaId() );
			}
			
			ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, siniestroDTO.getReporteCabinaId() );
			if( reporteCabina != null && this.envioOcraService.isPendienteOcra( reporteCabina.getNumeroReporte() ) ){
					this.envioOcraService.enviarAltaRoboOCRAServicio( siniestroDTO.getReporteCabinaId() );
			}
			
			if( this.servicioPublicoService.isPendientesSpv(siniestroDTO.getReporteCabinaId()) ) {
				this.servicioPublicoService.notificarReporteServicioPublico(siniestroDTO.getReporteCabinaId());
			}
			
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			return SUCCESS;
		}catch (NegocioEJBExeption ex){
			log.error(ex.getMessageClean());
			super.setMensajeError(ex.getMessageClean());
			return INPUT;
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	
	

	@Action (value = "mostrarIncisoAfectado", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/contenidoIncisoAfectado.jsp") })
	public String mostrarIncisoAfectado() {
		this.tieneCiaAsociada = this.tieneCiaSeguros();
		if(!siniestroDTO.getCargoDetalle() || siniestroDTO.getEsSiniestro() || soloConsulta == 1){
			siniestroService.obtenerDetalleSiniestroCabina(siniestroDTO);
			siniestroDTO.setCargoDetalle(true);
		}
		if( SystemCommonUtils.isValid(siniestroDTO.getCausaSiniestro()) &&
				SystemCommonUtils.isValid(siniestroDTO.getTipoResponsabilidad()) &&
				SystemCommonUtils.isValid(siniestroDTO.getTerminoAjuste())){
					this.mostrarCoberturasAfectacion = 1;
			}
		tipoVista = VISTA_INCISO_AFECTADO;
		return SUCCESS;	
	}

	@Action (value = "mostrarCierreReporte", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/contenidoCierreReporteSiniestro.jsp") })
	public String mostrarCierreReporte() {
		//proof if GeberarReferencia button is activated. This just when Recuperacion has a PaseAtencion linked.
		ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, siniestroDTO.getReporteCabinaId());
		Long estimacionId = ingresoService.comprobarSiRecuperacionTienePase(reporteCabina);
		if(estimacionId != null){
			mostrarBotonGenerarReferencia = true;
		}
		
		this.tieneCiaAsociada = this.tieneCiaSeguros();
		ciaSeguros = listadoService.getMapCiaDeSeguros(); 
		// Se comenta ya que no se requiere desplegar las cooberturas en este punto
		//this.mapCoberturasAfectables = listadoService.getCoberturasReporte(siniestroDTO.getReporteCabinaId(), null, null, null, Boolean.FALSE, Boolean.TRUE);
		
		catTipoPerdida = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PERDIDA, Boolean.TRUE);
		motivosRechazo = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVO_RECHAZO_SINIESTRO, Boolean.TRUE);
		bancoList = bancoFacadeRemote.findAll();
		if(!siniestroDTO.getCargoDetalle() || siniestroDTO.getEsSiniestro()){
			siniestroService.obtenerDetalleSiniestroCabina(siniestroDTO);
			siniestroDTO.setCargoDetalle(true);
		}
		catTipoResponsabilidad = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RESPONSABILIDAD, Boolean.TRUE);
		if(siniestroDTO.getCausaSiniestro()!=null 
				&& siniestroDTO.getTipoResponsabilidad() != null){
			catTerminoAjusteSiniestro = listadoService.obtenerTerminosAjuste(siniestroDTO.getCausaSiniestro(), siniestroDTO.getTipoResponsabilidad());
			catTerminoSiniestro       = listadoService.obtenerTerminosAjuste(siniestroDTO.getCausaSiniestro(), siniestroDTO.getTipoResponsabilidad());
		}
		
		catOrigenSiniestro = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ORIGEN_SINIESTRO, Boolean.TRUE);
		tipoVista = VISTA_CIERRE_REPORTE;
		return SUCCESS;	
	}
	
	@Action(value="puedeVerAfectacionInciso",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^verAfectacionInciso,mensaje,tipoMensaje,modoConsultaExpediente,vieneExpedienteJuridico,expedienteJuridico.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^verAfectacionInciso,mensaje,tipoMensaje,modoConsultaExpediente,vieneExpedienteJuridico,expedienteJuridico.*"})
		})
	public String puedeVerAfectacionInciso() {
		verAfectacionInciso = false;
		super.setMensajeError("Es necesario guardar previamente la información del inciso");
		try {			
			if(siniestroDTO.getReporteCabinaId() != null && siniestroDTO.getReporteCabinaId() > 0){				
				List<AutoIncisoReporteCabina> incisos = entidadService.findByProperty(AutoIncisoReporteCabina.class,
						"incisoReporteCabina.seccionReporteCabina.reporteCabina.id", siniestroDTO.getReporteCabinaId());
				if(incisos != null && incisos.size() > 0){
					if(incisos.get(0).getId() != null && incisos.get(0).getId() > 0){
						verAfectacionInciso = true;
						super.setMensajeError(null);
					}
				}							
			}
		} catch (Exception e) {			
			log.error(e);
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		return SUCCESS;
	}


	/**
	 * Obtiene el listado de las Coberturas de Afectacion de un Inciso de una Poliza
	 */
	@Action(value = "mostrarCoberturasAfectacionInciso", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/afectaciones/coberturasAfectacionIncisoGrid.jsp") })
	public String mostrarCoberturasAfectacionInciso(){		
		this.listAfectacionCoberturaSiniestroDTO = estimacionCoberturaSiniestroService.obtenerCoberturasAfectacion( siniestroDTO.getReporteCabinaId(), null, null, null );
		return SUCCESS;
	}
	/**
	 * Obtiene el los datos de riesgo asociados a una cobertura.
	 * @return
	 */

	@Action(value = "obtenerDatosRiesgo", results = { @Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/controlDinamicoRiesgoDetalle.jsp") })
	public String obtenerDatosRiesgo() {

		controles = polizaSiniestroService.obtenerDatosRiesgo(siniestroDTO.getReporteCabinaId(), idCobertura);

		return SUCCESS;
	}
	
	
	public void prepareMostrarEnviarValuacion(){
		valuadores = listadoService.obtenerValuadores();
	}
	
	
	@Action(value = "mostrarEnviarValuacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/contenidoEnvioValuacion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenidoEnvioValuacion.jsp")})
	public String mostrarEnviarValuacion(){
		if(siniestroDTO.getReporteCabinaId() != null && siniestroDTO.getReporteCabinaId() != 0){
			ReporteCabina reporte = entidadService.findById(ReporteCabina.class, siniestroDTO.getReporteCabinaId());
			if(reporte.getAjustador() != null){
				idAjustadorReporte = reporte.getAjustador().getId();
			}
		}
		return SUCCESS;
	}
	
	public void prepareEnviarSolicitudValuacion(){
		valuadores = listadoService.obtenerValuadores();
	}
	
	@Action(value = "enviarSolicitudValuacion", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
	                 "siniestroDTO.reporteCabinaId", "${siniestroDTO.reporteCabinaId}",
	                 "siniestroDTO.numValuacion", "${siniestroDTO.numValuacion}"}),
			@Result(name = INPUT, location = "/jsp/siniestros/cabina/reportecabina/contenidoEnvioValuacion.jsp")})
	public String enviarSolicitudValuacion(){
		try{
			if(!SystemCommonUtils.isNotNullNorZero(siniestroDTO.getNumValuacion()) && 
					SystemCommonUtils.isValid(codigoValuador) &&
					SystemCommonUtils.isNotNullNorZero(siniestroDTO.getReporteCabinaId())){
				siniestroDTO.setNumValuacion(valuacionReporteService.guardar(
						siniestroDTO.getReporteCabinaId(), codigoValuador, ValuacionReporteService.ESTATUS_EN_PROCESO));
			}else{
				setMensajeError("DEBE ELEGIR UN VALUADOR");
				return INPUT;
			}
		}catch(Exception e){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		setMensaje("Solicitud Enviada Correctamente");
		setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		methodName = "mostrarContenedor";
		namespace = "/siniestros/cabina/siniestrocabina";
		return INPUT;
	}
	
	@Action(value = "mostrarLlegadaOtraCia", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP)})
	public String mostrarLlegadaOtraCia(){
		this.ciaSeguros = listadoService.getMapCiaDeSeguros(); 
		return SUCCESS;
	}
	
	@Action(value = "agregarLlegadaOtraCia", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP)})
	public String agregarLlegadaOtraCia(){
		this.ciaSeguros = listadoService.getMapCiaDeSeguros(); 
		PrestadorServicio proovedor = this.entidadService.findById(PrestadorServicio.class, this.proveedorId);
		ReporteCabina reporteCabina = new ReporteCabina();
		reporteCabina.setId(this.idReporteCabina);
		this.llegadaCiaSeguros.setReporteCabina(reporteCabina);
		this.llegadaCiaSeguros.setProveedor(proovedor);
		this.llegadaCiaSeguros.setCodigoUsuarioCreacion(String.valueOf(usuarioService.getUsuarioActual()
				.getNombreUsuario()));
		this.entidadService.save(this.llegadaCiaSeguros);
		this.tieneCiaAsociada = this.tieneCiaSeguros();
		return SUCCESS;
	}
	
	@Action(value = "cargaLlegadasCiaGrid", results = {
			@Result(name = SUCCESS, location = LOCATION_LISTADOMOSTRARLLEGADACIAGRID_JSP) ,
			@Result(name = INPUT, location = LOCATION_LISTADOMOSTRARLLEGADACIAGRID_JSP)})
	public String cargaLlegadasCiaGrid(){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.id", this.idReporteCabina);
//		this.llegadaCiaSegurosList = this.entidadService.findByProperty(LlegadaCiaSeguros.class, "reporteCabina.id", this.idReporteCabina);
		this.llegadaCiaSegurosList = this.entidadService.findByPropertiesWithOrder(LlegadaCiaSeguros.class, params, "orden asc");
		return SUCCESS;
	}
	
	
	@Action(value = "eliminarLlegadaCia", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORMOSTRARLLEGADACIA_JSP)})
	public String eliminarLlegadaCia(){
		this.llegadaCiaSeguros = this.entidadService.findById(LlegadaCiaSeguros.class, Integer.valueOf(Long.toString(idLlegadaCia)));
		this.entidadService.remove(this.llegadaCiaSeguros);
		this.tieneCiaAsociada = this.tieneCiaSeguros();
		return SUCCESS;
	}
	
	@Action (value = "generarReferenciaRecuperacion", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String generarReferenciaRecuperacion() {	
		try{
			// TODO Validar que tenga al menos una cobertura afectada
			siniestroService.guardar(siniestroDTO);
			setMensaje("Se generó la recuperación correctamente");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			return SUCCESS;
		}catch (NegocioEJBExeption ex){
			log.error(ex.getMessageClean());
			super.setMensajeError(ex.getMessageClean());
			return INPUT;
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	
	@Action(value="buscarReferenciasBancarias",results={
			@Result(name=SUCCESS,location=LOCATION_REFERENCIA_BANCARIA_GRID),
			@Result(name=INPUT,location=LOCATION_REFERENCIA_BANCARIA_GRID)
			})
	public String buscarReferenciasBancarias(){	
		
		this.lstReferenciasBancarias= new ArrayList<ReferenciaBancariaDTO>();
		if(SystemCommonUtils.isNull(this.siniestroDTO.getRecuperacionId())){
			this.lstReferenciasBancarias=this.recuperacionService.obtenerCuentasBancarias();
		}else{
			String medioRecuperacion = null;
			if(siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_EFECTIVO) ||
					siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_EFECTIVO)){
				medioRecuperacion = Recuperacion.MedioRecuperacion.EFECTIVO.toString();
			}else if(siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_TDC) ||
					siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_TDC)){
				medioRecuperacion = Recuperacion.MedioRecuperacion.TDC.toString();
			}
			
			lstReferenciasBancarias = recuperacionService.obtenerReferenciasBancaria(siniestroDTO.getRecuperacionId(), medioRecuperacion);
			
			if(null==lstReferenciasBancarias || lstReferenciasBancarias.isEmpty() ){
				this.lstReferenciasBancarias=this.recuperacionService.obtenerCuentasBancarias();
			}
		}
			return SUCCESS;	
		
	}
	
	@Action(value = "actualizarEstatusValuacion", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estatusValuacion"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estatusValuacion"})
		})	
	public String actualizarEstatusValuacion(){
		estatusValuacion = siniestroService.obtenerEstatusValuacion(valuacionId);
		return SUCCESS;
	}
	
	
	@Action (value = "rechazarSiniestro", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String rechazarSiniestro() {	
		try{					
			siniestroService.rechazarSiniestro(siniestroDTO.getReporteCabinaId());	
			siniestroDTO.setEstatusReporte(EstatusReporteCabina.RECHAZADO.getValue());
			setMensaje("Se ha rechazado el Siniestro");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			setSoloConsulta((short) 1);
			return SUCCESS;

		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	
	@Action (value = "terminarSiniestro", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String terminarSiniestro() {	
		try{	
			siniestroService.terminarSiniestro(siniestroDTO.getReporteCabinaId());	
			siniestroDTO.setEstatusReporte(EstatusReporteCabina.TERMINADO.getValue());
			setMensaje("Se ha terminado el Siniestro");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			setSoloConsulta((short) 1);
			return SUCCESS;
		
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}

	
	@Action (value = "cancelarSiniestro", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String cancelarSiniestro() {	
		try{			
			siniestroService.cancelarSiniestro(siniestroDTO.getReporteCabinaId());		
			siniestroDTO.setEstatusReporte(EstatusReporteCabina.CANCELADO.getValue());
			setMensaje("Se ha cancelado el Siniestro");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			setSoloConsulta((short) 1);
//			sapAmisSiniestrosService.cancelarSiniestro(siniestroDTO);
			return SUCCESS;
		
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}
	
	@Action (value = "reaperturarReporte", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORSINIESTROCABINA_JSP) })
	public String reaperturarReporte() {	
					
		ReporteCabina reporte = siniestroService.reaperturarReporte(siniestroDTO.getReporteCabinaId());	
		siniestroDTO.setEstatusReporte(reporte.getEstatus());
		setMensaje("Se ha reaperturado el Siniestro");
		setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		setSoloConsulta((short) 0);
		return SUCCESS;
		
	}
	
	@Action(value = "validarPendientesReporte", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estatusPendienteReporte,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^estatusPendienteReporte,mensaje"})
		})	
	public String validarPendientesReporte(){
		try{
			siniestroService.validarCambioEstatus(siniestroDTO.getReporteCabinaId());	
			estatusPendienteReporte = 0;
		}catch (NegocioEJBExeption ex){
			log.error(ex.getMessageClean());
			if (ex.getErrorCode().equals("NE_O")) {
				super.setMensajeError(ex.getMessageClean());
				estatusPendienteReporte = 1;
			} else if (ex.getErrorCode().equals("NE_R")) {
				super.setMensajeError(ex.getMessageClean());
				estatusPendienteReporte = 2;
			}
		}
		return SUCCESS;
	}

	@Action(value = "mostrarCoberturasRechazo", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORCOBERTURASRECHAZO_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORCOBERTURASRECHAZO_JSP)})
	public String mostrarCoberturasRechazo(){
		return SUCCESS;
	}
	
	@Action(value = "buscarCoberturasRechazo", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORCOBERTURASRECHAZOGRID_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORCOBERTURASRECHAZO_JSP)})
	public String buscarCoberturasRechazo(){
		try{
			listAfectacionCoberturaSiniestroDTO = estimacionCoberturaSiniestroService.obtenerCoberturasAfectacion(idReporteRechazo);
		}catch(Exception ex){
			setMensajeError("No se pudo recuperar la informaci\u00F3n de las coberturas del reporte");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarCoberturasRechazo", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORCOBERTURASRECHAZO_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORCOBERTURASRECHAZO_JSP)})
	public String guardarCoberturasRechazo(){
		try{
				estimacionCoberturaSiniestroService.guardarCoberturaMontoRechazo(listAfectacionCoberturaSiniestroDTO, idReporteRechazo);
				siniestroDTO.setReporteCabinaId(idReporteRechazo);
				siniestroDTO = siniestroService.obtenerDetalleSiniestroCabina(siniestroDTO);
				
		}catch(Exception ex){
			setMensajeError("No se pudo guardar la informaci\u00F3n de las coberturas del reporte");
			return INPUT;
		}
		return SUCCESS;
	}
	
	private boolean tieneCiaSeguros(){
		if(this.siniestroDTO != null && this.siniestroDTO.getReporteCabinaId()!=null){
			List<LlegadaCiaSeguros> cias =  this.entidadService.findByProperty(LlegadaCiaSeguros.class, "reporteCabina.id", siniestroDTO.getReporteCabinaId());
			if(cias != null && cias.size()>0)
				return true;
			return false;
		}
		return false;
	}
	
	
	
	public Short getTipoVista() {
		return tipoVista;
	}

	public void setTipoVista(Short tipoVista) {
		this.tipoVista = tipoVista;
	}

	public Map<String, String> getCatRespuestaSimple() {
		return catRespuestaSimple;
	}

	public void setCatRespuestaSimple(Map<String, String> catRespuestaSimple) {
		this.catRespuestaSimple = catRespuestaSimple;
	}

	public Map<String, String> getCatGenero() {
		return catGenero;
	}

	public void setCatGenero(Map<String, String> catGenero) {
		this.catGenero = catGenero;
	}

	public Map<String, String> getCatTipoLicencia() {
		return catTipoLicencia;
	}

	public void setCatTipoLicencia(Map<String, String> catTipoLicencia) {
		this.catTipoLicencia = catTipoLicencia;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getCatCausaSiniestro() {
		return catCausaSiniestro;
	}

	public void setCatCausaSiniestro(Map<String, String> catCausaSiniestro) {
		this.catCausaSiniestro = catCausaSiniestro;
	}

	public Map<String, String> getCatTipoCorresponsabilidad() {
		return catTipoCorresponsabilidad;
	}

	public void setCatTipoCorresponsabilidad(
			Map<String, String> catTipoCorresponsabilidad) {
		this.catTipoCorresponsabilidad = catTipoCorresponsabilidad;
	}

	public SiniestroCabinaDTO getSiniestroDTO() {
		return siniestroDTO;
	}

	public void setSiniestroDTO(SiniestroCabinaDTO siniestroDTO) {
		this.siniestroDTO = siniestroDTO;
	}

	public Map<String, String> getCatTipoResponsabilidad() {
		return catTipoResponsabilidad;
	}

	public void setCatTipoResponsabilidad(Map<String, String> catTipoResponsabilidad) {
		this.catTipoResponsabilidad = catTipoResponsabilidad;
	}

	public Map<String, String> getCatTerminoAjusteSiniestro() {
		return catTerminoAjusteSiniestro;
	}

	public void setCatTerminoAjusteSiniestro(
			Map<String, String> catTerminoAjusteSiniestro) {
		this.catTerminoAjusteSiniestro = catTerminoAjusteSiniestro;
	}

	public SiniestroCabinaService getSiniestroService() {
		return siniestroService;
	}

	public void setSiniestroService(SiniestroCabinaService siniestroService) {
		this.siniestroService = siniestroService;
	}

	public Map<String, String> getCatColor() {
		return catColor;
	}

	public void setCatColor(Map<String, String> catColor) {
		this.catColor = catColor;
	}
	
	public Map<String, String> getCatOrigenSiniestro() {
		return catOrigenSiniestro;
	}

	public void setCatOrigenSiniestro(Map<String, String> catOrigenSiniestro) {
		this.catOrigenSiniestro = catOrigenSiniestro;
	}

	public Map<Long, String> getCiaSeguros() {
		return ciaSeguros;
	}

	public void setCiaSeguros(Map<Long, String> ciaSeguros) {
		this.ciaSeguros = ciaSeguros;
	}

	public Map<String, String> getCatTerminoSiniestro() {
		return catTerminoSiniestro;
	}

	public void setCatTerminoSiniestro(Map<String, String> catTerminoSiniestro) {
		this.catTerminoSiniestro = catTerminoSiniestro;
	}

	public Boolean getVerAfectacionInciso() {
		return verAfectacionInciso;
	}

	public void setVerAfectacionInciso(Boolean verAfectacionInciso) {
		this.verAfectacionInciso = verAfectacionInciso;
	}

	/**
	 * @param listAfectacionCoberturaSiniestroDTO the listAfectacionCoberturaSiniestroDTO to set
	 */
	public void setListAfectacionCoberturaSiniestroDTO(
			List<AfectacionCoberturaSiniestroDTO> listAfectacionCoberturaSiniestroDTO) {
		this.listAfectacionCoberturaSiniestroDTO = listAfectacionCoberturaSiniestroDTO;
	}

	/**
	 * @return the listAfectacionCoberturaSiniestroDTO
	 */
	public List<AfectacionCoberturaSiniestroDTO> getListAfectacionCoberturaSiniestroDTO() {
		return listAfectacionCoberturaSiniestroDTO;
	}

	/**
	 * @param estimacionCoberturaSiniestroService the estimacionCoberturaSiniestroService to set
	 */
	public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}

	/**
	 * @return the estimacionCoberturaSiniestroService
	 */
	public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
		return estimacionCoberturaSiniestroService;
	}

	/**
	 * @return the mostrarCoberturasAfectacion
	 */
	public int getMostrarCoberturasAfectacion() {
		return mostrarCoberturasAfectacion;
	}

	/**
	 * @param mostrarCoberturasAfectacion the mostrarCoberturasAfectacion to set
	 */
	public void setMostrarCoberturasAfectacion(int mostrarCoberturasAfectacion) {
		this.mostrarCoberturasAfectacion = mostrarCoberturasAfectacion;
	}

	public Long getIdReporteCabina() {
		return idReporteCabina;
	}

	public void setIdReporteCabina(Long idReporteCabina) {
		this.idReporteCabina = idReporteCabina;
	}

	public Long getIdCoberturaReporteCabina() {
		return idCoberturaReporteCabina;
	}

	public void setIdCoberturaReporteCabina(Long idCoberturaReporteCabina) {
		this.idCoberturaReporteCabina = idCoberturaReporteCabina;
	}

	public String getTipoCalculo() {
		return tipoCalculo;
	}

	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}

	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}

	/**
	 * @param soloConsulta the soloConsulta to set
	 */
	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	/**
	 * @return the soloConsulta
	 */
	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public Map<String, String> getCatTipoPerdida() {
		return catTipoPerdida;
	}

	public void setCatTipoPerdida(Map<String, String> catTipoPerdida) {
		this.catTipoPerdida = catTipoPerdida;
	}

	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}

	public void setControles(List<ControlDinamicoRiesgoDTO> controles) {
		this.controles = controles;
	}

	public Long getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	
	

	/**
	 * @return the valuadores
	 */
	public Map<String, String> getValuadores() {
		return valuadores;
	}

	/**
	 * @param valuadores the valuadores to set
	 */
	public void setValuadores(Map<String, String> valuadores) {
		this.valuadores = valuadores;
	}

	/**
	 * @return the codigoValuador
	 */
	public String getCodigoValuador() {
		return codigoValuador;
	}

	/**
	 * @param codigoValuador the codigoValuador to set
	 */
	public void setCodigoValuador(String codigoValuador) {
		this.codigoValuador = codigoValuador;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the valuacionReporteService
	 */
	public ValuacionReporteService getValuacionReporteService() {
		return valuacionReporteService;
	}

	/**
	 * @param valuacionReporteService the valuacionReporteService to set
	 */
	public void setValuacionReporteService(
			ValuacionReporteService valuacionReporteService) {
		this.valuacionReporteService = valuacionReporteService;
	}

	/**
	 * @return the entidadService
	 */
	public EntidadService getEntidadService() {
		return entidadService;
	}

	/**
	 * @param entidadService the entidadService to set
	 */
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the idAjustadorReporte
	 */
	public Long getIdAjustadorReporte() {
		return idAjustadorReporte;
	}

	/**
	 * @param idAjustadorReporte the idAjustadorReporte to set
	 */
	public void setIdAjustadorReporte(Long idAjustadorReporte) {
		this.idAjustadorReporte = idAjustadorReporte;
	}

	/**
	 * @return the validOnMillis
	 */
	public Long getValidOnMillis() {
		return validOnMillis;
	}

	/**
	 * @param validOnMillis the validOnMillis to set
	 */
	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}

	/**
	 * @return the recordFromMillis
	 */
	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	/**
	 * @param recordFromMillis the recordFromMillis to set
	 */
	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public HgsService getHgsService() {
		return hgsService;
	}

	public void setHgsService(HgsService hgsService) {
		this.hgsService = hgsService;
	}

	public EnvioOcraService getEnvioOcraService() {
		return envioOcraService;
	}

	public void setEnvioOcraService(EnvioOcraService envioOcraService) {
		this.envioOcraService = envioOcraService;
	}

	public LlegadaCiaSeguros getLlegadaCiaSeguros() {
		return llegadaCiaSeguros;
	}

	public void setLlegadaCiaSeguros(LlegadaCiaSeguros llegadaCiaSeguros) {
		this.llegadaCiaSeguros = llegadaCiaSeguros;
	}

	public Boolean getSaltaPrepare() {
		return saltaPrepare;
	}

	public void setSaltaPrepare(Boolean saltaPrepare) {
		this.saltaPrepare = saltaPrepare;
	}

	public List<LlegadaCiaSeguros> getLlegadaCiaSegurosList() {
		return llegadaCiaSegurosList;
	}

	public void setLlegadaCiaSegurosList(
			List<LlegadaCiaSeguros> llegadaCiaSegurosList) {
		this.llegadaCiaSegurosList = llegadaCiaSegurosList;
	}

	public Long getIdLlegadaCia() {
		return idLlegadaCia;
	}

	public void setIdLlegadaCia(Long idLlegadaCia) {
		this.idLlegadaCia = idLlegadaCia;
	}

	public Integer getProveedorId() {
		return proveedorId;
	}

	public void setProveedorId(Integer proveedorId) {
		this.proveedorId = proveedorId;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public List<BancoDTO> getBancoList() {
		return bancoList;
	}

	public void setBancoList(List<BancoDTO> bancoList) {
		this.bancoList = bancoList;
	}

	public List<ReferenciaBancariaDTO> getLstReferenciasBancarias() {
		return lstReferenciasBancarias;
	}

	public void setLstReferenciasBancarias(List<ReferenciaBancariaDTO> lstReferenciasBancarias) {
		this.lstReferenciasBancarias = lstReferenciasBancarias;
	}

	public Map<String, String> getMapCoberturasAfectables() {
		return mapCoberturasAfectables;
	}

	public void setMapCoberturasAfectables(
			Map<String, String> mapCoberturasAfectables) {
		this.mapCoberturasAfectables = mapCoberturasAfectables;
	}

	public Long getIdCompaniaSiniestro() {
		return idCompaniaSiniestro;
	}

	public void setIdCompaniaSiniestro(Long idCompaniaSiniestro) {
		this.idCompaniaSiniestro = idCompaniaSiniestro;
	}
	
	public String getEstatusValuacion() {
		return estatusValuacion;
	}

	public void setEstatusValuacion(String estatusValuacion) {
		this.estatusValuacion = estatusValuacion;
	}

	public Long getValuacionId() {
		return valuacionId;
	}

	public void setValuacionId(Long valuacionId) {
		this.valuacionId = valuacionId;
	}

	public int getEstatusPendienteReporte() {
		return estatusPendienteReporte;
	}

	public void setEstatusPendienteReporte(int estatusPendienteReporte) {
		this.estatusPendienteReporte = estatusPendienteReporte;
	}

	public Map<String, String> getMotivosRechazo() {
		return motivosRechazo;
	}

	public void setMotivosRechazo(Map<String, String> motivosRechazo) {
		this.motivosRechazo = motivosRechazo;
	}

	public ExpedienteJuridico getExpedienteJuridico() {
		return expedienteJuridico;
	}

	public void setExpedienteJuridico(ExpedienteJuridico expedienteJuridico) {
		this.expedienteJuridico = expedienteJuridico;
	}

	public Boolean getVieneExpedienteJuridico() {
		return vieneExpedienteJuridico;
	}

	public void setVieneExpedienteJuridico(Boolean vieneExpedienteJuridico) {
		this.vieneExpedienteJuridico = vieneExpedienteJuridico;
	}

	public Boolean getModoConsultaExpediente() {
		return modoConsultaExpediente;
	}

	public void setModoConsultaExpediente(Boolean modoConsultaExpediente) {
		this.modoConsultaExpediente = modoConsultaExpediente;
	}
	public Long getIdReporteRechazo() {
		return idReporteRechazo;
	}

	public void setIdReporteRechazo(Long idReporteRechazo) {
		this.idReporteRechazo = idReporteRechazo;
	}

	public IncisoSiniestroDTO getDetalleInciso() {
		return detalleInciso;
	}

	public void setDetalleInciso(IncisoSiniestroDTO detalleInciso) {
		this.detalleInciso = detalleInciso;
	}

	public Short getIncisoAutorizado() {
		return incisoAutorizado;
	}

	public void setIncisoAutorizado(Short incisoAutorizado) {
		this.incisoAutorizado = incisoAutorizado;
	}

	public Boolean getTieneCiaAsociada() {
		return tieneCiaAsociada;
	}

	public void setTieneCiaAsociada(Boolean tieneCiaAsociada) {
		this.tieneCiaAsociada = tieneCiaAsociada;
	}

	public IngresoService getIngresoService() {
		return ingresoService;
	}

	public void setIngresoService(IngresoService ingresoService) {
		this.ingresoService = ingresoService;
	}
	
	public Boolean getMostrarBotonGenerarReferencia() {
		return mostrarBotonGenerarReferencia;
	}

	public void setMostrarBotonGenerarReferencia(
			Boolean mostrarBotonGenerarReferencia) {
		this.mostrarBotonGenerarReferencia = mostrarBotonGenerarReferencia;
	}

	public Map<String, String> getCatSapAmisSupervisionCampo() {
		return catSapAmisSupervisionCampo;
	}

	public void setCatSapAmisSupervisionCampo(Map<String, String> catSapAmisSupervisionCampo) {
		this.catSapAmisSupervisionCampo = catSapAmisSupervisionCampo;
	}
}
