package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.personadireccion.PersonaFisicaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMoralMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteReporte;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.PrestadorGastoAjusteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/reporte/gastoAjuste")
public class GastoAjusteAction  extends BaseAction implements Preparable {

	private static final int	LONGITUD_BUSQUEDA_AUTOCOMPLETADO		= 3;
	private static final String	LOCATION_GASTOAJUSTEGRID_JSP			= "/jsp/siniestros/cabina/reportecabina/gastoAjusteGrid.jsp";
	private static final String	LOCATION_AUTOCOMPLETADOPRESTADOR_JSP	= "/jsp/siniestros/cabina/reportecabina/autoCompletadoPrestador.jsp";
	private static final String	LOCATION_GASTOAJUSTE_JSP				= "/jsp/siniestros/cabina/reportecabina/gastoAjuste.jsp";

	/**
	 * 
	 */
	private static final long serialVersionUID = 726205638972480870L;
	
	private static final String TIPO_PRESTADOR_GRUA = "GRU";
	private static final String TIPO_PRESTADOR_TALLER = "TALL";
	
	private GastoAjusteDTO gastoAjusteDTO = new GastoAjusteDTO();
	private List<Map<String, Object>> gastosAjusteList;
	private List<PrestadorGastoAjusteDTO> resultadoPrestadores;
	private Long idToReporte;
	private Map<String, String> tiposPrestador;
	private Map<String, String> motivosSituacion;
	private Map<String, String> tiposGrua;
	private String prestadorOrigen;
	private String nombrePrestador;
	private String nombreTaller;
	private String tipoPrestadorId;
	private Short soloConsulta;
	
	@Autowired
	@Qualifier("gastoAjusteServiceEJB")
	private GastoAjusteService gastoAjusteService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	private PrestadorDeServicioService prestadorService;
	
	@Autowired
	@Qualifier("servicioSiniestroServiceEJB")
	private ServicioSiniestroService servicioSiniestroService;

	@Override
	public void prepare(){
		tiposPrestador = gastoAjusteService.consultarPrestadoresGastoAjuste();
		motivosSituacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVO_SITUACION);
		tiposGrua = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_GRUA);
	}
	
	@Action (value = "buscarDatosPrestador", results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gastoAjusteDTO.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gastoAjusteDTO.*"})
		})
	public String buscarDatosPrestador(){
		PersonaMidas persona;
		String[] prestadorOrigenAux = prestadorOrigen.split(",");
		if(prestadorOrigenAux.length > 0){
			gastoAjusteDTO.setOrigen(Short.parseShort(prestadorOrigenAux[0]));
			gastoAjusteDTO.setPrestadorAjusteId(Integer.parseInt(prestadorOrigenAux[1]));
			if(gastoAjusteDTO.getOrigen() == GastoAjusteReporte.Origen.PRESTADOR.getValue()){
				persona = prestadorService.buscarPrestador(gastoAjusteDTO.getPrestadorAjusteId()).getPersonaMidas();
			}else{
				persona = servicioSiniestroService.obtener(ServicioSiniestro.class, new Long(gastoAjusteDTO.getPrestadorAjusteId())).getPersonaMidas();
			}
			nombrePrestador = persona.getNombre();
			if(persona.getTipoPersona().equalsIgnoreCase("PF")){
				PersonaFisicaMidas pf = (PersonaFisicaMidas)persona;
				gastoAjusteDTO.setCurp(pf.getCurp());
			}else{
				PersonaMoralMidas pm = (PersonaMoralMidas)persona;
				gastoAjusteDTO.setRfc(pm.getRfc());
			}
		}
		return SUCCESS;
	}
	
	@Action (value = "mostrarGasto", results = { 
			@Result(name = SUCCESS, location = LOCATION_GASTOAJUSTE_JSP) })
	public String mostrarContenedorCatalogo() {	
		GastoAjusteReporte gastoAjuste = gastoAjusteService.buscarGasto(gastoAjusteDTO.getId());
		idToReporte = gastoAjuste.getReporteCabina().getId();
		PersonaMidas persona;
		if(gastoAjuste.getOrigen() == GastoAjusteReporte.Origen.PRESTADOR.getValue()){
			persona = gastoAjuste.getPrestadorServicio().getPersonaMidas();
		}else{
			persona = gastoAjuste.getServicioSiniestro().getPersonaMidas();
		}
		nombrePrestador = persona.getNombre();
		tipoPrestadorId = gastoAjuste.getTipoPrestadorServicio();
		if(persona.getTipoPersona().equalsIgnoreCase("PF")){
			PersonaFisicaMidas pf = (PersonaFisicaMidas)persona;
			gastoAjusteDTO.setCurp(pf.getCurp());
		}else{
			PersonaMoralMidas pm = (PersonaMoralMidas)persona;
			gastoAjusteDTO.setRfc(pm.getRfc());
		}
		gastoAjusteDTO.setMotivoSituacion(String.valueOf(gastoAjuste.getMotivoSituacion()));
		gastoAjusteDTO.setMostrarSeccionGrua(isTipoPrestadorGrua(tipoPrestadorId));
		gastoAjusteDTO.setNumeroFactura(gastoAjuste.getNumeroFactura());
		gastoAjusteDTO.setNumeroReporteExterno(gastoAjuste.getNumeroReporteExterno());
		if(gastoAjusteDTO.getMostrarSeccionGrua()){
			gastoAjusteDTO.setTipoGrua(String.valueOf(gastoAjuste.getTipoGrua()));
			if(gastoAjuste.getTallerAsignado() != null){
				nombreTaller = gastoAjuste.getTallerAsignado().getPersonaMidas().getNombre();
				gastoAjusteDTO.setTallerAsignadoId(gastoAjuste.getTallerAsignado().getId());
			}
		}
		return SUCCESS;
	}
	
	@Action (value = "buscarNombrePrestador", results = { 
			@Result(name = SUCCESS, location = LOCATION_AUTOCOMPLETADOPRESTADOR_JSP) })
	public String buscarNombrePrestador(){
		if (SystemCommonUtils.isValid(nombrePrestador) && SystemCommonUtils.isValid(tipoPrestadorId) && nombrePrestador.length() > LONGITUD_BUSQUEDA_AUTOCOMPLETADO) {
			resultadoPrestadores = gastoAjusteService.buscarPrestadorPorNombreYTipo(nombrePrestador, tipoPrestadorId);
		}
		return SUCCESS;
	}
	
	@Action (value = "buscarNombreTaller", results = { 
			@Result(name = SUCCESS, location = LOCATION_AUTOCOMPLETADOPRESTADOR_JSP) })
	public String buscarNombreTaller(){
		if (nombreTaller != null && nombreTaller.length() > 3) {
			resultadoPrestadores = gastoAjusteService.buscarPrestadorPorNombreYTipo(nombreTaller, TIPO_PRESTADOR_TALLER);
		}
		return SUCCESS;
	}
	
	@Action (value = "guardar", results = { 
			@Result(name = SUCCESS, location = LOCATION_GASTOAJUSTE_JSP),
			@Result(name = INPUT, location = LOCATION_GASTOAJUSTE_JSP)})
	public String guardar(){	
		gastoAjusteDTO.setTipoPrestadorId(tipoPrestadorId);
		gastoAjusteService.guardarGastoAjuste(idToReporte, gastoAjusteDTO);
		limpiarVariables();	
		return SUCCESS;

	}
	
	@Action (value = "nuevo", results = { 
			@Result(name = SUCCESS, location = LOCATION_GASTOAJUSTE_JSP),
			@Result(name = INPUT, location = LOCATION_GASTOAJUSTE_JSP)})
	public String nuevo(){
		limpiarVariables();
		return SUCCESS;
	}
	
	@Action (value = "cargarDatosAjuste", results = { 
			@Result(name = SUCCESS, location = LOCATION_GASTOAJUSTEGRID_JSP),
			@Result(name = INPUT, location = LOCATION_GASTOAJUSTEGRID_JSP)})
	public String cargarDatosAjuste(){
		gastosAjusteList = gastoAjusteService.obtenerGastosAjuste(idToReporte);
		gastoAjusteDTO = new GastoAjusteDTO();
		gastoAjusteDTO.setMostrarSeccionGrua(false);
		return SUCCESS;
	}
	
	@Action (value = "mostrarGastosAjuste", results = { 
			@Result(name = SUCCESS, location = LOCATION_GASTOAJUSTE_JSP),
			@Result(name = INPUT, location = LOCATION_GASTOAJUSTE_JSP)})
	public String mostrarGastosAjuste(){
		return SUCCESS;
	}
	
	@Action (value = "onChangeTipoPrestador", results = { 
			@Result(name = SUCCESS, location = LOCATION_GASTOAJUSTE_JSP),
			@Result(name = INPUT, location = LOCATION_GASTOAJUSTE_JSP)})
	public String onChangeTipoPrestador(){
		gastoAjusteDTO.setMostrarSeccionGrua(isTipoPrestadorGrua(tipoPrestadorId));
		return SUCCESS;
	}
	
	public Boolean isTipoPrestadorGrua(String tipoPres){
		TipoPrestadorServicio tp = gastoAjusteService.buscarTipoPrestadorbyNombre(TIPO_PRESTADOR_GRUA);
		return tp.getNombre().equalsIgnoreCase(tipoPres);
	}
	
	public void limpiarVariables(){
		gastoAjusteDTO = new GastoAjusteDTO();
		prestadorOrigen = null;
		nombrePrestador = null;
		nombreTaller = null;
		tipoPrestadorId = null;
	}

	public GastoAjusteDTO getGastoAjusteDTO() {
		return gastoAjusteDTO;
	}

	public void setGastoAjusteDTO(GastoAjusteDTO gastoAjusteDTO) {
		this.gastoAjusteDTO = gastoAjusteDTO;
	}

	public List<Map<String, Object>> getGastosAjusteList() {
		return gastosAjusteList;
	}

	public void setGastosAjusteList(List<Map<String, Object>> gastosAjusteList) {
		this.gastosAjusteList = gastosAjusteList;
	}

	public Long getIdToReporte() {
		return idToReporte;
	}

	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}

	public Map<String, String> getTiposPrestador() {
		return tiposPrestador;
	}

	public void setTiposPrestador(Map<String, String> tiposPrestador) {
		this.tiposPrestador = tiposPrestador;
	}

	public Map<String, String> getMotivosSituacion() {
		return motivosSituacion;
	}

	public void setMotivosSituacion(Map<String, String> motivosSituacion) {
		this.motivosSituacion = motivosSituacion;
	}

	public Map<String, String> getTiposGrua() {
		return tiposGrua;
	}

	public void setTiposGrua(Map<String, String> tiposGrua) {
		this.tiposGrua = tiposGrua;
	}

	public GastoAjusteService getGastoAjusteService() {
		return gastoAjusteService;
	}

	public void setGastoAjusteService(GastoAjusteService gastoAjusteService) {
		this.gastoAjusteService = gastoAjusteService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public List<PrestadorGastoAjusteDTO> getResultadoPrestadores() {
		return resultadoPrestadores;
	}

	public void setResultadoPrestadores(
			List<PrestadorGastoAjusteDTO> resultadoPrestadores) {
		this.resultadoPrestadores = resultadoPrestadores;
	}

	public String getNombrePrestador() {
		return nombrePrestador;
	}

	public void setNombrePrestador(String nombrePrestador) {
		this.nombrePrestador = nombrePrestador;
	}

	public String getNombreTaller() {
		return nombreTaller;
	}

	public void setNombreTaller(String nombreTaller) {
		this.nombreTaller = nombreTaller;
	}

	public String getTipoPrestadorId() {
		return tipoPrestadorId;
	}

	public void setTipoPrestadorId(String tipoPrestadorId) {
		this.tipoPrestadorId = tipoPrestadorId;
	}

	public String getPrestadorOrigen() {
		return prestadorOrigen;
	}

	public void setPrestadorOrigen(String prestadorOrigen) {
		this.prestadorOrigen = prestadorOrigen;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}
	
	

}
