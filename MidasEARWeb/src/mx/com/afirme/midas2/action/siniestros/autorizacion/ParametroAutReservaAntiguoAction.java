package mx.com.afirme.midas2.action.siniestros.autorizacion;

import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroReservaAntiguo;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

 

@Component
@Scope("prototype")
@Namespace("/siniestros/autorizacion/historico")
public class ParametroAutReservaAntiguoAction extends BaseAction implements Preparable {

	private static final String	LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP	= "/jsp/siniestros/autorizacion/parametroAutReservaAntiguo.jsp";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long idParametro;
	private Map<Long,String> listOficina;
	private Map<String,String> listCoberturas;
	private Map<String,String> listCriterio;
	private List<ParametroReservaAntiguo> listParametros;
	private Map<String,String> listRoles;
	private Map<String,String> listTipoAjuste;
	private Map<String,String> listTipoSiniestro;
	private ParametroReservaAntiguo parametro;
	
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("autorizacionReservaServiceEJB")
	private AutorizacionReservaService autorizacionReservaService;

	public void prepare(){
		this.listOficina = listadoService.obtenerOficinasSiniestros();
		this.listCoberturas = this.listadoService.obtenerCoberturaPorSeccionConDescripcion(null);
		this.listTipoAjuste = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AJUSTE);
		this.listTipoSiniestro = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSA_SINIESTRO);
		this.listCriterio = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CRITERIO_COMPARACION);
		this.listRoles = this.autorizacionReservaService.obtenerRolesAutorizacion();
	}

	
	@Action(value = "mostrarContenedor", results = {
			@Result(name = SUCCESS, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP),
			@Result(name = INPUT, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP) })
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	@Action(value = "guardarConfiguracion", results = {
			@Result(name = SUCCESS, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP),
			@Result(name = INPUT, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP) })
	public String guardarConfiguracion(){
		this.autorizacionReservaService.guardarParametroAntiguedad(parametro);
		this.parametro = new ParametroReservaAntiguo();
		super.setMensaje(MensajeDTO.MENSAJE_GUARDAR_CONFIGURACION_AUT_RESERVA);
		super.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		return SUCCESS;
	}

	
	@Action(value = "inactivarConfiguracion", results = {
			@Result(name = SUCCESS, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP),
			@Result(name = INPUT, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP) })
	public String inactivarConfiguracion(){
		this.autorizacionReservaService.inactivarParametroAntiguedad(idParametro);
		return SUCCESS;
	}

	
	@Action(value = "listarParametros", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/autorizacion/parametroAutReservaAntGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_PARAMETROAUTRESERVAANTIGUO_JSP) })
	public String listarParametros(){
		this.listParametros = this.autorizacionReservaService.listarParametrosAntiguedad();
		return SUCCESS;
	}

	
	/**
	 * Método que muestra la sección de Configuración de un Parametro de Reservas Antigüas
	 * @return
	 */
	public String mostrarConfiguracion(){
		if(idParametro!=null){
			this.parametro = this.autorizacionReservaService.obtenerParametroAntiguedad(idParametro);
		}
		return "";
	}

	public Long getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(Long idParametro) {
		this.idParametro = idParametro;
	}

	public Map<Long, String> getListOficina() {
		return listOficina;
	}

	public void setListOficina(Map<Long, String> listOficina) {
		this.listOficina = listOficina;
	}

	

	public Map<String, String> getListCoberturas() {
		return listCoberturas;
	}


	public void setListCoberturas(Map<String, String> listCoberturas) {
		this.listCoberturas = listCoberturas;
	}


	public Map<String, String> getListCriterio() {
		return listCriterio;
	}

	public void setListCriterio(Map<String, String> listCriterio) {
		this.listCriterio = listCriterio;
	}

	public List<ParametroReservaAntiguo> getListParametros() {
		return listParametros;
	}

	public void setListParametros(List<ParametroReservaAntiguo> listParametros) {
		this.listParametros = listParametros;
	}

	public Map<String, String> getListRoles() {
		return listRoles;
	}

	public void setListRoles(Map<String, String> listRoles) {
		this.listRoles = listRoles;
	}

	public Map<String, String> getListTipoAjuste() {
		return listTipoAjuste;
	}

	public void setListTipoAjuste(Map<String, String> listTipoAjuste) {
		this.listTipoAjuste = listTipoAjuste;
	}

	public Map<String, String> getListTipoSiniestro() {
		return listTipoSiniestro;
	}

	public void setListTipoSiniestro(Map<String, String> listTipoSiniestro) {
		this.listTipoSiniestro = listTipoSiniestro;
	}

	public ParametroReservaAntiguo getParametro() {
		return parametro;
	}

	public void setParametro(ParametroReservaAntiguo parametro) {
		this.parametro = parametro;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public AutorizacionReservaService getAutorizacionReservaService() {
		return autorizacionReservaService;
	}

	public void setAutorizacionReservaService(
			AutorizacionReservaService autorizacionReservaService) {
		this.autorizacionReservaService = autorizacionReservaService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	

}