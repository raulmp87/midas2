package mx.com.afirme.midas2.action.siniestros.catalogo.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.AjustadorEstatusDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService.ReporteEstatusDTO;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/catalogo/servicio")
public class MapaSiniestrosAction  extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
// Variables para validar que mostrar y que NO mostrar
	private boolean mostrarOficinas = false;
	private boolean mostrarListadoAjustadores = false;
	private boolean mostrarInformacionReporte = false;

// Atributos a utilizar
	private Long oficinaId;
	private Long idToReporte;
	private Long idAjustador;
	private List<AjustadorEstatusDTO> listaAjustadores = new ArrayList<AjustadorEstatusDTO>();
	private ReporteCabina reporte;
	private LugarSiniestroMidas lugarAtencionOcurrido;
	private String latitud;
	private String longitud;
	private Map<String, String> zonas;
	private Map<String,String> tipoCarreteras;
	private String direccionSiniestro;	
	private List<ReporteEstatusDTO> listaReportes = new ArrayList<AjustadorEstatusService.ReporteEstatusDTO>();
	private ReporteEstatusDTO reporteCabina; 	
	private Coordenadas coordenadasOficina;
	private MensajeDTO objetoMensaje;
	private AjustadorEstatusDTO ajustador;
	private String tipoLugarSiniestro;
	

// Combos 
	private Map<Long,String> oficinasActivas;
	
// Servicios
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("ajustadorEstatusServiceEJB")
	private AjustadorEstatusService ajustadorEstatusService;
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Override
	public void prepare(){
		if( mostrarOficinas ){
			this.oficinasActivas = listadoService.obtenerOficinasSiniestros();
		}
		if( mostrarInformacionReporte && SystemCommonUtils.isNotNullNorZero(idToReporte)){
				this.reporte = reporteCabinaService.buscarReporte(idToReporte);
				direccionSiniestro = ajustadorEstatusService.obtenerStringDireccion(reporte.getLugarAtencion());
				lugarAtencionOcurrido = reporteCabinaService.mostrarLugarSiniestro( idToReporte, TipoLugar.AT );
				if( lugarAtencionOcurrido != null && lugarAtencionOcurrido.getCoordenadas() != null ){
					this.latitud = lugarAtencionOcurrido.getCoordenadas().getLatitud().toString();
					this.longitud = lugarAtencionOcurrido.getCoordenadas().getLongitud().toString();
				}
		}
	}
	
	@Action(value = "mostrarContenedorMapa", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/contenedorMapa.jsp"))
	public String mostrarContenedorMapa(){
		mostrarListadoAjustadores = true;
		return SUCCESS;
	}
	
	@Action(value="cargarUbicacionAjustadores",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaAjustadores.*"})})
	public String cargarUbicacionAjustadores(){
		listaAjustadores = ajustadorEstatusService.obtenerListaAjustadorEstatusDTO(oficinaId);
		return SUCCESS;
	}
	
	@Action(value="cargarUbicacionAjustadoresPorOficina",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaAjustadores.*"})})
	public String cargarUbicacionAjustadoresPorOficina(){
		listaAjustadores = ajustadorEstatusService.obtenerAjustadoresPorOficina(oficinaId);
		if(listaAjustadores != null && listaAjustadores.size() > 0 ){
			mostrarListadoAjustadores = true;
		}
		return SUCCESS;
	}
	
	@Action(value="cargarHistoricoAjustador",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaAjustadores.*"})})
	public String cargarHistoricoAjustador(){
		LocalDate today = LocalDate.now();
		listaAjustadores = ajustadorEstatusService.obtenerHistoricoPorAjustador(idAjustador, today.toDate());
		return SUCCESS;
	}

	@Action(value="obtenerReportesPorAjustador",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaReportes.*"})})
	public String obtenerReportesPorAjustador(){		
		listaReportes = ajustadorEstatusService.obtenerResumenReportesAsignados(idAjustador, idToReporte);
		return SUCCESS;
	}
	
	@Action(value="obtenerReportesPorOficina",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaReportes.*"})})
	public String obtenerReportesPorOficina(){		
		listaReportes = ajustadorEstatusService.obtenerResumenReportes(oficinaId);
		return SUCCESS;
	}

	@Action(value="obtenerAjustadoresParaCalcularRuta",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaAjustadores.*"})})
	public String obtenerAjustadoresParaCalcularRuta(){
		listaAjustadores = ajustadorEstatusService.obtenerAjustadoresParaCalcularRuta( idToReporte , oficinaId );

		return SUCCESS;
	}


	@Action(value = "mostrarMapaAjustadoresSiniestro", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/mapaAjustadoresSiniestro.jsp"))
	public String mostrarMapaAjustadoresSiniestro(){
		oficinaId = reporte.getOficina().getId();
		return SUCCESS;
	}
	
	@Action(value="obtenerInformacionReporte",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","root","reporteCabina"})})
	public String obtenerInformacionReporte(){		
		reporteCabina = ajustadorEstatusService.obtenerInformacionReporte(idToReporte);	
		return SUCCESS;
	}
	
	@Action(value = "obtenerCoordenadasOficina", results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","root","coordenadasOficina"})})
	public String obtenerCoordenadasOficina(){
		Oficina oficina = entidadService.findById(Oficina.class, oficinaId);
		if(oficina != null){
			coordenadasOficina = oficina.getCoordenadas();
		}
		return SUCCESS;
	}
	
	@Action(value="eliminarHistorialAjustadores",results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","root","objetoMensaje"})})
	public String eliminarHistorialAjustadores(){
		objetoMensaje = ajustadorEstatusService.eliminarHistoricoAjustadores(null);
		return SUCCESS;
	}
	
	
	@Action(value = "obtenerCoordenadasContactoAtencion", results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","root","ajustador"})})
	public String obtenerCoordenadasContactoAtencion(){
		//default TipoLugar.AT
		LugarSiniestroMidas.TipoLugar tipoLugar = LugarSiniestroMidas.TipoLugar.AT;
		if(!StringUtil.isEmpty(tipoLugarSiniestro) && tipoLugarSiniestro.equals(LugarSiniestroMidas.TipoLugar.OC.toString())){
			tipoLugar = LugarSiniestroMidas.TipoLugar.OC;
		}
		ajustador = ajustadorEstatusService.obtenerAjustadorContactoMovil(idToReporte, tipoLugar);
		return SUCCESS;
	}

	public boolean isMostrarOficinas() {
		return mostrarOficinas;
	}

	public void setMostrarOficinas(boolean mostrarOficinas) {
		this.mostrarOficinas = mostrarOficinas;
	}

	public boolean isMostrarListadoAjustadores() {
		return mostrarListadoAjustadores;
	}

	public void setMostrarListadoAjustadores(boolean mostrarListadoAjustadores) {
		this.mostrarListadoAjustadores = mostrarListadoAjustadores;
	}

	public boolean isMostrarInformacionReporte() {
		return mostrarInformacionReporte;
	}

	public void setMostrarInformacionReporte(boolean mostrarInformacionReporte) {
		this.mostrarInformacionReporte = mostrarInformacionReporte;
	}

	public Map<Long, String> getOficinasActivas() {
		return oficinasActivas;
	}

	public void setOficinasActivas(Map<Long, String> oficinasActivas) {
		this.oficinasActivas = oficinasActivas;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Long getOficinaId() {
		return oficinaId;
	}

	public void setOficinaId(Long oficinaId) {
		this.oficinaId = oficinaId;
	}

	public AjustadorEstatusService getAjustadorEstatusService() {
		return ajustadorEstatusService;
	}

	public void setAjustadorEstatusService(
			AjustadorEstatusService ajustadorEstatusService) {
		this.ajustadorEstatusService = ajustadorEstatusService;
	}

	public List<AjustadorEstatusDTO> getListaAjustadores() {
		return listaAjustadores;
	}

	public void setListaAjustadores(List<AjustadorEstatusDTO> listaAjustadores) {
		this.listaAjustadores = listaAjustadores;
	}

	public ReporteCabina getReporte() {
		return reporte;
	}

	public void setReporte(ReporteCabina reporte) {
		this.reporte = reporte;
	}

	public Long getIdToReporte() {
		return idToReporte;
	}

	public void setIdToReporte(Long idToReporte) {
		this.idToReporte = idToReporte;
	}

	public LugarSiniestroMidas getLugarAtencionOcurrido() {
		return lugarAtencionOcurrido;
	}

	public void setLugarAtencionOcurrido(LugarSiniestroMidas lugarAtencionOcurrido) {
		this.lugarAtencionOcurrido = lugarAtencionOcurrido;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public Map<String, String> getZonas() {
		return zonas;
	}

	public void setZonas(Map<String, String> zonas) {
		this.zonas = zonas;
	}

	public Map<String, String> getTipoCarreteras() {
		return tipoCarreteras;
	}

	public void setTipoCarreteras(Map<String, String> tipoCarreteras) {
		this.tipoCarreteras = tipoCarreteras;
	}

	public Long getIdAjustador() {
		return idAjustador;
	}

	public void setIdAjustador(Long idAjustador) {
		this.idAjustador = idAjustador;
	}

	public String getDireccionSiniestro() {
		return direccionSiniestro;
	}

	public void setDireccionSiniestro(String direccionSiniestro) {
		this.direccionSiniestro = direccionSiniestro;
	}

	public List<ReporteEstatusDTO> getListaReportes() {
		return listaReportes;
	}

	public void setListaReportes(List<ReporteEstatusDTO> listaReportes) {
		this.listaReportes = listaReportes;
	}

	public ReporteEstatusDTO getReporteCabina() {
		return reporteCabina;
	}

	public void setReporteCabina(ReporteEstatusDTO reporteCabina) {
		this.reporteCabina = reporteCabina;
	}

	public Coordenadas getCoordenadasOficina() {
		return coordenadasOficina;
	}

	public void setCoordenadasOficina(Coordenadas coordenadasOficina) {
		this.coordenadasOficina = coordenadasOficina;
	}

	public MensajeDTO getObjetoMensaje() {
		return objetoMensaje;
	}

	public void setObjetoMensaje(MensajeDTO mensaje) {
		this.objetoMensaje = mensaje;
	}

	public AjustadorEstatusDTO getAjustador() {
		return ajustador;
	}

	public void setAjustador(AjustadorEstatusDTO ajustador) {
		this.ajustador = ajustador;
	}

	public String getTipoLugarSiniestro() {
		return tipoLugarSiniestro;
	}

	public void setTipoLugarSiniestro(String tipoLugarSiniestro) {
		this.tipoLugarSiniestro = tipoLugarSiniestro;
	}

	
}
