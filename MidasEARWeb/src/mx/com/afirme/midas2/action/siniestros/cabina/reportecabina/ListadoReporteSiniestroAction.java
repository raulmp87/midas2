package mx.com.afirme.midas2.action.siniestros.cabina.reportecabina;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;

import mx.com.afirme.midas2.util.ExcelExporter;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/reportecabina")
public class ListadoReporteSiniestroAction  extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ReporteSiniestroDTO reporteSiniestroDTO;
	private List<ReporteSiniestroDTO> reportesSiniestro = new ArrayList<ReporteSiniestroDTO>();
	private Map<Long,String> oficinasActivas;
	private Map<String,String> estatusReporte;
	private boolean modoAsignar;
	private Map<String,String> tipoServicio;
	public TransporteImpresionDTO transporte;
	
	
	private static final String CONTENEDOR_BUSQUEDA_RECUPERACIONES = "/jsp/siniestros/cabina/reportecabina/contenedorBusquedaAsignacionReporteSiniestro.jsp";
	private static final String CONTENEDOR_BUSQUEDA_RECUPERACIONES_GRID = "/jsp/siniestros/cabina/reportecabina/busquedaAsignacionReporteSiniestroGrid.jsp";
	private static final String[] COLUMNAS_GRID_REPORTE_CABINA = {"oficina.nombreOficina", "reporteCabina.fechaHoraReporte", "numSiniestroCabina", "numReporteCabina", "numPoliza", "estatus", "incisoReporteCabina.nombreAsegurado", "reporteCabina.personaReporta", "reporteCabina.personaConductor", "autoIncisoReporteCabina.numeroSerie"};	
	
	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	
	@Override
	public void prepare(){
		this.oficinasActivas = listadoService.obtenerOficinasSiniestros();
		this.estatusReporte = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_REPORTE_CABINA);
	}
	
	@Action(value = "mostrarListadoReportes", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/listadoReporteSiniestro.jsp"))
	public String mostrarListadoReportes(){
		return SUCCESS;
	}
	
	@Action(value = "buscarReportes", results = @Result(name = SUCCESS, location = "/jsp/siniestros/cabina/reportecabina/listadoReporteSiniestroGrid.jsp"))
	public String buscarReportes(){
		if(reporteSiniestroDTO != null){
			reporteSiniestroDTO.setNombrePersona(StringUtil.decodeUri(reporteSiniestroDTO.getNombrePersona()));
			reporteSiniestroDTO.setNombreAsegurado(StringUtil.decodeUri(reporteSiniestroDTO.getNombreAsegurado()));
			reporteSiniestroDTO.setNombreConductor(StringUtil.decodeUri(reporteSiniestroDTO.getNombreConductor()));
			
			if(super.getCount() == null){
				super.setCount(20); 
			}
						
			if(super.getPosStart() == null){
				super.setPosStart(0);
			}			
			reporteSiniestroDTO.setPosStart(super.getPosStart());
			reporteSiniestroDTO.setCount(super.getCount());
			reporteSiniestroDTO.setOrderByAttribute(decodeOrderBy());
			if(super.getPosStart().intValue() == 0){
				super.setTotalCount(reporteCabinaService.contarReporteSiniestro(reporteSiniestroDTO));
			}		
			reportesSiniestro = reporteCabinaService.buscarReportes(reporteSiniestroDTO);						
		}
		return SUCCESS;
	}
	
	/**
	 * Metodo para exportar en excel el listado de reportes de siniestro
	 * @return
	 */
	@Action(value="exportarExcelReporteSiniestro",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
							"inputName","transporte.genericInputStream",
							"contentDisposition","attachment;filename=\"${transporte.fileName}\""}),
			@Result(name="EMPTY", location= ERROR),			
			@Result(name = INPUT, location = "/jsp/errorImpresiones.jsp", 
					params = {"mensaje", "${mensaje}" })
			
			})
	public String exportarExcelReporteSiniestro(){
		if(reporteSiniestroDTO != null){		
			try{
				
				reporteSiniestroDTO.setNombrePersona(StringUtil.decodeUri(reporteSiniestroDTO.getNombrePersona()));
				reporteSiniestroDTO.setNombreAsegurado(StringUtil.decodeUri(reporteSiniestroDTO.getNombreAsegurado()));
				reporteSiniestroDTO.setNombreConductor(StringUtil.decodeUri(reporteSiniestroDTO.getNombreConductor()));
				reportesSiniestro = reporteCabinaService.buscarReportes(reporteSiniestroDTO);						
				
				ExcelExporter exporter = new ExcelExporter(ReporteSiniestroDTO.class);	
				
				transporte = exporter.exportXLS(reportesSiniestro, "Listado de reportes de Siniestros");
				
			}catch(Exception e){
				setMensaje(e.getMessage());
				return INPUT;
			}
		}
		else{
			return "EMPTY";
		}
		return SUCCESS;
	}
	
	private String decodeOrderBy(){
		String order = null;
		if(!StringUtil.isEmpty(super.getOrderBy())){
			order = COLUMNAS_GRID_REPORTE_CABINA[Integer.valueOf(super.getOrderBy())];
			if(!StringUtil.isEmpty(super.getDirect())){
				order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
			}
		}		
		return order;
	}
	
	@Action(value = "buscarReportesAsignacion", results = @Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_RECUPERACIONES_GRID))
	public String buscarReportesAsignacion(){
		if(reporteSiniestroDTO != null){
			reporteSiniestroDTO.setSiniestrado(true);
			reporteSiniestroDTO.setNombrePersona(StringUtil.decodeUri(reporteSiniestroDTO.getNombrePersona()));
			reporteSiniestroDTO.setNombreAsegurado(StringUtil.decodeUri(reporteSiniestroDTO.getNombreAsegurado()));
			reporteSiniestroDTO.setNombreConductor(StringUtil.decodeUri(reporteSiniestroDTO.getNombreConductor()));
			reportesSiniestro = reporteCabinaService.buscarReportes(reporteSiniestroDTO);
		}
		return SUCCESS;
	}
	
	public void prepareMostrarBuscarReporteRecuperacion(){
		this.tipoServicio = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_SERVICIO);
	}
	
	@Action(value = "mostrarBuscarReporteRecuperacion", 
			results = @Result(name = SUCCESS, location = CONTENEDOR_BUSQUEDA_RECUPERACIONES)
			)
	public String mostrarBuscarReporteRecuperacion(){
		return SUCCESS;
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public ReporteSiniestroDTO getReporteSiniestroDTO() {
		return reporteSiniestroDTO;
	}

	public void setReporteSiniestroDTO(ReporteSiniestroDTO reporteSiniestroDTO) {
		this.reporteSiniestroDTO = reporteSiniestroDTO;
	}

	public List<ReporteSiniestroDTO> getReportesSiniestro() {
		return reportesSiniestro;
	}

	public void setReportesSiniestro(List<ReporteSiniestroDTO> reportesSiniestro) {
		this.reportesSiniestro = reportesSiniestro;
	}

	public Map<Long, String> getOficinasActivas() {
		return oficinasActivas;
	}

	public void setOficinasActivas(Map<Long, String> oficinasActivas) {
		this.oficinasActivas = oficinasActivas;
	}

	public Map<String, String> getEstatusReporte() {
		return estatusReporte;
	}

	public void setEstatusReporte(Map<String, String> estatusReporte) {
		this.estatusReporte = estatusReporte;
	}

	public boolean isModoAsignar() {
		return modoAsignar;
	}

	public void setModoAsignar(boolean modoAsignar) {
		this.modoAsignar = modoAsignar;
	}

	public Map<String, String> getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(Map<String, String> tipoServicio) {
		this.tipoServicio = tipoServicio;
	}		
	
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}		
		
}