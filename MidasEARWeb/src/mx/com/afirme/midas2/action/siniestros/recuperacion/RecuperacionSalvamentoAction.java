package mx.com.afirme.midas2.action.siniestros.recuperacion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.IndicadoresVentaDTO;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("prototype")
@Namespace("/siniestros/recuperacion/recuperacionSalvamento")
public class RecuperacionSalvamentoAction extends RecuperacionAction {

	private static final long serialVersionUID = 4241753553022419614L;
	
	private static final Logger log = Logger.getLogger(RecuperacionSalvamentoAction.class);
	
	private final String 	SALVAMENTO_NAMESPACE = "/siniestros/recuperacion/recuperacionSalvamento";
	
	private final String  	ACTION_MOSTRAR_CONTENEDOR             = "mostrarContenedorRecuperacionSalvamento";
	private final String	ACTION_MOSTRAR_CANCELACION            = "mostrarCancelarSalvamento";
	private final String	CONTENEDOR_RECUPERACION_SVM	          =	"/jsp/siniestros/recuperacion/salvamento/contenedorRecuperacionSalvamento.jsp";
	private final String	VENTANA_CANCELAR_RECUPERACION_SVM     = "/jsp/siniestros/recuperacion/salvamento/ventanaCancelacionSalvamento.jsp";
	private final String 	REFERENCIA_BANCARIA_GRID	          =	"/jsp/siniestros/recuperacion/referenciaBancariaGrid.jsp";
	private final String    MENSAJE_ERROR_SALVAR_VENTA_SALVAMENTO = "Es necesario ingresar primero la fecha inicio subasta";
	private final String    MENSAJE_ERROR_SALVAR_PRORROGA         = "Es necesaio contar con una venta de salvamento activa";
	private final String	HISTORIAL_SUBASTA_GRID                = "/jsp/siniestros/recuperacion/salvamento/historialSubastaSalvamentoGrid.jsp";
	
	private RecuperacionSalvamento recuperacion = new RecuperacionSalvamento();
	private Map<String, String> estados = new LinkedHashMap<String, String>();
	private Map<String, String> ciudades = new LinkedHashMap<String, String>();
	
	private VentaSalvamento          ventaSalvamento;
	List<ReferenciaBancariaDTO>      lstReferenciasBancarias;
	private boolean                  puedeSolicitarProrroga;
	private boolean                  puedeAutorizarProrroga;
	private String                   mensajeErrorVentaSalvamento;
	private IndicadoresVentaDTO      indicadores;
	private String                   correoComprador;
	private Long                     prestadorServicioId;
	private Map<Long,String>         ctgCompradores;
	private Map<String,String>       ctgAutorizarSubasta;
	private Map<Long,String>         ctgBancos = new HashMap<Long,String>();
	private String 					 usuarioCancelacion;
	private Date					 fechaCancelacion;
	private List<VentaSalvamento>    listadoVentaSalvamento;
	private String                   seleccionAutorizaSubasta;
	private boolean 				 isUsuarioValidoAprobarProrroga;

	
	@Autowired
	@Qualifier("recuperacionSalvamentoServiceEJB")
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	

	public void prepareMostrarContenedorRecuperacionSalvamento(){
		this.initDatosPantallaSalvamento();
	}
	
	/**
	 * Mostrar contenedor y cargar recuperacion salvamento
	 * @return
	 */
	@Action(value="mostrarContenedorRecuperacionSalvamento",results={
			@Result(name=SUCCESS,location=CONTENEDOR_RECUPERACION_SVM),
			@Result(name=INPUT,location=CONTENEDOR_RECUPERACION_SVM)
			})
	public String mostrarContenedorRecuperacionSalvamento(){
		return SUCCESS;		
	}

	/**
	 * Actualizar salvamento
	 * @return
	 */
	@Action(value="salvarSalvamento",results={
			@Result(name=INPUT,type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) 	
			})
	public String salvarSalvamento(){
		try{	
			
			if( this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(this.recuperacionId) == null ){

				this.recuperacionSalvamentoService.guardaSalvamentoConProvision(this.recuperacionId, this.recuperacion);

				super.setMensajeExito();
			}else{
				log.error("Existe una venta activa, no se puede actualizar la fecha de subasta");
				super.setMensajeError("Existe una venta activa, no se puede actualizar la fecha de subasta");
				return INPUT;
			}			
		}catch(Exception ex){
			log.error(ex);
			super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(ex.getMessage() != null ? 
					ex.getMessage() : ""));
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	/**
	 * Mostrar ventana de cancelacion derecuperacion de salvamento
	 * @return
	 */
	@Action(value="mostrarCancelarSalvamento",results={
			@Result(name=SUCCESS,location=VENTANA_CANCELAR_RECUPERACION_SVM),
			@Result(name=INPUT,location=CONTENEDOR_RECUPERACION_SVM)
			})	
	public String mostrarCancelarSalvamento(){
		recuperacion = recuperacionSalvamentoService.obtenerRecuperacionPorId(recuperacionId);		
		return SUCCESS;		
	}
	
	/**
	 * Cancelar una recuperacion de salvamento
	 * @return
	 */
	@Action(value="cancelarSalvamento",results={		
			@Result(name=INPUT,location=CONTENEDOR_RECUPERACION_SVM),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) 				
			})	
	public String cancelarSalvamento(){
		try{
			recuperacionSalvamentoService.cancelarRecuperacion(recuperacion.getId(), recuperacion.getMotivoCancelacion());
			super.setMensajeExito();			
		}catch(Exception ex){
			log.error(ex);
			super.setMensajeError(MENSAJE_ERROR_GENERAL + ": " + ex.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	@Action(value="guardarVentaDeSalvamento",results={
			@Result(name=INPUT,type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) 
	})
	public String guardarVentaDeSalvamento(){

		RecuperacionSalvamento recu = this.recuperacionSalvamentoService.obtenerRecuperacionSalvamentoById(this.recuperacionId); //this.entidadService.findById(RecuperacionSalvamento.class, this.recuperacionId);
		
		try{
			if( recu.getFechaInicioSubasta() == null ){
				super.setMensajeError(MENSAJE_ERROR_SALVAR_VENTA_SALVAMENTO);
				return INPUT;
			}else{
				
				if( this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(this.recuperacionId) == null && recu.getFechaInicioSubasta() !=null ){
					recuperacionSalvamentoService.guardarVentaSalvamento(this.recuperacionId,ventaSalvamento);
					super.setMensajeExito();
				}else{
					recuperacionSalvamentoService.editarVentaSalvamento(this.recuperacionId, ventaSalvamento);
					super.setMensajeExito();
				}
				
				return SUCCESS;
			}
		}catch(Exception e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(e.getMessage() != null ? 
					e.getMessage() : ""));
			return INPUT;
		}
		
		
	}
	
	
	@Action(value="mostrarGridReferencias",results={
			@Result(name=SUCCESS,location=REFERENCIA_BANCARIA_GRID),
			@Result(name=INPUT,location=REFERENCIA_BANCARIA_GRID)
	})	
	public String mostrarGridReferencias(){
		
		if( this.recuperacion != null ){
			this.lstReferenciasBancarias = this.recuperacionService.obtenerReferenciasBancaria(this.recuperacionId);
		}
		
		return SUCCESS;
	}
	
	
	@Action(value="obtenerCorreoDelComprador", results = {
		    @Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^correoComprador"}),
		    @Result(name=INPUT, type="json")
	})
	public String obtenerCorreoDelComprador(){
		
		PrestadorServicio prestador = this.entidadService.findById(PrestadorServicio.class, Integer.parseInt( this.prestadorServicioId.toString() ) );
		
		if( prestador != null ){
			this.correoComprador = prestador.getPersonaMidas().getContacto().getCorreoPrincipal();
			if( StringUtils.isEmpty(this.correoComprador) ){
				this.correoComprador = "";
			}
		}else{
			this.correoComprador = "";
		}
		
		return SUCCESS;
	}	
	
	
	@Action(value="cancelarAdjudicacion",results={
			@Result(name=INPUT,type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) 	
	})
	public String cancelarAdjudicacion(){
		
		try{
			this.recuperacionSalvamentoService.cancelarAdjudicacion(this.recuperacionId,this.ventaSalvamento.getComentarioCancelacion(), true);
			this.soloConsulta = false;
			super.setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_GENERAL.concat(": ").concat(ex.getMessage() != null ? 
					ex.getMessage() : ""));
			return INPUT;
		}
		return SUCCESS;
	}	
	
	private void mostrarTotalesVentaSalvamento(){
		
		ventaSalvamento = this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(this.recuperacionId);
		
		if( ventaSalvamento == null ){
			RecuperacionSalvamento recuperacionSalvamento = this.recuperacionSalvamentoService.obtenerRecuperacionPorId(this.recuperacionId);
			// PRECARGA DE IVA,PORCENTAJE,SUBTOTAL
			if( recuperacionSalvamento != null ){
				
				ventaSalvamento          = new VentaSalvamento();
				
				BigDecimal valorEstimado =  recuperacionSalvamento.getValorEstimado();
				String sIva              = this.parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.IVA_VENTA_SALVAMENTO);
				BigDecimal iva           = new BigDecimal(sIva); 
				
				ventaSalvamento.setTotalVenta(valorEstimado);
				ventaSalvamento.setPorcentajeIva(Integer.parseInt( iva.toString()));
				BigDecimal tmpBaseIva = iva.divide(new BigDecimal(100));
				BigDecimal tmpIva = tmpBaseIva.add(BigDecimal.ONE);
				
				BigDecimal tmpValEstimado = valorEstimado.divide(tmpIva,2,RoundingMode.CEILING);
				
				ventaSalvamento.setIva( tmpValEstimado.multiply(tmpBaseIva) );
				ventaSalvamento.setSubtotal( tmpValEstimado ); // valorEstimado.subtract(ventaSalvamento.getSubtotal())
				
				// # MOSTRAR EN 0 LA CANTIDAD DE SUBASTA
				recuperacion.setSubastaActual(this.recuperacion.getSubastaActual() == null ? 0:this.recuperacion.getSubastaActual());
				tieneVentaActiva = "n";
				esEdicionVentaRecuperacion = Boolean.FALSE;
				
			}
		}else{
			tieneVentaActiva = "s";
			esEdicionVentaRecuperacion = Boolean.TRUE;
			
		}
		
	}
	
	@Action(value="guardarProrroga",results={
			@Result(name=INPUT,type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) 	
	})
	public String guardarProrroga(){
		
		//if( this.tieneVentaActiva.equals("n") ){
		if( this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(this.recuperacionId) == null ){
			super.setMensajeError(MENSAJE_ERROR_SALVAR_PRORROGA);
		}else{
			// CREA LA PRORROGA
			if( StringUtils.isEmpty( seleccionAutorizaSubasta )  ){
				this.recuperacionSalvamentoService.solicitarProrroga(recuperacionId, recuperacion.getFechaLimiteProrroga(), recuperacion.getComentariosProrroga() );
				super.setMensajeExito();
			}else{
				// VALIDA NO ESTE AUTORIZADA LA PRORROGA
				if( !recuperacion.isAutorizacionProrroga() && this.recuperacionSalvamentoService.esUsuarioValidoParaAutorizarProrroga( this.usuarioService.getUsuarioActual() ) ){
					// AUTORIZA LA PRORROGA
					if( recuperacion.isSolicitudProrroga() && !StringUtils.isEmpty( seleccionAutorizaSubasta ) ){
						this.recuperacionSalvamentoService.autorizarProrroga(recuperacionId,seleccionAutorizaSubasta,recuperacion.getFechaLimiteProrroga(), recuperacion.getComentariosProrroga());
						super.setMensajeExito();
					}
				}else{
					super.setMensajeError("No se puede modificar la prorroga ya que esta se encuentra autorizada y/o el usuario no tiene permisos suficientes.");
					return INPUT;
				}
			}
		}
		
		return SUCCESS;
	}
	
	@Action(value="mostrarHistorialDeSubastasGrid",results={
			@Result(name=SUCCESS,location=HISTORIAL_SUBASTA_GRID),
			@Result(name=INPUT  ,location=HISTORIAL_SUBASTA_GRID)
	})	
	public String mostrarHistorialDeSubastasGrid(){
		this.listadoVentaSalvamento =  this.recuperacionSalvamentoService.obtenerVentasPorRecuperacion(this.recuperacionId);
		return SUCCESS;
	}
	
	@Action(value="guardarConfirmacionDeIngreso",results={
			@Result(name=INPUT,type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", ACTION_MOSTRAR_CONTENEDOR, "namespace", SALVAMENTO_NAMESPACE,
					"recuperacionId", "${recuperacionId}", "soloConsulta", "${soloConsulta}",					
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) 	
	})
	public String guardarConfirmacionDeIngreso(){
		try{
			
				this.recuperacionSalvamentoService.confirmarIngreso(
						this.recuperacionId,
						this.recuperacion.getBancoConfirmacionIngreso(),
						this.recuperacion.getCuentaConfirmacionIngreso(),
						this.recuperacion.getFechaDepositoConfirmacionIngreso(),
						this.recuperacion.getMontoConfirmacionIngreso()
				);
				super.setMensajeExito();
				return SUCCESS;
			
		}catch(Exception e){
			super.setMensajeError("Error al generar la confirmación del ingreso");
			return INPUT;
		}
		
	}
	
	private void initCombosVentaSalvamento(){
		this.ctgCompradores = recuperacionSalvamentoService.obtenerCompradores();
		estados = listadoService.getMapEstadosPorPaisMidas(PAIS_DEFAULT);
		this.ctgAutorizarSubasta = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.RESPUESTA_SIMPLE);
	}
	
	private void cargarDetalleRecuperacion(RecuperacionSalvamento recuperacion){
		setPropiedadesGenerales(recuperacion);	
		estados = listadoService.getMapEstadosPorPaisMidas(PAIS_DEFAULT);
		if(recuperacion != null && recuperacion.getCiudad() != null && recuperacion.getCiudad().getEstado() != null){
			ciudades = listadoService.getMapMunicipiosPorEstadoMidas(recuperacion.getCiudad().getEstado().getId());
		}else{
			ciudades = listadoService.getMapMunicipiosPorEstadoMidas(
					(entidadService.findById(CiudadMidas.class, recuperacion.getCiudad().getId())).getEstado().getId());
		}			
	}	
	
	private void initDatosPantallaSalvamento(){
		
		recuperacion = recuperacionSalvamentoService.obtenerRecuperacionPorId(recuperacionId);
		this.tieneVentaActiva = (this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(recuperacionId) != null ? "s":"n" );
		this.cargarDetalleRecuperacion(recuperacion);
		this.initCombosVentaSalvamento();
		this.mostrarTotalesVentaSalvamento();
		this.isUsuarioValidoAprobarProrroga = this.recuperacionSalvamentoService.esUsuarioValidoParaAutorizarProrroga(this.usuarioService.getUsuarioActual());
		
		this.ctgBancos = this.listadoService.getCtgBancosValidosSalvamentos();
		this.mostrarIndicadores();
		
		// INICIALIZA DATOS DE CONTROL DE USUARIO Y FECHA PARA LAS PESTAÑAS
		this.recuperacion.setConfirmadoPor            ( ( StringUtil.isEmpty( this.recuperacion.getConfirmadoPor() ) ? this.usuarioService.getUsuarioActual().getNombreUsuario() : this.recuperacion.getConfirmadoPor() ) );
		this.recuperacion.setFechaDepositoConfirmacion(  this.recuperacion.getFechaDepositoConfirmacion() == null ? new Date() : this.recuperacion.getFechaDepositoConfirmacion()  );
		this.recuperacion.setSolicitanteProrroga      ( ( StringUtil.isEmpty( this.recuperacion.getSolicitanteProrroga() ) ? this.usuarioService.getUsuarioActual().getNombreUsuario() : this.recuperacion.getSolicitanteProrroga() ) );
		this.recuperacion.setFechaSolicitudProrroga   ( this.recuperacion.getFechaSolicitudProrroga() == null ? new Date() : this.recuperacion.getFechaSolicitudProrroga());
		this.recuperacion.setSubastaActual            ( this.recuperacion.getSubastaActual() == null ? 0:this.recuperacion.getSubastaActual() );
		this.usuarioCancelacion = this.usuarioService.getUsuarioActual().getNombreUsuario();
		this.fechaCancelacion   = new Date();
	}
	
	
	public void mostrarIndicadores(){
		
		indicadores = this.recuperacionSalvamentoService.obtenerIndicadoresVenta(this.recuperacionId);
		
	}
	
	public RecuperacionSalvamento getRecuperacion() {
		return recuperacion;
	}

	public void setRecuperacion(RecuperacionSalvamento recuperacion) {
		this.recuperacion = recuperacion;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public Map<String, String> getCiudades() {
		return ciudades;
	}

	public void setCiudades(Map<String, String> ciudades) {
		this.ciudades = ciudades;
	}

	public VentaSalvamento getVentaSalvamento() {
		return ventaSalvamento;
	}

	public void setVentaSalvamento(VentaSalvamento ventaSalvamento) {
		this.ventaSalvamento = ventaSalvamento;
	}

	public boolean isPuedeSolicitarProrroga() {
		return puedeSolicitarProrroga;
	}

	public void setPuedeSolicitarProrroga(boolean puedeSolicitarProrroga) {
		this.puedeSolicitarProrroga = puedeSolicitarProrroga;
	}

	public boolean isPuedeAutorizarProrroga() {
		return puedeAutorizarProrroga;
	}

	public void setPuedeAutorizarProrroga(boolean puedeAutorizarProrroga) {
		this.puedeAutorizarProrroga = puedeAutorizarProrroga;
	}

	public String getMensajeErrorVentaSalvamento() {
		return mensajeErrorVentaSalvamento;
	}

	public void setMensajeErrorVentaSalvamento(String mensajeErrorVentaSalvamento) {
		this.mensajeErrorVentaSalvamento = mensajeErrorVentaSalvamento;
	}

	public IndicadoresVentaDTO getIndicadores() {
		return indicadores;
	}

	public void setIndicadores(IndicadoresVentaDTO indicadores) {
		this.indicadores = indicadores;
	}

	public String getCorreoComprador() {
		return correoComprador;
	}

	public void setCorreoComprador(String correoComprador) {
		this.correoComprador = correoComprador;
	}

	public Map<Long, String> getCtgCompradores() {
		return ctgCompradores;
	}

	public void setCtgCompradores(Map<Long, String> ctgCompradores) {
		this.ctgCompradores = ctgCompradores;
	}

	public Long getPrestadorServicioId() {
		return prestadorServicioId;
	}

	public void setPrestadorServicioId(Long prestadorServicioId) {
		this.prestadorServicioId = prestadorServicioId;
	}

	public List<ReferenciaBancariaDTO> getLstReferenciasBancarias() {
		return lstReferenciasBancarias;
	}

	public void setLstReferenciasBancarias(
			List<ReferenciaBancariaDTO> lstReferenciasBancarias) {
		this.lstReferenciasBancarias = lstReferenciasBancarias;
	}

	public String getUsuarioCancelacion() {
		return usuarioCancelacion;
	}

	public void setUsuarioCancelacion(String usuarioCancelacion) {
		this.usuarioCancelacion = usuarioCancelacion;
	}

	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}

	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}

	public List<VentaSalvamento> getListadoVentaSalvamento() {
		return listadoVentaSalvamento;
	}

	public void setListadoVentaSalvamento(
			List<VentaSalvamento> listadoVentaSalvamento) {
		this.listadoVentaSalvamento = listadoVentaSalvamento;
	}

	public Map<String, String> getCtgAutorizarSubasta() {
		return ctgAutorizarSubasta;
	}

	public void setCtgAutorizarSubasta(Map<String, String> ctgAutorizarSubasta) {
		this.ctgAutorizarSubasta = ctgAutorizarSubasta;
	}

	public String getSeleccionAutorizaSubasta() {
		return seleccionAutorizaSubasta;
	}

	public void setSeleccionAutorizaSubasta(String seleccionAutorizaSubasta) {
		this.seleccionAutorizaSubasta = seleccionAutorizaSubasta;
	}

	public boolean isUsuarioValidoAprobarProrroga() {
		return isUsuarioValidoAprobarProrroga;
	}

	public void setUsuarioValidoAprobarProrroga(
			boolean isUsuarioValidoAprobarProrroga) {
		this.isUsuarioValidoAprobarProrroga = isUsuarioValidoAprobarProrroga;
	}

	public Map<Long, String> getCtgBancos() {
		return ctgBancos;
	}

	public void setCtgBancos(Map<Long, String> ctgBancos) {
		this.ctgBancos = ctgBancos;
	}



}
