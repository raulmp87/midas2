package mx.com.afirme.midas2.action.siniestros.indemnizacion.perdidatotal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value="/siniestros/indemnizacion/perdidatotal")
public class PerdidaTotalAction extends BaseAction implements Preparable{

	private static final String	LOCATION_CONTENEDORDETERMINARPT_JSP	= "/jsp/siniestros/indemnizacion/contenedorDeterminarPT.jsp";
	private static final String	LOCATION_CONTENEDORAUTORIZACIONPT_JSP	= "/jsp/siniestros/indemnizacion/contenedorAutorizacionPT.jsp";
	private static final long serialVersionUID = -8388411657332862871L;
	private static final String MENSAJE_ERROR_BUSQUEDA = "No se encontraron resultados";
	
	private List<PerdidaTotalFiltro> listaPerdidasTotales;
	private List<IndemnizacionAutorizacion> listaAutorizaciones;
	private PerdidaTotalFiltro filtroPT;
//	private PerdidaTotalFiltro filtroPTSp;
	
	private Map<Long, String> 	oficinasActivas;
	private Map<String, String> tiposSiniestro;
	private Map<String, String> etapasPerdidaTotal;
	private Map<String, String> indemnizacionCoberturas;
	private Map<String, String> estatusIndemnizacion;
	
	private TransporteImpresionDTO transporte;
	
	//AUTORIZACION PT
	private Long idOrdenCompra;
	private IndemnizacionSiniestro indemnizacion;
	private String tipoAutorizacion;
	private String numeroSiniestro;
	private Date fechaAutorizacion;
	private static final String ESTATUS_ACEPTADO = "A";
	private static final String ESTATUS_RECHAZADO = "R";
	
	private String 	token;
	private String 	userName;
	//DETERMINACION PT
	private Long 	idIndemnizacion;
	private DeterminacionInformacionSiniestroDTO informacionSiniestro;
	private Date 	fechaDeterminacion;
	private String 	tipoSiniestroDesc;
	private String 	tipoSiniestro;
	private Boolean esRecepcionPagoDanios;
	
	//CARTA FINIQUITO
	private Boolean esFiniquitoAsegurado;
	private String metodoInvocar;
	private String pantallaOrigen;

	
	//CANCELACION
	private String methodName;
	private String namespace;
	
	private Integer esConsulta;
	private Integer guardadoCorrecto;
	private Boolean mostrarListadoVacio;
	
	//CONFIGURACION AUTORIZACION INDEMNIZACION
	private Map<String, String> listaRolesSinAutorizacionFinal;
	private String nombreRolAutorizador;
	
	private static final Integer TRUE = 1;
	private static final Integer FALSE = 0;
	
	private static final long ID_INVALIDO = 0L;
	
	//NOMBRE BENEFICIARIO EDITABLE
	private String nombreBeneficiarioEditable;
	
	

	@Autowired
	@Qualifier("perdidaTotalServiceEJB")
	private PerdidaTotalService perdidaTotalService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("cartaSiniestroServiceEJB")
	private CartaSiniestroService cartaSiniestroService;	
	
	@Override
	public void prepare(){
	}
	
	@Action(value = "mostrarCatalogo", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorPerdidaTotal.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorPerdidaTotal.jsp")})
	public String mostrar(){
		return SUCCESS;
	}
	
	@Action(value = "listar", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/listadoPerdidaTotalGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorPerdidaTotal.jsp")
			})
	public String listar() {
		if(mostrarListadoVacio){
			listaPerdidasTotales = new ArrayList<PerdidaTotalFiltro>();
		}else{
			listaPerdidasTotales = perdidaTotalService.buscarIndemnizaciones(filtroPT);
			if(listaPerdidasTotales == null || listaPerdidasTotales.isEmpty()){
				setMensajeError(MENSAJE_ERROR_BUSQUEDA);
			}
		}
		return SUCCESS;
	}
	
	
	@Action(value = "listarAutorizaciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/listadoAutorizacionIndemnnizacion.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/listadoAutorizacionIndemnnizacion.jsp")
			})
	public String listarAutorizaciones() {
			//tipoAutorizacion.toCharArray();
			idIndemnizacion.toString();
			listaAutorizaciones = perdidaTotalService.listaAutorizaciones(tipoAutorizacion, this.idIndemnizacion);
			/*if(listaPerdidasTotales == null || listaPerdidasTotales.isEmpty()){
				setMensajeError(MENSAJE_ERROR_BUSQUEDA);
			}*/
		
		return SUCCESS;
	}
	
	@Action(value="exportarResultados",results={@Result(name=SUCCESS,type="stream",
			params={"contentType","${transporte.contentType}",
			"inputName","transporte.genericInputStream",
			"contentDisposition",
			"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarResultados(){
		listaPerdidasTotales = perdidaTotalService.buscarIndemnizaciones(filtroPT);
		ExcelExporter exporter = new ExcelExporter(PerdidaTotalFiltro.class);
		transporte = exporter.exportXLS(listaPerdidasTotales, "Listado de Perdidas Totales");
		
		return SUCCESS;
		
	}
	
	@Action(value = "mostrarAutorizar", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORAUTORIZACIONPT_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORAUTORIZACIONPT_JSP)})
	public String mostrarAutorizar(){
		if(idOrdenCompra != null && idOrdenCompra.longValue() > ID_INVALIDO){
			indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestro(idOrdenCompra);
		}
		return SUCCESS;
	}
	
	@Action(value = "autorizar", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORAUTORIZACIONPT_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORAUTORIZACIONPT_JSP)})
	public String autorizar(){
		try{

			perdidaTotalService.autorizarPerdidaTotal(indemnizacion, tipoAutorizacion);
			indemnizacion.setEstatusIndemnizacion(ESTATUS_ACEPTADO);
			indemnizacion.setEstatusPerdidaTotal(ESTATUS_ACEPTADO);
			setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)ex).getMessageClean());
			}	
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "rechazar", results = { 
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORAUTORIZACIONPT_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORAUTORIZACIONPT_JSP)})
	public String rechazar(){
		try{

			perdidaTotalService.rechazarPerdidaTotal(indemnizacion, tipoAutorizacion);
			indemnizacion.setEstatusIndemnizacion(ESTATUS_RECHAZADO);
			indemnizacion.setEstatusPerdidaTotal(ESTATUS_RECHAZADO);
			setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)ex).getMessageClean());
			}	
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarDeterminacionPT", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORDETERMINARPT_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORDETERMINARPT_JSP)})
	public String mostrarDeterminacionPT(){
		if(idIndemnizacion != null && idIndemnizacion.longValue() > ID_INVALIDO){
			informacionSiniestro = perdidaTotalService.obtenerInformacionSiniestro(idIndemnizacion, tipoSiniestro, tipoSiniestroDesc);
			indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestroPorId(idIndemnizacion);
			esFiniquitoAsegurado = cartaSiniestroService.esCoberturaAsegurado(idIndemnizacion);
			esRecepcionPagoDanios = cartaSiniestroService.getEsRecepcionPagoDanios(indemnizacion.getId());
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarDeterminacionPT", results = { 
			@Result(name = INPUT, location = LOCATION_CONTENEDORDETERMINARPT_JSP) ,
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarDeterminacionPT", "namespace", "/siniestros/indemnizacion/perdidatotal",
					"idIndemnizacion", "${idIndemnizacion}", "esConsulta", "${esConsulta}",	
					"tipoSiniestroDesc", "${tipoSiniestroDesc}", "idOrdenCompra", "${idOrdenCompra}",
					"tipoSiniestro", "${tipoSiniestro}", 
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
					})					
	public String guardarDeterminacionPT(){
		try{
			if(esConsulta.equals(FALSE)){
				indemnizacion.setId(idIndemnizacion);
				/*Validar Guia*/
				BigDecimal zero= new BigDecimal("0.0");	
				if(null==indemnizacion.getGuiaNada() || null==indemnizacion.getGuiaKbb() ||
					null==indemnizacion.getGuiaEbc() || null==indemnizacion.getGuiaAutometrica()	){
					this.setMensajeError("En la Sección Guías Utilizadas, debe capturarse por lo menos una Guía ");
				}
				if(indemnizacion.getGuiaNada().compareTo(zero)!=1 && indemnizacion.getGuiaKbb().compareTo(zero)!=1 &&
						indemnizacion.getGuiaEbc().compareTo(zero)!=1 && indemnizacion.getGuiaAutometrica().compareTo(zero)!=1	){
						this.setMensajeError("En la Sección Guías Utilizadas, debe capturarse por lo menos el monto de una Guía");
						guardadoCorrecto = FALSE;
						return INPUT;
				}
				
				if (!nombreBeneficiarioEditable.isEmpty()){
					informacionSiniestro.setBeneficiario(nombreBeneficiarioEditable);
				}
				indemnizacion.setDeducible(informacionSiniestro.getMontoTotalDeducible());
				perdidaTotalService.guardarDeterminacionPT(indemnizacion, tipoSiniestro, informacionSiniestro, nombreBeneficiarioEditable);
				if(null!=idIndemnizacion)
					indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestroPorId(idIndemnizacion);

			}
			guardadoCorrecto = TRUE;
			setMensaje("Se ha guardado correctamente. Recuerde ajustar la reserva para " +
					"que coincida con el total a indemnizar.");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		}catch(Exception ex){
			guardadoCorrecto = FALSE;
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex.getCause()instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)ex).getMessageClean());
			}	
			return INPUT;
		}
		esRecepcionPagoDanios = cartaSiniestroService.getEsRecepcionPagoDanios(indemnizacion.getId());
		return SUCCESS;
	}
	
	@Action(value = "mostrarCancelarIndemnizacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorCancelarIndemnizacion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorCancelarIndemnizacion.jsp")})
	public String mostrarCancelarIndemnizacion(){
		if(idIndemnizacion != null && idIndemnizacion.longValue() > ID_INVALIDO){
			indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestroPorId(idIndemnizacion);
		}
		return SUCCESS;
	}
	
	@Action(value = "cancelarIndemnizacion", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
	                 "actionName", "${methodName}", "namespace", "${namespace}",
	                 "mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
	                 "mostrarListadoVacio", "false",
	                 "filtroPT.idIndemnizacion", "${filtroPT.idIndemnizacion}"}),
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorPerdidaTotal.jsp")})
	public String enviarSolicitudValuacion(){
		try{
			perdidaTotalService.cancelarIndemnizacion(idIndemnizacion, indemnizacion.getMotivoAutPerdidaTotal());
			indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestroPorId(idIndemnizacion);
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
			if (ex.getCause()instanceof NegocioEJBExeption) {
				super.setMensajeError(((NegocioEJBExeption)(ex.getCause())).getMessageClean());
			}	
			return INPUT;
		}
		setMensaje("Indemnizacion Cancelada");
		setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		methodName = "mostrarCatalogo";
		namespace = "/siniestros/indemnizacion/perdidatotal";
		filtroPT.setIdIndemnizacion(indemnizacion.getId());
		return SUCCESS;
	}
	
	@Action(value = "mostrarConfiguracionAusenciaIndemnizacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorConfiguracionAusenciaIndemnziacion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorConfiguracionAusenciaIndemnziacion.jsp")})
	public String mostrarConfiguracionAusenciaIndemnizacion(){
		return SUCCESS;
	}
	
	@Action(value = "guardarConfiguracionAusenciaIndemnizacion", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/contenedorConfiguracionAusenciaIndemnziacion.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/contenedorConfiguracionAusenciaIndemnziacion.jsp")})
	public String guardarConfiguracionAusenciaIndemnizacion(){
		try{
			if(perdidaTotalService.tieneRolPermisoDeAutorizacionFinal()){
				perdidaTotalService.guardarConfiguracionAutorizacionIndemnizacion(nombreRolAutorizador);
			}else{
				setMensajeError("El usuario no tiene permisos para cambiar esta acci\u00F3n");
				return INPUT;
			}
		}catch(Exception ex){
			setMensajeError("Error al guardar la Configuraci\u00F3n de Ausencia para Indemnizaci\u00F3n");
			return INPUT;
		}
		setMensajeExito();
		return SUCCESS;
	}

	@Action(value = "perdidaTotalCFDI", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/indemnizacion/perdidaTotalCFDI.jsp") ,
			@Result(name = INPUT, location = "/jsp/siniestros/indemnizacion/perdidaTotalCFDI.jsp")})
	public String perdidaTotalCFDI(){
		
		this.userName = usuarioService.getUsuarioActual().getNombreCompleto().trim();
		this.token = usuarioService.getUsuarioActual().getToken();
		System.out.println("userName  "+userName);
		System.out.println("token  "+token);	
		if(idIndemnizacion != null && idIndemnizacion.longValue() > ID_INVALIDO){
			informacionSiniestro = perdidaTotalService.obtenerInformacionSiniestro(idIndemnizacion, tipoSiniestro, tipoSiniestroDesc);
			indemnizacion = perdidaTotalService.buscarIndemnizacionSiniestroPorId(idIndemnizacion);
			esFiniquitoAsegurado = cartaSiniestroService.esCoberturaAsegurado(idIndemnizacion);
			esRecepcionPagoDanios = cartaSiniestroService.getEsRecepcionPagoDanios(indemnizacion.getId());
		}
		return SUCCESS;
	}
	/**
	 * @return the listaPerdidasTotales
	 */
	public List<mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro> getListaPerdidasTotales() {
		return listaPerdidasTotales;
	}

	/**
	 * @param listaPerdidasTotales the listaPerdidasTotales to set
	 */
	public void setListaPerdidasTotales(
			List<mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro> listaPerdidasTotales) {
		this.listaPerdidasTotales = listaPerdidasTotales;
	}

	/**
	 * @return the filtroPT
	 */
	public PerdidaTotalFiltro getFiltroPT() {
		return filtroPT;
	}

	/**
	 * @param filtroPT the filtroPT to set
	 */
	public void setFiltroPT(PerdidaTotalFiltro filtroPT) {
		this.filtroPT = filtroPT;
	}

	/**
	 * @return the oficinasActivas
	 */
	public Map<Long, String> getOficinasActivas() {
		if(oficinasActivas == null || oficinasActivas.isEmpty()){
			this.oficinasActivas = listadoService.obtenerOficinasSiniestros();
		}
		return oficinasActivas;
	}

	/**
	 * @param oficinasActivas the oficinasActivas to set
	 */
	public void setOficinasActivas(Map<Long, String> oficinasActivas) {
		this.oficinasActivas = oficinasActivas;
	}

	/**
	 * @return the tiposSiniestro
	 */
	public Map<String, String> getTiposSiniestro() {
		if(tiposSiniestro == null || tiposSiniestro.isEmpty()){
			tiposSiniestro = perdidaTotalService.obtenerTiposSiniestroPerdidaTotal();
		}
		return tiposSiniestro;
	}

	/**
	 * @param tiposSiniestro the tiposSiniestro to set
	 */
	public void setTiposSiniestro(Map<String, String> tiposSiniestro) {
		this.tiposSiniestro = tiposSiniestro;
	}

	/**
	 * @return the etapasPerdidaTotal
	 */
	public Map<String, String> getEtapasPerdidaTotal() {
		if(etapasPerdidaTotal == null || etapasPerdidaTotal.isEmpty()){
			etapasPerdidaTotal = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ETAPAS_PERDIDA_TOTAL); 
		}
		return etapasPerdidaTotal;
	}

	/**
	 * @param etapasPerdidaTotal the etapasPerdidaTotal to set
	 */
	public void setEtapasPerdidaTotal(Map<String, String> etapasPerdidaTotal) {
		this.etapasPerdidaTotal = etapasPerdidaTotal;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the idOrdenCompra
	 */
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	/**
	 * @param idOrdenCompra the idOrdenCompra to set
	 */
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	/**
	 * @return the indemnizacion
	 */
	public IndemnizacionSiniestro getIndemnizacion() {
		return indemnizacion;
	}

	/**
	 * @param indemnizacion the indemnizacion to set
	 */
	public void setIndemnizacion(IndemnizacionSiniestro indemnizacion) {
		this.indemnizacion = indemnizacion;
	}

	/**
	 * @return the tipoAutorizacion
	 */
	public String getTipoAutorizacion() {
		return tipoAutorizacion;
	}

	/**
	 * @param tipoAutorizacion the tipoAutorizacion to set
	 */
	public void setTipoAutorizacion(String tipoAutorizacion) {
		this.tipoAutorizacion = tipoAutorizacion;
	}

	/**
	 * @return the numeroSiniestro
	 */
	public String getNumeroSiniestro() {
		if(indemnizacion != null && indemnizacion.getSiniestro() != null){
			numeroSiniestro = indemnizacion.getSiniestro().getNumeroSiniestro(); 
			return numeroSiniestro;
		}else{
			return "";
		}
	}

	/**
	 * @param numeroSiniestro the numeroSiniestro to set
	 */
	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	/**
	 * @return the fechaAutorizacion
	 */
	public Date getFechaAutorizacion() {
		fechaAutorizacion = new Date();
		return fechaAutorizacion;
	}

	/**
	 * @param fechaAutorizacion the fechaAutorizacion to set
	 */
	public void setFechaAutorizacion(Date fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	/**
	 * @return the idIndemnizacion
	 */
	public Long getIdIndemnizacion() {
		return idIndemnizacion;
	}

	/**
	 * @param idIndemnizacion the idIndemnizacion to set
	 */
	public void setIdIndemnizacion(Long idIndemnizacion) {
		this.idIndemnizacion = idIndemnizacion;
	}

	/**
	 * @return the fechaDeterminacion
	 */
	public Date getFechaDeterminacion() {
		if(indemnizacion != null && indemnizacion.getFechaAutDeterminacion() != null){
			fechaDeterminacion = indemnizacion.getFechaAutDeterminacion();
		}else{
			fechaDeterminacion = new Date();
		}
		return fechaDeterminacion;
	}

	/**
	 * @param fechaDeterminacion the fechaDeterminacion to set
	 */
	public void setFechaDeterminacion(Date fechaDeterminacion) {
		this.fechaDeterminacion = fechaDeterminacion;
	}

	/**
	 * @return the tipoSiniestroDesc
	 */
	public String getTipoSiniestroDesc() {
		return tipoSiniestroDesc;
	}

	/**
	 * @param tipoSiniestroDesc the tipoSiniestroDesc to set
	 */
	public void setTipoSiniestroDesc(String tipoSiniestroDesc) {
		this.tipoSiniestroDesc = tipoSiniestroDesc;
	}

	/**
	 * @return the informacionSiniestro
	 */
	public DeterminacionInformacionSiniestroDTO getInformacionSiniestro() {
		return informacionSiniestro;
	}

	/**
	 * @param informacionSiniestro the informacionSiniestro to set
	 */
	public void setInformacionSiniestro(
			DeterminacionInformacionSiniestroDTO informacionSiniestro) {
		this.informacionSiniestro = informacionSiniestro;
	}

	/**
	 * @return the esConsulta
	 */
	public Integer getEsConsulta() {
		return esConsulta;
	}

	/**
	 * @param esConsulta the esConsulta to set
	 */
	public void setEsConsulta(Integer esConsulta) {
		this.esConsulta = esConsulta;
	}

	/**
	 * @return the guardadoCorrecto
	 */
	public Integer getGuardadoCorrecto() {
		return guardadoCorrecto;
	}

	/**
	 * @param guardadoCorrecto the guardadoCorrecto to set
	 */
	public void setGuardadoCorrecto(Integer guardadoCorrecto) {
		this.guardadoCorrecto = guardadoCorrecto;
	}

	/**
	 * @return the tipoSiniestro
	 */
	public String getTipoSiniestro() {
		return tipoSiniestro;
	}

	/**
	 * @param tipoSiniestro the tipoSiniestro to set
	 */
	public void setTipoSiniestro(String tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}

	/**
	 * @return the esFiniquitoAsegurado
	 */
	public Boolean getEsFiniquitoAsegurado() {
		return esFiniquitoAsegurado;
	}

	/**
	 * @param esFiniquitoAsegurado the esFiniquitoAsegurado to set
	 */
	public void setEsFiniquitoAsegurado(Boolean esFiniquitoAsegurado) {
		this.esFiniquitoAsegurado = esFiniquitoAsegurado;
	}

	/**
	 * @return the mostrarListadoVacio
	 */
	public Boolean getMostrarListadoVacio() {
		return mostrarListadoVacio;
	}

	/**
	 * @param mostrarListadoVacio the mostrarListadoVacio to set
	 */
	public void setMostrarListadoVacio(Boolean mostrarListadoVacio) {
		this.mostrarListadoVacio = mostrarListadoVacio;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the esRecepcionPagoDanios
	 */
	public Boolean getEsRecepcionPagoDanios() {
		return esRecepcionPagoDanios;
	}

	/**
	 * @param esRecepcionPagoDanios the esRecepcionPagoDanios to set
	 */
	public void setEsRecepcionPagoDanios(Boolean esRecepcionPagoDanios) {
		this.esRecepcionPagoDanios = esRecepcionPagoDanios;
	}

	public String getMetodoInvocar() {
		return metodoInvocar;
	}

	public void setMetodoInvocar(String metodoInvocar) {
		this.metodoInvocar = metodoInvocar;
	}

	public String getPantallaOrigen() {
		return pantallaOrigen;
	}

	public void setPantallaOrigen(String pantallaOrigen) {
		this.pantallaOrigen = pantallaOrigen;
	}

	public List<IndemnizacionAutorizacion> getListaAutorizaciones() {
		return listaAutorizaciones;
	}

	public void setListaAutorizaciones(
			List<IndemnizacionAutorizacion> listaAutorizaciones) {
		this.listaAutorizaciones = listaAutorizaciones;
	}

	/**
	 * @return the indemnizacionCoberturas
	 */
	public Map<String, String> getIndemnizacionCoberturas() {
		if(indemnizacionCoberturas == null || indemnizacionCoberturas.isEmpty()){
			this.indemnizacionCoberturas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.INDEMNIZACION_COBERTURAS);
		}
		return indemnizacionCoberturas;
	}

	/**
	 * @param indemnizacionCoberturas the indemnizacionCoberturas to set
	 */
	public void setIndemnizacionCoberturas(
			Map<String, String> indemnizacionCoberturas) {
		this.indemnizacionCoberturas = indemnizacionCoberturas;
	}

	/**
	 * @return the estatusIndemnizacion
	 */
	public Map<String, String> getEstatusIndemnizacion() {
		if(estatusIndemnizacion == null || estatusIndemnizacion.isEmpty()){
			this.estatusIndemnizacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_AUT_INDEMNIZACION);
		}
		return estatusIndemnizacion;
	}

	/**
	 * @param estatusIndemnizacion the estatusIndemnizacion to set
	 */
	public void setEstatusIndemnizacion(Map<String, String> estatusIndemnizacion) {
		this.estatusIndemnizacion = estatusIndemnizacion;
	}

	/**
	 * @return the nombreRolAutorizador
	 */
	public String getNombreRolAutorizador() {
		return nombreRolAutorizador;
	}

	/**
	 * @param nombreRolAutorizador the nombreRolAutorizador to set
	 */
	public void setNombreRolAutorizador(String nombreRolAutorizador) {
		this.nombreRolAutorizador = nombreRolAutorizador;
	}

	/**
	 * @return the listaRolesSinAutorizacionFinal
	 */
	public Map<String, String> getListaRolesSinAutorizacionFinal() {
		if(listaRolesSinAutorizacionFinal == null || listaRolesSinAutorizacionFinal.isEmpty()){
			this.listaRolesSinAutorizacionFinal = perdidaTotalService.obtenerListaRolesAutorizacionSinAutorizacionFinal();
		}
		return listaRolesSinAutorizacionFinal;
	}

	/**
	 * @param listaRolesSinAutorizacionFinal the listaRolesSinAutorizacionFinal to set
	 */
	public void setListaRolesSinAutorizacionFinal(
			Map<String, String> listaRolesSinAutorizacionFinal) {
		this.listaRolesSinAutorizacionFinal = listaRolesSinAutorizacionFinal;
	}

	public String getNombreBeneficiarioEditable() {
		return nombreBeneficiarioEditable;
	}

	public void setNombreBeneficiarioEditable(String nombreBeneficiarioEditable) {
		this.nombreBeneficiarioEditable = nombreBeneficiarioEditable;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}