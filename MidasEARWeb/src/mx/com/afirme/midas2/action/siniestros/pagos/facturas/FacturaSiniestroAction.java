/**
 * 
 */
package mx.com.afirme.midas2.action.siniestros.pagos.facturas;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.DatosGralOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.DevolucionesFacturasDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.FacturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.FacturaSiniestroService.FiltroFactura;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author admin
 *
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/pagos/facturas/facturaSiniestro")
public class FacturaSiniestroAction extends BaseAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6142231863201432885L;

	private FiltroFactura filtroFactura;
	
	private DocumentoFiscal facturaSiniestro;
	
	private DatosGralOrdenCompraDTO datosGralOrdenCompraDTO;
	
	private Map<String,String> mapMotivosDevolucion;
	
	private Map<Long, String> mapMonedaPago;
	
	private List<DevolucionesFacturasDTO> listaDevoluciones;
	
	private Boolean validacionDevolucionFactura;
	
	private Boolean bandejaPrincipal;
	
	private Long idOrdenCompra;
	
	private Long idFacturaSiniestro;
	
	private String idsEdicionMasivaDevolucion;
	
	private String actionName;
	
	private String namespace;
	
	private TransporteImpresionDTO transporte;
	
	
	
	
	
	private final String CONTENEDOR_REGISTRO_FACTURAS 		=	"/jsp/siniestros/pagos/facturas/contenedorRegistroFactura.jsp";
	private final String CONTENEDOR_DEVOLUCION_FACTURAS 	=	"/jsp/siniestros/pagos/facturas/contenedorDevolucionFactura.jsp";
	private final String HISTORIAL_DEVOLUCIONES_GRID 		=	"/jsp/siniestros/pagos/facturas/historialDevolucionesGrid.jsp";
	private final String CONTENEDOR_BUSQUEDA_DEVOLUCIONES 	=	"/jsp/siniestros/pagos/facturas/contenedorBusquedaDevoluciones.jsp";
	private final String BUSQUEDA_DEVOLUCIONES_GRID 		=	"/jsp/siniestros/pagos/facturas/busquedaDevolucionesGrid.jsp";
	private final String WINDOW_EDICION_MASIVA 				=	"/jsp/siniestros/pagos/facturas/edicionMasivaWindow.jsp";
	private final String INCLUDE_INFORMACION_DEVOLUCION		=	"/jsp/siniestros/pagos/facturas/include_informacion_devolucion.jsp";
	
	private Map<String,String> tipoProveedorMap;
	private Map<Long,String> proveedorMap;


	

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("facturaSiniestroServiceEJB")
	private FacturaSiniestroService facturaSiniestroService;

	
	@Override
	public void prepare() throws Exception {
		this.tipoProveedorMap=  listadoService.getMapTipoPrestador();
		this.proveedorMap=new LinkedHashMap<Long, String>();

		
	}
	
	@Action(value="mostrarRegistroFactura",results={
			@Result(name=SUCCESS,location=CONTENEDOR_REGISTRO_FACTURAS),
			@Result(name=INPUT,location=CONTENEDOR_REGISTRO_FACTURAS)
			})
	public String mostrarRegistroFactura(){
		facturaSiniestro 				= new DocumentoFiscal();
		OrdenCompra tempOrdenCompra		= new OrdenCompra ();
		
		this.mapMonedaPago = listadoService.getMapMonedas();
		
		datosGralOrdenCompraDTO 	= facturaSiniestroService.getCabeceraOrdenCompra(idOrdenCompra);
		this.idOrdenCompra=idOrdenCompra;
		tempOrdenCompra.setId(idOrdenCompra);
		//TODO Corregir relacion con ordenes de compra
		List<OrdenCompra> ordenes = new ArrayList<OrdenCompra>();
		ordenes.add(tempOrdenCompra);
		facturaSiniestro.setOrdenesCompra(ordenes);
		facturaSiniestro.setFechaRegistro(new Date());
		
		return SUCCESS;
	}
	
	
	@Action(value="guardarRegistroFactura",results={
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "${actionName}",
					"namespace", "${namespace}",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"idOrdenCompra","${idOrdenCompra}",
					"validacionDevolucionFactura", "${validacionDevolucionFactura}" }),
			@Result(name = "DEVOLUCION", type = "redirectAction", params = {
					"actionName", "${actionName}",
					"idOrdenCompra","${idOrdenCompra}",
					"validacionDevolucionFactura", "${validacionDevolucionFactura}" }),
			@Result(name=INPUT,location=CONTENEDOR_REGISTRO_FACTURAS)
			})
	public String guardarRegistroFactura(){
		String resultado = "";

		try {
			facturaSiniestroService.validarFacturaRegistrada( facturaSiniestro );
			if ( facturaSiniestroService.validacionMontoTotalOrdenCompraFactura(facturaSiniestro, this.idOrdenCompra) ){
				facturaSiniestroService.registrarFactura(facturaSiniestro,this.idOrdenCompra);
				setMensaje("Factura "+ facturaSiniestro.getNumeroFactura() + " Registrada con Exito");
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
				namespace = "/siniestros/pagos/pagosSinestro";
				actionName = "mostrar";
				resultado = SUCCESS; 
			}else{
				this.actionName = "mostrarRegistroFactura";
				this.validacionDevolucionFactura = Boolean.TRUE;
				resultado = "DEVOLUCION";
			}
		} catch (Exception e) {
			setMensaje(e.getMessage());
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			this.mapMonedaPago = listadoService.getMapMonedas();
			//TODO Corregir relacion con ordenes de compra
			if(null!=facturaSiniestro.getOrdenesCompra())
				//TODO Corregir relacion con ordenes de compra
				datosGralOrdenCompraDTO 	= facturaSiniestroService.getCabeceraOrdenCompra(facturaSiniestro.getOrdenesCompra().get(0).getId());

			resultado = INPUT;
		}
		
		return resultado;
	}
	
	
	@Action(value="mostrarDevolucionFactura",results={
			@Result(name=SUCCESS,location=CONTENEDOR_DEVOLUCION_FACTURAS),
			@Result(name=INPUT,location=CONTENEDOR_DEVOLUCION_FACTURAS)
			})
	public String mostrarDevolucionFactura(){
		facturaSiniestro 				= new DocumentoFiscal();
		OrdenCompra tempOrdenCompra		= new OrdenCompra ();
		
		this.mapMotivosDevolucion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);		
		datosGralOrdenCompraDTO 	= facturaSiniestroService.getCabeceraOrdenCompra(idOrdenCompra);

		tempOrdenCompra.setId(idOrdenCompra);
		//TODO Corregir relacion con ordenes de compra
		List<OrdenCompra> ordenes = new ArrayList<OrdenCompra>();
		ordenes.add(tempOrdenCompra);
		facturaSiniestro.setOrdenesCompra(ordenes);
		
		return SUCCESS;
	}
	
	
	@Action(value="cargarHistorialDevoluciones",results={
			@Result(name=SUCCESS,location=HISTORIAL_DEVOLUCIONES_GRID),
			@Result(name=INPUT,location=HISTORIAL_DEVOLUCIONES_GRID)
			})
	public String cargarHistorialDevoluciones(){
		listaDevoluciones = facturaSiniestroService.obtenerDevolucionesOrdenCompra(idOrdenCompra);
		return SUCCESS;
	}
	
	
	
	@Action(value="guardarDevolucionFactura",results={
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "${actionName}",
					"namespace", "${namespace}",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"idOrdenCompra","${idOrdenCompra}",
					"validacionDevolucionFactura", "${validacionDevolucionFactura}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "${actionName}",
					"namespace", "${namespace}",
					"mensaje", "${mensaje}",
					"bandejaPrincipal","${bandejaPrincipal}",
					"tipoMensaje", "${tipoMensaje}",
					"idOrdenCompra","${idOrdenCompra}" })
			})
	public String guardarDevolucionFactura(){
		try{
			//TODO Corregir relacion con ordenes de compra
			facturaSiniestroService.validateOrdenCompraConFacturaRegistradaAntesDevolver(this.idOrdenCompra, facturaSiniestro.getNumeroFactura());
			facturaSiniestroService.validateNumeroFacturaYaDevuelta(facturaSiniestro);
			facturaSiniestroService.devolverFactura(facturaSiniestro);
			setMensaje(getText("midas.siniestros.cabina.reporte.gastoAjuste.factura") +" "+ facturaSiniestro.getNumeroFactura() + " " + getText("midas.siniestros.pagos.factura.devolucion.mensaje.reigstradaDevuelta"));
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			
			if(bandejaPrincipal){
				namespace = "/siniestros/pagos/facturas/facturaSiniestro";
				actionName = "mostrarBusquedaDevoluciones";
			}else{
				namespace = "/siniestros/pagos/pagosSinestro";
				actionName = "mostrar";
			}
			
		}catch (Exception e){
			setMensaje(e.getMessage());
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			namespace = "/siniestros/pagos/facturas/facturaSiniestro";
			actionName = "mostrarDevolucionFactura";
			this.idOrdenCompra =this.idOrdenCompra;
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	@Action(value="mostrarBusquedaDevoluciones",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_DEVOLUCIONES),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_DEVOLUCIONES)
			})
	public String mostrarBusquedaDevoluciones(){		
		this.mapMotivosDevolucion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);		

		return SUCCESS;
	}
	
	
	@Action(value="cargarBusquedaDevoluciones",results={
			@Result(name=SUCCESS,location=BUSQUEDA_DEVOLUCIONES_GRID),
			@Result(name=INPUT,location=BUSQUEDA_DEVOLUCIONES_GRID)
			})
	public String cargarBusquedaDevoluciones(){
		listaDevoluciones = new ArrayList<DevolucionesFacturasDTO>();
		
		if(filtroFactura != null){
			listaDevoluciones = facturaSiniestroService.buscarFacturas(filtroFactura);
		}
		
		return SUCCESS;
	}
	
	
	
	@Action(value="mostrarEdicionMasiva",results={
			@Result(name=SUCCESS,location=WINDOW_EDICION_MASIVA),
			@Result(name=INPUT,location=WINDOW_EDICION_MASIVA)
			})
	public String mostrarEdicionMasiva(){				
		return SUCCESS;
	}
	
	
	@Action(value="edicionMasivaDevoluciones",results={
			@Result(name=SUCCESS,location=CONTENEDOR_BUSQUEDA_DEVOLUCIONES),
			@Result(name=INPUT,location=CONTENEDOR_BUSQUEDA_DEVOLUCIONES)
			})
	public String edicionMasivaDevoluciones(){	
		this.mapMotivosDevolucion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);		

		facturaSiniestroService.realizarEdicionMasivaDevoluciones(idsEdicionMasivaDevolucion, facturaSiniestro.getEntregada(), facturaSiniestro.getFechaEntrega());
		return SUCCESS;
	}
	
	
	
	
	@Action(value="editarDevolucionFactura",results={
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "${actionName}",
					"namespace", "${namespace}",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"idOrdenCompra","${idOrdenCompra}",
					"validacionDevolucionFactura", "${validacionDevolucionFactura}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "${actionName}",
					"namespace", "${namespace}",
					"mensaje", "${mensaje}",
					"bandejaPrincipal","${bandejaPrincipal}",
					"tipoMensaje", "${tipoMensaje}",
					"idOrdenCompra","${idOrdenCompra}" })
			})
	public String editarDevolucionFactura(){
		try{
			facturaSiniestroService.editarDevolucion(facturaSiniestro);
			
			setMensaje(getText("midas.siniestros.pagos.factura.devolucion.mensaje.actualizoInformacion"));
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			namespace = "/siniestros/pagos/facturas/facturaSiniestro";
			actionName = "mostrarBusquedaDevoluciones";
			
		}catch (Exception e){
			setMensaje(e.getMessage());
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			namespace = "/siniestros/pagos/facturas/facturaSiniestro";
			actionName = "mostrarDevolucionFactura";
			//TODO Corregir relacion con ordenes de compra
			this.idOrdenCompra = facturaSiniestro.getOrdenesCompra().get(0).getId();
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	
	@Action(value="mostrarEdicionDevolucion",results={
			@Result(name=SUCCESS,location=CONTENEDOR_DEVOLUCION_FACTURAS),
			@Result(name=INPUT,location=CONTENEDOR_DEVOLUCION_FACTURAS)
			})
	public String mostrarEdicionDevolucion(){	
		this.facturaSiniestro 		= facturaSiniestroService.getFacturaById(this.idFacturaSiniestro);
		this.mapMotivosDevolucion 	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);		
		//TODO Corregir relacion con ordenes de compra
		this.idOrdenCompra 			= this.facturaSiniestro.getOrdenesCompra().get(0).getId();
		datosGralOrdenCompraDTO 	= facturaSiniestroService.getCabeceraOrdenCompra(this.idOrdenCompra);
		return SUCCESS;
	}
	
	
	@Action(value = "imprimirFacturaDevolucion", results = {
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""}),
			@Result(name=INPUT,location=CONTENEDOR_DEVOLUCION_FACTURAS)
			})
	public String imprimirFacturaDevolucion(){
		DocumentoFiscal factura 	= null;
		try{
				transporte = facturaSiniestroService.imprimirDevolucionFactura(idFacturaSiniestro);
				factura	= facturaSiniestroService.getFacturaById(this.idFacturaSiniestro);
				transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
				transporte.setContentType("application/pdf");
				
				String fileName = "Devolucion_Factura_"+ factura.getNumeroFactura()+ "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
				transporte.setFileName(fileName);
			
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="loadInformacionDevolucion",results={
			@Result(name=SUCCESS,location=INCLUDE_INFORMACION_DEVOLUCION),
			@Result(name=INPUT,location=INCLUDE_INFORMACION_DEVOLUCION)
			})
	public String loadInformacionDevolucion(){	
		
		this.facturaSiniestro 		= facturaSiniestroService.getFacturaById(this.idFacturaSiniestro);
		this.mapMotivosDevolucion 	= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_CANCELACION_ORDENPAGO);	
		
		return SUCCESS;
	}
	
	
	
	

	/**
	 * @return the filtroFactura
	 */
	public FiltroFactura getFiltroFactura() {
		return filtroFactura;
	}

	/**
	 * @param filtroFactura the filtroFactura to set
	 */
	public void setFiltroFactura(FiltroFactura filtroFactura) {
		this.filtroFactura = filtroFactura;
	}

	/**
	 * @return the facturaSiniestroService
	 */
	public FacturaSiniestroService getFacturaSiniestroService() {
		return facturaSiniestroService;
	}

	/**
	 * @param facturaSiniestroService the facturaSiniestroService to set
	 */
	public void setFacturaSiniestroService(
			FacturaSiniestroService facturaSiniestroService) {
		this.facturaSiniestroService = facturaSiniestroService;
	}

	/**
	 * @return the facturaSiniestro
	 */
	public DocumentoFiscal getFacturaSiniestro() {
		return facturaSiniestro;
	}

	/**
	 * @param facturaSiniestro the facturaSiniestro to set
	 */
	public void setFacturaSiniestro(DocumentoFiscal facturaSiniestro) {
		this.facturaSiniestro = facturaSiniestro;
	}


	/**
	 * @return the motivosDevolucion
	 */
	public Map<String, String> getMapMotivosDevolucion() {
		return mapMotivosDevolucion;
	}


	/**
	 * @param motivosDevolucion the motivosDevolucion to set
	 */
	public void setMapMotivosDevolucion(Map<String, String> mapMotivosDevolucion) {
		this.mapMotivosDevolucion = mapMotivosDevolucion;
	}

	/**
	 * @return the validacionDevolucionFactura
	 */
	public Boolean getValidacionDevolucionFactura() {
		return validacionDevolucionFactura;
	}

	/**
	 * @param validacionDevolucionFactura the validacionDevolucionFactura to set
	 */
	public void setValidacionDevolucionFactura(Boolean validacionDevolucionFactura) {
		this.validacionDevolucionFactura = validacionDevolucionFactura;
	}

	/**
	 * @return the idOrdenCompra
	 */
	public Long getIdOrdenCompra() {
		return idOrdenCompra;
	}

	/**
	 * @param idOrdenCompra the idOrdenCompra to set
	 */
	public void setIdOrdenCompra(Long idOrdenCompra) {
		this.idOrdenCompra = idOrdenCompra;
	}

	/**
	 * @return the datosGralOrdenCompraDTO
	 */
	public DatosGralOrdenCompraDTO getDatosGralOrdenCompraDTO() {
		return datosGralOrdenCompraDTO;
	}

	/**
	 * @param datosGralOrdenCompraDTO the datosGralOrdenCompraDTO to set
	 */
	public void setDatosGralOrdenCompraDTO(
			DatosGralOrdenCompraDTO datosGralOrdenCompraDTO) {
		this.datosGralOrdenCompraDTO = datosGralOrdenCompraDTO;
	}

	/**
	 * @return the mapMonedaPago
	 */
	public Map<Long, String> getMapMonedaPago() {
		return mapMonedaPago;
	}

	/**
	 * @param mapMonedaPago the mapMonedaPago to set
	 */
	public void setMapMonedaPago(Map<Long, String> mapMonedaPago) {
		this.mapMonedaPago = mapMonedaPago;
	}

	/**
	 * @return the listaDevoluciones
	 */
	public List<DevolucionesFacturasDTO> getListaDevoluciones() {
		return listaDevoluciones;
	}

	/**
	 * @param listaDevoluciones the listaDevoluciones to set
	 */
	public void setListaDevoluciones(List<DevolucionesFacturasDTO> listaDevoluciones) {
		this.listaDevoluciones = listaDevoluciones;
	}

	/**
	 * @return the idEdicionMasivaDevolucion
	 */
	public String getIdsEdicionMasivaDevolucion() {
		return idsEdicionMasivaDevolucion;
	}

	/**
	 * @param idEdicionMasivaDevolucion the idEdicionMasivaDevolucion to set
	 */
	public void setIdsEdicionMasivaDevolucion(String idsEdicionMasivaDevolucion) {
		this.idsEdicionMasivaDevolucion = idsEdicionMasivaDevolucion;
	}

	/**
	 * @return the idFacturaSiniestro
	 */
	public Long getIdFacturaSiniestro() {
		return idFacturaSiniestro;
	}

	/**
	 * @param idFacturaSiniestro the idFacturaSiniestro to set
	 */
	public void setIdFacturaSiniestro(Long idFacturaSiniestro) {
		this.idFacturaSiniestro = idFacturaSiniestro;
	}

	/**
	 * @return the actionName
	 */
	public String getActionName() {
		return actionName;
	}

	/**
	 * @param actionName the actionName to set
	 */
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the bandejaPrincipal
	 */
	public Boolean getBandejaPrincipal() {
		return bandejaPrincipal;
	}

	/**
	 * @param bandejaPrincipal the bandejaPrincipal to set
	 */
	public void setBandejaPrincipal(Boolean bandejaPrincipal) {
		this.bandejaPrincipal = bandejaPrincipal;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public Map<String, String> getTipoProveedorMap() {
		return tipoProveedorMap;
	}

	public void setTipoProveedorMap(Map<String, String> tipoProveedorMap) {
		this.tipoProveedorMap = tipoProveedorMap;
	}

	public Map<Long, String> getProveedorMap() {
		return proveedorMap;
	}

	public void setProveedorMap(Map<Long, String> proveedorMap) {
		this.proveedorMap = proveedorMap;
	}

	
	

	
	
	

}
