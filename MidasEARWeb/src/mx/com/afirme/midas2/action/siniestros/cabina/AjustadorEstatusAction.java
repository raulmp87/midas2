package mx.com.afirme.midas2.action.siniestros.cabina;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/cabina/")
public class AjustadorEstatusAction extends BaseAction implements Preparable{

	private static final String	LOCATION_CONTENEDORREPORTECABINA_JSP	= "/jsp/siniestros/cabina/reportecabina/contenedorReporteCabina.jsp";

	private static final long serialVersionUID = 1L;
	
	private Long idServicioSiniestro;
	private Double latitud;
	private Double longitud;
	
	@Autowired
	@Qualifier("ajustadorEstatusServiceEJB")
	private AjustadorEstatusService ajustadorEstatusService;

	@Override
	public void prepare(){
	}
	
	@Action(value = "asignarAjustador", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORREPORTECABINA_JSP)})
	public String asignarAjustador(){
		return SUCCESS;
	}
	
	@Action(value = "guardarEstatusAjustador", results = {
			@Result(name = SUCCESS, location = LOCATION_CONTENEDORREPORTECABINA_JSP) ,
			@Result(name = INPUT, location = LOCATION_CONTENEDORREPORTECABINA_JSP)})
	public String guardarEstatusAjustador(){
		if(SystemCommonUtils.isNotNullNorZero(idServicioSiniestro) && 
				SystemCommonUtils.isNotNullNorZero(latitud)&& 
				SystemCommonUtils.isNotNullNorZero(longitud)){
			Long idNueva = ajustadorEstatusService.guardarAjustadorEstatus(this.idServicioSiniestro, this.latitud, this.longitud); 
			setMensaje( "nuevo ajustador estatus: " + idNueva);
		}else{
			setMensajeError("No se enviaron todos los datos necesarios");
			return INPUT;
		}
		return SUCCESS;
	}

	/**
	 * @return the idServicioSiniestro
	 */
	public Long getIdServicioSiniestro() {
		return idServicioSiniestro;
	}

	/**
	 * @param idServicioSiniestro the idServicioSiniestro to set
	 */
	public void setIdServicioSiniestro(Long idServicioSiniestro) {
		this.idServicioSiniestro = idServicioSiniestro;
	}

	/**
	 * @return the latitud
	 */
	public Double getLatitud() {
		return latitud;
	}

	/**
	 * @param latitud the latitud to set
	 */
	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	/**
	 * @return the longitud
	 */
	public Double getLongitud() {
		return longitud;
	}

	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

}
