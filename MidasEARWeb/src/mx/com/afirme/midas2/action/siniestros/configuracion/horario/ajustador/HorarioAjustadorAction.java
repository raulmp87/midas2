package mx.com.afirme.midas2.action.siniestros.configuracion.horario.ajustador;
 
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustadorId;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ConfiguracionDeDisponibilidadDTO;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ListadoDeDisponibilidadDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService.HorarioAjustadorFiltro;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.laboral.HorarioLaboralService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.laboral.HorarioLaboralService.HorarioLaboralFiltro;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.commons.io.IOUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/configuracion/horario/ajustador/horarioAjustador")
public class HorarioAjustadorAction extends BaseAction implements Preparable {

	// region static vars
	private static final long	serialVersionUID				= 1L;

	// region Nombres de acciones
	private static final String	actionNameMostrarConf			= "mostrarConfiguracion";
	private static final String	actionNameMostrarBusqueda		= "mostrarBusqueda";
	private static final String	actionNameValidarListarConf		= "validarListarConfiguracion";
	private static final String	actionNameListarConfiguracion	= "listarConfiguracion";
	private static final String	actionNameSalvarActualizar		= "salvarActualizar";
	private static final String	actionNameEnviarPorCorreo		= "enviarPorCorreo";
	private static final String	actionNameBorrarConf			= "borrarConfiguracion";
	private static final String	actionNameExportarExcel			= "exportarExcel";
	private static final int 	DIAS_LABORALES = 5;

	public String getActionNameMostrarConf() {
		return actionNameMostrarConf;
	}

	public String getActionNameMostrarBusqueda() {
		return actionNameMostrarBusqueda;
	}

	public String getActionNameListarConfiguracion() {
		return actionNameListarConfiguracion;
	}

	public String getActionNameValidarListarConfiguracion() {
		return actionNameValidarListarConf;
	}

	public String getActionNameSalvarActualizar() {
		return actionNameSalvarActualizar;
	}

	public String getActionNameBorrarConf() {
		return actionNameBorrarConf;
	}

	public String getActionNameEnviarPorCorreo() {
		return actionNameEnviarPorCorreo;
	}
	
	public String getActionNameExportarExcel(){
		return actionNameExportarExcel;
	}

	// endregion Nombres de acciones

	// region Jsps nombres y locations
	static final String	locJSPsFolder		= "/jsp/siniestros/configuracion/horario/ajustador/";

	static final String	jspHeader			= "horarioAjustadorHeader.jsp";

	static final String	jspConfiguracion	= "horarioAjustadorConfiguracion.jsp";
	static final String	locJspConfiguracion	= locJSPsFolder + jspConfiguracion;

	static final String	jspBusqueda			= "horarioAjustadorBusqueda.jsp";
	static final String	locJspBusqueda		= locJSPsFolder + jspBusqueda;

	static final String	jspCamposIngreso	= "horarioAjustadorConfiguracionCampos.jsp";
	static final String	locJspCamposIngreso	= locJSPsFolder + jspCamposIngreso;

	static final String	jspFiltro			= "horarioAjustadorBusquedaFiltro.jsp";
	static final String	locJspFiltro		= locJSPsFolder + jspFiltro;

	static final String	jspCorreos			= "horarioAjustadorBusquedaCorreos.jsp";
	static final String	locJspCorreos		= locJSPsFolder + jspCorreos;

	static final String	jspLstDispGrid		= "listadoDeDisponibilidadGrid.jsp";
	static final String	locLstDispGrid		= locJSPsFolder + jspLstDispGrid;

	public String getJspHeader() {
		return jspHeader;
	}

	public String getJspConfiguracion() {
		return jspConfiguracion;
	}

	public String getJspBusqueda() {
		return jspBusqueda;
	}

	public String getJspCamposIngreso() {
		return jspCamposIngreso;
	}

	public String getJspFiltro() {
		return jspFiltro;
	}

	public String getJspCorreos() {
		return jspCorreos;
	}

	public String getJspGrid() {
		return jspLstDispGrid;
	}

	// endregion Jsps nombres y locations
	// endregion static vars

	// region Jsp divs forms ids
	private static final String	divHeader		= "divHeader";
	private static final String	divConfCampos	= "divConfCamp";
	private static final String	formConfig		= "formConfig";
	private static final String	divBusqFiltro	= "divBusqFiltro";
	private static final String	formFiltro		= "formFiltro";
	private static final String	divEnvioCorreo	= "divEnvioCorreo";
	private static final String	formEnvioCorreo	= "formEnvioCorreo";
	private static final String	divLstDispGrid	= "divLstDispGrid";

	public String getDivHeader() {
		return divHeader;
	}

	public String getDivConfCampos() {
		return divConfCampos;
	}

	public String getFormConfig() {
		return formConfig;
	}

	public String getDivBusqFiltro() {
		return divBusqFiltro;
	}

	public String getFormFiltro() {
		return formFiltro;
	}

	public String getDivEnvioCorreo() {
		return divEnvioCorreo;
	}

	public String getFormEnvioCorreo() {
		return formEnvioCorreo;
	}

	public String getDivLstDispGrid() {
		return divLstDispGrid;
	}

	// endregion Jsp divs ids

	// region Grid lists
	private List<ListadoDeDisponibilidadDTO>	listadoDeDisponibilidadGrid;
	
	private List<HorarioAjustador> listadoHorariosBorrados;
	private List<String> selected;

	private Boolean								soloDatos	= false;

	public List<ListadoDeDisponibilidadDTO> getListadoDeDisponibilidadGrid() {
		if (listadoDeDisponibilidadGrid != null
				&& listadoDeDisponibilidadGrid.size() > 0) {
			cargarCatalogosEnGrid();
			return listadoDeDisponibilidadGrid;
		} else {
			return new ArrayList<ListadoDeDisponibilidadDTO>();
		}
	}

	public Boolean getSoloDatos() {
		return soloDatos;
	}

	public void setSoloDatos(Boolean soloDatos) {
		this.soloDatos = soloDatos;
	}

	public static String getPreA() {
		return "&#60;a  href=&#34;javascript: void(0);&#34; &#62;X&#60;&#47;a&#62; onclick=&#34;borrarConfiguracionAction('";
		// return
		// "<a href=\"javascript: void(0);\" onClick=\"borrarConfiguracionAction('";
	}

	public static String getPostA() {
		return "');&#34>X</a> ";
		// return "');\"&#62X&#60/a&#62";
	}

	// endregion Grid lists

	// region Filtros, DTOs
	private HorarioAjustadorFiltro	horarioAjustadorFiltro;

	public HorarioAjustadorFiltro getHorarioAjustadorFiltro() {
		return horarioAjustadorFiltro;
	}

	public void setHorarioAjustadorFiltro(
			HorarioAjustadorFiltro horarioAjustadorFiltro) {
		this.horarioAjustadorFiltro = horarioAjustadorFiltro;
	}

	// endregion Filtros, DTOs

	// endregion Jsp divs ids

	// region Exportar a Excel DTO
	private TransporteImpresionDTO		transporte;

	// endregion Exportar a Excel DTO

	// region Catalogos, Listas
	private Map<Long, String>	ctgOficinas;
	private Map<String, String>	ctgAjustadores			= new HashMap<String, String>();
	private Map<Long, String>	ctgHorariosLaborales;
	private Map<String, String>	ctgHorarios;
	private Map<String, String>	ctgTipoDisponibilidad;

	private Map<String, String>	ctgFormasDeEnvio;

	private List<String>		listaDias;
	private static final String	AGREGAR_QUITAR_TODOS	= "Agregar/Quitar Todos";
	private static final String	AJUSTADORES				= "Ajustadores";
	private List<String>		listaCorreoRoles;

	public Map<Long, String> getCtgOficinas() {
		if (ctgOficinas == null)
			ctgOficinas = listadoService.obtenerOficinasSiniestros();
		return ctgOficinas;
	}

	public void setCtgOficinas(Map<Long, String> ctgOficinas) {
		this.ctgOficinas = ctgOficinas;
	}

	public Map<String, String> getCtgAjustadores() {
		return ctgAjustadores;
	}

	public void setCtgAjustadores(Map<String, String> ctgAjustadores) {
		this.ctgAjustadores = ctgAjustadores;
	}

	public Map<Long, String> getCtgHorariosLaborales() {
		if (ctgHorariosLaborales == null)
			ctgHorariosLaborales = listadoService.getMapHorariosLaborales(true);
		return ctgHorariosLaborales;
	}

	public Map<String, String> getCtgHorarios() {
		if (ctgHorarios == null)
			ctgHorarios = this.listadoService
					.obtenerCatalogoValorFijo(HorarioLaboral.ctgHorariosTipo);
		return ctgHorarios;
	}

	public Map<String, String> getCtgTipoDisponibilidad() {
		if (ctgTipoDisponibilidad == null)
			ctgTipoDisponibilidad = this.listadoService
					.obtenerCatalogoValorFijo(HorarioAjustador.ctgDisponibilidadTipo);
		return ctgTipoDisponibilidad;
	}

	public Map<String, String> getCtgFormasDeEnvio() {
		if (ctgFormasDeEnvio == null)
			ctgFormasDeEnvio = this.listadoService
					.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_FORMAS_DE_ENVIO);
		return ctgFormasDeEnvio;
	}

	public List<String> getListaCorreoRoles() {
		if (listaCorreoRoles == null) {
			listaCorreoRoles = new ArrayList<String>();
			listaCorreoRoles.add(AGREGAR_QUITAR_TODOS);
			listaCorreoRoles.add(AJUSTADORES);
			listaCorreoRoles.add("Coordinador Cabina y Ajustes");
			listaCorreoRoles.add("Subdirector Siniestros Autos");
			listaCorreoRoles.add("Director Indemnización y Operación");
		}
		return listaCorreoRoles;
	}

	public void setListaCorreoRoles(List<String> listaCorreoRoles) {
		this.listaCorreoRoles = listaCorreoRoles;
	}

	public List<String> getListaDias() {
		if (listaDias == null) {
			listaDias = Arrays.asList(DateUtils.diasSem());
		}
		return listaDias;
	}

	public void setListaDias(List<String> listaDias) {
		this.listaDias = listaDias;
	}

	public String[] getDiasPredeterminado() {
		String[] pred = {};
		pred = Arrays.copyOfRange(DateUtils.diasSem(), 0, DIAS_LABORALES);
		return pred;
	}

	// endregion Catalogos, Listas

	// region Actions parameters
	private static final String					FORMA_DE_ENVIO_INDIVIDUAL	= "Individual";
	private static final String					FORMA_DE_ENVIO_GRUPAL		= "Grupal";

	private HorarioAjustadorId					horarioAjustadorId;

	private ConfiguracionDeDisponibilidadDTO	config;
	private String								correoRolesSeleccionados;
	private String								correoFormaDeEnvioCode;

	public String getCorreoRolesSeleccionados() {
		return correoRolesSeleccionados;
	}

	public void setCorreoRolesSeleccionados(String correoRolesSeleccionados) {
		// Remover agregar quitar todos
		this.correoRolesSeleccionados = correoRolesSeleccionados.replaceFirst(
				AGREGAR_QUITAR_TODOS + ",", "");
	}

	public String getCorreoFormaDeEnvioCode() {
		return correoFormaDeEnvioCode;
	}

	public void setCorreoFormaDeEnvioCode(String correoFormaDeEnvioCode) {
		this.correoFormaDeEnvioCode = correoFormaDeEnvioCode;
	}

	public void setHorarioAjustadorId(HorarioAjustadorId horarioAjustadorId) {
		this.horarioAjustadorId = horarioAjustadorId;
	}

	public HorarioAjustadorId getHorarioAjustadorId() {
		return horarioAjustadorId;
	}

	public void setConfig(ConfiguracionDeDisponibilidadDTO config) {
		this.config = config;
	}

	public ConfiguracionDeDisponibilidadDTO getConfig() {
		return config;
	}

	public Date getFechaInicialLimite() {
		return horarioAjustadorService.getFechaInicioLimite();
	}

	public Date getFechaFinalLimite() {
		return horarioAjustadorService.getFechaFinalLimite();
	}

	public Long getHorarioLaboralDefault() {
		HorarioLaboralFiltro horarioLaboralFiltro = new HorarioLaboralFiltro();
		horarioLaboralFiltro.setPredeterminado(true);
		return horarioLaboralService.listar(horarioLaboralFiltro).get(0)
				.getId();
	}

	// endregion Actions parameters

	// region Services
	@Autowired
	@Qualifier("horarioAjustadorServiceEJB")
	private HorarioAjustadorService	horarioAjustadorService;

	@Autowired
	@Qualifier("horarioLaboralServiceEJB")
	private HorarioLaboralService	horarioLaboralService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService			listadoService;

	// region Actions
	@Override
	public void prepare(){

	}

	public void prepareMostrarConfiguracion(){
		// ctgOficinas = listadoService.obtenerOficinasSiniestros();
	}

	@Action(value = actionNameMostrarConf, results = {
			@Result(name = SUCCESS, location = locJspConfiguracion),
			@Result(name = INPUT, location = locJspConfiguracion) })
	public String mostrarConfiguracion() {
		return SUCCESS;
	}

	@Action(value = actionNameMostrarBusqueda, results = {
			@Result(name = SUCCESS, location = locJspBusqueda),
			@Result(name = INPUT, location = locJspBusqueda) })
	public String mostrarBusqueda() {
		return SUCCESS;
	}

	@Action(value = actionNameValidarListarConf, results = {
			@Result(name = INPUT, location = locJspFiltro),
			@Result(name = SUCCESS, location = locJspFiltro) })
	public String validarListarConfiguracion() {
		return SUCCESS;
	}

	@Action(value = actionNameListarConfiguracion, results = { @Result(name = SUCCESS, location = locLstDispGrid) })
	public String listarConfiguracion() {
		if (horarioAjustadorFiltro != null) {
			cargarCatalogosEnGrid();
			if (horarioAjustadorFiltro.getAjustadoresIds() != null) {
				horarioAjustadorFiltro.getAjustadoresIds().remove("");
			}
			this.listadoDeDisponibilidadGrid = this.horarioAjustadorService
					.listar(horarioAjustadorFiltro);
			soloDatos = true;
		} else {
			listadoDeDisponibilidadGrid = new ArrayList<ListadoDeDisponibilidadDTO>();
		}
		return SUCCESS;
	}

	@Action(value = actionNameSalvarActualizar, results = {
			@Result(name = INPUT, location = locJspCamposIngreso),
			@Result(name = SUCCESS, location = locJspCamposIngreso) })
	public String salvarActualizar() {
		try {
			// validateSalvarActualizar();
			if (!(getFieldErrors().isEmpty())) {
				return INPUT;
			}
			// necesario para remover el header value
			if (config != null && config.getAjustadoresIds() != null) {
				config.getAjustadoresIds().remove("");
			}
			//original call
			//horarioAjustadorService.guardarHorariosAjustador(config);
			horarioAjustadorService.guardarHorariosAjustadorSP(config);
		} catch (NegocioEJBExeption ex) {
			catchNegocioEJBExeption(ex);
			return INPUT;
		} catch (Exception ex) {
			catchExceptionAndAddErrors(ex, HorarioAjustador.class);
			return INPUT;
		}
		// horarioAjustadorFiltro = new HorarioAjustadorFiltro();
		// horarioAjustadorFiltro.setAjustadoresIds(config.getAjustadoresIds());
		soloDatos = true;
		return SUCCESS;
	}

	@Action(value = actionNameBorrarConf, results = { @Result(name = SUCCESS, location = locLstDispGrid) })
	public String borrarConfiguracion() {
		try {

			listadoHorariosBorrados = horarioAjustadorService
					.borrar(selected);

		} catch (NegocioEJBExeption ex) {
			catchNegocioEJBExeption(ex);
			return SUCCESS;
		} catch (Exception ex) {
			// catchExceptionAndAddErrors(ex, HorarioAjustador.class);
			return SUCCESS;
		}

		this.listadoDeDisponibilidadGrid = this.horarioAjustadorService
				.listar(horarioAjustadorFiltro);

		soloDatos = true;
		return SUCCESS;
	}

	@Action(value = actionNameEnviarPorCorreo, results = {
			@Result(name = INPUT, location = locJspCorreos),
			@Result(name = SUCCESS, location = locJspCorreos) })
	public String enviarPorCorreo() {
		try {
			validateEnviarPorCorreo();
			validarListarConfiguracion();
			if (!(getFieldErrors().isEmpty())) {
				return INPUT;
			}
			String formaDeEnvio = getCtgFormasDeEnvio().get(
					correoFormaDeEnvioCode);

			List<String> idsString = horarioAjustadorFiltro.getAjustadoresIds();
			List<Long> idsLong = new ArrayList<Long>();

			if (formaDeEnvio.equals(FORMA_DE_ENVIO_INDIVIDUAL)) {
				if (correoRolesSeleccionados.contains(AJUSTADORES)) {
					for (String idString : idsString) {
						Long idLong = Long.parseLong(idString);

						// Respaldar ajustadores en Filtro
						List<String> idsRespaldo = horarioAjustadorFiltro
								.getAjustadoresIds();

						// Generar Excel con los horarios del ajustador y
						// regresar ajustadores en el filtro
						List<String> idsTemp = new ArrayList<String>();
						idsTemp.add(idString);
						horarioAjustadorFiltro.setAjustadoresIds(idsTemp);
						byte[] bytes = generarExcel(horarioAjustadorFiltro);
						horarioAjustadorFiltro.setAjustadoresIds(idsRespaldo);

						horarioAjustadorService.enviarPorCorreoIndividual(
								idLong, bytes);
					}
				}
			} else if (formaDeEnvio.equals(FORMA_DE_ENVIO_GRUPAL) && correoRolesSeleccionados.contains(AJUSTADORES)) {
					for (String idString : idsString) {
						Long idLong = Long.parseLong(idString);
						idsLong.add(idLong);
					}
			}

			String[] correos = correoRolesSeleccionados.trim().split(",");			
			if(!((correos.length == 1) && (correos[0].equals(AJUSTADORES))) || idsLong.size() > 0){

				byte[] bytes = generarExcel(horarioAjustadorFiltro);
				new ArrayList<Long>(horarioAjustadorFiltro.getAjustadoresIds()
						.size());
	
	
	
				for (int i = 0; i < correos.length; i++) {
					correos[i] = correos[i].trim();
				}
				horarioAjustadorService.enviarPorCorreoGrupal(correos, idsLong,
						bytes);
			}
		} catch (NegocioEJBExeption ex) {
			String[] mensajesArray = Arrays.copyOf(ex.getValues(),
					ex.getValues().length, String[].class);
			List<String> mensajes = Arrays.asList(mensajesArray);
			if (mensajes.size() > 0) {
				// super.setMensajeError(mensaje)
				super.setMensajeListaPersonalizado("Error en el envío de correos",
						mensajes, TIPO_MENSAJE_INFORMACION);
			}
			return INPUT;
		} catch (Exception ex) {
			catchExceptionAndAddErrors(ex, HorarioAjustador.class);
			return INPUT;
		}
		this.setMensajeExitoPersonalizado("Se enviaron los correos a los destinatarios seleccionados.");
		return SUCCESS;
	}

	@Action(value = actionNameExportarExcel, results = { @Result(name = SUCCESS, type = "stream", params = {
			"contentType", "${transporte.contentType}", "inputName",
			"transporte.genericInputStream", "contentDisposition",
			"attachment;filename=\"${transporte.fileName}\"" }) })
	public String exportarExcel() {
		transporte = new TransporteImpresionDTO();
		if (horarioAjustadorFiltro.getAjustadoresIds() != null) {
			horarioAjustadorFiltro.getAjustadoresIds().remove("");
		}
		
		List<ListadoDeDisponibilidadDTO> listadoDeDisponidilidad = horarioAjustadorService.listar(horarioAjustadorFiltro);
		
		ExcelExporter exporter = new ExcelExporter(ListadoDeDisponibilidadDTO.class);
		transporte = exporter.exportXLS(listadoDeDisponidilidad, "Listado_disponibilidad_ajustadores");
		
		return SUCCESS;
	}

	// endregion Actions

	// region Prepares
	/**
	 * Funcion que implementa la RDN {92BC06EF-C4D3-4001-B0DF-E2ECBACE3F89} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Cargar
	 * Información para Configurar
	 */
	public void prepareSalvarActualizar() {
		getCtgOficinas();
		getCtgTipoDisponibilidad();
		getCtgHorariosLaborales();
	}

	// endregion Prepares

	// region validates
	/**
	 * Funcion que valida la RDN {117567D2-C7B2-4333-8FC1-FB6363A0DC22} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Campos
	 * Obligatorios Configuración de Disponibilidad
	 */
	public void validateSalvarActualizar() {
		String msgCampoRequerido = "Campo Requerido";
		String msgSemanaValido = "Selecciona al menos un dia de la semana";
		String msgTipoDisponibilidad = "Selecciona el tipo de disponibilidad";
		String msgFechaFinalLimiteValido = "La fecha final tiene que ser menor";
		String msgFechaInicialLimiteValido = "La fecha inicial tiene que ser mayor";

		if (config.getFechaInicial() == null) {
			addFieldError("config.fechaInicial", msgCampoRequerido);
		} else {
			if (config.getFechaInicial().compareTo(getFechaInicialLimite()) < 0) {
				addFieldError("config.fechaInicial",
						msgFechaInicialLimiteValido);
			}
		}

		if (config.getFechaFinal() == null) {
			addFieldError("config.fechaFinal", msgCampoRequerido);
		} else {
			if (config.getFechaFinal().compareTo(getFechaFinalLimite()) > 0) {
				addFieldError("config.fechaFinal", msgFechaFinalLimiteValido);
			}
		}

		if (config.getDiasSeleccion() == null
				|| config.getDiasSeleccion().equals("")) {
			addFieldError("config.diasSeleccion", msgSemanaValido);
		}

		if (config.getAjustadoresIds() == null
				|| config.getAjustadoresIds().size() == 0) {
			addFieldError("config.ajustadoresIds", msgCampoRequerido);
		}

		if (config.getOficinaId() == null
				|| config.getOficinaId().equalsIgnoreCase("")) {
			addFieldError("config.oficinaId", msgCampoRequerido);
		}

		if (config.getHorarioLaboralId() == null) {
			addFieldError("config.horarioLaboralId", msgCampoRequerido);
		}

		if (config.getTipoDisponibilidadCode() == null
				|| config.getTipoDisponibilidadCode().equalsIgnoreCase("")) {
			addFieldError("config.tipoDisponibilidadCode",
					msgTipoDisponibilidad);
		}
		super.validate();

		// Llenar lista de ajustadores en caso de error
		ctgAjustadores = listadoService.getMapAjustadoresPorOficinaId(Long
				.parseLong(config.getOficinaId()));
	}

	/**
	 * Funcion que valida la RDN {3B578676-B7F8-4fee-8A1F-34D8E1864BF1} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Búsqueda por
	 * varios campos capturados
	 * 
	 * Solo valida campos requeridos.
	 */
	public void validateValidarListarConfiguracion() {
		// horarioAjustadorService.validarCamposRequeridosFiltro(horarioAjustadorFiltro);
		String msgError = "Ingresa al menos 2 campos";
		int camposRequeridos = 2;

		int contador = horarioAjustadorFiltro.validarAjustadoresIds() ? 0 : 1;
		contador += horarioAjustadorFiltro.validarFechaFinal() ? 0 : 1;
		contador += horarioAjustadorFiltro.validarFechaInicial() ? 0 : 1;
		contador += horarioAjustadorFiltro.validarHorarioFinalCode() ? 0 : 1;
		contador += horarioAjustadorFiltro.validarHorarioInicialCode() ? 0 : 1;
		contador += horarioAjustadorFiltro.validarOficinaId() ? 0 : 1;
		contador += horarioAjustadorFiltro.validarTipoDisponibilidadCode() ? 0
				: 1;

		if (contador > camposRequeridos) {
			addFieldError("filtro.ajustadoresIds", msgError);
			addFieldError("filtro.fechaFinal", msgError);
			addFieldError("filtro.fechaInicial", msgError);
			addFieldError("filtro.horarioFinalCode", msgError);
			addFieldError("filtro.horarioInicialCode", msgError);
			addFieldError("filtro.oficinaId", msgError);
			addFieldError("filtro.tipoDisponibilidadCode", msgError);
		}
		// Llenar lista de ajustadores en caso de error
		// ctgAjustadores =
		// listadoService.getMapAjustadoresPorOficinaId(Long.parseLong(oficinaId));
	}

	/**
	 * Funcion que valida la RDN {963E843F-6F04-4e67-89AD-DB8E5A42AC81} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Campos
	 * Obligatorios para Envío de Horario
	 * 
	 * Esta validacion se hace en la vista debido a que el generador de excel
	 * solo se encuentra en la vista
	 */
	private void validateEnviarPorCorreo() {
		String msgCampoRequerido = "Campo Requerido";

		if (correoRolesSeleccionados == null) {
			addFieldError("correoRolesSeleccionados", msgCampoRequerido);
		} else {
			if (correoRolesSeleccionados.contains(AJUSTADORES)
					&& horarioAjustadorFiltro.getAjustadoresIds().isEmpty()) {
				addFieldError("correoRolesSeleccionados",
						"Seleccione Ajustadores de la lista arriba");
			}
		}

		if (correoFormaDeEnvioCode == null
				|| correoFormaDeEnvioCode.equalsIgnoreCase("")) {
			addFieldError("correoFormaDeEnvioCode", msgCampoRequerido);
		}

		super.validate();
	}

	// endregion validates

	// region Private functions
	private void catchExceptionAndAddErrors(Exception ex, Class<?> clazz) {
		super.setMensajeError(MENSAJE_ERROR_GENERAL);
		if (ex.getCause() instanceof ConstraintViolationException) {
			super.setMensajeError(MENSAJE_ERROR_VALDACION);
			ConstraintViolationException constraintViolationException = (ConstraintViolationException) ex
					.getCause();
			addErrors(constraintViolationException.getConstraintViolations(),
					clazz.getName());
		}
		return;
	}

	private void catchNegocioEJBExeption(NegocioEJBExeption ex) {
		String[] mensajesArray = Arrays.copyOf(ex.getValues(),
				ex.getValues().length, String[].class);
		List<String> mensajes = Arrays.asList(mensajesArray);
		if (mensajes.size() > 0) {
			// super.setMensajeError(mensaje)
			super.setMensajeListaPersonalizado("Mensajes de Validación",
					mensajes, TIPO_MENSAJE_INFORMACION);
		}
		return;
	}

	private void cargarCatalogosEnGrid() {
		if (HorarioLaboral.ctgHorarios.size() == 0)
			HorarioLaboral.ctgHorarios = listadoService
					.obtenerCatalogoValorFijo(HorarioLaboral.ctgHorariosTipo);
		if (HorarioAjustador.ctgDisponibilidad.size() == 0)
			HorarioAjustador.ctgDisponibilidad = (ctgTipoDisponibilidad != null) ? ctgTipoDisponibilidad
					: listadoService
							.obtenerCatalogoValorFijo(HorarioAjustador.ctgDisponibilidadTipo);
	}

	private byte[] generarExcel(HorarioAjustadorFiltro filtro) {
		byte[] bytes = null;
		
		List<ListadoDeDisponibilidadDTO> listadoDeDisponidilidad = horarioAjustadorService.listar(filtro);
		
		ExcelExporter exporter = new ExcelExporter(ListadoDeDisponibilidadDTO.class);

		InputStream stream = exporter.export(listadoDeDisponidilidad);

		try {
			bytes = IOUtils.toByteArray(stream);
		} catch (IOException e) {

		}
		return bytes;
	}
	// endregion Private functions

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	public List<HorarioAjustador> getListadoHorariosBorrados() {
		return listadoHorariosBorrados;
	}

	public void setListadoHorariosBorrados(
			List<HorarioAjustador> listadoHorariosBorrados) {
		this.listadoHorariosBorrados = listadoHorariosBorrados;
	}

	public List<String> getSelected() {
		return selected;
	}

	public void setSelected(List<String> selected) {
		this.selected = selected;
	}
	
	
}
