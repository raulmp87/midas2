package mx.com.afirme.midas2.action;

import java.beans.Expression;
import java.beans.PropertyDescriptor;
import java.beans.Statement;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.Validator;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.cometd.bayeux.server.BayeuxServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.js.util.StringUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport {

	private static final long serialVersionUID = -8425502697277231274L;
	public static final String TIPO_MENSAJE_EXITO = "30";
	public static final String TIPO_MENSAJE_ERROR = "10";
	public static final String TIPO_MENSAJE_INFORMACION = "20";
	public static final String TIPO_MENSAJE_NONE = "none";
	public static final String MENSAJE_EXITO = "Acci\u00F3n realizada correctamente";
	public static final String MENSAJE_ERROR_GENERAL = "Ocurri\u00F3 un error inesperado";
	public static final String MENSAJE_ERROR_CONSTRAIN = "Ocurri\u00F3 un error de base datos, causas comunes:<br><ul><li>Registro duplicado</li><li>Registro con dependencias</li><li>Registro no cumple con la longitud esperada</li></ul>";
	public static final String MENSAJE_ERROR_ACTUALIZAR = "No es posible actualizar el registro";
	public static final String MENSAJE_ERROR_ELIMINAR = "No es posible eliminar el registro";
	public static final String MENSAJE_ERROR_VALDACION= "Ocurri\u00F3 un error de validaci\u00F3n en los datos enviados";
	public static final String MENSAJE_ERROR_OPTIMISTIC_LOCK = "Desde la ultima vez que se leyeron los datos han habido cambios, no es posible guardar.";
	public static final String MENSAJE_EXITO_SOL_INCOMPLETA = "Acci\u00F3n realizada correctamente. La solicitud ser\u00E1 completada cuando agregue un comentario y un documento.";
	public static final String MENSAJE_EXITO_ENDOSO = "Acci\u00F3n realizada correctamente.<br>Endoso n\u00Famero: ";
	public MailService mailService;
	public UsuarioService usuarioService;
	public SistemaContext sistemaContext;
	private Long totalCount;
	private Integer posStart;
	private Integer posActual;
	private Integer posFinish;
	private Integer paginaSig;
	private Integer paginaAnt;
	private List<Integer> pages = new ArrayList<Integer>(1);
	private String funcionPaginar;
	private String divGridPaginar;
	public static final int REGISTROS_A_MOSTRAR = 20;
	public static final String LISTADO_PAGINADO_ANTERIOR = "listadoPaginadoAnt";
	public static final String LISTADO_PAGINADO_ACTUAL = "listadoPaginadoAct";
	public static final String PARAM_PAGINADO_ANTERIOR = "paramPaginadoAnt";
	public static final String PARAM_PAGINADO_ACTUAL = "paramPaginadoAct";
	public static final String MAPA_PAGINAS = "pagesMap";
	public static final String PAGINADO_GRID = "/jsp//paginadoGrid.jsp";
	public static final String MAPA_RETORNO = "returnMap";
	private int esRetorno = 0;
	private Validator validator;
	private String tipoMensaje;
	private String mensaje;
	private String nextFunction;
	private String actionNameOrigen;
	private String namespaceOrigen;
	private String elementosSeleccionados;
	private String uuid;
	protected boolean cleanCache = false;
	public static final String ROW_INSERTED = "inserted";
	public static final String ROW_DELETED = "deleted";
	public static final String ROW_UPDATED = "updated";
	protected static final String ACTION_PARAM_CONTENT_TYPE = "contentType";
	protected static final String ACTION_PARAM_INPUT_NAME = "inputName";
	protected static final String ACTION_PARAM_CONTENT_DISPOSITION = "contentDisposition";
	protected static final String ACTION_PARAM_STREAM = "stream";
	private Map<String, String> fieldErrorAliases = new HashMap<String, String>();
	
	private boolean esExterno;

	public BaseAction() {
		this.tipoMensaje = TIPO_MENSAJE_NONE;
	}

	public Map<String, Object> getSession() {
		return ServletActionContext.getContext().getSession();
	}

	public void addErrors(Object obj, Class<?> groupCheck, Action actionName,
			String domainObjectPrefix) {
		Set<ConstraintViolation<Object>> constraintViolations;
		if (groupCheck != null) {
			constraintViolations = (Set<ConstraintViolation<Object>>) validator
					.validate(obj, groupCheck);
		} else {
			constraintViolations = (Set<ConstraintViolation<Object>>) validator
					.validate(obj);
		}
		StringBuilder errorName = new StringBuilder("");
		for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
			// Add erros using the prefix and exclude the prefix for those who
			// are properties of the action.
			System.out.println("******* add error");
			String errorMessage = constraintViolation.getMessage();
			System.out.println("******* error message " + errorMessage);
			Path propertyPath = constraintViolation.getPropertyPath();
			System.out.println("****** path property error " + propertyPath);
			errorName.delete(0,errorName.length());
			errorName.append(propertyPath.toString());
			if (!StringUtils.isBlank(domainObjectPrefix) && this.addDomainObjectPrefix(actionName, errorName.toString())) {
				errorName.insert(0, domainObjectPrefix);
				errorName.insert(domainObjectPrefix.length(), ".");
			}
			addFieldError(translateFieldErrorName(errorName.toString()), errorMessage);
		}
	}
	
	public void addErrors(Set<ConstraintViolation<?>> constraintViolations) {
		for (ConstraintViolation<?> constraintViolation : constraintViolations) {
			String errorMessage = constraintViolation.getMessage();
			Path propertyPath = constraintViolation.getPropertyPath();
			String errorName = propertyPath.toString();
			addFieldError(translateFieldErrorName(errorName), errorMessage);
		}
	}
	
	public void addErrors(Set<ConstraintViolation<?>> constraintViolations, String domainObjectPrefix) {
		for (ConstraintViolation<?> constraintViolation : constraintViolations) {
			String errorMessage = constraintViolation.getMessage();
			Path propertyPath = constraintViolation.getPropertyPath();
			String errorName = propertyPath.toString();
			if(!StringUtil.isEmpty(domainObjectPrefix)){
				errorName = domainObjectPrefix.concat(".").concat(errorName);
			}
			addFieldError(translateFieldErrorName(errorName), errorMessage);
		}
	}
	
	private String translateFieldErrorName(String fieldErrorName) {
		String fieldErrorAlias = fieldErrorAliases.get(fieldErrorName);
		if (fieldErrorAlias != null) {
			return fieldErrorAlias;
		}
		return fieldErrorName;
	}

	public void addErrors(Object obj, Action actionName,
			String domainObjectPrefix) {
		addErrors(obj, null, actionName, domainObjectPrefix);
	}

	public boolean addDomainObjectPrefix(Action action, String propertyName) {
		try {
			PropertyDescriptor propertyDescriptor = PropertyUtils
					.getPropertyDescriptor(action,
							getLastPropertyName(propertyName));
			if (propertyDescriptor == null) {
				return true;
			}
			return false;
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void addErrors(ApplicationException exception) {
		for (String error : exception.getErrors()) {
			addActionError(error);
		}

		for(Entry<String, List<String>> entry : exception.getFieldErrors().entrySet()) {
			String fieldName = entry.getKey();
			for (String fieldError : entry.getValue()) {
				addFieldError(translateFieldErrorName(fieldName), fieldError);
			}
		}
	}

	public String getLastPropertyName(String name) {
		int index = name.lastIndexOf(".");
		if (index == -1) {
			return name;
		}
		return name.substring(index + 1);
	}

	public void setMensajeExito() {
		setTipoMensaje(BaseAction.TIPO_MENSAJE_EXITO);
		setMensaje(BaseAction.MENSAJE_EXITO);
	}

	public void setMensajeExitoPersonalizado(String mensaje) {
		setTipoMensaje(BaseAction.TIPO_MENSAJE_EXITO);
		setMensaje(mensaje);
	}

	public void setMensajeError(String mensaje) {
		this.setTipoMensaje(BaseAction.TIPO_MENSAJE_ERROR);
		this.setMensaje(mensaje);
	}
	public void setMensajeListaPersonalizado(String encabezado,
			List<String> mensajes, String tipoMensaje) {
		this.setTipoMensaje(tipoMensaje);
		this.setMensaje(obtenerMensajeListaPersonalizado(encabezado, mensajes));

	}

	public String obtenerMensajeListaPersonalizado(String encabezado,List<String> mensajes) {
		StringBuilder msg = new StringBuilder();
		msg.append(encabezado).append("\n").append("\u003Cul\u003E\n");
		for (String str : mensajes) {
			msg.append("\u003Cli\u003E").append(str).append("\u003C/li\u003E\n");
		}
		msg.append("\u003C/ul\u003E");
		return msg.toString();
	}

	public void setMensajeMapaErrorPersonalizado(String encabezado,
			List<Map<String, String>> mensajes, String tipoMensaje) {
		StringBuilder msg = new StringBuilder(encabezado);
		msg.append("\n");
		String recurso = "mx.com.afirme.midas.RecursoDeMensajes";
		boolean errorGeneral = false;
		for (Map<String, String> mapa : mensajes) {
			for (String str : mapa.keySet()) {
				String error = UtileriasWeb.getMensajeRecurso(recurso,
						mapa.get(str));
				addFieldError(str, error);
				if ("exception.unknownError".equals(str)) {
					errorGeneral = true;
					msg.append("-").append(error).append("\n");
				}
			}
		}
		if (errorGeneral) {
			this.setMensaje(msg.toString());
			this.setTipoMensaje(tipoMensaje);
		}
	}

	public String getMensajeErrorGlobal(String mensaje) {
		return UtileriasWeb.getMensajeRecurso(sistemaContext.getArchivoRecursosFront(), mensaje);
	}

	public Usuario getUsuarioActual() {
		return usuarioService.getUsuarioActual();
	}

	public void setPosPaginado() {
		if (getPosActual() == null) {
			setPosActual(1);
		}
		setPosStart((getPosActual() - 1) * REGISTROS_A_MOSTRAR);
		setPosFinish(getPosStart() + REGISTROS_A_MOSTRAR);
		if (getPosFinish() > getTotalCount()) {
			setPosFinish(getTotalCount().intValue());
		}
		setPagesPaginado();
	}
	
	public void setPosPaginado(int registrosAMostrar) {
		if (getPosActual() == null) {
			setPosActual(1);
		}
		setPosStart((getPosActual() - 1) * registrosAMostrar);
		setPosFinish(getPosStart() + registrosAMostrar);
		if (getPosFinish() > getTotalCount()) {
			setPosFinish(getTotalCount().intValue());
		}
		setPagesPaginado(registrosAMostrar);
	}

	public void setPagesPaginado() {
		int pageIni = 1;
		try {
			if(getPosActual() == null){ setPosActual(1);}
			if(getPaginaSig() == null){ setPaginaSig(1);}
			if (getPosActual().intValue() == getPaginaSig().intValue()) {
				pageIni = getPosActual().intValue();
			} else if (getPosActual().intValue() == getPaginaAnt().intValue()) {
				pageIni = getPosActual().intValue() - 4;
			} else if (getPaginaAnt() != null && getPosActual().intValue() > getPaginaAnt().intValue()) {
				pageIni = getPaginaAnt().intValue() + 1;
			}
		}catch(Exception e){
			
		}
		double totalPages = Math.ceil((double) getTotalCount()
				/ (double) REGISTROS_A_MOSTRAR);
		double pageShow = 5;
		if (totalPages < pageShow) {
			pageShow = totalPages;
			setPaginaSig(0);
			setPaginaAnt(0);
		} else {
			if (pageIni > 1) {
				setPaginaAnt(pageIni - 1);
			} else {
				setPaginaAnt(0);
			}
			if (pageIni + 4 < totalPages) {
				setPaginaSig(pageIni + 5);
			} else {
				setPaginaSig(0);
			}
			if (pageIni + 4 > totalPages) {
				pageShow = totalPages - pageIni + 1;
			}
		}

		for (int i = 1; i <= pageShow; i++) {
			getPages().add(pageIni);
			pageIni++;
		}
	}
	
	public void setPagesPaginado(int registrosAMostrar) {
		int pageIni = 1;
		if (getPosActual() == getPaginaSig()) {
			pageIni = getPosActual();
		} else if (getPosActual() == getPaginaAnt()) {
			pageIni = getPosActual() - 4;
		} else if (getPaginaAnt() != null && getPosActual() > getPaginaAnt()) {
			pageIni = getPaginaAnt() + 1;
		}
		double totalPages = Math.ceil((double) getTotalCount()
				/ (double) registrosAMostrar);
		double pageShow = 5;
		if (totalPages < pageShow) {
			pageShow = totalPages;
			setPaginaSig(0);
			setPaginaAnt(0);
		} else {
			if (pageIni > 1) {
				setPaginaAnt(pageIni - 1);
			} else {
				setPaginaAnt(0);
			}
			if (pageIni + 4 < totalPages) {
				setPaginaSig(pageIni + 5);
			} else {
				setPaginaSig(0);
			}
			if (pageIni + 4 > totalPages) {
				pageShow = totalPages - pageIni + 1;
			}
		}

		for (int i = 1; i <= pageShow; i++) {
			getPages().add(pageIni);
			pageIni++;
		}
	}

	public Boolean setMapEnSession(Map<Integer, List<?>> paginas) {
		if (paginas != null) {
			try {
				ServletActionContext.getRequest().getSession()
						.setAttribute(MAPA_PAGINAS, paginas);
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, List<?>> getMapEnSession() {
		return (Map<Integer, List<?>>) ServletActionContext.getRequest()
				.getSession().getAttribute(MAPA_PAGINAS);
	}

	public Boolean removeMapEnSession() {
		try {
			ServletActionContext.getRequest().getSession()
					.removeAttribute(MAPA_PAGINAS);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public boolean listadoDeCache() {
		String param = (String) ServletActionContext.getRequest().getSession()
				.getAttribute(PARAM_PAGINADO_ANTERIOR);
		if (param == null
				|| !param.equals(getParamListado())
				|| ServletActionContext.getRequest().getSession()
						.getAttribute(LISTADO_PAGINADO_ANTERIOR) == null) {
			return false;
		}
		return true;
	}

	private String getParamListado() {
		return (new StringBuilder()).append(getPosActual()).append("_")
				.append(getTotalCount()).append("_")
				.append(getDivGridPaginar()).append("_")
				.append(getFuncionPaginar()).toString();
	}
	
	public Object getListadoPaginado() {
		return ServletActionContext.getRequest().getSession()
				.getAttribute(LISTADO_PAGINADO_ANTERIOR);
	}

	public void removeListadoEnSession() {
		ServletActionContext.getRequest().getSession()
				.removeAttribute(LISTADO_PAGINADO_ACTUAL);
		ServletActionContext.getRequest().getSession()
				.removeAttribute(LISTADO_PAGINADO_ANTERIOR);
		ServletActionContext.getRequest().getSession()
				.removeAttribute(PARAM_PAGINADO_ANTERIOR);
		ServletActionContext.getRequest().getSession()
				.removeAttribute(PARAM_PAGINADO_ACTUAL);
	}

	public void setListadoEnSession(Object listado) {
		if (listado == null) {
			removeListadoEnSession();

		} else {
			if (ServletActionContext.getRequest().getSession()
					.getAttribute(LISTADO_PAGINADO_ACTUAL) != null) {
				ServletActionContext
						.getRequest()
						.getSession()
						.setAttribute(
								LISTADO_PAGINADO_ANTERIOR,
								ServletActionContext.getRequest().getSession()
										.getAttribute(LISTADO_PAGINADO_ACTUAL));
				ServletActionContext
						.getRequest()
						.getSession()
						.setAttribute(
								PARAM_PAGINADO_ANTERIOR,
								ServletActionContext.getRequest().getSession()
										.getAttribute(PARAM_PAGINADO_ACTUAL));
			} else {
				ServletActionContext.getRequest().getSession()
						.setAttribute(LISTADO_PAGINADO_ANTERIOR, null);
				ServletActionContext.getRequest().getSession()
						.setAttribute(PARAM_PAGINADO_ANTERIOR, null);
			}
			ServletActionContext.getRequest().getSession()
					.setAttribute(LISTADO_PAGINADO_ACTUAL, listado);
			ServletActionContext.getRequest().getSession()
					.setAttribute(PARAM_PAGINADO_ACTUAL, getParamListado());
		}
	}

	public String getNextFunction() {
		return nextFunction;
	}

	public void setNextFunction(String nextFunction) {
		this.nextFunction = nextFunction;
	}

	public String getTipoMensaje() {
		return tipoMensaje;
	}

	public void setTipoMensaje(String tipoMensaje) {
		if (!tipoMensaje.equals("-1")) {
			this.tipoMensaje = tipoMensaje;
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		if (!mensaje.equals("-1")) {
			this.mensaje = mensaje;
		}

	}
	
	//Obtiene y guarda campos de pantalla anterior
	public void saveReturnFields(Class<?> c, Object object){
		try {
			Field[] fields = c.getDeclaredFields();
			Method[] methods = c.getDeclaredMethods();
			
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			if(fields != null && fields.length > 0){
				for(Field field : fields){
					if(!field.getType().getName().equals(List.class.getName()) && 
							!field.getType().getName().equals(Map.class.getName())){
					for(Method method: methods){
						String getMethod = "get" + field.getName();
						if(method.getName().toUpperCase().equals(getMethod.toUpperCase())){
							//Get the value 
							try{
								Expression expr = new Expression(object, method.getName(), new Object[0]);
								expr.execute();
								Object o = expr.getValue();				            
				            	if(o != null){
				            		map.put(field.getName(), o);
				            	}
				            	}catch(Exception ex){
				            		System.out.println(field.getName() + " = " + ex.getLocalizedMessage());
				            	}
				            break;
						}
					}
					}
				}
				
				//Guarda map en session
				if (ServletActionContext.getRequest().getSession().getAttribute(MAPA_RETORNO) != null) {
					ServletActionContext.getRequest().getSession().removeAttribute(MAPA_RETORNO);
				}
				ServletActionContext.getRequest().getSession().setAttribute(MAPA_RETORNO, map);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//Obtiene campos de session
	@SuppressWarnings("unchecked")
	public void getReturnFields(Class<?> c, Object object){
		if(this.getEsRetorno() == 0){
			return;
		}
		if (ServletActionContext.getRequest().getSession().getAttribute(MAPA_RETORNO) != null) {
			Map<String, Object> map = (Map<String, Object>) ServletActionContext.getRequest().getSession().getAttribute(MAPA_RETORNO);
			try {
				Field[] fields = c.getDeclaredFields();
				Method[] methods = c.getDeclaredMethods();
				if(fields != null && fields.length > 0){
					for(Field field : fields){
						if(map.containsKey(field.getName())){
							try{
							Object o = map.get(field.getName());				
							// Set the value 
							for(Method method : methods){
								//Set method
								String setMethod = "SET" + field.getName();
								if(method.getName().toUpperCase().equals(setMethod.toUpperCase())){
									Statement stmt = new Statement(object, method.getName(), new Object[]{o});
									stmt.execute();
									break;
								}
							}
							}catch(Exception ex){
								System.out.println(field.getName() + " = " + ex.getLocalizedMessage());
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			//Remueve parametros de session
			ServletActionContext.getRequest().getSession().removeAttribute(MAPA_RETORNO);
		}
	}
	

	@Autowired
	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	@Autowired
	@Qualifier("mailServiceEJB")
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Autowired
	@Qualifier("sistemaContextEJB")
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getPosStart() {
		return posStart;
	}

	public void setPosActual(Integer posActual) {
		this.posActual = posActual;
	}

	public Integer getPosActual() {
		return posActual;
	}

	public void setPosFinish(Integer posFinish) {
		this.posFinish = posFinish;
	}

	public Integer getPosFinish() {
		return posFinish;
	}

	public void setPages(List<Integer> pages) {
		this.pages = pages;
	}

	public List<Integer> getPages() {
		return pages;
	}

	public void setDivGridPaginar(String divGridPaginar) {
		this.divGridPaginar = divGridPaginar;
	}

	public String getDivGridPaginar() {
		return divGridPaginar;
	}

	public void setFuncionPaginar(String funcionPaginar) {
		this.funcionPaginar = funcionPaginar;
	}

	public String getFuncionPaginar() {
		return funcionPaginar;
	}

	public void setPaginaSig(Integer paginaSig) {
		this.paginaSig = paginaSig;
	}

	public Integer getPaginaSig() {
		return paginaSig;
	}

	public void setPaginaAnt(Integer paginaAnt) {
		this.paginaAnt = paginaAnt;
	}

	public Integer getPaginaAnt() {
		return paginaAnt;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}
	
	public String getElementosSeleccionados() {
		return elementosSeleccionados;
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setElementosSeleccionados(String elementosSeleccionados) {
		String delimitador = ",";
		String delimitadorNoSeleccionados = "X";
		String marcaQuitarSeleccion = "-";
		
		String[] elementos = null;
		String[] elementosDeseleccionados = null;
		
		List<String> elementosDistintos = new ArrayList<String>();
		List<String> elementosSinSeleccion = new ArrayList<String>();
		
		String elementosSeleccionadosDistintos = "";
		
		if (elementosSeleccionados != null) {
			
			if (elementosSeleccionados.indexOf(delimitadorNoSeleccionados) > -1) {
				String elementosNoSeleccionados = elementosSeleccionados.substring(0, elementosSeleccionados.indexOf(delimitadorNoSeleccionados));
				elementosSeleccionados = elementosSeleccionados.substring(elementosSeleccionados.indexOf(delimitadorNoSeleccionados) +1);
				//En caso que tenga un delimitador "extra"
				if (elementosSeleccionados.indexOf(delimitadorNoSeleccionados) > -1) {
					elementosSeleccionados = elementosSeleccionados.substring(0, elementosSeleccionados.indexOf(delimitadorNoSeleccionados));
				}
				
				elementosNoSeleccionados = elementosNoSeleccionados.replaceAll(delimitador + " ", "");
				
				if(!elementosNoSeleccionados.isEmpty()) {
					elementosDeseleccionados = elementosNoSeleccionados.split(delimitador);
					
					for (String elementoDeseleccionado : elementosDeseleccionados) {
						if (!elementoDeseleccionado.trim().equals("")) {
							elementosSinSeleccion.add(elementoDeseleccionado);
						}
					}
				}
			}
				
			elementosSeleccionados = elementosSeleccionados.replaceAll(delimitador + " ", "");
			if(!elementosSeleccionados.isEmpty()) {
				elementos = elementosSeleccionados.split(delimitador);
				
				for (String elemento : elementos) {
					if (!elemento.trim().equals("") && !elementosDistintos.contains(elemento)) {
						
						if(!elementosSinSeleccion.contains(elemento)) {
							elementosDistintos.add(elemento);
						} else {
							elementosDistintos.add(marcaQuitarSeleccion + elemento);
						}
					}
				}

			}	
		}
		this.elementosSeleccionados =obtenerElementoSeleccionadosDistintos(elementosDistintos,delimitador,marcaQuitarSeleccion);// elementosSeleccionadosDistintos;
	}

	public String obtenerElementoSeleccionadosDistintos(
			List<String> elementosDistintos, String delimitador, String marcaQuitarSeleccion) {
		StringBuilder elementosSeleccionadosDistinto = new StringBuilder("");
		for (String elemento : elementosDistintos) {

			if (elemento.indexOf(marcaQuitarSeleccion) > -1
					&& elementosDistintos.contains(elemento.substring(elemento.indexOf(marcaQuitarSeleccion) + 1))) continue;
			elementosSeleccionadosDistinto.append(elemento).append(delimitador);
		}
		return elementosSeleccionadosDistinto.toString();
	}

	/**
	 * @return the cleanCache
	 */
	public boolean isCleanCache() {
		return cleanCache;
	}

	/**
	 * @param cleanCache the cleanCache to set
	 */
	public void setCleanCache(boolean cleanCache) {
		this.cleanCache = cleanCache;
	}

	public void setEsRetorno(int esRetorno) {
		this.esRetorno = esRetorno;
	}

	public int getEsRetorno() {
		return esRetorno;
	}
	
	/**
	 * Indicates if the action was executed sucessfully.
	 * @return !hasErrors()
	 */
	public boolean isSuccess() {
		return !hasErrors();
	}
	
	/**
	 * Este metodo es util cuando deseamos utilizar los mensajes de "mensajesHeader.jsp", actualmente
	 * este opera utilizando mensaje y tipoMensaje. Para que los actionErrors se muestren en la ventana
	 * es este metodo los convierte a una cadena y despues lo asigna como mensaje de error (<code>setMensajeError</code>).
	 */
	public void convertirErrorsAMensajeError() {
		if (getActionErrors().size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (String actionError : getActionErrors()) {
				sb.append(actionError);
				sb.append("<br/>");
			}
			
			String mensaje = !StringUtils.isBlank(getMensaje()) ? getMensaje() + "<br/>": "";  
			setMensajeError(mensaje + sb.toString());
		}
	}
	
	/**
	 * Metodo de conveniencia para sintetizar el uso de cometd para 'empujar' un mensaje al cliente
	 * @param msg Mensaje a enviar
	 * @param channelName Nombre del canal que esta a la escucha del mensaje
	 */
	public void push(String msg, String channelName) {
		
		ServletContext sc = ServletActionContext.getServletContext();
		BayeuxServer bayeux = (BayeuxServer) sc.getAttribute(BayeuxServer.ATTRIBUTE);
		ExternalEventBroadcaster broadcaster = new ExternalEventBroadcaster(bayeux);
		broadcaster.onExternalEvent(msg, channelName);		
		
	}
	
	
	////
	//El total de registros a devolver en una consulta
	private Integer total;
		
    //Numero de registros a cargar en el set de datos
	private Integer count;
	
	//Indice de la columna por la cual se va a ordenar la consulta
	private String orderBy;
	
	//ASC o DESC
	private String direct;
	
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}
	
	public Map<String, String> getFieldErrorAliases() {
		return fieldErrorAliases;
	}
	
	public void setFieldErrorAlias(Map<String, String> fieldErrorAliases) {
		this.fieldErrorAliases = fieldErrorAliases;
	}
	
	public void addFieldErrorAlias(String fieldErrorName, String fieldErrorAlias) {
		fieldErrorAliases.put(fieldErrorName, fieldErrorAlias);
	}

	public void setEsExterno(boolean esExterno) {
		this.esExterno = esExterno;
	}

	public boolean isEsExterno() {
		return esExterno;
	}


}
