package mx.com.afirme.midas2.action.enlace;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster;
import mx.com.afirme.midas2.dto.enlace.CaseConversationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDeviationDTO;
import mx.com.afirme.midas2.service.enlace.CaseService;
import mx.com.afirme.midas2.util.apn.SendAPNs;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.cometd.bayeux.server.BayeuxServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
/**
 * Action principal para la app movil de enlace.
 * 
 * @author rmonper
 *
 */
@Component
@Scope("prototype")
public class EnlaceMovilAction extends BaseAction {

	/** serialVersionUID */
	private static final long serialVersionUID = -5452012150132227525L;
	/** LOG de EnlaceMovilAction */
	private final Logger LOG = Logger.getLogger(EnlaceMovilAction.class);	
	
	@Autowired
	private CaseService caseService;
	
	private List<CaseDTO> cases;
	private String createUser;
	private CaseDTO caso;
	private List<CaseConversationDTO> messages;
	private Object data;
	private CaseDeviationDTO caseDeviation;
	
	private String registrationId;
	private String pathFile;
	private String pw;
	private String msg;
	private String sender;
	private String senderName;
	private String evento;
	private String titleMsg;
	
	/**
	 * /rest/enlace/list.action
	 * @return json String
	 */
	public String list() {
		try {
			cases = caseService.getActiveCaseByUser(createUser);
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/newCase.action
	 * @return json String
	 */
	public String newCase() {
		try {
			final Long id = caseService.newCase(caso.getCaseTypeId(), caso.getCaseSeverity(), caso.getDescription(), createUser);
			caso.setId(id);
			advertiseByReverseAjaxAssign();
			return SUCCESS;
		} catch (Exception e) {
			LOG.error("--> Ocurrió un error al agregar un nuevo caso...\n"+e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/addMessage.action
	 * @return json String
	 */
	public String addMessage() {
		try {
			caseService.addMessage(caso.getId(), caso.getDescription(), createUser);
			advertiseByReverseAjaxAddMessage();
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/getMessagesList.action
	 * @return json String
	 */
	public String getMessagesList() {
		try {
			messages = caseService.getMessages(caso.getId());
			caso = caseService.findById(caso.getId());
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/getCaseTypesList.action
	 * @return json String
	 */
	public String getCaseTypesList() {
		try {
			data = caseService.getCaseTypes();
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	/**
	 * /rest/enlace/addDeviation.action
	 * @return json String
	 */
	public String addDeviation() {
		try {
			data = caseService.addDeviation(caseDeviation.getCreateUser(),
					caseDeviation.getDeviationUser(),
					caseDeviation.getIniDate(), caseDeviation.getEndDate());
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/getDeviation.action
	 * @return json String
	 */
	public String getDeviation() {
		try {
			data = caseService.getDeviationUserList(caseDeviation.getCreateUser());
			caseDeviation = caseService.getDeviation(caseDeviation.getCreateUser());
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/closeCase.action
	 * @return json String
	 */
	public String closeCase() {
		try {
			caseService.closeCase(caso.getId(), createUser);
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/enlace/reassign.action
	 * @return json String
	 */
	public String reassign() {
		try {
			caseService.reassign(caso.getId(), createUser);
			advertiseByReverseAjaxReassign();
			advertiseByReverseAjaxChangeStatus();
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	
	
	private void advertiseByReverseAjaxAddMessage() {
		final CaseDTO casoDB = caseService.findById(caso.getId());
		final Map<String, String> datos = new HashMap<String, String>();
		datos.put("caseId", String.valueOf(caso.getId()));
		datos.put("event", "/enlace/addMessage");
		final String notificationChannel = sistemaContext.getNotificationChannel();
		if (caseService.isManager(createUser)) {
			advertiseByReverseAjax(caso.getDescription(), casoDB.getAssignedUser(),
					notificationChannel + casoDB.getCreateUser() , datos);
		} else {
			advertiseByReverseAjax(caso.getDescription(), casoDB.getCreateUser(),
					notificationChannel + casoDB.getAssignedUser(), datos);
		}
	}
	
	private void advertiseByReverseAjaxAssign() {
		final CaseDTO casoDB = caseService.findById(caso.getId());
		final Map<String, String> datos = new HashMap<String, String>();
		datos.put("caseId", String.valueOf(caso.getId()));
		datos.put("event", "/enlace/assign");
		final String notificationChannel = sistemaContext.getNotificationChannel();
		final String message = String.format(
				"Se le ha asignado el caso %s para su atención", caso.getId());
		advertiseByReverseAjax(message, casoDB.getCreateUser(),
				notificationChannel + casoDB.getAssignedUser(), datos);
	}

	private void advertiseByReverseAjaxReassign() {
		final CaseDTO casoDB = caseService.findById(caso.getId());
		final Map<String, String> datos = new HashMap<String, String>();
		datos.put("caseId", String.valueOf(caso.getId()));
		datos.put("event", "/enlace/reassign");
		final String notificationChannel = sistemaContext.getNotificationChannel();
		final String message = String.format(
				"Se le ha reasignado el caso %s para su atención", caso.getId());
		advertiseByReverseAjax(message, "SISTEMA", notificationChannel + casoDB.getAssignedUser(), datos);
	}

	private void advertiseByReverseAjaxChangeStatus() {
		final CaseDTO casoDB = caseService.findById(caso.getId());
		final String message = String.format(
				"El caso %s ha cambiado a estatus %s", caso.getId(),
				casoDB.getStatusName());
		final Map<String, String> datos = new HashMap<String, String>();
		datos.put("caseId", String.valueOf(caso.getId()));
		datos.put("event", "/enlace/changeStatus");
		final String notificationChannel = sistemaContext.getNotificationChannel();
		advertiseByReverseAjax(message, "SISTEMA", notificationChannel + caso.getCreateUser(), datos);
	}
	
	private void advertiseByReverseAjax(String message, String senderUserName, String channel, Map<String, String> data) {
		final ServletContext sc = ServletActionContext.getServletContext();
		final BayeuxServer bayeux = (BayeuxServer) sc.getAttribute(BayeuxServer.ATTRIBUTE);
		final ExternalEventBroadcaster broadcaster = new ExternalEventBroadcaster(bayeux);
		final Map<String, String> datos = new HashMap<String, String>(data);
		datos.put("sender", usuarioService.buscarUsuarioPorNombreUsuario(senderUserName).getNombreCompleto());
		datos.put("message", message);
		final String json = new Gson().toJson(datos);
		broadcaster.onExternalEvent(json, channel);
	}
	
	/**
	 * Servicio para probar el envio de APNs
	 * 
	 * @return
	 */
	public String sendAPNsTest(){
		LOG.debug(">> testing sendAPNsTest");
		final Map<String, String> datos = new HashMap<String, String>();
		datos.put("caseId", String.valueOf(caso.getId()));
		
		datos.put("message",  msg);
		datos.put("sender", sender);
		datos.put("senderName", senderName);
		datos.put("evento", evento);
		datos.put("title", titleMsg);
		
		SendAPNs apn = new SendAPNs();
		try {
			apn.sendAPN(datos, registrationId, pathFile, pw, msg, "sandbox");
		} catch (Exception e){
			LOG.error(e.getMessage(), e);
		} 
		
		return SUCCESS;
	}

	/*--------------------- Getters & setters ---------------------*/
	public List<CaseDTO> getCases() {
		return cases;
	}

	public void setCases(List<CaseDTO> cases) {
		this.cases = cases;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public CaseDTO getCaso() {
		return caso;
	}

	public void setCaso(CaseDTO caso) {
		this.caso = caso;
	}

	public List<CaseConversationDTO> getMessages() {
		return messages;
	}

	public void setMessages(List<CaseConversationDTO> messages) {
		this.messages = messages;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public CaseDeviationDTO getCaseDeviation() {
		return caseDeviation;
	}

	public void setCaseDeviation(CaseDeviationDTO caseDeviation) {
		this.caseDeviation = caseDeviation;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	/**
	 * @return the pathFile
	 */
	public String getPathFile() {
		return pathFile;
	}

	/**
	 * @param pathFile the pathFile to set
	 */
	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	/**
	 * @return the pw
	 */
	public String getPw() {
		return pw;
	}

	/**
	 * @param pw the pw to set
	 */
	public void setPw(String pw) {
		this.pw = pw;
	}
	
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @return the senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param senderName the senderName to set
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/**
	 * @return the evento
	 */
	public String getEvento() {
		return evento;
	}

	/**
	 * @param evento the evento to set
	 */
	public void setEvento(String evento) {
		this.evento = evento;
	}

	/**
	 * @return the titleMsg
	 */
	public String getTitleMsg() {
		return titleMsg;
	}

	/**
	 * @param titleMsg the titleMsg to set
	 */
	public void setTitleMsg(String titleMsg) {
		this.titleMsg = titleMsg;
	}
}
