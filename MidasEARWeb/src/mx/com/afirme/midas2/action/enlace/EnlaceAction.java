package mx.com.afirme.midas2.action.enlace;

import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.enlace.CaseConversationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDTO;
import mx.com.afirme.midas2.dto.enlace.CaseDeviationDTO;
import mx.com.afirme.midas2.dto.enlace.CaseTypeDTO;
import mx.com.afirme.midas2.service.enlace.CaseService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Action para el módulo de enlace
 * 
 * @author rmonper
 *
 */
@Component
@Scope("prototype")
public class EnlaceAction extends BaseAction {

	/** LOG de EnlaceAction */
	private final Logger LOG = Logger.getLogger(EnlaceAction.class);

	/** serialVersionUID */
	private static final long serialVersionUID = -1131703057844424964L;
	
	@Autowired
	private CaseService caseService;
	
	private List<CaseDTO> cases;
	private String createUser;
	private boolean manager;
	private List<CaseTypeDTO> caseTypes;
	private Usuario usuario;
	private List<Usuario> deviationUsers;
	private CaseDeviationDTO caseDeviation;
	private CaseDTO caso;
	private List<CaseConversationDTO> messages;

	public String main() {
		try {
			cases = caseService.getActiveCaseByUser(usuarioService.getUsuarioActual().getNombreUsuario());
			usuario = usuarioService.getUsuarioActual();
			manager = usuarioService.tieneRol("Rol_Enlace", usuario);
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}

	public String tracing() {
		try {
			messages = caseService.getMessages(caso.getId());
			usuario = usuarioService.getUsuarioActual();
			manager = usuarioService.tieneRol("Rol_Enlace", usuario);
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}

	public String setup() {
		try {
			usuario = usuarioService.getUsuarioActual();
			manager = usuarioService.tieneRol("Rol_Enlace", usuario);
			deviationUsers = caseService.getDeviationUserList(usuario.getNombreUsuario());
			caseDeviation = caseService.getDeviation(usuario.getNombreUsuario());
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}

	public String add() {
		try {
			caseTypes = caseService.getCaseTypes();
			usuario = usuarioService.getUsuarioActual();
			manager = usuarioService.tieneRol("Rol_Enlace", usuario);
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}

	/* ************ Getters & setters ************ */
	public CaseService getCaseService() {
		return caseService;
	}

	public void setCaseService(CaseService caseService) {
		this.caseService = caseService;
	}

	public List<CaseDTO> getCases() {
		return cases;
	}

	public void setCases(List<CaseDTO> cases) {
		this.cases = cases;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public boolean isManager() {
		return manager;
	}

	public void setManager(boolean manager) {
		this.manager = manager;
	}

	public List<CaseTypeDTO> getCaseTypes() {
		return caseTypes;
	}

	public void setCaseTypes(List<CaseTypeDTO> caseTypes) {
		this.caseTypes = caseTypes;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getDeviationUsers() {
		return deviationUsers;
	}

	public void setDeviationUsers(List<Usuario> deviationUsers) {
		this.deviationUsers = deviationUsers;
	}

	public CaseDeviationDTO getCaseDeviation() {
		return caseDeviation;
	}

	public void setCaseDeviation(CaseDeviationDTO caseDeviation) {
		this.caseDeviation = caseDeviation;
	}

	public CaseDTO getCaso() {
		return caso;
	}

	public void setCaso(CaseDTO caso) {
		this.caso = caso;
	}

	public List<CaseConversationDTO> getMessages() {
		return messages;
	}

	public void setMessages(List<CaseConversationDTO> messages) {
		this.messages = messages;
	}
	
}
