package mx.com.afirme.midas2.action.comunicados;


import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.BaseAction;
import org.apache.struts2.ServletActionContext;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import javax.servlet.http.HttpServletRequest;
import mx.com.afirme.midas.sistema.seguridad.Rol;

@Component
@Scope("prototype")
@Namespace("/comunicados")
public class ComunicadosAction extends BaseAction {
	
	private static final long serialVersionUID = 1L;
	
	private static final String CONSOLA= "/jsp/comunicados/consola.jsp";
	private String idSesionUsuario;
	private String nombreUsuario;
	private String roleName;
	
	@Action(value="mostrarConsola",results={@Result( name=SUCCESS, location=CONSOLA)})
	public String mostrarConsola() {
		Usuario usuario=null;
		roleName = "";
		try{
			HttpServletRequest request= ServletActionContext.getRequest();
			usuario = (Usuario) UtileriasWeb.obtenValorSessionScope(request, sistemaContext.getUsuarioAccesoMidas());
			for (Rol rol :usuario.getRoles()){
				if (rol.getDescripcion().trim().compareTo("COMUNICADOS_ADMINISTRADOR") == 0 ||
					rol.getDescripcion().trim().compareTo("COMUNICADOS_ORIGINADOR") == 0){
					roleName = rol.getDescripcion();
				}
			}
			idSesionUsuario = usuario.getIdSesionUsuario();
			nombreUsuario = usuario.getNombreUsuario();
		}catch(Exception ex){
			usuario=null;
			roleName = "";
		}
		return SUCCESS;
	}

	public String getIdSesionUsuario() {
		return idSesionUsuario;
	}

	public void setIdSesionUsuario(String idSesionUsuario) {
		this.idSesionUsuario = idSesionUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
