package mx.com.afirme.midas2.action.catalogos.fuerzaventa.mediciones;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.asm.dto.UserDTO;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.wsc.core.ApplicationException;
import mx.com.afirme.midas.wsc.dto.fuerzaventa.mediciones.ReporteResponse;
import mx.com.afirme.midas.wsc.dto.seguridad.UserParameter;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.fuerzaventa.mediciones.Reporte;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.MedicionesService;

@Component
@Scope("prototype")
@ParentPackage(value ="restServices")
@Namespace("/rest/fuerzaventa/mediciones")
public class MedicionesAction extends BaseAction implements Preparable, ModelDriven<Object>  {

	private static final long serialVersionUID = -8093235058498687556L;
	private Object model;
	private Object data;
	
	
	public void prepareReporteSemanal() {
		model = new UserParameter();
	}
	
	public void prepareEsUsuarioValido() {
		model = new UserParameter();
	}
		
	@Action(value="reporteSemanal")
	public String reporteSemanal() {
		
		UserParameter param = (UserParameter) model;		
		
		UserDTO usuario = usuarioService.getUserDTOById(param.getId());
		
		Reporte reporte = medicionesService.obtenerUltimoReporteSemanal(usuario); 
		
		ReporteResponse reporteResponse = new ReporteResponse();
		
		try {
			
			reporteResponse.setFileName(reporte.getFileName());
			reporteResponse.setContentType(reporte.getContentType());
			reporteResponse.setCorte(reporte.getCorte());
			reporteResponse.setFileBytes(IOUtils.toByteArray(reporte.getInputStream()));
			
		} catch (IOException e) {
			
			throw new ApplicationException("Error al generar el archivo");
			
		}
		
		data = reporteResponse;
		
		return SUCCESS;
	}
	
	@Action(value="esUsuarioValido")
	public String esUsuarioValido () {
		
		UserParameter param = (UserParameter) model;
		
		UserDTO usuario = usuarioService.getUserDTOById(param.getId());
		
		data = medicionesService.validarUsuario(usuario);
		
		return SUCCESS;
		
	}
	
	
	@Autowired
	private MedicionesService medicionesService;

	@Override
	public Object getModel() {
		return model;
	}
	
	public Object getData() {
		return data;
	}

	@Override
	public void prepare() throws Exception {
	}

	
}
