package mx.com.afirme.midas2.action.catalogos.tarifa.agrupador.tarifa;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.RelacionesTarifaAgrupadorTarifaDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;


@Component
@Scope("prototype")
public class TarifaAgrupadorTarifaAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4520127088974367105L;
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public String mostrarCatalogo(){
		mapAgrupadorTarifa = tarifaAgrupadorTarifaService.getMapAgrupadorTarifaPorNegocio(claveNegocio);
		mapLineaNegocio =  new LinkedHashMap<BigDecimal, String>();
		mapMoneda = new LinkedHashMap<Short, String>();
		return SUCCESS;
	}
	
	public String obtenerAsociadas(){
		relacionesTarifaAgrupadorTarifaDTO = tarifaAgrupadorTarifaService.getRelationList(tarifaAgrupadorTarifa, claveNegocio);
		return SUCCESS;
	}
	
	public String obtenerDisponibles(){
		relacionesTarifaAgrupadorTarifaDTO = tarifaAgrupadorTarifaService.getRelationList(tarifaAgrupadorTarifa, claveNegocio);
		return SUCCESS;
	}
	
	public String relacionar(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = tarifaAgrupadorTarifaService.relacionarTarifaAgrupadorTarifa(accion, tarifaAgrupadorTarifa);
		return SUCCESS;
	}
	
	public String coberturasDisponibles(){
		return SUCCESS;
	}
	
	public TarifaAgrupadorTarifaAction(){
		claveNegocioAutos = "A";
	}

	
	private TarifaAgrupadorTarifa tarifaAgrupadorTarifa;
	private Map<AgrupadorTarifaId,String> mapAgrupadorTarifa;
	private Map<BigDecimal, String> mapLineaNegocio;
	private Map<Short, String> mapMoneda;
	private RelacionesTarifaAgrupadorTarifaDTO relacionesTarifaAgrupadorTarifaDTO;
	private RespuestaGridRelacionDTO respuesta;
	private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	private String claveNegocio;
	private String claveNegocioAutos;
	
	


	public String getClaveNegocioAutos() {
		return claveNegocioAutos;
	}

	public void setClaveNegocioAutos(String claveNegocioAutos) {
		this.claveNegocioAutos = claveNegocioAutos;
	}

	public TarifaAgrupadorTarifa getTarifaAgrupadorTarifa() {
		return tarifaAgrupadorTarifa;
	}

	public void setTarifaAgrupadorTarifa(TarifaAgrupadorTarifa tarifaAgrupadorTarifa) {
		this.tarifaAgrupadorTarifa = tarifaAgrupadorTarifa;
	}

	public Map<AgrupadorTarifaId, String> getMapAgrupadorTarifa() {
		return mapAgrupadorTarifa;
	}

	public void setMapAgrupadorTarifa(Map<AgrupadorTarifaId, String> mapAgrupadorTarifa) {
		this.mapAgrupadorTarifa = mapAgrupadorTarifa;
	}

	public Map<BigDecimal, String> getMapLineaNegocio() {
		return mapLineaNegocio;
	}

	public void setMapLineaNegocio(Map<BigDecimal, String> mapLineaNegocio) {
		this.mapLineaNegocio = mapLineaNegocio;
	}

	public Map<Short, String> getMapMoneda() {
		return mapMoneda;
	}

	public void setMapMoneda(Map<Short, String> mapMoneda) {
		this.mapMoneda = mapMoneda;
	}

	public RelacionesTarifaAgrupadorTarifaDTO getRelacionesTarifaAgrupadorTarifaDTO() {
		return relacionesTarifaAgrupadorTarifaDTO;
	}

	public void setRelacionesTarifaAgrupadorTarifaDTO(
			RelacionesTarifaAgrupadorTarifaDTO relacionesTarifaAgrupadorTarifaDTO) {
		this.relacionesTarifaAgrupadorTarifaDTO = relacionesTarifaAgrupadorTarifaDTO;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	
	@Autowired
	@Qualifier("tarifaAgrupadorTarifaServiceEJB")
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}
	


}
