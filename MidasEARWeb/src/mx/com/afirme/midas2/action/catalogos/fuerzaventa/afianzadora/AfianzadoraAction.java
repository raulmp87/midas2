package mx.com.afirme.midas2.action.catalogos.fuerzaventa.afianzadora;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.catalogos.sector.SectorFacadeRemote;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.service.fuerzaventa.AfianzadoraService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/afianzadora")
public class AfianzadoraAction extends CatalogoHistoricoAction{

	private static final long serialVersionUID = 5240456062889181802L;
	
	private Afianzadora afianzadora;
	private List<Afianzadora> listaAfianzadoras=new ArrayList<Afianzadora>();
	private List<SectorDTO> listaSectores=new ArrayList<SectorDTO>();
	private Afianzadora filtroAfianzadora;
	private Domicilio domicilio = new Domicilio();
	private AfianzadoraService afianzadoraService;
	private SectorFacadeRemote sectorFacadeRemote;
	private DomicilioFacadeRemote domicilioFacade;
	
	private String rangoFechas; 
	
	/*********************************************************************************************
	 *  Metodos setter y getter
	 ********************************************************************************************/
	public Afianzadora getAfianzadora() {
		return afianzadora;
	}

	public void setAfianzadora(Afianzadora afianzadora) {
		this.afianzadora = afianzadora;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}		
	
	public Afianzadora getFiltroAfianzadora() {
		return filtroAfianzadora;
	}

	public void setFiltroAfianzadora(Afianzadora filtroAfianzadora) {
		this.filtroAfianzadora = filtroAfianzadora;
	}
	public SectorFacadeRemote getSectorFacadeRemote() {
		return sectorFacadeRemote;
	}	

	public List<SectorDTO> getListaSectores() {
		return listaSectores;
	}

	public void setListaSectores(List<SectorDTO> listaSectores) {
		this.listaSectores = listaSectores;
	}

	public List<Afianzadora> getListaAfianzadoras() {
		return listaAfianzadoras;
	}

	public void setListaAfianzadoras(List<Afianzadora> listaAfianzadoras) {
		this.listaAfianzadoras = listaAfianzadoras;
	}


	public String getRangoFechas() {
		return rangoFechas;
	}

	public void setRangoFechas(String rangoFechas) {
		this.rangoFechas = rangoFechas;
	}

	@Autowired
	@Qualifier("sectorFacadeRemoteEJB")
	public void setSectorFacadeRemote(SectorFacadeRemote sectorFacadeRemote) {
		this.sectorFacadeRemote = sectorFacadeRemote;
	}
	
	@Autowired
	@Qualifier("afianzadoraServiceEJB")
	public void setAfianzadoraService(AfianzadoraService afianzadoraService) {
		this.afianzadoraService = afianzadoraService;
	}

	
	
	/*********************************************************************************************
	 *  Metodos y Acciones del Catalogo
	 ********************************************************************************************/
	
	@Override
	public void prepare() throws Exception {
		if("1".equals(tipoAccion)){
			afianzadora=new Afianzadora();
		}
		if(afianzadora!=null && afianzadora.getId()!=null){
			afianzadora = afianzadoraService.loadById(afianzadora);
		}
		
	}
	
	public void prepareMostrarContenedor(){
		
	}
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraCatalogo.jsp")
		})
		public String mostrarContenedor(){
			return SUCCESS;
		}
	
	@Action(value="lista",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraGrid.jsp")
		})
		@Override
		public String listar() {
			try{
				listaAfianzadoras = afianzadoraService.findByFiltersView(null);
			}catch(Exception e){
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraGrid.jsp")
		})
	@Override
	public String listarFiltrado() {
		try{
			if("consulta".equals(tipoAccion)){
				if(filtroAfianzadora==null){
					filtroAfianzadora=new Afianzadora();
               }
				filtroAfianzadora.setClaveEstatus(1L);
			}
			listaAfianzadoras=afianzadoraService.findByFiltersView(filtroAfianzadora);
			}catch(Exception e){
				e.printStackTrace();
				return INPUT;
			}
		return SUCCESS;
	}
	
	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraDetalle.jsp")
	})
	@Override
	public String verDetalle(){
		if(afianzadora.getFechaAlta()==null){
			afianzadora.setFechaAlta(new Date());
		}		
		setRangoFechas(UtileriasWeb.getFechaString(new Date()));
		listaSectores = sectorFacadeRemote.findAll();
		if(afianzadora!=null && afianzadora.getId()!=null){
			afianzadora = afianzadoraService.loadById(afianzadora);
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/afianzadora/afianzadoraDetalle.jsp")
		})		
		public String verDetalleHistorico(){
			if(afianzadora.getFechaAlta()==null){
				afianzadora.setFechaAlta(new Date());
			}		
			setRangoFechas(UtileriasWeb.getFechaString(new Date()));
			listaSectores = sectorFacadeRemote.findAll();
			if(afianzadora!=null && afianzadora.getId()!=null){
				try {
					afianzadora = afianzadoraService.loadById(afianzadora, this.getUltimaModificacion().getFechaHoraActualizacionString());
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return SUCCESS;
		}

	public String buscarDomicilio(){
		return SUCCESS;
	}
	
	public void validateGuardarAfianzadora(){
		addErrors(afianzadora, this, "afianzadora");
	}
	@Action(value="guardarAfianzadora",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",					
					"namespace","/fuerzaventa/afianzadora",
					"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"afianzadora.id","${afianzadora.id}",
					"idTipoOperacion","${idTipoOperacion}",
					"idRegistro","${afianzadora.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/afianzadora","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${afianzadora.id}"})
	})
	public String guardarAfianzadora(){
		try {			
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			if(afianzadora.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
//			TipoDomicilio tipoDomicilio = domicilioFacade.getTipoDomicilioPorClave("AFIA");
//			Domicilio domicilio=afianzadora.getDomicilio();
//			domicilio.setTipoDomicilio(tipoDomicilio.getValue());
//			Long idDomicilio=domicilioFacade.save(domicilio, null);
//			domicilio.setIdDomicilio(idDomicilio);
//			afianzadora.setDomicilio(domicilio);
			afianzadora=afianzadoraService.saveFull(afianzadora);
			guardarHistorico(TipoOperacionHistorial.AFIANZADORA, afianzadora.getId(),"midas.afianzadora.historial.accion",tipoAccionHistorial);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
			setIdTipoOperacion(80L);
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
			if(afianzadora.getId()!=null){
				tipoAccion="4";//se habilita el boton de actualizar
			}else{
				tipoAccion="1";//se habilita el boton de guardar
			}
			return INPUT;

		}
		 return SUCCESS;
	}
	
	@Action(value="eliminar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/fuerzaventa/afianzadora",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"afianzadora.id","${afianzadora.id}",
							"idTipoOperacion","${idTipoOperacion}",
							"idRegistro","${idRegistro}"
							}),
			 @Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
					 		"namespace","/fuerzaventa/afianzadora",
					 		"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idTipoOperacion","${idTipoOperacion}",
							"idRegistro","${idRegistro}"
							})		
		})
		@Override
	public String eliminar() {
		try {
			setIdRegistro(afianzadora.getId());
			afianzadoraService.unsubscribe(afianzadora);
			guardarHistorico(TipoOperacionHistorial.AFIANZADORA, afianzadora.getId(),"midas.afianzadora.historial.accion",TipoAccionHistorial.BAJA);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="3";	
			setIdTipoOperacion(80L);
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
			tipoAccion="3";
			
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^afianzadora\\.id,^afianzadora\\.razonSocial"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^afianzadora\\.id,^afianzadora\\.razonSocial"})
	})
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	
	
	/*********************************************************************************************
	 *  Otros Metodos
	 ********************************************************************************************/
	
	@Override
	public String guardar() {
		// TODO
		return null;
	}
	@Autowired
	@Qualifier("domicilioFacadeRemoteEJB")
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
		
}