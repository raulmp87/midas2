package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.service.catalogos.EntidadHistoricoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ReporteAgenteBaseAction extends BaseAction {

	public final static String EMPTY_RESULT = "No se obtuvieron agentes con los criterios de busqueda seleccionados.";
	protected CentroOperacionService centroOperacionService;
	protected ValorCatalogoAgentesService valorCatalogoAgentesService;	
	private static final long serialVersionUID = 1L;
	private List<CentroOperacion> centroOperacionList;
	private List<CentroOperacion> centroOperacionesSeleccionados;
	private Long idCentroOperacion;
	private List<Gerencia> gerenciasSeleccionadas;
	private Long idGerencia;
	private List<Ejecutivo> ejecutivosSeleccionados;
	private Long idEjecutivo;
	private List<Promotoria> promotoriasSeleccionadas;
	private Long idPromotoria;
	private List<ValorCatalogoAgentes> tipoPromotoria;
	private List<ValorCatalogoAgentes> tipoPromotoriasSeleccioadas;
	private List<ValorCatalogoAgentes> estatusAgente;
	private List<ValorCatalogoAgentes> estatusAgenteSeleccionados;
	private List<ValorCatalogoAgentes> tipoAgentesSeleccionados;
	private List<ValorCatalogoAgentes> tipoAgente;
	private List<ValorCatalogoAgentes> tipoCedula;
	private Long tipoCedulaSeleccionada;
	private InputStream reporteAgenteStream;
	private InputStream reporteCargosStream;
	private InputStream reporteProvisionStream;
	private String contentType;
	private String fileName;
	private Integer tipoReporte;
	private Locale locale = getLocale();
	private String mensaje;
	private GenerarPlantillaReporte generarPlantillaReporteService;
	private String mes;
	private String anio;
	private String mesFin;
	private String anioFin;
	private Long rangoInicio;
	private Long rangoFin;
	protected EntidadService entidadService;
	private Date fechaInicio;
	private Date fechaFin;
	public static final String TIPOSALIDAARCHIVO = "xlsx";
	public EntidadHistoricoService  entidadHistoricoService;
	public static final String PAGERROR = "/jsp/reportesAgentes/reporteError.jsp";
	
	private static List<ValorCatalogoAgentes> clasificacionAgente;
	private Long idClasificacionAgente;
	
	public Long getIdClasificacionAgente() {
		return idClasificacionAgente;
	}

	public void setIdClasificacionAgente(Long idClasificacionAgente) {
		this.idClasificacionAgente = idClasificacionAgente;
	}

	public String getMesFin() {
		return mesFin;
	}

	public void setMesFin(String mesFin) {
		this.mesFin = mesFin;
	}

	public String getAnioFin() {
		return anioFin;
	}

	public void setAnioFin(String anioFin) {
		this.anioFin = anioFin;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<CentroOperacion> getCentroOperacionList() {
		return centroOperacionList;
	}

	public void setCentroOperacionList(List<CentroOperacion> centroOperacionList) {
		this.centroOperacionList = centroOperacionList;
	}


	public List<Gerencia> getGerenciasSeleccionadas() {
		return gerenciasSeleccionadas;
	}

	public void setGerenciasSeleccionadas(List<Gerencia> gerenciasSeleccionadas) {
		this.gerenciasSeleccionadas = gerenciasSeleccionadas;
	}

	public List<Ejecutivo> getEjecutivosSeleccionados() {
		return ejecutivosSeleccionados;
	}

	public void setEjecutivosSeleccionados(
			List<Ejecutivo> ejecutivosSeleccionados) {
		this.ejecutivosSeleccionados = ejecutivosSeleccionados;
	}

	public List<Promotoria> getPromotoriasSeleccionadas() {
		return promotoriasSeleccionadas;
	}

	public void setPromotoriasSeleccionadas(
			List<Promotoria> promotoriasSeleccionadas) {
		this.promotoriasSeleccionadas = promotoriasSeleccionadas;
	}

	public String getMensaje() {
		return mensaje;
	}

	public static List<ValorCatalogoAgentes> getClasificacionAgente() {
		return clasificacionAgente;
	}

	public static void setClasificacionAgente(
			List<ValorCatalogoAgentes> clasificacionAgente) {
		ReporteAgenteBaseAction.clasificacionAgente = clasificacionAgente;
	}

	@Autowired
	@Qualifier("centroOperacionServiceEJB")
	public void setCentroOperacionService(
			CentroOperacionService centroOperacionService) {
		this.centroOperacionService = centroOperacionService;
	}

	public List<ValorCatalogoAgentes> getTipoPromotoria() {
		return tipoPromotoria;
	}

	public void setTipoPromotoria(List<ValorCatalogoAgentes> tipoPromotoria) {
		this.tipoPromotoria = tipoPromotoria;
	}

	public List<ValorCatalogoAgentes> getTipoPromotoriasSeleccioadas() {
		return tipoPromotoriasSeleccioadas;
	}

	public void setTipoPromotoriasSeleccioadas(
			List<ValorCatalogoAgentes> tipoPromotoriasSeleccioadas) {
		this.tipoPromotoriasSeleccioadas = tipoPromotoriasSeleccioadas;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}

	public List<ValorCatalogoAgentes> getTipoAgentesSeleccionados() {
		return tipoAgentesSeleccionados;
	}

	public void setTipoAgentesSeleccionados(
			List<ValorCatalogoAgentes> tipoAgentesSeleccionados) {
		this.tipoAgentesSeleccionados = tipoAgentesSeleccionados;
	}

	public List<ValorCatalogoAgentes> getEstatusAgente() {
		return estatusAgente;
	}

	public void setEstatusAgente(List<ValorCatalogoAgentes> estatusAgente) {
		this.estatusAgente = estatusAgente;
	}

	public List<ValorCatalogoAgentes> getEstatusAgenteSeleccionados() {
		return estatusAgenteSeleccionados;
	}

	public void setEstatusAgenteSeleccionados(
			List<ValorCatalogoAgentes> estatusAgenteSeleccionados) {
		this.estatusAgenteSeleccionados = estatusAgenteSeleccionados;
	}

	public Long getTipoCedulaSeleccionada() {
		return tipoCedulaSeleccionada;
	}

	public void setTipoCedulaSeleccionada(Long tipoCedulaSeleccionada) {
		this.tipoCedulaSeleccionada = tipoCedulaSeleccionada;
	}

	public InputStream getReporteAgenteStream() {
		return reporteAgenteStream;
	}

	public void setReporteAgenteStream(InputStream reporteAgenteStream) {
		this.reporteAgenteStream = reporteAgenteStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporteService(
			GenerarPlantillaReporte generarPlantillaReporteService) {
		this.generarPlantillaReporteService = generarPlantillaReporteService;
	}

	public Integer getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(Integer tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public List<ValorCatalogoAgentes> getTipoCedula() {
		return tipoCedula;
	}

	public void setTipoCedula(List<ValorCatalogoAgentes> tipoCedula) {
		this.tipoCedula = tipoCedula;
	}

	public List<ValorCatalogoAgentes> getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(List<ValorCatalogoAgentes> tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public GenerarPlantillaReporte getGenerarPlantillaReporteService() {
		return generarPlantillaReporteService;
	}

	public CentroOperacionService getCentroOperacionService() {
		return centroOperacionService;
	}

	public ValorCatalogoAgentesService getValorCatalogoAgentesService() {
		return valorCatalogoAgentesService;
	}

	public InputStream getReporteCargosStream() {
		return reporteCargosStream;
	}

	public void setReporteCargosStream(InputStream reporteCargosStream) {
		this.reporteCargosStream = reporteCargosStream;
	}

	public InputStream getReporteProvisionStream() {
		return reporteProvisionStream;
	}

	public void setReporteProvisionStream(InputStream reporteProvisionStream) {
		this.reporteProvisionStream = reporteProvisionStream;
	}

	public Long getIdCentroOperacion() {
		return idCentroOperacion;
	}

	public void setIdCentroOperacion(Long idCentroOperacion) {
		this.idCentroOperacion = idCentroOperacion;
	}

	public Long getIdGerencia() {
		return idGerencia;
	}

	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}

	public Long getIdEjecutivo() {
		return idEjecutivo;
	}

	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}

	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}
	public List<CentroOperacion> getCentroOperacionesSeleccionados() {
		return centroOperacionesSeleccionados;
	}

	public void setCentroOperacionesSeleccionados(
			List<CentroOperacion> centroOperacionesSeleccionados) {
		this.centroOperacionesSeleccionados = centroOperacionesSeleccionados;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public Long getRangoInicio() {
		return rangoInicio;
	}

	public void setRangoInicio(Long rangoInicio) {
		this.rangoInicio = rangoInicio;
	}

	public Long getRangoFin() {
		return rangoFin;
	}

	public void setRangoFin(Long rangoFin) {
		this.rangoFin = rangoFin;
	}
	
	@Autowired
	@Qualifier("entidadHistoricoEJB")
	public void setEntidadHistoricoService(EntidadHistoricoService entidadHistoricoService) {
		this.entidadHistoricoService = entidadHistoricoService;
	}
	
	protected List<ValorCatalogoAgentes> getValorCatalogoAgentes(String catalogo){
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("grupoCatalogoAgente.descripcion",catalogo);
		return entidadHistoricoService.findByProperties(ValorCatalogoAgentes.class, params);
	}
	
}
