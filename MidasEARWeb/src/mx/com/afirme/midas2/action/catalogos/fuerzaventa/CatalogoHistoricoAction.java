package mx.com.afirme.midas2.action.catalogos.fuerzaventa;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.service.catalogos.EntidadHistoricoService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.opensymphony.xwork2.Preparable;
/**
 * Accion para catalogos que requieran registrar historico
 * @author vmhersil
 *
 */

public abstract class CatalogoHistoricoAction extends BaseAction implements Preparable{
	public static enum TipoAccionHistorial{
		ALTA,BAJA,CAMBIO
	};
	private static final long serialVersionUID = 1L;
	public EntidadHistoricoService  entidadHistoricoService;
	protected ValorCatalogoAgentesService catalogoService;
	private Long idTipoOperacion;
	private Long idRegistro;
	private List<ActualizacionAgente> historial=new ArrayList<ActualizacionAgente>();
	private ActualizacionAgente ultimaModificacion;
	public String tipoAccion;
	private String idField;//Este campo se utilizara solo si se requiere mostrar en un modal el catalogo en juego, el idField
	private String divCarga;
	protected String result=SUCCESS;
	private static ValorCatalogoAgentes estatusActivo;
	private static ValorCatalogoAgentes estatusInactivo;
	private Integer pagingCount=10;
	private Long 	pagingStart;
	private Long	pagingLimit;
	private Long	maxRowNum;
	protected String movimiento;
	protected String proceso;
	private String closeModal;//este campo se utilizara como bandera para que no se cierre el modal abierto
	
	public abstract String guardar();
	
	public abstract String eliminar();
		
	public abstract String listar();
	
	public abstract String listarFiltrado();
		
	public abstract String verDetalle();
	/**
	 * Metodo que carga los valores de un catalogo especifico de la tabla de  TCGRUPOCATALOGOAGENTES
	 * @param catalogo Representa a la descripcion de la tabla de TCGRUPOCATALOGOAGENTES
	 * @return
	 */
	protected List<ValorCatalogoAgentes> cargarCatalogo(String catalogo){
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("grupoCatalogoAgente.descripcion",catalogo);
		return entidadHistoricoService.findByProperties(ValorCatalogoAgentes.class, params);
	}
	/**
	 * Metodo que obtiene los elementos de un catalogo filtrando solamente los que ocupan.
	 * @param nombreCatalogo Descripcion de Midas.tcGrupoCatalogoAgentes
	 * @param valoresEspecificosExcluir Valor de Midas.toValorCatalogoAgentes
	 * @return
	 */
	protected List<ValorCatalogoAgentes> cargarCatalogo(String nombreCatalogo,String... valoresEspecificos){
		List<ValorCatalogoAgentes> catalogo=new ArrayList<ValorCatalogoAgentes>();
		try {
			catalogo=catalogoService.obtenerElementosEspecificosPorCatalogo(nombreCatalogo, valoresEspecificos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return catalogo;
	}
	
	protected List<ValorCatalogoAgentes> cargarCatalogoLightWeight(String catalogo) throws Exception{
		return catalogoService.obtenerElementosPorCatalogo(catalogo);
	}
	/**
	 * Metodo prepare para que cuando se consulte VerDetalle de un action, siempre se cargue su ultima modificacion.
	 * @throws Exception
	 */
	public void prepareVerDetalle()throws Exception{
		TipoOperacionHistorial tipoOperacion=getTipoOperacionPorClave(idTipoOperacion);
		if(tipoOperacion!=null && idRegistro!=null){
			try {
				ultimaModificacion=entidadHistoricoService.getLastUpdate(tipoOperacion, idRegistro);	
				
			} catch (SystemException e) {
				setMensaje(e.getMessage());
			}
		}
	}
	
	public void prepareVerDetalleHistorico(String historyDate)throws Exception{
		TipoOperacionHistorial tipoOperacion=getTipoOperacionPorClave(idTipoOperacion);
		if(tipoOperacion!=null && idRegistro!=null){
			try {
				ultimaModificacion=entidadHistoricoService.getHistoryRecord(tipoOperacion, idRegistro, historyDate);
			} catch (SystemException e) {
				setMensaje(e.getMessage());
			}
		}
	}
	
	protected <E extends Entidad>void setFilterInSession(E filter){
		getSession().put("filtro", filter);
	}
	
	protected <E extends Entidad>E getFilterInSession(Class<E> c){
		Object o=getSession().get("filtro");
		E searchFilter=null;
		if(o!=null && c.isInstance(o)){
			searchFilter=(E)o;
		}
		return searchFilter;
	}
	
	/**
	 * Action: Obtiene la lista de historico de un registro de acuerdo a su tipo de operacion
	 * @return
	 */
	@Action(value="loadHistory",
			results={@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gridHistorial.jsp"),
					 @Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/gridHistorial.jsp")})	
	public String loadHistory(){
		TipoOperacionHistorial tipoOperacion=getTipoOperacionPorClave(idTipoOperacion);
		if(tipoOperacion!=null&& idRegistro!=null){
			try {
				historial=entidadHistoricoService.getHistory(tipoOperacion, idRegistro);
			} catch (SystemException e) {
				setMensaje(e.getMessage());
			}
		}
		return SUCCESS;
	}
	
	@Action(value="mostrarHistorial",
			results={@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/verHistorial.jsp"),
					 @Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/verHistorial.jsp")})	
	public String mostrarHistorial(){
		return SUCCESS;
	}
	/**
	 * Obtiene el tipo de operacion 
	 * @param id
	 * @return
	 */
	private TipoOperacionHistorial getTipoOperacionPorClave(Long id){
		TipoOperacionHistorial tipoOperacion=null;
		for(TipoOperacionHistorial tipo:TipoOperacionHistorial.values()){
			if(tipo.getValue().equals(id)){
				tipoOperacion=tipo;
				break;
			}
		}
		return tipoOperacion;
	}
	
	/**
	 * Guarda en el historial un mensaje de acuerdo a los mensajes de Recurso de mensajes
	 * @param tipoOperacion
	 * @param idRegistro
	 * @param messageResourceKey
	 * @param tipoAccion
	 * @return
	 */
	public ActualizacionAgente guardarHistorico(TipoOperacionHistorial tipoOperacion,Long idRegistro,String messageResourceKey,TipoAccionHistorial tipoAccion){
		Usuario usuario=getUsuarioActual();
		String userName=(usuario!=null)?usuario.getNombreUsuario():null;
		String message=null;
		String tipoMovimiento="";
		switch(tipoAccion){
			case ALTA:
				message=messageResourceKey+".alta";
				tipoMovimiento="ALTA";
			break;
			case BAJA:
				message=messageResourceKey+".baja";
				tipoMovimiento="BAJA";
			break;
			case CAMBIO:
				message=messageResourceKey+".cambio";
				tipoMovimiento="CAMBIO";
			break;
		}
		String comments=getMensajeDelRecurso(message, idRegistro,userName);
		return entidadHistoricoService.saveHistory(tipoOperacion, idRegistro, comments, userName,tipoMovimiento);
	}
	
	public String getMensajeDelRecurso(String key,Object... params){
		return UtileriasWeb.getMensajeRecurso("mx.com.afirme.midas.RecursoDeMensajes", key,params);
	}
	
	protected void onSuccess(){
		setMensaje(MENSAJE_EXITO);
		setTipoMensaje(TIPO_MENSAJE_EXITO);
		result=SUCCESS;
	}
	
	protected void onError(Throwable e){
		onError(e, true);
	}
	
	protected void onError(Throwable e,boolean includeCause){
		
		String mensaje = MENSAJE_ERROR_GENERAL;
		
		if (includeCause) {
			
			String[] msgError = (e.getMessage() != null ? e.getMessage() : MENSAJE_ERROR_GENERAL).split("Exception:");
			mensaje = msgError[msgError.length -1];
			
		}
		
		setMensaje(mensaje);
		setTipoMensaje(TIPO_MENSAJE_ERROR);
		e.printStackTrace();
		
		result=INPUT;
		
	}
	/**
	 * Obtiene el estatus de activo 
	 * @return
	 */
	protected ValorCatalogoAgentes getEstatusActivo(){
		if(estatusActivo==null){
			try {
				estatusActivo=catalogoService.obtenerElementoEspecifico("Tipos de Estatus", "ACTIVO");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return estatusActivo;
	}
	/**
	 * Obtiene el estatus de inactivo
	 * @return
	 */
	protected ValorCatalogoAgentes getEstatusInactivo(){
		if(estatusInactivo==null){
			try {
				estatusInactivo=catalogoService.obtenerElementoEspecifico("Tipos de Estatus", "INACTIVO");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return estatusInactivo;
	}
	
	/***Sets && gets****************************************************/
	@Autowired
	@Qualifier("entidadHistoricoEJB")
	public void setEntidadHistoricoService(EntidadHistoricoService entidadHistoricoService) {
		this.entidadHistoricoService = entidadHistoricoService;
	}	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService){
		this.catalogoService=catalogoService;
	}
	
	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public Long getIdTipoOperacion() {
		return idTipoOperacion;
	}

	public void setIdTipoOperacion(Long idTipoOperacion) {
		this.idTipoOperacion = idTipoOperacion;
	}

	public Long getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}

	public List<ActualizacionAgente> getHistorial() {
		return historial;
	}

	public void setHistorial(List<ActualizacionAgente> historial) {
		this.historial = historial;
	}

	public ActualizacionAgente getUltimaModificacion() {
		return ultimaModificacion;
	}

	public void setUltimaModificacion(ActualizacionAgente ultimaModificacion) {
		this.ultimaModificacion = ultimaModificacion;
	}

	public String getIdField() {
		return idField;
	}

	public void setIdField(String idField) {
		this.idField = idField;
	}

	public String getDivCarga() {
		return divCarga;
	}

	public void setDivCarga(String divCarga) {
		this.divCarga = divCarga;
	}

	public Integer getPagingCount() {
		return pagingCount;
	}

	public void setPagingCount(Integer pagingCount) {
		this.pagingCount = pagingCount;
	}

	public Long getPagingStart() {
		return pagingStart;
	}

	public void setPagingStart(Long pagingStart) {
		this.pagingStart = pagingStart;
	}

	public Long getPagingLimit() {
		pagingLimit=(pagingCount+pagingStart);
		return pagingLimit;
	}

	public void setPagingLimit(Long pagingLimit) {
		this.pagingLimit = pagingLimit;
	}

	public Long getMaxRowNum() {
		return maxRowNum;
	}

	public void setMaxRowNum(Long maxRowNum) {
		this.maxRowNum = maxRowNum;
	}
	public String getCloseModal() {
		return closeModal;
	}

	public void setCloseModal(String closeModal) {
		this.closeModal = closeModal;
	}
}
