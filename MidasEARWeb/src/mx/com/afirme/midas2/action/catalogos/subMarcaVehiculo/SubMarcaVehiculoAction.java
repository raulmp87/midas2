package mx.com.afirme.midas2.action.catalogos.subMarcaVehiculo;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.SubMarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.sistema.ServiceLocator;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.combos.etiqueta.MarcaVehiculo;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService.OficinaFiltro;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;



/**
 * Clase que maneja todas las peticiones del modulo de ordenes de compra
 * @author usuario
 * @version 1.0
 * @created 22-ago-2014 10:43:36 a.m.
 */
@Component
@Scope("prototype")
@Namespace("/catalogos/subMarcaVehiculo")


public class SubMarcaVehiculoAction extends BaseAction implements Preparable{
	

	private SubMarcaVehiculoDTO subMarcaVehiculoDTO = new SubMarcaVehiculoDTO();
	private List<SubMarcaVehiculoDTO> listaSubmarcas;
    private BigDecimal idTcMarcaVehiculo;
	private BigDecimal id;
	private String descripcion;
	private String mensajeMostrar;
	


	public String getMensajeMostrar() {
		return mensajeMostrar;
	}

	public void setMensajeMostrar(String mensajeMostrar) {
		this.mensajeMostrar = mensajeMostrar;
	}

	/*
	 * BANDEJA ORDENES COMPRA
	 * 
	 * */
	private final String LISTADO	="/jsp/catalogos/submarcaVehiculo/subMarcaGrid.jsp";
	
	

	 @Action(value="listadoSubmarcas",results={
				@Result(name=SUCCESS,location=LISTADO),
				@Result(name=INPUT,location=LISTADO)
				})
	public String listadoSubmarcas(){
				ServiceLocator serviceLocator = ServiceLocator.getInstance();
				SubMarcaVehiculoFacadeRemote beanRemoto;
				try {
					beanRemoto = serviceLocator.getEJB(SubMarcaVehiculoFacadeRemote.class);
					listaSubmarcas = beanRemoto.findByProperty("marcaVehiculoDTO.idTcMarcaVehiculo",idTcMarcaVehiculo);
				} catch (SystemException e) {
					listaSubmarcas=null;
				}
					
				 return SUCCESS;			
	 }
	 
	 @Action(value="relacionarSubmarcas",results={
				@Result(name=SUCCESS,location=LISTADO),
				@Result(name=INPUT,location=LISTADO)
				})
		public String relacionarSubmarcas(){
		 	ServiceLocator serviceLocator = ServiceLocator.getInstance();
			SubMarcaVehiculoFacadeRemote beanRemoto;	
			MarcaVehiculoFacadeRemote marcaVehiculoFacade;
			SubMarcaVehiculoDTO  dto=null;
			String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
			String grid =ServletActionContext.getRequest().getParameter("gr_id");
			String descripcion=ServletActionContext.getRequest().getParameter("valor");
			String secuencia = ServletActionContext.getRequest().getParameter("numeroSecuencia");
			try {
				beanRemoto = serviceLocator.getEJB(SubMarcaVehiculoFacadeRemote.class);
				if(!StringUtil.isEmpty(secuencia) && StringUtil.isNumeric(secuencia)){
					id= new BigDecimal(secuencia);
					subMarcaVehiculoDTO=beanRemoto.findById(id);	
				}
				if(this.idTcMarcaVehiculo != null && subMarcaVehiculoDTO!=null){
					marcaVehiculoFacade = serviceLocator.getEJB(MarcaVehiculoFacadeRemote.class);
					MarcaVehiculoDTO marcaVehiculoDTO= marcaVehiculoFacade.findById(idTcMarcaVehiculo);	
					subMarcaVehiculoDTO.setMarcaVehiculoDTO(marcaVehiculoDTO);
				}
				if(!StringUtil.isEmpty(descripcion)){
					descripcion=descripcion.toUpperCase();
				}
				subMarcaVehiculoDTO.setDescripcionSubMarcaVehiculo(descripcion);
				if (accion.equals(ROW_INSERTED)) {
					beanRemoto.save(subMarcaVehiculoDTO);
				} else if (accion.equals(ROW_DELETED)) {
					beanRemoto.delete(subMarcaVehiculoDTO);
				}else if (accion.equals(ROW_UPDATED)){

					beanRemoto.update(subMarcaVehiculoDTO);
				}
			} catch (SystemException e) {
				return SUCCESS;
			}
			return SUCCESS;
		}
		
		@Action (value = "validaEliminarRegistro", results={
	            @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeMostrar"}),
	            @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensajeMostrar"})
	      })
		public String validaEliminarRegistro(){
				this.mensajeMostrar=null;
				ServiceLocator serviceLocator = ServiceLocator.getInstance();
				EstiloVehiculoFacadeRemote beanRemoto;
				List<EstiloVehiculoDTO> estilos;
				try {
					beanRemoto = serviceLocator.getEJB(EstiloVehiculoFacadeRemote.class);
					estilos = beanRemoto.findByProperty("subMarcaVehiculoDTO.idSubTcMarcaVehiculo",id);
				} catch (SystemException e) {
					estilos=null;
				}
				if(estilos!=null && !estilos.isEmpty()){
					this.mensajeMostrar=getText("midas.catalogos.submmarca.mensaje");
				}
				 return SUCCESS;			
		}



	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}



	public List<SubMarcaVehiculoDTO> getListaSubmarcas() {
		return listaSubmarcas;
	}



	public void setListaSubmarcas(List<SubMarcaVehiculoDTO> listaSubmarcas) {
		this.listaSubmarcas = listaSubmarcas;
	}



	public BigDecimal getIdTcMarcaVehiculo() {
		return idTcMarcaVehiculo;
	}



	public void setIdTcMarcaVehiculo(BigDecimal idTcMarcaVehiculo) {
		this.idTcMarcaVehiculo = idTcMarcaVehiculo;
	}
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLISTADO() {
		return LISTADO;
	}




}