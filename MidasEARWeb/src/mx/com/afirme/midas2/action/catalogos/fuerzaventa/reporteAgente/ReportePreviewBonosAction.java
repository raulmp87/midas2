package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reportePreviewBonos")
@Component
@Scope("prototype")
public class ReportePreviewBonosAction extends ReporteAgenteBaseAction implements Preparable, ReportMethods {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5597259754595222700L;
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reportePreviewBonos.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private InputStream reportePreviewBonos;
	private List<Gerencia> gerenciaList = new ArrayList<Gerencia>(); 
	private List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
	private List<Promotoria> promorotiaList = new ArrayList<Promotoria>();
	private List <ConfigBonos>listTipoBono = new ArrayList<ConfigBonos>();
	private ConfigBonosService configBonosService;
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	
//	private CentroOperacion centroOperacion = new CentroOperacion();
//	private Gerencia gerencia = new Gerencia();
//	private Ejecutivo ejecutivo = new Ejecutivo();
//	private Promotoria promotoria = new Promotoria();
	private ConfigBonos bono = new ConfigBonos();
	private ValorCatalogoAgentes tipAgente = new ValorCatalogoAgentes();
/***********************************Common methods****************************************/
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public Agente getAgente() {
		return agente;
	}
	
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	public List<ConfigBonos> getListTipoBono() {
	return listTipoBono;
	}
	
	public void setListTipoBono(List<ConfigBonos> listTipoBono) {
		this.listTipoBono = listTipoBono;
	}
	public InputStream getReportePreviewBonos() {
		return reportePreviewBonos;
	}

	public void setReportePreviewBonos(InputStream reportePreviewBonos) {
		this.reportePreviewBonos = reportePreviewBonos;
	}
public List<Gerencia> getGerenciaList() {
		return gerenciaList;
	}

	public void setGerenciaList(List<Gerencia> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}

	public List<Ejecutivo> getEjecutivoList() {
		return ejecutivoList;
	}

	public void setEjecutivoList(List<Ejecutivo> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}

	public List<Promotoria> getPromorotiaList() {
		return promorotiaList;
	}

	public void setPromorotiaList(List<Promotoria> promorotiaList) {
		this.promorotiaList = promorotiaList;
	}

//	public CentroOperacion getCentroOperacion() {
//		return centroOperacion;
//	}
//
//	public void setCentroOperacion(CentroOperacion centroOperacion) {
//		this.centroOperacion = centroOperacion;
//	}
//
//	public Gerencia getGerencia() {
//		return gerencia;
//	}
//
//	public void setGerencia(Gerencia gerencia) {
//		this.gerencia = gerencia;
//	}
//
//	public Ejecutivo getEjecutivo() {
//		return ejecutivo;
//	}
//
//	public void setEjecutivo(Ejecutivo ejecutivo) {
//		this.ejecutivo = ejecutivo;
//	}
//
//	public Promotoria getPromotoria() {
//		return promotoria;
//	}
//
//	public void setPromotoria(Promotoria promotoria) {
//		this.promotoria = promotoria;
//	}
//
	public ConfigBonos getBono() {
		return bono;
	}

	public void setBono(ConfigBonos bono) {
		this.bono = bono;
	}

	public ValorCatalogoAgentes getTipAgente() {
		return tipAgente;
	}

	public void setTipAgente(ValorCatalogoAgentes tipAgente) {
		this.tipAgente = tipAgente;
	}

	public ConfigBonosService getConfigBonosService() {
		return configBonosService;
	}

/****************************************************************************************/
	
	@Action(value="mostrarFiltros", results={
			@Result(name=SUCCESS, location=FILTROS_REPORTE)
	})
	@Override
	public String mostrarFiltros() {
		agente = new Agente();
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="exportarToExcel", results={
			@Result(name=SUCCESS, type="stream", params={"contentType",
					"${contentType}", "contentDisposition",
					"attachtment;filename=\"${fileName}\"","inputName",
					"reportePreviewBonos"}),
			@Result(name="EMPTY", location=ERROR),
			@Result(name=INPUT, location=ERROR)
	})
	@Override
	public String exportarToExcel() {
		try{
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
			.imprimirReportePreviewBonosExcel(getIdCentroOperacion(), getIdGerencia(), getIdEjecutivo(),
					getIdPromotoria(), tipAgente, getMes(),getAnio(), agente, bono, null, ReporteAgenteBaseAction.TIPOSALIDAARCHIVO );
		//null es idCalculo
			if(transporte!=null){
				
				setReportePreviewBonos(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			if(TipoSalidaReportes.TO_EXCEL.getValue().equals(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO)){
				setContentType("application/xls");
			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octect-stream");
			}			
				setFileName("Reporte_Preview_Bonos."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"})
		})
		
		public String obtenerAgente(){
		try {
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
//			onSuccess();
		} catch (Exception e) {
//			onError(e);
			
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		
		try {
//			final CentroOperacion filtro = new CentroOperacion();
//			setCentroOperacionList(centroOperacionService.findByFilters(filtro));
			setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
			setTipoPromotoria(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipos de Promotoria"));
			setTipoAgente(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipo de Agente"));
			setEstatusAgente(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Estatus de Agente (Situacion)"));
			setTipoCedula(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipos de Cedula de Agente"));
			listTipoBono = configBonosService.findByFilters(null);
		} catch (Exception e) {
//			prioridadList = new LinkedList<ValorCatalogoAgentes>();
		}
	}

}
