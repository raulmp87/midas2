package mx.com.afirme.midas2.action.catalogos.tarifa.administracionUsuarioMovil;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.movil.cotizador.AdministracionUsuarioMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/administracionUsuarioMovil")
@Component
@Scope("prototype")
public class AdministracionUsuarioMovilAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4814866545981130881L;
	@Autowired
	private CotizacionMovilService cotizacionMovilService;
	private final String NAMESPACE="/tarifa/administracionUsuarioMovil";
	/***Components of view****************************************/
	private CotizacionMovilDTO cotizacionMovil;
	private AdministracionUsuarioMovil usuario;
	private List<AdministracionUsuarioMovil> listaUsuarios= new ArrayList<AdministracionUsuarioMovil>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	private String tabActiva;
	/**
	 * Action:
	 * 
	 */
	
	@Override
	public void prepare() throws Exception {
		if(cotizacionMovil!=null && "1".equals(tipoAccion)){
			cotizacionMovil=new CotizacionMovilDTO();
		}
	}

	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionUsuarioMovil/mostrar.jsp")
		})
		public String mostrar(){
		    this.setTabActiva("administracion_usuarios");
			cotizacionMovil=new CotizacionMovilDTO();
			return SUCCESS;
		}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionUsuarioMovil/administracionUsuarioMovilGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionUsuarioMovil/administracionUsuarioMovilGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		try{
			if("consulta".equals(tipoAccion)){
				if(usuario==null){
					usuario=new AdministracionUsuarioMovil();
               }
			}
			if(usuario!=null){
				listaUsuarios=cotizacionMovilService.findAdministracionUsuarioByFilters(usuario);
			}
			else{
				listaUsuarios=cotizacionMovilService.findAdministracionUsuarioByFilters(usuario);
			}
			
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionUsuarioMovil/detalle.jsp")
		})
	@Override
	public String verDetalle() {
		if(usuario != null){
			if(usuario.getIdusuario()!=null){
				usuario = cotizacionMovilService.findAdministracionUsuarioByFilters(usuario).get(0);
			}
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionUsuarioMovil/detalle.jsp")
		})	
	public String verDetalleHistorico() {
		if(cotizacionMovil != null){
			if(cotizacionMovil.getIdtocotizacionmovil()!=null){
				//descuentosAgente = gerenciaJpaService.loadById(descuentosAgente, this.getUltimaModificacion().getFechaHoraActualizacionString());
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion","excludeProperties","^gerencia\\.ejecutivos.*,^gerencia\\.personaResponsable.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion"})
	})
	
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","usuario.idusuario","${usuario.idusuario}","idTipoOperacion","${idTipoOperacion}","idRegistro","${usuario.idusuario}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${usuario.idusuario}"})})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;

			if(usuario.getIdusuario()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
				setIdTipoOperacion(20L);
				cotizacionMovilService.actualizar(usuario);
				guardarHistorico(TipoOperacionHistorial.GERENCIA, usuario.getIdusuario(),"midas.gerencia.historial.accion",TipoAccionHistorial.BAJA);
				onSuccess();
			}
			else{
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("Es necesario completar la dirección para eliminar la gerencia");
				setTipoAccion("3");
			}
		}catch(Exception e){
			setMensajeError(e.getMessage());
			return INPUT;
		}
		return result;
	}
	@Action(value="eliminar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","usuario.idusuario","${usuario.idusuario}",
					"idTipoOperacion","${idTipoOperacion}","idRegistro","${usuario.idusuario}"}),
			@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionUsuarioMovil/detalle.jsp")
		})
		@Override
		public String eliminar() {
			try {
				if(usuario.getIdusuario()!=null){
					setIdTipoOperacion(20L);
					usuario.setBajalogica(Short.valueOf(String.valueOf(Sistema.NO_SELECCIONADO)));
					cotizacionMovilService.actualizar(usuario);
					guardarHistorico(TipoOperacionHistorial.GERENCIA, usuario.getIdusuario(),"midas.gerencia.historial.accion",TipoAccionHistorial.BAJA);
					onSuccess();
				}
				else{
					setTipoMensaje(TIPO_MENSAJE_ERROR);
					setMensaje("Es necesario completar la dirección para eliminar la gerencia");
					setTipoAccion("3");
				}
			}catch(Exception e){
				usuario.setBajalogica(Short.valueOf(String.valueOf(Sistema.SELECCIONADO)));
				setMensajeError(e.getMessage());
				return INPUT;
			}
			return SUCCESS;
		}
		
	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public CotizacionMovilDTO getCotizacionMovil() {
		return cotizacionMovil;
	}
	public void setCotizacionMovil(CotizacionMovilDTO cotizacionMovil) {
		this.cotizacionMovil = cotizacionMovil;
	}
	
	@Override
	public String listar() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setUsuario(AdministracionUsuarioMovil usuario) {
		this.usuario = usuario;
	}

	public AdministracionUsuarioMovil getUsuario() {
		return usuario;
	}

	public void setListaUsuarios(List<AdministracionUsuarioMovil> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<AdministracionUsuarioMovil> getListaUsuarios() {
		return listaUsuarios;
	}

	
}
