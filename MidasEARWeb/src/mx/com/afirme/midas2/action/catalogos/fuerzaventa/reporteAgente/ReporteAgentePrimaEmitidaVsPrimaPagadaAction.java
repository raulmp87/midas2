package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/repPrimaPagadaVsPrimaEmitida")
@Component
@Scope("prototype")
public class ReporteAgentePrimaEmitidaVsPrimaPagadaAction extends ReporteAgenteBaseAction implements Preparable,
		ReportMethods {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8951641104459823188L;
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reportePrimaEmitidaVsPrimaPagada.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private List<CentroOperacion> centroOperacion = new ArrayList<CentroOperacion>();
	private InputStream reporte;
	private List<Gerencia> gerenciaList = new ArrayList<Gerencia>(); 
	private List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
	private List<Promotoria> promorotiaList = new ArrayList<Promotoria>();
	private Agente agente = new Agente();
	private Long idAgente;
	private AgenteMidasService agenteMidasService;
	private String labelFechaInicio = "Fecha Corte Inicio";
	private String labelFechaFin = "Fecha Corte Fin";
/*****************************setters and getters*******************************************************/
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	public List<CentroOperacion> getCentroOperacion() {
		return centroOperacion;
	}
	public void setCentroOperacion(List<CentroOperacion> centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	public List<Gerencia> getGerenciaList() {
		return gerenciaList;
	}
	public void setGerenciaList(List<Gerencia> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}
	public List<Ejecutivo> getEjecutivoList() {
		return ejecutivoList;
	}
	public void setEjecutivoList(List<Ejecutivo> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}
	public List<Promotoria> getPromorotiaList() {
		return promorotiaList;
	}
	public void setPromorotiaList(List<Promotoria> promorotiaList) {
		this.promorotiaList = promorotiaList;
	}
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	public InputStream getReporte() {
		return reporte;
	}
	public void setReporte(InputStream reporte) {
		this.reporte = reporte;
	}
	public Long getIdAgente() {
		return idAgente;
	}
	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}
	public String getLabelFechaInicio() {
		return labelFechaInicio;
	}
	public void setLabelFechaInicio(String labelFechaInicio) {
		this.labelFechaInicio = labelFechaInicio;
	}
	public String getLabelFechaFin() {
		return labelFechaFin;
	}
	public void setLabelFechaFin(String labelFechaFin) {
		this.labelFechaFin = labelFechaFin;
	}
/********************************common methods****************************************************/
	
	@Action(value="mostrarFiltros", results={
			@Result(name=SUCCESS, location=FILTROS_REPORTE)
	})
	@Override
	public String mostrarFiltros() {
		// TODO Auto-generated method stub
		agente = new Agente();
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="exportarToExcel", results={
			@Result(name=SUCCESS,type="stream", 
					params={
							"contentType","${contentType}",
							"contentDisposition","attachment;filename=\"${fileName}\"",
							"inputName","reporte"
						}),
			@Result(name="EMPTY", location=ERROR),
			@Result(name=INPUT, location=ERROR)
	})
	@Override
	public String exportarToExcel() {
		try{
			SimpleDateFormat formatoFech = new SimpleDateFormat("dd/MM/yyyy");
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
			.imprimirReporePrimaPagadaVsPrimaEmitida(
					formatoFech.format(getFechaInicio()),formatoFech.format(getFechaFin()), getIdCentroOperacion(), 
					getIdGerencia(), getIdEjecutivo(), getIdPromotoria(),agente.getIdAgente(), ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
			if(transporte !=null){
				setReporte(new ByteArrayInputStream(transporte.getByteArray()));
			}else{
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				setContentType("application/xls");
			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octet-stream; charset=UTF-8");
			}
				setFileName("Reporte."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"})
		})
		
		public String obtenerAgente(){
		try {
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
//			onSuccess();
		} catch (Exception e) {
//			onError(e);
			
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
//		final CentroOperacion filtro = new CentroOperacion();
//		setCentroOperacionList(centroOperacionService.findByFilters(filtro));
		setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
		setTipoPromotoria(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipos de Promotoria"));
		setTipoAgente(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipo de Agente"));
		setEstatusAgente(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Estatus de Agente (Situacion)"));
		setTipoCedula(valorCatalogoAgentesService
				.obtenerElementosPorCatalogo("Tipos de Cedula de Agente"));

	}

}
