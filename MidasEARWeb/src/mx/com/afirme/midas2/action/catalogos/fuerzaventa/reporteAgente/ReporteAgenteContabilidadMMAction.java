package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteAgenteContabilidad")
@Component
@Scope("prototype")
public class ReporteAgenteContabilidadMMAction extends ReporteAgenteBaseAction implements ReportMethods, Preparable{

	private static final long serialVersionUID = 1L;
	private Long idAgente;
	private List<AgenteView> listaAgentes;
	private List<Agente> agenteList;
	private String anioFin;
	private String mesFin;
	private final String ERROR = "/jsp/reportesAgentes/reporteError.jsp";
	@Override
	@Action(value = "mostrarFiltros", results = { @Result(name = SUCCESS, location = "/jsp/reportesAgentes/reporteAgenteContabilidad.jsp") })
	public String mostrarFiltros() {
		final CentroOperacion filtro = new CentroOperacion();
		setCentroOperacionList(centroOperacionService.findByFilters(filtro));
		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Action(value="exportMizar", results={
			@Result(name =SUCCESS, type="stream", params={"contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"","inputName",
					"reporteAgenteStream"}),
			@Result(name=INPUT,location=ERROR, params={
					"mensaje","${mensaje}"})
	})

	public String exportMizar() {
		setMensaje(EMPTY_RESULT);
		return INPUT;
	}
//	 "/jsp/reportesAgentes/reporteError.jsp"
	@Override
	@Action(value = "exportarToExcel", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location =ERROR, params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToExcel() {
		if (agenteList != null && !agenteList.isEmpty()) {
			idAgente = agenteList.get(0).getId();
		}
		try {
			//TransporteImpresionDTO
			InputStream transporte = getGenerarPlantillaReporteService()
			.imprimirReporteAgenteContabilidadMMToExcel(getAnio(),
					getMes(), anioFin, mesFin, getIdCentroOperacion(),
					getIdGerencia(), getIdEjecutivo(), getIdPromotoria(),
					idAgente, getLocale());
			if (transporte != null) {
				setReporteAgenteStream(transporte);
//				setReporteAgenteStream(new ByteArrayInputStream(
//						transporte.getByteArray()));
			} else {
				setMensaje(EMPTY_RESULT);
				return INPUT;
			}
			setContentType("application/zip");
//			setContentType("application/octet-stream");
			setFileName("reporteAgenteContabilidadMidas_"
					+ Calendar.getInstance().getTimeInMillis() + ".zip");
		} catch (RuntimeException exception) {
			setMensaje(EMPTY_RESULT);
			return INPUT;
		}
		
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		if (listaAgentes != null) {
			agenteList = new ArrayList<Agente>();
			for (AgenteView agenteView : listaAgentes) {
				Agente agente = new Agente();
				agente.setId(agenteView.getId());
				agenteList.add(agente);
			}
		}
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public String getAnioFin() {
		return anioFin;
	}

	public void setAnioFin(String anioFin) {
		this.anioFin = anioFin;
	}

	public String getMesFin() {
		return mesFin;
	}

	public void setMesFin(String mesFin) {
		this.mesFin = mesFin;
	}

}
