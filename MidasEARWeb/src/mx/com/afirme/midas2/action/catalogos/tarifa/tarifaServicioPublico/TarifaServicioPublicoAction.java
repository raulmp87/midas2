package mx.com.afirme.midas2.action.catalogos.tarifa.tarifaServicioPublico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoSumaAseguradaAd;
import mx.com.afirme.midas2.dto.RelacionesTarifaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;
import mx.com.afirme.midas2.service.tarifa.TarifaServicioPublicoService;






import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class TarifaServicioPublicoAction extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4099078427656302939L;
	
	private static final Logger log = LoggerFactory.getLogger(TarifaServicioPublicoAction.class);
	
	private Long idToNegocio;
	private Long idToNegProducto;
	private Long idToNegTipoPoliza;
	private Long idToNegSeccion;
	private Long idToPaqueteSeccion;
	private Long idToNegPaqueteSeccion;
	private Short idMonedaDTO;
	private String stateId;
	private String cityId;
	private BigDecimal idTcVigencia;
	private Short esLineaAutobuses;

	private List<Negocio>negocioList= new ArrayList<Negocio>();
	private List<NegocioProducto>negocioProductoList= new ArrayList<NegocioProducto>();
	private List<NegocioTipoPoliza>negocioTipoPolizaList= new ArrayList<NegocioTipoPoliza>();
	private List<NegocioSeccion> secciones = new ArrayList<NegocioSeccion>(1);
	private List<NegocioPaqueteSeccion> paquetes = new ArrayList<NegocioPaqueteSeccion>(1);
	private List<MonedaDTO> monedaList = new ArrayList<MonedaDTO>(1);
	private List<EstadoDTO> estadoList = new ArrayList<EstadoDTO>(1);
	private List<CiudadDTO> municipioList = new ArrayList<CiudadDTO>(1);
	private List<VigenciaDTO>vigenciaList= new ArrayList<VigenciaDTO>();
	
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private RelacionesTarifaServicioPublicoDTO relacionesTarifaServicioPublicoDTO; 
	private NegocioTipoPoliza negocioTipoPoliza;
	private NegocioSeccion negocioSeccion;
	private Negocio negocio;
	private NegocioProducto negocioProducto;
	private TarifaServicioPublico tarifaServicioPublico;
	private TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublicoSumaAseguradaAd;
	private TarifaServicioPublicoDeduciblesAd tarifaServicioPublicoDeduciblesAd;
	private VigenciaDTO vigenciaDTO;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;
	private RespuestaGridRelacionDTO respuesta;
	
	private NegocioProductoService negocioProductoService;
	private TarifaServicioPublicoService tarifaServicioPublicoService;
	private ListadoService listadoService;
	private EntidadService entidadService;
	
	@Autowired
	private NegocioService negocioService;

	@Override
	public void prepare() throws Exception {

	}
	
	public void prepareMostrar(){
		if(negocio==null){
			negocio = new Negocio();
		}
		
		if(negocio != null){
			Negocio filtro = new Negocio();
			filtro.setClaveEstatus((short)1);
			filtro.setClaveNegocio("A");
			negocioList = negocioService.findByFilters(filtro);
			negocioProductoList = negocio.getNegocioProductos();
		}
	}
	
	public String mostrar(){
		estadoList = listadoService.listarEstadosMX();
		return SUCCESS;		
	}
	
	public void prepareObtenerRelaciones(){		
		negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);
		monedaDTO = entidadService.findById(MonedaDTO.class, idMonedaDTO);
		estadoDTO = entidadService.findById(EstadoDTO.class, stateId);
		ciudadDTO = entidadService.findById(CiudadDTO.class, cityId);
		vigenciaDTO = entidadService.findById(VigenciaDTO.class, idTcVigencia);
		esLineaAutobuses=(tarifaServicioPublicoService.esParametroValido(new BigDecimal(idToNegSeccion),ParametroGeneralDTO.CODIGO_PARAM_GENERAL_LINEA_AUTOBUSES_SERVICIO_PUBLICO)) ? new Short("1") : new Short("0");
		
		tarifaServicioPublico = new TarifaServicioPublico();
		tarifaServicioPublico.setMonedaDTO(monedaDTO);
		tarifaServicioPublico.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
		tarifaServicioPublico.setVigenciaDTO(vigenciaDTO);
		if(estadoDTO != null){
			tarifaServicioPublico.setEstadoDTO(estadoDTO);
		}else{
			estadoDTO = new EstadoDTO();
			tarifaServicioPublico.setEstadoDTO(estadoDTO);			
		}
		if(ciudadDTO != null){
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);
		}else{
			ciudadDTO = new CiudadDTO();
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);			
		}		
	}
	
	public String obtenerRelaciones(){
		relacionesTarifaServicioPublicoDTO = tarifaServicioPublicoService.getRelationList(tarifaServicioPublico);
		return SUCCESS;
	}
	
	public void prepareGuardarMontosCoberturasAdicionales(){
		if(tarifaServicioPublico.getId() != null){
			tarifaServicioPublico = entidadService.findById(TarifaServicioPublico.class, tarifaServicioPublico.getId());
		} else {
			//Prepara nuevo
			negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, tarifaServicioPublico.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
			monedaDTO = entidadService.findById(MonedaDTO.class, tarifaServicioPublico.getMonedaDTO().getIdTcMoneda());
			estadoDTO = entidadService.findById(EstadoDTO.class, tarifaServicioPublico.getEstadoDTO().getStateId());
			ciudadDTO = entidadService.findById(CiudadDTO.class, tarifaServicioPublico.getCiudadDTO().getCityId());
			vigenciaDTO = entidadService.findById(VigenciaDTO.class, tarifaServicioPublico.getVigenciaDTO().getIdTcVigencia());
			CoberturaDTO coberturaDTO = entidadService.findById(CoberturaDTO.class, tarifaServicioPublico.getCoberturaDTO().getIdToCobertura());
			tarifaServicioPublico.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
			tarifaServicioPublico.setMonedaDTO(monedaDTO);
			tarifaServicioPublico.setCoberturaDTO(coberturaDTO);
			tarifaServicioPublico.setEstadoDTO(estadoDTO);
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);
			tarifaServicioPublico.setVigenciaDTO(vigenciaDTO);
		}
	}
	
	public String guardarMontosCoberturasAdicionales(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(tarifaServicioPublico.getEstadoDTO().getStateId().equals("")){
			tarifaServicioPublico.setEstadoDTO(null);
		}
		if(tarifaServicioPublico.getCiudadDTO().getCityId().equals("")){
			tarifaServicioPublico.setCiudadDTO(null);
		}	
		respuesta = tarifaServicioPublicoService.guardarMontosCoberturasAdicionales(accion, tarifaServicioPublico);
		return SUCCESS;
	}
	
	public void prepareGuardarMontosSumasAseguradasAdicionales(){
		if(tarifaServicioPublicoSumaAseguradaAd.getId() != null){
			tarifaServicioPublicoSumaAseguradaAd = entidadService.findById(TarifaServicioPublicoSumaAseguradaAd.class, tarifaServicioPublicoSumaAseguradaAd.getId());
		} else {
			//Prepara nuevo
			negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, tarifaServicioPublicoSumaAseguradaAd.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
			monedaDTO = entidadService.findById(MonedaDTO.class, tarifaServicioPublicoSumaAseguradaAd.getMonedaDTO().getIdTcMoneda());
			estadoDTO = entidadService.findById(EstadoDTO.class, tarifaServicioPublicoSumaAseguradaAd.getEstadoDTO().getStateId());
			ciudadDTO = entidadService.findById(CiudadDTO.class, tarifaServicioPublicoSumaAseguradaAd.getCiudadDTO().getCityId());
			vigenciaDTO = entidadService.findById(VigenciaDTO.class, tarifaServicioPublicoSumaAseguradaAd.getVigenciaDTO().getIdTcVigencia());
			CoberturaDTO coberturaDTO = entidadService.findById(CoberturaDTO.class, tarifaServicioPublicoSumaAseguradaAd.getCoberturaDTO().getIdToCobertura());
			tarifaServicioPublicoSumaAseguradaAd.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
			tarifaServicioPublicoSumaAseguradaAd.setMonedaDTO(monedaDTO);
			tarifaServicioPublicoSumaAseguradaAd.setCoberturaDTO(coberturaDTO);
			tarifaServicioPublicoSumaAseguradaAd.setEstadoDTO(estadoDTO);
			tarifaServicioPublicoSumaAseguradaAd.setCiudadDTO(ciudadDTO);
			tarifaServicioPublicoSumaAseguradaAd.setVigenciaDTO(vigenciaDTO);
		}
	}
	
	public String guardarMontosSumasAseguradasAdicionales(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(tarifaServicioPublicoSumaAseguradaAd.getEstadoDTO().getStateId().equals("")){
			tarifaServicioPublicoSumaAseguradaAd.setEstadoDTO(null);
		}
		if(tarifaServicioPublicoSumaAseguradaAd.getCiudadDTO().getCityId().equals("")){
			tarifaServicioPublicoSumaAseguradaAd.setCiudadDTO(null);
		}	
		respuesta = tarifaServicioPublicoService.guardarSumasAseguradasAdicionales(accion, tarifaServicioPublicoSumaAseguradaAd);
		return SUCCESS;
	}
	
	public void prepareGuardarMontosDeduciblesAdicionales(){
		if(tarifaServicioPublicoDeduciblesAd.getId() != null){
			tarifaServicioPublicoDeduciblesAd = entidadService.findById(TarifaServicioPublicoDeduciblesAd.class, tarifaServicioPublicoDeduciblesAd.getId());
		} else {
			//Prepara nuevo
			negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, tarifaServicioPublicoDeduciblesAd.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion());
			monedaDTO = entidadService.findById(MonedaDTO.class, tarifaServicioPublicoDeduciblesAd.getMonedaDTO().getIdTcMoneda());
			estadoDTO = entidadService.findById(EstadoDTO.class, tarifaServicioPublicoDeduciblesAd.getEstadoDTO().getStateId());
			ciudadDTO = entidadService.findById(CiudadDTO.class, tarifaServicioPublicoDeduciblesAd.getCiudadDTO().getCityId());
			vigenciaDTO = entidadService.findById(VigenciaDTO.class, tarifaServicioPublicoDeduciblesAd.getVigenciaDTO().getIdTcVigencia());
			CoberturaDTO coberturaDTO = entidadService.findById(CoberturaDTO.class, tarifaServicioPublicoDeduciblesAd.getCoberturaDTO().getIdToCobertura());
			tarifaServicioPublicoDeduciblesAd.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
			tarifaServicioPublicoDeduciblesAd.setMonedaDTO(monedaDTO);
			tarifaServicioPublicoDeduciblesAd.setCoberturaDTO(coberturaDTO);
			tarifaServicioPublicoDeduciblesAd.setEstadoDTO(estadoDTO);
			tarifaServicioPublicoDeduciblesAd.setCiudadDTO(ciudadDTO);
			tarifaServicioPublicoDeduciblesAd.setVigenciaDTO(vigenciaDTO);
		}
	}
	
	public String guardarMontosDeduciblesAdicionales(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(tarifaServicioPublicoDeduciblesAd.getEstadoDTO().getStateId().equals("")){
			tarifaServicioPublicoDeduciblesAd.setEstadoDTO(null);
		}
		if(tarifaServicioPublicoDeduciblesAd.getCiudadDTO().getCityId().equals("")){
			tarifaServicioPublicoDeduciblesAd.setCiudadDTO(null);
		}	
		respuesta = tarifaServicioPublicoService.guardarDeduciblesAdicionales(accion, tarifaServicioPublicoDeduciblesAd);
		return SUCCESS;
	}
	
	@SuppressWarnings("unused")
	private String getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParma){
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(idToGrupoParamGen);
		parametroGeneralId.setCodigoParametroGeneral(codigoParma);
		ParametroGeneralDTO parameter = entidadService.findById(ParametroGeneralDTO.class, parametroGeneralId);
		if(parameter!=null){
			return parameter.getValor();
		}
		return null;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public Long getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegTipoPoliza(Long idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public Long getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(Long idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public Long getIdToPaqueteSeccion() {
		return idToPaqueteSeccion;
	}

	public void setIdToPaqueteSeccion(Long idToPaqueteSeccion) {
		this.idToPaqueteSeccion = idToPaqueteSeccion;
	}

	public Long getIdToNegPaqueteSeccion() {
		return idToNegPaqueteSeccion;
	}

	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}

	public Short getIdMonedaDTO() {
		return idMonedaDTO;
	}

	public void setIdMonedaDTO(Short idMonedaDTO) {
		this.idMonedaDTO = idMonedaDTO;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public BigDecimal getIdTcVigencia() {
		return idTcVigencia;
	}

	public void setIdTcVigencia(BigDecimal idTcVigencia) {
		this.idTcVigencia = idTcVigencia;
	}
	
	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}
	

	public void setNegocioProductoList(List<NegocioProducto> negocioProductoList) {
		this.negocioProductoList = negocioProductoList;
	}

	public List<NegocioProducto> getNegocioProductoList() {
		return negocioProductoList;
	}

	public List<NegocioTipoPoliza> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setNegocioTipoPolizaList(
			List<NegocioTipoPoliza> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public NegocioProductoService getNegocioProductoService() {
		return negocioProductoService;
	}

	public List<NegocioSeccion> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<NegocioSeccion> secciones) {
		this.secciones = secciones;
	}

	public List<NegocioPaqueteSeccion> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(List<NegocioPaqueteSeccion> paquetes) {
		this.paquetes = paquetes;
	}

	public List<MonedaDTO> getMonedaList() {
		return monedaList;
	}

	public void setMonedaList(List<MonedaDTO> monedaList) {
		this.monedaList = monedaList;
	}

	public List<EstadoDTO> getEstadoList() {
		return estadoList;
	}

	public void setEstadoList(List<EstadoDTO> estadoList) {
		this.estadoList = estadoList;
	}

	public List<CiudadDTO> getMunicipioList() {
		return municipioList;
	}

	public void setMunicipioList(List<CiudadDTO> municipioList) {
		this.municipioList = municipioList;
	}
	
	public List<VigenciaDTO> getVigenciaList() {
		return vigenciaList;
	}

	public void setVigenciaList(List<VigenciaDTO> vigenciaList) {
		this.vigenciaList = vigenciaList;
	}
	
	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}

	public RelacionesTarifaServicioPublicoDTO getRelacionesTarifaServicioPublicoDTO() {
		return relacionesTarifaServicioPublicoDTO;
	}

	public void setRelacionesTarifaServicioPublicoDTO(
			RelacionesTarifaServicioPublicoDTO relacionesTarifaServicioPublicoDTO) {
		this.relacionesTarifaServicioPublicoDTO = relacionesTarifaServicioPublicoDTO;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public TarifaServicioPublico getTarifaServicioPublico() {
		return tarifaServicioPublico;
	}

	public void setTarifaServicioPublico(TarifaServicioPublico tarifaServicioPublico) {
		this.tarifaServicioPublico = tarifaServicioPublico;
	}

	public TarifaServicioPublicoSumaAseguradaAd getTarifaServicioPublicoSumaAseguradaAd() {
		return tarifaServicioPublicoSumaAseguradaAd;
	}

	public void setTarifaServicioPublicoSumaAseguradaAd(
			TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublicoSumaAseguradaAd) {
		this.tarifaServicioPublicoSumaAseguradaAd = tarifaServicioPublicoSumaAseguradaAd;
	}

	public TarifaServicioPublicoDeduciblesAd getTarifaServicioPublicoDeduciblesAd() {
		return tarifaServicioPublicoDeduciblesAd;
	}

	public void setTarifaServicioPublicoDeduciblesAd(
			TarifaServicioPublicoDeduciblesAd tarifaServicioPublicoDeduciblesAd) {
		this.tarifaServicioPublicoDeduciblesAd = tarifaServicioPublicoDeduciblesAd;
	}

	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}

	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}

	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}
	
	public VigenciaDTO getVigenciaDTO() {
		return vigenciaDTO;
	}

	public void setVigenciaDTO(VigenciaDTO vigenciaDTO) {
		this.vigenciaDTO = vigenciaDTO;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	@Autowired
	@Qualifier("negocioProductoServiceEJB")	
	public void setNegocioProductoService(
			NegocioProductoService negocioProductoService) {
		this.negocioProductoService = negocioProductoService;
	}

	@Autowired
	@Qualifier("tarifaServicioPublicoServiceEJB")
	public void setTarifaServicioPublicoService(
			TarifaServicioPublicoService tarifaServicioPublicoService) {
		this.tarifaServicioPublicoService = tarifaServicioPublicoService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public NegocioService getNegocioService() {
		return negocioService;
	}

	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	public Short getEsLineaAutobuses() {
		return esLineaAutobuses;
	}

	public void setEsLineaAutobuses(Short esLineaAutobuses) {
		this.esLineaAutobuses = esLineaAutobuses;
	}
}
