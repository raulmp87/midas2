package mx.com.afirme.midas2.action.catalogos.fuerzaventa.programacionBono;

import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CalculoBonoEjecuciones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.service.bonos.ProgramacionBonoService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.util.MidasException;
import java.util.Collections;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/programacionBono")
public class ProgramacionBonoAction extends CatalogoHistoricoAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5767611029850148907L;
	
	private ProgramacionBono programacionBono;
	private Long idTipoBono;
	private List<ValorCatalogoAgentes> tipoBonoList;
	private List<ValorCatalogoAgentes> periodoEjecucionList;
	private List<ProgramacionBono> programacionBonoList;
	private List<ConfigBonos> configuracionBonoList;
	private List<Negocio> negociosEspecialesList;
	private Short modoEjecucion = 1;
	private Integer bonosPorProgramar;
	private Integer bonosProgramados;	
	private Boolean ejecutar;
	private List<CalculoBonoEjecucionesView> monitorEjecucionesBonoList;
	private String gridMonitor = "BONOS";
	
	private List<GenericaAgentesView> listaNegociosEspeciales = new ArrayList<GenericaAgentesView>(1);
	private List<GenericaAgentesView> listaConfiguracionBonos = new ArrayList<GenericaAgentesView>(1);
	
	private ProgramacionBonoService programacionBonoService;
	
	@Autowired
	@Qualifier("programacionBonoServiceEJB")
	public void setProgramacionBonoService(ProgramacionBonoService programacionBonoService) {
		this.programacionBonoService = programacionBonoService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		if(isEmptyList(tipoBonoList)){
			tipoBonoList = cargarCatalogoLightWeight("Tipos de Bono");
		}
		if(isEmptyList(periodoEjecucionList)){
//			periodoEjecucionList = cargarCatalogoLightWeight("Periodos de Ajuste de Bonos");
			periodoEjecucionList = cargarCatalogo("Periodos de Ajuste de Bonos","Mensual", "Bimestral", "Trimestral","Semestral","Anual");
		}
	}
	
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonos.jsp")
		})
	public String mostrar(){
		return SUCCESS;
	}
	
	@Action(value="listarNegociosEspeciales",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaNegociosEspeciales.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaNegociosEspeciales.*"})
		})
	public String listarNegociosEspeciales(){
		try {
			if(idTipoBono != null && idTipoBono.equals(programacionBonoService.getTipoBonoNegocioEspecial().getId())){
				listaNegociosEspeciales = programacionBonoService.getNegocioEspeciales(idTipoBono);
			}
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="listarConfiguracionBono",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaConfiguracionBonos.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaConfiguracionBonos.*"})
		})
	public String listarConfiguracionBono(){
		try {
			if(idTipoBono != null){
				listaConfiguracionBonos = programacionBonoService.getConfiguracionBonos(idTipoBono);
			}
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="obtenerProgramados",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","bonosPorProgramar,bonosProgramados"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","bonosPorProgramar,bonosProgramados"})
		})
	public String obtenerProgramados(){
		Map<String, Integer> bonosProgYPorProg = programacionBonoService.obtenerProgramados(idTipoBono);
		bonosProgramados = bonosProgYPorProg.get("bonosProgramados");
		bonosPorProgramar = bonosProgYPorProg.get("bonosPorProgramar");
		return SUCCESS;
	}
	
	@Action(value="guardarProgramacionBono",results={
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrar","namespace","/fuerzaventa/programacionBono"})
//			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonos.jsp")
		})
	public String guardarProgramacionBono(){
		try {
			programacionBonoService.saveProgramacionBono(programacionBono, negociosEspecialesList, configuracionBonoList, getUsuarioActual(),ejecutar);
		} catch (MidasException e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="activarProgramacionBono",results={
			@Result(location="/jsp/relaciones/respuestaAsociacion.jsp")
		})
	public String activarProgramacionBono(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		programacionBonoService.activaProgramacionBono(accion, programacionBono);
		return SUCCESS;
	}
	
	@Action(value="calculoBono",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonos.jsp"),
			@Result(name=INPUT,type="redirectAction", 
				params={"actionName","mostrar","namespace","/fuerzaventa/programacionBono",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"})
		})
	public String calculoBono(){
		try{
			String BonoCero="";
			 List<ConfigBonos> listaBonoNoVigente = new ArrayList<ConfigBonos>();
			
			 listaBonoNoVigente = programacionBonoService.bonoIsVigente(configuracionBonoList);
			 configuracionBonoList = programacionBonoService.excluirBonoNoVigentes(listaBonoNoVigente,configuracionBonoList);
			programacionBonoService.ejecCalcBono(programacionBono, negociosEspecialesList, configuracionBonoList, getUsuarioActual(), true);
			
//			BonoCero= programacionBonoService.eliminarBonosCero(idsCalculos);
//			if(!BonoCero.equals("")){
//				throw new MidasException("No existen bonos pendientes de pago. Favor de validar la configuración realizada para los siguientes Bonos"+BonoCero);
//			}
			if(listaBonoNoVigente.isEmpty()){
				setMensaje("Se ha comenzado con el calculo de bono(s) correctamente.");
			}else{
				String idsConfigBono = obtenerIdsConfigBono(listaBonoNoVigente);
				setMensaje("A Comenzado el calclo de bono(s), Las siguientes configuraciones de bonos no se encuentran Vigentes: "+idsConfigBono.substring(0,idsConfigBono.length()-2));
			}
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return result;
	}
	
	public String obtenerIdsConfigBono(List<ConfigBonos> listaBonoNoVigente) {
		StringBuilder idsConfigBono = new StringBuilder("");
		Collections.reverse(listaBonoNoVigente);
		for(ConfigBonos obj:listaBonoNoVigente){
			if(obj!=null){
				idsConfigBono.append(obj.getId().toString()).append(", ");
			}
		}
		return idsConfigBono.toString();
	}

	@Action(value="eliminarProgramacionBono",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonos.jsp")
		})
	public String eliminarProgramacionBono(){
		String accion = "deleted";
		programacionBonoService.activaProgramacionBono(accion, programacionBono);
		return SUCCESS;
	}
	

	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="listar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/programacionBonosGrid.jsp")
		})
	@Override
	public String listar() {
		programacionBonoList = programacionBonoService.getProgramacionBonos();
		return SUCCESS;
	}

	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Action(value="monitorCalculoBono",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/monitorCalculoBono.jsp")
	})
	public String monitorCalculoBono(){		
		return SUCCESS;
	}

	@Action(value="listarMonitorBonoGrid",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/programacionBonos/monitorCalculoBonoGrid.jsp")
		})
	public String listarMonitorBonoGrid(){
		
		try {
			monitorEjecucionesBonoList=programacionBonoService.listaCalculoBonosMonitorEjecucion();
		} catch (MidasException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public void setTipoBonoList(List<ValorCatalogoAgentes> tipoBonoList) {
		this.tipoBonoList = tipoBonoList;
	}

	public List<ValorCatalogoAgentes> getTipoBonoList() {
		return tipoBonoList;
	}

	public void setProgramacionBono(ProgramacionBono programacionBono) {
		this.programacionBono = programacionBono;
	}

	public ProgramacionBono getProgramacionBono() {
		return programacionBono;
	}

	public void setProgramacionBonoList(List<ProgramacionBono> programacionBonoList) {
		this.programacionBonoList = programacionBonoList;
	}

	public List<ProgramacionBono> getProgramacionBonoList() {
		return programacionBonoList;
	}

	public void setPeriodoEjecucionList(List<ValorCatalogoAgentes> periodoEjecucionList) {
		this.periodoEjecucionList = periodoEjecucionList;
	}

	public List<ValorCatalogoAgentes> getPeriodoEjecucionList() {
		return periodoEjecucionList;
	}


	public void setConfiguracionBonoList(List<ConfigBonos> configuracionBonoList) {
		this.configuracionBonoList = configuracionBonoList;
	}


	public List<ConfigBonos> getConfiguracionBonoList() {
		return configuracionBonoList;
	}


	public void setIdTipoBono(Long idTipoBono) {
		this.idTipoBono = idTipoBono;
	}


	public Long getIdTipoBono() {
		return idTipoBono;
	}


	public void setListaNegociosEspeciales(List<GenericaAgentesView> listaNegociosEspeciales) {
		this.listaNegociosEspeciales = listaNegociosEspeciales;
	}


	public List<GenericaAgentesView> getListaNegociosEspeciales() {
		return listaNegociosEspeciales;
	}


	public void setListaConfiguracionBonos(List<GenericaAgentesView> listaConfiguracionBonos) {
		this.listaConfiguracionBonos = listaConfiguracionBonos;
	}


	public List<GenericaAgentesView> getListaConfiguracionBonos() {
		return listaConfiguracionBonos;
	}


	public void setNegociosEspecialesList(List<Negocio> negociosEspecialesList) {
		this.negociosEspecialesList = negociosEspecialesList;
	}


	public List<Negocio> getNegociosEspecialesList() {
		return negociosEspecialesList;
	}

	public void setModoEjecucion(Short modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}

	public Short getModoEjecucion() {
		return modoEjecucion;
	}

	public void setBonosPorProgramar(Integer bonosPorProgramar) {
		this.bonosPorProgramar = bonosPorProgramar;
	}

	public Integer getBonosPorProgramar() {
		return bonosPorProgramar;
	}

	public void setBonosProgramados(Integer bonosProgramados) {
		this.bonosProgramados = bonosProgramados;
	}

	public Integer getBonosProgramados() {
		return bonosProgramados;
	}

	public Boolean getEjecutar() {
		return ejecutar;
	}

	public void setEjecutar(Boolean ejecutar) {
		this.ejecutar = ejecutar;
	}

	public List<CalculoBonoEjecucionesView> getMonitorEjecucionesBonoList() {
		return monitorEjecucionesBonoList;
	}

	public void setMonitorEjecucionesBonoList(
			List<CalculoBonoEjecucionesView> monitorEjecucionesBonoList) {
		this.monitorEjecucionesBonoList = monitorEjecucionesBonoList;
	}

	public String getGridMonitor() {
		return gridMonitor;
	}

	public void setGridMonitor(String gridMonitor) {
		this.gridMonitor = gridMonitor;
	}	
	
	
	
}
