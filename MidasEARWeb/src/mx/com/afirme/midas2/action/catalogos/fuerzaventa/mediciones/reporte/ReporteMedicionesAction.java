package mx.com.afirme.midas2.action.catalogos.fuerzaventa.mediciones.reporte;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.asm.dto.UserDTO;
import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.fuerzaventa.mediciones.Reporte;
import mx.com.afirme.midas2.dto.fuerzaventa.mediciones.Reporte.Tipo;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.MedicionesService;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/mediciones/reporte")
public class ReporteMedicionesAction extends BaseAction implements Preparable  {
	
	
	@Action
	(value = "descargar", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Reporte Mediciones", "pdf"})},
			results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${reporte.contentType}",
					"inputName", "reporte.inputStream",
					"contentDisposition", "attachment;filename=${reporte.fileName}",
					"bufferSize","8192"})}) 
	public String descargar() {	
		
		UserDTO usuarioActualASM = usuarioService.getUserDTOById(getUsuarioActual().getId());
		
		if (tipo.intValue() == Tipo.ULTIMO_REPORTE_MENSUAL.value().intValue()) {
			
			reporte = medicionesService.obtenerUltimoReporteMensual(usuarioActualASM);
			
		} else if (tipo.intValue() == Tipo.PENULTIMO_REPORTE_MENSUAL.value().intValue()) {
			
			reporte = medicionesService.obtenerPenultimoReporteMensual(usuarioActualASM);
			
		} else { 
			
			//ULTIMO_REPORTE_SEMANAL
			
			reporte = medicionesService.obtenerUltimoReporteSemanal(usuarioActualASM);
			
		}
						
		return SUCCESS;
	}
		
	
	@Action(value="mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/mediciones/reporte.jsp"),
			@Result(name = "disabled", location = "/jsp/catalogos/fuerzaventa/mediciones/inhabilitado.jsp")})
	public String mostrar(){
				
		UserDTO usuarioActualASM = usuarioService.getUserDTOById(getUsuarioActual().getId());
				
		try {
			
			medicionesService.validarUsuario (usuarioActualASM);
			
		} catch (Exception ex) {
			
			return "disabled";
			
		}
		
		periodosMap = new LinkedHashMap<Integer, String>();
		
		periodosMap.put(Tipo.ULTIMO_REPORTE_SEMANAL.value(), 
				getText("midas.mediciones.agentes.reporte.tipo.semanal"));
		
		periodosMap.put(Tipo.ULTIMO_REPORTE_MENSUAL.value(), 
				getText("midas.mediciones.agentes.reporte.tipo.mensual", 
						new String[]{convertirFormato(medicionesService.obtenerUltimoPeriodoMensual())}));
		
		periodosMap.put(Tipo.PENULTIMO_REPORTE_MENSUAL.value(), 
				getText("midas.mediciones.agentes.reporte.tipo.mensual", 
						new String[]{convertirFormato(medicionesService.obtenerPenultimoPeriodoMensual())}));
		
		
		return SUCCESS;
		
	}
		
	
	private String convertirFormato(String periodo) {
				
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_ORIGEN, new Locale("es", "ES"));
		
			Date fecha;
		
			fecha = sdf.parse(periodo);
		
		
			sdf.applyPattern(FORMATO_DESTINO);
				
			return sdf.format(fecha);
			
		} catch (ParseException e) {
		
			throw new RuntimeException(e);
			
		}
		
	}
	
	
	@Override
	public void prepare() throws Exception {
	}
	
	private static final long serialVersionUID = -3683105033543687562L;
	
	@Autowired
	private MedicionesService medicionesService;
	
	private Reporte reporte;
	
	private Map<Integer, String> periodosMap;
	
	private Integer tipo;

	
	public Reporte getReporte() {
		return reporte;
	}

	
	public void setReporte(Reporte reporte) {
		this.reporte = reporte;
	}


	public Map<Integer, String> getPeriodosMap() {
		return periodosMap;
	}


	public void setPeriodosMap(Map<Integer, String> periodosMap) {
		this.periodosMap = periodosMap;
	}


	public Integer getTipo() {
		return tipo;
	}


	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	
	public static final String FORMATO_ORIGEN = "yyyyMM";
	
	public static final String FORMATO_DESTINO = "MMMMMMMMMM yyyy";
	
	
	
}
