package mx.com.afirme.midas2.action.catalogos.fuerzaventa.herenciaclientes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HerenciaClientes;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.HerenciaClientesView;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ClienteAgenteService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/herenciaclientes")
public class HerenciaClientesAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2974914789388104912L;
	private ClienteAgenteService clienteAgenteService;
	private ListadoService listadoService;
	private AgenteMidasService agenteMidasService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private FortimaxService fortimaxService;
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	private EntidadService entidadService;
	
	private ClienteAgente clienteAgente = new ClienteAgente();
	private HerenciaClientes filtroHerenciasClienteAgente;
	
	private Map<Long, String> gerenciaList = new LinkedHashMap<Long, String>();
	private Map<Long, String> ejecutivoList = new LinkedHashMap<Long, String>();
	private Map<Long, String> promotoriaList = new LinkedHashMap<Long, String>();
	
	private List<ClienteAgente> listaClientes =new ArrayList<ClienteAgente>();
	private List<ClienteAgente> listaClientesCedidos =new ArrayList<ClienteAgente>();
	private List<Agente>  listAgentes = new ArrayList<Agente>();
	private List<ValorCatalogoAgentes> listaMotivosCesion = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaTiposCedula = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listPrioridadesAgente = new ArrayList<ValorCatalogoAgentes>();
	private List<HerenciaClientesView> listaHerencias = new ArrayList<HerenciaClientesView>();
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private HerenciaClientes herenciaClientes;
	
	private Agente agenteOrigen = new Agente();
	private Agente agenteDestino = new Agente();
	private Long idGerencia;
	private Long idEjecutivo;
	private String isAgente;
	private String urlIfimax;
	private String rangoFechas; 	
	private String documentosFaltantes;
	private Long idHerencia;
	private ActualizacionAgente detHistorial=new ActualizacionAgente();
	/**********************************************************************************************
	 * Sets and gets
	 *********************************************************************************************/
	public ActualizacionAgente getDetHistorial() {
		return detHistorial;
	}

	public void setDetHistorial(ActualizacionAgente detHistorial) {
		this.detHistorial = detHistorial;
	}
	
	public String getUrlIfimax() {
		return urlIfimax;
	}
	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}	
	public Long getIdGerencia() {
		return idGerencia;
	}	
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}
	public Map<Long, String> getGerenciaList() {
		return gerenciaList;
	}
	public void setGerenciaList(Map<Long, String> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}
	public Map<Long, String> getEjecutivoList() {
		return ejecutivoList;
	}
	public void setEjecutivoList(Map<Long, String> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}
	public Map<Long, String> getPromotoriaList() {
		return promotoriaList;
	}
	public void setPromotoriaList(Map<Long, String> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}	
	public ClienteAgente getClienteAgente() {
		return clienteAgente;
	}
	public void setClienteAgente(ClienteAgente clienteAgente) {
		this.clienteAgente = clienteAgente;
	}			
	public HerenciaClientes getFiltroHerenciasClienteAgente() {
		return filtroHerenciasClienteAgente;
	}
	public void setFiltroHerenciasClienteAgente(
			HerenciaClientes filtroHerenciasClienteAgente) {
		this.filtroHerenciasClienteAgente = filtroHerenciasClienteAgente;
	}
	public List<ClienteAgente> getListaClientes() {
		return listaClientes;
	}
	public void setListaClientes(List<ClienteAgente> listaClientes) {
		this.listaClientes = listaClientes;
	}	
	public List<ClienteAgente> getListaClientesCedidos() {
		return listaClientesCedidos;
	}
	public void setListaClientesCedidos(List<ClienteAgente> listaClientesCedidos) {
		this.listaClientesCedidos = listaClientesCedidos;
	}
	public Agente getAgenteOrigen() {
		return agenteOrigen;
	}
	public void setAgenteOrigen(Agente agenteOrigen) {
		this.agenteOrigen = agenteOrigen;
	}
	public Agente getAgenteDestino() {
		return agenteDestino;
	}
	public void setAgenteDestino(Agente agenteDestino) {
		this.agenteDestino = agenteDestino;
	}	
	public List<Agente> getListAgentes() {
		return listAgentes;
	}
	public void setListAgentes(List<Agente> listAgentes) {
		this.listAgentes = listAgentes;
	}	
	public List<ValorCatalogoAgentes> getListaMotivosCesion() {
		return listaMotivosCesion;
	}
	public void setListaMotivosCesion(List<ValorCatalogoAgentes> listaMotivosCesion) {
		this.listaMotivosCesion = listaMotivosCesion;
	}	
	public HerenciaClientes getHerenciaClientes() {
		return herenciaClientes;
	}
	public void setHerenciaClientes(HerenciaClientes herenciaClientes) {
		this.herenciaClientes = herenciaClientes;
	}		
	public List<HerenciaClientesView> getListaHerencias() {
		return listaHerencias;
	}
	public void setListaHerencias(List<HerenciaClientesView> listaHerencias) {
		this.listaHerencias = listaHerencias;
	}	
	public String getIsAgente() {
		return isAgente;
	}
	public void setIsAgente(String isAgente) {
		this.isAgente = isAgente;
	}	
	public String getDocumentosFaltantes() {
		return documentosFaltantes;
	}
	public void setDocumentosFaltantes(String documentosFaltantes) {
		this.documentosFaltantes = documentosFaltantes;
	}
	public List<ValorCatalogoAgentes> getListaTiposCedula() {
		return listaTiposCedula;
	}
	public void setListaTiposCedula(List<ValorCatalogoAgentes> listaTiposCedula) {
		this.listaTiposCedula = listaTiposCedula;
	}
	public List<ValorCatalogoAgentes> getListPrioridadesAgente() {
		return listPrioridadesAgente;
	}
	public void setListPrioridadesAgente(
			List<ValorCatalogoAgentes> listPrioridadesAgente) {
		this.listPrioridadesAgente = listPrioridadesAgente;
	}	
	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}
	public void setListaDocumentosFortimax(
			List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}
	public String getRangoFechas() {
		return rangoFechas;
	}
	public void setRangoFechas(String rangoFechas) {
		this.rangoFechas = rangoFechas;
	}	
	public Long getIdHerencia() {
		return idHerencia;
	}
	public void setIdHerencia(Long idHerencia) {
		this.idHerencia = idHerencia;
	}
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("clienteAgenteServiceEJB")
	public void setClienteAgenteService(ClienteAgenteService clienteAgenteService) {
		this.clienteAgenteService = clienteAgenteService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}
	
	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	/**********************************************************************************************
	 * Metodos
	 *********************************************************************************************/
	
	
	@Override
	public void prepare() throws Exception {
		listaMotivosCesion = cargarCatalogo("Motivos de Cesion");
		gerenciaList=listadoService.getMapGerencias();
		
	}

	public void prepareMostrarContenedor(){
		
	}
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesCatalogo.jsp")
		})
		public String mostrarContenedor(){
			listaTiposCedula = cargarCatalogo("Tipos de Cedula de Agente");			
			return SUCCESS;
		}
	

	/*************************************************************************************************
	 *  Metodo para asignar clientes a un agente
	 **************************************************************************************************/
	@Action(value="guardarHerencia",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaClientesCedidos.*,^herenciaClientes\\.id,^herenciaClientes\\.status\\.valor"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaClientesCedidos,^herenciaClientes\\.id"})

		})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			if(herenciaClientes.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			//Se crea la herencia como inactiva
			ValorCatalogoAgentes valorCatalogoAgente =catalogoService.obtenerElementoEspecifico("Tipos de Estatus", "INACTIVO");
			herenciaClientes.setStatus(valorCatalogoAgente);
			herenciaClientes = clienteAgenteService.saveHerenciaCliente(herenciaClientes);
			guardarHistorico(TipoOperacionHistorial.HERENCIA_CLIENTES, herenciaClientes.getId(),"midas.herenciaClientes.historial.accion",tipoAccionHistorial);
			if(herenciaClientes.getId()!=null)
			{						
				clienteAgenteService.herenciaClientes(listaClientesCedidos,agenteDestino, herenciaClientes, "ACTIVO");	
				herenciaClientes=entidadService.findById(HerenciaClientes.class, herenciaClientes.getId());
			}
			
		} catch (Exception e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	/*************************************************************************************************
	 *  Lista Inicial del Catalogo 
	 **************************************************************************************************/
	@Action(value="listar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesGrid.jsp")
		})
	@Override
	public String listar() {
		try {
//			setIsAgente("ORIGEN");
//			listaHerencias = clienteAgenteService.findByFiltersView(null,getIsAgente());
		} catch (Exception e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}
		return SUCCESS;
	}

	/*************************************************************************************************
	 *  Lista los resultados obtenidos de la busqueda
	 **************************************************************************************************/
		@Action(value="listarFiltrado",results={
				@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesGrid.jsp"),
				@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesGrid.jsp")
			})
	@Override
	public String listarFiltrado() {
			try {
				listaHerencias = clienteAgenteService.findByFiltersView(filtroHerenciasClienteAgente,getIsAgente());
			} catch (Exception e) {
				// TODO Bloque catch generado autom�ticamente
				e.printStackTrace();
			}
		return SUCCESS;
	}

	/*************************************************************************************************
	 *  Metodo para abrir pantalla detalle
	 **************************************************************************************************/	
	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesDetalle.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesDetalle.jsp")
		})	
	@Override
	public String verDetalle() {
			listPrioridadesAgente = cargarCatalogo("Prioridades de Agente");		
			try {
				 if(herenciaClientes!=null && herenciaClientes.getId()!=null){
					 HerenciaClientes herencia = clienteAgenteService.obtenerHerenciaPorId(herenciaClientes);
					 herenciaClientes.setMotivoCesion(herencia.getMotivoCesion());
					 Long herenciaId = herencia.getId();
					 Long agenteOrigenId = herencia.getAgenteOrigen().getId();
					 Long agenteDestinoId = herencia.getAgenteDestino().getId();
					 agenteOrigen = agenteMidasService.findByClaveAgente(herencia.getAgenteOrigen());
					 agenteDestino = agenteMidasService.findByClaveAgente(herencia.getAgenteDestino());
					 listaClientes = clienteAgenteService.consultaDetalleHerenciaClientes(herenciaId,agenteOrigenId);
//					 listaClientesCedidos = clienteAgenteService.consultaDetalleHerenciaClientes(herenciaId, agenteDestinoId);
					 herenciaClientes.setStatus(herencia.getStatus());
				 }				 
			} catch (Exception e) {
				e.printStackTrace();
			}
		return SUCCESS;
	}
	@Action(value="listarClientesHeredadosGrid",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesCedidosGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesCedidosGrid.jsp")
		})
		public String listarClientesHeredadosGrid() {
			try {
				 listaClientesCedidos = clienteAgenteService.consultaDetalleHerenciaClientes(herenciaClientes.getId(), herenciaClientes.getAgenteDestino().getId());
			} catch (Exception e) {
				e.printStackTrace();
				setMensajeError(e.getMessage());
			}
			return SUCCESS;
		}
	
//	@Action(value="listarClientesHeredar",results={
//		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje,^listaClientes.*"}),
//		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje,^listaClientes"})
//	})
//	public String listarClientesHeredar() {
//		try {
//			listaClientes = clienteAgenteService.listarClientesAHeredar(agenteOrigen, agenteDestino);
//		} catch (Exception e) {
//			e.printStackTrace();
//			setMensajeError(e.getMessage());
//		}
//		return SUCCESS;
//	}
	
	@Action(value="listarClientesHeredar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesAcederGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/herenciaclientes/herenciaClientesAcederGrid.jsp")
		})
		public String listarClientesHeredarGrid() {
			try {
				listaClientes = clienteAgenteService.listarClientesAHeredar(agenteOrigen, agenteDestino);
			} catch (Exception e) {
				e.printStackTrace();
				setMensajeError(e.getMessage());
			}
			return SUCCESS;
		}
	
	@Action(value="mostrarDocumentos",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/herenciaclientes/documentosHerencia.jsp")
		})
		public String mostrarDocumentos(){
			try {
				agenteDestino=agenteMidasService.loadById(agenteDestino);		
				PersonaSeycosDTO personaSeycos = personaSeycosFacade.findById(agenteDestino.getPersona().getIdPersona());			
				Persona per = new Persona();
				per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
				per.setIdPersona(personaSeycos.getIdPersona());
				agenteDestino.setPersona(per);
				
				String[] fieldValues = new String[6];
				
				fieldValues[0]=herenciaClientes.getId().toString();
				if(per.getClaveTipoPersona()==1){
				fieldValues[1]=personaSeycos.getNombre();		
				fieldValues[2]=personaSeycos.getApellidoPaterno();
				fieldValues[3]=personaSeycos.getApellidoMaterno();
				}
				else{
					fieldValues[1]=personaSeycos.getRazonSocial();		
					fieldValues[2]=personaSeycos.getRazonSocial();
					fieldValues[3]=personaSeycos.getRazonSocial();
				}
				fieldValues[4]="1";
				fieldValues[5]="1";	
				
				fortimaxService.generateExpedient("AGENTES", fieldValues);
				if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(herenciaClientes.getId(), "AGENTES","HERENCIA DE CLIENTES")){
					List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorAplicacionTipoPersona("AGENTES", per.getClaveTipoPersona());
						for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
							if(doc!=null){
								String []respDoc=fortimaxService.generateDocument(herenciaClientes.getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+herenciaClientes.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
								if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
									DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
									docFortimax.setCatalogoDocumentoFortimax(doc);
									docFortimax.setExisteDocumento(0);
									docFortimax.setIdRegistro(herenciaClientes.getId());
									documentoEntidadService.save(docFortimax);
								}
							}
						}			
				}
				else{
					documentoEntidadService.sincronizarDocumentos(herenciaClientes.getId(), "AGENTES", "HERENCIA DE CLIENTES");
				}
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(herenciaClientes.getId(), "AGENTES", "HERENCIA DE CLIENTES");
				setMensaje("La herencia ha sido creada, debe subir el comprobante de cesion para activarla");
				setTipoAccion(MENSAJE_EXITO);
			} catch (Exception e) {
				e.printStackTrace();
			}			
			return SUCCESS;
		}
	
	@Action(value="guardarDocumentos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"})
		})
		public String guardarDocumentos() {
			try {			
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosFaltantes(herenciaClientes.getId(), "AGENTES", "HERENCIA DE CLIENTES");
				StringBuilder docsFaltantes = new StringBuilder();
				if(!listaDocumentosFortimax.isEmpty()){
					for(DocumentoEntidadFortimax docFaltante: listaDocumentosFortimax){
						docsFaltantes.append(docFaltante.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax());
						docsFaltantes.append(",");
					}
					setDocumentosFaltantes(docsFaltantes.toString().substring(0,docsFaltantes.length()-1));
				}
				if(herenciaClientes.getId()!=null&&documentosFaltantes==null||documentosFaltantes.isEmpty()){
					listaClientesCedidos = entidadService.findByProperty(ClienteAgente.class, "herenciaClientes", herenciaClientes);
					//Ya que se tiene el comprobante de cesion se cambia el status de la herencia a activo
					ValorCatalogoAgentes status =catalogoService.obtenerElementoEspecifico("Tipos de Estatus", "ACTIVO");
					herenciaClientes.setStatus(status);
					
					if(agenteDestino != null){
						if(agenteDestino.getId() != null){
							agenteDestino = agenteMidasService.loadById(agenteDestino);				
						}
					}
					
					clienteAgenteService.herenciaClientes(listaClientesCedidos,agenteDestino, herenciaClientes, "INACTIVO");
					herenciaClientes=entidadService.findById(HerenciaClientes.class, herenciaClientes.getId());
					herenciaClientes.setStatus(status);
					entidadService.save(herenciaClientes);
					setMensaje("La herencia ha sido activada exitosamente");
				}
			} catch (Exception e) {
				e.printStackTrace();
				setMensajeError("Error al Guardar");
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="generarLigaIfimax",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp")
		})
		public String generarLigaIfimax(){
		String []resp= new String[3];
		try {			
			agenteOrigen=agenteMidasService.loadById(agenteOrigen);
//			List<CatalogoDocumentoFortimax> docsCarpeta=documentoCarpetaService.obtenerDocumentosRequeridosPorCarpetaConTipoPersona("AGENTES", "HERENCIA DE CLIENTES", agenteDestino.getPersona().getClaveTipoPersona());				
//			resp=fortimaxService.generateLinkToDocument(herenciaClientes.getId(),"AGENTES", docsCarpeta.get(0).getNombreDocumentoFortimax()+"_"+herenciaClientes.getId());
			resp=fortimaxService.generateLinkToDocument(agenteOrigen.getId(),"AGENTES", "");
			urlIfimax=resp[0];		
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
		}	
	
	@Action(value="matchDocumentosAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchDocumentosAgente() {
			try {				
				documentoEntidadService.sincronizarDocumentos(agenteOrigen.getId(), "AGENTES", "HERENCIA DE CLIENTES");
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(agenteOrigen.getId(), "AGENTES", "HERENCIA DE CLIENTES");
				setMensaje(MENSAJE_EXITO);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			} catch (Exception e) {
				e.printStackTrace();
				setMensaje(MENSAJE_ERROR_GENERAL);
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
			return SUCCESS;
		}
	@Action(value="getLastHistoricoUpdate",results={
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^detHistorial\\.fechaHoraActualizacionString,^detHistorial\\.usuarioActualizacion"} ),
			@Result(name=INPUT, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^detHistorial\\.fechaHoraActualizacionString,^detHistorial\\.usuarioActualizacion"} )
	})
	public String getLastHistorcoUpdate() throws MidasException{
		TipoOperacionHistorial TipoOper=getTipoOperacionPorClave(110L);
		 List<ActualizacionAgente> historial=new ArrayList<ActualizacionAgente>();
		if(idHerencia!=null){
			try {
				historial=entidadHistoricoService.getHistory(TipoOper, idHerencia);
				if(!historial.isEmpty() || historial.get(0).getId()!=null){
					detHistorial=historial.get(0);
				}
			} catch (SystemException e) {
				setMensaje(e.getMessage());
				return INPUT;
			}
		}
		return SUCCESS;	
	}

	private TipoOperacionHistorial getTipoOperacionPorClave(Long id){
		TipoOperacionHistorial tipoOperacion=null;
		for(TipoOperacionHistorial tipo:TipoOperacionHistorial.values()){
			if(tipo.getValue().equals(id)){
				tipoOperacion=tipo;
				break;
			}
		}
		return tipoOperacion;
	}
	
}