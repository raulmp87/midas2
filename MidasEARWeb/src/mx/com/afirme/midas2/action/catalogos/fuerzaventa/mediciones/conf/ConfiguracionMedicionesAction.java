package mx.com.afirme.midas2.action.catalogos.fuerzaventa.mediciones.conf;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dao.fuerzaventa.mediciones.conf.ConfiguracionMedicionesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesPromotoria;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.MedicionesService;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.conf.ConfiguracionMedicionesService;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/mediciones/conf")
public class ConfiguracionMedicionesAction extends BaseAction implements Preparable {


	private static final long serialVersionUID = 1L;

	@Action(value="buscarAgentes", 
	interceptorRefs={@InterceptorRef(value="scrollableStack", params={"configAgente", "listadoConfigAgente"})},
	results = {
	@Result(name=SUCCESS, location = "/jsp/catalogos/fuerzaventa/mediciones/conf/columnasAgenteGrid.jsp")})
	public String buscarAgentes() {
		
		listadoConfigAgente = configuracionMedicionesDao.buscar(configAgente);
		
		return SUCCESS;
		
	}
	
	@Action(value="buscarPromotorias", 
	interceptorRefs={@InterceptorRef(value="scrollableStack", params={"configPromotoria", "listadoConfigPromotoria"})},
	results = {
	@Result(name=SUCCESS, location = "/jsp/catalogos/fuerzaventa/mediciones/conf/columnasPromotoriaGrid.jsp")})
	public String buscarPromotorias() {
		
		listadoConfigPromotoria = configuracionMedicionesDao.buscar(configPromotoria);
		
		return SUCCESS;
		
	}
	
	
	@Action(value="mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/mediciones/conf/configuracion.jsp")})
	public String mostrar(){
		
		correosAdicionales = configuracionMedicionesService.obtenerCorreosAdicionales();
		
		return SUCCESS;
		
	}

	@Action(value="actualizarAgente", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String actualizarAgente() {
		
		try {
		
			configuracionMedicionesService.actualizarAgente(configAgente.getAgente().getIdAgente(), configAgente.getEmail());
			
		} catch (Exception ex) {
		
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
	
	@Action(value="actualizarPromotoria", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String actualizarPromotoria() {
		
		try {
		
			configuracionMedicionesService.actualizarPromotoria(configPromotoria.getPromotoria().getIdPromotoria(), configPromotoria.getEmail());
			
		} catch (Exception ex) {
		
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
	
	@Action(value="agregarAgente", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String agregarAgente() {
		
		try {
			
			configuracionMedicionesService.agregarAgente(configAgente);
					
		} catch (Exception ex) {
		
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
		
	@Action(value="agregarPromotoria", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String agregarPromotoria() {
		
		try {
			
			configuracionMedicionesService.agregarPromotoria(configPromotoria);
		
		} catch (Exception ex) {
			
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
	}
	
	@Action(value="agregarCorreosAdicionales")
	public void agregarCorreosAdicionales() {
		
		configuracionMedicionesService.agregarCorreosAdicionales(correosAdicionales);
		
	}
	
	@Action(value="eliminarAgente", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String eliminarAgente() {
		
		try {
				
			configuracionMedicionesService.eliminarAgente(configAgente.getAgente().getIdAgente());
		
		} catch (Exception ex) {
			
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
	
	@Action(value="eliminarPromotoria", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String eliminarPromotoria() {
		
		try {
		
			configuracionMedicionesService.eliminarPromotoria(configPromotoria.getPromotoria().getIdPromotoria());
			
		} catch (Exception ex) {
			
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
	
	@Action(value="habilitarAgente", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String habilitarAgente() {
		
		try {
		
			configuracionMedicionesService.habilitarAgente(configAgente.getAgente().getIdAgente());
		
		} catch (Exception ex) {
			
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
	
	@Action(value="habilitarPromotoria", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String habilitarPromotoria() {
		
		try {
			
			configuracionMedicionesService.habilitarPromotoria(configPromotoria.getPromotoria().getIdPromotoria());
		
		} catch (Exception ex) {
			
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
	}
		
	@Action(value="enviarCorreosReporteSemanal", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^respuesta.*"})
			})
	public String enviarCorreosReporteSemanal() {
		
		try {
			
			medicionesService.enviarCorreosReporteSemanal();
		
		} catch (Exception ex) {
			
			respuesta = extractCustomMessage(ex);
						
		}
		
		return SUCCESS;
		
		
	}
	
		
	@Override
	public void prepare() throws Exception {
		
	}

		
	public ConfiguracionMedicionesAgente getConfigAgente() {
		return configAgente;
	}

	public void setConfigAgente(ConfiguracionMedicionesAgente configAgente) {
		this.configAgente = configAgente;
	}

	public ConfiguracionMedicionesPromotoria getConfigPromotoria() {
		return configPromotoria;
	}

	public void setConfigPromotoria(ConfiguracionMedicionesPromotoria configPromotoria) {
		this.configPromotoria = configPromotoria;
	}
		
	public List<ConfiguracionMedicionesAgente> getListadoConfigAgente() {
		return listadoConfigAgente;
	}

	public void setListadoConfigAgente(List<ConfiguracionMedicionesAgente> listadoConfigAgente) {
		this.listadoConfigAgente = listadoConfigAgente;
	}

	public List<ConfiguracionMedicionesPromotoria> getListadoConfigPromotoria() {
		return listadoConfigPromotoria;
	}

	public void setListadoConfigPromotoria(List<ConfiguracionMedicionesPromotoria> listadoConfigPromotoria) {
		this.listadoConfigPromotoria = listadoConfigPromotoria;
	}
	
	public String getCorreosAdicionales() {
		return correosAdicionales;
	}

	public void setCorreosAdicionales(String correosAdicionales) {
		this.correosAdicionales = correosAdicionales;
	}
	
	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	private String extractCustomMessage (Exception ex) {
		
		String[] msgError = (ex.getMessage() != null ? ex.getMessage() : MENSAJE_ERROR_GENERAL).split("Exception:");
		return msgError[msgError.length -1];
		
	}

	private ConfiguracionMedicionesAgente configAgente;
		
	private ConfiguracionMedicionesPromotoria configPromotoria;
		
	private List<ConfiguracionMedicionesAgente> listadoConfigAgente;
		
	private List<ConfiguracionMedicionesPromotoria> listadoConfigPromotoria;
	
	private String correosAdicionales;
	
	private String respuesta = "OK";
	
	private ConfiguracionMedicionesService configuracionMedicionesService;
	
	private ConfiguracionMedicionesDao configuracionMedicionesDao;
	
	private MedicionesService medicionesService;
	
	
	public ConfiguracionMedicionesService getConfiguracionMedicionesService() {
		return configuracionMedicionesService;
	}

	@Autowired
	public void setConfiguracionMedicionesService(ConfiguracionMedicionesService configuracionMedicionesService) {
		this.configuracionMedicionesService = configuracionMedicionesService;
	}

	
	public ConfiguracionMedicionesDao getConfiguracionMedicionesDao() {
		return configuracionMedicionesDao;
	}

	@Autowired
	public void setConfiguracionMedicionesDao(ConfiguracionMedicionesDao configuracionMedicionesDao) {
		this.configuracionMedicionesDao = configuracionMedicionesDao;
	}
	
	@Autowired
	public void setMedicionesService(MedicionesService medicionesService) {
		this.medicionesService = medicionesService;
	}
	
}
