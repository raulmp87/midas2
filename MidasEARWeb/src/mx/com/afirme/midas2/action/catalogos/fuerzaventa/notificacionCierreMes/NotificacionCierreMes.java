package mx.com.afirme.midas2.action.catalogos.fuerzaventa.notificacionCierreMes;

import mx.com.afirme.midas2.action.BaseAction;

import com.opensymphony.xwork2.Preparable;

public abstract class NotificacionCierreMes extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public abstract String guardar();
	
	public abstract String eliminar();
		
	public abstract String listar();
	
	public abstract String listarFiltrado();
		
	public abstract String verDetalle();
}
