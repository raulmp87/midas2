package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

public interface ReportMethods {
	/**
	 * Muestra los filtros
	 * 
	 * @return
	 */
	public String mostrarFiltros();

	/**
	 * Genera un pdf con la informacion generada en los filtros
	 * 
	 * @return
	 */
	public String exportarToPDF();

	/**
	 * Genera un excel con la informacion generada en los filtros
	 * 
	 * @return
	 */
	public String exportarToExcel();
}
