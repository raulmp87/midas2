package mx.com.afirme.midas2.action.catalogos.fuerzaventa.promotoria;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente.Situacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioPromotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioPromotoriaService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.validator.group.CapturaManualClavePromotoriaChecks;
import mx.com.afirme.midas2.validator.group.EditItemChecks;


@Component
@Scope("prototype")
@Namespace("/fuerzaventa/promotoria")
public class PromotoriaAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2306406311444197452L;
	/*********************************************************************************************
	 *  Services & Constants
	 ********************************************************************************************/
	private PromotoriaJPAService promotoriaService;
	private AgenteMidasService agenteMidasService;
	private EjecutivoService ejecutivoService;
	private ProductoBancarioPromotoriaService productoBancarioService;
	private ListadoService listadoService;
	private EntidadService entidadService;
	private BancoFacadeRemote bancoFacadeRemote;
	private static final String NAMESPACE="/fuerzaventa/promotoria";
	private static final String PRODUCTOS_BANCARIOS_GRID="/jsp/catalogos/fuerzaventa/promotoria/productosPromotoriaGrid.jsp";
	private static final String VER_DETALLE="/jsp/catalogos/fuerzaventa/promotoria/promotoriaDetalle.jsp";
	private static final String TAB_DATOS_GENERALES="datosPrincipalesPromotoria";
	private static final String TAB_DATOS_CONTABLES="datosContables";
	/*********************************************************************************************
	 *  View components
	 ********************************************************************************************/
	private Promotoria promotoria;
	private Promotoria filtroPromotoria;
	private Domicilio domicilio = new Domicilio();
	private List<PromotoriaView> listaPromotorias = new ArrayList<PromotoriaView>();
	private static List<ValorCatalogoAgentes> listaTipoPromotoria=new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaAgrupadores=new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaSituacion=new ArrayList<ValorCatalogoAgentes>();
	private List<EjecutivoView> listaEjecutivos=new ArrayList<EjecutivoView>();
	private List<Agente> listAgentes = new ArrayList<Agente>();
	private ProductoBancarioPromotoria producto;
	private List<ProductoBancarioPromotoria> productos=new ArrayList<ProductoBancarioPromotoria>();
	private List<ProductoBancario> productosBancarios=new ArrayList<ProductoBancario>();
	private static List<BancoDTO> listaBancos=new ArrayList<BancoDTO>();
	private ProductoBancario productoBancarioSeleccionado;
	private String tabActiva;
	private List<ProductoBancarioPromotoria> lista_ProdBancarios = new ArrayList<ProductoBancarioPromotoria>();
	private Long idPromotoria;
	private String fechaHoraActualizFormatoMostrar;
	private String  usuarioActualizacion;
	private ValorCatalogoAgentes situacionAutorizado;
	/*********************************************************************************************
	 *  Metodos y Acciones del Catalogo
	 ********************************************************************************************/
	
	@Override
	public void prepare() throws Exception {
		if("1".equals(tipoAccion)){
			promotoria=new Promotoria();
		}
//		if(promotoria!=null && promotoria.getId()!=null){
//			promotoria = promotoriaService.loadById(promotoria);
//		}
	}

	private void cargarComboTipoPromotoria(){
		if(isEmptyList(listaTipoPromotoria)){
			listaTipoPromotoria = cargarCatalogo("Tipos de Promotoria");
		}
	}
	
	private void cargarComboAgrupadores(){
		if(isEmptyList(listaAgrupadores)){
			listaAgrupadores = cargarCatalogo("Agrupadores de Promotorias");
		}
	}
	/**
	 * Se cargan solo los ejecutivos activos
	 */
	private void cargarComboEjecutivos(){
		Ejecutivo filtro=new Ejecutivo();
		filtro.setClaveEstatus(1l);
//		listaEjecutivos=ejecutivoService.findByFilters(filtro);
		listaEjecutivos=ejecutivoService.findByFiltersView(filtro);
	}
	
	private void cargarComboSituacion(){
		if(isEmptyList(listaSituacion)){
			listaSituacion=cargarCatalogo("Tipos de Estatus");
		}
	}
	
	public void prepareMostrarContenedor(){
		
	}
	
	/************************************************************************************************/
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/promotoria/promotoriaCatalogo.jsp")
		})
	public String mostrarContenedor(){
		cargarComboAgrupadores();
		cargarComboTipoPromotoria();
		cargarComboEjecutivos();
		cargarComboSituacion();
		return SUCCESS;
	}
	
	
	/************************************************************************************************/
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/promotoria/promotoriaGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/promotoria/promotoriaGrid.jsp")
		})
	@Override
	public String listarFiltrado() {
		try{
			listaPromotorias=promotoriaService.findByFiltersView(filtroPromotoria);
			}catch(Exception e){
				e.printStackTrace();
				return INPUT;
			}
		return SUCCESS;
	}	
	
	
	/************************************************************************************************/
	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location=VER_DETALLE)
		})
	@Override
	public String verDetalle(){		
		cargarComboAgrupadores();
		cargarComboTipoPromotoria();
		cargarComboEjecutivos();
//		cargarProductosBancarios();
		cargarBancos();
		if(promotoria!=null && promotoria.getId()!=null){
			promotoria = promotoriaService.loadById(promotoria);
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location=VER_DETALLE)
		})	
	public String verDetalleHistorico(){		
		cargarComboAgrupadores();
		cargarComboTipoPromotoria();
		cargarComboEjecutivos();
		cargarProductosBancarios();
		cargarBancos();
		if(promotoria!=null && promotoria.getId()!=null){
			promotoria = promotoriaService.loadById(promotoria, this.getUltimaModificacion().getFechaHoraActualizacionString());
			
			try {
				prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}

	/************************************************************************************************/
	@Action(value="mostrarAgentes",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/promotoria/mostrarAgentes.jsp")
		})
		public String mostrarAgentes(){
			try{
				listAgentes = agenteMidasService.findAgentesByPromotoria(promotoria.getId());
			}catch(Exception e){
				e.printStackTrace();
			}
			return SUCCESS;
		}
	/************************************************************************************************/
	public void validateGuardarPromotoria(){
		addErrors(promotoria, getValidationGroup(), this, "promotoria");
	}
	
	private Class<?> getValidationGroup() {
		
		if (promotoria.isCapturaManualClave()) {
			
			return CapturaManualClavePromotoriaChecks.class;
			
		}
			
		return EditItemChecks.class;
		
	}
	
	public void prepareGuardarPromotoria(){
		promotoria=new Promotoria();
	}
	@Action(value="guardarPromotoria",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","promotoria.id","${promotoria.id}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${promotoria.id}","tabActiva","${tabActiva}"}),
		@Result(name=INPUT,location=VER_DETALLE, params={"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tipoAccion","${tipoAccion}"})
	})
	public String guardarPromotoria(){
		try {
			
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			
			if(promotoria.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			
			promotoria=promotoriaService.saveFull(promotoria);
			guardarHistorico(TipoOperacionHistorial.PROMOTORIA, promotoria.getId(),"midas.promotoria.historial.accion",tipoAccionHistorial);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setTipoAccion(TipoAccionDTO.getEditar());
			setIdTipoOperacion(TipoOperacionHistorial.PROMOTORIA.getValue());
			setTabActiva(TAB_DATOS_CONTABLES);
						
		} catch (Exception e) {
			onError(e);
			return INPUT;
		}
		 return SUCCESS;
	}
	/**
	 * Action para navegar entre tabs
	 * @return
	 */
	@Action(value="moveToTab",results={
		@Result(name=SUCCESS,type="redirectAction",
			params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
			"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","promotoria.id","${promotoria.id}",
			"idTipoOperacion","${idTipoOperacion}","idRegistro","${promotoria.id}","tabActiva","${tabActiva}"}),
		@Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${promotoria.id}","tabActiva","${tabActiva}"})
	})
	public String moveToTab(){
		return result;
	}
	
	
	/************************************************************************************************/
	@Action(value="eliminar",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","promotoria.id","${promotoria.id}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${promotoria.id}"}),
				@Result(name=INPUT,type="redirectAction",
						params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
						"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","promotoria.id","${promotoria.id}",
						"idTipoOperacion","${idTipoOperacion}","idRegistro","${promotoria.id}"})

	})
	@Override
	public String eliminar() {
		try {
			setIdRegistro(promotoria.getId());
			setIdTipoOperacion(40L);
			promotoriaService.unsubscribe(promotoria);
			guardarHistorico(TipoOperacionHistorial.PROMOTORIA, promotoria.getId(),"midas.promotoria.historial.accion",TipoAccionHistorial.BAJA);
			onSuccess();
			tipoAccion="3";	
		} catch (Exception e) {
//			onError(e);
			setMensajeError(e.getMessage());
			tipoAccion="3";
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^promotoria.*,^situacionAutorizado.*","excludeProperties","^promotoria\\.agentesPromotores.*"})
	})
	public String findById() throws Exception {
		
		verDetalle();
		
		situacionAutorizado = catalogoService.obtenerElementoEspecifico(Situacion.GRUPO_CATALOGO, Situacion.AUTORIZADO.getValue());
		
		return SUCCESS;
		
	}
	
	/**
	 * Carga los productos bancarios registrados de una promotoria
	 */
	@Action(value="cargarProductosBancariosPorPromotoria",results={
		@Result(name=SUCCESS,location=PRODUCTOS_BANCARIOS_GRID),
		@Result(name=INPUT,location=PRODUCTOS_BANCARIOS_GRID)
	})
	public String cargarProductosBancariosPorPromotoria(){
		if(promotoria!=null && promotoria.getId()!=null){
			setTipoAccion(getTipoAccion()==null?"0":getTipoAccion());
			if(Integer.parseInt(getTipoAccion().trim())==5){
				productos=productoBancarioService.findByPromotoria(promotoria.getId(),(this.getUltimaModificacion().getFechaHoraActualizacionString()!=null)?this.getUltimaModificacion().getFechaHoraActualizacionString():"",getTipoAccion());
			}else{
				productos=productoBancarioService.findByPromotoria(promotoria.getId(),"",getTipoAccion());
			}
		}
		return result;
	}
	
	public void prepareGuardarProductoBancario(){
		int cont=0;
		for(ProductoBancarioPromotoria obj:lista_ProdBancarios){
			if(cont==0){
				idPromotoria = obj.getPromotoria().getId();
				cont++;
			}
		}
		lista_ProdBancarios = null;
	}
	
	@Action(value="guardarProductoBancario",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productos.*,mensaje,tipoMensaje, fechaHoraActualizFormatoMostrar, usuarioActualizacion"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productos.*,mensaje,tipoMensaje, fechaHoraActualizFormatoMostrar, usuarioActualizacion"})
	})
	public String guardarProductoBancario(){
		ActualizacionAgente actualizacionAgente = new ActualizacionAgente();
		try {
			productoBancarioService.saveListProductoBanc(lista_ProdBancarios);
//				productoBancarioService.saveProducto(obj);
			if(!lista_ProdBancarios.isEmpty()){
				guardarHistorico(TipoOperacionHistorial.PROMOTORIA, lista_ProdBancarios.get(0).getPromotoria().getId(),"midas.promotoria.historial.accion",TipoAccionHistorial.CAMBIO);
			}
			actualizacionAgente = obtenerultimamodif(idPromotoria);
			fechaHoraActualizFormatoMostrar = actualizacionAgente.getFechaHoraActualizacionString();
			usuarioActualizacion = actualizacionAgente.getUsuarioActualizacion();
			onSuccess();
		} catch (Exception e) {
			actualizacionAgente = obtenerultimamodif(idPromotoria);
			fechaHoraActualizFormatoMostrar = actualizacionAgente.getFechaHoraActualizacionString();
			usuarioActualizacion = actualizacionAgente.getUsuarioActualizacion();
			onError(e);
		}
		return result;
	}
	
	public ActualizacionAgente obtenerultimamodif(Long idPromotoria){
		ActualizacionAgente	actualizacionAgen = new ActualizacionAgente();
		try {
			actualizacionAgen = entidadHistoricoService.getLastUpdate(TipoOperacionHistorial.PROMOTORIA,idPromotoria);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return actualizacionAgen;
	}
	
	@Action(value="eliminarProductoBancario",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productos.*,mensaje,tipoMensaje"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})
	})
	public String eliminarProductoBancario(){
		try {
			Long idProducto=(producto!=null)?producto.getId():null;
			productoBancarioService.delete(idProducto);
			onSuccess();
		} catch (Exception e) {
			onError(e);
		}
		return result;
	}
	
	@Action(value="cargarProductoBancario",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productoBancarioSeleccionado.*","excludeProperties","^productoBancarioSeleccionado.banco,^productoBancarioSeleccionado\\.productoBancarioAgentes"})
	})
	public String cargarProductoBancario(){
		if(productoBancarioSeleccionado!=null && productoBancarioSeleccionado.getId()!=null){
			productoBancarioSeleccionado=entidadService.findById(ProductoBancario.class,productoBancarioSeleccionado.getId());
		}
		return SUCCESS;
	}
	
	@Action(value="listarProductosBancariosPorBanco",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productosBancarios.*\\.id,^productosBancarios.*\\.descripcion","excludeProperties","^productosBancarios.*\\.productoBancarioAgentes"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productosBancarios.*\\.id,^productosBancarios.*\\.descripcion","excludeProperties","^productosBancarios.*\\.productoBancarioAgentes"})
	})
	public String listarProductosBancariosPorBanco(){
		Long idBanco=(productoBancarioSeleccionado.getBanco()!=null && productoBancarioSeleccionado.getBanco().getIdBanco()!=null)?productoBancarioSeleccionado.getBanco().getIdBanco().longValue():null;
		productosBancarios=listadoService.getListaProductosBancariosPorBanco(idBanco);
		return SUCCESS;
	}
	/*********************************************************************************************
	 *  Commmon methods
	 ********************************************************************************************/
	private void cargarProductosBancarios(){
		productosBancarios=listadoService.getListaProductosBancarios();
	}
	
	private void cargarBancos(){
		if(isEmptyList(listaBancos)){
			listaBancos=bancoFacadeRemote.findAll();
		}
	}
	
	
	/*********************************************************************************************
	 *  Metodos setter y getter
	 ********************************************************************************************/
	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	public Promotoria getPromotoria() {
		return promotoria;
	}	
	
	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public List<PromotoriaView> getListaPromotorias() {
		return listaPromotorias;
	}

	public void setListaPromotorias(List<PromotoriaView> listaPromotorias) {
		this.listaPromotorias = listaPromotorias;
	}

	public Promotoria getFiltroPromotoria() {
		return filtroPromotoria;
	}

	public void setFiltroPromotoria(Promotoria filtroPromotoria) {
		this.filtroPromotoria = filtroPromotoria;
	}	

	public List<ValorCatalogoAgentes> getListaTipoPromotoria() {
		return listaTipoPromotoria;
	}

	public void setListaTipoPromotoria(
			List<ValorCatalogoAgentes> listaTipoPromotoria) {
		this.listaTipoPromotoria = listaTipoPromotoria;
	}

	public List<ValorCatalogoAgentes> getListaAgrupadores() {
		return listaAgrupadores;
	}

	public void setListaAgrupadores(List<ValorCatalogoAgentes> listaAgrupadores) {
		this.listaAgrupadores = listaAgrupadores;
	}	

	public List<Agente> getListAgentes() {
		return listAgentes;
	}

	public void setListAgentes(List<Agente> listAgentes) {
		this.listAgentes = listAgentes;
	}

	public List<EjecutivoView> getListaEjecutivos() {
		return listaEjecutivos;
	}

	public void setListaEjecutivos(List<EjecutivoView> listaEjecutivos) {
		this.listaEjecutivos = listaEjecutivos;
	}
	
	@Autowired
	@Qualifier("promotoriaJPAServiceEJB")
	public void setPromotoriaService(PromotoriaJPAService promotoriaService) {
		this.promotoriaService = promotoriaService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@Autowired
	@Qualifier("ejecutivoServiceEJB")
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("productoBancarioPromotoriaServiceEJB")
	public void setProductoBancarioService(ProductoBancarioPromotoriaService productoBancarioService) {
		this.productoBancarioService = productoBancarioService;
	}
	
	@Autowired
	@Qualifier("bancoFacadeRemoteEJB")
	public void setBancoFacadeRemote(BancoFacadeRemote bancoFacadeRemote) {
		this.bancoFacadeRemote = bancoFacadeRemote;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public ProductoBancarioPromotoria getProducto() {
		return producto;
	}

	public void setProducto(ProductoBancarioPromotoria producto) {
		this.producto = producto;
	}

	public List<ProductoBancarioPromotoria> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoBancarioPromotoria> productos) {
		this.productos = productos;
	}

	public List<ProductoBancario> getProductosBancarios() {
		return productosBancarios;
	}

	public void setProductosBancarios(List<ProductoBancario> productosBancarios) {
		this.productosBancarios = productosBancarios;
	}

	public List<BancoDTO> getListaBancos() {
		return listaBancos;
	}

	public void setListaBancos(List<BancoDTO> listaBancos) {
		this.listaBancos = listaBancos;
	}
	
	public ProductoBancario getProductoBancarioSeleccionado() {
		return productoBancarioSeleccionado;
	}

	public void setProductoBancarioSeleccionado(
			ProductoBancario productoBancarioSeleccionado) {
		this.productoBancarioSeleccionado = productoBancarioSeleccionado;
	}

	public List<ValorCatalogoAgentes> getListaSituacion() {
		return listaSituacion;
	}

	public void setListaSituacion(List<ValorCatalogoAgentes> listaSituacion) {
		this.listaSituacion = listaSituacion;
	}

	@Override
	public String guardar() {
		return null;
	}

	@Override
	public String listar() {
		return null;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public List<ProductoBancarioPromotoria> getLista_ProdBancarios() {
		return lista_ProdBancarios;
	}

	public void setLista_ProdBancarios(
			List<ProductoBancarioPromotoria> lista_ProdBancarios) {
		this.lista_ProdBancarios = lista_ProdBancarios;
	}

	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}

	public String getFechaHoraActualizFormatoMostrar() {
		return fechaHoraActualizFormatoMostrar;
	}

	public void setFechaHoraActualizFormatoMostrar(String fechaHoraActualizFormatoMostrar) {
		this.fechaHoraActualizFormatoMostrar = fechaHoraActualizFormatoMostrar;
	}

	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public ValorCatalogoAgentes getSituacionAutorizado() {
		return situacionAutorizado;
	}

	public void setSituacionAutorizado(ValorCatalogoAgentes situacionAutorizado) {
		this.situacionAutorizado = situacionAutorizado;
	}
	
	
}
