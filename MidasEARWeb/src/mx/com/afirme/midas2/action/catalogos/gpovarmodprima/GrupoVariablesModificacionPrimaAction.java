package mx.com.afirme.midas2.action.catalogos.gpovarmodprima;
import mx.com.afirme.midas2.domain.catalogos.*;

import java.util.LinkedHashMap;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.*;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class GrupoVariablesModificacionPrimaAction extends CatalogoAction implements Preparable {
	private Long id;

	private String tipoAccion; 

	private GrupoVariablesModificacionPrima grupoVariablesModificacionPrima;
	
	private List<GrupoVariablesModificacionPrima> grupoVariablesModificacionPrimaList;
		
	private GrupoVariablesModificacionPrimaService grupoVariablesModificacionPrimaService;
	
	private LinkedHashMap<Short,String> mapClaveTipoDetalle;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
	private static final long serialVersionUID = -5243724700575384865L;
	
	public void validateGuardar(){
		addErrors(grupoVariablesModificacionPrima, NewItemChecks.class, this, "grupoVariablesModificacionPrima");
		poblaClaveTipoDetalleMap();
	}
	
	public void validateEditar(){
		addErrors(grupoVariablesModificacionPrima, EditItemChecks.class, this, "grupoVariablesModificacionPrima");
	}

	public void prepare() throws Exception {

		if (getId() != null) {
			grupoVariablesModificacionPrima = grupoVariablesModificacionPrimaService.findById(getId());
		}
		
	}
	
	public String execute() {
		return null;
	}
	

	@Override
	public String eliminar() {
		grupoVariablesModificacionPrimaService.remove(grupoVariablesModificacionPrima);
		grupoVariablesModificacionPrima = new GrupoVariablesModificacionPrima();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String guardar() {
		grupoVariablesModificacionPrimaService.save(grupoVariablesModificacionPrima);
		grupoVariablesModificacionPrima = new GrupoVariablesModificacionPrima();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String listar() {
		grupoVariablesModificacionPrimaList = grupoVariablesModificacionPrimaService.findAll();
		return SUCCESS;
	}
	
	@Override
	public String listarFiltrado() {
		grupoVariablesModificacionPrimaList = grupoVariablesModificacionPrimaService.findByFilters(grupoVariablesModificacionPrima);
		
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		poblaClaveTipoDetalleMap();
		return SUCCESS;
	}

	
	public GrupoVariablesModificacionPrimaAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
		poblaClaveTipoDetalleMap();
	}
	
	
	



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public GrupoVariablesModificacionPrima getGrupoVariablesModificacionPrima() {
		return grupoVariablesModificacionPrima;
	}

	public void setGrupoVariablesModificacionPrima(
			GrupoVariablesModificacionPrima grupoVariablesModificacionPrima) {
		this.grupoVariablesModificacionPrima = grupoVariablesModificacionPrima;
	}

	public List<GrupoVariablesModificacionPrima> getGrupoVariablesModificacionPrimaList() {
		return grupoVariablesModificacionPrimaList;
	}

	public void setGrupoVariablesModificacionPrimaList(
			List<GrupoVariablesModificacionPrima> grupoVariablesModificacionPrimaList) {
		this.grupoVariablesModificacionPrimaList = grupoVariablesModificacionPrimaList;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	@Autowired
	@Qualifier("grupoVariablesModificacionPrimaEJB")
	public void setPrimaDescripcion(
			GrupoVariablesModificacionPrimaService grupoVariablesModificacionPrimaService) {
		this.grupoVariablesModificacionPrimaService = grupoVariablesModificacionPrimaService;
	}

	
	
	private void poblaClaveTipoDetalleMap(){
     
	mapClaveTipoDetalle = new LinkedHashMap<Short,String>();
	mapClaveTipoDetalle.put((short) 0, "Lista de Valores");
	mapClaveTipoDetalle.put((short) 1, "Lista de Rangos");
	
	}
	public void setMapClaveTipoDetalle(LinkedHashMap <Short,String> mapClaveTipoDetalle) {
		this.mapClaveTipoDetalle = mapClaveTipoDetalle;
        		
	}

	public LinkedHashMap <Short,String> getMapClaveTipoDetalle() {
		return mapClaveTipoDetalle;
	}
	
	
	

}
