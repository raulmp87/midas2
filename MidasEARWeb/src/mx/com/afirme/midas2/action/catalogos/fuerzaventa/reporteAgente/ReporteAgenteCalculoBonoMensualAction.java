package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO.EstatusEnvioRepCBMensual;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO.TipoBeneficiarioCBMensual;
import mx.com.afirme.midas2.dto.reportesAgente.EnvioReporteCalculoBonoMensualDTO.TipoEnvioRepCBMensual;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import mx.com.afirme.midas2.util.DateUtils;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/reporteAgenteCalculoBonoMensual")
public class ReporteAgenteCalculoBonoMensualAction extends ReporteAgenteBaseAction implements  Preparable,ReportMethods {

	/**
	 * 
	 */
	private static final long serialVersionUID = -979801868374897436L;
	private static Logger log = Logger.getLogger(ReporteAgenteCalculoBonoMensualAction.class);
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reporteAgenteCalculoBonoMensual.jsp";
	private Long idAgenteUsuario;
	private Long idBeneficiario;
	private Long tipoBeneficiario;
	private String tipoUsuario;
	private List<AgenteView> listaAgentes;
	private List<PromotoriaView> listaPromotorias;
	private List<Agente> agenteList;
	private List<Agente> agentesPromoList;
	private ConfigBonosService configBonosService;
	private UsuarioService usuarioService;
	private AgenteMidasService agenteMidasService;
	private Boolean chkTodosAgentes;	
	private Long idAgenteSeleccionado;
	private List<ValorCatalogoAgentes> tiposBeneficiario;
	private String idsSeleccionados;
	
	private ParametroGlobalService parametroGlobalService;
	
	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}	

	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@Autowired
	@Qualifier("parametroGlobalServiceEJB")
	public void setParametroGlobalService(
			ParametroGlobalService parametroGlobalService) {
		this.parametroGlobalService = parametroGlobalService;
	}
	
	//---
	

	@Override
	public void prepare() throws Exception {		
		
	}

	@Action(value="mostrarFiltros", results={
			@Result(name=SUCCESS, location=FILTROS_REPORTE),
			@Result(name = ERROR, location =FILTROS_REPORTE, params = {
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}"})
	})
	@Override
	public String mostrarFiltros() {

		try {

			this.obtenerTipoUsuario();

			setCentroOperacionList(this.entidadService
					.findAll(CentroOperacion.class));

			this.setTipoPromotoria(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipos de Promotoria"));

			this.setTipoAgente(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Clasificacion de Agente"));
			
			tiposBeneficiario = valorCatalogoAgentesService
					.obtenerElementosEspecificosPorCatalogo(
							"Tipo de Beneficiario", "Promotoria", "Agente");

			setGerenciasSeleccionadas(new LinkedList<Gerencia>());
			setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
			setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		} catch (Exception e) {

			e.printStackTrace();
			return ERROR;
		}

		return SUCCESS;
	}
	
	@Action(value = "buscarAgentes", results = { @Result(name = SUCCESS, location = "/jsp/reportesAgentes/reporteAgenteCalculoBonoMensualGrid.jsp") })
	public String buscarAgentes() {
				
			try {
				
				if(tipoBeneficiario.equals(GenerarPlantillaReporte.TIPO_BENEFICIARIO_AGENTE))
				{
					this.listaAgentes = configBonosService
					.obtenerAgentesPorConfiguracionCapturada(
							getCentroOperacionesSeleccionados(),
							getEjecutivosSeleccionados(),
							getGerenciasSeleccionadas(),
							getPromotoriasSeleccionadas(), null,
							getTipoAgentesSeleccionados(),
							getTipoPromotoriasSeleccioadas(),
							chkTodosAgentes == null ? agenteList : null,
							null);					
				}
				else
				{
					this.listaPromotorias = configBonosService
					.obtenerPromotoriasPorConfiguracionCapturada(
							getCentroOperacionesSeleccionados(),
							getEjecutivosSeleccionados(),
							getGerenciasSeleccionadas(),
							getPromotoriasSeleccionadas(),						
							getTipoPromotoriasSeleccioadas());	
					
					//convertir a lista agentes view para reusar JSP del grid.
					listaAgentes = new ArrayList<AgenteView>(); 
					for(PromotoriaView promotoria : listaPromotorias)
					{
						AgenteView agente = new AgenteView();
						agente.setId(promotoria.getId());
						agente.setIdAgente(promotoria.getId());
						agente.setNombreCompleto(promotoria.getDescripcion());
						           
						listaAgentes.add(agente);
					}
					
				}			

			} catch (Exception e) {
				e.printStackTrace();
			}		

		return SUCCESS;
	}

	@Action(value = "exportarToPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	@Override
	public String exportarToPDF() {		
		 
		Long claveBeneficiario = idBeneficiario;
		TransporteImpresionDTO transporte = getGenerarPlantillaReporteService().imprimirReporteAgenteCalculoBonoMensual(idBeneficiario, tipoUsuario, 
				getAnio(), getMes(), tipoBeneficiario);
		       
		//TEST servicio reporte bono pagado    
		//TransporteImpresionDTO transporte =  null;
		//	getGenerarPlantillaReporteService().imprimirReporteAgenteCalculoBonoPagado(idBeneficiario, tipoUsuario, tipoBeneficiario); //idCalculoBonos
		
		
		//TEST
		//configBonosService.enviarReporteCalculoBonoMensual(idBeneficiario,296L);
		
		    
		//configBonosService.enviarReporteCalculoBonoMensual(idBeneficiario, null,  
			//	getAnio(), getMes(), tipoBeneficiario);
		
		if (transporte != null) {
			setReporteAgenteStream(new ByteArrayInputStream(
					transporte.getByteArray()));
		} else {
			setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
			return INPUT;
		}
		
		if(tipoBeneficiario.equals(GenerarPlantillaReporte.TIPO_BENEFICIARIO_AGENTE))
		{
			Agente agente = new Agente();
			agente.setId(idBeneficiario);
			agente = agenteMidasService.loadById(agente);
			claveBeneficiario = agente.getIdAgente();
		}
		
		setContentType("application/pdf");
		setFileName("ReporteCalculoBono_ " + claveBeneficiario + "_.pdf");
		
		return SUCCESS;
	}

	@Override
	public String exportarToExcel() {
		
		return null;
	}

	private void obtenerTipoUsuario() throws Exception
	{
		if (usuarioService.tieneRolUsuarioActual("Rol_M2_AdministradorAgente") 
				|| usuarioService.tieneRolUsuarioActual("Rol_M2_CoordinadorAgente"))
		{
			tipoUsuario = GenerarPlantillaReporte.TIPO_USUARIO_ADMIN;
			
		}else if(usuarioService.tieneRolUsuarioActual("Rol_M2_Promotor"))
		{			
			tipoUsuario = GenerarPlantillaReporte.TIPO_USUARIO_PROMOTOR;   
			tipoBeneficiario = 839L;			
						
			Promotoria promotoria = entidadService.findByProperty(Promotoria.class, "personaResponsable.idPersona", 
					usuarioService.getAgenteUsuarioActual().getPersona().getIdPersona()).get(0);
			/*  TEST    
			Promotoria promotoria = entidadService.findByProperty(Promotoria.class, "personaResponsable.idPersona", 
					190462L).get(0);//test	*/
			//-------------------------------
			
			if(promotoria == null)
			{
				String mensajeE = "No se encontró promotoría asociada al usuario actual."; 
				this.setMensajeError(mensajeE);
				throw new RuntimeException(mensajeE);
			}
			
			this.setIdPromotoria(promotoria.getId());			
			
			agentesPromoList = agenteMidasService.findAgentesByPromotoria(promotoria.getId());
			
		}else if(usuarioService.tieneRolUsuarioActual("Rol_M2_Agente"))
		{
			tipoUsuario = GenerarPlantillaReporte.TIPO_USUARIO_AGENTE;
			Agente agente = usuarioService.getAgenteUsuarioActual();
			//Agente agente = agenteMidasService.findByCodigoUsuario("JUESPPER"); //test
			
			if(agente == null)
			{
				String mensajeE = "No se encontró agente asociado al usuario actual."; 
				this.setMensajeError(mensajeE);
				throw new RuntimeException(mensajeE);
			}
			
			idAgenteUsuario = agente.getId();
			
			
		}else
		{	
			String mensajeE = "El usuario no cuenta con un Rol valido configurado para acceder al Reporte."; 	
			this.setMensajeError(mensajeE);
			throw new RuntimeException(mensajeE); 			
		}
	}	
	
	@Action(value="enviarDocumentos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties","tipoMensaje,mensaje"})				
		})
	public String enviarDocumentos() {
		  
		List<Long> idsBeneficiarios = new ArrayList<Long>();

		idsSeleccionados = idsSeleccionados.replaceAll(", ", "");
		
		if (!idsSeleccionados.isEmpty()) {

			for (String idBeneficiarioStr : idsSeleccionados.split(",")) {
				idsBeneficiarios.add(Long.valueOf(idBeneficiarioStr));
			}

			Long limiteEnvios = 0L;

			try {
				String valor = parametroGlobalService
						.obtenerValorPorIdParametro(
								ParametroGlobalService.AP_MIDAS,
								ParametroGlobalService.LIMITE_ENVIO_MANUAL_REPORTE_CALCULOBONO_MENSUAL);

				limiteEnvios = (valor != null && !valor.isEmpty()) ? Long
						.valueOf(valor) : 0L;
						
			} catch (Exception e) {
				log.error(
						"Error al intentar leer el parametro global LIMITE_ENVIO_MANUAL_REPORTE_CALCULOBONO_MENSUAL",
						e);
			}
						             
			String fechaHoyTruncada =  DateUtils.toString(new Date(),
					"yyyy-MM-dd");
			
			Map<String,Object> params = new HashMap<String,Object>();
			ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();	
			try {
				estatus = valorCatalogoAgentesService.obtenerElementoEspecifico(GrupoCatalogoAgente.GRUPO_CAT_AGTE_ESTATUS_ENVIO_REPCB_MENSUAL,
						EstatusEnvioRepCBMensual.ENVIADO.getValue());
			} catch (Exception e) {
				
				log.error("Ocurrio un error al intentar obtener el catalogo estatus envio", e);
			}
			
			params.put("estatus", estatus);			

			StringBuilder queryWhere = new StringBuilder();
			queryWhere.append(" and model.fechaCreacion >= '" + fechaHoyTruncada + "'");    
			Long enviosRealizadosHoy = entidadService
					.findByColumnsAndPropertiesCount(
							EnvioReporteCalculoBonoMensualDTO.class, params,
							new HashMap<String, Object>(), queryWhere, null);

			if (idsBeneficiarios.size() <= (limiteEnvios- enviosRealizadosHoy)) {
				
				try{
				
				ValorCatalogoAgentes valorCatalogoAgentes = new ValorCatalogoAgentes();
				valorCatalogoAgentes.setId(tipoBeneficiario);
				valorCatalogoAgentes = valorCatalogoAgentesService.loadById(valorCatalogoAgentes);
				TipoBeneficiarioCBMensual beneficiario = null;
				
				if(valorCatalogoAgentes.getValor().equalsIgnoreCase(TipoBeneficiarioCBMensual.AGENTE.getValue()))
				{
					beneficiario = TipoBeneficiarioCBMensual.AGENTE;
					
				}else
				{
					beneficiario = TipoBeneficiarioCBMensual.PROMOTOR;
				}								
				
				Date fechaFiltroEnvioNotificacion = new Date();
				
				// registrar notificaciones
				configBonosService.registrarEnviosNotificacionRepCalculoBonoMensual(idsBeneficiarios, null,  
						getAnio(), getMes(), beneficiario, TipoEnvioRepCBMensual.MANUAL, 
						EstatusEnvioRepCBMensual.REGISTRADO, null);
				
				//enviar notificaciones				
				configBonosService.enviarNotificacionesRepCalculoBonoMensualRegistradas(TipoEnvioRepCBMensual.MANUAL, 
						EstatusEnvioRepCBMensual.REGISTRADO, fechaFiltroEnvioNotificacion);
				
				this.setMensajeExito();
				
				}catch(NegocioEJBExeption e)
				{
					this.setMensaje(e.getMessage());
					this.setTipoMensaje(TIPO_MENSAJE_INFORMACION); 
					 
				}
				catch(Exception e)
				{
					log.error("Error al procesar envio notificaciones Rep Calculo Bono Mensual", e);
					this.setMensajeError("Ha ocurrido un error al intentar procesar el Envio de Notificaciones");
				}
				
				
			} else {
				
				this.setMensajeError("No es posible enviar notificaciones a todos los beneficiarios seleccionados, " +
						"la suma de envíos realizados el día de hoy: " + enviosRealizadosHoy + " más el número de envíos que desea realizar: " + idsBeneficiarios.size()
						+ " superan el límite permitido: " + limiteEnvios);
			}

		}
		  
		return SUCCESS;
	}

	public Long getTipoBeneficiario() {
		return tipoBeneficiario;
	}

	public void setTipoBeneficiario(Long tipoBeneficiario) {
		this.tipoBeneficiario = tipoBeneficiario;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public Boolean getChkTodosAgentes() {
		return chkTodosAgentes;
	}

	public void setChkTodosAgentes(Boolean chkTodosAgentes) {
		this.chkTodosAgentes = chkTodosAgentes;
	}

	public Long getIdAgenteSeleccionado() {
		return idAgenteSeleccionado;
	}

	public void setIdAgenteSeleccionado(Long idAgenteSeleccionado) {
		this.idAgenteSeleccionado = idAgenteSeleccionado;
	}

	public Long getIdBeneficiario() {
		return idBeneficiario;
	}

	public void setIdBeneficiario(Long idBeneficiario) {
		this.idBeneficiario = idBeneficiario;
	}

	public List<ValorCatalogoAgentes> getTiposBeneficiario() {
		return tiposBeneficiario;
	}

	public void setTiposBeneficiario(List<ValorCatalogoAgentes> tiposBeneficiario) {
		this.tiposBeneficiario = tiposBeneficiario;
	}

	public List<Agente> getAgentesPromoList() {
		return agentesPromoList;
	}

	public void setAgentesPromoList(List<Agente> agentesPromoList) {
		this.agentesPromoList = agentesPromoList;
	}

	public Long getIdAgenteUsuario() {
		return idAgenteUsuario;
	}

	public void setIdAgenteUsuario(Long idAgenteUsuario) {
		this.idAgenteUsuario = idAgenteUsuario;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}	

}
