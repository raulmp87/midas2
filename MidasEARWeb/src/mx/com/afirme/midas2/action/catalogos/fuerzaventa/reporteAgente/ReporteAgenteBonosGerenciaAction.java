package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
@Namespace("/fuerzaventa/reporteAgenteBonosGerencia")
@Component
@Scope("prototype")
public class ReporteAgenteBonosGerenciaAction extends ReporteAgenteBaseAction implements ReportMethods, Preparable{

	private static final long serialVersionUID = 1L;
	private Long idAgente;
	private List<AgenteView> listaAgentes;
	private List<Agente> agenteList;
	@Override
	@Action(value = "mostrarFiltros", results = { @Result(name = SUCCESS, location = "/jsp/reportesAgentes/reporteAgenteBonosGerencia.jsp") })
	public String mostrarFiltros() {
//		final CentroOperacion filtro = new CentroOperacion();
//		setCentroOperacionList(centroOperacionService.findByFilters(filtro));
		setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		setClasificacionAgente(this.getValorCatalogoAgentes("Clasificacion de Agente"));
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	@Action(value = "exportarToExcel", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToExcel() {
		if (agenteList != null && !agenteList.isEmpty()) {
			idAgente = agenteList.get(0).getId();
		}
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
					.imprimirReporteAgenteBonosGerenciaToExcel(getAnio(),getMes(),
							getMesFin(),getIdCentroOperacion(), getIdGerencia(),
							getIdEjecutivo(), getIdPromotoria(), idAgente,getIdClasificacionAgente(),
							getLocale(), ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje(EMPTY_RESULT);
				return INPUT;
			}
			if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				setContentType("application/xls");
			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octet-stream");				
			}
			setFileName("reporteAgenteBonosXGerencia."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		} catch (RuntimeException exception) {
			setMensaje(EMPTY_RESULT);
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		if (listaAgentes != null) {
			agenteList = new ArrayList<Agente>();
			for (AgenteView agenteView : listaAgentes) {
				Agente agente = new Agente();
				agente.setId(agenteView.getId());
				agenteList.add(agente);
			}
		}
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

}
