package mx.com.afirme.midas2.action.catalogos;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas2.service.PersonaService;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

@Component("Persona")
@Scope("prototype")
@Namespace("/persona")
public class PersonaAction extends ActionSupport implements ServletRequestAware {
	private static final Logger LOG = Logger.getLogger(PersonaAction.class);
	
	
	/** serialVersionUID	 **/
	private static final long serialVersionUID = 4353957212273522121L;
	private static final String NOPHOTO_PATH = "/img/persona/nophoto.png";
	private static final String PHOTO_NAME = "foto.jpg";
	private InputStream inputStream;
	private HttpServletRequest servletRequest;
	private Long idPersona;
	private File foto;
	private String fotoContentType;
	private String fotoFileName;
	@EJB
	private PersonaService personaService;
	

	
	public InputStream obtenerFotoDefault() {
		try {
			final String filePath = servletRequest.getSession().getServletContext().getRealPath("/");
			final File file = new File(filePath + NOPHOTO_PATH);
			return new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new ByteArrayInputStream("?".getBytes());
		}
	}
	
	
	@Action(value = "obtenerFoto", results = { @Result(name = SUCCESS, type = "stream", params = {
			"contentType", "image/jpeg", "contentDisposition", PHOTO_NAME,
			"inputName", "inputStream",
			"contentDisposition", PHOTO_NAME }) })
	public String obtenerFoto() {
		try {
			if (idPersona == null) {
				inputStream = obtenerFotoDefault();
			} else {
				InputStream fotoPersona = personaService.obtenerFoto(idPersona);
				if (fotoPersona == null) {
					inputStream = obtenerFotoDefault();
				} else {
					inputStream = fotoPersona;
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarFoto", results = { @Result(name = SUCCESS, type = "json") })
	public String guardarFoto() {
		try {
			personaService.guardarFoto(idPersona, foto);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	

	/* ***** Getters and setters ***** */
	@Override
	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public File getFoto() {
		return foto;
	}
	public void setFoto(File foto) {
		this.foto = foto;
	}
	public String getFotoContentType() {
		return fotoContentType;
	}
	public void setFotoContentType(String fotoContentType) {
		this.fotoContentType = fotoContentType;
	}
	public String getFotoFileName() {
		return fotoFileName;
	}
	public void setFotoFileName(String fotoFileName) {
		this.fotoFileName = fotoFileName;
	}
	public PersonaService getPersonaService() {
		return personaService;
	}
	public void setPersonaService(PersonaService personaService) {
		this.personaService = personaService;
	}

}
