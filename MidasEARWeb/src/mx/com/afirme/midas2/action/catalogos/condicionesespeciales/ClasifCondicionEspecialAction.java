package mx.com.afirme.midas2.action.catalogos.condicionesespeciales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.domain.condicionesespeciales.Factor;
import mx.com.afirme.midas2.dto.condicionespecial.AreaCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.CoberturasCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.FactorCondicionDTO;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * 
 * @author Lizeth De La Garza
 * 
 * Seccion de Clasificacion de Condicion Especial
 * Nivel de Impacto, Areas de Impacto, Factores y Coberturas
 *
 */

@Component
@Scope("prototype")
@Namespace("/catalogos/condicionespecial/clasificacion")
public class ClasifCondicionEspecialAction extends BaseAction implements Preparable{
	
	private static final long serialVersionUID = 1L;
	
	private Long idCondicionEspecial;
	
	private Long idSeccion;
	
	private CondicionEspecial condicion;
	
	private List<EstatusCondicion>  listEstatus;
	
	private List<SeccionDTO> listSeccion;
	
	private List<CoberturasCondicionDTO> coberturasAsociadas = new ArrayList<CoberturasCondicionDTO>();
	
	private List<CoberturasCondicionDTO> coberturasDisponibles = new ArrayList<CoberturasCondicionDTO>();
	
	private List<AreaCondicionDTO> areasImpacto = new ArrayList<AreaCondicionDTO>();
	
	private List<FactorCondicionDTO> factores = new ArrayList<FactorCondicionDTO>();
	
	private FactorCondicionDTO factorDTO = new FactorCondicionDTO();
	
	private boolean consulta;
	
	private String accion;
	
	private BigDecimal idSeccionCobertura;
	
	private BigDecimal idCobertura;
	
	private Long idAreaImpacto;
	
	private boolean seleccionArea;
	
	private Long idFactor;
	
	private boolean seleccionFactor;
	
	private String valorFactor;
	
	private Short nivelImpacto;
	
	private Boolean vip;
	
	@Autowired
    @Qualifier("condicionEspecialServiceEJB")
	private CondicionEspecialService condicionEspecialService;
	
	@Autowired
	@Qualifier("seccionEJB")
	private SeccionFacadeRemote seccionFacade;

	@Override
	public void prepare() throws Exception {
		
		if (idCondicionEspecial != null) {
			condicion = condicionEspecialService.obtenerCondicion(idCondicionEspecial);
		}
		
		listEstatus = condicionEspecialService.obtenerEstatusDisponible(idCondicionEspecial);
		
		listSeccion = seccionFacade.listarVigentesAutosUsables();		
	}
	
	@Action (value = "mostrarClasificacion", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/clasificacionCondicionEspecial.jsp") })
	public String mostrarCondicion() {
		areasImpacto = condicionEspecialService.obtenerAreaCondicion(idCondicionEspecial);
		
		return SUCCESS;
	}	
	
	@Action (value = "salir", results = { 
			@Result(name = SUCCESS, type="redirectAction", 
					params={"actionName","mostrarContenedor",
					"namespace","/catalogos/condicionespecial"})})
	public String salir() {
		
		return SUCCESS;
	}
	
	@Action (value = "guardarNivelImpacto", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/nivelCondicion.jsp") })
	public String guardarNivelImpacto() {
		condicionEspecialService.guardarNivelImpacto(idCondicionEspecial, condicion.getNivelAplicacion());
		
		return SUCCESS;
	}
	
	@Action (value = "guardarVIP", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/nivelCondicion.jsp") })
	public String guardarVIP() {
		
		condicionEspecialService.guardarVIP(idCondicionEspecial, condicion.getVip());
		
		return SUCCESS;
	}
	
	/**	 
	  * SECCION COBERTURAS
    **/
	
	@Action
	(value = "obtenerCoberturasAsociadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/coberturasCondicionEspAsociadas.jsp") })
	public String obtenerCoberturasAsociadas() {
		
		coberturasAsociadas = condicionEspecialService.getLstCoberturasAsociadas(idCondicionEspecial);
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerCoberturasDisponibles", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/coberturasCondicionEspDisponibles.jsp") })
	public String obtenerCoberturasDisponibles() {
		
		coberturasDisponibles = condicionEspecialService.getLstCoberturasDisponibles(idCondicionEspecial, idSeccion);
		
		return SUCCESS;
	}
	
	@Action
	(value = "asociarCobertura", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/coberturasCondicionEspAsociadas.jsp") })
	public String asociarCobertura(){
			
		if ("asociaseccion".equals(accion)) {
			condicionEspecialService.asociarSeccionCondicion(idSeccionCobertura, idCondicionEspecial);
			
		} else if ("asociacobertura".equals(accion)) {
			condicionEspecialService.asociarCoberturaCondicion(idSeccionCobertura, idCobertura, idCondicionEspecial);
		}	
		coberturasAsociadas = condicionEspecialService.getLstCoberturasAsociadas(idCondicionEspecial);
		
		return SUCCESS;
	}
	
	@Action
	(value = "eliminarCobertura", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/coberturasCondicionEspDisponibles.jsp") })
	public String eliminarCobertura(){
			
		if ("eliminaseccion".equals(accion)) {
			condicionEspecialService.removerSeccionCondicion(idSeccionCobertura, idCondicionEspecial);
			
		} else if ("eliminacobertura".equals(accion)) {
			condicionEspecialService.removerCoberturaCondicion(idSeccionCobertura,  idCobertura, idCondicionEspecial);
		}		
		
		coberturasDisponibles = condicionEspecialService.getLstCoberturasDisponibles(idCondicionEspecial, idSeccion);
		
		return SUCCESS;
	}
	
	/**FIN SECCION COBERTURAS**/
	
	/**	 
	  * SECCION AREAS IMPACTO - FACTORES
    **/
	
	@Action
	(value = "obtenerFactoresCondicion", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/factoresCondicion.jsp") })
	public String obtenerFactoresCondicion() {
		
		factores = condicionEspecialService.obtenerFactorCondicion(idCondicionEspecial, idAreaImpacto);
		
		return SUCCESS;
	}
	
	@Action
	(value = "guardarAreaImpacto", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/areasCondicion.jsp") })
	public String guardarAreaImpacto() {
		
		if (seleccionArea) {
			condicionEspecialService.guardarAreaImpacto(idCondicionEspecial, idAreaImpacto);
		} else {
			condicionEspecialService.eliminarAreaImpacto(idCondicionEspecial, idAreaImpacto);
		}
		
		areasImpacto = condicionEspecialService.obtenerAreaCondicion(idCondicionEspecial);
		
		return SUCCESS;
	}
	
	@Action
	(value = "guardarFactores", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/factoresCondicion.jsp") })
	public String guardarFactores() {
		
		if (seleccionFactor) {
			condicionEspecialService.guardarFactor(idCondicionEspecial, idAreaImpacto, idFactor);
		} else {
			condicionEspecialService.eliminarFactor(idCondicionEspecial, idAreaImpacto, idFactor);
		}
		
		factores = condicionEspecialService.obtenerFactorCondicion(idCondicionEspecial, idAreaImpacto);
		
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarValorFactor", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/valorFactor.jsp") })
	public String mostrarValorFactor() {
		
		Factor factor = new Factor();
		factor.setId(idFactor);
		
		factorDTO.setIdAreaImpacto(idAreaImpacto);
		factorDTO.setIdCondicionEspecial(idCondicionEspecial);
		factorDTO.setFactor(factor);
		
		return SUCCESS;
	}
	
	
	@Action
	(value = "actualizarValorFactor", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/valorFactor.jsp") })
	public String actualizarValorFactor() {
		
		condicionEspecialService.guardarValorFactor(valorFactor, idCondicionEspecial, idAreaImpacto, idFactor);
		Factor factor = new Factor();
		factor.setId(idFactor);
		
		factorDTO.setIdAreaImpacto(idAreaImpacto);
		factorDTO.setIdCondicionEspecial(idCondicionEspecial);
		factorDTO.setFactor(factor);
		
		return SUCCESS;
	}
	
	/** FIN SECCION AREAS IMPACTO - FACTORES**/

	
	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}	

	public Long getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(Long idSeccion) {
		this.idSeccion = idSeccion;
	}

	public CondicionEspecial getCondicion() {
		return condicion;
	}

	public void setCondicion(CondicionEspecial condicion) {
		this.condicion = condicion;
	}

	public List<SeccionDTO> getListSeccion() {
		return listSeccion;
	}

	public void setListSeccion(List<SeccionDTO> listSeccion) {
		this.listSeccion = listSeccion;
	}

	public List<EstatusCondicion> getListEstatus() {
		return listEstatus;
	}

	public void setListEstatus(List<EstatusCondicion> listEstatus) {
		this.listEstatus = listEstatus;
	}	

	public List<CoberturasCondicionDTO> getCoberturasAsociadas() {
		return coberturasAsociadas;
	}

	public void setCoberturasAsociadas(
			List<CoberturasCondicionDTO> coberturasAsociadas) {
		this.coberturasAsociadas = coberturasAsociadas;
	}

	public List<CoberturasCondicionDTO> getCoberturasDisponibles() {
		return coberturasDisponibles;
	}

	public void setCoberturasDisponibles(
			List<CoberturasCondicionDTO> coberturasDisponibles) {
		this.coberturasDisponibles = coberturasDisponibles;
	}

	public List<AreaCondicionDTO> getAreasImpacto() {
		return areasImpacto;
	}

	public void setAreasImpacto(List<AreaCondicionDTO> areasImpacto) {
		this.areasImpacto = areasImpacto;
	}

	public List<FactorCondicionDTO> getFactores() {
		return factores;
	}

	public void setFactores(List<FactorCondicionDTO> factores) {
		this.factores = factores;
	}

	public FactorCondicionDTO getFactorDTO() {
		return factorDTO;
	}

	public void setFactorDTO(FactorCondicionDTO factorDTO) {
		this.factorDTO = factorDTO;
	}

	public boolean isConsulta() {
		return consulta;
	}

	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}


	public BigDecimal getIdSeccionCobertura() {
		return idSeccionCobertura;
	}

	public void setIdSeccionCobertura(BigDecimal idSeccionCobertura) {
		this.idSeccionCobertura = idSeccionCobertura;
	}

	public BigDecimal getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	} 	

	public Long getIdAreaImpacto() {
		return idAreaImpacto;
	}

	public void setIdAreaImpacto(Long idAreaImpacto) {
		this.idAreaImpacto = idAreaImpacto;
	}

	public boolean isSeleccionArea() {
		return seleccionArea;
	}

	public void setSeleccionArea(boolean seleccionArea) {
		this.seleccionArea = seleccionArea;
	}
	
	public Long getIdFactor() {
		return idFactor;
	}

	public void setIdFactor(Long idFactor) {
		this.idFactor = idFactor;
	}

	public boolean isSeleccionFactor() {
		return seleccionFactor;
	}

	public void setSeleccionFactor(boolean seleccionFactor) {
		this.seleccionFactor = seleccionFactor;
	}

	public String getValorFactor() {
		return valorFactor;
	}

	public void setValorFactor(String valorFactor) {
		this.valorFactor = valorFactor;
	}

	public Short getNivelImpacto() {
		return nivelImpacto;
	}

	public void setNivelImpacto(Short nivelImpacto) {
		this.nivelImpacto = nivelImpacto;
	}

	public Boolean getVip() {
		return vip;
	}

	public void setVip(Boolean vip) {
		this.vip = vip;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
	
	
	

}
