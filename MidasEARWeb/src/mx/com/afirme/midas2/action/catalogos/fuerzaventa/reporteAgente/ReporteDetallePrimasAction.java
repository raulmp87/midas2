package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteDetallePrimas")
@Component
@Scope("prototype")
@InterceptorRefs({ @InterceptorRef("timer"), @InterceptorRef("midas2Stack") })
public class ReporteDetallePrimasAction extends ReporteAgenteBaseAction
		implements Preparable, ReportMethods {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5603511785586688196L;
	private static final String NAMESPACE_DETALLE_PRIMAS_RETENCION_IMPUESTOS = "/fuerzaventa/retencionImpuestos";
	private static final String ACTION_DETALLE_PRIMAS = "mostrarFiltros";
	private static final String ACTION_DETALLE_PRIMAS_RETENCION_IMPUESTOS = "mostrarReporteRetencionImpuestos";
	private static final String FORM_DETALLE_PRIMAS = "/jsp/reportesAgentes/ReporteDetallePrimas.jsp";
	private List<Gerencia> gerenciaList = new ArrayList<Gerencia>(); 
	private List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
	private List<Promotoria> promorotiaList = new ArrayList<Promotoria>();
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	private InputStream reporteDetPrimasStream;
	
	private CentroOperacion centroOperacion = new CentroOperacion();
	private Gerencia gerencia = new Gerencia();
	private Ejecutivo ejecutivo = new Ejecutivo();
	private Promotoria promotoria = new Promotoria();
	private ValorCatalogoAgentes tipoAgentes = new ValorCatalogoAgentes();
	private Date fecha1 = new Date();
	private Date fecha2 = new Date();
	private String labelFechaInicio = "Fecha Corte Inicio";
	private String labelFechaFin = "Fecha Corte Fin";

	private RetencionImpuestosReporteView reporteRetencionImpuestosEstatales;
	private String nextAction;
	private String namespace;
	
	private TransporteImpresionDTO transporte;
	private String resultInputLocation;
	
	public static final String ESTATUS_REPORTE_COMPLETADO = "0";
	public static final String ESTATUS_REPORTE_EN_PROCESO = "1";
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public InputStream getReporteDetPrimasStream() {
		return reporteDetPrimasStream;
	}

	public void setReporteDetPrimasStream(InputStream reporteDetPrimasStream) {
		this.reporteDetPrimasStream = reporteDetPrimasStream;
	}

	public Agente getAgente() {
		return agente;
	}
	
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	public List<Gerencia> getGerenciaList() {
		return gerenciaList;
	}

	public void setGerenciaList(List<Gerencia> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}

	public List<Ejecutivo> getEjecutivoList() {
		return ejecutivoList;
	}

	public void setEjecutivoList(List<Ejecutivo> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}

	public List<Promotoria> getPromorotiaList() {
		return promorotiaList;
	}

	public void setPromorotiaList(List<Promotoria> promorotiaList) {
		this.promorotiaList = promorotiaList;
	}

	
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	public ValorCatalogoAgentes getTipoAgentes() {
		return tipoAgentes;
	}

	public void setTipoAgentes(ValorCatalogoAgentes tipoAgentes) {
		this.tipoAgentes = tipoAgentes;
	}

	public Date getFecha1() {
		return fecha1;
	}

	public void setFecha1(Date fecha1) {
		this.fecha1 = fecha1;
	}

	public Date getFecha2() {
		return fecha2;
	}

	public void setFecha2(Date fecha2) {
		this.fecha2 = fecha2;
	}

	public String getLabelFechaInicio() {
		return labelFechaInicio;
	}

	public void setLabelFechaInicio(String labelFechaInicio) {
		this.labelFechaInicio = labelFechaInicio;
	}

	public String getLabelFechaFin() {
		return labelFechaFin;
	}

	public void setLabelFechaFin(String labelFechaFin) {
		this.labelFechaFin = labelFechaFin;
	}

	/*******************************common method**********************************/
	@Action(value="mostrarFiltros",results={
			@Result(name=SUCCESS, location=FORM_DETALLE_PRIMAS)})
	@Override
	public String mostrarFiltros() {
		agente = null;
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		return null;
	}

	@Action(value = "exportarToExcel",
			interceptorRefs = {
			@InterceptorRef(value = "downloadFileStack", params = {
					"Reporte Detalle de Primas", "xlsx"})},
			results = {
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${transporte.contentType}",
					"inputName", "transporte.genericInputStream",
					"contentDisposition", "attachment;filename=${transporte.fileName}"
			}),
			@Result(name = INPUT, type = "redirectAction", params = {
					"namespace", "${namespace}", "actionName", "${nextAction}", "mensaje", "${mensaje}"
			})})
	@Override
	public String exportarToExcel() {
		DateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		
		try{
			if(reporteRetencionImpuestosEstatales != null) {
				namespace = NAMESPACE_DETALLE_PRIMAS_RETENCION_IMPUESTOS;
				nextAction = ACTION_DETALLE_PRIMAS_RETENCION_IMPUESTOS;
				transporte = getGenerarPlantillaReporteService().imprimirReporteDetPrimasRetencionImpuestosExcel(reporteRetencionImpuestosEstatales, ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
			} else {
				namespace = ServletActionContext.getActionMapping().getNamespace();
				nextAction = ACTION_DETALLE_PRIMAS;
				transporte = getGenerarPlantillaReporteService().imprimirReporteDetPrimasExcel(centroOperacion, gerencia, ejecutivo, promotoria, tipoAgentes, dateFormatter.format(getFechaInicio()), dateFormatter.format(getFechaFin()), agente,ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
			}
		} catch(RuntimeException e) {
			setMensaje(!e.getMessage().isEmpty() ? e.getMessage() : MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);
		}
				
		return SUCCESS;
	}
	
	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"})
		})
		
		public String obtenerAgente(){
		try {
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
//			onSuccess();
		} catch (Exception e) {
//			onError(e);
			
		}
		return SUCCESS;
	}

	@Override
	public void prepare()throws Exception{
		
	}
	public void prepareMostrarFiltros() throws Exception {

		try {
			final CentroOperacion filtro = new CentroOperacion();
//			setCentroOperacionList(centroOperacionService.findByFilters(filtro));
			setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
			setTipoPromotoria(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipos de Promotoria"));
			setTipoAgente(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Clasificacion de Agente"));// se cambia el catalogo Tipo de Agente por Clasificacion de Agente
			setEstatusAgente(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Estatus de Agente (Situacion)"));
			setTipoCedula(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipos de Cedula de Agente"));
		} catch (Exception e) {
//			prioridadList = new LinkedList<ValorCatalogoAgentes>();
		}
	}	

	public RetencionImpuestosReporteView getReporteRetencionImpuestosEstatales() {
		return reporteRetencionImpuestosEstatales;
	}
	public void setReporteRetencionImpuestosEstatales(
			RetencionImpuestosReporteView reporteRetencionImpuestosEstatales) {
		this.reporteRetencionImpuestosEstatales = reporteRetencionImpuestosEstatales;
	}
	public String getNextAction() {
		return nextAction;
	}
	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}
	public String getResultInputLocation() {
		return resultInputLocation;
	}
	public void setResultInputLocation(String resultInputLocation) {
		this.resultInputLocation = resultInputLocation;
	}

}
