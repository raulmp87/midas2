package mx.com.afirme.midas2.action.catalogos.fuerzaventa.grupoagente;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GrupoAgenteService;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * 
 * @author Efren Rodriguez
 *
 */

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/grupoAgente")
public class GrupoAgenteAction extends CatalogoAction implements Preparable {
	
	private static final Logger LOG = Logger.getLogger(GrupoAgenteAction.class);
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1601197314254103014L;
	private Agente filtroAgente = new Agente(); 
	private GrupoAgente filtroGrupoAgente; 
	private GrupoAgente grupoAgenteDTO; 
	private GrupoAgente tipoGrupoAgenteDTO;
	private String idsAgentes;
	private String clave;
	private String nombre;
	private String gerencia;
	
	private transient List<Object[]> listaAgenteDetalle;
	private List<AgenteView> listaAgente = new ArrayList<AgenteView>();
	private List<GrupoAgente> listaGrupoAgente = new ArrayList<GrupoAgente>();
	private List<AgenteView> listaAgenteGrid;
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	private transient AgenteMidasService agenteMidasService;
	
	@Autowired
	@Qualifier("grupoAgenteServiceEJB")
	private transient GrupoAgenteService grupoAgenteService;
	
				/* --------------------------- */
				/* --- GETTERS AND SETTERS --- */
				/* --------------------------- */
	public List<Object[]> getListaAgenteDetalle() {
		return listaAgenteDetalle;
	}

	public void setListaAgenteDetalle(List<Object[]> listaAgenteDetalle) {
		this.listaAgenteDetalle = listaAgenteDetalle;
	}

	public GrupoAgente getGrupoAgenteDTO() {
		return grupoAgenteDTO;
	}
	
	public GrupoAgente getTipoGrupoAgenteDTO() {
		return tipoGrupoAgenteDTO;
	}

	public void setTipoGrupoAgenteDTO(GrupoAgente tipoGrupoAgenteDTO) {
		this.tipoGrupoAgenteDTO = tipoGrupoAgenteDTO;
	}

	public void setGrupoAgenteDTO(GrupoAgente grupoAgenteDTO) {
		this.grupoAgenteDTO = grupoAgenteDTO;
	}

	public GrupoAgente getFiltroGrupoAgente() {
		return filtroGrupoAgente;
	}

	public void setFiltroGrupoAgente(GrupoAgente filtroGrupoAgente) {
		this.filtroGrupoAgente = filtroGrupoAgente;
	}

	public List<GrupoAgente> getListaGrupoAgente() {
		return listaGrupoAgente;
	}

	public void setListaGrupoAgente(List<GrupoAgente> listaGrupoAgente) {
		this.listaGrupoAgente = listaGrupoAgente;
	}

	public Agente getFiltroAgente() {
		return filtroAgente;
	}

	public void setFiltroAgente(Agente filtroAgente) {
		this.filtroAgente = filtroAgente;
	}

	public List<AgenteView> getListaAgente() {
		return listaAgente;
	}

	public void setListaAgente(List<AgenteView> listaAgente) {
		this.listaAgente = listaAgente;
	}

	public List<AgenteView> getListaAgenteGrid() {
		return listaAgenteGrid;
	}

	public void setListaAgenteGrid(List<AgenteView> listaAgenteGrid) {
		this.listaAgenteGrid = listaAgenteGrid;
	}
	
	public String getIdsAgentes() {
		return idsAgentes;
	}

	public void setIdsAgentes(String idsAgentes) {
		this.idsAgentes = idsAgentes;
	}
	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	
				/* --------------------------- */
				/* --- Metodos para el JSP --- */
				/* --------------------------- */
	
	public void prepare() throws Exception {
		//Implementacion necesaria por implementar Preparable, en este caso no es necesario utilizarlo.
	}

	@Action(value = "mostrarContenedor", results = { 
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteCatalogo.jsp") 
	})
	public String mostrarContenedor() {
		super.removeListadoEnSession();
		return SUCCESS;
	}

	@Action(value = "lista", results = { 
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteGrid.jsp") 
	})
	@Override
	public String listar() {
		return SUCCESS;
	}
	
	@Action(value = "listarFiltradoGrupo", results = {
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteGrid.jsp"),
		@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteGrid.jsp") 
	})
	@Override
	public String listarFiltrado() {
		try {
			listaGrupoAgente = grupoAgenteService.findByFilters(filtroGrupoAgente);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "listarFiltradoAgenteAsociado", results = {
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteGrupoGrid.jsp"),
		@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteGrupoGrid.jsp") 
	})
	public String listarFiltradoAgenteAsociado() {
		try {
			listaAgenteGrid = new ArrayList<AgenteView>();
			listaAgenteDetalle = grupoAgenteService.filtrarAgentesDeGrupo(grupoAgenteDTO.getId(),clave,nombre,gerencia);
			for (Object[] Agetem:listaAgenteDetalle){
				  AgenteView agente=new AgenteView();
				  agente.setId(Long.parseLong(Agetem[0].toString()));//id del grupo detalle
				  agente.setIdAgente(Long.parseLong(Agetem[1].toString()));
				  agente.setNombreCompleto(String.valueOf(Agetem[2]));
				  agente.setGerencia(String.valueOf(Agetem[3]));
				  listaAgenteGrid.add(agente);				  
			  }			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value = "listarFiltradoAgentes", results = {
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteGrid.jsp"),
		@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteGrid.jsp") 
	})
	public String listarFiltradoAgentes() {
		try {
			if (filtroAgente.getIdAgente() == null) {
				filtroAgente.setIdAgente(0l);
			}
			if (filtroAgente.getPromotoria().getEjecutivo().getGerencia().getIdGerencia() == null) {
				filtroAgente.getPromotoria().getEjecutivo().getGerencia().setIdGerencia(0l);
			}
			listaAgente = grupoAgenteService.filtrarAgente(filtroAgente);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "agenteAsociarGrupo", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteCatalogo.jsp") 
	})
	public String agenteAsociarGrupo() {
		loadById();
		return SUCCESS;
	}
	
	@Action(value = "obtenerAsociarGrupoAgentePorId", results = { 
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteGrupoGrid.jsp")
	})
	public String obtenerAsociarGrupoAgentePorId() {
		try {
			  listaAgenteGrid=new ArrayList<AgenteView>();
			  listaAgenteDetalle = grupoAgenteService.obtenerAgentesAsociadosEnGrupo(tipoGrupoAgenteDTO);
			  
			  for (Object[] Agetem:listaAgenteDetalle){
				  AgenteView agente=new AgenteView();
				  agente.setId(Long.parseLong(Agetem[0].toString()));//id del grupo detalle
				  agente.setIdAgente(Long.parseLong(Agetem[1].toString()));
				  agente.setNombreCompleto(String.valueOf(Agetem[2]));
				  agente.setGerencia(String.valueOf(Agetem[3]));
				  listaAgenteGrid.add(agente);
				  
			  }
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	@Action(value = "findAgentes", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/asociarAgenteGrid.jsp")
		})
	public String findAgentes() {
		try {
			listaAgente = agenteMidasService.findByFiltersWithAddressSupportPaging(filtroAgente,REGISTROS_A_MOSTRAR,0l);			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarAgentesAsociados", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje"})
	})
	public String guardarAgentesAsociadosEnGrupo() {
		try {
			grupoAgenteService.guardarAgentesAsociadosEnGrupo(grupoAgenteDTO.getId(),idsAgentes);
			setMensajeExito();
		} catch (Exception e) {
			setMensajeError("Error al guardar");
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}

				/* --------------------------- */
				/* --- ACCIONES PARA GRID ---- */
				/* --------------------------- */
	@Action(value = "guardar", results = {
		@Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "verDetalle", 
			"namespace", "/fuerzaventa/grupoAgente", 
			"tipoAccion", "${tipoAccion}", 
			"mensaje", "${mensaje}", 
			"grupoAgenteDTO.id", "${grupoAgenteDTO.id}",
			"idGrupo", "${grupoAgenteDTO.id}" 
		}),
		@Result(name = INPUT, type = "redirectAction", params = {
			"actionName", "verDetalle", 
			"namespace", "/fuerzaventa/grupoAgente", 
			"tipoAccion", "${tipoAccion}", 
			"mensaje", "${mensaje}", 
			"grupoAgenteDTO","${grupoAgenteDTO.id}", 
			"idGrupo", "${grupoAgenteDTO.id}" 
		})
	})
	@Override
	public String guardar() {
		setTipoAccion("4");
		try {
			grupoAgenteDTO.setFechaCreacion(new Date());
			grupoAgenteDTO.setFechaModificacion(new Date());
			entidadService.save(grupoAgenteDTO);
			setMensajeExito();
		} catch (Exception e) {
			setMensajeError("Ocurrio un Error al Guardar");
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	@Action(value = "eliminarGrupo", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
				"actionName", "verDetalle", 
				"namespace", "/fuerzaventa/grupoAgente", 
				"tipoAccion", "${tipoAccion}", 
				"mensaje", "${mensaje}", 
				"grupoAgenteDTO.id", "${grupoAgenteDTO.id}",
				"idRegistro", "${grupoAgenteDTO.id}" 
			}),
			@Result(name = INPUT, type = "redirectAction", params = {
				"actionName", "verDetalle", 
				"namespace", "/fuerzaventa/grupoAgente", 
				"tipoAccion","${tipoAccion}", 
				"mensaje", "${mensaje}", 
				"idRegistro", "${grupoAgenteDTO.id}" 
			})
	})
	public String eliminar() {
		setTipoAccion("3");
		try {
			grupoAgenteService.deleteGrupo(grupoAgenteDTO);
			setMensajeExito();
		} catch (Exception e) {
			setMensajeError("Error: Este grupo no se puede eliminar debido a que tiene agentes asociados.");
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	@Action(value = "verDetalle", results = { 
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/grupoAgente/grupoAgenteDetalle.jsp")
	})
	public String verDetalle() {
		loadByIdForVerDetalle();
		return SUCCESS;
	}
	
	/* COMMON METHODS */
	private void loadById() {
		if (grupoAgenteDTO != null && grupoAgenteDTO.getId() != null) {
			try {
				grupoAgenteDTO = grupoAgenteService.loadById(grupoAgenteDTO);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}
	
	private void loadByIdForVerDetalle() {
		loadById();
	}
	
}