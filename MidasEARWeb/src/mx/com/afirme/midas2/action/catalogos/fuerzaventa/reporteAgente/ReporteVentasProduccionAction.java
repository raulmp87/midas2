package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteVenteasProduccion")
@Component
@Scope("prototype")
public class ReporteVentasProduccionAction extends ReporteAgenteBaseAction implements Preparable, ReportMethods {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2111427620781256969L;
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reporteVentasProduccion.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private InputStream reporteVentasProduccion;
	private Integer crecimiento;
	private List<Gerencia> gerenciaList = new ArrayList<Gerencia>(); 
	private List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
	private List<Promotoria> promorotiaList = new ArrayList<Promotoria>();
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	
	private CentroOperacion centroOperacion = new CentroOperacion();
	private Gerencia gerencia = new Gerencia();
	private Ejecutivo ejecutivo = new Ejecutivo();
	private Promotoria promotoria = new Promotoria();
	private ValorCatalogoAgentes tipoAgentes = new ValorCatalogoAgentes();
	
	
	public InputStream getReporteVentasProduccion() {
		return reporteVentasProduccion;
	}

	public void setReporteVentasProduccion(InputStream reporteVentasProduccion) {
		this.reporteVentasProduccion = reporteVentasProduccion;
	}

	public Integer getCrecimiento() {
		return crecimiento;
	}

	public void setCrecimiento(Integer crecimiento) {
		this.crecimiento = crecimiento;
	}

	public List<Gerencia> getGerenciaList() {
		return gerenciaList;
	}

	public void setGerenciaList(List<Gerencia> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}

	public List<Ejecutivo> getEjecutivoList() {
		return ejecutivoList;
	}

	public void setEjecutivoList(List<Ejecutivo> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}

	public List<Promotoria> getPromorotiaList() {
		return promorotiaList;
	}

	public void setPromorotiaList(List<Promotoria> promorotiaList) {
		this.promorotiaList = promorotiaList;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public AgenteMidasService getAgenteMidasService() {
		return agenteMidasService;
	}

	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	public ValorCatalogoAgentes getTipoAgentes() {
		return tipoAgentes;
	}

	public void setTipoAgentes(ValorCatalogoAgentes tipoAgentes) {
		this.tipoAgentes = tipoAgentes;
	}

	@Action(value="mostrarFiltros", results={
		@Result(name=SUCCESS, location=FILTROS_REPORTE)
	})
	@Override
	public String mostrarFiltros() {
		agente = new Agente();
		setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		setClasificacionAgente(this.getValorCatalogoAgentes("Clasificacion de Agente"));
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="exportarToExcel",
			results={
				@Result(name=SUCCESS, type="stream", params={"contentType","${contentType}",
						"contentDisposition","attachtment;filename=\"${fileName}\"",
						"inputName","reporteAgenteStream" }),
				@Result(name="EMPTY", location=ERROR),
				@Result(name=INPUT,location=ERROR)
	})
	@Override
	public String exportarToExcel() {
	
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
					.imprimirReporteAgenteVentasProduccion(getAnio(),getMes(),getMesFin(),
							getCrecimiento() ,getIdCentroOperacion(), getIdGerencia(),
							getIdEjecutivo(), getIdPromotoria(), agente.getIdAgente(),
							getLocale(), ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje(EMPTY_RESULT);
				return INPUT;
			}
			if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				setContentType("application/xls");
			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octet-stream");				
			}
			setFileName("reporteAgenteVentasdeProducción."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		} catch (RuntimeException exception) {
			setMensaje(EMPTY_RESULT);
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}


}
