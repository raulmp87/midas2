package mx.com.afirme.midas2.action.catalogos.fuerzaventa.autorizacionagentes;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/autorizacionagentes")
public class AutorizacionAgentesAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5254231678983604843L;
	private ListadoService listadoService;
	private Agente agenteAutorizacion;
	private Agente filtroAgenteAutorizacion;
	private List<AgenteView> listaAgentesPorAutorizar=new ArrayList<AgenteView>();
	private AgenteMidasService agenteMidasService;
	private ValorCatalogoAgentesService catalogoService;
//	private PromotoriaJPAService promotoriaJpaService;
//	private Long idGerencia;
//	private Long idEjecutivo;
	private Map<Long, String> gerenciaList = new LinkedHashMap<Long, String>();
	private Map<Long, String> ejecutivoList = new LinkedHashMap<Long, String>();
	private Map<Long, String> promotoriaList = new LinkedHashMap<Long, String>();
//	private List<Promotoria> listPromotoria = new ArrayList<Promotoria>();
//	private List<Ejecutivo> listEjecutivo = new ArrayList<Ejecutivo>();
	private List<ValorCatalogoAgentes> listaCedulas = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaPrioridades = new ArrayList<ValorCatalogoAgentes>();
	private String idsAutorizar;
	private EntidadService entidadService;
	private String respuestaAutorizacion;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private FortimaxService fortimaxService;
	private ProductoBancarioAgenteService productoService;
	
/***Sets and gets****************************************************/
	
	public Agente getAgenteAutorizacion() {
		return agenteAutorizacion;
	}
	public void setAgenteAutorizacion(Agente agenteAutorizacion) {
		this.agenteAutorizacion = agenteAutorizacion;
	}
	public Agente getFiltroAgenteAutorizacion() {
		return filtroAgenteAutorizacion;
	}
	public void setFiltroAgenteAutorizacion(Agente filtroAgenteAutorizacion) {
		this.filtroAgenteAutorizacion = filtroAgenteAutorizacion;
	}	
	public List<AgenteView> getListaAgentesPorAutorizar() {
		return listaAgentesPorAutorizar;
	}
	public void setListaAgentesPorAutorizar(List<AgenteView> listaAgentesPorAutorizar) {
		this.listaAgentesPorAutorizar = listaAgentesPorAutorizar;
	}	
	
	public List<ValorCatalogoAgentes> getListaPrioridades() {
		return listaPrioridades;
	}
	public void setListaPrioridades(List<ValorCatalogoAgentes> listaPrioridades) {
		this.listaPrioridades = listaPrioridades;
	}
	//	public Long getIdEjecutivo() {
//		return idEjecutivo;
//	}
//	public void setIdEjecutivo(Long idEjecutivo) {
//		this.idEjecutivo = idEjecutivo;
//	}
	//	public List<Promotoria> getListPromotoria() {
//		return listPromotoria;
//	}
//	public void setListPromotoria(List<Promotoria> listPromotoria) {
//		this.listPromotoria = listPromotoria;
//	}	
	public List<ValorCatalogoAgentes> getListaCedulas() {
		return listaCedulas;
	}
	public void setListaCedulas(List<ValorCatalogoAgentes> listaCedulas) {
		this.listaCedulas = listaCedulas;
	}
	
	public String getIdsAutorizar() {
		return idsAutorizar;
	}
	public void setIdsAutorizar(String idsAutorizar) {
		this.idsAutorizar = idsAutorizar;
	}	
//	public List<Ejecutivo> getListEjecutivo() {
//		return listEjecutivo;
//	}
//	public void setListEjecutivo(List<Ejecutivo> listEjecutivo) {
//		this.listEjecutivo = listEjecutivo;
//	}
//	
	public Map<Long, String> getEjecutivoList() {
		return ejecutivoList;
	}
	public void setEjecutivoList(Map<Long, String> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}
	public Map<Long, String> getPromotoriaList() {
		return promotoriaList;
	}
	public void setPromotoriaList(Map<Long, String> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}
	
//	public Long getIdGerencia() {
//		return idGerencia;
//	}
//	public void setIdGerencia(Long idGerencia) {
//		this.idGerencia = idGerencia;
//	}
	public Map<Long, String> getGerenciaList() {
		return gerenciaList;
	}
	public void setGerenciaList(Map<Long, String> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}
	
	public String getRespuestaAutorizacion() {
		return respuestaAutorizacion;
	}
	public void setRespuestaAutorizacion(String respuestaAutorizacion) {
		this.respuestaAutorizacion = respuestaAutorizacion;
	}
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}
//	@Autowired
//	@Qualifier("promotoriaJPAServiceEJB")
//	public void setPromotoriaJpaService(PromotoriaJPAService promotoriaJpaService) {
//		this.promotoriaJpaService = promotoriaJpaService;
//	}
//	@Autowired
//	@Qualifier("ejecutivoServiceEJB")
//	public void setEjecutivoService(EjecutivoService ejecutivoService) {
//		this.ejecutivoService = ejecutivoService;
//	}
//
	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@Autowired
	@Qualifier("productoBancarioAgenteServiecEJB")
	public void setProductoService(ProductoBancarioAgenteService productoService) {
		this.productoService = productoService;
	}
/**************************************************************************/
	@Override
	public void prepare() throws Exception {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		gerenciaList=listadoService.getMapGerencias();
	}
	public void prepareMostrarContenedor(){
		
	}
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/autorizacionagentes/autorizacionAgentes.jsp")
		})
		public String mostrarContenedor(){
//			gerenciaList=listadoService.getMapGerencias();
			return SUCCESS;
		}
	
	
/*************************************************************************************************
 *  Lista Inicial del Catalogo 
 **************************************************************************************************/
	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}	
	

/*************************************************************************************************
 *  Lista los resultados obtenidos de la busqueda
 **************************************************************************************************/
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/autorizacionagentes/autorizacionAgentesGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/autorizacionagentes/autorizacionAgentesGrid.jsp")
		})
	@Override
	public String listarFiltrado() {
		ValorCatalogoAgentes tipoSituacion;
		try {
			tipoSituacion = catalogoService.obtenerElementoEspecifico("Estatus de Agente (Situacion)","PENDIENTE POR AUTORIZAR");
			if(filtroAgenteAutorizacion==null){
				filtroAgenteAutorizacion=new Agente();
			}
			filtroAgenteAutorizacion.setTipoSituacion(tipoSituacion);
			listaAgentesPorAutorizar = agenteMidasService.agentesPorAutorizar(filtroAgenteAutorizacion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//listaAgentesPorAutorizar = agenteMidasService.findByFilters(filtroAgenteAutorizacion);
		return SUCCESS;
	}
	
/*************************************************************************************************
 *  Metodo para abrir pantalla de rechazo de agente
 **************************************************************************************************/	
	@Action(value="rechazarAutorizacion",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/autorizacionagentes/rechazarAutorizacion.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/autorizacionagentes/rechazarAutorizacion.jsp")
		})
	@Override
	public String verDetalle() {
		listaCedulas = cargarCatalogo("Tipos de Cedula de Agente");
		listaPrioridades = cargarCatalogo("Prioridades de Agente");
		
		if(agenteAutorizacion != null && agenteAutorizacion.getId()!=null){
			agenteAutorizacion = agenteMidasService.loadById(agenteAutorizacion);
		}
		return SUCCESS;
	}
	
/*************************************************************************************************
 *  Metodo para cambiar el estatus del agente a "Rechazado"
 **************************************************************************************************/	

	@Action(value="eliminar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarContenedor",
							"namespace","/fuerzaventa/autorizacionagentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","mostrarContenedor",
							"namespace","/fuerzaventa/autorizacionagentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
		})
	@Override
	public String eliminar() {
		int diasPermiteRechazo = agenteAutorizacion.getDiasParaEmitirEnRechazo();
		String observacionesRechazo = agenteAutorizacion.getObservacionesRechazo();
		int motivoRechazo = agenteAutorizacion.getIdMotivoRechazo();
		Date fechaRechazo = agenteAutorizacion.getFechaAutorizacionRechazo();
			agenteAutorizacion = agenteMidasService.loadById(agenteAutorizacion);
		if(agenteAutorizacion!=null && agenteAutorizacion.getIdAgente() !=null){
			agenteAutorizacion.setDiasParaEmitirEnRechazo(diasPermiteRechazo);
			agenteAutorizacion.setObservacionesRechazo(observacionesRechazo);
			agenteAutorizacion.setIdMotivoRechazo(motivoRechazo);
			agenteAutorizacion.setFechaAutorizacionRechazo(fechaRechazo);
			
			try {			
				agenteMidasService.unsuscribe(agenteAutorizacion,"rechazar");				
				onSuccess();
			} catch (Exception e) {
				onError(e);
				setMensaje("Se han encontrado agente(s) con información incompleta");
			}
		}else{
			setMensaje("El Agente no cuenta con Número del Agente, no es posible cambiarle el estatus");
		}
	
		return SUCCESS;
	}

/*************************************************************************************************
 *  Metodo para cambiar el estatus del agente a "Aprobado"
 **************************************************************************************************/
	@Action(value="aprobarAgente",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarContenedor",
							"namespace","/fuerzaventa/autorizacionagentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","mostrarContenedor",
							"namespace","/fuerzaventa/autorizacionagentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
		})
	@Override
	public String guardar() {
		try {
			String[] idsPorAutorizar = getIdsAutorizar().split(",");
			int i;
			int existenErrores=0;
			int contador1=0;
			int contador2=0;
			StringBuilder detalleErrores = new StringBuilder("");
			
			Long tipoAgenteVentaDirecta = catalogoService.obtenerIdElementEspecifico("Tipo de Agente", "VENTAS DIRECTAS");
			Long tipoAgenteVentaDirecta2 = catalogoService.obtenerIdElementEspecifico("Tipo de Agente", "VENTA DIRECTA 2");
			
			for(i=0;i<idsPorAutorizar.length;i++){	
				
				Long id = Long.valueOf(idsPorAutorizar[i]);
				agenteAutorizacion = entidadService.findById(Agente.class, id);
				crearEstructuraAgente(agenteAutorizacion);
				List<DocumentoEntidadFortimax> listaDocumentosFortimax=documentoEntidadService.getListaDocumentosFaltantes(agenteAutorizacion.getId(), "AGENTES", "ALTA DE AGENTES");
				StringBuilder docsFaltantes = new StringBuilder();
				System.out.println("[AutorizacionAgentesAction]-[aprobarAgente]--tipoAgenteVentaDirecta="+tipoAgenteVentaDirecta);
				System.out.println("[AutorizacionAgentesAction]-[aprobarAgente]--tipoAgenteVentaDirecta2="+tipoAgenteVentaDirecta2);
				System.out.println("[AutorizacionAgentesAction]-[aprobarAgente]--agenteAutorizacion.getIdTipoAgente()="+agenteAutorizacion.getIdTipoAgente());
				
				//se identifica si el agente es de tipo venta directa para que en el siguiente paso no valide documentos.
				boolean esTipoVentaDir = false;
				if(tipoAgenteVentaDirecta.equals(agenteAutorizacion.getIdTipoAgente()) || tipoAgenteVentaDirecta2.equals(agenteAutorizacion.getIdTipoAgente())){
					esTipoVentaDir = true;
					System.out.println("[AutorizacionAgentesAction]-[aprobarAgente] si es de tipo venta directa ");
				}
				
				//se realiza conteo de los documentos pendientes por digitalizar
				int numDocumentosPendientes=0;
				if(!listaDocumentosFortimax.isEmpty() && !esTipoVentaDir){
					System.out.println("[AutorizacionAgentesAction]-[aprobarAgente] entra a validar los documentos a digitalizar");
					for(DocumentoEntidadFortimax docFaltante: listaDocumentosFortimax){
						docsFaltantes.append(docFaltante.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax());
						docsFaltantes.append(",");
					}
					numDocumentosPendientes++;					
				}
				
				//validar que almenos exista un prodcuto bancario con clave de pago marcada
				boolean existeProdBancConClave=false;
				List<ProductoBancarioAgente> productoBancario=productoService.findByAgente(agenteAutorizacion.getId());
				if(!productoBancario.isEmpty()){
					System.out.println("[AutorizacionAgentesAction]-[aprobarAgente] entra a validar producto bancario");
					for(ProductoBancarioAgente prod:productoBancario){
						if(prod.getClaveDefault()==1){
							existeProdBancConClave=true;
						}							
					}					
				}
				System.out.println("[AutorizacionAgentesAction]-[aprobarAgente] existeProdBancConClave="+existeProdBancConClave);
				System.out.println("[AutorizacionAgentesAction]-[aprobarAgente] numDocumentosPendientes="+numDocumentosPendientes);
				if(numDocumentosPendientes>0){
					existenErrores++;
					contador1++;
					detalleErrores.append(" -Se han encontrado ").append(contador1).append(" agente(s) que no contienen documentos requeridos por lo cual no han sido autorizados-");
				}				
				else if(!existeProdBancConClave){
					existenErrores++;
					contador2++;
					detalleErrores.append(" -Se ha(n) encontrado ").append(contador2).append(" agente(s) que no puede(n) ser Autorizado(s), se requiere minimo un producto bancario con calve de pago-");
				}
				else{
					agenteMidasService.unsuscribe(agenteAutorizacion,"autorizar");					
				}					
		   }			
		   if(existenErrores>0) {
			   setMensaje("Acción Terminada, pero "+detalleErrores.toString());
		   }else{
			   setMensaje(MENSAJE_EXITO);
		   }			
		   setTipoMensaje(TIPO_MENSAJE_EXITO);
		} catch (Exception e) {
			onError(e);
			setMensaje("Se han encontrado agente(s) con información incompleta");
		
		}
		return SUCCESS;
	}

	@Action(value="autorizaAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","respuestaAutorizacion"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","respuestaAutorizacion"})
		})	
	public String autorizaAgente() {		
		try {			
				String[] idsPorAutorizar = getIdsAutorizar().split(",");
				int i;
				int existenErrores=0;
				int contador1=0;
				int contador2=0;
				StringBuilder detalleErrores = new StringBuilder("");
				
				Long tipoAgenteVentaDirecta = catalogoService.obtenerIdElementEspecifico("Tipo de Agente", "VENTAS DIRECTAS");
				Long tipoAgenteVentaDirecta2 = catalogoService.obtenerIdElementEspecifico("Tipo de Agente", "VENTA DIRECTA 2");
				
				for(i=0;i<idsPorAutorizar.length;i++){	
					
					Long id = Long.valueOf(idsPorAutorizar[i]);
					agenteAutorizacion = entidadService.findById(Agente.class, id);
					crearEstructuraAgente(agenteAutorizacion);
					List<DocumentoEntidadFortimax> listaDocumentosFortimax=documentoEntidadService.getListaDocumentosFaltantes(agenteAutorizacion.getId(), "AGENTES", "ALTA DE AGENTES");
					StringBuilder docsFaltantes = new StringBuilder();
					System.out.println("[AutorizacionAgentesAction]-[autorizaAgente]--tipoAgenteVentaDirecta="+tipoAgenteVentaDirecta);
					System.out.println("[AutorizacionAgentesAction]-[autorizaAgente]--tipoAgenteVentaDirecta2="+tipoAgenteVentaDirecta2);
					System.out.println("[AutorizacionAgentesAction]-[autorizaAgente]--agenteAutorizacion.getIdTipoAgente()="+agenteAutorizacion.getIdTipoAgente());
					
					//se identifica si el agente es de tipo venta directa para que en el siguiente paso no valide documentos.
					boolean esTipoVentaDir = false;
					if(tipoAgenteVentaDirecta.equals(agenteAutorizacion.getIdTipoAgente()) || tipoAgenteVentaDirecta2.equals(agenteAutorizacion.getIdTipoAgente())){
						esTipoVentaDir = true;
						System.out.println("[AutorizacionAgentesAction]-[autorizaAgente] si es de tipo venta directa ");
					}
					
					//se realiza conteo de los documentos pendientes por digitalizar
					int numDocumentosPendientes=0;
					if(!listaDocumentosFortimax.isEmpty() && !esTipoVentaDir){
						System.out.println("[AutorizacionAgentesAction]-[autorizaAgente] entra a validar los documentos a digitalizar");
						for(DocumentoEntidadFortimax docFaltante: listaDocumentosFortimax){
							docsFaltantes.append(docFaltante.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax());
							docsFaltantes.append(",");
						}
						numDocumentosPendientes++;					
					}
					
					//validar que almenos exista un prodcuto bancario con clave de pago marcada
					boolean existeProdBancConClave=false;
					List<ProductoBancarioAgente> productoBancario=productoService.findByAgente(agenteAutorizacion.getId());
					if(!productoBancario.isEmpty()){
						System.out.println("[AutorizacionAgentesAction]-[autorizaAgente] entra a validar producto bancario");
						for(ProductoBancarioAgente prod:productoBancario){
							if(prod.getClaveDefault()==1){
								existeProdBancConClave=true;
							}							
						}					
					}
					System.out.println("[AutorizacionAgentesAction]-[autorizaAgente] existeProdBancConClave="+existeProdBancConClave);
					System.out.println("[AutorizacionAgentesAction]-[autorizaAgente] numDocumentosPendientes="+numDocumentosPendientes);
					System.out.println("[numDocumentosPendientes]-["+numDocumentosPendientes+"]");
					if(numDocumentosPendientes>0){
						existenErrores++;
						contador1++;
						detalleErrores.append(" -Se han encontrado ").append(contador1).append(" agente(s) que no contienen documentos requeridos por lo cual no han sido autorizados-");
					}				
					else if(!existeProdBancConClave){
						existenErrores++;
						contador2++;
						detalleErrores.append(" -Se ha(n) encontrado ").append(contador2).append(" agente(s) que no puede(n) ser Autorizado(s), se requiere minimo un producto bancario con calve de pago-");
					}
					else{
						agenteMidasService.unsuscribe(agenteAutorizacion,"autorizar");						
					}					
			   }
				System.out.println("[existenErrores]-["+existenErrores+"]");
			   if(existenErrores>0) {
				   setMensaje("Acción Terminada, pero "+detalleErrores.toString());
				   respuestaAutorizacion = "Acción Terminada, pero "+detalleErrores.toString();
			   }else{
				   setMensaje(MENSAJE_EXITO);
				   respuestaAutorizacion = "Acción realizada correctamente";
			   }			
//			   setTipoMensaje(TIPO_MENSAJE_EXITO);
//			   respuestaAutorizacion = "Acción realizada correctamente";
		} catch (Exception e) {			
			e.printStackTrace();
			respuestaAutorizacion=null;
			return INPUT;
		}
		
		return SUCCESS;
		
	}
	
	public void crearEstructuraAgente(Agente agente){
		try{
			agente=agenteMidasService.loadById(agente);		
			PersonaSeycosDTO personaSeycos = personaSeycosFacade.findById(agente.getPersona().getIdPersona());			
			Persona per = new Persona();
			per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
			per.setIdPersona(personaSeycos.getIdPersona());
			agente.setPersona(per);
			String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());			
			if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(agente.getId(), "AGENTES","ALTA DE AGENTES")){
				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpetaConTipoPersona("AGENTES", "ALTA DE AGENTES", per.getClaveTipoPersona());
					for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
						if(doc!=null){
							String []respDoc=fortimaxService.generateDocument(agente.getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+agente.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
							if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
								DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
								docFortimax.setCatalogoDocumentoFortimax(doc);
								docFortimax.setExisteDocumento(0);
								docFortimax.setIdRegistro(agente.getId());
								documentoEntidadService.save(docFortimax);
							}
						}
					}			
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
