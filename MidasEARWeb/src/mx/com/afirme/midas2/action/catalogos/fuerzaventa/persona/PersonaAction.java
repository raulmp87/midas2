package mx.com.afirme.midas2.action.catalogos.fuerzaventa.persona;

import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNull;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isValid;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.afirme.validation.curp.ValidateCurp;

import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilFacadeRemote;
import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.catalogos.sector.SectorFacadeRemote;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.wsCliente.curp.ValidaCURPDN;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote.TipoDomicilio;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

@Namespace("/fuerzaVenta/persona")
@Component
@Scope("prototype")
public class PersonaAction extends CatalogoHistoricoAction {

	private static final long serialVersionUID = 3180103021793183486L;

	private PersonaSeycosDTO personaSeycos;
	private Domicilio domicilio = new Domicilio();
	private String rfcSiglas;
	private String rfcFecha;
	private String rfcHomoclave;
	private String usuario;
	private String tabActiva;
	private String rangoFechas;
	private List<PersonaSeycosDTO> personaSeycosList = new ArrayList<PersonaSeycosDTO>();
	private List<Domicilio> domicilioList = new ArrayList<Domicilio>();
	private static List<SectorDTO> sectorList = new ArrayList<SectorDTO>();
	private static List<EstadoCivilDTO> estadosCiviles = new ArrayList<EstadoCivilDTO>();
	private PersonaSeycosFacadeRemote personaSeycosFacadeRemote;
	private SectorFacadeRemote sectorFacadeRemote;
	private EstadoCivilFacadeRemote estadoCivilFacade;
	private String isValidPersona;
	private String cargaInicial;
	private ValorCatalogoAgentesService catalogoService;
	/**Datos del curp**********************/
	private String estadoNacimientoCurp;
	private String codigoCURP;
	private String fechaNacimientoCurp;
	private String nombreCurp;
	private String apellidoPaternoCurp;
	private String apellidoMaternoCurp;
	private String sexoCurp;
	private String claveTipoPersonaCurp;
	private String curpValido;
	private Boolean isAdministradorAgente;

	private String idElemento = "";

	public void prepareMostrarPersonaDatosGrales() {
		if (isEmptyList(sectorList)) {
			sectorList = sectorFacadeRemote.findAll();
		}
		if (isEmptyList(estadosCiviles)) {
			estadosCiviles = estadoCivilFacade.findAll();
		}
		setRangoFechas(UtileriasWeb.getFechaString(new Date()));
		try {
			ValorCatalogoAgentes catalogo =new ValorCatalogoAgentes();
			catalogo = catalogoService.obtenerElementoEspecifico("Validacion por Usuario", "RFC");
			isAdministradorAgente = (catalogo.getClave().equals(getUsuarioActual().getNombreUsuario()))?true:false;
			cargarPersona();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "mostrarPersonaDatosGrales", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosGenerales.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosGenerales.jsp") })
	public String mostrarPersonaDatosGrales() {
		try {
			if (personaSeycos != null) {
				if (personaSeycos.getIdPersona() != null) {					
					
					if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona(),this.getUltimaModificacion().getFechaHoraActualizacionString());	
						
						prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
						
					}else
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());	
						
						prepareVerDetalle();
					}					
					
					/*Domicilio domicilioActual = null;
					for (Domicilio domicilio : personaSeycos.getDomicilios()) {
						if (domicilio.getTipoDomicilio() == null
								|| domicilio.getTipoDomicilio().trim()
										.equals("PERS")) {
							if (domicilioActual == null) {
								domicilioActual = domicilio;
							} else if (domicilioActual.getIdDomicilio()!=null &&domicilio.getIdDomicilio()!=null && domicilioActual.getIdDomicilio().getIdDomicilio() < domicilio
									.getIdDomicilio().getIdDomicilio()) {
								domicilioActual = domicilio;
							}
						}
					}
					domicilioList.add(domicilioActual);
					personaSeycos.setDomicilios(domicilioList);*/
					if (personaSeycos.getClaveTipoPersona() == 2) {
						rfcSiglas = personaSeycos.getCodigoRFC()
								.substring(0, 3);
						rfcFecha = personaSeycos.getCodigoRFC().substring(3, 9);
						rfcHomoclave = personaSeycos
								.getCodigoRFC()
								.substring(9,
										personaSeycos.getCodigoRFC().length())
								.trim();
					} else {
						rfcSiglas = personaSeycos.getCodigoRFC()
								.substring(0, 4);
						rfcFecha = personaSeycos.getCodigoRFC()
								.substring(4, 10);
						rfcHomoclave = personaSeycos.getCodigoRFC().substring(
								10, personaSeycos.getCodigoRFC().length());
					}
					
				} else {
					personaSeycos = new PersonaSeycosDTO();
					personaSeycos.setFechaAlta(new Date());
					personaSeycos.setClaveTipoPersona(Short.valueOf("1"));
					personaSeycos.setTipoSituacion(Short.valueOf("1"));
				}
			} else {
				personaSeycos = new PersonaSeycosDTO();
				personaSeycos.setFechaAlta(new Date());
				personaSeycos.setClaveTipoPersona(Short.valueOf("1"));
				personaSeycos.setTipoSituacion(Short.valueOf("1"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		tabActiva = "datosGenerales";
		
		return SUCCESS;
	}

	public void prepareMostrarPersonaDatosFiscales() {
		try {
			cargarPersona();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "mostrarPersonaDatosFiscales", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosFiscales.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosFiscales.jsp") })
	public String mostrarPersonaDatosFiscales() {
		try {
			if (personaSeycos != null) {
				if (personaSeycos.getIdPersona() != null)
					
					if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona(),this.getUltimaModificacion().getFechaHoraActualizacionString());	
						
						prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
						
						// obtener el Contacto o Representante Legal
						if(personaSeycos.getIdRepresentante()!=null){
							PersonaSeycosDTO representanteLeg = personaSeycosFacadeRemote
							.findById(personaSeycos.getIdRepresentante());
							personaSeycos.setNombreRepresentanteLegal(representanteLeg.getNombreCompleto());
						}
						
					}else
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());	
						
						prepareVerDetalle();
					}					
					
				if (personaSeycos.getClaveTipoPersona() == 2) {
					rfcSiglas = personaSeycos.getCodigoRFC().substring(0, 3);
					rfcFecha = personaSeycos.getCodigoRFC().substring(3, 9);
					rfcHomoclave = personaSeycos.getCodigoRFC().substring(9,
							personaSeycos.getCodigoRFC().length());
				} else {
					rfcSiglas = personaSeycos.getCodigoRFC().substring(0, 4);
					rfcFecha = personaSeycos.getCodigoRFC().substring(4, 10);
					rfcHomoclave = personaSeycos.getCodigoRFC().substring(10,
							personaSeycos.getCodigoRFC().length());
				}
				/*Domicilio domicilioFiscActual = null;
				Domicilio domicilioPersActual = null;
				for (Domicilio domicilio : personaSeycos.getDomicilios()) {
					if (domicilio.getTipoDomicilio() != null
							&& domicilio.getTipoDomicilio().trim()
									.equals("FISC")) {
						if (domicilioFiscActual == null) {
							domicilioFiscActual = domicilio;
						} else if (domicilioFiscActual.getIdDomicilio()!=null && domicilio.getIdDomicilio()!=null && domicilioFiscActual.getIdDomicilio().getIdDomicilio() < domicilio
								.getIdDomicilio().getIdDomicilio()) {
							domicilioFiscActual = domicilio;
						}
					} else if (domicilio.getTipoDomicilio() != null
							&& domicilio.getTipoDomicilio().trim()
									.equals("PERS")) {
						if (domicilioPersActual == null) {
							domicilioPersActual = domicilio;
						} else if (domicilioPersActual.getIdDomicilio()!=null && domicilio.getIdDomicilio()!=null && domicilioPersActual.getIdDomicilio().getIdDomicilio()< domicilio
								.getIdDomicilio().getIdDomicilio()) {
							domicilioPersActual = domicilio;
						}
					}
				}
				domicilioList.add(domicilioPersActual);
				domicilioList.add(domicilioFiscActual);
				personaSeycos.setDomicilios(domicilioList);*/
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	public void prepareMostrarPersonaDatosOficina() {
		try {
			cargarPersona();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Action(value = "mostrarPersonaDatosOficina", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosOficina.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosOficina.jsp") })
	public String mostrarPersonaDatosOficina() {
		try {
			if (personaSeycos != null) {
				if (personaSeycos.getIdPersona() != null)
					
					if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona(),this.getUltimaModificacion().getFechaHoraActualizacionString());	
						
						prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
						
					}else
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());	
						
						prepareVerDetalle();
					}					
					
				/*Domicilio domicilioActual = null;
				Domicilio domicilioPersActual = null;
				Domicilio domicilioFiscActual = null;
				for (Domicilio domicilio : personaSeycos.getDomicilios()) {
					if (domicilio.getTipoDomicilio() != null
							&& domicilio.getTipoDomicilio().trim()
									.equals("OFIC")) {
						if (domicilioActual == null) {
							domicilioActual = domicilio;
						} else if (domicilioActual.getIdDomicilio()!=null && domicilio.getIdDomicilio() !=null && domicilioActual.getIdDomicilio().getIdDomicilio() < domicilio
								.getIdDomicilio().getIdDomicilio()) {
							domicilioActual = domicilio;
						}
					} else if (domicilio.getTipoDomicilio() != null
							&& domicilio.getTipoDomicilio().trim()
									.equals("PERS")) {
						if (domicilioPersActual == null) {
							domicilioPersActual = domicilio;
						} else if (domicilioPersActual.getIdDomicilio()!=null && domicilio.getIdDomicilio()!=null && domicilioPersActual.getIdDomicilio().getIdDomicilio() < domicilio
								.getIdDomicilio().getIdDomicilio()) {
							domicilioPersActual = domicilio;
						}
					} else if (domicilio.getTipoDomicilio() != null
							&& domicilio.getTipoDomicilio().trim()
									.equals("FISC")) {
						if (domicilioFiscActual == null) {
							domicilioFiscActual = domicilio;
						} else if (domicilioFiscActual.getIdDomicilio() !=null && domicilio.getIdDomicilio() !=null && domicilioFiscActual.getIdDomicilio().getIdDomicilio() < domicilio
								.getIdDomicilio().getIdDomicilio()) {
							domicilioFiscActual = domicilio;
						}
					}
				}
				domicilioList.add(domicilioPersActual);
				domicilioList.add(domicilioFiscActual);
				domicilioList.add(domicilioActual);
				personaSeycos.setDomicilios(domicilioList);*/
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	public void prepareMostrarPersonaDatosContacto() {
		try {
			cargarPersona();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Common method
	 * 
	 * @throws Exception
	 */
	private void cargarPersona() throws Exception {
		if (personaSeycos != null && personaSeycos.getIdPersona() != null) {
			
			if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
			{
				personaSeycos = personaSeycosFacadeRemote
				.findById(personaSeycos.getIdPersona(),this.getUltimaModificacion().getFechaHoraActualizacionString());						
			}else
			{
				personaSeycos = personaSeycosFacadeRemote.findById(personaSeycos
						.getIdPersona());				
			}			
			
		} else {
			personaSeycos = new PersonaSeycosDTO();
		}
	}

	@Action(value = "mostrarPersonaDatosContacto", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosContacto.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/personaDatosContacto.jsp") })
	public String mostrarPersonaDatosContacto() {
		try {
			if (personaSeycos != null) {
				if (personaSeycos.getIdPersona() != null)
					
					if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona(),this.getUltimaModificacion().getFechaHoraActualizacionString());	
						
						prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
						
					}else
					{
						personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());
						
						prepareVerDetalle();
					}				
			}
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "mostrarCapturaPersona", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/detallePersona.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/detallePersona.jsp") })
	public String mostrarCapturaPersona() {
		if (tabActiva == null || tabActiva.equals("")) {
			tabActiva = "datosGenerales";
		}
		return SUCCESS;
	}

	public void validateGuardarDatosGeneralesPersona() {
		// addErrors(personaSeycos, this, "personaSeycos");
	}

	@Action(value = "guardarDatosGeneralesPersona", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}",
					"tabActiva", "${tabActiva}", "personaSeycos.idPersona",
					"${personaSeycos.idPersona}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}" }) })
	public String guardarDatosGeneralesPersona() {
		try {
			if (personaSeycos != null && personaSeycos.getDomicilios() == null) {
				personaSeycos.setDomicilios(new ArrayList<Domicilio>());
			}
			Domicilio domicilio = (personaSeycos.getDomicilios() != null && !personaSeycos
					.getDomicilios().isEmpty()) ? personaSeycos.getDomicilios()
					.get(0) : null;
			if (domicilio != null) {
				domicilio.setTipoDomicilio(TipoDomicilio.PERSONAL.getValue());
			}
			tabActiva = "datosFiscales";
			guardarPersonaGeneralFiscal("datosGenerales");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
	}

	public void validateGuardarDatosFiscalesPersona() {
		addErrors(personaSeycos, this, "personaSeycos");
	}

	@Action(value = "guardarDatosFiscalesPersona", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}",
					"tabActiva", "${tabActiva}", "personaSeycos.idPersona",
					"${personaSeycos.idPersona}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}",
					"tabActiva", "${tabActiva}", "personaSeycos.idPersona",
					"${personaSeycos.idPersona}" }) })
	public String guardarDatosFiscalesPersona() {
		try {
			Domicilio domicilio = (personaSeycos.getDomicilios() != null && !personaSeycos
					.getDomicilios().isEmpty()) ? personaSeycos.getDomicilios()
					.get(1) : null;
			if (domicilio != null) {
				domicilio.setTipoDomicilio(TipoDomicilio.FISCAL.getValue());
				personaSeycos.getDomicilios().clear();
				personaSeycos.getDomicilios().add(domicilio);
			}
			tabActiva = "datosOficina";
			guardarPersonaGeneralFiscal("datosFiscales");
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
	}

	@SuppressWarnings("unused")
	private void guardarPersonaGeneralFiscal(String tab) {
		try {
			TipoAccionHistorial tipoAccionHistorial = TipoAccionHistorial.ALTA;
			if (personaSeycos != null && personaSeycos.getIdPersona() != null) {
				tipoAccionHistorial = TipoAccionHistorial.CAMBIO;
			}
			StringBuilder rfc = new StringBuilder();
			rfc.append(rfcSiglas);
			rfc.append(rfcFecha);
			rfc.append(rfcHomoclave);
			personaSeycos.setCodigoRFC(rfc.toString());
			personaSeycos = personaSeycosFacadeRemote.save(personaSeycos);
			guardarHistoricoPersona(tab, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Guarda los datos de historico de persona segun la pestaña y el tipo de
	 * accion
	 * 
	 * @param tab
	 * @param unsubscribe
	 */
	private void guardarHistoricoPersona(String tab, boolean unsubscribe) {
		TipoAccionHistorial tipoAccionHistorial = TipoAccionHistorial.ALTA;
		if (unsubscribe) {
			tipoAccionHistorial = TipoAccionHistorial.BAJA;
		} else if (personaSeycos != null
				&& personaSeycos.getIdPersona() != null) {
			tipoAccionHistorial = TipoAccionHistorial.CAMBIO;
		}
		String keyMessage = "midas.persona.historial.accion." + tab;
		guardarHistorico(TipoOperacionHistorial.PERSONA,
				personaSeycos.getIdPersona(), keyMessage, tipoAccionHistorial);
	}

	public void validateGuardarDatosOficinaPersona() {
		addErrors(personaSeycos, this, "personaSeycos");
	}

	@Action(value = "guardarDatosOficinaPersona", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}",
					"tabActiva", "${tabActiva}", "personaSeycos.idPersona",
					"${personaSeycos.idPersona}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}",
					"tabActiva", "${tabActiva}", "personaSeycos.idPersona",
					"${personaSeycos.idPersona}" }) })
	public String guardarDatosOficinaPersona() {
		try {
			Domicilio domicilioOficina = personaSeycos.getDomicilios().get(2);
			personaSeycos.getDomicilios().clear();
			personaSeycos.getDomicilios().add(domicilioOficina);
			personaSeycosFacadeRemote.saveOficeData(personaSeycos);
			tabActiva = "datosContacto";
			guardarHistoricoPersona("datosOficina", false);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
	}

	public void validateGuardarDatosContactoPersona() {
		addErrors(personaSeycos, this, "personaSeycos");
	}

	@Action(value = "guardarDatosContactoPersona", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "tipoAccion", "${tipoAccion}",
					"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
					"tabActiva", "${tabActiva}", "personaSeycos.idPersona",
					"${personaSeycos.idPersona}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona", "namespace",
					"/fuerzaVenta/persona", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}", "tipoAccion",
					"${tipoAccion}", "tabActiva", "${tabActiva}",
					"personaSeycos.idPersona", "${personaSeycos.idPersona}" }) })
	public String guardarDatosContactoPersona() {
		try {
			personaSeycosFacadeRemote.saveContactData(personaSeycos);
			tabActiva = "datosContacto";
			guardarHistoricoPersona(tabActiva, false);
			// setMensaje(MENSAJE_EXITO);
			// setTipoMensaje(TIPO_MENSAJE_EXITO);
			// tipoAccion="4";
			// setIdTipoOperacion(90L);
			return SUCCESS;
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			e.printStackTrace();
			return INPUT;
		}
	}

	@Action(value = "eliminarPersona", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarListadoPersona", "mensaje",
					"${mensaje}", "tipoMensaje", "${tipoMensaje}", "namespace",
					"/fuerzaVenta/persona", "tabActiva", "${tabActiva}",
					"personaSeycos.idPersona", "${personaSeycos.idPersona}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarCapturaPersona",
					"personaSeycos.idPersona", "${personaSeycos.idPersona}",
					"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}",
					"tipoAccion", "${tipoAccion}", "namespace",
					"/fuerzaVenta/persona" }) })
	public String eliminarPersona() {
		try {
			personaSeycosFacadeRemote.unsubscribe(personaSeycos);
			tabActiva = "Persona";
			guardarHistoricoPersona(tabActiva, true);
			setMensaje("La persona ha sido dada de baja exitosamente");
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			return SUCCESS;
		} catch (Exception e) {
			setTipoAccion("3");
			e.printStackTrace();
			setMensaje(MENSAJE_ERROR_ACTUALIZAR+" "+e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
	}

	@Action(value = "mostrarListadoPersona", results = {
		@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/listadoPersona.jsp"),
		@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/listadoPersona.jsp"),
		@Result(name = "SUCCESSAGNTE", location = "/jsp/suscripcion/cotizacion/auto/agente/include/busquedaPersona.jsp")})
	public String mostrarListadoPersona() {
		if(tipoAccion!=null && tipoAccion.equals("cotizador_agentes")){
			return "SUCCESSAGNTE";
		}
		return SUCCESS;
	}

	@Action(value = "cargarListadoPersona", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/persona/listadoPersonaGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/fuerzaventa/persona/listadoPersonaGrid.jsp") })
	public String cargarListadoPersona() {
		try {
			if ("consulta".equalsIgnoreCase(tipoAccion)) {
				if (personaSeycos == null) {
					personaSeycos = new PersonaSeycosDTO();
				}
				personaSeycos.setSituacionPersona("AC");
			}
			// boolean filtroVacioPersona=filtroVacio();
			boolean esCargaInicial = (isValid(cargaInicial) && "1"
					.equals(cargaInicial));
			if (esCargaInicial) {
				personaSeycos = null;
			}
			personaSeycosList = personaSeycosFacadeRemote.findByFilters(
					personaSeycos, false);
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}

	private boolean filtroVacio() {
		boolean esVacio = false;
		if (isNotNull(personaSeycos)) {
			if (!isValid(personaSeycos.getNombreCompleto())
					&& !isValid(personaSeycos.getRazonSocial())
					&& !isValid(personaSeycos.getCodigoRFC())
					&& !isValid(personaSeycos.getTelOficina())
					&& isNull(personaSeycos.getFechaAlta())
					&& isNull(personaSeycos.getFechaBaja())) {
				esVacio = true;
			}
			List<Domicilio> domicilios = personaSeycos.getDomicilios();
			Domicilio domicilio = (!isEmptyList(domicilios)) ? domicilios
					.get(0) : null;
			boolean domicilioEmpty = isEmptyAddress(domicilio);
			if (domicilioEmpty) {
				esVacio = true;
			}
		}
		return esVacio;
	}

	private boolean isEmptyAddress(Domicilio domicilio) {
		boolean isEmpty = true;
		if (isNotNull(domicilio)) {
			if (isValid(domicilio.getCalleNumero())) {
				isEmpty = false;
			}
			if (isValid(domicilio.getCodigoPostal())) {
				isEmpty = false;
			}
			if (isValid(domicilio.getCiudad())) {
				isEmpty = false;
			}
			if (isValid(domicilio.getClaveCiudad())) {
				isEmpty = false;
			}
			if (isValid(domicilio.getClaveEstado())) {
				isEmpty = false;
			}
			if (isValid(domicilio.getNombreColonia())) {
				isEmpty = false;
			}
		}
		return isEmpty;
	}

	/**
	 * Action para cargar el detalle de info de una persona
	 * 
	 * @return
	 */
	@Action(value = "findById", results = {
			@Result(name = SUCCESS, type = "json", params = { "noCache",
					"true", "ignoreHierarchy", "false", "includeProperties",
					"^personaSeycos.*" }),
			@Result(name = INPUT, type = "json", params = { "noCache", "true",
					"ignoreHierarchy", "false", "includeProperties",
					"^personaSeycos.*" }) })
	public String findById() {
		if (personaSeycos != null && personaSeycos.getIdPersona() != null) {
			try {
				personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
		}
		return SUCCESS;
	}

	@Action(value = "revisionPersonaCompleta", results = {
			@Result(name = SUCCESS, type = "json", params = { "noCache",
					"true", "ignoreHierarchy", "false", "includeProperties",
					"isValidPersona" }),
			@Result(name = INPUT, type = "json", params = { "noCache", "true",
					"ignoreHierarchy", "false", "includeProperties",
					"isValidPersona" }) })
	public String revisionPersonaCompleta() {
		if (personaSeycos != null && personaSeycos.getIdPersona() != null) {
			try {
				personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());
				if ("IN".equals(personaSeycos.getSituacionPersona())) {
					boolean tieneDomicilioOficina = false, tieneDomicilioFiscal = false, tieneDomicilioPersonal = false;
					for (Domicilio domicilio : personaSeycos.getDomicilios()) {
						// Se valida que el tipo de domicilio sea OFIC,FISC,PERS
						// de lo contrario no es valido
						if (domicilio!=null && domicilio.getTipoDomicilio() != null) {
							if ("PERS".equalsIgnoreCase(domicilio.getTipoDomicilio().trim())) {
								tieneDomicilioPersonal = true;
							} else if ("OFIC".equalsIgnoreCase(domicilio.getTipoDomicilio().trim())) {
								tieneDomicilioOficina = true;
							} else if ("FISC".equalsIgnoreCase(domicilio.getTipoDomicilio().trim())) {
								tieneDomicilioFiscal = true;
							}
						}
					}
					// Si no tiene los 3 domicilios entonces es invalido
					if (!(tieneDomicilioFiscal && tieneDomicilioOficina && tieneDomicilioPersonal)) {
						isValidPersona = "nok";
					} else if (personaSeycos.getTelCasa() == null
							|| personaSeycos.getTelCasa().isEmpty()) {// Sino,
																		// si no
																		// tiene
																		// telefono
																		// entonces
																		// tambien
																		// es
																		// invalido
						isValidPersona = "nok";
					} else {
						isValidPersona = "ok";
					}
				} else {
					isValidPersona = "La persona ya se encuentra Activa";
				}
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
		}
		return SUCCESS;
	}

	@Action(value = "activaPersonaCompleta", results = {
			@Result(name = SUCCESS, type = "json", params = { "noCache",
					"true", "ignoreHierarchy", "false", "includeProperties",
					"isValidPersona,mensaje,tipoMensaje,tipoAccion" }),
			@Result(name = INPUT, type = "json", params = { "noCache", "true",
					"ignoreHierarchy", "false", "includeProperties",
					"isValidPersona,mensaje,tipoMensaje,tipoAccion" }) })
	public String activaPersonaCompleta() {
		if (personaSeycos != null && personaSeycos.getIdPersona() != null) {
			try {
				TipoAccionHistorial tipoAccionHistorial = TipoAccionHistorial.ALTA;
				if (personaSeycos != null && personaSeycos.getIdPersona() != null) {
					tipoAccionHistorial = TipoAccionHistorial.CAMBIO;
				}
				personaSeycos = personaSeycosFacadeRemote
						.findById(personaSeycos.getIdPersona());
				personaSeycos.setSituacionPersona("AC");
				personaSeycosFacadeRemote.saveWithoutAddress(personaSeycos);
				isValidPersona = "ok";
				guardarHistoricoPersona("datosGenerales", false);
				onSuccess();
			} catch (Exception e) {
				onError(e);
				return INPUT;
			}
		}
		return SUCCESS;
	}
	/**
	 * Action para valdiar el curp
	 * @param estadoNacimientoCurp
	 * @param codigoCURP
	 * @param fechaNacimientoCurp
	 * @param nombreCurp
	 * @param apellidoPaternoCurp
	 * @param apellidoMaternoCurp
	 * @param sexoCurp
	 * @param claveTipoPersonaCurp
	 * @return
	 */
	@Action(value="validarCurpPersona",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true", "ignoreHierarchy", "false", "includeProperties","curpValido"})	
	})
	public String validaCURPPersonaAgentes() {
		boolean valorReturn = true;
		if (estadoNacimientoCurp != null && !estadoNacimientoCurp.equals("") && codigoCURP != null && !codigoCURP.equals("") && fechaNacimientoCurp != null && !fechaNacimientoCurp.equals("")
				&& nombreCurp != null && !nombreCurp.equals("") && apellidoPaternoCurp != null && !apellidoPaternoCurp.equals("") && apellidoMaternoCurp != null && !apellidoMaternoCurp.equals("")
				&& sexoCurp != null && !sexoCurp.equals("") && claveTipoPersonaCurp != null && claveTipoPersonaCurp.equals("1")) {
			ValidateCurp curpVO = new ValidateCurp();
			curpVO.setBornState(estadoNacimientoCurp);
			curpVO.setCapturedCurp(codigoCURP);
			curpVO.setPersonFirstLastName(apellidoPaternoCurp);
			curpVO.setPersonSecondLastName(apellidoMaternoCurp);
			curpVO.setBirthDate(fechaNacimientoCurp);
			curpVO.setPersonName(nombreCurp);
			if (sexoCurp != null && sexoCurp.equals("M")) {
				curpVO.setPersonGender("MASCULINO");
			} else {
				curpVO.setPersonGender("FEMENINO");
			}
			try {
				curpVO = ValidaCURPDN.getInstancia().valida(curpVO);
				valorReturn = curpVO.isValid();
			} catch (ExcepcionDeAccesoADatos e) {
				valorReturn = false;
				e.printStackTrace();
			} catch (SystemException e) {
				valorReturn = false;
				e.printStackTrace();
			}
		} else {
			if (claveTipoPersonaCurp != null && claveTipoPersonaCurp.equals("2")) {
				valorReturn = true;
			} else {
				valorReturn = false;
			}
		}
		curpValido=(valorReturn)?"1":"0";
		return SUCCESS;
	}

	@SuppressWarnings("deprecation")
	private String calcularRangoFechas() {
		Calendar fecha = Calendar.getInstance();
		Integer anioActual = fecha.get(Calendar.YEAR);
		return (anioActual - Sistema.EDAD_MAXIMA) + ":"
				+ (anioActual - Sistema.EDAD_MINIMA);
	}

	public String getRfcSiglas() {
		return rfcSiglas;
	}

	public void setRfcSiglas(String rfcSiglas) {
		this.rfcSiglas = rfcSiglas;
	}

	public String getRfcFecha() {
		return rfcFecha;
	}

	public void setRfcFecha(String rfcFecha) {
		this.rfcFecha = rfcFecha;
	}

	public String getRfcHomoclave() {
		return rfcHomoclave;
	}

	public void setRfcHomoclave(String rfcHomoclave) {
		this.rfcHomoclave = rfcHomoclave;
	}

	public PersonaSeycosDTO getPersonaSeycos() {
		return personaSeycos;
	}

	public void setPersonaSeycosDTO(PersonaSeycosDTO personaSeycos) {
		this.personaSeycos = personaSeycos;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public void setPersonaSeycos(PersonaSeycosDTO personaSeycos) {
		this.personaSeycos = personaSeycos;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List<SectorDTO> getSectorList() {
		return sectorList;
	}

	public void setSectorList(List<SectorDTO> sectorList) {
		this.sectorList = sectorList;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public List<PersonaSeycosDTO> getPersonaSeycosList() {
		return personaSeycosList;
	}

	public void setPersonaSeycosList(List<PersonaSeycosDTO> personaSeycosList) {
		this.personaSeycosList = personaSeycosList;
	}

	public List<EstadoCivilDTO> getEstadosCiviles() {
		return estadosCiviles;
	}

	public void setEstadosCiviles(List<EstadoCivilDTO> estadosCiviles) {
		this.estadosCiviles = estadosCiviles;
	}

	public String getRangoFechas() {
		return rangoFechas;
	}

	public void setRangoFechas(String rangoFechas) {
		this.rangoFechas = rangoFechas;
	}

	@Override
	public void prepare() throws Exception {
		// Si no es modificacion, se limpian los datos de la persona y luego se
		// planchan al entrar a
		// cada metodo correspondiente
		// TODO NOTA: NO borrar, de lo contrario se mostraran datos anteriores
		// de la entidad guardada anteriormente.
		if (!"4".equals(tipoAccion)) {
			personaSeycos = new PersonaSeycosDTO();
		}
	}

	@Override
	public String guardar() {
		return null;
	}

	@Override
	public String eliminar() {
		return null;
	}

	@Override
	public String listar() {
		return null;
	}

	@Override
	public String listarFiltrado() {
		return null;
	}

	public String getIsValidPersona() {
		return isValidPersona;
	}

	public void setIsValidPersona(String isValidPersona) {
		this.isValidPersona = isValidPersona;
	}

	/**
	 * ==========================================================
	 * 						Datos del curp
	 * ==========================================================
	 */
	
	public String getCurpValido() {
		return curpValido;
	}

	public void setCurpValido(String curpValido) {
		this.curpValido = curpValido;
	}
	
	

	public String getEstadoNacimientoCurp() {
		return estadoNacimientoCurp;
	}

	public void setEstadoNacimientoCurp(String estadoNacimientoCurp) {
		this.estadoNacimientoCurp = estadoNacimientoCurp;
	}

	public String getCodigoCURP() {
		return codigoCURP;
	}

	public void setCodigoCURP(String codigoCURP) {
		this.codigoCURP = codigoCURP;
	}

	public String getFechaNacimientoCurp() {
		return fechaNacimientoCurp;
	}

	public void setFechaNacimientoCurp(String fechaNacimientoCurp) {
		this.fechaNacimientoCurp = fechaNacimientoCurp;
	}

	public String getNombreCurp() {
		return nombreCurp;
	}

	public void setNombreCurp(String nombreCurp) {
		this.nombreCurp = nombreCurp;
	}

	public String getApellidoPaternoCurp() {
		return apellidoPaternoCurp;
	}

	public void setApellidoPaternoCurp(String apellidoPaternoCurp) {
		this.apellidoPaternoCurp = apellidoPaternoCurp;
	}

	public String getApellidoMaternoCurp() {
		return apellidoMaternoCurp;
	}

	public void setApellidoMaternoCurp(String apellidoMaternoCurp) {
		this.apellidoMaternoCurp = apellidoMaternoCurp;
	}

	public String getSexoCurp() {
		return sexoCurp;
	}

	public void setSexoCurp(String sexoCurp) {
		this.sexoCurp = sexoCurp;
	}

	public String getClaveTipoPersonaCurp() {
		return claveTipoPersonaCurp;
	}

	public void setClaveTipoPersonaCurp(String claveTipoPersonaCurp) {
		this.claveTipoPersonaCurp = claveTipoPersonaCurp;
	}

	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacadeRemote(
			PersonaSeycosFacadeRemote personaSeycosFacadeRemote) {
		this.personaSeycosFacadeRemote = personaSeycosFacadeRemote;
	}

	@Autowired
	@Qualifier("sectorFacadeRemoteEJB")
	public void setSectorFacadeRemote(SectorFacadeRemote sectorFacadeRemote) {
		this.sectorFacadeRemote = sectorFacadeRemote;
	}

	@Autowired
	@Qualifier("estadoCivilFacadeRemoteEJB")
	public void setEstadoCivilFacade(EstadoCivilFacadeRemote estadoCivilFacade) {
		this.estadoCivilFacade = estadoCivilFacade;
	}

	@Override
	public String verDetalle() {
		return SUCCESS;
	}

	public String getCargaInicial() {
		return cargaInicial;
	}

	public void setCargaInicial(String cargaInicial) {
		this.cargaInicial = cargaInicial;
	}
	
	public Boolean getIsAdministradorAgente() {
		return isAdministradorAgente;
	}

	public void setIsAdministradorAgente(Boolean isAdministradorAgente) {
		this.isAdministradorAgente = isAdministradorAgente;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	public String getIdElemento() {
		return idElemento;
	}

	public void setIdElemento(String idElemento) {
		this.idElemento = idElemento;
	}
}
