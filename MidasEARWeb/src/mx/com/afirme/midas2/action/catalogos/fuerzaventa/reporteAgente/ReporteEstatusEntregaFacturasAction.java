package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteEstatusEntregaFacturas")
@Component
@Scope("prototype")
@InterceptorRefs({
    @InterceptorRef("timer"),
    @InterceptorRef("midas2Stack")
})
public class ReporteEstatusEntregaFacturasAction  extends ReporteAgenteBaseAction implements Preparable, ReportMethods{

	private static final long serialVersionUID = 2762886038572443657L;
	public static final Logger LOG = Logger.getLogger(ReporteEstatusEntregaFacturasAction.class);
	

	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reporteEstatusEntregaFacturas.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private DatosAgenteEstatusEntregaFactura datos = new DatosAgenteEstatusEntregaFactura();
	private InputStream reporteStream;
	private List<String> listStatus = new ArrayList<String>(1);
	private Date fechaMinFiltro;
	
	public void prepareMostrarFiltros(){	      
		    
		try {
			setCentroOperacionList( this.entidadService.findAll(CentroOperacion.class));
			setTipoPromotoria(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Tipos de Promotoria"));
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");	
			fechaMinFiltro = dateFormat.parse("01/01/2015");			
			listStatus.add(DatosAgenteEstatusEntregaFactura.estatusFactura.ENTREGADA.valor());
			listStatus.add(DatosAgenteEstatusEntregaFactura.estatusFactura.PENDIENTE.valor());
			listStatus.add(DatosAgenteEstatusEntregaFactura.estatusFactura.NO_TIENE_PAGOS.valor());
		} catch (Exception e) {
			LOG.error(e);
		}
	}
	
	@Action(value="mostrarFiltros",results={
			@Result(name=SUCCESS, location=FILTROS_REPORTE)})
	@Override
	public String mostrarFiltros() {
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		return null;
	}

	@Action(value="exportarToExcel",results={
			@Result(name=SUCCESS, type="stream",
					params={
					"contentType","${contentType}",
					"contentDisposition","attachment;filename=\"${fileName}\"",
					"inputName","reporteStream"
				}),
			@Result(name="EMPTY", location=ERROR),
			@Result(name= INPUT,  location = ERROR)
	})
	@Override
	public String exportarToExcel() {
		
		try{
			String dato = getEstatus();
			datos.setEstatus(dato);
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
			.imprimirReporteEstatusEntregaFacturaExcel(datos);
			
			if(transporte!=null){
				setReporteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			setFileName("reporte_Estatus_Entrega_Facturas.xlsx");
		} catch (RuntimeException error) {
			LOG.error(error);
			setMensaje(error.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		
	}

	public String getEstatus() {
		StringBuilder estatus = new StringBuilder("");
		Iterator<Entry<String, Boolean>> it = datos.getListEstatus().entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Boolean> pairs = (Entry<String, Boolean>)it.next();
			String key=SystemCommonUtils.getValidProperty(pairs.getKey());
			if(key.equals("0") && pairs.getValue()){
				if(estatus.toString().isEmpty()){
					estatus.append(DatosAgenteEstatusEntregaFactura.estatusFactura.ENTREGADA.valor());
				}
			}else if(key.equals("1") && pairs.getValue()){
				if(estatus.toString().isEmpty()){
					estatus.append(DatosAgenteEstatusEntregaFactura.estatusFactura.PENDIENTE.valor());
				}else{
					estatus.append("','").append(DatosAgenteEstatusEntregaFactura.estatusFactura.PENDIENTE.valor());
				}
			}if(key.equals("2") && pairs.getValue()){
				if(estatus.toString().isEmpty()){
					estatus.append(DatosAgenteEstatusEntregaFactura.estatusFactura.NO_TIENE_PAGOS.valor());
				}else{
					estatus.append("','").append(DatosAgenteEstatusEntregaFactura.estatusFactura.NO_TIENE_PAGOS.valor());
				}
			}
		}
		return estatus.toString();
	}

	public DatosAgenteEstatusEntregaFactura getDatos() {
		return datos;
	}

	public void setDatos(DatosAgenteEstatusEntregaFactura datos) {
		this.datos = datos;
	}

	public List<String> getListStatus() {
		return listStatus;
	}

	public void setListStatus(List<String> listStatus) {
		this.listStatus = listStatus;
	}

	public InputStream getReporteStream() {
		return reporteStream;
	}

	public void setReporteStream(InputStream reporteStream) {
		this.reporteStream = reporteStream;
	}	
	
	public Date getFechaMinFiltro() {
		return fechaMinFiltro;
	}

	public void setFechaMinFiltro(Date fechaMinFiltro) {
		this.fechaMinFiltro = fechaMinFiltro;
	}
	
}