package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteAgenteTopAgente")
@Component
@Scope("prototype")
public class ReporteAgenteTopAgentesAction extends ReporteAgenteBaseAction
		implements ReportMethods, Preparable {

	private static final long serialVersionUID = 1L;
	private Date fechaInicio;
	private Date fechaFin;
	private ConfigBonosService configBonosService;
	private ConfigBonos configuracionBono;
	private List<AgenteView> listaAgentes;
	private List<Agente> agenteList;
	private Integer topAgente;
	@Override
	public void prepare() throws Exception {
		if (listaAgentes != null) {
			agenteList = new ArrayList<Agente>();
			for (AgenteView agenteView : listaAgentes) {
				Agente agente = new Agente();
				agente.setId(agenteView.getId());
				agenteList.add(agente);
			}
		}
		
		if (getIdCentroOperacion() != null) {
			setCentroOperacionesSeleccionados(new LinkedList<CentroOperacion>());
			getCentroOperacionesSeleccionados().add(new CentroOperacion(getIdCentroOperacion().longValue()));
		}
		if (getIdEjecutivo() != null) {
			setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
			getEjecutivosSeleccionados().add(new Ejecutivo(getIdEjecutivo().longValue()));
		}
		
		if (getIdGerencia() != null) {
			setGerenciasSeleccionadas(new LinkedList<Gerencia>());
			getGerenciasSeleccionadas().add(new Gerencia(getIdGerencia().longValue()));
		}
		if (getIdPromotoria() != null) {
			setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
			getPromotoriasSeleccionadas().add(new Promotoria(getIdPromotoria().longValue()));
		}
	}

	@Override
	@Action(value = "mostrarFiltros", results = { @Result(name = SUCCESS, location = "/jsp/reportesAgentes/reporteAgenteTop.jsp") })
	public String mostrarFiltros() {
		final CentroOperacion filtro = new CentroOperacion();
//		setCentroOperacionList(centroOperacionService.findByFilters(filtro));
		setCentroOperacionList( this.entidadService.findAll(CentroOperacion.class));
		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		return SUCCESS;
	}

	@Override
	@Action(value = "exportarToPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToPDF() {
		try {
			List<AgenteView> listaAgentesView = configBonosService
			.obtenerAgentesPorConfiguracionCapturada(getCentroOperacionesSeleccionados(), getEjecutivosSeleccionados(),
					getGerenciasSeleccionadas(),
					getPromotoriasSeleccionadas(), null, null, null,
					agenteList, null);
			
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
			.imprimirReporteAgenteTopAgente(listaAgentesView,
					getAnio(), getMes(), getTipoReporte(),topAgente, getLocale());
			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje(EMPTY_RESULT);
				return INPUT;
			}
			setContentType("application/xls");
			if (getTipoReporte() == null || getTipoReporte() == 1) {
				setFileName("reporteAgenteTopGeneral.xls");
			} else {
				setFileName("reporteAgenteTopDetallado.xls");
			}
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(EMPTY_RESULT);
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareExportarToExcel() {
	
	}

	@Override
	@Action(value = "exportarToExcel", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToExcel() {
		try {
//			List<AgenteView> listaAgentesView = configBonosService
//			.obtenerAgentesPorConfiguracionCapturada(getCentroOperacionesSeleccionados(), getEjecutivosSeleccionados(),
//					getGerenciasSeleccionadas(),
//					getPromotoriasSeleccionadas(), null, null, null,
//					agenteList, null);
			
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
					.imprimirReporteAgenteTopAgenteToExcel(agenteList,
							getCentroOperacionesSeleccionados(), getEjecutivosSeleccionados(),
							getGerenciasSeleccionadas(),getPromotoriasSeleccionadas(),
							getAnio(), getMes(), getTipoReporte(),topAgente, getLocale(), ReporteAgenteBaseAction.TIPOSALIDAARCHIVO,fechaInicio,fechaFin);
			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje(EMPTY_RESULT);
				return INPUT;
			}
			if(TipoSalidaReportes.TO_EXCEL.getValue().equals(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO)){
				setContentType("application/xls");
			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octet-stream");
			}
				setFileName("reporteAgenteTopGeneral."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	
	
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	public ConfigBonos getConfiguracionBono() {
		return configuracionBono;
	}

	public void setConfiguracionBono(ConfigBonos configuracionBono) {
		this.configuracionBono = configuracionBono;
	}

	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public Integer getTopAgente() {
		return topAgente;
	}

	public void setTopAgente(Integer topAgente) {
		this.topAgente = topAgente;
	}

}
