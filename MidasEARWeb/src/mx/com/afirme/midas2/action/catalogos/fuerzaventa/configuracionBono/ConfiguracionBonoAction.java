package mx.com.afirme.midas2.action.catalogos.fuerzaventa.configuracionBono;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.bonos.BonoExcepcionPoliza;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionTipoCedula;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionesTipoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCentroOperacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCobertura;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoEjecutivo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepNegocio;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoGerencia;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoLineaVenta;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPrioridad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoProducto;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRamo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSeccion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSituacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSubramo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.bonos.ConfigBonosDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfigBonosNegView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.ExcepcionesPolizaView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bonos.BonoExcepcionPolizaService;
import mx.com.afirme.midas2.service.bonos.BonoExclusionTipoCedulaService;
import mx.com.afirme.midas2.service.bonos.BonoExclusionesTipoBonoService;
import mx.com.afirme.midas2.service.bonos.ConfigBonoExcepAgenteService;
import mx.com.afirme.midas2.service.bonos.ConfigBonoExcepNegocioService;
import mx.com.afirme.midas2.service.bonos.ConfigBonoRangoAplicaService;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/configuracionBono")
@InterceptorRefs({
    @InterceptorRef("timer"),
    @InterceptorRef("midas2Stack")
})
public class ConfiguracionBonoAction extends CatalogoHistoricoAction implements Preparable{

	private static final long serialVersionUID = 495457133833778588L;
	
	private ConfigBonosService configBonosService;  
	private ConfigBonos configuracionBono;
	private ConfigBonosDTO filtroConfigBono;
	private List<ConfigBonosDTO> listaConfiguracionesBonos = new ArrayList<ConfigBonosDTO>();
	private BonoExclusionesTipoBonoService tipoBonoService;
	private BonoExcepcionPolizaService excepcionPolizaService;
	private BonoExclusionTipoCedulaService exclusionTipoCedulaService;

	private Map<Long, String> gerenciaList = new LinkedHashMap<Long, String>();
	private Map<Long, String> ejecutivoList = new LinkedHashMap<Long, String>();
	private Map<Long, String> promotoriaList = new LinkedHashMap<Long, String>();
	
	
	
	private List<ValorCatalogoAgentes> listaLineaVenta = new ArrayList<ValorCatalogoAgentes>(); 
	private List<ValorCatalogoAgentes> listaLineaVentaSeleccionadas = new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigBonoLineaVenta> listLineaVentaSelec = new ArrayList<ConfigBonoLineaVenta>();

	private List<GenericaAgentesView> listaProductos = new ArrayList<GenericaAgentesView>();
	private List<ConfigBonoProducto> listaProductosSeleccionados = new ArrayList<ConfigBonoProducto>();
	
	private List<GenericaAgentesView> listaRamos = new ArrayList<GenericaAgentesView>();
	private List<ConfigBonoRamo> listaRamosSeleccionados = new ArrayList<ConfigBonoRamo>();
	
	private List<GenericaAgentesView> listaSubRamos = new ArrayList<GenericaAgentesView>();
	private List<ConfigBonoSubramo> listaSubRamosSeleccionados = new ArrayList<ConfigBonoSubramo>();
	
	private List<GenericaAgentesView> listaLineaNegocio = new ArrayList<GenericaAgentesView>();
	private List<ConfigBonoSeccion> listaLineaNegocioSeleccionados = new ArrayList<ConfigBonoSeccion>();
	
	private List<GenericaAgentesView> listaCoberturas = new ArrayList<GenericaAgentesView>();
	private List<ConfigBonoCobertura> listaCoberturasSeleccionadas = new ArrayList<ConfigBonoCobertura>();
	

	
	private static List<ValorCatalogoAgentes> listaTipoMoneda = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaPeriodosAjuste = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaBonos = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaProduccionSobre = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaSiniestralidadsobre = new  ArrayList<ValorCatalogoAgentes>();	
	private static List<ValorCatalogoAgentes> listaPeriodoComparacion = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaModosAplicacion = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaNivelSiniestralidad = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaSubtpoBono = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaNivelCrecimiento = new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> listaNivelProductividad = new ArrayList<ValorCatalogoAgentes>();	
	
	private List<CentroOperacionView> listaCentroOperacion = new ArrayList<CentroOperacionView>();
	private List<ConfigBonoCentroOperacion> centroOperacionesSeleccionados = new ArrayList<ConfigBonoCentroOperacion>();
	
	private List<GerenciaView> listaGerencias = new ArrayList<GerenciaView>();
	private List<ConfigBonoGerencia> gerenciasSeleccionadas = new ArrayList<ConfigBonoGerencia>();
	
	private List<EjecutivoView> listaEjecutivos = new ArrayList<EjecutivoView>();
	private List<ConfigBonoEjecutivo> ejecutivosSeleccionados = new ArrayList<ConfigBonoEjecutivo>();
	
	private List<PromotoriaView> listaPromotorias = new ArrayList<PromotoriaView>();
	private List<ConfigBonoPromotoria> promotoriasSeleccionadas = new ArrayList<ConfigBonoPromotoria>();
	private List<Long>promotoriasSelecionadasLong = new ArrayList<Long>();
	
	private List<ValorCatalogoAgentes> listaTiposPromotoria = new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigBonoTipoPromotoria> tipoPromotoriasSeleccionadas = new ArrayList<ConfigBonoTipoPromotoria>();
	
	private List<ValorCatalogoAgentes> listaTiposAgente = new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigBonoTipoAgente>  tipoAgentesSeleccionados = new ArrayList<ConfigBonoTipoAgente>();
	
	private List<ValorCatalogoAgentes> listaPrioridad = new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigBonoPrioridad> prioridadesSeleccionadas = new ArrayList<ConfigBonoPrioridad>();

	private List <ValorCatalogoAgentes>listaSituacion = new ArrayList<ValorCatalogoAgentes>();
	private List <ConfigBonoSituacion>situacionesSeleccionadas = new ArrayList<ConfigBonoSituacion>();
	
	private List<ValorCatalogoAgentes> catalogoTipocedula=new ArrayList<ValorCatalogoAgentes>();
	
	private List<ValorCatalogoAgentes> catalogoTipoBono = new ArrayList<ValorCatalogoAgentes>();
	
	private Agente agente = new Agente();
	private Promotoria promotoria = new Promotoria();
	private EntidadService entidadService;
	
	private ListadoService listadoService = null;
	
	private Map<Long, String> listaNegociosActivos = null;
	private ConfigBonosNegView filtroNegocio = new ConfigBonosNegView();
	private List<ConfigBonosNegView> listaNegDesasoc = new ArrayList<ConfigBonosNegView>();
	private List<ConfigBonosNegView> nuevaListaNeg = new ArrayList<ConfigBonosNegView>();
	private List<ConfigBonosNegView> obtenerNegocios = new ArrayList<ConfigBonosNegView>();
	private ExcepcionesPolizaView filtroPoliza= new ExcepcionesPolizaView();
	private List<ExcepcionesPolizaView> listaPolizas = new  ArrayList <ExcepcionesPolizaView>();
	private Long obtenerIdPoliza;
	private Long numeroPolizaSeycos;
	
	private List <ConfigBonos>obtenerListCatalogoTipoBono = new ArrayList<ConfigBonos>();
	private List<BonoExclusionesTipoBono> listaBusquedaBono = new ArrayList<BonoExclusionesTipoBono>();
	
	private List<ConfigBonoExcepAgente> listaExcepxionesAgentes = new ArrayList<ConfigBonoExcepAgente>();
	private ConfigBonoExcepAgenteService configBonoExcepAgenteService;
	
	private List<ConfigBonoExcepNegocio> listaExcepxionesNegocios = new ArrayList<ConfigBonoExcepNegocio>();
	private ConfigBonoExcepNegocioService configBonoExcepNegocioService;
	
	private List<ConfigBonoRangoAplica> listRangoFacotres;
	private ConfigBonoRangoAplicaService configBonoRangoAplicaService;
	
	private List<ValorCatalogoAgentes> listaPeriodoEjecucion = new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigBonosDTO> listaDescripcionBono = new ArrayList<ConfigBonosDTO>();
	
	private PolizaDTO polizaDto;
	
	private List <ValorCatalogoAgentes>obtenerListCatalogoTipoCedula = new ArrayList<ValorCatalogoAgentes>();
	private List<BonoExclusionTipoCedula> listaBusquedaCedula = new ArrayList<BonoExclusionTipoCedula>();
	
	private List<BonoExcepcionPoliza> listabonoExcepcionPoliza = new ArrayList<BonoExcepcionPoliza>();
	private String tabActiva;
	
	
	private List<ConfigBonoAgente> listaAgentes = new ArrayList<ConfigBonoAgente>();
	private List<ConfigBonoAplicaAgente> listaAplicaAgentes = new ArrayList<ConfigBonoAplicaAgente>();
	private List<ConfigBonoAplicaPromotoria> listaAplicaPromotorias = new ArrayList<ConfigBonoAplicaPromotoria>(); 
	private List<ConfigBonoPoliza> listaConfigPolizas = new ArrayList<ConfigBonoPoliza>();
	private ValorCatalogoAgentesService catalogoService;

	private List<Agente> lista_agentes = new ArrayList<Agente>();
	private List<Agente> lista_AplicaAgentes= new ArrayList<Agente>();
	private List<ExcepcionesPolizaView> lista_Polizas = new ArrayList<ExcepcionesPolizaView>();
	private List<Promotoria> lista_AplicaPromotorias = new ArrayList<Promotoria>();

	private final String CONFIGURACIONBONOSCATALOGO ="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosCatalogo.jsp";
	private final String CONFIGURACIONBONOS ="/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonos.jsp";
	private final String DETALLEBONOS="/jsp/catalogos/fuerzaventa/configuracionBonos/detalleBonos.jsp";
	private final String CONFIGURACIONBONOSGRID = "/jsp/catalogos/fuerzaventa/configuracionBonos/configuracionBonosGrid.jsp";
	private final String EXCEPCIONESBONOS ="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/ExcepcionesBonos.jsp";
	private final String AGREGARPOLIZA ="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/agregarPoliza.jsp";
	private final String AGREGARNEGOCIO ="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/agregarNegocio.jsp";
	private final String LISTARAGREGARPOLIZAGRID ="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/listarAgregarPolizaGrid.jsp";
	private final String AGENTEGRID = "/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/agenteGrid.jsp";
	private final String NEGOCIOGRID ="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/negocioGrid.jsp";
	private final String RANGOFACTORES ="/jsp/catalogos/fuerzaventa/configuracionBonos/rangoFactores.jsp";
	private final String RANGOFACTORESGRID ="/jsp/catalogos/fuerzaventa/configuracionBonos/rangoFactoresGrid.jsp";
	private final String POLIZAGRID ="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/polizaGrid.jsp";
	private final String MODALAGENTE="/jsp/catalogos/fuerzaventa/configuracionBonos/obtenerAgentesPorConfig.jsp";
	private final String GRIDAGENTE="/jsp/catalogos/fuerzaventa/configuracionBonos/agenteGrid.jsp";
	private final String EXCL_CVEAMIS_GRID="/jsp/catalogos/fuerzaventa/configuracionBonos/excepciones/claveAmisGrid.jsp";
	private Long clonar;
	
	private List<Long> gerenciaLong = new ArrayList<Long>();
	private List<Long> promotoriaLong = new ArrayList<Long>();
	private List<Long> ejecutivoLong = new ArrayList<Long>();
		
	
	private List<ConfigBonos> listaBonoEclusion = new ArrayList<ConfigBonos>();
	private List<AgenteView> listaAgente = new ArrayList<AgenteView>();
	private String tipoBeneficiarioString;
	private boolean onlyHeaders;
	private Long isCheckedImporte;
	private AgenteMidasService agenteMidasService;
	
	private List<ConfigBonoRangoClaveAmis> listaRangosClaveAmis;
	/************gets y sets************/
	
	public List<Long> getEjecutivoLong() {
		return ejecutivoLong;
	}
	public void setEjecutivoLong(List<Long> ejecutivoLong) {
		this.ejecutivoLong = ejecutivoLong;
	}
	
	public List<Long> getPromotoriaLong() {
		return promotoriaLong;
	}
	public void setPromotoriaLong(List<Long> promotoriaLong) {
		this.promotoriaLong = promotoriaLong;
	}
	
	
	public List<Long> getGerenciaLong() {
		return gerenciaLong;
	}
	public void setGerenciaLong(List<Long> gerenciaLong) {
		this.gerenciaLong = gerenciaLong;
	}
	 
	public List<ValorCatalogoAgentes> getListaLineaVenta() {
		return listaLineaVenta;
	}
	public ExcepcionesPolizaView getFiltroPoliza() {
		return filtroPoliza;
	}
	public void setFiltroPoliza(ExcepcionesPolizaView filtroPoliza) {
		this.filtroPoliza = filtroPoliza;
	}
	public ConfigBonos getConfiguracionBono() {
		return configuracionBono;
	}
	public void setConfiguracionBono(ConfigBonos configuracionBono) {
		this.configuracionBono = configuracionBono;
	}
	public void setListaLineaVenta(List<ValorCatalogoAgentes> listaLineaVenta) {
		this.listaLineaVenta = listaLineaVenta;
	}	
	public List<GenericaAgentesView> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<GenericaAgentesView> listaProductos) {
		this.listaProductos = listaProductos;
	}	
	public List<GenericaAgentesView> getListaRamos() {
		return listaRamos;
	}
	public void setListaRamos(List<GenericaAgentesView> listaRamos) {
		this.listaRamos = listaRamos;
	}	
	public List<ConfigBonoProducto> getListaProductosSeleccionados() {
		return listaProductosSeleccionados;
	}
	public void setListaProductosSeleccionados(List<ConfigBonoProducto> listaProductosSeleccionados) {
		this.listaProductosSeleccionados = listaProductosSeleccionados;
	}	
	public List<ValorCatalogoAgentes> getListaLineaVentaSeleccionadas() {
		return listaLineaVentaSeleccionadas;
	}
	public void setListaLineaVentaSeleccionadas(List<ValorCatalogoAgentes> listaLineaVentaSeleccionadas) {
		this.listaLineaVentaSeleccionadas = listaLineaVentaSeleccionadas;
	}	
	public List<ConfigBonoRamo> getListaRamosSeleccionados() {
		return listaRamosSeleccionados;
	}
	public void setListaRamosSeleccionados(List<ConfigBonoRamo> listaRamosSeleccionados) {
		this.listaRamosSeleccionados = listaRamosSeleccionados;
	}	
	public List<GenericaAgentesView> getListaSubRamos() {
		return listaSubRamos;
	}
	public void setListaSubRamos(List<GenericaAgentesView> listaSubRamos) {
		this.listaSubRamos = listaSubRamos;
	}
	public List<ConfigBonoSubramo> getListaSubRamosSeleccionados() {
		return listaSubRamosSeleccionados;
	}
	public void setListaSubRamosSeleccionados(List<ConfigBonoSubramo> listaSubRamosSeleccionados) {
		this.listaSubRamosSeleccionados = listaSubRamosSeleccionados;
	}
	public List<GenericaAgentesView> getListaLineaNegocio() {
		return listaLineaNegocio;
	}
	public void setListaLineaNegocio(List<GenericaAgentesView> listaLineaNegocio) {
		this.listaLineaNegocio = listaLineaNegocio;
	}
	public List<ConfigBonoSeccion> getListaLineaNegocioSeleccionados() {
		return listaLineaNegocioSeleccionados;
	}
	public void setListaLineaNegocioSeleccionados(List<ConfigBonoSeccion> listaLineaNegocioSeleccionados) {
		this.listaLineaNegocioSeleccionados = listaLineaNegocioSeleccionados;
	}
	public List<GenericaAgentesView> getListaCoberturas() {
		return listaCoberturas;
	}
	public void setListaCoberturas(List<GenericaAgentesView> listaCoberturas) {
		this.listaCoberturas = listaCoberturas;
	}
	public List<ConfigBonoCobertura> getListaCoberturasSeleccionadas() {
		return listaCoberturasSeleccionadas;
	}
	public void setListaCoberturasSeleccionadas(List<ConfigBonoCobertura> listaCoberturasSeleccionadas) {
		this.listaCoberturasSeleccionadas = listaCoberturasSeleccionadas;
	}	
	public List<ValorCatalogoAgentes> getListaPeriodosAjuste() {
		return listaPeriodosAjuste;
	}
	public void setListaPeriodosAjuste(List<ValorCatalogoAgentes> listaPeriodosAjuste) {
		this.listaPeriodosAjuste = listaPeriodosAjuste;
	}	
	public List<ValorCatalogoAgentes> getListaBonos() {
		return listaBonos;
	}
	public void setListaBonos(List<ValorCatalogoAgentes> listaBonos) {
		this.listaBonos = listaBonos;
	}
	public List<ValorCatalogoAgentes> getListaProduccionSobre() {
		return listaProduccionSobre;
	}
	public void setListaProduccionSobre(List<ValorCatalogoAgentes> listaProduccionSobre) {
		this.listaProduccionSobre = listaProduccionSobre;
	}
	public List<ValorCatalogoAgentes> getListaSiniestralidadsobre() {
		return listaSiniestralidadsobre;
	}
	public void setListaSiniestralidadsobre(List<ValorCatalogoAgentes> listaSiniestralidadsobre) {
		this.listaSiniestralidadsobre = listaSiniestralidadsobre;
	}	
	public List<ValorCatalogoAgentes> getListaTipoMoneda() {
		return listaTipoMoneda;
	}
	public void setListaTipoMoneda(List<ValorCatalogoAgentes> listaTipoMoneda) {
		this.listaTipoMoneda = listaTipoMoneda;
	}	
	public List<ValorCatalogoAgentes> getListaPeriodoComparacion() {
		return listaPeriodoComparacion;
	}
	public void setListaPeriodoComparacion(List<ValorCatalogoAgentes> listaPeriodoComparacion) {
		this.listaPeriodoComparacion = listaPeriodoComparacion;
	}
	public List<CentroOperacionView> getListaCentroOperacion() {
		return listaCentroOperacion;
	}
	public void setListaCentroOperacion(List<CentroOperacionView> listaCentroOperacion) {
		this.listaCentroOperacion = listaCentroOperacion;
	}
	public List<GerenciaView> getListaGerencias() {
		return listaGerencias;
	}
	public void setListaGerencias(List<GerenciaView> listaGerencias) {
		this.listaGerencias = listaGerencias;
	}
	public List<EjecutivoView> getListaEjecutivos() {
		return listaEjecutivos;
	}
	public void setListaEjecutivos(List<EjecutivoView> listaEjecutivos) {
		this.listaEjecutivos = listaEjecutivos;
	}
	public List<PromotoriaView> getListaPromotorias() {
		return listaPromotorias;
	}
	public void setListaPromotorias(List<PromotoriaView> listaPromotorias) {
		this.listaPromotorias = listaPromotorias;
	}
	public List<ValorCatalogoAgentes> getListaTiposPromotoria() {
		return listaTiposPromotoria;
	}
	public void setListaTiposPromotoria(List<ValorCatalogoAgentes> listaTiposPromotoria) {
		this.listaTiposPromotoria = listaTiposPromotoria;
	}
	public List<ValorCatalogoAgentes> getListaTiposAgente() {
		return listaTiposAgente;
	}
	public void setListaTiposAgente(List<ValorCatalogoAgentes> listaTiposAgente) {
		this.listaTiposAgente = listaTiposAgente;
	}
	public List<ValorCatalogoAgentes> getListaPrioridad() {
		return listaPrioridad;
	}
	public void setListaPrioridad(List<ValorCatalogoAgentes> listaPrioridad) {
		this.listaPrioridad = listaPrioridad;
	}	
	public List<ConfigBonoCentroOperacion> getCentroOperacionesSeleccionados() {
		return centroOperacionesSeleccionados;
	}
	public void setCentroOperacionesSeleccionados(List<ConfigBonoCentroOperacion> centroOperacionesSeleccionados) {
		this.centroOperacionesSeleccionados = centroOperacionesSeleccionados;
	}	
	public List<ConfigBonoGerencia> getGerenciasSeleccionadas() {
		return gerenciasSeleccionadas;
	}
	public void setGerenciasSeleccionadas(List<ConfigBonoGerencia> gerenciasSeleccionadas) {
		this.gerenciasSeleccionadas = gerenciasSeleccionadas;
	}
	public List<ConfigBonoEjecutivo> getEjecutivosSeleccionados() {
		return ejecutivosSeleccionados;
	}
	public void setEjecutivosSeleccionados(List<ConfigBonoEjecutivo> ejecutivosSeleccionados) {
		this.ejecutivosSeleccionados = ejecutivosSeleccionados;
	}
	public List<ConfigBonoPromotoria> getPromotoriasSeleccionadas() {
		return promotoriasSeleccionadas;
	}
	public void setPromotoriasSeleccionadas(List<ConfigBonoPromotoria> promotoriasSeleccionadas) {
		this.promotoriasSeleccionadas = promotoriasSeleccionadas;
	}
	public List<ConfigBonoTipoPromotoria> getTipoPromotoriasSeleccionadas() {
		return tipoPromotoriasSeleccionadas;
	}
	public void setTipoPromotoriasSeleccionadas(List<ConfigBonoTipoPromotoria> tipoPromotoriasSeleccionadas) {
		this.tipoPromotoriasSeleccionadas = tipoPromotoriasSeleccionadas;
	}
	public List<ConfigBonoTipoAgente> getTipoAgentesSeleccionados() {
		return tipoAgentesSeleccionados;
	}
	public void setTipoAgentesSeleccionados(List<ConfigBonoTipoAgente> tipoAgentesSeleccionados) {
		this.tipoAgentesSeleccionados = tipoAgentesSeleccionados;
	}
	public List<ConfigBonoPrioridad> getPrioridadesSeleccionadas() {
		return prioridadesSeleccionadas;
	}
	public void setPrioridadesSeleccionadas(List<ConfigBonoPrioridad> prioridadesSeleccionadas) {
		this.prioridadesSeleccionadas = prioridadesSeleccionadas;
	}
	

	public ConfigBonosDTO getFiltroConfigBono() {
		return filtroConfigBono;
	}
	public void setFiltroConfigBono(ConfigBonosDTO filtroConfigBono) {
		this.filtroConfigBono = filtroConfigBono;
	}
	
	public List<ConfigBonosDTO> getListaConfiguracionesBonos() {
		return listaConfiguracionesBonos;
	}
	public void setListaConfiguracionesBonos(
			List<ConfigBonosDTO> listaConfiguracionesBonos) {
		this.listaConfiguracionesBonos = listaConfiguracionesBonos;
	}
	public static List<ValorCatalogoAgentes> getListaModosAplicacion() {
		return listaModosAplicacion;
	}
	public static void setListaModosAplicacion(
			List<ValorCatalogoAgentes> listaModosAplicacion) {
		ConfiguracionBonoAction.listaModosAplicacion = listaModosAplicacion;
	}	
	
	public List<ValorCatalogoAgentes> getListaSituacion() {
		return listaSituacion;
	}
	public void setListaSituacion(List<ValorCatalogoAgentes> listaSituacion) {
		this.listaSituacion = listaSituacion;
	}
	public List<ConfigBonoSituacion> getSituacionesSeleccionadas() {
		return situacionesSeleccionadas;
	}
	public void setSituacionesSeleccionadas(
			List<ConfigBonoSituacion> situacionesSeleccionadas) {
		this.situacionesSeleccionadas = situacionesSeleccionadas;
	}
	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}
	
	
	@Autowired
	@Qualifier("configBonoExcepAgenteServiceEJB")
	public void setConfigBonoExcepAgenteService(
			ConfigBonoExcepAgenteService configBonoExcepAgenteService) {
		this.configBonoExcepAgenteService = configBonoExcepAgenteService;
	}
	
	public List<ConfigBonoExcepNegocio> getListaExcepxionesNegocios() {
		return listaExcepxionesNegocios;
	}
	public void setListaExcepxionesNegocios(
			List<ConfigBonoExcepNegocio> listaExcepxionesNegocios) {
		this.listaExcepxionesNegocios = listaExcepxionesNegocios;
	}
	
	@Autowired
	@Qualifier("configBonoExcepNegocioServiceEJB")
	public void setConfigBonoExcepNegocioService(
			ConfigBonoExcepNegocioService configBonoExcepNegocioService) {
		this.configBonoExcepNegocioService = configBonoExcepNegocioService;
	}
	
	
	@Autowired
	@Qualifier("configBonoRangoAplicaServiceEJB")
	public void setConfigBonoRangoAplicaService(
			ConfigBonoRangoAplicaService configBonoRangoAplicaService) {
		this.configBonoRangoAplicaService = configBonoRangoAplicaService;
	}
	public List<ValorCatalogoAgentes> getCatalogoTipocedula() {
		return catalogoTipocedula;
	}
	public void setCatalogoTipocedula(List<ValorCatalogoAgentes> catalogoTipocedula) {
		this.catalogoTipocedula = catalogoTipocedula;
	}
	
	public List<ValorCatalogoAgentes> getCatalogoTipoBono() {
		return catalogoTipoBono;
	}
	public void setCatalogoTipoBono(List<ValorCatalogoAgentes> catalogoTipoBono) {
		this.catalogoTipoBono = catalogoTipoBono;
	}
	public Agente getAgente() {
		return agente;
	}
	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	public Map<Long, String> getListaNegociosActivos() {
		return listaNegociosActivos;
	}
	public void setListaNegociosActivos(Map<Long, String> listaNegociosActivos) {
		this.listaNegociosActivos = listaNegociosActivos;
	}
	public ConfigBonosNegView getFiltroNegocio() {
		return filtroNegocio;
	}
	public void setFiltroNegocio(ConfigBonosNegView filtroNegocio) {
		this.filtroNegocio = filtroNegocio;
	}
		
	public List<ConfigBonosNegView> getListaNegDesasoc() {
		return listaNegDesasoc;
	}
	public void setListaNegDesasoc(List<ConfigBonosNegView> listaNegDesasoc) {
		this.listaNegDesasoc = listaNegDesasoc;
	}
	
	public List<ConfigBonosNegView> getNuevaListaNeg() {
		return nuevaListaNeg;
	}
	public void setNuevaListaNeg(List<ConfigBonosNegView> nuevaListaNeg) {
		this.nuevaListaNeg = nuevaListaNeg;
	}
	public List<ConfigBonosNegView> getObtenerNegocios() {
		return obtenerNegocios;
	}
	public void setObtenerNegocios(List<ConfigBonosNegView> obtenerNegocios) {
		this.obtenerNegocios = obtenerNegocios;
	}
	public List<ExcepcionesPolizaView> getListaPolizas() {
		return listaPolizas;
	}
	public void setListaPolizas(List<ExcepcionesPolizaView> listaPolizas) {
		listaPolizas = listaPolizas;
	}
	public Long getObtenerIdPoliza() {
		return obtenerIdPoliza;
	}
	public void setObtenerIdPoliza(Long obtenerIdPoliza) {
		this.obtenerIdPoliza = obtenerIdPoliza;
	}
	
	public List<ConfigBonoExcepAgente> getListaExcepxionesAgentes() {
		return listaExcepxionesAgentes;
	}
	public void setListaExcepxionesAgentes(
			List<ConfigBonoExcepAgente> listaExcepxionesAgentes) {
		this.listaExcepxionesAgentes = listaExcepxionesAgentes;
	}
	
	public List<ConfigBonoRangoAplica> getListRangoFacotres() {
		return listRangoFacotres;
	}
	public void setListRangoFacotres(List<ConfigBonoRangoAplica> listRangoFacotres) {
		this.listRangoFacotres = listRangoFacotres;
	}	
	
	public List<ConfigBonoRangoClaveAmis> getListaRangosClaveAmis() {
		return listaRangosClaveAmis;
	}
	public void setListaRangosClaveAmis(
			List<ConfigBonoRangoClaveAmis> listaRangosClaveAmis) {
		this.listaRangosClaveAmis = listaRangosClaveAmis;
	}
	
	@Autowired
	@Qualifier("BonoExclusionesTipoBonoServiceEJB")
	public void setTipoBonoService(BonoExclusionesTipoBonoService tipoBonoService) {
		this.tipoBonoService = tipoBonoService;
	}
	@Autowired
	@Qualifier("BonoExcepcionPolizaServiceEJB")
	public void setExcepcionPolizaService(BonoExcepcionPolizaService excepcionPolizaService) {
		this.excepcionPolizaService = excepcionPolizaService;
	}
	
	@Autowired
	@Qualifier("BonoExclusionTipoCedulaServiceEJB")
	public void setExclusionTipoCedulaService(BonoExclusionTipoCedulaService exclusionTipoCedulaService) {
		this.exclusionTipoCedulaService = exclusionTipoCedulaService;
	}
	
	public List<ConfigBonos> getObtenerListCatalogoTipoBono() {
		return obtenerListCatalogoTipoBono;
	}
	public void setObtenerListCatalogoTipoBono(
			List<ConfigBonos> obtenerListCatalogoTipoBono) {
		this.obtenerListCatalogoTipoBono = obtenerListCatalogoTipoBono;
	}
	
	
	public Promotoria getPromotoria() {
		return promotoria;
	}
	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}	

	public Map<Long, String> getGerenciaList() {
		return gerenciaList;
	}
	public void setGerenciaList(Map<Long, String> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}
	public Map<Long, String> getEjecutivoList() {
		return ejecutivoList;
	}
	public void setEjecutivoList(Map<Long, String> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}
	public Map<Long, String> getPromotoriaList() {
		return promotoriaList;
	}
	public void setPromotoriaList(Map<Long, String> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}	
	public List<ValorCatalogoAgentes> getListaPeriodoEjecucion() {
		return listaPeriodoEjecucion;
	}
	public void setListaPeriodoEjecucion(
			List<ValorCatalogoAgentes> listaPeriodoEjecucion) {
		this.listaPeriodoEjecucion = listaPeriodoEjecucion;
	}
	public List<ConfigBonosDTO> getListaDescripcionBono() {
		return listaDescripcionBono;
	}
	public void setListaDescripcionBono(List<ConfigBonosDTO> listaDescripcionBono) {
		this.listaDescripcionBono = listaDescripcionBono;
	}
	public PolizaDTO getPolizaDto() {
		return polizaDto;
	}
	public void setPolizaDto(PolizaDTO polizaDto) {
		this.polizaDto = polizaDto;
	}
	public List<ValorCatalogoAgentes> getObtenerListCatalogoTipoCedula() {
		return obtenerListCatalogoTipoCedula;
	}
	public void setObtenerListCatalogoTipoCedula(
			List<ValorCatalogoAgentes> obtenerListCatalogoTipoCedula) {
		this.obtenerListCatalogoTipoCedula = obtenerListCatalogoTipoCedula;
	}
	
	public List<BonoExclusionesTipoBono> getListaBusquedaBono() {
		return listaBusquedaBono;
	}
	public void setListaBusquedaBono(List<BonoExclusionesTipoBono> listaBusquedaBono) {
		this.listaBusquedaBono = listaBusquedaBono;
	}
	public List<BonoExclusionTipoCedula> getListaBusquedaCedula() {
		return listaBusquedaCedula;
	}
	public void setListaBusquedaCedula(
			List<BonoExclusionTipoCedula> listaBusquedaCedula) {
		this.listaBusquedaCedula = listaBusquedaCedula;
	}
	
	public List<BonoExcepcionPoliza> getListabonoExcepcionPoliza() {
		return listabonoExcepcionPoliza;
	}
	public void setBonoExcepcionPoliza(List<BonoExcepcionPoliza> listabonoExcepcionPoliza) {
		this.listabonoExcepcionPoliza = listabonoExcepcionPoliza;
	}
	
	public String getTabActiva() {
		return tabActiva;
	}
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	
	
	
	public List<ConfigBonoAgente> getListaAgentes() {
		return listaAgentes;
	}
	public void setListaAgentes(List<ConfigBonoAgente> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}
	public List<ConfigBonoAplicaAgente> getListaAplicaAgentes() {
		return listaAplicaAgentes;
	}
	public void setListaAplicaAgentes(
			List<ConfigBonoAplicaAgente> listaAplicaAgentes) {
		this.listaAplicaAgentes = listaAplicaAgentes;
	}
	public List<ConfigBonoAplicaPromotoria> getListaAplicaPromotorias() {
		return listaAplicaPromotorias;
	}
	public void setListaAplicaPromotorias(
			List<ConfigBonoAplicaPromotoria> listaAplicaPromotorias) {
		this.listaAplicaPromotorias = listaAplicaPromotorias;
	}
	
	
	public List<ConfigBonoPoliza> getListaConfigPolizas() {
		return listaConfigPolizas;
	}
	public void setListaConfigPolizas(List<ConfigBonoPoliza> listaConfigPolizas) {
		this.listaConfigPolizas = listaConfigPolizas;
	}
	
	public List<ConfigBonoLineaVenta> getListLineaVentaSelec() {
		return listLineaVentaSelec;
	}
	public void setListLineaVentaSelec(
			List<ConfigBonoLineaVenta> listLineaVentaSelec) {
		this.listLineaVentaSelec = listLineaVentaSelec;
	}
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	public List<Agente> getLista_agentes() {
		return lista_agentes;
	}
	public void setLista_agentes(List<Agente> lista_agentes) {
		this.lista_agentes = lista_agentes;
	}
	public List<Agente> getLista_AplicaAgentes() {
		return lista_AplicaAgentes;
	}
	public void setLista_AplicaAgentes(List<Agente> lista_AplicaAgentes) {
		this.lista_AplicaAgentes = lista_AplicaAgentes;
	}
	public List<ExcepcionesPolizaView> getLista_Polizas() {
		return lista_Polizas;
	}
	public void setLista_Polizas(List<ExcepcionesPolizaView> lista_Polizas) {
		this.lista_Polizas = lista_Polizas;
	}
	
	public List<Promotoria> getLista_AplicaPromotorias() {
		return lista_AplicaPromotorias;
	}
	public void setLista_AplicaPromotorias(List<Promotoria> lista_AplicaPromotorias) {
		this.lista_AplicaPromotorias = lista_AplicaPromotorias;
	}
	
	public Long getClonar() {
		return clonar;
	}
	public void setClonar(Long clonar) {
		this.clonar = clonar;
	}
	
	public List<ConfigBonos> getListaBonoEclusion() {
		return listaBonoEclusion;
	}
	public void setListaBonoEclusion(List<ConfigBonos> listaBonoEclusion) {
		this.listaBonoEclusion = listaBonoEclusion;
	}
	
	public List<AgenteView> getListaAgente() {
		return listaAgente;
	}
	public void setListaAgente(List<AgenteView> listaAgente) {
		this.listaAgente = listaAgente;
	}
	
	public String getTipoBeneficiarioString() {
		return tipoBeneficiarioString;
	}
	public void setTipoBeneficiarioString(String tipoBeneficiarioString) {
		this.tipoBeneficiarioString = tipoBeneficiarioString;
	}
	public void setOnlyHeaders(boolean onlyHeaders) {
		this.onlyHeaders = onlyHeaders;
	}
	public Long getNumeroPolizaSeycos() {
		return numeroPolizaSeycos;
	}
	public void setNumeroPolizaSeycos(Long numeroPolizaSeycos) {
		this.numeroPolizaSeycos = numeroPolizaSeycos;
	}
	public Long getIsCheckedImporte() {
		return isCheckedImporte;
	}
	public void setIsCheckedImporte(Long isCheckedImporte) {
		this.isCheckedImporte = isCheckedImporte;
	}
	
	public List<Long> getPromotoriasSelecionadasLong() {
		return promotoriasSelecionadasLong;
	}
	public void setPromotoriasSelecionadasLong(List<Long> promotoriasSelecionadasLong) {
		this.promotoriasSelecionadasLong = promotoriasSelecionadasLong;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public static List<ValorCatalogoAgentes> getListaNivelSiniestralidad() {
		return listaNivelSiniestralidad;
	}
	public static void setListaNivelSiniestralidad(
			List<ValorCatalogoAgentes> listaNivelSiniestralidad) {
		ConfiguracionBonoAction.listaNivelSiniestralidad = listaNivelSiniestralidad;
	}
	
	public static List<ValorCatalogoAgentes> getListaSubtpoBono() {
		return listaSubtpoBono;
	}
	public static void setListaSubtpoBono(List<ValorCatalogoAgentes> listaSubtpoBono) {
		ConfiguracionBonoAction.listaSubtpoBono = listaSubtpoBono;
	}
	
	public static List<ValorCatalogoAgentes> getListaNivelCrecimiento() {
		return listaNivelCrecimiento;
	}
	public static void setListaNivelCrecimiento(
			List<ValorCatalogoAgentes> listaNivelCrecimiento) {
		ConfiguracionBonoAction.listaNivelCrecimiento= listaNivelCrecimiento;
	}
	public static List<ValorCatalogoAgentes> getListaNivelProductividad() {
		return listaNivelProductividad;
	}
	public static void setListaNivelProductividad(
			List<ValorCatalogoAgentes> listaNivelProductividad) {
		ConfiguracionBonoAction.listaNivelProductividad = listaNivelProductividad;
	}
	
	/*************************actions*****************************/

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		prepareVerDetalle();
	}	
	public void validateGuardar(){
		addErrors(configuracionBono,this, "configuracionBono");
	}
	
	public void prepareMostrarContenedor(){
		inicializarListas();
	}
	private void cargarCombosChecados(){
		try {
			listLineaVentaSelec = configBonosService.getLineaVentaPorConfiguracion(configuracionBono);
			listaProductos = configBonosService.getProductoRelacionados(configuracionBono);
			listaRamos = configBonosService.getRamoRelacionados(configuracionBono);
			listaSubRamos = configBonosService.getSubRamoRelacionados(configuracionBono);
			listaLineaNegocio = configBonosService.getLineaNegociosRelacionados(configuracionBono);
			listaCoberturasSeleccionadas = configBonosService.getCoberturasPorConfiguracion(configuracionBono);
			listaCoberturas = configBonosService.getCoberturasRelacionados(configuracionBono);
			
			//listas
			
			lista_agentes = configBonosService.getAgentesByIdConfigBono(configuracionBono.getId());
			lista_Polizas = configBonosService.getPolizaByIDConfigBono(configuracionBono.getId());
			listaAplicaPromotorias = configBonosService.getAplicaPromotoriasByIdConfigBono(configuracionBono.getId());
			lista_AplicaAgentes = configBonosService.getAplicaAgentesByIdConfigBono(configuracionBono.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void inicializarListas(){
		try {
			if(configuracionBono!=null && configuracionBono.getId()!=null){
				cargarCombosChecados();
			}
			gerenciaList=listadoService.getMapGerencias();
			ejecutivoList=listadoService.getMapEjecutivos();
	//		promotoriaList = listadoService.getMapPromotorias();
			listaCentroOperacion = configBonosService.getCentroOperacionList();
			listaGerencias = configBonosService.getGerenciaList();
			listaEjecutivos = configBonosService.getEjecutivoList();
			listaPromotorias = configBonosService.getPromotoriaList();
			listaTiposPromotoria = configBonosService.getTipoPromotoriaList();
			listaTiposAgente = configBonosService.getTipoDeAgenteList();
			listaPrioridad = configBonosService.getPrioridadList();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		promotoriaList = configBonosService.getPromotoriaList();
		try{
			if(isEmptyList(listaLineaVenta)){
//				listaLineaVenta=configBonosService.getLineasVenta();
				listaLineaVenta = configBonosService.getLineasVentasRelacionadas(configuracionBono);
			}
			if(isEmptyList(listaTipoMoneda)){
				listaTipoMoneda = catalogoService.obtenerElementosPorCatalogo("Tipo Moneda");
//				listaTipoMoneda=cargarCatalogoLightWeight("Tipo Moneda");
			}
			if(isEmptyList(listaPeriodosAjuste)){
//				listaPeriodosAjuste=cargarCatalogoLightWeight("Periodos de Ajuste de Bonos");
				listaPeriodosAjuste = catalogoService.obtenerElementosPorCatalogo("Periodos de Ajuste de Bonos");
			}
			if(isEmptyList(listaBonos)){
//				listaBonos = cargarCatalogoLightWeight("Tipos de Bono");
				listaBonos = catalogoService.obtenerElementosPorCatalogo("Tipos de Bono");
			}
			if(isEmptyList(listaProduccionSobre)){
//				listaProduccionSobre = cargarCatalogoLightWeight("Produccion Sobre ...");
				listaProduccionSobre = catalogoService.obtenerElementosPorCatalogo("Produccion Sobre ...");
			}
			if(isEmptyList(listaSiniestralidadsobre)){
				listaSiniestralidadsobre = cargarCatalogo("Siniestralidad Sobre ...");
			}
			if(isEmptyList(listaPeriodoComparacion)){
				listaPeriodoComparacion = cargarCatalogo("Periodo de Comparacion");
			}
			if(isEmptyList(listaModosAplicacion)){
				listaModosAplicacion = cargarCatalogo("Modos de Aplicacion Bonos");
			}
			if(isEmptyList(listaSituacion)){
				listaSituacion=cargarCatalogo("Estatus de Agente (Situacion)");
			}
			if(isEmptyList(listaNivelSiniestralidad)){
				listaNivelSiniestralidad = catalogoService.obtenerElementosPorCatalogo("Nivel de Siniestralidad de Bonos");
			}
			if(isEmptyList(listaSubtpoBono)){
				listaSubtpoBono = catalogoService.obtenerElementosPorCatalogo("Subtipo de Bonos");
			}
			if(isEmptyList(listaNivelCrecimiento)){
				listaNivelCrecimiento = catalogoService.obtenerElementosPorCatalogo("Nivel de Crecimiento de Bonos");
			}
			if(isEmptyList(listaNivelProductividad)){
				listaNivelProductividad = catalogoService.obtenerElementosPorCatalogo("Nivel de Productividad de Bonos");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=CONFIGURACIONBONOSCATALOGO)
		})
	public String mostrarContenedor(){	
		try {
			listaTiposPromotoria = configBonosService.getTipoPromotoriaList();
			listaPrioridad = configBonosService.getPrioridadList();	
			listaCentroOperacion = configBonosService.getCentroOperacionList();
			listaTiposAgente = configBonosService.getTipoDeAgenteList();
			listaPeriodoEjecucion =cargarCatalogo("Periodos de Ejecucion de Comisiones");
			listaProductos = configBonosService.getProductos();
			listaRamos = configBonosService.getRamos();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location=CONFIGURACIONBONOS)
		})
	@Override
	public String verDetalle() {
		try {
				inicializarListas();
				ConfigBonosDTO filtroConfigBono=new ConfigBonosDTO();
				filtroConfigBono.setActivo(1);
				listaDescripcionBono = configBonosService.findByFiltersView(filtroConfigBono);
//				if(configuracionBono.getId()!=null && configuracionBono!= null){
//					listLineaVentaSelec = configBonosService.getLineaVentaPorConfiguracion(configuracionBono);
//				}
//				listaCentroOperacion = configBonosService.getCentroOperacionList();
//				listaGerencias = configBonosService.getGerenciaList();
//				listaEjecutivos = configBonosService.getEjecutivoList();
//				listaPromotorias = configBonosService.getPromotoriaList();
//				listaTiposPromotoria = configBonosService.getTipoPromotoriaList();
//				listaTiposAgente = configBonosService.getTipoDeAgenteList();
//				listaPrioridad = configBonosService.getPrioridadList();	
				if(configuracionBono.getId()!=null){
				configuracionBono = configBonosService.loadById(configuracionBono);
				if(configuracionBono.getPagoSinFactura()==1){
					configuracionBono.setPagoSinFacturaBoolean(true);
				}else{
					configuracionBono.setPagoSinFacturaBoolean(false);
				}
				if(configuracionBono.getActivo()==1){
					configuracionBono.setActivoBoolean(true);
				}else{
					configuracionBono.setActivoBoolean(false);
				}
				if(configuracionBono.getAjusteDeOficina()==1){
					configuracionBono.setAjusteDeOficinaBoolean(true);
				}else{
					configuracionBono.setAjusteDeOficinaBoolean(false);
				}
				if(configuracionBono.getGlobal()==1){
					configuracionBono.setGlobaBoolean(true);
				}else{
					configuracionBono.setGlobaBoolean(false);
				}
				if(configuracionBono.getTodosAgentes()==1){
					configuracionBono.setTodosAgentesBoolean(true);
				}else{
					configuracionBono.setTodosAgentesBoolean(false);
				}
				if(configuracionBono.getTodasPromotorias()==1){
					configuracionBono.setTodasPromotoriasBoolean(true);
				}else{
					configuracionBono.setTodasPromotoriasBoolean(false);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="listarProductosPorLineaDeVenta",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaProductos.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaProductos.*"})
		})
	public String listarProductosPorLineaDeVenta(){
		try {
//			listaProductos = configBonosService.getProductosPorLineaVenta(listaLineaVentaSeleccionadas);
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="listarRamosPorProducto",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaRamos.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaRamos.*"})
		})
	public String listarRamosPorProducto(){
		try {
			if(listaProductosSeleccionados != null){
				
				listaRamos = configBonosService.getRamosPorProductos(listaProductosSeleccionados);			
			}
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="listarSubramosPorRamoYLineaNegocioPorRamo",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaSubRamos.*,^listaLineaNegocio.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaSubRamos.*,^listaLineaNegocio.*"})
		})
	public String listarSubramosPorRamo(){
		try {
			if(listaRamosSeleccionados != null){				
				listaSubRamos = configBonosService.getSubramosPorRamos(listaRamosSeleccionados);
//				listaLineaNegocio = configBonosService.getLineasNegocioPorRamos(listaRamosSeleccionados);
				listaLineaNegocio = configBonosService.getLineasNegocioPorProducto(listaProductosSeleccionados);
				
			}
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="listarCoberturasPorLineaNegocio",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaCoberturas.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaCoberturas.*"})
		})
	public String listarCoberturasPorLineaNegocio(){
		try {
			if(listaLineaNegocioSeleccionados != null){				
				listaCoberturas= configBonosService.getCoberturasPorLineasNegocio(listaLineaNegocioSeleccionados);				
			}
		} catch (Exception e) {		
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="guardarConfiguracionDeBonos",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalleBono","namespace","/fuerzaventa/configuracionBono","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","configuracionBono.id","${configuracionBono.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${configuracionBono.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalleBono","namespace","/fuerzaventa/configuracionBono","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${configuracionBono.id}"}),
			@Result(name="test",location=DETALLEBONOS)
		})
	public String guardarConfiguracionDeBonos(){
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			String keyMessage= "midas.configBonos.historial.accion";
			
			if(configuracionBono.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			
			cargaListas();
				
			configBonosService.saveConfiguration(configuracionBono);
			guardarHistorico(TipoOperacionHistorial.CONFIGURACION_BONOS, configuracionBono.getId(),keyMessage,tipoAccionHistorial);
			setIdTipoOperacion(70L);

			
			onSuccess();
		} catch (Exception e) {		
			inicializarListas();
			onError(e);
			result="test";
		}
		return result;
	}
	
	
	private List<ConfigBonoGerencia> obtenerGerenciasSeleccionadas(){
		List<ConfigBonoGerencia> list = new ArrayList<ConfigBonoGerencia>();
		if(!isEmptyList(gerenciaLong)){
			for(Long idGerencia :gerenciaLong){
				if(isNotNull(idGerencia)){
					ConfigBonoGerencia obj = new ConfigBonoGerencia();
					Gerencia gerencia = new Gerencia();
					gerencia.setId(idGerencia);
					obj.setGerencia(gerencia);
					list.add(obj);
				}
			}
		}
		return list;
	}
	
	private List<ConfigBonoEjecutivo> obtenerEjecutivosSeleccionadas(){
		List<ConfigBonoEjecutivo> list = new ArrayList<ConfigBonoEjecutivo>();
		if(!isEmptyList(ejecutivoLong)){
			for(Long idEjecutivo :ejecutivoLong){
				if(isNotNull(idEjecutivo)){
					ConfigBonoEjecutivo obj = new ConfigBonoEjecutivo();
					Ejecutivo ejecutivo = new Ejecutivo();
					ejecutivo.setId(idEjecutivo);
					obj.setEjecutivo(ejecutivo);
					list.add(obj);
				}
			}
		}
		return list;
	}
	private List<ConfigBonoPromotoria> obtenerPromotoriaSeleccionadas(){
		List<ConfigBonoPromotoria> list = new ArrayList<ConfigBonoPromotoria>();
		if(!isEmptyList(promotoriaLong)){
			for(Long idPromotoria :promotoriaLong){
				if(isNotNull(idPromotoria)){
					ConfigBonoPromotoria obj = new ConfigBonoPromotoria();
					Promotoria promotoria = new Promotoria();
					promotoria.setId(idPromotoria);
					obj.setPromotoria(promotoria);
					list.add(obj);
				}
			}
		}
		return list;
	}
	
	
	@Action(value ="verDetalleBono", results={
		@Result(name = SUCCESS, location = DETALLEBONOS)
	})
	
	public String verDetalleBono(){
		return SUCCESS;
	}
	
	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=CONFIGURACIONBONOSGRID),
			@Result(name=INPUT,location=CONFIGURACIONBONOSGRID)
		})
	@Override
	public String listarFiltrado() {
		try {
			listaConfiguracionesBonos = configBonosService.findByFiltersView(filtroConfigBono);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 ============================================================ Excepciones ============================================================
	 * */
	public void prepareVerExcepcion(){
		try {
			catalogoTipocedula=catalogoService.obtenerElementosPorCatalogo("Tipos de Cedula de Agente");
//			catalogoTipoBono = catalogoService.obtenerElementosPorCatalogo("Tipos de Bono");
			listaBonoEclusion = entidadService.findAll(ConfigBonos.class);
			listaBusquedaCedula = exclusionTipoCedulaService.findById(configuracionBono.getId());
			listaBusquedaBono = tipoBonoService.loadByConfiguration(configuracionBono.getId());
			configuracionBono = entidadService.findById(ConfigBonos.class, configuracionBono.getId());
			ValorCatalogoAgentes modoAplic =  catalogoService.obtenerElementoEspecifico("Modos de Aplicacion Bonos","Importe");//PORCENTAJE
			Map<String,Object> param = new HashMap<String, Object>();
			param.put("id", configuracionBono.getId());
			param.put("modoAplicacion.id", modoAplic.getId());
			isCheckedImporte = entidadService.findByPropertiesCount(ConfigBonos.class, param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Action(value = "verExcepcion", results ={
			@Result(name =SUCCESS, location =EXCEPCIONESBONOS)
	})
	public String verExcepcion(){
		
		return SUCCESS;
	}
	
	@Action(value = "agregarPoliza", results ={
			@Result(name =SUCCESS, location =AGREGARPOLIZA)
	})
	public String agregarPoliza(){
		return SUCCESS;
	}
	
	@Action(value="seleccionarPoliza",results ={
			@Result(name=SUCCESS,type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^filtroPoliza.*"}),
			@Result(name=INPUT,type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^filtroPoliza.*"})
	})
	public String seleccionarPoliza(){
	
			try {
				filtroPoliza = configBonosService.getInfoPoliza(filtroPoliza);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return SUCCESS;
	}
	
	public void prepareAgregarNegocio(){
		listaNegociosActivos = listadoService.listarNegociosActivos(null);
	}
	
	@Action (value="agregarNegocio", results={
			@Result(name=SUCCESS, location=AGREGARNEGOCIO)
	})
	
	public String agregarNegocio(){
		return SUCCESS;
	}
	
	@Action(value="seleccionarAgente", results ={
		@Result(name = SUCCESS, type ="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.idAgente,^agente\\.id,^agente\\.persona\\.nombreCompleto"})	
	})	
	public String seleccionarAgente(){
		agente=agenteMidasService.loadById(agente);
		return SUCCESS;
	}
	
	@Action(value="llenarListaNegDesasoc", results={
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^nuevaListaNeg.*"})
	})
	public String llenarListaNegDesasoc(){
		
		nuevaListaNeg.addAll(configBonosService.getListNegociosDesasociado(filtroNegocio));
		return SUCCESS;
	}
	
	@Action(value="regresarObjetoNegocios", results={
			@Result(name=SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^nuevaListaNeg.*"})
	})
	public String regresarObjetoNegocios(){
		for(ConfigBonosNegView elementoList :obtenerNegocios){
			List<ConfigBonosNegView> listTemp = new ArrayList<ConfigBonosNegView>();
			listTemp.addAll(configBonosService.getListNegociosDesasociado(elementoList));
			for(ConfigBonosNegView elemntoFin :listTemp){
				nuevaListaNeg.add(elemntoFin);
			}
		}
		return SUCCESS;
	}
	
	@Action(value="listarPolizas",results={
			@Result(name=SUCCESS, location=LISTARAGREGARPOLIZAGRID)
	})
	
	public String listarPolizas(){
		if (onlyHeaders) {
			listaPolizas = new ArrayList<ExcepcionesPolizaView>();
		} else {
			filtroPoliza.setNumeroPoliza(getObtenerIdPoliza());
			filtroPoliza.setNumeroPolizaSeycos(getNumeroPolizaSeycos());
			System.out.println(getObtenerIdPoliza()+", "+getNumeroPolizaSeycos());
			listaPolizas = configBonosService.getPolizaExcepsiones(filtroPoliza,configuracionBono);
		}
		return SUCCESS;
	}
	
	@Action(value="listarExcepcionAgentes",results={
			@Result(name=SUCCESS, location=AGENTEGRID)
	})
	
	public String listarExcepcionAgentes(){
		try {
			
			listaExcepxionesAgentes = configBonoExcepAgenteService.loadByConfigBono(configuracionBono);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public void prepareSaveConfigBonoExcepAgente(){
		listaExcepxionesAgentes = new ArrayList<ConfigBonoExcepAgente>();
		System.out.println("****listaExcepxionesAgentes********se limpia lalista");
	}
	@Action(value="saveConfigBonoExcepAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*"})
	})
	public String saveConfigBonoExcepAgente(){
		
		configBonosService.saveConfigBonoExcepcionAgente(listaExcepxionesAgentes, configuracionBono);
		
		return SUCCESS;
	}
	
	@Action(value="listarExcepcionNegocios",results={
			@Result(name=SUCCESS, location=NEGOCIOGRID)
	})
	
	public String listarExcepcionNegocios(){
		try {
			
			if(configuracionBono.getId()!=null){
				try {
					ConfigBonosDTO filtroConfigBono=new ConfigBonosDTO();
					filtroConfigBono.setId(configuracionBono.getId());
					listaDescripcionBono = configBonosService.findByFiltersView(filtroConfigBono);
				
					configuracionBono.setTipoBeneficiario(listaDescripcionBono.get(0).getTipoBeneficiario());
					
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
			
			listaExcepxionesNegocios = configBonoExcepNegocioService.loadByConfigBono(configuracionBono);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public void prepareSaveConfigBonoExcepNegocio(){
		listaExcepxionesNegocios = new ArrayList<ConfigBonoExcepNegocio>();
	}
	@Action(value="saveConfigBonoExcepNegocio",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*"})
	})
	public String saveConfigBonoExcepNegocio(){
		
		configBonosService.saveConfigBonoExcepcionNegocio(listaExcepxionesNegocios, configuracionBono);
		
		return SUCCESS;
	}
	
	@Action(value="mostrarRangoFactores",results={
			@Result(name=SUCCESS, location=RANGOFACTORES),
			@Result(name=INPUT, location=RANGOFACTORES)
	})	
	public String mostrarRangoFactores(){
		return SUCCESS;
	}
	
	@Action(value="listarRangoFactores",results={
			@Result(name=SUCCESS,location=RANGOFACTORESGRID),
			@Result(name=INPUT,location=RANGOFACTORESGRID)
		})
	public String listarRangoFactores() {
		try {
			listRangoFacotres=configBonoRangoAplicaService.loadByConfigBono(configuracionBono);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public void prepareSaveListRangoFactores(){
		listRangoFacotres = new ArrayList<ConfigBonoRangoAplica>();
	}
	@Action(value="saveListRangoFactores",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*,mensaje,tipoMensaje"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*,mensaje,tipoMensaje"})
	})
	public String saveListRangoFactores(){
		try {
			configuracionBono = configBonoRangoAplicaService.saveConfigBonoExcepAgente(configuracionBono,listRangoFacotres);
			if(!listRangoFacotres.isEmpty()){
				setMensaje("Los rangos han sido guardados exitosamente"+validarCrecimientoYsupMeta(listRangoFacotres));
			}else{
				setMensaje("Los rangos han sido eliminados exitosamente");
			}
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		} catch (Exception e) {			
			String[] mensajeError=e.toString().split("Exception:");
			setMensaje(mensajeError[1]);
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			e.printStackTrace();
		}
		return SUCCESS;
	}
	public String validarCrecimientoYsupMeta(List<ConfigBonoRangoAplica> listRangoFacotres){
		int cont  =0;
		int cont2 =0;
		for(ConfigBonoRangoAplica obj:listRangoFacotres){
			if (obj.getPctCrecimientoInicial() != null
					&& obj.getPctCrecimientoFinal() != null
					&& (obj.getPctCrecimientoFinal().compareTo(BigDecimal.ZERO) != 0 
							|| obj.getPctCrecimientoInicial().compareTo(BigDecimal.ZERO) != 0)) {
				cont++;
			}
			if(obj.getPctSupMetaInicial()!=null 
					&& obj.getPctSupMetaFinal()!=null
							&& (obj.getPctSupMetaFinal().compareTo(BigDecimal.ZERO) != 0 
									|| obj.getPctSupMetaInicial().compareTo(BigDecimal.ZERO) != 0)){
				cont2++;
			}
		}
		if(cont>0 && cont2>0){
			return ", es necesario llene los siguientes campos Periodo de Comparación y Producción Minima $";
		}else if(cont>0){
			return ", es necesario llene el siguiente campo Periodo de Comparación";
		}else if(cont2>0){
			return ", es necesario el siguiente campo Producción Minima $";
		}
		return "";
	}
	@Action(value="seleccionarPromotoria", results ={
			@Result(name = SUCCESS, type ="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^promotoria.*"})	
		})
		
		public String seleccionarPromotoria(){
		promotoria=entidadService.findById(Promotoria.class,promotoria.getId());
			return SUCCESS;
		}
	
	@Action(value="saveConfigBonoExcluTipoBono", results ={
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false"})
	})
	
	public String saveConfigBonoExcluTipoBono(){
			
		configBonosService.saveConfigBonoExclusionTipoBono(obtenerListCatalogoTipoBono, configuracionBono);
		
		return null;
	}

	@Action(value="saveConfigBonoExcluTipoCedula", results={
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false"})
	})
	
	public String saveConfigBonoExcluTipoCedula(){
		
		configBonosService.saveConfigBonoExclusionTipoCedula(obtenerListCatalogoTipoCedula, configuracionBono);
		
		return null;
	}
	
	public void prepareSaveConfigBonoExcepPoliza(){
		listabonoExcepcionPoliza = new ArrayList<BonoExcepcionPoliza>();
	}
	
	@Action(value="saveConfigBonoExcepPoliza",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configuracionBono.*"})
	})
	public String saveConfigBonoExcepPoliza(){
		
		configBonosService.saveConfigBonoExcepcionPoliza(listabonoExcepcionPoliza, configuracionBono);
		
		return SUCCESS;
	}
	
	@Action(value="listarExcepcionPoliza",results={
			@Result(name=SUCCESS, location=POLIZAGRID)
	})
	
	public String listarExcepcionPoliza(){
		try {
			
			if(configuracionBono.getId()!=null){
				try {
					ConfigBonosDTO filtroConfigBono=new ConfigBonosDTO();
					filtroConfigBono.setId(configuracionBono.getId());
					listaDescripcionBono = configBonosService.findByFiltersView(filtroConfigBono);
				
					configuracionBono.setTipoBeneficiario(listaDescripcionBono.get(0).getTipoBeneficiario());
					
				} catch (Exception e) {
					e.printStackTrace();
				}	
			}
			
			listabonoExcepcionPoliza = excepcionPolizaService.loadByConfigBono(configuracionBono);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="grabarYcopiarConfigBonos",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalleBono","namespace","/fuerzaventa/configuracionBono","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}","configuracionBono.id","${configuracionBono.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${configuracionBono.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalleBono","namespace","/fuerzaventa/configuracionBono","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}","idTipoOperacion","${idTipoOperacion}","idRegistro","${configuracionBono.id}"})
		})
	public String grabarYcopiar(){
		
			try {
				
				cargaListas();
				
				configuracionBono = configBonosService.clonarObjeto(configuracionBono);
				TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
				String keyMessage= "midas.configBonos.historial.accion";
				guardarHistorico(TipoOperacionHistorial.CONFIGURACION_BONOS, configuracionBono.getId(),keyMessage,tipoAccionHistorial);
				setIdTipoOperacion(70L);
				onSuccess();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				onError(e);
			}
		return SUCCESS;
	}
	
	public void prepareGuardarExclusiones() throws Exception {
		configuracionBono = entidadService.findById(ConfigBonos.class, configuracionBono.getId());
	}	
	
	@Action(value="guardarExclusiones",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalleBono","namespace","/fuerzaventa/configuracionBono","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","configuracionBono.id","${configuracionBono.id}","tabActiva","${tabActiva}","idTipoOperacion","${idTipoOperacion}","idRegistro","${configuracionBono.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalleBono","namespace","/fuerzaventa/configuracionBono","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}","idTipoOperacion","${idTipoOperacion}","idRegistro","${configuracionBono.id}"})
		})
		
	public String guardarExclusiones(){
		
		try {
			configuracionBono = configBonosService.saveExclusiones(configuracionBono);
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			String keyMessage= "midas.configBonos.historial.accion";
			guardarHistorico(TipoOperacionHistorial.CONFIGURACION_BONOS, configuracionBono.getId(),keyMessage,tipoAccionHistorial);
			setIdTipoOperacion(70L);
			onSuccess();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			onError(e);
		}
		return SUCCESS;
	}
	
	public void prepareObtenerAgentesXConfiguracion(){
		promotoriasSeleccionadas=new ArrayList<ConfigBonoPromotoria>();
	}
	@Action(value="obtenerAgentesXConfiguracion",results={
			@Result(name=SUCCESS,location=GRIDAGENTE)
	})
	public String obtenerAgentesXConfiguracion(){
		
		try {
			ConfigBonos configuracion = new ConfigBonos();
			configuracion.setListaCentroOperaciones(centroOperacionesSeleccionados);		
			configuracion.setListaGerencias(gerenciasSeleccionadas);
			configuracion.setListaEjecutivos(ejecutivosSeleccionados);
			configuracion.setListaPromotorias(obtenerPromotoriasSeleccionadas(promotoriasSelecionadasLong));
			configuracion.setListaTiposPromotoria(tipoPromotoriasSeleccionadas);
					
			configuracion.setListaConfigPolizas(listaConfigPolizas);
			listaAgentes.clear();
			for(Agente objAgente : lista_agentes){
				ConfigBonoAgente objConfigBonoAgente = new ConfigBonoAgente();
				objConfigBonoAgente.setAgente(objAgente);
				listaAgentes.add(objConfigBonoAgente);
			}
			configuracion.setListaAgentes(listaAgentes);
			configuracion.setListaTipoAgentes(tipoAgentesSeleccionados);
			configuracion.setListaPrioridades(prioridadesSeleccionadas);
			configuracion.setListaSituaciones(situacionesSeleccionadas);
			
	
			listaAgente = configBonosService.getAgentePorConfiguracion(configuracion);
			
			
		
			
//			listaAplicaAgentes.clear();
//			for ( Agente objAgente : lista_AplicaAgentes ){
//				ConfigBonoAplicaAgente objAplicaAgente = new ConfigBonoAplicaAgente();
//				objAplicaAgente.setAgente(objAgente);
//				listaAplicaAgentes.add(objAplicaAgente);
//			}
//			configuracionBono.setListaAplicaAgentes(listaAplicaAgentes);
			
//			listaAplicaPromotorias.clear();
//			for(Promotoria objPromotoria : lista_AplicaPromotorias){
//				ConfigBonoAplicaPromotoria aplicaPromotoria = new ConfigBonoAplicaPromotoria();
//				aplicaPromotoria.setPromotoria(objPromotoria);
//				listaAplicaPromotorias.add(aplicaPromotoria);
//			}
//			configuracionBono.setListaAplicaPromotorias(listaAplicaPromotorias);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="abrirModalGirdAgente", results={
			@Result(name=SUCCESS,location=MODALAGENTE)
	})
	
	public String AbrirModalGirdAgente(){
		
		return SUCCESS;
	}
	
	@Action(value="validarGridRanfoFactores", results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^mensaje,tipoMensaje"})
	})
	public String validarGridRanfoFactores(){
		try{
			configBonoRangoAplicaService.validarRangofactores(listRangoFacotres);
			setMensaje("Los Rangos estan configurados exitosamente");
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		}catch (Exception e) {	
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
		}
		return SUCCESS;
	}
	
	private List<ConfigBonoPromotoria> obtenerPromotoriasSeleccionadas(List<Long> lista){
		List<ConfigBonoPromotoria> litConfigBonoPromotoria= new ArrayList<ConfigBonoPromotoria>();
		for(Long obj : lista){
			Promotoria prom = new Promotoria();
			ConfigBonoPromotoria objConfigBonoPromotoria= new ConfigBonoPromotoria();
			prom.setId(obj);
			objConfigBonoPromotoria.setPromotoria(prom);
			litConfigBonoPromotoria.add(objConfigBonoPromotoria);
		}
		return litConfigBonoPromotoria;
	}
	
	@Action(value="listarExclucionCveAmis",results={
			@Result(name=SUCCESS, location=EXCL_CVEAMIS_GRID)
	})
	
	public String listarExclucionCveAmis(){
		try {
			
			listaRangosClaveAmis = configBonosService.getListaBonoRangosClaveAmis(configuracionBono);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	@Action(value="saveListaExclucionCveAmis",results={
			@Result(name=SUCCESS, location=EXCL_CVEAMIS_GRID)
	})
	
	public String saveListaExclucionCveAmis(){
			
		configBonosService.saveConfigBonoRangosClaveAmis(listaRangosClaveAmis,configuracionBono);
		
		return SUCCESS;
	}
	
	
	private void cargaListas() throws Exception {
		
		//TODO quitar cuando se corrija de raiz el modelo de configuracion de Bonos
		
		if(configuracionBono != null){
			for(ValorCatalogoAgentes listaLineaVenta:listaLineaVentaSeleccionadas){
				if(listaLineaVenta!=null && listaLineaVenta.getClave()!=""){
					ConfigBonoLineaVenta objLineaVenta = new ConfigBonoLineaVenta();
					
					ValorCatalogoAgentes filtro=new ValorCatalogoAgentes();
		            GrupoCatalogoAgente catalogoLineaVenta=new GrupoCatalogoAgente();
		            catalogoLineaVenta.setDescripcion("Lineas de Venta");
		            filtro.setGrupoCatalogoAgente(catalogoLineaVenta);
		            filtro.setClave(listaLineaVenta.getClave());
		            
		            List <ValorCatalogoAgentes> valorCatalogoAgentesobj = catalogoService.findByFilters(filtro);
					objLineaVenta.setIdLineaVenta(valorCatalogoAgentesobj.get(0).getId());
					listLineaVentaSelec.add(objLineaVenta);
				}
			}
			configuracionBono.setLineaVenta(listLineaVentaSelec);
			configuracionBono.setProductos(listaProductosSeleccionados);
			configuracionBono.setRamos(listaRamosSeleccionados);
			configuracionBono.setSubramos(listaSubRamosSeleccionados);
			configuracionBono.setSecciones(listaLineaNegocioSeleccionados);
			configuracionBono.setCoberturas(listaCoberturasSeleccionadas);
			
			configuracionBono.setListaCentroOperaciones(centroOperacionesSeleccionados);
			configuracionBono.setListaGerencias(obtenerGerenciasSeleccionadas());
			configuracionBono.setListaEjecutivos(obtenerEjecutivosSeleccionadas());
			configuracionBono.setListaPromotorias(obtenerPromotoriaSeleccionadas());
			configuracionBono.setListaTiposPromotoria(tipoPromotoriasSeleccionadas);
			configuracionBono.setListaTipoAgentes(tipoAgentesSeleccionados);
			configuracionBono.setListaPrioridades(prioridadesSeleccionadas);
			configuracionBono.setListaSituaciones(situacionesSeleccionadas);
			listaAgentes.clear();
			for(Agente objAgente : lista_agentes){
				ConfigBonoAgente objConfigBonoAgente = new ConfigBonoAgente();
				objConfigBonoAgente.setAgente(objAgente);
				listaAgentes.add(objConfigBonoAgente);
			}
			configuracionBono.setListaAgentes(listaAgentes);
			
			listaAplicaAgentes.clear();
			for ( Agente objAgente : lista_AplicaAgentes ){
				ConfigBonoAplicaAgente objAplicaAgente = new ConfigBonoAplicaAgente();
				objAplicaAgente.setAgente(objAgente);
				listaAplicaAgentes.add(objAplicaAgente);
			}
			configuracionBono.setListaAplicaAgentes(listaAplicaAgentes);
			
			listaAplicaPromotorias.clear();
			for(Promotoria objPromotoria : lista_AplicaPromotorias){
				ConfigBonoAplicaPromotoria aplicaPromotoria = new ConfigBonoAplicaPromotoria();
				aplicaPromotoria.setPromotoria(objPromotoria);
				listaAplicaPromotorias.add(aplicaPromotoria);
			}
			configuracionBono.setListaAplicaPromotorias(listaAplicaPromotorias);
			configuracionBono.setListaConfigPolizas(listaConfigPolizas);
			configuracionBono.setFechaCreacion(new Date());
			
			Long idTipoBenefConfig = null;
			try {
				idTipoBenefConfig = catalogoService.obtenerIdElementEspecifico("Tipo de Beneficiario", tipoBeneficiarioString);
			} catch (Exception ex) {
				//Do nothing
			} finally {
			
				configuracionBono.setTipoBeneficiario(idTipoBenefConfig);
			}
			
		
		}
	}
	
}
