package mx.com.afirme.midas2.action.catalogos.fuerzaventa.centrooperacion;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/centrooperacion")
@Component
@Scope("prototype")
public class CentroOperacionAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7766324106791396821L;
	private CentroOperacionService centroOperacionService;
	private GerenciaJPAService gerenciaJpaService;
	private final String VERDETALLE_JSP="/jsp/catalogos/fuerzaventa/centrooperacion/detalle.jsp";
	private final String LISTAR_JSP="/jsp/catalogos/fuerzaventa/centrooperacion/mostrar.jsp";
	private final String GRID_JSP="/jsp/catalogos/fuerzaventa/centrooperacion/centroOperacionGrid.jsp";
	private final String NAMESPACE="/fuerzaventa/centrooperacion";
	/***Components of view****************************************/
	private CentroOperacion centroOperacion;
	private CentroOperacion filtroCentro;
	private List<CentroOperacionView> listaCentroOperacion = new ArrayList<CentroOperacionView>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion;
	private List<Gerencia> listGerencias = new ArrayList<Gerencia>();	
	/**
	 * Action:
	 * 
	 */
	@Override
	public void prepare() throws Exception {
		if(centroOperacion!=null){
			if(centroOperacion.getId() !=null){
				centroOperacion = centroOperacionService.loadById(centroOperacion);
			}
			if("1".equals(tipoAccion)){
				centroOperacion=new CentroOperacion();
			}
		}
	}
	
	
	@Action(value="guardar",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","centroOperacion.id","${centroOperacion.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${centroOperacion.id}"}),
				@Result(name=INPUT,type="redirectAction",
						params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
						"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","centroOperacion.id","${centroOperacion.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${centroOperacion.id}"})})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			if(centroOperacion.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			centroOperacion = centroOperacionService.saveFull(centroOperacion);
			guardarHistorico(TipoOperacionHistorial.CENTRO_OPERACION, centroOperacion.getId(),"midas.centroOperacion.historial.accion",tipoAccionHistorial);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);		
			setTipoAccion("4");
			setIdTipoOperacion(10L);
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void validateGuardar() {
//		String domainObjectPrefix = "centroOperacion";
//        addErrors(centroOperacion, NewItemChecks.class, this, domainObjectPrefix);
	}
	public void validateEditar(){
		addErrors(centroOperacion, EditItemChecks.class, this, "centroOperacion");
	}

	@Action(value="eliminar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","centroOperacion.id","${centroOperacion.id}",
					"idTipoOperacion","${idTipoOperacion}","idRegistro","${centroOperacion.id}"}),
			@Result(name=INPUT,location=VERDETALLE_JSP)
	})

	@Override
	public String eliminar() {
		try {
			setIdTipoOperacion(10L);
			setIdRegistro(centroOperacion.getId());
			centroOperacionService.unsubscribe(centroOperacion);
			guardarHistorico(TipoOperacionHistorial.CENTRO_OPERACION, centroOperacion.getId(),"midas.centroOperacion.historial.accion",TipoAccionHistorial.BAJA);
			setMensajeExito();
		}catch(Exception e){
			e.printStackTrace();
			centroOperacion.setClaveEstatus(1L);
			setMensajeError("No es posible eliminar el Centro de Operación, está relacionado a una Gerencia");
			return INPUT;
		}
		return SUCCESS;
	}
	

	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location=VERDETALLE_JSP)
	})
	@Override
	public String verDetalle(){
		if(centroOperacion!=null){
			if(centroOperacion.getId() !=null){
			    try {
				    centroOperacion = centroOperacionService.loadById(centroOperacion);
				    setIdTipoOperacion(10L);
					setIdRegistro(centroOperacion.getId());
					prepareVerDetalle();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "verDetalleHistorico", results = { @Result(name = SUCCESS, location = VERDETALLE_JSP) })
	public String verDetalleHistorico() {
		if (centroOperacion != null) {
			if (centroOperacion.getId() != null) {
				try {
				    centroOperacion = centroOperacionService
						.loadById(centroOperacion, this.getUltimaModificacion().getFechaHoraActualizacionString());				   
				    prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}
	
	public void prepareMostrarContenedor(){}
	
	@Action(value="mostrarContenedor",results={
		@Result(name=SUCCESS,location=LISTAR_JSP)
	})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	
	@Action(value="mostrarBusquedaResponsable",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/centrooperacion/mostrarBusquedaResponsable.jsp")
	})
	public String mostrarBusquedaResponsable(){
		return SUCCESS;
	}
	
	@Action(value="mostrarGerencias",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/centrooperacion/mostrarGerencias.jsp")
	})
	public String mostrarGerencias(){
		try{
			listGerencias = gerenciaJpaService.findByCentroOperacion(centroOperacion.getId());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="listarBusquedaResponsable",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/centrooperacion/listarBusquedaResponsable.jsp")
	})
	public String listarBusquedaResponsable(){
		return SUCCESS;
	}

	@Action(value="lista",results={
		@Result(name=SUCCESS,location=GRID_JSP),
		@Result(name=INPUT,location=GRID_JSP)
	})
	@Override
	public String listar() {
		return SUCCESS;
	}

	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location=GRID_JSP),
		@Result(name=INPUT,location=GRID_JSP)
	})
	@Override
	public String listarFiltrado() {
		try{
//			Map<String, Object> params=ReflectionUtils.getMapFromBean(filtro);
			if("consulta".equals(tipoAccion)){
				if(filtroCentro==null){
					filtroCentro=new CentroOperacion();
               }
				filtroCentro.setClaveEstatus(1L);
			}
			listaCentroOperacion=centroOperacionService.findByFiltersView(filtroCentro);
		}catch(Exception e){
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	@Action(value="findById",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^centroOperacion\\.id,^centroOperacion\\.descripcion","excludeProperties","^centroOperacion\\.gerencias.*,^centroOperacion\\.personaResponsable.*"}),
		/*@Result(name="1",type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^centroOperacion.*","excludeProperties","^centroOperacion\\.gerencias.*"}),*/
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^centroOperacion.*"})
	})
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}

	/***Sets and gets****************************************************/
	@Autowired
	@Qualifier("centroOperacionServiceEJB")
	public void setCentroOperacionService(CentroOperacionService centroOperacionService){
		this.centroOperacionService=centroOperacionService;
	}
	
	@Autowired
	@Qualifier("gerenciaJPAServiceEJB")
	public void setGerenciaJpaService(GerenciaJPAService gerenciaJpaService) {
		this.gerenciaJpaService = gerenciaJpaService;
	}
	
	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	public List<CentroOperacionView> getListaCentroOperacion() {
		return listaCentroOperacion;
	}
	public void setListaCentroOperacion(List<CentroOperacionView> listaCentroOperacion) {
		this.listaCentroOperacion = listaCentroOperacion;
	}

	public CentroOperacion getFiltroCentro() {
		return filtroCentro;
	}

	public void setFiltroCentro(CentroOperacion filtroCentro) {
		this.filtroCentro = filtroCentro;
	}

	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}


	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}
	
	public String getTipoAccion() {
		return tipoAccion;
	}


	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}


	public List<Gerencia> getListGerencias() {
		return listGerencias;
	}


	public void setListGerencias(List<Gerencia> listGerencias) {
		this.listGerencias = listGerencias;
	}

}