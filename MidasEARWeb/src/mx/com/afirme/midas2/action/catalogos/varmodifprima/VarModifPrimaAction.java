package mx.com.afirme.midas2.action.catalogos.varmodifprima;

import mx.com.afirme.midas2.domain.catalogos.*;

import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.*;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class VarModifPrimaAction extends CatalogoAction implements Preparable {
	private Long id;

	private String tipoAccion;

	private VarModifPrima tcVarModifPrima;

	private List<VarModifPrima> tcVarModifPrimaList;

	private VarModifPrimaService tcVarModifPrimaService;

	private TipoAccionDTO catalogoTipoAccionDTO;
	private static final long serialVersionUID = -5243724700575384865L;

	public void validateGuardar() {
		addErrors(tcVarModifPrima, NewItemChecks.class, this, "tcVarModifPrima");
	}

	public void validateEditar() {
		addErrors(tcVarModifPrima, EditItemChecks.class, this,
				"tcVarModifPrima");
	}

	public void prepare() throws Exception {

		if (getId() != null) {
			tcVarModifPrima = tcVarModifPrimaService.findById(getId());
		}

	}

	public String execute() {
		return null;
	}

	@Override
	public String eliminar() {
		tcVarModifPrimaService.remove(tcVarModifPrima);
		tcVarModifPrima = new VarModifPrima();

		return SUCCESS;
	}

	@Override
	public String guardar() {
		tcVarModifPrimaService.save(tcVarModifPrima);
		tcVarModifPrima = new VarModifPrima();

		return SUCCESS;
	}

	@Override
	public String listar() {
		tcVarModifPrimaList = tcVarModifPrimaService.findAll();

		return SUCCESS;
	}

	@Override
	public String listarFiltrado() {
		tcVarModifPrimaList = tcVarModifPrimaService
				.findByFilters(tcVarModifPrima);

		return SUCCESS;
	}

	@Override
	public String verDetalle() {

		return SUCCESS;
	}

	public VarModifPrimaAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public VarModifPrima getPrimaDescripcion() {
		return tcVarModifPrima;
	}

	public void setVarModifDescripcion(VarModifPrima tcVarModifPrima) {
		this.tcVarModifPrima = tcVarModifPrima;
	}

	public List<VarModifPrima> getPrimaDescripcionList() {
		return tcVarModifPrimaList;
	}

	public void setPrimaDescripcionList(List<VarModifPrima> tcVarModifPrimaList) {
		this.tcVarModifPrimaList = tcVarModifPrimaList;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	@Autowired
	@Qualifier("tcVarModifPrimaServiceEJB")
	public void sePrimaDescripcion(VarModifPrimaService tcVarModifPrimaService) {
		this.tcVarModifPrimaService = tcVarModifPrimaService;
	}

}
