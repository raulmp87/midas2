package mx.com.afirme.midas2.action.catalogos.servvehiculolinnegtipoveh;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVehId;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ServVehiculoLinNegTipoVehAction extends CatalogoAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3871830311046820678L;


	public void validateGuardar(){
		addErrors(servVehiculoLinNegTipoVeh, NewItemChecks.class, this, "servVehiculoLinNegTipoVeh");
		initDetalle();
	}
	
	
	@Override
	public void prepare() throws Exception {
		if (getId() != null) {
			servVehiculoLinNegTipoVeh = entidadService.findById(ServVehiculoLinNegTipoVeh.class, getId());
		}
	}
	

	public void prepareMostrarCatalogo() throws Exception {
		if (getId() != null) {
			servVehiculoLinNegTipoVeh = new ServVehiculoLinNegTipoVeh();
			id = new ServVehiculoLinNegTipoVehId();
		}		
	}
	
	
	public String mostrarCatalogo(){
		beforeMostrarCatalogo();
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		entidadService.remove(servVehiculoLinNegTipoVeh);
		servVehiculoLinNegTipoVeh = new ServVehiculoLinNegTipoVeh();
		beforeMostrarCatalogo();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String guardar() {
		entidadService.save(servVehiculoLinNegTipoVeh);
		servVehiculoLinNegTipoVeh = new ServVehiculoLinNegTipoVeh();
		beforeMostrarCatalogo();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String listar() {
		return SUCCESS;
	}
	
	@Override
	public String listarFiltrado() {
		servVehiculoLinNegTipoVehList = 
			listadoService.getListarServVehiculoLinNegTipoVehPorSeccion(
					servVehiculoLinNegTipoVeh.getId().getIdToSeccion());
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		initDetalle();		
		return SUCCESS;
	}
	
	private void beforeMostrarCatalogo(){
		seccionList = listadoService.getListarSeccionesVigentesAutosUsables();
	}
	
	private void initDetalle(){
		seccionList = listadoService.getListarSeccionesVigentesAutosUsables();
		tipoServicioVehiculoMap = new LinkedHashMap<BigDecimal, String>();
		tipoVehiculoMap = new LinkedHashMap<BigDecimal, String>();
		
		if(servVehiculoLinNegTipoVeh != null && servVehiculoLinNegTipoVeh.getId().getIdToSeccion() !=null){
			tipoVehiculoMap = listadoService.getMapTipoVehiculoPorSeccion(BigDecimal.valueOf(servVehiculoLinNegTipoVeh.getId().getIdToSeccion()));
			tipoServicioVehiculoMap = listadoService.getMapTipoServicioVehiculoPorTipoVehiculo((BigDecimal.valueOf(servVehiculoLinNegTipoVeh.getId().getIdTcTipoVehiculo())));
		}
	}
	
	public ServVehiculoLinNegTipoVehAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	
	private ServVehiculoLinNegTipoVehId id;

	private String tipoAccion;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
		
	private ServVehiculoLinNegTipoVeh servVehiculoLinNegTipoVeh;
	
	private List<SeccionDTO> seccionList;
	
    private List<ServVehiculoLinNegTipoVeh> servVehiculoLinNegTipoVehList;
    
    private  Map<BigDecimal, String> tipoServicioVehiculoMap;
    
    private Map<BigDecimal, String> tipoVehiculoMap;
    

	public ServVehiculoLinNegTipoVehId getId() {
		return id;
	}

	public void setId(ServVehiculoLinNegTipoVehId id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	public ServVehiculoLinNegTipoVeh getServVehiculoLinNegTipoVeh() {
		return servVehiculoLinNegTipoVeh;
	}

	public void setServVehiculoLinNegTipoVeh(
			ServVehiculoLinNegTipoVeh servVehiculoLinNegTipoVeh) {
		this.servVehiculoLinNegTipoVeh = servVehiculoLinNegTipoVeh;
	}

	public List<SeccionDTO> getSeccionList() {
		return seccionList;
	}

	public void setSeccionList(List<SeccionDTO> seccionList) {
		this.seccionList = seccionList;
	}

	public List<ServVehiculoLinNegTipoVeh> getServVehiculoLinNegTipoVehList() {
		return servVehiculoLinNegTipoVehList;
	}

	public void setServVehiculoLinNegTipoVehList(
			List<ServVehiculoLinNegTipoVeh> servVehiculoLinNegTipoVehList) {
		this.servVehiculoLinNegTipoVehList = servVehiculoLinNegTipoVehList;
	}

	public Map<BigDecimal, String> getTipoServicioVehiculoMap() {
		return tipoServicioVehiculoMap;
	}

	public void setTipoServicioVehiculoMap(
			Map<BigDecimal, String> tipoServicioVehiculoMap) {
		this.tipoServicioVehiculoMap = tipoServicioVehiculoMap;
	}

	public Map<BigDecimal, String> getTipoVehiculoMap() {
		return tipoVehiculoMap;
	}

	public void setTipoVehiculoMap(Map<BigDecimal, String> tipoVehiculoMap) {
		this.tipoVehiculoMap = tipoVehiculoMap;
	}



	private ListadoService listadoService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(
			ListadoService listadoService) {
		this.listadoService = listadoService;
	}


}
