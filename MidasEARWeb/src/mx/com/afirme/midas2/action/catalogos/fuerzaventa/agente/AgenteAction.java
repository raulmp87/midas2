package mx.com.afirme.midas2.action.catalogos.fuerzaventa.agente;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNull;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isValid;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilFacadeRemote;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoFacadeRemote;
import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.catalogos.sector.SectorFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.DocumentoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EntretenimientoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HijoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.TipoDocumentoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteCarteraClientesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.DomicilioView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote.TipoDomicilio;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ClienteAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService.TipoContrato;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/agente")
@InterceptorRefs({
    @InterceptorRef("timer"),
    @InterceptorRef("midas2Stack")
})
public class AgenteAction extends CatalogoHistoricoAction implements Preparable{
	
	private static final Logger LOG = Logger.getLogger(AgenteAction.class);
	/** serialVersionUID	 **/
	private static final long serialVersionUID = 3554147864521589305L;
	private InputStream contratoInputStream;
	private String contentType;
	private String fileName;
	private ListadoService listadoService;
	private AgenteMidasService agenteMidasService;
	private BancoFacadeRemote bancoFacadeRemote;
	private ProductoBancarioAgenteService productoService;
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	private ClienteAgenteService clienteAgenteService;
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	private ImpresionesService impresionesService;
	private ClienteFacadeRemote clienteFacadeRemote;
	private FortimaxService fortimaxService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private EstadoFacadeRemote estadoFacadeRemote;
	private MunicipioFacadeRemote municipioFacadeRemote;
	private Long tipoOperac;
	private Long tipoAccionAgente;
	/***Components of view****************************************************************/
	private ClienteAgente clienteAgente;
	private Agente agente = new Agente();
	private Agente filtroAgente = new Agente();
	private HijoAgente hijoAgente;
	private EntretenimientoAgente entretenimientoAgente; 
	private List<AgenteView> listaAgente= new ArrayList<AgenteView>();
	private List<ClienteAgente> listaCarteraCliente= new ArrayList<ClienteAgente>();
	private List<ClienteJPA> listClientes = new ArrayList<ClienteJPA>();
	private List<HijoAgente> listHijoAgentes = new ArrayList<HijoAgente>();
	private List<ClienteGenericoDTO>clientesList = new ArrayList<ClienteGenericoDTO>();
	private List<EntretenimientoAgente> listEntretenimiento = new ArrayList<EntretenimientoAgente>();
	private List<ValorCatalogoAgentes> listTipoEntretenimiento = new ArrayList<ValorCatalogoAgentes>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion;
	private String tabActiva;
	private Long idGerencia;
	private Long idEjecutivo;
	private String rfcSiglas;
	private String rfcFecha;
	private String rfcHomoclave;
	private Map<Long, String> ejecutivoList = new LinkedHashMap<Long, String>();
	private Map<Long, String> promotoriaList = new LinkedHashMap<Long, String>();
	private List<TipoDocumentoAgente> catalogoTipoDocumentosAgente=new ArrayList<TipoDocumentoAgente>();
	private List<DocumentoAgente> catalogoDocumentosAgente=new ArrayList<DocumentoAgente>();
	private List<ProductoBancarioAgente> productosAgente=new ArrayList<ProductoBancarioAgente>();
	private DomicilioFacadeRemote domicilioFacade;
	private ProductoBancarioAgente producto;
	private ProductoBancario productoBancarioSeleccionado;
	private String claveDefault;
	private String test;
	private String urlIfimax;
	private List<DocumentoAgente> listaAuxDocumentosAgente=new ArrayList<DocumentoAgente>();
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private EstadoCivilFacadeRemote estadoCivilFacade;
	private String domicilioCompleto;
	private SectorFacadeRemote sectorFacadeRemote;
	private Agente altaPorInternet;
	private PersonaSeycosDTO personaSeycosDTO;
	private String documentosFaltantes;
	private String productosBancariosAgregados;
	private String hijosAgente;
	private String entretenimientosAgente;	
	private ClienteJPA clienteJPA;
	private String asociado;
	private Integer moduloOrigen;
	private Long idEstatusRelacion;
	private String tipoDocumento;
	private List<ClienteAgente> listaClientes = new ArrayList<ClienteAgente>();	
	private String tipoReturn;
	private Agente cargaMasiva;
	private File archivo;
    private InputStream inputStream;
    private int registrosInsertados;
    private int registrosNoInsertados;
    private String logErrores="";
    private String carpetaDocumento;
    private ProductoBancarioAgente idProductoBancario = new ProductoBancarioAgente();
    private int id_Row;
	private int claveDefaultInt;
	private List <Integer> regresarValores = new ArrayList<Integer>();
	private Long tipoOper;
    private List <String>lista=new ArrayList<String>();   
    private Boolean claveAgenteSoloLectura = false;
    private boolean agentePromotor = false;
	/***Performance**************************************************************/
	private List<ValorCatalogoAgentes> catalogoMotivoEstatus=new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> catalogoPrioridad=new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> catalogoTipocedula=new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> catalogoTipoAgente=new ArrayList<ValorCatalogoAgentes>();
	private static List<ValorCatalogoAgentes> catalogoTipoSituacion=new ArrayList<ValorCatalogoAgentes>();
	private static List<ProductoBancario> productosBancarios=new ArrayList<ProductoBancario>();
	private static List<BancoDTO> listaBancos=new ArrayList<BancoDTO>();
	private Map<Long, String> gerenciaList = new LinkedHashMap<Long, String>();
	private static List<SectorDTO> sectorList = new ArrayList<SectorDTO>();//
	private static List<EstadoCivilDTO> estadosCiviles=new ArrayList<EstadoCivilDTO>();//
	private DomicilioView datosDomicilio;
	private List<AgenteCarteraClientesView> listaCarteraClienteGrid = new ArrayList<AgenteCarteraClientesView>();  
	private static List<ValorCatalogoAgentes> catalogoClasificacionAgentes = new ArrayList<ValorCatalogoAgentes>();
	/**********Fortimax*********************************/
	
	private List<DocumentosAgrupados>listDocAgrupado = new ArrayList<DocumentosAgrupados>();
	private CatalogoDocumentoFortimax documentoFortimax;
	private String nombreCarpeta;
	private String nombreDocumento;
	private String nombreGaveta;
	private String modalFortimax; 
	private String fechaHoraActualizFormatoMostrar;
	private String  usuarioActualizacion;
	private String situacionAgenteString;
	
	private String archivotxt;
	@Override
	public void prepare() throws Exception {
		if(tipoAccion!=null && tipoAccion.equals("1") && (tabActiva ==null || tabActiva.equals(""))){
			agente=new Agente();
		}
//		cargarCatalogosIniciales();
		//prepareVerDetalle();
	}
	public void prepareMostrarContenedor(){
//		gerenciaList=listadoService.getMapGerencias();
//		estadosCiviles=estadoCivilFacade.findAll();
//		sectorList=sectorFacadeRemote.findAll();
		catalogoTipoSituacion=cargarCatalogo("Estatus de Agente (Situacion)");
		try {
			cargarCatalogosIniciales();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
//		filtroAgente=getFilterInSession(Agente.class);
	}
	
	
	@Action(value="mostrarContenedor",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/agenteCatalogo.jsp")
	})
	public String mostrarContenedor(){
		super.removeListadoEnSession();
		return SUCCESS;
	}
	
	@Action(value="paginarAgente",results={
		@Result(name=SUCCESS,location=PAGINADO_GRID)
	})
	public String paginar(){
		initMaxRowNum();
		return SUCCESS;
	}
	
	public void initMaxRowNum(){
		if(getTotalCount() == null){
			setTotalCount(agenteMidasService.getMaxRowNumFindByFiltersWithAddressSupport(filtroAgente));
			setListadoEnSession(null);
		}
		setPosPaginado();
	}
	
	@Action(value="lista",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/agenteGrid.jsp")
	})
	@Override
	public String listar() {
		filtroAgente=null;
//		initMaxRowNum();
//		Long posicionInicial=(getPosStart()!=null)?new Long(getPosStart()):1L;
		listaAgente = agenteMidasService.findByFiltersWithAddressSupportPaging(filtroAgente,REGISTROS_A_MOSTRAR,0l);//posicionInicial
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/agenteGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/agenteGrid.jsp")
	})
	@Override
	public String listarFiltrado(){
		try{
//			initMaxRowNum();
//			Long posicionInicial=(getPosStart()!=null)?new Long(getPosStart()):1L;
//			setFilterInSession(filtroAgente);
			listaAgente = agenteMidasService.findByFiltersWithAddressSupportPaging(filtroAgente,REGISTROS_A_MOSTRAR,0l);//posicionInicial
//			catalogoService.obtenerElementoEspecifico("Tipos de Cedula de Agente", "RIESGOS ESPECIALES");
//			catalogoService.obtenerElementosPorCatalogo("Tipos de Cedula de Agente");
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
		return SUCCESS;
	}
	
	private boolean isEmptyAddress(Domicilio domicilio){
		boolean isEmpty=true;
		if(isNotNull(domicilio)){
			if(isValid(domicilio.getCalleNumero())){
				isEmpty=false;
			}
			if(isValid(domicilio.getCodigoPostal())){
				isEmpty=false;
			}
			if(isValid(domicilio.getCiudad())){
				isEmpty=false;
			}
			if(isValid(domicilio.getClaveCiudad())){
				isEmpty=false;
			}
			if(isValid(domicilio.getClaveEstado())){
				isEmpty=false;
			}
			if(isValid(domicilio.getNombreColonia())){
				isEmpty=false;
			}
		}
		return isEmpty;
	}
	/**
	 * Metodo que evita que se mande info de filtros vacia y que alente la consulta por filtros
	 */
	private void revisarFiltrosVacios(){
		if(isNotNull(filtroAgente)){
			Promotoria promotoria=filtroAgente.getPromotoria();
			if(isNotNull(promotoria) && isNull(promotoria.getId())){
				filtroAgente.setPromotoria(null);
			}
			Persona persona=filtroAgente.getPersona();
			if(isNotNull(persona)){
				boolean personaVacia=false;
				if(!isValid(persona.getNombreCompleto()) && !isValid(persona.getRfc()) && !isValid(persona.getTelefonoOficina())){
					personaVacia=true;
				}
				Domicilio domicilio=(!isEmptyList(persona.getDomicilios()))?persona.getDomicilios().get(0):null;
				boolean domicilioEmpty=isEmptyAddress(domicilio); 
				if(domicilioEmpty){
					personaVacia=true;
				}
				if(personaVacia){
					filtroAgente.setPersona(null);
				}
			}
		}
	}
	
	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/detalleAgente.jsp")
	})
	@Override
	public String verDetalle(){
		if(agente!=null){
			if(agente.getId() !=null){
				agente = agenteMidasService.loadById(agente);
				setIdTipoOperacion(50L);
				setIdRegistro(agente.getId());
			}
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/detalleAgente.jsp")
		})		
		public String verDetalleHistorico(){
			if(agente!=null){
				if(agente.getId() !=null){
					agente = agenteMidasService.loadById(agente, this.getUltimaModificacion().getFechaHoraActualizacionString());
					
					try {
						prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}
			}
			return SUCCESS;
		}
	
	private void cargarCatalogosIniciales() throws Exception{		
		gerenciaList=listadoService.getMapGerencias();
		
		if(isEmptyList(estadosCiviles)){
			estadosCiviles=estadoCivilFacade.findAll();
		}
		if(isEmptyList(sectorList)){
			sectorList=sectorFacadeRemote.findAll();
		}
		if(isEmptyList(catalogoMotivoEstatus)){
			catalogoMotivoEstatus=catalogoService.obtenerElementosPorCatalogo("Motivo de Estatus del Agente");
		}
		if(isEmptyList(catalogoPrioridad)){
			catalogoPrioridad=catalogoService.obtenerElementosPorCatalogo("Prioridades de Agente");
		}
			catalogoTipocedula=catalogoService.obtenerElementosPorCatalogo("Tipos de Cedula de Agente");
		
		if(isEmptyList(catalogoTipoAgente)){
			catalogoTipoAgente=catalogoService.obtenerElementosPorCatalogo("Tipo de Agente","AGENTE PROMOTOR");
		}
		if(isEmptyList(listaBancos)){
			listaBancos=bancoFacadeRemote.findAll();
		}
		if(isEmptyList(catalogoClasificacionAgentes)){
			catalogoClasificacionAgentes=catalogoService.obtenerElementosPorCatalogo("Clasificacion de Agente");
		}
//		if(isEmptyList(productosBancarios)){
//			productosBancarios=listadoService.getListaProductosBancarios();
//		}
	}
	
	public void prepareMostrarInfoGeneral(){
		if(agente.getId()==null){
			agente= new Agente();
		}
		try {
			cargarCatalogosIniciales();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
//		gerenciaList=listadoService.getMapGerencias();
//		catalogoMotivoEstatus=cargarCatalogo("Motivo de Estatus del Agente");
//		catalogoPrioridad=cargarCatalogo("Prioridades de Agente");
//		try {
//			catalogoTipocedula=catalogoService.obtenerElementosPorCatalogo("Tipos de Cedula de Agente");
//		} catch (Exception e) {		
//			e.printStackTrace();
//		}
//		catalogoTipoAgente=cargarCatalogo("Tipo de Agente");
		catalogoTipoSituacion=cargarCatalogo("Estatus de Agente (Situacion)");
		if(agente==null || agente.getId()==null){//Si es nuevo el agente, la situacion solo es pendiente por autorizar.
			if(catalogoTipoSituacion!=null && !catalogoTipoSituacion.isEmpty()){
				ValorCatalogoAgentes elemento=null;
				for(ValorCatalogoAgentes elem:catalogoTipoSituacion){
					if("PENDIENTE POR AUTORIZAR".equals(elem.getValor())){
						elemento=elem;
						break;
					}
				}
				catalogoTipoSituacion.clear();
				catalogoTipoSituacion.add(elemento);
			}			
		}	
		else{			
			if(agente.getId() !=null){	
				
				if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
				{
					agente = agenteMidasService.loadById(agente, this.getUltimaModificacion().getFechaHoraActualizacionString());					
				}
				else
				{
					agente=entidadService.findById(Agente.class,agente.getId());					
				}				
				
				if(agente.getPromotoria()!=null){
					ejecutivoList=listadoService.getMapEjecutivosPorGerencia(agente.getPromotoria().getEjecutivo().getGerencia().getId());
					promotoriaList=listadoService.getMapPromotoriasPorEjecutivo(agente.getPromotoria().getEjecutivo().getId());
					setIdGerencia(agente.getPromotoria().getEjecutivo().getGerencia().getId());
					setIdEjecutivo(agente.getPromotoria().getEjecutivo().getId());
				}
				else{
					gerenciaList=listadoService.getMapGerencias();
				}
				
				claveAgenteSoloLectura = agenteMidasService.isAgenteAutorizadoPreviamente(agente.getId());
				
				agentePromotor = agenteMidasService.isAgentePromotor(agente.getId());
				
			}			
		}
	}
	
	@Action(value="mostrarInfoGeneral",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/generales/agenteDatosGenerales.jsp")
	})
	public String mostrarInfoGeneral(){		
		if(agente.getId()==null){
			agente.setFechaAlta(new Date());
		}
		return SUCCESS;
	}
	
	public void prepareGuardarGeneralAgente(){
		if(agente!=null && agente.getId()!=null){
			agente=agenteMidasService.loadById(agente);
		}
		else{
			agente= new Agente();
		}
	}
		
	public void validateGuardarGeneralAgente() {
//		addErrors(agente, this, "agente");
	}
	
	@Action(value="guardarGeneralAgente",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/agente","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
					"tipoAccionAgente","${tipoAccionAgente}","tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","tabActiva","${tabActiva}","agente.id","${agente.id}","idRegistro", "${agente.id}", "idTipoOperacion", "${tipoOper}"}),					 
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/detalleAgente.jsp", params={"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}"})})
	public String guardarGeneralAgente() {
		try {
			if(agente.getId()==null){
				agente.setFechaAlta(new Date());
			}	
				tipoOper = 50L; 
			if(getModuloOrigen()!=null){
				tipoOper = 120L;
			}
			ValorCatalogoAgentes cedula = new ValorCatalogoAgentes();
			cedula.setId(agente.getTipoCedula().getId());
			agente.setTipoCedula(cedula);
			ValorCatalogoAgentes clasificacionAgt = new ValorCatalogoAgentes();
			clasificacionAgt.setId(agente.getClasificacionAgentes().getId());
			agente.setClasificacionAgentes(clasificacionAgt);
			Ejecutivo ejec = new Ejecutivo();
			Gerencia gerencia = new Gerencia();
			ejec.setId(idEjecutivo);
			gerencia.setId(idGerencia);
			agente.getPromotoria().setEjecutivo(ejec);
			agente.getPromotoria().getEjecutivo().setGerencia(gerencia);
			agente=agenteMidasService.saveDatosGeneralesAgente(agente);
			guardarHistoricoAgente("info_general",false);
			setTabActiva("domicilios");
			setMensajeExito();
			setTipoMensaje(TIPO_MENSAJE_EXITO);
//			Map<String, Map<String, List<String>>> mapCorreos = agenteMidasService
//					.obtenerCorreos(agente.getId(),
//							GenericMailService.P_ALTA_AGENTE,
//							GenericMailService.M_ALTA_REGISTRO);
//			agenteMidasService.enviarCorreo(mapCorreos, null, null,
//					GenericMailService.T_BIENVENIDO, null);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			tabActiva="info_general";
			setMensajeError(e.toString());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareMostrarDomicilioAgente(){
//		if(agente!=null){
//			if(agente.getId() !=null){
//				agente = agenteMidasService.loadById(agente);
//			}
//		}
	}	
	
	@Action(value="mostrarDomicilioAgente",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/domicilios/agenteDomicilio.jsp")
	})
	public String mostrarDomicilioAgente(){
		try {
			if(agente!=null && agente.getId() !=null){
				
				if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
				{
					agente = agenteMidasService.loadById(agente, this.getUltimaModificacion().getFechaHoraActualizacionString());					
				}
				else
				{
					agente = agenteMidasService.loadById(agente);					
				}
				
			}
			if(agente!=null && agente.getPersona() !=null){
				List<Domicilio> listDomicilio = new ArrayList<Domicilio>();
				Long idPersona=agente.getPersona().getIdPersona();
				PersonaSeycosDTO persona;
				if(tipoAccion.equalsIgnoreCase("5")) // Consulta Historico
				{
					persona = personaSeycosFacade.findById(idPersona, this.getUltimaModificacion().getFechaHoraActualizacionString());
					agente.getPersona().setDomicilios(persona.getDomicilios());
					
				}else
				{

					persona = personaSeycosFacade.findById(idPersona);	
					listDomicilio = persona.getDomicilios();
					Domicilio domicilioPersActual = null;
					Domicilio domicilioOficActual = null;
					Domicilio domicilioFiscActual = null;
					if(listDomicilio != null && !listDomicilio.isEmpty()){
						for(Domicilio domicilio:listDomicilio){
						  if(domicilio!=null){
							if(domicilio.getTipoDomicilio()!=null && "PERS".equals(domicilio.getTipoDomicilio().trim())){
								if(domicilioPersActual==null){
									domicilioPersActual=domicilio;
								}
								else if(domicilio.getIdDomicilio()!=null && domicilioPersActual.getIdDomicilio()!=null && domicilioPersActual.getIdDomicilio().getIdDomicilio()<domicilio.getIdDomicilio().getIdDomicilio()){
									domicilioPersActual=domicilio;
								}
							}
							else if(domicilio.getTipoDomicilio()!=null && "OFIC".equals((domicilio.getTipoDomicilio()!=null)?domicilio.getTipoDomicilio().trim():"")){
								if(domicilioOficActual==null){
									domicilioOficActual=domicilio;
								}
								else if(domicilio.getIdDomicilio()!=null && domicilioOficActual.getIdDomicilio()!=null && domicilioOficActual.getIdDomicilio().getIdDomicilio()<domicilio.getIdDomicilio().getIdDomicilio()){
									domicilioOficActual=domicilio;
								}
							}
							else if(domicilio.getTipoDomicilio()!=null && "FISC".equals((domicilio.getTipoDomicilio()!=null)?domicilio.getTipoDomicilio().trim():"")){
								if(domicilioFiscActual==null){
									domicilioFiscActual=domicilio;
								}
								else if(domicilio.getIdDomicilio()!=null && domicilioFiscActual.getIdDomicilio()!=null && domicilioFiscActual.getIdDomicilio().getIdDomicilio()<domicilio.getIdDomicilio().getIdDomicilio()){
									domicilioFiscActual=domicilio;
								}
							}
						  }
						}
						
					}
					listDomicilio.clear();
					listDomicilio.add(domicilioPersActual);
					listDomicilio.add(domicilioOficActual);
					listDomicilio.add(domicilioFiscActual);
					agente.getPersona().setDomicilios(listDomicilio);	
				}				
				
						
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public void validateGuardarDomicilioAgente() {
//		String domainObjectPrefix = "agente";
//        addErrors(agente, Agente.class, this, domainObjectPrefix);
        addErrors(agente, this, "agente");
	}
	
	@Action(value="guardarDomicilioAgente",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/agente","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
					"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","tabActiva","${tabActiva}","agente.id","${agente.id}"}),					 
			@Result(name=INPUT,type="redirectAction", params={"actionName","verDetalle","namespace","/fuerzaventa/agente"})})
	public String guardarDomicilioAgente() {
		try {
			if(agente.getPersona() != null && agente.getPersona().getDomicilios()!=null && !agente.getPersona().getDomicilios().isEmpty()){				
				if(agente.getPersona().getDomicilios().get(0)!=null){					 
					agente.getPersona().getDomicilios().get(0).setTipoDomicilio(TipoDomicilio.PERSONAL.getValue());					
				}
				if(agente.getPersona().getDomicilios().get(1)!=null){					
					agente.getPersona().getDomicilios().get(1).setTipoDomicilio(TipoDomicilio.OFICINA.getValue());					
				}
				agente=agenteMidasService.saveDomiciliosAgente(agente);
				guardarHistoricoAgente("domicilios",false);
				setMensajeExito();
				tabActiva="datosFiscales";
			}		
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareMostrarDatosFiscales(){
		try {
			if(agente!=null){
				if(agente.getId() !=null){
					
					if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
					{
						agente = agenteMidasService.loadById(agente, this.getUltimaModificacion().getFechaHoraActualizacionString());
						if(agente.getPersona().getIdRepresentante()!=null){
							personaSeycosDTO = personaSeycosFacade.findById(agente.getPersona().getIdRepresentante(), 
									this.getUltimaModificacion().getFechaHoraActualizacionString());
						}
						
					}else
					{
						agente = agenteMidasService.loadById(agente);
						if(agente.getPersona().getIdRepresentante()!=null){
							personaSeycosDTO = personaSeycosFacade.findById(agente.getPersona().getIdRepresentante());
						}						
					}					
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}	
	
	@Action(value="mostrarDatosFiscales",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/fiscales/agenteDatosFiscales.jsp")
	})
	public String mostrarDatosFiscales(){
		try{
			if(agente!=null && agente.getPersona() !=null){				
				List<Domicilio> listDomicilio = new ArrayList<Domicilio>();
				Long idPersona=agente.getPersona().getIdPersona();
				PersonaSeycosDTO persona;
				if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
				{
					persona = personaSeycosFacade.findById(idPersona, this.getUltimaModificacion().getFechaHoraActualizacionString());
					agente.getPersona().setDomicilios(persona.getDomicilios());
				}
				else
				{
					persona = personaSeycosFacade.findById(idPersona);
					agente.getPersona().setDomicilios(persona.getDomicilios());
				}
				
				/*listDomicilio = persona.getDomicilios();
				Domicilio domicilioPersActual = null;
				Domicilio domicilioFiscActual = null;
				if(listDomicilio != null && !listDomicilio.isEmpty()){
					for(Domicilio domicilio:listDomicilio){
						if(domicilio.getTipoDomicilio()!=null && "PERS".equals(domicilio.getTipoDomicilio().trim())){
							if(domicilioPersActual==null){
								domicilioPersActual=domicilio;
							}
							else if(domicilioPersActual.getIdDomicilio()!=null && domicilio.getIdDomicilio()!=null && domicilioPersActual.getIdDomicilio().getIdDomicilio()<domicilio.getIdDomicilio().getIdDomicilio()){
								domicilioPersActual=domicilio;
							}
						}
						else if(domicilio.getTipoDomicilio()!=null && "FISC".equals((domicilio.getTipoDomicilio()!=null)?domicilio.getTipoDomicilio().trim():"")){
							if(domicilioFiscActual==null){
								domicilioFiscActual=domicilio;
							}
							else if(domicilioFiscActual.getIdDomicilio()!=null && domicilio.getIdDomicilio()!=null && domicilioFiscActual.getIdDomicilio().getIdDomicilio()<domicilio.getIdDomicilio().getIdDomicilio()){
								domicilioFiscActual=domicilio;
							}
						}				
					}
					listDomicilio.clear();
					listDomicilio.add(domicilioFiscActual);
					listDomicilio.add(domicilioPersActual);
					agente.getPersona().setDomicilios(listDomicilio);	
				}*/
			}
		}catch(Exception e){
			setMensajeError(e.getMessage());
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public void prepareGuardarDatosFiscales(){
		agente=agenteMidasService.loadById(agente);
	}
	
	public void validateGuardarDatosFiscales() {
		String domainObjectPrefix = "agente";
        //addErrors(agente, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	@Action(value="guardarDatosFiscales",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/agente","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
				"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","tabActiva","${tabActiva}","agente.id","${agente.id}"})
	})
	public String guardarDatosFiscales() {
		try {
			agente = agenteMidasService.saveDatosFiscalesAgente(agente);
			setMensajeExito();
			guardarHistoricoAgente("datosFiscales",false);
			tabActiva="datosContables";
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareMostrarDatosContables(){
		if(agente!=null){
			if(agente.getId() !=null){
				
				if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
				{
					agente = agenteMidasService.loadById(agente, this.getUltimaModificacion().getFechaHoraActualizacionString());
				}
				else
				{
					agente=entidadService.findById(Agente.class, agente.getId());					
				}				
			}
		}
	}
	
	@Action(value="mostrarDatosContables",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/contables/agenteDatosContables.jsp")
	})
	public String mostrarDatosContables(){
		cargarBancos();
		//cargarProductosBancarios();
		return SUCCESS;
	}
	private void cargarBancos(){
		listaBancos=bancoFacadeRemote.findAll();
	}
	
	private void cargarProductosBancarios(){
		productosBancarios=listadoService.getListaProductosBancarios();
	}
	
	public void validateGuardarDatosContables() {
		String domainObjectPrefix = "agente";
        //addErrors(agente, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	public void prepareGuardarDatosContables(){
		agente=entidadService.findById(Agente.class, agente.getId());
	}
	
	@Action(value="guardarDatosContables",results={		
		@Result(name=SUCCESS,type="redirectAction",	params={"actionName","verDetalle","namespace","/fuerzaventa/agente","idRegistro", "${agente.id}", "idTipoOperacion", "50",
				"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}","agente.id","${agente.id}"}),
		@Result(name=INPUT,type="redirectAction",	params={"actionName","verDetalle","namespace","/fuerzaventa/agente","idRegistro", "${agente.id}", "idTipoOperacion", "50",
						"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}","agente.id","${agente.id}"})	
	})
	public String guardarDatosContables() {
		try {
			agente=agenteMidasService.saveDatosContablesAgente(agente);
			agenteMidasService.llenarYGuardarListaProductosBancarios(productosBancariosAgregados,agente);
			guardarHistoricoAgente("datosContables",false);
			setMensajeExito();
			tabActiva="datosExtra";
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError(e.getMessage());
			tabActiva="datosContables";
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareMostrarDatosExtra(){
		if(agente!=null){
			if(agente.getId() !=null){
				
				if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
				{
					agente = agenteMidasService.loadById(agente, this.getUltimaModificacion().getFechaHoraActualizacionString());					
				}
				else
				{
					agente = agenteMidasService.loadById(agente);					
				}				
			}
		}
	}
	
	@Action(value="mostrarDatosExtra",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/extras/agenteDatosExtra.jsp")
	})
	public String mostrarDatosExtra() throws Exception{
//		GrupoCatalogoAgente grupoCatalogoAgente = new GrupoCatalogoAgente();
//		grupoCatalogoAgente.setId((long)80);
		GrupoCatalogoAgente grupoCatalogoAgente = catalogoService.obtenerCatalogoPorDescripcion("Tipos de Entretenimiento");
		ValorCatalogoAgentes valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setGrupoCatalogoAgente(grupoCatalogoAgente);
		
		listTipoEntretenimiento = entidadService.findByProperty(ValorCatalogoAgentes.class, "grupoCatalogoAgente.id", valorCatalogoAgentes.getGrupoCatalogoAgente().getId());
		return SUCCESS;
	}
	
	public void validateGuardarDatosExtra() {
		String domainObjectPrefix = "agente";
        //addErrors(agente, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	public void prepareGuardarDatosExtra(){
		agente = entidadService.findById(Agente.class, agente.getId());
	}
	
	@Action(value="guardarDatosExtra",results={
			@Result(name=SUCCESS,type="redirectAction",	params={"actionName","verDetalle","namespace","/fuerzaventa/agente","idRegistro", "${agente.id}", "idTipoOperacion", "50",
					"tipoAccion","${tipoAccion}","moduloOrigen","${moduloOrigen}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}","agente.id","${agente.id}"})
	})
	public String guardarDatosExtra() {
		try {			
			agente = entidadService.save(agente);
			agenteMidasService.eliminarEntretenimientosAgente(agente.getId());
			agenteMidasService.eliminarHijosAgente(agente.getId());
			agenteMidasService.llenarYGuardarListaHijosAgente(hijosAgente, agente);
			agenteMidasService.llenarYGuardarListaEntretenimientosAgente(entretenimientosAgente, agente);
			guardarHistoricoAgente("datosExtra",false);
			tabActiva="documentos";
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoEntretenimiento",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/extras/listAgenteEntretenimientoGrid.jsp"),
			@Result(name=ERROR,location="/jsp/catalogos/fuerzaventa/agente/extras/listAgenteEntretenimientoGrid.jsp")
	})
	public String listarFiltradoEntretenimiento(){
		try{
			if(agente!=null && agente.getId()!=null){
				
				if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
				{
					listEntretenimiento = agenteMidasService.findByPropertyWithHistorySupport(EntretenimientoAgente.class, "agente.id", 
							agente.getId(), this.getUltimaModificacion().getFechaHoraActualizacionString());
				}
				else
				{
					listEntretenimiento = entidadService.findByProperty(EntretenimientoAgente.class, "agente.id", agente.getId());						
				}				
			}
			if(tabActiva.equals("altaInternet")){
				listEntretenimiento=new ArrayList<EntretenimientoAgente>();
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Action(value="guardarDatosEntretenimientoExtra",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"})
	})
	public String guardarDatosEntretenimientoExtra() {
		try {
			if(agente!=null && agente.getId()!=null && entretenimientoAgente!=null){
				entretenimientoAgente.setAgente(agente);
				entidadService.save(entretenimientoAgente);
			}
			setMensajeExito();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="eliminarDatosEntretenimientoExtra",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","test"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","test"})
	})
	public String eliminarDatosEntretenimientoExtra() {
		try {
			entretenimientoAgente = entidadService.findById(EntretenimientoAgente.class, entretenimientoAgente.getId());
			entidadService.remove(entretenimientoAgente);
			setMensajeExito();
			setTest("1");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Eliminar el Entretenimiento del Agente");
			setTest("0");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoHijoAgente",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/extras/listAgenteHijoGrid.jsp"),
			@Result(name=ERROR,location="/jsp/catalogos/fuerzaventa/agente/extras/listAgenteHijoGrid.jsp")
	})
	public String listarFiltradoHijoAgente(){
		try{
			if(agente!=null && agente.getId()!=null){
				
				if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
				{
					listHijoAgentes = agenteMidasService.findByPropertyWithHistorySupport(HijoAgente.class, "agente.id", agente.getId(), this.getUltimaModificacion().getFechaHoraActualizacionString());
				}
				else
				{
					listHijoAgentes = entidadService.findByProperty(HijoAgente.class, "agente.id", agente.getId());						
				}							
			}
			if(tabActiva.equals("altaInternet")){
				listHijoAgentes=new ArrayList<HijoAgente>();
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Action(value="guardarDatosHijoExtra",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"})
	})
	public String guardarDatosHijoExtra() {
		try {
			if(agente!=null && agente.getId()!=null && hijoAgente!=null){
				hijoAgente.setAgente(agente);
				entidadService.save(hijoAgente);
			}
			setMensajeExito();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="eliminarDatosHijoExtra",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","test"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","test"})
	})
	public String eliminarDatosHijoExtra() {
		try {
			hijoAgente = entidadService.findById(HijoAgente.class, hijoAgente.getId());
			entidadService.remove(hijoAgente);
			setMensajeExito();
			setTest("1");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Eliminar el Hijo del Agente");
			setTest("0");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="mostrarDocumentos",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/agenteDocumentos.jsp")
	})
	public String mostrarDocumentos(){
		try {
			agente=agenteMidasService.loadById(agente);		
			PersonaSeycosDTO personaSeycos = personaSeycosFacade.findById(agente.getPersona().getIdPersona());			
			Persona per = new Persona();
			per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
			per.setIdPersona(personaSeycos.getIdPersona());
			agente.setPersona(per);
			String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());	
			
			List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpetaConTipoPersona("AGENTES", "ALTA DE AGENTES", per.getClaveTipoPersona());
			
			for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
				if(doc!=null){
					String []respDoc=fortimaxService.generateDocument(agente.getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+agente.getId(), doc.getCarpeta().getNombreCarpetaFortimax());					
				}
			}
			
			if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(agente.getId(), "AGENTES","ALTA DE AGENTES")){
//				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorAplicacionTipoPersona("AGENTES", per.getClaveTipoPersona());
//				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpetaConTipoPersona("AGENTES", "ALTA DE AGENTES", per.getClaveTipoPersona());
					for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
						if(doc!=null){
//							String []respDoc=fortimaxService.generateDocument(agente.getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+agente.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
//							if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
								DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
								docFortimax.setCatalogoDocumentoFortimax(doc);
								docFortimax.setExisteDocumento(0);
								docFortimax.setIdRegistro(agente.getId());
								documentoEntidadService.save(docFortimax);
//							}
						}
					}			
			}
			else{
				documentoEntidadService.sincronizarDocumentos(agente.getId(), "AGENTES", "ALTA DE AGENTES");
			}
			listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(agente.getId(), "AGENTES", "ALTA DE AGENTES");		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}

	@Action(value="matchDocumentosAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchDocumentosAgente() {
			try {				
				documentoEntidadService.sincronizarDocumentos(agente.getId(), "AGENTES", "ALTA DE AGENTES");
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(agente.getId(), "AGENTES", "ALTA DE AGENTES");
				setMensaje(MENSAJE_EXITO);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				setMensaje(MENSAJE_ERROR_GENERAL);
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
			return SUCCESS;
		}
	
	public void validateGuardarDocumentos() {
		String domainObjectPrefix = "agente";
        //addErrors(agente, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	@Action(value="guardarDocumentos",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"})
	})
	public String guardarDocumentos() {
		try {			
			agente = entidadService.findById(Agente.class, agente.getId());
			Long tipoAgenteVentaDirecta = catalogoService.obtenerIdElementEspecifico("Tipo de Agente", "VENTAS DIRECTAS");
			Long tipoAgenteVentaDirecta2 = catalogoService.obtenerIdElementEspecifico("Tipo de Agente", "VENTA DIRECTA 2");
			listaDocumentosFortimax=documentoEntidadService.getListaDocumentosFaltantes(agente.getId(), "AGENTES", "ALTA DE AGENTES");
			StringBuilder docsFaltantes = new StringBuilder();
			if(!listaDocumentosFortimax.isEmpty() && (!tipoAgenteVentaDirecta.equals(agente.getIdTipoAgente()) || !tipoAgenteVentaDirecta2.equals(agente.getIdTipoAgente()))){
				for(DocumentoEntidadFortimax docFaltante: listaDocumentosFortimax){
					docsFaltantes.append(docFaltante.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax());
					docsFaltantes.append(",");
				}
				setDocumentosFaltantes(docsFaltantes.toString().substring(0,docsFaltantes.length()-1));
			}else{
				guardarHistoricoAgente("documentos",false);
			}			
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="generarLigaIfimax",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp")
		})
		public String generarLigaIfimax(){
		String []resp= new String[3];
		try {			
//			List<CatalogoDocumentoFortimax> docsCarpeta=documentoCarpetaService.obtenerDocumentosRequeridosPorCarpetaConTipoPersona("AGENTES", "ALTA DE AGENTES", agente.getPersona().getClaveTipoPersona());				
//			resp=fortimaxService.generateLinkToDocument(agente.getId(),"AGENTES", docsCarpeta.get(0).getNombreDocumentoFortimax()+"_"+agente.getId());
			resp=fortimaxService.generateLinkToDocument(agente.getId(),"AGENTES","");
			urlIfimax=resp[0];		
		} catch (Exception e) {			
			LOG.error(e.getMessage(), e);			
			return INPUT;
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
		}	
	@Action(value="imprimirContrato",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","contratoInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})})	
	public String imprimirContrato() {
		Locale locale= getLocale();		
		agente=entidadService.findById(Agente.class, agente.getId());
		TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirContratoAgente(agente,TipoContrato.AGENTE,locale);				
		contratoInputStream=new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
		String []expediente=new String[1];
		expediente[0]=agente.getId().toString();
		try {
			fortimaxService.uploadFile("CONTRATO_"+agente.getId()+".pdf", transporteImpresionDTO, "AGENTES",expediente , "01 ALTA DE AGENTES");
		} catch (Exception e) {			
			LOG.error(e.getMessage(), e);
		}
		contentType = "application/pdf";
		fileName = "contrato.pdf";		
		return SUCCESS;		
	}
	
	@Action(value="mostrarCarteraClientes",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/cartera/agenteCarteraClientes.jsp")
		})
	public String mostrarCarteraClientes(){
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoCarteraCliente",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/cartera/listClientesGrid.jsp"),
		@Result(name=ERROR,location="/jsp/catalogos/fuerzaventa/agente/cartera/listClientesGrid.jsp")
	})
	public String listarFiltradoCarteraCliente(){
		try{
			listaCarteraCliente.clear();
			if(agente!=null && agente.getId()!=null){
				clienteAgente = new ClienteAgente();
				clienteAgente.setAgente(agente);
				idEstatusRelacion=catalogoService.obtenerIdElementEspecifico("Tipos de Estatus","ACTIVO");
				
				if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
				{					
//					listaCarteraCliente = clienteAgenteService.findByFilters(clienteAgente, this.getUltimaModificacion().getFechaHoraActualizacionString());
					listaCarteraClienteGrid = clienteAgenteService.findByFiltersCarteraClientesGrid(clienteAgente, this.getUltimaModificacion().getFechaHoraActualizacionString());
				}
				else
				{
					listaCarteraClienteGrid = clienteAgenteService.findByFiltersCarteraClientesGrid(clienteAgente, null);
//					listaCarteraCliente = clienteAgenteService.findByFilters(clienteAgente);						
				}							
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Action(value="selectCliente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","personaSeycosDTO"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","personaSeycosDTO"})
		})
		public String selectCliente() {
			try {
				personaSeycosDTO = personaSeycosFacade.findById(agente.getPersona().getIdPersona());
				
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				setMensajeError("Error al Guardar");
				return INPUT;
			}
			return SUCCESS;
		}
	@Action(value="clienteAsociado",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","asociado"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","asociado"})
		})
		public String clienteAsociado() {
			try {				
				asociado=clienteAgenteService.buscarAsociacionClienteAgente(clienteAgente, agente.getId());
				
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				setMensajeError("Error al Guardar");
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="listarFiltradoBusquedaCliente",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/cartera/listBusquedaClientesGrid.jsp"),
		@Result(name=ERROR,location="/jsp/catalogos/fuerzaventa/agente/cartera/listBusquedaClientesGrid.jsp")
	})
	public String listarFiltradoBusquedaCliente(){
		try{			
			ClienteGenericoDTO clienteGenericoDTO = new ClienteGenericoDTO();
			clienteGenericoDTO.setNombre(clienteAgente.getCliente().getNombre());
			clienteGenericoDTO.setApellidoPaterno(clienteAgente.getCliente().getApellidoPaterno());
			clienteGenericoDTO.setApellidoMaterno(clienteAgente.getCliente().getApellidoMaterno());
			clientesList=clienteFacadeRemote.findByFilters(clienteGenericoDTO, null);			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return ERROR;
		}
		return SUCCESS;
	}
	@Action(value="obtenerClientePorAgente",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente\\.*"}),
		@Result(name="1",type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente\\.cliente.*"}),
		@Result(name="2",type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente\\.cliente.*,^clienteAgente\\.agente.*"}),
		@Result(name="3",type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente\\.cliente"}),
		@Result(name="4",type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente\\.cliente\\.*"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente.cliente\\.*"}),
		@Result(name=ERROR,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^clienteAgente.cliente\\.*"})
	})
	public String obtenerClientePorAgente(){
		try{
			if(clienteAgente!=null){ 
					if(clienteAgente.getCliente()!=null && clienteAgente.getCliente().getIdCliente()!=null){
						if(clienteAgente.getAgente()!=null && clienteAgente.getAgente().getIdAgente()!=null){
							clienteAgente = clienteAgenteService.loadByIdCheckAgent(clienteAgente);
							
							if(clienteAgente.getCliente() != null){
								test="1";
							}
							if(clienteAgente.getAgente() != null){
								test="2";
							}
							if(test!=null && !test.isEmpty()){
								return test;
							}
								
						}
					}
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public void validateGuardarCarteraClientes() {
		String domainObjectPrefix = "agente";
        //addErrors(agente, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	@Action(value="guardarCarteraClientes",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","asociado, fechaHoraActualizFormatoMostrar, usuarioActualizacion"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","asociado, fechaHoraActualizFormatoMostrar, usuarioActualizacion"})
	})
	public String guardarCarteraClientes() {
		ActualizacionAgente actualizacionAgente = new ActualizacionAgente();
		try {
			clienteAgenteService.asignarCliente(clienteAgente,agente,"ACTIVO");	
			if(agente != null){
				if(agente.getId() != null){
					agente = agenteMidasService.loadById(agente);				
				}
			}
		} catch (Exception e) {
			try {
				clienteAgenteService.desAsignarCliente(clienteAgente);
			} catch (RuntimeException e1) {
				LOG.error(e1.getMessage(), e1);
				return INPUT;
			}
			setMensajeError("Ocurrio un error al asignar el cliente");
			asociado="errorAsignacion";
			return INPUT;
		}
		try {
			clienteAgenteService.generarEndososCambioAgente(clienteAgente, agente);
		} catch (Exception e) {
			actualizacionAgente = obtenerultimamodif(agente);
			fechaHoraActualizFormatoMostrar = actualizacionAgente.getFechaHoraActualizacionString();
			usuarioActualizacion = actualizacionAgente.getUsuarioActualizacion(); 
			try {
				clienteAgenteService.desAsignarCliente(clienteAgente);
			} catch (RuntimeException e1) {
				LOG.error(e1.getMessage(), e1);
				return INPUT;
			}
			setMensajeError("Ya existe una cotización");
			asociado="errorendoso";
			return INPUT;
		}
		guardarHistoricoAgente("carteraClientes",false);
		actualizacionAgente = obtenerultimamodif(agente);
		fechaHoraActualizFormatoMostrar = actualizacionAgente.getFechaHoraActualizacionString();
		usuarioActualizacion = actualizacionAgente.getUsuarioActualizacion();
		setMensajeExito();
		asociado="exito";
		return SUCCESS;
	}
	
	public ActualizacionAgente obtenerultimamodif(Agente agente){
		ActualizacionAgente	actualizacionAgen = new ActualizacionAgente();
		try {
			actualizacionAgen = entidadHistoricoService.getLastUpdate(TipoOperacionHistorial.AGENTE,agente.getId());
		} catch (SystemException e) {
			LOG.error(e.getMessage(), e);
		} 
		return actualizacionAgen;
	}
	
	@Action(value="guardar",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"})
	})
	public String guardar() {
		try {
			//agente = agenteMidasService.save(agente);
			Long idAgente= agente.getId();
			String usuario=(getUsuarioActual()!=null)?getUsuarioActual().getNombreUsuario():"";
			//guardarHistorico(TipoOperacionHistorial.AGENTE, idAgente, UtileriasWeb.getMensajeRecurso("mx.com.afirme.midas.RecursoDeMensajes", "midas.centroOperacion.historial.accion.guardar",idAgente,usuario));
			setMensajeExito();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void validateGuardar() {
		String domainObjectPrefix = "agente";
        addErrors(agente, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	public void prepareEliminar(){
		agente=entidadService.findById(Agente.class,agente.getId());
	}
	
	@Action(value="eliminar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarContenedor","namespace","/fuerzaventa/agente","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
					"tipoAccion","${tipoAccion}","tabActiva","${tabActiva}","agente.id","${agente.id}"}),					 
					@Result(name=INPUT,type="redirectAction", params={"actionName","verDetalle","namespace","/fuerzaventa/agente","tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tipoAccion","${tipoAccion}","agente.id","${agente.id}"}),
							@Result(name=NONE ,type="redirectAction",
									params={"actionName","mostrarContenedor","namespace","/fuerzaventa/autorizacionagentes","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
									"tipoAccion","${tipoAccion}","tabActiva","${tabActiva}","agente.id","${agente.id}"})})
	public String eliminar() {
		try {
			ValorCatalogoAgentes estatus=catalogoService.obtenerElementoEspecifico("Estatus de Agente (Situacion)","INACTIVO");
			ValorCatalogoAgentes motivoEstatus=catalogoService.obtenerElementoEspecifico("Motivo de Estatus del Agente","SOLICITUD DE LA EMPRESA");		
			Long idMotivoEstatus =motivoEstatus.getId();
			agente.setTipoSituacion(estatus);
			agente.setIdMotivoEstatusAgente(idMotivoEstatus);
			agenteMidasService.disable(agente);
			guardarHistoricoAgente("Agente",true);
			setTabActiva("domicilios");
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setTipoAccion("3");
		}catch(Exception e){			
			setMensaje(MENSAJE_ERROR_ELIMINAR);				
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			setTipoAccion("3");
			return INPUT; 
		}
		if(getModuloOrigen()!=null && getModuloOrigen().equals(1)){
			return NONE;
		}else{
			return SUCCESS;
		}
	}
	@Action(value="findById",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente.*"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","agente"})
	})
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	/**
	 * ========================================================================
	 * Seccion de productos bancarios por agente
	 * ========================================================================
	 */
	
	@Action(value="guardarProducto",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"}),
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})
	})
	public String guardarProductoBancario(){
		try{
			entidadService.save(producto);
			setMensajeExitoPersonalizado("Se ha registrado el producto exitosamente!");
		}catch(Exception e){
			setMensajeError("Error al guardar el producto bancario, causado por:"+e.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}
	@Action(value="eliminarProductoBancario",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"}),
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})
	})
	public String eliminarProductoBancario(){
		if(producto!=null){
			try {
				productoService.delete(producto.getId());
				setMensajeExitoPersonalizado("Se ha eliminado el producto exitosamente!");
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}else{
			setMensajeError("Favor de elegir un producto bancario del agente para poder eliminarlo.");
		}
		return SUCCESS;
	}
	
	@Action(value="listarProductosBancariosPorAgente",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/contables/productosAgenteGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/contables/productosAgenteGrid.jsp")
	})
	public String listarProductosBancariosPorAgente(){
		if(agente!=null && agente.getId()!=null){
			
			if(tipoAccion.equalsIgnoreCase("5"))// Consulta Historico
			{				
				productosAgente=productoService.findByAgente(agente.getId(), this.getUltimaModificacion().getFechaHoraActualizacionString());
			}
			else
			{
				productosAgente=productoService.findByAgente(agente.getId());				
			}			
		}
		if(tabActiva.equals("altaInternet")){
			productosAgente=new ArrayList<ProductoBancarioAgente>();
		}
		return SUCCESS;
	}
	
//	@Action(value="cargarProductoBancario",results={
//		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productoBancarioSeleccionado.*","excludeProperties","^productoBancarioSeleccionado.banco,^productoBancarioSeleccionado\\.productoBancarioAgentes"})
//	})
	@Action(value="cargarProductoBancario",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/contables/cargarProductoBancario.jsp")//type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productoBancarioSeleccionado.*","excludeProperties","^productoBancarioSeleccionado.banco,^productoBancarioSeleccionado\\.productoBancarioAgentes"})
		})
	public String cargarProductoBancario(){
		if(productoBancarioSeleccionado!=null && productoBancarioSeleccionado.getId()!=null){
			productoBancarioSeleccionado=entidadService.findById(ProductoBancario.class,productoBancarioSeleccionado.getId());
			
		}
		return SUCCESS;
	}
	
//	@Action(value="listarProductosBancariosPorBanco",results={
//		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productosBancarios.*\\.id,^productosBancarios.*\\.descripcion","excludeProperties","^productosBancarios.*\\.productoBancarioAgentes"}),
//		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^productosBancarios.*\\.id,^productosBancarios.*\\.descripcion","excludeProperties","^productosBancarios.*\\.productoBancarioAgentes"})
//	})
	public String listarProductosBancariosPorBanco(){
		System.out.println("*****entrando al action listarProductosBancariosPorBanco.action "+new Date());
		Long idBanco=(productoBancarioSeleccionado.getBanco()!=null && productoBancarioSeleccionado.getBanco().getIdBanco()!=null)?productoBancarioSeleccionado.getBanco().getIdBanco().longValue():null;
		productosBancarios=listadoService.getListaProductosBancariosPorBanco(idBanco);
		System.out.println("*****Saliendo del action listarProductosBancariosPorBanco.action "+new Date());
		return SUCCESS;
	}
	
	public void validateGuardarProductoBancario(){
		addErrors(producto, this,"producto");
	}
	
	/**
	 * ========================================================================
	 * Seccion de Fianza de agente
	 * ========================================================================
	 */
	@Action(value="guardarFianzaAgente",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/agente",
				"tabActiva","${tabActiva}","agente.id","${agente.id}"}),					 
		@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarInfoGeneral","namespace","/fuerzaventa/agente"})}
	)
	public String guardarFianzaAgente(){
		try {
			agenteMidasService.saveDatosContablesAgente(agente);
			setMensajeExitoPersonalizado("Se han guardado los datos de la fianza para el agente.");
			tabActiva="datosExtra";
		} catch (Exception e) {
			setMensajeError(e.getMessage());
		}
		return SUCCESS;
	}
	//Alta por Internet
	@Action(value="mostrarContenedorInternet",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/altaPorInternet.jsp")
	})
	public String mostrarContenedorInternet(){
		try {
//		GrupoCatalogoAgente grupoCatalogoAgente = new GrupoCatalogoAgente();
//		grupoCatalogoAgente.setId((long)80);
		GrupoCatalogoAgente grupoCatalogoAgente = catalogoService.obtenerCatalogoPorDescripcion("Tipos de Entretenimiento");		
		ValorCatalogoAgentes valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setGrupoCatalogoAgente(grupoCatalogoAgente);
		listTipoEntretenimiento = entidadService.findByProperty(ValorCatalogoAgentes.class, "grupoCatalogoAgente.id", valorCatalogoAgentes.getGrupoCatalogoAgente().getId());
		cargarBancos();
//		cargarProductosBancarios();
		prepareMostrarContenedor();
		catalogoTipocedula=cargarCatalogo("Tipos de Cedula de Agente");
		altaPorInternet= new Agente();
		altaPorInternet.setFechaAlta(new Date());
		return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return INPUT;
		}
	}
	@Action(value="guardarDatosAgenteInternet",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarContenedorInternet","namespace","/fuerzaventa/agente","mensaje","${mensaje}","tipoMensaje", "${tipoMensaje}","agente.id","${agente.id}"}),
					
					@Result(name=INPUT,type="redirectAction", 
						 params={"actionName","mostrarContenedorInternet","namespace","/fuerzaventa/agente","mensaje","${mensaje}","tipoMensaje", "${tipoMensaje}"})}
	)
	
	public String guardarDatosAgenteInternet(){		
		try {			
			PersonaSeycosDTO personaSeycosDTO = agenteMidasService.llenarPersonaSeycos(altaPorInternet.getPersona());			
			StringBuilder rfc = new StringBuilder();			
			rfc.append(rfcSiglas);
			rfc.append(rfcFecha);
			rfc.append(rfcHomoclave);		
			personaSeycosDTO.setSituacionPersona("AC");
			personaSeycosDTO.setCodigoRFC(rfc.toString());			
			personaSeycosDTO=personaSeycosFacade.save(personaSeycosDTO);
			List<Domicilio>domicilioOficina=new ArrayList<Domicilio>();
			domicilioOficina.add(personaSeycosDTO.getDomicilios().get(2));
			personaSeycosDTO.setDomicilios(domicilioOficina);
			personaSeycosDTO=personaSeycosFacade.saveContactData(personaSeycosDTO);
			Persona persona=entidadService.getReference(Persona.class, personaSeycosDTO.getIdPersona());
			altaPorInternet.setPersona(persona);
			altaPorInternet=agenteMidasService.saveAltaPorInternet(altaPorInternet);
			agenteMidasService.llenarYGuardarListaProductosBancarios(productosBancariosAgregados,altaPorInternet);
			agenteMidasService.llenarYGuardarListaHijosAgente(hijosAgente, altaPorInternet);
			agenteMidasService.llenarYGuardarListaEntretenimientosAgente(entretenimientosAgente, altaPorInternet);
			//Se establece el agente para guardar registro historico de persona y de agente
			agente = altaPorInternet;
			guardarHistorico(TipoOperacionHistorial.PERSONA,
					agente.getPersona().getIdPersona(), "midas.persona.historial.accion.datosGenerales", TipoAccionHistorial.ALTA);
			guardarHistoricoAgente("info_general",false);
			setMensaje("El agente "+altaPorInternet.getPersona().getNombreCompleto()+" ha sido registrado y queda en espera de autorizacion");
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			return SUCCESS;
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL+" "+e);
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
	}
	
	@Action(value="agregarEntretenimientoTem",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/extras/listAgenteEntretenimientoGrid.jsp")
		})
		public String agregarEntretenimientoTemp() {
			try {
				listEntretenimiento.add(entretenimientoAgente);
			} catch (Exception e) {
				
				return INPUT;
			}
			return SUCCESS;
		}

	private void guardarHistoricoAgente(String tab,boolean unsubscribe){
		String agenteString = "agente";
		TipoOperacionHistorial tipOperacion = TipoOperacionHistorial.AGENTE;
		if (moduloOrigen!=null){
			agenteString = "autorizacionAgente";
			tipOperacion = TipoOperacionHistorial.AUTORIZACION_AGENTES;
		}
		TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
		if(unsubscribe){
			tipoAccionHistorial=TipoAccionHistorial.BAJA;
		}else if(agente!=null && agente.getId()!=null){
			tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
		}
		String keyMessage="midas."+agenteString+".historial.accion."+tab;
		guardarHistorico(tipOperacion, agente.getId(),keyMessage,tipoAccionHistorial);
	}
	/**
	 * Limpia el objeto
	 */
	public void prepareFindByClaveAgente(){
		agente=new Agente();
	}
	
	@Action(value="findByClaveAgente",results={
			@Result(name=SUCCESS,type="json",params={
					"noCache","true",
					"ignoreHierarchy","false",
					"includeProperties",
					"^agente\\.id," +
					"^agente\\.idAgente," +
					"^agente\\.idTipoAgente," +
					"^agente\\.clasificacionAgentes\\.id," +
					"^agente\\.clasificacionAgentes\\.valor," +
					"^agente\\.numeroFianza," +
					"^agente\\.fechaVencimientoFianza," +
					"^agente\\.vencimientoFianzaString," +
					"^agente\\.numeroCedula," +
					"^agente\\.fechaVencimientoCedula," +
					"^agente\\.vencimientoCedulaString," +
					"^agente\\.tipoSituacion," +
					"^agente\\.tipoSituacion\\.id," +
					"^agente\\.tipoSituacion\\.valor," +
					"^agente\\.idMotivoEstatusAgente," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto," +					
					"^agente\\.promotoria\\.descripcion, " +
					"^agente\\.idPrioridadAgente," +
					"^agente\\.tipoCedula\\.id," +
					"^agente\\.tipoCedula\\.valor," +
					"^agente\\.persona\\.nombreCompleto," +
					"^agente\\.persona\\.claveTipoPersona," +
					"^agente\\.persona\\.curp," +
					"^agente\\.persona\\.rfc," +
					"^agente\\.promotoria\\.ejecutivo\\.id," +					
					"tipoMensaje,mensaje","excludeProperties","^agente\\.cambios.*,^agente\\.afianzadora.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^agente\\.id," +
					"^agente\\.idAgente," +
					"^agente\\.idTipoAgente," +
					"^agente\\.clasificacionAgentes\\.id," +
					"^agente\\.clasificacionAgentes\\.valor," +
					"^agente\\.numeroFianza," +
					"^agente\\.fechaVencimientoFianza," +
					"^agente\\.vencimientoFianzaString," +
					"^agente\\.numeroCedula," +
					"^agente\\.fechaVencimientoCedula," +	
					"^agente\\.vencimientoCedulaString," +	
					"^agente\\.tipoSituacion," +
					"^agente\\.tipoSituacion\\.id," +
					"^agente\\.tipoSituacion\\.valor," +
					"^agente\\.idMotivoEstatusAgente," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto," +					
					"^agente\\.promotoria\\.descripcion, " +
					"^agente\\.idPrioridadAgente," +
					"^agente\\.tipoCedula\\.id," +
					"^agente\\.tipoCedula\\.valor," +
					"^agente\\.persona\\.nombreCompleto," +
					"^agente\\.persona\\.claveTipoPersona," +
					"^agente\\.persona\\.curp," +
					"^agente\\.persona\\.rfc," +
					"^agente\\.promotoria\\.ejecutivo\\.id," +	
					"tipoMensaje,mensaje","excludeProperties","^agente\\.cambios,^agente\\.afianzadora.*"})	
		})
		public String findByClaveAgente() {
			try {
				
				//agente =agenteMidasService.findByClaveAgente(agente);
				agente =agenteMidasService.loadByClave(agente);
				onSuccess();
				
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			return SUCCESS;
		}
	
	@Action(value="findByClaveAgenteSolicitudes",results={
			@Result(name=SUCCESS,type="json",params={
					"noCache","true",
					"ignoreHierarchy","false",
					"includeProperties",
					"^agente\\.id," +
					"^agente\\.idAgente," +
					"^agente\\.idTipoAgente," +
					"^agente\\.numeroFianza," +
					"^agente\\.fechaVencimientoFianza," +
					"^agente\\.vencimientoFianzaString," +
					"^agente\\.numeroCedula," +
					"^agente\\.fechaVencimientoCedula," +
					"^agente\\.vencimientoCedulaString," +
					"^agente\\.tipoSituacion," +
					"^agente\\.tipoSituacion\\.id," +
					"^agente\\.tipoSituacion\\.valor," +
					"^agente\\.idMotivoEstatusAgente," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto," +					
					"^agente\\.promotoria\\.descripcion, " +
					"^agente\\.idPrioridadAgente," +
					"^agente\\.tipoCedula\\.id," +
					"^agente\\.tipoCedula\\.valor," +
					"^agente\\.persona\\.nombreCompleto," +
					"^agente\\.persona\\.claveTipoPersona," +
					"^agente\\.persona\\.curp," +
					"^agente\\.persona\\.rfc," +
					"^agente\\.promotoria\\.ejecutivo\\.id," +					
					"tipoMensaje,mensaje","excludeProperties","^agente\\.cambios.*,^agente\\.afianzadora.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^agente\\.id," +
					"^agente\\.idAgente," +
					"^agente\\.idTipoAgente," +
					"^agente\\.numeroFianza," +
					"^agente\\.fechaVencimientoFianza," +
					"^agente\\.vencimientoFianzaString," +
					"^agente\\.numeroCedula," +
					"^agente\\.fechaVencimientoCedula," +	
					"^agente\\.vencimientoCedulaString," +	
					"^agente\\.tipoSituacion," +
					"^agente\\.tipoSituacion\\.id," +
					"^agente\\.tipoSituacion\\.valor," +
					"^agente\\.idMotivoEstatusAgente," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto," +					
					"^agente\\.promotoria\\.descripcion, " +
					"^agente\\.idPrioridadAgente," +
					"^agente\\.tipoCedula\\.id," +
					"^agente\\.tipoCedula\\.valor," +
					"^agente\\.persona\\.nombreCompleto," +
					"^agente\\.persona\\.claveTipoPersona," +
					"^agente\\.persona\\.curp," +
					"^agente\\.persona\\.rfc," +
					"^agente\\.promotoria\\.ejecutivo\\.id," +	
					"tipoMensaje,mensaje","excludeProperties","^agente\\.cambios,^agente\\.afianzadora.*"})	
		})
		public String findByClaveAgenteSolicitudes() {
			try {
				
				agente =agenteMidasService.loadByClaveSolicitudes(agente);
				onSuccess();
				
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			return SUCCESS;
		}
	
	public void prepareFindByClaveAgenteSolicitudes(){
		agente = new Agente();
	}
	
	@Action(value="findByIdSimple",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^agente\\.id," +
					"^agente\\.idAgente," +
					"^agente\\.clasificacionAgentes\\.id," +
					"^agente\\.clasificacionAgentes\\.valor," +
					"^agente\\.idTipoAgente," +
					"^agente\\.numeroFianza," +
					"^agente\\.fechaVencimientoFianza," +
					"^agente\\.vencimientoFianzaString," +
					"^agente\\.numeroCedula," +
					"^agente\\.fechaVencimientoCedula," +	
					"^agente\\.vencimientoCedulaString," +	
					"^agente\\.tipoSituacion," +
					"^agente\\.tipoSituacion\\.id," +
					"^agente\\.tipoSituacion\\.valor," +
					"^agente\\.idMotivoEstatusAgente," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto," +					
					"^agente\\.promotoria\\.descripcion, " +
					"^agente\\.idPrioridadAgente," +
					"^agente\\.tipoCedula\\.id," +
					"^agente\\.tipoCedula\\.valor," +
					"^agente\\.persona\\.nombreCompleto," +
					"^agente\\.persona\\.claveTipoPersona," +
					"^agente\\.persona\\.curp," +
					"^agente\\.persona\\.rfc," +
					"^agente\\.promotoria\\.ejecutivo\\.id," +	
					"tipoMensaje,mensaje","excludeProperties","^agente\\.cambios.*,^agente\\.afianzadora.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^agente\\.id," +
					"^agente\\.clasificacionAgentes\\.id," +
					"^agente\\.clasificacionAgentes\\.valor," +
					"^agente\\.idAgente," +
					"^agente\\.idTipoAgente," +
					"^agente\\.numeroFianza," +
					"^agente\\.fechaVencimientoFianza," +
					"^agente\\.vencimientoFianzaString," +
					"^agente\\.numeroCedula," +
					"^agente\\.fechaVencimientoCedula," +
					"^agente\\.vencimientoCedulaString," +
					"^agente\\.tipoSituacion," +
					"^agente\\.tipoSituacion\\.id," +
					"^agente\\.tipoSituacion\\.valor," +
					"^agente\\.idMotivoEstatusAgente," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion," +
					"^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable," +
					"^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto," +					
					"^agente\\.promotoria\\.descripcion, " +
					"^agente\\.idPrioridadAgente," +
					"^agente\\.tipoCedula\\.id," +
					"^agente\\.tipoCedula\\.valor," +
					"^agente\\.persona\\.nombreCompleto," +
					"^agente\\.persona\\.claveTipoPersona," +
					"^agente\\.persona\\.curp," +
					"^agente\\.persona\\.rfc," +
					"^agente\\.promotoria\\.ejecutivo\\.id," +	
					"tipoMensaje,mensaje","excludeProperties","^agente\\.cambios,^agente\\.afianzadora.*"})	
		})
		public String findByIdSimple() {
			try {
				
				agente =agenteMidasService.loadById(agente);
				onSuccess();
				
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			return SUCCESS;
		}
	
	@Action(value="mostrarClientes",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/mostrarClientes.jsp")
		})
	public String mostrarClientes(){
		try{
			if(agente!=null && agente.getId()!=null){
				clienteAgente = new ClienteAgente();
				clienteAgente.setAgente(agente);
				ValorCatalogoAgentes estatusActivo = catalogoService.obtenerElementoEspecifico("Tipos de Estatus","ACTIVO");
				Integer estatus = (int)(long) estatusActivo.getId();
				clienteAgente.setClaveEstatus(estatus);
				listaClientes = clienteAgenteService.findByFilters(clienteAgente);
			}
		}catch(Exception e){
			onError(e);
		}
		return SUCCESS;
	}
	
	/**
	 * ===========================================================
	 * Carga Masiva
	 * ===========================================================
	 */
	@Action(value="mostrarCargaMasiva",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/cargaMasivaAgente.jsp")
		})
	public String mostrarCargaMasiva(){
		return SUCCESS;
	}
	
	@Action(value="respuestaCargaMasivaAgente", results={
			@Result(name=SUCCESS,location ="/jsp/catalogos/fuerzaventa/agente/respuestaCargaMasivaAgente.jsp")	
		})
		
		public String respuestaCargaMasivaAgente(){
			return SUCCESS;
	}
	
	@Action(value="cargaMasiva", results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","respuestaCargaMasivaAgente","namespace","/fuerzaventa/agente",//"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}",
					"registrosInsertados","${registrosInsertados}","registrosNoInsertados","${registrosNoInsertados}","logErrores","${logErrores}"
				})		
	})
	
	public String cargaMasiva(){
		try {
			lista = agenteMidasService.cargaMasiva(getArchivo().toString());
			registrosInsertados = Integer.parseInt(lista.get(lista.size()-2));
			registrosNoInsertados = Integer.parseInt(lista.get(lista.size()-1));
			if (lista.size() >= 3) {
				final StringBuilder sb = new StringBuilder(lista.size() * 25);
				sb.append(logErrores);
				for (int i = 0; i < lista.size() - 2; i++) {
					sb.append("<br>").append(lista.get(i));
				}
				logErrores = sb.toString();
			}
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setMensaje("Acción realizada correctamente");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		lista.clear();
		return SUCCESS;
	}

	@Action(value="downloadLayoutCargaMasiva", 
        results={
            @Result(name=SUCCESS, type="stream", 
            params = {
            		"contentType","application/octet-stream; charset=UTF-8", 
            		"contentDisposition", "attachment; filename=Layout_agente.txt" ,
                    "inputName", "inputStream",
                    "bufferSize", "1024"
            })
        }           
    )    
    public String downloadLayoutCargaMasiva() throws Exception {
		inputStream = new ByteArrayInputStream(("NOMBRE|APELLIDO PATERNO|APELLIDO MATERNO|ESTADO CIVIL|SEXO      "
				+ "|ESTADO DE NACIMIENTO|CIUDAD DE NACIMIENTO|FECHA DE NACIMIENTO|CURP |RFC          "
				+ "|CÓDIGO POSTAL|COLONIA   |CALLE Y NÚMERO     |NÚMERO DE CEDULA|PROMOTORÍA          "
				+ "|FECHA DE AUTORIZACIÓN CEDULA|FECHA DE VENCIMIENTO CEDULA|TIPO DE CEDULA|NÚMERO DE FIANZA"
				+ "|MONTO DE FIANZA|FECHA DE AUTORIZACION FIANZA|FECHA DE VENCIMIENTO FIANZA|AFIANZADORA"
				+ "|TELEFONO DE OFICINA|TELEFONO CASA|TELEFONO CELULAR|CORREO ELECTRONICO").getBytes());
		return SUCCESS;
    }

	
	@Action(value ="BorrarProductoBancario",
			results={
				@Result(name=SUCCESS, type="json",
						params={"noCache","true","ignoreHierarchy","false","includeProperties","^regresarValores.*"})
			}
	)
	
	public String BorrarProductoBancario(){
		try {
			
//			if (idProductoBancario.getId()!=0){
//				productoService.delete(idProductoBancario.getId());
//			}
			regresarValores.add(getId_Row());
			regresarValores.add(getClaveDefaultInt());
			regresarValores.add(1);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			regresarValores.add(getId_Row());
			regresarValores.add(getClaveDefaultInt());
			regresarValores.add(0);
		}
		return SUCCESS;
	}
	
	@Action(value="findAgenteDetallado",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.idAgente,^agente\\.id,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.claveTipoPersona,^agente\\.persona\\.rfc,^agente\\.numeroFianza,^agente\\.numeroCedula,^agente\\.tipoSituacion\\.valor,^agente\\.tipoAgente,^agente\\.vencimientoFianzaString,^agente\\.vencimientoCedulaString,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,domicilioCompleto","excludeProperties","^agente\\.cambios.*,^agente\\.afianzadora.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.idAgente,^agente\\.id,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.claveTipoPersona,^agente\\.persona\\.rfc,^agente\\.numeroFianza,^agente\\.numeroCedula,^agente\\.tipoSituacion\\.valor,^agente\\.tipoAgente,^agente\\.vencimientoFianzaString,^agente\\.vencimientoCedulaString,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,domicilioCompleto","excludeProperties","^agente\\.cambios,^agente\\.afianzadora.*"})	
		})
		public String findAgenteDetallado() {
			try {
				Agente agente_obj = new Agente();
				if(agente.getId()!=null){
					agente_obj.setId(agente.getId());
					agente = new Agente();
					agente =agenteMidasService.loadById(agente_obj);
				}
				if(agente.getIdAgente()!=null){
					agente_obj.setIdAgente(agente.getIdAgente());
					agente = new Agente();
					agente = agenteMidasService.loadByClave(agente_obj);
				}
				if(agente!=null){
					List<Domicilio>domicilio=domicilioFacade.findDomiciliosActualesPorPersona(agente.getPersona().getIdPersona(), "PERS", null);
					if(!domicilio.isEmpty()&& domicilio.get(0)!=null){
						Domicilio dom=domicilio.get(0);	
						EstadoDTO estado = estadoFacadeRemote.findById(dom.getClaveEstado());
						MunicipioDTO municipio = municipioFacadeRemote.findById(dom.getClaveCiudad());
						setDomicilioCompleto(dom.getCalleNumero()+" "+dom.getNombreColonia()+" C.P. "+dom.getCodigoPostal()+" "+municipio.getMunicipalityName()+", "+estado.getStateName());
					}
					ValorCatalogoAgentes tipoAgt=entidadService.findById(ValorCatalogoAgentes.class, agente.getIdTipoAgente());
					agente.setTipoAgente(tipoAgt.getValor());
				}
				onSuccess();
				
			} catch (Exception e) {
				onError(e);				
			}
			return SUCCESS;
		}
	
	
	
	@Action(value="domicilioAgente",results={
			@Result(name=SUCCESS,type="json",params={
					"noCache","true",
					"ignoreHierarchy","false",
					"includeProperties",
					"^datosDomicilio.*," +				
					"tipoMensaje,mensaje"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false",
					"includeProperties",
					"^datosDomicilio.*," +					
					"tipoMensaje,mensaje"})	
		})
		public String domicilioAgente() {
			try {
				
				datosDomicilio = agenteMidasService.findDomicilioAgente(agente);
				onSuccess();
				
			} catch (Exception e) {
				onError(e);
			}
			return SUCCESS;
		}
	
	/**------------------------------------------------------------------------------------------------------
	 * ********************************Mostrar N Documentos Fortimax*******************************************
	 ------------------------------------------------------------------------------------------------------*/
	public void prepareMostrarNDocumentosMismo(){
		
		if(agente!=null && agente.getId()!=null){
			crearEstructuraFortimax(agente);
		}
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("nombreDocumento", getNombreDocumento());
		parametros.put("carpeta.nombreCarpeta", getNombreCarpeta());	
		List<CatalogoDocumentoFortimax>document=new ArrayList<CatalogoDocumentoFortimax>();
		document=entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
		setDocumentoFortimax(document.get(0));
	}
	
	@Action(value="mostrarNDocumentosMismo",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String mostrarNDocumentosMismo(){
		
		try {
			DocumentosAgrupados docs = new DocumentosAgrupados();
			HashMap<String,Object> properties = new HashMap<String,Object>();
			properties.put("idEntidad",agente.getId());
			properties.put("idDocumento",documentoFortimax.getId());
			List<DocumentosAgrupados>documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
			if(documentoCrear.isEmpty()){
				docs.setIdAgrupador(1L);
				docs.setIdEntidad(agente.getId());
				documentoEntidadService.saveDocumentosAgrupados(docs, 1, getNombreDocumento(), getNombreGaveta(), getNombreCarpeta());
			}else{
				docs.setIdAgrupador(documentoCrear.get(0).getIdAgrupador());
				docs.setIdEntidad(agente.getId());
			}					
			documentosFaltantes=documentoEntidadService.sincronizarGrupoDocumentos(getNombreDocumento(), getNombreGaveta(), getNombreCarpeta(), docs);
			if(documentosFaltantes==null||documentosFaltantes.equals("")){
				docs.setIdAgrupador(documentoCrear.get(0).getIdAgrupador()+1);
				docs.setIdEntidad(agente.getId());
				documentoEntidadService.saveDocumentosAgrupados(docs, 1, getNombreDocumento(), getNombreGaveta(), getNombreCarpeta());
			}
			documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
			for(DocumentosAgrupados doc:documentoCrear){
				DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
				CatalogoDocumentoFortimax catDocFortimax = new CatalogoDocumentoFortimax();
				catDocFortimax.setNombreDocumento(doc.getNombre());
				catDocFortimax.setNombreDocumentoFortimax(doc.getNombre());
				docFortimax.setCatalogoDocumentoFortimax(catDocFortimax);
				docFortimax.setExisteDocumento(doc.getExisteDocumento());
				listaDocumentosFortimax.add(docFortimax);
			}
			 
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return SUCCESS;
	}
	@Action(value="matchNDocumentosMismo",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchNDocumentosMismo() {
		
		try {
			DocumentosAgrupados docs = new DocumentosAgrupados();
			HashMap<String,Object> properties = new HashMap<String,Object>();
			HashMap<String,Object> parametros = new HashMap<String,Object>();
			if(!getNombreDocumento().equals("")){
				parametros.put("nombreDocumento", getNombreDocumento());
			}
			parametros.put("carpeta.nombreCarpeta", getNombreCarpeta());
			List<CatalogoDocumentoFortimax> documentoFortimaxList = entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
			documentoFortimax =  (documentoFortimaxList!=null)?documentoFortimaxList.get(0):null;
			for(CatalogoDocumentoFortimax fortimax:documentoFortimaxList){
				properties.put("idEntidad",agente.getId());
				properties.put("idDocumento",fortimax.getId());
				List<DocumentosAgrupados>documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
				if(documentoCrear.isEmpty()){
	//				docs.setIdAgrupador(1L);
					docs.setIdEntidad(agente.getId());
				}else{
	//				docs.setIdAgrupador(documentoCrear.get(0).getIdAgrupador());
					docs.setIdEntidad(agente.getId());
				}					
				documentosFaltantes=documentoEntidadService.sincronizarGrupoDocumentos(getNombreDocumento(), getNombreGaveta(), getNombreCarpeta(), docs);
				documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
				for(DocumentosAgrupados doc:documentoCrear){
					DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
					CatalogoDocumentoFortimax catDocFortimax = new CatalogoDocumentoFortimax();
					catDocFortimax.setNombreDocumento(doc.getNombre());
					catDocFortimax.setNombreDocumentoFortimax(doc.getNombre());
					docFortimax.setCatalogoDocumentoFortimax(catDocFortimax);
					docFortimax.setExisteDocumento(doc.getExisteDocumento());
					listaDocumentosFortimax.add(docFortimax);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			setMensajeError("Error al Guardar");
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="mostrarDocGenerico",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
	public String mostrarDocGenerico(){
		try{
			agente=agenteMidasService.loadById(agente);		
			PersonaSeycosDTO personaSeycos = personaSeycosFacade.findById(agente.getPersona().getIdPersona());			
			Persona per = new Persona();
			per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
			per.setIdPersona(personaSeycos.getIdPersona());
			agente.setPersona(per);
			String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());
					
			
			if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(agente.getId(), getNombreGaveta(),getNombreCarpeta())){
				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpeta(getNombreGaveta(),getNombreCarpeta());
					for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
						if(doc!=null){
							String []respDoc=fortimaxService.generateDocument(agente.getId(), getNombreGaveta(), doc.getNombreDocumentoFortimax()+"_"+agente.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
							if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
								DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
								docFortimax.setCatalogoDocumentoFortimax(doc);
								docFortimax.setExisteDocumento(0);
								docFortimax.setIdRegistro(agente.getId());
								documentoEntidadService.save(docFortimax);
							}
						}
					}			
			}
			else{
				documentoEntidadService.sincronizarDocumentos(agente.getId(), "AGENTES", getNombreCarpeta());
			}
			listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(agente.getId(),getNombreGaveta(), getNombreCarpeta());
	} catch (Exception e) {
		LOG.error(e.getMessage(), e);
	}
		return SUCCESS;
	}
	
	@Action(value="matchDocumentosGenerico",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchDocumentosGenerico() {
			try {				
				documentoEntidadService.sincronizarDocumentos(agente.getId(), getNombreGaveta(), getNombreCarpeta());
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(agente.getId(), getNombreGaveta(), getNombreCarpeta());
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				return INPUT;
			}
			return SUCCESS;
		}
	
	
	public void crearEstructuraFortimax(Agente agente){
		try{
			agente=agenteMidasService.loadById(agente);
			PersonaSeycosDTO personaSeycos = personaSeycosFacade.findById(agente.getPersona().getIdPersona());			
			Persona per = new Persona();
			per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
			per.setIdPersona(personaSeycos.getIdPersona());
			agente.setPersona(per);
			String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());			
			if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(agente.getId(), "AGENTES","ALTA DE AGENTES")){
	//			List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorAplicacionTipoPersona("AGENTES", per.getClaveTipoPersona());
				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpetaConTipoPersona("AGENTES", "ALTA DE AGENTES", per.getClaveTipoPersona());
					for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
						if(doc!=null){
							String []respDoc=fortimaxService.generateDocument(agente.getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+agente.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
							if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
								DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
								docFortimax.setCatalogoDocumentoFortimax(doc);
								docFortimax.setExisteDocumento(0);
								docFortimax.setIdRegistro(agente.getId());
								documentoEntidadService.save(docFortimax);
							}
						}
					}
					LogDeMidasWeb.log("se creo la estructura de las carpetas en fortimax..." + this, Level.WARNING, null);
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
	}
	
	@Action(value="mostrarMotEstatusXSituacionDelAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^catalogoMotivoEstatus.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^catalogoMotivoEstatus.*"})
		})
	public String mostrarMotEstatusXSituacionDelAgente(){
		try {
			if("PENDIENTE POR AUTORIZAR".equals(situacionAgenteString)){				
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","ALTA");
			}
			if("AUTORIZADO".equals(situacionAgenteString)){
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","REGULAR");
			}
			if("RECHAZADO".equals(situacionAgenteString)){
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","NUEVO","INFORMACION INCOMPLETA");
			}
			if("BAJA".equals(situacionAgenteString)){
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","SOLICITUD DEL AGENTE","ALTA SINIESTRALIDAD","BAJA PRODUCTIVIDAD","AUDITORIA","CEDULA VENCIDA","SOLICITUD DE AUTORIDAD","DEFUNCIÓN");
			}
			if("INACTIVO".equals(situacionAgenteString)){
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","SOLICITUD DE LA EMPRESA");
			}
			if("RECHAZADO CON EMISION".equals(situacionAgenteString)){
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","SIN MOTIVO");
			}
			if("SUSPENDIDO".equals(situacionAgenteString)){
				catalogoMotivoEstatus=catalogoService.obtenerElementosEspecificosPorCatalogo("Motivo de Estatus del Agente","AUDITORIA - ACTO IRREGULAR","AUDITORIA - REVISION PREVENTIVA","CEDULA VENCIDA","BAJA PRODUCTIVIDAD","DEFUNCIÓN","ALTA SINIESTRALIDAD");
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}	
		return SUCCESS;
	}
	
	/***Sets and gets****************************************************/
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Autowired
	@Qualifier("bancoFacadeRemoteEJB")
	public void setBancoFacadeRemote(BancoFacadeRemote bancoFacadeRemote) {
		this.bancoFacadeRemote = bancoFacadeRemote;
	}
	
	@Autowired
	@Qualifier("productoBancarioAgenteServiecEJB")
	public void setProductoService(ProductoBancarioAgenteService productoService) {
		this.productoService = productoService;
	}
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@Autowired
	@Qualifier("clienteAgenteServiceEJB")
	public void setClienteAgenteService(ClienteAgenteService clienteAgenteService) {
		this.clienteAgenteService = clienteAgenteService;
	}
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}
	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}
	public Agente getAgente() {
		return agente;
	}
	
	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Agente getFiltroAgente() {
		return filtroAgente;
	}

	public void setFiltroAgente(Agente filtroAgente) {
		this.filtroAgente = filtroAgente;
	}

	public List<AgenteView> getListaAgente() {
		return listaAgente;
	}

	public void setListaAgente(List<AgenteView> listaAgente) {
		this.listaAgente = listaAgente;
	}

	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	
	public String getTabActiva() {
		return tabActiva;
	}
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	public Map<Long, String> getGerenciaList() {
		return gerenciaList;
	}
	public void setGerenciaList(Map<Long, String> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}	
	public Long getIdGerencia() {
		return idGerencia;
	}
	public void setIdGerencia(Long idGerencia) {
		this.idGerencia = idGerencia;
	}
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}
	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}	
	public Map<Long, String> getEjecutivoList() {
		return ejecutivoList;
	}
	public void setEjecutivoList(Map<Long, String> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}
	public Map<Long, String> getPromotoriaList() {
		return promotoriaList;
	}
	public void setPromotoriaList(Map<Long, String> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}
	
	public List<ValorCatalogoAgentes> getCatalogoMotivoEstatus() {
		return catalogoMotivoEstatus;
	}
	public void setCatalogoMotivoEstatus(
			List<ValorCatalogoAgentes> catalogoMotivoEstatus) {
		this.catalogoMotivoEstatus = catalogoMotivoEstatus;
	}
	public List<ValorCatalogoAgentes> getCatalogoPrioridad() {
		return catalogoPrioridad;
	}
	public void setCatalogoPrioridad(List<ValorCatalogoAgentes> catalogoPrioridad) {
		this.catalogoPrioridad = catalogoPrioridad;
	}
	public List<ValorCatalogoAgentes> getCatalogoTipocedula() {
		return catalogoTipocedula;
	}
	public void setCatalogoTipocedula(List<ValorCatalogoAgentes> catalogoTipocedula) {
		this.catalogoTipocedula = catalogoTipocedula;
	}
	public List<ValorCatalogoAgentes> getCatalogoTipoAgente() {
		return catalogoTipoAgente;
	}
	public void setCatalogoTipoAgente(List<ValorCatalogoAgentes> catalogoTipoAgente) {
		this.catalogoTipoAgente = catalogoTipoAgente;
	}
	
	public List<ProductoBancario> getProductosBancarios() {
		return productosBancarios;
	}
	public void setProductosBancarios(List<ProductoBancario> productosBancarios) {
		this.productosBancarios = productosBancarios;
	}
	public List<BancoDTO> getListaBancos() {
		return listaBancos;
	}
	public void setListaBancos(List<BancoDTO> listaBancos) {
		this.listaBancos = listaBancos;
	}
	
	public ProductoBancarioAgente getProducto() {
		return producto;
	}
	public void setProducto(ProductoBancarioAgente producto) {
		this.producto = producto;
	}
	
	public List<ProductoBancarioAgente> getProductosAgente() {
		return productosAgente;
	}
	public void setProductosAgente(List<ProductoBancarioAgente> productosAgente) {
		this.productosAgente = productosAgente;
	}
	public String getClaveDefault() {
		return claveDefault;
	}
	public void setClaveDefault(String claveDefault) {
		this.claveDefault = claveDefault;
	}
	public ProductoBancario getProductoBancarioSeleccionado() {
		return productoBancarioSeleccionado;
	}
	public void setProductoBancarioSeleccionado(
			ProductoBancario productoBancarioSeleccionado) {
		this.productoBancarioSeleccionado = productoBancarioSeleccionado;
	}
	
	public List<ValorCatalogoAgentes> getCatalogoTipoSituacion() {
		return catalogoTipoSituacion;
	}
	public void setCatalogoTipoSituacion(
			List<ValorCatalogoAgentes> catalogoTipoSituacion) {
		this.catalogoTipoSituacion = catalogoTipoSituacion;
	}
	
	public List<ClienteAgente> getListaCarteraCliente() {
		return listaCarteraCliente;
	}

	public void setListaCarteraCliente(List<ClienteAgente> listaCarteraCliente) {
		this.listaCarteraCliente = listaCarteraCliente;
	}

	public ClienteAgente getClienteAgente() {
		return clienteAgente;
	}

	public void setClienteAgente(ClienteAgente clienteAgente) {
		this.clienteAgente = clienteAgente;
	}
	public List<ClienteJPA> getListClientes() {
		return listClientes;
	}
	public void setListClientes(List<ClienteJPA> listClientes) {
		this.listClientes = listClientes;
	}
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
	public HijoAgente getHijoAgente() {
		return hijoAgente;
	}
	public void setHijoAgente(HijoAgente hijoAgente) {
		this.hijoAgente = hijoAgente;
	}
	public void setListHijoAgentes(List<HijoAgente> listHijoAgentes) {
		this.listHijoAgentes = listHijoAgentes;
	}
	public List<HijoAgente> getListHijoAgentes() {
		return listHijoAgentes;
	}
	public void setListEntretenimiento(List<EntretenimientoAgente> listEntretenimiento) {
		this.listEntretenimiento = listEntretenimiento;
	}
	public List<EntretenimientoAgente> getListEntretenimiento() {
		return listEntretenimiento;
	}
	public EntretenimientoAgente getEntretenimientoAgente() {
		return entretenimientoAgente;
	}
	public void setEntretenimientoAgente(EntretenimientoAgente entretenimientoAgente) {
		this.entretenimientoAgente = entretenimientoAgente;
	}
	public void setListTipoEntretenimiento(List<ValorCatalogoAgentes> listTipoEntretenimiento) {
		this.listTipoEntretenimiento = listTipoEntretenimiento;
	}
	public List<ValorCatalogoAgentes> getListTipoEntretenimiento() {
		return listTipoEntretenimiento;
	}
	public String getUrlIfimax() {
		return urlIfimax;
	}
	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}
	public List<TipoDocumentoAgente> getCatalogoTipoDocumentosAgente() {
		return catalogoTipoDocumentosAgente;
	}
	public void setCatalogoTipoDocumentosAgente(
			List<TipoDocumentoAgente> catalogoTipoDocumentosAgente) {
		this.catalogoTipoDocumentosAgente = catalogoTipoDocumentosAgente;
	}
	public List<DocumentoAgente> getCatalogoDocumentosAgente() {
		return catalogoDocumentosAgente;
	}
	public void setCatalogoDocumentosAgente(
			List<DocumentoAgente> catalogoDocumentosAgente) {
		this.catalogoDocumentosAgente = catalogoDocumentosAgente;
	}
	public List<DocumentoAgente> getListaAuxDocumentosAgente() {
		return listaAuxDocumentosAgente;
	}
	public void setListaAuxDocumentosAgente(
			List<DocumentoAgente> listaAuxDocumentosAgente) {
		this.listaAuxDocumentosAgente = listaAuxDocumentosAgente;
	}
	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}
	public void setListaDocumentosFortimax(
			List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}
	public String getDocumentosFaltantes() {
		return documentosFaltantes;
	}
	public void setDocumentosFaltantes(String documentosFaltantes) {
		this.documentosFaltantes = documentosFaltantes;
	}
	public InputStream getContratoInputStream() {
		return contratoInputStream;
	}
	public void setContratoInputStream(InputStream contratoInputStream) {
		this.contratoInputStream = contratoInputStream;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}	
	
	public List<SectorDTO> getSectorList() {
		return sectorList;
	}

	public void setSectorList(List<SectorDTO> sectorList) {
		this.sectorList = sectorList;
	}
	@Autowired
	@Qualifier("sectorFacadeRemoteEJB")
	public void setSectorFacadeRemote(SectorFacadeRemote sectorFacadeRemote) {
		this.sectorFacadeRemote = sectorFacadeRemote;
	}
	public List<EstadoCivilDTO> getEstadosCiviles() {
		return estadosCiviles;
	}

	public void setEstadosCiviles(List<EstadoCivilDTO> estadosCiviles) {
		this.estadosCiviles = estadosCiviles;
	}
	@Autowired
	@Qualifier("estadoCivilFacadeRemoteEJB")
	public void setEstadoCivilFacade(EstadoCivilFacadeRemote estadoCivilFacade) {
		this.estadoCivilFacade = estadoCivilFacade;
	}
	@Autowired
	@Qualifier("domicilioFacadeRemoteEJB")
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
	@Autowired
	@Qualifier("estadoFacadeRemoteEJB")
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}
	@Autowired
	@Qualifier("municipioFacadeRemoteEJB")
	public void setMunicipioFacadeRemote(MunicipioFacadeRemote municipioFacadeRemote) {
		this.municipioFacadeRemote = municipioFacadeRemote;
	}
	public Agente getAltaPorInternet() {
		return altaPorInternet;
	}
	public void setAltaPorInternet(Agente altaPorInternet) {
		this.altaPorInternet = altaPorInternet;
	}
/*	public PersonaSeycosDTO getPersona() {
		return persona;
	}
	public void setPersona(PersonaSeycosDTO persona) {
		this.persona = persona;
	}*/
	public String getRfcSiglas() {
		return rfcSiglas;
	}
	public void setRfcSiglas(String rfcSiglas) {
		this.rfcSiglas = rfcSiglas;
	}
	public String getRfcFecha() {
		return rfcFecha;
	}
	public void setRfcFecha(String rfcFecha) {
		this.rfcFecha = rfcFecha;
	}
	public String getRfcHomoclave() {
		return rfcHomoclave;
	}
	public void setRfcHomoclave(String rfcHomoclave) {
		this.rfcHomoclave = rfcHomoclave;
	}
	public String getProductosBancariosAgregados() {
		return productosBancariosAgregados;
	}
	public void setProductosBancariosAgregados(String productosBancariosAgregados) {
		this.productosBancariosAgregados = productosBancariosAgregados;
	}
	public String getHijosAgente() {
		return hijosAgente;
	}
	public void setHijosAgente(String hijosAgente) {
		this.hijosAgente = hijosAgente;
	}
	public String getEntretenimientosAgente() {
		return entretenimientosAgente;
	}
	public void setEntretenimientosAgente(String entretenimientosAgente) {
		this.entretenimientosAgente = entretenimientosAgente;
	}
	public ClienteJPA getClienteJPA() {
		return clienteJPA;
	}
	public void setClienteJPA(ClienteJPA clienteJPA) {
		this.clienteJPA = clienteJPA;
	}
	public List<ClienteGenericoDTO> getClientesList() {
		return clientesList;
	}
	public void setClientesList(List<ClienteGenericoDTO> clientesList) {
		this.clientesList = clientesList;
	}
	public PersonaSeycosDTO getPersonaSeycosDTO() {
		return personaSeycosDTO;
	}
	public void setPersonaSeycosDTO(PersonaSeycosDTO personaSeycosDTO) {
		this.personaSeycosDTO = personaSeycosDTO;
	}
	public String getAsociado() {
		return asociado;
	}
	public void setAsociado(String asociado) {
		this.asociado = asociado;
	}
	public Integer getModuloOrigen() {
		return moduloOrigen;
	}
	public void setModuloOrigen(Integer moduloOrigen) {
		this.moduloOrigen = moduloOrigen;
	}
	public Long getIdEstatusRelacion() {
		return idEstatusRelacion;
	}
	public void setIdEstatusRelacion(Long idEstatusRelacion) {
		this.idEstatusRelacion = idEstatusRelacion;
	}
	public List<ClienteAgente> getListaClientes() {
		return listaClientes;
	}
	public void setListaClientes(List<ClienteAgente> listaClientes) {
		this.listaClientes = listaClientes;
	}
	public String getTipoReturn() {
		return tipoReturn;
	}
	public void setTipoReturn(String tipoReturn) {
		this.tipoReturn = tipoReturn;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getArchivotxt() {
		return archivotxt;
	}
	public void setArchivotxt(String archivotxt) {
		this.archivotxt = archivotxt;
	}
	public Agente getCargaMasiva() {
		return cargaMasiva;
	}
	public void setCargaMasiva(Agente cargaMasiva) {
		this.cargaMasiva = cargaMasiva;
	}
	public File getArchivo() {
		return archivo;
	}
	public void setArchivo(File archivo) {
		this.archivo = archivo;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public int getRegistrosInsertados() {
		return registrosInsertados;
	}
	public void setRegistrosInsertados(int registrosInsertados) {
		this.registrosInsertados = registrosInsertados;
	}
	public int getRegistrosNoInsertados() {
		return registrosNoInsertados;
	}
	public void setRegistrosNoInsertados(int registrosNoInsertados) {
		this.registrosNoInsertados = registrosNoInsertados;
	}
	public String getCarpetaDocumento() {
		return carpetaDocumento;
	}	
	public ProductoBancarioAgente getIdProductoBancario() {
		return idProductoBancario;
	}
	public void setIdProductoBancario(ProductoBancarioAgente idProductoBancario) {
		this.idProductoBancario = idProductoBancario;
	}
	public int getId_Row() {
		return id_Row;
	}
	public void setId_Row(int id_Row) {
		this.id_Row = id_Row;
	}
	public int getClaveDefaultInt() {
		return claveDefaultInt;
	}
	public void setClaveDefaultInt(int claveDefaultInt) {
		this.claveDefaultInt = claveDefaultInt;
	}
	public List<Integer> getRegresarValores() {
		return regresarValores;
	}
	public void setRegresarValores(List<Integer> regresarValores) {
		this.regresarValores = regresarValores;
	}
	public Long getTipoOper() {
		return tipoOper;
	}
	public void setTipoOper(Long tipoOper) {
		this.tipoOper = tipoOper;
	}
	public Long getTipoOperac() {
		return tipoOperac;
	}
	public void setTipoOperac(Long tipoOperac) {
		this.tipoOperac = tipoOperac;
	}
	public String getLogErrores() {
		return logErrores;
	}
	public void setLogErrores(String logErrores) {
		this.logErrores = logErrores;
	}
	public Long getTipoAccionAgente() {
		return tipoAccionAgente;
	}
	public void setTipoAccionAgente(Long tipoAccionAgente) {
		this.tipoAccionAgente = tipoAccionAgente;
	}
	public String getDomicilioCompleto() {
		return domicilioCompleto;
	}
	public void setDomicilioCompleto(String domicilioCompleto) {
		this.domicilioCompleto = domicilioCompleto;
	}
	public DomicilioView getDatosDomicilio() {
		return datosDomicilio;
	}
	public void setDatosDomicilio(DomicilioView datosDomicilio) {
		this.datosDomicilio = datosDomicilio;
	}
	public List<DocumentosAgrupados> getListDocAgrupado() {
		return listDocAgrupado;
	}
	public void setListDocAgrupado(List<DocumentosAgrupados> listDocAgrupado) {
		this.listDocAgrupado = listDocAgrupado;
	}
	public CatalogoDocumentoFortimax getDocumentoFortimax() {
		return documentoFortimax;
	}
	public void setDocumentoFortimax(CatalogoDocumentoFortimax documentoFortimax) {
		this.documentoFortimax = documentoFortimax;
	}
	public String getNombreCarpeta() {
		return nombreCarpeta;
	}
	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}
	public String getNombreDocumento() {
		return nombreDocumento;
	}
	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}
	public String getNombreGaveta() {
		return nombreGaveta;
	}
	public void setNombreGaveta(String nombreGaveta) {
		this.nombreGaveta = nombreGaveta;
	}
	public String getModalFortimax() {
		return modalFortimax;
	}
	public void setModalFortimax(String modalFortimax) {
		this.modalFortimax = modalFortimax;
	}
	public List<AgenteCarteraClientesView> getListaCarteraClienteGrid() {
		return listaCarteraClienteGrid;
	}
	public void setListaCarteraClienteGrid(
			List<AgenteCarteraClientesView> listaCarteraClienteGrid) {
		this.listaCarteraClienteGrid = listaCarteraClienteGrid;
	}
	public String getFechaHoraActualizFormatoMostrar() {
		return fechaHoraActualizFormatoMostrar;
	}
	public void setFechaHoraActualizFormatoMostrar(
			String fechaHoraActualizFormatoMostrar) {
		this.fechaHoraActualizFormatoMostrar = fechaHoraActualizFormatoMostrar;
	}
	public String getUsuarioActualizacion() {
		return usuarioActualizacion;
	}
	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}	
	
	
	public String getSituacionAgenteString() {
		return situacionAgenteString;
	}

	public void setSituacionAgenteString(String situacionAgenteString) {
		this.situacionAgenteString = situacionAgenteString;
	}
	public static List<ValorCatalogoAgentes> getCatalogoClasificacionAgentes() {
		return catalogoClasificacionAgentes;
	}
	public void setCatalogoClasificacionAgentes(List<ValorCatalogoAgentes> catalogoClasificacionAgentes) {
		this.catalogoClasificacionAgentes = catalogoClasificacionAgentes;
	}
	
	
	public Boolean getClaveAgenteSoloLectura() {
		return claveAgenteSoloLectura;
	}
	public void setClaveAgenteSoloLectura(Boolean claveAgenteSoloLectura) {
		this.claveAgenteSoloLectura = claveAgenteSoloLectura;
	}
		
	public boolean isAgentePromotor() {
		return agentePromotor;
	}
	
	public void setAgentePromotor(boolean agentePromotor) {
		this.agentePromotor = agentePromotor;
	}
	

}
