package mx.com.afirme.midas2.action.catalogos.paquete;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.PaqueteService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class PaqueteAction extends CatalogoAction implements Preparable {

	public PaqueteAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Paquete paquete;

	private List<Paquete> paqueteList;
	
	private TipoAccionDTO catalogoTipoAccionDTO;		
	
	private PaqueteService paqueteService;
	
	private ParametroGeneralService parametroGeneralService;

	private List<ParametroGeneralDTO> listaTipoDeSeguro = new ArrayList<ParametroGeneralDTO>();

	private String duplicado;
	
	private EntidadService entidadService;
	
	private String accion;

	private static final String TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL = "Tipo de Seguro";

	
	public void validateGuardar() {
		paquete.setId(getId());
		duplicado="false";
		addErrors(paquete, NewItemChecks.class, this, "paquete");
		if (!this.hasErrors()){	
			if (accion.equals("1")){
				Paquete paqueteExistente = null;
				paqueteExistente = entidadService.findById(Paquete.class, paquete.getId());
				if(paqueteExistente != null){
					addFieldError("id", getText("error.duplicado.campo"));		
					duplicado="true";
				}
			}
		}
		
	}

	public void validateEditar() {
		setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		addErrors(paquete, EditItemChecks.class, this, "paquete");
	}


	public void prepare() throws Exception {
		setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		if (getId() != null) {
			paquete = paqueteService.prepareVerDetalle(getId());
		}
	}
	
	


	@Override
	public String guardar() {
		if(paqueteService.guardar(paquete)){
			paquete = new Paquete();
			setMensajeExito();
		}
		setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		entidadService.remove(paquete);
		paquete = new Paquete();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String listar() {
		paqueteList = paqueteService.listAll();
		setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		return SUCCESS;
	}

	@Override
	public String listarFiltrado() {
		paqueteList = paqueteService.findByFilters(paquete);
		setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		return SUCCESS;
	}
	
	public void prepareVerDetalle(){
		if(getId() != null){
			paquete = paqueteService.prepareVerDetalle(getId());
			setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		}
	}

	@Override
	public String verDetalle() {
		setListaTipoDeSeguro(parametroGeneralService.listByGroup(TIPO_DE_SEGURO_GRUPO_PARAMETRO_GRAL));
		return SUCCESS;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Paquete getPaquete() {
		return paquete;
	}

	public void setPaquete(Paquete paquete) {
		this.paquete = paquete;
	}

	public List<Paquete> getPaqueteList() {
		return paqueteList;
	}

	public void setPaqueteList(List<Paquete> paqueteList) {
		this.paqueteList = paqueteList;
	}
	
	public void setDuplicado(String duplicado) {
		this.duplicado = duplicado;
	}

	public String getDuplicado() {
		return duplicado;
	}	

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;

	}
	
	@Autowired
	@Qualifier("paqueteEJB")
	public void setPaqueteService(PaqueteService paqueteService) {
		this.paqueteService = paqueteService;
	}
	
	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	public List<ParametroGeneralDTO> getListaTipoDeSeguro() {
		return listaTipoDeSeguro;
	}

	public void setListaTipoDeSeguro(List<ParametroGeneralDTO> listaTipoDeSeguro) {
		this.listaTipoDeSeguro = listaTipoDeSeguro;
	}
	
	@Autowired
	@Qualifier("parametroGeneralServiceEJB")
	public void setParametroGeneralService(ParametroGeneralService parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}
}
