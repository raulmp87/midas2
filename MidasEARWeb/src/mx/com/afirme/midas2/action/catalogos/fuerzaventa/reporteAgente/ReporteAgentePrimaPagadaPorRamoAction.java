package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reportePrimaPagadaPorRamo")
@Component
@Scope("prototype")
public class ReporteAgentePrimaPagadaPorRamoAction extends
		ReporteAgenteBaseAction implements ReportMethods, Preparable {

	private static final long serialVersionUID = 1L;
	private ConfigBonosService configBonosService;
	private Date fechaInicio;
	private Date fechaFinal;
	private Date fechaInicial;
	private List<AgenteView> listaAgentes;
	private List<Agente> agenteList;
	private String labelFechaInicio = "Fecha Corte Inicio";
	private String labelFechaFin = "Fecha Corte Fin";
	
	@Override
	@Action(value = "mostrarFiltros", results = { @Result(name = SUCCESS, location = "/jsp/reportesAgentes/reportePrimaPagadaPorRamo.jsp") })
	public String mostrarFiltros() {
		final CentroOperacion filtro = new CentroOperacion();
//		setCentroOperacionList(centroOperacionService.findByFilters(filtro));
		setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
		setGerenciasSeleccionadas(new LinkedList<Gerencia>());
		setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
		setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
		setClasificacionAgente(new LinkedList<ValorCatalogoAgentes>());
		
		return SUCCESS;
	}

	@Override
	@Action(value = "exportarToPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	public void prepareExportarToExcel() {
		if (listaAgentes != null) {
			agenteList = new ArrayList<Agente>();
			for (AgenteView agenteView : listaAgentes) {
				Agente agente = new Agente();
				agente.setId(agenteView.getId());
				agenteList.add(agente);
			}
		}
		
		if (getIdCentroOperacion() != null) {
			setCentroOperacionesSeleccionados(new LinkedList<CentroOperacion>());
			getCentroOperacionesSeleccionados().add(new CentroOperacion(getIdCentroOperacion().longValue()));
		}
		if (getIdEjecutivo() != null) {
			setEjecutivosSeleccionados(new LinkedList<Ejecutivo>());
			getEjecutivosSeleccionados().add(new Ejecutivo(getIdEjecutivo().longValue()));
		}
		
		if (getIdGerencia() != null) {
			setGerenciasSeleccionadas(new LinkedList<Gerencia>());
			getGerenciasSeleccionadas().add(new Gerencia(getIdGerencia().longValue()));
		}
		if (getIdPromotoria() != null) {
			setPromotoriasSeleccionadas(new LinkedList<Promotoria>());
			getPromotoriasSeleccionadas().add(new Promotoria(getIdPromotoria().longValue()));
		}
	}

	@Override
	@Action(value = "exportarToExcel", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToExcel() {
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
					.imprimirReporteAgentePrimaPagadaPorRamoToExcel(
							agenteList, fechaInicial, fechaFinal,
						    getCentroOperacionesSeleccionados(), 
							getEjecutivosSeleccionados(),
							getGerenciasSeleccionadas(),
							getPromotoriasSeleccionadas(),
							getTipoReporte(), getLocale(), "xlsx");		
			if (transporte != null && transporte.getByteArray() != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje(EMPTY_RESULT);
				return INPUT;
			}
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				setFileName("reporteAgentePrimaPagadaPorRamoGeneral.xlsx");
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(EMPTY_RESULT);
			return INPUT;
		}
		return SUCCESS;
	}

	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<AgenteView> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteView> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public List<Agente> getAgenteList() {
		return agenteList;
	}
	
	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public String getLabelFechaInicio() {
		return labelFechaInicio;
	}

	public void setLabelFechaInicio(String labelFechaInicio) {
		this.labelFechaInicio = labelFechaInicio;
	}

	public String getLabelFechaFin() {
		return labelFechaFin;
	}

	public void setLabelFechaFin(String labelFechaFin) {
		this.labelFechaFin = labelFechaFin;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	
}
