package mx.com.afirme.midas2.action.catalogos.fuerzaventa.recepcionDocumentos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.recepcionDocumentos.RecepcionDocumentos;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.recepcionDocumentos.RecepcionDocumentosService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/recepcionDocs")
public class RecepcionDocumentosAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7360809103130160283L;
	
	private Agente agente;
	private Integer mesInicio;
	private Integer mesFin;
	private RecepcionDocumentos recepDocs;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private FortimaxService fortimaxService;
	private EntidadService entidadService;
	private String urlIfimax;
	private String documentosFaltantes;
	private ValorCatalogoAgentesService catalogoService;
	private RecepcionDocumentosService recepDocsService;
	private CatalogoDocumentoFortimax documentoFortimax;
	private List<ValorCatalogoAgentes>motivosRechazo=new ArrayList<ValorCatalogoAgentes>();
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private List<DocumentosAgrupados>listDocAgrupado = new ArrayList<DocumentosAgrupados>();
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private AgenteMidasService agenteMidasService;
	private List<EntregoDocumentosView> listaFacturasMeses =  new ArrayList<EntregoDocumentosView>();
	private List<ValorCatalogoAgentes> listaFacturasMesesEntregadas =  new ArrayList<ValorCatalogoAgentes>();
	private Long anio;
	private String nombreDocumento;
	
	@Action(value="verValidacionManual",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/recepcionDocumentos/validacionManual.jsp")
		})
		public String verValidacionManual() {
		agente = new Agente();
			return SUCCESS;
		}

	public void prepareMostrarDocumentos(){						
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("nombreDocumento", "RECIBO");
		parametros.put("carpeta.nombreCarpeta", "RECIBOS");	
		List<CatalogoDocumentoFortimax>document=new ArrayList<CatalogoDocumentoFortimax>();
		document=entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
		setDocumentoFortimax(document.get(0));
	}
	
	@Action(value="mostrarDocumentos",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/recepcionDocumentos/documentosFortimax.jsp")
		})
		public String mostrarDocumentos(){
			try {			
				DocumentosAgrupados docs = new DocumentosAgrupados();
				HashMap<String,Object> properties = new HashMap<String,Object>();
				properties.put("idEntidad",agente.getId());
				properties.put("idDocumento",documentoFortimax.getId());
//				List<DocumentosAgrupados>
				listDocAgrupado=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
				if(listDocAgrupado.isEmpty()){
					docs.setIdAgrupador(1L);
					docs.setIdEntidad(agente.getId());
				}else{
					docs.setIdAgrupador(listDocAgrupado.get(0).getIdAgrupador()+1);
					docs.setIdEntidad(agente.getId());
				}				
				String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());	
				 documentoEntidadService.saveDocumentosAgrupados(docs, 1, "RECIBO", "AGENTES", "RECIBOS");
			} catch (Exception e) {
				e.printStackTrace();
			}
	return SUCCESS;
	}
	
	@Action(value="generarLigaIfimax",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp"),
			@Result(name="errorExpediente",location=ERROR)
		})
		public String generarLigaIfimax(){
		String []resp= new String[3];
		
		
		try {
			String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());						
			String[] respGenerate=fortimaxService.generateDocument(agente.getId(),"AGENTES",nombreDocumento,"05 RECIBOS DE HONORARIOS/FACTURA");	
			System.out.println(respGenerate[0]);
			//List<CatalogoDocumentoFortimax> docsCarpeta=documentoCarpetaService.obtenerDocumentosRequeridosPorCarpetaConTipoPersona("AGENTES", "RECIBOS", agente.getPersona().getClaveTipoPersona());				
			resp=fortimaxService.generateLinkToDocument(agente.getId(),"AGENTES", "");
			String[] existe = fortimaxService.getDocumentFortimax(agente.getId(),"AGENTES");
			urlIfimax=resp[0];	
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(resp[2].contains("No existe el Expediente")){
			setMensaje(resp[2]);
			return "errorExpediente";
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
		}	
	
	@Action(value="guardarDocumentos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"})
		})
		public String guardarDocumentos() {
			try {						
				DocumentosAgrupados docs = new DocumentosAgrupados();
				HashMap<String,Object> properties = new HashMap<String,Object>();
				properties.put("idEntidad",agente.getId());
				properties.put("idDocumento",documentoFortimax.getId());
				List<DocumentosAgrupados>documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
				if(documentoCrear.isEmpty()){
					docs.setIdAgrupador(1L);
					docs.setIdEntidad(agente.getId());
				}else{
					docs.setIdAgrupador(documentoCrear.get(0).getIdAgrupador());
					docs.setIdEntidad(agente.getId());
				}					
				documentosFaltantes=documentoEntidadService.sincronizarGrupoDocumentos("RECIBO", "AGENTES", "RECIBOS", docs);
				if(documentosFaltantes==null||documentosFaltantes.equals("")){
					recepDocsService.actualizarStatusRecibos(docs, mesInicio, mesFin);
				}
			} catch (Exception e) {
				e.printStackTrace();
				setMensajeError("Error al Guardar");
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="rechazarRecibo",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verValidacionManual","namespace","/recepcionDocs","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
					"tipoAccion","${tipoAccion}"}),					 
			@Result(name=INPUT,type="redirectAction", 
					params={"actionName","verValidacionManual","namespace","/recepcionDocs","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
					"tipoAccion","${tipoAccion}"})})
		public String rechazarRecibo(){
			try {
				recepDocsService.saveRechazoDocumentos(recepDocs, mesInicio, mesFin);
				setMensaje(MENSAJE_EXITO);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			} catch (MidasException e) {
				setMensaje(MENSAJE_ERROR_GENERAL);
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="verRechazarRecibo",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/recepcionDocumentos/rechazoRecibo.jsp")
		})
		public String verRechazarRecibo(){
			try {
				motivosRechazo=catalogoService.obtenerElementosPorCatalogo("Motivo Rechazo Documento");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return SUCCESS;
		}
	
	@Action(value="matchDocumentosAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchDocumentosAgente() {
			try {				
				documentoEntidadService.sincronizarDocumentos(agente.getId(), "AGENTES", "RECIBOS");
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(agente.getId(), "AGENTES", "RECIBOS");
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="muestraDocumentosEntregadosMesAnio",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaFacturasMeses.*"}),
		@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaFacturasMeses.*"})
	})
	public String muestraDocumentosEntregadosMesAnio(){
		
		try {
			Agente agenteBus = agenteMidasService.findByClaveAgente(agente);
			Long idAgente= agenteBus.getId();
			listaFacturasMeses = recepDocsService.listaFacturasMesAnio(anio,idAgente);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	

	
	@Action(value="auditarDocumentosEntregadosMesAnio",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaFacturasMeses.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaFacturasMeses.*"})
		})
		public String auditarDocumentosEntregadosMesAnio() {
			try {				
				
				documentoEntidadService.auditarDocumentosEntregadosAnio(agente.getId(),anio,"AGENTES");
				listaFacturasMeses = recepDocsService.listaFacturasMesAnio(anio,agente.getId());
				
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
	@Override
	public void prepare() throws Exception {
		agente=null;
	}

	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}
	
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	
	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("recepDocumentosServiceEJB")
	public void setRecepDocsService(RecepcionDocumentosService recepDocsService) {
		this.recepDocsService = recepDocsService;
	}

	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	public Agente getAgente() {
		return agente;
	}
	
	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public void setListaDocumentosFortimax(
			List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}	
	
	public String getUrlIfimax() {
		return urlIfimax;
	}
	
	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}
	
	public String getDocumentosFaltantes() {
		return documentosFaltantes;
	}
	
	public void setDocumentosFaltantes(String documentosFaltantes) {
		this.documentosFaltantes = documentosFaltantes;
	}

	public CatalogoDocumentoFortimax getDocumentoFortimax() {
		return documentoFortimax;
	}

	public void setDocumentoFortimax(CatalogoDocumentoFortimax documentoFortimax) {
		this.documentoFortimax = documentoFortimax;
	}

	public Integer getMesInicio() {
		return mesInicio;
	}

	public void setMesInicio(Integer mesInicio) {
		this.mesInicio = mesInicio;
	}

	public Integer getMesFin() {
		return mesFin;
	}

	public void setMesFin(Integer mesFin) {
		this.mesFin = mesFin;
	}

	public RecepcionDocumentos getRecepDocs() {
		return recepDocs;
	}

	public void setRecepDocs(RecepcionDocumentos recepDocs) {
		this.recepDocs = recepDocs;
	}

	public List<ValorCatalogoAgentes> getMotivosRechazo() {
		return motivosRechazo;
	}

	public void setMotivosRechazo(List<ValorCatalogoAgentes> motivosRechazo) {
		this.motivosRechazo = motivosRechazo;
	}

	public List<DocumentosAgrupados> getListDocAgrupado() {
		return listDocAgrupado;
	}

	public void setListDocAgrupado(List<DocumentosAgrupados> listDocAgrupado) {
		this.listDocAgrupado = listDocAgrupado;
	}	
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public List<EntregoDocumentosView> getListaFacturasMeses() {
		return listaFacturasMeses;
	}

	public void setListaFacturasMeses(List<EntregoDocumentosView> listaFacturasMeses) {
		this.listaFacturasMeses = listaFacturasMeses;
	}

	public List<ValorCatalogoAgentes> getListaFacturasMesesEntregadas() {
		return listaFacturasMesesEntregadas;
	}

	public void setListaFacturasMesesEntregadas(
			List<ValorCatalogoAgentes> listaFacturasMesesEntregadas) {
		this.listaFacturasMesesEntregadas = listaFacturasMesesEntregadas;
	}
	
	public Long getAnio() {
		return anio;
	}

	public void setAnio(Long anio) {
		this.anio = anio;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	
}
