package mx.com.afirme.midas2.action.catalogos.cobpaquetesseccion;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.dto.RelacionesCobPaquetesSeccionDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.CobPaquetesSeccionService;


@Component
@Scope("prototype")
public class CobPaquetesSeccionAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4520127088974367105L;
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public String mostrarCatalogo(){
		secciones = cobPaquetesSeccionService.getListarSeccionesVigentesAutos();
		paquetes = new ArrayList<Paquete>();
		return SUCCESS;
	}
	
	public String obtenerCoberturaSeccionAsociadas(){
		relacionesCobPaquetesSeccionDTO = cobPaquetesSeccionService.getRelationLists(cobPaquetesSeccion);
		return SUCCESS;
	}
	
	public String obtenerCoberturaSeccionDisponibles(){
		relacionesCobPaquetesSeccionDTO = cobPaquetesSeccionService.getRelationLists(cobPaquetesSeccion);
		return SUCCESS;
	}
	
	public String relacionarCoberturaSeccion(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = cobPaquetesSeccionService.relacionarCoberturaSeccion(accion,cobPaquetesSeccion);
		return SUCCESS;
	}
	
	public String coberturasDisponibles(){
		return SUCCESS;
	}

	
	private CobPaquetesSeccion cobPaquetesSeccion;
	private List<SeccionDTO> secciones;
	private List<Paquete> paquetes;
	private RelacionesCobPaquetesSeccionDTO relacionesCobPaquetesSeccionDTO;
	private RespuestaGridRelacionDTO respuesta;
	private CobPaquetesSeccionService cobPaquetesSeccionService;
	
	
	
	public CobPaquetesSeccion getCobPaquetesSeccion() {
		return cobPaquetesSeccion;
	}



	public void setCobPaquetesSeccion(CobPaquetesSeccion cobPaquetesSeccion) {
		this.cobPaquetesSeccion = cobPaquetesSeccion;
	}



	public List<SeccionDTO> getSecciones() {
		return secciones;
	}



	public void setSecciones(List<SeccionDTO> secciones) {
		this.secciones = secciones;
	}



	public List<Paquete> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(List<Paquete> paquetes) {
		this.paquetes = paquetes;
	}

	public RelacionesCobPaquetesSeccionDTO getRelacionesCobPaquetesSeccionDTO() {
		return relacionesCobPaquetesSeccionDTO;
	}



	public void setRelacionesCobPaquetesSeccionDTO(
			RelacionesCobPaquetesSeccionDTO relacionesCobPaquetesSeccionDTO) {
		this.relacionesCobPaquetesSeccionDTO = relacionesCobPaquetesSeccionDTO;
	}


	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	@Autowired
	@Qualifier("cobPaquetesSeccionServiceEJB")
	public void setCobPaquetesSeccionService(CobPaquetesSeccionService cobPaquetesSeccionService){
		this.cobPaquetesSeccionService = cobPaquetesSeccionService;
	}


}
