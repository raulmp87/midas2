package mx.com.afirme.midas2.action.catalogos.tarifa.administracionConsultaMovil;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction.TipoAccionHistorial;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;
import mx.com.afirme.midas2.domain.movil.cliente.InfoDescargaPoliza;
import mx.com.afirme.midas2.domain.movil.cliente.PolizaDescargaMovil;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/administracionConsultaMovil")
@Component
@Scope("prototype")
public class AdministracionConsultaMovilAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4814866545981130881L;
	private final String NAMESPACE="/tarifa/administracionConsultaMovil";
	/***Components of view****************************************/
	@Autowired
	private ClientePolizasService clientePolizasService;
	private PolizasClienteMovil polizasClienteMovil;
	private ClientePolizas clientePolizas;
	private String polizasPermitidos;
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	private String tabActiva;
	
	//////*****Consulta Móvil*******//////
	private List<InfoDescargaPoliza> listarFiltrado = new ArrayList<InfoDescargaPoliza>();
	private List<PolizasClienteMovil> listarPolizasCliente = new ArrayList<PolizasClienteMovil>();
	private String usuario;
	/**
	 * Action:
	 * 
	 */
	@Override
	public void prepare() throws Exception {
		if(clientePolizas!=null && "1".equals(tipoAccion)){
			clientePolizas=new ClientePolizas();
		}
	}
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionConsultaMovil/mostrar.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionConsultaMovil/mostrar.jsp")
		})	
		public String mostrar(){
			listarFiltrado = clientePolizasService.getRegistroDescargas();
			return SUCCESS;
		}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionConsultaMovil/administracionConsultaMovilGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionConsultaMovil/administracionConsultaMovilGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		listarFiltrado = clientePolizasService.getRegistroDescargas();
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/detalle.jsp")
		})
	@Override
	public String verDetalle() {
		if(clientePolizas != null){
			if(clientePolizas.getPolizaNo()!=null){
				//clientePolizas = cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_MOVIL).get(0);
			}
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/detalle.jsp")
		})	
	public String verDetalleHistorico() {
		if(clientePolizas != null){
			if(clientePolizas.getPolizaNo()!=null){
				//descuentosAgente = gerenciaJpaService.loadById(descuentosAgente, this.getUltimaModificacion().getFechaHoraActualizacionString());
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion","excludeProperties","^gerencia\\.ejecutivos.*,^gerencia\\.personaResponsable.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion"})
	})
	
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	////***ConsultaMovil****////
	// Seccion detallada de descargas
	public String mostrarDescargas(){
		return SUCCESS;
	}
	
	@Action(value="mostrarCatalogoTarifaMovil",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/tarifaMovil/tarifaMovilCatalogo.jsp")
		})
	public String getSucess(){
		this.setTabActiva("administracion_consulta_movil");
		return SUCCESS;
	}
	
	@Action(value="bloquearUsuario",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/tarifaMovil/tarifaMovilCatalogo.jsp")
		})
	public String bloquearUsuario(){
		this.setTabActiva("administracion_consulta_movil");
		clientePolizasService.bloqueaUsuario(usuario);
		return SUCCESS;
	}
	
	@Action(value="desbloqueaUsuario",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/tarifaMovil/tarifaMovilCatalogo.jsp")
		})
	public String desbloqueaUsuario(){
		this.setTabActiva("administracion_consulta_movil");
		clientePolizasService.desbloqueaUsuario(usuario);
		return SUCCESS;
	}
	
	public String verDetalleDescargas(){		
		return SUCCESS;
	}
	
	public String listarDetalle(){
		listarFiltrado = clientePolizasService.getDetalleDescargas(usuario);
		return SUCCESS;
	}
	
	//////////////Seccion Consulta Polizas//////////
	/*@Action(value="consultaPolizas",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionConsultaMovil/consultaPolizasGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionConsultaMovil/consultaPolizasGrid.jsp")
		})
	public String consultaPolizas(){
		listarPolizasCliente = clientePolizasService.findAll();
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoPolizas",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionConsultaMovil/consultaPolizasGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionConsultaMovil/consultaPolizasGrid.jsp")
		})

	public String listarFiltradoPolizas() {
		try{
			if("consulta".equals(tipoAccion)){
				if(polizasClienteMovil==null){
					polizasClienteMovil=new PolizasClienteMovil();
               }
			}
			if(polizasClienteMovil!=null){
				listarPolizasCliente = clientePolizasService.findByFilters(polizasClienteMovil);
			}
			else{
				listarPolizasCliente = clientePolizasService.findAll();
			}
			
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}
	@Action(value="verDetalleConsultaPolizas",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionConsultaMovil/detalleConsultaPolizas.jsp")
		})
	//@Override
	public String verDetalleConsultaPolizas() {
		if(polizasClienteMovil != null){
			if(polizasClienteMovil.getId()!=null){
				polizasClienteMovil = clientePolizasService.findById(polizasClienteMovil.getId());
			}
		}
		return SUCCESS;
	}*/
	
	/////////Seccion Admin Polizas////////////////
	/*@Action(value="verDetalleAdministracionPolizas",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionConsultaMovil/detalleAdministracionPolizas.jsp")
		})
	
	public String verDetalleAdministracionPolizas() {
		if(polizasClienteMovil != null){
			if(polizasClienteMovil.getId()!=null){
				polizasClienteMovil = clientePolizasService.findById(polizasClienteMovil.getId());
			}
		}
		return SUCCESS;
	}*/
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalleAdministracionPolizas","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","polizasClienteMovil.id",
					"${polizasClienteMovil.id}","idTipoOperacion","${idTipoOperacion}",
					"idRegistro","${polizasClienteMovil.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalleAdministracionPolizas","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}",
					"idRegistro","${polizasClienteMovil.id}"})})
	@Override
	public String guardar() {
		try {
			if(polizasClienteMovil.getId()!=null){
				PolizasClienteMovil polizasClienteMovil=null;
				polizasClienteMovil.setPolizasPermitidos(new Long(polizasPermitidos));
				clientePolizasService.update(polizasClienteMovil);	
			}

		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return result;
	}
	
	/////////////////////////////////////////////
	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	
	@Override
	public String eliminar() {
		return null;
	}
	
	@Override
	public String listar() {
		return null;
	}
	
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	
	public String getTabActiva() {
		return tabActiva;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public List<InfoDescargaPoliza> getListarFiltrado() {
		return listarFiltrado;
	}
	
	public void setListarFiltrado(List<InfoDescargaPoliza> listarFiltrado) {
		this.listarFiltrado = listarFiltrado;
	}
	public List<PolizasClienteMovil> getListarPolizasCliente() {
		return listarPolizasCliente;
	}
	public void setListarPolizasCliente(
			List<PolizasClienteMovil> listarPolizasCliente) {
		this.listarPolizasCliente = listarPolizasCliente;
	}
	public ClientePolizasService getClientePolizasService() {
		return clientePolizasService;
	}
	public void setClientePolizasService(ClientePolizasService clientePolizasService) {
		this.clientePolizasService = clientePolizasService;
	}
	public PolizasClienteMovil getPolizasClienteMovil() {
		return polizasClienteMovil;
	}
	public void setPolizasClienteMovil(PolizasClienteMovil polizasClienteMovil) {
		this.polizasClienteMovil = polizasClienteMovil;
	}
	public ClientePolizas getClientePolizas() {
		return clientePolizas;
	}
	public void setClientePolizas(ClientePolizas clientePolizas) {
		this.clientePolizas = clientePolizas;
	}
	public String getPolizasPermitidos() {
		return polizasPermitidos;
	}
	public void setPolizasPermitidos(String polizasPermitidos) {
		this.polizasPermitidos = polizasPermitidos;
	}
	
}

