package mx.com.afirme.midas2.action.catalogos.tarifa.administracionCotizacionPortal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/administracionCotizacionPortal")
@Component
@Scope("prototype")
public class AdministracionCotizacionPortalAction extends CatalogoHistoricoAction implements Preparable{

	private static final long serialVersionUID = 661973027364063003L;
	@Autowired
	private CotizacionMovilService cotizacionMovilService;
	private CotizacionMovilDTO cotizacionMovil;
	private List<CotizacionMovilDTO> listaCotizacionMovil= new ArrayList<CotizacionMovilDTO>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	private String tabActiva;

	@Override
	public void prepare() throws Exception {
		if(cotizacionMovil!=null && "1".equals(tipoAccion)){
			cotizacionMovil=new CotizacionMovilDTO();
		}
	}
	
	@Action(value="mostrar",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionPortal/mostrar.jsp")
	})
	public String mostrar(){
		cotizacionMovil=new CotizacionMovilDTO();
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionPortal/administracionCotizacionPortalGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionCotizacionPortal/administracionCotizacionPortalGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		try{
			if ("consulta".equals(tipoAccion) && cotizacionMovil==null){
				cotizacionMovil = new CotizacionMovilDTO();
			}
			
			if (cotizacionMovil != null) {
				listaCotizacionMovil = cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL);
			} else {
				listaCotizacionMovil = cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL);
			}
			
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionPortal/detalle.jsp")
		})
	@Override
	public String verDetalle() {
		if (cotizacionMovil != null && cotizacionMovil.getIdtocotizacionmovil()!=null) {
			cotizacionMovil = cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL).get(0);
		}
		return SUCCESS;
	}
	
	@Override
	public String guardar() {

		return null;
	}
	@Override
	public String eliminar() {

		return null;
	}
	@Override
	public String listar() {

		return null;
	}

	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public CotizacionMovilDTO getCotizacionMovil() {
		return cotizacionMovil;
	}
	
	public void setCotizacionMovil(CotizacionMovilDTO cotizacionMovil) {
		this.cotizacionMovil = cotizacionMovil;
	}
	
	public List<CotizacionMovilDTO> getListaCotizacionMovil() {
		return listaCotizacionMovil;
	}
	
	public void setListaCotizacionMovil(
			List<CotizacionMovilDTO> listaCotizacionMovil) {
		this.listaCotizacionMovil = listaCotizacionMovil;
	}
	
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	
	public String getTabActiva() {
		return tabActiva;
	}
}

