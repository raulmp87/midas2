package mx.com.afirme.midas2.action.catalogos.domicilio;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
@Component
@Scope("prototype")
@Namespace("/catalogos/domicilio")
public class DomicilioAction extends CatalogoHistoricoAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DomicilioFacadeRemote domicilioFacade;
	private final String LISTAR_JSP="/jsp/catalogos/domicilio/domicilioCatalogo.jsp";
	private final String DETALLE_JSP="/jsp/catalogos/domicilio/domicilioDetalle.jsp";
	private final String GRID_JSP="/jsp/catalogos/domicilio/domicilioGrid.jsp";
	private String accion=SUCCESS;
	/***Components of view*****************************************/
	private Domicilio domicilio;
	private Domicilio filtroDomicilio;
	private List<Domicilio> domicilios=new ArrayList<Domicilio>();
	private String esIFrame;
	/***Actions****************************************************/
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}

	@Action(value="guardarDomicilio",results={
		@Result(name=SUCCESS,type="redirectAction", 
				params={"actionName","verDetalle","namespace","/catalogos/domicilio",
				"tipoAccion","${tipoAccion}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","domicilio.idDomicilio","${domicilio.idDomicilio}","idRegistro","${domicilio.idDomicilio}","idTipoOperacion","100"}
		),
		@Result(name=INPUT,location=DETALLE_JSP)
	})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			if(domicilio!=null && domicilio.getIdDomicilio()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			Long idDomicilio=domicilioFacade.save(domicilio, null);
			DomicilioPk pkDomicilio=new DomicilioPk();
			pkDomicilio.setIdDomicilio(idDomicilio);
			domicilio.setIdDomicilio(pkDomicilio);
			guardarHistorico(TipoOperacionHistorial.DOMICILIO, idDomicilio,"midas.domicilio.historial.accion",tipoAccionHistorial);
			setMensaje("Acción realizada correctamente");
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		} catch (Exception e) {
			e.printStackTrace();
			setMensajeError("Error al guardar,causado por:"+e.getMessage());
			setMensaje("Error al guardar,causado por:"+e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			accion=INPUT;
		}
		return accion;
	}

	@Action(value="eliminarDomicilio",results={
		@Result(name=SUCCESS,location=LISTAR_JSP),
		@Result(name=INPUT,location=DETALLE_JSP)
	})
	@Override
	public String eliminar() {
		
		try {
			domicilioFacade.remove((domicilio!=null && domicilio.getIdDomicilio()!=null)?domicilio.getIdDomicilio().getIdDomicilio():null);
			guardarHistorico(TipoOperacionHistorial.DOMICILIO, domicilio.getIdDomicilio().getIdDomicilio(),"midas.domicilio.historial.accion",TipoAccionHistorial.BAJA);
			setMensajeExito();
		} catch (Exception e) {
			e.printStackTrace();
			setMensajeError("Error al guardar,causado por:"+e.getMessage());
			accion=INPUT;
		}
		return accion;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return accion;
	}

	@Action(value="filtrarDomicilio",results={
		@Result(name=SUCCESS,location=GRID_JSP),
		@Result(name=INPUT,location=GRID_JSP)
	})
	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		domicilios=domicilioFacade.findByFilters(filtroDomicilio);
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location=DETALLE_JSP),
		@Result(name=INPUT,location=LISTAR_JSP)
	})
	@Override
	public String verDetalle() {
		if(domicilio!=null && domicilio.getIdDomicilio()!=null && domicilio.getIdDomicilio().getIdDomicilio()!=null){
			try {
				if("1".equals(tipoAccion)){
					domicilio=new Domicilio();
				}
				else{
					domicilio=domicilioFacade.findById(domicilio.getIdDomicilio().getIdDomicilio());
				}
			} catch (Exception e) {
				e.printStackTrace();
				accion=INPUT;
			}
		}
		return accion;
	}
	
	@Action(value="init",results={
		@Result(name=SUCCESS,location=LISTAR_JSP)
	})
	public String init(){
		return accion;
	}
	
	@Action(value="loadById",results={
		@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","root","domicilio"})
	})
	public String loadById(){
		verDetalle();
		return accion;
	}
	
	/***Sets and gets**********************************************/
	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public Domicilio getFiltroDomicilio() {
		return filtroDomicilio;
	}

	public void setFiltroDomicilio(Domicilio filtroDomicilio) {
		this.filtroDomicilio = filtroDomicilio;
	}

	public List<Domicilio> getDomicilios() {
		return domicilios;
	}

	public void setDomicilios(List<Domicilio> domicilios) {
		this.domicilios = domicilios;
	}

	public String getEsIFrame() {
		return esIFrame;
	}

	public void setEsIFrame(String esIFrame) {
		this.esIFrame = esIFrame;
	}

	@Autowired
	@Qualifier("domicilioFacadeRemoteEJB")
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
}
