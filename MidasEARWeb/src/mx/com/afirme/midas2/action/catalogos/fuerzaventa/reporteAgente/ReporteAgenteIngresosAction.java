package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteAgenteIngresos")
@Component
@Scope("prototype")
public class ReporteAgenteIngresosAction extends ReporteAgenteBaseAction implements
		ReportMethods, Preparable {

	private static final long serialVersionUID = 1L;
	
	@Override
	@Action(value = "mostrarFiltros", results = { @Result(name = SUCCESS, location = "/jsp/reportesAgentes/reporteAgenteIngresos.jsp") })
	public String mostrarFiltros() {
		return SUCCESS;
	}

	@Override
	@Action(value = "exportarToPDF", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToPDF() {
		return null;
	}

	@Override
	@Action(value = "exportarToExcel", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = {
					"mensaje", "${mensaje}" }) })
	public String exportarToExcel() {
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
					.imprimirReporteAgenteIngresosToExcel(getAnio(), getMes(),
							getRangoInicio(), getRangoFin(), getTipoReporte(),
							getLocale(),"xlsx");//getTipoSalidaArchivo()
			
			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return INPUT;
			}
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				setFileName("reporteIngresosAgente.xlsx");
		} catch (RuntimeException error) {
			error.printStackTrace();
			setMensaje(error.getMessage());
			return INPUT;
		} catch (Exception error) {
			setMensaje(error.getMessage());
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
