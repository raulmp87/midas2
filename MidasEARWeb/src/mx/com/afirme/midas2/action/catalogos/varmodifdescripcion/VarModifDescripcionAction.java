package mx.com.afirme.midas2.action.catalogos.varmodifdescripcion;

import java.util.List;

import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.VarModifDescripcionService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class VarModifDescripcionAction extends CatalogoAction implements Preparable {

	private static final long serialVersionUID = -5243724700575384865L;
	
	public void validateGuardar(){
		catalogoValorFijoDTOList = varModifDescripcionService.listarCombo();
		addErrors(varModifDescripcion, NewItemChecks.class, this, "varModifDescripcion");
	}
	
	public void validateEditar(){
		addErrors(varModifDescripcion, EditItemChecks.class, this, "varModifDescripcion");
	}

	public void prepare() throws Exception {

		if (getId() != null) {
			varModifDescripcion = varModifDescripcionService.findById(getId());
		}
		
	}
	
	public String execute() {
		return null;
	}
	

	@Override
	public String eliminar() {
		varModifDescripcionService.remove(varModifDescripcion);
		catalogoValorFijoDTOList = varModifDescripcionService.listarCombo();
		//varModifDescripcion = new VarModifDescripcion();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String guardar() {
		varModifDescripcionService.save(varModifDescripcion);
		//varModifDescripcion = new VarModifDescripcion();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String listar() {
		varModifDescripcionList = varModifDescripcionService.findAll();
		
		return SUCCESS;
	}
	
	@Override
	public String listarFiltrado() {		
		varModifDescripcionList = varModifDescripcionService.findByFilters(varModifDescripcion);		
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		catalogoValorFijoDTOList = varModifDescripcionService.listarCombo();
		return SUCCESS;
	}

	
	public VarModifDescripcionAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	
	public String listarCombo(){
		//varModifDescripcion = new VarModifDescripcion();
		//varModifDescripcionList = new ArrayList<VarModifDescripcion>();
		catalogoValorFijoDTOList = varModifDescripcionService.listarCombo();
		return SUCCESS;
	}
	
	
	
	private Long id;

	private String tipoAccion; 

	private VarModifDescripcion varModifDescripcion;
	
	private List<VarModifDescripcion> varModifDescripcionList;
		
	private VarModifDescripcionService varModifDescripcionService;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
	
	private List<CatalogoValorFijoDTO>  catalogoValorFijoDTOList;
	
	public List<CatalogoValorFijoDTO> getCatalogoValorFijoDTOList() {
		return catalogoValorFijoDTOList;
	}

	public void setCatalogoValorFijoDTOList(
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOList) {
		this.catalogoValorFijoDTOList = catalogoValorFijoDTOList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	
	public VarModifDescripcion getVarModifDescripcion() {
		return varModifDescripcion;
	}

	public void setVarModifDescripcion(VarModifDescripcion varModifDescripcion) {
		this.varModifDescripcion = varModifDescripcion;
	}

	public List<VarModifDescripcion> getVarModifDescripcionList() {
		return varModifDescripcionList;
	}

	public void setVarModifDescripcionList(List<VarModifDescripcion> varModifDescripcionList) {
		this.varModifDescripcionList = varModifDescripcionList;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	@Autowired
	@Qualifier("varModifDescripcionEJB")
	public void setVarModifDescripcionService(
			VarModifDescripcionService varModifDescripcionService) {
		this.varModifDescripcionService = varModifDescripcionService;
	}

}
