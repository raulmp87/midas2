package mx.com.afirme.midas2.action.catalogos.fuerzaventa.ejecutivo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Namespace("/fuerzaventa/ejecutivo")
@Component
@Scope("prototype")
public class EjecutivoAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6416820604722785352L;
	/**
	 * 
	 */
	private EjecutivoService ejecutivoService;
	private PromotoriaJPAService promotoriaJpaService;
	private final String NAMESPACE="/fuerzaventa/ejecutivo";
	/***Components of view****************************************/
	private Ejecutivo ejecutivo;
	private Ejecutivo filtroEjecutivo;
	private List<Ejecutivo> listaEjecutivo = new ArrayList<Ejecutivo>();
	private List<EjecutivoView> listaEjecutivoView = new ArrayList<EjecutivoView>();
	private List<Promotoria> listPromotoria = new ArrayList<Promotoria>();
	private List<ValorCatalogoAgentes> listaTipoEjecutivo=new ArrayList<ValorCatalogoAgentes>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	
	/**
	 * Action:
	 * 
	 */
//	public void prepare(){}
	@Override
	public void prepare() throws Exception {
		if("1".equals(tipoAccion)){
			ejecutivo=new Ejecutivo();
		}
	}
	
	@Action(value="guardar",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","ejecutivo.id","${ejecutivo.id}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${ejecutivo.id}"}),
		@Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${ejecutivo.id}"})
	})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			tipoAccion="1";
			if(ejecutivo.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
				tipoAccion="4";//se habilita el boton de actualizar
			}
			ejecutivo = ejecutivoService.saveFull(ejecutivo);
			Long idEjecutivo=ejecutivo.getId();
			guardarHistorico(TipoOperacionHistorial.EJECUTIVO, idEjecutivo,"midas.ejecutivo.historial.accion",tipoAccionHistorial);
			setMensaje("Acción realizada correctamente");
            setTipoMensaje(TIPO_MENSAJE_EXITO);
            setIdTipoOperacion(30L);
            setTipoAccion("4");
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
	        setTipoMensaje(TIPO_MENSAJE_ERROR);		
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void validateGuardar() {
		String domainObjectPrefix = "ejecutivo";
        addErrors(ejecutivo, NewItemChecks.class, this, domainObjectPrefix);
	}
	
	public void validateEditar(){
		addErrors(ejecutivo, EditItemChecks.class, this, "centroOperacion");
	}

	@Action(value="eliminar",results={
//			@Result(name=SUCCESS,type="redirectAction",params={"actionName","listarFiltrado","namespace","/fuerzaventa/ejecutivo","ejecutivo","${ejecutivo}"}),
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","ejecutivo.id","${ejecutivo.id}",
					"idTipoOperacion","${idTipoOperacion}","idRegistro","${ejecutivo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","ejecutivo.id","${ejecutivo.id}",
					"idTipoOperacion","${idTipoOperacion}","idRegistro","${ejecutivo.id}"})
//			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/ejecutivo/detalle.jsp","tipoAccion","${tipoAccion}",)
	})
	@Override
	public String eliminar() {
		try {
			setIdTipoOperacion(30L);
			setIdRegistro(ejecutivo.getId());
			ejecutivoService.unsubscribe(ejecutivo);
			guardarHistorico(TipoOperacionHistorial.EJECUTIVO, ejecutivo.getId(),"midas.ejecutivo.historial.accion",TipoAccionHistorial.BAJA);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="3";	
		}catch(Exception e){
//			setMensaje(MENSAJE_ERROR_GENERAL);
//			e.printStackTrace();
			String mensaje = e.toString();
			 mensaje=mensaje.substring(21);
			setMensajeError(mensaje);	
			tipoAccion="3";
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="lista",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoGrid.jsp")
	})
	@Override
	public String listar() {
		try{
			listaEjecutivoView = ejecutivoService.findByFiltersView(null);
		}catch(Exception e){
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	public void prepareListarFiltrado(){
		filtroEjecutivo = new Ejecutivo();
	}

	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/ejecutivo/ejecutivoGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		try{
			//Map<String, Object> params=ReflectionUtils.getMapFromBean(filtroEjecutivo);
			if("consulta".equals(tipoAccion)){
				if(filtroEjecutivo==null){
					filtroEjecutivo=new Ejecutivo();
               }
				filtroEjecutivo.setClaveEstatus(1L);
			}
			listaEjecutivoView = ejecutivoService.findByFiltersView(filtroEjecutivo);
			}catch(Exception e){
				e.printStackTrace();
				return INPUT;
			}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/detalle.jsp")
	})
	@Override
	public String verDetalle() {
		if(ejecutivo!=null && ejecutivo.getId() !=null){
			ejecutivo = ejecutivoService.loadById(ejecutivo);
		}else{
			ejecutivo=new Ejecutivo();
		}
		cargarTiposEjecutivos();
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/detalle.jsp")
		})		
		public String verDetalleHistorico() {
			if(ejecutivo!=null && ejecutivo.getId() !=null){
				ejecutivo = ejecutivoService.loadById(ejecutivo, this.getUltimaModificacion().getFechaHoraActualizacionString());
				
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}else{
				ejecutivo=new Ejecutivo();
			}
			cargarTiposEjecutivos();
			return SUCCESS;
		}
	
	private void cargarTiposEjecutivos(){
		listaTipoEjecutivo=cargarCatalogo("Tipos de Ejecutivo");
	}
	
	public String prepareMostrar(){
		return SUCCESS;
	}
	
	@Action(value="mostrarContenedor",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/mostrar.jsp")
	})
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	@Action(value="mostrarBusquedaPersona",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/mostrarBusquedaPersona.jsp")
	})
	public String mostrarBusquedaPersona(){
		return SUCCESS;
	}
	
	@Action(value="listarPersona",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/listarPersona.jsp")
	})
	public String listarPersona(){
		return SUCCESS;
	}
	
	@Action(value="mostrarBusquedaGerencia",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/mostrarBusquedaGerencia.jsp")
	})
	public String mostrarBusquedaGerencia(){
		return SUCCESS;
	}
	
	@Action(value="listarGerencias",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/listarGerencias.jsp")
	})
	public String listarGerencias(){
		return SUCCESS;
	}
	
	@Action(value="mostrarPromotoria",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/ejecutivo/mostrarPromotoria.jsp")
	})
	public String mostrarPromotoria(){
		try{
			listPromotoria = promotoriaJpaService.findByEjecutivo(ejecutivo.getId());
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^ejecutivo\\.id,^ejecutivo\\.personaResponsable\\.nombreCompleto"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^ejecutivo\\.id,^ejecutivo\\.personaResponsable\\.nombreCompleto"})
	})
	
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	
	
	/***Sets and gets****************************************************/
	@Autowired
	@Qualifier("ejecutivoServiceEJB")
	public void setEjecutivoService(EjecutivoService ejecutivoService){
		this.ejecutivoService = ejecutivoService;
	}
	@Autowired
	@Qualifier("promotoriaJPAServiceEJB")
	public void setPromotoriaJpaService(PromotoriaJPAService promotoriaJpaService) {
		this.promotoriaJpaService = promotoriaJpaService;
	}
	
	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	
	public List<Ejecutivo> getListaEjecutivo() {
		return listaEjecutivo;
	}
	public void setListaEjecutivo(List<Ejecutivo> listaEjecutivo) {
		this.listaEjecutivo= listaEjecutivo;
	}

	public Ejecutivo getFiltroEjecutivo() {
		return filtroEjecutivo;
	}

	public void setFiltroEjecutivo(Ejecutivo filtroEjecutivo) {
		this.filtroEjecutivo = filtroEjecutivo;
	}

	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public List<Promotoria> getListPromotoria() {
		return listPromotoria;
	}

	public void setListPromotoria(List<Promotoria> listPromotoria) {
		this.listPromotoria = listPromotoria;
	}

	public List<ValorCatalogoAgentes> getListaTipoEjecutivo() {
		return listaTipoEjecutivo;
	}

	public void setListaTipoEjecutivo(List<ValorCatalogoAgentes> listaTipoEjecutivo) {
		this.listaTipoEjecutivo = listaTipoEjecutivo;
	}

	public List<EjecutivoView> getListaEjecutivoView() {
		return listaEjecutivoView;
	}

	public void setListaEjecutivoView(List<EjecutivoView> listaEjecutivoView) {
		this.listaEjecutivoView = listaEjecutivoView;
	}	
	
}



