package mx.com.afirme.midas2.action.catalogos.condicionesespeciales;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.ArchivoAdjuntoCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * 
 * @author Lizeth De La Garza
 *
 */

@Component
@Scope("prototype")
@Namespace("/catalogos/condicionespecial/catalogo")
public class DefinirCondicionEspecialAction extends BaseAction implements Preparable{


	private static final long serialVersionUID = 1L;
	
	private Long idCondicionEspecial;
	
	private CondicionEspecial condicion;
	
	private List<EstatusCondicion>  listEstatus;
	
	private boolean consulta;
	
	private List<ArchivoAdjuntoCondicionEspecial> archivosAdjuntos;
	
	private BigDecimal idToControlArchivo;
	
	@Autowired
    @Qualifier("condicionEspecialServiceEJB")
	private CondicionEspecialService condicionEspecialService;

	@Override
	public void prepare() throws Exception {
		
		if (idCondicionEspecial != null) {
			condicion = condicionEspecialService.obtenerCondicion(idCondicionEspecial);
		}
		
		listEstatus = condicionEspecialService.obtenerEstatusDisponible(idCondicionEspecial);
		
	}
	
	@Action (value = "mostrarCondicion", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/condicionEspecial.jsp") })
	public String mostrarCondicion() {
		return SUCCESS;
	}
	
	@Action (value = "guardarCondicion", results = { 
			@Result(name = SUCCESS, type="redirectAction", 
					params={"actionName","mostrarContenedorCatalogo",
					"namespace","/catalogos/condicionespecial",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"idCondicionEspecial","${idCondicionEspecial}"
					}) ,
			@Result(name = "ACTIVACION", type="redirectAction",
					params={"actionName","mostrarContenedor",
					"namespace","/catalogos/condicionespecial",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}"}),
			@Result(name = INPUT, location = "/jsp/catalogos/condicionesespeciales/condicionEspecial.jsp")})
	public String guardarCondicion() {
		String returnParam = SUCCESS;
	
		if (condicion.getEstatus() == EstatusCondicion.PROCESO.getValue()) {
			
			if (idCondicionEspecial != null) {
				condicion.setId(idCondicionEspecial);
			}
			
			condicion = condicionEspecialService.guardarCondicionEnProceso(condicion);
			
			idCondicionEspecial = condicion.getId();
			
			super.setMensajeExitoPersonalizado(getText("midas.condicionespecial.guardarProcesoCondicion"));
			
		} else if (condicion.getEstatus() == EstatusCondicion.ALTA.getValue()) {
			
			condicionEspecialService.guardarCondicionActivacion(condicion);
			returnParam = "ACTIVACION";
			super.setMensajeExitoPersonalizado(getText("midas.condicionespecial.guardarAltaCondicion"));
		}

		return returnParam;
		
	}
	
	@Action (value = "salir", results = { 
			@Result(name = SUCCESS, type="redirectAction", 
					params={"actionName","mostrarContenedor",
					"namespace","/catalogos/condicionespecial"})})
	public String salir() {		
		return SUCCESS;
	}
	
	/*
	 * INICIO ADJUNTAR ARCHIVOS
	 */
	
	@Action (value = "mostrarAdjuntarArchivos", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/contenedorAdjuntarArchivos.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/condicionesespeciales/condicionEspecial.jsp")})
	public String mostrarAdjuntarArchivos() {			
		return SUCCESS;		
	}
	
	@Action (value = "listarArchivosAdjuntos", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/archivosAdjuntosGrid.jsp") })
	public String listarArchivosAdjuntos() {
		archivosAdjuntos = condicionEspecialService.listarArchivosAdjuntos(idCondicionEspecial);
		return SUCCESS;		
	}
	
	@Action (value = "cargarArchivoAdjunto", results = { 
			@Result(name = INPUT, location = "/jsp/catalogos/condicionesespeciales/contenedorAdjuntarArchivos.jsp"),			
			@Result(name= SUCCESS, type="redirectAction",
					params={"actionName","mostrarAdjuntarArchivos","namespace","/catalogos/condicionespecial/catalogo",
							"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
							"idCondicionEspecial","${idCondicionEspecial}","consulta","${consulta}"})})
	public String cargarArchivoAdjunto() {		
		try {
			ControlArchivoDTO controlArchivo = 
				ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);			
			condicionEspecialService.guardarArchivoAdjunto(controlArchivo, idCondicionEspecial, this.getUsuarioActual().getNombreUsuario());			
		} catch (SystemException e) {
			//e.printStackTrace();
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}	
		return SUCCESS;		
	}
	
	@Action (value = "eliminarArchivoAdjunto", results = { 
			@Result(name = INPUT, location = "/jsp/catalogos/condicionesespeciales/contenedorAdjuntarArchivos.jsp"),			
			@Result(name= SUCCESS, type="redirectAction",
					params={"actionName","mostrarAdjuntarArchivos","namespace","/catalogos/condicionespecial/catalogo",
							"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",
							"idCondicionEspecial","${idCondicionEspecial}","consulta","${consulta}"})})
	public String eliminarArchivoAdjunto() {									
		condicionEspecialService.eliminarArchivoAdjunto(idToControlArchivo);
		setMensajeExito();
		return SUCCESS;		
	}
	
	/*
	 * FIN ADJUNTAR ARCHIVOS
	 */
	
	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public CondicionEspecial getCondicion() {
		return condicion;
	}

	public void setCondicion(CondicionEspecial condicion) {
		this.condicion = condicion;
	}

	public List<EstatusCondicion> getListEstatus() {
		return listEstatus;
	}

	public void setListEstatus(List<EstatusCondicion> listEstatus) {
		this.listEstatus = listEstatus;
	}

	public boolean isConsulta() {
		return consulta;
	}

	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}

	public List<ArchivoAdjuntoCondicionEspecial> getArchivosAdjuntos() {
		return archivosAdjuntos;
	}

	public void setArchivosAdjuntos(
			List<ArchivoAdjuntoCondicionEspecial> archivosAdjuntos) {
		this.archivosAdjuntos = archivosAdjuntos;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
}
