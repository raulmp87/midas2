package mx.com.afirme.midas2.action.catalogos.parametroGeneral;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/catalogos/parametroGeneral")
public class ParametroGeneralAction extends CatalogoAction implements
		Preparable {

	private static final long serialVersionUID = 1L;
	private List<ParametroGeneralDTO> listaParametroGeneral = new ArrayList<ParametroGeneralDTO>();
	private ParametroGeneralService parametroGeneralService;
	private ParametroGeneralDTO parametroGeneral;
	private RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
	private String groupName;
	private String paramName;

	@Autowired
	@Qualifier("parametroGeneralServiceEJB")
	public void setparametroGeneralService(
			ParametroGeneralService parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}

    @Action(value = "obtenerJSONPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "parametroGeneral"+
                            "parametroGeneral,"+
                            "parametroGeneral\\.id,"+
                            "parametroGeneral\\.id\\.idToGrupoParametroGeneral,"+
                            "parametroGeneral\\.id\\.codigoParametroGeneral,"+
                            "parametroGeneral\\.grupoParametroGeneral,"+
                            "parametroGeneral\\.grupoParametroGeneral\\.idToGrupoParametroGeneral,"+
                            "parametroGeneral\\.grupoParametroGeneral\\.descripcionGrupoParametroGral,"+
                            "parametroGeneral\\.descripcionParametroGeneral,"+
                            "parametroGeneral\\.claveTipoValor,"+
                            "parametroGeneral\\.valor"
            })
        })
    public String obtenerJSONPorFiltros(){
		setParametroGeneral(parametroGeneralService.findByDescCode(groupName, paramName));
        return "json";
    }

    @Action(value = "listByGroup", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "parametroGeneral"+
                            "listaParametroGeneral\\[\\d+\\],"+
                            "listaParametroGeneral\\[\\d+\\]\\.id,"+
                            "listaParametroGeneral\\[\\d+\\]\\.id\\.idToGrupoParametroGeneral,"+
                            "listaParametroGeneral\\[\\d+\\]\\.id\\.codigoParametroGeneral,"+
                            "listaParametroGeneral\\[\\d+\\]\\.grupoParametroGeneral,"+
                            "listaParametroGeneral\\[\\d+\\]\\.grupoParametroGeneral\\.idToGrupoParametroGeneral,"+
                            "listaParametroGeneral\\[\\d+\\]\\.grupoParametroGeneral\\.descripcionGrupoParametroGral,"+
                            "listaParametroGeneral\\[\\d+\\]\\.descripcionParametroGeneral,"+
                            "listaParametroGeneral\\[\\d+\\]\\.claveTipoValor,"+
                            "listaParametroGeneral\\[\\d+\\]\\.valor"
            })
        })
    public String listByGroup(){
		setListaParametroGeneral(parametroGeneralService.listByGroup(groupName));
        return "json";
    }

	@Override
	public void prepare() throws Exception {
	}
	
	@Action(value = "mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/parametroGeneral/parametroGeneral.jsp")
	})
	public String mostrar() {	
		return SUCCESS;
	}
	
	@Action(value = "lista", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/parametroGeneral/parametroGeneralGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/parametroGeneral/parametroGeneralGrid.jsp") })
	@Override
	public String listar() {
		try {			
			listaParametroGeneral = parametroGeneralService.listAll();
		} catch (Exception e) {
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/parametroGeneral/parametroGeneralRespuesta.jsp"),
			@Result(name = INPUT, location = "/jsp/catalogos/parametroGeneral/parametroGeneralRespuesta.jsp") })
	@Override
	public String guardar() {
		try {
			parametroGeneralService.save(parametroGeneral);
			respuesta.setTipoMensaje("30");
		} catch (Exception e) {
			respuesta.setTipoMensaje("10");
		}
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		return null;
	}

	@Override
	public String listarFiltrado() {
		return null;
	}

	@Override
	public String verDetalle() {
		return null;
	}

	public List<ParametroGeneralDTO> getListaParametroGeneral() {
		return listaParametroGeneral;
	}

	public void setListaParametroGeneral(List<ParametroGeneralDTO> listaParametroGeneral) {
		this.listaParametroGeneral = listaParametroGeneral;
	}

	public ParametroGeneralDTO getParametroGeneral() {
		return parametroGeneral;
	}

	public void setParametroGeneral(ParametroGeneralDTO parametroGeneral) {
		this.parametroGeneral = parametroGeneral;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public void setGroupName(String groupName){
		this.groupName = groupName;
	}

	public void setParamName(String paramName){
		this.paramName = paramName;
	}
}
