package mx.com.afirme.midas2.action.catalogos.prestadorservicio; 

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Validator;

import mx.com.afirme.midas.sistema.seguridad.UsuarioPrestadorServicio;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.DatoContactoMidas;
import mx.com.afirme.midas2.domain.personadireccion.DireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas.TIPO_DOMICILIO;
import mx.com.afirme.midas2.domain.personadireccion.PersonaFisicaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMoralMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.InformacionBancaria;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.prestadordeservicio.PrestadorDeServicioDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.SystemCommonUtils;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/siniestros/catalogos/prestadorDeServicio")
public class CatPrestadorServicioAction extends BaseAction implements
		Preparable {
	
	
	public  static final Logger log = Logger.getLogger(CatPrestadorServicioAction.class);

	private static final String	LOCATION_LISTADOINFOBANCARIAGRID_JSP	= "/jsp/siniestros/catalogo/prestadorservicio/listadoInfoBancariaGrid.jsp";
	private static final String	LOCATION_BUSQUEDARAPIDA_JSP	            = "/jsp/siniestros/catalogo/prestadorservicio/contenedorBusquedaAsignacionPrestadorServicio.jsp";
	private static final String	LOCATION_BUSQUEDARAPIDA_GRID	        = "/jsp/siniestros/catalogo/prestadorservicio/contenedorBusquedaAsignacionPrestadorServicioGrid.jsp";

	private static final String	LOCATION_MOSTRARPRESTADORSERVICIO_JSP	= "/jsp/siniestros/catalogo/prestadorservicio/mostrarPrestadorServicio.jsp";


	private static final String	LOCATION_CONTENEDORPRESTADORSERVICIO_JSP	= "/jsp/siniestros/catalogo/prestadorservicio/contenedorPrestadorServicio.jsp";


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String ESTATUS_ACTIVO = "1";
	private static final Integer ESTATUS_INFOBANCARIA_ACTIVO = 1;
	private static final Integer ESTATUS_INFOBANCARIA_INACTIVO = 2;
	
	private static final String FN_M2_VER_DATOS = "FN_M2_ConcultaProveedor_CA";
	
	private boolean consulta;
	private PrestadorServicio entidad;
	private Map<String, String> estatus;
	private PrestadorServicioFiltro filtroCatalogo;
	private Map<Long, String> oficinas;
	private Map<String, String> tiposPersona;
	private Map<String, String> formaPago;
	private Map<String, String> tiposPrestador;
	private Map<Long,String> ciaSeguros;
	private List<PrestadorServicioRegistro> listPrestadorServicio;
	private PrestadorDeServicioDTO dto;
	private Integer index = 1;
	private InformacionBancaria informacionBancaria = new InformacionBancaria();
	private List<InformacionBancaria> listInfoBancaria;
	private String strTipoPrestadorSelected;
	private String currentUser;
	private Map<Long, String> bancos;
	private Map<Long, String> tipoMoneda;
	private String prestadorId;
	private String rfc;
	private Integer infoBancId;
	private List<String> tiposPrestadorParaConsultar;
	private Long activeInformacionBancaria;
	private boolean modoAsignar;
	private boolean modoEditar;
	private String tipoPrestador;	
	private TransporteImpresionDTO transporte;		
	private String claveUsuarioPrestador;

	@Resource
    private Validator validator;
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	private UsuarioService usuarioService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	private PrestadorDeServicioService prestadorServicioService;

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("direccionMidasServiceEJB")
	private DireccionMidasService direccionService;

	@Override
	public void prepare() {
		this.estatus = listadoService
				.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS);
		this.tiposPersona = listadoService
				.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PERSONA);
		this.formaPago = listadoService
		.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.FORMAPAGO_CA);
		this.oficinas = listadoService.obtenerOficinasSiniestros();
		this.tiposPrestador = listadoService.getMapTipoPrestador();
		this.filtroCatalogo = new PrestadorServicioFiltro();
		this.currentUser = String.valueOf(usuarioService.getUsuarioActual()
				.getNombreUsuario());
		this.bancos =  listadoService.getMapBancosMidas();
		this.tipoMoneda =  listadoService.getMapCaTipoMoneda();
		this.informacionBancaria = new InformacionBancaria();
		this.tiposPrestadorParaConsultar = new ArrayList<String>();		
		this.activeInformacionBancaria = -1L;
//		loadPrestadorServicio();
		
	}

	@Action(value = "buscar", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/prestadorservicio/listadoPrestadorServicioGrid.jsp"),
			@Result(name = INPUT, location = LOCATION_CONTENEDORPRESTADORSERVICIO_JSP) })
	public String buscar() {
		if(filtroCatalogo.getTipoPrestadorStr()!=null){
			filtroCatalogo.setTipoPrestadorStr(mx.com.afirme.midas.sistema.StringUtil.decodeUri(filtroCatalogo.getTipoPrestadorStr()));
		}
		
		filtroCatalogo.setNombrePersona(mx.com.afirme.midas.sistema.StringUtil.decodeUri(filtroCatalogo.getNombrePersona()));
		listPrestadorServicio = prestadorServicioService
				.buscar(this.filtroCatalogo); 
		//this.agregaNombreEstatusEnPrestadorDeServicio();
		return SUCCESS;
	}
	
	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = LOCATION_CONTENEDORPRESTADORSERVICIO_JSP) })
	public String mostrarContenedor() {
		return SUCCESS;	
	}
	
	public void prepareMostrarAltaDePrestador(){
		strTipoPrestadorSelected = null;
		this.entidad = new PrestadorServicio();
		PersonaMidas persona = new PersonaMidas();
		this.entidad.setPersonaMidas(persona);
		this.dto = new PrestadorDeServicioDTO();
		this.informacionBancaria = new InformacionBancaria();
		this.dto.setEstatus(ESTATUS_ACTIVO);
		this.ciaSeguros = listadoService.getMapCiaDeSeguros();
		this.dto.setTienePermisoCA(usuarioService.tienePermisoUsuarioActual(FN_M2_VER_DATOS));
		log.info("tienePermiso>>"+this.dto.isTienePermisoCA());		
	}

	@Action(value = "mostrarAltaDePrestador", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRARPRESTADORSERVICIO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORPRESTADORSERVICIO_JSP) })
	public String mostrarAltaDePrestador() {
		this.consulta = false;
		return SUCCESS;
	}     	
	
	public void prepareConsultaPrestador(){
		this.dto = new PrestadorDeServicioDTO();
		this.informacionBancaria = new InformacionBancaria();
		this.ciaSeguros = listadoService.getMapCiaDeSeguros();
		this.dto.setTienePermisoCA(usuarioService.tienePermisoUsuarioActual(FN_M2_VER_DATOS));		
	}	
	
	@Action(value = "cosultarPrestador", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRARPRESTADORSERVICIO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORPRESTADORSERVICIO_JSP) })
	public String consultaPrestador() {
		this.consulta = true;
		this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
		cargaDTOParaConsulta();	
		return SUCCESS;
	}	
	
	public void prepareEditarPrestador(){
		strTipoPrestadorSelected = null;
		this.dto = new PrestadorDeServicioDTO();
		this.informacionBancaria = new InformacionBancaria();
		this.ciaSeguros = listadoService.getMapCiaDeSeguros();
		this.dto.setTienePermisoCA(usuarioService.tienePermisoUsuarioActual(FN_M2_VER_DATOS));
	}
	
	@Action(value = "editarPrestador", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRARPRESTADORSERVICIO_JSP),
			@Result(name = INPUT, location = LOCATION_CONTENEDORPRESTADORSERVICIO_JSP) })
	public String editarPrestador() {
		this.informacionBancaria = new InformacionBancaria();
		this.consulta = false;
		this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
		cargaDTOParaConsulta();	
		return SUCCESS;
	}
	

	
	private void cargaDTOParaConsulta(){
		PersonaMidas persona = entidad.getPersonaMidas();
		if(this.ciaSeguros != null){
			for(Long  key : this.ciaSeguros.keySet()){
				String value = this.ciaSeguros.get(key);
				if(value.equals(this.entidad.getCiaSegurosPolizaRC())){
					this.dto.setCiaDeSeguros(key);
					break;
				}
			}
		}
		if(persona instanceof PersonaFisicaMidas){
			PersonaFisicaMidas personaFisicaMidas = (PersonaFisicaMidas)persona;
			this.dto.setNombre(personaFisicaMidas.getNombrePersona());
			this.dto.setApellidoPaterno(personaFisicaMidas.getApellidoPaterno());
			this.dto.setApellidoMaterno(personaFisicaMidas.getApellidoMaterno());
			this.dto.setFechaNacimiento(personaFisicaMidas.getFechaModificacion());
		}else{
			PersonaMoralMidas personaMoralMidas = (PersonaMoralMidas)persona;
			this.dto.setNombreDeLaEmpresa(personaMoralMidas.getNombreRazonSocial());
			this.dto.setNombreComercial(personaMoralMidas.getNombre());
			this.dto.setAdministrador(personaMoralMidas.getRepresentanteLegal());
		}
		this.dto.setNumPrestador(this.entidad.getId().toString());
		this.dto.setTerminoVigencia(this.entidad.getVigenciaPolizaRC());
		this.dto.setCurp((persona instanceof PersonaFisicaMidas)? ((PersonaFisicaMidas)persona).getCurp():"");
		this.dto.setTipoPersona((persona instanceof PersonaFisicaMidas)?"PF":"PM");
		this.dto.setEstatus(String.valueOf(this.entidad.getEstatus()));
		this.dto.setOficinaId(this.entidad.getOficina().getId());
		this.dto.setRfc(persona.getRfc());
		this.dto.setNumPoliza(this.entidad.getNumPolizaRC());
		persona.getPersonaDireccion().isEmpty();		
		if(persona.getPersonaDireccion() != null && persona.getPersonaDireccion().size() > 0){
			this.dto.setNombreCalle(persona.getPersonaDireccion().get(persona.getPersonaDireccion().size() - 1).getDireccion().getCalle());
			this.dto.setNombreCalleNumero((persona.getPersonaDireccion().get(persona.getPersonaDireccion().size() - 1).getDireccion().getNumExterior()));
			this.dto.setNumeroInterno((persona.getPersonaDireccion().get(persona.getPersonaDireccion().size() - 1).getDireccion().getNumInterior()));
			this.dto.setCodigoPostal((persona.getPersonaDireccion().get(persona.getPersonaDireccion().size() - 1).getDireccion().getCodigoPostalColonia()));
			CiudadMidas ciudad = null;				
			DireccionMidas direccion = persona.getPersonaDireccion().get(persona.getPersonaDireccion().size() - 1).getDireccion();
			if(persona.getPersonaDireccion().get(persona.getPersonaDireccion().size() - 1).getDireccion().getColonia() != null){
				this.dto.setNombreColonia(direccion.getColonia().getId());
				ColoniaMidas colonia = entidadService.findById(ColoniaMidas.class, direccion.getColonia().getId());
				//ciudad = direccionService.obtenerCiudadPorCP(colonia.getCodigoPostal());
				ciudad = direccionService.obtenerCiudadPorColonia(colonia.getId());
			}else{
				ciudad = direccionService.obtenerCiudadPorCP(direccion.getCodigoPostalColonia());
				this.dto.setNombreColoniaDiferente(direccion.getOtraColonia());
			}			
			this.dto.setReferencia(direccion.getReferencia());
			if(ciudad != null){
				this.dto.setIdPaisString(ciudad.getEstado().getPais().getId());
				this.dto.setIdEstadoString(ciudad.getEstado().getId());
				this.dto.setIdMunicipioString(ciudad.getId());
			}
		}
		this.dto.setServiciosProfesionales(this.entidad.getServiciosProfesionales());
		
		//COMPENSACIONES ADICIONALES////
		this.dto.setAutosCa(this.entidad.getAutosCa());		
		this.dto.setVidaCa(this.entidad.getVidaCa());
		this.dto.setDaniosCa(this.entidad.getDaniosCa());
		this.dto.setFormaPagoCa(this.entidad.getFormaPagoCa());		
		
		//Recupera los datos de direccion
		this.dto.setEmailAdicional(persona.getContacto().getCorreosAdicionales());
		this.dto.setOtros(persona.getContacto().getTelOtro2());
		this.dto.setOtrosLada(persona.getContacto().getTelOtro2Lada());
		this.dto.setCelular(persona.getContacto().getTelCelular());
		this.dto.setCelularLada(persona.getContacto().getTelCelularLada());
		this.dto.setEmailPrincipal(persona.getContacto().getCorreoPrincipal());
		this.dto.setTelefono2(persona.getContacto().getTelOtro1());
		this.dto.setTelefono2Lada(persona.getContacto().getTelOtro1Lada());
		this.dto.setTelefono1(persona.getContacto().getTelCasa());
		this.dto.setTelefono1Lada(persona.getContacto().getTelCasaLada());
		this.dto.setFechaDeAlta(this.entidad.getFechaCreacion());
		this.dto.setFechaDeBaja(this.entidad.getFechaModEstatus());
		UsuarioPrestadorServicio usuarioPrestador =  prestadorServicioService.obtenerUsuarioPorPrestador(this.entidad.getId());
		if(usuarioPrestador != null){
			this.claveUsuarioPrestador = usuarioPrestador.getCodigoUsuario();
		}
	}

	public String mostrarListado() {
		return null;
	}

	@Action(value = "salvarCatalogo", results = {
			@Result(name = SUCCESS, location = LOCATION_MOSTRARPRESTADORSERVICIO_JSP),
			@Result(name = INPUT, location = LOCATION_MOSTRARPRESTADORSERVICIO_JSP) })
	public String salvarCatalogo() {
		 this.ciaSeguros = listadoService.getMapCiaDeSeguros();
		 List<String> mensajes = this.validateInput();
		 if(!mensajes.isEmpty()){
			 super.setMensajeListaPersonalizado("Campos Requeridos", mensajes, MENSAJE_ERROR_VALDACION);
			 return INPUT;
		 }
		 createPrestadorServicio();
		 entidad = prestadorServicioService.guardar(entidad);
		 this.dto.setTienePermisoCA(usuarioService.tienePermisoUsuarioActual(FN_M2_VER_DATOS));
		 this.dto.setNumPrestador(String.valueOf(entidad.getId()));
		 this.consulta = false;
		 super.setMensajeExito();
		 return SUCCESS;
	}
	
	private List<String> validateInput(){
		List<String> mensajes = new ArrayList<String>();
		if(this.dto.getTipoPersona().equals("PF")){
			this.validateInputPersonaFisica(mensajes);
		}else{
			this.validateInputPersonaMoral(mensajes);
		}
		validNotNullField(mensajes, "Oficina", this.dto.getOficinaId());
		validNotNullField(mensajes, "Estatus", this.dto.getEstatus());
		if(this.dto.getNumPrestador()==null || this.dto.getNumPrestador().equals("")){
			validNotNullField(mensajes, "Tipos de Prestador", this.strTipoPrestadorSelected);
//			validNotNullField(mensajes, "Informacion Bancaria - Banco", this.getInformacionBancaria().getBancoId());
//			validNotNullField(mensajes, "Informacion Bancaria - No. de Cuenta", this.getInformacionBancaria().getNumeroCuenta());
//			validNotNullField(mensajes, "Informacion Bancaria - CLABE", this.getInformacionBancaria().getClabe());
		}
		validNotNullField(mensajes, "Pais", this.dto.getIdPaisString());
		validNotNullField(mensajes, "Municipio", this.dto.getIdMunicipioString());
		validNotNullField(mensajes, "Estado", this.dto.getIdEstadoString());
		validNotNullField(mensajes, "Codigo Postal", this.dto.getCodigoPostal());
		validNotNullField(mensajes, "", this.dto.getCodigoPostal());
		return mensajes;
	}
	
	
	private void validateInputPersonaFisica(List<String> mensajes){
		validNotNullField(mensajes, "Nombre(s)", this.dto.getNombre());
		validNotNullField(mensajes, "Apellido Paterno", this.dto.getApellidoPaterno());
		validNotNullField(mensajes, "Apellido Materno", this.dto.getApellidoMaterno());
		validNotNullField(mensajes, "Fecha de Nacimiento", this.dto.getFechaNacimiento());
		validNotNullField(mensajes, "Curp", this.dto.getCurp());
		validNotNullField(mensajes, "Rfc", this.dto.getRfc());
	}
	
	private void validateInputPersonaMoral(List<String> mensajes){
		validNotNullField(mensajes, "Nombre de la Empresa", this.dto.getNombreDeLaEmpresa());
		validNotNullField(mensajes, "Administrador", this.dto.getAdministrador());
		validNotNullField(mensajes, "Rfc", this.dto.getRfc());
	}
	
	private void validNotNullField(List<String> mensajes, String fieldName, Object obj){
		if(obj==null){
			mensajes.add(fieldName);
		}else{
			if(obj instanceof String && ((String)obj).equals("")){
				mensajes.add(fieldName);
			}
		}
	}		
	
//	private PrestadorServicio createPrestadorServicio(){
	private void createPrestadorServicio(){
//		PrestadorServicio prestadorServicio = new PrestadorServicio();
		List<TipoPrestadorServicio> tiposPrestadorSelected = null;
		if(this.dto.getNumPrestador()!=null && !this.dto.getNumPrestador().equals("")){
			this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.dto.getNumPrestador()));
//			prestadorServicio.setId(Integer.valueOf(this.dto.getNumPrestador()));
//			prestadorServicio.setFechaModificacion(Calendar.getInstance().getTime());
//			prestadorServicio.setCodigoUsuarioModificacion(this.currentUser);
//			prestadorServicio.setFechaCreacion( this.entidad.getFechaCreacion());
//			prestadorServicio.setCodigoUsuarioCreacion( this.entidad.getCodigoUsuarioCreacion() );
			entidad.setFechaModificacion(Calendar.getInstance().getTime());
			entidad.setCodigoUsuarioModificacion(this.currentUser);
			
			if(this.strTipoPrestadorSelected==null || this.strTipoPrestadorSelected.equals("")){
				tiposPrestadorSelected = this.entidad.getTiposPrestador();
			}else{
				tiposPrestadorSelected = this.createTipoPrestadorSelectedList();
			}
		}else{
				tiposPrestadorSelected = this.createTipoPrestadorSelectedList();
				entidad = new PrestadorServicio();
				entidad.setFechaCreacion(Calendar.getInstance().getTime());
				entidad.setCodigoUsuarioCreacion(this.currentUser);
				entidad.setFechaModificacion(Calendar.getInstance().getTime());
		}
		
		if( this.dto.getEstatus().equals("1")){
			entidad.setFechaModEstatus(null);
		}else{
			entidad.setFechaModEstatus(Calendar.getInstance().getTime());
		}
			
		PersonaMidas personaMidas = completaYGuardaInformacionDePersona();
		entidad.setTiposPrestador(tiposPrestadorSelected);
		entidad.setNumPolizaRC(this.dto.getNumPoliza());
		entidad.setPersonaMidas(personaMidas);
		entidad.setEstatus(Integer.valueOf(this.dto.getEstatus()));
		entidad.setVigenciaPolizaRC(this.dto.getTerminoVigencia());
		entidad.setCiaSegurosPolizaRC(this.ciaSeguros.get(this.dto.getCiaDeSeguros()));
		entidad.setServiciosProfesionales( dto.getServiciosProfesionales() );
		//COMPENSACIONES ADICIONALES////
		entidad.setAutosCa(dto.getAutosCa());
		entidad.setVidaCa(dto.getVidaCa());
		entidad.setDaniosCa(dto.getDaniosCa());
		entidad.setFormaPagoCa(dto.getFormaPagoCa());
		
		Oficina oficina = new Oficina();
		oficina.setId(this.dto.getOficinaId());
		entidad.setOficina(oficina);
		List<InformacionBancaria> informacionBancariaList = completeInformacionBancaria();
		entidad.setInformacionBancaria(informacionBancariaList);
//		return prestadorServicio;
	}
	
	@Action(value = "salvarDocBancaria", results = {
			@Result(name = SUCCESS, location = LOCATION_LISTADOINFOBANCARIAGRID_JSP),
			@Result(name = INPUT, location = LOCATION_LISTADOINFOBANCARIAGRID_JSP)})	
	public String salvarDocBancaria() {
		this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
		informacionBancaria.setEstatus(0);
		informacionBancaria.setFechaCreacion(new Date());
		informacionBancaria.setCodigoUsuarioCreacion(String.valueOf(usuarioService.getUsuarioActual().getNombreUsuario()));
		infoBancId = Integer.parseInt(entidadService.saveAndGetId(informacionBancaria).toString());
		entidad.getInformacionBancaria().add(informacionBancaria);
		entidadService.save(entidad);
		actualizaInformacionBancaria();
		return SUCCESS;
	}

	public String salvarTipoPrestador() {
		return null;
	}

	@Action(value = "mostrarTipoPrestador", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/prestadorservicio/tipoDePrestador.jsp"),
			@Result(name = INPUT, location = LOCATION_MOSTRARPRESTADORSERVICIO_JSP) })
	public String mostrarTipoPrestador() {
		if(this.prestadorId!=null && !this.prestadorId.equals("")){
			this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
			if(entidad.getTiposPrestador()!=null && entidad.getTiposPrestador().size()>0){
				for(TipoPrestadorServicio tipoPrestador : entidad.getTiposPrestador()){
					this.tiposPrestadorParaConsultar.add(tipoPrestador.getNombre());
				}
			}
		}
		return SUCCESS;
	}

	private void loadPrestadorServicio() {
		this.entidad = new PrestadorServicio();
	}

	private List<InformacionBancaria>  completeInformacionBancaria() {
		List<InformacionBancaria> newInformacionBancariaList = new ArrayList<InformacionBancaria>();
		// Ya existe ?
		String clabe = this.informacionBancaria.getClabe();
		String numeroCuenta = this.informacionBancaria.getNumeroCuenta();
		if (clabe != null && !clabe.equals("") && numeroCuenta != null && !numeroCuenta.equals("")) {
			
			if(this.dto.getNumPrestador() != null && !this.dto.getNumPrestador().equals("")){
//				this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
				List<InformacionBancaria> currentInformacionBancariaList = this.entidad.getInformacionBancaria();
				if(currentInformacionBancariaList!=null){
					for(InformacionBancaria infoBancaria : currentInformacionBancariaList){
						if(infoBancaria.getEstatus().intValue() == ESTATUS_INFOBANCARIA_ACTIVO){
							infoBancaria.setFechaModificacion(Calendar.getInstance().getTime());
							infoBancaria.setCodigoUsuarioModificacion(this.currentUser);
							infoBancaria.setEstatus(ESTATUS_INFOBANCARIA_INACTIVO);
						}
						newInformacionBancariaList.add(infoBancaria);
					}
				}
			}
			// Es nueva la InformacionBancaria
			this.informacionBancaria.setEstatus(ESTATUS_INFOBANCARIA_ACTIVO);
			this.informacionBancaria.setFechaCreacion(Calendar.getInstance()
					.getTime());
			this.informacionBancaria.setFechaModificacion(Calendar
					.getInstance().getTime());
			this.informacionBancaria.setCodigoUsuarioCreacion(this.currentUser);
			newInformacionBancariaList.add(this.informacionBancaria);
		}else if( this.entidad.getId()!=null ){
			newInformacionBancariaList = this.entidad.getInformacionBancaria();
		}
		return newInformacionBancariaList;
	}

//	private void agregaNombreEstatusEnPrestadorDeServicio() {
//		for (PrestadorServicioRegistro entidad : listPrestadorServicio) {
//			String estatusNombre = this.estatus.get(entidad.getEstatus().toString());
//			entidad.setEstatusNombre(estatusNombre);
//			entidad.getOficina().setOficinaNombre(
//					this.oficinas.get(entidad.getOficina().getId()));
//			//String tipoPrestadorStr = entidad.getTipoPrestadoresStr();
//			//System.out.println(tipoPrestadorStr);
//		}
//	}

	private List<TipoPrestadorServicio> createTipoPrestadorSelectedList() {
		List<TipoPrestadorServicio> tiposPrestadorSelectedList = new ArrayList<TipoPrestadorServicio>();
		String []strTipoPrestadorSelectedArray = this.strTipoPrestadorSelected.trim().replaceAll("\\s+", "").split(",");
		for (String tipoPrestadorId : strTipoPrestadorSelectedArray) {
			String tipoPrestadorStr = tipoPrestadorId.trim();
			List<TipoPrestadorServicio> tipoPrestadorServicioList =  this.entidadService.findByProperty(TipoPrestadorServicio.class, "nombre",tipoPrestadorStr);
			if(tipoPrestadorServicioList.size()>0){
				tiposPrestadorSelectedList.add(tipoPrestadorServicioList.get(0));
			}
			
		}
		return tiposPrestadorSelectedList;
	}

	private PersonaMidas completaYGuardaInformacionDePersona() {
		PersonaMidas personaMidas = null;
		if (this.dto.getTipoPersona().equals("PF")) {
			PersonaFisicaMidas personaFisica = new PersonaFisicaMidas();
			if(entidad.getId() != null){
				personaFisica = entidadService.findById(PersonaFisicaMidas.class, entidad.getPersonaMidas().getId() );
			}
			this.createPersonaFisica(personaFisica);
			personaMidas = personaFisica;
		} else if (this.dto.getTipoPersona().equals("PM")) {
			PersonaMoralMidas personaMoral = new PersonaMoralMidas();
			if(entidad.getId() != null){
				personaMoral = entidadService.findById(PersonaMoralMidas.class, entidad.getPersonaMidas().getId());
			}
			this.createPersonaMoral(personaMoral);
			personaMidas = personaMoral;
		}
		return personaMidas;
	}



	private DireccionMidas createDireccionMidas(DireccionMidas direccionMidas) {
		ColoniaMidas colonia = null;
		if(this.dto.getNombreColonia() != null){
		colonia = entidadService.findById(ColoniaMidas.class, this.dto.getNombreColonia());
	}
		if(direccionMidas.getId() == null){
			direccionMidas = new DireccionMidas(this.dto.getNombreCalle(), this.dto.getNombreCalleNumero(), 
				colonia, this.dto.getNombreColoniaDiferente(), 1, currentUser);
		}
		direccionMidas.setCalle(this.dto.getNombreCalle());
		direccionMidas.setNumExterior(this.dto.getNombreCalleNumero());
		direccionMidas.setColonia(colonia);
		direccionMidas.setOtraColonia(this.dto.getNombreColoniaDiferente());
		direccionMidas.setIdCodigoPostalOtraColonia(1);
		direccionMidas.setCodigoPostalColonia(this.dto.getCodigoPostal());
		direccionMidas.setNumInterior(this.dto.getNumeroInterno());
		direccionMidas.setReferencia(this.dto.getReferencia());
		direccionMidas.setCodigoUsuarioModificacion(currentUser);
		direccionMidas.setFechaModificacion(Calendar.getInstance().getTime());
		return direccionMidas;
	}

	private void  createPersonaMidas(PersonaMidas personaMidas) {
		personaMidas.setNombre(dto.getNombre());
		personaMidas.setTipoPersona(this.dto.getTipoPersona());
		personaMidas.setCveNacionalidad(dto.getIdPaisString());
		personaMidas.setActivo(Boolean.TRUE);
		personaMidas.setRfc(dto.getRfc());
		
		String[] rfc = getRFC(dto.getRfc());
		if(SystemCommonUtils.isNotNull(rfc)){
			personaMidas.setCodigoRfc(rfc[0]);
			personaMidas.setFechaRfc(rfc[1]);
			personaMidas.setHomoclaveRfc(rfc[2]);
		}
		
		if(personaMidas.getId() == null){
			personaMidas.setCodigoUsuarioCreacion(currentUser);
			personaMidas.setFechaCreacion(Calendar.getInstance().getTime());
		}
		
		personaMidas.setFechaModificacion(Calendar.getInstance().getTime());
		personaMidas.setCodigoUsuarioModificacion(currentUser);
//		personaMidas.setPersonaDireccion(new ArrayList<PersonaDireccionMidas>() );
		DireccionMidas direccion = new DireccionMidas();
		if(personaMidas.getId() != null){		
			List<PersonaDireccionMidas> personaDirecciones = entidadService.findByProperty(
					PersonaDireccionMidas.class, "persona.id", personaMidas.getId());
			if(personaDirecciones != null && personaDirecciones.size() > 0){
				direccion = personaDirecciones.get(0).getDireccion();
			}
			//direccion = entidadService.findById(DireccionMidas.class, personaMidas.getPersonaDireccion().get(0).getId().getDireccionId() );
		}
		direccion = createDireccionMidas(direccion);
		personaMidas.agregarDireccion(direccion, TIPO_DOMICILIO.OFICINA, currentUser);
		DatoContactoMidas contacto = new DatoContactoMidas();
		if(personaMidas.getId() != null){
			contacto = entidadService.findById(DatoContactoMidas.class, personaMidas.getContacto().getId());
		}
		personaMidas.setContacto(createDatoContacto(contacto));
		

	}

	private String[] getRFC(String str){
		String[] rfc = null;
		if (str.length() >= 12){
			int longFecha = 6;
			int longHomoclave = 3;
			int longCodigo = str.length() - longFecha - longHomoclave;
			rfc = new String[3];;
			rfc[0] = str.substring(0, longCodigo);
			rfc[1] = str.substring(longCodigo, longCodigo + longFecha);
			rfc[2] = str.substring(longCodigo + longFecha, longCodigo + longFecha + longHomoclave);
		}

		return rfc;
	}
	
	private void createPersonaFisica(PersonaFisicaMidas personaFisica) {
		this.createPersonaMidas(personaFisica);
		personaFisica.setNombrePersona(this.dto.getNombre());
		personaFisica.setApellidoPaterno(this.dto.getApellidoPaterno());
		personaFisica.setApellidoMaterno(this.dto.getApellidoMaterno());
		personaFisica.setFechaNacimiento(this.dto.getFechaNacimiento());
		personaFisica.setCurp(this.dto.getCurp());
		personaFisica.setCveSexo("M");
		personaFisica.setCveEstadoCivil("S");
		String nombrePersonaMidas = (this.dto.getNombre().concat(" ").concat(this.dto.getApellidoPaterno()).
			concat(" ").concat(!StringUtil.isEmpty(this.dto.getApellidoMaterno())?this.dto.getApellidoMaterno():"")).trim();
		personaFisica.setNombre(nombrePersonaMidas);
		
	}

	private void createPersonaMoral(PersonaMoralMidas personaMoral) {
		this.createPersonaMidas(personaMoral);
		personaMoral.setNombre(this.dto.getNombreComercial());
		personaMoral.setNombreRazonSocial(this.dto.getNombreDeLaEmpresa());
		personaMoral.setFechaConstruccion(Calendar.getInstance().getTime());
		personaMoral.setRepresentanteLegal(this.dto.getAdministrador());
	}

	private DatoContactoMidas createDatoContacto(DatoContactoMidas datoContacto) {
		if(datoContacto.getId() == null){
		datoContacto = new DatoContactoMidas(
				this.dto.getEmailPrincipal(), this.dto.getTelefono1(), this.dto.getTelefono1Lada(),
				null, currentUser);
		}
		datoContacto.setCorreoPrincipal(this.dto.getEmailPrincipal());
		datoContacto.setTelCasa(this.dto.getTelefono1());
		datoContacto.setTelCasaLada(this.dto.getTelefono1Lada());
		datoContacto.setTelOtro1Lada(this.dto.getTelefono2Lada());
		datoContacto.setTelOtro1(this.dto.getTelefono2());
		datoContacto.setTelCelularLada(this.dto.getCelularLada());
		datoContacto.setTelCelular(this.dto.getCelular());
		datoContacto.setTelOtro2Lada(this.dto.getOtrosLada());
		datoContacto.setTelOtro2(this.dto.getOtros());
		datoContacto.setCorreosAdicionales(this.dto.getEmailAdicional());
		datoContacto.setCodigoUsuarioModificacion(currentUser);
		datoContacto.setFechaModificacion(Calendar.getInstance().getTime());
		return datoContacto;
	}

	@Action(value = "mostrarInfoBancaria", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/prestadorservicio/contenedorInfoBancaria.jsp") })
	public String mostrarInfoBancaria() {
		return SUCCESS;
	}
	
	@Action(value = "actualizaInfoBancaria", results = { @Result(name = SUCCESS, location = LOCATION_LISTADOINFOBANCARIAGRID_JSP) })
	public String actualizaInfoBancaria() {
		actualizaInformacionBancaria();
		return SUCCESS;
	}
	
	private void  actualizaInformacionBancaria() {
		if (this.prestadorId != null && !this.prestadorId.equals("") && infoBancId!=null ) {
			this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
			List<InformacionBancaria> currentInformacionBancariaList = this.entidad.getInformacionBancaria();
			for(InformacionBancaria infoBancaria : currentInformacionBancariaList){
					if(infoBancaria.getId().intValue() == this.infoBancId.intValue()){
						infoBancaria.setFechaModificacion(Calendar.getInstance().getTime());
						infoBancaria.setCodigoUsuarioModificacion(this.currentUser);
						infoBancaria.setFechaModEstatus(Calendar.getInstance().getTime());
						infoBancaria.setEstatus(ESTATUS_INFOBANCARIA_ACTIVO);
					}else{
						if(infoBancaria.getEstatus().intValue() == ESTATUS_INFOBANCARIA_ACTIVO){
							infoBancaria.setFechaModificacion(Calendar.getInstance().getTime());
							infoBancaria.setCodigoUsuarioModificacion(this.currentUser);
							infoBancaria.setFechaModEstatus(Calendar.getInstance().getTime());
							infoBancaria.setEstatus(ESTATUS_INFOBANCARIA_INACTIVO);
						}
					}
			}
		}
		entidadService.save(this.entidad);
		this.listInfoBancaria = this.entidad.getInformacionBancaria();
		this.loadBankName(this.listInfoBancaria);
	}
	
	public void validateMostrarContenedorInfoBancaria(){
		this.informacionBancaria = new InformacionBancaria();
	}

	@Action(value = "mostrarContenedorInfoBancaria", results = { @Result(name = SUCCESS, location = LOCATION_LISTADOINFOBANCARIAGRID_JSP) })
	public String mostrarContenedorInfoBancaria() {
//		this.listInfoBancaria = entidadService
//				.findAll(InformacionBancaria.class);
		if(this.prestadorId!=null && !this.prestadorId.equals("")){
			this.entidad = entidadService.findById(PrestadorServicio.class, Integer.valueOf(this.prestadorId));
			if(entidad.getInformacionBancaria()!=null ){ 
				entidad.getInformacionBancaria().size();
				listInfoBancaria=entidad.getInformacionBancaria();
				this.loadBankName(listInfoBancaria);
				this.loadStatusName(listInfoBancaria);
			}else{
				listInfoBancaria=new ArrayList<InformacionBancaria>();
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarBusquedaRapida", results = { @Result(name = SUCCESS, location = LOCATION_BUSQUEDARAPIDA_JSP) })
	public String mostrarBusquedaRapida(){
		return SUCCESS;
	}
	
	@Action(value = "mostrarBusquedaRapidaGrid", results = { @Result(name = SUCCESS, location = LOCATION_BUSQUEDARAPIDA_GRID) })
	public String mostrarBusquedaRapidaGrid(){
		
		filtroCatalogo.setTipoPrestadorStr(tipoPrestador);
		filtroCatalogo.setEstatus(1);
		listPrestadorServicio = prestadorServicioService.buscar(this.filtroCatalogo);
		
		return SUCCESS;
	}
	
	@Action(value="exportarResultados",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})

	public String exportarAExcel(){
		filtroCatalogo.setNombrePersona(mx.com.afirme.midas.sistema.StringUtil.decodeUri(filtroCatalogo.getNombrePersona()));
		listPrestadorServicio = prestadorServicioService.buscar(this.filtroCatalogo);
		//this.agregaNombreEstatusEnPrestadorDeServicio();
		
		ExcelExporter exporter = new ExcelExporter(PrestadorServicioRegistro.class);
		transporte = exporter.exportXLS(listPrestadorServicio, "Prestadores de Servicios");
		
		return SUCCESS;		
	}
	
	@Action(value = "generarUsuarioPrestador", results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^claveUsuarioPrestador,mensaje,tipoMensaje"})})
	public String generarUsuarioPrestador(){
		try{
			if(!StringUtil.isEmpty(prestadorId)){
				UsuarioPrestadorServicio usuario =  prestadorServicioService.obtenerUsuarioPorPrestador(Integer.valueOf(prestadorId));
				if(usuario != null){
					claveUsuarioPrestador = usuario.getCodigoUsuario();
					throw new RuntimeException(getText("midas.prestadorservicio.usuario.existe"));
				}else{
					usuario = prestadorServicioService.generarUsuario(Integer.valueOf(prestadorId));
					claveUsuarioPrestador = usuario.getCodigoUsuario();
					this.setMensajeExitoPersonalizado(getText("midas.prestadorservicio.usuario.generar.exito"));
				}
			}else{				
				throw new RuntimeException(getText("midas.prestadorservicio.noexiste"));
			}	
		}catch(Exception ex){
			log.error(ex);
			this.setMensajeError(ex.getMessage());			
		}		
		return SUCCESS;
	}
	
	
	@Action(value = "actualizarUsuarioPrestador", results={ @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^claveUsuarioPrestador,mensaje,tipoMensaje"})})
	public String actualizarUsuarioPrestador(){
		try{
			if(true){
				throw new RuntimeException("En este momento no se encuentra activa la funcionalidad de actualizar usuario");
			}
			if(!StringUtil.isEmpty(prestadorId)){
				prestadorServicioService.actualizarUsuarioPrestador(Integer.valueOf(prestadorId), claveUsuarioPrestador);
				this.setMensajeExitoPersonalizado(getText("midas.prestadorservicio.usuario.actualizar.exito"));				
			}else{
				throw new RuntimeException(getText("midas.prestadorservicio.noexiste"));
			}		
		}catch(Exception ex){
			log.error(ex);
			this.setMensajeError(ex.getMessage());		
		}		
		return SUCCESS;
	}
	
	private void loadBankName(List<InformacionBancaria> listInfoBancaria){
		for(InformacionBancaria infoBancaria: listInfoBancaria){
			infoBancaria.setBankName(this.findBankName(infoBancaria.getBancoId()));
		}
	}
	
	private void loadStatusName(List<InformacionBancaria> listInfoBancaria){
		for(InformacionBancaria infoBancaria: listInfoBancaria){
			infoBancaria.setStatusName(this.estatus.get(infoBancaria.getEstatus().toString()));
		}
	}
	
	
	
	private String findBankName(Integer bankId){
		BancoMidas banco = entidadService.findById(BancoMidas.class, bankId.longValue());
		if(banco != null){
			return banco.getNombre();
		}
		return "BANCO DESCONOCIDO";
	}

	public Map<Long, String> getBancos() {
		return bancos;
	}

	public void setBancos(Map<Long, String> bancos) {
		this.bancos = bancos;
	}

	public boolean isConsulta() {
		return consulta;
	}

	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}

	public PrestadorServicio getEntidad() {
		return entidad;
	}

	public void setEntidad(PrestadorServicio entidad) {
		this.entidad = entidad;
	}

	public Map<String, String> getEstatus() {
		return estatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}

	public PrestadorServicioFiltro getFiltroCatalogo() {
		return filtroCatalogo;
	}

	public void setFiltroCatalogo(PrestadorServicioFiltro filtroCatalogo) {
		this.filtroCatalogo = filtroCatalogo;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public Map<String, String> getTiposPersona() {
		return tiposPersona;
	}

	public void setTiposPersona(Map<String, String> tiposPersona) {
		this.tiposPersona = tiposPersona;
	}

	

	public Map<String, String> getTiposPrestador() {
		return tiposPrestador;
	}

	public void setTiposPrestador(Map<String, String> tiposPrestador) {
		this.tiposPrestador = tiposPrestador;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public PrestadorDeServicioService getPrestadorServicioService() {
		return prestadorServicioService;
	}

	public void setPrestadorServicioService(
			PrestadorDeServicioService prestadorServicioService) {
		this.prestadorServicioService = prestadorServicioService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public PrestadorDeServicioDTO getDto() {
		return dto;
	}

	public void setDto(PrestadorDeServicioDTO dto) {
		this.dto = dto;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	/**
	 * @param informacionBancaria
	 *            the informacionBancaria to set
	 */
	public void setInformacionBancaria(InformacionBancaria informacionBancaria) {
		this.informacionBancaria = informacionBancaria;
	}

	/**
	 * @return the informacionBancaria
	 */
	public InformacionBancaria getInformacionBancaria() {
		return informacionBancaria;
	}

	/**
	 * @param listInfoBancaria
	 *            the listInfoBancaria to set
	 */
	public void setListInfoBancaria(List<InformacionBancaria> listInfoBancaria) {
		this.listInfoBancaria = listInfoBancaria;
	}

	/**
	 * @return the listInfoBancaria
	 */
	public List<InformacionBancaria> getListInfoBancaria() {
		return listInfoBancaria;
	}

//	public List<Long> getTipoPrestadorSelected() {
//		return tipoPrestadorSelected;
//	}
//
//	public void setTipoPrestadorSelected(List<Long> tipoPrestadorSelected) {
//		this.tipoPrestadorSelected = tipoPrestadorSelected;
//	}

	public String getCurrentUser() {
		return currentUser;
	}



	public String getStrTipoPrestadorSelected() {
		return strTipoPrestadorSelected;
	}

	public void setStrTipoPrestadorSelected(String strTipoPrestadorSelected) {
		this.strTipoPrestadorSelected = strTipoPrestadorSelected;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public String getPrestadorId() {
		return prestadorId;
	}

	public void setPrestadorId(String prestadorId) {
		this.prestadorId = prestadorId;
	}

	public List<String> getTiposPrestadorParaConsultar() {
		return tiposPrestadorParaConsultar;
	}

	public void setTiposPrestadorParaConsultar(
			List<String> tiposPrestadorParaConsultar) {
		this.tiposPrestadorParaConsultar = tiposPrestadorParaConsultar;
	}

	public Map<Long, String> getCiaSeguros() {
		return ciaSeguros;
	}

	public void setCiaSeguros(Map<Long, String> ciaSeguros) {
		this.ciaSeguros = ciaSeguros;
	}

	public DireccionMidasService getDireccionService() {
		return direccionService;
	}

	public void setDireccionService(DireccionMidasService direccionService) {
		this.direccionService = direccionService;
	}

	public Validator getValidator() {
		return validator;
	}

	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public Long getActiveInformacionBancaria() {
		return activeInformacionBancaria;
	}

	public void setActiveInformacionBancaria(Long activeInformacionBancaria) {
		this.activeInformacionBancaria = activeInformacionBancaria;
	}

	public Integer getInfoBancId() {
		return infoBancId;
	}

	public void setInfoBancId(Integer infoBancId) {
		this.infoBancId = infoBancId;
	}

	public boolean isModoAsignar() {
		return modoAsignar;
	}

	public void setModoAsignar(boolean modoAsignar) {
		this.modoAsignar = modoAsignar;
	}

	public boolean isModoEditar() {
		return modoEditar;
	}

	public void setModoEditar(boolean modoEditar) {
		this.modoEditar = modoEditar;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public List<PrestadorServicioRegistro> getListPrestadorServicio() {
		return listPrestadorServicio;
	}

	public void setListPrestadorServicio(
			List<PrestadorServicioRegistro> listPrestadorServicio) {
		this.listPrestadorServicio = listPrestadorServicio;
	}

	public String getTipoPrestador() {
		return tipoPrestador;
	}

	public void setTipoPrestador(String tipoPrestador) {
		this.tipoPrestador = tipoPrestador;
	}

	public String getClaveUsuarioPrestador() {
		return claveUsuarioPrestador;
	}

	public void setClaveUsuarioPrestador(String claveUsuarioPrestador) {
		this.claveUsuarioPrestador = claveUsuarioPrestador;
	}

	public Map<String, String> getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(Map<String, String> formaPago) {
		this.formaPago = formaPago;
	}

	public Map<Long, String> getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(Map<Long, String> tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}
	
}
