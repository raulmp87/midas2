package mx.com.afirme.midas2.action.catalogos;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class CatalogoAction extends BaseAction {
		
	private static final long serialVersionUID = 5037502892451856040L;

	public abstract String guardar();
		
	public abstract String eliminar();
		
	public abstract String listar();
	
	public abstract String listarFiltrado();
		
	public abstract String verDetalle();
	
	public EntidadService entidadService;
	
	public String tipoAccion;

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	@Autowired
	@Qualifier("entidadEJB")	
	public void setCatalogoService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	
}
