package mx.com.afirme.midas2.action.catalogos.condicionesespeciales;

import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;
import mx.com.afirme.midas2.dto.condicionespecial.VarAjusteCondicionDTO;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * 
 * @author Lizeth De La Garza
 *
 */

@Component
@Scope("prototype")
@Namespace("/catalogos/condicionespecial/variablesajuste")
public class VariableAjusteAction extends BaseAction implements Preparable{
	
	private static final long serialVersionUID = 1L;
	
	private Long idCondicionEspecial;
	
	private Long idVariableAjuste;
	
	private Long idValorVariableAjuste;
	
	private CondicionEspecial condicion;
	
	private String valorAjuste;
	
	private String nombreVariable;
	
	private List<EstatusCondicion>  listEstatus;
	
	private VarAjusteCondicionDTO variableAjusteDTO = new VarAjusteCondicionDTO();
	
	private List<VarAjusteCondicionDTO> variablesAsociadas;
	
	private List<VarAjusteCondicionDTO> variablesDisponibles;
		
	private boolean consulta;
	
	@Autowired
    @Qualifier("condicionEspecialServiceEJB")
	private CondicionEspecialService condicionEspecialService;

	@Override
	public void prepare() throws Exception {
		
		if (idCondicionEspecial != null) {
			condicion = condicionEspecialService.obtenerCondicion(idCondicionEspecial);
		}
		
		listEstatus = condicionEspecialService.obtenerEstatusDisponible(idCondicionEspecial);
		
	}
	
	@Action (value = "mostrarVariables", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/variablesAjuste.jsp") })
	public String mostrarCondicion() {
		
		
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerVariablesDisponibles", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/variablesAjusteDisponibles.jsp") })
	public String obtenerVariablesDisponibles() {
		
		if (!StringUtil.isEmpty(nombreVariable)) {
			variablesDisponibles = condicionEspecialService.obtenerVariablePorNombre(idCondicionEspecial, nombreVariable);
		} else {
			variablesDisponibles = condicionEspecialService.getLstVariablesDisponibles(idCondicionEspecial);
		}
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerVariablesAsociadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/variablesAjusteAsociadas.jsp") })
	public String obtenerVariablesAsociadas() {
		variablesAsociadas = condicionEspecialService.getLstVariablesAsociadas(idCondicionEspecial);
		return SUCCESS;
	}
	
	@Action
	(value = "accionVariableAjuste", results = { 
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp") })
	public String accionVariableAjuste() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		
		if (accion != null && accion.equals("inserted")) {
			condicionEspecialService.guardarVariableAjuste(variableAjusteDTO.getVariableAjuste().getId(), idCondicionEspecial);
		} else if (accion != null && accion.equals("deleted")) {
			condicionEspecialService.eliminarVariableAjuste(variableAjusteDTO.getVariableAjuste().getId(), idCondicionEspecial);
		}
		return SUCCESS;
	}
	
	@Action
	(value = "asociarTodasVariablesAjuste", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/variablesAjuste.jsp") })
	public String asociarTodasVariablesAjuste() {
		
		condicionEspecialService.asociarVariablesAjuste(idCondicionEspecial, nombreVariable);
		return SUCCESS;
	}
	
	@Action
	(value = "eliminarTodasVariablesAjuste", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/variablesAjuste.jsp") })
	public String eliminarTodasVariablesAjuste() {
		
		condicionEspecialService.eliminarVariablesAjuste(idCondicionEspecial);
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarValorVariable", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/valorVariableAjuste.jsp") })
	public String mostrarValorVariable() {
		
		VariableAjuste variable = new VariableAjuste();
		variable.setId(idVariableAjuste);
		variableAjusteDTO.setVariableAjuste(variable);
		variableAjusteDTO.setIdCondicionEspecial(idCondicionEspecial);
		
		return SUCCESS;
	}
	
	
	@Action
	(value = "actualizarValorVariable", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/valorVariableAjuste.jsp") })
	public String actualizarValorVariable() {
		
		condicionEspecialService.guardarValorVariable(idCondicionEspecial, idVariableAjuste, idValorVariableAjuste, valorAjuste );
		
		//idVariableAjuste = condicionEspecialService.obtenerIdVariablePorValor(idValorVariableAjuste);
		
		VariableAjuste variable = new VariableAjuste();
		variable.setId(idVariableAjuste);
		variableAjusteDTO.setVariableAjuste(variable);
		variableAjusteDTO.setIdCondicionEspecial(idCondicionEspecial);		
		
		return SUCCESS;
	}
	
	@Action (value = "salir", results = { 
			@Result(name = SUCCESS, type="redirectAction", 
					params={"actionName","mostrarContenedor",
					"namespace","/catalogos/condicionespecial"})})
	public String salir() {
		
		return SUCCESS;
	}
	
	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}	

	public Long getIdVariableAjuste() {
		return idVariableAjuste;
	}

	public void setIdVariableAjuste(Long idVariableAjuste) {
		this.idVariableAjuste = idVariableAjuste;
	}	

	public Long getIdValorVariableAjuste() {
		return idValorVariableAjuste;
	}

	public void setIdValorVariableAjuste(Long idValorVariableAjuste) {
		this.idValorVariableAjuste = idValorVariableAjuste;
	}

	public CondicionEspecial getCondicion() {
		return condicion;
	}

	public void setCondicion(CondicionEspecial condicion) {
		this.condicion = condicion;
	}	

	public String getValorAjuste() {
		return valorAjuste;
	}

	public void setValorAjuste(String valorAjuste) {
		this.valorAjuste = valorAjuste;
	}	

	public String getNombreVariable() {
		return nombreVariable;
	}

	public void setNombreVariable(String nombreVariable) {
		this.nombreVariable = nombreVariable;
	}

	public List<EstatusCondicion> getListEstatus() {
		return listEstatus;
	}

	public void setListEstatus(List<EstatusCondicion> listEstatus) {
		this.listEstatus = listEstatus;
	}	

	public boolean isConsulta() {
		return consulta;
	}

	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}	

	public VarAjusteCondicionDTO getVariableAjusteDTO() {
		return variableAjusteDTO;
	}

	public void setVariableAjusteDTO(VarAjusteCondicionDTO variableAjusteDTO) {
		this.variableAjusteDTO = variableAjusteDTO;
	}

	public List<VarAjusteCondicionDTO> getVariablesAsociadas() {
		return variablesAsociadas;
	}

	public void setVariablesAsociadas(List<VarAjusteCondicionDTO> variablesAsociadas) {
		this.variablesAsociadas = variablesAsociadas;
	}

	public List<VarAjusteCondicionDTO> getVariablesDisponibles() {
		return variablesDisponibles;
	}

	public void setVariablesDisponibles(
			List<VarAjusteCondicionDTO> variablesDisponibles) {
		this.variablesDisponibles = variablesDisponibles;
	}	
	
	

}
