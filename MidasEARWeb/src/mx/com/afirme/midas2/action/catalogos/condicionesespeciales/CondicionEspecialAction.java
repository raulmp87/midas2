package mx.com.afirme.midas2.action.catalogos.condicionesespeciales;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * 
 * @author Lizeth De La Garza
 *
 */


@Component
@Scope("prototype")
@Namespace("/catalogos/condicionespecial")
public class CondicionEspecialAction   extends BaseAction implements Preparable {
	
	private static final int	LONGITUD_BUSQUEDA_AUTOCOMPLETADO	= 3;

	private static final long serialVersionUID = 1L;	
	
	public static final Short VISTA_CONDICIONES = 1;
	
	public static final Short VISTA_RANKING = 2;

	private CondicionEspecialDTO condicionEspecialFiltro;
	
	private List<EstatusCondicion>  listEstatus;
	
	private List<CondicionEspecial> listCondiciones;
	
	private Long idCondicionEspecial;
	
	private Short estatus;
	
	private Short tipoVista;
	
	private boolean consulta;
	
	private String nombreCondicion;
	
	private TransporteImpresionDTO transporte;
	
	private String nombreCondicionEspecial;
	
	private Boolean muestraCondiciones;
	
	private Map<String, Long> condicionesAutocomplete = new HashMap<String, Long>();
	
	private List<CondicionEspecial> resultadosCondicion;
	
	@Autowired
    @Qualifier("condicionEspecialServiceEJB")
	private CondicionEspecialService condicionEspecialService;
	
	@Override
	public void prepare() throws Exception {
		listEstatus = Arrays.asList(EstatusCondicion.values()); 		
	}
	
	@Action (value = "mostrarContenedor", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/contenedorCondiciones.jsp") })
	public String mostrarContenedor() {	
		
		return SUCCESS;
		
	}

	@Action (value = "mostrarCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/condicionEspecialListado.jsp") })
	public String mostrarCondiciones() {	
		tipoVista = VISTA_CONDICIONES;
		return SUCCESS;
		
	}
	
	@Action (value = "mostrarRanking", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/condicionEspecialListado.jsp") })
	public String mostrarRanking() {	
		
		tipoVista = VISTA_RANKING;
		return SUCCESS;
		
	}
	
	@Action (value = "buscarCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/condicionEspecialGrid.jsp") })
	public String buscarCondiciones() {	
		listCondiciones = condicionEspecialService.obtenerCondiciones(condicionEspecialFiltro);
		return SUCCESS;
		
	}
	
	
	@Action (value = "buscarCondicionAutocompletado", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/resultadoAutoCompCondicion.jsp") })
	public String buscarCondicionAutocompletado() {	
		if (nombreCondicion != null && nombreCondicion.length() > LONGITUD_BUSQUEDA_AUTOCOMPLETADO) {
			resultadosCondicion = condicionEspecialService.obtenerCondicionesNombreCodigo(nombreCondicion);

		}
		return SUCCESS;
		
	}
	
	@Action (value = "buscarRanking", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/condicionEspecialGrid.jsp") })
	public String buscarRanking() {	
		return SUCCESS;
		
	}
	
	@Action (value = "modificarEstatus", results = { 
			@Result(name = SUCCESS, type="redirectAction", 
					params={"actionName","mostrarContenedor",
					"namespace","/catalogos/condicionespecial"
					}) ,
			@Result(name = INPUT, location = "/jsp/catalogos/condicionesespeciales/condicionEspecialListado.jsp")})
	public String modificarEstatus() {	
		condicionEspecialService.modificarEstatus(idCondicionEspecial, estatus);
		return SUCCESS;		
	}

	@Action (value = "mostrarContenedorCatalogo", results = { 
			@Result(name = SUCCESS, 
					params={
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}"
					} ,
					location = "/jsp/catalogos/condicionesespeciales/contenedorCondicionEspecial.jsp") })
	public String mostrarContenedorCatalogo() {	
		
		return SUCCESS;
		
	}

	/**
	 * Manda a exportar en un documento PDF la información de la condición especial
	 * @return
	 */
	@Action(value="imprimirCondicion",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporte.contentType}",
					"inputName","transporte.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporte.fileName}\""})})
	public String imprimirCondicion(){
		try{
			transporte = condicionEspecialService.imprimirCondicionEspecial(idCondicionEspecial);
			transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
			transporte.setContentType("application/pdf");
			CondicionEspecial condicion = condicionEspecialService.obtenerCondicion(idCondicionEspecial);
			
			String fileName = condicion.getCodigo()+ "_" + condicion.getNombre() + "_" + UtileriasWeb.getFechaString(new Date()) + ".pdf";
			transporte.setFileName(fileName);
		}
		catch(Exception e){

		}
		return SUCCESS;
	}
	
	@Action (value = "mostrarCopiarCondEspecial", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/confirmacionCopiaCondicionEspecial.jsp") })
	public String mostrarCopiarCondEspecial() {	
		nombreCondicionEspecial = "";
		return SUCCESS;		
	}
	
	@Action (value = "copiarCondicionEspecial", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/condicionesespeciales/confirmacionCopiaCondicionEspecial.jsp") })
	public String copiarCondicionEspecial() {	
		if(nombreCondicionEspecial != null && !nombreCondicionEspecial.equals("")){
			try{
				CondicionEspecial condicionEspecial = condicionEspecialService.copiarCondicionEspecial(idCondicionEspecial, nombreCondicionEspecial);
				setMensaje(MensajeDTO.MENSAJE_COPIAR_COND_ESP + condicionEspecial.getCodigo()+ " - " +condicionEspecial.getNombre());
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			}catch (Exception e){
				e.printStackTrace();
				setMensaje(MensajeDTO.MENSAJE_ERROR_COPIAR_COTIZACION);
				setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			}
		}else{
			setMensaje("El nombre es requerido");
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
		}
		return SUCCESS;	
	}
	
	public CondicionEspecialDTO getCondicionEspecialFiltro() {
		return condicionEspecialFiltro;
	}

	public void setCondicionEspecialFiltro(
			CondicionEspecialDTO condicionEspecialFiltro) {
		this.condicionEspecialFiltro = condicionEspecialFiltro;
	}

	public List<EstatusCondicion> getListEstatus() {
		return listEstatus;
	}

	public void setListEstatus(List<EstatusCondicion> listEstatus) {
		this.listEstatus = listEstatus;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}

	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}

	public List<CondicionEspecial> getListCondiciones() {
		return listCondiciones;
	}

	public void setListCondiciones(List<CondicionEspecial> listCondiciones) {
		this.listCondiciones = listCondiciones;
	}

	public Short getTipoVista() {
		return tipoVista;
	}

	public void setTipoVista(Short tipoVista) {
		this.tipoVista = tipoVista;
	}

	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	public boolean isConsulta() {
		return consulta;
	}

	public void setConsulta(boolean consulta) {
		this.consulta = consulta;
	}

	public String getNombreCondicion() {
		return nombreCondicion;
	}

	public void setNombreCondicion(String nombreCondicion) {
		this.nombreCondicion = nombreCondicion;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	public String getNombreCondicionEspecial() {
		return nombreCondicionEspecial;
	}

	public void setNombreCondicionEspecial(String nombreCondicionEspecial) {
		this.nombreCondicionEspecial = nombreCondicionEspecial;
	}

	public Boolean getMuestraCondiciones() {
		return muestraCondiciones;
	}

	public void setMuestraCondiciones(Boolean muestraCondiciones) {
		this.muestraCondiciones = muestraCondiciones;
	}
	
	public Map<String, Long> getCondicionesAutocomplete() {
		return condicionesAutocomplete;
	}

	public void setCondicionesAutocomplete(Map<String, Long> condicionesAutocomplete) {
		this.condicionesAutocomplete = condicionesAutocomplete;
	}

	public List<CondicionEspecial> getResultadosCondicion() {
		return resultadosCondicion;
	}

	public void setResultadosCondicion(List<CondicionEspecial> resultadosCondicion) {
		this.resultadosCondicion = resultadosCondicion;
	}

}