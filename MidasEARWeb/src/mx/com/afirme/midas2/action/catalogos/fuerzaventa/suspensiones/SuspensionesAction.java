package mx.com.afirme.midas2.action.catalogos.fuerzaventa.suspensiones;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.SuspensionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.SuspensionAgenteService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/suspensiones")
public class SuspensionesAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6646996742324449155L;
	private SuspensionAgente suspension;
	private SuspensionAgente filtrar;
	private List<SuspensionAgente> listaSolicitudesDeSuspension = new ArrayList<SuspensionAgente>();
	private List<ValorCatalogoAgentes> listaEstatusMovimiento = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaEstatusAgente = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaMotivoEstatus = new ArrayList<ValorCatalogoAgentes>();
	private SuspensionAgenteService suspensionService;
	private final String SUSPENSIONMOSTRARCONTENEDOR = "/jsp/catalogos/fuerzaventa/suspensiones/suspensionesCatalogo.jsp";
	private final String SUSPENSIONESVERDETALLE = "/jsp/catalogos/fuerzaventa/suspensiones/suspensionesDetalle.jsp";
	private final String SUSPENSIONESGRID = "/jsp/catalogos/fuerzaventa/suspensiones/suspensionesGrid.jsp";
	private String nombreUsuarioActual;
	private FortimaxService fortimaxService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private String urlIfimax;
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private List<EntregoDocumentosView> listaDocumentosFortimaxView = new ArrayList<EntregoDocumentosView>();
	private AgenteMidasService agenteMidasService;
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	private String situacionAgenteString;
	
	@Override
	public void prepare() throws Exception {
		listaEstatusMovimiento = cargarCatalogo("Estatus de Suspension de Agentes");
		listaEstatusAgente = cargarCatalogo("Estatus de Agente (Situacion)");		
		listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente");
	}

	public void prepareMostrarContenedor(){
		
	}
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=SUSPENSIONMOSTRARCONTENEDOR)
		})
		public String mostrarContenedor(){
			return SUCCESS;
		}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",					
					"namespace","/fuerzaventa/suspensiones",
					"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"suspension.id","${suspension.id}",
					"idTipoOperacion","${idTipoOperacion}",
					"idRegistro","${suspension.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/suspensiones","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${suspension.id}"})
	})
	@Override
	public String guardar() {
		try {
//			Agente agente = agenteMidasService.findByClaveAgente(suspension.getAgente());
			
			suspension=suspensionService.guardarSolicitudDeSuspension(suspension);
			//se obtienen los documentos requeridos para la solicitud de suspension
//			List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpeta("AGENTES", "SUSPENSIONES");
//			//se crea la estructura del agente en fortimax
//			String []respExp=agenteMidasService.generateExpedientAgent(suspension.getAgente().getId());
//			
//			 /*lista de documentos que estan dados de alta en el portal de fortimax*/
//			String[] documentosFortimax = fortimaxService.getDocumentFortimax(suspension.getAgente().getId(),"AGENTES");
//			//se crean los documentos en fortimax 
//			for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
//				if(doc!=null){
//					String []respDoc=fortimaxService.generateDocument(suspension.getAgente().getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+suspension.getAgente().getId()+"_Solicitud_"+suspension.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
//					if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")||respDoc[0].toString()!="false"){
//						DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
//						docFortimax.setCatalogoDocumentoFortimax(doc);
//						docFortimax.setExisteDocumento(0);
//						docFortimax.setIdRegistro(suspension.getAgente().getId());
//						documentoEntidadService.save(docFortimax);
//					}
//				}
//			}			
//			

			onSuccess();
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="listar",results={
			@Result(name=SUCCESS,location=SUSPENSIONESGRID),
			@Result(name=INPUT,location=SUSPENSIONESGRID)
		})
	@Override
	public String listar() {
		try {
			listaSolicitudesDeSuspension = suspensionService.findByFiltersView(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=SUSPENSIONESGRID),
			@Result(name=INPUT,location=SUSPENSIONESGRID)
		})
	@Override
	public String listarFiltrado() {
		try {
			listaSolicitudesDeSuspension = suspensionService.findByFiltersView(filtrar);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	
	@Action(value="verDetalle",results={
		@Result(name=SUCCESS,location=SUSPENSIONESVERDETALLE)
	})
	@Override
	public String verDetalle() {
		try {
			
			String usuario=(getUsuarioActual()!=null)?getUsuarioActual().getNombreUsuario():"";
			nombreUsuarioActual = usuario;
			
			if(suspension!=null && suspension.getId()!=null){
				suspension = suspensionService.loadById(suspension.getId());
				setTipoAccion("2");
			
//				Agente agente = new Agente();
//				agente=agenteMidasService.loadById(suspension.getAgente());		
//				PersonaSeycosDTO personaSeycos = personaSeycosFacade.findById(agente.getPersona().getIdPersona());			
//				Persona per = new Persona();
//				per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
//				per.setIdPersona(personaSeycos.getIdPersona());
//				agente.setPersona(per);
//	
//				String []respExp=agenteMidasService.generateExpedientAgent(agente.getId());
//				if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(agente.getId(), "AGENTES","SUSPENSIONES")){
//					List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorAplicacionTipoPersona("AGENTES", per.getClaveTipoPersona());
//						for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
//							if(doc!=null){
//								String []respDoc=fortimaxService.generateDocument(agente.getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+agente.getId(), doc.getCarpeta().getNombreCarpetaFortimax());
//								if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
//									DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
//									docFortimax.setCatalogoDocumentoFortimax(doc);
//									docFortimax.setExisteDocumento(0);
//									docFortimax.setIdRegistro(agente.getId());
//									documentoEntidadService.save(docFortimax);
//								}
//							}
//						}			
//				}
//				else{
//					documentoEntidadService.sincronizarDocumentos(suspension.getAgente().getId(), "AGENTES", "SUSPENSIONES");
//				}
//				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(suspension.getAgente().getId(), "AGENTES", "SUSPENSIONES");
		
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="autorizarSuspension",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",					
					"namespace","/fuerzaventa/suspensiones",
					"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"suspension.id","${suspension.id}",
					"idTipoOperacion","${idTipoOperacion}",
					"idRegistro","${suspension.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/suspensiones","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${suspension.id}"})
	})	
	public String autorizarSuspension() {
		try {
			suspension = suspensionService.autorizaSolicitudDeSuspension(suspension);
			Long idAgente= suspension.getAgente().getId();
			String usuario=(getUsuarioActual()!=null)?getUsuarioActual().getNombreUsuario():"";
			guardarHistorico(TipoOperacionHistorial.AGENTE, idAgente,"midas.agente.historial.accion.Agente.suspension",TipoAccionHistorial.CAMBIO);// UtileriasWeb.getMensajeRecurso("mx.com.afirme.midas.RecursoDeMensajes", "midas.agente.historial.accion.Agente.suspension",idAgente,usuario), TipoAccionHistorial.CAMBIO);
			tipoAccion="2"; 
			onSuccess();
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="rechazarSuspension",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",					
					"namespace","/fuerzaventa/suspensiones",
					"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"suspension.id","${suspension.id}",
					"idTipoOperacion","${idTipoOperacion}",
					"idRegistro","${suspension.id}"}),			
		    @Result(name=INPUT,type="redirectAction",
				params={"actionName","verDetalle","namespace","/fuerzaventa/suspensiones","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${suspension.id}"})
		})	
		public String rechazarSuspension() {
			try {
				suspension = suspensionService.rechazaSolicitudDeSuspension(suspension);
				tipoAccion="2"; 
				onSuccess();
			} catch (Exception e) {
				onError(e);
			}
			return SUCCESS;
		}
	
	
	@Action(value="generarLigaIfimax",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp")
		})
	public String generarLigaIfimax(){
		
		Agente agente = new Agente();
		agente=agenteMidasService.loadById(suspension.getAgente());		
		PersonaSeycosDTO personaSeycos;
		try {
			personaSeycos = personaSeycosFacade.findById(agente.getPersona().getIdPersona());
			Persona per = new Persona();
			per.setClaveTipoPersona(personaSeycos.getClaveTipoPersona().longValue());
			per.setIdPersona(personaSeycos.getIdPersona());
			agente.setPersona(per);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}			
		
		
		String []resp= new String[3];
		try {		
//			List<CatalogoDocumentoFortimax> docsCarpeta=documentoCarpetaService.obtenerDocumentosRequeridosPorCarpetaConTipoPersona("AGENTES", "SUSPENSIONES", agente.getPersona().getClaveTipoPersona());		
//			resp=fortimaxService.generateLinkToDocument(agente.getId(),"AGENTES", docsCarpeta.get(0).getNombreDocumentoFortimax()+"_"+agente.getId());
			resp=fortimaxService.generateLinkToDocument(agente.getId(),"AGENTES", "");
			urlIfimax=resp[0];		
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
	}	
	@Action(value="matchDocumentos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchDocumentos() {
			try {				
				documentoEntidadService.sincronizarDocumentos(suspension.getAgente().getId(), "AGENTES", "SUSPENSIONES");
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(suspension.getAgente().getId(), "AGENTES", "SUSPENSIONES");
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
	
	
	@Action(value="mostrarMotEstatusXSituacionAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaMotivoEstatus.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaMotivoEstatus.*"})
		})
	public String mostrarMotEstatusXSituacionAgente(){

		if("PENDIENTE POR AUTORIZAR".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","ALTA");
		}
		if("AUTORIZADO".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","REGULAR");
		}
		if("RECHAZADO".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","NUEVO","INFORMACION INCOMPLETA");
		}
		if("BAJA".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","SOLICITUD DEL AGENTE","ALTA SINIESTRALIDAD","BAJA PRODUCTIVIDAD","AUDITORIA","CEDULA VENCIDA","SOLICITUD DE AUTORIDAD","DEFUNCIÓN");
		}
		if("INACTIVO".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","SOLICITUD DE LA EMPRESA");
		}
		if("RECHAZADO CON EMISION".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","SIN MOTIVO");
		}
		if("SUSPENDIDO".equals(situacionAgenteString)){
			listaMotivoEstatus = cargarCatalogo("Motivo de Estatus del Agente","AUDITORIA - ACTO IRREGULAR","AUDITORIA - REVISION PREVENTIVA","CEDULA VENCIDA","BAJA PRODUCTIVIDAD","DEFUNCIÓN","ALTA SINIESTRALIDAD");
		}
		
		return SUCCESS;
	}
	
	@Action(value="generarDocSuspensionEnFortimax",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"})
	})
	public String generarDocSuspensionEnFortimax(){
		try {
			suspensionService.crearYGenerarDocumentosFortimaxSuspension(suspension);
			listaDocumentosFortimaxView = suspensionService.consultaEstatusDocumentosSuspension(suspension.getId(),suspension.getAgente().getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return SUCCESS;
	}
	/*************************************************************
	 *  metodo para consultar los documentos que estan guardados en la base de datos y armar html para
	 *  saber si ya estan digitalizados
	 * *********************************************************************/
	@Action(value="mostrarDocumentosADigitalizar",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
	})
	public String mostrarDocumentosADigitalizar(){
		try {
			listaDocumentosFortimaxView = suspensionService.consultaEstatusDocumentosSuspension(suspension.getId(),suspension.getAgente().getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return SUCCESS;
	}
	@Action(value="generarLigaIfimaxSuspension",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp"),
			@Result(name="errorExpediente",location=ERROR)
		})
	public String generarLigaIfimaxSuspension(){
		String []resp= new String[3];		
		
		try {			
	
			resp=fortimaxService.generateLinkToDocument(suspension.getAgente().getId(),"AGENTES", "");			
			urlIfimax=resp[0];	
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(resp[2].contains("No existe el Expediente")){
			setMensaje(resp[2]);
			return "errorExpediente";
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
	}	
	

	@Action(value="auditarDocumentosSuspensionDigitalizar",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"})
		})
		public String auditarDocumentosSuspensionDigitalizar() {
			try {				
				
				suspensionService.auditarDocumentosEntregadosSuspension(suspension.getId(),suspension.getAgente().getId(),"AGENTES");

				listaDocumentosFortimaxView = suspensionService.consultaEstatusDocumentosSuspension(suspension.getId(),suspension.getAgente().getId());
				
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
	/**********gets and sets***********/
	public SuspensionAgente getSuspension() {
		return suspension;
	}

	public void setSuspension(SuspensionAgente suspension) {
		this.suspension = suspension;
	}

	public SuspensionAgente getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(SuspensionAgente filtrar) {
		this.filtrar = filtrar;
	}
	
	public List<SuspensionAgente> getListaSolicitudesDeSuspension() {
		return listaSolicitudesDeSuspension;
	}

	public void setListaSolicitudesDeSuspension(
			List<SuspensionAgente> listaSolicitudesDeSuspension) {
		this.listaSolicitudesDeSuspension = listaSolicitudesDeSuspension;
	}
	
	public List<ValorCatalogoAgentes> getListaEstatusMovimiento() {
		return listaEstatusMovimiento;
	}

	public void setListaEstatusMovimiento(
			List<ValorCatalogoAgentes> listaEstatusMovimiento) {
		this.listaEstatusMovimiento = listaEstatusMovimiento;
	}	

	public List<ValorCatalogoAgentes> getListaEstatusAgente() {
		return listaEstatusAgente;
	}

	public void setListaEstatusAgente(List<ValorCatalogoAgentes> listaEstatusAgente) {
		this.listaEstatusAgente = listaEstatusAgente;
	}
	

	public List<ValorCatalogoAgentes> getListaMotivoEstatus() {
		return listaMotivoEstatus;
	}

	public void setListaMotivoEstatus(List<ValorCatalogoAgentes> listaMotivoEstatus) {
		this.listaMotivoEstatus = listaMotivoEstatus;
	}


	@Autowired
	@Qualifier("suspensionAgenteEJB")
	public void setSuspensionService(SuspensionAgenteService suspensionService) {
		this.suspensionService = suspensionService;
	}

	public String getNombreUsuarioActual() {
		return nombreUsuarioActual;
	}

	public void setNombreUsuarioActual(String nombreUsuarioActual) {
		this.nombreUsuarioActual = nombreUsuarioActual;
	}
	
	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}

	public String getUrlIfimax() {
		return urlIfimax;
	}

	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}

	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public void setListaDocumentosFortimax(	List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}
	
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}

	public String getSituacionAgenteString() {
		return situacionAgenteString;
	}

	public void setSituacionAgenteString(String situacionAgenteString) {
		this.situacionAgenteString = situacionAgenteString;
	}
	
	public List<EntregoDocumentosView> getListaDocumentosFortimaxView() {
		return listaDocumentosFortimaxView;
	}

	public void setListaDocumentosFortimaxView(
			List<EntregoDocumentosView> listaDocumentosFortimaxView) {
		this.listaDocumentosFortimaxView = listaDocumentosFortimaxView;
	}
}
