/**
 * 
 */
package mx.com.afirme.midas2.action.catalogos.solicitudsuspensionservicio;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudsuspensionservicio.SolicitudSuspensionServicio;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudsuspensionservicio.CatSolicitudSuspensionServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudsuspensionservicio.CatSolicitudSuspensionServicioService.SolicitudSuspensionServicioFiltro;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author simavera
 * 
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/catalogos/suspensionServicio")
public class CatSolicitudSuspensionServicioAction extends CatalogoAction implements Preparable {

	private static final long serialVersionUID = -1606944611018560269L;

	private List<SolicitudSuspensionServicio> listSuspensionServicios;
	private Long idSuspensionServicio;
	private Long tipoVentana;
	private Map<String, String> listEstatus;
	private Map<Long, String> listOficinas;
	private SolicitudSuspensionServicio entidad;
	private SolicitudSuspensionServicioFiltro filtroCatalogo;
	private TransporteImpresionDTO transporte; 
	private String namespace;
	private String methodName;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	@Autowired
	@Qualifier("solicitudSuspensionServicioServiceEJB")
	private CatSolicitudSuspensionServicioService catSolicitudSuspensionServicioService;

	@Autowired
	@Qualifier("catalogoSiniestroServiceEJB")
	private CatalogoSiniestroService catalogoSiniestroService;

	@Override
	public void prepare() throws Exception {
		
		this.listEstatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_SUSPENSION_SERVICIO);
		this.listOficinas = listadoService.obtenerOficinasSiniestros();
		this.filtroCatalogo = new SolicitudSuspensionServicioFiltro();
	}

	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/solicitudsuspensionservicio/contenedorListadoSolicitudSuspensionServicio.jsp") })
	public String mostrarContenedor() {
		return SUCCESS;
	}

	@Action(value = "buscar", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/solicitudsuspensionservicio/listadoSolicitudSuspensionServicioGrid.jsp") })
	public String buscar() {
		listSuspensionServicios = catSolicitudSuspensionServicioService.buscar( this.filtroCatalogo );
		loadEstatusName();
		return SUCCESS;
	}

	private void loadEstatusName() {
		for (SolicitudSuspensionServicio element : listSuspensionServicios) {
			element.setEstatusName( this.listEstatus.get(element.getEstatus().toString()) );
		}
	}

	@Action(value = "mostrarSuspensionServicio", results = { @Result(name = SUCCESS, location = "/jsp/siniestros/catalogo/solicitudsuspensionservicio/contenedorSolicitudSuspensionServicio.jsp") })
	public String mostrarSuspensionServicio() {
		this.entidad = this.entidadService.findById( SolicitudSuspensionServicio.class, this.idSuspensionServicio );
		return SUCCESS;
	}

	@Action(value = "guardar", results = {
			@Result(name = INPUT, location = "/jsp/siniestros/catalogo/solicitudsuspensionservicio/contenedorSolicitudSuspensionServicio.jsp"),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarSuspensionServicio", "namespace",
					"/siniestros/catalogos/suspensionServicio",
					"idSuspensionServicio", "${idSuspensionServicio}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}) })
	@Override
	public String guardar() {
		try {		
			SolicitudSuspensionServicio entidadAGuardar = this.completaInformaciondeEntidad();
			catSolicitudSuspensionServicioService.save(entidadAGuardar);
			idSuspensionServicio = entidadAGuardar.getId();
			super.setMensajeExito();			
		} catch (Exception ex) {
			if (ex.getCause()instanceof ConstraintViolationException) {
				super.setMensajeError(MENSAJE_ERROR_VALDACION);
				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
				addErrors(constraintViolationException.getConstraintViolations(),"entidad");
			}	
			return INPUT;
		}
		return SUCCESS;
	}

	private SolicitudSuspensionServicio completaInformaciondeEntidad() {
		String codigoUsuario = this.catSolicitudSuspensionServicioService.findCurrentUser();
		SolicitudSuspensionServicio entidadEnBD = null;
		
		// Valida que NO sea una entidad nueva
		if ( this.idSuspensionServicio != null && this.idSuspensionServicio != 0 ) {
			entidadEnBD = this.entidadService.findById(	SolicitudSuspensionServicio.class, this.idSuspensionServicio );
			entidadEnBD.setFechaModificacion(Calendar.getInstance().getTime());
			Integer currentStatus = entidadEnBD.getEstatus();
			// Valida si se modificó el estatus
			if (this.entidad.getEstatus().intValue() != currentStatus.intValue()) {
				entidadEnBD.setFechaModEstatus(Calendar.getInstance().getTime());
				entidadEnBD.setCodigoUsuarioModificacion(codigoUsuario);
			}
			entidadEnBD.setNumeroSerie(this.entidad.getNumeroSerie());
			entidadEnBD.setOficina(this.entidad.getOficina());
			entidadEnBD.setEstatus(this.entidad.getEstatus());
			entidadEnBD.setFechaInicio(this.entidad.getFechaInicio());
			entidadEnBD.setFechaFin(this.entidad.getFechaFin());
			entidadEnBD.setMotivo(this.entidad.getMotivo());
			this.entidad.getKey();
		} else {
			// Se agrega la informacion para entidad nueva
			entidadEnBD = this.entidad;
			entidadEnBD.setCodigoUsuarioCreacion(codigoUsuario);
			entidadEnBD.setFechaCreacion(Calendar.getInstance().getTime());
			entidadEnBD.setFechaModificacion(Calendar.getInstance().getTime());
		}
		return entidadEnBD;
	}
	
	@Action(value="exportarResultados",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})
	public String exportarAExcel(){
		
		this.listSuspensionServicios = catSolicitudSuspensionServicioService.buscar( this.filtroCatalogo );
		
		this.agregaNombreEstatus();
		
		ExcelExporter exporter = new ExcelExporter(SolicitudSuspensionServicio.class);
		transporte = exporter.exportXLS(listSuspensionServicios, "Solicitud de Suspension de Servicios");
		
		return SUCCESS;
		
	}

	private void agregaNombreEstatus() {
		for (SolicitudSuspensionServicio element : listSuspensionServicios) {
			String estatusNombre = this.listEstatus.get(element.getEstatus().toString());
			element.setEstatusName(estatusNombre);
		}
	}
	
	@Action(value = "eliminar", results = {
			@Result(name = INPUT, location = "/jsp/siniestros/catalogo/solicitudsuspensionservicio/contenedorListadoSolicitudSuspensionServicio.jsp"),
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName","${methodName}",
					"namespace","${namespace}",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}",
					"idSuspensionServicio", "${idSuspensionServicio}" }) })
	@Override
	public String eliminar() {
		try {
			SolicitudSuspensionServicio entidad = null;
			entidad = this.entidadService.findById( SolicitudSuspensionServicio.class,  this.idSuspensionServicio );
			this.entidadService.remove( entidad );
			setMensaje( MensajeDTO.MENSAJE_ELIMINAR_SOLICITUD_SUSPENSION );
			setTipoMensaje( MensajeDTO.TIPO_MENSAJE_EXITO );
		} catch (Exception ex) {
			if ( ex.getCause()instanceof ConstraintViolationException ) {
				super.setMensajeError( MensajeDTO.MENSAJE_ERROR_ELIMINAR_SOLICITUD_SUSPENSION );
				ConstraintViolationException constraintViolationException = ( ConstraintViolationException )ex.getCause();
				addErrors( constraintViolationException.getConstraintViolations(),"entidad" );

			}	
			return INPUT;
		}
		namespace = "/siniestros/catalogos/suspensionServicio";
        methodName = "mostrarContenedor";
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		return null;
	}

	/**
	 * @return the listSuspensionServicios
	 */
	public List<SolicitudSuspensionServicio> getListSuspensionServicios() {
		return listSuspensionServicios;
	}

	/**
	 * @param listSuspensionServicios
	 *            the listSuspensionServicios to set
	 */
	public void setListSuspensionServicios(
			List<SolicitudSuspensionServicio> listSuspensionServicios) {
		this.listSuspensionServicios = listSuspensionServicios;
	}

	/**
	 * @return the idSuspensionServicio
	 */
	public Long getIdSuspensionServicio() {
		return idSuspensionServicio;
	}

	/**
	 * @param idSuspensionServicio
	 *            the idSuspensionServicio to set
	 */
	public void setIdSuspensionServicio(Long idSuspensionServicio) {
		this.idSuspensionServicio = idSuspensionServicio;
	}

	/**
	 * @param tipoVentana
	 *            the tipoVentana to set
	 */
	public void setTipoVentana(Long tipoVentana) {
		this.tipoVentana = tipoVentana;
	}

	/**
	 * @return the tipoVentana
	 */
	public Long getTipoVentana() {
		return tipoVentana;
	}

	/**
	 * @return the entidadService
	 */
	public EntidadService getEntidadService() {
		return entidadService;
	}

	/**
	 * @param entidadService
	 *            the entidadService to set
	 */
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the listEstatus
	 */
	public Map<String, String> getListEstatus() {
		return listEstatus;
	}

	/**
	 * @param listEstatus
	 *            the listEstatus to set
	 */
	public void setListEstatus(Map<String, String> listEstatus) {
		this.listEstatus = listEstatus;
	}

	/**
	 * @return the listOficinas
	 */
	public Map<Long, String> getListOficinas() {
		return listOficinas;
	}

	/**
	 * @param listOficinas
	 *            the listOficinas to set
	 */
	public void setListOficinas(Map<Long, String> listOficinas) {
		this.listOficinas = listOficinas;
	}

	/**
	 * @return the catSolicitudSuspensionServicioService
	 */
	public CatSolicitudSuspensionServicioService getCatSolicitudSuspensionServicioService() {
		return catSolicitudSuspensionServicioService;
	}

	/**
	 * @param catSolicitudSuspensionServicioService
	 *            the catSolicitudSuspensionServicioService to set
	 */
	public void setCatSolicitudSuspensionServicioService(CatSolicitudSuspensionServicioService catSolicitudSuspensionServicioService) {
		this.catSolicitudSuspensionServicioService = catSolicitudSuspensionServicioService;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(SolicitudSuspensionServicio entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the entidad
	 */
	public SolicitudSuspensionServicio getEntidad() {
		return entidad;
	}

	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}

	/**
	 * @param listadoService
	 *            the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the filtroCatalogo
	 */
	public SolicitudSuspensionServicioFiltro getFiltroCatalogo() {
		return filtroCatalogo;
	}

	/**
	 * @param filtroCatalogo
	 *            the filtroCatalogo to set
	 */
	public void setFiltroCatalogo(
			SolicitudSuspensionServicioFiltro filtroCatalogo) {
		this.filtroCatalogo = filtroCatalogo;
	}

	@Override
	public String listar() {
		return null;
	}

	@Override
	public String listarFiltrado() {
		return null;
	}

	/**
	 * @return the catalogoSiniestroService
	 */
	public CatalogoSiniestroService getCatalogoSiniestroService() {
		return catalogoSiniestroService;
	}

	/**
	 * @param catalogoSiniestroService
	 *            the catalogoSiniestroService to set
	 */
	public void setCatalogoSiniestroService(CatalogoSiniestroService catalogoSiniestroService) {
		this.catalogoSiniestroService = catalogoSiniestroService;
	}

	/**
	 * @return the transporte
	 */
	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	/**
	 * @param transporte the transporte to set
	 */
	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

}
