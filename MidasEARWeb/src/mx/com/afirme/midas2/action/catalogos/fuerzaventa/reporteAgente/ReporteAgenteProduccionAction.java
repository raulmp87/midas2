package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.InputStream;
import java.util.Calendar;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteProduccion")
@Component
@Scope("prototype")
public class ReporteAgenteProduccionAction extends ReporteAgenteBaseAction implements Preparable, ReportMethods {

	private static final long serialVersionUID = 2900605322519452855L;
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reporteAgenteProduccion.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private InputStream reporteProduccion;
	
	public InputStream getReporteProduccion() {
		return reporteProduccion;
	}

	public void setReporteProduccion(InputStream reporteProduccion) {
		this.reporteProduccion = reporteProduccion;
	}

	@Action(value="mostrarFiltros", results={
			@Result(name = SUCCESS, location= FILTROS_REPORTE)
	} )
	@Override
	public String mostrarFiltros() {
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value = "exportarToExcel", results={
			@Result(name = SUCCESS, type="stream", params={"contentType","${contentType}",
					"contentDisposition","attachtment;filename=\"${fileName}\"",
					"inputName","reporteProduccion" }),
			@Result(name = "EMPTY", location=ERROR ),
			@Result(name = INPUT, location=ERROR )
	})
	@Override
	public String exportarToExcel() {
		try{
			InputStream transporte = getGenerarPlantillaReporteService()
			.imprimirReporteProduccion(getFechaInicio(),getFechaFin());
		
			if(transporte != null){
				setReporteProduccion(transporte);
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			setContentType("application/zip");
			setFileName("reporteProduccion_"
					+ Calendar.getInstance().getTimeInMillis() + ".zip");
		} catch (RuntimeException error) {
			error.printStackTrace();
			setMensaje(error.getCause().getMessage());
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
