package mx.com.afirme.midas2.action.catalogos.fuerzaventa.configuracionPagoComisiones;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.isNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComCentroOperacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComEjecutivo;
import mx.com.afirme.midas2.domain.comisiones.ConfigComGerencia;
import mx.com.afirme.midas2.domain.comisiones.ConfigComMotivoEstatus;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPrioridad;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComSituacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoAgente;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.comisiones.ItemsConfigComision;
import mx.com.afirme.midas2.dto.comisiones.ConfigComisionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.comisiones.ConfigComisionesService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/configuracionPagoComisiones")
@Component
@Scope("prototype")
public class ConfiguracionPagoComisionesAction extends CatalogoHistoricoAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2588481309927343796L;

	private ConfigComisionesService configComisionesService;
	private EntidadService entidadService;
	private EjecutivoService ejecutivoService; 
	private final String COMISIONES_GRID="/jsp/catalogos/fuerzaventa/configuracionPagosComisiones/pagosComisionesGrid.jsp";
	private ConfigComisiones filtroConfigComisiones;
	private ConfigComisiones configComisiones;
	private ConfigComisionesDTO  listaFiltro;
	private String agentesCuentaAfirme;
	private String todosAgentesAfirme; 
	private String rangoFechas;
	
	private List<CentroOperacionView>centroOperacion = new ArrayList<CentroOperacionView>();
	private List<ConfigComCentroOperacion> centroOperacionesSeleccionados = new ArrayList<ConfigComCentroOperacion>();
	
	private List<GerenciaView> listarGerencia = new ArrayList<GerenciaView>();
	private List<ConfigComGerencia> listarConfigComGerencia = new ArrayList<ConfigComGerencia>();
	private List<Long>listarConfigComGerenciaInt = new ArrayList<Long>();
	
	private List<ValorCatalogoAgentes> catalogoTipoAgente=new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigComTipoAgente> tipoAgenteSeleccionado=new ArrayList<ConfigComTipoAgente>();

	private List<EjecutivoView> ejecutivoList = new ArrayList<EjecutivoView>();
	private List<ConfigComEjecutivo> listarConfigComEjecutivo = new ArrayList<ConfigComEjecutivo>();
	private List<Long> listarConfigComEjecutivoInt = new ArrayList<Long>();
	
	private List<ValorCatalogoAgentes> catalogoPrioridad = new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigComPrioridad> catalogoPrioridadSeleccionado = new ArrayList<ConfigComPrioridad>();
	
	private List<PromotoriaView>promotoriaList =new ArrayList<PromotoriaView>();
	private List<ConfigComPromotoria>ListarConfigComPromotoria =new ArrayList<ConfigComPromotoria>();
	private List<Long>listarConfigComPromotoriaInt =new ArrayList<Long>();
	
	private List <ValorCatalogoAgentes>catalogoTipoSituacion = new ArrayList<ValorCatalogoAgentes>();
	private List <ConfigComSituacion>seleccionarTipoSituacion = new ArrayList<ConfigComSituacion>();	
	
	private List<ValorCatalogoAgentes> listaTipoPromotoria=new ArrayList<ValorCatalogoAgentes>();
	private List<ConfigComTipoPromotoria>TipoPromotoriaSeleccionado =new ArrayList<ConfigComTipoPromotoria>();
	
	private List<ValorCatalogoAgentes>catalogoperiodoEjecucion = new ArrayList<ValorCatalogoAgentes>();	
	private List<ValorCatalogoAgentes>modoEjecucion = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes>periodoejecucion = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes>diasEjecucion = new ArrayList<ValorCatalogoAgentes>();
	
	private List <ConfigComisiones>listarConfigPAgosComisionesFiltro = new ArrayList<ConfigComisiones>();
	private List <ConfigComisionesDTO>listaConfigPagoComisiones = new ArrayList<ConfigComisionesDTO>();
	
	private List <ValorCatalogoAgentes> listMotivoStatus = new ArrayList<ValorCatalogoAgentes>();
	private List <ConfigComMotivoEstatus> listMotivoStatusSeleccionado = new ArrayList<ConfigComMotivoEstatus>();
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
//		prepareVerDetalle();
	}
	public void prepareMostrarContenedor(){
			try {
				centroOperacion = configComisionesService.getCentroOperacionList();
//				listarGerencia = entidadService.findAll(Gerencia.class);
//				ejecutivoList = ejecutivoService.findByFilters(null);
//				promotoriaList = entidadService.findAll(Promotoria.class);
				listarGerencia = configComisionesService.getGerenciaList();
				ejecutivoList = configComisionesService.getEjecutivoList();
				promotoriaList = configComisionesService.getPromotoriaList();
				catalogoPrioridad = cargarCatalogo("Prioridades de Agente");
				catalogoTipoAgente=cargarCatalogo("Clasificacion de Agente");//Tipo de Agente
				listaTipoPromotoria = cargarCatalogo("Tipos de Promotoria");
				modoEjecucion = cargarCatalogo("Modos de Ejecucion de Comisiones");
				periodoejecucion = cargarCatalogo("Periodos de Ejecucion de Comisiones");
				catalogoTipoSituacion=cargarCatalogo("Estatus de Agente (Situacion)");
				catalogoperiodoEjecucion = cargarCatalogo("Per\u00EDodos de Ejecuci\u00F3n de Comisiones");
				setRangoFechas(UtileriasWeb.getFechaString(new Date()));
				listMotivoStatus = cargarCatalogo("Motivo de Estatus del Agente");
				cargarDiasEjecucion();
				
				if (configComisiones !=null && configComisiones.getId() != null){
					catalogoTipoSituacion = getCatalogoTipoSituacion(catalogoTipoSituacion ,configComisiones);
					listMotivoStatus = getMotivoStatus(listMotivoStatus, configComisiones);
					listaTipoPromotoria = getListaTipoPromotoria(listaTipoPromotoria, configComisiones);
					catalogoTipoAgente = getTipoAgenteChecked(catalogoTipoAgente, configComisiones);
					catalogoPrioridad = getCatalogoPrioridadChecked(catalogoPrioridad, configComisiones);
					promotoriaList = getPromotoriaChecked(promotoriaList, configComisiones);
					ejecutivoList = getEjecutivosChecked(ejecutivoList, configComisiones);
					listarGerencia = getGerenciaChecked(listarGerencia,  configComisiones);
					centroOperacion = getCentroOperacionChecked(centroOperacion, configComisiones);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private void cargarDiasEjecucion() {
		ValorCatalogoAgentes lunes = new ValorCatalogoAgentes();
		lunes.setId(new Long(1));
		lunes.setValor("Lunes");
		ValorCatalogoAgentes martes = new ValorCatalogoAgentes();
		martes.setId(new Long(2));
		martes.setValor("Martes");
		ValorCatalogoAgentes miercoles = new ValorCatalogoAgentes();
		miercoles.setId(new Long(3));
		miercoles.setValor("Miercoles");
		ValorCatalogoAgentes jueves = new ValorCatalogoAgentes();
		jueves.setId(new Long(4));
		jueves.setValor("Jueves");
		ValorCatalogoAgentes viernes = new ValorCatalogoAgentes();
		viernes.setId(new Long(5));
		viernes.setValor("Viernes");
		ValorCatalogoAgentes sabado = new ValorCatalogoAgentes();
		sabado.setId(new Long(6));
		sabado.setValor("Sabado");
		ValorCatalogoAgentes domingo = new ValorCatalogoAgentes();
		domingo.setId(new Long(7));
		domingo.setValor("Domingo");
		
		diasEjecucion.add(lunes);
		diasEjecucion.add(martes);
		diasEjecucion.add(miercoles);
		diasEjecucion.add(jueves);
		diasEjecucion.add(viernes);
		diasEjecucion.add(sabado);
		diasEjecucion.add(domingo);
		
	}
	public List<ValorCatalogoAgentes> getCatalogoTipoSituacion(List<ValorCatalogoAgentes> list, ConfigComisiones config )throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getSituacionesPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(ValorCatalogoAgentes catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<ValorCatalogoAgentes> getMotivoStatus(List<ValorCatalogoAgentes> list, ConfigComisiones config )throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getMotivoEstatusPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(ValorCatalogoAgentes catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<ValorCatalogoAgentes> getListaTipoPromotoria(List<ValorCatalogoAgentes> list, ConfigComisiones config)throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getTiposPromotoriaPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(ValorCatalogoAgentes catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<ValorCatalogoAgentes> getTipoAgenteChecked(List<ValorCatalogoAgentes> list, ConfigComisiones config) throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getTiposAgentePorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(ValorCatalogoAgentes catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<ValorCatalogoAgentes> getCatalogoPrioridadChecked(List<ValorCatalogoAgentes> list, ConfigComisiones config) throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getPrioridadesPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(ValorCatalogoAgentes catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<PromotoriaView> getPromotoriaChecked(List<PromotoriaView> list, ConfigComisiones config) throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getPromotoriasPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(PromotoriaView catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<EjecutivoView> getEjecutivosChecked(List<EjecutivoView> list, ConfigComisiones config) throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getEjecutivosPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(EjecutivoView catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<GerenciaView> getGerenciaChecked(List<GerenciaView> list,  ConfigComisiones config) throws Exception{
		List<ItemsConfigComision> listChecked = configComisionesService.getGerenciasPorConfigChecked(config);
		if(!listChecked.isEmpty()){
			for(ItemsConfigComision  obj:listChecked){
				for(GerenciaView catalogo:list){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		return list;
	}
	public List<CentroOperacionView> getCentroOperacionChecked(List<CentroOperacionView> listaCO, ConfigComisiones config) throws Exception{
		List<ItemsConfigComision>  listaCentros = configComisionesService.getCentrosOperacionPorConfigChecked(config);
		if(!listaCentros.isEmpty()){
			for(ItemsConfigComision  obj:listaCentros){
				for(CentroOperacionView catalogo:listaCO){
					if(obj.getIdElemento().equals(catalogo.getId())){ 
						catalogo.setChecado(1);
						break;
					}
				}
			}
		}
		
		return  listaCO;
	}
	
	public void validateGuardar(){
		addErrors(configComisiones,this, "configComisiones");
	}
	
	@Action(value="mostrarContenedor", results={
			@Result(name=SUCCESS, location="/jsp/catalogos/fuerzaventa/configuracionPagosComisiones/configuracionPagosComisiones.jsp")
	})
	public String mostrarContenedor(){

		return SUCCESS;
	}
	
	@Override
	@Action(value="guardarConfigPagosComisiones", results={
			@Result(name=SUCCESS, type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/configuracionPagoComisiones","tipoAccion","${tipoAccion}","idRegistro","${configComisiones.id}",
							"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","configComisiones.id","${configComisiones.id}"
					}
			),
			@Result(name=INPUT, location="/jsp/catalogos/fuerzaventa/configuracionPagosComisiones/detallePagosComisiones.jsp"
//					params={"actionName","verDetalle","namespace","/fuerzaventa/configuracionPagoComisiones","tipoAccion","${tipoAccion}",
//					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}"}
			)}
	)
	public String guardar() {
			
			try {
				TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
				String keyMessage= "midas.configPagoComisiones.historial.accion";
				
				if(configComisiones.getId()!=null){
					tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
				}
				if(configComisiones.getTodosLosAgentesAfirme()==1){
					agentesCuentaAfirme = configComisionesService.obtenerAgentesCuentaAfirme();
					configComisiones.setAgentes(agentesCuentaAfirme);
				}
				
				configComisiones.setListaCentroOperaciones(centroOperacionesSeleccionados);
				listarConfigComGerencia = getGerenciasSeleccionadas();
				configComisiones.setListaGerencias(listarConfigComGerencia);
				listarConfigComEjecutivo=getEjecutivosSeleccionados();
				configComisiones.setListaEjecutivos(listarConfigComEjecutivo);
				configComisiones.setListaPrioridades(catalogoPrioridadSeleccionado);
				ListarConfigComPromotoria = getPromotoriaSeleccionado();
				configComisiones.setListaPromotorias(ListarConfigComPromotoria);
				configComisiones.setListaSituaciones(seleccionarTipoSituacion);
				configComisiones.setListaTipoAgentes(tipoAgenteSeleccionado);
				configComisiones.setListaTiposPromotoria(TipoPromotoriaSeleccionado);
				configComisiones.setListaMotivoEstatus(listMotivoStatusSeleccionado);
				configComisiones = configComisionesService.saveConfiguration(configComisiones);
				
				guardarHistorico(TipoOperacionHistorial.CONFIGURACION_COMISIIONES, configComisiones.getId(),keyMessage,tipoAccionHistorial);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
				setMensaje("Acción realizada correctamente");
				setIdTipoOperacion(60L);
				tipoAccion="4";
				return SUCCESS; 
			} catch (Exception e) {
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("Error al guardar los datos");
				// TODO Auto-generated catch block
				e.printStackTrace();
				return INPUT;
			}			
	}
	
	private List<ConfigComEjecutivo> getEjecutivosSeleccionados(){
		List<ConfigComEjecutivo> list=new ArrayList<ConfigComEjecutivo>();
		if(!isEmptyList(listarConfigComEjecutivoInt)){
			for(Long idEjecutivo:listarConfigComEjecutivoInt){
				if(isNotNull(idEjecutivo)){
					ConfigComEjecutivo obj=new ConfigComEjecutivo();
					Ejecutivo ej=new Ejecutivo();
					ej.setId(idEjecutivo);
					obj.setEjecutivo(ej);
					list.add(obj);
				}
			}
		}
		return list;
	}
	
	private List<ConfigComGerencia> getGerenciasSeleccionadas(){
		List<ConfigComGerencia> list = new ArrayList<ConfigComGerencia>();
		if(!isEmptyList(listarConfigComGerenciaInt)){
			for(Long idGerencia :listarConfigComGerenciaInt){
				if(isNotNull(idGerencia)){
					ConfigComGerencia obj = new ConfigComGerencia();
					Gerencia gerencia = new Gerencia();
					gerencia.setId(idGerencia);
					obj.setGerencia(gerencia);
//					if(!obj.getId().equals(null)){
						list.add(obj);
//					}
				}
			}
		}
		return list;
	}
	
	private List <ConfigComPromotoria> getPromotoriaSeleccionado(){
		List <ConfigComPromotoria> list =  new ArrayList<ConfigComPromotoria>();
		if(!isEmptyList(listarConfigComPromotoriaInt)){
			for(Long idPromotoria:listarConfigComPromotoriaInt){
				if(isNotNull(idPromotoria)){
					ConfigComPromotoria obj = new ConfigComPromotoria();
					Promotoria prom = new Promotoria();
					prom.setId(idPromotoria);
					obj.setPromotoria(prom);
//					if(!obj.getId().equals(null)){
						list.add(obj);
//					}
				}
			}
		}
		return list;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Action(value="listar",results={
		@Result(name=SUCCESS,location=COMISIONES_GRID),
		@Result(name=INPUT,location=COMISIONES_GRID)
	})
	public String listar() {
		try{
			listaConfigPagoComisiones=configComisionesService.findByFiltersView(null);
			}catch(Exception e){
				e.printStackTrace();
				return INPUT;
			}
		return SUCCESS;
	}
	
	@Override
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=COMISIONES_GRID),
			@Result(name=INPUT,location=COMISIONES_GRID)
		})
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		try{
			listaConfigPagoComisiones=configComisionesService.findByFiltersView(listaFiltro);
			}catch(Exception e){
				e.printStackTrace();
				return INPUT;
			}
		return SUCCESS;
	}

	@Override
	@Action(value="verDetalle", results={
			@Result(name=SUCCESS, location="/jsp/catalogos/fuerzaventa/configuracionPagosComisiones/detallePagosComisiones.jsp")
	})
	public String verDetalle() {
		try {
//			prepareVerDetalle();
			prepareMostrarContenedor();
			if(configComisiones!=null && configComisiones.getId()!=null){
				try {
					configComisiones = configComisionesService.loadById(configComisiones);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	return SUCCESS;
}

/******************  sets y gets  ************************/
	@Autowired
	@Qualifier("configComisionesServiceEJB")
	public void setAfianzadoraService(ConfigComisionesService configComisionesService) {
		this.configComisionesService = configComisionesService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public List<EjecutivoView> getEjecutivoList() {
		return ejecutivoList;
	}
	public void setEjecutivoList(List<EjecutivoView> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}
	public List<ValorCatalogoAgentes> getCatalogoPrioridad() {
		return catalogoPrioridad;
	}
	public void setCatalogoPrioridad(List<ValorCatalogoAgentes> catalogoPrioridad) {
		this.catalogoPrioridad = catalogoPrioridad;
	}
	public List<CentroOperacionView> getCentroOperacion() {
		return centroOperacion;
	}
	public void setCentroOperacion(List<CentroOperacionView> centroOperacion) {
		this.centroOperacion = centroOperacion;
	}
	public List<ValorCatalogoAgentes> getCatalogoTipoAgente() {
		return catalogoTipoAgente;
	}
	public void setCatalogoTipoAgente(List<ValorCatalogoAgentes> catalogoTipoAgente) {
		this.catalogoTipoAgente = catalogoTipoAgente;
	}
	public List<PromotoriaView> getPromotoriaList() {
		return promotoriaList;
	}
	public void setPromotoriaList(List<PromotoriaView> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}
	public List<ValorCatalogoAgentes> getListaTipoPromotoria() {
		return listaTipoPromotoria;
	}
	public void setListaTipoPromotoria(
			List<ValorCatalogoAgentes> listaTipoPromotoria) {
		this.listaTipoPromotoria = listaTipoPromotoria;
	}
	public List<ValorCatalogoAgentes> getModoEjecucion() {
		return modoEjecucion;
	}
	public void setModoEjecucion(List<ValorCatalogoAgentes> modoEjecucion) {
		this.modoEjecucion = modoEjecucion;
	}
	public List<ValorCatalogoAgentes> getPeriodoejecucion() {
		return periodoejecucion;
	}
	public void setPeriodoejecucion(List<ValorCatalogoAgentes> periodoejecucion) {
		this.periodoejecucion = periodoejecucion;
	}
	@Autowired
	@Qualifier("ejecutivoServiceEJB")
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	public List<ValorCatalogoAgentes> getCatalogoTipoSituacion() {
		return catalogoTipoSituacion;
	}
	public void setCatalogoTipoSituacion(
			List<ValorCatalogoAgentes> catalogoTipoSituacion) {
		this.catalogoTipoSituacion = catalogoTipoSituacion;
	}

	public ConfigComisiones getConfigComisiones() {
		return configComisiones;
	}
	public void setConfigComisiones(ConfigComisiones configComisiones) {
		this.configComisiones = configComisiones;
	}
	public List<GerenciaView> getListarGerencia() {
		return listarGerencia;
	}
	public void setListarGerencia(List<GerenciaView> listarGerencia) {
		this.listarGerencia = listarGerencia;
	}
	public List<ValorCatalogoAgentes> getCatalogoperiodoEjecucion() {
		return catalogoperiodoEjecucion;
	}
	public void setCatalogoperiodoEjecucion(
			List<ValorCatalogoAgentes> catalogoperiodoEjecucion) {
		this.catalogoperiodoEjecucion = catalogoperiodoEjecucion;
	}
	public List<ConfigComCentroOperacion> getCentroOperacionesSeleccionados() {
		return centroOperacionesSeleccionados;
	}
	public void setCentroOperacionesSeleccionados(
			List<ConfigComCentroOperacion> centroOperacionesSeleccionados) {
		this.centroOperacionesSeleccionados = centroOperacionesSeleccionados;
	}
	public List<ConfigComGerencia> getListarConfigComGerencia() {
		return listarConfigComGerencia;
	}
	public void setListarConfigComGerencia(
			List<ConfigComGerencia> listarConfigComGerencia) {
		this.listarConfigComGerencia = listarConfigComGerencia;
	}
	public List<ConfigComTipoAgente> getTipoAgenteSeleccionado() {
		return tipoAgenteSeleccionado;
	}
	public void setTipoAgenteSeleccionado(
			List<ConfigComTipoAgente> tipoAgenteSeleccionado) {
		this.tipoAgenteSeleccionado = tipoAgenteSeleccionado;
	}
	public List<ConfigComEjecutivo> getListarConfigComEjecutivo() {
		return listarConfigComEjecutivo;
	}
	public void setListarConfigComEjecutivo(
			List<ConfigComEjecutivo> listarConfigComEjecutivo) {
		this.listarConfigComEjecutivo = listarConfigComEjecutivo;
	}
	public List<ConfigComPrioridad> getCatalogoPrioridadSeleccionado() {
		return catalogoPrioridadSeleccionado;
	}
	public void setCatalogoPrioridadSeleccionado(
			List<ConfigComPrioridad> catalogoPrioridadSeleccionado) {
		this.catalogoPrioridadSeleccionado = catalogoPrioridadSeleccionado;
	}
	public List<ConfigComPromotoria> getListarConfigComPromotoria() {
		return ListarConfigComPromotoria;
	}
	public void setListarConfigComPromotoria(
			List<ConfigComPromotoria> listarConfigComPromotoria) {
		ListarConfigComPromotoria = listarConfigComPromotoria;
	}
	public List<ConfigComTipoPromotoria> getTipoPromotoriaSeleccionado() {
		return TipoPromotoriaSeleccionado;
	}
	public void setTipoPromotoriaSeleccionado(
			List<ConfigComTipoPromotoria> tipoPromotoriaSeleccionado) {
		TipoPromotoriaSeleccionado = tipoPromotoriaSeleccionado;
	}
	public List<ConfigComSituacion> getSeleccionarTipoSituacion() {
		return seleccionarTipoSituacion;
	}
	public void setSeleccionarTipoSituacion(
			List<ConfigComSituacion> seleccionarTipoSituacion) {
		this.seleccionarTipoSituacion = seleccionarTipoSituacion;
	}
	public List<ConfigComisionesDTO> getListaConfigPagoComisiones() {
		return listaConfigPagoComisiones;
	}
	public void setListaConfigPagoComisiones(
			List<ConfigComisionesDTO> listaConfigPagoComisiones) {
		this.listaConfigPagoComisiones = listaConfigPagoComisiones;
	}
	public List<ConfigComisiones> getListarConfigPAgosComisionesFiltro() {
		return listarConfigPAgosComisionesFiltro;
	}
	public void setListarConfigPAgosComisionesFiltro(
			List<ConfigComisiones> listarConfigPAgosComisionesFiltro) {
		this.listarConfigPAgosComisionesFiltro = listarConfigPAgosComisionesFiltro;
	}
	public ConfigComisiones getFiltroConfigComisiones() {
		return filtroConfigComisiones;
	}
	public void setFiltroConfigComisiones(ConfigComisiones filtroConfigComisiones) {
		this.filtroConfigComisiones = filtroConfigComisiones;
	}
	public ConfigComisionesDTO getListaFiltro() {
		return listaFiltro;
	}
	public void setListaFiltro(ConfigComisionesDTO listaFiltro) {
		this.listaFiltro = listaFiltro;
	}
	public String getRangoFechas() {
		return rangoFechas;
	}
	public void setRangoFechas(String rangoFechas) {
		this.rangoFechas = rangoFechas;
	}
	public String getAgentesCuentaAfirme() {
		return agentesCuentaAfirme;
	}
	public void setAgentesCuentaAfirme(String agentesCuentaAfirme) {
		this.agentesCuentaAfirme = agentesCuentaAfirme;
	}
	public String getTodosAgentesAfirme() {
		return todosAgentesAfirme;
	}
	public void setTodosAgentesAfirme(String todosAgentesAfirme) {
		this.todosAgentesAfirme = todosAgentesAfirme;
	}
	public List<Long> getListarConfigComEjecutivoInt() {
		return listarConfigComEjecutivoInt;
	}
	public void setListarConfigComEjecutivoInt(
			List<Long> listarConfigComEjecutivoInt) {
		this.listarConfigComEjecutivoInt = listarConfigComEjecutivoInt;
	}
	public List<Long> getListarConfigComGerenciaInt() {
		return listarConfigComGerenciaInt;
	}
	public void setListarConfigComGerenciaInt(List<Long> listarConfigComGerenciaInt) {
		this.listarConfigComGerenciaInt = listarConfigComGerenciaInt;
	}
	public List<Long> getListarConfigComPromotoriaInt() {
		return listarConfigComPromotoriaInt;
	}
	public void setListarConfigComPromotoriaInt(
			List<Long> listarConfigComPromotoriaInt) {
		this.listarConfigComPromotoriaInt = listarConfigComPromotoriaInt;
	}
	public List<ValorCatalogoAgentes> getListMotivoStatus() {
		return listMotivoStatus;
	}
	public void setListMotivoStatus(List<ValorCatalogoAgentes> listMotivoStatus) {
		this.listMotivoStatus = listMotivoStatus;
	}
	public List<ConfigComMotivoEstatus> getListMotivoStatusSeleccionado() {
		return listMotivoStatusSeleccionado;
	}
	public void setListMotivoStatusSeleccionado(
			List<ConfigComMotivoEstatus> listMotivoStatusSeleccionado) {
		this.listMotivoStatusSeleccionado = listMotivoStatusSeleccionado;
	}
	public List<ValorCatalogoAgentes> getDiasEjecucion() {
		return diasEjecucion;
	}
	public void setDiasEjecucion(List<ValorCatalogoAgentes> diasEjecucion) {
		this.diasEjecucion = diasEjecucion;
	}
	
	
	
}