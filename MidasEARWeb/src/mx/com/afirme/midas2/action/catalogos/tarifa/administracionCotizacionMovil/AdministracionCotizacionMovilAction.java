package mx.com.afirme.midas2.action.catalogos.tarifa.administracionCotizacionMovil;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/administracionCotizacionMovil")
@Component
@Scope("prototype")
public class AdministracionCotizacionMovilAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4814866545981130881L;
	@Autowired
	private CotizacionMovilService cotizacionMovilService;
	private final String NAMESPACE="/tarifa/administracionCotizacionMovil";
	/***Components of view****************************************/
	private CotizacionMovilDTO cotizacionMovil;
	private List<CotizacionMovilDTO> listaCotizacionMovil= new ArrayList<CotizacionMovilDTO>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	private String tabActiva;
	/**
	 * Action:
	 * 
	 */
	
	@Override
	public void prepare() throws Exception {
		if(cotizacionMovil!=null && "1".equals(tipoAccion)){
			cotizacionMovil=new CotizacionMovilDTO();
		}
	}
	
	@Action(value="mostrar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/mostrar.jsp")
		})
		public String mostrar(){
			cotizacionMovil=new CotizacionMovilDTO();
			return SUCCESS;
		}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/administracionCotizacionMovilGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/administracionCotizacionMovilGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		try{
			if("consulta".equals(tipoAccion)){
				if(cotizacionMovil==null){
					cotizacionMovil=new CotizacionMovilDTO();
               }
			}
			if(cotizacionMovil!=null){
				listaCotizacionMovil=cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_MOVIL);
			}
			else{
				listaCotizacionMovil=cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_MOVIL);
			}
			
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/detalle.jsp")
		})
	@Override
	public String verDetalle() {
		if(cotizacionMovil != null){
			if(cotizacionMovil.getIdtocotizacionmovil()!=null){
				cotizacionMovil = cotizacionMovilService.findCotizacionByFilters(cotizacionMovil, SolicitudCotizacionMovil.TIPO_COTIZACION_MOVIL).get(0);
			}
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/administracionCotizacionMovil/detalle.jsp")
		})	
	public String verDetalleHistorico() {
		if(cotizacionMovil != null){
			if(cotizacionMovil.getIdtocotizacionmovil()!=null){
				//descuentosAgente = gerenciaJpaService.loadById(descuentosAgente, this.getUltimaModificacion().getFechaHoraActualizacionString());
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion","excludeProperties","^gerencia\\.ejecutivos.*,^gerencia\\.personaResponsable.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion"})
	})
	
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	@Override
	public String guardar() {

		return null;
	}
	@Override
	public String eliminar() {

		return null;
	}
	public CotizacionMovilDTO getCotizacionMovil() {
		return cotizacionMovil;
	}
	public void setCotizacionMovil(CotizacionMovilDTO cotizacionMovil) {
		this.cotizacionMovil = cotizacionMovil;
	}
	public List<CotizacionMovilDTO> getListaCotizacionMovil() {
		return listaCotizacionMovil;
	}
	public void setListaCotizacionMovil(
			List<CotizacionMovilDTO> listaCotizacionMovil) {
		this.listaCotizacionMovil = listaCotizacionMovil;
	}
	@Override
	public String listar() {

		return null;
	}
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	public String getTabActiva() {
		return tabActiva;
	}
	
}

