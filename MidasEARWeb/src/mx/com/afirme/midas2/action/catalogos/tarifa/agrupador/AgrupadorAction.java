package mx.com.afirme.midas2.action.catalogos.tarifa.agrupador;

import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AgrupadorAction extends CatalogoAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private AgrupadorTarifa agrupadorTarifa;
	
	private List<AgrupadorTarifa> agrupadorTarifaList;
	
	private String negocio;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
	
	private AgrupadorTarifaService agrupadorTarifaService;
	
	private String descripcionAgrupadorTarifa;
	
	public AgrupadorAction(){
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	
	public void validateGuardar() {
		addErrors(agrupadorTarifa, NewItemChecks.class, this, "agrupadorTarifa");
	}

	public void validateEditar() {
		addErrors(agrupadorTarifa, EditItemChecks.class, this, "agrupadorTarifa");
	}
	
	@Override
	public void prepare() throws Exception {
		if (getId() != null && !getId().equals("")) {
			agrupadorTarifa = agrupadorTarifaService.findById(getId());
		}
	}

	@Override
	public String guardar() {
		if(agrupadorTarifa.getClaveNegocio()== null)agrupadorTarifa.setClaveNegocio(getNegocio());
		agrupadorTarifa.setCodigoUsuarioCreacion("USERTMP");
		agrupadorTarifa.setCodigoUsuarioActivacion("USERTMP");
		agrupadorTarifaService.save(agrupadorTarifa);
		agrupadorTarifa = new AgrupadorTarifa();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		entidadService.remove(agrupadorTarifa);
		setMensajeExito();
		return SUCCESS;	
	}

	@Override
	public String listar() {
		agrupadorTarifaList = entidadService.findAll(AgrupadorTarifa.class);
		return SUCCESS;
	}

	@Override
	public String listarFiltrado() {
		if(agrupadorTarifa == null){ 
			agrupadorTarifa = new  AgrupadorTarifa();
		}
		agrupadorTarifa.setClaveNegocio(getNegocio());
		agrupadorTarifaList = agrupadorTarifaService.findByFilters(agrupadorTarifa);
		return SUCCESS;
	}

	public String nuevaVersion() {
		agrupadorTarifa = agrupadorTarifaService.createNewVersion(agrupadorTarifa);
		return SUCCESS;
	}
	@Override
	public String verDetalle() {
		return SUCCESS;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public AgrupadorTarifa getAgrupadorTarifa() {
		return agrupadorTarifa;
	}

	public void setAgrupadorTarifa(AgrupadorTarifa agrupadorTarifa) {
		this.agrupadorTarifa = agrupadorTarifa;
	}

	public List<AgrupadorTarifa> getAgrupadorTarifaList() {
		return agrupadorTarifaList;
	}

	public void setAgrupadorTarifaList(List<AgrupadorTarifa> agrupadorTarifaList) {
		this.agrupadorTarifaList = agrupadorTarifaList;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	@Autowired
	@Qualifier("agrupadorTarifaEJB")
	public void setAgrupadoTarifaService(AgrupadorTarifaService agrupadorTarifaService){
		this.agrupadorTarifaService = agrupadorTarifaService;
	}

	public void setDescripcionAgrupadorTarifa(String descripcionAgrupadorTarifa) {
		this.descripcionAgrupadorTarifa = descripcionAgrupadorTarifa;
	}

	public String getDescripcionAgrupadorTarifa() {
		return descripcionAgrupadorTarifa;
	}
}
