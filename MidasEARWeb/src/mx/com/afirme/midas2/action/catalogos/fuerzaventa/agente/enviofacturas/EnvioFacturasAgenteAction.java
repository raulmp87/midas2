package mx.com.afirme.midas2.action.catalogos.fuerzaventa.agente.enviofacturas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.util.ServletContextAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgenteDet;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HonorariosAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EnvioFacturaAgenteService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

@Namespace("/fuerzaventa/agente/envioFacturaAgentes")
@Component
@Scope("prototype")
public class EnvioFacturasAgenteAction extends CatalogoHistoricoAction implements Preparable, ServletContextAware{
	
	private static Logger log = Logger.getLogger(EnvioFacturasAgenteAction.class);
	private static final long serialVersionUID = 1L;
	private Agente agente = new Agente();
	private EnvioFacturaAgente envioFacturaAgente;
	private AgenteMidasService agenteMidasService;
	private EntidadService entidadService;
	private EnvioFacturaAgenteService envioFacturaService;
	private List<EnvioFacturaAgente> listaEnvios;
	private List<EnvioFacturaAgenteDet>listaEnviosDet;
	private HttpServletResponse response;
	private UsuarioService usuarioService;
	
	private File facturaXml;
	private String facturaXmlContentType;
	private String facturaXmlFileName;
	private Long idAgente;
	private Long idEnvio;
	private String numeroCedula;
	private String vencimientoCedula;
	private String tipoAgente;
	private String nombreAgente;
	private String centroOperacion;
	private String gerencia;
	private String estatus;
	private String anioMes;
	private Boolean isAgente;
	private Boolean isAdministrador;
	private Boolean isAgentePromotor;
	private Boolean tieneFacturasPendientes;
	private Boolean isAutorizadorExcepcion;
	private Boolean autorizacion1;
	private Boolean autorizacion2;
	private String msgFacturasPendientes;
	private Boolean disableAutorizacion1;
	private Boolean disableAutorizacion2;
	private List<AgenteView> agenteList;
	
	private String rfc;
	private String ejecutivo;
	private String tabActiva;
	private final String AGENTECONTENEDOR = "/jsp/catalogos/fuerzaventa/envioFacturaAgentes/contenedorEnvioFacturaAgente.jsp";
	private final String AGENTEENVIO = "/jsp/catalogos/fuerzaventa/envioFacturaAgentes/envioFacturaAgentes.jsp";
	private final String AGENTEENVIOGRID = "/jsp/catalogos/fuerzaventa/envioFacturaAgentes/envioFacturaGrid.jsp";
	private final String AGENTEENVIODETGRID = "/jsp/catalogos/fuerzaventa/envioFacturaAgentes/envioFacturaDetGrid.jsp";
		
	@Action(value="mostrarContenedor",results={
			@Result(name = SUCCESS,location=AGENTECONTENEDOR)})
	public String mostrarContenedor(){
		this.tabActiva = ConstantesCompensacionesAdicionales.TAB_COMISIONES;
		return SUCCESS;
	}
	
	@Action(value="init", results={
			@Result(name= SUCCESS, location=AGENTEENVIO)			
	})	
	public String init(){
		Usuario usuario = usuarioService.getUsuarioActual();
		isAgente = false;
		this.tabActiva = ConstantesCompensacionesAdicionales.TAB_COMISIONES;
		isAdministrador = false;
		isAgentePromotor = false;
		anioMes = "N/A";
		tieneFacturasPendientes = false;
		msgFacturasPendientes = "";
		
		isAgente = usuarioService.tieneRolUsuarioActual(sistemaContext.getRolAgenteAutos());
		
		isAdministrador = usuarioService.tienePermisoUsuarioActual("FN_M2_Carga_Facturas_Administrador");
		
		isAgentePromotor = usuarioService.tieneRolUsuarioActual(sistemaContext.getRolPromotorAutos());
		
		isAutorizadorExcepcion = false;
					
		try{
			if (isAgentePromotor) {
				Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
				Agente agenteDummy = new Agente();
				if (agenteUsuarioActual != null) {
					agenteDummy.setPromotoria(agenteUsuarioActual.getPromotoria());
					agenteList = agenteMidasService.findByFilterLightWeight(agenteDummy);
					LOG.debug("agenteUsuarioActual.getPromotoria().getIdPromotoria():"+ agenteUsuarioActual.getPromotoria().getIdPromotoria());
				} else {
					isAgentePromotor = false;
				}
			}

		if(!isAdministrador||(idAgente!=null && !isAgentePromotor)){
			if(idAgente==null){
				setAgente(envioFacturaService.getDatosAgente(usuario.getNombreUsuario(), usuario.getPersonaId(),null));
			}else{
				setAgente(envioFacturaService.getDatosAgente(null, null,idAgente));

			}	
		
				if(getAgente()!=null){
										
					HonorariosAgente honorarios = envioFacturaService.findHonorariosByIdAgente(getAgente().getIdAgente());
	
					iniciaValores();
					
					if (honorarios == null) {
						addActionMessage("Usted no Tiene Facturas Pendientes");
					} else {
						anioMes = honorarios.getAnioMes().toString().substring(0,4)+"-"+honorarios.getAnioMes().toString().substring(4,6);
						
						isAutorizadorExcepcion = usuarioService.tienePermisoUsuarioActual("FN_M2_Agentes_Autorizar_Excepcion_Factura");
						
						autorizacion1 = honorarios.getAutorizadorExcepcion1() != null;
						autorizacion2 = honorarios.getAutorizadorExcepcion2() != null;
						
						disableAutorizacion1 = !(usuarioService.tieneRolUsuarioActual("Rol_M2_Autorizador_Excepcion_1") && honorarios.getAutorizadorExcepcion1() == null);
						disableAutorizacion2 = !(usuarioService.tieneRolUsuarioActual("Rol_M2_Autorizador_Excepcion_2") && honorarios.getAutorizadorExcepcion2() == null);
						
					}
					
					tieneFacturasPendientes();
				}
			}
		}catch (Exception e) {
			addActionError("Error al iniciar Datos de Agente");
			this.setMensajeError("Error al Cargar Lista de Envios");
			log.error(" --init() ", e);
		}
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado", results={
			@Result(name=SUCCESS,location=AGENTEENVIOGRID),		
			@Result(name=INPUT,location=AGENTEENVIOGRID)
	})	
	@Override
	public String listarFiltrado() {
		listaEnvios = new ArrayList<EnvioFacturaAgente>();
		if(getAgente()!=null){
			try{
				setListaEnvios(envioFacturaService.findByAgente(idAgente));
			}catch (Exception e) {
				log.error(" --listarFiltrado() ", e);
				this.setMensajeError("Error al Cargar Lista de Envios");
			}
		}
		return SUCCESS;
	}
	
	@Action(value="listarDetalle", results={
			@Result(name=SUCCESS,location=AGENTEENVIODETGRID),		
			@Result(name=INPUT,location=AGENTEENVIODETGRID)
	})	
	public String listarDetalle() {
		if(getAgente()!=null){
			try{
				listaEnviosDet = new ArrayList<EnvioFacturaAgenteDet>();
				setListaEnviosDet(envioFacturaService.findByIdEnvio(idEnvio));
			}catch (Exception e) {
				log.error(" --listarDetalle() ", e);
				this.setMensajeError("Error al Cargar Lista Detalle de Envios");
			}
		}
		return SUCCESS;
	}
	
	@Action(value="subirArchivo", 
			results={
				@Result(name=SUCCESS,type="redirectAction",
						params={"actionName","init","namespace","/fuerzaventa/agente/envioFacturaAgentes","idAgente","${idAgente}"}),					 
				@Result(name=INPUT,type="redirectAction",
						params={"actionName","init","namespace","/fuerzaventa/agente/envioFacturaAgentes","idAgente","${idAgente}"})
	})	
	public String subirArchivo() {
		if(getAgente()!=null){
			try{
				if(facturaXml != null){
					Usuario usuario = usuarioService.getUsuarioActual();
					String usuarioName =usuario.getNombreUsuario();
					EnvioFacturaAgente envio = envioFacturaService.persistEnvio(idAgente, facturaXml,usuarioName);
					generaRespuesta("Archivo cargado con exito, su factura Fue "+ envio.getStatus().getValor());
				}else{
					generaRespuesta("Error al cargar su Factura, Revise el archivo enviado");
				}
			}catch (FileNotFoundException e) {
				log.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, Revise el archivo enviado");
			}catch(PersistenceException e){
				log.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, Revise tus datos Fiscales");
			}catch(ConnectException e){
				log.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, No se puede conectar con el validador, pruebe dentro de un momento");
			}catch(Exception e){
				log.error(" --subirArchivo() ", e);
				generaRespuesta("Error al cargar su Factura, pruebe dentro de un momento");
			}
			
		}
		return SUCCESS;
	}
	
	@Action(value="marcarFacturasAntiguas",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","init","namespace","/fuerzaventa/agente/envioFacturaAgentes","idAgente","${idAgente}"})					 
	})	
	public String marcarFacturasAntiguas(){
		try{
						
			envioFacturaService.marcarFacturasAntiguas(idAgente);
			
			this.setMensajeExito();
			
		}catch (Exception e) {
			log.error(" --marcarFacturasAntiguas() ", e);
			this.setMensajeError(getMensajeErrorGlobal("midas.agente.factura.antigua.marcado.boton.error"));
		}
		return SUCCESS;
	}
	
	
	@Action(value="autorizaExcepcion")
	public String autorizaExcepcion() {
		
		envioFacturaService.autorizaExcepcion(idAgente);
				
		return NONE;
	}
	
	public String generaRespuesta(String mensaje){
		try {
			setResponse(ServletActionContext.getResponse());
			getResponse().setContentType("text/html");
        	PrintWriter out = getResponse().getWriter();
        	out.write(mensaje);
        	out.flush();
        	out.close();
        } catch (IOException ioe) {
        	log.error(" --generaRespuesta() ", ioe);
        }

        return com.opensymphony.xwork2.Action.NONE;
	}
		
	public void iniciaValores(){
		idAgente = agente.getIdAgente();
		numeroCedula = agente.getNumeroCedula();
		vencimientoCedula = agente.getVencimientoCedulaString();
		tipoAgente = agente.getTipoAgente();
		nombreAgente = agente.getPersona().getNombreCompleto();
		centroOperacion = agente.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().getDescripcion();
		gerencia = agente.getPromotoria().getEjecutivo().getGerencia().getDescripcion();
		estatus = agente.getTipoSituacion().getValor();
		rfc = agente.getPersona().getRfc();
		ejecutivo = agente.getPromotoria().getEjecutivo().getPersonaResponsable().getNombreCompleto();	
	}
	
	private void tieneFacturasPendientes(){
		
		tieneFacturasPendientes = envioFacturaService.tieneFacturasAntiguasPendientes(getAgente().getIdAgente());
		
		if (tieneFacturasPendientes) {
			
			if(isAdministrador) {
				
				msgFacturasPendientes = getText("midas.agente.factura.antigua.pendiente.admin");
				
			} else {
				
				msgFacturasPendientes = getText("midas.agente.factura.antigua.pendiente.agente");
				
			}
			
		}
				
	}
	
	@Override
	public void prepare() throws Exception {
		// Auto-generated method stub
		
	}

	@Override
	public String guardar() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String listar() {
		// Auto-generated method stub
		return null;
	}

	@Override
	public String verDetalle() {
		// Auto-generated method stub
		return null;
	}
	
	public AgenteMidasService getAgenteMidasService() {
		return agenteMidasService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService){
			this.agenteMidasService = agenteMidasService;	
	}
	
	public EnvioFacturaAgenteService getEnvioFacturaService() {
		return envioFacturaService;
	}
	
	@Autowired
	@Qualifier("envioFacturaAgenteEJB")
	public void setEnvioFacturaService(EnvioFacturaAgenteService envioFacturaService) {
		this.envioFacturaService = envioFacturaService;
	}
	
	
	public EntidadService getEntidadService() {
		return entidadService;
	}
	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	
	private void setAgente(Agente agente) {
		this.agente = agente;
	}
	
	private Agente getAgente() {
		return agente;
	}
	
	public EnvioFacturaAgente getEnvioFacturaAgente() {
		return envioFacturaAgente;
	}

	public void setEnvioFacturaAgente(EnvioFacturaAgente envioFacturaAgente) {
		this.envioFacturaAgente = envioFacturaAgente;
	}
	
	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getNumeroCedula() {
		return numeroCedula;
	}

	public void setNumeroCedula(String numeroCedula) {
		this.numeroCedula = numeroCedula;
	}

	public String getVencimientoCedula() {
		return vencimientoCedula;
	}

	public void setVencimientoCedula(String vencimientoCedula) {
		this.vencimientoCedula = vencimientoCedula;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}

	public void setTipoAgente(String tipoAgente) {
		this.tipoAgente = tipoAgente;
	}

	public String getNombreAgente() {
		return nombreAgente;
	}

	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	public String getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(String centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public String getGerencia() {
		return gerencia;
	}

	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}
	
	public List<EnvioFacturaAgente> getListaEnvios() {
		return listaEnvios;
	}

	public void setListaEnvios(List<EnvioFacturaAgente> listaEnvios) {
		this.listaEnvios = listaEnvios;
	}
	
	public List<EnvioFacturaAgenteDet> getListaEnviosDet() {
		return listaEnviosDet;
	}

	public void setListaEnviosDet(List<EnvioFacturaAgenteDet> listaEnviosDet) {
		this.listaEnviosDet = listaEnviosDet;
	}
	
	public Long getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(Long idEnvio) {
		this.idEnvio = idEnvio;
	}
	
	public File getFacturaXml() {
		return facturaXml;
	}

	public void setFacturaXml(File facturaXml) {
		this.facturaXml = facturaXml;
	}
	
	public String getFacturaXmlContentType() {
		return facturaXmlContentType;
	}

	public void setFacturaXmlContentType(String facturaXmlContentType) {
		this.facturaXmlContentType = facturaXmlContentType;
	}

	public String getFacturaXmlFileName() {
		return facturaXmlFileName;
	}

	public void setFacturaXmlFileName(String facturaXmlFileName) {
		this.facturaXmlFileName = facturaXmlFileName;
	}

	@Override
	public void setServletContext(ServletContext arg0) {
		// Auto-generated method stub
		
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	
	public String getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(String anioMes) {
		this.anioMes = anioMes;
	}
	
	public Boolean getIsAgente() {
		return isAgente;
	}

	public void setIsAgente(Boolean isAgente) {
		this.isAgente = isAgente;
	}
	
	public Boolean getIsAdministrador() {
		return isAdministrador;
	}

	public void setIsAdministrador(Boolean isAdministrador) {
		this.isAdministrador = isAdministrador;
	}

	public Boolean getTieneFacturasPendientes() {
		return tieneFacturasPendientes;
	}

	public void setTieneFacturasPendientes(Boolean tieneFacturasPendientes) {
		this.tieneFacturasPendientes = tieneFacturasPendientes;
	}
	
	public Boolean getIsAutorizadorExcepcion() {
		return isAutorizadorExcepcion;
	}

	public void setIsAutorizadorExcepcion(Boolean isAutorizadorExcepcion) {
		this.isAutorizadorExcepcion = isAutorizadorExcepcion;
	}
		
	public Boolean getAutorizacion1() {
		return autorizacion1;
	}

	public void setAutorizacion1(Boolean autorizacion1) {
		this.autorizacion1 = autorizacion1;
	}

	public Boolean getAutorizacion2() {
		return autorizacion2;
	}

	public void setAutorizacion2(Boolean autorizacion2) {
		this.autorizacion2 = autorizacion2;
	}

	public String getMsgFacturasPendientes() {
		return msgFacturasPendientes;
	}

	public void setMsgFacturasPendientes(String msgFacturasPendientes) {
		this.msgFacturasPendientes = msgFacturasPendientes;
	}
	
	public Boolean getDisableAutorizacion1() {
		return disableAutorizacion1;
	}

	public void setDisableAutorizacion1(Boolean disableAutorizacion1) {
		this.disableAutorizacion1 = disableAutorizacion1;
	}

	public Boolean getDisableAutorizacion2() {
		return disableAutorizacion2;
	}

	public void setDisableAutorizacion2(Boolean disableAutorizacion2) {
		this.disableAutorizacion2 = disableAutorizacion2;
	}

	public List<AgenteView> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<AgenteView> agenteList) {
		this.agenteList = agenteList;
	}

	public Boolean getIsAgentePromotor() {
		return isAgentePromotor;
	}

	public void setIsAgentePromotor(Boolean isAgentePromotor) {
		this.isAgentePromotor = isAgentePromotor;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}	
	
}
