package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteDetalleBonos")
@Component
@Scope("prototype")
public class ReporteAgenteBonosDetalleAction extends ReporteAgenteBaseAction implements Preparable, ReportMethods {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6756910451571273299L;
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reporteAgenteDetBonos.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private InputStream reporteDetalleBonos; 
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	private Date fechaInicial;
	private Date fechaFinal;
	private String labelFechaInicio = "Fecha Corte Inicio";
	private String labelFechaFin = "Fecha Corte Fin";

	/**************************************setter and getters***********************************************/
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public InputStream getReporteDetalleBonos() {
		return reporteDetalleBonos;
	}

	public void setReporteDetalleBonos(InputStream reporteDetalleBonos) {
		this.reporteDetalleBonos = reporteDetalleBonos;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getLabelFechaInicio() {
		return labelFechaInicio;
	}

	public void setLabelFechaInicio(String labelFechaInicio) {
		this.labelFechaInicio = labelFechaInicio;
	}

	public String getLabelFechaFin() {
		return labelFechaFin;
	}

	public void setLabelFechaFin(String labelFechaFin) {
		this.labelFechaFin = labelFechaFin;
	}

	/*************************************common methods***************************************************/
	@Action(value="mostrarFiltros", results={
			@Result(name=SUCCESS, location=FILTROS_REPORTE)
	})
	
	@Override
	public String mostrarFiltros() {
		agente = new Agente();
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="exportarToExcel", results={
		@Result(name=SUCCESS,type="stream", params={"contentType",
				"${contentType}", "contentDisposition",
				"attachtment;filename=\"${fileName}\"","inputName",
				"reporteDetalleBonos"
		}),
		@Result(name="EMPTY", location= ERROR),
		@Result(name=INPUT, location=ERROR)
	})
	@Override
	public String exportarToExcel() {
		try{
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
			.imprimirReporteDetalleBonosExcel(agente.getIdAgente(),getFechaInicial(), getFechaFinal(), ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		
			if(transporte!=null){
				
				setReporteDetalleBonos(new ByteArrayInputStream(
						transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
				setContentType("application/xls");
			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octect-stream");
			}
				setFileName("reporteDetalleBonos."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
//			}
		} catch (RuntimeException error) {
			error.printStackTrace();
			setMensaje(error.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"})
		})
		
		public String obtenerAgente(){
		try {
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
		} catch (Exception e) {
			
		}
		return SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

}
