package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import com.opensymphony.xwork2.Preparable;
@Namespace("/fuerzaventa/reporteProvision")
@Component
@Scope("prototype")
public class ReporteProvisionAction extends ReporteAgenteBaseAction implements Preparable,ReportMethods{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ProvisionImportesRamo filtroReporteProvision;
	private final String FILTROS_REPORTE="/jsp/reportesAgentes/reporteProvision.jsp";
	private final String ERROR="/jsp/reportesAgentes/reporteError.jsp";
	private InputStream reporteProvisionToExcel;
	private Map<Long, String> gerenciaList = new LinkedHashMap<Long, String>();
	private Map<Long, String> ejecutivoList = new LinkedHashMap<Long, String>();
	private Map<Long, String> promotoriaList = new LinkedHashMap<Long, String>();
	private List <ConfigBonos>listTipoBono = new ArrayList<ConfigBonos>();
	private ConfigBonosService configBonosService;
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	private ListadoService listadoService; 
	
	private CentroOperacion centroOperacion = new CentroOperacion();
	private Gerencia gerencia = new Gerencia();
	private Ejecutivo ejecutivo = new Ejecutivo();
	private Promotoria promotoria = new Promotoria();
	private ConfigBonos bono = new ConfigBonos();
	private ValorCatalogoAgentes tipAgente = new ValorCatalogoAgentes();
	private String anio;
	private String mes;
	
	@Action(value="mostrarFiltros", results={
			@Result(name = SUCCESS, location= FILTROS_REPORTE)
	} )
	@Override
	public String mostrarFiltros() {
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="exportarToExcel", results={
			@Result(name=SUCCESS, type="stream", params={"contentType",
					"${contentType}", "contentDisposition",
					"attachtment;filename=\"${fileName}\"","inputName",
					"reporteProvisionToExcel"}),
			@Result(name="EMPTY", location=ERROR),
			@Result(name=INPUT, location=ERROR)
	})
	@Override
	public String exportarToExcel() {
	try{
		TransporteImpresionDTO transporte = getGenerarPlantillaReporteService().
												imprimirReporteProvision(anio,mes,filtroReporteProvision, ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		
		if(transporte!=null){
			setReporteProvisionToExcel(new ByteArrayInputStream(transporte.getByteArray()));
		} else {
			setMensaje("No se obtuvieron provisiones con los criterios de busqueda seleccionados.");
			return "EMPTY";
		}
		if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
			setContentType("application/xls");
		}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
			setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		}else{
			setContentType("application/octect-stream");
		}
		if (getTipoReporte() == null || getTipoReporte() == 1) {
			setFileName("Reporte_Provision.xls");
		} else {
			setFileName("Reporte_Provision_Detallado.xls");
		}
	} catch (RuntimeException error) {
		error.printStackTrace();
		return INPUT;
	}
	return SUCCESS;
	}

	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.fechaVencimientoCedula,^agente\\.numeroCedula,^agente\\.fechaVencimientoFianza,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente"})
		})
		
		public String obtenerAgente(){
		try {
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
//			onSuccess();
		} catch (Exception e) {
//			onError(e);
			
		}
		return SUCCESS;
	}


	@Override
	public void prepare() throws Exception {
		try {
//			final CentroOperacion filtro = new CentroOperacion();
//			setCentroOperacionList(centroOperacionService.findByFilters(filtro));
			setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
			gerenciaList=listadoService.getMapGerencias();
			ejecutivoList=listadoService.getMapEjecutivos();
			promotoriaList = listadoService.getMapPromotorias();
			listTipoBono = configBonosService.findByFilters(null);
		} catch (Exception e) {
		}
	}
	
	

	
	
	
	public ProvisionImportesRamo getFiltroReporteProvision() {
		return filtroReporteProvision;
	}

	public void setFiltroReporteProvision(
			ProvisionImportesRamo filtroReporteProvision) {
		this.filtroReporteProvision = filtroReporteProvision;
	}

	

	public InputStream getReporteProvisionToExcel() {
		return reporteProvisionToExcel;
	}

	public void setReporteProvisionToExcel(InputStream reporteProvisionToExcel) {
		this.reporteProvisionToExcel = reporteProvisionToExcel;
	}

	public Map<Long, String> getGerenciaList() {
		return gerenciaList;
	}

	public void setGerenciaList(Map<Long, String> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}

	public Map<Long, String> getEjecutivoList() {
		return ejecutivoList;
	}

	public void setEjecutivoList(Map<Long, String> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}

	public Map<Long, String> getPromotoriaList() {
		return promotoriaList;
	}

	public void setPromotoriaList(Map<Long, String> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}

	public List<ConfigBonos> getListTipoBono() {
		return listTipoBono;
	}

	public void setListTipoBono(List<ConfigBonos> listTipoBono) {
		this.listTipoBono = listTipoBono;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	public ConfigBonos getBono() {
		return bono;
	}

	public void setBono(ConfigBonos bono) {
		this.bono = bono;
	}

	@Autowired
	@Qualifier("configBonosServiceEJB")
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public ValorCatalogoAgentes getTipAgente() {
		return tipAgente;
	}

	public void setTipAgente(ValorCatalogoAgentes tipAgente) {
		this.tipAgente = tipAgente;
	}

	
	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	

}
