package mx.com.afirme.midas2.action.catalogos.fuerzaventa.cedulasubramo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.CedulaSubramoService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/cedulasubramo")
public class CedulaSubramoAction extends CatalogoHistoricoAction {

	private static final long serialVersionUID = 8964829442869819688L;

	private List<ValorCatalogoAgentes> listaCedulas = new ArrayList<ValorCatalogoAgentes>();
	private ValorCatalogoAgentes filtroCedulas;
	private ValorCatalogoAgentes cedula;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private CedulaSubramo cedulaSubramo;

	private CedulaSubramoService cedulaSubramoService;
	private EntidadService entidadService;

	private List<RamoDTO> listaRamos = new ArrayList<RamoDTO>();
	private List<SubRamoDTO> listaSubRamos = new ArrayList<SubRamoDTO>();
	private List<CedulaSubramo> listaSubRamosAsociados = new ArrayList<CedulaSubramo>();

	private RamoDTO ramo;
	private ValorCatalogoAgentes tipoCedula;

	/*******************************************************************************************
	 * set y get
	 ******************************************************************************************/

	public CedulaSubramo getCedulaSubramo() {
		return cedulaSubramo;
	}

	public List<ValorCatalogoAgentes> getListaCedulas() {
		return listaCedulas;
	}

	public void setListaCedulas(List<ValorCatalogoAgentes> listaCedulas) {
		this.listaCedulas = listaCedulas;
	}

	public ValorCatalogoAgentes getFiltroCedulas() {
		return filtroCedulas;
	}

	public void setFiltroCedulas(ValorCatalogoAgentes filtroCedulas) {
		this.filtroCedulas = filtroCedulas;
	}

	public void setCedulaSubramo(CedulaSubramo cedulaSubramo) {
		this.cedulaSubramo = cedulaSubramo;
	}

	public ValorCatalogoAgentes getCedula() {
		return cedula;
	}

	public void setCedula(ValorCatalogoAgentes cedula) {
		this.cedula = cedula;
	}

	public List<RamoDTO> getListaRamos() {
		return listaRamos;
	}

	public void setListaRamos(List<RamoDTO> listaRamos) {
		this.listaRamos = listaRamos;
	}

	public List<SubRamoDTO> getListaSubRamos() {
		return listaSubRamos;
	}

	public void setListaSubRamos(List<SubRamoDTO> listaSubRamos) {
		this.listaSubRamos = listaSubRamos;
	}

	public RamoDTO getRamo() {
		return ramo;
	}

	public void setRamo(RamoDTO ramo) {
		this.ramo = ramo;
	}

	public ValorCatalogoAgentes getTipoCedula() {
		return tipoCedula;
	}

	public void setTipoCedula(ValorCatalogoAgentes tipoCedula) {
		this.tipoCedula = tipoCedula;
	}

	public List<CedulaSubramo> getListaSubRamosAsociados() {
		return listaSubRamosAsociados;
	}

	public void setListaSubRamosAsociados(
			List<CedulaSubramo> listaSubRamosAsociados) {
		this.listaSubRamosAsociados = listaSubRamosAsociados;
	}

	@Autowired
	@Qualifier("cedulaSubramoServiceEJB")
	public void setCedulaSubramoService(
			CedulaSubramoService cedulaSubramoService) {
		this.cedulaSubramoService = cedulaSubramoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}
	
	/*****************************************************************************************************
	 * metodos para el manejo de cedulas
	 ****************************************************************************************************/
	@Override
	public void prepare() throws Exception {
		// TODO Ap�ndice de m�todo generado autom�ticamente

	}

	public void prepareMostrarContenedor() {

	}

	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoCatalogo.jsp") })
	public String mostrarContenedor() {
		return SUCCESS;
	}

	/*** Guardar Cedula **************************************************************************************************/
	public void validateGuardar() {
		addErrors(cedula, this, "cedula");
	}

	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace",
					"/fuerzaventa/cedulasubramo", "tipoAccion",
					"${tipoAccion}", "mensaje", "${mensaje}", "tipoMensaje",
					"${tipoMensaje}", "cedula.id", "${cedula.id}",
					"idRegistro", "${cedula.id}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace",
					"/fuerzaventa/cedulasubramo", "tipoAccion",
					"${tipoAccion}", "mensaje", "${mensaje}", "tipoMensaje",
					"${tipoMensaje}", "idRegistro", "${cedula.id}" }) })
	@Override
	public String guardar() {
		try {
			GrupoCatalogoAgente grupo = valorCatalogoAgentesService
					.obtenerCatalogoPorDescripcion("Tipos de Cedula de Agente");
			cedula.setGrupoCatalogoAgente(grupo);
			// grupo = entidadService.getReference(GrupoCatalogoAgente.class,
			// grupo.getId());
			// cedula.setGrupoCatalogoAgente(grupo);
			entidadService.save(cedula);
			catalogoService.guardarElementoCatalogoEnSeycos(cedula);
			setTipoAccion("4");
			onSuccess();
		} catch (Exception e) {
			onError(e);
			return INPUT;

		}
		return SUCCESS;
	}

	/*** Eliminar Cedula **************************************************************************************************/
	@Override
	public String eliminar() {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	/*** Lista Inicial Cedulas **************************************************************************************************/
	@Action(value="listar",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoGrid.jsp")
			})
	@Override
	public String listar() {
		try {
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		
	}

	/*** Lista Filtrada de Cedulas **************************************************************************************************/
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoGrid.jsp")
			})
	@Override
	public String listarFiltrado() {
		try {
			listaCedulas = valorCatalogoAgentesService.findByTipoCedula(filtroCedulas);
			// valorCatalogoAgentesService.obtenerElementosPorCatalogo("Tipos de Cedula de Agente");
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	/*** Ver Detalle de Cedula **************************************************************************************************/
	@Action(value="verDetalle", results = {
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaSubramoDetalle.jsp") 
			})
	@Override
	public String verDetalle() {

		if (cedula != null && cedula.getId() != null) {
			try {
				cedula = valorCatalogoAgentesService.loadById(cedula);
			} catch (Exception e) {
				// TODO Bloque catch generado autom�ticamente
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}

	/*****************************************************************************************************
	 * metodos para el manejo de asociacion de cedulas
	 ****************************************************************************************************/

	@Action(value="cedulaAsociacionRamo", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/cedulasubramo/cedulaAsociacionRamo.jsp")
			})
	public String cedulaAsociacionRamo() {

		if (cedula != null && cedula.getId() != null) {
			try {
				cedula = valorCatalogoAgentesService.loadById(cedula);
				listaRamos = cedulaSubramoService.obtenerRamos();
			} catch (Exception e) {
				// TODO Bloque catch generado autom�ticamente
				e.printStackTrace();
			}
		}

		return SUCCESS;
	}

	@Action(value = "findSubramosByRamos", results = { @Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/cedulasubramo/subramosDisponiblesGrid.jsp")
	// @Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaSubRamos.*\\.idTcSubRamo,^listaSubRamos.*\\.descripcionSubRamo","excludeProperties","^listaSubRamos.*\\.ramoDTO"}),
	// @Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaSubRamos.*"})
	})
	public String findSubramosByRamos() {
		try {
			listaSubRamos = cedulaSubramoService.obtenerSubRamosPorRamo(ramo);
			if (listaSubRamos != null && !listaSubRamos.isEmpty()) {
				List<SubRamoDTO> aux = new ArrayList<SubRamoDTO>();
				for (SubRamoDTO s : listaSubRamos) {
					SubRamoDTO temp = new SubRamoDTO();
					temp.setIdTcSubRamo(s.getIdTcSubRamo());
					temp.setDescripcionSubRamo(s.getDescripcionSubRamo());
					aux.add(temp);
				}
				listaSubRamos = aux;
			}

		} catch (Exception e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value = "obtenerSubRamosAsociadosPorCedula", results = { @Result(name = SUCCESS, location = "/jsp/catalogos/fuerzaventa/cedulasubramo/subramosAsociadosGrid.jsp") })
	public String obtenerSubRamosAsociadosPorCedula() {
		try {
			listaSubRamosAsociados = cedulaSubramoService
					.obtenerSubRamosAsociadosPorCedula(tipoCedula);
		} catch (Exception e) {
			// TODO Bloque catch generado autom�ticamente
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value = "guardarAsociacionCedulaSubRamos", results = {
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","mensaje,tipoMensaje"})
	})
	public String guardarAsociacionCedulaSubRamos() {

		try {
			cedulaSubramoService.guardarAsociacion(listaSubRamos, tipoCedula);
			onSuccess();
			return SUCCESS;
		} catch (Exception e) {
			onError(e);
			return INPUT;
		}	
	}	
	
	@Action(value = "eliminarTipoCedula", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarContenedor", "namespace",
					"/fuerzaventa/cedulasubramo", "tipoAccion",
					"${tipoAccion}", "mensaje", "${mensaje}", "tipoMensaje",
					"${tipoMensaje}", "cedula.id", "${cedula.id}",
					"idRegistro", "${cedula.id}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "verDetalle", "namespace",
					"/fuerzaventa/cedulasubramo", "tipoAccion",
					"${tipoAccion}", "mensaje", "${mensaje}", "tipoMensaje",
					"${tipoMensaje}", "idRegistro", "${cedula.id}" }) })
	public String eliminarTipoCedula() {
		try{
			valorCatalogoAgentesService.deleteTipoCedula(cedula);	
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			return SUCCESS;
		}
		catch(Exception e){
			setTipoAccion("3");
			setMensaje(e.toString());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}		
	}
}
