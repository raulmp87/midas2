package mx.com.afirme.midas2.action.catalogos.fuerzaventa.provisiones;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.provisiones.ToAjusteProvision;
import mx.com.afirme.midas2.dto.fuerzaventa.AjusteProvisionDto;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesBonoAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesImportePorRamo;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.provisiones.ProvisionesBonosAgenteService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/provisiones")
public class ProvisionesAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String PROVISIONMOSTRARCONTENEDOR = "/jsp/catalogos/fuerzaventa/provisiones/provisionesCatalogo.jsp";
	private final String PROVISIONESVERDETALLE = "/jsp/catalogos/fuerzaventa/provisiones/provisionesDetalle.jsp";
	private final String PROVISIONESGRID = "/jsp/catalogos/fuerzaventa/provisiones/provisionesGrid.jsp";
	
	private ListadoService listadoService;
	private List<ProvisionesBonoAgentesView> listaProvisiones = new ArrayList<ProvisionesBonoAgentesView>();
	private ProvisionesBonosAgenteService provisionesService;
	private ProvisionesAgentes filtroProvision;
	private ProvisionesAgentes provision;
	private List<ValorCatalogoAgentes> ListEstatusProvision = new ArrayList<ValorCatalogoAgentes>();
	private List<GenericaAgentesView> comboBono = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboGerencia = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboGerente = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboEjecutivo = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboPromotoria = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboLinaVenta = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboProducto = new ArrayList<GenericaAgentesView>();
	private List<GenericaAgentesView> comboLineaNegocio = new ArrayList<GenericaAgentesView>();
	private ProvisionImportesRamo filtrosDetalleProvision =new ProvisionImportesRamo();
	private List<ProvisionesImportePorRamo> listaRamosProvision = new ArrayList<ProvisionesImportePorRamo>();
	private List<ProvisionImportesRamo> listaImporteDeRamos = new ArrayList<ProvisionImportesRamo>();
	private List<ProvisionesImportePorRamo> listaImporteTotalPorRamo = new ArrayList<ProvisionesImportePorRamo>();
	private Long idProvision;
	private AjusteProvisionDto ajusteProvision = new AjusteProvisionDto();
	private List<ToAjusteProvision> listajusteProvison= new ArrayList<ToAjusteProvision>();
//	private List<GenericaAgentesView> listaDeRamos = new ArrayList<GenericaAgentesView>();
	
	@Override
	public void prepare() throws Exception {
		
		
	}

	public void prepareMostrarContenedor(){
		
	}
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=PROVISIONMOSTRARCONTENEDOR)
	})
	public String mostrarContenedor(){
		ListEstatusProvision = cargarCatalogo("Estatus Calculo Comisiones");
		return SUCCESS;
	}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace","/fuerzaventa/provisiones","tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","provision.id","${provision.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${provision.id}"}),
					@Result(name=INPUT,type="redirectAction",
							params={"actionName","verDetalle","namespace","/fuerzaventa/provisiones","tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","provision.id","${provision.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${provision.id}"})
		})	
	
	@Override
	public String guardar() {
		try {
			provision=provisionesService.loadById(idProvision);
			filtrosDetalleProvision.setProvisionAgentes(provision);
			provisionesService.saveAjusteProvision(listajusteProvison);
//			listaImporteDeRamos = provisionesService.updateImporteRamo(listaImporteDeRamos,filtrosDetalleProvision);			
			
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="autorizar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/provisiones",
							"provision.id","${provision.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${provision.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/provisiones",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${provision.id}"})
	})
	public String autorizar() {
		try {
			Long idProvision = filtrosDetalleProvision.getProvisionAgentes().getId();
			provisionesService.autorizarProvision(idProvision);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="listar",results={
			@Result(name=SUCCESS,location=PROVISIONESGRID),
			@Result(name=INPUT,location=PROVISIONESGRID)
		})
	@Override
	public String listar() {
		try {
			listaProvisiones = provisionesService.findByFilterWiew(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=PROVISIONESGRID),
			@Result(name=INPUT,location=PROVISIONESGRID)
		})
	@Override
	public String listarFiltrado() {
		try {
			listaProvisiones = provisionesService.findByFilterWiew(filtroProvision);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location=PROVISIONESVERDETALLE)
		})
	@Override
	public String verDetalle() {
		try {
			Long idProvision = provision.getId();
			provision = provisionesService.loadById(idProvision);
			ajusteProvision = provisionesService.listAjusteProvision(idProvision);
			comboBono = provisionesService.creaFiltroBonos(idProvision);	
			comboGerencia = provisionesService.creaFiltroGerencia(idProvision);
//			comboGerente = provisionesService.creaFiltroGerente(provision.getId());
//			comboEjecutivo = provisionesService.creaFiltroEjecutivo(idProvision);
//			comboPromotoria = provisionesService.creaFiltroPromotoria(idProvision);
			comboLinaVenta = provisionesService.creaFiltroLineaVenta(provision.getId());
//			comboProducto = provisionesService.creaFiltroProducto(provision.getId());
//			comboLineaNegocio = provisionesService.creaFiltroLineaNegocio(provision.getId());
			listaImporteTotalPorRamo = provisionesService.importeTotalPorRamo(idProvision);
//			listaDeRamos = provisionesService.listaDeRamos(); 
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="buscaRamosPorFiltro",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaRamosProvision.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaRamosProvision.*"})
	})
	public String buscaRamosPorFiltro(){
		if(filtrosDetalleProvision!=null){			
			try {
				listaRamosProvision = provisionesService.getRamosByFilterProvision(filtrosDetalleProvision);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}
	
	public List<ProvisionesBonoAgentesView> getListaProvisiones() {
		return listaProvisiones;
	}

	public void setListaProvisiones(List<ProvisionesBonoAgentesView> listaProvisiones) {
		this.listaProvisiones = listaProvisiones;
	}	
	
	public ProvisionesAgentes getFiltroProvision() {
		return filtroProvision;
	}

	public void setFiltroProvision(ProvisionesAgentes filtroProvision) {
		this.filtroProvision = filtroProvision;
	}
	
	public List<ValorCatalogoAgentes> getListEstatusProvision() {
		return ListEstatusProvision;
	}

	public void setListEstatusProvision(
			List<ValorCatalogoAgentes> listEstatusProvision) {
		ListEstatusProvision = listEstatusProvision;
	}

	public List<GenericaAgentesView> getComboBono() {
		return comboBono;
	}

	public void setComboBono(List<GenericaAgentesView> comboBono) {
		this.comboBono = comboBono;
	}

	
	public List<GenericaAgentesView> getComboGerencia() {
		return comboGerencia;
	}

	public void setComboGerencia(List<GenericaAgentesView> comboGerencia) {
		this.comboGerencia = comboGerencia;
	}

	public List<GenericaAgentesView> getComboGerente() {
		return comboGerente;
	}

	public void setComboGerente(List<GenericaAgentesView> comboGerente) {
		this.comboGerente = comboGerente;
	}

	public List<GenericaAgentesView> getComboEjecutivo() {
		return comboEjecutivo;
	}

	public void setComboEjecutivo(List<GenericaAgentesView> comboEjecutivo) {
		this.comboEjecutivo = comboEjecutivo;
	}

	public List<GenericaAgentesView> getComboPromotoria() {
		return comboPromotoria;
	}

	public void setComboPromotoria(List<GenericaAgentesView> comboPromotoria) {
		this.comboPromotoria = comboPromotoria;
	}

	public List<GenericaAgentesView> getComboLinaVenta() {
		return comboLinaVenta;
	}

	public void setComboLinaVenta(List<GenericaAgentesView> comboLinaVenta) {
		this.comboLinaVenta = comboLinaVenta;
	}

	public List<GenericaAgentesView> getComboProducto() {
		return comboProducto;
	}

	public void setComboProducto(List<GenericaAgentesView> comboProducto) {
		this.comboProducto = comboProducto;
	}

	public List<GenericaAgentesView> getComboLineaNegocio() {
		return comboLineaNegocio;
	}

	public void setComboLineaNegocio(List<GenericaAgentesView> comboLineaNegocio) {
		this.comboLineaNegocio = comboLineaNegocio;
	}

	public ProvisionImportesRamo getFiltrosDetalleProvision() {
		return filtrosDetalleProvision;
	}

	public void setFiltrosDetalleProvision(
			ProvisionImportesRamo filtrosDetalleProvision) {
		this.filtrosDetalleProvision = filtrosDetalleProvision;
	}

	public List<ProvisionesImportePorRamo> getListaRamosProvision() {
		return listaRamosProvision;
	}

	public void setListaRamosProvision(List<ProvisionesImportePorRamo> listaRamosProvision) {
		this.listaRamosProvision = listaRamosProvision;
	}

	public ProvisionesAgentes getProvision() {
		return provision;
	}

	public void setProvision(ProvisionesAgentes provision) {
		this.provision = provision;
	}
	
	public List<ProvisionImportesRamo> getListaImporteDeRamos() {
		return listaImporteDeRamos;
	}

	public void setListaImporteDeRamos(
			List<ProvisionImportesRamo> listaImporteDeRamos) {
		this.listaImporteDeRamos = listaImporteDeRamos;
	}
	
	

	public List<ProvisionesImportePorRamo> getListaImporteTotalPorRamo() {
		return listaImporteTotalPorRamo;
	}

	public void setListaImporteTotalPorRamo(
			List<ProvisionesImportePorRamo> listaImporteTotalPorRamo) {
		this.listaImporteTotalPorRamo = listaImporteTotalPorRamo;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("provisionBonoAgenteEJB")
	public void setProvisionesService(ProvisionesBonosAgenteService provisionesService) {
		this.provisionesService = provisionesService;
	}

	public Long getIdProvision() {
		return idProvision;
	}

	public void setIdProvision(Long idProvision) {
		this.idProvision = idProvision;
	}

	public AjusteProvisionDto getAjusteProvision() {
		return ajusteProvision;
	}

	public void setAjusteProvision(AjusteProvisionDto ajusteProvision) {
		this.ajusteProvision = ajusteProvision;
	}

	public List<ToAjusteProvision> getListajusteProvison() {
		return listajusteProvison;
	}

	public void setListajusteProvison(List<ToAjusteProvision> listajusteProvison) {
		this.listajusteProvison = listajusteProvison;
	}



//	public List<GenericaAgentesView> getListaDeRamos() {
//		return listaDeRamos;
//	}
//
//	public void setListaDeRamos(List<GenericaAgentesView> listaDeRamos) {
//		this.listaDeRamos = listaDeRamos;
//	}	
}

