package mx.com.afirme.midas2.action.catalogos.tarifa.agrupador.seccion;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccionId;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaSeccionService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaService;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AgrupadorSeccionAction extends CatalogoAction implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String negocio;
	
	private AgrupadorTarifaSeccionId id;
	private String key;

	private AgrupadorTarifaSeccion agrupadorTarifaSeccion;
	
	private List<AgrupadorTarifaSeccion> agrupadorTarifaSeccionList;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
	
	private List<SeccionDTO> seccionesList;
	private List<MonedaDTO> monedasList;
	 private Map<BigDecimal, String> agrupadoresMap;
	
	private AgrupadorTarifaService agrupadorTarifaService;
	private AgrupadorTarifaSeccionService agrupadorTarifaSeccionService;
	private SeccionFacadeRemote seccionFacadeRemote;
	private MonedaFacadeRemote monedaFacadeRemote;
	
	
	public AgrupadorSeccionAction(){
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	
	public void validateGuardar(){
		if(negocio.equals("D")) agrupadorTarifaSeccion.getId().setIdMoneda(new Long(484));
		addErrors(agrupadorTarifaSeccion, NewItemChecks.class, this, "agrupadorTarifaSeccion");
		initDetalle();
		
	}
	
	@Override
	public void prepare() throws Exception {
	
		if(getId() != null){
			agrupadorTarifaSeccion = entidadService.findById(AgrupadorTarifaSeccion.class, getId());
		}
		if(getKey() != null && !getKey().equals("")){
			setId(new AgrupadorTarifaSeccionId(key));
			agrupadorTarifaSeccion = entidadService.findById(AgrupadorTarifaSeccion.class, getId());
		}
	}
	
	public void prepareGuardar() throws Exception {
		AgrupadorTarifaSeccion agrupadorTarifaSeccionSalvado = null;
		if(getId() != null){
			agrupadorTarifaSeccionSalvado = entidadService.findById(AgrupadorTarifaSeccion.class, agrupadorTarifaSeccion.getId());	
			agrupadorTarifaSeccionSalvado.setId(agrupadorTarifaSeccion.getId());
			agrupadorTarifaSeccionSalvado.setClaveDefault(agrupadorTarifaSeccion.getClaveDefault());
			agrupadorTarifaSeccionSalvado.setMonedaDTO(agrupadorTarifaSeccion.getMonedaDTO());
			agrupadorTarifaSeccionSalvado.setSeccionDTO(agrupadorTarifaSeccion.getSeccionDTO());
			agrupadorTarifaSeccionSalvado.setAgrupadorTarifa(agrupadorTarifaSeccion.getAgrupadorTarifa());
			agrupadorTarifaSeccion = agrupadorTarifaSeccionSalvado;			
		}else{
			agrupadorTarifaSeccionSalvado = entidadService.findById(AgrupadorTarifaSeccion.class, agrupadorTarifaSeccion.getId());
			initDetalle();
			if(agrupadorTarifaSeccionSalvado != null){
				super.setMensajeError("El registro ya existe");
				throw new RuntimeException("El registro ya existe");
			}
			
		}
	}

	@Override
	public String guardar() {
		//Guardar en el entity las referencias a Seccion y Moneda
		agrupadorTarifaSeccion.setSeccionDTO(seccionFacadeRemote.findById(new BigDecimal(agrupadorTarifaSeccion.getId().getIdToSeccion())));
		agrupadorTarifaSeccion.setMonedaDTO(monedaFacadeRemote.findById(new Short(agrupadorTarifaSeccion.getId().getIdMoneda().toString())));
		agrupadorTarifaSeccionService.save(agrupadorTarifaSeccion);
		agrupadorTarifaSeccion = new AgrupadorTarifaSeccion();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		entidadService.remove(agrupadorTarifaSeccion);
		agrupadorTarifaSeccion = new AgrupadorTarifaSeccion();
		initDetalle();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String listar() {
//		agrupadorTarifaSeccionList = agrupadorTarifaSeccionService.findByBusiness(negocio);
		return SUCCESS;
	}

	@Override
	public String listarFiltrado() {
		
		if(agrupadorTarifaSeccion==null)
			agrupadorTarifaSeccion = new AgrupadorTarifaSeccion();		

		if(negocio.equals("D"))
			agrupadorTarifaSeccion.getMonedaDTO().setIdTcMoneda((short) 484);
		
		agrupadorTarifaSeccionList = agrupadorTarifaSeccionService.findByBusiness(negocio, agrupadorTarifaSeccion);
		
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		initDetalle();
		
		return SUCCESS;
	}
	
	public String mostrarCatalogo(){
		beforeMostrarCatalogo();
		return SUCCESS;
	}
	
	private void beforeMostrarCatalogo(){
//		seccionesList = seccionFacadeRemote.listarVigentesPorNegocio(negocio);
//		monedasList = monedaFacadeRemote.findAll();
		initDetalle();
	}
	
	private void initDetalle(){
		seccionesList = seccionFacadeRemote.listarVigentesPorNegocio(negocio);
		monedasList = monedaFacadeRemote.findAll();
		AgrupadorTarifa filtroAgrupadorTarifa = new AgrupadorTarifa();
		filtroAgrupadorTarifa.setClaveNegocio(negocio);
		if(negocio.equals("D")){
			agrupadoresMap = agrupadorTarifaService.findByNegocio(negocio,new Long(484));
		}else{
			if(agrupadorTarifaSeccion != null && agrupadorTarifaSeccion.getId().getIdMoneda()!=null){
				agrupadoresMap = agrupadorTarifaService.findByNegocio(negocio, new Long(agrupadorTarifaSeccion.getId().getIdMoneda()));
			}else{
				agrupadoresMap =  new LinkedHashMap<BigDecimal, String>();
			}
		}
		
	}
	
	public AgrupadorTarifaSeccionId getId() {
		return id;
	}
	public void setId(AgrupadorTarifaSeccionId id) {
		this.id = id;
	}
	public AgrupadorTarifaSeccion getAgrupadorTarifaSeccion() {
		return agrupadorTarifaSeccion;
	}
	public void setAgrupadorTarifaSeccion(
			AgrupadorTarifaSeccion agrupadorTarifaSeccion) {
		this.agrupadorTarifaSeccion = agrupadorTarifaSeccion;
	}
	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}
	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}
	public List<AgrupadorTarifaSeccion> getAgrupadorTarifaSeccionList() {
		return agrupadorTarifaSeccionList;
	}
	public void setAgrupadorTarifaSeccionList(
			List<AgrupadorTarifaSeccion> agrupadorTarifaSeccionList) {
		this.agrupadorTarifaSeccionList = agrupadorTarifaSeccionList;
	}
	public String getNegocio(){
		return negocio;
	}
	public void setNegocio(String negocio){
		this.negocio = negocio.trim();
	}
	public List<SeccionDTO> getSeccionesList(){
		return seccionesList;
	}
	public void setSeccionesList(List<SeccionDTO> seccionesList){
		this.seccionesList = seccionesList;
	}
	public List<MonedaDTO> getMonedasList(){
		return monedasList;
	}
	public void setMonedasList(List<MonedaDTO> monedasList){
		this.monedasList = monedasList;
	}

	public Map<BigDecimal, String> getAgrupadoresMap() {
		return agrupadoresMap;
	}

	public void setAgrupadoresMap(Map<BigDecimal, String> agrupadoresMap) {
		this.agrupadoresMap = agrupadoresMap;
	}

	public String getKey(){
		return key;
	}
	public void setKey(String key){
		this.key = key;
	}
	@Autowired
	@Qualifier("agrupadorTarifaEJB")
	public void setAgrupadoTarifaService(AgrupadorTarifaService agrupadorTarifaService){
		this.agrupadorTarifaService = agrupadorTarifaService;
	}
	@Autowired
	@Qualifier("agrupadorTarifaSeccionEJB")
	public void setAgrupadorTarifaSeccionService(
			AgrupadorTarifaSeccionService agrupadorTarifaSeccionService) {
		this.agrupadorTarifaSeccionService = agrupadorTarifaSeccionService;
	}
	
	@Autowired
	@Qualifier("seccionEJB")
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote){
		this.seccionFacadeRemote = seccionFacadeRemote;
	}
	
	@Autowired
	@Qualifier("monedaEJB")
	public void setMonedaFacadeRemote(MonedaFacadeRemote monedaFacadeRemote){
		this.monedaFacadeRemote = monedaFacadeRemote;
	}

}
