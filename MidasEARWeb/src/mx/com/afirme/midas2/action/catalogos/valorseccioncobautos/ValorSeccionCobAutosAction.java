package mx.com.afirme.midas2.action.catalogos.valorseccioncobautos;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutosId;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ValorSeccionCobAutosAction extends CatalogoAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5595046599841643524L;

	public void validateGuardar(){
		addErrors(valorSeccionCobAutos, NewItemChecks.class, this, "valorSeccionCobAutos");
		addErrors(valorSeccionCobAutos.getId(), NewItemChecks.class, this, "valorSeccionCobAutos.id");
		initDetalle();
	}
	
	public void validateEditar(){
		addErrors(valorSeccionCobAutos, EditItemChecks.class, this, "valorSeccionCobAutos");
	}
	
	@Override
	public void prepare() throws Exception {
		if (getId() != null) {
			valorSeccionCobAutos = entidadService.findById(ValorSeccionCobAutos.class, getId());
		}
		
	}
	

	public void prepareMostrarCatalogo() throws Exception {
		if (getId() != null) {
			valorSeccionCobAutos = new ValorSeccionCobAutos();
			id = new ValorSeccionCobAutosId();
		}
		
	}
	
	
	public String mostrarCatalogo(){
		beforeMostrarCatalogo();
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		entidadService.remove(valorSeccionCobAutos);
		setMensajeExito();
		beforeMostrarCatalogo();
		return SUCCESS;
	}

	@Override
	public String guardar() {
		
		if(valorSeccionCobAutos.getValorSumaAseguradaMin().intValue()>valorSeccionCobAutos.getValorSumaAseguradaMax().intValue()){
			setMensaje("El Valor Suma Asegurada M\u00EDnimo no puede ser Mayor a la Suma Asegurada M\u00E1xima");
			return INPUT ;
		}

		if(valorSeccionCobAutos.getValorSumaAseguradaDefault().intValue()< valorSeccionCobAutos.getValorSumaAseguradaMin().intValue()){
			setMensaje("El Valor Suma Asegurada Default no puede ser menor a la Suma Asegurada M\u00EDnima");
			setTipoMensaje("30");
			return INPUT;
		}

		if(valorSeccionCobAutos.getValorSumaAseguradaDefault().intValue()> valorSeccionCobAutos.getValorSumaAseguradaMax().intValue()){
			setMensaje("El Valor Suma Asegurada Default no puede ser Mayor a la Suma Asegurada M\u00E1xima");
			setTipoMensaje("30");
			return INPUT;
		}			
		entidadService.save(valorSeccionCobAutos);
		setMensajeExito();
		beforeMostrarCatalogo();
		return SUCCESS;
	}

	@Override
	public String listar() {
		return SUCCESS;
	}
	
	@Override
	public String listarFiltrado() {
		valorSeccionCobAutosList = listadoService.getListarValorSeccionCobAutosPorSeccion(valorSeccionCobAutos.getId().getIdToSeccion());
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		initDetalle();
		
		return SUCCESS;
	}
	
	private void beforeMostrarCatalogo(){
		seccionList = listadoService.getListarSeccionesVigentesAutosUsables();
	}
	
	private void initDetalle(){
		seccionList = listadoService.getListarSeccionesVigentesAutosUsables();
		coberturaSeccionMap = new LinkedHashMap<BigDecimal, String>();
		monedaList = listadoService.getListarMonedas();
		tipoUsoVehiculoMap = new LinkedHashMap<BigDecimal, String>();
		tipoLimiteSumaAsegMap = listadoService.getMapTipoLimiteSumaAseg();
		
		if(valorSeccionCobAutos != null && valorSeccionCobAutos.getId().getIdToSeccion() !=null){
			coberturaSeccionMap = listadoService.getMapCoberturasSeccionPorSeccion(BigDecimal.valueOf(valorSeccionCobAutos.getId().getIdToSeccion()));
			tipoUsoVehiculoMap = listadoService.getMapTipoUsoVehiculoPorSeccion(BigDecimal.valueOf(valorSeccionCobAutos.getId().getIdToSeccion()));
		}
	}

	
	public ValorSeccionCobAutosAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	
	
	
	private ValorSeccionCobAutosId id;

	private String tipoAccion;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
		
	private ValorSeccionCobAutos valorSeccionCobAutos;
	
	private List<SeccionDTO> seccionList;
	
    private List<ValorSeccionCobAutos> valorSeccionCobAutosList;
    
    private Map<BigDecimal, String>  coberturaSeccionMap;
    
    private Map<BigDecimal, String> tipoUsoVehiculoMap;
    
    private List<MonedaDTO> monedaList;
    
    private Map<Short, String> tipoLimiteSumaAsegMap;
    

	public  ValorSeccionCobAutosId getId() {
		return id;
	}

	public void setId(ValorSeccionCobAutosId id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	public ValorSeccionCobAutos getValorSeccionCobAutos() {
		return valorSeccionCobAutos;
	}

	public void setValorSeccionCobAutos(ValorSeccionCobAutos valorSeccionCobAutos) {
		this.valorSeccionCobAutos = valorSeccionCobAutos;
	}

	public List<SeccionDTO> getSeccionList() {
		return seccionList;
	}

	public void setSeccionList(List<SeccionDTO> seccionList) {
		this.seccionList = seccionList;
	}

	public List<ValorSeccionCobAutos> getValorSeccionCobAutosList() {
		return valorSeccionCobAutosList;
	}

	public void setValorSeccionCobAutosList(
			List<ValorSeccionCobAutos> valorSeccionCobAutosList) {
		this.valorSeccionCobAutosList = valorSeccionCobAutosList;
	}

	public Map<BigDecimal, String> getCoberturaSeccionMap() {
		return coberturaSeccionMap;
	}

	public void setCoberturaSeccionMap(
			Map<BigDecimal, String> coberturaSeccionMap) {
		this.coberturaSeccionMap = coberturaSeccionMap;
	}

	public Map<BigDecimal, String> getTipoUsoVehiculoMap() {
		return tipoUsoVehiculoMap;
	}

	public void setTipoUsoVehiculoMap(Map<BigDecimal, String> tipoUsoVehiculoMap) {
		this.tipoUsoVehiculoMap = tipoUsoVehiculoMap;
	}

	public List<MonedaDTO> getMonedaList() {
		return monedaList;
	}

	public void setMonedaList(List<MonedaDTO> monedaList) {
		this.monedaList = monedaList;
	}
	
	public Map<Short, String> getTipoLimiteSumaAsegMap() {
		return tipoLimiteSumaAsegMap;
	}

	public void setTipoLimiteSumaAsegMap(Map<Short, String> tipoLimiteSumaAsegMap) {
		this.tipoLimiteSumaAsegMap = tipoLimiteSumaAsegMap;
	}



	private ListadoService listadoService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(
			ListadoService listadoService) {
		this.listadoService = listadoService;
	}


}
