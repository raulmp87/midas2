package mx.com.afirme.midas2.action.catalogos.fuerzaventa.gerencia;
import static mx.com.afirme.midas2.utils.SystemCommonUtils.*;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/gerencia")
@Component
@Scope("prototype")
public class GerenciaAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4814866545981130881L;
	private GerenciaJPAService gerenciaJpaService;
	private EjecutivoService ejecutivoService;
	private CentroOperacionService centroOperacionService;
	private final String NAMESPACE="/fuerzaventa/gerencia";
	/***Components of view****************************************/
	private Gerencia gerencia;
	private Gerencia filtroGerencia;
	private List<Gerencia> listaGerencia = new ArrayList<Gerencia>();
	private List<GerenciaView> listaGerenciaView = new ArrayList<GerenciaView>();
	private List<CentroOperacionView> listaCentrosOperacion=new ArrayList<CentroOperacionView>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	private List<Ejecutivo> listEjecutivos = new ArrayList<Ejecutivo>();
	private static CentroOperacion centroOperacionFindAll;
	/**
	 * Action:
	 * 
	 */
	
	@Override
	public void prepare() throws Exception {
		if(gerencia!=null && "1".equals(tipoAccion)){
			gerencia=new Gerencia();
		}
	}
	
	public void prepareMostrarContenedor(){
//		filtroGerencia=getFilterInSession(Gerencia.class);
	}
	
	@Action(value="mostrarContenedor",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/mostrar.jsp")
	})
	public String mostrarContenedor(){
		cargarCentrosOperacion();
		gerencia=new Gerencia();
		return SUCCESS;
	}
	
	public void prepareGuardar(){
		if(gerencia!=null && gerencia.getId()!=null){
			gerencia=gerenciaJpaService.loadById(gerencia);
			setIdTipoOperacion(20L);
			setIdRegistro(gerencia.getId());
		}
	}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","gerencia.id","${gerencia.id}","idTipoOperacion","${idTipoOperacion}","idRegistro","${gerencia.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${gerencia.id}"})})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			if(gerencia.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			gerencia=gerenciaJpaService.saveFull(gerencia);
            guardarHistorico(TipoOperacionHistorial.GERENCIA, gerencia.getId(),"midas.gerencia.historial.accion",tipoAccionHistorial);
            onSuccess();
            tipoAccion="4";            
            setIdTipoOperacion(20L);
		} catch (Exception e) {
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
//			onError(e);
			return INPUT;
		}
		return result;
	}
	public void validateGuardar() {
		String domainObjectPrefix = "gerencia";
		 addErrors(gerencia, NewItemChecks.class, this, domainObjectPrefix);
	}

	@Action(value="eliminar",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","gerencia.id","${gerencia.id}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${gerencia.id}"}),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/gerencia/detalle.jsp")
	})
	@Override
	public String eliminar() {
		try {
			if(gerencia.getDomicilio().getIdDomicilio().getIdDomicilio()!=null){
				setIdTipoOperacion(20L);
				gerenciaJpaService.unsubscribe(gerencia);
				guardarHistorico(TipoOperacionHistorial.GERENCIA, gerencia.getId(),"midas.gerencia.historial.accion",TipoAccionHistorial.BAJA);
				onSuccess();
			}
			else{
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("Es necesario completar la dirección para eliminar la gerencia");
				setTipoAccion("3");
			}
		}catch(Exception e){
			gerencia.setClaveEstatus(1l);
			setMensajeError(e.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="lista",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/gerenciaGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/gerencia/gerenciaGrid.jsp")
	})
	@Override
	public String listar() {
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/gerenciaGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/gerencia/gerenciaGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		try{
			if("consulta".equals(tipoAccion)){
				if(filtroGerencia==null){
					filtroGerencia=new Gerencia();
               }
				filtroGerencia.setClaveEstatus(1L);
			}
			//setFilterInSession(filtroGerencia);
			listaGerenciaView=gerenciaJpaService.findByFiltersView(filtroGerencia);
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/detalle.jsp")
		})
	@Override
	public String verDetalle() {
		if(gerencia != null){
			if(gerencia.getId()!=null){
				gerencia = gerenciaJpaService.loadById(gerencia);
			}
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/detalle.jsp")
		})	
	public String verDetalleHistorico() {
		if(gerencia != null){
			if(gerencia.getId()!=null){
				gerencia = gerenciaJpaService.loadById(gerencia, this.getUltimaModificacion().getFechaHoraActualizacionString());
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}

	@Action(value="mostrarCentroOperacion",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/mostrarCentroOperacion.jsp")
		})
	public String mostrarCentroOperacion(){
		return SUCCESS;
	}
	
	@Action(value="listarCentroOperacion",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/listarCentroOperacion.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/gerencia/listarCentroOperacion.jsp")
	})
	public String listarCentroOperacion(){
		
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoCentroOperacion",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/listarCentroOperacion.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/gerencia/listarCentroOperacion.jsp")
	})
	public String listarFiltradoCentroOperacion(){
		return SUCCESS;
	}
	
	@Action(value="mostrarEjecutivos",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/gerencia/mostrarEjecutivos.jsp")
		})
	public String mostrarEjecutivos(){
		try{
			listEjecutivos = ejecutivoService.findByGerencia(gerencia.getId());
		}catch(Exception e){
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion","excludeProperties","^gerencia\\.ejecutivos.*,^gerencia\\.personaResponsable.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion"})
	})
	
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}

	private void cargarCentrosOperacion(){
		//Se manda un filtro en blanco para que regrese la lista de todos los centros de operacion.
		if(isNull(centroOperacionFindAll)){
			centroOperacionFindAll=new CentroOperacion();
		}
		listaCentrosOperacion=centroOperacionService.findByFiltersView(centroOperacionFindAll);
	}

	/***Sets and gets****************************************************/
	

	@Autowired
	@Qualifier("gerenciaJPAServiceEJB")
	public void setGerenciaJpaService(GerenciaJPAService gerenciaJpaService) {
		this.gerenciaJpaService = gerenciaJpaService;
	}

	@Autowired
	@Qualifier("ejecutivoServiceEJB")
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	
	@Autowired
	@Qualifier("centroOperacionServiceEJB")
	public void setCentroOperacionService(
			CentroOperacionService centroOperacionService) {
		this.centroOperacionService = centroOperacionService;
	}
	
	public List<CentroOperacionView> getListaCentrosOperacion() {
		return listaCentrosOperacion;
	}

	public void setListaCentrosOperacion(List<CentroOperacionView> listaCentrosOperacion) {
		this.listaCentrosOperacion = listaCentrosOperacion;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}
	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}
	public Gerencia getFiltroGerencia() {
		return filtroGerencia;
	}
	public void setFiltroGerencia(Gerencia filtroGerencia) {
		this.filtroGerencia = filtroGerencia;
	}
	public List<Gerencia> getListaGerencia() {
		return listaGerencia;
	}
	public void setListaGerencia(List<Gerencia> listaGerencia) {
		this.listaGerencia = listaGerencia;
	}
	
	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public List<Ejecutivo> getListEjecutivos() {
		return listEjecutivos;
	}

	public void setListEjecutivos(List<Ejecutivo> listEjecutivos) {
		this.listEjecutivos = listEjecutivos;
	}

	public List<GerenciaView> getListaGerenciaView() {
		return listaGerenciaView;
	}

	public void setListaGerenciaView(List<GerenciaView> listaGerenciaView) {
		this.listaGerenciaView = listaGerenciaView;
	}	
	
}

