package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/reporteAgenteResumenDetalleBonos")
@Component
@Scope("prototype")
public class ReporteAgenteResumenDetalleBonoAction extends ReporteAgenteBaseAction implements ReportMethods, Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1615936761653867129L;
	public static final String PANTALLA_FILTROS = "/jsp/reportesAgentes/reporteAgenteResumenDetBono.jsp";
	public static final String GRID_AGENTES = "/jsp/reportesAgentes/reporteResumenDetalleBonoAgenteGrid.jsp";
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	private List<AgenteView> listaAgentesView = new ArrayList<AgenteView>(); 
	private List<Gerencia> gerenciaList = new ArrayList<Gerencia>(); 
	private List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
	private List<Promotoria> promorotiaList = new ArrayList<Promotoria>();
	
	private CentroOperacion centroOperacion = new CentroOperacion();
	private Gerencia gerencia = new Gerencia();
	private Ejecutivo ejecutivo = new Ejecutivo();
	private Promotoria promotoria = new Promotoria();
	private ValorCatalogoAgentes tipoAgentes = new ValorCatalogoAgentes();
	private ConfigBonosService configBonosService;
	private Long sinFiltro;

	public List<AgenteView> getListaAgentesView() {
		return listaAgentesView;
	}

	public void setListaAgentesView(List<AgenteView> listaAgentesView) {
		this.listaAgentesView = listaAgentesView;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public AgenteMidasService getAgenteMidasService() {
		return agenteMidasService;
	}

	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public CentroOperacion getCentroOperacion() {
		return centroOperacion;
	}

	public void setCentroOperacion(CentroOperacion centroOperacion) {
		this.centroOperacion = centroOperacion;
	}

	public Gerencia getGerencia() {
		return gerencia;
	}

	public void setGerencia(Gerencia gerencia) {
		this.gerencia = gerencia;
	}

	public Ejecutivo getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(Ejecutivo ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Promotoria getPromotoria() {
		return promotoria;
	}

	public void setPromotoria(Promotoria promotoria) {
		this.promotoria = promotoria;
	}

	public ValorCatalogoAgentes getTipoAgentes() {
		return tipoAgentes;
	}

	public void setTipoAgentes(ValorCatalogoAgentes tipoAgentes) {
		this.tipoAgentes = tipoAgentes;
	}
	
	@EJB
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}
	
	public List<Gerencia> getGerenciaList() {
		return gerenciaList;
	}

	public void setGerenciaList(List<Gerencia> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}

	public List<Ejecutivo> getEjecutivoList() {
		return ejecutivoList;
	}

	public void setEjecutivoList(List<Ejecutivo> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}

	public List<Promotoria> getPromorotiaList() {
		return promorotiaList;
	}

	public void setPromorotiaList(List<Promotoria> promorotiaList) {
		this.promorotiaList = promorotiaList;
	}

	public Long getSinFiltro() {
		return sinFiltro;
	}

	public void setSinFiltro(Long sinFiltro) {
		this.sinFiltro = sinFiltro;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}
	
	public void prepareMostrarFiltros() throws Exception {

		try {
			setCentroOperacionList(this.entidadService.findAll(CentroOperacion.class));
			setTipoAgente(valorCatalogoAgentesService
					.obtenerElementosPorCatalogo("Clasificacion de Agente"));// se cambia el catalogo Tipo de Agente por Clasificacion de Agente
			agente = new Agente();
			setGerenciaList(new LinkedList<Gerencia>());
			setPromorotiaList(new LinkedList<Promotoria>());
			setEjecutivoList(new LinkedList<Ejecutivo>());
		} catch (Exception e) {
//			prioridadList = new LinkedList<ValorCatalogoAgentes>();
		}
	}
	@Override
	@Action(value = "mostrarFiltros", results={@Result(name=SUCCESS, location=PANTALLA_FILTROS)})
	public String mostrarFiltros() {
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Action(value="exportarToExcel", results={
			@Result(name=SUCCESS, type="stream", params= { "contentType",
					"${contentType}", "contentDisposition",
					"attachment;filename=\"${fileName}\"", "inputName",
					"reporteAgenteStream" }),
			@Result(name=INPUT, location= PAGERROR, params = {
					"mensaje", "${mensaje}" })
	})
	public String exportarToExcel() {
//		try{
//			TransporteImpresionDTO transporte = 
//				getGenerarPlantillaReporteService()
//						.imprimirReporteResumenDetalleBonosExcel(agente.getIdAgente());
		
//			if(transporte!=null){
//				
//				setReporteAgenteStream(new ByteArrayInputStream(
//						transporte.getByteArray()));
//			} else {
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
//			}
//			if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_EXCEL.getValue())){
//				setContentType("application/xls");
//			}else if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
//				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//			}else{
//				setContentType("application/octect-stream");
//			}
//				setFileName("reporteDetalleBonos."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
//		} catch (RuntimeException error) {
//			error.printStackTrace();
//			setMensaje(error.getMessage());
//			return INPUT;
//		}
//	return SUCCESS;
	}

	@Action(value = "listarDocumentos", results = { @Result(name = SUCCESS, location = GRID_AGENTES ) })
	public String listarDocumentos() {
		

			try {
				listaAgentesView  = new ArrayList<AgenteView>();
				if (getSinFiltro().equals(0L)){
					listaAgentesView = configBonosService
							.obtenerAgentesPorFuerzaVenta(
									centroOperacion.getId(),
									ejecutivo.getId(),
									gerencia.getId(),
									promotoria.getId(),
									tipoAgentes.getId(),
									agente.getIdAgente());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		return SUCCESS;
	}
}
