package mx.com.afirme.midas2.action.catalogos.tarifa.descuentoAgenteMovil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;


import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.excels.CargarExcelDescuentoAgenteMovil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.tarifa.DescuentoAgenteService;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/descuentoAgenteMovil")
@Component
@Scope("prototype")
public class DescuentoAgenteMovilAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4814866545981130881L;
	@Autowired
	private DescuentoAgenteService descuentoAgenteService;
	private final String NAMESPACE="/tarifa/descuentoAgenteMovil";
	/***Components of view****************************************/
	private DescuentosAgenteDTO descuentosAgente;
	
	private List<DescuentosAgenteDTO> listaDescuentoAgente= new ArrayList<DescuentosAgenteDTO>();
	private TipoAccionDTO tipoAccionCentro;
	private String tipoAccion; 
	private EntidadService entidadService;
	private String claveNegocio;
	private String claveAgente;
	/*carga masiva*/
	private List <String>lista=new ArrayList<String>();
    private int registrosInsertados;
    private int registrosNoInsertados;
    private String logErrores="";
    private String tabActiva;
    /**/
	File fileUpload;
	@EJB
	private AgenteMidasService agenteMidasService;
	CargarExcelDescuentoAgenteMovil cargarExcelDescuentoAgenteMovil;
	/**
	 * Action:
	 * 
	 */
	
	@Override
	public void prepare() throws Exception {
		if(descuentosAgente!=null && "1".equals(tipoAccion)){
			descuentosAgente=new DescuentosAgenteDTO();
		}
	}
	public String verCatalogoDescuentoAgenteMovil(){
		this.setTabActiva("descuento_agente_movil");
		return SUCCESS;
	}
	public void prepareCargarDescuentoAgente(){
	}
	
	public String cargarDescuentoAgente(){
		try{
			long time_start, time_end;
			time_start = System.currentTimeMillis();
			
			cargarExcelDescuentoAgenteMovil = new CargarExcelDescuentoAgenteMovil();
			descuentoAgenteService.setDescuentoAgenteExcelList(cargarExcelDescuentoAgenteMovil.leerXLS(fileUpload));
			//tarifasService.cargaMasiva(getFileUpload().toString());
			time_end = System.currentTimeMillis();
			long timpoCargaMls=time_end - time_start;
			String mensaje="Tiempo carga =" + timpoCargaMls +"milisegundos" + ";" + timpoCargaMls/1000 +"segundos" ;
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setMensaje("Acción realizada correctamente " + mensaje);
			System.out.print(mensaje);
		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	@Action(value="cargaMasiva", results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","respuestaCargaMasivaAgente","namespace",NAMESPACE,//"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}",
						"registrosInsertados","${registrosInsertados}","registrosNoInsertados","${registrosNoInsertados}","logErrores","${logErrores}"
					})		
		})
		
		public String cargaMasiva(){
			try {
				lista = descuentoAgenteService.cargaMasiva(getFileUpload().toString());
				registrosInsertados = Integer.parseInt(lista.get(lista.size()-2));
				registrosNoInsertados = Integer.parseInt(lista.get(lista.size()-1));
				if(lista.size()>=3){
					logErrores += obtenerLogErrores(lista);
				}
				setTipoMensaje(TIPO_MENSAJE_EXITO);
				setMensaje("Acción realizada correctamente");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lista.clear();
			return SUCCESS;
		}
	
	public String obtenerLogErrores(List<String> lista) {
		StringBuilder log = new StringBuilder(logErrores);
		for(int i=0; i<lista.size()-2;i++){
			log.append("<br>").append(lista.get(i));
		}
		return log.toString();
	}
	@Action(value="respuestaCargaMasivaAgente", results={
			@Result(name=SUCCESS,location ="/jsp/catalogos/tarifa/descuentoAgenteMovil/respuestaCargaMasivaDescuentoAgenteMovil.jsp")	
		})
		
		public String respuestaCargaMasivaAgente(){
			return SUCCESS;
	}
	public void prepareMostrarContenedor(){
	}
	
	@Action(value="mostrarContenedor",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/mostrar.jsp")
	})
	public void prepareMostrarContenedorPrincipal(){
	}
	@Action(value="mostrarContenedorPrincipal",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/contenedor.jsp")
		})
		public String mostrarContenedorPrincipal(){
			descuentosAgente = new DescuentosAgenteDTO();
			return SUCCESS;
		}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","descuentosAgente.idtcdescuentosagente","${descuentosAgente.idtcdescuentosagente}","idTipoOperacion","${idTipoOperacion}","idRegistro","${descuentosAgente.idtcdescuentosagente}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idTipoOperacion","${idTipoOperacion}","idRegistro","${descuentosAgente.idtcdescuentosagente}"})})
	@Override
	public String guardar() {
		try {
			Agente agente=new Agente();
			boolean agenteValido=false;
			agente.setIdAgente(new Long(descuentosAgente.getClaveagente()));
			List<Agente> agenteList;
			agenteList= agenteMidasService.findByFilters(agente);
			for(Agente agenteTmp:agenteList){
				if(agenteTmp!=null)
					agenteValido=true;
			}	
				if(agenteValido == true){
					descuentosAgente.setClaveagente(descuentosAgente.getClaveagente().replace(",","").trim());
					descuentosAgente.setPorcentaje(descuentosAgente.getPorcentaje().replace("%","").trim());
					if(descuentosAgente.getIdtcdescuentosagente() == null){
							descuentosAgente=descuentoAgenteService.save(descuentosAgente);
					}else{
						result=descuentoAgenteService.update(descuentosAgente);
						if(result.equals("duplicado")){
							setMensaje("La clave promo ya existe");
							setTipoMensaje(TIPO_MENSAJE_ERROR);
							return INPUT;
						}
					}
		            onSuccess();
		            tipoAccion="4";            
		            setIdTipoOperacion(20L);
				}else{
					setMensaje("La clave de Agente que Inserto no es Valida");
					setTipoMensaje(TIPO_MENSAJE_ERROR);
					return INPUT;
				}

		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return result;
	}
	public void validateGuardar() {
		String domainObjectPrefix = "descuentosAgente";
		 addErrors(descuentosAgente, NewItemChecks.class, this, domainObjectPrefix);
	}

	@Action(value="eliminar",results={
		@Result(name=SUCCESS,type="redirectAction",
				params={"actionName","verDetalle","namespace",NAMESPACE,"tipoAccion","${tipoAccion}",
				"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","descuentosAgente.idtcdescuentosagente","${descuentosAgente.idtcdescuentosagente}",
				"idTipoOperacion","${idTipoOperacion}","idRegistro","${descuentosAgente.idtcdescuentosagente}"}),
		@Result(name=INPUT,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/detalle.jsp")
	})
	@Override
	public String eliminar() {
		try {
			if(descuentosAgente.getIdtcdescuentosagente()!=null){
				setIdTipoOperacion(20L);
				descuentosAgente = descuentoAgenteService.findById(descuentosAgente.getIdtcdescuentosagente());
				descuentosAgente.setBajalogica(Short.valueOf("0"));
				LOG.info("setBajaLogica>>"+descuentosAgente.getBajalogica());		
				descuentoAgenteService.update(descuentosAgente);
				onSuccess();
			}
			else{
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				setMensaje("Es necesario completar la dirección para eliminar la gerencia");
				setTipoAccion("3");
			}
		}catch(Exception e){
			LOG.info("setBajaLogica>>"+descuentosAgente.getBajalogica());
			descuentosAgente.setBajalogica(Short.valueOf(Short.valueOf("1")));
			setMensajeError(e.getMessage());
			return INPUT;
		}
		return SUCCESS;
	}
	
	public String listar() {				
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado",results={
		@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/descuentoAgenteMovilGrid.jsp"),
		@Result(name=INPUT,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/descuentoAgenteMovilGrid.jsp")
	})
	@Override
	public String listarFiltrado() {
		LOG.info("claveNegocio>>"+claveNegocio);
		try{
			 if(descuentosAgente == null){
				 descuentosAgente = new DescuentosAgenteDTO();
			 }
			descuentosAgente.setClaveNegocio(claveNegocio);
			descuentosAgente.setClaveagente(claveAgente);
			if("consulta".equals(tipoAccion)){
				if(descuentosAgente == null){
					descuentosAgente = new DescuentosAgenteDTO();
               }
			}
			if(descuentosAgente != null){
				listaDescuentoAgente=descuentoAgenteService.findByFilters(descuentosAgente);
			}else{
				listaDescuentoAgente=descuentoAgenteService.findByFilters(descuentosAgente);
			}
			
		}catch(Exception e){
			onError(e);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/detalle.jsp")
		})
	@Override
	public String verDetalle() {
		if(descuentosAgente != null){
			if(descuentosAgente.getIdtcdescuentosagente()!=null){
				descuentosAgente = descuentoAgenteService.findById(descuentosAgente.getIdtcdescuentosagente());
			}
		}
		return SUCCESS;
	}
	
	@Action(value="verDetalleHistorico",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/descuentoAgenteMovil/detalle.jsp")
		})	
	public String verDetalleHistorico() {
		if(descuentosAgente != null){
			if(descuentosAgente.getIdtcdescuentosagente()!=null){
				try {
					prepareVerDetalleHistorico(this.getUltimaModificacion().getFechaHoraActualizacionString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return SUCCESS;
	}	
	
	@Action(value="findById",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion","excludeProperties","^gerencia\\.ejecutivos.*,^gerencia\\.personaResponsable.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^gerencia\\.id,^gerencia\\.descripcion"})
	})
	
	public String findById() throws Exception {
		verDetalle();
		return SUCCESS;
	}
	public TipoAccionDTO getTipoAccionCentro() {
		return tipoAccionCentro;
	}

	public void setTipoAccionCentro(TipoAccionDTO tipoAccionCentro) {
		this.tipoAccionCentro = tipoAccionCentro;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public DescuentosAgenteDTO getDescuentosAgente() {
		return descuentosAgente;
	}
	public void setDescuentosAgente(DescuentosAgenteDTO descuentosAgente) {
		this.descuentosAgente = descuentosAgente;
	}	
	public List<DescuentosAgenteDTO> getListaDescuentoAgente() {
		return listaDescuentoAgente;
	}
	public void setListaDescuentoAgente(
			List<DescuentosAgenteDTO> listaDescuentoAgente) {
		this.listaDescuentoAgente = listaDescuentoAgente;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	public File getFileUpload() {
		return fileUpload;
	}
	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}
	public List<String> getLista() {
		return lista;
	}
	public void setLista(List<String> lista) {
		this.lista = lista;
	}
	public int getRegistrosInsertados() {
		return registrosInsertados;
	}
	public void setRegistrosInsertados(int registrosInsertados) {
		this.registrosInsertados = registrosInsertados;
	}
	public int getRegistrosNoInsertados() {
		return registrosNoInsertados;
	}
	public void setRegistrosNoInsertados(int registrosNoInsertados) {
		this.registrosNoInsertados = registrosNoInsertados;
	}
	public String getLogErrores() {
		return logErrores;
	}
	public void setLogErrores(String logErrores) {
		this.logErrores = logErrores;
	}
	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}
	public String getTabActiva() {
		return tabActiva;
	}
	public String getClaveNegocio() {
		return claveNegocio;
	}
	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}
	public String getClaveAgente() {
		return claveAgente;
	}
	public void setClaveAgente(String claveAgente) {
		this.claveAgente = claveAgente;
	}
	 
	
}