package mx.com.afirme.midas2.action.catalogos.colorvehiculo;

import java.util.List;

import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.ColorVehiculo;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.ColorVehiculoService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ColorVehiculoAction extends CatalogoAction implements Preparable {

	private static final long serialVersionUID = -5243724700575384865L;
	
	public void validateGuardar(){
		addErrors(colorVehiculo, NewItemChecks.class, this, "colorVehiculo");
	}
	
	public void validateEditar(){
		addErrors(colorVehiculo, EditItemChecks.class, this, "colorVehiculo");
	}

	public void prepare() throws Exception {

		if (getId() != null) {
			colorVehiculo = entidadService.findById(ColorVehiculo.class, getId());
		}
		
	}
	
	public String execute() {
		return null;
	}
	

	@Override
	public String eliminar() {
		entidadService.remove(colorVehiculo);
		colorVehiculo = new ColorVehiculo();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String guardar() {
		entidadService.save(colorVehiculo);
		colorVehiculo = new ColorVehiculo();
		setMensajeExito();
		return SUCCESS;
	}

	@Override
	public String listar() {
		colorVehiculoList = entidadService.findAll(ColorVehiculo.class);
		
		return SUCCESS;
	}
	
	@Override
	public String listarFiltrado() {
		colorVehiculoList = colorVehiculoService.findByFilters(colorVehiculo);
		
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		return SUCCESS;
	}

	
	public ColorVehiculoAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}
	

	
	private Long id;

	private String tipoAccion; 

	private ColorVehiculo colorVehiculo;
	
	private List<ColorVehiculo> colorVehiculoList;
		
	private ColorVehiculoService colorVehiculoService;
	
	private EntidadService entidadService;
	
	private TipoAccionDTO catalogoTipoAccionDTO;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	
	public ColorVehiculo getColorVehiculo() {
		return colorVehiculo;
	}

	public void setColorVehiculo(ColorVehiculo colorVehiculo) {
		this.colorVehiculo = colorVehiculo;
	}

	public List<ColorVehiculo> getColorVehiculoList() {
		return colorVehiculoList;
	}

	public void setColorVehiculoList(List<ColorVehiculo> colorVehiculoList) {
		this.colorVehiculoList = colorVehiculoList;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	@Autowired
	@Qualifier("colorVehiculoEJB")
	public void setColorVehiculoService(
			ColorVehiculoService colorVehiculoService) {
		this.colorVehiculoService = colorVehiculoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
}
