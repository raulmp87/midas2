package mx.com.afirme.midas2.action.catalogos.modprima;

import java.util.List;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;
import mx.com.afirme.midas2.domain.catalogos.ModPrima;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.GrupoVariablesModificacionPrimaService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ModPrimaAction extends CatalogoAction implements Preparable {

	public ModPrimaAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private ModPrima modPrima;
	private List<ModPrima> modPrimaList;
	private GrupoVariablesModificacionPrimaService grupoVariablesModificacionPrimaService;
	private List<GrupoVariablesModificacionPrima> grupoList;
	private TipoAccionDTO catalogoTipoAccionDTO;
	private EntidadService entidadService;
	private String tipoAccion;
	private GrupoVariablesModificacionPrima grupoVariablesModificacionPrima;
	private List<GrupoVariablesModificacionPrima> grupoVariablesModificacionPrimaList;
	public Short obtenerCveDetalle;
	private String tipoGrid;

	public void validateGuardar() {
		addErrors(modPrima, NewItemChecks.class, this, "modPrima");
	}

	public void validateEditar() {
		addErrors(modPrima, EditItemChecks.class, this, "modPrima");
	}

	
	public String mostrarCatalogo(){
		grupoList = grupoVariablesModificacionPrimaService.findAll();
	 return SUCCESS;
	}
	

	
	@Override
	public void prepare() throws Exception {
		if (getId() != null) {
	    modPrima = entidadService.findById(ModPrima.class,getId());
		}
	}


	@Override
	public String guardar() {
		entidadService.save(modPrima);
		modPrima = new ModPrima();
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		entidadService.remove(modPrima);
		modPrima = new ModPrima();
		return SUCCESS;
	}

	@Override
	public String listar() {
		if (getId() != null) {
			grupoVariablesModificacionPrima = grupoVariablesModificacionPrimaService.findById(getId());
			obtenerCveDetalle               = grupoVariablesModificacionPrima.getClaveTipoDetalle();
		}
		if (obtenerCveDetalle==0)tipoGrid="DETALLE_0";
		if (obtenerCveDetalle==1)tipoGrid="DETALLE_1";
		
		modPrimaList = entidadService.findAll(ModPrima.class);
		return tipoGrid;
	}

	@Override
	public String listarFiltrado() {
		grupoVariablesModificacionPrimaList = grupoVariablesModificacionPrimaService.findByFilters(grupoVariablesModificacionPrima);
	
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		return SUCCESS;
	}
	
	public String verDetalleDos() {
		return SUCCESS;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ModPrima getModPrima() {
		return modPrima;
	}

	public void setModPrima(ModPrima modPrima) {
		this.modPrima = modPrima;
	}

	public List<ModPrima> getModPrimaList() {
		return modPrimaList;
	}

	public void setModPrimaList(List<ModPrima> modPrimaList) {
		this.modPrimaList = modPrimaList;
	}
	
	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public List<GrupoVariablesModificacionPrima> getGrupoList() {
		return grupoList;
	}

	public void setGrupoList(List<GrupoVariablesModificacionPrima> grupoList) {
		this.grupoList = grupoList;
	}

	public List<GrupoVariablesModificacionPrima> getGrupoVariablesModificacionPrimaList() {
		return grupoVariablesModificacionPrimaList;
	}

	public void setGrupoVariablesModificacionPrimaList(
			List<GrupoVariablesModificacionPrima> grupoVariablesModificacionPrimaList) {
		this.grupoVariablesModificacionPrimaList = grupoVariablesModificacionPrimaList;
	}

	public Short getObtenerCveDetalle() {
		return obtenerCveDetalle;
	}

	public void setObtenerCveDetalle(Short obtenerCveDetalle) {
		this.obtenerCveDetalle = obtenerCveDetalle;
	}

	public GrupoVariablesModificacionPrimaService getGrupoVariablesModificacionPrimaService() {
		return grupoVariablesModificacionPrimaService;
	}

	@Autowired
	@Qualifier("grupoVariablesModificacionPrimaEJB")
	public void setGrupoVariablesModificacionPrimaService(
			GrupoVariablesModificacionPrimaService grupoVariablesModificacionPrimaService) {
		this.grupoVariablesModificacionPrimaService = grupoVariablesModificacionPrimaService;
	}

	public GrupoVariablesModificacionPrima getGrupoVariablesModificacionPrima() {
		return grupoVariablesModificacionPrima;
	}

	public void setGrupoVariablesModificacionPrima(
			GrupoVariablesModificacionPrima grupoVariablesModificacionPrima) {
		this.grupoVariablesModificacionPrima = grupoVariablesModificacionPrima;
	}

}
