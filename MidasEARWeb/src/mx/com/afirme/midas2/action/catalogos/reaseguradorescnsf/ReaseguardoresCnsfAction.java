package mx.com.afirme.midas2.action.catalogos.reaseguradorescnsf;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.catalogos.CatalogoAction;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsf;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.reaseguradorcnsf.ReaseguradorCnsfService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/catalogos/reaseguradorescnsf")
public class ReaseguardoresCnsfAction extends CatalogoAction implements Preparable{
	
	
	private static final long serialVersionUID = 1L;
	private static final String NAMESPACE = "/catalogos/reaseguradorescnsf";
	private static final String USUARIO_PREFIX = " - M2";
	private ReaseguradorCnsfMov reaseguradorMovs;
	private List<ReaseguradorCnsfMov> reaseguradorMovsList;
	private EntidadService entidadService;
	private ReaseguradorCnsfService reaseguradorCnsfService;
	private BigDecimal id;
	private String tipoAccion; 
	private TipoAccionDTO catalogoTipoAccionDTO;
	private UsuarioService usuarioService;
	private InputStream inputStream;
	private String fileName;
	
	private Map<BigDecimal, String> agenciasList = new HashMap<BigDecimal, String>();
	private Map<BigDecimal, String> calificacionesList = new HashMap<BigDecimal, String>();
	
	List<CalificacionAgenciaDTO> listaCal = new ArrayList<CalificacionAgenciaDTO>();
	public ReaseguardoresCnsfAction() {
		catalogoTipoAccionDTO = new TipoAccionDTO();
	}

	@Action
	(value = "mostrarReaseguradores", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfCatalogo.jsp") })
	public String mostrarGeneracion() {			
		
		agenciasList = reaseguradorCnsfService.getMapAgencias();
		
		return SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		if (getId() != null) {
			reaseguradorMovs = entidadService.findById(ReaseguradorCnsfMov.class, getId());
		}
	}

	@Action
	(value = "guardar", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfCatalogo.jsp"), 
			@Result(name = INPUT, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfDetalle.jsp" )})
	public String guardar() {
		List<ReaseguradorCnsf> lista = new ArrayList<ReaseguradorCnsf>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaUltimoCorte = reaseguradorCnsfService.getFechaUltimoCorteProcesado();
		
		if(reaseguradorMovs.getFechacorte().compareTo(fechaUltimoCorte) != 0) {
			reaseguradorMovs = new ReaseguradorCnsfMov();
			setMensajeError("No es posible editar registros pertenecientes a cortes anteriores a la fecha " + sdf.format(fechaUltimoCorte));
			return SUCCESS;
		}
		
		Usuario usuario = usuarioService.getUsuarioActual();
		reaseguradorMovs.setUsuario(usuario.getNombreUsuario() + USUARIO_PREFIX);
		
		if(reaseguradorMovs.getIdreasegurador() != null ) {
			 lista = entidadService.findByProperty(ReaseguradorCnsf.class, "idCnsfReasegurador",reaseguradorMovs.getIdreasegurador());
		}
		
		if(!lista.isEmpty()){
			reaseguradorMovs.setIdreasegurador(lista.get(0).getIdCnsfReasegurador());
			ReaseguradorCnsf cnsf = entidadService.findById(ReaseguradorCnsf.class, reaseguradorMovs.getIdreasegurador());
			
			if(!cnsf.getClaveCnsf().equals(reaseguradorMovs.getClaveReasegurador())) {
				cnsf.setClaveCnsfAnterior(cnsf.getClaveCnsf());
				cnsf.setClaveCnsf(reaseguradorMovs.getClaveReasegurador());
			}
			
			cnsf.setNombreReasegurador(reaseguradorMovs.getNombreReasegurador());
			cnsf.setUsuario(usuario.getNombreUsuario() + USUARIO_PREFIX);
			
			entidadService.save(cnsf);
		} else {
			ReaseguradorCnsf reaseguradorCnsf =  new ReaseguradorCnsf();
			
			reaseguradorCnsf.setNombreReasegurador(this.reaseguradorMovs.getNombreReasegurador());
			reaseguradorCnsf.setClaveCnsf(this.reaseguradorMovs.getClaveReasegurador());
			reaseguradorCnsf.setClaveCnsfAnterior(this.reaseguradorMovs.getClaveReaseguradorAnt());
			reaseguradorCnsf.setUsuario(usuario.getNombreUsuario() + USUARIO_PREFIX);
			
			entidadService.save(reaseguradorCnsf);
			reaseguradorMovs.setIdreasegurador(reaseguradorCnsf.getIdCnsfReasegurador());
		}
		agenciasList = reaseguradorCnsfService.getMapAgencias();
		entidadService.save(reaseguradorMovs);
		reaseguradorMovs = new ReaseguradorCnsfMov();
		setMensajeExito();
		return SUCCESS;
	}

	@Action
	(value = "eliminar", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfCatalogo.jsp"), 
			@Result(name = INPUT, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfDetalle.jsp" )})
	public String eliminar() {
		Usuario usuario = usuarioService.getUsuarioActual();
		reaseguradorMovs.setUsuario(usuario.getNombreUsuario());		
		reaseguradorCnsfService.saveReaseguradorMovs(reaseguradorMovs);
		
		reaseguradorCnsfService.deleteReaseguradorMovs(reaseguradorMovs.getIdreaseguradormov());
		reaseguradorMovs = new ReaseguradorCnsfMov();
		setMensajeExito();
		return SUCCESS;
	}

	@Override 
	public String listar() {
		reaseguradorMovsList = entidadService.findAll(ReaseguradorCnsfMov.class);
		return SUCCESS;
	}
 
	@Action
	(value = "mostrarCatalogoCalificaciones", results = { 
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaCal\\[\\d+\\]\\.id,^listaCal\\[\\d+\\]\\.calificacion"}) })	
	public String mostrarCatalogoCalificaciones() {
		System.out.println("*****entrando al action mostrarCatalogoCalificaciones.action "+new Date());
		BigDecimal idBanco=(reaseguradorMovs.getIdagencia()!=null && reaseguradorMovs.getIdagencia()!=null)?reaseguradorMovs.getIdagencia():null;
		listaCal=reaseguradorCnsfService.getListCalificaciones(idBanco);
		System.out.println("*****Saliendo del action mostrarCatalogoCalificaciones.action "+new Date());
		return SUCCESS; 
	}

	@Action
	(value = "listarFiltrado", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfGrid.jsp") })
	public String listarFiltrado() {
		reaseguradorMovsList = reaseguradorCnsfService.findByFilters(reaseguradorMovs);
		return SUCCESS; 
	}
	
	
	@Action(value = "exportarFiltrado", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"text/csv", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" })
	})					
	public String exportarFiltrado() {
		reaseguradorMovsList = reaseguradorCnsfService.findByFilters(reaseguradorMovs);
		 
		try {
			inputStream = new ByteArrayInputStream(reaseguradorCnsfService.toArrayByte(reaseguradorMovsList));
		} catch (Exception e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		}
		fileName = "ReaseguradoresValidosCNSF.txt";
		return SUCCESS; 
	}

	@Action
	(value = "verDetalle", results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/reaseguradorcnsf/reaseguradorCnsfDetalle.jsp") })
	public String verDetalle() {
		
		if(reaseguradorMovs != null){
			ReaseguradorCnsf lista = entidadService.findById(ReaseguradorCnsf.class, reaseguradorMovs.getIdreasegurador());
			
			reaseguradorMovs.setNombreReasegurador(lista.getNombreReasegurador());
			reaseguradorMovs.setClaveReasegurador(lista.getClaveCnsf());
			reaseguradorMovs.setClaveReaseguradorAnt(lista.getClaveCnsfAnterior());
			reaseguradorMovs.setIdTcReasegurador(lista.getIdTcReasegurador());
			calificacionesList = reaseguradorCnsfService.getMapCalificaciones(reaseguradorMovs.getIdagencia());
		} 
		agenciasList = reaseguradorCnsfService.getMapAgencias(); 
		return SUCCESS;
	}

	public ReaseguradorCnsfMov getReaseguradorMovs() {
		return reaseguradorMovs;
	}

	public void setReaseguradorMovs(ReaseguradorCnsfMov reaseguradorMovs) {
		this.reaseguradorMovs = reaseguradorMovs;
	}

	public List<ReaseguradorCnsfMov> getReaseguradorMovsList() {
		return reaseguradorMovsList;
	}

	public void setReaseguradorMovsList(
			List<ReaseguradorCnsfMov> reaseguradorMovsList) {
		this.reaseguradorMovsList = reaseguradorMovsList;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("reaseguradorCnsfEJB")
	public void setReaseguradorCnsfService(
			ReaseguradorCnsfService reaseguradorCnsfService) {
		this.reaseguradorCnsfService = reaseguradorCnsfService;
	}
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public TipoAccionDTO getCatalogoTipoAccionDTO() {
		return catalogoTipoAccionDTO;
	}

	public void setCatalogoTipoAccionDTO(TipoAccionDTO catalogoTipoAccionDTO) {
		this.catalogoTipoAccionDTO = catalogoTipoAccionDTO;
	}

	public Map<BigDecimal, String> getAgenciasList() {
		return agenciasList;
	}

	public void setAgenciasList(Map<BigDecimal, String> agenciasList) {
		this.agenciasList = agenciasList;
	}

	public Map<BigDecimal, String> getCalificacionesList() {
		return calificacionesList;
	}

	public void setCalificacionesList(Map<BigDecimal, String> calificacionesList) {
		this.calificacionesList = calificacionesList;
	}

	public List<CalificacionAgenciaDTO> getListaCal() {
		return listaCal;
	}

	public void setListaCal(List<CalificacionAgenciaDTO> listaCal) {
		this.listaCal = listaCal;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
