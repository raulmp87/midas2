package mx.com.afirme.midas2.action.catalogos.tarifa.tarifaMovil;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.movil.RespuestaCargaMasiva;
import mx.com.afirme.midas2.domain.movil.cotizador.CatalogoEstatusActualizacion;
import mx.com.afirme.midas2.domain.movil.cotizador.Estado;
import mx.com.afirme.midas2.domain.movil.cotizador.TarifasDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.excels.CargarExcelTarifasCotizacionMovilOptimizacion;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.domain.movil.cotizador.Resultado;

import com.opensymphony.xwork2.Preparable;

@Namespace("/tarifa/tarifaMovil")
@Component
@Scope("prototype")
public class TarifaMovilAction extends CatalogoHistoricoAction implements Preparable{

	
	/** Log de TarifaMovilAction */
	private static final Logger LOG = Logger.getLogger(TarifaMovilAction.class);
	/**serialVersionUID	 **/
	private static final long serialVersionUID = 1L;
	private final String NAMESPACE="/tarifa/tarifaMovil";
	private CatalogoEstatusActualizacion param;
	File fileUpload;
	private CargarExcelTarifasCotizacionMovilOptimizacion cargarExcelTarifasCotizacionMovilOptimizacion;
	@Autowired
	CotizacionMovilService tarifasService;
	private List <String>lista=new ArrayList<String>();
    private int registrosInsertados;
    private int registrosNoInsertados;
    private int registrosActualizados;
    private String logErrores="";
    List<TarifasDTO> tarifasExcelList;
    TarifasDTO tarifasDTO;
    private ImpresionesService impresionesService;
	private InputStream inputStream;
	private String contentType;
	private String fileName;
	private Boolean isDirecto;
	List<Estado>  estados;
	private String tabActiva;
	private String subTabActiva;
	private EstadoFacadeRemote estadoFacade;
	private List<EstadoDTO> estadosList=new ArrayList<EstadoDTO>();
	
	private List<CatalogoEstatusActualizacion>  estadoList;
	private List<CatalogoEstatusActualizacion>  marcaList;
	
	@Action(value="cargarTarifas", results={
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
			@Result(name = INPUT, location = "/jsp/error.jsp")
		})
	public String cargarTarifas(){
		try{
			String mensaje="";
			long timeStart = System.currentTimeMillis();
			cargarExcelTarifasCotizacionMovilOptimizacion = new CargarExcelTarifasCotizacionMovilOptimizacion();
			cargarExcelTarifasCotizacionMovilOptimizacion.setTarifasService(tarifasService);
			List<TarifasDTO> tarifasList= new ArrayList<TarifasDTO>();
			RespuestaCargaMasiva respuesta= new  RespuestaCargaMasiva();
			tarifasList=cargarExcelTarifasCotizacionMovilOptimizacion.leerXLS(fileUpload);
			Integer limiteCarga=Integer.parseInt(tarifasService.getCantidadMaximaCargaMasivaTarifas());
			if(tarifasList.size()>limiteCarga){
				mensaje="[El archivo no puede contener mas de "+limiteCarga+" registros] ";
			}else{
				tarifasDTO = new TarifasDTO();
				respuesta=tarifasService.saveTarifasExcelList(tarifasList,tarifasDTO);
				tarifasService.actualizarVistaMaterializada();
				LOG.info("registrosActualizados:"+respuesta.getRegistrosActualizados());
				registrosActualizados=respuesta.getRegistrosActualizados();
				registrosInsertados = respuesta.getRegistrosInsertados();
				registrosNoInsertados = respuesta.getRegistrosNoInsertados();
			}				
			List<Resultado> resxls= new ArrayList<Resultado>();
			resxls.add(procesarResultado("Registros Insertados:",String.valueOf(registrosInsertados)));	
			resxls.add(procesarResultado("Registros Actualizados:",String.valueOf(registrosActualizados)));
			resxls.add(procesarResultado("Registros No Insertados:",String.valueOf(registrosNoInsertados)));
			long timeEnd = System.currentTimeMillis();
			long timpoCargaMls = timeEnd - timeStart;
		    mensaje = mensaje+ "Tiempo carga =" + (timpoCargaMls / 1000)/60 + " Minutos";
		    resxls.add(procesarResultado("Mensaje:",mensaje));
		    if(respuesta.getRegistrosAfectados()!=null){
		    	if(respuesta.getRegistrosAfectados().size()>=1){
		    		logErrores = obtenerLogErrores(respuesta);
					for(String obj:respuesta.getRegistrosAfectados()){
						Resultado rest= new Resultado();
						rest.setResultado(obj);
						rest.setId("Error:");
						resxls.add(rest);
					}
				}
		    }  		    
		    setTipoMensaje(TIPO_MENSAJE_EXITO);
			setMensaje("Acción realizada correctamente " + mensaje);
			///////////
			contentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			//tarifasExcelList = tarifasService.findTarifasByFilters(tarifasDTO);
			TransporteImpresionDTO respuestaxls =impresionesService.getExcel(resxls, 
					"midas.movil.cliente.cotizador.tarifas.plantilla.archivo.respuesta.nombre",ConstantesReporte.TIPO_XLSX);
			inputStream = new ByteArrayInputStream(respuestaxls.getByteArray());
			fileName = " ResultadosCargaMasiva-" + new Date().toString() + ".xlsx";
			//////////
		} catch (ApplicationException e) {
			logErrores = getErrors(e);
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			logErrores = "" + e.getMessage();
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	public String obtenerLogErrores(RespuestaCargaMasiva respuesta) {
		StringBuilder log = new StringBuilder(logErrores);
		for(String obj:respuesta.getRegistrosAfectados()){
			log.append("<br>").append(obj);
		}
		return log.toString();
	}
	private String getErrors(ApplicationException e){
		StringBuilder sb = new StringBuilder();
		for(String object:e.getErrors()){
			sb.append("<br>"+object);
		}
		return sb.toString();
	}
	@Action(value = "respuestaCargaMasivaTarifaMovil", 
			results = { 
			@Result(name = SUCCESS, location = "/jsp/catalogos/tarifa/tarifaMovil/respuestaCargaMasivaTariasMovil.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
			
	public String respuestaCargaMasivaAgente(){
			return SUCCESS;
	}
	
	@Action(value = "generarExcelTarifaMovil", results = {
			@Result(name = SUCCESS, type = "stream", params = { "contentType",
					"${contentType}", "namespace", NAMESPACE, "inputName",
					"inputStream", "contentDisposition",
					"attachment;filename=\"${fileName}\"" }),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String generarDetalleExcel() {
		asignarTabsDefault("descargraTarifasMovil");
		contentType = "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		tarifasExcelList = tarifasService.findTarifasByFilters(tarifasDTO);
		TransporteImpresionDTO respuesta =impresionesService.getExcel(tarifasExcelList, 
				"midas.movil.cliente.cotizador.tarifas.plantilla.archivo.nombre",ConstantesReporte.TIPO_XLSX);
		estadosList=estadoFacade.findByProperty("countryId","PAMEXI");
		inputStream = new ByteArrayInputStream(respuesta.getByteArray());
		fileName = " TarifasMovil-" + new Date().toString() + ".xlsx";
		return SUCCESS;
	}
	
	
	public String getSucess(){
		asignarTabsDefault("cargaTarifasXLS");
		estadosList=estadoFacade.findByProperty("countryId","PAMEXI");
		estados = tarifasService.getALLEstados();
		return SUCCESS;
	}
	@Action(value="mostrarEstados",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/tarifaMovil/configCatalogoTarifaEstadosMovilGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/tarifa/tarifaMovil/configCatalogoTarifaEstadosMovilGrid.jsp")
		})	
		public String mostrarEstados(){
			estadoList = tarifasService.obtenerCatalogoEstados();
			return SUCCESS;
		}
	@Action(value="mostrarMarcas",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/tarifa/tarifaMovil/configCatalogoTarifaMarcasMovilGrid.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/tarifa/tarifaMovil/configCatalogoTarifaMarcasMovilGrid.jsp")
		})	
		public String mostrarMarcas(){
			marcaList = tarifasService.obtenerCatalogoMarcas();
			return SUCCESS;
		}
	@Action(value="desactivarEstado", results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarCatalogoTarifaMovil","namespace",NAMESPACE
					}),
			@Result(name = INPUT, location = "/jsp/error.jsp")
		})
	public String desactivarEstado(){
		asignarTabsDefault("configTarifasMovilEstados");
		param.setBajaLogica(new Short("0"));
		tarifasService.desactivarTarifaEstados(param);
		tarifasService.actualizarVistaMaterializada();
		return SUCCESS;
	}
	
	@Action(value="activarEstado", results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarCatalogoTarifaMovil","namespace",NAMESPACE
					}),
			@Result(name = INPUT, location = "/jsp/error.jsp")
		})
	public String activarEstado(){
		asignarTabsDefault("configTarifasMovilEstados");
		param.setBajaLogica(new Short("1"));
		tarifasService.desactivarTarifaEstados(param);
		tarifasService.actualizarVistaMaterializada();
		return SUCCESS;
	}
	@Action(value="desactivarMarca", results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarCatalogoTarifaMovil","namespace",NAMESPACE
					}),
			@Result(name = INPUT, location = "/jsp/error.jsp")
		})
	public String desactivarMarca(){
		asignarTabsDefault("configTarifasMovilMarcas");
		param.setBajaLogica(new Short("0"));
		tarifasService.desactivarTarifasMarcas(param);
		tarifasService.actualizarVistaMaterializada();
		return SUCCESS;
	}
	
	@Action(value="activarMarca", results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","mostrarCatalogoTarifaMovil","namespace",NAMESPACE
					}),
			@Result(name = INPUT, location = "/jsp/error.jsp")
		})
	public String activarMarca(){
		asignarTabsDefault("configTarifasMovilMarcas");
		param.setBajaLogica(new Short("1"));
		tarifasService.desactivarTarifasMarcas(param);
		tarifasService.actualizarVistaMaterializada();
		return SUCCESS;
	}
	
	public String getKey(){
		return null;
	}
	public void setKey(String key){
		
	}
	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}

	public List<String> getLista() {
		return lista;
	}

	public void setLista(List<String> lista) {
		this.lista = lista;
	}

	public int getRegistrosInsertados() {
		return registrosInsertados;
	}

	public void setRegistrosInsertados(int registrosInsertados) {
		this.registrosInsertados = registrosInsertados;
	}

	public int getRegistrosNoInsertados() {
		return registrosNoInsertados;
	}

	public void setRegistrosNoInsertados(int registrosNoInsertados) {
		this.registrosNoInsertados = registrosNoInsertados;
	}

	public String getLogErrores() {
		return logErrores;
	}

	public void setLogErrores(String logErrores) {
		this.logErrores = logErrores;
	}
	public List<TarifasDTO> getTarifasExcelList() {
		return tarifasExcelList;
	}
	public void setTarifasExcelList(List<TarifasDTO> tarifasExcelList) {
		this.tarifasExcelList = tarifasExcelList;
	}
    @Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Boolean getIsDirecto() {
		return isDirecto;
	}
	public void setIsDirecto(Boolean isDirecto) {
		this.isDirecto = isDirecto;
	}
	public TarifasDTO getTarifasDTO() {
		return tarifasDTO;
	}
	public void setTarifasDTO(TarifasDTO tarifasDTO) {
		this.tarifasDTO = tarifasDTO;
	}
	public List<Estado> getEstados() {
		return estados;
	}
	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	@Override
	public void prepare() throws Exception {
	}

	@Override
	public String guardar() {
		return null;
	}

	@Override
	public String eliminar() {
		return null;
	}

	@Override
	public String listar() {
		return null;
	}

	@Override
	public String listarFiltrado() {
		return null;
	}

	@Override
	public String verDetalle() {
		return null;
	}
	@Autowired
	@Qualifier("estadoFacadeRemoteEJB")
	public void setEstadoFacade(EstadoFacadeRemote estadoFacade) {
		this.estadoFacade = estadoFacade;
	}

	public List<EstadoDTO> getEstadosList() {
		return estadosList;
	}

	public void setEstadosList(List<EstadoDTO> estadosList) {
		this.estadosList = estadosList;
	}
	public void setRegistrosActualizados(int registrosActualizados) {
		this.registrosActualizados = registrosActualizados;
	}
	public int getRegistrosActualizados() {
		return registrosActualizados;
	}	
	public List<CatalogoEstatusActualizacion> getEstadoList() {
		return estadoList;
	}
	public void setEstadoList(List<CatalogoEstatusActualizacion> estadoList) {
		this.estadoList = estadoList;
	}
	public List<CatalogoEstatusActualizacion> getMarcaList() {
		return marcaList;
	}
	public void setMarcaList(List<CatalogoEstatusActualizacion> marcaList) {
		this.marcaList = marcaList;
	}
	public void setParam(CatalogoEstatusActualizacion param) {
		this.param = param;
	}
	public CatalogoEstatusActualizacion getParam() {
		return param;
	}
	public void setSubTabActiva(String subTabActiva) {
		this.subTabActiva = subTabActiva;
	}
	public String getSubTabActiva() {
		return subTabActiva;
	}
	private void asignarTabsDefault(String subTab){
		if(tabActiva==null){
			this.setTabActiva("info_general");
		}
		
		if(subTabActiva==null){
			this.setSubTabActiva(subTab);
		}
	}
	private Resultado procesarResultado (String id,String resultado){
		Resultado res= new Resultado();
		res.setResultado(resultado);
		res.setId(id);
		return res;
	}
}
