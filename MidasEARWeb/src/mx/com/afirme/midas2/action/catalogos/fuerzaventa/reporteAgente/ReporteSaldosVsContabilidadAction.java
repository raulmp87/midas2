package mx.com.afirme.midas2.action.catalogos.fuerzaventa.reporteAgente;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.RepMizarSelectMizar;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;
import mx.com.afirme.midas2.service.fuerzaventa.RepSaldoVSContService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/fuerzaventa/SaldosVsContabilidad")
@Component
@Scope("prototype")
public class ReporteSaldosVsContabilidadAction extends ReporteAgenteBaseAction implements Preparable, ReportMethods{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6660837731647880297L;
	public static final String FILTRO_REPORTE ="/jsp/reportesAgentes/saldosVsContabilidad.jsp";
	public static final String ERROR = "/jsp/reportesAgentes/reporteError.jsp";
	private InputStream reporte;
	private Date fechaCorte;
	public String labelFechaFinCorte="Fecha Fin de Corte";
	private RepSaldoVSContService repSaldoVSContService;
		
	@Autowired
	@Qualifier("repSaldoVSContServiceEJB")
	public void setRepSaldoVSContService(RepSaldoVSContService repSaldoVSContService) {
		this.repSaldoVSContService = repSaldoVSContService;
	}

	public String getLabelFechaFinCorte() {
		return labelFechaFinCorte;
	}

	public void setLabelFechaFinCorte(String labelFechaFinCorte) {
		this.labelFechaFinCorte = labelFechaFinCorte;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public InputStream getReporte() {
		return reporte;
	}

	public void setReporte(InputStream reporte) {
		this.reporte = reporte;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Action(value="mostrarFiltros", results={
		@Result(name = SUCCESS, location=FILTRO_REPORTE)
	})
	@Override
	public String mostrarFiltros() {
		return SUCCESS;
	}

	@Override
	public String exportarToPDF() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="exportarToExcel", results={
			@Result(name=SUCCESS,type="stream", 
					params={
					"contentType","${contentType}",
					"contentDisposition","attachment;filename=\"${fileName}\"",
					"inputName","reporte"
				}),
	@Result(name="EMPTY", location=ERROR),
	@Result(name=INPUT, location=ERROR)
	})
	@Override
	public String exportarToExcel() {
		try{
			List<SaldosVsContabilidadView> listaSaldosVsCont = new ArrayList<SaldosVsContabilidadView>();
			try {
				SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
				String formatoFech = formatoFecha.format(fechaCorte);
				//*************se eliminan los registros de la tabla de paso *************
				repSaldoVSContService.deleteTPSaldosVsContabilidad();
				//************* se hace la consulta a mizar *************
				SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
				String fechaString = formato.format(fechaCorte);
				SimpleDateFormat formatoAnioMes= new SimpleDateFormat("yyyy/MM");
				String anioMes = formatoAnioMes.format(fechaCorte);
				List<RepMizarSelectMizar> lista = new ArrayList<RepMizarSelectMizar>();
				lista = repSaldoVSContService.consultaMizar(anioMes, fechaString);
				//************* se inserta los datos obtenidos de la consulta en la tabla de paso  *************
				repSaldoVSContService.insertTPSaldosVsContabilidad(lista);
				//************* se ejecuta el sp stp_repSaldosVsContabilidad  *************
				listaSaldosVsCont = repSaldoVSContService.executeSpSaldosVSContabilidad(formatoFech);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService()
			.imprimirReporteSaldosVsContabilidad(listaSaldosVsCont, ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
			if(transporte !=null){
				setReporte(new ByteArrayInputStream(transporte.getByteArray()));
			}else{
				setMensaje("No se obtuvieron agentes con los criterios de busqueda seleccionados.");
				return "EMPTY";
			}
			if(ReporteAgenteBaseAction.TIPOSALIDAARCHIVO.equals(TipoSalidaReportes.TO_XLSX.getValue())){
//				setContentType("application/xlsx");
				setContentType("application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			}else{
				setContentType("application/octet-stream; charset=UTF-8");
			}
				setFileName("ReporteSaldosVsContabilidad."+ReporteAgenteBaseAction.TIPOSALIDAARCHIVO);
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

}
