package mx.com.afirme.midas2.action.agtSaldoMovto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.agtSaldoMovto.AgtSaldo;
import mx.com.afirme.midas2.domain.agtSaldoMovto.MovimientosDelSaldo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.agtSaldoMovto.AgtSaldoView;
import mx.com.afirme.midas2.dto.agtSaldoMovto.MovimientosDelSaldoView;
import mx.com.afirme.midas2.service.agtSaldoMovto.AgtSaldoService;
import mx.com.afirme.midas2.service.agtSaldoMovto.MovimientosDelSaldoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("prototype")
@Namespace("/agtSaldoMovto")
public class AgtSaldoMovimientoAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String MOVIMIENTOSDELSALDOCATALOGO = "/jsp/agtSaldoMovto/movimientosDelSaldo/movimientosDelSaldoCatalogo.jsp";
	private final String MOVIMIENTOSDELSALDOGRID = "/jsp/agtSaldoMovto/movimientosDelSaldo/movimientosDelSaldoGrid.jsp";
	private final String AGENTESALDOCATALOGO = "/jsp/agtSaldoMovto/agtSaldoCatalogo.jsp";
	private final String AGENTESALDOGRID="/jsp/agtSaldoMovto/agtSaldoGrid.jsp";
	private final String DETALLEAGTSALDOMOV = "/jsp/agtSaldoMovto/detalleAgtSaldoMov.jsp";
	private List<ValorCatalogoAgentes> catalogoMoneda = new ArrayList<ValorCatalogoAgentes>();
	private AgtSaldo agtSaldo= new AgtSaldo();  
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private EntidadService entidadService;
	private List<AgtSaldoView> listAgtSaldoView = new ArrayList<AgtSaldoView>();
	private AgtSaldoService agtSaldoService; 
	private List<Long>anios = new ArrayList<Long>();
	private MovimientosDelSaldo filtroMovimientosDelSaldo;	
	private AgtSaldoView detalleAgtSaldoView;
	private List<MovimientosDelSaldoView> listaMovimientosDelSaldo =  new ArrayList<MovimientosDelSaldoView>();
	private MovimientosDelSaldo detalleMovimiento;	

	private MovimientosDelSaldoService movimientosDelSaldoService; 
	
	private RamoFacadeRemote ramoFacadeRemote;
	private SubRamoFacadeRemote subRamoFacadeRemote;
	private final String GRUPO_CATALOGO_ORIGEN_REMESA  = "Claves Origen Remesa";
	private final String GRUPO_CONCEPTO_MOVIMIENTOS_AGENTES  = "Concepto Movimiento Agente";
	private final String GRUPO_CLAVE_TIPO_MOVIMIENTO_CONCEPTO_COBRANZAS  = "Clave de Tipo de Movimiento del Concepto de Cobranzas";

	public AgtSaldoView getDetalleAgtSaldoView() {
		return detalleAgtSaldoView;
	}

	public void setDetalleAgtSaldoView(AgtSaldoView detalleAgtSaldoView) {
		this.detalleAgtSaldoView = detalleAgtSaldoView;
	}

	public List<Long> getAnios() {
		return anios;
	}

	public void setAnios(List<Long> anios) {
		this.anios = anios;
	}

	@Autowired
	@Qualifier("AgtSaldoServiceEJB")
	public void setAgtSaldoService(AgtSaldoService agtSaldoService) {
		this.agtSaldoService = agtSaldoService;
	}
	

	public List<AgtSaldoView> getListAgtSaldoView() {
		return listAgtSaldoView;
	}


	public void setListAgtSaldoView(List<AgtSaldoView> listAgtSaldoView) {
		this.listAgtSaldoView = listAgtSaldoView;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public List<ValorCatalogoAgentes> getCatalogoMoneda() {
		return catalogoMoneda;
	}

	public void setCatalogoMoneda(List<ValorCatalogoAgentes> catalogoMoneda) {
		this.catalogoMoneda = catalogoMoneda;
	}

	public AgtSaldo getAgtSaldo() {
		return agtSaldo;
	}

	public void setAgtSaldo(AgtSaldo agtSaldo) {
		this.agtSaldo = agtSaldo;
	}

	//****************************************
	
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		catalogoMoneda = cargarCatalogo("Tipo Moneda");
		
	}
	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=AGENTESALDOCATALOGO)
	})
	@Override
	public String listar() {
		return SUCCESS;
	}
	
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=AGENTESALDOGRID),		
			@Result(name=INPUT,location=AGENTESALDOGRID)
	})
	
	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		try {
			
			if(agtSaldo!=null && agtSaldo.getIdAgente()!=null){				
				//Actualizamos el último periodo del año con estatus abierto
				
				if(agtSaldo!=null && agtSaldo.getIdAgente()!=null && agtSaldo.getIdAgente().getIdAgente()!=null){
					
					Long anio;
					if(agtSaldo.getAnio()!=null){
						anio = agtSaldo.getAnio(); 
					}else{
						int anno = Calendar.getInstance().get(Calendar.YEAR);
						anio = Long.parseLong(String.valueOf(anno));
					}					
					agtSaldoService.actualizarSaldosMesAbierto(agtSaldo.getIdAgente().getIdAgente(), anio);
					
				}				
				
				listAgtSaldoView = agtSaldoService.findByFilterview(agtSaldo);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.persona\\.nombreCompleto,^agente\\.tipoSituacion\\.valor,^agente\\.descripMotivoEstatusAgente,^agente\\.fechaAltaString,^agente\\.id,^agente\\.idAgente"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.persona\\.nombreCompleto,^agente\\.tipoSituacion\\.valor,^agente\\.descripMotivoEstatusAgente,^agente\\.fechaAltaString,^agente\\.id,^agente\\.idAgente"})
		})
		
		public String obtenerAgente(){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//Debe obtener el agente sin importar la situacion que tenga.
			agente.setTipoSituacion(null);			
			
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
				ValorCatalogoAgentes tipoAg = entidadService.findById(ValorCatalogoAgentes.class, agente.getIdTipoAgente());
				if(tipoAg!=null){
					agente.setTipoAgente(tipoAg.getValor());
				}
				ValorCatalogoAgentes motivoStatus = new ValorCatalogoAgentes();
				motivoStatus.setId(agente.getIdMotivoEstatusAgente());
				motivoStatus = catalogoService.loadById(motivoStatus);
				agente.setDescripMotivoEstatusAgente(motivoStatus.getValor());
				String fechaString=(agente.getFechaAlta()!=null)?sdf.format(agente.getFechaAlta()):"";
				agente.setFechaAltaString(fechaString);
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	
	@Action(value="verDetalleSaldoMovimientos",results={
			@Result(name=SUCCESS,location=MOVIMIENTOSDELSALDOCATALOGO)
	})
	public String verDetalleSaldoMovimientos(){
		AgtSaldo objAgtSaldo= new AgtSaldo();
		try {
			objAgtSaldo.setAnioMes((long) Integer.valueOf(filtroMovimientosDelSaldo.getAnioMes()));
			objAgtSaldo.setIdAgente(filtroMovimientosDelSaldo.getAgente());
			detalleAgtSaldoView = agtSaldoService.obtenerMovimientoSaldoPorAnioMesYAgente(objAgtSaldo);
		} catch (MidasException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoDetalle",results={
			@Result(name=SUCCESS,location=MOVIMIENTOSDELSALDOGRID),
			@Result(name=INPUT,location=MOVIMIENTOSDELSALDOGRID)
	})
	public String listarFiltradoDetalle(){
		try {
			
			listaMovimientosDelSaldo = movimientosDelSaldoService.findByFiltersView(filtroMovimientosDelSaldo);
		} catch (MidasException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(value="verDetalleMovimiento",results={
			@Result(name=SUCCESS,location=DETALLEAGTSALDOMOV)
	})
	public String verDetalleMovimiento()
	{
		AgtSaldo objAgtSaldo= new AgtSaldo();
		try {
			objAgtSaldo.setAnioMes((long) Integer.valueOf(filtroMovimientosDelSaldo.getAnioMes()));
			objAgtSaldo.setIdAgente(filtroMovimientosDelSaldo.getAgente());
			filtroMovimientosDelSaldo.setAgente(null);
			detalleAgtSaldoView = agtSaldoService.obtenerMovimientoSaldoPorAnioMesYAgente(objAgtSaldo);
			detalleMovimiento = movimientosDelSaldoService.findByFilters(filtroMovimientosDelSaldo).get(0);
			detalleMovimiento.getAgenteOrigen();
			
			if(detalleMovimiento.getIdRamoContable() != null && !detalleMovimiento.getIdRamoContable().isEmpty())
			{
				RamoDTO ramo = ramoFacadeRemote.findById(new BigDecimal(detalleMovimiento.getIdRamoContable()));
				detalleMovimiento.setDescripcionRamo(ramo.getDescripcion());
			}
			
			if(detalleMovimiento.getIdRamoContable() != null && !detalleMovimiento.getIdRamoContable().isEmpty()
					&& detalleMovimiento.getIdSubramoContable() != null && !detalleMovimiento.getIdSubramoContable().isEmpty())
			{
				String codigoSubramo = (detalleMovimiento.getIdRamoContable().concat(detalleMovimiento.getIdSubramoContable())).trim();
				
				List<SubRamoDTO> listaSubramos = subRamoFacadeRemote.findByProperty("codigoSubRamo",new BigDecimal(codigoSubramo));
				if(!listaSubramos.isEmpty())
				{
					detalleMovimiento.setDescripcionSubramo(listaSubramos.get(0).getDescripcionSubRamo());					
				}				
			}	
			
			ValorCatalogoAgentes filtro;
			GrupoCatalogoAgente grupo;
			List<ValorCatalogoAgentes> listaResultados;
			
			if(detalleMovimiento.getClaveOrigenRemesa() != null && !detalleMovimiento.getClaveOrigenRemesa().isEmpty())
			{
				filtro = new ValorCatalogoAgentes();
				grupo = new GrupoCatalogoAgente();
				grupo.setDescripcion(GRUPO_CATALOGO_ORIGEN_REMESA);
				filtro.setGrupoCatalogoAgente(grupo);
				filtro.setClave(detalleMovimiento.getClaveOrigenRemesa());			
				
				listaResultados = valorCatalogoAgentesService.findByFilters(filtro);
				if(!listaResultados.isEmpty())
				{
					detalleMovimiento.setDescripcionOrigenRemesa(listaResultados.get(0).getValor());					
				}
			}
			
			if(detalleMovimiento.getIdConcepto()!= null && detalleMovimiento.getCvetCptoAco() != null 
					&& !detalleMovimiento.getCvetCptoAco().isEmpty())
			{
				filtro = new ValorCatalogoAgentes();
				grupo = new GrupoCatalogoAgente();
				grupo.setDescripcion(GRUPO_CONCEPTO_MOVIMIENTOS_AGENTES);
				filtro.setGrupoCatalogoAgente(grupo);
				filtro.setClave(detalleMovimiento.getCvetCptoAco());
				filtro.setIdRegistro(detalleMovimiento.getIdConcepto());
				
				listaResultados = valorCatalogoAgentesService.findByFilters(filtro);
				if(!listaResultados.isEmpty())
				{
					detalleMovimiento.setDescripcionConcepto(listaResultados.get(0).getValor());					
				}
				
				filtro = new ValorCatalogoAgentes();
				grupo = new GrupoCatalogoAgente();
				grupo.setDescripcion(GRUPO_CLAVE_TIPO_MOVIMIENTO_CONCEPTO_COBRANZAS);
				filtro.setGrupoCatalogoAgente(grupo);
				filtro.setClave(detalleMovimiento.getCvetCptoAco());
				
				listaResultados = valorCatalogoAgentesService.findByFilters(filtro);
				if(!listaResultados.isEmpty())
				{
					detalleMovimiento.setDescripcionCatConcepto(listaResultados.get(0).getValor());					
				}				
			}			
			
		} catch (Exception e) {
			this.setMensajeError("Ha ocurrido un error, favor de intentar mas tarde.");		
			e.printStackTrace();
		}
		
		return SUCCESS;
	}

	public MovimientosDelSaldo getFiltroMovimientosDelSaldo() {
		return filtroMovimientosDelSaldo;
	}

	public void setFiltroMovimientosDelSaldo(
			MovimientosDelSaldo filtroMovimientosDelSaldo) {
		this.filtroMovimientosDelSaldo = filtroMovimientosDelSaldo;
	}

	public List<MovimientosDelSaldoView> getListaMovimientosDelSaldo() {
		return listaMovimientosDelSaldo;
	}

	public void setListaMovimientosDelSaldo(List<MovimientosDelSaldoView> listaMovimientosDelSaldo) {
		this.listaMovimientosDelSaldo = listaMovimientosDelSaldo;
	}

	@Autowired
	@Qualifier("movimientosDelSaldoEJB")
	public void setMovimientosDelSaldoService(MovimientosDelSaldoService movimientosDelSaldoService) {
		this.movimientosDelSaldoService = movimientosDelSaldoService;
	}
	
	public MovimientosDelSaldo getDetalleMovimiento() {
		return detalleMovimiento;
	}

	public void setDetalleMovimiento(MovimientosDelSaldo detalleMovimiento) {
		this.detalleMovimiento = detalleMovimiento;
	}	
	
	public RamoFacadeRemote getRamoFacadeRemote() {
		return ramoFacadeRemote;
	}

	@Autowired
	@Qualifier("ramoFacadeRemoteEJB")
	public void setRamoFacadeRemote(RamoFacadeRemote ramoFacadeRemote) {
		this.ramoFacadeRemote = ramoFacadeRemote;
	}

	public SubRamoFacadeRemote getSubRamoFacadeRemote() {
		return subRamoFacadeRemote;
	}
	@Autowired
	@Qualifier("subRamoFacadeRemoteEJB")
	public void setSubRamoFacadeRemote(SubRamoFacadeRemote subRamoFacadeRemote) {
		this.subRamoFacadeRemote = subRamoFacadeRemote;
	}
}
