package mx.com.afirme.midas2.action.portal.cotizador;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DatosDesglosePagosMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.impresionesM2.GenerarReportes;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;
import mx.com.afirme.midas2.service.portal.cotizador.CotizadorPortalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;

@Component
@Scope("prototype")
public class CotizadorPortalAction extends BaseAction implements ModelDriven<Object>{

	private static final long serialVersionUID = -5710284774803303121L;
	
	private BigDecimal idToCotizacionMidas;
	private String descripcionVehiculo;
	private String estado;
	private String nombreProspecto;
	private String emailProspecto;
	private String telefonoProspecto;
	private String descripcionPaquete;
	private String descripcionFormaPago;
	private Object model;
	private Object data;
	private InputStream genericInputStream;
	private String      contentType;
	private String     fileName;

	private CotizadorPortalService cotizadorPortalService;
	@Autowired
	private CotizacionMovilService cotizacionMovilService;
	private EntidadService entidadService;


	
	public String cargarVehiculos() {
		data = cotizadorPortalService.getVehiculos(descripcionVehiculo);
		return SUCCESS;
	}
	
	public String cargarEstados() {
		data = cotizadorPortalService.getEstados(estado);
		return SUCCESS;
	}

	public String cargarPaquetes() {
		data = cotizadorPortalService.getPaquetes();
		return SUCCESS;
	}
	
	public String cotizar(boolean contratar) {
		
		model = new SolicitudCotizacionMovil();

		
		if(estado == null || estado.isEmpty()) {
			throw new ApplicationException("Debe proporcionar la descripci\u00F3n de un Estado.");
		}
	
		if(descripcionVehiculo == null || descripcionVehiculo.isEmpty()) {
			throw new ApplicationException("Debe proporcionar la descripci\u00F3n de un veh\u00EDculo.");
		}
		
		Long idTarifa = cotizadorPortalService.getTarifa(estado, descripcionVehiculo);

		SolicitudCotizacionMovil solicitud = (SolicitudCotizacionMovil) model;
		solicitud.setIdDescripcionEstilo(idTarifa);
		solicitud.setTipoCotizacion(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL);
		solicitud.setNombre(nombreProspecto);
		solicitud.setEmail(emailProspecto);
		solicitud.setTelefono(telefonoProspecto);
		solicitud.setContratar(contratar);
		if(contratar) {
			if (idToCotizacionMidas != null) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("idtocotizacionmidas", idToCotizacionMidas);
				CotizacionMovilDTO cotizacionMovil = cotizadorPortalService.buscaCotizacion(params);
				if(cotizacionMovil.getProcesada()) {
					solicitud.setIdToCotizacionMidas(null);
					solicitud.setIdToCotizacionMovil(null);
				} else {
					solicitud.setIdToCotizacionMidas(cotizacionMovil.getIdtocotizacionmidas());
					solicitud.setIdToCotizacionMovil(cotizacionMovil.getIdtocotizacionmovil());
				}
			} else {
				throw new ApplicationException("Debe proporcionar un n\u00FAmero v\u00E1lido de cotizaci\u00F3n.");
			}
					
			if(descripcionPaquete != null && !descripcionPaquete.isEmpty()) {
				Long idPaquete = cotizadorPortalService.getPaquete(descripcionPaquete);
				solicitud.setIdPaquete(idPaquete);
			} else {
				throw new ApplicationException("Debe seleccionar un paquete.");
			}
				
			if(descripcionFormaPago != null && !descripcionFormaPago.isEmpty()) {
				BigDecimal idFormaPago = cotizadorPortalService.getFormaPago(descripcionFormaPago);
				solicitud.setIdFormaPago(idFormaPago);
			} else {
				throw new ApplicationException("Debe seleccionar una forma de pago.");
			}
			
			if(solicitud.getNombre() == null || solicitud.getNombre().isEmpty() ||
					solicitud.getEmail() == null || solicitud.getEmail().isEmpty() ||
					solicitud.getTelefono() == null || solicitud.getTelefono().isEmpty()) {
				throw new ApplicationException("Debe proporcionar un nombre, email y telefono.");
			}
		}

		CotizacionMovilDTO cotizacionMovil = cotizacionMovilService.getCotizacionAuto(solicitud);
		if (cotizacionMovil != null) {
			if(contratar) {
				try {
					enviarCotizacionPorCorreo(true, contratar);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			} else {
				List<ResumenCotMovilDTO> resumenList = new ArrayList<ResumenCotMovilDTO>();
				resumenList = cotizacionMovilService.inicializarDatosParaImpresion(solicitud, cotizacionMovil);
					
				List<DatosDesglosePagosMovil> datosDesgloseList = new ArrayList<DatosDesglosePagosMovil>();
				for(ResumenCotMovilDTO resumen:resumenList) {
					datosDesgloseList.add(resumen.getDatosDesglosePagosMovil());
				}
					
				cotizacionMovil.setDatosDesglosePagosMovil(datosDesgloseList);
			}
			
			data = cotizacionMovil;
		}

		
		return SUCCESS;
	}
	
	public String mostrarCotizacionAuto() {
		return this.cotizar(false);
	}
	
	public String contratarCotizacionAuto() {
		 return this.cotizar(true);
	}

	public String mostrarDetalleCotizacion() {
		data = cotizadorPortalService.cargarDetalleCoberturas(idToCotizacionMidas, descripcionPaquete);
		return SUCCESS;		
	}

	public String enviarCorreoProspecto() {
		return enviarCotizacionPorCorreo(false, false);
	}
	
	public String enviarCotizacionPorCorreo(Boolean esAgente, Boolean contratar) {
		ByteArrayAttachment attachment = null;
		GenerarReportes generarReportes = new GenerarReportes();
		List<String> destinatarios = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		List<ResumenCotMovilDTO> cotizacionMovilList = new ArrayList<ResumenCotMovilDTO>();
		String mensaje = null;
		String title = null;
		String greeting = null;
		Locale locale = getLocale();
		CotizacionMovilDTO cotizacionMovil = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idtocotizacionmidas", idToCotizacionMidas);
		cotizacionMovil = cotizadorPortalService.buscaCotizacion(params);
		
		SolicitudCotizacionMovil solicitud = new SolicitudCotizacionMovil();
		solicitud.setTipoCotizacion(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL);
		
		if (cotizacionMovil != null) {
			String vehiculo = cotizacionMovil.getModelovehiculo() + " " + cotizacionMovil.getTipovehiculo() + " " + 
			cotizacionMovil.getMarcavehiculo() + " " + cotizacionMovil.getDescripciontarifa();
			Long idTarifa = cotizadorPortalService.getTarifa(cotizacionMovil.getEstado(), vehiculo);
			solicitud.setIdDescripcionEstilo(idTarifa);
			cotizacionMovilList = cotizacionMovilService.inicializarDatosParaImpresion(solicitud, cotizacionMovil);
		}
			
		Usuario usuario = new Usuario();
		usuario.setNombre(nombreProspecto == null ? "Cliente" : nombreProspecto);
		generarReportes.setEntidadService(entidadService);
			
		attachment = new ByteArrayAttachment("CotizacionPortal_"+idToCotizacionMidas+".pdf",
					TipoArchivo.PDF, generarReportes.imprimirCotizacionMovil(
							cotizacionMovilList, locale, usuario, solicitud));
		adjuntos.add(attachment);	
		if (attachment != null) {
			if(!esAgente || contratar) {
				/* correo a cliente */
				if(emailProspecto == null || emailProspecto.isEmpty()) 
					throw new ApplicationException("Debe proporcionar un email para enviar la cotización");

				    mensaje = getText("midas.correo.cotizacion.cuerpo");
					destinatarios.add(emailProspecto);
					title = usuario.getNombre().concat("-").concat(cotizacionMovil.getIdtocotizacionmidas().toString());
					greeting = getText("midas.correo.cotizacion.saludo",
							new String[] { usuario.getNombre() });
					mailService.sendMailWithExceptionHandler(
							destinatarios,
							title,
							mensaje,
							adjuntos,
							getText("midas.correo.cotizacion.titulo"),
							greeting);
			}
			
			if(esAgente && contratar) {
				/* correo a Agente */
				mensaje = "Nos complace informarle que un prospecto suyo a utilizado nuestra aplicaci\u00F3n M\u00F3vil de Cotizaci\u00F3n y deseamos "
						+ "le de seguimiento para el cierre de su venta, la informaci\u00F3n correspondiente se encuentra en el documento Anexo "
						+ "Favor de comunicarse con su prospecto lo mas r\u00E1pido posible.";
				destinatarios.add(cotizacionMovil.getDescuentoAgente().getEmail());
				title = cotizacionMovil.getDescuentoAgente().getNombre().concat("-")
					.concat(cotizacionMovil.getIdtocotizacionmidas().toString());
				greeting = getText("midas.correo.cotizacion.saludo",
						new String[] { cotizacionMovil.getDescuentoAgente().getNombre() });
				mailService.sendMailWithExceptionHandler(
						destinatarios,
						title,
						mensaje,
						adjuntos,
						getText("midas.correo.cotizacion.titulo"),
						greeting);
			}
		}
		
		return SUCCESS;
	}
	
	public String imprimirCotizacion() {
		GenerarReportes generarReportes = new GenerarReportes();
		List<ResumenCotMovilDTO> cotizacionMovilList = new ArrayList<ResumenCotMovilDTO>();
		Locale locale = getLocale();
		CotizacionMovilDTO cotizacionMovil = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idtocotizacionmidas", idToCotizacionMidas);
		cotizacionMovil = cotizadorPortalService.buscaCotizacion(params);
		
		SolicitudCotizacionMovil solicitud = new SolicitudCotizacionMovil();
		solicitud.setTipoCotizacion(SolicitudCotizacionMovil.TIPO_COTIZACION_PORTAL);
		String vehiculo = cotizacionMovil.getModelovehiculo() + " " + cotizacionMovil.getTipovehiculo() + " " + 
		cotizacionMovil.getMarcavehiculo() + " " + cotizacionMovil.getDescripciontarifa();
		Long idTarifa = cotizadorPortalService.getTarifa(cotizacionMovil.getEstado(), vehiculo);
		solicitud.setIdDescripcionEstilo(idTarifa);
		cotizacionMovilList = cotizacionMovilService.inicializarDatosParaImpresion(solicitud, cotizacionMovil);
			
		Usuario usuario = new Usuario();
		usuario.setNombre(nombreProspecto);
		generarReportes.setEntidadService(entidadService);
				
		genericInputStream = new ByteArrayInputStream(generarReportes.imprimirCotizacionMovil(cotizacionMovilList, locale, usuario, solicitud));
		contentType = "application/pdf";
		fileName = "CotizacionPortal_"+idToCotizacionMidas+".pdf";

		return SUCCESS;
	}	

	public String cargarFormasPago() {
		data = cotizadorPortalService.getFormasPago();
		return SUCCESS;
	}
	
	public BigDecimal getIdToCotizacionMidas() {
		return idToCotizacionMidas;
	}

	public void setIdToCotizacionMidas(BigDecimal idToCotizacionMidas) {
		this.idToCotizacionMidas = idToCotizacionMidas;
	}

	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}

	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNombreProspecto() {
		return nombreProspecto;
	}

	public void setNombreProspecto(String nombreProspecto) {
		this.nombreProspecto = nombreProspecto;
	}

	public String getEmailProspecto() {
		return emailProspecto;
	}

	public void setEmailProspecto(String emailProspecto) {
		this.emailProspecto = emailProspecto;
	}

	public String getTelefonoProspecto() {
		return telefonoProspecto;
	}

	public void setTelefonoProspecto(String telefonoProspecto) {
		this.telefonoProspecto = telefonoProspecto;
	}

	public String getDescripcionPaquete() {
		return descripcionPaquete;
	}

	public void setDescripcionPaquete(String descripcionPaquete) {
		this.descripcionPaquete = descripcionPaquete;
	}

	public String getDescripcionFormaPago() {
		return descripcionFormaPago;
	}

	public void setDescripcionFormaPago(String descripcionFormaPago) {
		this.descripcionFormaPago = descripcionFormaPago;
	}

	public Object getModel() {
		return model;
	}

	public void setModel(Object model) {
		this.model = model;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Autowired
	@Qualifier("cotizadorPortalServiceEJB")
	public void setCotizadorPortalService(
			CotizadorPortalService cotizadorPortalService) {
		this.cotizadorPortalService = cotizadorPortalService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
