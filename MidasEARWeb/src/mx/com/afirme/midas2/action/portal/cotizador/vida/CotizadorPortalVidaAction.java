package mx.com.afirme.midas2.action.portal.cotizador.vida;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.portal.cotizador.vida.CotizadorPortalVidaService;
import mx.com.afirme.vida.domain.movil.cotizador.CrearCotizacionVidaParameterDTO;
import mx.com.afirme.vida.domain.movil.cotizador.OccupationDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizadorPortalVidaAction extends BaseAction implements
	Preparable, ModelDriven<Object>{

	private static final long serialVersionUID = 3121133138288793491L;
	private Object model;
	private Object data;
	private InputStream genericInputStream;
	private String      contentType;
	private String     fileName;

	@Autowired
	private CotizadorPortalVidaService cotizadorPortalVidaService;
	
	public void prepareCotizarVida() {
		model = new CrearCotizacionVidaParameterDTO();
	}
	
	public String cotizarVida() {
		data = cotizadorPortalVidaService.cotizar((CrearCotizacionVidaParameterDTO)model, false);
		return SUCCESS;
	}
	
	public void prepareContratarCotizacionVida() {
		model = new CrearCotizacionVidaParameterDTO();
	}
	
	public String contratarCotizacionVida() {
		data = cotizadorPortalVidaService.cotizar((CrearCotizacionVidaParameterDTO)model, true);
		return SUCCESS;
	}
	
	public String cargarSumasAseguradas() {
		data = cotizadorPortalVidaService.obtenerSumaAseguradaList();
		return SUCCESS;
		
	}

	public void prepareCargarOcupaciones(){
    	model= new OccupationDTO();
    	
    }
	
	public String cargarOcupaciones(){
		data = cotizadorPortalVidaService.getGiroOcupacion((OccupationDTO)model);
		return SUCCESS;
	}
	
	public String cargarPaquetes() {
		data = cotizadorPortalVidaService.getPaquetes();
		return SUCCESS;
	}
	
	public String cargarClavesSexo() {
		data = cotizadorPortalVidaService.getClaveSexo();
		return SUCCESS;
	}
	
	public void prepareImprimirCotizacion() {
		model = new CrearCotizacionVidaParameterDTO();
	}
	
	public String imprimirCotizacion() {
		genericInputStream = new ByteArrayInputStream(cotizadorPortalVidaService.imprimirCotizacion((CrearCotizacionVidaParameterDTO)model));
		contentType = "application/pdf";
		fileName = CrearCotizacionVidaParameterDTO.PORTALPDF;

		return SUCCESS;
	}
	
	public void prepareEnviarCorreoProspecto() {
		model = new CrearCotizacionVidaParameterDTO();
	}
	
	public String enviarCorreoProspecto() {
		cotizadorPortalVidaService.enviarCorreo((CrearCotizacionVidaParameterDTO)model);
		return SUCCESS;
	}
	
	public String cargarFormasPago() {
		data = cotizadorPortalVidaService.getFormasPago();
		return SUCCESS;
	}
	
	public void prepareMostrarDetalleCotizacion() {
		model = new CrearCotizacionVidaParameterDTO();
	}
	
	public String mostrarDetalleCotizacion() {
		data = cotizadorPortalVidaService.cargarDetalleCoberturas((CrearCotizacionVidaParameterDTO)model);

		return SUCCESS;
	}
	
	public void prepare() throws Exception {

	}

	public Object getModel() {
		return model;
	}

	public void setModel(Object model) {
		this.model = model;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
