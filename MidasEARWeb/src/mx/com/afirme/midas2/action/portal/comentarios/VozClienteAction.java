package mx.com.afirme.midas2.action.portal.comentarios;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.portal.seguros.Comentario;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.portal.seguros.VozClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class VozClienteAction extends BaseAction {
	
	private static final long serialVersionUID = -9061385372127375660L;
	private Object data;
	
	@Autowired
	@Qualifier("VozClienteServiceEJB")
	private VozClienteService vozClienteService;
	
	private Comentario comentario;
	
	public String sendMail(){
		if(!this.vozClienteService.enviarComentario(comentario)){
			throw new ApplicationException("Los comentarios no pudieron ser enviados.");
		}
		
		return SUCCESS;
	}
	
	public String getDepartments(){
		setData(this.vozClienteService.obtenerDepartamentos());
		return SUCCESS;
	}
	
	public String getOffices(){
		setData(this.vozClienteService.obtenerOficinas());
		return SUCCESS;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Comentario getComentario() {
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}	
}
