package mx.com.afirme.midas2.action.documentos.referencias;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.documentos.anexos.DocumentoAnexoSeccion;
import mx.com.afirme.midas2.service.documentos.anexos.DocumentoAnexoSeccionService;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class DocumentosReferenciasBasicoAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(DocumentosReferenciasBasicoAction.class);
	private List<DocumentoAnexoSeccion>  docAnexoList;
	private DocumentoAnexoSeccionService documentoAnexoSeccionService;
	private DocumentoAnexoSeccion documentoAnexoSeccion;
	private String idToSeccion;
	
	@Override
	public void prepare() throws Exception {
		
	}
	
	public String obtenerDocumentosReferencias(){
		Long idToSeccion = Long.valueOf(this.idToSeccion);
		
		try {
			docAnexoList = documentoAnexoSeccionService.findDocumentoAnexoSeccionByIdToSeccion( idToSeccion );
		} catch (Exception e) {
			LOG.error( "obtenerDocumentosReferencias: " + e.toString() );
		} 

		return SUCCESS;
	}
	
	public String guardarDocumentosReferencias(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		
		try {
			if(documentoAnexoSeccionService.updateDocumentoAnexoSeccion(accion,documentoAnexoSeccion) != null){
				super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
			}else{
				super.setMensajeExito();
			}
		} catch (Exception e) {
			LOG.error( "guardarDocumentosReferencias: " + e.toString() );
		}
		return SUCCESS;
	}
	
	public String actualizarDocumentosReferencias(){
		
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");		
		try {
			if(documentoAnexoSeccionService.updateDocumentoAnexoSeccion(accion, documentoAnexoSeccion) != null){
				super.setMensajeExito();
			}else{
				super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
			}
		} catch (Exception e) {
			LOG.error( "actualizarDocumentosReferencias: " + e.toString() );
		}
		return SUCCESS;
	}
	
	public void mostrarDocumentosAnexos(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)
	throws IOException, SystemException, ExcepcionDeAccesoADatos {
		Long idToSeccion = Long.valueOf(this.idToSeccion);
		
		try {
			docAnexoList = documentoAnexoSeccionService.findDocumentoAnexoSeccionByIdToSeccion( idToSeccion );
			MidasJsonBase json = new MidasJsonBase();
			for ( DocumentoAnexoSeccion item : docAnexoList ){			
				
				MidasJsonRow row = new MidasJsonRow();
				row.setId(item.getId().toString());
				row.setDatos(
						item.getIdToSeccion().toString(),
						item.getNombre(),
						item.getClaveObligatoriedad().toString(),
						item.getNumeroSecuencia().toString(),
						(!item.getDescripcion().equals("default")?item.getDescripcion():""),
						"/MidasWeb/img/icons/ico_verdetalle.gif^Descargar^javascript:descargarDocumentoSeccion("+
						item.getIdControlArchivo()+");^_self"
				);
				json.addRow(row);				
			}
			
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			pw.write(json.toString());
			pw.flush();
			pw.close();
			
		} catch (Exception e) {
			LOG.error( "mostrarDocumentosAnexos: " + e.toString() );
		} 		
	}	

	public List<DocumentoAnexoSeccion> getDocAnexoList() {
		return docAnexoList;
	}

	public void setDocAnexoList(List<DocumentoAnexoSeccion> docAnexoList) {
		this.docAnexoList = docAnexoList;
	}

	public String getIdToSeccion() {
		return idToSeccion;
	}

	public void setIdToSeccion(String idToSeccion) {
		this.idToSeccion = idToSeccion;
	}
	
	@Autowired
	@Qualifier("documentoAnexoSeccionServiceEJB")
	public void setDocumentoAnexoSeccionService(
			DocumentoAnexoSeccionService documentoAnexoSeccionService) {
		this.documentoAnexoSeccionService = documentoAnexoSeccionService;
	}

	public DocumentoAnexoSeccion getDocumentoAnexoSeccion() {
		return documentoAnexoSeccion;
	}

	public void setDocumentoAnexoSeccion(DocumentoAnexoSeccion documentoAnexoSeccion) {
		this.documentoAnexoSeccion = documentoAnexoSeccion;
	}
	

}
