package mx.com.afirme.midas2.action.emision.ejecutivoColoca;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.emision.ejecutivoColoca.EjecutivoColocaService;
import mx.com.afirme.midas2.service.emision.ejecutivoColoca.PolizaEjecutivoColocaDTO;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.opensymphony.xwork2.Preparable;

@Component
@Namespace(value = "/emision/ejecutivoColoca")
public class EjecutivoColocaAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3628128050098554391L;	
	
	private  List<PolizaEjecutivoColocaDTO>  listaPolizasEjecutivoColoca = new ArrayList<PolizaEjecutivoColocaDTO>() ;
	private PolizaEjecutivoColocaDTO polizaEditar;
	private String polizaMidas="";
	private String polizaSeycos=""; 
	private String endoso="";
	private String agentes="";
	private String fechaInicial="";
	private String fechaFinal="";
	private String numeroPolizaEditar;
	private Boolean polizaGuardada;
	private String userName;
	private String idPolizaEditar="";
	private String claveNeg="";
	private String idCotizacionEditar="";
	private String identificador="";
	private List< com.afirme.eibs.services.EibsUserService.UserEibsVO> listaEjecutivos= new ArrayList< com.afirme.eibs.services.EibsUserService.UserEibsVO>();
	private boolean banderaRol = false;
	private Usuario usuario;
	
	@Autowired
	@Qualifier("ejecutivoColocaService")
	private EjecutivoColocaService ejecutivoColocaService;
	
	private Usuario getUsuarioSesion() {
		HttpServletRequest request = ServletActionContext.getRequest();
		SistemaContext sistemaContext = WebApplicationContextUtils
				.getWebApplicationContext(request.getServletContext()).getBean(
						SistemaContext.class);
		return (Usuario) request.getSession().getAttribute(
				sistemaContext.getUsuarioAccesoMidas());
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Action(value = "mostrarContenedorEjecutivoColoca", results = @Result(name = SUCCESS, location = "/jsp/emision/ejecutivoColoca/ejecutivoColoca.jsp"))
	public String mostrarContenedorEjecutivoColoca() {
		return SUCCESS;
	}
	
	@Action(value = "obtenerResultadosEjecutivoColoca", results = { 
			@Result(name = SUCCESS, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaGrid.jsp")		
		})
	public String obtenerResultadosEjecutivoColoca(){
		listaPolizasEjecutivoColoca=ejecutivoColocaService.getPolizasEjecutivoColoca(polizaMidas, polizaSeycos, endoso, fechaInicial, fechaFinal, agentes, claveNeg);
		return SUCCESS;
	}
	
	@Action(value = "getPolizaEjecutivoColoca", results = { 
			@Result(name = SUCCESS, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp"),
			@Result(name = INPUT, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp")		
		})
	public String getPolizaEjecutivoColoca(){
		usuario = this.getUsuarioSesion();
		//Mandar llamar procedure 
		banderaRol = ejecutivoColocaService.getRolUser(usuario.getNombreUsuario());
		
		polizaEditar=ejecutivoColocaService.getPolizaEjecutivoColoca(polizaMidas, polizaSeycos, endoso, fechaInicial, fechaFinal, agentes, claveNeg);
		listaEjecutivos=ejecutivoColocaService.getEjecutivoColocaList(userName);
		
		return SUCCESS;
	}
	
	@Action(value = "getEjecutivoColocaList", results = { 
			@Result(name = SUCCESS, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp"),
			@Result(name = INPUT, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp")		
		})
	public String getEjecutivoColocaList(){
		listaEjecutivos=ejecutivoColocaService.getEjecutivoColocaList(numeroPolizaEditar);
		return SUCCESS;
	}
	
	@Action(value = "savePolizaEjecutivoColoca", results = { 
			@Result(name = INPUT, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp"),
			@Result(name = SUCCESS, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp")
					})	
	public String savePolizaEjecutivoColoca(){
		polizaGuardada=ejecutivoColocaService.savePolizaEjecutivoColoca(numeroPolizaEditar,idCotizacionEditar,idPolizaEditar, claveNeg,userName, endoso, identificador);
		return SUCCESS;
	}
	
	
	@Action(value = "getRolUser", results = { 
			@Result(name = INPUT, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp"),
			@Result(name = SUCCESS, location = "/jsp/emision/ejecutivoColoca/ejecutivoColocaAsignar.jsp")
					})	
	public String getRolUser(){
		usuario = this.getUsuarioSesion();

		banderaRol= ejecutivoColocaService.getRolUser(usuario.getNombreUsuario());
		return SUCCESS;
	}
	
	
	public List<PolizaEjecutivoColocaDTO> getListaPolizasEjecutivoColoca() {
		return listaPolizasEjecutivoColoca;
	}
	public void setListaPolizasEjecutivoColoca(
			List<PolizaEjecutivoColocaDTO> listaPolizasEjecutivoColoca) {
		this.listaPolizasEjecutivoColoca = listaPolizasEjecutivoColoca;
	}
	public PolizaEjecutivoColocaDTO getPolizaEditar() {
		return polizaEditar;
	}
	public void setPolizaEditar(PolizaEjecutivoColocaDTO polizaEditar) {
		this.polizaEditar = polizaEditar;
	}
	public String getPolizaMidas() {
		return polizaMidas;
	}
	public void setPolizaMidas(String polizaMidas) {
		this.polizaMidas = polizaMidas;
	}
	public String getPolizaSeycos() {
		return polizaSeycos;
	}
	public void setPolizaSeycos(String polizaSeycos) {
		this.polizaSeycos = polizaSeycos;
	}
	public String getEndoso() {
		return endoso;
	}
	public void setEndoso(String endoso) {
		this.endoso = endoso;
	}
	
	public String getNumeroPolizaEditar() {
		return numeroPolizaEditar;
	}
	public void setNumeroPolizaEditar(String numeroPolizaEditar) {
		this.numeroPolizaEditar = numeroPolizaEditar;
	}
	public Boolean getPolizaGuardada() {
		return polizaGuardada;
	}
	public void setPolizaGuardada(Boolean polizaGuardada) {
		this.polizaGuardada = polizaGuardada;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List< com.afirme.eibs.services.EibsUserService.UserEibsVO> getListaEjecutivos() {
		return listaEjecutivos;
	}
	public void setListaEjecutivos(List< com.afirme.eibs.services.EibsUserService.UserEibsVO> listaEjecutivos) {
		this.listaEjecutivos = listaEjecutivos;
	}
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaFinal() {
		return fechaFinal;
	}
	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}
	public String getAgentes() {
		return agentes;
	}
	public void setAgentes(String agentes) {
		this.agentes = agentes;
	}
	public String getIdPolizaEditar() {
		return idPolizaEditar;
	}
	public void setIdPolizaEditar(String idPolizaEditar) {
		this.idPolizaEditar = idPolizaEditar;
	}
	public void setClaveNeg(String claveNeg) {
		this.claveNeg = claveNeg;
	}
	public String getClaveNeg() {
		return claveNeg;
	}
	public void setIdCotizacionEditar(String idCotizacionEditar) {
		this.idCotizacionEditar = idCotizacionEditar;
	}
	public String getIdCotizacionEditar() {
		return idCotizacionEditar;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getIdentificador() {
		return identificador;
	}
	public boolean getBanderaRol() {
		return banderaRol;
	}
	public void setBanderaRol(boolean banderaRol) {
		this.banderaRol = banderaRol;
	}

}
