package mx.com.afirme.midas2.action.emision.conductoresAdicionales;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.emision.conductoresAdicionales.PolizaAutoSeycos;
import mx.com.afirme.midas2.service.emision.conductoresAdicionales.ConfigConductoresAdicionalesService;

import com.opensymphony.xwork2.Preparable;

/**
 * @author Adriana Flores
 *
 */
@Component
@Namespace(value = "/emision/conductoresAdicionales")
public class ConfigConductoresAdicionalesAction extends BaseAction implements
		Preparable {
	public static final Logger LOGGER = Logger
			.getLogger(ConfigConductoresAdicionalesAction.class);

	private static final long serialVersionUID = 7226735640277347435L;
	private PolizaAutoSeycos polizaAutoSeycos;
	private String numeroPoliza;

	@Autowired
	@Qualifier("configConductoresAdicionalesEJB")
	private ConfigConductoresAdicionalesService configConductoresAdicionalesService;

	/**
	 * Método Action para mostrar contenedor
	 */
	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = "/jsp/emision/conductoresAdicionales/configConductoresAdicionales.jsp") })
	public String mostrarContendor() {

		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {

	}

	public void validateBuscarPoliza() {

	}

	/**
	 * Método Action encargado de obtener la poliza buscada
	 */
	@Action(value = "buscarPoliza", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/conductoresAdicionales/configConductoresAdicionales.jsp"),
			@Result(name = INPUT, location = "/jsp/emision/conductoresAdicionales/configConductoresAdicionales.jsp") })
	public String buscarPoliza() {

		if (numeroPoliza != null && !numeroPoliza.equalsIgnoreCase("")) {

			String[] poliza = numeroPoliza.split("-");

			Map<String, Object> filtros = new HashMap<String, Object>();
			filtros.put("numeroPoliza", poliza[1]);
			int inciso = 1;

			if (polizaAutoSeycos.getIdPolizaAutoSeycos().getNumeroInciso() != null) {
				inciso = polizaAutoSeycos.getIdPolizaAutoSeycos()
						.getNumeroInciso();
			}
			filtros.put("numeroInciso", inciso);			
			filtros.put("numeroRenovacion", poliza[2]);
			filtros.put("codigoTipoPoliza", poliza[0].substring(2, 4));
			filtros.put("codigoProducto", poliza[0].substring(0, 2));

			polizaAutoSeycos = configConductoresAdicionalesService
					.buscarPoliza(filtros);
			if (polizaAutoSeycos == null) {
				super.setMensaje("No se encontraron resultados");
			}
		}

		return SUCCESS;
	}

	public void validateGuardarConductoresAdicionales() {

	}

	/**
	 * Método Action encargado de guardar los conductores adicionales
	 */
	@Action(value = "guardarConductoresAdicionales", results = {
			@Result(name = INPUT, location = "/jsp/emision/conductoresAdicionales/configConductoresAdicionales.jsp"),
			@Result(name = SUCCESS, location = "/jsp/emision/conductoresAdicionales/configConductoresAdicionales.jsp") })
	public String guardarConductoresAdicionales() {

		int updated = configConductoresAdicionalesService
				.guardarConductoresAdicionales(polizaAutoSeycos);
		if (updated != 0) {
			super.setMensajeExito();
		} else {
			super.setMensajeError("Registro no actualizado");
		}

		return SUCCESS;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public ConfigConductoresAdicionalesService getConfigConductoresAdicionalesService() {
		return configConductoresAdicionalesService;
	}

	public void setConfigConductoresAdicionalesService(
			ConfigConductoresAdicionalesService configConductoresAdicionalesService) {
		this.configConductoresAdicionalesService = configConductoresAdicionalesService;
	}

	public PolizaAutoSeycos getPolizaAutoSeycos() {
		return polizaAutoSeycos;
	}

	public void setPolizaAutoSeycos(PolizaAutoSeycos polizaAutoSeycos) {
		this.polizaAutoSeycos = polizaAutoSeycos;
	}
}
