package mx.com.afirme.midas2.action.emision.ppct;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.emision.ppct.CoberturaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.ConfiguracionPago;
import mx.com.afirme.midas2.domain.emision.ppct.OrdenPago;
import mx.com.afirme.midas2.domain.emision.ppct.Proveedor;
import mx.com.afirme.midas2.domain.emision.ppct.Recibo;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboProveedor;
import mx.com.afirme.midas2.dto.emision.ppct.SaldoDTO;
import mx.com.afirme.midas2.dto.emision.ppct.StatusSolicitudChequeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.emision.ppct.PagoProveedorService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/emision/ppct")
public class PagoProveedorAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 1L;
	private PagoProveedorService pagoProveedorService;
	private OrdenPago ordenPago;
	private ReciboProveedor reciboProveedor;
	private List<OrdenPago> listado;
	private List<Recibo> detalle;
	private List<StatusSolicitudChequeDTO> listStatus;
	private List<Proveedor> listProveedores;
	private List<CoberturaSeycos> listCoberturas;
	private String claveNeg;
	private Proveedor proveedor;
	private Date fechaCorte;
	private Boolean historico;
	private String sessionId;
	private BigDecimal importePreeliminar;
	private List<BigDecimal> listImporte = new LinkedList<BigDecimal>();
	private TransporteImpresionDTO archivo;
	private SaldoDTO filtroReporte;

	@Action(value="mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/ppct/listOrdenPago.jsp")})
	public String mostrar(){
		ordenPago = new OrdenPago();
		ordenPago.setClaveNegocio(claveNeg);
		ordenPago.setProveedor(new Proveedor());
		listProveedores = pagoProveedorService.obtenerProveedores();
		listStatus = pagoProveedorService.obtenerStatus();
		return SUCCESS;
	}

	@Action(value="buscar", 
			interceptorRefs={@InterceptorRef(value="scrollableStack", params={"ordenPago", "listado"})},
			results = {
			@Result(name=SUCCESS, location = "/jsp/emision/ppct/grid/columnasOrdenPagoGrid.jsp")})
	public String buscar(){
		listado = pagoProveedorService.obtenerOrdenesPago(ordenPago);
		return SUCCESS;
	}
	
	@Action(value="/emision/ppct/ordenpago/verDetalle", results = {
			@Result(name = SUCCESS, location ="/jsp/emision/ppct/detalle/detalleOrdenPago.jsp")
	})
	public String verOrdenPago(){
		ordenPago = pagoProveedorService.obtenerOrdenPago(ordenPago);
		listCoberturas = pagoProveedorService.obtenerCoberturasProveedor(ordenPago.getProveedor(), ordenPago.getClaveNegocio());
		historico=true;
		return SUCCESS;
	}
	
	@Action(value="agregar", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/ppct/agregarOrdenPago.jsp")})
	public String agregar(){
		listProveedores = pagoProveedorService.obtenerProveedores();
		return SUCCESS;
	}

	@Action(value = "/emision/ppct/ordenpago/continuar", results = {
			@Result(name = SUCCESS, location="/jsp/emision/ppct/detalle/detalleOrdenPago.jsp"),
			@Result(name=INPUT , location = "/jsp/emision/ppct/agregarOrdenPago.jsp")})
	public String continuar(){
		sessionId = ServletActionContext.getRequest().getRequestedSessionId();
		listProveedores = pagoProveedorService.obtenerProveedores();
		ordenPago = pagoProveedorService.obtenerPreeliminar(ordenPago, sessionId);
		listCoberturas = pagoProveedorService.obtenerCoberturasProveedor(ordenPago.getProveedor(), ordenPago.getClaveNegocio());
		historico=false;
		return SUCCESS;
	}
	
	@Action(value = "/emision/ppct/ordenpago/regresar", results = {
			@Result(name=SUCCESS , location = "/jsp/emision/ppct/agregarOrdenPago.jsp")})
	public String regresar(){
		listProveedores = pagoProveedorService.obtenerProveedores();
		sessionId = ServletActionContext.getRequest().getRequestedSessionId();
		pagoProveedorService.desasignarRecibosPreeliminares(sessionId);
		return SUCCESS;
	}
	
	@Action(value = "/emision/ppct/ordenpago/cancelar", results = {
			@Result(name=SUCCESS , type="redirectAction", 
					params={"actionName","mostrar",
							"namespace","/emision/ppct",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT , type="redirectAction", 
					params={"actionName","mostrar",
							"namespace","/emision/ppct",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
			})	
	public String cancelar(){
		try{
			pagoProveedorService.cancelarOrdenPago(ordenPago);
			setMensaje("Se cancelo correctamente la Orden de Pago "+ ordenPago.getId());
			setTipoMensaje(TIPO_MENSAJE_EXITO);	
		}catch(RuntimeException e){
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}catch(Exception e){
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(value="/emision/ppct/detalle/buscar", 
			interceptorRefs={@InterceptorRef(value="scrollableStack", params={"reciboProveedor", "detalle"})},
			results = {
			@Result(name=SUCCESS, location = "/jsp/emision/ppct/grid/columnasReciboGrid.jsp")})
	public String detalleBuscar(){
		sessionId = ServletActionContext.getRequest().getRequestedSessionId();
		reciboProveedor.setSesion(sessionId);
		
		reciboProveedor.setConfiguracionPago(new ConfiguracionPago());
		
		reciboProveedor.getConfiguracionPago().setProveedor(ordenPago.getProveedor());
		
		reciboProveedor.setOrdenPago(ordenPago);
		
		detalle = pagoProveedorService.obtenerRecibosProveedor(reciboProveedor, historico);
		return SUCCESS;
	}
	
	@Action(value="/emision/ppct/detalle/seleccionar", results = {
			@Result(name=SUCCESS, type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listImporte.*"})
			})
	public String seleccionar(){
		listImporte = new LinkedList<BigDecimal>();
		listImporte.add(pagoProveedorService.seleccionar(reciboProveedor.getId(), importePreeliminar));			
		return SUCCESS;
	}
	
	@Action(value="/emision/ppct/ordenpago/guardar", results = {
			@Result(name=SUCCESS , location="/jsp/catalogos/mensajesHeader.jsp"),
			@Result(name=INPUT , location="/jsp/emision/ppct/detalle/detalleOrdenPago.jsp")
			})
	public String guardar(){
		try{
			sessionId = ServletActionContext.getRequest().getRequestedSessionId();
			ordenPago = pagoProveedorService.guardarOrdenPago(ordenPago, sessionId);
			setMensaje("Se guardo con exito la Orden de Pago "+ ordenPago.getId());
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setNextFunction("cerrarVentanaAgregar();");
		}catch(RuntimeException e){
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);	
			listCoberturas = pagoProveedorService.obtenerCoberturasProveedor(ordenPago.getProveedor(), ordenPago.getClaveNegocio());
			historico=false;		
			e.printStackTrace();
			return INPUT;
		}catch(Exception e){
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="/reportes/ppct/saldos/mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/reportes/reporteSaldos.jsp")})
	public String mostrarReporteSaldos(){
		
		filtroReporte = new SaldoDTO();
		filtroReporte.setClaveNegocio(claveNeg);
		
		return SUCCESS;
	}
	
	@Action
	(value = "/reportes/ppct/saldos/exportarDetalleExcel", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Detalle Reporte de Saldos", "xls"})},
			results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${archivo.contentType}",
					"inputName", "archivo.genericInputStream",
					"contentDisposition", "attachment;filename=${archivo.fileName}"})}) 
	public String exportarDetalleExcel() {	
					
		archivo = pagoProveedorService.obtenerDetalleReporteSaldos(filtroReporte);
				
		return SUCCESS;
	}
	
	@Action
	(value = "/reportes/ppct/saldos/exportarResumenExcel", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Resumen Reporte de Saldos", "xls"})},
			results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${archivo.contentType}",
					"inputName", "archivo.genericInputStream",
					"contentDisposition", "attachment;filename=${archivo.fileName}"})}) 
	public String exportarResumenExcel() {	
		
		archivo = pagoProveedorService.obtenerResumenReporteSaldos(filtroReporte);
		
		return SUCCESS;
	}
	
	
	@Override
	public void prepare() throws Exception {
		
		
	}
	
	@Autowired
	public void setPagoProveedorService(
			PagoProveedorService pagoProveedorService) {
		this.pagoProveedorService = pagoProveedorService;
	}
	
	public OrdenPago getOrdenPago() {
		return ordenPago;
	}

	public void setOrdenPago(OrdenPago ordenPago) {
		this.ordenPago = ordenPago;
	}

	public ReciboProveedor getReciboProveedor() {
		return reciboProveedor;
	}

	public void setReciboProveedor(ReciboProveedor reciboProveedor) {
		this.reciboProveedor = reciboProveedor;
	}
	
	public List<OrdenPago> getListado() {
		return listado;
	}

	public void setListado(List<OrdenPago> listado) {
		this.listado = listado;
	}

	public List<Recibo> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<Recibo> detalle) {
		this.detalle = detalle;
	}
	
	public PagoProveedorService getPagoProveedorService() {
		return pagoProveedorService;
	}

	public List<StatusSolicitudChequeDTO> getListStatus() {
		return listStatus;
	}

	public void setListStatus(List<StatusSolicitudChequeDTO> listStatus) {
		this.listStatus = listStatus;
	}

	public List<Proveedor> getListProveedores() {
		return listProveedores;
	}

	public void setListProveedores(List<Proveedor> listProveedores) {
		this.listProveedores = listProveedores;
	}


	public List<CoberturaSeycos> getListCoberturas() {
		return listCoberturas;
	}

	public void setListCoberturas(List<CoberturaSeycos> listCoberturas) {
		this.listCoberturas = listCoberturas;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public String getClaveNeg() {
		return claveNeg;
	}

	public void setClaveNeg(String claveNeg) {
		this.claveNeg = claveNeg;
	}

	public Date getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(Date fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public Boolean getHistorico() {
		return historico;
	}

	public void setHistorico(Boolean historico) {
		this.historico = historico;
	}

	public BigDecimal getImportePreeliminar() {
		return importePreeliminar;
	}

	public void setImportePreeliminar(BigDecimal importePreeliminar) {
		this.importePreeliminar = importePreeliminar;
	}	

	public List<BigDecimal> getListImporte() {
		return listImporte;
	}

	public void setListImporte(List<BigDecimal> listImporte) {
		this.listImporte = listImporte;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public TransporteImpresionDTO getArchivo() {
		return archivo;
	}

	public void setArchivo(TransporteImpresionDTO archivo) {
		this.archivo = archivo;
	}

	public SaldoDTO getFiltroReporte() {
		return filtroReporte;
	}

	public void setFiltroReporte(SaldoDTO filtroReporte) {
		this.filtroReporte = filtroReporte;
	}
	
	
}