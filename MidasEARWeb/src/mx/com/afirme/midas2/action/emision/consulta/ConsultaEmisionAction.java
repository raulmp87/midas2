package mx.com.afirme.midas2.action.emision.consulta;

import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.emision.consulta.Consulta;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaEmision;
import mx.com.afirme.midas2.dto.emision.consulta.SeccionConsulta;
import mx.com.afirme.midas2.service.emision.consulta.ConsultaEmisionService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/emision/consulta")
public class ConsultaEmisionAction  extends BaseAction implements Preparable {

	private static final long serialVersionUID = 967064356389761672L;

	
	@Action(value = "navegar", results = { 
			@Result(name = SeccionConsulta.AGENTE, location = "/jsp/emision/consulta/grid/consultaAgenteGrid.jsp"),
			@Result(name = SeccionConsulta.CLIENTE, location = "/jsp/emision/consulta/grid/consultaClienteGrid.jsp"),
			@Result(name = SeccionConsulta.POLIZA, location = "/jsp/emision/consulta/grid/consultaPolizaGrid.jsp"),
			@Result(name = SeccionConsulta.ANEXO, location = "/jsp/emision/consulta/grid/consultaAnexoGrid.jsp"),
			@Result(name = SeccionConsulta.ENDOSO, location = "/jsp/emision/consulta/grid/consultaEndosoGrid.jsp"),
			@Result(name = SeccionConsulta.VEHICULO, location = "/jsp/emision/consulta/grid/consultaVehiculoGrid.jsp"),
			@Result(name = SeccionConsulta.COBERTURA, location = "/jsp/emision/consulta/grid/consultaCoberturaGrid.jsp"),
			@Result(name = SeccionConsulta.SINIESTRO, location = "/jsp/emision/consulta/grid/consultaSiniestroGrid.jsp"),
			@Result(name = SeccionConsulta.COBRANZA, location = "/jsp/emision/consulta/grid/consultaCobranzaGrid.jsp")
	})			
	public String navegar() {
		
		if (seccion == null) seccion = SeccionConsulta.AGENTE;
		
		listado = consultaEmisionService.navegar(consultaEmision, seccion, posStart, count, orderBy, direct);
		
		return seccion;
	}

	
	@Action(value = "seleccionar", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/consulta/emisionConsultaDetalle.jsp") })
	public String seleccionar() {
		
		consultaEmision = consultaEmisionService.seleccionar(consultaEmision, seccion);
		return SUCCESS;
	}
	
	
	@Action(value = "mostrarDetalle", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/consulta/emisionConsultaDetalle.jsp") })
	public String mostrarDetalle() {
		return SUCCESS;
	}
	
	@Action(value = "mostrar", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/consulta/emisionConsulta.jsp") })
	public String mostrar() {
		return SUCCESS;
	}
	
	@Action(value = "mostrarResumen", results = {
			@Result(name = SUCCESS, location = "/jsp/emision/consulta/emisionConsultaResumen.jsp") })
	public String mostrarResumen() {
		return SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		consultaEmision = new ConsultaEmision();
	}
	
	
	private ConsultaEmision consultaEmision;
	
	private String seccion;
		
	private Integer posStart;
	
	private Integer count;
	
	private String orderBy;
	
	private String direct;
	
	private List<Consulta> listado;
	
	
	public ConsultaEmision getConsultaEmision() {
		return consultaEmision;
	}

	public void setConsultaEmision(ConsultaEmision consultaEmision) {
		this.consultaEmision = consultaEmision;
	}
	
	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
	public Integer getPosStart() {
		return posStart;
	}

	public void setPosStart(Integer posStart) {
		this.posStart = posStart;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getDirect() {
		return direct;
	}

	public void setDirect(String direct) {
		this.direct = direct;
	}

	public List<Consulta> getListado() {
		return listado;
	}

	public void setListado(List<Consulta> listado) {
		this.listado = listado;
	}


	private ConsultaEmisionService consultaEmisionService;
	
	@Autowired
	@Qualifier("consultaEmisionServiceEJB")
	public void setConsultaEmisionService(
			ConsultaEmisionService consultaEmisionService) {
		this.consultaEmisionService = consultaEmisionService;
	}
	
}
