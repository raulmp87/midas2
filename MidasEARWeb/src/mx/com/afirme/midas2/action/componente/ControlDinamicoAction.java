package mx.com.afirme.midas2.action.componente;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import mx.com.afirme.midas.base.CacheableDTO;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.service.componente.ComponenteService;
import mx.com.afirme.midas2.validator.group.EditItemChecks;
import mx.com.afirme.midas2.validator.group.EditVarModifPrimaChecks;
import mx.com.afirme.midas2.validator.group.NewItemChecks;
import mx.com.afirme.midas2.validator.group.NewVarModifPrimaChecks;
import mx.com.afirme.midas2.validator.group.TarifaAutoModifChecks;
import mx.com.afirme.midas2.dto.TipoAccionDTO;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author root
 *
 */
@Component
@Scope("prototype")
public class ControlDinamicoAction extends BaseAction implements Preparable{
	private static final long serialVersionUID = 1L;

	
	/**
	 * Asegurar que los valores de tarifaVersionId esten como hiddens.
	 */
	private TarifaVersionId tarifaVersionId;
	
	private List<RegistroDinamicoDTO> listaRegistros;
	
	private RegistroDinamicoDTO registroDinamico;
	
	private Object registro;
	
	private String accion;
	private String clave;
	private String claveTipoVista;
	private ComponenteService componenteService;
	private String prefix;
	
	private static final String namePrefix = "registro";
	
	
	public ControlDinamicoAction() {	
	}
	
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	@SuppressWarnings("unused")
	private RegistroDinamicoDTO createRegistroDinamicoDTO() {
		boolean editable = false;
		
		RegistroDinamicoDTO registroDinamicoDTO = new RegistroDinamicoDTO();
		registroDinamicoDTO.setId("TAR-1-2-3-4-5");
		List<ControlDinamicoDTO> controlDinamicoDTOs = new ArrayList<ControlDinamicoDTO>();
		registroDinamicoDTO.setControles(controlDinamicoDTOs);
		
		ControlDinamicoDTO textbox = new ControlDinamicoDTO();
		textbox.setAtributoMapeo("miTexto");
		textbox.setEtiqueta("midas.mock.textfield");
		textbox.setTipoControl(ControlDinamicoDTO.TEXTBOX);
		textbox.setValor("Un valor");
		textbox.setEditable(editable);
		
		ControlDinamicoDTO select = new ControlDinamicoDTO();
		select.setAtributoMapeo("miSelect");
		select.setEtiqueta("midas.mock.select");
		select.setTipoControl(ControlDinamicoDTO.SELECT);
		select.setValor("H");
		select.setEditable(editable);
		
		
		List<CacheableDTO> cacheableDTOs = new ArrayList<CacheableDTO>();
		CacheableObject item1 = new CacheableObject();
		item1.setDescription("Hombre");
		item1.setId("H");
		
		CacheableObject item2 = new CacheableObject();
		item2.setDescription("Mujer");
		item2.setId("M");
		
		cacheableDTOs.add(item1);
		cacheableDTOs.add(item2);
		
		select.setElementosCombo(cacheableDTOs);

		
		ControlDinamicoDTO checkbox = new ControlDinamicoDTO();
		checkbox.setAtributoMapeo("miCheckbox");
		checkbox.setEtiqueta("midas.mock.checkbox");
		checkbox.setTipoControl(ControlDinamicoDTO.CHECKBOX);
		checkbox.setValor("true");
		checkbox.setEditable(editable);
		
		ControlDinamicoDTO radio = new ControlDinamicoDTO();
		radio.setAtributoMapeo("miRadio");
		radio.setEtiqueta("midas.mock.radiobutton");
		radio.setTipoControl(ControlDinamicoDTO.RADIOBUTTON);
		radio.setValor("H");
		radio.setEditable(editable);
		radio.setElementosCombo(cacheableDTOs);
		
		ControlDinamicoDTO hidden = new ControlDinamicoDTO();
		hidden.setAtributoMapeo("miHidden");
		hidden.setTipoControl(ControlDinamicoDTO.HIDDEN);
		hidden.setValor("oculto");
		
		controlDinamicoDTOs.add(textbox);
		controlDinamicoDTOs.add(select);
		controlDinamicoDTOs.add(checkbox);
		controlDinamicoDTOs.add(radio);
		controlDinamicoDTOs.add(hidden);
		
		return registroDinamicoDTO;
		
//		when(componenteService.getRegistro(anyString(), anyString())).thenReturn(registroDinamicoDTO);
	}

	class CacheableObject extends CacheableDTO {
		private static final long serialVersionUID = 1L;
		private String id;
		private String description;
		
		@Override
		public String getDescription() {
			return description;
		}
		
		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public Object getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
			

		@Override
		public boolean equals(Object arg0) {
			return false;
		}
		
	}
	
	public void prepareListadoRegistros(){
		// invocación del servicio que regresa el listado de registros dinámicos a partir de un TarifaVersionId.
		listaRegistros = componenteService.getListado(clave,claveTipoVista);
		
		//invocación del servicio que regresa un registro listo para agregar, que contenga los controles.
		//Para renderear el encabezado.
		
		registroDinamico = componenteService.getRegistro(clave,claveTipoVista, TipoAccionDTO.getAgregarModificar());
		if(registroDinamico.getControles() != null){
			int numControlesVista = 0;
			for(ControlDinamicoDTO control : registroDinamico.getControles()){
				if(control.getTipoControl() != ControlDinamicoDTO.HIDDEN){
					numControlesVista++;
				}
			}
			registroDinamico.setNumControlesVista(numControlesVista);
		}
	}
	
	public String listadoRegistros(){
		return SUCCESS;
	}
	
	public void prepareVerDetalle() {
		registroDinamico = componenteService.getRegistro(clave, claveTipoVista, accion);
	}
	
	public String verDetalle(){
		if(accion.equals(TipoAccionDTO.getFiltrar())){
			
			return "filtro";
		}
		return SUCCESS;
	}
	
	public void prepareActualizarRegistro(){
		registro = componenteService.getRegistroEspecifico(clave);
	}
	
	public String actualizarRegistro(){
		componenteService.actualizarRegistro(registro, accion);
		setMensajeExito(); 
		return SUCCESS;
	}
	
	public void prepareFiltrarRegistros(){
		registro = componenteService.getRegistroEspecifico(clave);
	}
	
	public String filtrarRegistros(){
		// invocaci�n del servicio que regresa el listado de registros dinamicos a partir de un objeto como filtro.
		listaRegistros = componenteService.getListado(clave,claveTipoVista,registro);
		
		//invocaci�n del servicio que regresa un registro listo para agregar, que contenga los controles.
		//Para renderear el encabezado.
		registroDinamico = componenteService.getRegistro(clave,claveTipoVista, TipoAccionDTO.getVer());
		return SUCCESS;
	}
	
	public void validateActualizarRegistro() {
		System.out.print("Entro a validar");
		setMensajeError("Revise los datos");
		registroDinamico = componenteService.getRegistroParaValidacion(clave, claveTipoVista, accion, registro);
		if(claveTipoVista != null && claveTipoVista.equals("VistaTarifaVarModificadoraPrima")){
			if (accion.equalsIgnoreCase(TipoAccionDTO.getAgregarModificar())) {
				addErrors(registro, TarifaAutoModifChecks.class, this, "registro");
			}
			if (accion.equalsIgnoreCase(TipoAccionDTO.getEditar())) {
				addErrors(registro, TarifaAutoModifChecks.class, this, "registro");
			}
		}else{
			if(clave.contains("VMP_")){
				if (accion.equalsIgnoreCase(TipoAccionDTO.getAgregarModificar())) {
					addErrors(registro, NewVarModifPrimaChecks.class, this, "registro");
				}
				if (accion.equalsIgnoreCase(TipoAccionDTO.getEditar())) {
					addErrors(registro, EditVarModifPrimaChecks.class, this, "registro");
				}
			}else{
				if (accion.equalsIgnoreCase(TipoAccionDTO.getAgregarModificar())) {
					addErrors(registro, NewItemChecks.class, this, "registro");
				}
				if (accion.equalsIgnoreCase(TipoAccionDTO.getEditar())) {
					addErrors(registro, EditItemChecks.class, this, "registro");
				}
			}
		}
	}
	
	@Autowired
	@Qualifier("componenteEJB")
	public void setComponenteService(ComponenteService componenteService){
		this.componenteService = componenteService;
	}
	
	public List<RegistroDinamicoDTO> getListaRegistros() {
		return listaRegistros;
	}

	public void setListaRegistros(List<RegistroDinamicoDTO> listaRegistros) {
		this.listaRegistros = listaRegistros;
	}

	public Object getRegistro() {
		return registro;
	}

	public void setRegistro(Object registro) {
		this.registro = registro;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public TarifaVersionId getTarifaVersionId() {
		return tarifaVersionId;
	}

	public void setTarifaVersionId(TarifaVersionId tarifaVersionId) {
		this.tarifaVersionId = tarifaVersionId;
	}

	public RegistroDinamicoDTO getRegistroDinamico() {
		return registroDinamico;
	}

	public void setRegistroDinamico(RegistroDinamicoDTO registroDinamico) {
		this.registroDinamico = registroDinamico;
	}
	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getClaveTipoVista() {
		return claveTipoVista;
	}

	public void setClaveTipoVista(String claveTipoVista) {
		this.claveTipoVista = claveTipoVista;
	}
	
	public String getNamePrefix() {
		return ControlDinamicoAction.namePrefix;
	}
	
	public boolean isMostrarSubmit() {
		if (accion.equalsIgnoreCase(TipoAccionDTO.getVer())) {
			return false;
		}
		return true;
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSubmitKey() {
		
		String submitKey = "midas.boton.";
		if (accion.equalsIgnoreCase(TipoAccionDTO.getAgregarModificar())) {
			submitKey = submitKey + "guardar";
		} else if (accion.equalsIgnoreCase(TipoAccionDTO.getEditar())) {
			submitKey = submitKey + "actualizar";
		} else if (accion.equalsIgnoreCase(TipoAccionDTO.getEliminar())) {
			submitKey = submitKey + "borrar";
		}
		return submitKey;
	}

	@Override
	public void prepare() throws Exception {
	}
	
}
