package mx.com.afirme.midas2.action.componente.correo;

import java.io.ByteArrayInputStream;

import mx.com.afirme.midas2.action.BaseAction;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class ControlCorreoAction extends BaseAction{

	private static final long serialVersionUID = 3094606682638145994L;

	private String tipoEnvio;	
	private String archivoAEnviar;
	
	private ByteArrayInputStream archivoAdjunto;	
	private Integer tipoCorreo;	
	private String nombreDestinario;
	private String correoDestinatario;

	public String capturarDatosCorreo(){
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		tipoCorreo = (Integer) valueStack.findValue(tipoEnvio, Integer.class);
		archivoAdjunto = (ByteArrayInputStream) valueStack.findValue(archivoAEnviar, ByteArrayInputStream.class);
		
		return SUCCESS;
	}
	
	public Integer getTipoCorreo() {
		return tipoCorreo;
	}

	public void setTipoCorreo(Integer tipoCorreo) {
		this.tipoCorreo = tipoCorreo;
	}

	public String getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(String tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public String getArchivoAEnviar() {
		return archivoAEnviar;
	}

	public void setArchivoAEnviar(String archivoAEnviar) {
		this.archivoAEnviar = archivoAEnviar;
	}

	public ByteArrayInputStream getArchivoAdjunto() {
		return archivoAdjunto;
	}

	public void setArchivoAdjunto(ByteArrayInputStream archivoAdjunto) {
		this.archivoAdjunto = archivoAdjunto;
	}

	public String getNombreDestinario() {
		return nombreDestinario;
	}

	public void setNombreDestinario(String nombreDestinario) {
		this.nombreDestinario = nombreDestinario;
	}

	public String getCorreoDestinatario() {
		return correoDestinatario;
	}

	public void setCorreoDestinatario(String correoDestinatario) {
		this.correoDestinatario = correoDestinatario;
	}

}
