package mx.com.afirme.midas2.action.componente.paises;

import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.ListadoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/componente/paises")
public class PaisesCtg extends BaseAction implements Preparable {
	
	
	private int    cpId;
	private String cp;
	private String coloniaId;
	private String ciudadId;
	private String estadoId;
	private String paisId;
	
	private Map<String, String> paises   = new LinkedHashMap<String, String>();
	private Map<String, String> estados  = new LinkedHashMap<String, String>();
	private Map<String, String> ciudades = new LinkedHashMap<String, String>();
	private Map<String, String> colonias = new LinkedHashMap<String, String>();
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	

	@Action(value="paisJson", results = {
		    @Result(name=SUCCESS, type="json"),
		    @Result(name=INPUT, type="json")
	})
	public String paisJson() throws Exception {
		paises = this.listadoService.getMapPaises();	
		return SUCCESS;
	}
	

	@Action(value="estadoJson", results = {
		    @Result(name=SUCCESS, type="json"),
		    @Result(name=INPUT, type="json")
	})
	public String estadoJson() throws Exception {
		estados = listadoService.getMapEstados(paisId);
		return SUCCESS;
	}
	

	@Action(value="ciudadJson", results = {
		    @Result(name=SUCCESS, type="json"),
		    @Result(name=INPUT, type="json")
	})
	public String ciudadJson() throws Exception {
		ciudades = listadoService.getMapMunicipiosPorEstado(estadoId);
		return SUCCESS;
	}	
	

	@Action(value="coloniaJson", results = {
		    @Result(name=SUCCESS, type="json"),
		    @Result(name=INPUT, type="json")
	})
	public String coloniaJson() throws Exception {
		colonias = listadoService.getMapColoniasSameValue(ciudadId);
		return SUCCESS;
	}
	
	

	@Action(value="codigoPostalJson", results = {
		    @Result(name="success", type="json"),
		    @Result(name=INPUT, type="json")
	})
	public String codigoPostalJson() throws Exception {
		//System.out.println("Colonia ID"+ URLDecoder.decode( coloniaId) );
		cp = listadoService.getCodigoPostalByColonyNameAndCityId(String.valueOf(coloniaId), String.valueOf(ciudadId));
		return SUCCESS;
	}
	
	
	
	public String getPaisId() {
		return paisId;
	}
	public void setPaisId(String paisId) {
		this.paisId = paisId;
	}
	public Map<String, String> getPaises() {
		return paises;
	}
	public void setPaises(Map<String, String> paises) {
		this.paises = paises;
	}
	public String getEstadoId() {
		return estadoId;
	}
	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}
	public Map<String, String> getEstados() {
		return estados;
	}
	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}
	public String getCiudadId() {
		return ciudadId;
	}
	public void setCiudadId(String ciudadId) {
		this.ciudadId = ciudadId;
	}
	public Map<String, String> getCiudades() {
		return ciudades;
	}
	public void setCiudades(Map<String, String> ciudades) {
		this.ciudades = ciudades;
	}
	public String getColoniaId() {
		return coloniaId;
	}
	public void setColoniaId(String coloniaId) {
		this.coloniaId = coloniaId;
	}
	public Map<String, String> getColonias() {
		return colonias;
	}
	public void setColonias(Map<String, String> colonias) {
		this.colonias = colonias;
	}
	public int getCpId() {
		return cpId;
	}
	public void setCpId(int cpId) {
		this.cpId = cpId;
	}

}
