package mx.com.afirme.midas2.action.componente.vehiculo;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.afirme.midas.catalogos.codigopostal.CodigoPostalDN;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.js.util.StringUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class ControlVehiculoAction extends BaseAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4777510835482795758L;
	
	//Servicios
	private ListadoService listadoService;
	private NegocioSeccionService negocioSeccionService;
	private EntidadService entidadService;
	private IncisoService incisoService;
	private Boolean aplicaFlotilla = true;
	//Recibe el nombre asociado al objeto del action que se desea poblar.
	private String idEstadoName;
	private String idMunicipioName;
	private String idCotizacionName;
	private String idMonedaName;
	private String idNegocioSeccionName;
	private String idMarcaVehiculoName;
	private String idEstiloVehiculoName;
	private String modificadoresPrimaName;
	private String modificadoresDescripcionName;
	private String descripcionFinalName;
	private String idModeloVehiculoName;
	private String idTipoUsoVehiculoName;
	private String idNegocioPaqueteName;
	private String checkEsFronterizoName;
	private String idExpressName;
	//
	private String idNegocioName;
	private String idProductoName;
	private String idTipoPolizaName;
	private String pctDescuentoEstadoName;
	private String serieName;
	private String vinValidoName;
	private String coincideEstiloName;
	private String observacionesSesaName;
	private String claveAmisName;
	private String claveSesaName;
	private String idTipoServicioVehiculoName;
	private String idCodigoPostalName;
	//Recibe el nombre de la funcion a ejecutar en la propiedad OnChange del listado paquete
	private String onChangePaquete="";
	
	//Bandera que indica si un control del componente se muestra o no.
	private Boolean mostrarEstado = true;
	private Boolean mostrarMunicipio = true;
	private Boolean mostrarLineaNegocio = true;
	private Boolean mostrarMarcaVehiculo = true ;
	private Boolean mostrarEstiloVehiculo = true;
	private Boolean mostrarAjustarCaracteristicas = true;
	private Boolean mostrarModeloVehiculo = true;
	private Boolean mostrarTipoUsoVehiculo = true;
	private Boolean mostrarPaquete = true;
	private Boolean cotizacionExpress = false;
	private Boolean mostrarCheckEsFronterizo = false;
	private Boolean mostrarTipoServicio = true;
	
	
	//Recibe el valor de los paramatros con los que se invoca el componente
	private String idEstado;
	private String idMunicipio;
	private BigDecimal idCotizacion;
	private BigDecimal idMoneda;
	private BigDecimal idNegocioSeccion;
	private BigDecimal idMarcaVehiculo;
	private String idEstiloVehiculo;
	private String modificadoresPrima;
	private String modificadoresDescripcion;
	private String descripcionFinal;
	private Short idModeloVehiculo;
	private BigDecimal idTipoUsoVehiculo;
	private Long idNegocioPaquete;
	private Short soloConsulta = 0;
	private String estiloSeleccionado;
	private EstiloVehiculoDTO estiloModificarCaracteristicas;
	private String descripcionBase;
	//
	private Long idNegocio;
	private BigDecimal idProducto;
	private BigDecimal idTipoPoliza;
	private Double pctDescuentoEstado;
	private String serie;
	private Boolean vinValido;
	private Boolean coincideEstilo;
	private String observacionesSesa;
	private String claveAmis;
	private String claveSesa;
	private BigDecimal idTipoServicioVehiculo;
	private String idCodigoPostal;
	
	
	//Listados presentes en el componente 
	private Map<String, String> estadoMap;	
	private Map<String, String> municipioMap = new LinkedHashMap<String, String>();
	private List<NegocioSeccion> negocioSeccionList;
	private Map<BigDecimal, String> marcaMap = new LinkedHashMap<BigDecimal, String>();
	private Map<String, String> estiloMap = new LinkedHashMap<String, String>();
	private Map<Short, Short> modeloMap = new LinkedHashMap<Short, Short>();
	private List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList;
	private Map<Long, String> paqueteMap = new LinkedHashMap<Long, String>();
	private Map<BigDecimal, String> tipoUsoMap = new LinkedHashMap<BigDecimal, String>();
	private Map<BigDecimal, String> tipoServicioMap = new LinkedHashMap<BigDecimal, String>();
	
	private Integer usuarioExterno = 2;
	
	//Propiedades de los controles
	private String posicionEtiqueta = "left"; 
	private String requerido = "true";
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	public String getDatosVehiculo(){
		//Carga los valores del valuestack para asignarlas a las propiedades correspondientes del componente
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		idEstado = (String) valueStack.findValue(idEstadoName, String.class);
		idMunicipio = (String) valueStack.findValue(idMunicipioName, String.class);
		idCotizacion = (BigDecimal) valueStack.findValue(idCotizacionName, BigDecimal.class);
		idMoneda = (BigDecimal) valueStack.findValue(idMonedaName, BigDecimal.class);
		idNegocioSeccion = (BigDecimal) valueStack.findValue(idNegocioSeccionName, BigDecimal.class);
		idMarcaVehiculo = (BigDecimal) valueStack.findValue(idMarcaVehiculoName, BigDecimal.class);
		idEstiloVehiculo = (String) valueStack.findValue(idEstiloVehiculoName, String.class);
		modificadoresPrima = (String) valueStack.findValue(modificadoresPrimaName, String.class);
		modificadoresDescripcion = (String) valueStack.findValue(modificadoresDescripcionName, String.class);
		descripcionFinal = (String) valueStack.findValue(descripcionFinalName, String.class);
		idModeloVehiculo = (Short) valueStack.findValue(idModeloVehiculoName, Short.class);
		idTipoUsoVehiculo = (BigDecimal) valueStack.findValue(idTipoUsoVehiculoName, BigDecimal.class);
		idNegocioPaquete = (Long) valueStack.findValue(idNegocioPaqueteName, Long.class);
		Long id = (Long) valueStack.findValue(idExpressName, Long.class);
		pctDescuentoEstado =  (Double) valueStack.findValue(pctDescuentoEstadoName, Double.class);
		serie = (String) valueStack.findValue(serieName, String.class);
		vinValido = (Boolean) valueStack.findValue(vinValidoName, Boolean.class);
		coincideEstilo = (Boolean) valueStack.findValue(coincideEstiloName, Boolean.class);
		observacionesSesa = (String) valueStack.findValue(observacionesSesaName, String.class);
		claveAmis = (String) valueStack.findValue(claveAmisName, String.class);
		claveSesa = (String) valueStack.findValue(claveSesaName, String.class);
		idTipoServicioVehiculo=(BigDecimal) valueStack.findValue(idTipoServicioVehiculoName, BigDecimal.class);
		idCodigoPostal = (String) valueStack.findValue(idCodigoPostalName, String.class);

		//Carga los listados de acuerdo a los valores recibidos en los parametros
		CotizacionDTO cotizacionDTO = null;
		if(idCotizacion != null){
			 cotizacionDTO = entidadService.findById(CotizacionDTO.class, idCotizacion);
		}	
		if(cotizacionDTO != null && cotizacionDTO.getSolicitudDTO() != null && cotizacionDTO.getSolicitudDTO().getNegocio() != null){
			estadoMap = listadoService.getEstadosPorNegocioId(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), cotizacionExpress);
		}
		if(cotizacionExpress){
			estadoMap = listadoService.getEstadosPorNegocioId(null, cotizacionExpress);
		}
		//estadoMap = listadoService.getMapEstadosMX();
		if(idEstado != null){
			municipioMap = listadoService.getMapMunicipiosPorEstado(idEstado);
		}
		if(cotizacionExpress){
			//mostrarLineaNegocio=false;
			if(idNegocioSeccion != null){
				negocioSeccionList = negocioSeccionService.getSeccionListByExpress(idNegocioSeccion);
				if(id == null){
					idNegocioSeccion = null;
				}
			}
				
			mostrarTipoUsoVehiculo = false;
			mostrarPaquete = false;
			mostrarCheckEsFronterizo = true;
			mostrarTipoServicio = false;
		}else{
			if(idCotizacion != null){
				negocioSeccionList = negocioSeccionService.getSeccionListByCotizacion(cotizacionDTO);
			}
		}
		
		if(idNegocioSeccion != null)
			marcaMap = listadoService.getMapMarcaVehiculoPorNegocioSeccion(idNegocioSeccion);
		
		if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null)
			estiloMap = listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idMarcaVehiculo,idNegocioSeccion, idMoneda);
		if(idEstiloVehiculo != null )
			modeloMap = listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(idMoneda, idEstiloVehiculo, idNegocioSeccion);
		if(idModeloVehiculo != null)
			tipoUsoMap = listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(idEstiloVehiculo, idNegocioSeccion);
		if(idTipoUsoVehiculo != null)	
			paqueteMap = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion);	
		if(idTipoServicioVehiculo!=null)
			tipoServicioMap=listadoService.getMapTipoServicioVehiculo(idNegocioSeccion);
		
		if(aplicaFlotilla == false){
			return "individual";
		}
		return SUCCESS;
	}
	
	public String getDatosVehiculoAgente(){
		//Carga los valores del valuestack para asignarlas a las propiedades correspondientes del componente
		

		ValueStack valueStack = ActionContext.getContext().getValueStack();
		idEstado = (String) valueStack.findValue(idEstadoName, String.class);
		idMunicipio = (String) valueStack.findValue(idMunicipioName, String.class);
		idCotizacion = (BigDecimal) valueStack.findValue(idCotizacionName, BigDecimal.class);
		idMoneda = (BigDecimal) valueStack.findValue(idMonedaName, BigDecimal.class);
		idNegocioSeccion = (BigDecimal) valueStack.findValue(idNegocioSeccionName, BigDecimal.class);
		idMarcaVehiculo = (BigDecimal) valueStack.findValue(idMarcaVehiculoName, BigDecimal.class);
		idEstiloVehiculo = (String) valueStack.findValue(idEstiloVehiculoName, String.class);
		modificadoresPrima = (String) valueStack.findValue(modificadoresPrimaName, String.class);
		modificadoresDescripcion = (String) valueStack.findValue(modificadoresDescripcionName, String.class);
		descripcionFinal = (String) valueStack.findValue(descripcionFinalName, String.class);
		idModeloVehiculo = (Short) valueStack.findValue(idModeloVehiculoName, Short.class);
		idTipoUsoVehiculo = (BigDecimal) valueStack.findValue(idTipoUsoVehiculoName, BigDecimal.class);
		idNegocioPaquete = (Long) valueStack.findValue(idNegocioPaqueteName, Long.class);
		Long id = (Long) valueStack.findValue(idExpressName, Long.class);
		//Agente
		idNegocio = (Long) valueStack.findValue(idNegocioName, Long.class);
		idProducto = (BigDecimal) valueStack.findValue(idProductoName, BigDecimal.class);
		idTipoPoliza = (BigDecimal) valueStack.findValue(idTipoPolizaName, BigDecimal.class);
		pctDescuentoEstado =  (Double) valueStack.findValue(pctDescuentoEstadoName, Double.class);
		serie = (String) valueStack.findValue(serieName, String.class);
		vinValido = (Boolean) valueStack.findValue(vinValidoName, Boolean.class);
		observacionesSesa = (String) valueStack.findValue(observacionesSesaName, String.class);
		coincideEstilo = (Boolean) valueStack.findValue(coincideEstiloName, Boolean.class);
		claveAmis = (String) valueStack.findValue(claveAmisName, String.class);
		claveSesa = (String) valueStack.findValue(claveSesaName, String.class);
		idTipoServicioVehiculo=(BigDecimal) valueStack.findValue(idTipoServicioVehiculoName, BigDecimal.class);
		idCodigoPostal = (String) valueStack.findValue(idCodigoPostalName, String.class);

		//Carga los listados de acuerdo a los valores recibidos en los parametros
		CotizacionDTO cotizacionDTO = null;
		if(idCotizacion != null){
			 cotizacionDTO = entidadService.findById(CotizacionDTO.class, idCotizacion);
			  //Cambio de Negocio se reinician valores
			 try{
			  NegocioProducto negocioProducto = entidadService.findById(NegocioProducto.class, idProducto.longValue());
			  NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idTipoPoliza);
			  if(!idNegocio.equals(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio()) ||
					  !negocioProducto.getProductoDTO().getIdToProducto().equals(cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto()) ||
					  !negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza().equals(cotizacionDTO.getTipoPolizaDTO().getIdToTipoPoliza())){
				  idEstado = null;
				  idMunicipio = null;
				  idNegocioSeccion = null;
				  idMarcaVehiculo = null;
				  idEstiloVehiculo = null;
				  idModeloVehiculo = null;
				  idTipoUsoVehiculo = null;
				  idNegocioPaquete = null;
				  descripcionFinal = null;
			  }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
		}
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual") &&
				idEstado == null && idMunicipio == null) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			List<Domicilio> domicilios = agenteUsuarioActual.getPersona().getDomicilios();
			for(Domicilio dom: domicilios){
				if(dom.getTipoDomicilio().equals("PERS")){
					idEstado = dom.getClaveEstado();
					idMunicipio = dom.getClaveCiudad();
				}
			}
		}
		
		if(idNegocio != null){
			estadoMap = listadoService.getEstadosPorNegocioId(idNegocio, cotizacionExpress);
		}
		if(cotizacionExpress){
			estadoMap = listadoService.getEstadosPorNegocioId(null, cotizacionExpress);
		}
		//estadoMap = listadoService.getMapEstadosMX();
		if(idEstado != null){
			municipioMap = listadoService.getMapMunicipiosPorEstado(idEstado);
		}
		if(cotizacionExpress){
			//mostrarLineaNegocio=false;
			if(idNegocioSeccion != null){
				negocioSeccionList = negocioSeccionService.getSeccionListByExpress(idNegocioSeccion);
				if(id == null){
					idNegocioSeccion = null;
				}
			}
				
			mostrarTipoUsoVehiculo = false;
			mostrarPaquete = false;
			mostrarCheckEsFronterizo = true;
			mostrarTipoServicio = false;
		}else{
			if(idNegocio != null && idProducto != null && idTipoPoliza != null){
				negocioSeccionList = negocioSeccionService.getSeccionListByIdNegocioProductoTipoPoliza(
					idProducto, idNegocio, idTipoPoliza);
			}
		}
		
		if(idNegocioSeccion != null)
			marcaMap = listadoService.getMapMarcaVehiculoPorNegocioSeccion(idNegocioSeccion);
		
		if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null)
			modeloMap = listadoService.getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(idMoneda, idMarcaVehiculo, idNegocioSeccion);
					
		if(idModeloVehiculo != null )
			estiloMap = listadoService.getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMoneda(idMarcaVehiculo,idModeloVehiculo,idNegocioSeccion, idMoneda, null);
		
		if(idEstiloVehiculo != null)
			tipoUsoMap = listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(idEstiloVehiculo, idNegocioSeccion);
		
		if(idTipoUsoVehiculo != null)	
			paqueteMap = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion);
		
		if(idTipoServicioVehiculo!=null)
			tipoServicioMap=listadoService.getMapTipoServicioVehiculo(idNegocioSeccion);
		
		if(aplicaFlotilla == false){
			return "individual";
		}
		
		// se obtienen tipo de usuario para bloquear o no el campo de descripcion final
		Usuario usuario = usuarioService.getUsuarioActual();
		usuarioExterno = usuario.getTipoUsuario();
		
		return SUCCESS;
	}
	
	public String mostrarOtrasCaract(){
		String[] estiloId = null;
		if(!StringUtil.isEmpty(estiloSeleccionado)){
			estiloId = estiloSeleccionado.split("_");
			EstiloVehiculoId id = new EstiloVehiculoId(estiloId[0], estiloId[1],
					BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
			estiloModificarCaracteristicas = entidadService.findById(EstiloVehiculoDTO.class, id);
			descripcionBase = estiloModificarCaracteristicas.getDescripcionBase();
			if(modificadoresDescripcion == null || modificadoresDescripcion.isEmpty()){
				try{
					modificadoresDescripcion = incisoService.getModificadoresDescripcionDefautlt(estiloModificarCaracteristicas);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		caracteristicasVehiculoList = incisoService.listarCaracteristicas(modificadoresDescripcion);
		return SUCCESS;
	}
	
/*	public void validateDefinirOtrasCaract(){
		int i=0;
		for(CaracteristicasVehiculoDTO caracteristicasVehiculoDTO:caracteristicasVehiculoList){			
			addErrors(caracteristicasVehiculoDTO, NewItemChecks.class, this, "caracteristicasVehiculoList["+i+"]");			
			i++;			
		}
	}*/
	
	public String definirOtrasCaract(){
		IncisoAutoCot incisoAutoCot = incisoService.definirOtrasCaract(caracteristicasVehiculoList, "");
		descripcionFinal = descripcionBase;
		descripcionFinal = (descripcionFinal + " " + incisoAutoCot.getDescripcionFinal()).trim();
		modificadoresDescripcion = incisoAutoCot.getModificadoresDescripcion();
		return SUCCESS;
	}


	public String getIdEstadoName() {
		return idEstadoName;
	}


	public void setIdEstadoName(String idEstadoName) {
		this.idEstadoName = idEstadoName;
	}


	public String getIdMunicipioName() {
		return idMunicipioName;
	}


	public void setIdMunicipioName(String idMunicipioName) {
		this.idMunicipioName = idMunicipioName;
	}


	public String getIdCotizacionName() {
		return idCotizacionName;
	}

	public void setIdCotizacionName(String idCotizacionName) {
		this.idCotizacionName = idCotizacionName;
	}

	public String getIdMonedaName() {
		return idMonedaName;
	}

	public void setIdMonedaName(String idMonedaName) {
		this.idMonedaName = idMonedaName;
	}

	public String getIdNegocioSeccionName() {
		return idNegocioSeccionName;
	}


	public void setIdNegocioSeccionName(String idNegocioSeccionName) {
		this.idNegocioSeccionName = idNegocioSeccionName;
	}


	public String getIdMarcaVehiculoName() {
		return idMarcaVehiculoName;
	}


	public void setIdMarcaVehiculoName(String idMarcaVehiculoName) {
		this.idMarcaVehiculoName = idMarcaVehiculoName;
	}


	public String getIdEstiloVehiculoName() {
		return idEstiloVehiculoName;
	}


	public void setIdEstiloVehiculoName(String idEstiloVehiculoName) {
		this.idEstiloVehiculoName = idEstiloVehiculoName;
	}


	public String getModificadoresPrimaName() {
		return modificadoresPrimaName;
	}

	public void setModificadoresPrimaName(String modificadoresPrimaName) {
		this.modificadoresPrimaName = modificadoresPrimaName;
	}

	public String getModificadoresDescripcionName() {
		return modificadoresDescripcionName;
	}

	public void setModificadoresDescripcionName(String modificadoresDescripcionName) {
		this.modificadoresDescripcionName = modificadoresDescripcionName;
	}

	public String getDescripcionFinalName() {
		return descripcionFinalName;
	}

	public void setDescripcionFinalName(String descripcionFinalName) {
		this.descripcionFinalName = descripcionFinalName;
	}

	public String getIdModeloVehiculoName() {
		return idModeloVehiculoName;
	}


	public void setIdModeloVehiculoName(String idModeloVehiculoName) {
		this.idModeloVehiculoName = idModeloVehiculoName;
	}


	public String getIdTipoUsoVehiculoName() {
		return idTipoUsoVehiculoName;
	}


	public void setIdTipoUsoVehiculoName(String idTipoUsoVehiculoName) {
		this.idTipoUsoVehiculoName = idTipoUsoVehiculoName;
	}


	public String getIdNegocioPaqueteName() {
		return idNegocioPaqueteName;
	}


	public void setIdNegocioPaqueteName(String idNegocioPaqueteName) {
		this.idNegocioPaqueteName = idNegocioPaqueteName;
	}


	public String getCheckEsFronterizoName() {
		return checkEsFronterizoName;
	}

	public void setCheckEsFronterizoName(String checkEsFronterizoName) {
		this.checkEsFronterizoName = checkEsFronterizoName;
	}

	public String getOnChangePaquete() {
		return onChangePaquete;
	}


	public void setOnChangePaquete(String onChangePaquete) {
		this.onChangePaquete = onChangePaquete;
	}


	public Boolean getMostrarEstado() {
		return mostrarEstado;
	}


	public void setMostrarEstado(Boolean mostrarEstado) {
		this.mostrarEstado = mostrarEstado;
	}


	public Boolean getMostrarMunicipio() {
		return mostrarMunicipio;
	}


	public void setMostrarMunicipio(Boolean mostrarMunicipio) {
		this.mostrarMunicipio = mostrarMunicipio;
	}


	public Boolean getMostrarLineaNegocio() {
		return mostrarLineaNegocio;
	}


	public void setMostrarLineaNegocio(Boolean mostrarLineaNegocio) {
		this.mostrarLineaNegocio = mostrarLineaNegocio;
	}


	public Boolean getMostrarMarcaVehiculo() {
		return mostrarMarcaVehiculo;
	}


	public void setMostrarMarcaVehiculo(Boolean mostrarMarcaVehiculo) {
		this.mostrarMarcaVehiculo = mostrarMarcaVehiculo;
	}


	public Boolean getMostrarEstiloVehiculo() {
		return mostrarEstiloVehiculo;
	}


	public void setMostrarEstiloVehiculo(Boolean mostrarEstiloVehiculo) {
		this.mostrarEstiloVehiculo = mostrarEstiloVehiculo;
	}


	public Boolean getMostrarAjustarCaracteristicas() {
		return mostrarAjustarCaracteristicas;
	}

	public void setMostrarAjustarCaracteristicas(
			Boolean mostrarAjustarCaracteristicas) {
		this.mostrarAjustarCaracteristicas = mostrarAjustarCaracteristicas;
	}

	public Boolean getMostrarModeloVehiculo() {
		return mostrarModeloVehiculo;
	}


	public void setMostrarModeloVehiculo(Boolean mostrarModeloVehiculo) {
		this.mostrarModeloVehiculo = mostrarModeloVehiculo;
	}


	public Boolean getMostrarTipoUsoVehiculo() {
		return mostrarTipoUsoVehiculo;
	}


	public void setMostrarTipoUsoVehiculo(Boolean mostrarTipoUsoVehiculo) {
		this.mostrarTipoUsoVehiculo = mostrarTipoUsoVehiculo;
	}


	public Boolean getMostrarPaquete() {
		return mostrarPaquete;
	}


	public void setMostrarPaquete(Boolean mostrarPaquete) {
		this.mostrarPaquete = mostrarPaquete;
	}


	public Boolean getCotizacionExpress() {
		return cotizacionExpress;
	}


	public void setCotizacionExpress(Boolean cotizacionExpress) {
		if(cotizacionExpress != null){
			this.cotizacionExpress = cotizacionExpress;
		}
	}


	public Boolean getMostrarCheckEsFronterizo() {
		return mostrarCheckEsFronterizo;
	}

	public void setMostrarCheckEsFronterizo(Boolean mostrarCheckEsFronterizo) {
		this.mostrarCheckEsFronterizo = mostrarCheckEsFronterizo;
	}

	public String getIdEstado() {
		return idEstado;
	}


	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}


	public String getIdMunicipio() {
		return idMunicipio;
	}


	public void setIdMunicipio(String idMunicipio) {
		this.idMunicipio = idMunicipio;
	}


	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public BigDecimal getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}


	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}


	public BigDecimal getIdMarcaVehiculo() {
		return idMarcaVehiculo;
	}


	public void setIdMarcaVehiculo(BigDecimal idMarcaVehiculo) {
		this.idMarcaVehiculo = idMarcaVehiculo;
	}


	public String getIdEstiloVehiculo() {
		return idEstiloVehiculo;
	}


	public void setIdEstiloVehiculo(String idEstiloVehiculo) {
		this.idEstiloVehiculo = idEstiloVehiculo;
	}


	public String getModificadoresPrima() {
		return modificadoresPrima;
	}

	public void setModificadoresPrima(String modificadoresPrima) {
		this.modificadoresPrima = modificadoresPrima;
	}

	public String getModificadoresDescripcion() {
		return modificadoresDescripcion;
	}

	public void setModificadoresDescripcion(String modificadoresDescripcion) {
		this.modificadoresDescripcion = modificadoresDescripcion;
	}

	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	public Short getIdModeloVehiculo() {
		return idModeloVehiculo;
	}


	public void setIdModeloVehiculo(Short idModeloVehiculo) {
		this.idModeloVehiculo = idModeloVehiculo;
	}


	public Long getIdNegocioPaquete() {
		return idNegocioPaquete;
	}


	public void setIdNegocioPaquete(Long idNegocioPaquete) {
		this.idNegocioPaquete = idNegocioPaquete;
	}


	public BigDecimal getIdTipoUsoVehiculo() {
		return idTipoUsoVehiculo;
	}


	public void setIdTipoUsoVehiculo(BigDecimal idTipoUsoVehiculo) {
		this.idTipoUsoVehiculo = idTipoUsoVehiculo;
	}


	public Map<String, String> getEstadoMap() {
		return estadoMap;
	}


	public void setEstadoMap(Map<String, String> estadoMap) {
		this.estadoMap = estadoMap;
	}


	public Map<String, String> getMunicipioMap() {
		return municipioMap;
	}


	public void setMunicipioMap(Map<String, String> municipioMap) {
		this.municipioMap = municipioMap;
	}


	public List<NegocioSeccion> getNegocioSeccionList() {
		return negocioSeccionList;
	}


	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}


	public Map<BigDecimal, String> getMarcaMap() {
		return marcaMap;
	}


	public void setMarcaMap(Map<BigDecimal, String> marcaMap) {
		this.marcaMap = marcaMap;
	}


	public Map<String, String> getEstiloMap() {
		return estiloMap;
	}


	public void setEstiloMap(Map<String, String> estiloMap) {
		this.estiloMap = estiloMap;
	}


	public Map<Short, Short> getModeloMap() {
		return modeloMap;
	}


	public void setModeloMap(Map<Short, Short> modeloMap) {
		this.modeloMap = modeloMap;
	}


	public List<CaracteristicasVehiculoDTO> getCaracteristicasVehiculoList() {
		return caracteristicasVehiculoList;
	}


	public void setCaracteristicasVehiculoList(
			List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList) {
		this.caracteristicasVehiculoList = caracteristicasVehiculoList;
	}


	public Map<Long, String> getPaqueteMap() {
		return paqueteMap;
	}


	public void setPaqueteMap(Map<Long, String> paqueteMap) {
		this.paqueteMap = paqueteMap;
	}


	public Map<BigDecimal, String> getTipoUsoMap() {
		return tipoUsoMap;
	}


	public void setTipoUsoMap(Map<BigDecimal, String> tipoUsoMap) {
		this.tipoUsoMap = tipoUsoMap;
	}

	public String getPosicionEtiqueta() {
		return posicionEtiqueta;
	}

	public void setPosicionEtiqueta(String posicionEtiqueta) {
		this.posicionEtiqueta = posicionEtiqueta;
	}

	public String getRequerido() {
		return requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	public Boolean getAplicaFlotilla() {
		return aplicaFlotilla;
	}

	public void setAplicaFlotilla(Boolean aplicaFlotilla) {
		this.aplicaFlotilla = aplicaFlotilla;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public String getEstiloSeleccionado() {
		return estiloSeleccionado;
	}

	public void setEstiloSeleccionado(String estiloSeleccionado) {
		this.estiloSeleccionado = estiloSeleccionado;
	}

	public EstiloVehiculoDTO getEstiloModificarCaracteristicas() {
		return estiloModificarCaracteristicas;
	}

	public void setEstiloModificarCaracteristicas(
			EstiloVehiculoDTO estiloModificarCaracteristicas) {
		this.estiloModificarCaracteristicas = estiloModificarCaracteristicas;
	}

	public String getDescripcionBase() {
		return descripcionBase;
	}

	public void setDescripcionBase(String descripcionBase) {
		this.descripcionBase = descripcionBase;
	}

	public void setIdExpressName(String idExpressName) {
		this.idExpressName = idExpressName;
	}

	public String getIdExpressName() {
		return idExpressName;
	}

	public void setIdNegocioName(String idNegocioName) {
		this.idNegocioName = idNegocioName;
	}

	public String getIdNegocioName() {
		return idNegocioName;
	}

	public void setIdProductoName(String idProductoName) {
		this.idProductoName = idProductoName;
	}

	public String getIdProductoName() {
		return idProductoName;
	}

	public void setIdTipoPolizaName(String idTipoPolizaName) {
		this.idTipoPolizaName = idTipoPolizaName;
	}

	public String getIdTipoPolizaName() {
		return idTipoPolizaName;
	}

	public String getPctDescuentoEstadoName() {
		return pctDescuentoEstadoName;
	}

	public void setPctDescuentoEstadoName(String pctDescuentoEstadoName) {
		this.pctDescuentoEstadoName = pctDescuentoEstadoName;
	}

	public void setIdNegocio(Long idNegocio) {
		this.idNegocio = idNegocio;
	}

	public Long getIdNegocio() {
		return idNegocio;
	}

	public void setIdProducto(BigDecimal idProducto) {
		this.idProducto = idProducto;
	}

	public BigDecimal getIdProducto() {
		return idProducto;
	}

	public void setIdTipoPoliza(BigDecimal idTipoPoliza) {
		this.idTipoPoliza = idTipoPoliza;
	}

	public BigDecimal getIdTipoPoliza() {
		return idTipoPoliza;
	}
	

	public Double getPctDescuentoEstado() {
		return pctDescuentoEstado;
	}

	public void setPctDescuentoEstado(Double pctDescuentoEstado) {
		this.pctDescuentoEstado = pctDescuentoEstado;
	}

	public String getSerieName() {
		return serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getVinValidoName() {
		return vinValidoName;
	}

	public void setVinValidoName(String vinValidoName) {
		this.vinValidoName = vinValidoName;
	}

	public Boolean getVinValido() {
		return vinValido;
	}

	public void setVinValido(Boolean vinValido) {
		this.vinValido = vinValido;
	}

	public String getObservacionesSesaName() {
		return observacionesSesaName;
	}

	public void setObservacionesSesaName(String observacionesSesaName) {
		this.observacionesSesaName = observacionesSesaName;
	}

	public String getObservacionesSesa() {
		return observacionesSesa;
	}

	public void setObservacionesSesa(String observacionesSesa) {
		this.observacionesSesa = observacionesSesa;
	}

	public String getCoincideEstiloName() {
		return coincideEstiloName;
	}

	public void setCoincideEstiloName(String coincideEstiloName) {
		this.coincideEstiloName = coincideEstiloName;
	}

	public Boolean getCoincideEstilo() {
		return coincideEstilo;
	}

	public void setCoincideEstilo(Boolean coincideEstilo) {
		this.coincideEstilo = coincideEstilo;
	}

	public String getClaveAmisName() {
		return claveAmisName;
	}

	public void setClaveAmisName(String claveAmisName) {
		this.claveAmisName = claveAmisName;
	}

	public String getClaveSesaName() {
		return claveSesaName;
	}

	public void setClaveSesaName(String claveSesaName) {
		this.claveSesaName = claveSesaName;
	}

	public String getClaveAmis() {
		return claveAmis;
	}

	public void setClaveAmis(String claveAmis) {
		this.claveAmis = claveAmis;
	}

	public String getClaveSesa() {
		return claveSesa;
	}

	public void setClaveSesa(String claveSesa) {
		this.claveSesa = claveSesa;
	}

	public Map<BigDecimal, String> getTipoServicioMap() {
		return tipoServicioMap;
	}

	public void setTipoServicioMap(Map<BigDecimal, String> tipoServicioMap) {
		this.tipoServicioMap = tipoServicioMap;
	}

	public String getIdTipoServicioVehiculoName() {
		return idTipoServicioVehiculoName;
	}

	public void setIdTipoServicioVehiculoName(String idTipoServicioVehiculoName) {
		this.idTipoServicioVehiculoName = idTipoServicioVehiculoName;
	}

	public BigDecimal getIdTipoServicioVehiculo() {
		return idTipoServicioVehiculo;
	}

	public void setIdTipoServicioVehiculo(BigDecimal idTipoServicioVehiculo) {
		this.idTipoServicioVehiculo = idTipoServicioVehiculo;
	}

	public Boolean getMostrarTipoServicio() {
		return mostrarTipoServicio;
	}

	public void setMostrarTipoServicio(Boolean mostrarTipoServicio) {
		this.mostrarTipoServicio = mostrarTipoServicio;
	}

	public String getIdCodigoPostal() {
		return idCodigoPostal;
	}

	public void setIdCodigoPostal(String idCodigoPostal) {
		this.idCodigoPostal = idCodigoPostal;
	}

	public String getIdCodigoPostalName() {
		return idCodigoPostalName;
	}

	public void setIdCodigoPostalName(String idCodigoPostalName) {
		this.idCodigoPostalName = idCodigoPostalName;
	}
	
	
	
	public Integer getUsuarioExterno() {
		return usuarioExterno;
	}

	public void setUsuarioExterno(Integer usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}

	
	
	
	
	
	
	
}
