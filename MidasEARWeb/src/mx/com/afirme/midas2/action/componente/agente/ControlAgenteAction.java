package mx.com.afirme.midas2.action.componente.agente;

import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class ControlAgenteAction extends BaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//Component Params
	
	private String idAgenteCompName;
	private String idTcAgenteCompName;
	private String nombreAgenteCompName;
	private Boolean permiteBuscar;
	private CotizacionDTO cotizacion;
	
	/**
	 * Agente utilizado como modelo. Es necesario poner este nombre para evitar que haya colisiones 
	 * por nombre con otros actions que esten en el ValueStack.
	 */
	private AgenteView controlAgenteActionAgente;	
	private EntidadService entidadService;
	private AgenteMidasService agenteMidasService;
	
	public String getInfoAgente(){
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		Long idAgente = (Long) valueStack.findValue(idAgenteCompName, Long.class);
		Integer idTcAgente = (Integer) valueStack.findValue(idTcAgenteCompName, Integer.class);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			//No se permite buscar ya que el unico agente para seleccionar es el agente que tiene asociado el usuario.
			permiteBuscar = false;
		}
		
		if (idTcAgente != null || idAgente != null) {
			//Buscar agente.
			if (idAgente != null) {
				controlAgenteActionAgente = buscarPorId(idAgente);
			} else if (idTcAgente != null) {
				controlAgenteActionAgente = buscarPorIdTcAgente(idTcAgente);
				//Intentar nuevamente asumiendo que el idTcAgente es el id
				if (controlAgenteActionAgente == null) {
					controlAgenteActionAgente = buscarPorId(Long.valueOf(idTcAgente));
				}
			}
		}
		
		if (controlAgenteActionAgente == null) {
			controlAgenteActionAgente = crearAgenteNoDisponible();
		}
		
		return SUCCESS;
	}
	
	public String getInfoAgenteCotizador(){
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		Long idAgente = (Long) valueStack.findValue(idAgenteCompName, Long.class);
		Integer idTcAgente = (Integer) valueStack.findValue(idTcAgenteCompName, Integer.class);
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			//No se permite buscar ya que el unico agente para seleccionar es el agente que tiene asociado el usuario.
			permiteBuscar = false;
		}
		
		if (idTcAgente != null || idAgente != null) {
			//Buscar agente.
			if (idAgente != null) {
				controlAgenteActionAgente = buscarPorId(idAgente);
			} else if (idTcAgente != null) {
				controlAgenteActionAgente = buscarPorIdTcAgente(idTcAgente);
				//Intentar nuevamente asumiendo que el idTcAgente es el id
				if (controlAgenteActionAgente == null) {
					controlAgenteActionAgente = buscarPorId(Long.valueOf(idTcAgente));
				}
			}
		}else if(cotizacion != null && cotizacion.getSolicitudDTO().getCodigoAgente() != null){
			controlAgenteActionAgente = buscarPorIdTcAgente(cotizacion.getSolicitudDTO().getCodigoAgente().intValue());
		}
		
		if (controlAgenteActionAgente == null) {
			controlAgenteActionAgente = crearAgenteNoDisponible();
		}
		
		return SUCCESS;
	}
	
	private AgenteView crearAgenteNoDisponible() {
		AgenteView agenteView = new AgenteView();
		agenteView.setId(0l);
		agenteView.setNombreCompleto("No Disponible");
		return agenteView;
	}

	private AgenteView buscarPorId(Long id) {
		Agente filtroAgente = new Agente();
		filtroAgente.setId(id);
		List<AgenteView> agenteViewList = agenteMidasService.findByFilterLightWeight(filtroAgente);	
		if (agenteViewList.size() == 1) {
			return agenteViewList.get(0);
		}
		return null;
	}
	
	private AgenteView buscarPorIdTcAgente(Integer idTcAgente) {
		Agente filtroAgente = new Agente();
		filtroAgente.setIdAgente(Long.valueOf(idTcAgente));
		List<AgenteView> agenteViewList = agenteMidasService.findByFilterLightWeight(filtroAgente);	
		if (agenteViewList.size() == 1) {
			return agenteViewList.get(0);
		}
		return null;
	}

	public String getIdAgenteCompName() {
		return idAgenteCompName;
	}
	
	public void setIdAgenteCompName(String idAgenteCompName) {
		this.idAgenteCompName = idAgenteCompName;
	}

	public String getIdTcAgenteCompName() {
		return idTcAgenteCompName;
	}

	public void setIdTcAgenteCompName(String idTcAgenteCompName) {
		this.idTcAgenteCompName = idTcAgenteCompName;
	}

	public String getNombreAgenteCompName() {
		return nombreAgenteCompName;
	}

	public void setNombreAgenteCompName(String nombreAgenteCompName) {
		this.nombreAgenteCompName = nombreAgenteCompName;
	}

	public Boolean getPermiteBuscar() {
		return permiteBuscar;
	}

	public void setPermiteBuscar(Boolean permiteBuscar) {
		this.permiteBuscar = permiteBuscar;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}


	public AgenteView getControlAgenteActionAgente() {
		return controlAgenteActionAgente;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}
	
}
