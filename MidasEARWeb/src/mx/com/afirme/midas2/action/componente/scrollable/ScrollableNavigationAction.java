package mx.com.afirme.midas2.action.componente.scrollable;

import mx.com.afirme.midas2.action.BaseAction;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/componente/scrollable")
public class ScrollableNavigationAction extends BaseAction {

	private static final long serialVersionUID = -1115569232378526793L;

	/* (non-Javadoc)
	 * Esta accion/componente permite como parametros:
	 * 
	 	name : Nombre del grid. Por default esta el nombre 'scrollableGrid'
	 	form : Nombre de la forma que contiene al grid
		loadAction : ruta del Action para cargar el grid
		selectJs (opcional) : funcion js al seleccionar un registro (tiene que recibir como parametro una variable que se llenara con el objeto seleccionado serializado) 
		checkJs (opcional) : funcion js al seleccionar un checkbox en el grid
		cellChangedJs (opcional) : funcion js al modificarse una celda
		gridColumnsPath : ruta del jsp con la definicion de las columnas del grid (XML)
		gridRowPath : ruta del jsp con la definicion de los datos del registro en el grid (XML)

	 * @see com.opensymphony.xwork2.ActionSupport#execute()
	 */
	@Action(value = "scrollableGrid", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/scrollable/scrollableArea.jsp") })
	public String execute() {		
		
		if (name == null) {
			name = "scrollableGrid";
		}
				
		return SUCCESS;
	}
		
	private String name;
	
	private String form;
	
	private String loadAction;
	
	private String selectJs;
	
	private String checkJs;
	
	private String cellChangedJs;
	
	private String gridColumnsPath;
	
	private String gridRowPath;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getLoadAction() {
		return loadAction;
	}

	public void setLoadAction(String loadAction) {
		this.loadAction = loadAction;
	}

	public String getSelectJs() {
		return selectJs;
	}

	public void setSelectJs(String selectJs) {
		this.selectJs = selectJs;
	}
	
	public String getCheckJs() {
		return checkJs;
	}

	public void setCheckJs(String checkJs) {
		this.checkJs = checkJs;
	}

	public String getCellChangedJs() {
		return cellChangedJs;
	}

	public void setCellChangedJs(String cellChangedJs) {
		this.cellChangedJs = cellChangedJs;
	}

	public String getGridColumnsPath() {
		return gridColumnsPath;
	}

	public void setGridColumnsPath(String gridColumnsPath) {
		this.gridColumnsPath = gridColumnsPath;
	}

	public String getGridRowPath() {
		return gridRowPath;
	}

	public void setGridRowPath(String gridRowPath) {
		this.gridRowPath = gridRowPath;
	}
	
	

	
}
