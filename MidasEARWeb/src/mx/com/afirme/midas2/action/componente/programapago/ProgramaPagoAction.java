package mx.com.afirme.midas2.action.componente.programapago;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilFacadeRemote;
import mx.com.afirme.midas.catalogos.giro.CatalogoGiroDTO;
import mx.com.afirme.midas.catalogos.giro.CatalogoGiroFacadeRemote;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.catalogos.sector.SectorFacadeRemote;
import mx.com.afirme.midas.catalogos.tipodomiciliacion.TipoDomiciliacionDTO;
import mx.com.afirme.midas.catalogos.tipodomiciliacion.TipoDomiciliacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDN;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.resumen.ResumenCotizacionDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDN;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.json.MidasJsonBase;
import mx.com.afirme.midas.sistema.json.MidasJsonRow;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cobranza.programapago.ProgramaPago;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToProgPago;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToRecibo;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.dto.cobranza.programapago.ReciboVO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService.ContenedorDatosProgramaPago;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService.Saldo;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

@Component
@Scope("prototype")
@Namespace("/componente/programapago")
public class ProgramaPagoAction extends BaseAction implements Preparable {
	
	public static final Logger LOGGER = Logger.getLogger(ProgramaPagoAction.class);
	
	private static final long serialVersionUID = -895178401377026017L;

	private InputStream genericInputStream;
	private String contentType;
	private String fileName;
	
	private Saldo saldo;
	private String nombreContratante;
	private String rfc;
	private String direccionFiscal;
	private List<String> personasRelacionadas;
	private Map<String, String> estadoMap;
	private Map<String, String> municipioMap;
	private Map<String, String> coloniasMap;
	private Map<String, String> paisesMap;
	private ListadoService listadoService;
	private ContenedorDatosProgramaPago contenedorDatosProgramaPago;
	private String listJsonRecibosEditada;
	private RecuotificacionService recuotificacionService;
	private ClienteFacadeRemote clienteFacade;
	private List<ClienteGenericoDTO> mediosPagoCliente = new ArrayList<ClienteGenericoDTO>();
	private List<PaisDTO> paises = new ArrayList<PaisDTO>();
	private String fechaActual;
	private List<SectorDTO> sectores = new ArrayList<SectorDTO>();
	private SectorFacadeRemote sectorFacade;
	private List<EstadoCivilDTO> estadosCiviles = new ArrayList<EstadoCivilDTO>();
	private EstadoCivilFacadeRemote estadoCivilFacade;
	private List<CatalogoGiroDTO> giros = new ArrayList<CatalogoGiroDTO>();
	private List<BancoEmisorDTO> bancos = new ArrayList<BancoEmisorDTO>(1);
	private List<TipoDomiciliacionDTO> mediosPagoDomiciliacion = new ArrayList<TipoDomiciliacionDTO>();
	private CatalogoGiroFacadeRemote catalogoGiroFacade;
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	private TipoDomiciliacionFacadeRemote tipoDomiciliacionFacade;
	private PaisFacadeRemote paisFacade;
	private Map<String, String> personasAseguradas = new HashMap<String, String>();
	private Integer tipoPersona;
	private ClienteGenericoDTO cliente; // Este seria el Cliente Fiscal
	private BigDecimal idToCotizacion;
	private Long idToProgramaPago;
	private List<ToProgPago> listProgPago = new ArrayList<ToProgPago>();
	private List<ToRecibo> listRecibo = new ArrayList<ToRecibo>();
	private ProgramaPagoService programaPagoService;
	private Boolean banderaClienteCot;
	private Boolean errorGuardadoCliente;
	private String mensageError;
	private String idClienteAsegurado;
	private CotizacionService cotizacionService;
	private NegocioRecuotificacionService negocioRecuotificacionService;
	
	private boolean clienteCot = false;
	
	private Boolean banderaRecuotManual;

	private Boolean tieneRolRecuotificacion = Boolean.FALSE;
	
	private BigDecimal idCliente;

	private Boolean clienteExistenteEnSeycos;

	private Boolean banderaClienteProgramaPago;
	
	
	/*
	 * Atributos de la adaptacion del componente de recuotificacio de Midas
	 * Daños
	 */
	private ProgramaPago programaPagoForm;
	private String id;
	private String readOnly;
	private List<ToProgPago> resList = new ArrayList<ToProgPago>();
	private List<ToProgPagoGraper> proPagosBandera = new ArrayList<ProgramaPagoAction.ToProgPagoGraper>();
	
	private List<String> listaProgPagoJson = new ArrayList<String>();
	private String respuestaJason;
	private String respuestaJasonSaldoOriginal;
	private InputStream stream;
	private String idProgPago;
	private Integer banderaRecuotifica = RecuotificacionService.AccionRecuotificacion.NO_RECUOTIFICAR.ordinal(); // Valor por defecto de la bandera

	private String impPrimaNeta;
	private String impDerechos;
	private String impRecargos;
	private String impIva;
	private String impPrimaTotal;
	private String idCotizacion;
	private EntidadService entidadService;
	private CalculoService calculoService;
	
	//* VALORES PARA ENDOSO DE AJUSTE DE PRIMA
	private BigDecimal idContinuity;
	private String tipoFormaPago;
	private BigDecimal numeroEndoso;
	
	private PolizaService polizaService;

	
	

	// valor para determinar el tipo de movimiento de programa de pago
	private String tipoMov;
	private BigDecimal idToSolicitud;
	private BigDecimal idToPoliza;
	
	
	
	/***** DCR *****/
	
	private String json;
	
	private Double saldoCampo;
	
	private String tipoSaldo;
	
	private Long progPagoId;
	
	/*****/

	@Action(value = "getlistaProgramaPagos", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/programaPagoListado.jsp"),
			@Result(name = INPUT, location = "/jsp/componente/programapago/programaPagoListado.jsp") })
	public String listar() {

		/* Llamada al Stored Procedure de recuatificacion */

		return SUCCESS;
	}

	@Action(value = "getlistaProgramaPagosOriginal", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/programaPagoListado.jsp"),
			@Result(name = INPUT, location = "/jsp/componente/programapago/programaPagoListado.jsp") })
	public String listarProgPagosOriginal() {

		/* Llamada al Stored Procedure de recuatificacion */

		Map<String, Object> out = null;
		try {
			out = recuotificacionService.generaProgramasPago(idToCotizacion,
					banderaRecuotifica, tipoMov, null);

			BigDecimal codigoRespuesta = (BigDecimal) out.get("pId_Cod_Resp");
			String descripcionRespuesta = (String) out.get("pDesc_Resp");

			if (codigoRespuesta != null) {
				LogDeMidasWeb.log("Codigo de respuesta del SP" + codigoRespuesta, Level.INFO, null);
			}
			if (descripcionRespuesta != null) {
				LogDeMidasWeb.log("Descripcion de respuesta del SP" + codigoRespuesta, Level.INFO, null);
			}

		} catch (Exception ex) {
			LogDeMidasWeb.log("Ocurrio un error en la llamada al SP recuatificacion: " + ex.getMessage(), Level.INFO, null);
			throw new RuntimeException(ex);
		}

		return SUCCESS;
	}

	@Action(value = "llenaListadoProgramaPagos", results = { @Result(name = SUCCESS, location = "/jsp/componente/programapago/programaPagoListadoGrid.jsp") })
	public String llenaListadoProgramaPagos() throws Exception {
		BigDecimal IdToCotizacion = this.getIdToCotizacion();
		List<ToProgPago> listProgPago;// = new ArrayList<ToProgPago>();
		listProgPago = programaPagoService.getProgramaPago(IdToCotizacion);

		this.setListProgPago(listProgPago);
		return SUCCESS;
	}

	@Action(value = "getlistaRecibos", results = { @Result(name = SUCCESS, location = "/jsp/componente/programapago/reciboListado.jsp") })
	public String getlistaRecibos() throws Exception {
		ContenedorDatosProgramaPago datos = new ContenedorDatosProgramaPago();
		datos = programaPagoService.obtenerDatosCliente(
				this.getIdToProgramaPago(), this.getIdToCotizacion());
		this.setNombreContratante(datos.getCliente().getNombreCompleto());
		this.setRfc(datos.getCliente().getRfcCobranza());
		this.setDireccionFiscal(datos.getCliente().getDireccionCompleta());

		return SUCCESS;
	}

	@Action(value = "llenaListadoRecibos", results = { @Result(name = SUCCESS, location = "/jsp/componente/programapago/reciboListadoGrid.jsp") })
	public String llenaListadoRecibos() throws Exception {
		BigDecimal IdToCotizacion = this.getIdToCotizacion();
		Long idToProgramaPago = this.getIdToProgramaPago();
		List<ToRecibo> listRecibo = programaPagoService.getRecibo(
				IdToCotizacion, idToProgramaPago);
		this.setListRecibo(listRecibo);
		return SUCCESS;
	}

	// @Action(value="getTotalesRealesPeriodoPago",
	// results={
	// @Result(name=SUCCESS, type =
	// "json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^saldo\\.impPrimaNeta,^saldo\\.impDerechos,^saldo\\.impBonComis,^saldo\\.impRcgosPagoFR,^saldo\\.impIVA,^saldo\\.impPrimaTotal"
	// })
	// })
	// public String getTotalesRealesPeriodoPago() throws Exception {
	// saldo =
	// programaPagoService.obtenerSaldoDiferencia(this.getIdToCotizacion());
	// return SUCCESS;
	// }
	//
	// @Action(value="getSaldoCotizacionActual",
	// results={
	// @Result(name=SUCCESS, type =
	// "json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^saldo\\.impPrimaNeta,^saldo\\.impDerechos,^saldo\\.impBonComis,^saldo\\.impRcgosPagoFR,^saldo\\.impIVA,^saldo\\.impPrimaTotal"
	// })
	// })
	// public String getSaldoCotizacionActual() throws Exception {
	// saldo = programaPagoService.obtenerSaldoActual(this.getIdToCotizacion());
	// return SUCCESS;
	// }

	@Action(value = "getSaldoDiferencia", results = { @Result(name = SUCCESS, type = "json", params = {
			"noCache",
			"true",
			"ignoreHierarchy",
			"false",
			"includeProperties",
			"^saldo\\.impPrimaNeta,^saldo\\.impDerechos,^saldo\\.impBonComis,^saldo\\.impRcgosPagoFR,^saldo\\.impIVA,^saldo\\.impPrimaTotal" }) })
	public String getSaldoDiferencia() throws Exception {
		saldo = programaPagoService.obtenerSaldoDiferencia(this
				.getIdToCotizacion());
		return SUCCESS;
	}

	@Action(value = "getSaldoPeriodoPago", results = { @Result(name = SUCCESS, type = "json", params = {
			"noCache",
			"true",
			"ignoreHierarchy",
			"false",
			"includeProperties",
			"^saldo\\.impPrimaNeta,^saldo\\.impDerechos,^saldo\\.impBonComis,^saldo\\.impRcgosPagoFR,^saldo\\.impIVA,^saldo\\.impPrimaTotal" }) })
	public String getSaldoPeriodoPago() throws Exception {
		saldo = programaPagoService.obtenerSaldoPeriodoPago(this
				.getIdToProgramaPago());
		return SUCCESS;
	}

	@Action(value = "getSaldoCotizacionActual", results = { @Result(name = SUCCESS, type = "json", params = {
			"noCache",
			"true",
			"ignoreHierarchy",
			"false",
			"includeProperties",
			"^saldo\\.impPrimaNeta,^saldo\\.impDerechos,^saldo\\.impBonComis,^saldo\\.impRcgosPagoFR,^saldo\\.impIVA,^saldo\\.impPrimaTotal" }) })
	public String getSaldoCotizacionActual() throws Exception {
		saldo = programaPagoService
				.obtenerSaldoActual(this.getIdToCotizacion());
		return SUCCESS;
	}

	@Action(value = "getSaldoCotizacionOriginal", results = { @Result(name = SUCCESS, type = "json", params = {
			"noCache",
			"true",
			"ignoreHierarchy",
			"false",
			"includeProperties",
			"^saldo\\.impPrimaNeta,^saldo\\.impDerechos,^saldo\\.impBonComis,^saldo\\.impRcgosPagoFR,^saldo\\.impIVA,^saldo\\.impPrimaTotal" }) })
	public String getSaldoCotizacionOriginal() throws Exception {
		saldo = programaPagoService.obtenerSaldoOriginal(this
				.getIdToCotizacion());
		return SUCCESS;
	}

	// @Action(value="guardarRecibo",
	// results={
	// @Result(name=SUCCESS,
	// location="/jsp/componente/programapago/reciboListado.jsp")
	// })
	// public String guardarRecibo() throws Exception{
	// try
	// {
	// List listBeans = null;
	// Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
	// //.setDateFormat("dd/MM/yyyy").create();
	// Type type = new TypeToken<ArrayList<ToRecibo>>(){}.getType();
	// ArrayList<ToRecibo> list = new ArrayList<ToRecibo>();
	// listBeans = gson.fromJson( this.listJsonRecibosEditada, type);
	// list.addAll( listBeans );
	// programaPagoService.guardar(list,this.getIdToCotizacion());
	// return SUCCESS;
	// }
	// catch (Exception je)
	// {
	// }
	//
	//
	// // programaPagoService.obtenerSaldoOriginal(this.getIdToCotizacion());
	// return SUCCESS;
	// }

	public void prepareDomicilio() {
		// Pais-Municipio-Ciudad
		setEstadoMap(listadoService.getMapEstados("PAMEXI"));

		if (contenedorDatosProgramaPago != null
				&& contenedorDatosProgramaPago.getDomicilioCliente() != null) {
			// Carga los estados para Mexico
			setEstadoMap(listadoService.getMapEstados("PAMEXI"));

			if (contenedorDatosProgramaPago.getDomicilioCliente()
					.getClaveEstado() != null) {
				setMunicipioMap(listadoService
						.getMapMunicipiosPorEstado(contenedorDatosProgramaPago
								.getDomicilioCliente().getClaveEstado()));
			} else {
				setMunicipioMap(new HashMap<String, String>());
			}
			if (contenedorDatosProgramaPago.getDomicilioCliente()
					.getClaveCiudad() != null) {
				setColoniasMap(listadoService
						.getMapColonias(contenedorDatosProgramaPago
								.getDomicilioCliente().getClaveCiudad()));
			} else {
				setColoniasMap(new HashMap<String, String>());
			}
		} else {
			setMunicipioMap(new HashMap<String, String>());
			setColoniasMap(new HashMap<String, String>());
		}
	}

	@Action(value = "modificaPersona", results = { @Result(name = SUCCESS, location = "/jsp/componente/programapago/modificaPersona.jsp") })
	public String modificaPersona() throws Exception {
		ToProgPago progPagoEdit = new ToProgPago();
		Long idContratanteProgPago = new Long(0);

		prepareDomicilio();

		personasAseguradas = programaPagoService.obtenerPersonasAseguradasCot(idToCotizacion);

		progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
		idContratanteProgPago = progPagoEdit.getIdContratante();
		
		if (idToProgramaPago != null) {
			progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
			idContratanteProgPago = progPagoEdit.getIdContratante();

			if (idContratanteProgPago != null) {
				progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
				cliente = new ClienteGenericoDTO();
				BigDecimal idCliente = new BigDecimal(progPagoEdit.getIdContratante());

				// Condicion para determinar la informacion del cliente que se
				// carga en la pantalla

				if (idClienteAsegurado != null) {
					String identificador = idClienteAsegurado.substring(0, 1);
					if (!identificador.equals("c") && !identificador.equals("e")) {
						cliente.setIdCliente(new BigDecimal(idClienteAsegurado));
						cliente = clienteFacade.loadById(cliente);
					//creamos cliente default
					}else{
						clienteCot = false;
					}
				} else {
					cliente.setIdCliente(idCliente);
					cliente = clienteFacade.loadById(cliente);
				}
			}
		}
		//banderaClienteCot = programaPagoService.validaClineteOriginalCot(idToCotizacion, cliente);
		if (cliente == null || cliente.getIdCliente() == null){
			clienteExistenteEnSeycos = false;
			banderaClienteCot = true;
			banderaClienteProgramaPago = false;
		}else{
			clienteExistenteEnSeycos = true;
			banderaClienteCot = programaPagoService.validaClineteOriginalCot(idToCotizacion, cliente);
			banderaClienteProgramaPago = this.esClienteDeProgramaPago(idContratanteProgPago, cliente.getIdCliente().longValue());
		}
		inicializarCampos();
		if (cliente != null && cliente.getIdCliente() != null) {
			try {
				// this.cliente=clienteFacade.loadById(cliente);
				consultarMediosPago();
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}
		/**/

		this.setPersonasRelacionadas(personasRelacionadas);
		return SUCCESS;
	}

	private boolean esClienteDeProgramaPago(Long idClienteProgramaPago, Long idClienteSeleccionado){
		return idClienteProgramaPago.longValue() == idClienteSeleccionado.longValue();
	}
	
	@Action(value = "guardarCliente", results = { @Result(name = SUCCESS, location = "/jsp/componente/programapago/modificaPersona.jsp") })
	public String guardarNuevoCliente() throws Exception {

		ToProgPago progPagoEdit = new ToProgPago();
		
		//if (programaPagoService.validaClineteOriginalCot(idToCotizacion,cliente)) {

			try {
				if (cliente != null) {
					if (!UtileriasWeb.esCadenaVacia(cliente.getIdClienteString())) {
						cliente.setIdCliente(new BigDecimal(cliente.getIdClienteString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getIdToPersonaString())) {
						cliente.setIdToPersona(new BigDecimal(cliente.getIdToPersonaString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getClaveTipoPersonaString())) {
						cliente.setClaveTipoPersona(Short.parseShort(cliente.getClaveTipoPersonaString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getIdEstadoNacimientoString())) {
						cliente.setIdEstadoNacimiento(new BigDecimal(cliente.getIdEstadoNacimientoString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getIdColoniaString())) {
						cliente.setIdColonia(new BigDecimal(cliente.getIdColoniaString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getIdMunicipioString())) {
						cliente.setIdMunicipio(new BigDecimal(cliente.getIdMunicipioString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getIdEstadoString())) {
						cliente.setIdEstado(new BigDecimal(cliente.getIdEstadoString()));
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getCodigoRFC())) {
						cliente.setCodigoRFCFiscal(cliente.getCodigoRFC());
					}
					if (!UtileriasWeb.esCadenaVacia(cliente.getTelefono())) {
						cliente.setTelefonoFiscal(cliente.getTelefono());
					}
				}

				// Verificar si se utiliza este codigo
				/*
				 * if("consulta".equals(tipoAccion)){ validaNegocio=true; }
				 */
				// CAMBIO se pone que siempre se valide el negocio
				boolean validaNegocio = false;
				cliente = clienteFacade.saveFullData(cliente, null, validaNegocio);

				// Actualizar tablas
				try {

					if (idToProgramaPago != null) {
						// Se actualiza el Programa de pago
						progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
						progPagoEdit.setIdContratante(cliente.getIdCliente().longValue());
						programaPagoService.guardar(progPagoEdit);
					}
				} catch (Exception ex) {
					mensageError = mensageError + " -->" + ex.getMessage();
				}

				try {
					String identificador = idClienteAsegurado.substring(0, 1);
					String idAutoIncisoCot = idClienteAsegurado.substring(1,idClienteAsegurado.length());
					
					if (identificador.equals("c") || identificador.equals("e")){
						//programaPagoService.actualizarAutoIncisoCot(idAutoIncisoCot, cliente);
						programaPagoService.actualizarClienteInciso(identificador, new BigDecimal(idAutoIncisoCot), cliente.getIdCliente(), cliente.getNombre());
					}
					else{
						LogDeMidasWeb.log("No es un cliente nuevo, no se actualiza el inciso", Level.INFO, null);
					}
					
				} catch (Exception ex) {
					LogDeMidasWeb.log("Ocurrio un error al actualizar el IncisoAutoCot de Pago: " + ex.getMessage(), Level.INFO, null);
					mensageError = mensageError + " -->" + ex.getMessage();
				}

			} catch (Exception ex) {
				LogDeMidasWeb.log("Ocurrio un error al guardar el cliente: " + ex.getMessage(), Level.INFO, null);
				recargarOnError();
				banderaClienteCot = true;
				errorGuardadoCliente = true;
				mensageError = "**El cliente no fue guardado exitosamente, complete la informacion de la forma y vuelva a guardar";
				mensageError = mensageError + " -->" + ex.getMessage();
			}

		//} else {
		//	System.out.println("No se puede modificar el cliente de la cotizacion original");
		//}
	    banderaClienteProgramaPago = true;
		recargarOnError();
		return SUCCESS;
	}
	
	
	@Action(value = "asignarClienteAProgramaPago", results = { @Result(name = SUCCESS, location = "/jsp/componente/programapago/modificaPersona.jsp") })
	public String asignarClienteAProgramaPago() throws Exception {

		ToProgPago progPagoEdit = new ToProgPago();
		
			try {
					progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
					progPagoEdit.setIdContratante(idCliente.longValue());
					programaPagoService.guardar(progPagoEdit);
					
					//buscamos el cliente para ponerlo en la forma
					cliente = new ClienteGenericoDTO();
					cliente.setIdCliente(idCliente);
					try {
						cliente = clienteFacade.loadById(cliente);
					} catch (Exception e) {
						LogDeMidasWeb.log("Excepcion al buscar al cliente del programa de pago: " + e.getMessage(), Level.WARNING, null);
					}
				
			} catch (Exception ex) {
				LogDeMidasWeb.log("Ocurrio un error al guardar el cliente: " + ex.getMessage(), Level.WARNING, null);
				recargarOnError();
				banderaClienteCot = true;
				errorGuardadoCliente = true;
				mensageError = "**El cliente no fue guardado exitosamente, complete la informacion de la forma y vuelva a guardar";
				mensageError = mensageError + " -->" + ex.getMessage();
			}
		clienteCot = true;
		banderaClienteProgramaPago = true;
		recargarOnError();
		return SUCCESS;
	}

	public void recargarOnError() throws Exception {

		ToProgPago progPagoEdit = new ToProgPago();
		Long idContratanteProgPago = new Long(0);

		prepareDomicilio();

		personasAseguradas = programaPagoService.obtenerPersonasAseguradasCot(idToCotizacion);

		if (idToProgramaPago != null) {
			progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
			idContratanteProgPago = progPagoEdit.getIdContratante();

			if (idContratanteProgPago != null) {
				progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
			}

		}
		
		banderaClienteCot = programaPagoService.validaClineteOriginalCot(idToCotizacion, cliente);
		
		inicializarCampos();
		if (cliente != null && cliente.getIdCliente() != null) {
			try {
				// this.cliente=clienteFacade.loadById(cliente);
				consultarMediosPago();
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}
		/**/

		this.setPersonasRelacionadas(personasRelacionadas);

	}

	private void inicializarCampos() {
		setFechaActual(UtileriasWeb.getFechaString(new Date()));
		cargarPaises();
		// cargarEstados((cliente!=null)?cliente.getIdPaisString():null);
		if (cliente == null || (cliente != null && (cliente.getClaveNacionalidad() == null || cliente.getClaveNacionalidad().isEmpty()))) {
			
			//Si el cliente es el de la cotizacion, lo buscamos en el programa de pago
			if (clienteCot){
			
				ToProgPago progPagoEdit = programaPagoService.getProgramaPago(idToProgramaPago);
				Long idContratanteProgPago = progPagoEdit.getIdContratante();
				cliente = new ClienteGenericoDTO();
				cliente.setIdCliente(new BigDecimal(idContratanteProgPago));
				try {
					cliente = clienteFacade.loadById(cliente);
				} catch (Exception e) {
					LogDeMidasWeb.log("Excepcion al buscar al cliente del programa de pago: " + e.getMessage(), Level.WARNING, null);
				}
			}
			//si no, creamos default
			else{
				if (cliente == null) {
					cliente = new ClienteGenericoDTO();
					cliente.setEstadoCivil("S"); // Opcion default para clientes nuevos.
				}
				cliente.setClaveNacionalidad("PAMEXI");
				cliente.setIdPaisString("PAMEXI");
				cliente.setTipoSituacionString("A");
			}
		}
		sectores = sectorFacade.findAll();
		estadosCiviles = estadoCivilFacade.findAll();
		giros = catalogoGiroFacade.findAll();
		bancos = bancoEmisorFacade.findAll();
		mediosPagoDomiciliacion = tipoDomiciliacionFacade.findAll();
	}

	private void cargarPaises() {
		paises = paisFacade.findAll();
	}

	public void consultarMediosPago() {
		if (cliente != null && cliente.getIdCliente() != null) {
			try {
				mediosPagoCliente = clienteFacade.loadMediosPagoPorCliente(
						cliente, null);
			} catch (Exception e) {
				setMensajeError(e.getMessage());
			}
		}

	}

	/**
	 * Manda a exportar en un documento PDF la información de la condición
	 * especial
	 * 
	 * @return
	 */
	@Action(value = "imprimirProgramaPagos", results = { @Result(name = SUCCESS, type = "stream", params = {
			"contentType", "${contentType}", "inputName", "genericInputStream",
			"contentDisposition", "attachment;filename=\"${fileName}\"" }) })
	public String imprimirProgramaPagos() {
		try {
			TransporteImpresionDTO transporte = programaPagoService
					.imprimirProgramaPagos(this.getIdToCotizacion());
			genericInputStream = new ByteArrayInputStream(
					transporte.getByteArray());
			contentType = "application/pdf";
			// CondicionEspecial condicion =
			// programaPagoService.obtenerCondicion(this.getIdToCotizacion());

			fileName = "ProgramaPagos_"
					+ UtileriasWeb.getFechaString(new Date()) + ".pdf";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	/**
	 * Action para hacer el switch de las pantallas de persona fisica a moral
	 * segun sea el caso
	 * 
	 * @return
	 */
	@Action(value = "switchTipoPersona", results = {
			@Result(name = "personaFisica", location = "/jsp/cliente/catalogoClientes/detalleClienteTabsPersonaFisica.jsp"),
			@Result(name = "personaMoral", location = "/jsp/cliente/catalogoClientes/detalleClienteTabsPersonaMoral.jsp") })
	public String switchTipoPersona() {
		String accion = "personaFisica";
		Short claveTipoPersona = null;
		if (tipoPersona == 2) {
			accion = "personaMoral";
			claveTipoPersona = new Short("2");
		} else {
			claveTipoPersona = new Short("1");
		}
		// if(cliente!=null){
		// cliente.setClaveTipoPersona(claveTipoPersona);
		// }
		if (cliente != null) {
			cliente.setClaveTipoPersona(claveTipoPersona);
		}

		// inicializarCampos();
		return accion;
	}

	
	@Action(value = "cargarProgramaPagoOriginal", results = {			
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "cargarProgramaPago", "namespace",
					"/componente/programapago",
					"idToCotizacion","${idToCotizacion}",
					"readOnly","${readOnly}",
					"tipoMov", "${tipoMov}",
					"idToSolicitud","${idToSolicitud}",
					"idToPoliza","${idToPoliza}",
					"tipoMov", "${tipoMov}",
					"idContinuity","${idContinuity}",
					"tipoFormaPago","${tipoFormaPago}",
					"numeroEndoso","${numeroEndoso}",
					"mensaje","${mensaje}"}),
			@Result(name = INPUT, location = "/jsp/componente/programapago/programaPagoGrid.jsp") })
	public String cargarProgramaPagoOriginal(){		
		recuotificacionService.generaProgramasPago(idToCotizacion, RecuotificacionService.AccionRecuotificacion.RECUOTIFICA_A_ORIGINAL.ordinal(),tipoMov,idToSolicitud);
		recuotificacionService.marcarCotizacionRecuotificada(idToCotizacion, RecuotificacionService.TipoRecuotificacion.MANUAL, Boolean.FALSE);
		return SUCCESS;
	}
	
	@Action(value = "cargarProgramaPago", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/programaPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String cargarProgramaPago() throws Exception {
		programaPagoForm = new ProgramaPago();
	    CotizacionDTO cotizacion;  
	    
	    programaPagoForm.setReadOnly(new Boolean(readOnly));
	    programaPagoForm.setIdToSolicitud(idToSolicitud != null ? idToSolicitud : BigDecimal.ZERO);
		programaPagoForm.setIdToPoliza(idToPoliza != null ? idToPoliza : BigDecimal.ZERO);
		programaPagoForm.setIdContinuity(idContinuity != null ? idContinuity : BigDecimal.ZERO);
		programaPagoForm.setNumeroEndoso(numeroEndoso != null ? numeroEndoso : BigDecimal.ZERO);
		programaPagoForm.setTipoMov(tipoMov);
		

		//SI ES UN ENDOSO
		if (tipoMov != null && tipoMov.equals("E")) {
			LOG.debug("Id de Solicitud: " + idToSolicitud);
			LOG.debug("Id poliza: " + idToPoliza);
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
			
			if (polizaDTO != null){
				LOG.debug("Poliza no nula");
				cotizacion = polizaDTO.getCotizacionDTO();
				if (cotizacion != null){
					LOG.debug("Cotizacion no nula");
					LOG.debug("Id de cotizacion: " + cotizacion.getIdToCotizacion());
					idToCotizacion = cotizacion.getIdToCotizacion(); 
				}
			}
			
			if (idToCotizacion == null){
				LOG.debug("Id de cotizacion nula, seteando id de solicitud");
				idToCotizacion = idToSolicitud;
			}
			
			cotizacion = cotizacionService.obtenerCotizacionPorId(idToCotizacion);;
			programaPagoForm.setIdToCotizacion(idToCotizacion);		
			programaPagoForm.setCotizacionDTO(cotizacion);
			
			
			recuotificacionService.generaProgramasPago(idToCotizacion, RecuotificacionService.AccionRecuotificacion.RECUOTIFICA_A_ORIGINAL.ordinal(), tipoMov,idToSolicitud);
			
		
		}else if (tipoMov != null && tipoMov.equals("AP")){
			LOG.debug("INICIO DEL METODO CARGAR PROGRAMA PAGO AJUSTE DE PRIMA");
			
			LOG.debug("Id de continuidad: " + idContinuity);
			LOG.debug("Tipo Forma de Pago: " + tipoFormaPago);
			LOG.debug("Id Poliza: " + idToPoliza);
			LOG.debug("Numero Endoso: " + numeroEndoso);
			programaPagoForm.setIdContinuity(idContinuity);
			programaPagoForm.setNumeroEndoso(numeroEndoso);
			
			PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
			cotizacion = polizaDTO.getCotizacionDTO();
			idToCotizacion = cotizacion.getIdToCotizacion(); 	
			programaPagoForm.setIdToCotizacion(idToCotizacion);

			//1: Igual poliza, 2: Anual
			BigDecimal tfp = BigDecimal.ZERO;
			if (tipoFormaPago.trim().equals("1")){
				tfp = new BigDecimal(tipoFormaPago);
			}
			programaPagoForm.setTipoFormaPago(tfp.toString());
			try {
				Map<String, Object> out = recuotificacionService.generaProgramasPagoEndosoAP(idContinuity, tfp, banderaRecuotifica.toString(), numeroEndoso);
				BigDecimal idsol = (BigDecimal) out.get("outIdEjecucion");
				programaPagoForm.setIdToSolicitud(idsol);
				BigDecimal codigoRespuesta = (BigDecimal) out.get("outCodResp");
				String descripcionRespuesta = (String) out.get("outDesResp");
				if (codigoRespuesta != null) {
					LogDeMidasWeb.log("Codigo de respuesta del SP Endoso" + codigoRespuesta, Level.INFO, null);
				}else{
					LogDeMidasWeb.log("Codigo de respuesta nulo", Level.INFO, null);
				}
				if (descripcionRespuesta != null) {
					LogDeMidasWeb.log("Descripcion de respuesta del SP Endoso" + codigoRespuesta, Level.INFO, null);
				}else{
					LogDeMidasWeb.log("Descripcion de respuesta nulo", Level.INFO, null);
				}
			} catch (Exception ex) {
				LogDeMidasWeb.log("Ocurrio un error en la llamada al SP recuatificacion Endoso: " + ex.getMessage(), Level.WARNING, null);
				return INPUT;
			}
		
	    //SI ES UNA POLIZA NORMAL
		} else {
			
			cotizacion = cotizacionService.obtenerCotizacionPorId(idToCotizacion);;
			programaPagoForm.setIdToCotizacion(idToCotizacion);		
			programaPagoForm.setCotizacionDTO(cotizacion);
			
			LOG.debug("INICIO DEL METODO CARGAR PROGRAMA PAGO");
			LOG.debug("Valor de tipoMov" + tipoMov);
			banderaRecuotManual=true;
			
		}
		
		NegocioRecuotificacion negRec = negocioRecuotificacionService.obtenerNegocioRecuotificacion(
				cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		if(negRec != null){
			programaPagoForm.setHasEditPrimaTotalPermit(negRec.getActivarModificacionPrimaTotal());
			programaPagoForm.setAjustePermitidoPrimaTotal(negRec.getMontoPrimaTotal());											
			tieneRolRecuotificacion = 
				negocioRecuotificacionService.validarPermisoRecuotificacionManual(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), 
						Long.valueOf(usuarioService.getUsuarioActual().getId()));
		}
		programaPagoForm.setTieneRolRecuotificacion(tieneRolRecuotificacion);
		
		return SUCCESS;
	}

	// METODO QUE REALIZA LOS CALCULOS DE LOS VALORES TOTALES EN BASE A LA
	// LOGICA IMPLEMENTADA EN CLASE POLIZADN ANTES DE EMITIR LA POLIZA
	public CotizacionDTO calcularTotalesDeCotizacion(BigDecimal idCotizacion) {

		LOG.debug("Iniciando metodo de calculo de totales de cotizacion");
		CotizacionDTO dto = new CotizacionDTO();

		dto = cotizacionService.obtenerCotizacionPorId(idCotizacion);
	
		if (dto != null) {

			LOG.debug("Objeto cotizacion no nulo");

			List<CoberturaCotizacionDTO> coberturasContratadas = null;
			try {
				coberturasContratadas = CoberturaCotizacionDN.getInstancia().listarCoberturasContratadas(dto.getIdToCotizacion());
			} catch (SystemException e) {			
				LogDeMidasWeb.log("Ocurrio un error al obtener el listado de coberturas contratadas: " + e.getMessage() + e.getMessage(), Level.WARNING, null);
			}


			try {
				ResumenCotizacionDN.getInstancia().calcularTotalesResumenCotizacion(dto,coberturasContratadas);
			} catch (SystemException e) {
				
				LogDeMidasWeb.log("Excepcion al calcular los totales de resumen de cotizacion: " + e.getMessage(), Level.INFO, null);
			}

			calculoService.obtenerResumenCotizacion(dto, false);

			dto.setPorcentajePagoFraccionado(calculoService
					.getPorcentajeRecargoFraccionado(dto).doubleValue());
			// dto.setPorcentajePagoFraccionado(resumenCostos.getRecargo());

			dto.setPorcentajeIva(dto.getFactorIVA());

			LOG.debug("Valores calculados. Resultados: ");
			LOG.debug("id Cotizacion: "
					+ (dto.getIdToCotizacion() == null ? "idCot Nulo" : dto
							.getIdToCotizacion().toString()));
			LOG.debug("Dias por devengar: "
					+ (dto.getDiasPorDevengar() == null ? "diasPorDevengar nulo"
							: dto.getDiasPorDevengar().toString()));
			LOG.debug("Derechos Poliza: "
					+ (dto.getDerechosPoliza() == null ? "derechosPoliza Nulo"
							: dto.getDerechosPoliza().toString()));
			LOG.debug("% Pago Frac: "
					+ (dto.getPorcentajePagoFraccionado() == null ? "%pagoFrac Nulo"
							: dto.getPorcentajePagoFraccionado().toString()));
			LOG.debug("Codigo usuario Mod: "
					+ (dto.getCodigoUsuarioModificacion() == null ? "usuarioMod Nulo"
							: dto.getCodigoUsuarioModificacion()));
			LOG.debug("Folio poliza asoc: "
					+ (dto.getFolioPolizaAsociada() == null ? "Folio pol asociada Nulo"
							: dto.getFolioPolizaAsociada().toString()));

		} else {
			LOG.debug("Objeto cotizacion nulo, retornando valores default");
			dto = this.getCotizacionDTOInfo(idCotizacion);
		}
		return dto;

	}

	// METODO PARA CARGAR INFORMACION DE DTO DE PRUEBA
	private CotizacionDTO getCotizacionDTOInfo(BigDecimal idCotizacion) {
		CotizacionDTO dto = new CotizacionDTO();

		dto.setIdToCotizacion(idCotizacion);
		dto.setDiasPorDevengar(365D);
		dto.setDerechosPoliza(new Double("0"));
		dto.setPorcentajePagoFraccionado(new Double("0"));
		dto.setCodigoUsuarioModificacion("0");
		dto.setFolioPolizaAsociada(null);

		return dto;
	}

	@Action(value = "obtenerProgramasDePagoJSON", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerProgramasDePagoJSON() throws SystemException,
			IOException {

		Boolean ro = new Boolean(readOnly);
		resList = programaPagoService.getProgramaPago(idToCotizacion);
		
		respuestaJason = ToProgPagoGraper.encapsulaToProgpago(resList,
				idToCotizacion, ro, tipoMov, idToSolicitud, idToPoliza,
				idContinuity, tipoFormaPago, numeroEndoso );

		// convert String into InputStream
		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;

		return SUCCESS;
	}

	// METODO PARA OBTENER JSON CON SALDO DE PAGO ORIGINAL
	@Action(value = "obtenerSaldoOriginal", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerSaldoOriginal() {

		// Variables que indica el numero de Inciso
		Long numInciso = null;

		CotizacionDTO dtoToEmit = null;
	
		dtoToEmit = cotizacionService.obtenerCotizacionPorId(idToCotizacion);
        
        LOG.debug("Valor de porcentaje de pago fraccionado: " + dtoToEmit.getPorcentajePagoFraccionado());

		if (idProgPago != null) {
			ToProgPago progPagoFuente = entidadService.findById(ToProgPago.class, new Long(idProgPago));
			numInciso = progPagoFuente.getNumInciso().longValue();
		}

		Saldo saldoOriginal = programaPagoService.obtenerSaldoOriginalPorInciso(dtoToEmit, null, numInciso);

		saldoOriginal.setPorcentajePagoFraccionado(new BigDecimal(dtoToEmit.getPorcentajePagoFraccionado().toString()));

	
		List<Saldo> saldoList = new ArrayList<Saldo>();
		saldoList.add(saldoOriginal);
		String json = this.getSaldoJson(saldoOriginal);

		respuestaJasonSaldoOriginal = json.toString();
		
		// convert String into InputStream
		InputStream is = new ByteArrayInputStream(
				respuestaJasonSaldoOriginal.getBytes());
		stream = is;

		return SUCCESS;

	}

	// METODO INTERNO QUE REGRESA CADENA JSON A PARTIR DE DTO DE SALDO
	private String getSaldoJson(Saldo dto) {
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"impPrimaNeta\"").append(":").append("\"")
				.append(dto.getImpPrimaNeta().toString()).append("\",");
		json.append("\"impDerechos\"").append(":").append("\"")
				.append(dto.getImpDerechos().toString()).append("\",");
		json.append("\"impRcgosPagoFR\"").append(":").append("\"")
				.append(dto.getImpRcgosPagoFR().toString()).append("\",");
		json.append("\"impIVA\"").append(":").append("\"")
				.append(dto.getImpIVA().toString()).append("\",");
		json.append("\"porcentajePagoFraccionado\"").append(":").append("\"")
				.append(dto.getPorcentajePagoFraccionado().toString())
				.append("\",");
		json.append("\"impPrimaTotal\"").append(":").append("\"")
				.append(dto.getImpPrimaTotal().toString()).append("\"");

		json.append("}");

		return json.toString();
	}

	// METODO PARA OBTENER JSON CON SALDO DE PERIODO DE PAGO
	@Action(value = "obtenerSaldoPeriodoDePago", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerSaldoPeriodoDePago() throws SystemException,
			IOException {

		Saldo saldoOriginal = programaPagoService
				.obtenerSaldoPeriodoPago(idToCotizacion);

		String json = this.getSaldoJson(saldoOriginal);
		respuestaJasonSaldoOriginal = json.toString();

		InputStream is = new ByteArrayInputStream(
				respuestaJasonSaldoOriginal.getBytes());
		stream = is;
		return SUCCESS;

	}

	// METODO PARA OBTENER JSON CON SALDO DE PERIODO DE PAGO
	@Action(value = "obtenerSaldoProgPagoIn", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerSaldoProgPagoIn() throws SystemException, IOException {

		Saldo saldoOriginal = programaPagoService.obtenerSaldoProgoPagoIn(new BigDecimal(idProgPago));

		String json = this.getSaldoJson(saldoOriginal);
		respuestaJasonSaldoOriginal = json.toString();

		InputStream is = new ByteArrayInputStream(respuestaJasonSaldoOriginal.getBytes());
		stream = is;
		return SUCCESS;

	}

	// METODO PARA OBTENER JSON CON SALDO DE PERIODO DE PAGO
	// Metodo que trae los totales del inciso
	@Action(value = "getSaldoRecibosIn", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String getSaldoRecibosIn() throws SystemException, IOException {

		if (idProgPago != null) {

			Saldo saldoRecibosInciso = programaPagoService.obtenerSaldoRecibosIn(new BigDecimal(idProgPago));
			String json = this.getSaldoJson(saldoRecibosInciso);
			respuestaJason = json.toString();

		}

		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;
		return SUCCESS;

	}

	@Action(value = "obtenerSaldoDeDiferenciaInciso", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerSaldoDeDiferenciaInciso() throws SystemException,
			IOException {

		Saldo saldoDif = programaPagoService.obtenerSaldoDiferenciaInciso(new BigDecimal(idProgPago));
		String json = this.getSaldoJson(saldoDif);
		respuestaJason = json.toString();

		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;
		return SUCCESS;
	}
	
	@Action(value = "validarTotalesCotizacion", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String validarTotalesCotizacion() throws SystemException,
			IOException {

		Map<String, Object> res = recuotificacionService.validaTotalesDeCotizacionSalir(idToCotizacion);
		BigDecimal resultado = (BigDecimal) res.get("outIdEjecucion");
		
		StringBuilder myJson = new StringBuilder();
		myJson.append("{");
		myJson.append("\"resultado\"").append(":").append("\"")
				.append(resultado.toString()).append("\"");
		myJson.append("}");
		
		InputStream is = new ByteArrayInputStream(myJson.toString().getBytes());
		stream = is;
		return SUCCESS;
	}

	@Action(value = "obtenerSaldoDeDiferencia", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerSaldoDeDiferencia() throws SystemException,
			IOException {

		CotizacionDTO dtoToEmit = null;
		if (programaPagoForm == null
				|| programaPagoForm.getCotizacionDTO() == null) {
			dtoToEmit = this.calcularTotalesDeCotizacion(idToCotizacion);
		}

		else {
			dtoToEmit = programaPagoForm.getCotizacionDTO();
		}

		Saldo saldoDif = programaPagoService.obtenerSaldoDiferencia(dtoToEmit);
		String json = this.getSaldoJson(saldoDif);
		respuestaJason = json.toString();

		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;
		return SUCCESS;
	}

	
	/**
	 * @author User MGGE
	 * @return
	 */
	// IMPLEMENTACION ACTION PROGRAMA PAGO
	@Action(value = "mostrarRecibos", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/recibosGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String mostrarRecibos() {

		BigDecimal id = idToCotizacion;
		programaPagoForm = new ProgramaPago();

		CotizacionDTO cotDTO = cotizacionService.obtenerCotizacionPorId(id);
		
		
		ToProgPago programaPago = programaPagoService.getProgramaPago(new BigDecimal(idProgPago).longValue());
		LOG.debug("ID CONTRATANTE DE PROGRAMA DE PAGO" + programaPago.getIdContratante());
		boolean tieneContteProgPago=programaPago.getIdContratante()==null?false:true;
		BigDecimal idContratanteProgPago=null;
		if(tieneContteProgPago)
			idContratanteProgPago = new BigDecimal(programaPago.getIdContratante());
		if (cotDTO != null) {
			BigDecimal idContratante = cotDTO.getIdToPersonaContratante();

			LOG.debug("Id contratante: " + idContratante);
			getClienteDTO(idContratanteProgPago);
			
			//20170104 Permiso recuotificacion manual
			NegocioRecuotificacion negRec = negocioRecuotificacionService.obtenerNegocioRecuotificacion(
					cotDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
			if(negRec != null){
				programaPagoForm.setHasEditPrimaTotalPermit(negRec.getActivarModificacionPrimaTotal());
				programaPagoForm.setAjustePermitidoPrimaTotal(negRec.getMontoPrimaTotal());											
				tieneRolRecuotificacion = 
					negocioRecuotificacionService.validarPermisoRecuotificacionManual(cotDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), 
							Long.valueOf(usuarioService.getUsuarioActual().getId()));
			}	

		} else {
			programaPagoForm.setNombreDelContrante("");
			programaPagoForm.setRfc("");
			programaPagoForm.setDireccionFiscal("");
		}

		programaPagoForm.setIdToCotizacion(id);
		programaPagoForm.setIdProgPago(new Long(idProgPago));
		programaPagoForm.setReadOnly(new Boolean(readOnly));
		
		programaPagoForm.setTipoMov(tipoMov);
		programaPagoForm.setIdToSolicitud(idToSolicitud);
		programaPagoForm.setIdToPoliza(idToPoliza);
		
		programaPagoForm.setIdContinuity(idContinuity);
		programaPagoForm.setTipoFormaPago(tipoFormaPago);
		programaPagoForm.setNumeroEndoso(numeroEndoso);
			
		programaPagoForm.setTieneRolRecuotificacion(tieneRolRecuotificacion);
		return SUCCESS;
	}
	
	private ClienteDTO getClienteDTO(BigDecimal idContratanteProgPago){
		ClienteDTO clienteDTO;
		try {

			clienteDTO = ClienteDN.getInstancia().verDetalleCliente(idContratanteProgPago, "");
			

			if (clienteDTO != null) {
				programaPagoForm.setNombreDelContrante((clienteDTO
						.obtenerNombreCliente() == null ? "" : clienteDTO
						.obtenerNombreCliente()));
				programaPagoForm.setRfc((clienteDTO.getCodigoRFC() == null ? ""
						: clienteDTO.getCodigoRFC()));
				programaPagoForm.setDireccionFiscal((clienteDTO
						.obtenerDireccionCliente() == null ? "" : clienteDTO
						.obtenerDireccionCliente()));
			} else {
				programaPagoForm.setNombreDelContrante("");
				programaPagoForm.setRfc("");
				programaPagoForm.setDireccionFiscal("");
			}

		} catch (SystemException e) {
			LogDeMidasWeb.log("Error al obtener el ClienteDTO: " + e.getMessage(), Level.WARNING, null);
			clienteDTO = null;
			e.printStackTrace();
		}
		return clienteDTO;
		
	}

	@Action(value = "obtenerRecibosJSON", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerRecibosJSON() throws SystemException, IOException {

		Format fmt = new SimpleDateFormat("dd/MM/yyyy");
		Boolean ro = new Boolean(readOnly);
		List<ToRecibo> resList = programaPagoService.getRecibo(idToCotizacion, // Verificar
																				// la
																				// logica
																				// interna
				new Long(idProgPago));

		Collections.sort(resList, new Comparator<ToRecibo>() {
			public int compare(ToRecibo s1, ToRecibo s2) {
				return (s1.getNumExhibicion() > s2.getNumExhibicion() ? 1 : -1);
			}
		});

		MidasJsonBase json = new MidasJsonBase();
		for (ToRecibo e : resList) {
			MidasJsonRow row = new MidasJsonRow();
			row.setId(e.getId().toString());
			row.setDatos(e.getNumExhibicion().toString(), e.getCveRecibo(), fmt
					.format(e.getFechaVencimiento()), fmt.format(e
					.getFechaCubreDesde()), fmt.format(e.getFechaCubreHasta()),
					e.getImpPrimaNeta().toString(), e.getImpRcgosPagoFR()
							.toString(), e.getImpDerechos().toString(), e
							.getImpIVA().toString(), e.getImpPrimaTotal()
							.toString(), this.getReciboButton(e.getId(), ro),
					this.rowControlButton(e.getId(), ro));
			json.addRow(row);
		}
		respuestaJason = json.toString();
		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;

		return SUCCESS;
	}

	@Action(value = "obtenerSumatoriaDeUnProgramaPago", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String obtenerSumatoriaDeUnProgramaPago() {

		Saldo saldoOriginal = programaPagoService
				.obtenerSaldoPeriodoPago(new Long(idProgPago));
		String json = this.getSaldoJson(saldoOriginal);

		respuestaJason = json.toString();
		InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
		stream = is;

		return SUCCESS;

	}

	private String getReciboButton(Long recordId, boolean ro) {
		StringBuilder botones = new StringBuilder();

		// Si no es read only, agregamos el boton de eliminar
		if (!ro) {
			botones.append("<a href=\'javascript: void(0);\' "
					+ "onclick=\'eliminarRecibo("
					+ recordId
					+ ")\'>"
					+ "<img border=\'0px\' alt=\'Eliminar Recibo\' title=\'Eliminar Recibo\' src=\'/MidasWeb/img/common/b_delete.gif\'/></a>");
		}
		return botones.toString();
	}

	//TODO FIx this
	private String rowControlButton(Long recordId, boolean ro) {
		if (true) {
			return "</br>";
		} else {
			return "<a href='javascript: void(0)' onclick='javascript: marcarRow("
					+ recordId
					+ ")' ><img alt='Registro No Editado' title='Registro No Editado' src='/MidasWeb/img/common/b_ok.jpg' border='0px' height='15' width='15'></a>";
		}
	}

	@Action(value = "guardarRecibo", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String guardarRecibo() {

		String json = listJsonRecibosEditada;

		List<ReciboVO> dtoList = null;
		Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
		Type type = new TypeToken<ArrayList<ReciboVO>>() {
		}.getType();
		dtoList = gson.fromJson(json, type);

		ArrayList<ToRecibo> list = new ArrayList<ToRecibo>();

		for (ReciboVO e : dtoList) {
			ToRecibo recibo = new ToRecibo();
			recibo.setId(e.getId());
			recibo.setNumExhibicion(e.getNumExhibicion());
			recibo.setCveRecibo(e.getCveRecibo());
			recibo.setFechaVencimiento(e.getFechaVencimiento());
			recibo.setFechaCubreDesde(e.getFechaCubreDesde());
			recibo.setFechaCubreHasta(e.getFechaCubreHasta());
			recibo.setImpPrimaNeta(e.getImpPrimaNeta());
			recibo.setImpRcgosPagoFR(e.getImpRcgosPagoFR());
			recibo.setImpDerechos(e.getImpDerechos());
			recibo.setImpIVA(e.getImpIVA());
			recibo.setImpPrimaTotal(e.getImpPrimaTotal());

			list.add(recibo);
		}

		programaPagoService.guardar(list, new Long(idProgPago));
		stream = new ByteArrayInputStream("".getBytes());

		return SUCCESS;
	}

	@Action(value = "agregarProgramaPago", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String agregarProgramaPago() throws SystemException, IOException {

		ToProgPago dto = new ToProgPago();
		CotizacionDTO cot = new CotizacionDTO();

		if (impPrimaNeta != null) {
			// dto.setImpPrimaNeta();
			dto.setImpPrimaNetaDiferencia(new BigDecimal(impPrimaNeta));
		}
		if (impDerechos != null) {
			dto.setImpDerechos(new BigDecimal(impDerechos));
		}
		if (impRecargos != null) {
			dto.setImpRcgosPagoFR(new BigDecimal(impRecargos));
		}
		if (impIva != null) {
			dto.setImpIVA(new BigDecimal(impIva));
		}
		if (impPrimaTotal != null) {
			dto.setImpPrimaTotal(new BigDecimal(impPrimaTotal));
		}
		if (idProgPago != null) {
			dto.setId(new Long(idProgPago));
		}
		if (id != null) {
			cot.setIdToCotizacion(new BigDecimal(id));
			dto.setCotizacion(cot);
		}
		dto.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual()
				.getNombreUsuario());

		List<ToRecibo> recibos = programaPagoService.getRecibo(dto
				.getCotizacion().getIdToCotizacion(), dto.getId());

		dto.setRecibos(recibos);
		
		BigDecimal tipoDeFormaDePago = BigDecimal.ONE;
		//Si es un ajuste de prima, validamos el valor de forma de pago
		if (tipoMov.equals("AP")){
			//Si es una, la forma de pago es igual a la poliza
			if (!(tipoFormaPago.equals("1"))){
				tipoDeFormaDePago = BigDecimal.ZERO;
			}
		}

		Map<String, String> res = programaPagoService.agregarProgramaPago(dto, tipoDeFormaDePago);

		if (res.get("res").equals("30")) {
			LOG.debug("Ejecucion del procedimiento exitosa...");
		} else {
			LOG.warn("Error el la ejecucion del proceso: " + res.get("resDesc"));
		}

		stream = new ByteArrayInputStream("".getBytes());

		return SUCCESS;

	}

	@Action(value = "eliminarProgramaPago", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public void eliminarProgramaPago() throws NumberFormatException,
			SystemException {

		programaPagoService.eliminarProgPago(new Long(idProgPago));

	}

	/**
	 * 
	 * ENDOSOS
	 * 
	 * */

	@Action(value = "cargarProgramaPagoEndoso", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/programaPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String cargarProgramaPagoEndoso() throws Exception {
		return SUCCESS;
	}

	public String getImpPrimaNeta() {
		return impPrimaNeta;
	}

	public void setImpPrimaNeta(String impPrimaNeta) {
		this.impPrimaNeta = impPrimaNeta;
	}

	public String getImpDerechos() {
		return impDerechos;
	}

	public void setImpDerechos(String impDerechos) {
		this.impDerechos = impDerechos;
	}

	public String getImpRecargos() {
		return impRecargos;
	}

	public void setImpRecargos(String impRecargos) {
		this.impRecargos = impRecargos;
	}

	public String getImpIva() {
		return impIva;
	}

	public void setImpIva(String impIva) {
		this.impIva = impIva;
	}

	public String getImpPrimaTotal() {
		return impPrimaTotal;
	}

	public void setImpPrimaTotal(String impPrimaTotal) {
		this.impPrimaTotal = impPrimaTotal;
	}

	public String getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public List<ToProgPago> getListProgPago() {
		return listProgPago;
	}

	public void setListProgPago(List<ToProgPago> listProgPago) {
		this.listProgPago = listProgPago;
	}

	public List<ToRecibo> getListRecibo() {
		return listRecibo;
	}

	public void setListRecibo(List<ToRecibo> listRecibo) {
		this.listRecibo = listRecibo;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Long getIdToProgramaPago() {
		return idToProgramaPago;
	}

	public void setIdToProgramaPago(Long idToProgramaPago) {
		this.idToProgramaPago = idToProgramaPago;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}

	public String getListJsonRecibosEditada() {
		return listJsonRecibosEditada;
	}

	public void setListJsonRecibosEditada(String listJsonRecibosEditada) {
		this.listJsonRecibosEditada = listJsonRecibosEditada;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getDireccionFiscal() {
		return direccionFiscal;
	}

	public void setDireccionFiscal(String direccionFiscal) {
		this.direccionFiscal = direccionFiscal;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Map<String, String> getEstadoMap() {
		return estadoMap;
	}

	public void setEstadoMap(Map<String, String> estadoMap) {
		this.estadoMap = estadoMap;
	}

	public Map<String, String> getMunicipioMap() {
		return municipioMap;
	}

	public void setMunicipioMap(Map<String, String> municipioMap) {
		this.municipioMap = municipioMap;
	}

	public Map<String, String> getColoniasMap() {
		return coloniasMap;
	}

	public void setColoniasMap(Map<String, String> coloniasMap) {
		this.coloniasMap = coloniasMap;
	}

	public Map<String, String> getPaisesMap() {
		return paisesMap;
	}

	public void setPaisesMap(Map<String, String> paisesMap) {
		this.paisesMap = paisesMap;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public ContenedorDatosProgramaPago getContenedorDatosProgramaPago() {
		return contenedorDatosProgramaPago;
	}

	public void setContenedorDatosProgramaPago(
			ContenedorDatosProgramaPago contenedorDatosProgramaPago) {
		this.contenedorDatosProgramaPago = contenedorDatosProgramaPago;
	}

	public Integer getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(Integer tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public List<String> getPersonasRelacionadas() {
		return personasRelacionadas;
	}

	public void setPersonasRelacionadas(List<String> personasRelacionadas) {
		this.personasRelacionadas = personasRelacionadas;
	}

	public RecuotificacionService getRecuotificacionService() {
		return recuotificacionService;
	}

	@Autowired
	@Qualifier("recuotificacionServiceEJB")
	public void setRecuotificacionService(RecuotificacionService recuotificacionService) {
		this.recuotificacionService = recuotificacionService;
	}

	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	public List<ClienteGenericoDTO> getMediosPagoCliente() {
		return mediosPagoCliente;
	}

	public void setMediosPagoCliente(List<ClienteGenericoDTO> mediosPagoCliente) {
		this.mediosPagoCliente = mediosPagoCliente;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public List<PaisDTO> getPaises() {
		return paises;
	}

	public void setPaises(List<PaisDTO> paises) {
		this.paises = paises;
	}

	public List<SectorDTO> getSectores() {
		return sectores;
	}

	public void setSectores(List<SectorDTO> sectores) {
		this.sectores = sectores;
	}

	@Autowired
	@Qualifier("sectorFacadeRemoteEJB")
	public void setSectorFacade(SectorFacadeRemote sectorFacade) {
		this.sectorFacade = sectorFacade;
	}

	public List<EstadoCivilDTO> getEstadosCiviles() {
		return estadosCiviles;
	}

	public void setEstadosCiviles(List<EstadoCivilDTO> estadosCiviles) {
		this.estadosCiviles = estadosCiviles;
	}

	@Autowired
	@Qualifier("estadoCivilFacadeRemoteEJB")
	public void setEstadoCivilFacade(EstadoCivilFacadeRemote estadoCivilFacade) {
		this.estadoCivilFacade = estadoCivilFacade;
	}

	public List<CatalogoGiroDTO> getGiros() {
		return giros;
	}

	public void setGiros(List<CatalogoGiroDTO> giros) {
		this.giros = giros;
	}

	public List<BancoEmisorDTO> getBancos() {
		return bancos;
	}

	public void setBancos(List<BancoEmisorDTO> bancos) {
		this.bancos = bancos;
	}

	public List<TipoDomiciliacionDTO> getMediosPagoDomiciliacion() {
		return mediosPagoDomiciliacion;
	}

	public void setMediosPagoDomiciliacion(
			List<TipoDomiciliacionDTO> mediosPagoDomiciliacion) {
		this.mediosPagoDomiciliacion = mediosPagoDomiciliacion;
	}

	@Autowired
	@Qualifier("catalogoGiroFacadeRemoteEJB")
	public void setCatalogoGiroFacade(
			CatalogoGiroFacadeRemote catalogoGiroFacade) {
		this.catalogoGiroFacade = catalogoGiroFacade;
	}

	@Autowired
	@Qualifier("bancoEmisorFacadeRemoteEJB")
	public void setBancoEmisorFacade(BancoEmisorFacadeRemote bancoEmisorFacade) {
		this.bancoEmisorFacade = bancoEmisorFacade;
	}

	@Autowired
	@Qualifier("tipoDomiciliacionFacadeRemoteEJB")
	public void setTipoDomiciliacionFacadeRemote(
			TipoDomiciliacionFacadeRemote tipoDomiciliacionFacade) {
		this.tipoDomiciliacionFacade = tipoDomiciliacionFacade;
	}

	@Autowired
	@Qualifier("paisFacadeRemoteEJB")
	public void setPaisFacade(PaisFacadeRemote paisFacade) {
		this.paisFacade = paisFacade;
	}

	public ClienteGenericoDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteGenericoDTO cliente) {
		this.cliente = cliente;
	}

	public Map<String, String> getPersonasAseguradas() {
		return personasAseguradas;
	}

	public void setPersonasAseguradas(Map<String, String> personasAseguradas) {
		this.personasAseguradas = personasAseguradas;
	}

	public Saldo getSaldo() {
		return saldo;
	}

	public void setSaldo(Saldo saldo) {
		this.saldo = saldo;
	}

	public String getIdClienteAsegurado() {
		return idClienteAsegurado;
	}

	public void setIdClienteAsegurado(String idClienteAsegurado) {
		this.idClienteAsegurado = idClienteAsegurado;
	}

	public Boolean getBanderaClienteCot() {
		return banderaClienteCot;
	}

	public void setBanderaClienteCot(Boolean banderaClienteCot) {
		this.banderaClienteCot = banderaClienteCot;
	}

	public Boolean getErrorGuardadoCliente() {
		return errorGuardadoCliente;
	}

	public void setErrorGuardadoCliente(Boolean errorGuardadoCliente) {
		this.errorGuardadoCliente = errorGuardadoCliente;
	}

	public String getMensageError() {
		return mensageError;
	}

	public void setMensageError(String mensageError) {
		this.mensageError = mensageError;
	}

	public CotizacionService getCotizacionService() {
		return cotizacionService;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	public ProgramaPago getProgramaPagoForm() {
		return programaPagoForm;
	}

	public void setProgramaPagoForm(ProgramaPago programaPagoForm) {
		this.programaPagoForm = programaPagoForm;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}

	public List<ToProgPago> getResList() {
		return resList;
	}

	public void setResList(List<ToProgPago> resList) {
		this.resList = resList;
	}

	public List<ToProgPagoGraper> getProPagosBandera() {
		return proPagosBandera;
	}

	public void setProPagosBandera(List<ToProgPagoGraper> proPagosBandera) {
		this.proPagosBandera = proPagosBandera;
	}

	public List<String> getListaProgPagoJson() {
		return listaProgPagoJson;
	}

	public void setListaProgPagoJson(List<String> listaProgPagoJson) {
		this.listaProgPagoJson = listaProgPagoJson;
	}

	public InputStream getStream() {
		return stream;
	}

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

	public String getRespuestaJason() {
		return respuestaJason;
	}

	public void setRespuestaJason(String respuestaJason) {
		this.respuestaJason = respuestaJason;
	}

	public String getRespuestaJasonSaldoOriginal() {
		return respuestaJasonSaldoOriginal;
	}

	public void setRespuestaJasonSaldoOriginal(
			String respuestaJasonSaldoOriginal) {
		this.respuestaJasonSaldoOriginal = respuestaJasonSaldoOriginal;
	}

	public Integer getBanderaRecuitifica() {
		return banderaRecuotifica;
	}

	public void setBanderaRecuitifica(Integer banderaRecuitifica) {
		this.banderaRecuotifica = banderaRecuitifica;
	}

	public String getIdProgPago() {
		return idProgPago;
	}

	public void setIdProgPago(String idProgPago) {
		this.idProgPago = idProgPago;
	}

	public CalculoService getCalculoService() {
		return calculoService;
	}

	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	public static class ToProgPagoGraper implements Serializable {

	
		private String acciones;

		private String numProgPago;
		private String nombreContratante;
		private String numRecibos;
		private String inicioPrograma;
		private String finPrograma;
		private String impPrimaNeta;
		private String impRcgosPagoFR;
		private String impDerechos;
		private String impIVA;
		private String impPrimaTotal;

		public String getAcciones() {
			return acciones;
		}

		public void setAcciones(String acciones) {
			this.acciones = acciones;
		}

		public String getNumProgPago() {
			return numProgPago;
		}

		public void setNumProgPago(String numProgPago) {
			this.numProgPago = numProgPago;
		}

		public String getNombreContratante() {
			return nombreContratante;
		}

		public void setNombreContratante(String nombreContratante) {
			this.nombreContratante = nombreContratante;
		}

		public String getNumRecibos() {
			return numRecibos;
		}

		public void setNumRecibos(String numRecibos) {
			this.numRecibos = numRecibos;
		}

		public String getInicioPrograma() {
			return inicioPrograma;
		}

		public void setInicioPrograma(String inicioPrograma) {
			this.inicioPrograma = inicioPrograma;
		}

		public String getFinPrograma() {
			return finPrograma;
		}

		public void setFinPrograma(String finPrograma) {
			this.finPrograma = finPrograma;
		}

		public String getImpPrimaNeta() {
			return impPrimaNeta;
		}

		public void setImpPrimaNeta(String impPrimaNeta) {
			this.impPrimaNeta = impPrimaNeta;
		}

		public String getImpRcgosPagoFR() {
			return impRcgosPagoFR;
		}

		public void setImpRcgosPagoFR(String impRcgosPagoFR) {
			this.impRcgosPagoFR = impRcgosPagoFR;
		}

		public String getImpDerechos() {
			return impDerechos;
		}

		public void setImpDerechos(String impDerechos) {
			this.impDerechos = impDerechos;
		}

		public String getImpIVA() {
			return impIVA;
		}

		public void setImpIVA(String impIVA) {
			this.impIVA = impIVA;
		}

		public String getImpPrimaTotal() {
			return impPrimaTotal;
		}

		public void setImpPrimaTotal(String impPrimaTotal) {
			this.impPrimaTotal = impPrimaTotal;
		}

		public static String encapsulaToProgpago(
				List<ToProgPago> listaProgramasPago, BigDecimal idToCotizacion,
				Boolean ro, String tipoMov, BigDecimal idToSolicitud, BigDecimal idToPoliza,
				BigDecimal idContinuity, String tipoFormaPago, BigDecimal numeroEndoso) {

			Format fmt = new SimpleDateFormat("dd/MM/yyyy");
			MidasJsonBase json = new MidasJsonBase();

			for (ToProgPago elemento : listaProgramasPago) {

				MidasJsonRow row = new MidasJsonRow();
				row.setId(elemento.getId().toString());
				row.setDatos(
						elemento.getNumProgPago().toString(),
						elemento.getNumInciso().toString(),
						elemento.getNombreContratante(),
						String.valueOf(elemento.getNumRecibos()),
						fmt.format(elemento.getInicioPrograma()),
						fmt.format(elemento.getFinPrograma()),
						elemento.getImpPrimaNeta().toString(),
						elemento.getImpRcgosPagoFR().toString(),
						elemento.getImpDerechos().toString(),
						elemento.getImpIVA().toString(),
						elemento.getImpPrimaTotal().toString(),
						getBotonesAccion(elemento.getId(),
								         idToCotizacion.toString(), 
								         ro, 
								         tipoMov, 
								         idToSolicitud, 
								         idToPoliza,
								         idContinuity,
								         tipoFormaPago,
								         numeroEndoso));
				json.addRow(row);

				// listaProgPagosGraper.add(graper);
			}

			return json.toString();
		}

		private static String getBotonesAccion(Long toProgPago,
				                               String idToCotizacion, 
				                               boolean ro, 
				                               String tipoMov, 
				                               BigDecimal idToSolicitud, 
				                               BigDecimal idToPoliza,
				                               BigDecimal idContinuity,
				                               String tipoFormaPago,
				                               BigDecimal numeroEndoso) {
			StringBuilder botones = new StringBuilder();
			String tm = "\""+tipoMov.toUpperCase()+"\"";

			if (!ro) {
				botones.append("<a href=\'javascript: void(0);\' "
						+ "onclick=\'eliminarProgramaPago("
						+ toProgPago
						+ ","
						+ idToCotizacion
						+ ")\'>"
						+ "<img border=\'0px\' alt=\'Eliminar Programa de Pago\' title=\'Eliminar Programa de Pago\' src=\'/MidasWeb/img/common/b_delete.gif\'/></a>");
			}
			botones.append("<a href=\'javascript: void(0);\' "
					+ "onclick=\'verRecibos("
					+ toProgPago
					+ ","
					+ idToCotizacion
					+ ","
					+ tm
					+ ","
					+ idToSolicitud
					+ ","
					+ idToPoliza
					+ ","
					+ idContinuity
					+ ","
					+ tipoFormaPago
					+ ","
					+ numeroEndoso
					+ ")\'>"
					+ "<img border=\'0px\' alt=\'Ver Detalle de Recibos\' title=\'Ver Detalle de Recibos\' src=\'/MidasWeb/img/icons/ico_editar.gif\'/></a>");

			return botones.toString();
		}
	}

	
	//METODO DE ENDOSO AJUSTE DE PRIMA
	@Action(value = "cargarProgramaPagoAP", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/programaPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String cargarProgramaPagoAP() throws Exception {
		programaPagoForm = new ProgramaPago();
		Map<String, Object> out = null;
		BigDecimal tipoFormaDePago = BigDecimal.ZERO;
		
		LOG.debug("INICIO DEL METODO CARGAR PROGRAMA PAGO AJUSTE DE PRIMA");
		
		LOG.debug("Id de continuidad: " + idContinuity);
		LOG.debug("Tipo Forma de Pago: " + tipoFormaPago);
		LOG.debug("Id Poliza: " + idToPoliza);
		
		PolizaDTO polizaDTO = PolizaDN.getInstancia().getPorId(idToPoliza);
		CotizacionDTO cotizacion = polizaDTO.getCotizacionDTO();
		idToCotizacion = cotizacion.getIdToCotizacion(); 
	
		/*
		if (polizaDTO != null){
			LOG.info("Poliza no nula");
			CotizacionDTO cotizacion = polizaDTO.getCotizacionDTO();
			if (cotizacion != null){
				LOG.info("Cotizacion no nula");
				LOG.info("Id de cotizacion: " + cotizacion.getIdToCotizacion());
				idToCotizacion = cotizacion.getIdToCotizacion(); 
			}
		}
			
		if (idToCotizacion == null){
			LOG.info("Id de cotizacion nula, seteando id de solicitud");
			idToCotizacion = idToSolicitud;
		}*/
			
		programaPagoForm.setIdToCotizacion(idToCotizacion);
		programaPagoForm.setReadOnly(new Boolean(readOnly));
		CotizacionDTO cotizacionDTO = cotizacionService.obtenerCotizacionPorId(idToCotizacion);
		programaPagoForm.setCotizacionDTO(cotizacionDTO);
		programaPagoForm.setIdToPoliza(idToPoliza);
		programaPagoForm.setTipoMov("E");
		//1: Igual poliza, 2: Anual
		if (tipoFormaPago.trim().equals("1")){
			tipoFormaDePago = new BigDecimal(tipoFormaPago);
		}
		try {
			out = recuotificacionService.generaProgramasPagoEndosoAP(idContinuity, tipoFormaDePago, banderaRecuotifica.toString(), numeroEndoso);			
			BigDecimal idsol = (BigDecimal) out.get("outIdEjecucion");
			programaPagoForm.setIdToSolicitud(idsol);
			BigDecimal codigoRespuesta = (BigDecimal) out.get("outCodResp");
			String descripcionRespuesta = (String) out.get("outDesResp");
			if (codigoRespuesta != null) {
				LogDeMidasWeb.log("Codigo de respuesta del SP Endoso" + codigoRespuesta, Level.INFO, null);
			}else{
				LogDeMidasWeb.log("Codigo de respuesta nulo", Level.INFO, null);
			}
			if (descripcionRespuesta != null) {
				LogDeMidasWeb.log("Descripcion de respuesta del SP Endoso" + codigoRespuesta, Level.INFO, null);
			}else{
				LogDeMidasWeb.log("Descripcion de respuesta nulo", Level.INFO, null);
			}
		} catch (Exception ex) {
			LogDeMidasWeb.log("Ocurrio un error en la llamada al SP recuatificacion Endoso: " + ex.getMessage(), Level.WARNING, null);
			return INPUT;
		}
		
		return SUCCESS;
	}
	
	
	@Action(value = "validaPrimasDeEndoso", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String validaPrimasDeEndoso() throws SystemException, IOException {
		
		try{
			if (tipoMov.equals("AP")){
				LOG.info("Obteniendo id de solicitud a partir de continuidad: " + idContinuity );
				idToSolicitud = recuotificacionService.obtenerSolicitudDeContinuidad(idContinuity);
			}
			LOG.info("Id de Solicitud a validar: " + idToSolicitud );
			Map<String, Object> res = recuotificacionService.validaPrimasDeEndoso(idToSolicitud);
			BigDecimal resultado = (BigDecimal) res.get("IdEjecucion");
			String result = "0";
			if (resultado.compareTo(BigDecimal.ZERO) > 0 ){
				result = "1";
			}
			StringBuilder myJson = new StringBuilder();
			myJson.append("{");
			myJson.append("\"resultado\"").append(":").append("\"")
				.append(result).append("\"");
			myJson.append("}");
		
			//InputStream is = new ByteArrayInputStream(respuestaJason.getBytes());
			InputStream is = new ByteArrayInputStream(myJson.toString().getBytes());
			stream = is;
			return SUCCESS;
		}catch(Exception e){
			LOG.info("Excepcion al validar las primas del endoso: " + e.getMessage());
			return ERROR;
		}
	}
	
	@Action(value = "validaEstatusRecuotificacion", results = { @Result(name = "success", type = "stream", params = {
			"inputName", "stream" }) })
	public String validaEstatusRecuotificacion() throws SystemException,
			IOException {

		Integer resultado = programaPagoService.obtenerParametroGeneral(new BigDecimal("22"), new BigDecimal("22030"));
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"resultado\"");
		json.append(":");
		json.append("\"").append(resultado.toString()).append("\"");
		json.append("}");
		// convert String into InputStream
		LOG.info("Resultado json: " + json.toString());
		InputStream is = new ByteArrayInputStream(json.toString().getBytes());
		stream = is;


		return SUCCESS;
	}
	
	public Boolean getBanderaRecuotManual() {
		return banderaRecuotManual;
	}

	public void setBanderaRecuotManual(Boolean banderaRecuotManual) {
		this.banderaRecuotManual = banderaRecuotManual;
	}

	public Boolean getTieneRolRecuotificacion() {
		return tieneRolRecuotificacion;
	}

	public void setTieneRolRecuotificacion(Boolean tieneRolRecuotificacion) {
		this.tieneRolRecuotificacion = tieneRolRecuotificacion;
	}
	
	public BigDecimal getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(BigDecimal numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public BigDecimal getIdContinuity() {
		return idContinuity;
	}

	public void setIdContinuity(BigDecimal idContinuity) {
		this.idContinuity = idContinuity;
	}

	public String getTipoFormaPago() {
		return tipoFormaPago;
	}

	public void setTipoFormaPago(String tipoFormaPago) {
		this.tipoFormaPago = tipoFormaPago;
	}
	
	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public Boolean getClienteExistenteEnSeycos() {
		return clienteExistenteEnSeycos;
	}

	public void setClienteExistenteEnSeycos(Boolean clienteExistenteEnSeycos) {
		this.clienteExistenteEnSeycos = clienteExistenteEnSeycos;
	}
	
	public Boolean getBanderaClienteProgramaPago() {
		return banderaClienteProgramaPago;
	}

	public void setBanderaClienteProgramaPago(Boolean banderaClienteProgramaPago) {
		this.banderaClienteProgramaPago = banderaClienteProgramaPago;
	}
	
	public PolizaService getPolizaService() {
		return polizaService;
	}

	@Autowired
	@Qualifier("polizaServiceEJB")
	public void setPolizaService(PolizaService polizaService) {
		this.polizaService = polizaService;
	}
	
	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public String getTipoMov() {
		return tipoMov;
	}

	public void setTipoMov(String tipoMov) {
		this.tipoMov = tipoMov;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("programaPagoEJB")
	public void setProgramaPagoService(ProgramaPagoService programaPagoService) {
		this.programaPagoService = programaPagoService;
	}
	
	@Autowired
	@Qualifier("negocioRecuotificacionEJB")
	public void setNegocioRecuotificacionService(NegocioRecuotificacionService negocioRecuotificacionService) {
		this.negocioRecuotificacionService = negocioRecuotificacionService;
	}
	
	
	/***********************************/
	

	@Action(value = "mostrarContenedorProgramaPago", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/contenedorProgramasPago.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String mostrarContenedorProgramaPago(){
		
		return SUCCESS;
	}
	
	@Action(value = "cargarListadoProgramaPago", results = {
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/listadoProgramasPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/error.jsp") })
	public String cargarListadoProgramaPago(){
		return SUCCESS;
	}
	
	@Action(value = "nuevoProgramaPago", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/listadoProgramasPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/componente/programapago/listadoProgramasPagoGrid.jsp")		
	})
	public String nuevoProgramaPago(){		
		return SUCCESS;
	}
	
	@Action(value = "modificarProgramaPago", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,json,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,json,mensaje"})
		})
	public String modificarProgramaPago(){
		return SUCCESS;
	}	
	
	@Action(value = "obtenerSaldo", results = { 
			@Result(name = SUCCESS, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,tipoSaldo,saldo,mensaje"}) ,
			@Result(name = INPUT, type="json", params={"noCache","true","ignoreHierarchy","false","includeProperties","^recuotificacion\\.id,versionId,tipoSaldo,saldo,mensaje"})
		})
	public String obtenerSaldo(){
		return SUCCESS;
	}
	
	@Action(value = "eliminarProgramaPagoV2", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/programapago/listadoProgramasPagoGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/componente/programapago/listadoProgramasPagoGrid.jsp")		
	})
	public String eliminarProgramaPagoV2(){	
		return SUCCESS;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Double getSaldoCampo() {
		return saldoCampo;
	}

	public void setSaldoCampo(Double saldoCampo) {
		this.saldoCampo = saldoCampo;
	}

	public String getTipoSaldo() {
		return tipoSaldo;
	}

	public void setTipoSaldo(String tipoSaldo) {
		this.tipoSaldo = tipoSaldo;
	}

	public Long getProgPagoId() {
		return progPagoId;
	}

	public void setProgPagoId(Long progPagoId) {
		this.progPagoId = progPagoId;
	}
	
	
}
