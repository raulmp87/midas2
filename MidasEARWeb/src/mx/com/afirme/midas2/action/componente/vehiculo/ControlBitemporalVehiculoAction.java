package mx.com.afirme.midas2.action.componente.vehiculo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
@Namespace("/componente/vehiculo/bitemporal")
public class ControlBitemporalVehiculoAction extends BaseAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4777510835482795758L;
	
	//Servicios
	private ListadoService listadoService;
	private NegocioSeccionService negocioSeccionService;
	private EntidadBitemporalService entidadBitemporalService;
	private IncisoService incisoService;
	private Boolean aplicaFlotilla = true;
	//Recibe el nombre asociado al objeto del action que se desea poblar.
	private String idEstadoName;
	private String idMunicipioName;
	private String idCotizacionName;
	private String idMonedaName;
	private String idNegocioSeccionName;
	private String idMarcaVehiculoName;
	private String idEstiloVehiculoName;
	private String modificadoresPrimaName;
	private String modificadoresDescripcionName;
	private String descripcionFinalName;
	private String idModeloVehiculoName;
	private String idTipoUsoVehiculoName;
	private String idNegocioPaqueteName;
	private String checkEsFronterizoName;
	private String validoEnName;
	private String idAutoIncisoContinuityName;
	private Double pctDescuentoEstado;
	private String pctDescuentoEstadoName;
	private String serieName;
	private String vinValidoName;
	private String coincideEstiloName;
	private String observacionesSesaName;
	private String idCodigoPostalName;
	
	//Recibe el nombre de la funcion a ejecutar en la propiedad OnChange del listado paquete
	private String onChangePaquete="";
	
	//Bandera que indica si un control del componente se muestra o no.
	private Boolean mostrarEstado = true;
	private Boolean mostrarMunicipio = true;
	private Boolean mostrarLineaNegocio = true;
	private Boolean mostrarMarcaVehiculo = true ;
	private Boolean mostrarEstiloVehiculo = true;
	private Boolean mostrarAjustarCaracteristicas = true;
	private Boolean mostrarModeloVehiculo = true;
	private Boolean mostrarTipoUsoVehiculo = true;
	private Boolean mostrarPaquete = true;
	private Boolean cotizacionExpress = false;
	private Boolean mostrarCheckEsFronterizo = false;
	
	
	//Recibe el valor de los paramatros con los que se invoca el componente
	private String idEstado;
	private String idMunicipio;
	private BigDecimal idCotizacion;
	private BigDecimal idMoneda;
	private BigDecimal idNegocioSeccion;
	private BigDecimal idMarcaVehiculo;
	private String idEstiloVehiculo;
	private String modificadoresPrima;
	private String modificadoresDescripcion;
	private String descripcionFinal;
	private Short idModeloVehiculo;
	private BigDecimal idTipoUsoVehiculo;
	private Long idNegocioPaquete;
	private Date validoEn;
	private Short soloConsulta = 0;
	private Long idAutoIncisoContinuity;
	private String serie;
	private Boolean vinValido;
	private Boolean coincideEstilo;
	private String observacionesSesa;
	private String idCodigoPostal;
	
	//Listados presentes en el componente 
	private Map<String, String> estadoMap;	
	private Map<String, String> municipioMap = new LinkedHashMap<String, String>();
	private List<NegocioSeccion> negocioSeccionList;
	private Map<BigDecimal, String> marcaMap = new LinkedHashMap<BigDecimal, String>();
	private Map<String, String> estiloMap = new LinkedHashMap<String, String>();
	private Map<Short, Short> modeloMap = new LinkedHashMap<Short, Short>();
	private List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList;
	private Map<Long, String> paqueteMap = new LinkedHashMap<Long, String>();
	private Map<BigDecimal, String> tipoUsoMap = new LinkedHashMap<BigDecimal, String>();
	
	
	
	//Propiedades de los controles
	private String posicionEtiqueta = "left"; 
	private String requerido = "true";

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}

	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Action
	(value = "getDatosVehiculo", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/vehiculo/datosVehiculo.jsp"), 
			@Result(name = "individual", location = "/jsp/componente/vehiculo/datosVehiculoInputs.jsp") })
	public String getDatosVehiculo(){
		//Carga los valores del valuestack para asignarlas a las propiedades correspondientes del componente
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		idEstado = (String) valueStack.findValue(idEstadoName, String.class);
		idMunicipio = (String) valueStack.findValue(idMunicipioName, String.class);
		idCotizacion = (BigDecimal) valueStack.findValue(idCotizacionName, BigDecimal.class);
		idMoneda = (BigDecimal) valueStack.findValue(idMonedaName, BigDecimal.class);
		idNegocioSeccion = (BigDecimal) valueStack.findValue(idNegocioSeccionName, BigDecimal.class);
		idMarcaVehiculo = (BigDecimal) valueStack.findValue(idMarcaVehiculoName, BigDecimal.class);
		idEstiloVehiculo = (String) valueStack.findValue(idEstiloVehiculoName, String.class);
		modificadoresPrima = (String) valueStack.findValue(modificadoresPrimaName, String.class);
		modificadoresDescripcion = (String) valueStack.findValue(modificadoresDescripcionName, String.class);
		descripcionFinal = (String) valueStack.findValue(descripcionFinalName, String.class);
		idModeloVehiculo = (Short) valueStack.findValue(idModeloVehiculoName, Short.class);
		idTipoUsoVehiculo = (BigDecimal) valueStack.findValue(idTipoUsoVehiculoName, BigDecimal.class);
		idNegocioPaquete = (Long) valueStack.findValue(idNegocioPaqueteName, Long.class);
		validoEn = (Date) valueStack.findValue(validoEnName, Date.class);
		idAutoIncisoContinuity = (Long) valueStack.findValue(idAutoIncisoContinuityName, Long.class);
		pctDescuentoEstado =  (Double) valueStack.findValue(pctDescuentoEstadoName, Double.class);
		serie = (String) valueStack.findValue(serieName, String.class);
		vinValido = (Boolean) valueStack.findValue(vinValidoName, Boolean.class);
		coincideEstilo = (Boolean) valueStack.findValue(coincideEstiloName, Boolean.class);
		observacionesSesa = (String) valueStack.findValue(observacionesSesaName, String.class);
		idCodigoPostal = (String) valueStack.findValue(idCodigoPostalName, String.class);

		//Carga los listados de acuerdo a los valores recibidos en los parametros
		BitemporalCotizacion bitemporalCotizacion = null;
		if(idCotizacion != null){
			bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, idCotizacion, TimeUtils.getDateTime(validoEn));
		}	
		if(bitemporalCotizacion != null && bitemporalCotizacion.getValue().getSolicitud() != null && bitemporalCotizacion.getValue().getSolicitud().getNegocio() != null){
			estadoMap = listadoService.getEstadosPorNegocioId(bitemporalCotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), cotizacionExpress);
		}
		if(cotizacionExpress){
			estadoMap = listadoService.getEstadosPorNegocioId(null, cotizacionExpress);
		}
		//estadoMap = listadoService.getMapEstadosMX();
		if(idEstado != null){
			municipioMap = listadoService.getMapMunicipiosPorEstado(idEstado);
		}
		if(cotizacionExpress){
			mostrarLineaNegocio=false;
			mostrarTipoUsoVehiculo = false;
			mostrarPaquete = false;
			mostrarCheckEsFronterizo = true;
		}else{
			if(idCotizacion != null){
				CotizacionDTO cotizacionDTO = new CotizacionDTO();
				cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
				cotizacionDTO.setTipoPolizaDTO(new TipoPolizaDTO());
				cotizacionDTO.getSolicitudDTO().setIdToSolicitud(bitemporalCotizacion.getValue().getSolicitud().getIdToSolicitud());
				cotizacionDTO.getTipoPolizaDTO().setIdToTipoPoliza(bitemporalCotizacion.getValue().getTipoPoliza().getIdToTipoPoliza());
				negocioSeccionList = negocioSeccionService.getSeccionListByCotizacion(cotizacionDTO);
			}	
		}
		
		if(idNegocioSeccion != null)
			marcaMap = listadoService.getMapMarcaVehiculoPorNegocioSeccion(idNegocioSeccion);
		
		if(idMarcaVehiculo != null && idNegocioSeccion != null && idMoneda != null)
			estiloMap = listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idMarcaVehiculo,idNegocioSeccion, idMoneda, idAutoIncisoContinuity);
		if(idEstiloVehiculo != null )
			modeloMap = listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(idMoneda, idEstiloVehiculo, idNegocioSeccion);
		if(idModeloVehiculo != null)
			tipoUsoMap = listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(idEstiloVehiculo, idNegocioSeccion);
		if(idTipoUsoVehiculo != null)	
			paqueteMap = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion);	
		
		if(aplicaFlotilla == false){
			return "individual";
		}
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarOtrasCaract", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/vehiculo/configurarCaracteristicasVehiculo.jsp") })
	public String mostrarOtrasCaract(){
		caracteristicasVehiculoList=incisoService.listarCaracteristicas(null);
		return SUCCESS;
	}
	
/*	public void validateDefinirOtrasCaract(){
		int i=0;
		for(CaracteristicasVehiculoDTO caracteristicasVehiculoDTO:caracteristicasVehiculoList){			
			addErrors(caracteristicasVehiculoDTO, NewItemChecks.class, this, "caracteristicasVehiculoList["+i+"]");			
			i++;			
		}
	}*/
	@Action
	(value = "definirOtrasCaract", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/vehiculo/caracteristicasVehiculoArea.jsp") })
	public String definirOtrasCaract(){
		//TODO Migrar a bitemporalidad
		IncisoAutoCot incisoAutoCot = incisoService.definirOtrasCaract(caracteristicasVehiculoList, "");
		descripcionFinal = incisoAutoCot.getDescripcionFinal();
		modificadoresDescripcion = incisoAutoCot.getModificadoresDescripcion();
		return SUCCESS;
	}


	public String getIdEstadoName() {
		return idEstadoName;
	}


	public void setIdEstadoName(String idEstadoName) {
		this.idEstadoName = idEstadoName;
	}


	public String getIdMunicipioName() {
		return idMunicipioName;
	}


	public void setIdMunicipioName(String idMunicipioName) {
		this.idMunicipioName = idMunicipioName;
	}


	public String getIdCotizacionName() {
		return idCotizacionName;
	}

	public void setIdCotizacionName(String idCotizacionName) {
		this.idCotizacionName = idCotizacionName;
	}

	public String getIdMonedaName() {
		return idMonedaName;
	}

	public void setIdMonedaName(String idMonedaName) {
		this.idMonedaName = idMonedaName;
	}

	public String getIdNegocioSeccionName() {
		return idNegocioSeccionName;
	}


	public void setIdNegocioSeccionName(String idNegocioSeccionName) {
		this.idNegocioSeccionName = idNegocioSeccionName;
	}


	public String getIdMarcaVehiculoName() {
		return idMarcaVehiculoName;
	}


	public void setIdMarcaVehiculoName(String idMarcaVehiculoName) {
		this.idMarcaVehiculoName = idMarcaVehiculoName;
	}


	public String getIdEstiloVehiculoName() {
		return idEstiloVehiculoName;
	}


	public void setIdEstiloVehiculoName(String idEstiloVehiculoName) {
		this.idEstiloVehiculoName = idEstiloVehiculoName;
	}


	public String getModificadoresPrimaName() {
		return modificadoresPrimaName;
	}

	public void setModificadoresPrimaName(String modificadoresPrimaName) {
		this.modificadoresPrimaName = modificadoresPrimaName;
	}

	public String getModificadoresDescripcionName() {
		return modificadoresDescripcionName;
	}

	public void setModificadoresDescripcionName(String modificadoresDescripcionName) {
		this.modificadoresDescripcionName = modificadoresDescripcionName;
	}

	public String getDescripcionFinalName() {
		return descripcionFinalName;
	}

	public void setDescripcionFinalName(String descripcionFinalName) {
		this.descripcionFinalName = descripcionFinalName;
	}

	public String getIdModeloVehiculoName() {
		return idModeloVehiculoName;
	}


	public void setIdModeloVehiculoName(String idModeloVehiculoName) {
		this.idModeloVehiculoName = idModeloVehiculoName;
	}


	public String getIdTipoUsoVehiculoName() {
		return idTipoUsoVehiculoName;
	}


	public void setIdTipoUsoVehiculoName(String idTipoUsoVehiculoName) {
		this.idTipoUsoVehiculoName = idTipoUsoVehiculoName;
	}


	public String getIdNegocioPaqueteName() {
		return idNegocioPaqueteName;
	}


	public void setIdNegocioPaqueteName(String idNegocioPaqueteName) {
		this.idNegocioPaqueteName = idNegocioPaqueteName;
	}


	public String getCheckEsFronterizoName() {
		return checkEsFronterizoName;
	}

	public void setCheckEsFronterizoName(String checkEsFronterizoName) {
		this.checkEsFronterizoName = checkEsFronterizoName;
	}

	public String getValidoEnName() {
		return validoEnName;
	}

	public void setValidoEnName(String validoEnName) {
		this.validoEnName = validoEnName;
	}
	
	public String getIdAutoIncisoContinuityName() {
		return idAutoIncisoContinuityName;
	}

	public void setIdAutoIncisoContinuityName(String idAutoIncisoContinuityName) {
		this.idAutoIncisoContinuityName = idAutoIncisoContinuityName;
	}

	public String getOnChangePaquete() {
		return onChangePaquete;
	}


	public void setOnChangePaquete(String onChangePaquete) {
		this.onChangePaquete = onChangePaquete;
	}


	public Boolean getMostrarEstado() {
		return mostrarEstado;
	}


	public void setMostrarEstado(Boolean mostrarEstado) {
		this.mostrarEstado = mostrarEstado;
	}


	public Boolean getMostrarMunicipio() {
		return mostrarMunicipio;
	}


	public void setMostrarMunicipio(Boolean mostrarMunicipio) {
		this.mostrarMunicipio = mostrarMunicipio;
	}


	public Boolean getMostrarLineaNegocio() {
		return mostrarLineaNegocio;
	}


	public void setMostrarLineaNegocio(Boolean mostrarLineaNegocio) {
		this.mostrarLineaNegocio = mostrarLineaNegocio;
	}


	public Boolean getMostrarMarcaVehiculo() {
		return mostrarMarcaVehiculo;
	}


	public void setMostrarMarcaVehiculo(Boolean mostrarMarcaVehiculo) {
		this.mostrarMarcaVehiculo = mostrarMarcaVehiculo;
	}


	public Boolean getMostrarEstiloVehiculo() {
		return mostrarEstiloVehiculo;
	}


	public void setMostrarEstiloVehiculo(Boolean mostrarEstiloVehiculo) {
		this.mostrarEstiloVehiculo = mostrarEstiloVehiculo;
	}


	public Boolean getMostrarAjustarCaracteristicas() {
		return mostrarAjustarCaracteristicas;
	}

	public void setMostrarAjustarCaracteristicas(
			Boolean mostrarAjustarCaracteristicas) {
		this.mostrarAjustarCaracteristicas = mostrarAjustarCaracteristicas;
	}

	public Boolean getMostrarModeloVehiculo() {
		return mostrarModeloVehiculo;
	}


	public void setMostrarModeloVehiculo(Boolean mostrarModeloVehiculo) {
		this.mostrarModeloVehiculo = mostrarModeloVehiculo;
	}


	public Boolean getMostrarTipoUsoVehiculo() {
		return mostrarTipoUsoVehiculo;
	}


	public void setMostrarTipoUsoVehiculo(Boolean mostrarTipoUsoVehiculo) {
		this.mostrarTipoUsoVehiculo = mostrarTipoUsoVehiculo;
	}


	public Boolean getMostrarPaquete() {
		return mostrarPaquete;
	}


	public void setMostrarPaquete(Boolean mostrarPaquete) {
		this.mostrarPaquete = mostrarPaquete;
	}


	public Boolean getCotizacionExpress() {
		return cotizacionExpress;
	}


	public void setCotizacionExpress(Boolean cotizacionExpress) {
		if(cotizacionExpress != null){
			this.cotizacionExpress = cotizacionExpress;
		}
	}


	public Boolean getMostrarCheckEsFronterizo() {
		return mostrarCheckEsFronterizo;
	}

	public void setMostrarCheckEsFronterizo(Boolean mostrarCheckEsFronterizo) {
		this.mostrarCheckEsFronterizo = mostrarCheckEsFronterizo;
	}

	public String getIdEstado() {
		return idEstado;
	}


	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}


	public String getIdMunicipio() {
		return idMunicipio;
	}


	public void setIdMunicipio(String idMunicipio) {
		this.idMunicipio = idMunicipio;
	}


	public BigDecimal getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(BigDecimal idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public BigDecimal getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}


	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}


	public BigDecimal getIdMarcaVehiculo() {
		return idMarcaVehiculo;
	}


	public void setIdMarcaVehiculo(BigDecimal idMarcaVehiculo) {
		this.idMarcaVehiculo = idMarcaVehiculo;
	}


	public String getIdEstiloVehiculo() {
		return idEstiloVehiculo;
	}


	public void setIdEstiloVehiculo(String idEstiloVehiculo) {
		this.idEstiloVehiculo = idEstiloVehiculo;
	}


	public String getModificadoresPrima() {
		return modificadoresPrima;
	}

	public void setModificadoresPrima(String modificadoresPrima) {
		this.modificadoresPrima = modificadoresPrima;
	}

	public String getModificadoresDescripcion() {
		return modificadoresDescripcion;
	}

	public void setModificadoresDescripcion(String modificadoresDescripcion) {
		this.modificadoresDescripcion = modificadoresDescripcion;
	}

	public String getDescripcionFinal() {
		return descripcionFinal;
	}

	public void setDescripcionFinal(String descripcionFinal) {
		this.descripcionFinal = descripcionFinal;
	}

	public Short getIdModeloVehiculo() {
		return idModeloVehiculo;
	}


	public void setIdModeloVehiculo(Short idModeloVehiculo) {
		this.idModeloVehiculo = idModeloVehiculo;
	}


	public Long getIdNegocioPaquete() {
		return idNegocioPaquete;
	}


	public void setIdNegocioPaquete(Long idNegocioPaquete) {
		this.idNegocioPaquete = idNegocioPaquete;
	}


	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public BigDecimal getIdTipoUsoVehiculo() {
		return idTipoUsoVehiculo;
	}


	public void setIdTipoUsoVehiculo(BigDecimal idTipoUsoVehiculo) {
		this.idTipoUsoVehiculo = idTipoUsoVehiculo;
	}


	public Map<String, String> getEstadoMap() {
		return estadoMap;
	}


	public void setEstadoMap(Map<String, String> estadoMap) {
		this.estadoMap = estadoMap;
	}


	public Map<String, String> getMunicipioMap() {
		return municipioMap;
	}


	public void setMunicipioMap(Map<String, String> municipioMap) {
		this.municipioMap = municipioMap;
	}


	public List<NegocioSeccion> getNegocioSeccionList() {
		return negocioSeccionList;
	}


	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}


	public Map<BigDecimal, String> getMarcaMap() {
		return marcaMap;
	}


	public void setMarcaMap(Map<BigDecimal, String> marcaMap) {
		this.marcaMap = marcaMap;
	}


	public Map<String, String> getEstiloMap() {
		return estiloMap;
	}


	public void setEstiloMap(Map<String, String> estiloMap) {
		this.estiloMap = estiloMap;
	}


	public Map<Short, Short> getModeloMap() {
		return modeloMap;
	}


	public void setModeloMap(Map<Short, Short> modeloMap) {
		this.modeloMap = modeloMap;
	}


	public List<CaracteristicasVehiculoDTO> getCaracteristicasVehiculoList() {
		return caracteristicasVehiculoList;
	}


	public void setCaracteristicasVehiculoList(
			List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList) {
		this.caracteristicasVehiculoList = caracteristicasVehiculoList;
	}


	public Map<Long, String> getPaqueteMap() {
		return paqueteMap;
	}


	public void setPaqueteMap(Map<Long, String> paqueteMap) {
		this.paqueteMap = paqueteMap;
	}


	public Map<BigDecimal, String> getTipoUsoMap() {
		return tipoUsoMap;
	}


	public void setTipoUsoMap(Map<BigDecimal, String> tipoUsoMap) {
		this.tipoUsoMap = tipoUsoMap;
	}

	public String getPosicionEtiqueta() {
		return posicionEtiqueta;
	}

	public void setPosicionEtiqueta(String posicionEtiqueta) {
		this.posicionEtiqueta = posicionEtiqueta;
	}

	public String getRequerido() {
		return requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	public Boolean getAplicaFlotilla() {
		return aplicaFlotilla;
	}

	public void setAplicaFlotilla(Boolean aplicaFlotilla) {
		this.aplicaFlotilla = aplicaFlotilla;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Long getIdAutoIncisoContinuity() {
		return idAutoIncisoContinuity;
	}

	public void setIdAutoIncisoContinuity(Long idAutoIncisoContinuity) {
		this.idAutoIncisoContinuity = idAutoIncisoContinuity;
	}

	public Double getPctDescuentoEstado() {
		return pctDescuentoEstado;
	}

	public void setPctDescuentoEstado(Double pctDescuentoEstado) {
		this.pctDescuentoEstado = pctDescuentoEstado;
	}

	public String getPctDescuentoEstadoName() {
		return pctDescuentoEstadoName;
	}

	public void setPctDescuentoEstadoName(String pctDescuentoEstadoName) {
		this.pctDescuentoEstadoName = pctDescuentoEstadoName;
	}

	public String getSerieName() {
		return serieName;
	}

	public void setSerieName(String serieName) {
		this.serieName = serieName;
	}

	public String getVinValidoName() {
		return vinValidoName;
	}

	public void setVinValidoName(String vinValidoName) {
		this.vinValidoName = vinValidoName;
	}

	public String getCoincideEstiloName() {
		return coincideEstiloName;
	}

	public void setCoincideEstiloName(String coincideEstiloName) {
		this.coincideEstiloName = coincideEstiloName;
	}

	public String getObservacionesSesaName() {
		return observacionesSesaName;
	}

	public void setObservacionesSesaName(String observacionesSesaName) {
		this.observacionesSesaName = observacionesSesaName;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Boolean getVinValido() {
		return vinValido;
	}

	public void setVinValido(Boolean vinValido) {
		this.vinValido = vinValido;
	}

	public Boolean getCoincideEstilo() {
		return coincideEstilo;
	}

	public void setCoincideEstilo(Boolean coincideEstilo) {
		this.coincideEstilo = coincideEstilo;
	}

	public String getObservacionesSesa() {
		return observacionesSesa;
	}

	public void setObservacionesSesa(String observacionesSesa) {
		this.observacionesSesa = observacionesSesa;
	}	
	public String getIdCodigoPostal() {
		return idCodigoPostal;
	}

	public void setIdCodigoPostal(String idCodigoPostal) {
		this.idCodigoPostal = idCodigoPostal;
	}

	public String getIdCodigoPostalName() {
		return idCodigoPostalName;
	}

	public void setIdCodigoPostalName(String idCodigoPostalName) {
		this.idCodigoPostalName = idCodigoPostalName;
	}
}
