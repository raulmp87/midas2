package mx.com.afirme.midas2.action.componente.incisos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
@Namespace("/componente/incisos")
public class ListadoIncisosDinamicoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idNoIncisoName;
	private String idDescVehiculoName;
	private String idLineaNegocioName;
	private String idNumeroSerieName;
	private String idPaqueteName;
	private String idNumeroMotorName;
	private String idPlacasName;
	private String idConductorName;
	private String idAseguradoName;
	private String idToPolizaName;
	private String idValidoEnName;
	private String idAccionEndosoName;
	private String idClaveTipoEndosoName;
	private String idNumeroCotizacionName;
	
	private int idTipoVista = 0;
	private short estatusCotizacion;
	private Boolean hasRecordsInProcess;
	private Long idToPoliza;
	private Date validoEn;
	private String accionEndoso;
	private Short claveTipoEndoso;
	private Integer numeroCotizacion;	
	
	//Filtros Busqueda
	private BigDecimal numeroInciso;
	private String descripcionVehiculo;
	private String lineaNegocio;
	private String numeroSerie;
	private String paquete;
	private String numeroMotor;
	private String placas;
	private String nombreConductor;
	private String nombreAsegurado;
	
	//Tipos de Vistas
	public static final int VISTA_ALTA_INCISO = 1;
	public static final int VISTA_CAMBIO_DATOS = 2;
	public static final int VISTA_MOVIMIENTOS = 3;
	public static final int VISTA_BAJA_INCISO = 4;
	public static final int VISTA_BAJA_INCISO_PERDIDA_TOTAL = 5;
	public static final int VISTA_COMPLEMENTAR_INCISO = 6;
	public static final int VISTA_REHABILITAR_INCISO = 7;
	public static final int VISTA_CANCELACION_POLIZA_PERDIDA_TOTAL = 8;
	public static final int VISTA_DESAGRUPACION_RECIBOS = 9;

	
	private List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>(1);
	
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
    private IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
    
    private String idsSeleccionados;
    private EntidadService entidadService;
    private ListadoService listadoService;
    private NegocioSeccionService negocioSeccionService;
    private EntidadBitemporalService entidadBitemporalService;    
    private IncisoBitemporalService incisoBitemporalService;	

	private List<NegocioSeccion> negocioSeccionList = new ArrayList<NegocioSeccion>(1);;
    private Set<String> descripcionesIncisos = new HashSet<String>();;
    private Map<Long, String> paquetes = new LinkedHashMap<Long, String>();
    private Boolean filtrar = false;
    private Boolean habilitaCobranzaInciso = false;
    
    private CobranzaIncisoService cobranzaIncisoService;    

	@Autowired
	@Qualifier("cobranzaIncisoServiceEJB")
	public void setCobranzaIncisoService(CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}
	
	@Autowired
    @Qualifier("listadoIncisosDinamicoServiceEJB")
	public void setListadoIncisosDinamicoService(
			ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("IncisoBitemporalServiceEJB")
	public void setIncisoBitemporalService(
			IncisoBitemporalService incisoBitemporalService) {
		this.incisoBitemporalService = incisoBitemporalService;
	}
	
	public void prepareMostrarListadoCotizacionesDinamico()
	{
		System.out.println("prepareMostrarListadoCotizacionesDinamico()");

        ValueStack valueStack = ActionContext.getContext().getValueStack();		
		idToPoliza= (Long) valueStack.findValue(idToPolizaName,Long.class);
		validoEn = (Date) valueStack.findValue(idValidoEnName, Date.class);
		claveTipoEndoso = (Short) valueStack.findValue(idClaveTipoEndosoName,Short.class);
		
		if(validoEn == null)
		{
			validoEn = new Date();
		}
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(idToPoliza));
		
		BitemporalCotizacion bitemporalCotizacion;
		
		if (claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL || 
				claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL) {
			
			bitemporalCotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
					.getCotizacionDTO().getIdToCotizacion(), TimeUtils.getDateTime(validoEn));	
			
		}else
		{
			bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
					.getCotizacionDTO().getIdToCotizacion(), TimeUtils.getDateTime(validoEn));			
		}				
		   
		//if(!bitemporalCotizacion.getContinuity().getIncisoContinuities().getValue().isEmpty())
		if(bitemporalCotizacion != null && bitemporalCotizacion.getEntidadContinuity() != null)
		{
			negocioSeccionList = listadoIncisosDinamicoService.getSeccionList(claveTipoEndoso, bitemporalCotizacion.getEntidadContinuity().getId(), validoEn);
			if(descripcionesIncisos == null || descripcionesIncisos.isEmpty()){
				descripcionesIncisos = listadoIncisosDinamicoService.listarIncisosDescripcion(claveTipoEndoso, bitemporalCotizacion.getEntidadContinuity().getId(), validoEn);
			}
			if(paquetes == null)
			{
				paquetes =  new LinkedHashMap<Long, String>();
			}			
		}
		else{
			negocioSeccionList = new ArrayList<NegocioSeccion>(1);
			paquetes =  new LinkedHashMap<Long, String>();
			descripcionesIncisos = new HashSet<String>();
		}
		
		this.numeroCotizacion = bitemporalCotizacion.getEntidadContinuity().getNumero();
		this.estatusCotizacion = bitemporalCotizacion.getValue().getClaveEstatus();
		
		incisoBitemporalService.borrarIncisosNoAsociados(bitemporalCotizacion.getEntidadContinuity(), TimeUtils.getDateTime(validoEn));		
	}

	@Action
	(value = "mostrarListadoCotizacionesDinamico", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp")})	
	public String mostrarListadoCotizacionesDinamico() 
	{		
		System.out.println("mostrarListadoCotizacionesDinamico()");		
		
		ValueStack valueStack = ActionContext.getContext().getValueStack();	
		
		idToPoliza= (Long) valueStack.findValue(idToPolizaName,Long.class);
		validoEn = (Date) valueStack.findValue(idValidoEnName, Date.class);
		accionEndoso = (String) valueStack.findValue(idAccionEndosoName,String.class);
		claveTipoEndoso = (Short) valueStack.findValue(idClaveTipoEndosoName,Short.class);
		
		/*switch (idTipoVista) 
		{
			case VISTA_CAMBIO_DATOS:
				break;
			case VISTA_COMPLEMENTAR_INCISO: 
			case VISTA_MOVIMIENTOS:
			case VISTA_ALTA_INCISO:
				numeroInciso = (BigDecimal) valueStack.findValue(idNoIncisoName,BigDecimal.class);
				descripcionVehiculo = (String) valueStack.findValue(idDescVehiculoName, String.class);
				lineaNegocio = (String) valueStack.findValue(idLineaNegocioName,String.class);			
				paquete = (String) valueStack.findValue(idPaqueteName, String.class);
				break;
			case VISTA_BAJA_INCISO:
			case VISTA_BAJA_INCISO_PERDIDA_TOTAL:
				numeroInciso = (BigDecimal) valueStack.findValue(idNoIncisoName,BigDecimal.class);
				descripcionVehiculo = (String) valueStack.findValue(idDescVehiculoName, String.class);			
				numeroSerie = (String) valueStack.findValue(idNumeroSerieName,String.class);		
				numeroMotor = (String) valueStack.findValue(idNumeroMotorName,String.class);
				placas = (String) valueStack.findValue(idPlacasName, String.class);
				nombreConductor = (String) valueStack.findValue(idConductorName,String.class);
				nombreAsegurado = (String) valueStack.findValue(idAseguradoName,String.class);
				break;
				
			default:
		}	*/	
				
		return SUCCESS;
	}
	
	@Action
	(value = "busquedaIncisosPaginacion", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") })
	public String busquedaIncisosPaginacion() {	
		
		if (filtros != null){
			if(filtrar){
				if(getTotalCount() == null){
					setTotalCount(listadoIncisosDinamicoService.obtenerTotalPaginacionIncisos(filtros, claveTipoEndoso, idToPoliza, validoEn, true, idTipoVista));
					setListadoEnSession(null);
				}
				setPosPaginado();
			}
		}
		
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action
	(value = "busquedaIncisos", results = { 
			@Result(name = SUCCESS, location = "/jsp/componente/incisos/listadoIncisosDinamicoGrid.jsp") })
	public String busquedaIncisos(){	
		
		if(idToPoliza != null){
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(idToPoliza));
			habilitaCobranzaInciso = cobranzaIncisoService.habilitaCobranzaInciso(poliza.getCotizacionDTO().getIdToCotizacion());
		}
		
		if (filtros != null){
			if(filtrar){
				if(!listadoDeCache()){
					filtros.setPrimerRegistroACargar(getPosStart());
					filtros.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
					listaIncisosCotizacion = listadoIncisosDinamicoService.seleccionarIncisos(listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, claveTipoEndoso, idToPoliza, validoEn, true, idTipoVista), getElementosSeleccionados());
					setListadoEnSession(listaIncisosCotizacion);
				}else{
					listaIncisosCotizacion = listadoIncisosDinamicoService.seleccionarIncisos((List<BitemporalInciso>) getListadoPaginado(), getElementosSeleccionados());
					setListadoEnSession(listaIncisosCotizacion);
				}
			}
		}
		
		if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
		{
			ServletActionContext.getContext().getSession().put("listaIncisosCotizacion", listaIncisosCotizacion);
		}
				
		return SUCCESS;
	}
	
	@Action
	(value = "multiplicarInciso", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String multiplicarInciso()
	{
		return SUCCESS;		
	}
	
	@Action
	(value = "eliminarInciso", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String eliminarInciso()
	{
		return SUCCESS;		
	}
	
	@Action
	(value = "editarInciso", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String editarInciso()
	{
		//TODO
		return INPUT;		
	}
	
	@Action
	(value = "verDetalleVehiculo", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String verDetalleVehiculo()
	{
		return SUCCESS;		
	}
	
	@Action
	(value = "verDetalleAsegurado", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String verDetalleAsegurado()
	{
		return SUCCESS;		
	}
	
	@Action
	(value = "consultarInciso", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String consultarInciso()
	{
		//TODO
		
		String[] idsPorAutorizar = idsSeleccionados.split(",");
		
		return INPUT;		
	}
	
	@Action
	(value = "agregar", results =  {
	        @Result(name = SUCCESS,location = "/jsp/componente/incisos/listadoIncisosDinamicoFiltros.jsp" )})
	public String agregar()
	{
		//TODO
		return INPUT;		
	}
	
	public String getIdNoIncisoName() {
		return idNoIncisoName;
	}
	public void setIdNoIncisoName(String idNoIncisoName) {
		this.idNoIncisoName = idNoIncisoName;
	}
	public String getIdDescVehiculoName() {
		return idDescVehiculoName;
	}
	public void setIdDescVehiculoName(String idDescVehiculoName) {
		this.idDescVehiculoName = idDescVehiculoName;
	}
	public String getIdLineaNegocioName() {
		return idLineaNegocioName;
	}
	public void setIdLineaNegocioName(String idLineaNegocioName) {
		this.idLineaNegocioName = idLineaNegocioName;
	}
	public String getIdNumeroSerieName() {
		return idNumeroSerieName;
	}
	public void setIdNumeroSerieName(String idNumeroSerieName) {
		this.idNumeroSerieName = idNumeroSerieName;
	}
	public String getIdPaqueteName() {
		return idPaqueteName;
	}
	public void setIdPaqueteName(String idPaqueteName) {
		this.idPaqueteName = idPaqueteName;
	}
	public String getIdNumeroMotorName() {
		return idNumeroMotorName;
	}
	public void setIdNumeroMotorName(String idNumeroMotorName) {
		this.idNumeroMotorName = idNumeroMotorName;
	}
	public String getIdPlacasName() {
		return idPlacasName;
	}
	public void setIdPlacasName(String idPlacasName) {
		this.idPlacasName = idPlacasName;
	}
	public String getIdConductorName() {
		return idConductorName;
	}
	public void setIdConductorName(String idConductorName) {
		this.idConductorName = idConductorName;
	}
	public String getIdAseguradoName() {
		return idAseguradoName;
	}
	public void setIdAseguradoName(String idAseguradoName) {
		this.idAseguradoName = idAseguradoName;
	}
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	public String getDescripcionVehiculo() {
		return descripcionVehiculo;
	}
	public void setDescripcionVehiculo(String descripcionVehiculo) {
		this.descripcionVehiculo = descripcionVehiculo;
	}
	public String getLineaNegocio() {
		return lineaNegocio;
	}
	public void setLineaNegocio(String lineaNegocio) {
		this.lineaNegocio = lineaNegocio;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}
	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
	public String getPaquete() {
		return paquete;
	}
	public void setPaquete(String paquete) {
		this.paquete = paquete;
	}
	public String getNumeroMotor() {
		return numeroMotor;
	}
	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}
	public String getPlacas() {
		return placas;
	}
	public void setPlacas(String placas) {
		this.placas = placas;
	}
	public String getNombreConductor() {
		return nombreConductor;
	}
	public void setNombreConductor(String nombreConductor) {
		this.nombreConductor = nombreConductor;
	}
	public String getNombreAsegurado() {
		return nombreAsegurado;
	}
	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}	

	public int getIdTipoVista() {
		return idTipoVista;
	}

	public void setIdTipoVista(int idTipoVista) {
		this.idTipoVista = idTipoVista;
	}

	public short getEstatusCotizacion() {
		return estatusCotizacion;
	}

	public void setEstatusCotizacion(short estatusCotizacion) {
		this.estatusCotizacion = estatusCotizacion;
	}

	public Boolean getHasRecordsInProcess() {
		return hasRecordsInProcess;
	}

	public void setHasRecordsInProcess(Boolean hasRecordsInProcess) {
		this.hasRecordsInProcess = hasRecordsInProcess;
	}

	public Long getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(Long idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public String getIdToPolizaName() {
		return idToPolizaName;
	}

	public void setIdToPolizaName(String idToPolizaName) {
		this.idToPolizaName = idToPolizaName;
	}

	public String getIdValidoEnName() {
		return idValidoEnName;
	}

	public void setIdValidoEnName(String idValidoEnName) {
		this.idValidoEnName = idValidoEnName;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public String getIdAccionEndosoName() {
		return idAccionEndosoName;
	}

	public void setIdAccionEndosoName(String idAccionEndosoName) {
		this.idAccionEndosoName = idAccionEndosoName;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public String getIdClaveTipoEndosoName() {
		return idClaveTipoEndosoName;
	}

	public void setIdClaveTipoEndosoName(String idClaveTipoEndosoName) {
		this.idClaveTipoEndosoName = idClaveTipoEndosoName;
	}

	public IncisoCotizacionDTO getFiltros() {
		return filtros;
	}

	public void setFiltros(IncisoCotizacionDTO filtros) {
		this.filtros = filtros;
	}

	public List<BitemporalInciso> getListaIncisosCotizacion() {
		return listaIncisosCotizacion;
	}

	public void setListaIncisosCotizacion(
			List<BitemporalInciso> listaIncisosCotizacion) {
		this.listaIncisosCotizacion = listaIncisosCotizacion;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}

	public Integer getNumeroCotizacion() {
		return numeroCotizacion;
	}

	public void setNumeroCotizacion(Integer numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}

	public String getIdNumeroCotizacionName() {
		return idNumeroCotizacionName;
	}

	public void setIdNumeroCotizacionName(String idNumeroCotizacionName) {
		this.idNumeroCotizacionName = idNumeroCotizacionName;
	}

	public List<NegocioSeccion> getNegocioSeccionList() {
		return negocioSeccionList;
	}

	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}

	public Set<String> getDescripcionesIncisos() {
		return descripcionesIncisos;
	}

	public void setDescripcionesIncisos(Set<String> descripcionesIncisos) {
		this.descripcionesIncisos = descripcionesIncisos;
	}

	public Map<Long, String> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(Map<Long, String> paquetes) {
		this.paquetes = paquetes;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}

	public void setHabilitaCobranzaInciso(Boolean habilitaCobranzaInciso) {
		this.habilitaCobranzaInciso = habilitaCobranzaInciso;
	}

	public Boolean getHabilitaCobranzaInciso() {
		return habilitaCobranzaInciso;
	}
}
