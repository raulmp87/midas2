package mx.com.afirme.midas2.action.componente.condicionespecial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService.FiltroCondicionEspecialCotizacion.Asociacion;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/componente/condicionespecial")
public class ComplementarCondicionEspecialAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -3055035001609597727L;
	
	private BigDecimal idToNegocio;
	private BigDecimal idToCotizacion;
	private CotizacionDTO cotizacionDTO;
	private EntidadService entidadService;
	private String codigoNombre;
	private NegocioCondicionEspecial negocioCondicionEspecial =  new NegocioCondicionEspecial();
	private CondicionEspecial condicionEspecial =  new CondicionEspecial();
	private List<CondicionEspecial> condicionesAsociadas = new ArrayList<CondicionEspecial>();
	private List<CondicionEspecial> condicionesDisponibles = new ArrayList<CondicionEspecial>();
	private List<SeccionDTO> secciones = new ArrayList<SeccionDTO>();
	private String idSeccion;
	private Negocio negocio = new Negocio();
	private Long idCondicionEspecial;
	private BigDecimal numeroInciso;
	private int esFiltrado;
	private String tipoLlamada;
	


	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	private NegocioSeccionService negocioSeccionService;

	@Autowired
	@Qualifier("condicionEspecialCotizacionServiceEJB")
	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	
	
	public ComplementarCondicionEspecialAction() {

	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void prepare() {
			if(idToCotizacion != null && idToNegocio == null){
				cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				idToNegocio = BigDecimal.valueOf(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
				ServletActionContext.getContext().getSession().put("idNegocio", idToNegocio);
				secciones = negocioSeccionService.listarSeccionNegocioByIdNegocio(idToNegocio);
				NivelAplicacion nivelAplicacion = numeroInciso == null ? NivelAplicacion.POLIZA : NivelAplicacion.INCISO;
				condicionesAsociadas = condicionEspecialCotizacionService.obtenerCondicionEspecial(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, idCondicionEspecial, Asociacion.ASOCIADA);
				
			}	
	}

	@Action(value = "mostrarContenedor", results = @Result(name = SUCCESS, location = "/jsp/componente/condicionespecial/componenteCondicionEspecial.jsp"))
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	@Action(value = "obtenerCondicionesDisponibles", results = @Result(name = SUCCESS, location = "/jsp/componente/condicionespecial/componenteCondicionEspecialDisponibleGrid.jsp"))
	public String obtenerCondicionesDisponibles() {
		NivelAplicacion nivelAplicacion = numeroInciso == null ? NivelAplicacion.POLIZA : NivelAplicacion.INCISO;
		if(StringUtil.isEmpty(codigoNombre)){
			condicionesDisponibles = condicionEspecialCotizacionService.obtenerCondicionEspecial(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, idCondicionEspecial, Asociacion.DISPONIBLE);
		}else{
			condicionesDisponibles = condicionEspecialCotizacionService.buscarCondicionesEspeciales(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, idCondicionEspecial, codigoNombre);
			esFiltrado = 1;
		}
		return SUCCESS;
	}

	@Action(value = "obtenerCondicionesAsociadas", results = @Result(name = SUCCESS, location = "/jsp/componente/condicionespecial/componenteCondicionEspecialAsociadoGrid.jsp"))
	public String obtenerCondicionesAsociadas() {
		NivelAplicacion nivelAplicacion = numeroInciso == null ? NivelAplicacion.POLIZA : NivelAplicacion.INCISO;
		condicionesAsociadas = condicionEspecialCotizacionService.obtenerCondicionEspecial(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, idCondicionEspecial, Asociacion.ASOCIADA);
		return SUCCESS;
	}

	@Action(value = "relacionarCondicionEspecial", results = {
									@Result(name = SUCCESS, location = "/jsp/componente/condicionespecial/componenteCondicionEspecial.jsp"),
									@Result(name = INPUT, location = "/jsp/componente/condicionespecial/componenteCondicionEspecial.jsp")
									}
			)
	public String relacionarCondicionEspecial() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(accion.equals("inserted")){
			condicionEspecialCotizacionService.agregarCondicionEspecial(idToCotizacion, numeroInciso, condicionEspecial.getId());
		}else if(accion.equals("deleted")){
			try{
				condicionEspecialCotizacionService.removerCondicionEspecial(idToCotizacion, numeroInciso, condicionEspecial.getId());				
			}catch(NegocioEJBExeption e){
				setMensajeError(e.getMessage());
				return INPUT;
			}

		}
		return SUCCESS;
	}
	
	@Action(value = "asociarTodas", results = {
								@Result(name = SUCCESS, location = "/jsp/componente/condicionespecial/componenteCondicionEspecial.jsp"),
								@Result(name = INPUT, location = "/jsp/componente/condicionespecial/componenteCondicionEspecial.jsp")
								}
			)
	public String asociarTodas() {
		String accion = ServletActionContext.getRequest().getParameter("accion");
		NivelAplicacion nivelAplicacion = numeroInciso == null ? NivelAplicacion.POLIZA : NivelAplicacion.INCISO;
		codigoNombre = esFiltrado == 0 ? null : codigoNombre;
		if(accion.equals("inserted")){
			condicionEspecialCotizacionService.relacionarTodas(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, codigoNombre);	
		}else if(accion.equals("deleted")){
			try{
				condicionEspecialCotizacionService.quitarTodas(idToCotizacion, numeroInciso, nivelAplicacion);
			}catch(NegocioEJBExeption e){
				setMensajeError(e.getMessage());
				return INPUT;
			}

		}
		return SUCCESS;
	}

	public List<SeccionDTO> getSecciones() {
		return secciones;
	}

	public void setSecciones(List<SeccionDTO> secciones) {
		this.secciones = secciones;
	}

	public List<CondicionEspecial> getCondicionesDisponibles() {
		return condicionesDisponibles;
	}

	public void setCondicionesDisponibles( List<CondicionEspecial> condicionesDisponibles) {
		this.condicionesDisponibles = condicionesDisponibles;
	}

	public String getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(String idSeccion) {
		this.idSeccion = idSeccion;
	}

	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public NegocioCondicionEspecial getNegocioCondicionEspecial() {
		return negocioCondicionEspecial;
	}

	public void setNegocioCondicionEspecial(
			NegocioCondicionEspecial negocioCondicionEspecial) {
		this.negocioCondicionEspecial = negocioCondicionEspecial;
	}

	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}

	public String getCodigoNombre() {
		return codigoNombre;
	}

	public void setCodigoNombre(String codigoNombre) {
		this.codigoNombre = codigoNombre;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public Long getIdCondicionEspecial() {
		return idCondicionEspecial;
	}

	public void setIdCondicionEspecial(Long idCondicionEspecial) {
		this.idCondicionEspecial = idCondicionEspecial;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public void setCondicionesAsociadas(List<CondicionEspecial> condicionesAsociadas) {
		this.condicionesAsociadas = condicionesAsociadas;
	}

	public CondicionEspecial getCondicionEspecial() {
		return condicionEspecial;
	}

	public void setCondicionEspecial(CondicionEspecial condicionEspecial) {
		this.condicionEspecial = condicionEspecial;
	}

	public List<CondicionEspecial> getCondicionesAsociadas() {
		return condicionesAsociadas;
	}

	public int getEsFiltrado() {
		return esFiltrado;
	}

	public void setEsFiltrado(int esFiltrado) {
		this.esFiltrado = esFiltrado;
	}

	public String getTipoLlamada() {
		return tipoLlamada;
	}

	public void setTipoLlamada(String tipoLlamada) {
		this.tipoLlamada = tipoLlamada;
	}

}