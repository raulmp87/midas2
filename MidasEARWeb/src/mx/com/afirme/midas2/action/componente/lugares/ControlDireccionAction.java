package mx.com.afirme.midas2.action.componente.lugares;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.ListadoService;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

@Namespace("/componente/direccion")
@Component
@Scope("prototype")
public class ControlDireccionAction extends BaseAction{

	private static final long serialVersionUID = 7706611140295732125L;

	private String idPaisName = "idPais";
	private String idPaisNacimientoName = "idPaisNacimiento";
	private String idEstadoName = "idEstado";
	private String idCiudadName = "idCiudad";
	private String idColoniaName = "idColonia";
	private String cpName = "cp";
	private String calleNumeroName = "calleNumero";
	private String numeroName = "Numero";
	
	private String idDomicilioName = "idDomicilio";
	private String idColoniaCheckName="idColoniaCheck";
	private String nuevaColoniaName="nuevaColonia";
	private String idColoniaString="idColoniaString";
	private Map<String, String> paises = new LinkedHashMap<String, String>();
	private Map<String, String> estados = new LinkedHashMap<String, String>();
	private Map<String, String> ciudades = new LinkedHashMap<String, String>();
	private Map<String, String> colonias = new LinkedHashMap<String, String>();
	private String idPais;
	private String cp;
	private String idEstado;
	private String idCiudad;
	private String idColonia;
	private String nuevaColonia;
	private String labelPais;
	private String labelEstado;
	private String labelCiudad;
	private String labelCalleNumero;
	private String labelColonia;
	private String labelCodigoPostal;
	private String labelPosicion;
	private String labelNuevaColonia;
	private String componente;
	private String calleNumero;
	private String idDomicilio;
	private String requerido;
	private String readOnly;
	private String enableSearchButton="true";
	private String funcionResult;
	private String idColoniaStr;
	private ListadoService listadoService;
	private String labelPaisNacimiento;
	private String idPaisNacimiento;
	private String numero;
	private String labelNumero;
	
		
	@Action(value="combosDireccion",
			results={@Result(name=SUCCESS,location="/jsp/componente/direccion.jsp"),
					 @Result(name=INPUT,location="/jsp/componente/direccion.jsp")})			
	public String cargarCombosLugares(){
				ValueStack valueStack = ActionContext.getContext().getValueStack();		
				cp = (String) valueStack.findValue(cpName, String.class);
				idPais = (String) valueStack.findValue(idPaisName, String.class);
				idEstado = (String) valueStack.findValue(idEstadoName, String.class);
				idCiudad = (String) valueStack.findValue(idCiudadName, String.class);
				idColonia = (String) valueStack.findValue(idColoniaName, String.class);				
				idDomicilio=(String) valueStack.findValue(idDomicilioName,String.class);
				calleNumero=(String) valueStack.findValue(calleNumeroName,String.class);
				nuevaColonia=(String) valueStack.findValue(nuevaColoniaName,String.class);
				idColoniaStr=(String) valueStack.findValue(idColoniaString,String.class);
				idPaisNacimiento=(String) valueStack.findValue(idPaisNacimientoName,String.class);
				numero=(String) valueStack.findValue(numeroName,String.class);
					if(StringUtils.isBlank(idPais)){
					idPais="PAMEXI";
				}
				paises = listadoService.getMapPaises();				
					estados = listadoService.getMapEstados(idPais);			
					if (StringUtils.isNotBlank(idEstado)) {
						ciudades = listadoService.getMapMunicipiosPorEstado(idEstado);						
						if (StringUtils.isNotBlank(idCiudad)) {
							colonias=listadoService.getMapColoniasSameValue(idCiudad);
							if (StringUtils.isNotBlank(idColoniaStr) && idColoniaStr.indexOf("-") > 0) {
								cp = listadoService.getCodigoPostal(idColoniaStr);
							}else if (StringUtils.isNotBlank(idCiudad)) {
								cp = listadoService.getCodigoPostalByColonyNameAndCityId(idColonia, idCiudad);
							}
						}
					}
				if(StringUtils.isBlank(readOnly)){
					readOnly="false";
				}else{
					if(readOnly.equals("true")){
						readOnly="true";
					}else{
						readOnly="false";
					}
				}
				if(StringUtils.isBlank(enableSearchButton)){
					enableSearchButton="true";
				}
				
				if(StringUtils.isBlank(funcionResult)){
					funcionResult="populateDomicilio";
				}
		return SUCCESS;
	}
	
	public String getIdPaisName() {
		return idPaisName;
	}

	public void setIdPaisName(String idPaisName) {
		this.idPaisName = idPaisName;
	}

	public String getIdEstadoName() {
		return idEstadoName;
	}

	public void setIdEstadoName(String idEstadoName) {
		this.idEstadoName = idEstadoName;
	}

	public String getIdCiudadName() {
		return idCiudadName;
	}

	public void setIdCiudadName(String idCiudadName) {
		this.idCiudadName = idCiudadName;
	}

	public String getIdColoniaName() {
		return idColoniaName;
	}

	public void setIdColoniaName(String idColoniaName) {
		this.idColoniaName = idColoniaName;
	}

	public String getCpName() {
		return cpName;
	}

	public void setCpName(String cpName) {
		this.cpName = cpName;
	}

	public String getIdPais() {
		return idPais;
	}

	public void setIdPais(String idPais) {
		this.idPais = idPais;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getIdCiudad() {
		return idCiudad;
	}

	public void setIdCiudad(String idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getIdColonia() {
		return idColonia;
	}

	public void setIdColonia(String idColonia) {
		this.idColonia = idColonia;
	}

	public Map<String, String> getPaises() {
		return paises;
	}

	public void setPaises(Map<String, String> paises) {
		this.paises = paises;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public Map<String, String> getCiudades() {
		return ciudades;
	}

	public void setCiudades(Map<String, String> ciudades) {
		this.ciudades = ciudades;
	}

	public Map<String, String> getColonias() {
		return colonias;
	}

	public void setColonias(Map<String, String> colonias) {
		this.colonias = colonias;
	}

	public String getLabelPais() {
		return labelPais;
	}

	public void setLabelPais(String labelPais) {
		this.labelPais = labelPais;
	}

	public String getLabelEstado() {
		return labelEstado;
	}

	public void setLabelEstado(String labelEstado) {
		this.labelEstado = labelEstado;
	}

	public String getLabelCiudad() {
		return labelCiudad;
	}

	public void setLabelCiudad(String labelCiudad) {
		this.labelCiudad = labelCiudad;
	}

	public String getComponente() {
		return componente;
	}

	public void setComponente(String componente) {
		this.componente = componente;
	}

	public String getLabelPosicion() {
		return labelPosicion;
	}

	public void setLabelPosicion(String labelPosicion) {
		this.labelPosicion = labelPosicion;
	}

	public String getCalleNumero() {
		return calleNumero;
	}

	public void setCalleNumero(String calleNumero) {
		this.calleNumero = calleNumero;
	}

	public String getCalleNumeroName() {
		return calleNumeroName;
	}

	public void setCalleNumeroName(String calleNumeroName) {
		this.calleNumeroName = calleNumeroName;
	}

	public String getIdDomicilioName() {
		return idDomicilioName;
	}

	public void setIdDomicilioName(String idDomicilioName) {
		this.idDomicilioName = idDomicilioName;
	}

	public String getIdDomicilio() {
		return idDomicilio;
	}

	public void setIdDomicilio(String idDomicilio) {
		this.idDomicilio = idDomicilio;
	}

	public String getLabelCalleNumero() {
		return labelCalleNumero;
	}

	public void setLabelCalleNumero(String labelCalleNumero) {
		this.labelCalleNumero = labelCalleNumero;
	}

	public String getLabelColonia() {
		return labelColonia;
	}

	public void setLabelColonia(String labelColonia) {
		this.labelColonia = labelColonia;
	}

	public String getLabelCodigoPostal() {
		return labelCodigoPostal;
	}

	public void setLabelCodigoPostal(String labelCodigoPostal) {
		this.labelCodigoPostal = labelCodigoPostal;
	}

	public String getRequerido() {
		return requerido;
	}

	public void setRequerido(String requerido) {
		this.requerido = requerido;
	}

	public String getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}

	public String getEnableSearchButton() {
		return enableSearchButton;
	}

	public void setEnableSearchButton(String enableSearchButton) {
		this.enableSearchButton = enableSearchButton;
	}
	
	public String getFuncionResult() {
		return funcionResult;
	}

	public void setFuncionResult(String funcionResult) {
		this.funcionResult = funcionResult;
	}

	public String getNuevaColoniaName() {
		return nuevaColoniaName;
	}

	public void setNuevaColoniaName(String nuevaColoniaName) {
		this.nuevaColoniaName = nuevaColoniaName;
	}

	public String getLabelNuevaColonia() {
		return labelNuevaColonia;
	}

	public void setLabelNuevaColonia(String labelNuevaColonia) {
		this.labelNuevaColonia = labelNuevaColonia;
	}

	public String getNuevaColonia() {
		return nuevaColonia;
	}

	public void setNuevaColonia(String nuevaColonia) {
		this.nuevaColonia = nuevaColonia;
	}

	public String getIdColoniaCheckName() {
		return idColoniaCheckName;
	}

	public void setIdColoniaCheckName(String idColoniaCheckName) {
		this.idColoniaCheckName = idColoniaCheckName;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public String getIdColoniaString() {
		return idColoniaString;
	}

	public void setIdColoniaString(String idColoniaString) {
		this.idColoniaString = idColoniaString;
	}

	public String getIdColoniaStr() {
		return idColoniaStr;
	}

	public void setIdColoniaStr(String idColoniaStr) {
		this.idColoniaStr = idColoniaStr;
	}



	public String getLabelPaisNacimiento() {
		return labelPaisNacimiento;
	}

	public void setLabelPaisNacimiento(String labelPaisNacimiento) {
		this.labelPaisNacimiento = labelPaisNacimiento;
	}

	public String getIdPaisNacimiento() {
		return idPaisNacimiento;
	}

	public void setIdPaisNacimiento(String idPaisNacimiento) {
		this.idPaisNacimiento = idPaisNacimiento;
	}

	public String getIdPaisNacimientoName() {
		return idPaisNacimientoName;
	}

	public void setIdPaisNacimientoName(String idPaisNacimientoName) {
		this.idPaisNacimientoName = idPaisNacimientoName;
	}

	public String getNumeroName() {
		return numeroName;
	}

	public void setNumeroName(String numeroName) {
		this.numeroName = numeroName;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLabelNumero() {
		return labelNumero;
	}

	public void setLabelNumero(String labelNumero) {
		this.labelNumero = labelNumero;
	}
	
}
