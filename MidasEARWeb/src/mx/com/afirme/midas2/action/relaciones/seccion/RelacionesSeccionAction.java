package mx.com.afirme.midas2.action.relaciones.seccion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.relaciones.RelacionesSeccionService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class RelacionesSeccionAction extends ActionSupport implements
		Preparable {

	private static final long serialVersionUID = -2759458017001470529L;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}
	public String mostrar() {
		return SUCCESS;
	}
	
	public String obtenerPaquetesAsociados() {
		paquetesAsociados = relacionesSeccionService.obtenerPaquetesAsociados(idSeccion);
		return SUCCESS;
	}


	public String obtenerPaquetesDisponibles() {
		paquetesDisponibles = relacionesSeccionService.obtenerPaquetesDisponibles(idSeccion);
		return SUCCESS;
	}
	
	
	public String accionSobrePaquetesAsociados() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = relacionesSeccionService.relacionarPaquetes(accion, idSeccion, idPaquete);
		return SUCCESS;
	}

	
	private BigDecimal idSeccion;
	
	private RespuestaGridRelacionDTO respuesta;
	
	private RelacionesSeccionService relacionesSeccionService;
	
	private List<Paquete> paquetesAsociados;
	
	private List<Paquete> paquetesDisponibles;
		
	private Long idPaquete;

	

		
	public BigDecimal getIdSeccion() {
		return idSeccion;
	}
	
	
	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}
	
	
	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}
	
	
	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}
	
	
	public Long getIdPaquete() {
		return idPaquete;
	}
	
	
	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}
	
	
	public List<Paquete> getPaquetesAsociados() {
		return paquetesAsociados;
	}
	
	
	public List<Paquete> getPaquetesDisponibles() {
		return paquetesDisponibles;
	}
	
	@Autowired
	@Qualifier("relacionesSeccionEJB")
	public void setRelacionesSeccionService(
			RelacionesSeccionService relacionesSeccionService) {
		this.relacionesSeccionService = relacionesSeccionService;
	}
	
	
}
