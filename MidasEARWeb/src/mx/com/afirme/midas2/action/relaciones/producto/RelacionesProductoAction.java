package mx.com.afirme.midas2.action.relaciones.producto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.relaciones.RelacionesProductoService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class RelacionesProductoAction extends ActionSupport implements Preparable {

	private static final long serialVersionUID = 3633201369927619558L;

	@Override
	public void prepare() throws Exception {
		if (formasPagoAsociadas == null) formasPagoAsociadas = new ArrayList<FormaPagoDTO>();
		if (formasPagoDisponibles == null) formasPagoDisponibles = new ArrayList<FormaPagoDTO>();
		
		if (mediosPagoAsociadas == null) mediosPagoAsociadas = new ArrayList<MedioPagoDTO>();
		if (mediosPagoDisponibles == null) mediosPagoDisponibles = new ArrayList<MedioPagoDTO>();
	}
	
	
	public String mostrar() {
		return SUCCESS;
	}
	
	public String obtenerFormasPagoAsociadas() {
		formasPagoAsociadas = relacionesProductoService.obtenerFormasPagoAsociadas(idProducto);
		return SUCCESS;
	}


	public String obtenerFormasPagoDisponibles() {
		formasPagoDisponibles = relacionesProductoService.obtenerFormasPagoDisponibles(idProducto);
		return SUCCESS;
	}
	
	
	public String accionSobreFormasPagoAsociadas() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = relacionesProductoService.relacionarFormasPago(accion, idProducto, idFormaPago);
		return SUCCESS;
	}
	
	public String obtenerMediosPagoAsociadas() {
		mediosPagoAsociadas = relacionesProductoService.obtenerMediosPagoAsociadas(idProducto);
		return SUCCESS;
	}


	public String obtenerMediosPagoDisponibles() {
		mediosPagoDisponibles = relacionesProductoService.obtenerMediosPagoDisponibles(idProducto);
		return SUCCESS;
	}
	
	
	public String accionSobreMediosPagoAsociadas() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = relacionesProductoService.relacionarMediosPago(accion, idProducto, idMedioPago);
		return SUCCESS;
	}
	
	
	private BigDecimal idProducto;
	
	private RespuestaGridRelacionDTO respuesta;
	
	private RelacionesProductoService relacionesProductoService;
	
	private List<FormaPagoDTO> formasPagoAsociadas;
	
	private List<FormaPagoDTO> formasPagoDisponibles;
		
	private Integer idFormaPago;
	
	private List<MedioPagoDTO> mediosPagoAsociadas;
	
	private List<MedioPagoDTO> mediosPagoDisponibles;
		
	private Integer idMedioPago;
	
	
	public BigDecimal getIdProducto() {
		return idProducto;
	}


	public void setIdProducto(BigDecimal idProducto) {
		this.idProducto = idProducto;
	}

	
	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}


	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}


	public List<FormaPagoDTO> getFormasPagoAsociadas() {
		return formasPagoAsociadas;
	}

	
	public List<FormaPagoDTO> getFormasPagoDisponibles() {
		return formasPagoDisponibles;
	}

	
	public Integer getIdFormaPago() {
		return idFormaPago;
	}


	public void setIdFormaPago(Integer idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public Integer getIdMedioPago() {
		return idMedioPago;
	}


	public void setIdMedioPago(Integer idMedioPago) {
		this.idMedioPago = idMedioPago;
	}


	public List<MedioPagoDTO> getMediosPagoAsociadas() {
		return mediosPagoAsociadas;
	}


	public List<MedioPagoDTO> getMediosPagoDisponibles() {
		return mediosPagoDisponibles;
	}


	@Autowired
	@Qualifier("relacionesProductoEJB")
	public void setRelacionesProductoService(
			RelacionesProductoService relacionesProductoService) {
		this.relacionesProductoService = relacionesProductoService;
	}


	

}
