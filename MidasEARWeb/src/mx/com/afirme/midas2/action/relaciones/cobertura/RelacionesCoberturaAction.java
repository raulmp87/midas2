package mx.com.afirme.midas2.action.relaciones.cobertura;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.relaciones.RelacionesCoberturaService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class RelacionesCoberturaAction extends ActionSupport implements Preparable {

	private static final long serialVersionUID = 5782986622823121722L;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public String mostrar() {
		return SUCCESS;
	}
	
	public String obtenerGruposVariablesAsociados() {
		gruposAsociados = relacionesCoberturaService.obtenerGruposVariablesAsociados(idSeccion, idCobertura);
		return SUCCESS;
	}


	public String obtenerGruposVariablesDisponibles() {
		gruposDisponibles = relacionesCoberturaService.obtenerGruposVariablesDisponibles(idSeccion,  idCobertura);
		return SUCCESS;
	}
	
	
	public String accionSobreGruposVariablesAsociados() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = relacionesCoberturaService.relacionarGruposVariables(accion, idSeccion, idCobertura, idGrupoVariables);
		return SUCCESS;
	}

	
	private BigDecimal idCobertura;
	private BigDecimal idSeccion;
	
	private RespuestaGridRelacionDTO respuesta;
	
	private RelacionesCoberturaService relacionesCoberturaService;
	
	private List<GrupoVariablesModificacionPrima> gruposAsociados;
	
	private List<GrupoVariablesModificacionPrima> gruposDisponibles;
		
	private Long idGrupoVariables;

	public BigDecimal getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(BigDecimal idCobertura) {
		this.idCobertura = idCobertura;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public Long getIdGrupoVariables() {
		return idGrupoVariables;
	}

	public void setIdGrupoVariables(Long idGrupoVariables) {
		this.idGrupoVariables = idGrupoVariables;
	}
	
	public List<GrupoVariablesModificacionPrima> getGruposAsociados() {
		return gruposAsociados;
	}

	public List<GrupoVariablesModificacionPrima> getGruposDisponibles() {
		return gruposDisponibles;
	}

		
	@Autowired
	@Qualifier("relacionesCoberturaEJB")
	public void setRelacionesCoberturaService(
			RelacionesCoberturaService relacionesCoberturaService) {
		this.relacionesCoberturaService = relacionesCoberturaService;
	}

	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}

	public BigDecimal getIdSeccion() {
		return idSeccion;
	}
	
	
}
