/**
 * 
 */
package mx.com.afirme.midas2.action.informacionVehicular;

import java.util.LinkedList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

/**
 * @author jreyes
 *
 */
@SuppressWarnings("serial")
@Component
@Scope("prototype")
@Namespace("/informacionvehicular")
public class InformacionVehicularAction extends BaseAction implements Preparable {

	
	private String numeroVin;
	private List<Respuesta> listVehiculos;
	
	@Autowired
	@Qualifier("informacionVehicularServiceEJB")
	private InformacionVehicularService informacionVehicularService;
		
	public InformacionVehicularService getInformacionVehicularService() {
		return informacionVehicularService;
	}

	public void setInformacionVehicularService(
			InformacionVehicularService informacionVehicularService) {
		this.informacionVehicularService = informacionVehicularService;
	}

	public String getNumeroVin() {
		return numeroVin;
	}

	public void setNumeroVin(String numeroVin) {
		this.numeroVin = numeroVin;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}

	@Action(value="init", results={@Result(name=SUCCESS, location="/jsp/informacionvehicular/init.jsp")})
	public String init(){
		return SUCCESS;
	}
	
	@Action(value="show", results={@Result(name=SUCCESS, location="/jsp/informacionvehicular/listGrid.jsp")})
	public String show(){
		listVehiculos = new LinkedList<Respuesta>();
		return SUCCESS;
	}

	@Action(value="data", results={@Result(name=SUCCESS, location="/jsp/informacionvehicular/listGrid.jsp")})
	public String getData(){
		//listVehiculos = informacionVehicularService.getConsultaVin(numeroVin);
		return SUCCESS;
	}
	
}
