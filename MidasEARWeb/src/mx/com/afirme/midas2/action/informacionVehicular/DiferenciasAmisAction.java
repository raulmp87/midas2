package mx.com.afirme.midas2.action.informacionVehicular;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.diferenciasAmis.DiferenciasAmisService;
import mx.com.afirme.midas2.util.MidasException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class DiferenciasAmisAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<DiferenciasAmis> diferenciasAmisList = new ArrayList<DiferenciasAmis>();
	private List<Agente> agenteList = new ArrayList<Agente>(1);
	private String idParam1; //id
	private String idParam2; //idToPpoliza
    private String descripcionBusquedaAgente = null;
	private boolean agenteControlDeshabilitado; 
	
	private String fechaIni, fechaFin, polizaNum, agenteId, estatus; //busqueda por filtros

	private ListadoService listadoService;
	private DiferenciasAmisService diferenciasAmisService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("diferenciasAmisServiceEJB")
	public void setDiferenciasAmisService(
			DiferenciasAmisService diferenciasAmisService) {
		this.diferenciasAmisService = diferenciasAmisService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}
	
	public String listar(){		
		return SUCCESS;
	}
	
	public String buscarDiferenciasAmis(){		

		String mensaje = "";
		
		try {
			diferenciasAmisList = diferenciasAmisService.traeLista();
			if ( diferenciasAmisList.size() == 0 ){
				mensaje = "No existen polizas con diferencias.";
			}
			
		} catch (MidasException e) {
			
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		
		super.setMensaje(mensaje);
		
		return SUCCESS;
	}
	
	public String buscarDiferenciasAmisFiltros(){		

		String mensaje = "";
		
		try {
			diferenciasAmisList = diferenciasAmisService.traeListaFiltros( this.construyeFiltro() );
			if ( diferenciasAmisList.size() == 0 ){
				mensaje = "No existen polizas con diferencias.";
			}
			
		} catch (MidasException e) {
			
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		
		super.setMensaje(mensaje);
		
		return SUCCESS;
	}
	
	private DiferenciasAmis construyeFiltro(){
		
		DiferenciasAmis filtro = new DiferenciasAmis();
		try{
			
			filtro.setFechaIni( this.getDateFromString(fechaIni) );
			filtro.setFechaFin( this.getDateFromString(fechaFin) );
			filtro.setNumeroPoliza( (polizaNum == null || polizaNum.equals("")) ? new BigDecimal(0) :  new BigDecimal(polizaNum));
			filtro.setAgenteId( (agenteId == null || agenteId.equals("")) ? 0L : Long.parseLong(agenteId) );
			filtro.setEstatus( (estatus == null || estatus.equals("")) ? 0 : Short.parseShort(estatus) );
			
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return filtro;
		
	}
	
	private Date getDateFromString( String fechaStr ){
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		
		try{
			if ( fechaStr != null && !fechaStr.equals("") ){
				
				date = formatter.parse(fechaStr);
			}
		}
		catch( Exception e ){
			e.printStackTrace();
		}
		return date;
	}
	
	public String solicitarCambio(){
		
		String mensaje = "";
		DiferenciasAmis diferenciasAmisObj = new DiferenciasAmis();
		Long idLong;
		
		try {
			
			idLong = Long.parseLong( idParam1 );
			diferenciasAmisObj = diferenciasAmisService.traerDiferenciasObject( idLong );
			
			diferenciasAmisService.solicitarCambio( diferenciasAmisObj );
			//diferenciasAmisList = diferenciasAmisService.traeLista(); 
			this.estatus = "1"; //estatus por default: 1
			diferenciasAmisList = diferenciasAmisService.traeListaFiltros( this.construyeFiltro() );
			
			try {
				this.enviarNotificacion( diferenciasAmisObj );
			} catch (Exception e) {
				super.setMensajeError(MENSAJE_ERROR_GENERAL);
				return MENSAJE_ERROR_GENERAL;
			}
			
		} catch (MidasException e) {
			
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return MENSAJE_ERROR_GENERAL;
		}
		
		super.setMensaje(mensaje);
		
		return SUCCESS;
		
	}
	
	private void enviarNotificacion( DiferenciasAmis diferenciasAmisObj ) throws Exception{
		
		List<String> addressList = new ArrayList<String>();
		String nombreDestinatario = "";
		Agente agente = new Agente();
		String incisoFormateado;
		
		try{
			agente = diferenciasAmisService.traerAgenteObject( diferenciasAmisObj.getIdToCotizacion() );
			
			nombreDestinatario = agente.getPersona().getNombreCompleto();
			
			addressList.add(agente.getPersona().getEmail());
			
			incisoFormateado = this.traerIncisoFormateado(diferenciasAmisObj);
			
			mailService.sendMail(addressList, getText("midas.emision.autos.correo.titulo"), 
					getText("midas.emision.autos.correo.cuerpo", new String[]{incisoFormateado}), null, 
					getText("midas.emision.autos.correo.titulo"), getText("midas.emision.autos.correo.saludo", new String[]{nombreDestinatario}));
			
		}
		catch( Exception e ){
			throw e;			
		}
		
	}
	
	private String traerIncisoFormateado( DiferenciasAmis diferenciasAmisObj ) throws Exception {
		
		String result = "";
		
		result = result + "Poliza:&nbsp;" + idParam2 + "<br>";
		result = result + "Inciso:&nbsp;" + diferenciasAmisObj.getNumeroInciso() + "<br>";
		result = result + "Numero de serie:&nbsp;" + diferenciasAmisObj.getNumeroSerie() + "<br>";
		result = result + "Version:&nbsp;" + diferenciasAmisObj.getVersion() + "<br>";
		result = result + "Marca:&nbsp;" + diferenciasAmisObj.getMarca() + "<br>";
		result = result + "Modelo:&nbsp;" + diferenciasAmisObj.getAnos() + "<br>";
		result = result + "Puertas:&nbsp;" + diferenciasAmisObj.getPuertas() + "<br>";
		result = result + "Cilindros:&nbsp;" + diferenciasAmisObj.getCilindros() + "<br>";
		result = result + "Combustible:&nbsp;" + diferenciasAmisObj.getCombustible() + "<br>";
		result = result + "Interiores:&nbsp;" + diferenciasAmisObj.getInteriores() + "<br>";
		result = result + "Pasajeros:&nbsp;" + diferenciasAmisObj.getPasajeros();		
		
		return result;
		
	}
	
	public String ventanaBusqueda() {
		return SUCCESS;
	}

	public void prepareBuscarAgente() {
		if (descripcionBusquedaAgente != null) {
			agenteList = listadoService.listaAgentesPorDescripcion(descripcionBusquedaAgente.trim().toUpperCase());
		} else {
			agenteList = new ArrayList<Agente>();
		}		
	}
	
	public String buscarAgente() {
		return SUCCESS;
	}

	public List<DiferenciasAmis> getDiferenciasAmisList() {
		return diferenciasAmisList;
	}

	public void setDiferenciasAmisList(List<DiferenciasAmis> diferenciasAmisList) {
		this.diferenciasAmisList = diferenciasAmisList;
	}

	public String getIdParam1() {
		return idParam1;
	}

	public void setIdParam1(String idParam1) {
		this.idParam1 = idParam1;
	}

	public String getIdParam2() {
		return idParam2;
	}

	public void setIdParam2(String idParam2) {
		this.idParam2 = idParam2;
	}

	public boolean isAgenteControlDeshabilitado() {
		return agenteControlDeshabilitado;
	}

	public void setAgenteControlDeshabilitado(boolean agenteControlDeshabilitado) {
		this.agenteControlDeshabilitado = agenteControlDeshabilitado;
	}

	public String getDescripcionBusquedaAgente() {
		return descripcionBusquedaAgente;
	}

	public void setDescripcionBusquedaAgente(String descripcionBusquedaAgente) {
		this.descripcionBusquedaAgente = descripcionBusquedaAgente;
	}

	public List<Agente> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(List<Agente> agenteList) {
		this.agenteList = agenteList;
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getPolizaNum() {
		return polizaNum;
	}

	public void setPolizaNum(String polizaNum) {
		this.polizaNum = polizaNum;
	}

	public String getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
}
