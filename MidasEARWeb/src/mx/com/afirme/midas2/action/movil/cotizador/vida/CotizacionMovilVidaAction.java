package mx.com.afirme.midas2.action.movil.cotizador.vida;

import java.io.File;

import mx.com.afirme.midas2.service.movil.cotizador.vida.CotizacionMovilVidaService;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.vida.domain.movil.cotizador.CrearCotizacionVidaParameterDTO;
import mx.com.afirme.vida.domain.movil.cotizador.OccupationDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizacionMovilVidaAction extends BaseAction implements
		Preparable, ModelDriven<Object> {

	private static final long serialVersionUID = -3831211473840721273L;
	
	Long id;
	private Object model;
	private Object data;
	File fileUpload;
	@Autowired
	CotizacionMovilVidaService cotizacionMovilVidaService;
	
	public String cotizarVida() {
		try {
			data = cotizacionMovilVidaService.cotizar((CrearCotizacionVidaParameterDTO)model);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return SUCCESS;
	}
	
	public String cargarSumaAsegurada() {
		data = cotizacionMovilVidaService.obtenerSumaAseguradaList();
		return SUCCESS;
		
	}

	public String cargarOcupacion(){
		data=cotizacionMovilVidaService.getGiroOcupacion((OccupationDTO)model);
		return SUCCESS;
	}
	
	public String cargarOcupacionPorId(){
		data=cotizacionMovilVidaService.getOcupacionPorId(id);
		return SUCCESS;
	}

	public void prepare() throws Exception {

	}

	public void prepareCotizarVida() {
		model = new CrearCotizacionVidaParameterDTO();
	}
	
	public void prepareCargarOcupacion(){
    	model= new OccupationDTO();
    	
    }

	public Object getModel() {
		return model;
	}

	public void setModel(Object model) {
		this.model = model;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}
}