package mx.com.afirme.midas2.action.movil.cliente.polizas;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.domain.movil.cliente.InfoPolizaParameter;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;
//import mx.com.afirme.midas2.domain.movil.cliente.NotificacionCliente;
//import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ClientePolizasAction extends BaseAction implements Preparable, ModelDriven<Object> {

	private static final long serialVersionUID = 83047975925908977L;
	@Autowired
	private ClientePolizasService polizasService;
	
	@Autowired
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	
	public MailService mailService;
	protected EntidadService entidadService;	
	
	private Object model;
	private Object data;
	private String tipo;
	private String username;
	private String polizaNo;
	private String polizaID;
	private String polizaSize;
	//private String correo;
	private String tipoConsulta;
	///tipoConsulta=1 se envia la caratula.
	///tipoConsulta=2 se envia la Recibo y XML.
	private String correoAgente;
	private String URL;
	private String llaveFiscal;
	private String tipoPoliza;
	private String isNotificationPage;
	private String clave;
	private Long nivel;
	private NumeroPolizaCompleto numeroPoliza;
	private Long idPadre;
	private String imei;
	private boolean consultaPoliza;
	private Long idNotificacionRenov;
	private InputStream genericInputStream;
	private String contentType;
	private String fileName;
	private byte[] fileBytes;
	
	/**
	 * @return the consultaPoliza
	 */
	public boolean isConsultaPoliza() {
		return consultaPoliza;
	}

	/**
	 * @param consultaPoliza the consultaPoliza to set
	 */
	public void setConsultaPoliza(boolean consultaPoliza) {
		this.consultaPoliza = consultaPoliza;
	}

	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * @param imei the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}

	/**
	 * @return the idPadre
	 */
	public Long getIdPadre() {
		return idPadre;
	}

	/**
	 * @param idPadre the idPadre to set
	 */
	public void setIdPadre(Long idPadre) {
		this.idPadre = idPadre;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public Long getNivel() {
		return nivel;
	}

	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}
	
	/**
	 * @return the numeroPoliza
	 */
	public NumeroPolizaCompleto getNumeroPoliza() {
		return numeroPoliza;
	}
	/**
	 * @param numeroPoliza the numeroPoliza to set
	 */
	public void setNumeroPoliza(NumeroPolizaCompleto numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public String getIsNotificationPage() {
		return isNotificationPage;
	}

	public void setIsNotificationPage(String isNotificationPage) {
		this.isNotificationPage = isNotificationPage;
	}
	public String getTipoPoliza() {
		return tipoPoliza;
	}
	public void setTipoPoliza(String tipoPoliza) {
		this.tipoPoliza = tipoPoliza;
	}
	public String getLlaveFiscal() {
		return llaveFiscal;
	}
	public void setLlaveFiscal(String llaveFiscal) {
		this.llaveFiscal = llaveFiscal;
	}
	public String getURL() {
		return URL;
	}
	public String getCorreoAgente() {
		return correoAgente;
	}
	public void setCorreoAgente(String correoAgente) {
		this.correoAgente = correoAgente;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	@Autowired
	@Qualifier("mailServiceEJB")
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	@Autowired
	@Qualifier("generarPlantillaBitemporalServiceEJB")
    public void setGeneraPlantillaReporteBitemporalService(GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService){
    	this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService; 
    }
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService){
		this.entidadService = entidadService;
	}
	/*public String getCorreo() {
		return correo;
	}
	
	public void setCorreo(String correo) {
		this.correo = correo;
	}*/

	public String getPolizaSize() {
		return polizaSize;
	}

	public void setPolizaSize(String polizaSize) {
		this.polizaSize = polizaSize;
	}

	public String getPolizaID() {
		return polizaID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPolizaID(String polizaID) {
		this.polizaID = polizaID;
	}

	public String getPolizaNo() {
		return polizaNo;
	}

	public void setPolizaNo(String polizaNo) {
		this.polizaNo = polizaNo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Long getIdNotificacionRenov() {
		return idNotificacionRenov;
	}
	public void setIdNotificacionRenov(Long idNotificacionRenov) {
		this.idNotificacionRenov = idNotificacionRenov;
	}
	public String buscarPolizas(){
		data = polizasService.buscarPolizas((InfoPolizaParameter) model);	
		return SUCCESS;
	}
	
	public String buscarPolizasPorUsuario(){
		data = polizasService.buscarPolizasPorUsuario();	
		return SUCCESS;
	}
	
	public String buscarPolizasByUsuario(){
		data = polizasService.buscarPolizasPorUsuario(this.username);	
		return SUCCESS;
	}
	
	public String getSiniestrosByTipoProducto(){
		data = polizasService.getSiniestrosByTipoProducto(this.clave, this.nivel);	
		return SUCCESS;
	}
	
	public String getSiniestrosByPadre(){
		data = polizasService.getSiniestrosByPadre(this.idPadre);	
		return SUCCESS;
	}
	
	@Override
	public Object getModel() {
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void prepareGuardarPoliza(){
		model = new InfoPolizaParameter();
	}
	public void prepareBuscarPolizas(){
		model = new InfoPolizaParameter();
	}
	
	public String guardarPoliza(){
		polizasService.guardarPoliza((InfoPolizaParameter) model);		
		return SUCCESS;	
	}
	public String validateGuardar() {
		data = polizasService.validateGuardar((InfoPolizaParameter) model);	
		return SUCCESS;	
	}
	public String existePoliza() {
		data = polizasService.existePoliza(polizaNo);
		return SUCCESS;
	}
	
	public void prepareActualizarDatos(){
		model = new InfoPolizaParameter();
	}
	public void prepareValidateGuardar(){
		model = new InfoPolizaParameter();
	}
	
	public String actualizarDatos(){
		polizasService.actualizarPolizasClienteMovil((InfoPolizaParameter) model);
		//polizasService.actualizarDatos((InfoPolizaParameter) model);
		return SUCCESS;
	}
	
	public void prepareExisteRegistro(){
		model = new InfoPolizaParameter();
	}

	public String enviarCaratula(){
		//usuario=usuarioService.getUsuarioActual();
		EnvioCaratulaParameter param = new EnvioCaratulaParameter();
		//param.setCorreo(correo);
		param.setPolizaID(polizaID);
		param.setURL(URL);
		//param.setPolizaMidas(isMidas.equals("true"));
		param.setMailService(mailService);
		param.setEntidadService(entidadService);
		param.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
		param.setUsername(username);	
		param.setTipoPoliza(tipoPoliza);
		//para pruebas con usuario
		if (StringUtils.isNotBlank(correoAgente)) {
			param.setMailAgente(correoAgente);
		}		
		polizasService.enviarCaratula(param);
		return SUCCESS;
	}
	
	public String mostrarRecibos(){
		if(isNotificationPage != null){
			this.mostrarNotficaciones();
		}else{
			LOG.info("clientePolizas.Estatus>>"+ClientePolizas.EstatusPolizasMovil.CARATULA);
			if(tipoConsulta.equals(String.valueOf(ClientePolizas.EstatusPolizasMovil.CARATULA))){
				this.enviarCaratula();
			}else if(tipoConsulta.equals(String.valueOf(ClientePolizas.EstatusPolizasMovil.RECIBO))){
				if(StringUtils.isNotBlank(llaveFiscal)){
					this.envioRecibos();
				}else{
					data = polizasService.mostrarRecibos(polizaID);
				}
			}
		}

		return SUCCESS;
	}
	//enviarDocumentosRenovacion
	public String enviarDocumentosRenovacion(){
		LOG.info("Entra a enviarDocumentosRenovacionAction");
		//usuario=usuarioService.getUsuarioActual();
		EnvioCaratulaParameter param = new EnvioCaratulaParameter();
		param.setPolizaID(polizaID);
		param.setMailService(mailService);
		param.setEntidadService(entidadService);
		param.setIdNotificacionRenov(idNotificacionRenov);
		param.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
		polizasService.enviarDocumentosRenovacion(param);
		return SUCCESS;
	}
	public String eliminarPoliza(){
		polizasService.eliminarPoliza(polizaID);
		return SUCCESS;
	}
	
	public String infoPoliza(){
		data = polizasService.infoPoliza(polizaID);
		return SUCCESS;
	}
	/*
	 
	 ErrorBuilder eb = new ErrorBuilder();
		List<ClientePolizas> lista = null;
		if ((polizaNo == null) || (StringUtils.isBlank(polizaNo))) {
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
					"Requerido"));
		} 
	 * */
	public String bloqueaUsuario(){
		polizasService.bloqueaUsuario(username);
		return SUCCESS;
	}
	
	public String desbloqueaUsuario(){		
		polizasService.desbloqueaUsuario(username);
		return SUCCESS;
	}
	
	public String solicitudDesbloqueo(){
		//    usuario=usuarioService.getUsuarioActual();
		EnvioCaratulaParameter params = new EnvioCaratulaParameter();
		
		params.setUsername(username);
		params.setPolizaID(polizaID);
		params.setMailService(mailService);
		params.setEntidadService(entidadService);
		params.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
		//params.setPolizaMidas(isMidas.equals("true"));
		
		polizasService.solicitudDesbloqueo(params);
		return SUCCESS;
	}
	
	/*public String existeRegistro(){
		data = polizasService.getNotification();
		return SUCCESS;
	}*/
	
	public String envioRecibos(){
		EnvioCaratulaParameter params = new EnvioCaratulaParameter();
		params.setMailService(mailService);
		params.setEntidadService(entidadService);
		params.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
		params.setLlaveFiscal(llaveFiscal);
		
		polizasService.envioRecibos(params);
		return SUCCESS;
	}
	
	/**
	 * Mostrar las notificaciones
	 * @return
	 */
	public String mostrarNotficaciones(){
		data = polizasService.getNotification();
		return SUCCESS;
	}
	
	public String validarEstatusUsuarioActual(){
		data=polizasService.validarEstatusUsuarioActual();
		return SUCCESS;
	}
	
	///////////////PRUEBA DE NOTIFICACION//////
	//@Schedule(minute="*/10", hour="10", dayOfMonth="*", month="*", year="*", persistent= false)
	public String enviarMensajeTest(){
		LogDeMidasWeb.log(this.getClass().getName()+"Entrando a enviarMensajeTest()", 
    			Level.INFO, null);
		try {
			final List<String> registrationId = usuarioService.getRegistrationIds(usuarioService.getUsuarioActual().getNombreUsuario());
			if(CollectionUtils.isNotEmpty(registrationId)) {		
				final Builder builder = new Builder();
				builder.addData("message",  "Tiene una nuava notificacion de Cobranza Afirme");
				builder.addData("evento", "Mi evento Cobranza");
				builder.addData("title", "Cobranza Afirme");
				final Sender sender = new Sender(sistemaContext.getClienteMovilSenderKey());//Se obtiene despues de registrar la app en la consola de desarrolladores de google
				sender.send(builder.build(), registrationId, 0);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public String enviarNotificacionesCliente(){
		LogDeMidasWeb.log(this.getClass().getName()+"Entrando a envioNotificacionesCliente()", 
    			Level.INFO, null);
		ErrorBuilder eb = new ErrorBuilder();
		try{
			polizasService.envioNotificacionesCliente();
			/*List <NotificacionCliente> envioNotificacionesList= polizasService.getEnviarNotificacion();
			for (NotificacionCliente notificacionTmp : envioNotificacionesList) {
				final List<String> registrationIdList = usuarioService.getRegistrationIds(notificacionTmp.getUsuario());
				if(!registrationIdList.isEmpty()){
					for(String idsRegistration : registrationIdList){
							String idRegistration = idsRegistration.toString();
						String registrationIdAndroid = idRegistration.substring(2);
						String resultRegistrationId=registrationIdAndroid;
						String so = String.valueOf(idRegistration.charAt(0)) ;
						if(so.equals("A")){
							final Builder builder = new Builder();
							builder.addData("message",  notificacionTmp.getMensaje());
							builder.addData("evento","Mensaje Cobranza");
							builder.addData("title", notificacionTmp.getTipoNotificacion());
							final Sender sender = new Sender(sistemaContext.getClienteMovilSenderKey());//Se obtiene despues de registrar la app en la consola de desarrolladores de google
							sender.send(builder.build(), resultRegistrationId, 0);
							}else if(so.equals("I")){
								throw new ApplicationException(eb.addFieldError("Sistema Operativo",
								"Sistema Operativo IOs no Encontrado"));
						}
				    }
				}else{
					throw new ApplicationException(eb.addFieldError("RegistrationId",
					"RegistrationId No encontrado"));
				}
			}*/
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}

	public String getTypePoliza(){
		if (numeroPoliza != null) {
			this.setTipoPoliza(polizasService.getTipoProductoPoliza(numeroPoliza));
		} else{
			addFieldError("numeroPoliza", "numeroPoliza es requerido");
			return INPUT;
		}
		
		return SUCCESS;
	}
	public String descargaPoliza(){
		PolizasClienteMovil polizasClienteMovil= polizasService.findById(new Long(polizaID));
	    try {
	    	fileBytes = polizasService.generarPdf(polizasClienteMovil); 
			setGenericInputStream(new ByteArrayInputStream(fileBytes));
			setContentType("application/pdf");
			setFileName("CaratulaPoliza_"+polizasClienteMovil.getPolizaDTO().getNumeroPolizaFormateada()+".pdf");
			}catch(Exception e){
				LOG.error("Ocurrio un error al descargar documentacion de renovacion movil", e);
				setMensaje("Error al imprimir favor de intentarlo mas tarde");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
			return SUCCESS;	
	}
	
	public String validateGuardarPolizaMigrada() {
		data = polizasService.validateGuardarPolizaMigrada((InfoPolizaParameter) model);	
		return SUCCESS;	
	}
	
	public void prepareValidateGuardarPolizaMigrada(){
		model = new InfoPolizaParameter();
	}
	
	public String addPolizasMigradasToUser(){
		LogDeMidasWeb.log(this.getClass().getName()+"--> addPolizasMigradasToUser() imei:"+this.imei+" userName:"+this.username, Level.INFO, null);
		
		try {
			polizasService.addPolizasMigradasToUser(this.imei, this.username);
		} catch(Exception e){
			addFieldError("polizasMigradas", "Ocurrió un error inesperado al agregar las pólizas migradas para el user:"+this.username+" con el imei:"+this.imei);
		}
		
		return SUCCESS;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	public InputStream getGenericInputStream() {
		return genericInputStream;
	}
	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
}