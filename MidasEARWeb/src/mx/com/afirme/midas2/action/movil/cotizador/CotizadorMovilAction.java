package mx.com.afirme.midas2.action.movil.cotizador;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.SolicitudCotizacionMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionClavePromo;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionEstatusUsuarioActual;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilDTO;
import mx.com.afirme.midas2.impresionesM2.GenerarReportes;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizadorMovilAction extends BaseAction implements Preparable,
		ModelDriven<Object> {
	
	/** Log de CotizadorMovilAction */
	private static final Logger LOG = Logger
			.getLogger(CotizadorMovilAction.class);
	/*** serialVersionUID */
	private static final long serialVersionUID = -2207742727541479942L;
	
	Long id;
	@Autowired
	CotizacionMovilService tarifasService;
	private EntidadService entidadService;
	private Object model;
	private Object data;
	File fileUpload;
	public String getSucess() {
		return SUCCESS;
	}
	public String cargarMarcas() {
		data = tarifasService.getMarcas();
		return SUCCESS;
	}

	public String cargarModelos() {
		data = tarifasService.getModelos(id);
		return SUCCESS;
	}

	public String cargarTiposVehiculo() {
		data = tarifasService.getTipos(id);
		return SUCCESS;
	}

	public String cargarEstadoVehiculo() {
		data = tarifasService.getEstados(id);
		return SUCCESS;
	}

	public String cargarDescripcionVehiculo() {
		data = tarifasService.getDescripcionVehiculo(id);
		return SUCCESS;
	}

	public String mostrarCotizacionAuto() {
		CotizacionMovilDTO cotizacionMovil;
		cotizacionMovil = tarifasService.getCotizacionAuto((SolicitudCotizacionMovil) model);
		if (cotizacionMovil != null) {
			if(cotizacionMovil.getUsuario()!=null&&cotizacionMovil.getIdtocotizacionmidas()!=null){
				try {
					enviarCotizacionPorCorreo((SolicitudCotizacionMovil) model,
							cotizacionMovil);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}
			cotizacionMovil.setUsuario(null);
			data = cotizacionMovil;
		} 

		return SUCCESS;
	}
	public String validarClavePromo(){
		data=tarifasService.validarClavePromo((ValidacionClavePromo)model);
		return SUCCESS;
	}
	public String validarEstatusUsuarioActual(){
		  data=tarifasService.validarEstatusUsuarioActual((SolicitudCotizacionMovil) model);
		return SUCCESS;
	}
	public String enviarCotizacionPorCorreo(SolicitudCotizacionMovil model,
			CotizacionMovilDTO cotizacionMovil) {
		ByteArrayAttachment attachment = null;
		GenerarReportes generarReportes = new GenerarReportes();
		String [] correosDestinatarios = null;
		List<String> destinatarios = new ArrayList<String>();
		List<String> destinatariosAgente = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		List<ResumenCotMovilDTO> cotizacionMovilList = new ArrayList<ResumenCotMovilDTO>();
		Locale locale = getLocale();
		try {
			Usuario usuario  = cotizacionMovil.getUsuario();
			if (cotizacionMovil != null) {
				cotizacionMovilList = tarifasService
						.inicializarDatosParaImpresion(model, cotizacionMovil);
			}
			generarReportes.setEntidadService(entidadService);
			attachment = new ByteArrayAttachment("CotizacionMovil.pdf",
					TipoArchivo.PDF, generarReportes.imprimirCotizacionMovil(
							cotizacionMovilList, locale, usuario, model));
			if (attachment != null) {
				String nombreCliente=UtileriasWeb.regresaObjectEsNuloParametro(usuario.getNombreCompleto().toLowerCase(), " ").toString();
				String MensajeAgente = "Nos complace informarle que el prospecto: "+" "+ nombreCliente.toLowerCase() +" "+"a utilizado nuestra aplicaci\u00F3n M\u00F3vil de Cotizaci\u00F3n y deseamos "
						+ "le de seguimiento para el cierre de su venta, la informaci\u00F3n correspondiente se encuentra en el documento Anexo "
						+ "Favor de comunicarse con su prospecto lo mas r\u00E1pido posible.";
				correosDestinatarios = cotizacionMovil.getDescuentoAgente().getEmail().split(";");
				for(int i=0;i<=correosDestinatarios.length-1;i++){
					destinatariosAgente.add(correosDestinatarios[i]);
				}
				destinatarios.add(usuario.getEmail());
				adjuntos.add(attachment);
				/* correo a cliente */
				mailService.sendMail(
						destinatarios,
						usuario.getNombre()
								.concat("-")
								.concat(cotizacionMovil.getIdtocotizacionmidas()
										.toString()),
						getText("midas.correo.cotizacion.cuerpo"),
						adjuntos,
						getText("midas.correo.cotizacion.titulo"),
						getText("midas.correo.cotizacion.saludo",
								new String[] { usuario.getNombre() }));
				/* correo a Agente */
				mailService.sendMail(
						destinatariosAgente,
						cotizacionMovil
								.getDescuentoAgente()
								.getNombre()
								.concat("-")
								.concat(cotizacionMovil.getIdtocotizacionmidas()
										.toString()),
						MensajeAgente,
						adjuntos,
						getText("midas.correo.cotizacion.titulo"),
						getText("midas.correo.cotizacion.saludo",
								new String[] { cotizacionMovil
										.getDescuentoAgente().getNombre() }));
			}
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			setMensaje("Error al enviar el correo favor de intentarlo mas tarde"
					+ ex);
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;
	}

	@Override
	public Object getModel() {
		return model;
	}

	public void setModel(Object model) {
		this.model = model;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	@Override
	public void prepare() throws Exception {
	}

	public void prepareMostrarCotizacionAuto() {
		model = new SolicitudCotizacionMovil();
	}
	public void prepareValidarClavePromo(){
    	model= new ValidacionClavePromo();
    }
    public void prepareValidarEstatusUsuarioActual(){
    	model= new SolicitudCotizacionMovil();
    }
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public File getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(File fileUpload) {
		this.fileUpload = fileUpload;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

}