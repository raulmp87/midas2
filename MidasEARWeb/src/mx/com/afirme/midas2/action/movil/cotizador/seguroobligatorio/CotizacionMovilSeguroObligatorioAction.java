package mx.com.afirme.midas2.action.movil.cotizador.seguroobligatorio;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Locale;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cotizador.seguroobligatorio.CotizacionMovilSeguroObligatorioService;
import mx.com.afirme.midas2.service.seguridad.EncryptorService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizacionMovilSeguroObligatorioAction extends BaseAction implements Preparable, ModelDriven<Object>{

	private static final long serialVersionUID = 8580842177842427071L;
	private static final Logger LOG = Logger.getLogger(CotizacionMovilSeguroObligatorioAction.class);
	
	private String id;
	private Object model;
	private Object data;
	private String isPoliza;
	private String idToCotizacion;
	private Integer idMedioPago;
	private String email;
	private String idCatalogo;
	private CotizacionDTO cotizacion;
	
	private InputStream genericInputStream;
	private String      contentType;
	private String     fileName;
	private String tipoTarjeta;
	private CuentaPagoDTO cuentaPagoDTO;
	private Integer idBanco;
	private String idConductoCobro;
	private BigDecimal numeroInciso;
	private BigDecimal idToPersonaContratante;
	private Locale locale = getLocale();
	
	private BigDecimal idMarca;
	private String claveTarifa;
	private String idEstado;
	private String qs;
	private ClienteGenericoDTO filtroCliente;
	private String isMovil;
	
	@Override
	public Object getModel() {
		return model;
	}
	
	public CotizacionMovilSeguroObligatorioAction(){
		
	}

	@Override
	public void prepare() throws Exception {
		if(!StringUtil.isEmpty(idToCotizacion)){
			cotizacion = entidadService.findById(CotizacionDTO.class, 
					BigDecimal.valueOf(Long.valueOf(idToCotizacion)));
		}
		
		if(!StringUtil.isEmpty(qs)){
			encryptorService.setValuesByEncriptedParams(this, qs);
		}
	}

	public void prepareGuardarCobranza() {
		model = new CuentaPagoDTO();
	}
	
	public String getListCotizacionSO(){
		data = cotizacionMovilSO.getListCotizacionSeguroObligatorio(id, isPoliza);
		return SUCCESS;
	}
	
	public String getDatosInicialesCSO(){
		data = cotizacionMovilSO.getDatosInicialesCSO( id );
		return SUCCESS;
	}
	
	public String getDatosCobranza(){
		data = cotizacionMovilSO.getDatosCobranza(id, idMedioPago);
		return SUCCESS;
	}

	public String getDatosConducto(){
		data = cotizacionMovilSO.getDatosConducto(id, idToPersonaContratante, idConductoCobro);
		return SUCCESS;
	}

	public String getDatosTC(){
		data = cotizacionMovilSO.getDatosTC((CuentaPagoDTO)model, idMedioPago);
		return SUCCESS;
	}
	
	public String guardarCobranza(){
		data = cotizacionMovilSO.guardarCobranza(id, idMedioPago, tipoTarjeta, idBanco, (CuentaPagoDTO)model);
		return SUCCESS;
	}
	
	public String validarDatosComplementarios() {
		data = cotizacionMovilSO.validarListaParaEmitir(id);
		return SUCCESS;
	}
	
	public String getCatalogoGeneral(){
		LOG.info("Catalogo"+idCatalogo);
		data = cotizacionMovilSO.getCatalogo(idCatalogo, id);
		return SUCCESS;
	}
	
	public String getResumenCosto(){
		data = cotizacionMovilSO.getResumenCosto(id, isPoliza, numeroInciso);
		return SUCCESS;
	}
	
	public String imprimirPdf() {
		TransporteImpresionDTO transporte = cotizacionMovilSO.getPDFSeguroObligatorio(id, isPoliza, locale, isMovil);
		genericInputStream = new ByteArrayInputStream(transporte.getByteArray());
		contentType = transporte.getContentType();
		fileName = transporte.getFileName();

		return SUCCESS;
	}
	
	public String getCertificado(){
		data = cotizacionMovilSO.generaPlantillaPoliza(id, isPoliza, locale);
		
		return SUCCESS;
	}

	public void prepareGetTarifasServicioPublico() {
		model = new TarifaServicioPublico();
	}
	
	public String getTarifasServicioPublico(){
		data = cotizacionMovilSO.getTarifasServicioPublico((TarifaServicioPublico) model);
		return SUCCESS;
	}
	
	public String getListMarcasByNegSeccion(){
		data = cotizacionMovilSO.getListMarcasByNegSeccion(BigDecimal.valueOf(Long.valueOf(id)));
		return SUCCESS;
	}
	
	public String getListModelosByMarca(){
		data = cotizacionMovilSO.getListModelosByMarca(BigDecimal.valueOf(Long.valueOf(id)), idMarca);
		return SUCCESS;
	}
	
	public void prepareGenerarCotizacionServicioPublico() {
		model = new CotizacionDTO();
	}
	
	public String generarCotizacionServicioPublico(){
		data = cotizacionMovilSO.generarCotizacionServicioPublico((CotizacionDTO) model, 
				BigDecimal.valueOf(Long.valueOf(id)), claveTarifa, idEstado);
		return SUCCESS;
	}
	
	public void prepareSaveCotizacion() {
		model = new IncisoCotizacionDTO();
	}
	
	public String saveCotizacion(){
		data = cotizacionMovilSO.saveCotizacion((IncisoCotizacionDTO)model, id);
		return SUCCESS;
	}

	public String envioPDFEmail() {
		data = cotizacionMovilSO.envioPDFEmail(id, locale, isPoliza);	
		return SUCCESS;
	}

	public String emitir() {
		data = cotizacionMovilSO.emitir(id);	
		return SUCCESS;
	}
		
	public void prepareBuscarCliente(){
		model = new ClienteDTO();
	}
	
	public String buscarCliente(){
		data = cotizacionMovilSO.buscarCliente((ClienteDTO) model);
		return SUCCESS;
	}
	
	public void prepareGuardarCliente(){
		model = new ClienteGenericoDTO();
	}
	
	public String guardarCliente(){
		data = cotizacionMovilSO.guardarCliente((ClienteGenericoDTO) model);
		return SUCCESS;
	}

	public String getClienteByCotizacion(){
		if (cotizacion != null && cotizacion.getIdToPersonaContratante() != null){
			LOG.info("--> cotizacion.getIdToPersonaContratante():"+cotizacion.getIdToPersonaContratante());
			data = cotizacionMovilSO.getClienteById(cotizacion.getIdToPersonaContratante(), cotizacion.getCodigoUsuarioCotizacion());
		}

		return SUCCESS;
	}
	
	public String getIncisoAutoCotByIdCotizacion(){
		if (cotizacion != null){
			data = cotizacionMovilSO.getIncisoAutoCotByIdCotizacion(id);
		}
		
		return SUCCESS;
	}
	
	public String getDatosCotizacion(){
		if (cotizacion != null){
			data = cotizacionMovilSO.getDatosCotizacion(cotizacion);
		}
		
		return SUCCESS;
	}

	public void prepareBuscarEstilo(){
		model = new IncisoAutoCot();
	}
	
	public String buscarEstilo(){
		data = cotizacionMovilSO.getEstiloPorMarcaNegocioyDescripcion(idToCotizacion, (IncisoAutoCot)model);
		return SUCCESS;
	}
	
	public String getConductosCobroCotizacion(){
		data = cotizacionMovilSO.getConductosCobroCotizacion(id);
		return SUCCESS;
	}
	
	@Autowired
	@Qualifier("cotizacionMovilSeguroObligatorioServiceEJB")
	private CotizacionMovilSeguroObligatorioService cotizacionMovilSO;

	private EncryptorService encryptorService;

	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("encryptorServiceEJB")
	public void setEncryptorService(EncryptorService encryptorService) {
		this.encryptorService = encryptorService;
	}
	
	public void setCotizacionMovilSO(
			CotizacionMovilSeguroObligatorioService cotizacionMovilSO) {
		this.cotizacionMovilSO = cotizacionMovilSO;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIsPoliza() {
		return isPoliza;
	}

	public void setIsPoliza(String isPoliza) {
		this.isPoliza = isPoliza;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setModel(Object model) {
		this.model = model;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}
	
	public Integer getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(Integer idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public CuentaPagoDTO getCuentaPagoDTO() {
		return cuentaPagoDTO;
	}

	public void setCuentaPagoDTO(CuentaPagoDTO cuentaPagoDTO) {
		this.cuentaPagoDTO = cuentaPagoDTO;
	}

	public Integer getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}

	public String getIdConductoCobro() {
		return idConductoCobro;
	}

	public void setIdConductoCobro(String idConductoCobro) {
		this.idConductoCobro = idConductoCobro;
	}

	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public String getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(String idCatalogo) {
		this.idCatalogo = idCatalogo;
	}

	public BigDecimal getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(BigDecimal idMarca) {
		this.idMarca = idMarca;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getClaveTarifa() {
		return claveTarifa;
	}

	public void setClaveTarifa(String claveTarifa) {
		this.claveTarifa = claveTarifa;
	}

	public String getQs() {
		return qs;
	}

	public void setQs(String qs) {
		this.qs = qs;
	}

	public ClienteGenericoDTO getFiltroCliente() {
		return filtroCliente;
	}

	public void setFiltroCliente(ClienteGenericoDTO filtroCliente) {
		this.filtroCliente = filtroCliente;
	}

	public String getIsMovil() {
		return isMovil;
	}

	public void setIsMovil(String isMovil) {
		this.isMovil = isMovil;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}	
}