package mx.com.afirme.midas2.action.movil.ajustador;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Level;


import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.movil.ajustador.Bien;
import mx.com.afirme.midas2.domain.movil.ajustador.CoberturaIncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.DireccionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.IncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarContactoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarTerminacionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Persona;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.SaveDocumentoSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Tercero;
import mx.com.afirme.midas2.domain.movil.ajustador.TerceroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Vehiculo;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilMidasService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AjustadorMovilMidasAction extends BaseAction implements Preparable, ModelDriven<Object> {
	
	@Autowired
	private AjustadorMovilMidasService ajustadorMovilMidasService;
	@Autowired
	private AjustadorEstatusService ajustadorEstatusService;

	private Object data;
	private Object model;
	private String id;
	private String tipo;
	private String tipoEstimacion;
	private String numeroSerie;
	
	private byte[] fileBytes;
	private InputStream genericInputStream;
	private String contentType;
	private String fileName;
	
	@Override
	public Object getModel() {
		return model;
	}
	
	
	public static class MostrarPolizaParameter {
		private String id;
		private Long idReporte;
		private Date fechaOcurrido;
		
		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public Date getFechaOcurrido() {
			return fechaOcurrido;
		}
		
		public void setFechaOcurrido(Date fechaOcurrido) {
			this.fechaOcurrido = fechaOcurrido;
		}
		public Long getIdReporte() {
			return this.idReporte;
		}

		public void setIdReporte(Long idReporte) {
			this.idReporte = idReporte;
		}
	}
	
	public static class ActualizarPosicionParameter {
		private Long idAjustador; 
		private Double latitud;
		private Double longitud;
		
		public Long getIdAjustador() {
			return idAjustador;
		}
		
		public void setIdAjustador(Long idAjustador) {
			this.idAjustador = idAjustador;
		}
		
		public Double getLatitud() {
			return latitud;
		}
		
		public void setLatitud(Double latitud) {
			this.latitud = latitud;
		}
		
		public Double getLongitud() {
			return longitud;
		}
		
		public void setLongitud(Double longitud) {
			this.longitud = longitud;
		}
	}
	
	public static class SolicitarValuacionParameter {
		private Long reporteCabinaId; 
		private String codigoValuador;
		public Long getReporteCabinaId() {
			return reporteCabinaId;
		}
		public void setReporteCabinaId(Long reporteCabinaId) {
			this.reporteCabinaId = reporteCabinaId;
		}
		public String getCodigoValuador() {
			return codigoValuador;
		}
		public void setCodigoValuador(String codigoValuador) {
			this.codigoValuador = codigoValuador;
		}
	}
	public static class OrdenLlegadaCiaParameter {
		private Long idReporte;
		private Integer idCia;
		private Integer orden;
		
		public Long getIdReporte() {
			return idReporte;
		}
		public void setIdReporte(Long idReporte) {
			this.idReporte = idReporte;
		}
		public Integer getIdCia() {
			return idCia;
		}
		public void setIdCia(Integer idCia) {
			this.idCia = idCia;
		}
		public Integer getOrden() {
			return orden;
		}
		public void setOrden(Integer orden) {
			this.orden = orden;
		}
	}
	public String buscarReporteSiniestrosAsignados() {
		data = ajustadorMovilMidasService.getReporteSiniestrosAsignados(id);
		return SUCCESS;
	}

	public String mostrarReporteSiniestro() {
		data = ajustadorMovilMidasService.getReporteSiniestro(Long.parseLong(id));
		return SUCCESS;
	}
	
	public String mostrarReporteSiniestroAsignado() {
		data = ajustadorMovilMidasService.getReporteSiniestroAsignado(id, tipo);
		return SUCCESS;
	}

	public String buscarPolizas() {
		data = ajustadorMovilMidasService.findPoliza((FindPolizaParameter) model);
		return SUCCESS;
	}
	public String asignarInciso() {
		ajustadorMovilMidasService.asignarInciso((IncisoParameter) model, this.getUsuarioActual().getNombreUsuario());		
		return SUCCESS;		
	}
	public void prepareBuscarPolizas() {
		model = new FindPolizaParameter();
	}
	public void prepareAsignarInciso() {
		model = new IncisoParameter();
	}

	public String coberturasAfecta() {
		data = ajustadorMovilMidasService.getCoberturasAfecta((IncisoParameter) model);		
		return SUCCESS;
	}
	public String marcarContacto() {
		ajustadorMovilMidasService.marcarContacto((MarcarContactoParameter) model);		
		return SUCCESS;		
	}
	public String catalogo(){
		data = ajustadorMovilMidasService.getCatalogoSimple(id);
		return SUCCESS;
	}
	public String municipios() {
		data = ajustadorMovilMidasService.getMunicipios((DireccionParameter)model);
		return SUCCESS;
	}
	public String tiposSiniestro(){  
		data = ajustadorMovilMidasService.getTiposSiniestro(id);
		return SUCCESS;
	}
	public String responsabilidades(){ 
		data = ajustadorMovilMidasService.getResponsabilidad("tipo_responsabilidad",((ReporteSiniestroParameter) model).getTipoSiniestro());
		return SUCCESS;
	}
	public String terminosDeAjuste(){ 
		data = ajustadorMovilMidasService.getTerminosAjuste(((ReporteSiniestroParameter) model).getTipoSiniestro(),
				((ReporteSiniestroParameter) model).getResponsabilidad());
		return SUCCESS;
	}
	public String tiposPases(){
		data = ajustadorMovilMidasService.getTiposPases((CoberturaIncisoParameter)model);
		return SUCCESS;
	}
	public String infoCp() {
		data = ajustadorMovilMidasService.getInfoCP((DireccionParameter)model);
		return SUCCESS;
	}
	public String mostrarPoliza() {
		MostrarPolizaParameter parameter = (MostrarPolizaParameter) model;
		data = ajustadorMovilMidasService.getPoliza(parameter.getId(),parameter.getIdReporte(), parameter.getFechaOcurrido());
		return SUCCESS;
	}
	public String marcarTerminacion() {
		ajustadorMovilMidasService.marcarTerminacion((MarcarTerminacionParameter) model);
		return SUCCESS;		
	}
	public String terceros(){
		data = ajustadorMovilMidasService.getTerceros((CoberturaIncisoParameter)model);
		return SUCCESS;
	}
	public String tercero(){
		data = ajustadorMovilMidasService.getTercero((TerceroParameter)model);
		return SUCCESS;
	}
	public String guardaInfoGralReporte() {
		ajustadorMovilMidasService.guardaInfoGralReporte((ReporteSiniestroParameter) model);		
		return SUCCESS;		
	}
	
	public String convierteReclamacion(){
		data = ajustadorMovilMidasService.convertirReclamacion((ReporteSiniestroParameter)model, this.getUsuarioActual().getNombreUsuario());
		return SUCCESS;
	}
	public String saveDocumentoSiniestro() {
		ajustadorMovilMidasService.saveDocumentoSiniestro((SaveDocumentoSiniestroParameter) model);
		return SUCCESS;
	}	
	public String getTiposDocumentosSiniestro() {
		data = ajustadorMovilMidasService.getTiposDocumentosSiniestro();
		return SUCCESS;
	}
	
	public String imprimirDeclaracionSiniestro(){
		try{
				fileBytes = ajustadorMovilMidasService.getBytesPase((TerceroParameter)model, "DDS", this.getUsuarioActual().getNombreCompleto());
				setGenericInputStream(new ByteArrayInputStream(fileBytes));
				setContentType("application/pdf");
				setFileName("DeclaracionSiniestro.pdf");
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase de declaracion de siniestro.",Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}	
	
	public String imprimirDeclaracionSiniestroTxt(){
		try{
				data = ajustadorMovilMidasService.generarImpresion((TerceroParameter)model, "DDS", this.getUsuarioActual().getNombreCompleto());
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase de declaracion de siniestro.",Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;	
	}
	
	public String imprimirValeGrua(){
		try{
				fileBytes = ajustadorMovilMidasService.getBytesPase((TerceroParameter)model, "VDG", this.getUsuarioActual().getNombreCompleto());
				genericInputStream = new ByteArrayInputStream(fileBytes);
				contentType = "application/pdf";
				fileName = "ValeDeGrua.pdf";
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase de vale de grua.",Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}
	
	public String imprimirValeGruaTxt(){
		try{
				data = ajustadorMovilMidasService.generarImpresion((TerceroParameter)model, "VDG", this.getUsuarioActual().getNombreCompleto());
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase de vale de grua.",Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}
	
	public String imprimirPase(){
		try{

				fileBytes = ajustadorMovilMidasService.getBytesPase((TerceroParameter)model, tipo, this.getUsuarioActual().getNombreCompleto());
				genericInputStream = new ByteArrayInputStream(fileBytes);
				contentType = "application/pdf";
				fileName = "PaseAtencion.pdf";
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase "+fileName,Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}
	
	public String imprimirPaseTxt(){
		try{
				data = ajustadorMovilMidasService.generarImpresion((TerceroParameter)model, tipo, this.getUsuarioActual().getNombreCompleto());
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase "+fileName,Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}
	
	public String afectaReserva(){
		data = ajustadorMovilMidasService.afectaReserva(model, this.getUsuarioActual().getNombreUsuario());
		return SUCCESS;
	}
	
	public String actualizarPosicion() {
		ActualizarPosicionParameter actualizarPosicionParam = (ActualizarPosicionParameter) model;		
		ajustadorEstatusService.guardarAjustadorEstatusMovil(actualizarPosicionParam.getIdAjustador(), 
				actualizarPosicionParam.getLatitud(), actualizarPosicionParam.getLongitud(), true);
		return SUCCESS;
	}
	
	public String confirmarEnteradoAsignacion() {
		ajustadorMovilMidasService.confirmarEnteradoAsignacion(((ReporteSiniestroParameter) model).getId(), ((ReporteSiniestroParameter) model).getAjustadorId() );
		return SUCCESS;
	}
	public String condicionesEspeciales() {
		MostrarPolizaParameter parameter = (MostrarPolizaParameter) model;
		data = ajustadorMovilMidasService.obtenerCondicionesEspeciales(parameter.getId(), parameter.getIdReporte());
		LOG.info("Condiciones Especiales :" + data);
		return SUCCESS;
	}
	public String solicitarValuacion(){
		SolicitarValuacionParameter parameter = (SolicitarValuacionParameter) model;
		data = ajustadorMovilMidasService.guardarValuacion(parameter.getReporteCabinaId(), parameter.getCodigoValuador());
		return SUCCESS;
	}
	public String talleres() {
		data = ajustadorMovilMidasService.getTalleres((PrestadorServicioFiltro)model);
		return SUCCESS;
	}
	//listadoSiniestros
	public String listadoSiniestros() {
		data = ajustadorMovilMidasService.getListadoSiniestros(numeroSerie);
		return SUCCESS;
	}
	//tieneAntiguedadNumSerie
	public String tieneAntiguedadNumSerie() {
		data = ajustadorMovilMidasService.getTieneAntiguedadNumSerie(id,numeroSerie);
		return SUCCESS;
	}
	//solicitarAutorizacion
	public String solicitarAutorizacion() {
		data = ajustadorMovilMidasService.solicitarAutorizacionVigencia(Long.parseLong(id));
		return SUCCESS;
	}
	//guardarOrdenDeLlegada
	public String guardarOrdenDeLlegada() {
		OrdenLlegadaCiaParameter parameter = (OrdenLlegadaCiaParameter) model;
		ajustadorMovilMidasService.guardarLlegadaCia(parameter.getIdReporte(), parameter.getIdCia(), parameter.getOrden());
		return SUCCESS;
	}
	//obtenerListaLegadaCompañia
	public String obtenerListaLlegadaCia() {
		data = ajustadorMovilMidasService.llegadaCiasReporte(Long.parseLong(id));
		LOG.info("Lista llegada Cia :" + data);
		return SUCCESS;
	}
	// catalogo de sap amis supervision de campo
	public String listadoSapAmisSupervisionDeCampo() {
		data = ajustadorMovilMidasService.getListadoSapAmisSupervisionDeCampo();
		return SUCCESS;
	}
	//eliminarElementoListCia
	public String eliminarElementoListCia() {
		ajustadorMovilMidasService.eliminarLlegadaCia(Long.parseLong(id));
		return SUCCESS;
	}
	@Override
	public void prepare() throws Exception {
	}	
	public void prepareMostrarPoliza() {
		model = new MostrarPolizaParameter();
	}
	public void prepareCoberturasAfecta() {
		model = new IncisoParameter();
	}
	public void prepareMarcarContacto() {
		model = new MarcarContactoParameter();
	}
	public void prepareMunicipios() {
		model = new DireccionParameter();
	}
	public void prepareResponsabilidades() {
		model = new ReporteSiniestroParameter();
	}
	public void prepareTerminosDeAjuste() {
		model = new ReporteSiniestroParameter();
	}
	public void prepareTiposPases(){
		model = new CoberturaIncisoParameter();
	}
	public void prepareInfoCp() {
		model = new DireccionParameter();
	}
	public void prepareConvierteReclamacion(){
		model = new ReporteSiniestroParameter();
	}
	public void prepareActualizarPosicion() {
		model = new ActualizarPosicionParameter();
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void prepareMarcarTerminacion() {
		model = new MarcarTerminacionParameter();
	}
	public void prepareTerceros() {
		model = new CoberturaIncisoParameter();
	}
	public void prepareGuardaInfoGralReporte() {
		model = new ReporteSiniestroParameter();
	}
	public void prepareSaveDocumentoSiniestro() {
		model = new SaveDocumentoSiniestroParameter();
	}
	public void prepareTercero() {
		model = new TerceroParameter();
	}
	public void prepareImprimirDeclaracionSiniestro() {
		prepareTercero();
	}
	public void prepareImprimirValeGrua() {
		prepareTercero();
	}
	public void prepareImprimirPase() {
		prepareTercero();
	}
	public void prepareImprimirDeclaracionSiniestroTxt() {
		prepareTercero();
	}
	public void prepareImprimirValeGruaTxt() {
		prepareTercero();
	}
	public void prepareImprimirPaseTxt() {
		prepareTercero();
	}
	public void prepareAfectaReserva(){
		if(tipoEstimacion.equals("RCP") || tipoEstimacion.equals("RCJ") || tipoEstimacion.equals("GME") || tipoEstimacion.equals("GMC")){
			model = new Persona();
		} else if(tipoEstimacion.equals("RCB")){
			model = new Bien();
		} else if(tipoEstimacion.equals("DMA") || tipoEstimacion.equals("RCV")){
			model = new Vehiculo();
		} else {
			model = new Tercero();
		}		
	}
	public void prepareConfirmarEnteradoAsignacion() {
		model = new ReporteSiniestroParameter();
	}	
	public void prepareCondicionesEspeciales() {
		model = new MostrarPolizaParameter();
	}
	public void prepareSolicitarValuacion() {
		model = new SolicitarValuacionParameter();
	}
	public void prepareGuardarOrdenDeLlegada() {
		model = new OrdenLlegadaCiaParameter();
	}
	public void prepareTalleres() {
		model = new PrestadorServicioFiltro();
	}	
	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
	
	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipoEstimacion() {
		return tipoEstimacion;
	}

	public void setTipoEstimacion(String tipoEstimacion) {
		this.tipoEstimacion = tipoEstimacion;
	}
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}
}