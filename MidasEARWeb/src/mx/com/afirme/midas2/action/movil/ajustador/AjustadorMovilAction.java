package mx.com.afirme.midas2.action.movil.ajustador;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.movil.ajustador.Bien;
import mx.com.afirme.midas2.domain.movil.ajustador.CoberturaIncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.DireccionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.IncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarContactoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarTerminacionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Persona;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.SaveDocumentoSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Tercero;
import mx.com.afirme.midas2.domain.movil.ajustador.TerceroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Vehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus_;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AjustadorMovilAction extends BaseAction implements Preparable, ModelDriven<Object> {
	
	@Autowired
	private AjustadorMovilService ajustadorMovilService;
	@Autowired
	private AjustadorEstatusService ajustadorEstatusService;
	
	private String id;
	private Object model;
	private Object data;
	private String tipo;
	private String idTercero;
	
	private byte[] fileBytes;
	private InputStream genericInputStream;
	private String contentType;
	private String fileName;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getIdTercero() {
		return idTercero;
	}

	public void setIdTercero(String idTercero) {
		this.idTercero = idTercero;
	}

	public Object getData() {
		return data;
	}
	
	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public static class MostrarPolizaParameter {
		private String id;
		private Date fechaOcurrido;
		
		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public Date getFechaOcurrido() {
			return fechaOcurrido;
		}
		
		public void setFechaOcurrido(Date fechaOcurrido) {
			this.fechaOcurrido = fechaOcurrido;
		}
	}
	
	public static class ActualizarPosicionParameter {
		private Long idAjustador; 
		private Double latitud;
		private Double longitud;
		
		public Long getIdAjustador() {
			return idAjustador;
		}
		
		public void setIdAjustador(Long idAjustador) {
			this.idAjustador = idAjustador;
		}
		
		public Double getLatitud() {
			return latitud;
		}
		
		public void setLatitud(Double latitud) {
			this.latitud = latitud;
		}
		
		public Double getLongitud() {
			return longitud;
		}
		
		public void setLongitud(Double longitud) {
			this.longitud = longitud;
		}
	}

	@Override
	public void prepare() throws Exception {
	}

	public void prepareAsignarInciso() {
		model = new IncisoParameter();
	}
	
	public void prepareMarcarContacto() {
		model = new MarcarContactoParameter();
	}
	
	public void prepareMarcarTerminacion() {
		model = new MarcarTerminacionParameter();
	}
	
	public void prepareBuscarPolizas() {
		model = new FindPolizaParameter();
	}
		
	public void prepareMostrarPoliza() {
		model = new MostrarPolizaParameter();
	}
	
	public void prepareConfirmarEnteradoAsignacion() {
		model = new ReporteSiniestroParameter();
	}

	public void prepareTiposSiniestro() {
		model = new ReporteSiniestroParameter();
	}
	
	public void prepareTerminosDeAjuste() {
		model = new ReporteSiniestroParameter();
	}
	
	public void prepareResponsabilidades() {
		model = new ReporteSiniestroParameter();
	}
	
	public void prepareGuardaInfoGralReporte() {
		model = new ReporteSiniestroParameter();
	}
	
	public void prepareCoberturasAfecta() {
		model = new IncisoParameter();
	}
	
	public void prepareTerceros() {
		model = new CoberturaIncisoParameter();
	}
	
	public void prepareMunicipios() {
		model = new DireccionParameter();
	}
	
	public void prepareInfoCp() {
		model = new DireccionParameter();
	}
	
	public void prepareTercero() {
		model = new TerceroParameter();
	}
	
	public void prepareConvierteReclamacion(){
		model = new ReporteSiniestroParameter();
	}
	
	public void prepareActualizarPosicion() {
		model = new ActualizarPosicionParameter();
	}
	
	public void prepareAfectaReserva(){
		if(tipo.equals("PERS") || tipo.equals("PERS_ASEG")){
			model = new Persona();
		} else if(tipo.equals("BIEN")){
			model = new Bien();
		} else if(tipo.equals("VEH") || tipo.equals("ASEG")){
			model = new Vehiculo();
		} else {
			model = new Tercero();
		}		
	}
	
	public void prepareTiposPases(){
		model = new CoberturaIncisoParameter();
	}
	
	public void prepareSaveDocumentoSiniestro() {
		model = new SaveDocumentoSiniestroParameter();
	}
	
	@Override
	public Object getModel() {
		return model;
	}
	
	public String mostrarReporteSiniestro() {
		data = ajustadorMovilService.getReporteSiniestro(Long.parseLong(id));
		return SUCCESS;
	}
	
	public String mostrarReporteSiniestroAsignado() {
		data = ajustadorMovilService.getReporteSiniestroAsignado(Long.parseLong(id), tipo);
		return SUCCESS;
	}
	
	public String asignarInciso() {
		ajustadorMovilService.asignarInciso((IncisoParameter) model, this.getUsuarioActual().getNombreUsuario());
		ajustadorMovilService.enviarNotificacion((IncisoParameter) model, this.getUsuarioActual().getNombreUsuario());
		return SUCCESS;		
	}
	
	public String guardaInfoGralReporte() {
		ajustadorMovilService.guardaInfoGralReporte((ReporteSiniestroParameter) model);		
		return SUCCESS;		
	}
	
	public String marcarContacto() {
		ajustadorMovilService.marcarContacto((MarcarContactoParameter) model);		
		return SUCCESS;		
	}
	
	public String marcarTerminacion() {
		ajustadorMovilService.marcarTerminacion((MarcarTerminacionParameter) model);
		return SUCCESS;		
	}
	
	public String mostrarPoliza() {
		MostrarPolizaParameter parameter = (MostrarPolizaParameter) model;
		data = ajustadorMovilService.getPoliza(parameter.getId(), parameter.getFechaOcurrido());
		return SUCCESS;
	}
	
	public String buscarPolizas() {
		data = ajustadorMovilService.findPoliza((FindPolizaParameter) model);
		return SUCCESS;		
	}
	
	public String buscarReporteSiniestrosAsignados() {
		data = ajustadorMovilService.getReporteSiniestrosAsignados(id);
		return SUCCESS;
	}
	
	public String confirmarEnteradoAsignacion() {
		data = ajustadorMovilService.confirmarEnteradoAsignacion(((ReporteSiniestroParameter) model).getId());
		return SUCCESS;
	}

	public String tiposSiniestro(){
		data = ajustadorMovilService.getTiposSiniestro(((ReporteSiniestroParameter) model).getTipoInciso());
		return SUCCESS;
	}
	
	public String terminosDeAjuste(){
		data = ajustadorMovilService.getTerminosAjuste(((ReporteSiniestroParameter) model).getTipoSiniestro(),
				((ReporteSiniestroParameter) model).getResponsabilidad());
		return SUCCESS;
	}
	
	public String responsabilidades(){
		data = ajustadorMovilService.getResponsabilidad(((ReporteSiniestroParameter) model).getTipoSiniestro(),
				((ReporteSiniestroParameter) model).getTerminoAjuste());
		return SUCCESS;
	}
	
	public String municipios() {
		data = ajustadorMovilService.getMunicipios((DireccionParameter)model);
		return SUCCESS;
	}
	
	public String infoCp() {
		data = ajustadorMovilService.getInfoCP((DireccionParameter)model);
		return SUCCESS;
	}
	
	public String coberturasAfecta() {
		data = ajustadorMovilService.getCoberturasAfecta((IncisoParameter) model);		
		return SUCCESS;
	}
	
	public String terceros(){
		data = ajustadorMovilService.getTerceros((CoberturaIncisoParameter)model);
		return SUCCESS;
	}
	
	public String tercero(){
		data = ajustadorMovilService.getTercero((TerceroParameter)model);
		return SUCCESS;
	}
	
	public String afectaReserva(){
		data = ajustadorMovilService.afectaReserva(model, this.getUsuarioActual().getNombreUsuario());
		return SUCCESS;
	}
	
	public String catalogo(){
		data = ajustadorMovilService.getCatalogoSimple(id);
		return SUCCESS;
	}
	
	public String tiposPases(){
		data = ajustadorMovilService.getTiposPases((CoberturaIncisoParameter)model);
		return SUCCESS;
	}
	
	public void prepareImprimirDeclaracionSiniestro() {
		prepareTercero();
	}
	
	public String imprimirDeclaracionSiniestro(){
		try{
			fileBytes = ajustadorMovilService.getBytesPase((TerceroParameter)model, "DDS", this.getUsuarioActual().getNombreCompleto());
			genericInputStream = new ByteArrayInputStream(fileBytes);
			contentType = "application/pdf";
			fileName = "DeclaracionSiniestro.pdf";			
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase de declaracion de siniestro.",Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}	
	
	public void prepareImprimirValeGrua() {
		prepareTercero();
	}
	
	public String imprimirValeGrua(){
		try{
			fileBytes = ajustadorMovilService.getBytesPase((TerceroParameter)model, "VDG", this.getUsuarioActual().getNombreCompleto());
			genericInputStream = new ByteArrayInputStream(fileBytes);
			contentType = "application/pdf";
			fileName = "ValeDeGrua.pdf";			
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase de vale de grua.",Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}
	
	public void prepareImprimirPase() {
		prepareTercero();
	}
	
	public String imprimirPase(){
		TerceroParameter tercero = (TerceroParameter)model;
		try{
			
			if(tipo.equalsIgnoreCase("PAT") || tipo.equalsIgnoreCase("RAC")){
				if(tipo.equalsIgnoreCase("PAT") && tercero.getCoberturaId().intValue() == 1){
					fileName = "AtencionATallerAsegurado.pdf";
				}else if((tipo.equalsIgnoreCase("PAT") && tercero.getCoberturaId().intValue() != 1) || tipo.equalsIgnoreCase("RAC")){
					fileName = "AtencionATallerTercero.pdf";
				}				
			}else if(tipo.equalsIgnoreCase("PDD")){
				fileName = "AtencionParaPagoDeDa�o.pdf";
			}else if(tipo.equalsIgnoreCase("MGO") || tipo.equalsIgnoreCase("GMT")){
				fileName = "ServicioMedico.pdf";
			}else if(tipo.equalsIgnoreCase("OCI")){
				fileName = "ServicioObraCivil.pdf";
			}			
			fileBytes = ajustadorMovilService.getBytesPase((TerceroParameter)model, tipo, this.getUsuarioActual().getNombreCompleto());
			genericInputStream = new ByteArrayInputStream(fileBytes);
			contentType = "application/pdf";		
		}
		catch(Exception e){
			LogDeMidasWeb.log("Fallo la impresion del pase "+fileName,Level.SEVERE, e);
			setMensaje("Error al imprimir favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}			
		return SUCCESS;		
	}	
	
	public String getTiposDocumentosSiniestro() {
		data = ajustadorMovilService.getTiposDocumentosSiniestro();
		return SUCCESS;
	}
	
	public String saveDocumentoSiniestro() {
		ajustadorMovilService.saveDocumentoSiniestro((SaveDocumentoSiniestroParameter) model);
		return SUCCESS;
	}
	
	public String convierteReclamacion(){
		data = ajustadorMovilService.convertirReclamacion((ReporteSiniestroParameter)model, this.getUsuarioActual().getNombreUsuario());
		return SUCCESS;
	}
	
	public String actualizarPosicion() {
		ActualizarPosicionParameter actualizarPosicionParam = (ActualizarPosicionParameter) model;		
		ajustadorEstatusService.guardarAjustadorEstatusSeycos(actualizarPosicionParam.getIdAjustador(), 
				actualizarPosicionParam.getLatitud(), actualizarPosicionParam.getLongitud());
		return SUCCESS;
	}

}