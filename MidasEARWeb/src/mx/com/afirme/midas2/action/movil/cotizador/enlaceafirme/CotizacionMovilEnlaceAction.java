package mx.com.afirme.midas2.action.movil.cotizador.enlaceafirme;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes.VehiculoView;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cotizador.enlaceafirme.CotizacionMovilEnlaceService;
import mx.com.afirme.midas2.service.seguridad.EncryptorService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class CotizacionMovilEnlaceAction extends BaseAction implements Preparable, ModelDriven<Object>{

	private static final long serialVersionUID = 8580842177842427071L;
	private static final Logger LOG = Logger.getLogger(CotizacionMovilEnlaceAction.class);
	
	private String id;
	private Object model;
	private Object data;
	private String isPoliza;
	private String idToCotizacion;
	private Integer idMedioPago;
	private String email;
	private String idCatalogo;
	private CotizacionDTO cotizacion;
	
	private InputStream genericInputStream;
	private String      contentType;
	private String     fileName;
	private String tipoTarjeta;
	private CuentaPagoDTO cuentaPagoDTO;
	private Integer idBanco;
	private String idConductoCobro;
	private BigDecimal numeroInciso;
	private BigDecimal idToPersonaContratante;
	private Locale locale = getLocale();
	
	private BigDecimal idMarca;
	private String claveTarifa;
	private String idEstado;
	private String qs;
	private ClienteGenericoDTO filtroCliente;
	private String isMovil;
	private String forma;
	private BigDecimal idNegocioSeccion;
	private VehiculoView vehiculo;
	
	private String name;
	private Map<String, String> values;
	private Boolean required;
	
	@Override
	public Object getModel() {
		return model;
	}
	
	public CotizacionMovilEnlaceAction(){
		
	}

	@Override
	public void prepare() throws Exception {
		if(!StringUtil.isEmpty(idToCotizacion)){
			cotizacion = entidadService.findById(CotizacionDTO.class, 
					BigDecimal.valueOf(Long.valueOf(idToCotizacion)));
		}
		
		if(!StringUtil.isEmpty(qs)){
			encryptorService.setValuesByEncriptedParams(this, qs);
		}
	}

	public void prepareGuardarCobranza() {
		model = new CuentaPagoDTO();
	}
	
	public String getListCotizacionSO(){
		data = cotizacionMovil.getListCotizacionSeguroObligatorio(id, isPoliza);
		return SUCCESS;
	}
	
	public String getDatosInicialesCSO(){
		data = cotizacionMovil.getDatosInicialesCSO( id );
		return SUCCESS;
	}
	
	public String getDatosCobranza(){
		data = cotizacionMovil.getDatosCobranza(id, idMedioPago);
		return SUCCESS;
	}

	public String getDatosConducto(){
		data = cotizacionMovil.getDatosConducto(id, idToPersonaContratante, idConductoCobro);
		return SUCCESS;
	}

	public String getDatosTC(){
		data = cotizacionMovil.getDatosTC((CuentaPagoDTO)model, idMedioPago);
		return SUCCESS;
	}
	
	public String guardarCobranza(){
		data = cotizacionMovil.guardarCobranza(id, idMedioPago, tipoTarjeta, idBanco, (CuentaPagoDTO)model);
		return SUCCESS;
	}
	
	public String validarDatosComplementarios() {
		data = cotizacionMovil.validarListaParaEmitir(id);
		return SUCCESS;
	}
	
	public String getCatalogoGeneral(){
		LOG.info("Catalogo"+idCatalogo);
		data = cotizacionMovil.getCatalogo(idCatalogo, id);
		return SUCCESS;
	}
	
	public String getResumenCosto(){
		data = cotizacionMovil.getResumenCosto(id, isPoliza, numeroInciso);
		return SUCCESS;
	}
	
	public String getResumen(){
		data = cotizacionMovil.getResumen(cotizacion);
		return SUCCESS;
	}
	
	public String imprimirPdf() {
		TransporteImpresionDTO transporte = cotizacionMovil.getPDFSeguroObligatorio(id, isPoliza, locale, isMovil, email);
		genericInputStream = new ByteArrayInputStream(transporte.getByteArray());
		contentType = transporte.getContentType();
		fileName = transporte.getFileName();

		return SUCCESS;
	}
	
	public String getCertificado(){
		data = cotizacionMovil.generaPlantillaPoliza(id, isPoliza, locale, email);
		
		return SUCCESS;
	}

	public void prepareGetTarifasServicioPublico() {
		model = new TarifaServicioPublico();
	}
	
	public String getTarifasServicioPublico(){
		data = cotizacionMovil.getTarifasServicioPublico((TarifaServicioPublico) model);
		return SUCCESS;
	}
	
	public String getListMarcasByNegSeccion(){
		data = cotizacionMovil.getListMarcasByNegSeccion(BigDecimal.valueOf(Long.valueOf(id)));
		return SUCCESS;
	}
	
	public String getListModelosByMarca(){
		data = cotizacionMovil.getListModelosByMarca(BigDecimal.valueOf(Long.valueOf(id)), idMarca);
		return SUCCESS;
	}
	
	public void prepareGenerarCotizacionServicioPublico() {
		model = new CotizacionDTO();
	}
	
	public String generarCotizacionServicioPublico(){
		data = cotizacionMovil.generarCotizacionServicioPublico((CotizacionDTO) model, 
				BigDecimal.valueOf(Long.valueOf(id)), claveTarifa, idEstado);
		return SUCCESS;
	}
	
	public void prepareSaveCotizacion() {
		model = new IncisoCotizacionDTO();
	}
	
	public String saveCotizacion(){
		data = cotizacionMovil.saveCotizacion((IncisoCotizacionDTO)model, forma);
		return SUCCESS;
	}

	public String envioPDFEmail() {
		data = cotizacionMovil.envioPDFEmail(id, locale, isPoliza,email);
		return SUCCESS;
	}

	public String emitir() {
		data = cotizacionMovil.emitir(id,email);	
		return SUCCESS;
	}
		
	public void prepareBuscarCliente(){
		model = new ClienteDTO();
	}
	
	public String buscarCliente(){
		data = cotizacionMovil.buscarCliente((ClienteDTO) model);
		return SUCCESS;
	}
	
	public void prepareGuardarCliente(){
		model = new ClienteGenericoDTO();
	}
	
	public String guardarCliente(){
		data = cotizacionMovil.guardarCliente((ClienteGenericoDTO) model);
		return SUCCESS;
	}

	public String getClienteByCotizacion(){
		if (cotizacion != null && cotizacion.getIdToPersonaContratante() != null){
			LOG.info("--> cotizacion.getIdToPersonaContratante():"+cotizacion.getIdToPersonaContratante());
			data = cotizacionMovil.getClienteById(cotizacion.getIdToPersonaContratante(), cotizacion.getCodigoUsuarioCotizacion());
		}

		return SUCCESS;
	}
	
	public String getIncisoAutoCotByIdCotizacion(){
		if (cotizacion != null){
			data = cotizacionMovil.getIncisoAutoCotByIdCotizacion(id);
		}
		
		return SUCCESS;
	}
	
	public String getDatosCotizacion(){
		if (cotizacion != null){
			data = cotizacionMovil.getDatosCotizacion(cotizacion);
		}
		
		return SUCCESS;
	}

	public void prepareBuscarEstilo(){
		model = new IncisoAutoCot();
	}
	
	public String buscarEstilo(){
		data = cotizacionMovil.getEstiloPorMarcaNegocioyDescripcion(idToCotizacion, (IncisoAutoCot)model);
		return SUCCESS;
	}
	
	public String getConductosCobroCotizacion(){
		data = cotizacionMovil.getConductosCobroCotizacion(id);
		return SUCCESS;
	}
	
	public String getInicialesCotizacion(){
		data = cotizacionMovil.getInicialesCotizacion( id , idToCotizacion);
		return SUCCESS;
	}
	
	public String getDatosLineaNegocio(){
		data = cotizacionMovil.getDatosLineaNegocio( id );
		return SUCCESS;
	}
	
	public void prepareShowOtherCharacteristics() {
		model = new IncisoCotizacionDTO();
	}
	
	public String showOtherCharacteristics(){
		IncisoCotizacionDTO incisoCotizacion = (IncisoCotizacionDTO) model;
		data = cotizacionMovil.showOtherCharacteristics(vehiculo.getIdEstiloVehiculo(), incisoCotizacion.getIncisoAutoCot().getModificadoresDescripcion());
		return SUCCESS;
	}
	
	public String getRiskData(){		
		data = cotizacionMovil.getRiskData( new BigDecimal(idToCotizacion), numeroInciso, name, required, values );
		return SUCCESS;
	}
	
	public String getPaymentScheme(){
		data = cotizacionMovil.getPaymentScheme(new BigDecimal(idToCotizacion));
		return SUCCESS;
	}
	
	public String getListPaquetesByNegSeccion() {
		data = cotizacionMovil.getListPaquetesByNegSeccion(idNegocioSeccion);
		return SUCCESS;
	}
	
	private CotizacionMovilEnlaceService cotizacionMovil;

	private EncryptorService encryptorService;

	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("encryptorServiceEJB")
	public void setEncryptorService(EncryptorService encryptorService) {
		this.encryptorService = encryptorService;
	}
	
	@Autowired
	@Qualifier("cotizacionMovilEnlaceServiceEJB")
	public void setCotizacionMovil( CotizacionMovilEnlaceService cotizacionMovil ) {
		this.cotizacionMovil = cotizacionMovil;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIsPoliza() {
		return isPoliza;
	}

	public void setIsPoliza(String isPoliza) {
		this.isPoliza = isPoliza;
	}

	public String getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(String idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setModel(Object model) {
		this.model = model;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}
	
	public Integer getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(Integer idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public CuentaPagoDTO getCuentaPagoDTO() {
		return cuentaPagoDTO;
	}

	public void setCuentaPagoDTO(CuentaPagoDTO cuentaPagoDTO) {
		this.cuentaPagoDTO = cuentaPagoDTO;
	}

	public Integer getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}

	public String getIdConductoCobro() {
		return idConductoCobro;
	}

	public void setIdConductoCobro(String idConductoCobro) {
		this.idConductoCobro = idConductoCobro;
	}

	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public String getIdCatalogo() {
		return idCatalogo;
	}

	public void setIdCatalogo(String idCatalogo) {
		this.idCatalogo = idCatalogo;
	}

	public BigDecimal getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(BigDecimal idMarca) {
		this.idMarca = idMarca;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getClaveTarifa() {
		return claveTarifa;
	}

	public void setClaveTarifa(String claveTarifa) {
		this.claveTarifa = claveTarifa;
	}

	public String getQs() {
		return qs;
	}

	public void setQs(String qs) {
		this.qs = qs;
	}

	public ClienteGenericoDTO getFiltroCliente() {
		return filtroCliente;
	}

	public void setFiltroCliente(ClienteGenericoDTO filtroCliente) {
		this.filtroCliente = filtroCliente;
	}

	public String getIsMovil() {
		return isMovil;
	}

	public void setIsMovil(String isMovil) {
		this.isMovil = isMovil;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}

	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}

	public VehiculoView getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(VehiculoView vehiculo) {
		this.vehiculo = vehiculo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}	
}
