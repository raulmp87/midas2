package mx.com.afirme.midas2.action.repuve;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisEmision;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisPTT;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRecuperacion;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRobo;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.repuve.RepuveSisService;

/*******************************************************************************
 * Nombre Interface: 	RepuveSisAction.
 * 
 * Descripcion: 		Action para la ejecucion de los Servicios de Bitacoras.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Component
@Scope("prototype")
@Namespace("/repuve/sistemas")
public class RepuveSisAction extends ActionSupport {
	private static final long serialVersionUID = 1L;	

	private RepuveSisService repuveSisService;

    private List<RepuveSisEmision> repuveSisEmisionList = new ArrayList<RepuveSisEmision>();
    private List<RepuveSisRobo> repuveSisRoboList = new ArrayList<RepuveSisRobo>();
    private List<RepuveSisRecuperacion> repuveSisRecuperacionList = new ArrayList<RepuveSisRecuperacion>();
    private List<RepuveSisPTT> repuveSisPTTList = new ArrayList<RepuveSisPTT>();
    private ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    private int numRegXPag;
    private int numPagina;
	private static final String CONTENEDOR = "/jsp/repuve/contenedor.jsp";

	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = CONTENEDOR) })
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
    @Action(value = "obtenerEmisionPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "repuveSisEmisionList"+
                            "repuveSisEmisionList\\[\\d+\\],"+
                            "repuveSisEmisionList\\[\\d+\\]\\.id,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.repuveNivNci\\.id,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.repuveNivNci\\.niv,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.repuveNivNci\\.nci,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.numeroPoliza,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catTerminosCobertura,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catTerminosCobertura\\.descripcion,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catUbicacionMunicipio\\.descCatUbicacionMunicipio,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catTipoPersona\\.descCatTipoPersona,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.titular,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.fechaVigenciaInicio,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.fechaVigenciaFin,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.catMotivoCancelacion\\.descripcion,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "repuveSisEmisionList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
            })
        })
    public String obtenerEmisionPorFiltros(){
        setRepuveSisEmisionList(repuveSisService.obtenerEmisionPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Action(value = "obtenerRoboPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "repuveSisRoboList"+
                            "repuveSisRoboList\\[\\d+\\],"+
                            "repuveSisRoboList\\[\\d+\\]\\.id,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "repuveSisRoboList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"+
                            "repuveSisRoboList\\[\\d+\\]\\.repuveNivNci\\.id,"+
                            "repuveSisRoboList\\[\\d+\\]\\.repuveNivNci\\.niv,"+
                            "repuveSisRoboList\\[\\d+\\]\\.repuveNivNci\\.nci,"+
                            "repuveSisRoboList\\[\\d+\\]\\.numeroPoliza,"+
                            "repuveSisRoboList\\[\\d+\\]\\.numeroSiniestro,"+
                            "repuveSisRoboList\\[\\d+\\]\\.fechaRobo,"+
                            "repuveSisRoboList\\[\\d+\\]\\.catUbicacionMunicipio\\.descCatUbicacionMunicipio,"+
                            "repuveSisRoboList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado,"+
                            "repuveSisRoboList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                            "repuveSisRoboList\\[\\d+\\]\\.numeroAveriguacionPrevia,"+
                            "repuveSisRoboList\\[\\d+\\]\\.fechaAveriguacionPrevia"
            })
        })
    public String obtenerRoboPorFiltros(){
setRepuveSisRoboList(repuveSisService.obtenerRoboPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Action(value = "obtenerRecuperacionPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "repuveSisRecuperacionList"+
                            "repuveSisRecuperacionList\\[\\d+\\],"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.id,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.repuveSisRobo\\.id,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.repuveSisRobo\\.repuveNivNci\\.niv,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.repuveSisRobo\\.repuveNivNci\\.nci,"+
							"repuveSisRecuperacionList\\[\\d+\\]\\.repuveSisRobo\\.numeroPoliza,"+
							"repuveSisRecuperacionList\\[\\d+\\]\\.repuveSisRobo\\.numeroSiniestro,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.catUbicacionMunicipio\\.descCatUbicacionMunicipio,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.fechaRecuperacion,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.numeroAveriguacionPrevia,"+
                            "repuveSisRecuperacionList\\[\\d+\\]\\.fechaAveriguacionPrevia"
        })
    })
    public String obtenerRecuperacionPorFiltros(){
        setRepuveSisRecuperacionList(repuveSisService.obtenerRecuperacionPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

    @Action(value = "obtenerPTTPorFiltros", results = { @Result(name = "json", type="json", params = {
                    "includeProperties", "repuveSisPTTList"+
                            "repuveSisPTTList\\[\\d+\\],"+
                            "repuveSisPTTList\\[\\d+\\]\\.id,"+
                            "repuveSisPTTList\\[\\d+\\]\\.repuveNivNci\\.id,"+
                            "repuveSisPTTList\\[\\d+\\]\\.repuveNivNci\\.niv,"+
                            "repuveSisPTTList\\[\\d+\\]\\.repuveNivNci\\.nci,"+
                            "repuveSisPTTList\\[\\d+\\]\\.fechaPTT,"+                            
                            "repuveSisPTTList\\[\\d+\\]\\.numeroPoliza,"+
							"repuveSisPTTList\\[\\d+\\]\\.catTerminosCobertura,"+
							"repuveSisPTTList\\[\\d+\\]\\.catTerminosCobertura\\.descripcion,"+
                            "repuveSisPTTList\\[\\d+\\]\\.catUbicacionMunicipio\\.descCatUbicacionMunicipio,"+
                            "repuveSisPTTList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado,"+
                            "repuveSisPTTList\\[\\d+\\]\\.catUbicacionMunicipio\\.catUbicacionEstado\\.descCatUbicacionEstado,"+
                            "repuveSisPTTList\\[\\d+\\]\\.numeroAveriguacionPrevia,"+
                            "repuveSisPTTList\\[\\d+\\]\\.fechaAveriguacionPrevia,"+
                            "repuveSisPTTList\\[\\d+\\]\\.porcentajePerdida,"+
                            "repuveSisPTTList\\[\\d+\\]\\.catMotivoPTT\\.motivoPerdidaTotal,"+
                            "repuveSisPTTList\\[\\d+\\]\\.numeroSiniestro,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.id,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.id,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catOperaciones\\.descCatOperaciones,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.id,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.catEstatusBitacora\\.descCatEstatusBitacora,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaOperacion,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaRegistro,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.fechaEnvio,"+
                            "repuveSisPTTList\\[\\d+\\]\\.sapAmisBitacoras\\.observaciones"
            })
        })
    public String obtenerPTTPorFiltros(){
        setRepuveSisPTTList(repuveSisService.obtenerPTTPorFiltros(parametrosConsulta, numRegXPag, numPagina));
        return "json";
    }

	@Autowired
	@Qualifier("repuveSisServiceEJB")
	public void setRepuveSisService(RepuveSisService repuveSisService) {
		this.repuveSisService = repuveSisService;
	}

    public void setParametrosConsulta(ParametrosConsulta parametrosConsulta) {
        this.parametrosConsulta = parametrosConsulta;
    }

	public List<RepuveSisEmision> getRepuveSisEmisionList() {
		return repuveSisEmisionList;
	}

	public void setRepuveSisEmisionList(List<RepuveSisEmision> repuveSisEmisionList) {
		this.repuveSisEmisionList = repuveSisEmisionList;
	}

	public void setNumRegXPag(int numRegXPag) {
		this.numRegXPag = numRegXPag;
	}

	public void setNumPagina(int numPagina) {
		this.numPagina = numPagina;
	}

	public void setRepuveSisRoboList(List<RepuveSisRobo> repuveSisRoboList) {
		this.repuveSisRoboList = repuveSisRoboList;
	}

	public void setRepuveSisRecuperacionList(
			List<RepuveSisRecuperacion> repuveSisRecuperacionList) {
		this.repuveSisRecuperacionList = repuveSisRecuperacionList;
	}

	public void setRepuveSisPTTList(List<RepuveSisPTT> repuveSisPTTList) {
		this.repuveSisPTTList = repuveSisPTTList;
	}

	public List<RepuveSisRobo> getRepuveSisRoboList() {
		return repuveSisRoboList;
	}

	public List<RepuveSisRecuperacion> getRepuveSisRecuperacionList() {
		return repuveSisRecuperacionList;
	}

	public List<RepuveSisPTT> getRepuveSisPTTList() {
		return repuveSisPTTList;
	}
}
