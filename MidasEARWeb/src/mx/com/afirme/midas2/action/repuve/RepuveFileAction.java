package mx.com.afirme.midas2.action.repuve;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import mx.com.afirme.midas2.service.repuve.RepuveFileService;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;

@Component
@Scope("prototype")
@Namespace("/repuve/file")
public class RepuveFileAction extends ActionSupport{
	private static final long serialVersionUID = 1L;
	
	//Variables para el consumo de los Servicios.
	private RepuveFileService repuveFileService;
    private SapAmisUtilsService sapAmisUtilsService;
    //Variables para el uso de Parametros (Entrada/Salida)
	private int estatus = -1;
	private int tipoArchivo;//1 Avisos - 2 Consultas.
	private String fileName;
	private File myFile;
    private String fechaProcesarArchivo;
    private InputStream inputStream;

    @Action(value = "upload",
            results = { @Result(name = "success", type="json", params = {
                    "includeProperties", "estatus"
    		})
    	})
	public String upload(){
		try{
			estatus = repuveFileService.procesarArchivoRepuve(myFile, fileName) ? 0 : -1;
		    return SUCCESS;
		}catch(Exception e){
			return ERROR;
		}
	}

    @Action(value = "download",
            results = {
            	@Result(name = "success", type="stream",
            	params = {
            		"contentType","application/octet-stream; charset=UTF-8",
            		"contentDisposition", "attachment; filename=\"${fileName}\"",
                    "inputName", "inputStream",
                    "bufferSize", "1024"
    			})
    		})
	public String download(){
		if(fechaProcesarArchivo!= null && !fechaProcesarArchivo.equals("")){
			myFile = repuveFileService.generarArchivoRepuve(sapAmisUtilsService.convertToDateStr(fechaProcesarArchivo), tipoArchivo);
			fileName = myFile.getName();
			try{
				inputStream = new FileInputStream(myFile);
			    return SUCCESS;
			}catch(Exception e){
				return ERROR;
			}
		}else{
			return ERROR;
		}
	}

	@Autowired
	@Qualifier("repuveFileServiceEJB")
	public void setRepuveFileService(RepuveFileService repuveFileService) {
		this.repuveFileService = repuveFileService;
	}

	@Autowired
	@Qualifier("sapAmisUtilsServiceEJB")
	public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
		this.sapAmisUtilsService = sapAmisUtilsService;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getFechaProcesarArchivo() {
		return fechaProcesarArchivo;
	}

	public void setFechaProcesarArchivo(String fechaProcesarArchivo) {
		this.fechaProcesarArchivo = fechaProcesarArchivo;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public int getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(int tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
}