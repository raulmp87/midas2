package mx.com.afirme.midas2.action.folioReexpedible;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.apache.struts2.convention.annotation.Action;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.folioReexpedible.FiltroFolioReexpedible;
import mx.com.afirme.midas2.domain.folioReexpedible.TcTipoFolio;
import mx.com.afirme.midas2.domain.folioReexpedible.ToFolioReexpedible;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.service.folioReexpedibleService.FolioReexpedibleService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;

@Namespace("/folio/reexpedible")
@Component
@Scope("prototype")
public class FolioReexpedibleAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(FolioReexpedibleAction.class);
	
	private static final String ACTION_FOLIOS_REEXPEDIBLES_LIST = "/jsp/folio/reexpedible/foliosReexpediblesList.jsp";
	
	private static final String ACTION_FOLIOS_REEXPEDIBLES_BUSCAR = "/jsp/folio/reexpedible/folioReexpedibleBuscar.jsp";
	
	private static final String ACTION_FOLIOS_VINCULAR = "/jsp/folio/reexpedible/vinculacionAgente/folioReexpedibleVincularAgente.jsp";

	private ToFolioReexpedible toFolio;
	
	private String numeroFolios;
	private List<TcTipoFolio> tipoFolios;
	private List<AgenteDTO> agentes;
	private List<Negocio> negocios;
	private List<FiltroFolioReexpedible> foliosExpedidos;
	private FiltroFolioReexpedible filtroFolioRe;
	private String mensajeFolio;
	private String stringMapJson;
	private boolean filtroFolio;

	@Autowired
	private FolioReexpedibleService folioService;
	
	@Autowired
	private AgenteService agenteService;
	
	@Autowired
	private CotizadorAgentesService cotizadorAgentesService;
	
	@Autowired
	private NegocioService negocioService;
	
	public FolioReexpedibleAction() {
	}

	@Override
	public void prepare() throws Exception {
	}

	@Action(value = "init", results = { @Result(name = SUCCESS, location = ACTION_FOLIOS_REEXPEDIBLES_BUSCAR) })
	public String init() {
		inicializar();
		return SUCCESS;
	}
	
	@Action(value="initVinculacion", results = {@Result(name=SUCCESS, location = ACTION_FOLIOS_VINCULAR)})
	public String initVinculacion(){
		log.info(":::::::::::::: [  INFO  ] :::::::::::::: ENTRA AL INIT VINCULAR");
		filtroFolioRe = new FiltroFolioReexpedible();
		log.info(":::::::::::::: [  INFO  ] :::::::::::::: SALE DEL INIT VINCULAR");
		return SUCCESS;
	}
	
	@Action(value="vincularFolios", results = {@Result(name = SUCCESS, type="json", params={"noCache","true", "ignoreHierarchy", "false", 
			"includeProperties", "stringMapJson"})})
	public String vincularFolios() throws JsonGenerationException, JsonMappingException, IOException{
		log.info(":::::::::::::: [  INFO  ] :::::::::::::: ENTRA AL METODO VINCULAR");
		stringMapJson = folioService.vincularFolios(filtroFolioRe);
		
		log.info(":::::::::::::: [  INFO  ] :::::::::::::: MAPA STRING " + stringMapJson);
		
		log.info(":::::::::::::: [  INFO  ] :::::::::::::: SALE DEL METODO VINCULAR");
		return SUCCESS;
	}
	
	@Action(value="guardar", results = {@Result(name=SUCCESS,type="json",params={"noCache","true", "ignoreHierarchy", "false", 
			"includeProperties","mensajeFolio"})})
	public String guardar(){
		boolean bandera = false;
		boolean camposFaltantes =  true;
		
		Long idAgente = new Long(toFolio.getAgenteFacultativo());
		
		if(idAgente != null && idAgente > 0L){
    		Agente filtro = new Agente();
    		filtro.setIdAgente(idAgente);
    		Agente result = cotizadorAgentesService.getAgente(filtro);
    		
    		toFolio.setAgenteFacultativo(result.getId());
    		toFolio.setAgenteVinculado(result.getId());
		}
		
		if(toFolio == null || toFolio.getNegocio() <= 0L || toFolio.getNegprod() <= 0L || toFolio.getNegTpoliza() <= 0L || toFolio.getIdTipoMoneda() <= 0L || 
				toFolio.getVigencia() <= 0L || toFolio.getFechaValidesHasta() == null || numeroFolios == null || numeroFolios.trim().length() <= 0 ||
				toFolio.getAgenteFacultativo() <= 0L || toFolio.getTipoFolio() == null || toFolio.getTipoFolio().getIdTipoFolio() <= 0L){
			camposFaltantes = false;
		}
		if(camposFaltantes){
    		bandera = this.folioService.guardarFolioReexpedible(toFolio, numeroFolios);
    		if(bandera){
    			mensajeFolio = "Se persistieron de manera correcta los folios.|" + bandera;
    		} else {
    			mensajeFolio = "Hubo un problema al momento de guardar.|" + bandera;
    		}
		} else {
			mensajeFolio = "Hace falta llenar algunos campos, favor de llenar los campos marcados con un * .|" + bandera;
		}
		return SUCCESS;
	}
	
	private void inicializar(){
		toFolio = new ToFolioReexpedible();
		toFolio.setTipoFolio(new TcTipoFolio());
		
		agentes = agenteService.listarAgentes();
		Collections.sort(agentes, new Comparator<AgenteDTO>() {
			@Override
			public int compare(AgenteDTO object1, AgenteDTO object2) {
				return new CompareToBuilder().append(object1.getNombre(),object2.getNombre()).toComparison();
			}
		});
		
		Negocio neg = new Negocio();
		neg.setClaveNegocio("A");
		neg.setClaveEstatus((short) 1);
		this.negocios = negocioService.findByFilters(neg);
		this.tipoFolios = folioService.consultarTiposDeFolios();
		this.filtroFolioRe = new FiltroFolioReexpedible();
		this.filtroFolioRe.setMoneda(new MonedaDTO());
		this.filtroFolioRe.setNegocio(new Negocio());
		this.filtroFolioRe.setProducto(new NegocioProducto());
		this.filtroFolioRe.setTipoFolio(new TcTipoFolio());
		this.filtroFolioRe.setVigencia(new VigenciaDTO());
		this.filtroFolioRe.setTipoPoliza(new NegocioTipoPoliza());
	}
	
	@Action(value = "buscar" , results = { @Result (name = SUCCESS, location = ACTION_FOLIOS_REEXPEDIBLES_LIST, params =  {"mensaje", "${mensajeFolio}"}) })
	public String buscar(){
		log.info(":::: >>>>> ENTRA AL METODO DE BUSCAR");
		log.info(":::: [ INFO ] EVALUA SI EL FILTRO ES DIF DE NULL: " + (filtroFolioRe != null));
		if(filtroFolioRe != null){
			
			filtroFolioRe.setFolioFin(StringUtil.decodeUri(filtroFolioRe.getFolioFin()));
			filtroFolioRe.setFolioInicio(StringUtil.decodeUri(filtroFolioRe.getFolioInicio()));
			
			if(super.getCount() == null){
				super.setCount(20);
			}
			
			if(super.getPosStart() == null){
				super.setPosStart(0);
			}
			
    		filtroFolioRe.setPosStart(super.getPosStart());
    		filtroFolioRe.setCount(super.getCount());
    		//filtroFolioRe.setOrderByAttribute(decodeOrderBy());
    		if(super.getPosStart().intValue() == 0){
    			super.setTotalCount(folioService.contarRegistrosFolios(filtroFolioRe, filtroFolio));
    		}
    		this.foliosExpedidos = folioService.consultarFolios(filtroFolioRe, filtroFolio);
    		
    		if(foliosExpedidos == null || foliosExpedidos.size() < 1){
    			mensajeFolio = "No se encontraron registros";
    		}
		}
		return SUCCESS;
	}
	
	public FolioReexpedibleService getFolioService() {
		return folioService;
	}

	public void setFolioService(FolioReexpedibleService folioService) {
		this.folioService = folioService;
	}

	public List<TcTipoFolio> getTipoFolios() {
		return tipoFolios;
	}

	public void setTipoFolios(List<TcTipoFolio> tipoFolios) {
		this.tipoFolios = tipoFolios;
	}

	public List<AgenteDTO> getAgentes() {
		return agentes;
	}

	public void setAgentes(List<AgenteDTO> agentes) {
		this.agentes = agentes;
	}

	public List<Negocio> getNegocios() {
		return negocios;
	}

	public void setNegocios(List<Negocio> negocios) {
		this.negocios = negocios;
	}

	public AgenteService getAgenteService() {
		return agenteService;
	}

	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}

	public NegocioService getNegocioService() {
		return negocioService;
	}

	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	public String getNumeroFolios() {
		return numeroFolios;
	}

	public void setNumeroFolios(String numeroFolios) {
		this.numeroFolios = numeroFolios;
	}

	public List<FiltroFolioReexpedible> getFoliosExpedidos() {
		return foliosExpedidos;
	}

	public void setFoliosExpedidos(List<FiltroFolioReexpedible> foliosExpedidos) {
		this.foliosExpedidos = foliosExpedidos;
	}

	public FiltroFolioReexpedible getFiltroFolioRe() {
		return filtroFolioRe;
	}

	public void setFiltroFolioRe(FiltroFolioReexpedible filtroFolioRe) {
		this.filtroFolioRe = filtroFolioRe;
	}

	public ToFolioReexpedible getToFolio() {
		
		return toFolio;
	}

	public void setToFolio(ToFolioReexpedible toFolio) {
		this.toFolio = toFolio;
	}

	public String getMensajeFolio() {
		return mensajeFolio;
	}

	public void setMensajeFolio(String mensajeFolio) {
		this.mensajeFolio = mensajeFolio;
	}

	public String getStringMapJson() {
		return stringMapJson;
	}

	public void setStringMapJson(String stringMapJson) {
		this.stringMapJson = stringMapJson;
	}

	public boolean isFiltroFolio() {
		return filtroFolio;
	}

	public void setFiltroFolio(boolean filtroFolio) {
		this.filtroFolio = filtroFolio;
	}

	public CotizadorAgentesService getCotizadorAgentesService() {
		return cotizadorAgentesService;
	}

	public void setCotizadorAgentesService(
			CotizadorAgentesService cotizadorAgentesService) {
		this.cotizadorAgentesService = cotizadorAgentesService;
	}
}
