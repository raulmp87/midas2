package mx.com.afirme.midas2.action.suscripcion.solicitud.rechazar;

import java.math.BigDecimal;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudPolizaService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
@Component
@Scope("prototype")
public class RechazarSolicitudPolizaAction extends BaseAction  implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private SolicitudPolizaService solicitudPolizaService;
	private SolicitudDTO solicitud;
	private String comentario;
	private ComentariosService comentariosService;
	
	@Autowired
	@Qualifier("comentarioServiceEJB")
	public void setComentariosService(ComentariosService comentariosService) {
		this.comentariosService = comentariosService;
	}
	
	@Override
	public void prepare() throws Exception {
		if(getId() != null){
			solicitud = solicitudPolizaService.findById(getId());
		}
	}
	public String mostrarVentana(){
		if(solicitud == null)return ERROR;
		return SUCCESS;
	}
	

	public String rechazar(){
		if(solicitud != null){
			comentariosService.guardarComentario(comentario, solicitud);
			solicitud.setMotivoRechazo(comentario);
			solicitudPolizaService.rechazarSolicitud(solicitud);
			return SUCCESS;
		}else{
			return ERROR;
		}
	}
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Autowired
	@Qualifier("solicitudPolizaServiceEJB")
	public void setSolicitudPolizaService(
			SolicitudPolizaService solicitudPolizaService) {
		this.solicitudPolizaService = solicitudPolizaService;
	}
	public SolicitudDTO getSolicitud() {
		return solicitud;
	}
	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}	
}
