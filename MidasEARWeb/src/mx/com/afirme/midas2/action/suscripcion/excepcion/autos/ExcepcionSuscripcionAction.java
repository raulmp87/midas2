package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionSuscripcionAutoDescripcionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 6214455466014762689L;

	protected ExcepcionSuscripcionNegocioAutosService excepcionService;
	
	protected ListadoService listadoService = null;
	
	protected String cveNegocio = "A";
	
	private List<ExcepcionSuscripcionAutoDescripcionDTO> excepciones = null;
	
	private ExcepcionSuscripcionAutoDescripcionDTO excepcion = new ExcepcionSuscripcionAutoDescripcionDTO();
	
	protected Long excepcionId;

	protected Boolean agregada;
	
	private boolean habilitado;
	
	private boolean autorizacion;

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("excepcionNegocioAutoSuscripcionEJB")
	public void setExcepcionService(ExcepcionSuscripcionNegocioAutosService excepcionService) {
		this.excepcionService = excepcionService;
	}

	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
		
	public void prepareMostrarContenedor() {	
		if(excepciones == null){
			excepciones =  new ArrayList<ExcepcionSuscripcionAutoDescripcionDTO>();
		}	
	}
	
	public void prepareMostrarExcepcion(){
		getSession().put("idToExcepcion", excepcionId);
		excepcion = (ExcepcionSuscripcionAutoDescripcionDTO)excepcionService.obtenerExcepcion(excepcionId);
		setHabilitado(excepcion.isHabilitado());
		setAutorizacion(excepcion.isAutorizacion());
	}
	
	public String mostrarExcepcion(){
		return SUCCESS;
	}
	
	public String eliminarExcepcion(){
		try{
			excepcionService.eliminarExcepcion(excepcionId);
			super.setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ELIMINAR);
		}
		return SUCCESS;
	}
	
	
	public void validateGuardarExcepcion() {
		addErrors(excepcion, NewItemChecks.class, this, "excepcion");
		if(hasErrors() && excepcionId != null){
			prepareMostrarExcepcion();	
		}
	}
	
	public String guardarExcepcion(){
		try{
			excepcion.setCveNegocio(cveNegocio);
			excepcion.setIdExcepcion(obtenerExcepcion());
			excepcion.setHabilitado(habilitado);
			excepcion.setAutorizacion(autorizacion);
			excepcionId = excepcionService.salvarExcepcion(excepcion);			
			super.setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}
		return SUCCESS;
	}
	
	protected Long obtenerExcepcion(){
		return (Long)getSession().get("idToExcepcion");
	}
		
	public String mostrarContenedor() {
		return SUCCESS;
	}
	
	public void prepareMostrarAgregar() {
		setHabilitado(true);
		setAutorizacion(true);
	}
	
	public String mostrarAgregar() {
		getSession().remove("idToExcepcion");
		return SUCCESS;
	}

	public String listarExcepciones(){
		
		excepciones = (List<ExcepcionSuscripcionAutoDescripcionDTO>)excepcionService.listarExcepciones(cveNegocio);	
		return SUCCESS;
	}

	public List<ExcepcionSuscripcionAutoDescripcionDTO> getExcepciones() {
		return excepciones;
	}

	public void setExcepciones(List<ExcepcionSuscripcionAutoDescripcionDTO> excepciones) {
		this.excepciones = excepciones;
	}

	public Long getExcepcionId() {
		return excepcionId;
	}

	public void setExcepcionId(Long excepcionId) {
		this.excepcionId = excepcionId;
	}


	public ExcepcionSuscripcionAutoDescripcionDTO getExcepcion() {
		return excepcion;
	}

	public void setExcepcion(ExcepcionSuscripcionAutoDescripcionDTO excepcion) {
		this.excepcion = excepcion;
	}
	
	
	public void setAgregada(Boolean agregada) {
		this.agregada = agregada;
	}


	public Boolean getAgregada() {
		return agregada;
	}

	public boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	public boolean isAutorizacion() {
		return autorizacion;
	}

	public void setAutorizacion(boolean autorizacion) {
		this.autorizacion = autorizacion;
	}
	
}
