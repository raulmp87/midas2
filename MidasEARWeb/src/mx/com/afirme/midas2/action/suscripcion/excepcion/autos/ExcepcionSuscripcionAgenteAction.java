package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.util.Map;

import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionAgenteAction extends
		ExcepcionSuscripcionAction implements Preparable {

	private static final long serialVersionUID = 4434248582524442211L;
	private Map<Object, String> agentes = null;
	private String agenteId = null;

	Logger logger = Logger.getLogger(ExcepcionSuscripcionAgenteAction.class);

	@Override
	public void prepare() throws Exception {

	}

	public void prepareMostrarAgente() {
		excepcionId = super.obtenerExcepcion();
		if (excepcionId != null) {
			agenteId = excepcionService.obtenerCondicionValor(excepcionId,
					TipoCondicion.AGENTE);
		}
		if (agentes == null) {
			agentes = listadoService.listarAgentes();
		}
	}

	public String mostrarAgente() {
		return SUCCESS;
	}

	public String guardarAgente() {
		try {
			excepcionId = super.obtenerExcepcion();
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.AGENTE, agenteId);
			super.setMensajeExitoPersonalizado("Condici\u00f3n agregada a la excepci\u00f3n");
			super.setNextFunction("parent.cerrarVentanaModal('agente');");
			// Condición Agregada con exito
			super.setAgregada(true);
		} catch (Exception ex) {
			super.setMensajeError("No se puedo agregar la condici\u00f3n a la excepci\u00f3n en este momento");
		}
		return SUCCESS;
	}

	public void setAgentes(Map<Object, String> agentes) {
		this.agentes = agentes;
	}

	public Map<Object, String> getAgentes() {
		return agentes;
	}
	

	public String getAgenteId() {
		return agenteId;
	}

	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

}