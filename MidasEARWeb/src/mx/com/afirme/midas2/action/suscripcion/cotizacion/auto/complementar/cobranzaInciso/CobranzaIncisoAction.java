package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.cobranzaInciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.complementar.cobranzaInciso.CobranzaIncisoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CobranzaIncisoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7598929611788680033L;
	
	private BigDecimal idToCotizacion;
	private BigDecimal idCliente;
	private CotizacionDTO cotizacionDTO;
	private ClienteDTO cliente;
	private BigDecimal idMedioPago;
	private Long idConductoCobro;
	private EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO;
	private BigDecimal idToPersonaContratante;
	
	private IncisoCotizacionDTO incisoCotizacion;
	private Set<String> descripcionesIncisos;
	private List<NegocioSeccion> negocioSeccionList;
	private Map<Long, String> paquetes = null;
	
	private Map<BigDecimal, String> clientesCobro;
	private List<MedioPagoDTO> medioPagoDTOs = new ArrayList<MedioPagoDTO>(1);
	private Map<Long, String> conductosDeCobro;
	private Map<Boolean,String> mapDatosIguales = new HashMap<Boolean, String>();
	private List<BancoEmisorDTO> bancos;
	
	private CuentaPagoDTO cuentaPagoDTO;
	private String tipoTarjeta;
	private Integer idBanco;

	private String anio;
	private String mes;
	
	private Map<String, String> promociones;
	private Boolean datosIguales;
	private Map<String, String> estadoMap;
	private Map<String, String> municipioMap;
	private Map<String, String> coloniasMap;
	private Map<String, String> paisesMap;	

	private Short soloConsulta = 0;
	
	private List<CobranzaIncisoDTO> incisosCobranza = new ArrayList<CobranzaIncisoDTO>(1);
	private List<IncisoCotizacionDTO> incisos = new ArrayList<IncisoCotizacionDTO>(1);
	
	private EntidadService entidadService;
	private ListadoService listadoService;
	private NegocioSeccionService negocioSeccionService;
	private CobranzaIncisoService cobranzaIncisoService;
	private CalculoService calculoService;
	private CotizacionService cotizacionService;
	private PolizaPagoService pagoService;
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	
	@Autowired
	@Qualifier("bancoEmisorFacadeRemoteEJB")
	public void setBancoEmisorFacade(BancoEmisorFacadeRemote bancoEmisorFacade) {
		this.bancoEmisorFacade = bancoEmisorFacade;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@Autowired
	@Qualifier("cobranzaIncisoServiceEJB")
	public void setCobranzaIncisoService(CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@Autowired
	@Qualifier("polizaPagoServiceEJB")
	public void setPagoService(PolizaPagoService pagoService) {
		this.pagoService = pagoService;
	}

	@Override
	public void prepare() throws Exception {
		if(cotizacionDTO != null && cotizacionDTO.getIdToCotizacion() != null){
			idToCotizacion = cotizacionDTO.getIdToCotizacion();
		}
		if(incisoCotizacion == null){
			incisoCotizacion = new IncisoCotizacionDTO();
			incisoCotizacion.setId(new IncisoCotizacionId());
			incisoCotizacion.setIncisoAutoCot(new IncisoAutoCot());
		}
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			
			negocioSeccionList = negocioSeccionService.getSeccionListByCotizacionInciso(cotizacionDTO);
			if(descripcionesIncisos == null){
				descripcionesIncisos = listadoService.listarIncisosDescripcionByCotId(idToCotizacion);
			}
			if(paquetes == null){
				if (incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId() != null) {
					paquetes = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(idToCotizacion,new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()));
				} else {
					paquetes =  new LinkedHashMap<Long, String>();
				}
			}
		}else{
			negocioSeccionList = new ArrayList<NegocioSeccion>(1);
			paquetes =  new LinkedHashMap<Long, String>();
			descripcionesIncisos = new HashSet<String>();
		}
		try{
			bancos = bancoEmisorFacade.findAll();
		}catch(Exception e){
		}
	}
	
	public void init(){
		try{
			final ResumenCostosDTO resumen = calculoService.obtenerResumenCotizacion(cotizacionDTO, false);
			cotizacionDTO.setPrimaNetaTotal(resumen.getPrimaNetaCoberturas());

			esquemaPagoCotizacionDTO = cotizacionService.findEsquemaPagoCotizacion(
					cotizacionDTO.getFechaInicioVigencia(),
					cotizacionDTO.getFechaInicioVigencia(),
					cotizacionDTO.getFechaFinVigencia(),
					Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),
					BigDecimal.valueOf(cotizacionDTO.getPrimaNetaTotal()),
					BigDecimal.valueOf(cotizacionDTO.getValorDerechosUsuario()*cotizacionDTO.getIncisoCotizacionDTOs().size()),
					BigDecimal.valueOf(cotizacionDTO.getPorcentajeIva()),
					Integer.valueOf(cotizacionDTO.getIdMoneda().toString()),
					Double.valueOf(cotizacionDTO.getPrimaNetaTotal().doubleValue() * (cotizacionDTO.getPorcentajePagoFraccionado() / 100)),
					true);
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			if(cotizacionDTO.getIdToPersonaContratante() != null && idMedioPago != null){
				conductosDeCobro = listadoService.getConductosCobro(
						cotizacionDTO.getIdToPersonaContratante().longValue(),
						idMedioPago.intValue());
			}else if(cotizacionDTO.getIdToPersonaContratante() != null && cotizacionDTO.getIdMedioPago() != null){
				conductosDeCobro = listadoService.getConductosCobro(
						cotizacionDTO.getIdToPersonaContratante().longValue(),
						cotizacionDTO.getIdMedioPago().intValue());
			}else{
				conductosDeCobro = new HashMap<Long, String>();
			}
			
		} catch(RuntimeException ex) {
			ex.printStackTrace();
			conductosDeCobro = new HashMap<Long, String>();
		}
		try{
			medioPagoDTOs = cotizacionDTO.getSolicitudDTO().getProductoDTO().getMediosPago();
		}catch(Exception e){
			
		}
		
	}
	
	public String mostrar(){		
		return SUCCESS;
	}
	
	public String mostrarContratante(){	
		init();		
		return SUCCESS;
	}
	
	public String mostrarAsegurado(){
		return SUCCESS;
	}
	
	public String mostrarAgregarNuevo(){
		try{
			medioPagoDTOs = cotizacionDTO.getSolicitudDTO().getProductoDTO().getMediosPago();
			conductosDeCobro = new HashMap<Long, String>();
			clientesCobro = cobranzaIncisoService.getClientesCobroCotizacion(idToCotizacion);
			if(idToPersonaContratante != null && idMedioPago != null){
				conductosDeCobro = listadoService.getConductosCobro(idToPersonaContratante.longValue(), idMedioPago.intValue());				
			}
			mapDatosIguales.put(true, "SI");
			mapDatosIguales.put(false, "NO");
		}catch(Exception e){		
		}		
		return SUCCESS;
	}	
	
	public String aplicarCobranza(){
		if(incisos != null && !incisos.isEmpty()){
			cobranzaIncisoService.aplicarCobranzaIncisos(incisos, idToCotizacion, idMedioPago, idConductoCobro, false);
		}
		return SUCCESS;
	}
	
	public String aplicarCobranzaTodos(){		
		cobranzaIncisoService.aplicarCobranzaIncisos(null, idToCotizacion, idMedioPago, idConductoCobro, true);
		return SUCCESS;
	}
	
	public String buscarIncisoAseg(){
		incisoCotizacion.setPrimerRegistroACargar(super.getPosStart());
		incisoCotizacion.setNumeroMaximoRegistrosACargar(BaseAction.REGISTROS_A_MOSTRAR);
		incisosCobranza = cobranzaIncisoService.getCobranzaIncisos(incisoCotizacion);
		return SUCCESS;
	}
	
	public String aplicarCobranzaAsegurado(){
		if(incisos != null && !incisos.isEmpty()){
			cobranzaIncisoService.aplicarCobranzaAseguradoIncisos(incisos);
			this.setMensajeExito();
		}else{
			this.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}
		return SUCCESS;
	}
	
	public String guardarCobranzaInciso() {
		//tabActiva = "cobranza";
		if (getIdToCotizacion() != null) {
			cotizacionDTO = entidadService.findById(CotizacionDTO.class,
					idToCotizacion);
		}
		try{
			switch (getIdMedioPago().intValue()) {
			case 4:
				// Tarjeta de credito
				cuentaPagoDTO.setTipoConductoCobro(CuentaPagoDTO.TipoCobro.TARJETA_CREDITO);
				// Inicializa el tipo de tarjeta
				if (tipoTarjeta.equals("MC") ){
					cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.MASTERCARD);
				}else{
					cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.VISA);
				}
				break;
			case 8:
				// Domiciliacion
				cuentaPagoDTO.setTipoConductoCobro(CuentaPagoDTO.TipoCobro.DOMICILIACION);
				break;
			case 1386:
				// Cuenta Afirme
				cuentaPagoDTO.setTipoConductoCobro(CuentaPagoDTO.TipoCobro.CUENTA_AFIRME);
				cuentaPagoDTO.setIdBanco((long)idBanco);
				break;
			case 15:
				cuentaPagoDTO = new CuentaPagoDTO();
				cuentaPagoDTO.setTipoConductoCobro(CuentaPagoDTO.TipoCobro.EFECTIVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		if (cotizacionDTO != null && cuentaPagoDTO != null) {
			
			if(idConductoCobro != null){
				cuentaPagoDTO.setIdConductoCliente(Long.valueOf(idConductoCobro));
			}			
			// Constrante
			cuentaPagoDTO.setIdCliente(idToPersonaContratante);
			//cuentaPagoDTO.setFechaVencimiento(mes+anio);
			try{
				pagoService.guardarDatosCobranzaCliente(cuentaPagoDTO);
			}catch(Exception e){
				String message = e.getMessage();
				if(message.indexOf("java.lang.RuntimeException:") != -1){
					super.setMensajeError(message.substring(message.indexOf("java.lang.RuntimeException:")+27,message.length()));
				}else{
					super.setMensajeError(MENSAJE_ERROR_GENERAL);
				}
				return SUCCESS;
			}			
		}else{
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		super.setMensajeExito();
		return SUCCESS;
	}
	
	/**
	 * Obtiene los datos de la cuenta
	 */
	public void prepareComplementarInfoCuentaInciso() {
		// Obtiene la cotizacion
		if (getIdToCotizacion() != null) {
			cotizacionDTO = entidadService.findById(CotizacionDTO.class,idToCotizacion);
		}
		// Obtiene el conducto de cobro de la persona
		try {
			if (idToPersonaContratante != null && idConductoCobro != null) {
				cuentaPagoDTO = pagoService.obtenerConductoCobro(idToPersonaContratante,Long.valueOf(idConductoCobro));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	public String complementarInfoCuentaInciso() {
		if (cuentaPagoDTO == null) {
			cuentaPagoDTO = new CuentaPagoDTO();
		}
		switch (getIdMedioPago().intValue()) {
		case 4:
			// Tarjeta de credito
			if (cuentaPagoDTO.getDiaPago() == null) {
				Calendar calendar = Calendar.getInstance();
				cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
			}
			if(cuentaPagoDTO.getTipoTarjeta() != null) {
				tipoTarjeta = cuentaPagoDTO.getTipoTarjeta().valor();
			}
			if(cuentaPagoDTO.getCuentaNoEncriptada() != null){
				setPromociones(listadoService.getPromocionesTC(cuentaPagoDTO.getCuentaNoEncriptada()));
			}else{
				//Prevenir error
				setPromociones(new HashMap<String, String>());
			}
			
			return "tc";
		case 8:
			// Domiciliacion
			return "domi";
		case 1386:
			// Cuenta Afirme
			// Banco default AFIME
			BancoEmisorDTO afirme = bancoEmisorFacade.findByName("AFIRME");
			cuentaPagoDTO.setIdBanco(afirme.getIdBanco().longValue());
			idBanco = afirme.getIdBanco();
			return "domi";
		default:
			// Cualquier otra cosa
			return "domi";
		}
	}

	/**
	 * Obtiene los datos el titular
	 */
	public void prepareComplementarInfoTitularInciso() {
		// Obtiene la cotizacion
		if (getIdToCotizacion() != null) {
			cotizacionDTO = entidadService.findById(CotizacionDTO.class,
					idToCotizacion);
		}
	}
	
	public void prepareDomicilio() {
		// Pais-Municipio-Ciudad
		setEstadoMap(listadoService.getMapEstados("PAMEXI"));
		if (cuentaPagoDTO != null && cuentaPagoDTO.getDomicilio() != null) {
			// Carga los estados para Mexico
			setEstadoMap(listadoService.getMapEstados("PAMEXI"));
			
			if (cuentaPagoDTO.getDomicilio().getClaveEstado() != null) {
				setMunicipioMap(listadoService
						.getMapMunicipiosPorEstado(cuentaPagoDTO.getDomicilio().getClaveEstado()));
			}else{
				setMunicipioMap(new HashMap<String, String>());
			}
			if (cuentaPagoDTO.getDomicilio().getClaveCiudad() != null) {
				setColoniasMap(listadoService.getMapColonias(cuentaPagoDTO
						.getDomicilio().getClaveCiudad()));
			}else{
				setColoniasMap(new HashMap<String, String>());
			}
		}else{
			setMunicipioMap(new HashMap<String, String>());
			setColoniasMap(new HashMap<String, String>());
		}
	}	
	
	public String complementarInfoTitularInciso() {
		try {
			if (!datosIguales) {
				cuentaPagoDTO = new CuentaPagoDTO();
			} else {
				if (idConductoCobro == null) {
					cuentaPagoDTO = pagoService.obtenerConductoCobroCliente(idToPersonaContratante);
				} else {
					cuentaPagoDTO = pagoService.obtenerConductoCobro(idToPersonaContratante,Long.valueOf(idConductoCobro));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		prepareDomicilio();		
		return SUCCESS;
	}	

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setIncisosCobranza(List<CobranzaIncisoDTO> incisosCobranza) {
		this.incisosCobranza = incisosCobranza;
	}

	public List<CobranzaIncisoDTO> getIncisosCobranza() {
		return incisosCobranza;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public void setDescripcionesIncisos(Set<String> descripcionesIncisos) {
		this.descripcionesIncisos = descripcionesIncisos;
	}

	public Set<String> getDescripcionesIncisos() {
		return descripcionesIncisos;
	}

	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}

	public List<NegocioSeccion> getNegocioSeccionList() {
		return negocioSeccionList;
	}

	public void setPaquetes(Map<Long, String> paquetes) {
		this.paquetes = paquetes;
	}

	public Map<Long, String> getPaquetes() {
		return paquetes;
	}

	public void setMedioPagoDTOs(List<MedioPagoDTO> medioPagoDTOs) {
		this.medioPagoDTOs = medioPagoDTOs;
	}

	public List<MedioPagoDTO> getMedioPagoDTOs() {
		return medioPagoDTOs;
	}

	public void setConductosDeCobro(Map<Long, String> conductosDeCobro) {
		this.conductosDeCobro = conductosDeCobro;
	}

	public Map<Long, String> getConductosDeCobro() {
		return conductosDeCobro;
	}

	public void setIncisos(List<IncisoCotizacionDTO> incisos) {
		this.incisos = incisos;
	}

	public List<IncisoCotizacionDTO> getIncisos() {
		return incisos;
	}

	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdConductoCobro(Long idConductoCobro) {
		this.idConductoCobro = idConductoCobro;
	}

	public Long getIdConductoCobro() {
		return idConductoCobro;
	}

	public void setEsquemaPagoCotizacionDTO(EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO) {
		this.esquemaPagoCotizacionDTO = esquemaPagoCotizacionDTO;
	}

	public EsquemaPagoCotizacionDTO getEsquemaPagoCotizacionDTO() {
		return esquemaPagoCotizacionDTO;
	}

	public void setClientesCobro(Map<BigDecimal, String> clientesCobro) {
		this.clientesCobro = clientesCobro;
	}

	public Map<BigDecimal, String> getClientesCobro() {
		return clientesCobro;
	}

	public void setMapDatosIguales(Map<Boolean,String> mapDatosIguales) {
		this.mapDatosIguales = mapDatosIguales;
	}

	public Map<Boolean,String> getMapDatosIguales() {
		return mapDatosIguales;
	}

	
	public CuentaPagoDTO getCuentaPagoDTO() {
		return cuentaPagoDTO;
	}

	public void setCuentaPagoDTO(CuentaPagoDTO cuentaPagoDTO) {
		this.cuentaPagoDTO = cuentaPagoDTO;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public Integer getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setPromociones(Map<String, String> promociones) {
		this.promociones = promociones;
	}

	public Map<String, String> getPromociones() {
		return promociones;
	}

	public Boolean getDatosIguales() {
		return datosIguales;
	}

	public void setDatosIguales(Boolean datosIguales) {
		this.datosIguales = datosIguales;
	}

	public void setEstadoMap(Map<String, String> estadoMap) {
		this.estadoMap = estadoMap;
	}

	public Map<String, String> getEstadoMap() {
		return estadoMap;
	}

	public void setMunicipioMap(Map<String, String> municipioMap) {
		this.municipioMap = municipioMap;
	}

	public Map<String, String> getMunicipioMap() {
		return municipioMap;
	}

	public void setColoniasMap(Map<String, String> coloniasMap) {
		this.coloniasMap = coloniasMap;
	}

	public Map<String, String> getColoniasMap() {
		return coloniasMap;
	}

	public void setPaisesMap(Map<String, String> paisesMap) {
		this.paisesMap = paisesMap;
	}

	public Map<String, String> getPaisesMap() {
		return paisesMap;
	}

	public void setBancos(List<BancoEmisorDTO> bancos) {
		this.bancos = bancos;
	}

	public List<BancoEmisorDTO> getBancos() {
		return bancos;
	}
}
