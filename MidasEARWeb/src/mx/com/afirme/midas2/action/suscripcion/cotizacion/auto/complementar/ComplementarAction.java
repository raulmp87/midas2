package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ComplementarAction extends BaseAction implements Preparable {


	private Long id;
	private String tipoAccion;
	private String claveNegocio;
	private TipoAccionDTO tipoAccionDTO;
	private BigDecimal cotizacionId;
	private BigDecimal incisoId;	
	private String tabActiva;
	private Short soloConsulta = 0;
	private CotizacionDTO cotizacionDTO;
	private Short habilitaCobranzaInciso = 0;
	private EntidadService entidadService;
	
	private static final Logger LOG = Logger.getLogger(ComplementarAction.class);
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ComplementarAction() {
		tipoAccionDTO = new TipoAccionDTO();
	}
	

	
	@Override
	public void prepare() throws Exception {
	}
	
	public String iniciarComplementar(){
		long start = System.currentTimeMillis();
		LOG.info("Entrando a iniciarComplementar" );
		if(cotizacionId != null){
			setCotizacionDTO(entidadService.findById(CotizacionDTO.class, cotizacionId));			
			if(cotizacionDTO != null 
					&& cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas().intValue() == 1
					&& (cotizacionDTO.getTipoAgrupacionRecibos() == null || 
							cotizacionDTO.getTipoAgrupacionRecibos().equals(CotizacionDTO.TIPO_AGRUPACION_RECIBOS.UBICACION.getValue()))){
				try{
					if(cotizacionDTO.getSolicitudDTO().getNegocio().getAplicaReciboGlobal() != null && 
							cotizacionDTO.getSolicitudDTO().getNegocio().getAplicaReciboGlobal()){
						habilitaCobranzaInciso = 1;
					}	
				}catch(Exception e){					
				}
			}
		}
		LOG.info("Saliendo de inciarComplementar : " + ((System.currentTimeMillis() - start) / 1000.0));
		return SUCCESS;
	}
	
	public void prepareVerCotizacion(){
		
	}
	
	public String verCotizacion(){
		return SUCCESS;
	}

	public TipoAccionDTO getTipoAccionDTO() {
		return tipoAccionDTO;
	}

	public void setTipoAccionDTO(TipoAccionDTO tipoAccionDTO) {
		this.tipoAccionDTO = tipoAccionDTO;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio.trim();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getTipoAccion() {
		return tipoAccion;
	}
	
	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}



	public void setCotizacionId(BigDecimal cotizacionId) {
		this.cotizacionId = cotizacionId;
	}



	public BigDecimal getCotizacionId() {
		return cotizacionId;
	}



	public void setIncisoId(BigDecimal incisoId) {
		this.incisoId = incisoId;
	}



	public BigDecimal getIncisoId() {
		return incisoId;
	}



	public String getTabActiva() {
		return tabActiva;
	}



	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}



	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}



	public Short getSoloConsulta() {
		return soloConsulta;
	}



	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}



	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}



	public void setHabilitaCobranzaInciso(Short habilitaCobranzaInciso) {
		this.habilitaCobranzaInciso = habilitaCobranzaInciso;
	}



	public Short getHabilitaCobranzaInciso() {
		return habilitaCobranzaInciso;
	}

	
}