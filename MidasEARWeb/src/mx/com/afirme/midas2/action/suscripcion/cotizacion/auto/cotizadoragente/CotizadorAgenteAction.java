package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.cotizadoragente;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import mx.com.afirme.midas.agente.AgenteDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos.DocAnexosService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizadorAgenteAction extends BaseAction  implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3020595047158357880L;
	public static final Logger LOG = Logger.getLogger(CotizadorAgenteAction.class);
	private String pdfDownload;
	private CotizacionDTO cotizacion;
	private IncisoCotizacionDTO incisoCotizacion;
	private Agente agente;
	private int soloConsulta = 0;
	private boolean updateSolicitud = false;
	private BigDecimal idToCotizacion;
	private BigDecimal negocioSeccionId;
	private BigDecimal primaAIgualar = null;
	private Boolean cerrarVentanaIgualacion = false;
	private String tabActiva;
	private BigDecimal idControlArchivo;
	private String nombreArchivoPrima;
	private Boolean mostrarSeleccionarAgente = true;
	private Boolean validacionesPendientes = false;
	private BigDecimal numInciso;
	
	private List<Negocio> negocioAgenteList = new ArrayList<Negocio>(1);
	private Map<Long, String> negocioProductoList;
	private Map<BigDecimal, String> negocioTipoPolizaList;
	private Map<Integer, String> formasdePagoList;
	private Map<Long, Double> derechosPolizaList = new HashMap<Long, Double>(1);
	private List<CoberturaCotizacionDTO> coberturaCotizacionList;
	
	//Almacenar listado de vigencias asociadas al negocio y vigencia default
	private Map<Integer, String> tiposVigenciaList = new HashMap<Integer, String>(1);	
	private Integer vigenciaDefault = null;
	
	private NegocioService negocioService;
	private AgenteMidasService agenteMidasService;
	private CotizacionService cotizacionService;
	private EntidadService entidadService;
	private IncisoService incisoService;
	private ListadoService listadoService;
	private CalculoService calculoService;
	private NegocioTipoPolizaService negocioTipoPolizaService;
	private SolicitudAutorizacionService autorizacionService;
//	private NegocioDerechosService negocioDerechosService;
	private DocAnexosService docAnexosService;
	private FronterizosService fronterizosService;

	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;	
	
	@Override
	public void prepare() throws Exception {
		if(idToCotizacion != null){ //COTIZADOR AGENTES
			cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		}
	}
	
	private void init(){
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			cotizacion.getSolicitudDTO().setCodigoAgente(BigDecimal.valueOf(agenteUsuarioActual.getId()));
			mostrarSeleccionarAgente = false;
		}else{
			mostrarSeleccionarAgente = true;
		}
		if(cotizacion.getSolicitudDTO().getCodigoAgente() != null && cotizacion.getSolicitudDTO().getCodigoAgente().intValue() > 0){
			negocioAgenteList = negocioService
					.listarNegociosPorAgenteUnionNegociosLibres(
							Integer.valueOf(cotizacion.getSolicitudDTO()
									.getCodigoAgente().intValue()), Negocio.CLAVE_NEGOCIO_AUTOS);
			if(negocioAgenteList.size() == 1){
				if(cotizacion.getSolicitudDTO().getNegocio() == null){
					cotizacion.getSolicitudDTO().setNegocio(new Negocio());
				}
				cotizacion.getSolicitudDTO().getNegocio().setIdToNegocio(negocioAgenteList.get(0).getIdToNegocio());				
			}	
				final Agente filtroAgente = new Agente();
				filtroAgente.setId(cotizacion.getSolicitudDTO().getCodigoAgente().longValue());
				final List<AgenteView> agenteViewResult =  agenteMidasService.findByFilterLightWeight(filtroAgente);
				if (agenteViewResult != null && !agenteViewResult.isEmpty()) {
					agente = new Agente();
					agente.setIdAgente(agenteViewResult.get(0).getIdAgente());
					agente.setPersona(new Persona());
					agente.getPersona().setNombreCompleto(agenteViewResult.get(0).getNombreCompleto());
				}

		}else{
			/*
			Negocio negocio = new Negocio();
			negocio.setClaveNegocio("A");
			negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
			negocioAgenteList = negocioService.findByFilters(negocio);
			*/
			negocioAgenteList = new ArrayList<Negocio>(1);
		}
		
		if(cotizacion.getIdToCotizacion() != null){			
			negocioProductoList = listadoService.getMapNegProductoPorNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			Long idToNegProducto = listadoService.getNegocioProducto(
					cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto());
			negocioTipoPolizaList = listadoService.getMapNegTipoPolizaPorNegProductoFlotillaOIndividual(idToNegProducto,0);
			
			NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(idToNegProducto, cotizacion.getTipoPolizaDTO().getIdToTipoPoliza());
			cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
			formasdePagoList = listadoService.getMapFormasdePagoByNegTipoPoliza(negocioTipoPoliza.getIdToNegTipoPoliza());
			//derechosPolizaList = cotizacion.getSolicitudDTO().getNegocio().getNegocioDerechoPolizas();
			try{
				derechosPolizaList = listadoService.getMapDerechosPolizaByNegocio(
						cotizacion.getIdToCotizacion(), 
						(incisoCotizacion.getId().getNumeroInciso() != null)? incisoCotizacion.getId().getNumeroInciso() : new BigDecimal(1) );
			}catch(Exception e){
			}
			try {
				calculoService.obtenerResumenCotizacion(cotizacion, false);
			}catch(Exception e) {
				LOG.error(e);
			}
			
			//Valida solicitudes de autorizacion
			List<SolicitudExcepcionCotizacion> lista = autorizacionService.listarSolicitudesInvalidas(cotizacion.getIdToCotizacion());
			if(lista != null && !lista.isEmpty()){
				validacionesPendientes = true;
			}
			
			tiposVigenciaList = listadoService.obtenerTiposVigenciaNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			vigenciaDefault = listadoService.obtenerVigenciaDefault(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		}else{
			negocioProductoList = new HashMap<Long, String>(1);
			negocioTipoPolizaList = new HashMap<BigDecimal, String>(1);
			formasdePagoList = new HashMap<Integer, String>(1);
			derechosPolizaList = new HashMap<Long, Double>(1);
			tiposVigenciaList = new HashMap<Integer, String>(1);
			vigenciaDefault = -1;
		}
	}
	
	public String mostrarContenedor(){
		if(cotizacion == null){
			cotizacion = new CotizacionDTO();
			cotizacion.setIdToCotizacion(null);
			cotizacion.setClaveEstatus(null);
		}
		return SUCCESS;
	}
	
	public void prepareMostrar(){
		if(cotizacion == null){
			cotizacion = new CotizacionDTO();
			cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
			
			SolicitudDTO solicitud = new SolicitudDTO();
			cotizacion.setSolicitudDTO(solicitud);
			cotizacion.setFechaInicioVigencia(new Date());
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(new Date());
			gcFechaFin.add(GregorianCalendar.YEAR, 1);
			cotizacion.setFechaFinVigencia(gcFechaFin.getTime());
			
			cotizacion.setPorcentajeDescuentoGlobal(new Double(0));
			cotizacion.setPorcentajebonifcomision(new Double(0));
			cotizacion.setFechaSeguimiento(new Date());
		}
	}
	
	public String mostrar(){
		init();
		return SUCCESS;
	}
	
	private void validaCotizacion(){
		if(cotizacion.getIdToCotizacion()!=null){
			try{
				if(updateSolicitud){
						cotizacion = cotizacionService.updateSolicitudCotizacion(cotizacion);
				}
				cotizacion = cotizacionService.guardarCotizacion(cotizacion);
				this.setMensajeExito();
			}catch(Exception ex){
				this.setMensajeError(ex.getMessage());
			}
		 }else{
			 //Guarda cotizacion
			 cotizacion =cotizacionService.crearCotizacion(cotizacion,null);
			 this.setMensajeExitoPersonalizado("Se cre\u00F3 la cotizaci\u00F3n:" + cotizacion.getIdToCotizacion());
			 this.guardarBitacora();
		 }
		try{
			idToCotizacion = cotizacion.getIdToCotizacion();
			recalcularCoberturasInciso();
		
			//Generar condiciones obligatorias
			condicionEspecialCotizacionService.generaCondicionesBaseCompletas(idToCotizacion);
			
			//Calcular Descuento Global (Ya no se aplica como descuento, solamente se utiliza para la exclusion de bonos)
			cotizacion.setPorcentajeDescuentoGlobal(calculoService.calculaPorcentajeDescuentoGlobal(idToCotizacion));
			
			docAnexosService.generateCobDocumentsByCotizacion(cotizacion.getIdToCotizacion());
		}catch(Exception e){
			LOG.error(e);
		}
	}
	
	public void prepareGeneraCotizacionAgente() throws Exception {
		if(cotizacion.getIdToCotizacion()!=null){
			  Long idToNegocio = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
			  Long idToNegProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto();
			  BigDecimal idToNegTipoPoliza = cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza();
			  BigDecimal idAgente = cotizacion.getSolicitudDTO().getCodigoAgente();
			  cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());			  
			  //Cambio de Negocio
			  NegocioProducto negocioProducto = entidadService.findById(NegocioProducto.class, idToNegProducto);
			  NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
			  if(idToNegocio != cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio() ||
					  negocioProducto.getProductoDTO().getIdToProducto() !=  cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto() ||
					  negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza() != cotizacion.getTipoPolizaDTO().getIdToTipoPoliza() ||
					  idAgente != cotizacion.getSolicitudDTO().getCodigoAgente()){
				  updateSolicitud = true;
			  }
		}
		try {
			if(incisoCotizacion == null){
				incisoCotizacion = new IncisoCotizacionDTO();
				incisoCotizacion.setId(new IncisoCotizacionId());
			} else {
				if (incisoCotizacion.getId() != null
						&& incisoCotizacion.getId().getIdToCotizacion() != null
						&& incisoCotizacion.getId().getNumeroInciso() != null) {
					incisoCotizacion = entidadService.findById(
							IncisoCotizacionDTO.class, incisoCotizacion.getId());
				}
				if (incisoCotizacion.getId() != null && incisoCotizacion.getId().getNumeroInciso() == null
						&& incisoCotizacion.getNumeroSecuencia() != null) {
					List<IncisoCotizacionDTO> listTemp = incisoService
							.listarIncisosConFiltro(incisoCotizacion);
					if (listTemp != null && !listTemp.isEmpty()) {
						incisoCotizacion = listTemp.get(0);
					} else{
						incisoCotizacion = new IncisoCotizacionDTO();
					}
					
				}
				if (incisoCotizacion.getIncisoAutoCot() != null
						&& incisoCotizacion.getIncisoAutoCot()
								.getNegocioSeccionId() != null) {
					negocioSeccionId = BigDecimal.valueOf(incisoCotizacion
							.getIncisoAutoCot().getNegocioSeccionId());
					incisoCotizacion.setIdToSeccion(negocioSeccionId);
				}
				
				if (incisoCotizacion.getId() != null
						&& incisoCotizacion.getId().getIdToCotizacion() != null) {
					CotizacionDTO cotizacionDTO = entidadService.findById(
							CotizacionDTO.class, incisoCotizacion.getId()
									.getIdToCotizacion());
					incisoCotizacion.setCotizacionDTO(cotizacionDTO);
				}
				
			}
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
	}
	
	private void recalcularCoberturasInciso(){
		IncisoCotizacionDTO incisoGuardado = null;
		boolean riesgosValidos = true;
		try{
			if(incisoCotizacion != null && incisoCotizacion.getId() != null){
				incisoGuardado = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
				if(incisoGuardado != null){
					NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
					SeccionDTO seccionDTO = negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
					if(incisoGuardado.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccionDTO.getIdToSeccion().longValue()){
						incisoService.borrarInciso(incisoCotizacion);
						incisoCotizacion.getId().setNumeroInciso(null);
					}
				}
			}
			if(cotizacion != null){
				if(incisoCotizacion.getId() == null){
					incisoCotizacion.setId(new IncisoCotizacionId());
				}
				if(incisoCotizacion.getId().getIdToCotizacion() == null){
					incisoCotizacion.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());
				}
			}
			
			incisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
			
			incisoCotizacion = incisoService.prepareGuardarInciso(
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
					coberturaCotizacionList);
						
			if(incisoCotizacion.getId().getNumeroInciso() != null){	
				riesgosValidos = listadoService.validaDatosRiesgosCompletosCotizacion(
						incisoCotizacion.getId().getIdToCotizacion(),incisoCotizacion.getId().getNumeroInciso());
				if (incisoCotizacion.getIncisoAutoCot().getCodigoPostal()==null || incisoCotizacion.getIncisoAutoCot().getCodigoPostal().toString().trim()==""){
					String mensaje = String.format("El C\u00F3digo postal es obligatorio");
				    setMensajeError(mensaje);
				}else{
					
					
					if(!riesgosValidos){//20
//						setNextFunction("jQuery('#btnDatosAdicionalesPaquete').show()");
						setNextFunction("mostrarDatosRiesgo("
								+ incisoCotizacion.getId().getIdToCotizacion()
								+ ", " + incisoCotizacion.getId().getNumeroInciso()
								+ ");jQuery('#btnGuardar').css('display', 'none');jQuery('#btnDatosAdicionalesPaquete').show()");
							
						setMensajeExitoPersonalizado("Faltan guardar los datos adicionales del paquete");
						
					}else{
						super.setNextFunction(null);
						calculoService.calcular(incisoCotizacion);
					}	
				}			
				
			}else{
				setMensajeError("Error no se pudo realizar correctamente la accion solicitada");
			}	
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			LOG.error("Error al calcular el inciso", ex);
		}
	}
	
	public String generaCotizacionAgente() {
		validaCotizacion();			
		init();		
		return SUCCESS;
	}
	
	public Integer indicex;
	public String generaCotizacionAgenteCobertura(){	
		validaCotizacion();	
		if(indicex==null){
			indicex = new Integer(0);
		}
		super.setNextFunction("mostrarDaniosOcasionadosPorLaCarga2("+indicex+")");		
		return SUCCESS;
	}
	
	public String verIgualarPrimasAgente(){		
		if(idControlArchivo != null){
			try{
				ControlArchivoDTO archivo = entidadService.findById(ControlArchivoDTO.class, idControlArchivo);
				this.nombreArchivoPrima = archivo.getNombreArchivoOriginal();
			}catch(Exception e){
			}
		}
		return SUCCESS;
	}
	
	public String igualarPrimasAgente(){
		try{
			if(idToCotizacion == null){
				throw new RuntimeException("No existe cotizacion");
			}
			if(primaAIgualar == null){
				throw new RuntimeException("Prima Total debe existir y ser mayor a cero");
			}
			calculoService.igualarPrima(idToCotizacion, primaAIgualar.doubleValue(), true);
			
			TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService.validaIgualacionCotizadorAgentes(idToCotizacion);
			Long solicitudId = null;
			if(terminarCotizacionDTO != null){
				if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
					solicitudId = autorizacionService.guardarSolicitudDeAutorizacion(idToCotizacion, "", 
							usuarioService.getUsuarioActual().getId().longValue(), usuarioService.getUsuarioActual().getId().longValue(),
							"A", terminarCotizacionDTO.getExcepcionesList());
				}
			}
			super.setNextFunction("verDetalleCotizacionAgente("+idToCotizacion+",0)");
			if(solicitudId == null){
				super.setMensajeExito();
			}else{
				super.setMensajeExitoPersonalizado("Acci\u00F3n realizada correctamente. Se cre\u00F3 la solicitud de autorizaci\u00F3n: " +solicitudId);
			}
			cerrarVentanaIgualacion = true;
		}catch(Exception ex){
			super.setMensajeError(ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage());
		}
		return SUCCESS;	
	}
	
	public String restaurarPrimaCotizacionAgente(){
		try{
			if(idToCotizacion == null){
				throw new RuntimeException("No existe cotizacion");
			}			
			calculoService.restaurarPrimaCotizacion(idToCotizacion);
			super.setNextFunction("verDetalleCotizacionAgente("+idToCotizacion+",0)");
			super.setMensajeExito();
			cerrarVentanaIgualacion = true;
		}catch(Exception ex){
			super.setMensajeError(ex.getMessage());
		}
		return SUCCESS;	
	}
	
	private void guardarBitacora(){
		if (cotizacion!=null && incisoCotizacion!=null 
				&& incisoCotizacion.getIncisoAutoCot()!=null 
				&& incisoCotizacion.getIncisoAutoCot().getNumeroSerie()!=null
				&& !incisoCotizacion.getIncisoAutoCot().getNumeroSerie().equals("")){
			String serie = incisoCotizacion.getIncisoAutoCot().getNumeroSerie();
			try{
				for (CoberturaCotizacionDTO item: coberturaCotizacionList){					
					if ( item.getConsultaAsegurada() != null 
							&& item.getConsultaAsegurada()
							&& item.getValorSumaAsegurada() != null 
							&&	item.getId().getIdToSeccion() != null ){
						SeccionDTO seccion = entidadService.findById(SeccionDTO.class, item.getId().getIdToSeccion());
						fronterizosService.guardarBitacora(cotizacion.getIdToCotizacion(),new BigDecimal(1),
								null,serie,new BigDecimal(item.getValorSumaAsegurada()),BigDecimal.ZERO,seccion.getTipoValidacionNumSerie());		
					}
				}
			}
			catch(Exception e){
				LOG.error(e);
			}
		}
	}

	public String validarDatosComplementarios() {
		return SUCCESS;
	}
	
	public void validarCedulaAgente() throws SystemException, IOException {
		Long codigoAgente = cotizacion.getSolicitudDTO().getCodigoAgente().longValue();
		Agente agenteActual = entidadService.findById(Agente.class, codigoAgente);
		ValorCatalogoAgentes tipoCedula = agenteActual.getTipoCedula();
		BigDecimal idNegocioSeccion = BigDecimal.valueOf(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId().longValue());
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idNegocioSeccion);
		SeccionDTO seccion = negocioSeccion.getSeccionDTO();
		
		boolean esCedulaValida = AgenteDN.getInstancia().validarCedulaAgente(tipoCedula.getId(), AgenteDN.PROPIEDAD_SECCION, seccion.getIdToSeccion());
		String mensaje = "";
		if(!esCedulaValida)
			mensaje = String.format("El agente de Tipo de Cedula %s No cuenta con permisos de venta para la linea de negocio %s seleccionada", tipoCedula.getClave(), seccion.getDescripcion());
		HttpServletResponse response = ServletActionContext.getResponse();
		PrintWriter printWriter = response.getWriter();
		printWriter.write(mensaje);
		printWriter.close();
	}
	
	@Autowired
	@Qualifier("condicionEspecialCotizacionServiceEJB")
	public void setCondicionEspecialCotizacionService(CondicionEspecialCotizacionService condicionEspecialCotizacionService) {
		this.condicionEspecialCotizacionService = condicionEspecialCotizacionService;
	}
	
	@Autowired
	@Qualifier("docAnexosServiceEJB")
	public void setDocAnexosService(DocAnexosService docAnexosService) {
		this.docAnexosService = docAnexosService;
	}
	
//	@Autowired
//	@Qualifier("negocioDerechosServiceEJB")
//	public void setNegocioDerechosService(NegocioDerechosService negocioDerechosService) {
//		this.negocioDerechosService = negocioDerechosService;
//	}	
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}	
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("calculoIncisoAutosServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("negocioTipoPolizaServiceEJB")
	public void setNegocioTipoPolizaService(NegocioTipoPolizaService negocioTipoPolizaService) {
		this.negocioTipoPolizaService = negocioTipoPolizaService;
	}
	
	@Autowired
	@Qualifier("solicitudAutorizacionService")
	public void setAutorizacionService(
			SolicitudAutorizacionService autorizacionService) {
		this.autorizacionService = autorizacionService;
	}

	@Autowired
	@Qualifier("fronterizosServiceEJB")
	public void setFronterizosService(FronterizosService fronterizosService) {
		this.fronterizosService = fronterizosService;
	}

	public void setFormasdePagoList(Map<Integer, String> formasdePagoList) {
		this.formasdePagoList = formasdePagoList;
	}

	public Map<Integer, String> getFormasdePagoList() {
		return formasdePagoList;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}


	public Map<Long, Double> getDerechosPolizaList() {
		return derechosPolizaList;
	}

	public void setDerechosPolizaList(Map<Long, Double> derechosPolizaList) {
		this.derechosPolizaList = derechosPolizaList;
	}

	public void setNegocioAgenteList(List<Negocio> negocioAgenteList) {
		this.negocioAgenteList = negocioAgenteList;
	}

	public List<Negocio> getNegocioAgenteList() {
		return negocioAgenteList;
	}

	public void setNegocioProductoList(Map<Long, String> negocioProductoList) {
		this.negocioProductoList = negocioProductoList;
	}

	public Map<Long, String> getNegocioProductoList() {
		return negocioProductoList;
	}

	public void setNegocioTipoPolizaList(Map<BigDecimal, String> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public Map<BigDecimal, String> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setSoloConsulta(int soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public int getSoloConsulta() {
		return soloConsulta;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public void setCoberturaCotizacionList(List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		this.coberturaCotizacionList = coberturaCotizacionList;
	}

	public List<CoberturaCotizacionDTO> getCoberturaCotizacionList() {
		return coberturaCotizacionList;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setPrimaAIgualar(BigDecimal primaAIgualar) {
		this.primaAIgualar = primaAIgualar;
	}

	public BigDecimal getPrimaAIgualar() {
		return primaAIgualar;
	}

	public void setCerrarVentanaIgualacion(Boolean cerrarVentanaIgualacion) {
		this.cerrarVentanaIgualacion = cerrarVentanaIgualacion;
	}

	public Boolean getCerrarVentanaIgualacion() {
		return cerrarVentanaIgualacion;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setIdControlArchivo(BigDecimal idControlArchivo) {
		this.idControlArchivo = idControlArchivo;
	}

	public BigDecimal getIdControlArchivo() {
		return idControlArchivo;
	}

	public void setNombreArchivoPrima(String nombreArchivoPrima) {
		this.nombreArchivoPrima = nombreArchivoPrima;
	}

	public String getNombreArchivoPrima() {
		return nombreArchivoPrima;
	}

	public void setMostrarSeleccionarAgente(Boolean mostrarSeleccionarAgente) {
		this.mostrarSeleccionarAgente = mostrarSeleccionarAgente;
	}

	public Boolean getMostrarSeleccionarAgente() {
		return mostrarSeleccionarAgente;
	}

	public void setValidacionesPendientes(Boolean validacionesPendientes) {
		this.validacionesPendientes = validacionesPendientes;
	}

	public Boolean getValidacionesPendientes() {
		return validacionesPendientes;
	}

	public BigDecimal getNumInciso() {
		return numInciso;
	}

	public void setNumInciso(BigDecimal numInciso) {
		this.numInciso = numInciso;
	}

	public String getPdfDownload() {
		return pdfDownload;
	}

	public void setPdfDownload(String pdfDownload) {
		this.pdfDownload = pdfDownload;
	}

	public Integer getIndicex() {
		return indicex;
	}

	public void setIndicex(Integer indicex) {
		this.indicex = indicex;
	}
	/**
	 * @return the tiposVigenciaList
	 */
	public Map<Integer, String> getTiposVigenciaList() {
		return tiposVigenciaList;
	}

	/**
	 * @param tiposVigenciaList the tiposVigenciaList to set
	 */
	public void setTiposVigenciaList(Map<Integer, String> tiposVigenciaList) {
		this.tiposVigenciaList = tiposVigenciaList;
	}

	/**
	 * @return the vigenciaDefault
	 */
	public Integer getVigenciaDefault() {
		return vigenciaDefault;
	}

	/**
	 * @param vigenciaDefault the vigenciaDefault to set
	 */
	public void setVigenciaDefault(Integer vigenciaDefault) {
		this.vigenciaDefault = vigenciaDefault;
	}
	
}
