package mx.com.afirme.midas2.action.suscripcion.solicitud.asignar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudPolizaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AsignarSolicitudPolizaAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private SolicitudPolizaService solicitudPolizaService;
	private SolicitudDTO solicitud;
	private List<Usuario> usuariosCoordinadores = new ArrayList<Usuario>();
	private List<Usuario> usuariosSuscriptores = new ArrayList<Usuario>();
	private Boolean esMesaControl=true;
	private Short tipoAsignacion;
	private CotizacionDTO cotizacionDTO;
	private Comentario comentario;
	private String valor;
	private Map<String, String> tipoAsignaciones = new HashMap<String, String>();
	
	
	@Override
	public void prepare() throws Exception {
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Coordinador")) {
			tipoAsignaciones.put("1", "Coordinador");
		}
		
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Suscriptor")) {
			tipoAsignaciones.put("2", "Suscriptor");	
		}
		
		//Seleccionar el valor default en base al permiso del usuario.
		if (getTipoAsignacion() == null
				&& usuarioService
						.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Coordinador")) {
			tipoAsignacion = (short) 1;
		} else if (getTipoAsignacion() == null
				&& usuarioService
						.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Suscriptor")) {
			tipoAsignacion = (short) 2;
		}		
		

		if(getId() != null){
			if(solicitud==null)solicitud = new SolicitudDTO();
			solicitud = solicitudPolizaService.findById(getId());
		}
	}
	
	public String mostrarVentana() {
		try{
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Coordinador")) {
			usuariosCoordinadores = usuarioService
					.buscarUsuariosPorNombreRolSimple(sistemaContext
							.getRolCoordinadorAutos());
			if(usuariosCoordinadores == null)usuariosCoordinadores = new ArrayList<Usuario>();
		}else{
			usuariosCoordinadores = new ArrayList<Usuario>();
		}

		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Suscriptor")) {
			usuariosSuscriptores = usuarioService
					.buscarUsuariosPorNombreRolSimple(sistemaContext
							.getRolSuscriptorAutos());
			if(usuariosSuscriptores == null)usuariosSuscriptores = new ArrayList<Usuario>();
		}else{
			usuariosSuscriptores = new ArrayList<Usuario>();
		}
		}catch(Exception e){
			usuariosCoordinadores = new ArrayList<Usuario>();
			usuariosSuscriptores = new ArrayList<Usuario>();
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String asignar(){
	  try{		 
		if(tipoAsignacion.equals("1")){
		    solicitudPolizaService.asignarCoordinador(solicitud);
		}else{
			solicitud = solicitudPolizaService.asignarSuscriptor(solicitud);
            solicitudPolizaService.comentario(solicitud,comentario,getValor());
            super.setMensajeExito();
			setNextFunction("ventanaAsignacion.close();");
		}
	  }catch(Exception ex){
		  super.setMensajeError(MENSAJE_ERROR_GENERAL);
	  }
	  return SUCCESS;
	  
	  
	}	
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	@Autowired
	@Qualifier("solicitudPolizaServiceEJB")
	public void setSolicitudPolizaService(
			SolicitudPolizaService solicitudPolizaService) {
		this.solicitudPolizaService = solicitudPolizaService;
	}
	public SolicitudDTO getSolicitud() {
		return solicitud;
	}
	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}
	public Boolean getEsMesaControl() {
		return esMesaControl;
	}
	public void setEsMesaControl(Boolean esMesaControl) {
		this.esMesaControl = esMesaControl;
	}
	public Short getTipoAsignacion() {
		return tipoAsignacion;
	}
	public void setTipoAsignacion(Short tipoAsignacion) {
		this.tipoAsignacion = tipoAsignacion;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public List<Usuario> getUsuariosCoordinadores() {
		return usuariosCoordinadores;
	}

	public void setUsuariosCoordinadores(List<Usuario> usuariosCoordinadores) {
		this.usuariosCoordinadores = usuariosCoordinadores;
	}

	public List<Usuario> getUsuariosSuscriptores() {
		return usuariosSuscriptores;
	}

	public void setUsuariosSuscriptores(List<Usuario> usuariosSuscriptores) {
		this.usuariosSuscriptores = usuariosSuscriptores;
	}

	public Comentario getComentario() {
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	public Map<String, String> getTipoAsignaciones() {
		return tipoAsignaciones;
	}

}
