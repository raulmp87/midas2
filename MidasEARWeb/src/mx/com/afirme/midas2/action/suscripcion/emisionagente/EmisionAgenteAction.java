package mx.com.afirme.midas2.action.suscripcion.emisionagente;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.clientesapi.dto.InterviewException;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import com.opensymphony.xwork2.Preparable;
@Component
@Scope("prototype")
public class EmisionAgenteAction extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToCotizacion;
	private Long id;
	private CotizacionDTO cotizacion;
	private PolizaDTO poliza;
	private Boolean aplicaEndoso = false;
	private EndosoService endosoService;
	private EntidadService entidadService;
	private CotizacionService cotizacionService;
	private EmisionService emisionService;
	private	List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private ClientesApiService  clienteRest;
	private EntrevistaDTO entrevista;
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@Autowired
	@Qualifier("emisionServiceEJB")
	public void setEmisionService(EmisionService emisionService) {
		this.emisionService = emisionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void prepare() throws Exception {

	}
	
	public void prepareEmitir(){
		if(idToCotizacion != null){
			cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			id = idToCotizacion.longValue();//para listado excepciones
		}
	}
	
	
	/**
	 * Invoca al cliente REST para almacenar la entrevista dentro de la
	 * aplicación cliente único Carga nuevamente la cotización para regresar al
	 * flujo de la emisión
	 * 
	 * @return regresa al flujo de emision
	 * 
	 * 
	 * */
	public String guardarEntrevista(){
		
		
		clienteRest.createInterview(cotizacion.getIdToPersonaContratante().longValue(), entrevista);
		cotizacion =entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());			
		
		
		return SUCCESS;
	}
	public String emitir(){
		
		if(cotizacion != null){
			try{
				TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(cotizacion.getIdToCotizacion());
				if(validacion != null){
					if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE.getEstatus().shortValue()){
						super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);					
						return ERROR;
					}else if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
						excepcionesList = 	validacion.getExcepcionesList();
						super.setMensajeError(validacion.getMensajeError());
						return "excepciones";	
					}else{
						super.setMensajeError(validacion.getMensajeError());				
						return ERROR;	
					}
				}
				
				
				ResponseEntity<Long> resp=null;
				try{
				 resp=clienteRest.clienteUnificado(null,cotizacion.getIdToPersonaContratante().longValue());
				}catch (HttpStatusCodeException e) {
					
					super.setMensajeError("No se encontro el usuario asociado a la poliza");				
					return ERROR;	
				}	
				try {
					clienteRest.findInterViews(cotizacion.getIdToPersonaContratante().longValue());
				} catch (InterviewException e) {
					
					
					 ClienteUnicoDTO cliente=clienteRest.findClienteUnicoById(resp.getBody());
					 
					 ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(cliente.getIdClienteUnico()));
						
					String validarPrima = entityPrima.getBody();
					if(validarPrima.contains("true")){
						LOG.info("\n\n\n\nprimaCalculada"+entityPrima.getBody());
					
						entrevista= new EntrevistaDTO();
						
						entrevista.setIdToCotizacion(cotizacion.getIdToCotizacion());
						if(cliente.getIdToPersonaString().compareTo("1") == 0){
							return "entrevistasPF";
						}
						else {
							entrevista.setPep(false );
							entrevista.setCuentaPropia(true);
							return "entrevistasPM";
						}
					}
				}
				Map<String, String> mensajeEmision = emisionService.emitir(cotizacion, false);
				String icono = mensajeEmision.get("icono");
				String mensaje = mensajeEmision.get("mensaje");
				if(icono.equals("30")){				
					String idPoliza = mensajeEmision.get("idpoliza");
					poliza = entidadService.findById(PolizaDTO.class, BigDecimal.valueOf(Long.valueOf(idPoliza)));
					List<EndosoDTO> endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", poliza.getIdToPoliza(), Boolean.TRUE);
					if(endosoPolizaList != null && endosoPolizaList.size()>0){
						EndosoDTO ultimoEndosoDTO = endosoPolizaList.get(endosoPolizaList.size() - 1);
						super.setNextFunction("$_imprimirDetallePoliza("
								+ poliza.getIdToPoliza() + ","
								+ ultimoEndosoDTO.getClaveTipoEndoso() + ","
								+ ultimoEndosoDTO.getValidFrom().getTime()
								+ ","
								+ ultimoEndosoDTO.getRecordFrom().getTime()+")");
					}
					super.setMensajeExitoPersonalizado("confirm:message:" + mensaje + " " + poliza.getNumeroPolizaFormateada() + " \u00BFDesea imprimir la poliza ahora\u003F");
					if(cotizacion.getIdToPersonaContratante()!=null){
						
						clienteRest.validatePeps(cotizacion.getIdToPersonaContratante().longValue(),poliza.getNumeroPolizaFormateada());
							
					}
					if(cotizacion.getIdToPersonaAsegurado()!=null){
						
						clienteRest.validatePeps(cotizacion.getIdToPersonaAsegurado().longValue(),poliza.getNumeroPolizaFormateada());
							
					}
					return SUCCESS;
				}else if(icono.equals("10")){
					super.setMensajeError(mensaje);
					return ERROR;
				}
				//TODO verificar este return
				return SUCCESS;				
			}catch(RuntimeException e){
				super.setMensajeError(MENSAJE_ERROR_GENERAL);
				return ERROR;
			}
			
		}
		super.setMensajeError("Contizacion invalida");
		return ERROR;
	}
	
	public String editar(){
		return SUCCESS;
	}
	
	
	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}

	public PolizaDTO getPoliza() {
		return poliza;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public void setExcepcionesList(
			List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAplicaEndoso() {
		return aplicaEndoso;
	}

	public void setAplicaEndoso(Boolean aplicaEndoso) {
		this.aplicaEndoso = aplicaEndoso;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	public EntrevistaDTO getEntrevista() {
		return entrevista;
	}

	public void setEntrevista(EntrevistaDTO entrevista) {
		this.entrevista = entrevista;
	}

}
