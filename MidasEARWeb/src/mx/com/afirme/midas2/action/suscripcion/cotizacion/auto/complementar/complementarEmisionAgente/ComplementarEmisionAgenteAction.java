package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.complementarEmisionAgente;

import java.math.BigDecimal;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.suscripcion.emision.EmisionAction;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.cliente.ClienteAsociadoJPAService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;

@Component
@Scope("prototype")
public class ComplementarEmisionAgenteAction extends BaseAction implements Preparable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = LoggerFactory.getLogger(ComplementarEmisionAgenteAction.class);
	
	private BigDecimal idToCotizacion;
	private BigDecimal idCliente;
	private CotizacionDTO cotizacionDTO;
	private ClienteDTO cliente;
	private MensajeDTO mensajeDTO;
	private EntidadService entidadService;
	private CotizacionService cotizacionService;
	private ClienteFacadeRemote clienteFacade;
	private ClienteAsociadoJPAService clienteAsociadoJPAService;
	private boolean negocioAsociado;
	private Long idToNegocio;
	private Negocio negocio;
	private Short soloConsulta = 0;
	
	private ClientesApiService clienteRest;
	
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}

	@Autowired
	@Qualifier("clienteAsociadoJPAServiceEJB")
	public void setClienteAsociadoJPAService(
			ClienteAsociadoJPAService clienteAsociadoJPAService) {
		this.clienteAsociadoJPAService = clienteAsociadoJPAService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void prepare() throws Exception {		
	}
	
	public void prepareVerComplementarEmision(){
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			setIdToNegocio(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
			// Valida que una Cotizacion tenga Clientes asociados al negocio
			
			if (!clienteAsociadoJPAService.listarClientesAsociados(idToNegocio).isEmpty()){
				setNegocioAsociado(true);
			}
			if(cotizacionDTO.getIdToPersonaContratante() != null){
				try {
					filtro.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
					filtro = clienteFacade.loadByIdNoAddress(filtro);
					setCliente(filtro);
					// Set nombre de Persona moral
					if(getCliente().getClaveTipoPersona() == 2){
						getCliente().setNombre(
								filtro.getNombreCompleto());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			setListadoEnSession(null);
		}
	}
	
	public String verComplementarEmision(){
		if(idToCotizacion != null){
			mensajeDTO = cotizacionService.validarListaParaEmitir(idToCotizacion);
		}
		return SUCCESS;
	}
	
	public String seleccionarContratante(){
		LOG.info(">>>>> Entrando contratante Action");
		if(idToCotizacion != null){
			
			LOG.info("-- Validar datos fiscales");
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);			
			if(cotizacionService.actualizarDatosContratanteCotizacion(idToCotizacion, idCliente, this.getUsuarioActual().getNombreUsuario()) == null){
				setMensajeError("confirm:message:Favor de complementar el C\u00F3digo postal en los datos fiscales del cliente seleccionado, \uu00BFDesea hacerlo ahora\u003F");
				setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				setNextFunction("complementarContratante("+idCliente+","+idToCotizacion+")");
				return SUCCESS;
			}
			
			//Validaciones Prima Mayor
			LOG.info("-- Validar pais y ocupación de cliente " + idCliente.longValue());
			try {
				ResponseEntity<Long> resp = clienteRest.clienteUnificado(null, idCliente.longValue());
				
				
				ClienteUnicoDTO clienteUnico = clienteRest.findClienteUnicoById(resp.getBody());
				
				LOG.info("-- Tipo de cliente " + clienteUnico.getClaveTipoPersonaString());
				
				if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("1")){
				
					if(clienteUnico.getOcupacionCNSF() == null || clienteUnico.getOcupacionCNSF().equals("")
							|| clienteUnico.getIdPaisNacimiento() == null || clienteUnico.getIdPaisNacimiento().equals("")){
						
						ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
						String validarPrima = entityPrima.getBody();
						
						if(validarPrima.contains("true")){
						
							setMensajeError("confirm:message:Favor de complementar el País de Nacimiento y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+idCliente+","+idToCotizacion+")");
							return SUCCESS;
						}
					}
				}else if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("2")){
					
					if(clienteUnico.getCveGiroCNSF() == null || clienteUnico.getCveGiroCNSF().equals("")
							|| clienteUnico.getIdPaisConstitucion() == null || clienteUnico.getIdPaisConstitucion().equals("")){
						
						ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
						String validarPrima = entityPrima.getBody();
						
						if(validarPrima.contains("true")){
							setMensajeError("confirm:message:Favor de complementar el País de Constituci\u00f3n y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+idCliente+","+idToCotizacion+")");
							return SUCCESS;
						}
					}
				}
				
				
			} catch (HttpStatusCodeException e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- seleccionarContratante():Validaciones Prima Mayor ", e);
				return ERROR;
			} catch (Exception e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- seleccionarContratante():Validaciones Prima Mayor ", e);
				return ERROR;
			}			
			
			super.setMensajeExito();
			// Si el nuevo contratante tiene un iva valida si cambio
			try{
				boolean cambioIVA = cotizacionService.validaIVACotizacion(idToCotizacion, this.getUsuarioActual().getNombreUsuario());
				if(cambioIVA){		
					super.setMensajeExitoPersonalizado("IVA ajustado debido a domicilio fiscal del Contrante, favor de terminar la cotizaci\u00f3n.");
					return "changeCotizacion";	
				}
			}catch(Exception ex){
				super.setMensajeError("EL CLIENTE SELECCIONADO NO CUENTA CON INFORMACION DEL DOMICILIO, POR LO CUAL EL SISTEMA TOMARA EL IVA DEL 16%.");
				super.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				return SUCCESS;
			}			
		}
		
		LOG.info("<<<<< Saliendo Seleccionar contratante Action");
		return SUCCESS;
	}
	
	public void prepareMostrarClientesAsociados() {
		negocio = entidadService.findById(Negocio.class, idToNegocio);
	}
	public String mostrarClientesAsociados() {
		return SUCCESS;
	}	

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}
	
	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public boolean isNegocioAsociado() {
		return negocioAsociado;
	}

	public void setNegocioAsociado(boolean negocioAsociado) {
		this.negocioAsociado = negocioAsociado;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}
	
	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}

}