package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.digitalComplementarios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.digitalComplementarios.DocDigitalComplementariosService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class DocDigitalComplementariosAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToCotizacion;
	private List<DocumentoDigitalCotizacionDTO> docDigitalCotizacionList = new ArrayList<DocumentoDigitalCotizacionDTO>(1);
	private DocumentoDigitalCotizacionDTO docDigitalCotDTO;
	private BigDecimal idDocumentoDigitalCotizacion;
	private BigDecimal idSolicitud;
	private String aclaraciones;
	private CotizacionDTO cotizacionDTO;

	private DocDigitalComplementariosService docDigitalComplementariosService;
	private EntidadService entidadService;
	private String tabActiva;
	private Short soloConsulta = 0;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("docDigitalComplementariosServiceEJB")
	public void setDocDigitalComplementariosService(
			DocDigitalComplementariosService docDigitalComplementariosService) {
		this.docDigitalComplementariosService = docDigitalComplementariosService;
	}


	@Override
	public void prepare() throws Exception {
		
	}
	
	public void prepareVerDigitalComplementarios(){
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			idSolicitud = cotizacionDTO.getSolicitudDTO().getIdToSolicitud();
			ServletActionContext.getContext().getSession().put("idSolicitud", idSolicitud);
			aclaraciones = cotizacionDTO.getAclaraciones();
		}
	}
	
	public String verDigitalComplementarios(){
		return SUCCESS;
	}
	
	public void prepareMostrarDocDigitalCotizacion(){
		if(idToCotizacion != null){
			docDigitalCotizacionList = docDigitalComplementariosService.listarDocDigitalesCotizacion(idToCotizacion);
		}
	}
	
	public String mostrarDocDigitalCotizacion(){
		return SUCCESS;
	}
	
	public void prepareEliminarDocDigitalCotizacion(){
		if(idDocumentoDigitalCotizacion != null){
			docDigitalCotDTO = docDigitalComplementariosService.findById(idDocumentoDigitalCotizacion);
		}
		
	}
	
	public String eliminarDocDigitalCotizacion(){
		if(docDigitalCotDTO != null){
			docDigitalComplementariosService.eliminaDocumentoDigitalCotizacionDTO(docDigitalCotDTO);
		}
		return SUCCESS;
	}
	
	public void prepareGuardarAclaraciones(){
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		}
	}
	
	public String guardarAclaraciones(){
		
		cotizacionDTO.setAclaraciones(aclaraciones);
		try{
			entidadService.save(cotizacionDTO);
			super.setMensajeExito();
			setTabActiva("documentosDigitales");
			if(cotizacionDTO.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
				this.setNamespaceOrigen("/suscripcion/cotizacion/auto/complementar");
				this.setActionNameOrigen("iniciarComplementar");
			}else{
				this.setNamespaceOrigen("/suscripcion/cotizacion/auto");
				this.setActionNameOrigen("verDetalleCotizacionContenedor");				
			}
		}catch(RuntimeException e){
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}



	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setDocDigitalCotizacionList(List<DocumentoDigitalCotizacionDTO> docDigitalCotizacionList) {
		this.docDigitalCotizacionList = docDigitalCotizacionList;
	}

	public List<DocumentoDigitalCotizacionDTO> getDocDigitalCotizacionList() {
		return docDigitalCotizacionList;
	}


	public void setDocDigitalCotDTO(DocumentoDigitalCotizacionDTO docDigitalCotDTO) {
		this.docDigitalCotDTO = docDigitalCotDTO;
	}


	public DocumentoDigitalCotizacionDTO getDocDigitalCotDTO() {
		return docDigitalCotDTO;
	}


	public void setIdDocumentoDigitalCotizacion(
			BigDecimal idDocumentoDigitalCotizacion) {
		this.idDocumentoDigitalCotizacion = idDocumentoDigitalCotizacion;
	}


	public BigDecimal getIdDocumentoDigitalCotizacion() {
		return idDocumentoDigitalCotizacion;
	}


	public void setIdSolicitud(BigDecimal idSolicitud) {
		this.idSolicitud = idSolicitud;
	}


	public BigDecimal getIdSolicitud() {
		return idSolicitud;
	}

	public void setAclaraciones(String aclaraciones) {
		this.aclaraciones = aclaraciones;
	}

	public String getAclaraciones() {
		return aclaraciones;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

}