package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.util.List;

import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;
import mx.com.afirme.midas2.validator.group.NewItemChecks;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionModeloAction extends
		ExcepcionSuscripcionAction implements Preparable {

	private static final long serialVersionUID = 4434248582524442211L;
	private Integer modeloInicial;
	private Integer modeloFinal;
	private Boolean checkBox;
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

	public void prepareMostrarModelo() throws Exception {
		excepcionId = super.obtenerExcepcion();
		if (excepcionId != null) {
			List<String> lista = excepcionService.obtenerCondicionValores(
					excepcionId, TipoCondicion.MODELO);
			if (lista != null && !lista.isEmpty()) {
				if(Integer.parseInt(lista.get(0)) < Integer.parseInt(lista.get(1))){
					modeloInicial = Integer.parseInt(lista.get(0));
					modeloFinal = Integer.parseInt(lista.get(1));
				}else{
					modeloInicial = Integer.parseInt(lista.get(1));
					modeloFinal = Integer.parseInt(lista.get(0));	
				}
			}

		}
	}

	public String mostrarModelo() throws Exception {
		return SUCCESS;
	}

	public void validateGuardarModelo() {
		if(checkBox){
			addErrors(modeloInicial, NewItemChecks.class, this, "modeloInicial");
			addErrors(modeloFinal, NewItemChecks.class, this, "modeloFinal");
		}
	}

	/**
	 * Guarda el modelo en la bd
	 * 
	 * @return
	 */
	public String guardarModelo() {
		try {
			excepcionId = super.obtenerExcepcion();
			excepcionService.agregarCondicion(
					excepcionId,
					TipoCondicion.MODELO,
					ExcepcionSuscripcionService.TipoValor.RANGO,
					new String[] {
							(modeloInicial != null ? modeloInicial.toString()
									: null),
							(modeloFinal != null ? modeloFinal.toString()
									: null) });
			super.setMensajeExitoPersonalizado("Condici�n agregada a la excepci�n");
			// Condici�n Agregada con exito
			super.setAgregada(true);
		} catch (Exception ex) {
			super.setMensajeError("No se puedo agregar la condici�n a la excepci�n en este momento");
		}
		return SUCCESS;
	}

	public Integer getModeloInicial() {
		return modeloInicial;
	}

	public void setModeloInicial(Integer modeloIncial) {
			this.modeloInicial = modeloIncial;	
	}
	public Integer getModeloFinal() {
		return modeloFinal;
	}

	public void setModeloFinal(Integer modeloFinal) {
			this.modeloFinal = modeloFinal;
	}

	/**
	 * @param checkBox el checkBox a establecer
	 */
	public void setCheckBox(Boolean checkBox) {
		this.checkBox = checkBox;
	}

	/**
	 * @return el checkBox
	 */
	public Boolean getCheckBox() {
		return checkBox;
	}

}