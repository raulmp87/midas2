/**
 * 
 */
package mx.com.afirme.midas2.action.suscripcion.solicitud;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.excel.DatosSolicitudEnTramiteExcel;
import mx.com.afirme.midas.sistema.excel.Propiedad;
import mx.com.afirme.midas.sistema.excel.Validacion;
import mx.com.afirme.midas.sistema.excel.Validacion.TipoValidacion;
import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudDataEnTramiteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


/**
 * @author jreyes
 *
 */
@Component
@Scope("prototype")
public class SolicitudDataEnTramiteAction extends BaseAction  implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5709373463049341938L;
	private SolicitudDataEnTramiteService solicitudDataEnTramiteService;

	@Autowired
	@Qualifier("solicitudDataEnTramiteServiceEJB")
	public void setSolicitudDataEnTramiteService(
			SolicitudDataEnTramiteService solicitudDataEnTramiteService) {
		this.solicitudDataEnTramiteService = solicitudDataEnTramiteService;
	}

	public SolicitudDataEnTramiteService getSolicitudDataEnTramiteService() {
		return solicitudDataEnTramiteService;
	}

	private BigDecimal idToSolicitud;
	
	private BigDecimal idToControlArchivo;
	
	private String claveNegocio;
	
	private InputStream genericInputStream;
	private String     fileName;
	private String      contentType;	
	
	public InputStream getGenericInputStream() {
		return genericInputStream;
	}

	public void setGenericInputStream(InputStream genericInputStream) {
		this.genericInputStream = genericInputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	public String carga(){
		
		String mensaje = "";
		if(idToControlArchivo!=null) {
			try{

			System.out.print("Entes de control archivo");
				ControlArchivoDTO controlArchivoDTO;
				controlArchivoDTO = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
				
				DatosSolicitudEnTramiteExcel<SolicitudDataEnTramiteDTO> excel = new DatosSolicitudEnTramiteExcel<SolicitudDataEnTramiteDTO>(SolicitudDataEnTramiteDTO.class);
				excel.setIdToSolicitud(idToSolicitud);
				excel.setArchivo(controlArchivoDTO);
			
					if(excel.isValid()) {
						
						Propiedad propiedad = null;
						
						propiedad = new Propiedad(0, "descripcion");
						propiedad.setDescripcion("DESCRIPCION");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(1, "modelo");
						propiedad.setDescripcion("MODELO");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(2, "serie");
						propiedad.setDescripcion("SERIE");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(3, "equipoEspecial");
						propiedad.setDescripcion("EQUIPO_ESPECIAL");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						propiedad.addValidacion(new Validacion(TipoValidacion.NO_REQUERIDO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(4, "adaptacion");
						propiedad.setDescripcion("ADAPTACION");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						propiedad.addValidacion(new Validacion(TipoValidacion.NO_REQUERIDO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(5, "observaciones");
						propiedad.setDescripcion("OBSERVACIONES");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						propiedad.addValidacion(new Validacion(TipoValidacion.NO_REQUERIDO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(6, "fechaInicioVigencia", 6);
						propiedad.setDescripcion("FECHAINICIOVIGENCIA");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						excel.addPropiedad(propiedad);
						
						propiedad = new Propiedad(7, "fechaFinVigencia", 6);
						propiedad.setDescripcion("FECHAFINVIGENCIA");
						propiedad.addValidacion(new Validacion(TipoValidacion.ALFANUMERICO));
						excel.addPropiedad(propiedad);
					
						excel.setExcludeEmptyRow(true); //Ignora filas completamente vacias
						List<SolicitudDataEnTramiteDTO> rowsValidos = excel.getRegistrosValidos();
						List<SolicitudDataEnTramiteDTO> rowsInvalidos = excel.getRegistrosInvalidos();
						
						if(rowsInvalidos.isEmpty() || rowsInvalidos.size() == 0) {
							
							solicitudDataEnTramiteService.save(idToSolicitud, rowsValidos);
							
							mensaje += "Se han cargado "+rowsValidos.size()+" n\u00FAmeros de serie para la Solicitud " + idToSolicitud;
							this.setMensajeExitoPersonalizado(mensaje);	
							
						} else {
							mensaje += "Se encontraron los siguientes errores:";
							
							StringBuilder msg = new StringBuilder(mensaje);
							msg.append("\n").append("<br>");
							for (String str : excel.getRegistrosInvalidosMensaje()) {
								msg.append("<br>").append(str);
							}
							this.setTipoMensaje(BaseAction.TIPO_MENSAJE_ERROR);
							this.setMensaje(msg.toString());
						}
							
						
					} else {
						mensaje +=  "El archivo no es v\u00e1lido.";
						this.setMensajeError(mensaje);
					}
				
			
			} catch (SystemException e) {
				mensaje += "Ocurri\u00F3 un error al cargar el archivo, intente nuevamente.";
				this.setMensajeError(mensaje);
			} catch (FileNotFoundException e) {
				mensaje += "Ocurri\u00F3 un error al cargar el archivo, Error en el Archivo.";
				this.setMensajeError(mensaje);
			} catch (IOException e) {
				mensaje += "Ocurri\u00F3 un error al cargar el archivo, Error al leer el archivo.";
				this.setMensajeError(mensaje);
			} 
		}
		this.setClaveNegocio("A");
		return SUCCESS;
	}

 	public String descargarExcelCartaCobertura(){
		
			try {
				genericInputStream = solicitudDataEnTramiteService.descargarExcelCartaCobertura(idToSolicitud);
				contentType = "application/vnd.ms-excel";
				fileName = "Datos_de_CartaCobertura_Solicitud"+idToSolicitud+".xls";
				
			} catch (IOException e) {
				e.printStackTrace();
				setMensaje("Error al obtener excel favor de intentarlo mas tarde");
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return ERROR;
			} 
			
			return SUCCESS;
 	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Apéndice de método generado automáticamente
		
	}
}