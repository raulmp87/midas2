package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.reasignar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ReasignarCotizacionPolizaAction extends BaseAction  implements Preparable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	private CotizacionDTO cotizacionDTO;
	private SolicitudDTO solicitud;
	private List<Usuario> usuariosSuscriptores = new ArrayList<Usuario>();
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	private CotizacionService cotizacionService;
	private String nombreUsuarioAsignado;

	



	@Override
	public void prepare() throws Exception {
		if(getId() != null){
			if(cotizacionDTO==null)cotizacionDTO = new CotizacionDTO();
			cotizacionDTO = cotizacionFacadeRemote.findById(getId());
			solicitud = cotizacionDTO.getSolicitudDTO();
			if (solicitud != null && solicitud.getCodigoUsuarioAsignacion() != null) {
				Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(cotizacionDTO.getSolicitudDTO().getCodigoUsuarioAsignacion());
				if (usuario != null) {
					nombreUsuarioAsignado = usuario.getNombreCompleto();
				} else {
					nombreUsuarioAsignado = "";
				}
			}
		}
	}
	
	public String mostrarVentana() {	
		//FIXME: llenado temporal de la lista de usuarios
		try{
			if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Asignar_Suscriptor")) {
				usuariosSuscriptores = usuarioService
						.buscarUsuariosPorNombreRol(sistemaContext
								.getRolSuscriptorAutos());
				if(usuariosSuscriptores == null)usuariosSuscriptores = new ArrayList<Usuario>();
			}else{
				usuariosSuscriptores = new ArrayList<Usuario>();
			}
		}catch(Exception e){
			usuariosSuscriptores = new ArrayList<Usuario>();
			e.printStackTrace();
		}
			/*
		usuariosSuscriptores = new ArrayList<Usuario>();
		usuariosSuscriptores.add(usuarioService.getUsuarioActual());
		usuariosSuscriptores = usuarioService
		.buscarUsuariosPorNombreRol(sistemaContext
				.getRolSuscriptorAutos());	
		if(usuariosSuscriptores == null)usuariosSuscriptores = new ArrayList<Usuario>();
		*/
		return SUCCESS;
	}
	
	
	public String reasignar(){
		  try{		 
			if(solicitud != null){
				cotizacionDTO.setSolicitudDTO(solicitud);
			}
			cotizacionService.reasignarSuscriptor(cotizacionDTO);
			setMensaje(getText("midas.cotizacion.asignar.mensaje.exito", new String[]{cotizacionDTO.getNumeroCotizacion()}));
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		  }catch(Exception ex){
			  super.setMensajeError(MENSAJE_ERROR_GENERAL);
		  }
		  return SUCCESS;
		  
		}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}


	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public List<Usuario> getUsuariosSuscriptores() {
		return usuariosSuscriptores;
	}

	public void setUsuariosSuscriptores(List<Usuario> usuariosSuscriptores) {
		this.usuariosSuscriptores = usuariosSuscriptores;
	}

	@Autowired
	@Qualifier("cotizacionFacadeRemoteEJB")
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(
			CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	public void setNombreUsuarioAsignado(String nombreUsuarioAsignado) {
		this.nombreUsuarioAsignado = nombreUsuarioAsignado;
	}

	public String getNombreUsuarioAsignado() {
		return nombreUsuarioAsignado;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}	
}
