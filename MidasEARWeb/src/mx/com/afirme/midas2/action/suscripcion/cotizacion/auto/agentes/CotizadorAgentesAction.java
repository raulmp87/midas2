package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.agentes;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.agente.AgenteDN;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.DocumentoDTO;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.domain.cobranza.cliente.CotizacionCobranza;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes.VehiculoView;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.canalventa.NegocioCanalVenta;
import mx.com.afirme.midas2.domain.negocio.ligaagente.ConfiguracionLigaAgente;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CaracteristicasVehiculoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.cliente.CotizacionCobranzaService;
import mx.com.afirme.midas2.service.folioReexpedibleService.FolioReexpedibleService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.canalventa.CanalVentaNegocioService;
import mx.com.afirme.midas2.service.negocio.ligaagente.ConfiguracionLigaAgenteService;
import mx.com.afirme.midas2.service.negocio.producto.mediopago.NegocioMedioPagoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo.CotizacionExtService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas.cotizacion.extrainfo.CotizacionExtDTO;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;
/*** COTIZADOR DE AUTOS INDIVIDUAL **/
@Component
@Scope("prototype")
public class CotizadorAgentesAction extends BaseAction  implements Preparable{

	private static final long serialVersionUID = -1L;

	/** Id de medio de pago agente o efectivo */
	private static final int MEDIOPAGO_AGENTE = 15;
	private static final String MEDIOPAGO_EFECTIVO = "EFECTIVO";
	private static final int VALORMAXIMOUNIDADVIGENCIA = 365;
	private static final int DATOS_GENERALES = 1;
	private static final int DATOS_PAQUETES = 2;
	private static final int DATOS_COMPLEMENTARIOS = 3;
	public  static final int DATOS_COBRANZA_EMISION = 4;
	private static final String LIGA_AGENTE = "LIGA_AGENTE";
	public  static final Logger LOG = Logger.getLogger(CotizadorAgentesAction.class);
	private EntrevistaDTO entrevista;
	private ClientesApiService  clienteRest;
	private CotizacionDTO cotizacion;
	private IncisoCotizacionDTO incisoCotizacion;
	private ResumenCostosDTO resumenCostosDTO;
	private Agente agente;
	private Integer saveAsegurado;
	private BigDecimal idToCotizacion;
	private CotizadorAgentesService cotizadorAgentesService;
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	private EntidadService entidadService;
    private CotizacionService cotizacionService;
    private CotizacionExtService cotizacionExtService;
	private ListadoService listadoService;
	private IncisoService incisoService;
	private CalculoService calculoService;
	private EmisionService emisionService;
	private MensajeDTO mensajeDTO;
	private Boolean cuentaConAgente;
	private Boolean isAgente;
	private String estiloSeleccionado;
	private int forma = 1;
	private String nameUser;
	private BigDecimal numeroInciso;
	private Long idNegocioPaqueteBase;
	private Long idNegocioBase;
	private BigDecimal idProductoBase;
	private BigDecimal idTipoPolizaBase;
	private BigDecimal idNegocioSeccionBase;
	private BigDecimal idTipoUsoBase;
	private BigDecimal idTipoPoliza ;
	private Long idTipoProducto;
	private String pdfDownload;
	private boolean updateSolicitud;
	private IncisoAutoCot incisoAutoCot;
	private List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
	private List<CatalogoDocumentoFortimax> listaDocumentosFortimax = new ArrayList<CatalogoDocumentoFortimax>(1);
	private Boolean newCotizadorAgente;	
	private List<Negocio> negocioAgenteList = new ArrayList<Negocio>(1);
	private Map<String, String> negocioProductoList = new HashMap<String, String>(1);
	private Map<String, String> negocioTipoPolizaList = new HashMap<String, String>(1);
	private List<VehiculoView> listaFlotilla;
	private VehiculoView vehiculo;
	private BigDecimal idMoneda;
	private BigDecimal idNegocioSeccion;
	private BigDecimal idMarcaVehiculo;
	private String idEstiloVehiculo;
	private Short idModeloVehiculo;
	private Map<String, String> estadoMap = new HashMap<String, String>(1);
	private Map<String, String> municipioMap = new HashMap<String, String>(1);
	private Map<String, String>  listarNegocios = new HashMap<String, String>(1);
	private Map<String, String> marcaMap = new HashMap<String, String>(1);
	private Map<String, String> modeloMap = new HashMap<String, String>(1);
	private Map<String, String> tipoUsoMap = new HashMap<String, String>(1);
	private Map<String, String> formasdePagoList = new HashMap<String, String>(1);
	private Map<Long, String> paqueteMap;
	private Map<Long, Double> derechoMap;
	private List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList;
	private String modificadoresDescripcion;
	private List<CoberturaCotizacionDTO> coberturaCotizacionList;
	private String descripcionBusquedaAgente;
	private String descripcionBusquedaEstilos;
	private List<AgenteView> agentesList;
	private List<File> files = new ArrayList<File>(1);
    private List<String> filesFileName = new ArrayList<String>(1);
    private List<String> filesContentType = new ArrayList<String>(1);
    private BigDecimal idToPoliza;
	private String numeroPoliza;
	private Boolean isJson;
	private boolean guardaDatoConductorInciso = true;
	private boolean guardaObservacionesInciso = false;
	private boolean requiereDatosAdicionales;
	private String bienasegurado="";
	private Map<Object, String> diasSalario = new LinkedHashMap<Object, String>();
	private Short numAsientos;
	private BigDecimal idAgrupadorPasajeros;
	private Date currentDate;
	private Long idPromotoria;
	private boolean compatilbeExplorador;
	private FronterizosService fronterizosService;
	private HashMap<String, Map<String, String>> infoVehicular;
	private Boolean guardarEntrevista;
	private Boolean guardarDocumentacion;
	private String idCotizacion;
	private boolean correoObligatorio;
	private boolean folioReexpedible;
	private String correo;
	private String nombreContratante;
	private Long idNeg;
	private Map<Long, String> conductosDeCobro; 
	private Map<Boolean,String> mapDatosIguales = new HashMap<Boolean, String>(1);
	private Map<String, String> medioPagos = new HashMap<String, String>(1);
	private Integer nextTap = 1;
	private Map<String, String> mapGenerico = new HashMap<String, String>(1);
	private boolean datosIguales = false;
	private boolean statusExcepcion = false;//JFGG
	private String mensajeExcepcion = null;//JFGG
	private long idExcepcion = 0;//JFGG
	private String functionPrintPoliza;
	
	//Almacenar listado de vigencias asociadas al negocio
	private Map<Integer, String> tiposVigenciaList = new HashMap<Integer, String>(1);
	private Integer vigenciaDefault = null;
	private BigDecimal idTipoVigenciaBase;
	
	//MLA
	private Long configuracionId;
	private ConfiguracionLigaAgente configuracion;
	private Boolean finalizaCotizacion;	
	//Codigo Liga Agente Token Identificador
	private String clati; 
	private boolean inicioVigenciaValido;
	private Long configuracionIdResumen;
	private Long configuracionIdEnCoberturas;
	//CODIGO POSTAL
	private String codigoPostal;
	private String nomPaquete;
	private String nomFormaPago;
	List<DocumentoDTO>  documentacion;
	
	//for AS360 project
	private Long    conductoCobroIdCot;
	private String  userTieneRolEjecutivo;
	private Map <String, String> listaDeEjecutivosMap;
	private String  ejecutivoColocado;
	private PolizaPagoService pagoService;
	
	private List<MedioPagoDTO> medioPagoDTOs;
	@Autowired
	@Qualifier("negocioMedioPagoServiceEJB")
	private NegocioMedioPagoService negocioMedioPagoService;
	
	private Boolean forzarArt140 = Boolean.FALSE;

	// Mostrar mensaje de inspeccion de acuerdo al negocio
	@Autowired
	@Qualifier("canalVentaNegocioServiceEJB")
	private CanalVentaNegocioService canalVentaNegocioService;
	private Boolean mostrarMensaje = Boolean.FALSE;
	public static final String CLIENTEUNICO = "idClienteUnico";
	public static final String MENSAJEINSPECCION = "verMensajeInspeccion";
	
	private Integer usuarioExterno = 2;
	
	public PolizaPagoService getPagoService() {
		return pagoService;
	}
	@Autowired
	@Qualifier("polizaPagoServiceEJB")
	public void setPagoService(PolizaPagoService pagoService) {
		this.pagoService = pagoService;
	}
	
	public Long getConductoCobroIdCot() {
		return conductoCobroIdCot;
	}
	public void setConductoCobroIdCot(Long conductoCobroIdCot) {
		this.conductoCobroIdCot = conductoCobroIdCot;
	}
	public String getUserTieneRolEjecutivo() {
		return userTieneRolEjecutivo;
	}
	public void setUserTieneRolEjecutivo(String userTieneRolEjecutivo) {
		this.userTieneRolEjecutivo = userTieneRolEjecutivo;
	}
	public Map<String, String> getListaDeEjecutivosMap() {
		return listaDeEjecutivosMap;
	}
	public void setListaDeEjecutivosMap(Map<String, String> listaDeEjecutivosMap) {
		this.listaDeEjecutivosMap = listaDeEjecutivosMap;
	}
	public String getEjecutivoColocado() {
		return ejecutivoColocado;
	}
	public void setEjecutivoColocado(String ejecutivoColocado) {
		this.ejecutivoColocado = ejecutivoColocado;
	}
	//for AS360 project end 

	@Autowired
	private NegocioService negocioService;
	
	@Autowired
	private FolioReexpedibleService folioService;
	
	@Autowired
	private ConfiguracionLigaAgenteService configuracionLigaAgenteService;
	
	@Autowired
	@Qualifier("CotizacionCobranzaEJB")
	private CotizacionCobranzaService cotizacionCobranzaService;
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
	@Override
	public void prepare() {
		if(idToCotizacion != null){///
			cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);// no hay cotizacion
			correoObligatorio = cotizacion.getSolicitudDTO().getNegocio().isCorreoObligatorio();
		}
		aplicarArt140(cotizacion);
	}
	
	public String ventanaAgentes(){
		return SUCCESS;
	}
	
	public String buscarAgente(){
		agentesList = cotizadorAgentesService.getAgentesPorDescripcion(descripcionBusquedaAgente);
		return SUCCESS;
	}
	
	public String listarAgentesPorPromotoria(){
		agentesList = cotizadorAgentesService.getAgentesPorPromotoria(idPromotoria);
		return SUCCESS;
	}
	
	public String buscarEstilo(){
		mapGenerico = cotizadorAgentesService.getEstiloPorMarcaNegocioyDescripcion(idMarcaVehiculo,
				idModeloVehiculo, idNegocioSeccion, idMoneda, null, descripcionBusquedaEstilos, idAgrupadorPasajeros);
		return SUCCESS;
	}
	
	//MLA Action desde el cual se accede al cotizador por medio de las ligas de agentes
	public String mostrarCotizadorLigaAgente(){
		currentDate = new Date();
		if(clati == null
				|| clati.isEmpty()){
			setMensajeError("Liga Invalida");
			return INPUT;
		}else{
			List<ConfiguracionLigaAgente> configuraciones = entidadService.findByProperty(ConfiguracionLigaAgente.class, "token", clati);
			if(configuraciones == null
					|| configuraciones.isEmpty()
					|| configuraciones.get(0).getEstatus().equals(ConfiguracionLigaAgente.Estatus.INACTIVO.toString())
					|| configuraciones.get(0).getEstatus().equals(ConfiguracionLigaAgente.Estatus.SUSPENDIDO.toString())){
				setMensajeError("Liga Invalida");
				return INPUT;
			}else{
				configuracion = configuraciones.get(0);
				configuracionId = configuracion.getId();
			}
		}
		return SUCCESS;
	}//MLA
	
	public void prepareMostrarContenedor(){
		if(idToCotizacion!=null){
			userTieneRolEjecutivo = pagoService.validarUsuarioActualSiEsEjecutivoOrSucursal();
			if( forma == DATOS_COBRANZA_EMISION && userTieneRolEjecutivo != null ){
				if(!userTieneRolEjecutivo.equals("NINGUNO") ){
					cargarYMostrarListaEjecutivos();
				}
				
			}
			cotizacion = cotizacionFacadeRemote.findById(idToCotizacion);
			LOG.info("::::  [ INFO ]  :::: EL ID DE NEGOCIO ES: ----------- >  " + cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			idNeg = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
			
			if(numeroInciso!=null){
				IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
				incisoCotizacionId.setIdToCotizacion(idToCotizacion);
				incisoCotizacionId.setNumeroInciso(numeroInciso);
				incisoCotizacion =  entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionId);
				incisoCotizacion.setCotizacionDTO(cotizacion);
			}
		}
		if(cotizacion == null){
			cotizacion = new CotizacionDTO();
			cotizacion.setIdToCotizacion(null);
			cotizacion.setClaveEstatus(null);
		}
		
		if(cotizacion.getIdMoneda()==null){
			cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
		}
		
		folioReexpedible = folioService.isFolioVenta(cotizacion.getFolio());
		//MLA carga la informacion del agente en la nueva cotizacion
		if(configuracionId != null){
			configuracion = entidadService.findById(ConfiguracionLigaAgente.class, configuracionId);
			if(configuracion != null &&
					cotizacion != null &&
					cotizacion.getSolicitudDTO() == null){
				cotizacion.setSolicitudDTO(new SolicitudDTO());
			}
			cotizacion.getSolicitudDTO().setCodigoAgente(BigDecimal.valueOf(configuracion.getAgente().getId()));
		}//MLA
		
		aplicarArt140(cotizacion);// ejecutar al dar clic sobre el boton de avanzar
	}
	
	public String mostrarContenedor(){
		try {
			
			if(!compatilbeExplorador){
				return "validaExplorador";
			}
			
			if(nameUser==null){
				Usuario usuario = usuarioService.getUsuarioActual();
				if(configuracionId != null){
					nameUser = usuario.getNombreCompleto();
				}else{
					nameUser = usuario.getNombreUsuario().concat(" ").concat(usuario.getNombreCompleto());
				}
			}
			init();
			
			//MLA Si se accedio al cotizador desde una liga de agente y en su configuracion no cuenta con la opcion emitir, se manda a la pantalla de finalizacion.
			// si la liga tiene configurada la opcion emitir, entonces el usuario decide si finaliza la cotizacion o avanza al paso 3.
			if(configuracionId != null
					&& forma == DATOS_COMPLEMENTARIOS
					&& ( (configuracion.getEmitir() == null || !configuracion.getEmitir()) 
							|| (finalizaCotizacion != null && finalizaCotizacion)
						)
			   ){
				
				return "successLigaAgente";
			}//MLA
		} catch(Exception e) {
			LOG.error(this.getClass().getName() + ".prepareMostrarContenedor.Excepcion: " + e, e);
		}
		return SUCCESS;
	}
	
	public void init(){
		cotizadorAgentesService.setNombreAgenteCompleto(cotizacion);		
		getUsuarioAgente();
		currentDate = new Date();

		Usuario usuario = usuarioService.getUsuarioActual();
		if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
			usuarioExterno = usuario.getTipoUsuario();
		}

		switch (forma) {
		case DATOS_GENERALES:
			BigDecimal codigoAgente = null;
			if(cotizacion.getSolicitudDTO()!= null){
				codigoAgente = cotizacion.getSolicitudDTO().getCodigoAgente();
				cuentaConAgente = codigoAgente!=null;
			}
			
			if(cuentaConAgente && codigoAgente!=null){
				agente = cotizadorAgentesService.getAgente(codigoAgente.longValue());
			}
			if(cotizacion.getIdToCotizacion()==null){	
				//Valore predefinidos
				setValoresPredefinidos(codigoAgente);

				BigDecimal idTipoUso = new BigDecimal(cotizadorAgentesService.getValorParameter(
						ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
						ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_TIPOUSO_BASE));
				BigDecimal idNegocioSeccionDefault = new BigDecimal(cotizadorAgentesService.getValorParameter(
						ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
						ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIOSECCION_BASE));

				idNegocioSeccion = (idNegocioSeccion==null?idNegocioSeccionDefault:idNegocioSeccion);
				
				if(incisoCotizacion == null){
					incisoCotizacion = new IncisoCotizacionDTO();					
				}
				
				IncisoAutoCot incisoAutoCotDTO = incisoCotizacion.getIncisoAutoCot();
				if(incisoAutoCotDTO==null){
					incisoAutoCotDTO = new IncisoAutoCot();
					incisoAutoCotDTO.setNegocioSeccionId(idNegocioSeccion.longValue());
					incisoAutoCotDTO.setTipoUsoId(idTipoUso.longValue());
				}
				
				incisoCotizacion.setIncisoAutoCot(incisoAutoCotDTO);
				negocioProductoList = new HashMap<String, String>(1);
				negocioTipoPolizaList = new HashMap<String, String>(1) ;
				estadoMap = new HashMap<String, String>(1);
				municipioMap = new HashMap<String, String>(1);
				if(cotizacion.getSolicitudDTO().getNegocio() != null){
					tiposVigenciaList = listadoService.obtenerTiposVigenciaNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
					vigenciaDefault = listadoService.obtenerVigenciaDefault(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
				}
			}else{
				setTipoPoliza();
				if(incisoCotizacion != null && incisoCotizacion.getIncisoAutoCot().getEstiloId() !=null){
					EstiloVehiculoDTO datosEstilo = cotizadorAgentesService.getEstiloVehiculo(
							incisoCotizacion.getIncisoAutoCot().getEstiloId());
					if(datosEstilo!=null){
						estiloSeleccionado = datosEstilo.getDescription();
						idEstiloVehiculo = datosEstilo.getId().getClaveEstilo().concat(" - ").concat(estiloSeleccionado);
						numAsientos = cotizadorAgentesService.obtenerNumeroAsientos(incisoCotizacion);
					}
				}
				
				negocioAgenteList = cotizadorAgentesService.getNegociosPorAgente(codigoAgente.intValue(), Negocio.CLAVE_NEGOCIO_AUTOS);
				//MLA Si entra al cotizador desde la liga de agente, carga el unicamente el negocio de la configuracion de ligas de agente
				if(configuracion == null){
					if(!negocioAgenteList.contains(cotizacion.getSolicitudDTO().getNegocio())){
						Negocio neg = negocioService.findById(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
						negocioAgenteList.add(neg);
					}
				}else{
					negocioAgenteList = new ArrayList<Negocio>();
					negocioAgenteList.add(configuracion.getNegocio());
				}
				//MLA
				Map<String, Map<String, String>> mapList = cotizadorAgentesService.getListasVehiculo(cotizacion, 
						incisoCotizacion.getIncisoAutoCot().getEstiloId(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo(),
						new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()), incisoCotizacion.getIncisoAutoCot().getEstadoId().toString());
				cargarListasDatosPoliza(mapList);
				
				tiposVigenciaList = listadoService.obtenerTiposVigenciaNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
				vigenciaDefault = listadoService.obtenerVigenciaDefault(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			}
			if(this.getMensaje()!=null){
				this.setMensaje(this.getMensaje());
				this.setTipoMensaje(this.getTipoMensaje());	
			}
    		break;
		case DATOS_PAQUETES:
			setTipoPoliza();
			bienasegurado = cotizadorAgentesService.getBienAsegurado(incisoCotizacion);
			idNegocioSeccion = (idNegocioSeccion!=null?idNegocioSeccion:
				new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()));
			resumenCostosDTO = new ResumenCostosDTO();			
			resumenCostosDTO.setValorIvaSeleccionado(resumenCostosDTO.getIvaList().get(0));
			setListDatosPaquetes();
			if(this.getMensaje()!=null){
				this.setMensaje(this.getMensaje());
				this.setTipoMensaje(this.getTipoMensaje());
			}else{
				this.setMensajeExito();
			}
    		break;

		case DATOS_COMPLEMENTARIOS:
			if(incisoCotizacion!=null){
				guardaDatoConductorInciso = incisoService.infoConductorEsRequerido(incisoCotizacion);
				try{
					guardaObservacionesInciso = incisoService.observacionesEsRequerido(incisoCotizacion);
					
					if(incisoCotizacion!=null && incisoCotizacion.getCotizacionDTO()!=null) {
						getConfigCanalVenta(incisoCotizacion.getCotizacionDTO());
					}
				}catch(Exception e){
					LOG.error("Error al obtener el booleano ObservacionesInciso", e);
				}
			}			
			this.setMensajeExito();
        	break;

		case DATOS_COBRANZA_EMISION:
			try{
				conductosDeCobro = listadoService.getConductosCobro(
						cotizacion.getIdToPersonaContratante().longValue(),
						cotizacion.getIdMedioPago().intValue());
			} catch(RuntimeException exception) {
				conductosDeCobro = new HashMap<Long, String>(1);
			}
			
			CotizacionCobranza cotizacionCobranza = cotizacionCobranzaService.findFirstByProperty("cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
			if (cotizacionCobranza != null){
				datosIguales = cotizacionCobranza.getClienteEsTitular();
			}
			
			mapDatosIguales.put(true, "SI");
			mapDatosIguales.put(false, "NO");
			
			medioPagos.putAll(listadoService.getMediosPago(cotizacion.getSolicitudDTO().getNegocio()));
			if (this.getMensaje()!=null){
				this.setMensaje(this.getMensaje());
				this.setTipoMensaje(this.getTipoMensaje());
			}
			
			List<PolizaDTO> polizas = entidadService.findByProperty(PolizaDTO.class,
						"cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
			for(PolizaDTO poliza : polizas){
				functionPrintPoliza = emisionService.setFunctionPrintPoliza(poliza.getIdToPoliza());
				break;
			}
			
			
			if(incisoCotizacion != null){
				bienasegurado = cotizadorAgentesService.getBienAsegurado(incisoCotizacion);		
				idNegocioSeccion = (idNegocioSeccion!=null?idNegocioSeccion:
					new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()));
				
				
				if(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId()!=null){
					NegocioPaqueteSeccion negPaqSeccion = entidadService.findById(NegocioPaqueteSeccion.class, 
							incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
					if(negPaqSeccion != null){
						nomPaquete = negPaqSeccion.getPaquete().getDescripcion();	
					}
				}
				
				if(cotizacion.getIdFormaPago() != null){
					NegocioFormaPago negFormPago = entidadService.findById(NegocioFormaPago.class, 
							cotizacion.getIdFormaPago());
					if(negFormPago != null){
						nomFormaPago = negFormPago.getFormaPagoDTO().getDescripcion();
					}					
				}				

			}if(entrevista!=null){
				
				if (guardarEntrevista!=null&&guardarEntrevista){
					
					clienteRest.createInterview(cotizacion.getIdToPersonaContratante().longValue(), entrevista);
					
				}
			}
			if (guardarDocumentacion!=null&&guardarDocumentacion){
				
				List<DocumentoDTO> listToBorrar = new ArrayList<DocumentoDTO>();
				for (int i=0;i<documentacion.size();i++){
						DocumentoDTO documento =documentacion.get(i);
						if(documento.getSeleccionado()==null||!documento.getSeleccionado()){
							listToBorrar.add(documento);
						}
					}
				  documentacion.removeAll(listToBorrar);
				  
				  clienteRest.saveDocument(cotizacion.getIdToPersonaContratante().longValue(), documentacion);
				
			}
			List<NegocioProducto> negocioProductos = entidadService.findByProperty(NegocioProducto.class, "negocio", cotizacion.getSolicitudDTO().getNegocio());
			for(NegocioProducto negocioProducto : negocioProductos){
				if (cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto().equals(negocioProducto.getProductoDTO().getIdToProducto())){
					List<NegocioMedioPago> mediosAsociados = this.negocioMedioPagoService.getRelationLists(negocioProducto).getAsociadas();
					setMedioPagoDTOs(mediosAsociados);
				}
			}
		}
		nextTap = forma>=nextTap?forma:nextTap;
		aplicarArt140(cotizacion);
	}

	public String mostrarDatosVehiculo(){
		Map<String, Map<String, String>> mapList = cotizadorAgentesService.getListasVehiculo(cotizacion, 
				incisoCotizacion.getIncisoAutoCot().getEstiloId(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo(),
				null, null);
		cargarListasDatosPoliza(mapList);
		return SUCCESS;
	}
	
	public String mostrarOtrasCaract(){
		try{
			caracteristicasVehiculoList = cotizadorAgentesService.mostrarOtrasCaracteristicas(estiloSeleccionado, modificadoresDescripcion);
		}catch(Exception e){
			LOG.error("Error al obtener las caracteristicas del Vehiculo", e);
			caracteristicasVehiculoList = new ArrayList<CaracteristicasVehiculoDTO>(1);
		}
		return SUCCESS;
	}
	
	public String definirOtrasCaract(){
		incisoAutoCot = cotizadorAgentesService.definirOtrasCaracteristicas(
				caracteristicasVehiculoList, estiloSeleccionado);
		return SUCCESS;
	}
	
	public CotizacionDTO complementaCotizacion(){
		CotizacionDTO cotizacionDto = new CotizacionDTO();
		Long idNegPaqSeccion;
		idNegocioPaqueteBase = cotizadorAgentesService.cambioNegocio(idNegocioSeccion);
		if(idToCotizacion!=null){
			cotizacionDto = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			
			switch (forma){
			case DATOS_PAQUETES:
				cotizacionDto.setPorcentajebonifcomision(cotizacion.getPorcentajebonifcomision());
				cotizacionDto.setIdFormaPago(cotizacion.getIdFormaPago());
				cotizacionDto.getNegocioDerechoPoliza().setIdToNegDerechoPoliza(
						cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza());
				cotizacionDto.setPorcentajeIva(cotizacion.getPorcentajeIva());
				cotizacionDto.setPorcentajePagoFraccionado(cotizacion.getPorcentajePagoFraccionado());
				idNegPaqSeccion = incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId();
				Double descPorEstado = incisoCotizacion.getIncisoAutoCot().getPctDescuentoEstado();
				incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
				incisoCotizacion.getIncisoAutoCot().setNegocioPaqueteId(idNegPaqSeccion);
				incisoCotizacion.getIncisoAutoCot().setPctDescuentoEstado(descPorEstado);
				break;
			case DATOS_GENERALES:
				idTipoPoliza = cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza();
				cotizacionDto.setNombreContratante(cotizacion.getNombreContratante());
				cotizacionDto.setFechaInicioVigencia(cotizacion.getFechaInicioVigencia());
				cotizacionDto.setFechaFinVigencia(cotizacion.getFechaFinVigencia());
				if(!CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO.equals(cotizacionDto.getTipoCotizacion())){
					incisoCotizacion.getIncisoAutoCot().setNegocioPaqueteId(idNegocioPaqueteBase);
				}
				incisoCotizacion = cotizadorAgentesService.saveDatosVehiculo(vehiculo, incisoCotizacion, true);
				LOG.info("--> cotizacion.folio:"+cotizacion.getFolio());
				if (cotizacion.getFolio() != null && !cotizacion.getFolio().equals("")){
					cotizacionDto.setFolio(cotizacion.getFolio());
				}
				break;
			}
		}else{
			setDatosCotizacionPaquete();
			incisoCotizacion = cotizadorAgentesService.saveDatosVehiculo(vehiculo, incisoCotizacion, false);
		}
		return cotizacionDto;
	}

	public String saveCotizacion() throws SystemException {
		try{
			boolean next = false;
			CotizacionDTO cotizacionDto = complementaCotizacion();
			
			BigDecimal negSeccionId = new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId());
			
			Long codigoAgente = (Long) cotizacion.getSolicitudDTO().getCodigoAgente().longValue();
			Agente agenteActual = entidadService.findById(Agente.class, codigoAgente);
			ValorCatalogoAgentes tipoCedula = agenteActual.getTipoCedula();
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, negSeccionId);
			SeccionDTO seccion = negocioSeccion.getSeccionDTO();
			boolean esCedulaValida = AgenteDN.getInstancia().validarCedulaAgente(tipoCedula.getId(), AgenteDN.PROPIEDAD_SECCION, seccion.getIdToSeccion());
			if(!esCedulaValida) {
				String mensajeError = String.format("El agente de Tipo de Cedula %s No cuenta con permisos de venta para la linea de negocio %s seleccionada", tipoCedula.getClave(), seccion.getDescripcion());
				this.setMensaje(mensajeError);
				this.setTipoMensaje(TIPO_MENSAJE_ERROR);
				init();
				return INPUT;
			}
			
			if (incisoCotizacion.getIncisoAutoCot().getCodigoPostal()==null || incisoCotizacion.getIncisoAutoCot().getCodigoPostal().toString().trim()==""){
				String mensaje = String.format("El C\u00F3digo postal es obligatorio");
		    	setMensajeError(mensaje);
				init();
		    	return INPUT;	
			}
		
			if(validaCotizacion(cotizacion)){
				if(idToCotizacion==null){
					cotizacion = cotizadorAgentesService.crearCotizacion(cotizacion, numeroInciso);
					next = true;//JFGG
					this.setMensaje("Se cre\u00F3 la cotizaci\u00F3n:" + cotizacion.getIdToCotizacion());
					this.setTipoMensaje(TIPO_MENSAJE_EXITO);
				}else{
					if(updateSolicitud){
						cotizacion = cotizacionService.updateSolicitudCotizacion(cotizacion);
					}
					cotizacion = cotizacionService.guardarCotizacion(cotizacionDto);
					next = true;//JFGG
				}
			}else{
				init();
				return INPUT;
			}

			coberturaCotizacionList = cotizadorAgentesService.mostrarCorberturaCotizacion(coberturaCotizacionList, incisoCotizacion);
			incisoCotizacion.setCotizacionDTO(cotizacion);
			idToCotizacion = cotizacion.getIdToCotizacion();
			recalcularCoberturasInciso();
			numeroInciso = incisoCotizacion.getId().getNumeroInciso();
			
			forma = DATOS_PAQUETES;
			if(next){
				String mensaje = null;
				Map<String, Object> map = cotizacionExtService.evalueExcepcion(cotizacion, true);
				if(map!= null && !map.containsKey("statusExcepcion") && "true".equals(String.valueOf(map.get("statusExcepcion")))){//JFGG
					CotizacionExtDTO cotizacionExtDto = cotizacionExtService.getCotizacionExt(cotizacion.getIdToCotizacion());//JFGG
					statusExcepcion = cotizacionExtDto.isStatusException();
					mensaje = String.valueOf(map.get("statusExcepcion"));
					if(map.containsKey("idExcepcion") &&  map.get("idExcepcion") != null) {
						idExcepcion = Long.parseLong(String.valueOf(map.get("idExcepcion")));
					} else { idExcepcion = 0; }
					
				    setMensajeError(mensaje);
				    return INPUT;
				}
			}
			if(isJson){
				return "successJson";
			}
		} catch(Exception e) {
			LOG.error(this.getClass().getName() + ".saveCotizacion.Excepcion: " + e, e);
			return INPUT;
		}
		return SUCCESS;
	}

	private void setDatosCotizacionPaquete() {
		vehiculo.setIdNegocioSeccion(idNegocioSeccion);
		incisoCotizacion.getIncisoAutoCot().setNegocioPaqueteId(idNegocioPaqueteBase);
		incisoCotizacion.getIncisoAutoCot().setIdMoneda(cotizacion.getIdMoneda().shortValue());
		idTipoPoliza = cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza();
		idTipoProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto();
		idNegocioSeccion = vehiculo.getIdNegocioSeccion();
	}

	private void recalcularCoberturasInciso(){
		try{
			borrarInciso();
			if(cotizacion != null){
				if(incisoCotizacion.getId() == null){
					incisoCotizacion.setId(new IncisoCotizacionId());
				}
				if(incisoCotizacion.getId().getIdToCotizacion() == null){
					incisoCotizacion.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());
				}
				incisoCotizacion.setCotizacionDTO(cotizacion);
			}
			
			incisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
			incisoCotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));
			
			incisoCotizacion = incisoService.prepareGuardarIncisoAgente(
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
					coberturaCotizacionList,null, null, null, null);
			
			calcularInciso();
			//Calcular Descuento Global (Ya no se aplica como descuento, solamente se utiliza para la exclusion de bonos)
			cotizacion.setPorcentajeDescuentoGlobal(calculoService.calculaPorcentajeDescuentoGlobal(idToCotizacion));
			//Se agregam anexos y condiciones
			cotizadorAgentesService.generaAnexosYCondiciones(idToCotizacion);
		}catch(Exception ex){
			LOG.error("Error al calcular el inciso", ex);
		}
	}
	
	private void calcularInciso(){
		boolean riesgosValidos = true;
		if(incisoCotizacion.getId().getNumeroInciso() != null){	
			riesgosValidos = listadoService.validaDatosRiesgosCompletosCotizacion(
					incisoCotizacion.getId().getIdToCotizacion(),incisoCotizacion.getId().getNumeroInciso());
			if(!riesgosValidos){
				requiereDatosAdicionales = true;
			}else{
				calculoService.calcular(incisoCotizacion);
			}
		}
	}
	
	private void borrarInciso() {
		IncisoCotizacionDTO incisoGuardado = null;
		if(incisoCotizacion != null && incisoCotizacion.getId() != null){
			incisoGuardado = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
			if(incisoGuardado != null){
				NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
				SeccionDTO seccionDTO = negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
				if(incisoGuardado.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccionDTO.getIdToSeccion().longValue()){
					incisoService.borrarInciso(incisoCotizacion);
					incisoCotizacion.getId().setNumeroInciso(null);
				}
			}
		}
	}

	public String verEsquemaPagoAgente(){
		try{
			esquemaPagoCotizacionList=cotizadorAgentesService.verEsquemaPagoAgente(cotizacion);
		}catch(Exception e){
			LOG.error("Error al obtener el esquema de Pago", e);
			esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
		}
		return SUCCESS;
	}
	
	public String mostrarResumenTotalesCotizacionAgente() {
		try {
			cargarYMostrarListaEjecutivos();
			//JFGG
			//CotizacionDTO cotizacionDTO = cotizacionService.obtenerCotizacionPorId(cotizacion.getIdToCotizacion());
			CotizacionExtDTO cotizacionExtDto = cotizacionExtService.getCotizacionExt(cotizacion.getIdToCotizacion());
			statusExcepcion = cotizacionExtDto.isStatusException();
			resumenCostosDTO = cotizadorAgentesService.mostrarResumenTotalesCotizacionAgente(cotizacion, statusExcepcion);

			CotizacionDTO cotizacionDTO = cotizacionService.obtenerCotizacionPorId(cotizacion.getIdToCotizacion());
			if(statusExcepcion){
				Map<String, Object> map = cotizacionExtService.evalueExcepcion(cotizacionDTO, true);
				mensajeExcepcion = String.valueOf(map.get("mensaje"));
				if(map.containsKey("idExcepcion") &&  map.get("idExcepcion") != null) {
					idExcepcion = Long.parseLong(String.valueOf(map.get("idExcepcion")));
				} else { idExcepcion = 0; }
			}
			resumenCostosDTO.setShowExcepcion(true);
			resumenCostosDTO.setStatusExcepcion(statusExcepcion);
			resumenCostosDTO.setMensajeExcepcion(mensajeExcepcion);
			//JFGG
			
			
		} catch(Exception e) {
			LOG.error("mostrarResumenTotalesCotizacionAgente.Exception: " + e, e);
			resumenCostosDTO = new ResumenCostosDTO();
		}
		return SUCCESS;
	}
	
	public String verDetalleIncisoAgente(){
	    coberturaCotizacionList = cotizadorAgentesService.verDetalleIncisoAgente(incisoCotizacion);
	    if(incisoCotizacion.getId()!=null){
	    	incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
	    }
	    
		diasSalario =  cotizadorAgentesService.obtenerDiasSalario();
		
	    return SUCCESS;
	}
	
	public String actualizarDatosAsegurados() throws Exception{
		incisoCotizacion = cotizadorAgentesService.saveDatosComplementariosAsegurado(
					incisoCotizacion, saveAsegurado);
		return SUCCESS;
	}
	
	public String getListaDocumentos(){
		aplicarArt140(incisoCotizacion.getCotizacionDTO());
		listaDocumentosFortimax = cotizadorAgentesService.getListaDocumentosFortimax(
				incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante().longValue());
		return SUCCESS;
	}
	
	public String getURLFortimax(){
		aplicarArt140(incisoCotizacion.getCotizacionDTO());
		mapGenerico = cotizadorAgentesService.getURLFortimax(
				incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante().longValue());
		return SUCCESS;
	}

	public void cargarYMostrarListaEjecutivos(){
		userTieneRolEjecutivo = pagoService.validarUsuarioActualSiEsEjecutivoOrSucursal();
		if(userTieneRolEjecutivo!=null && !userTieneRolEjecutivo.equals(PolizaPagoService.NO_TIENE_ROL_EJECUTIVO)){
			listaDeEjecutivosMap = pagoService.cargarListaEjecutivosPorUsuario(userTieneRolEjecutivo);
		}
	}
	
	public String saveDatosComplementarios(){
		
		if(correo != null && correo.trim().length() > 5){
			cotizacion.getSolicitudDTO().setEmail(correo);
		}
		nombreContratante = cotizacion.getSolicitudDTO().getNombreCompleto();
		incisoCotizacion = cotizadorAgentesService.saveDatosComplementarios(files, filesFileName, filesContentType, incisoCotizacion);
		idToCotizacion = incisoCotizacion.getId().getIdToCotizacion();
		numeroInciso = incisoCotizacion.getId().getNumeroInciso();
		forma = DATOS_COBRANZA_EMISION;
		newCotizadorAgente = true;
		LOG.info(":::::::::::  [ INFO ]  :::::::::::::  CORREO ELECTRÓNICO CLIENTE " + correo);
		LOG.info(":::::::::::  [ INFO ]  :::::::::::::  CORREO CLIENTE " + nombreContratante);
		return SUCCESS;
	}
	
	public String validarDatosComplementarios() {
		mensajeDTO = cotizadorAgentesService.validarListaParaEmitir(idToCotizacion);
		return SUCCESS;
	}
	
	private boolean validaCotizacion(CotizacionDTO cotizacionDTO) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateTime inicioVigencia = new DateTime(cotizacionDTO.getFechaInicioVigencia());
		DateTime finVigencia = new DateTime(cotizacionDTO.getFechaFinVigencia());
		long days = Days.daysBetween(inicioVigencia,finVigencia).getDays();
		if(days<0){
			this.setMensajeError("La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia");
			return false;
		}
		
		//aplicarArt140();
		if(getForzarArt140() && files !=null && files.isEmpty() && listaDocumentosFortimax!=null && !listaDocumentosFortimax.isEmpty()){
			this.setMensajeError("La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia");
			return false;
		}
		
		if(idTipoPoliza!=null){
			NegocioTipoPoliza negocioTipoPoliza  = entidadService.findById(NegocioTipoPoliza.class, idTipoPoliza);
			if(negocioTipoPoliza!=null){
				cotizacionDTO.setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
				Long valorMaximoUnidadVigencia = cotizacionDTO.getTipoPolizaDTO().getProductoDTO().getValorMaximoUnidadVigencia().longValue();
				// Valida que los días de vigencia de la cotizacion no sean mayor a 365 dias
				if ( valorMaximoUnidadVigencia<= VALORMAXIMOUNIDADVIGENCIA && 
						days > VALORMAXIMOUNIDADVIGENCIA){
						this.setMensajeError("Los dias de vigencia de la cotizacion no pueden ser mayores a un anio");
						return false;
				}
				// Valida que los días de vigencia de la cotizacion no sean mayores a los del producto
				if (days > valorMaximoUnidadVigencia) {
						this.setMensajeError("Los dias de la cotizacion son mayores a los dias de vigencia del producto");
						return false;
				}
				// Valida que la fecha fin fija del negocio sea igual que la 
				// Fecha fin de la cotizacion y compara las fechas
				Date fechaFija = cotizacionDTO.getSolicitudDTO().getNegocio().getFechaFinVigenciaFija();
				if ( fechaFija!= null && 
						!DateUtils.isSameDay(fechaFija, cotizacion.getFechaFinVigencia())) {
					cotizacion.setFechaFinVigencia(fechaFija);
					this.setMensajeError("La fecha fin de vigencia debe de ser igual a " + dateFormat.format(fechaFija));
					return false;
				}
			}
		}
		return true;	
	}
	
	public String validarInicioVigencia(){
		//MLA Validar la fecha maxima de retroactividad y de diferimiento del negocio
		try{
			inicioVigenciaValido = true;
			Negocio negocio = entidadService.findById(Negocio.class, cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			BigDecimal diasDiferimiento = negocio.getDiasDiferimiento();
			BigDecimal diasRetroactividad = negocio.getDiasRetroactividad();
			LocalDate fechaActual = new LocalDate();
			LocalDate iniVigencia = new LocalDate(cotizacion.getFechaInicioVigencia());
			if(iniVigencia.isBefore(fechaActual)){
				int diasAntes = Days.daysBetween(iniVigencia, fechaActual).getDays();
				if(diasAntes > diasRetroactividad.intValue()){
					this.setMensajeError("La fecha de inicio de vigencia excede el maximo de dias de retroactividad del negocio: " + diasRetroactividad);
					inicioVigenciaValido = false;
				}
			}else if (iniVigencia.isAfter(fechaActual)){
				int diasDespues = Days.daysBetween(fechaActual, iniVigencia).getDays();
				if(diasDespues > diasDiferimiento.intValue()){
					this.setMensajeError("La fecha de inicio de vigencia excede el maximo de dias de diferimiento del negocio: " + diasDiferimiento);
					inicioVigenciaValido = false;
				}
			}
		}catch(Exception ex){
			LOG.error("Error al validar el inicio de vigencia: ", ex);
		}
		return SUCCESS;
		//MLA
	}
	
	
	private void cargarListasDatosPoliza(
			Map<String, Map<String, String>> mapList) {
		if(mapList.containsKey("negocioProductoList") && negocioProductoList.isEmpty()){
				negocioProductoList.putAll(mapList.get("negocioProductoList"));
		}
		if(mapList.containsKey("negocioTipoPolizaList") && negocioTipoPolizaList.isEmpty()){
				negocioTipoPolizaList.putAll(mapList.get("negocioTipoPolizaList"));
		}
		if(mapList.containsKey("listarNegocios") && listarNegocios.isEmpty()){
				listarNegocios.putAll(mapList.get("listarNegocios"));
		}
		if(mapList.containsKey("marcaMap") && marcaMap.isEmpty()){
				marcaMap.putAll(mapList.get("marcaMap"));
		}
		if(mapList.containsKey("modeloMap") && modeloMap.isEmpty()){
				modeloMap.putAll(mapList.get("modeloMap"));
		}
		if(mapList.containsKey("tipoUsoMap") && tipoUsoMap.isEmpty()){
				tipoUsoMap.putAll(mapList.get("tipoUsoMap"));
		}
		if(mapList.containsKey("estadoMap") && estadoMap.isEmpty()){
				estadoMap.putAll(mapList.get("estadoMap"));
		}
		if(mapList.containsKey("municipioMap") && municipioMap.isEmpty()){
				municipioMap.putAll(mapList.get("municipioMap"));
		}
		
	}

	private void getUsuarioAgente() {
		LOG.info("Iniciando proceso de getUsuarioAgente()");
		isAgente = false;
		cuentaConAgente = false;
		if(cotizacion.getSolicitudDTO()==null){
			cotizacion.setSolicitudDTO(new SolicitudDTO());
		}
		
		if(usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			LOG.info("El usuario actual tiene el permiso FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual o FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual");
			LOG.info("Obtener el Agente de acuerdo al usuario Actual");
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			if (agenteUsuarioActual!=null){
				LOG.info("Usuario Agente: "+agenteUsuarioActual.getIdAgente());
				cotizacion.getSolicitudDTO().setAgente(agenteUsuarioActual);
				cotizacion.getSolicitudDTO().setCodigoAgente(BigDecimal.valueOf(agenteUsuarioActual.getId()));
				cuentaConAgente = true;
				isAgente = true;
			}
		}else{
			LOG.info("El usuario actual no tiene los permisos FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual, FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual");
			cuentaConAgente = cotizacion.getSolicitudDTO().getCodigoAgente()!=null;
		}
	}

	private void setTipoPoliza() {
		NegocioTipoPoliza negocioTipoPoliza = cotizadorAgentesService.getNegocioTipoPoliza(cotizacion);
		cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
		if(idTipoPoliza==null){
			idTipoPoliza = cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza();
		}
	}
	
	private void setValoresPredefinidos(BigDecimal codigoAgente) {
		if(cuentaConAgente && codigoAgente!=null){
			idNegocioBase = new Long(cotizadorAgentesService.getValorParameter(
					ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES, 
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIO_BASE));
			idProductoBase = new BigDecimal(cotizadorAgentesService.getValorParameter(
					ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_PRODUCTO_BASE));
			idTipoPolizaBase = new BigDecimal(cotizadorAgentesService.getValorParameter(
					ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_POLIZA_BASE));
			idTipoVigenciaBase = new BigDecimal(cotizadorAgentesService.getValorParameter(
					ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_POLIZA_BASE));
			
			//MLA se agrega validacion para que en caso de que se acceda desde la liga de agente, esto no se ejecute
			if(configuracion == null){
				//MLA
				negocioAgenteList = cotizadorAgentesService.getNegociosPorAgente(
						codigoAgente.intValue(), Negocio.CLAVE_NEGOCIO_AUTOS);			
				if(negocioAgenteList.size() >= 1){
					boolean tieneIdNegocioBase = false;
					if(cotizacion.getSolicitudDTO().getNegocio() == null){
						cotizacion.getSolicitudDTO().setNegocio(new Negocio());
					}
					for(Negocio negocio: negocioAgenteList){
						if(idNegocioBase.equals(negocio.getIdToNegocio())){
							tieneIdNegocioBase = true;
						}
					}
					cotizacion.getSolicitudDTO().getNegocio().setIdToNegocio(
							tieneIdNegocioBase?idNegocioBase:negocioAgenteList.get(0).getIdToNegocio());				
				}
			}else{
				//MLA Se carga el negocio en la lista del cotizador, se carga el negocio en la cotizacion 
				cotizacion.setTipoCotizacion(LIGA_AGENTE);
				negocioAgenteList = new ArrayList<Negocio>();
				negocioAgenteList.add(configuracion.getNegocio());
				if(cotizacion.getSolicitudDTO().getNegocio() == null){
					cotizacion.getSolicitudDTO().setNegocio(configuracion.getNegocio());
				}
				//MLA
			}
		}		
	}

	private void setListDatosPaquetes() {
		Long idNegocio = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
		if(cotizacion.getIdToCotizacion() != null
				&& incisoCotizacion != null
				&& incisoCotizacion.getId() != null
				&& incisoCotizacion.getId().getNumeroInciso() != null){
			derechoMap = listadoService.getMapDerechosPolizaByNegocio(cotizacion.getIdToCotizacion(), incisoCotizacion.getId().getNumeroInciso());
		}else if(idNegocio!=null){
			derechoMap = listadoService.getMapDerechosPolizaByNegocio(new BigDecimal(idNegocio));
		}
		paqueteMap = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idNegocioSeccion);
		formasdePagoList = listadoService.getMapFormasdePagoByNegTipoPolizaString(idTipoPoliza);
	}

	public String mostrarInformacionVehicular(){
		
		try {			
			idNegocioSeccion = vehiculo.getIdNegocioSeccion();
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idNegocioSeccion);
			CotizacionDTO cotizacion = incisoCotizacion.getCotizacionDTO();
			
			if ( negocioSeccion != null && cotizacion != null ){
				
				infoVehicular = fronterizosService.mostrarInformacionVehicular( vehiculo.getNumeroSerie(), negocioSeccion, 
						vehiculo.getIdMoneda(), vehiculo.getIdEstado(), incisoCotizacion.getId().getIdToCotizacion(), 
						incisoCotizacion.getId().getNumeroInciso(), cotizacion);
				
				if ( infoVehicular != null ){
					if ( infoVehicular.containsKey("desactivada") ){
						super.setMensajeError("desactivada");
					}
					else if ( infoVehicular.containsKey("oSesas") ){
						super.setMensajeError(this.traeMensaje(infoVehicular));
					}
					else{
						super.setMensajeExitoPersonalizado("El n\u00FAmero de serie ingresado ha sido validado.");	
					}
				}
				else{
					super.setMensajeError("N\u00FAmero de serie incorrecto.");
				}
			}
			else{
				super.setMensajeError("No se pudo validar el n\u00FAmero de serie.");
			}			
		}		
		catch (Exception e) {
			LOG.error("No se pudo validar el n\u00FAmero de serie.", e);
			super.setMensajeError("No se pudo validar el n\u00FAmero de serie.");
		}		
		return SUCCESS;
	}
	
	private String traeMensaje( HashMap<String, Map<String, String>> infoVehicular ){
		Map<String,String> map = infoVehicular.get("oSesas");
		return map.get("ob");
	}
	
	public String validarFechasCotizacion(){
		mapGenerico = cotizadorAgentesService.validaCotizacion(cotizacion, idTipoPoliza);
		return SUCCESS;
	}
	
	public String finalizarCotizacionLigaAgente(){
		try{
			currentDate = new Date();
			prepareMostrarContenedor();
			mostrarContenedor();
			String nombreContratante = "";
			String correoContratante = "";
			String telefonoContratante ="";
			if(cotizacion != null
					&& cotizacion.getNombreContratante() != null ){
				nombreContratante = cotizacion.getNombreContratante();
				correoContratante = cotizacion.getCorreoContratante();
				telefonoContratante = cotizacion.getTelefonoContratante();
			}
			configuracionLigaAgenteService.enviarNotificacionCreacionCotizacion(configuracionId, idToCotizacion, nombreContratante, correoContratante, telefonoContratante, getLocale());
			configuracionLigaAgenteService.enviarCotizacionLigaAgenteAlProspecto(configuracionId, idToCotizacion, nombreContratante, correoContratante, getLocale());
		}catch(Exception e){
			LOG.error("Error al enviar la notificaci\u00F3n de la cotizaci\u00F3n de liga de agente", e);
			setMensaje("Error al crear la cotizaci\u00F3n. Favor de intentarlo m\u00E1s tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return SUCCESS;
	}
	
	/**
	 * Permite saber si los documentos son obligatorios o no
	 */
	private void aplicarArt140(CotizacionDTO pcCotizacion){
		// Validar si es obligatorio 
		CotizacionDTO lcCotizacion = pcCotizacion==null?cotizacion:pcCotizacion;
		
		if( lcCotizacion!=null && lcCotizacion.getSolicitudDTO()==null && lcCotizacion.getIdToCotizacion()!=null && lcCotizacion.getIdToCotizacion().compareTo(BigDecimal.ZERO) > 0){
			lcCotizacion = entidadService.findById(CotizacionDTO.class, lcCotizacion.getIdToCotizacion() );

			if( lcCotizacion!=null && lcCotizacion.getSolicitudDTO()==null && lcCotizacion.getIdToCotizacion()!=null && lcCotizacion.getIdToCotizacion().compareTo(BigDecimal.ZERO) > 0){
				final List<CotizacionDTO> cotizacions = entidadService.findByProperty(CotizacionDTO.class, "cotizacionDTO.idToCotizacion", lcCotizacion.getIdToCotizacion() );
				if(cotizacions!=null && !cotizacions.isEmpty()){
					lcCotizacion = cotizacions.get(0);
				}
			}
		}
		if(!getForzarArt140()){
			if( (idNeg != null && idNeg>0)
					|| (lcCotizacion!=null && lcCotizacion.getSolicitudDTO() !=null && lcCotizacion.getSolicitudDTO().getNegocio()!=null) 
					|| (idToCotizacion!=null) ){

				Negocio neg = null;
				if(idNeg != null && idNeg>0){ //cot
					neg = entidadService.findById(Negocio.class, idNeg);//okey
				} else if(lcCotizacion!=null && lcCotizacion.getSolicitudDTO() !=null && lcCotizacion.getSolicitudDTO().getNegocio()!=null){
					neg = lcCotizacion.getSolicitudDTO().getNegocio();//okey
				} else if(idToCotizacion!=null) {
					final CotizacionDTO cotizacionLocal = cotizacionFacadeRemote.findById(idToCotizacion);
					if(cotizacionLocal!=null && cotizacionLocal.getSolicitudDTO() !=null && cotizacionLocal.getSolicitudDTO().getNegocio()!=null){
						neg = cotizacionLocal.getSolicitudDTO().getNegocio();
					}
				}

				if(neg==null){
					forzarArt140 = Boolean.FALSE;
				} else {
					forzarArt140 = neg.getAplicaValidacionArticulo140();
				}
			}	
		}
	}
	
	
	@Autowired
	public void setCotizadorAgentesService(
			CotizadorAgentesService cotizadorAgentesService) {
		this.cotizadorAgentesService = cotizadorAgentesService;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@Autowired
	@Qualifier("cotizacionExtServiceEJB")
	public void setCotizacionExtService(CotizacionExtService cotizacionExtService) {//JFGG
		this.cotizacionExtService = cotizacionExtService;
	}

	@Autowired
	@Qualifier("calculoIncisoAutosServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("cotizacionFacadeRemoteEJB")
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("emisionServiceEJB")
	public void setEmisionService(EmisionService emisionService) {
		this.emisionService = emisionService;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	public List<String> getFilesFileName() {
		return filesFileName;
	}

	public void setFilesFileName(List<String> filesFileName) {
		this.filesFileName = filesFileName;
	}

	public List<String> getFilesContentType() {
		return filesContentType;
	}

	public void setFilesContentType(List<String> filesContentType) {
		this.filesContentType = filesContentType;
	}

	public CotizadorAgentesService getCotizadorAgentesService() {
		return cotizadorAgentesService;
	}

	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}
	
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionList() {
		return coberturaCotizacionList;
	}

	public void setCoberturaCotizacionList(
			List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		this.coberturaCotizacionList = coberturaCotizacionList;
	}
	public List<EsquemaPagoCotizacionDTO> getEsquemaPagoCotizacionList() {
		return esquemaPagoCotizacionList;
	}

	public void setEsquemaPagoCotizacionList(
			List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList) {
		this.esquemaPagoCotizacionList = esquemaPagoCotizacionList;
	}

	public Map<Long, String> getPaqueteMap() {
		return paqueteMap;
	}

	public void setPaqueteMap(Map<Long, String> paqueteMap) {
		this.paqueteMap = paqueteMap;
	}

	public Map<Long, Double> getDerechoMap() {
		return derechoMap;
	}

	public void setDerechoMap(Map<Long, Double> derechoMap) {
		this.derechoMap = derechoMap;
	}

	public Map<String, String> getFormasdePagoList() {
		return formasdePagoList;
	}

	public void setFormasdePagoList(Map<String, String> formasdePagoList) {
		this.formasdePagoList = formasdePagoList;
	}

	public Boolean getCuentaConAgente() {
		return cuentaConAgente;
	}

	public void setCuentaConAgente(Boolean cuentaConAgente) {
		this.cuentaConAgente = cuentaConAgente;
	}

	public List<Negocio> getNegocioAgenteList() {
		return negocioAgenteList;
	}

	public void setNegocioAgenteList(List<Negocio> negocioAgenteList) {
		this.negocioAgenteList = negocioAgenteList;
	}

	public Map<String, String> getNegocioProductoList() {
		return negocioProductoList;
	}

	public void setNegocioProductoList(Map<String, String> negocioProductoList) {
		this.negocioProductoList = negocioProductoList;
	}

	public Map<String, String> getNegocioTipoPolizaList() {
		return negocioTipoPolizaList;
	}

	public void setNegocioTipoPolizaList(
			Map<String, String> negocioTipoPolizaList) {
		this.negocioTipoPolizaList = negocioTipoPolizaList;
	}

	public Map<String, String> getEstadoMap() {
		return estadoMap;
	}

	public void setEstadoMap(Map<String, String> estadoMap) {
		this.estadoMap = estadoMap;
	}

	public Map<String, String> getMunicipioMap() {
		return municipioMap;
	}

	public void setMunicipioMap(Map<String, String> municipioMap) {
		this.municipioMap = municipioMap;
	}
	
	public BigDecimal getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(BigDecimal idMoneda) {
		this.idMoneda = idMoneda;
	}

	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}

	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}

	public BigDecimal getIdMarcaVehiculo() {
		return idMarcaVehiculo;
	}

	public void setIdMarcaVehiculo(BigDecimal idMarcaVehiculo) {
		this.idMarcaVehiculo = idMarcaVehiculo;
	}

	public String getIdEstiloVehiculo() {
		return idEstiloVehiculo;
	}

	public void setIdEstiloVehiculo(String idEstiloVehiculo) {
		this.idEstiloVehiculo = idEstiloVehiculo;
	}

	public Map<String, String> getListarNegocios() {
		return listarNegocios;
	}

	public void setListarNegocios(Map<String, String> listarNegocios) {
		this.listarNegocios = listarNegocios;
	}

	public Short getIdModeloVehiculo() {
		return idModeloVehiculo;
	}

	public void setIdModeloVehiculo(Short idModeloVehiculo) {
		this.idModeloVehiculo = idModeloVehiculo;
	}

	public Map<String, String> getMarcaMap() {
		return marcaMap;
	}

	public void setMarcaMap(Map<String, String> marcaMap) {
		this.marcaMap = marcaMap;
	}

	public Map<String, String> getModeloMap() {
		return modeloMap;
	}

	public void setModeloMap(Map<String, String> modeloMap) {
		this.modeloMap = modeloMap;
	}

	public Map<String, String> getTipoUsoMap() {
		return tipoUsoMap;
	}

	public void setTipoUsoMap(Map<String, String> tipoUsoMap) {
		this.tipoUsoMap = tipoUsoMap;
	}

	public String getDescripcionBusquedaAgente() {
		return descripcionBusquedaAgente;
	}

	public void setDescripcionBusquedaAgente(String descripcionBusquedaAgente) {
		this.descripcionBusquedaAgente = descripcionBusquedaAgente;
	}

	public List<AgenteView> getAgentesList() {
		return agentesList;
	}

	public void setAgentesList(List<AgenteView> agentesList) {
		this.agentesList = agentesList;
	}

	public List<VehiculoView> getListaFlotilla() {
		return listaFlotilla;
	}

	public void setListaFlotilla(List<VehiculoView> listaFlotilla) {
		this.listaFlotilla = listaFlotilla;
	}

	public VehiculoView getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(VehiculoView vehiculo) {
		this.vehiculo = vehiculo;
	}

	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	public String getEstiloSeleccionado() {
		return estiloSeleccionado;
	}

	public void setEstiloSeleccionado(String estiloSeleccionado) {
		this.estiloSeleccionado = estiloSeleccionado;
	}

	public List<CaracteristicasVehiculoDTO> getCaracteristicasVehiculoList() {
		return caracteristicasVehiculoList;
	}

	public void setCaracteristicasVehiculoList(
			List<CaracteristicasVehiculoDTO> caracteristicasVehiculoList) {
		this.caracteristicasVehiculoList = caracteristicasVehiculoList;
	}

	public String getModificadoresDescripcion() {
		return modificadoresDescripcion;
	}

	public void setModificadoresDescripcion(String modificadoresDescripcion) {
		this.modificadoresDescripcion = modificadoresDescripcion;
	}

	public String getDescripcionBusquedaEstilos() {
		return descripcionBusquedaEstilos;
	}

	public void setDescripcionBusquedaEstilos(String descripcionBusquedaEstilos) {
		this.descripcionBusquedaEstilos = descripcionBusquedaEstilos;
	}

	public Integer getForma() {
		return forma;
	}

	public void setForma(Integer forma) {
		this.forma = forma;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public Long getIdNegocioPaqueteBase() {
		return idNegocioPaqueteBase;
	}

	public void setIdNegocioPaqueteBase(Long idNegocioPaqueteBase) {
		this.idNegocioPaqueteBase = idNegocioPaqueteBase;
	}

	public Integer getSaveAsegurado() {
		return saveAsegurado;
	}

	public void setSaveAsegurado(Integer saveAsegurado) {
		this.saveAsegurado = saveAsegurado;
	}

	public Long getIdNegocioBase() {
		return idNegocioBase;
	}

	public void setIdNegocioBase(Long idNegocioBase) {
		this.idNegocioBase = idNegocioBase;
	}

	public boolean isUpdateSolicitud() {
		return updateSolicitud;
	}

	public void setUpdateSolicitud(boolean updateSolicitud) {
		this.updateSolicitud = updateSolicitud;
	}
	
	public IncisoAutoCot getIncisoAutoCot() {
		return incisoAutoCot;
	}

	public void setIncisoAutoCot(IncisoAutoCot incisoAutoCot) {
		this.incisoAutoCot = incisoAutoCot;
	}

	public List<CatalogoDocumentoFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public void setListaDocumentosFortimax(
			List<CatalogoDocumentoFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}

	public BigDecimal getIdProductoBase() {
		return idProductoBase;
	}

	public void setIdProductoBase(BigDecimal idProductoBase) {
		this.idProductoBase = idProductoBase;
	}

	public BigDecimal getIdTipoPolizaBase() {
		return idTipoPolizaBase;
	}

	public void setIdTipoPolizaBase(BigDecimal idTipoPolizaBase) {
		this.idTipoPolizaBase = idTipoPolizaBase;
	}

	public BigDecimal getIdNegocioSeccionBase() {
		return idNegocioSeccionBase;
	}

	public void setIdNegocioSeccionBase(BigDecimal idNegocioSeccionBase) {
		this.idNegocioSeccionBase = idNegocioSeccionBase;
	}

	public BigDecimal getIdTipoUsoBase() {
		return idTipoUsoBase;
	}

	public void setIdTipoUsoBase(BigDecimal idTipoUsoBase) {
		this.idTipoUsoBase = idTipoUsoBase;
	}

	public BigDecimal getIdTipoPoliza() {
		return idTipoPoliza;
	}

	public void setIdTipoPoliza(BigDecimal idTipoPoliza) {
		this.idTipoPoliza = idTipoPoliza;
	}

	public Boolean getNewCotizadorAgente() {
		return newCotizadorAgente;
	}

	public void setNewCotizadorAgente(Boolean newCotizadorAgente) {
		this.newCotizadorAgente = newCotizadorAgente;
	}

	public BigDecimal getIdToPoliza() {
		return idToPoliza;
	}

	public void setIdToPoliza(BigDecimal idToPoliza) {
		this.idToPoliza = idToPoliza;
	}

	public Long getIdTipoProducto() {
		return idTipoProducto;
	}

	public void setIdTipoProducto(Long idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}

	public String getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(String numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public Boolean getIsJson() {
		return isJson;
	}

	public void setIsJson(Boolean isJson) {
		this.isJson = isJson;
	}

	public boolean isGuardaDatoConductorInciso() {
		return guardaDatoConductorInciso;
	}

	public void setGuardaDatoConductorInciso(boolean guardaDatoConductorInciso) {
		this.guardaDatoConductorInciso = guardaDatoConductorInciso;
	}

	public boolean isGuardaObservacionesInciso() {
		return guardaObservacionesInciso;
	}

	public void setGuardaObservacionesInciso(boolean guardaObservacionesInciso) {
		this.guardaObservacionesInciso = guardaObservacionesInciso;
	}

	public boolean isRequiereDatosAdicionales() {
		return requiereDatosAdicionales;
	}

	public void setRequiereDatosAdicionales(boolean requiereDatosAdicionales) {
		this.requiereDatosAdicionales = requiereDatosAdicionales;
	}

	public String getBienasegurado() {
		return bienasegurado;
	}

	public void setBienasegurado(String bienasegurado) {
		this.bienasegurado = bienasegurado;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}

	public Map<Object, String> getDiasSalario() {
		return diasSalario;
	}

	public void setDiasSalario(Map<Object, String> diasSalario) {
		this.diasSalario = diasSalario;
	}

	public Short getNumAsientos() {
		return numAsientos;
	}

	public void setNumAsientos(Short numAsientos) {
		this.numAsientos = numAsientos;
	}	

	public BigDecimal getIdAgrupadorPasajeros() {
		return idAgrupadorPasajeros;
	}

	public void setIdAgrupadorPasajeros(BigDecimal idAgrupadorPasajeros) {
		this.idAgrupadorPasajeros = idAgrupadorPasajeros;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public Boolean getIsAgente() {
		return isAgente;
	}

	public void setIsAgente(Boolean isAgente) {
		this.isAgente = isAgente;
	}
	
	public Long getIdPromotoria() {
		return idPromotoria;
	}

	public void setIdPromotoria(Long idPromotoria) {
		this.idPromotoria = idPromotoria;
	}

	public boolean isCompatilbeExplorador() {
		return compatilbeExplorador;
	}

	public void setCompatilbeExplorador(boolean compatilbeExplorador) {
		this.compatilbeExplorador = compatilbeExplorador;
	}

	@Autowired
	@Qualifier("fronterizosServiceEJB")
	public void setFronterizosService(FronterizosService fronterizosService) {
		this.fronterizosService = fronterizosService;
	}

	public HashMap<String, Map<String, String>> getInfoVehicular() {
		return infoVehicular;
	}

	public void setInfoVehicular(
			HashMap<String, Map<String, String>> infoVehicular) {
		this.infoVehicular = infoVehicular;
	}

	public String getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
		idToCotizacion = new BigDecimal(idCotizacion);
	}

	public NegocioService getNegocioService() {
		return negocioService;
	}

	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	public Long getIdNeg() {
		return idNeg;
	}

	public void setIdNeg(Long idNeg) {
		this.idNeg = idNeg;
	}

	public boolean isCorreoObligatorio() {
		return correoObligatorio;
	}

	public void setCorreoObligatorio(boolean correoObligatorio) {
		this.correoObligatorio = correoObligatorio;
	}

	public boolean isFolioReexpedible() {
		return folioReexpedible;
	}

	public void setFolioReexpedible(boolean folioReexpedible) {
		this.folioReexpedible = folioReexpedible;
	}

	public FolioReexpedibleService getFolioService() {
		return folioService;
	}

	public void setFolioService(FolioReexpedibleService folioService) {
		this.folioService = folioService;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public Map<Long, String> getConductosDeCobro() {
		return conductosDeCobro;
	}

	public void setConductosDeCobro(Map<Long, String> conductosDeCobro) {
		this.conductosDeCobro = conductosDeCobro;
	}

	public Map<Boolean, String> getMapDatosIguales() {
		return mapDatosIguales;
	}

	public void setMapDatosIguales(Map<Boolean, String> mapDatosIguales) {
		this.mapDatosIguales = mapDatosIguales;
	}

	public Map<String, String> getMedioPagos() {
		return medioPagos;
	}

	public void setMedioPagos(Map<String, String> medioPagos) {
		this.medioPagos = medioPagos;
	}

	public Integer getNextTap() {
		return nextTap;
	}

	public void setNextTap(Integer nextTap) {
		this.nextTap = nextTap;
	}

	public Map<String, String> getMapGenerico() {
		return mapGenerico;
	}

	public void setMapGenerico(Map<String, String> mapGenerico) {
		this.mapGenerico = mapGenerico;
	}

	public Long getConfiguracionId() {
		return configuracionId;
	}

	public void setConfiguracionId(Long configuracionId) {
		this.configuracionId = configuracionId;
	}

	public ConfiguracionLigaAgente getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(ConfiguracionLigaAgente configuracion) {
		this.configuracion = configuracion;
	}

	public String getClati() {
		return clati;
	}

	public void setClati(String clati) {
		this.clati = clati;
	}

	public boolean isInicioVigenciaValido() {
		return inicioVigenciaValido;
	}

	public void setInicioVigenciaValido(boolean inicioVigenciaValido) {
		this.inicioVigenciaValido = inicioVigenciaValido;
	}

	public Long getConfiguracionIdResumen() {
		return configuracionIdResumen;
	}

	public void setConfiguracionIdResumen(Long configuracionIdResumen) {
		this.configuracionIdResumen = configuracionIdResumen;
	}

	public Long getConfiguracionIdEnCoberturas() {
		return configuracionIdEnCoberturas;
	}

	public void setConfiguracionIdEnCoberturas(Long configuracionIdEnCoberturas) {
		this.configuracionIdEnCoberturas = configuracionIdEnCoberturas;
	}

	public boolean isDatosIguales() {
		return datosIguales;
	}

	public void setDatosIguales(boolean datosIguales) {
		this.datosIguales = datosIguales;
	}

	public String getFunctionPrintPoliza() {
		return functionPrintPoliza;
	}

	public void setFunctionPrintPoliza(String functionPrintPoliza) {
		this.functionPrintPoliza = functionPrintPoliza;
	}
	
	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getNomPaquete() {
		return nomPaquete;
	}

	public String getNomFormaPago() {
		return nomFormaPago;
	}

	public void setNomPaquete(String nomPaquete) {
		this.nomPaquete = nomPaquete;
	}

	public void setNomFormaPago(String nomFormaPago) {
		this.nomFormaPago = nomFormaPago;
	}
	

	public EntrevistaDTO getEntrevista() {
		return entrevista;
	}
	public void setEntrevista(EntrevistaDTO entrevista) {
		this.entrevista = entrevista;
	}
	public Boolean getGuardarEntrevista() {
		return guardarEntrevista;
	}
	public void setGuardarEntrevista(Boolean guardarEntrevista) {
		this.guardarEntrevista = guardarEntrevista;
	}
	public Boolean getGuardarDocumentacion() {
		return guardarDocumentacion;
	}
	public void setGuardarDocumentacion(Boolean guardarDocumentacion) {
		this.guardarDocumentacion = guardarDocumentacion;
	}
	public List<DocumentoDTO> getDocumentacion() {
		return documentacion;
	}
	public void setDocumentacion(List<DocumentoDTO> documentacion) {
		this.documentacion = documentacion;
	}
	public String getPdfDownload() {
		return pdfDownload;
	}
	public void setPdfDownload(String pdfDownload) {
		this.pdfDownload = pdfDownload;
	}
	public Boolean getFinalizaCotizacion() {
		return finalizaCotizacion;
	}
	public void setFinalizaCotizacion(Boolean finalizaCotizacion) {
		this.finalizaCotizacion = finalizaCotizacion;
	}
	
	public List<MedioPagoDTO> getMedioPagoDTOs() {
		return medioPagoDTOs;
	}
	
	public void setMedioPagoDTOs(final List<NegocioMedioPago> mediosAsociados) {
		final List<MedioPagoDTO> medioTmp = new ArrayList<MedioPagoDTO>();
		for (final NegocioMedioPago negocioMedioPago : mediosAsociados){
			
			// El medio de pago "Agente" cambia de nombre por efectivo en el cotizador de agentes
			if( MEDIOPAGO_AGENTE == negocioMedioPago.getMedioPagoDTO().getKey() ){
				negocioMedioPago.getMedioPagoDTO().setDescripcion(MEDIOPAGO_EFECTIVO);
			}

			medioTmp.add(negocioMedioPago.getMedioPagoDTO());
		}
		
		this.medioPagoDTOs = medioTmp;
	}
	
	public Boolean getForzarArt140() {
		if(forzarArt140 == null){
			forzarArt140 = Boolean.FALSE;
		}
		return forzarArt140;
	}
	public void setForzarArt140(Boolean forzarArt140) {
		this.forzarArt140 = forzarArt140;
	}
	public boolean isStatusExcepcion() {
		return statusExcepcion;
	}
	public void setStatusExcepcion(boolean statusExcepcion) {
		this.statusExcepcion = statusExcepcion;
	}
	public String getMensajeExcepcion() {
		return mensajeExcepcion;
	}
	public void setMensajeExcepcion(String mensajeExcepcion) {
		this.mensajeExcepcion = mensajeExcepcion;
	}
	public long getIdExcepcion() {
		return idExcepcion;
	}
	public void setIdExcepcion(long idExcepcion) {
		this.idExcepcion = idExcepcion;
	}
	/** Mostrar mensaje de inspeccion de acuerdo al negocio */
	public void getConfigCanalVenta(CotizacionDTO pcCotizacion){
		// Obtener el ID del Negocio

		if(pcCotizacion!=null && pcCotizacion.getSolicitudDTO()!=null 
				&& pcCotizacion.getSolicitudDTO().getNegocio()!=null
				&& pcCotizacion.getSolicitudDTO().getNegocio().getIdToNegocio()!=null) {
			BigDecimal idToNegocio = BigDecimal.valueOf(pcCotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());

			// Obtiener los canales de venta 
			List<NegocioCanalVenta> canalVentas = canalVentaNegocioService.getCamposPorTipoYNegocio(TIPO_CATALOGO.GPO_GPOS, CLIENTEUNICO, idToNegocio);

			// Evaluar los canales de venta y condicionar si estan activos
			if(canalVentas!=null){
				for (NegocioCanalVenta negocioCanalVenta : canalVentas) {
					if(negocioCanalVenta.getCatalogo()!=null && negocioCanalVenta.getCatalogo().getCodigo()!=null && MENSAJEINSPECCION.equals(negocioCanalVenta.getCatalogo().getCodigo())){
						setMostrarMensaje(negocioCanalVenta.getStatus());
						break;
					}
				}
			}
		}
	}
	
	public Boolean getMostrarMensaje() {
		if(mostrarMensaje == null){
			mostrarMensaje = Boolean.FALSE;
		}
		return mostrarMensaje;
	}
	public void setMostrarMensaje(Boolean mostrarMensaje) {
		this.mostrarMensaje = mostrarMensaje;
	}
	/**
	 * @return the tiposVigenciaList
	 */
	public Map<Integer, String> getTiposVigenciaList() {
		return tiposVigenciaList;
	}
	/**
	 * @param tiposVigenciaList the tiposVigenciaList to set
	 */
	public void setTiposVigenciaList(Map<Integer, String> tiposVigenciaList) {
		this.tiposVigenciaList = tiposVigenciaList;
	}
	/**
	 * @return the idTipoVigenciaBase
	 */
	public BigDecimal getIdTipoVigenciaBase() {
		return idTipoVigenciaBase;
	}
	/**
	 * @param idTipoVigenciaBase the idTipoVigenciaBase to set
	 */
	public void setIdTipoVigenciaBase(BigDecimal idTipoVigenciaBase) {
		this.idTipoVigenciaBase = idTipoVigenciaBase;
	}
	/**
	 * @return the vigenciaDefault
	 */
	public Integer getVigenciaDefault() {
		return vigenciaDefault;
	}
	/**
	 * @param vigenciaDefault the vigenciaDefault to set
	 */
	public void setVigenciaDefault(Integer vigenciaDefault) {
		this.vigenciaDefault = vigenciaDefault;
	}
	
	public Integer getUsuarioExterno() {
		return usuarioExterno;
	}

	public void setUsuarioExterno(Integer usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}
}