package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.agentes.cotizadorServicioPublico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.folioReexpedible.FiltroFolioReexpedible;
import mx.com.afirme.midas2.domain.folioReexpedible.ToFolioReexpedible;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoSumaAseguradaAd;
import mx.com.afirme.midas2.dto.RelacionesTarifaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.folioReexpedibleService.FolioReexpedibleService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.tarifa.TarifaServicioPublicoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizadorServicioPublicoAction extends BaseAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8527814755010215753L;
	public static final Logger LOG = Logger.getLogger(CotizadorServicioPublicoAction.class);
	public static final BigDecimal CODIGO_AGENTE_BASE = BigDecimal.valueOf(Long.valueOf("7251"));
	
	private CotizacionDTO cotizacion;
	private IncisoCotizacionDTO incisoCotizacion;
	private BigDecimal idToCotizacion;
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	private CotizacionService cotizacionService;
	private IncisoService incisoService;
	private CoberturaService coberturaService;
		
	private Long idToNegocio;
	private Long idToNegProducto;
	private Long idToNegTipoPoliza;
	private Long idToNegSeccion;
	private Long idToPaqueteSeccion;
	private Long idToNegPaqueteSeccion;
	private Short idMonedaDTO;
	private String stateId;
	private String cityId;
	private BigDecimal codigoAgente;
	private Short esLineaAutobuses;

	private List<Negocio>negocioList= new ArrayList<Negocio>(1);
	private List<NegocioProducto>negocioProductoList= new ArrayList<NegocioProducto>(1);
	private List<CoberturaCotizacionDTO> coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>(1);
	
	private NegocioPaqueteSeccion negocioPaqueteSeccion;
	private RelacionesTarifaServicioPublicoDTO relacionesTarifaServicioPublicoDTO; 
	private NegocioTipoPoliza negocioTipoPoliza;
	private NegocioSeccion negocioSeccion;
	private Negocio negocio;
	private NegocioProducto negocioProducto;
	private TarifaServicioPublico tarifaServicioPublico;
	private TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublicoSumaAseguradaAd;
	private TarifaServicioPublicoDeduciblesAd tarifaServicioPublicoDeduciblesAd;
	private VigenciaDTO vigenciaDTO;
	private MonedaDTO monedaDTO;
	private EstadoDTO estadoDTO;
	private CiudadDTO ciudadDTO;
	private RespuestaGridRelacionDTO respuesta;
	
	private NegocioProductoService negocioProductoService;
	private TarifaServicioPublicoService tarifaServicioPublicoService;
	private ListadoService listadoService;
	private EntidadService entidadService;
	
	private String idCoberturaNA;
	private BigDecimal idTcVigencia;
	private Double sumaAsegurado;
	private BigDecimal idAgrupadorPasajeros;
	private String claveTarifa;
	
	private String cveFolio;
	private FiltroFolioReexpedible toFiltro;
	private ToFolioReexpedible toFolio;
	private boolean banderaFolio;
	private String mensaje;
	
	@Autowired
	private FolioReexpedibleService folioService;
	
	@Autowired
	private NegocioService negocioService;
	
	@Autowired
	private CotizadorAgentesService cotizadorAgentesService;
	
	@Override
	public void prepare() throws Exception {
		
	}
	
	public void prepareMostrar(){
	}
	
	public String mostrar(){
		String idToNegociosBase = this.getValorParameter(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES, ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIO_BASE_SERVICIOPUBLICO); 
		if(idToNegociosBase != null) {
			List<String> idsNegocios = Arrays.asList(idToNegociosBase.split(UtileriasWeb.SEPARADOR_COMA));
			
			codigoAgente = CODIGO_AGENTE_BASE;
			
			if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
				Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
				if (agenteUsuarioActual != null && agenteUsuarioActual.getId() != null && agenteUsuarioActual.getId().compareTo(0L) > 0) {
					codigoAgente = BigDecimal.valueOf(agenteUsuarioActual.getId());
				}
			}
			
			List<Negocio>negociosList = this.obtenerNegocios();
			
		    for (Negocio negocio: negociosList){
		    	for(String id : idsNegocios){		    		
    		    	if(negocio.getIdToNegocio().toString().trim().equals(id.trim())){	
    		    		negocioList.add(negocio);
    		    		negocioProductoList = negocio.getNegocioProductos();
    		    	}
		    	}
		    }
		    
		    if (negocioList == null || negocioList.size() == 0) {
		    	LOG.info("El agente no tiene permitido vender el negocio: " + idToNegocio);
		    	idToNegocio = null;
		    }
		}
		return SUCCESS;		
	}
	
	public String mostrarFolio(){
		banderaFolio = (cveFolio != null && cveFolio.trim().length() > 0 ? Boolean.TRUE : Boolean.FALSE);
		toFiltro = new FiltroFolioReexpedible();
		toFolio = new ToFolioReexpedible();
		negocioList = this.obtenerNegocios();
		return SUCCESS;
	}
	
	private List<Negocio> obtenerNegocios(){
		Negocio neg = new Negocio();
		neg.setClaveNegocio("A");
		neg.setClaveEstatus((short) 1);
		if(usuarioService.tienePermisoUsuarioActual("Rol_M2_Ejecutivo_Sucursales")){
			neg.setAplicaSucursal(Boolean.TRUE);
		}
		return negocioService.findByFilters(neg);
	}
	
	public String buscarFolio(){
		LOG.info("::::::::::::    [ info ]    ::::::::::::    ENTRA ALMETODO BUSCAR FOLIOS");
		buscaFolio();
		codigoAgente = (toFolio != null && toFolio.getAgenteVinculado() > 0 ? new BigDecimal(toFolio.getAgenteVinculado()) : CODIGO_AGENTE_BASE);
		return SUCCESS;
	}
	
	private void buscaFolio(){
		StringBuilder mensajeFolio = new StringBuilder();
		toFolio = new ToFolioReexpedible();
		folioService.obtenerFolioVenta(toFiltro, mensajeFolio, toFolio);
		mensaje = mensajeFolio.toString();
		LOG.info("::::::::::::    [ info ]    ::::::::::::    MENSAJE ACUAL " + mensaje);
	}
	
	public void prepareObtenerRelaciones(){		
		negocioPaqueteSeccion=entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);
		monedaDTO = entidadService.findById(MonedaDTO.class, idMonedaDTO);
		estadoDTO = entidadService.findById(EstadoDTO.class, stateId);
		ciudadDTO = entidadService.findById(CiudadDTO.class, cityId);
		vigenciaDTO = entidadService.findById(VigenciaDTO.class, idTcVigencia);
		esLineaAutobuses=(tarifaServicioPublicoService.esParametroValido(new BigDecimal(idToNegSeccion),ParametroGeneralDTO.CODIGO_PARAM_GENERAL_LINEA_AUTOBUSES_SERVICIO_PUBLICO)) ? new Short("1") : new Short("0");
		
		tarifaServicioPublico = new TarifaServicioPublico();
		tarifaServicioPublico.setMonedaDTO(monedaDTO);
		tarifaServicioPublico.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
		tarifaServicioPublico.setVigenciaDTO(vigenciaDTO);
		if(estadoDTO != null){
			tarifaServicioPublico.setEstadoDTO(estadoDTO);
		}else{
			estadoDTO = new EstadoDTO();
			tarifaServicioPublico.setEstadoDTO(estadoDTO);			
		}
		if(ciudadDTO != null){
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);
		}else{
			ciudadDTO = new CiudadDTO();
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);			
		}		
	}
	
	public String obtenerRelaciones(){
        relacionesTarifaServicioPublicoDTO = tarifaServicioPublicoService.getTarifasServicioPublico(tarifaServicioPublico);
        return SUCCESS;
    }
	
	public void prepareGenerarCotizacionServicioPublico(){
		if(getIdToCotizacion()!=null) {
			cotizacion = cotizacionFacadeRemote.findById(getIdToCotizacion());
		}
		
		if (cotizacion == null) {
			 cotizacion = new CotizacionDTO();
		 }
	}
	
	public String generarCotizacionServicioPublico(){
		 cotizacion.getSolicitudDTO().setCodigoAgente(codigoAgente);
		 if(idTcVigencia != null) {
			 BigDecimal idFormaPago = listadoService.getIdToFormaDePagoByNegocioValid(
						cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza(), idTcVigencia);
			 
			 if(idFormaPago != null) {
				 cotizacion.setIdFormaPago(idFormaPago); 
			 }
		 }
		 
		Negocio neg = negocioService.findById(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		 
		GregorianCalendar gcFechaInicio = new GregorianCalendar();
		int numeroDiasEspera = neg.getNumeroDiasEspera();
		
		LOG.info("::::::::  [ INFO ]  :::::::: N\\U00FAMERO DE DIAS DE ESPERA ------------------------>>>: " + numeroDiasEspera);
		 
		 gcFechaInicio.setTime(new Date());
		 gcFechaInicio.set(GregorianCalendar.DAY_OF_MONTH, gcFechaInicio.get(GregorianCalendar.DAY_OF_MONTH) + numeroDiasEspera);
		 
		 cotizacion.setFechaInicioVigencia(gcFechaInicio.getTime());
			
		GregorianCalendar gcFechaFin = new GregorianCalendar();
		gcFechaFin.setTime(new Date());
		gcFechaFin.set(GregorianCalendar.DAY_OF_MONTH, gcFechaFin.get(GregorianCalendar.DAY_OF_MONTH) + numeroDiasEspera);
		if(idTcVigencia.compareTo(BigDecimal.ONE)==0)
			gcFechaFin.add(GregorianCalendar.YEAR, 1);
		else
			gcFechaFin.add(GregorianCalendar.MONTH, 6);
		
		 cotizacion.setFechaFinVigencia(gcFechaFin.getTime());		
		 cotizacion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto();
	     cotizacion.setPorcentajebonifcomision(Double.valueOf("0.0"));
	     Long idToNegDerechoPoliza = listadoService.getDerechoPolizaDefault(BigDecimal.valueOf(idToNegocio));
	     NegocioDerechoPoliza negocioPoliza = new NegocioDerechoPoliza();
	     negocioPoliza.setIdToNegDerechoPoliza(idToNegDerechoPoliza);
	     cotizacion.setNegocioDerechoPoliza(negocioPoliza);
	     cotizacion.setPorcentajeIva(Double.valueOf("16"));
	     cotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));
	     if(cveFolio != null && cveFolio.trim().length() > 5){
	    	 Log.info("::::::::::::::::::    [ INFO ]    ::::::::::::::::::    EL FILTRO FOLIO ES DIFERENTE DE NULO");
	    	 cotizacion.setFolio(cveFolio);
	    	 toFiltro = new FiltroFolioReexpedible();
	    	 toFiltro.setFolioInicio(cveFolio);
	    	 buscaFolio();
	    	 toFolio.setActivo(Boolean.FALSE);
	    	 folioService.actualizarFolioReexpedble(toFolio);
	     } else {
	    	 cotizacion.setFolio(CotizacionDTO.NUEVOCOTIZADOR);
	     }
		 cotizacion.setTipoCotizacion(CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO);
		 
		 if(cotizacion.getIdFormaPago() != null && cotizacion.getPorcentajePagoFraccionado() == null){
			 Double pcte = listadoService.getPctePagoFraccionado(cotizacion.getIdFormaPago().intValue(), 
					 cotizacion.getIdMoneda().shortValue());
			 cotizacion.setPorcentajePagoFraccionado(pcte);
		 }		 
		 
		 if(getIdToCotizacion() == null) {
			 cotizacion =cotizacionService.crearCotizacion(cotizacion,null);
			 idToCotizacion = cotizacion.getIdToCotizacion();
		 } else {
			 cotizacion = cotizacionService.guardarCotizacion(cotizacion);
		 }		 
		 generaIncisoCotizacion();
		return SUCCESS;
	}
	
	public String generaIncisoCotizacion(){
		if(getIdToCotizacion()!=null) {
			cotizacion = cotizacionFacadeRemote.findById(getIdToCotizacion());
			if(cotizacion != null){
				setIncisoCot(cotizacion);
				BigDecimal idNegocioSeccion = BigDecimal.valueOf(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId());
				String idMarca = UtileriasWeb.getValueMap(listadoService.getMapMarcaVehiculoPorNegocioSeccionString(idNegocioSeccion));
				String idTipoUso = listadoService.getMapTipoUsoVehiculoByNegocio(idNegocioSeccion).keySet().iterator().next().toString();
				Boolean esServicioPublico=(Boolean)listadoService.getSpvData(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio()).get("esServicioPublico");
					
				incisoCotizacion.getIncisoAutoCot().setTipoUsoId(Long.valueOf(idTipoUso));
				incisoCotizacion.getIncisoAutoCot().setMarcaId(BigDecimal.valueOf(Long.valueOf(idMarca)));
				incisoCotizacion.getIncisoAutoCot().setIdMoneda(cotizacion.getIdMoneda().shortValue());
				incisoCotizacion.getIncisoAutoCot().setEstiloId(UtileriasWeb.STRING_EMPTY);
				incisoCotizacion.getIncisoAutoCot().setModeloVehiculo(Short.valueOf(UtileriasWeb.STRING_CERO));
				
				if (esServicioPublico){
					incisoCotizacion.getIncisoAutoCot().setObservacionesinciso("LA COBERTURA DE RESPONSABILIDAD CIVIL TIENE EL CARACTER DE SEGURO OBLIGATORIO POR "+
																		   	   "LO QUE NO PODRA CESAR EN SUS EFECTOS, RESCINDIRSE NI DARSE POR TERMINADA CON ANTERIORIDAD "+
																		       "A LA FECHA DE TERMINACION DE SU VIGENCIA Y EL PAGO DEBERA EFECTUARSE DE CONTADO.");
				}
				
				incisoCotizacion.getIncisoAutoCot().setIdAgrupadorPasajeros(idAgrupadorPasajeros);
				incisoCotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));
			}
			coberturasSO();
			recalcularCoberturasInciso();
		}
		
		return SUCCESS;
	}
	
	private void coberturasSO(){
		List<CoberturaCotizacionDTO>coberturasList = mostrarCorberturaCotizacion(incisoCotizacion);
		String[] ids = idCoberturaNA.split(UtileriasWeb.SEPARADOR_COMA);
		String[] clavesTarifa = claveTarifa.split(UtileriasWeb.SEPARADOR_COMA);
        for(CoberturaCotizacionDTO cobertura : coberturasList){
        	for(String clave : clavesTarifa){
        		String[] tarifa = clave.split(UtileriasWeb.SEPARADOR_MEDIO);
        		if(tarifa!=null){
        			if(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(
        					BigDecimal.valueOf(Long.valueOf(tarifa[0])))==0){
        				cobertura.setValorSumaAsegurada(tarifa[1]);
        				if (tarifa.length>2) {
        					if(tarifa[2] != null) {
        						cobertura.setValorDeducible(tarifa[2]);
        					}
        				}
        				break;
        			}
        		}
        	}
        	
            cobertura.setClaveContrato(Short.valueOf("1"));
            if(!idCoberturaNA.isEmpty()){
            	boolean contratarCobertura = true;
            	for (String id: ids) {
                     if(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().compareTo(
                    		 BigDecimal.valueOf(Long.valueOf(id))) == 0){
                    	 contratarCobertura = false;
                     }
            	}
            	if(contratarCobertura)
            		 coberturaCotizacionList.add(cobertura);
            }else{
            	coberturaCotizacionList.add(cobertura);
            }
         }
	}
	
	private void recalcularCoberturasInciso(){
		IncisoCotizacionDTO incisoGuardado = null;
		try{
			if(incisoCotizacion != null && incisoCotizacion.getId() != null){
				incisoGuardado = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
				if(incisoGuardado != null){
					NegocioPaqueteSeccion negPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
					SeccionDTO seccionDTO = negPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
					if(incisoGuardado.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccionDTO.getIdToSeccion().longValue()){
						incisoService.borrarInciso(incisoCotizacion);
						incisoCotizacion.getId().setNumeroInciso(null);
					}
				}
			}
			
			incisoCotizacion = incisoService.prepareGuardarIncisoAgente(
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
					coberturaCotizacionList,null, null, null, null);
		}catch(Exception ex){
			LOG.error("Error al calcular el inciso", ex);
		}
	}

	private List<CoberturaCotizacionDTO> mostrarCorberturaCotizacion(IncisoCotizacionDTO incisoCotizacion){
		CotizacionDTO cotizacionDTO = incisoCotizacion.getCotizacionDTO();		
		List<CoberturaCotizacionDTO>coberturaListCotizacion = new ArrayList<CoberturaCotizacionDTO>(1);
		NegocioPaqueteSeccion negPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.valueOf(incisoCotizacion.getIncisoAutoCot().getEstiloId());
		if(incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
			if(incisoCotizacion.getIncisoAutoCot().getEstadoId().trim().equals(UtileriasWeb.STRING_EMPTY)) {
				incisoCotizacion.getIncisoAutoCot().setEstadoId(null);
			}
		}
		if(incisoCotizacion.getIncisoAutoCot().getMunicipioId() != null){
			if(incisoCotizacion.getIncisoAutoCot().getMunicipioId().trim().equals(UtileriasWeb.STRING_EMPTY)) {
				incisoCotizacion.getIncisoAutoCot().setMunicipioId(null);
			}
		}
		Long numeroSecuencia = new Long("1");
		numeroSecuencia = numeroSecuencia + incisoService.maxSecuencia(cotizacionDTO.getIdToCotizacion());
		try{
			NegocioTipoPoliza negTipoPoliza = negPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza();
			coberturaListCotizacion = coberturaService.getCoberturas(negTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio(), 
					negTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto(), 
					negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(),
				negPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), 
				cotizacionDTO.getIdToCotizacion(), cotizacionDTO.getIdMoneda().shortValue(), numeroSecuencia,
				estiloVehiculoId.getClaveEstilo(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), 
				new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null);					
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			coberturaListCotizacion = new ArrayList<CoberturaCotizacionDTO>(1);
		}
		return 	coberturaListCotizacion;
	}
	
	private String getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParma){
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(idToGrupoParamGen);
		parametroGeneralId.setCodigoParametroGeneral(codigoParma);
		ParametroGeneralDTO parameter = entidadService.findById(ParametroGeneralDTO.class, parametroGeneralId);
		if(parameter!=null){
			return parameter.getValor();
		}
		return null;
	}
	
	private void setIncisoCot(CotizacionDTO cotizacion) {
		if(incisoCotizacion==null) {
			incisoCotizacion = new IncisoCotizacionDTO();
		}
		
		if(incisoCotizacion.getId() == null){
			incisoCotizacion.setId(new IncisoCotizacionId());
		}
		if(incisoCotizacion.getId().getIdToCotizacion() == null){
			incisoCotizacion.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());
		}
		incisoCotizacion.setCotizacionDTO(cotizacion);
		
		if(incisoCotizacion.getIncisoAutoCot()==null){
			IncisoAutoCot incisoAutoCot = new IncisoAutoCot();
			incisoCotizacion.setIncisoAutoCot(incisoAutoCot);
		}
		incisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
	}
	
	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}
	

	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public Long getIdToNegProducto() {
		return idToNegProducto;
	}

	public void setIdToNegProducto(Long idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public Long getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setIdToNegTipoPoliza(Long idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public Long getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(Long idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public Long getIdToPaqueteSeccion() {
		return idToPaqueteSeccion;
	}

	public void setIdToPaqueteSeccion(Long idToPaqueteSeccion) {
		this.idToPaqueteSeccion = idToPaqueteSeccion;
	}

	public Long getIdToNegPaqueteSeccion() {
		return idToNegPaqueteSeccion;
	}

	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}

	public Short getIdMonedaDTO() {
		return idMonedaDTO;
	}

	public void setIdMonedaDTO(Short idMonedaDTO) {
		this.idMonedaDTO = idMonedaDTO;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public BigDecimal getCodigoAgente() {
		return codigoAgente;
	}

	public void setCodigoAgente(BigDecimal codigoAgente) {
		this.codigoAgente = codigoAgente;
	}

	public BigDecimal getIdTcVigencia() {
		return idTcVigencia;
	}

	public void setIdTcVigencia(BigDecimal idTcVigencia) {
		this.idTcVigencia = idTcVigencia;
	}
	
	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}
	

	public void setNegocioProductoList(List<NegocioProducto> negocioProductoList) {
		this.negocioProductoList = negocioProductoList;
	}

	public List<NegocioProducto> getNegocioProductoList() {
		return negocioProductoList;
	}

	public NegocioProductoService getNegocioProductoService() {
		return negocioProductoService;
	}
	
	public NegocioPaqueteSeccion getNegocioPaqueteSeccion() {
		return negocioPaqueteSeccion;
	}

	public void setNegocioPaqueteSeccion(NegocioPaqueteSeccion negocioPaqueteSeccion) {
		this.negocioPaqueteSeccion = negocioPaqueteSeccion;
	}

	public RelacionesTarifaServicioPublicoDTO getRelacionesTarifaServicioPublicoDTO() {
		return relacionesTarifaServicioPublicoDTO;
	}

	public void setRelacionesTarifaServicioPublicoDTO(
			RelacionesTarifaServicioPublicoDTO relacionesTarifaServicioPublicoDTO) {
		this.relacionesTarifaServicioPublicoDTO = relacionesTarifaServicioPublicoDTO;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public TarifaServicioPublico getTarifaServicioPublico() {
		return tarifaServicioPublico;
	}

	public void setTarifaServicioPublico(TarifaServicioPublico tarifaServicioPublico) {
		this.tarifaServicioPublico = tarifaServicioPublico;
	}

	public TarifaServicioPublicoSumaAseguradaAd getTarifaServicioPublicoSumaAseguradaAd() {
		return tarifaServicioPublicoSumaAseguradaAd;
	}

	public void setTarifaServicioPublicoSumaAseguradaAd(
			TarifaServicioPublicoSumaAseguradaAd tarifaServicioPublicoSumaAseguradaAd) {
		this.tarifaServicioPublicoSumaAseguradaAd = tarifaServicioPublicoSumaAseguradaAd;
	}

	public TarifaServicioPublicoDeduciblesAd getTarifaServicioPublicoDeduciblesAd() {
		return tarifaServicioPublicoDeduciblesAd;
	}

	public void setTarifaServicioPublicoDeduciblesAd(
			TarifaServicioPublicoDeduciblesAd tarifaServicioPublicoDeduciblesAd) {
		this.tarifaServicioPublicoDeduciblesAd = tarifaServicioPublicoDeduciblesAd;
	}

	public MonedaDTO getMonedaDTO() {
		return monedaDTO;
	}

	public void setMonedaDTO(MonedaDTO monedaDTO) {
		this.monedaDTO = monedaDTO;
	}

	public EstadoDTO getEstadoDTO() {
		return estadoDTO;
	}

	public void setEstadoDTO(EstadoDTO estadoDTO) {
		this.estadoDTO = estadoDTO;
	}

	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}
	
	public VigenciaDTO getVigenciaDTO() {
		return vigenciaDTO;
	}

	public void setVigenciaDTO(VigenciaDTO vigenciaDTO) {
		this.vigenciaDTO = vigenciaDTO;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}
	
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionList() {
		return coberturaCotizacionList;
	}

	public void setCoberturaCotizacionList(
			List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		this.coberturaCotizacionList = coberturaCotizacionList;
	}

	public String getIdCoberturaNA() {
		return idCoberturaNA;
	}

	public void setIdCoberturaNA(String idCoberturaNA) {
		this.idCoberturaNA = idCoberturaNA;
	}
	
	public Double getSumaAsegurado() {
		return sumaAsegurado;
	}

	public void setSumaAsegurado(Double sumaAsegurado) {
		this.sumaAsegurado = sumaAsegurado;
	}

	public BigDecimal getIdAgrupadorPasajeros() {
		return idAgrupadorPasajeros;
	}

	public void setIdAgrupadorPasajeros(BigDecimal idAgrupadorPasajeros) {
		this.idAgrupadorPasajeros = idAgrupadorPasajeros;
	}
	
	public String getClaveTarifa() {
		return claveTarifa;
	}

	public void setClaveTarifa(String claveTarifa) {
		this.claveTarifa = claveTarifa;
	}

	@Autowired
	@Qualifier("negocioProductoServiceEJB")	
	public void setNegocioProductoService(
			NegocioProductoService negocioProductoService) {
		this.negocioProductoService = negocioProductoService;
	}

	@Autowired
	@Qualifier("tarifaServicioPublicoServiceEJB")
	public void setTarifaServicioPublicoService(
			TarifaServicioPublicoService tarifaServicioPublicoService) {
		this.tarifaServicioPublicoService = tarifaServicioPublicoService;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("cotizacionFacadeRemoteEJB")
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("coberturaServiceEJB")
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}

	public String getCveFolio() {
		return cveFolio;
	}

	public void setCveFolio(String cveFolio) {
		this.cveFolio = cveFolio;
	}

	public FolioReexpedibleService getFolioService() {
		return folioService;
	}

	public void setFolioService(FolioReexpedibleService folioService) {
		this.folioService = folioService;
	}

	public boolean isBanderaFolio() {
		return banderaFolio;
	}

	public void setBanderaFolio(boolean banderaFolio) {
		this.banderaFolio = banderaFolio;
	}

	public FiltroFolioReexpedible getToFiltro() {
		return toFiltro;
	}

	public void setToFiltro(FiltroFolioReexpedible toFiltro) {
		this.toFiltro = toFiltro;
	}

	public ToFolioReexpedible getToFolio() {
		return toFolio;
	}

	public void setToFolio(ToFolioReexpedible toFolio) {
		this.toFolio = toFolio;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public CotizadorAgentesService getCotizadorAgentesService() {
		return cotizadorAgentesService;
	}

	public void setCotizadorAgentesService(
			CotizadorAgentesService cotizadorAgentesService) {
		this.cotizadorAgentesService = cotizadorAgentesService;
	}
	
	public Short getEsLineaAutobuses() {
		return esLineaAutobuses;
	}

	public void setEsLineaAutobuses(Short esLineaAutobuses) {
		this.esLineaAutobuses = esLineaAutobuses;
	}
}