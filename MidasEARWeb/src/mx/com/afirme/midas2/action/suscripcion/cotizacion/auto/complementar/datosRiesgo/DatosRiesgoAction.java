package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.datosRiesgo;


import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.util.ValidadorJs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class DatosRiesgoAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = -3671023516626343962L;

	
	public String pruebaConfiguracion(){
		return SUCCESS;
	}
	/* (non-Javadoc)
	 * Esta clase permite como parametros:
	 * name (opcional) default: datosRiesgo
	 * idCotizacion 
	 * numeroInciso
	 */
	@SuppressWarnings("unchecked")
	public String cargaControles() {		
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		
		if (name == null) {
			name = "datosRiesgo";
		}
		
		if (requerido == null) {
			requerido = true;
		}
		
		valores  = (Map<String, String>) valueStack.findValue(name, Map.class);
		if(valores == null){
			valores = new LinkedHashMap<String, String>();
		}
		controles = configuracionDatoIncisoService.getDatosRiesgo(valores, idToCotizacion, numeroInciso);
		return SUCCESS;
	}
	
	private List<ControlDinamicoRiesgoDTO> controles;	
	private BigDecimal idToCotizacion;
	private BigDecimal numeroInciso;
	private String name;
	private Map<String, String> valores;
	private ValidadorJs validadorJs;
	private Boolean requerido;
	private String labelPosition = "left";
	private ConfiguracionDatoIncisoService configuracionDatoIncisoService;
	
	
	@Autowired
	@Qualifier("configuracionDatoIncisoServiceEJB")
	public void setConfiguracionDatoIncisoService(ConfiguracionDatoIncisoService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}
	
	@Autowired
	@Qualifier("validadorJs")
	public void setValidadorJs(ValidadorJs validadorJs) {
		this.validadorJs = validadorJs;
	}
	
	public ValidadorJs getValidadorJs() {
		return validadorJs;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}
	
	public Map<String, String> getValores() {
		return valores;
	}

	public void setValores(Map<String, String> valores) {
		this.valores = valores;
	}

	public Boolean getRequerido() {
		return requerido;
	}

	public void setRequerido(Boolean requerido) {
		this.requerido = requerido;
	}
	
	public String getLabelPosition() {
		return labelPosition;
	}
	
	public void setLabelPosition(String labelPosition) {
		this.labelPosition = labelPosition;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}
	
}
