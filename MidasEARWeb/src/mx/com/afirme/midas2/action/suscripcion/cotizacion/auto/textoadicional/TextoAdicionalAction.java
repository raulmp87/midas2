package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.textoadicional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.textoadicional.TextoAdicionalService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class TextoAdicionalAction  extends BaseAction  implements Preparable {
private static final long serialVersionUID = 1L;	
private CotizacionDTO cotizacion;
private List<TexAdicionalCotDTO> textosAdicionales = new ArrayList<TexAdicionalCotDTO>(1);
private TextoAdicionalService    textoAdicionalService;
private TexAdicionalCotDTO texAdicionalCotDTO = new TexAdicionalCotDTO();
private EntidadService entidadService;
private Date fecha = new Date();
private String usuario;
private Short soloConsulta = 0;


	@Override
	public void prepare() throws Exception {
		if(cotizacion == null){
			cotizacion = new CotizacionDTO();
		}
		if (cotizacion.getIdToCotizacion() != null) {
			cotizacion = entidadService.findById(CotizacionDTO.class,
					cotizacion.getIdToCotizacion());
		}
		setUsuario(getUsuarioActual().getNombreUsuario());
	 }


	public String listar(){		
		return SUCCESS;
	}
	
	public void prepareObtenerTexAdicionalGrid(){
		
	}
	public String obtenertexAdicionalGrid() {
		textosAdicionales = textoAdicionalService.listarFiltrado(cotizacion);
		return SUCCESS;
	}

	
	public void prepareAccionTextoAdicional(){
		String accion = ServletActionContext.getRequest().getParameter(
				"!nativeeditor_status");
		if (accion.equalsIgnoreCase("deleted") || accion.equalsIgnoreCase("updated")) {
			try {
				if (cotizacion.getIdToCotizacion() != null) {
					texAdicionalCotDTO =	entidadService.findById(TexAdicionalCotDTO.class, texAdicionalCotDTO.getIdToTexAdicionalCot());
				}
			} catch (EJBTransactionRolledbackException e) {
				System.out
						.println("ERROR AL INTENTAR INSTANCIAR texAdicionalCotDTO");
			}
		}
	}
	
	public String accionTextoAdicional(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		if(cotizacion != null){
			 if(accion.equalsIgnoreCase("updated")){
			     textoAdicionalService.actualizarGrid(accion,texAdicionalCotDTO,cotizacion);
			 }else if(accion.equalsIgnoreCase("deleted")){
				 textoAdicionalService.eliminaRow(texAdicionalCotDTO);
			 }else{
				 textoAdicionalService.guardarGrid(cotizacion, texAdicionalCotDTO);
			 }
		}
		return SUCCESS;
	}
	
	public List<TexAdicionalCotDTO> getTextosAdicionales() {
		return textosAdicionales;
	}
	
	
	public void setTextosAdicionales(List<TexAdicionalCotDTO> textosAdicionales) {
		this.textosAdicionales = textosAdicionales;
	}
	
	
	public TexAdicionalCotDTO getTexAdicionalCotDTO() {
		return texAdicionalCotDTO;
	}
	
	public void setTexAdicionalCotDTO(TexAdicionalCotDTO texAdicionalCotDTO) {
		this.texAdicionalCotDTO = texAdicionalCotDTO;
	}
	
	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}
	
	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}
	

	@Autowired
	@Qualifier("textoAdicionalServiceEJB")
	public void setTextoAdicionalService(TextoAdicionalService textoAdicionalService) {
		this.textoAdicionalService = textoAdicionalService;
	}


	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}


	public Short getSoloConsulta() {
		return soloConsulta;
	}

}
