package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;
//import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPFiltradoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaSPBitacoraService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype") 
@Namespace(value = "/suscripcion/cotizacion/auto/cargaMasivaServicioPublico/bitacora")
public class BitacoraCargaMasivaSPAction extends BaseAction implements Preparable{
	
	private static final String LOCATION_MOSTAR_CONTENEDOR_BITACORA ="/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/bitacorasResultados/bitacorasCargaMasivaSPublico.jsp";
    private static final String LOCATION_MOSTARCONTENEDOR_BITACORA_GRID ="/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/bitacorasResultados/bitacorasCargaMasivaSPublicoGrid.jsp";
    private static final int INIT_VAL = 0;

	@Autowired
	@Qualifier("cargaMasivaSPBitacoraService")
	private CargaMasivaSPBitacoraService cargaMasivaSPBitacoraService;

	private static final long serialVersionUID = 1L;
	
	//private BigDecimal idToControlArchivo;
	private BitacoraCargaMasivaSPFiltradoDTO bitacoraFiltradoDTO;
	private Long resultadoConteo;
	
	private String poliza;
	private String numSerie;
	private String usuarioCarga;
	private Date fechaInicioCarga;
	private Date fechaFinCarga;
	private String estatus;
	private String codigoError;
	
	List<CargaMasivaServicioPublico> listArchivosAdjuntos;
	List<BitacoraCargaMasivaSPResultadoDTO> resultado = new ArrayList<BitacoraCargaMasivaSPResultadoDTO>();
	
	
	/*******************************************MÉTODOS PREPARE PARA EJECUCIÓN DE ACTIONS***************************************************/
	
	@Override
	public void prepare() throws Exception {

	}
	
	/****************************************************************WEB METOD ACTION*****************************************************************/
	
	
	@Action(value = "mostrarContBitacorasCargaMasivaSPublico", results = { @Result(name = SUCCESS, location = LOCATION_MOSTAR_CONTENEDOR_BITACORA),@Result(name = INPUT, location = LOCATION_MOSTAR_CONTENEDOR_BITACORA)})
	public String mostrarContBitacorasCargaMasivaSPublico() {
		return SUCCESS;
	}
	
	
	@Action(value = "buscarContBitacorasCargaMasivaSPublicoGrid", results = {@Result(name = SUCCESS, location = LOCATION_MOSTARCONTENEDOR_BITACORA_GRID),  @Result(name = INPUT, location = LOCATION_MOSTARCONTENEDOR_BITACORA_GRID)})
	public String buscarContBitacorasCargaMasivaSPublicoGrid() {
		prepareBusquedaPaginada();
		if(super.getPosStart().intValue() == 0){
			super.setTotalCount(cargaMasivaSPBitacoraService.conteoBitacoraCargaMasivaService(bitacoraFiltradoDTO));
		}
		bitacoraFiltradoDTO.setPosStart(super.getPosStart());
		bitacoraFiltradoDTO.setCount(super.getCount());
		bitacoraFiltradoDTO.setOrderByAttribute(decodeOrderBy());
		resultado = cargaMasivaSPBitacoraService.buscarBitacora(bitacoraFiltradoDTO);
		return SUCCESS;
    }	
	
	private void prepareBusquedaPaginada(){
		if(super.getCount() == null){
			super.setCount(REGISTROS_A_MOSTRAR);
		}
					
		if(super.getPosStart() == null){
			super.setPosStart(INIT_VAL);
		}
		
	}
	
	private String decodeOrderBy(){
		String order = null;
		if(!StringUtil.isEmpty(super.getOrderBy())){
			order = ""; //TODO AGREGAR ARREGLO CON NOMBRE DE LAS COLUMNAS PARA EL ORDER BY //COLUMNAS_GRID_CONFIG_CARMASSERPUB[Integer.valueOf(super.getOrderBy())];
			if(!StringUtil.isEmpty(super.getDirect())){
				order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
			}
		}
		return order;
	}
      
	/*****************************SET & GET**************************************/

	public List<BitacoraCargaMasivaSPResultadoDTO> getResultado() {
		return resultado;
	}

	public void setResultado(List<BitacoraCargaMasivaSPResultadoDTO> resultado) {
		this.resultado = resultado;
	}
	public void setBitacoraFiltradoDTO(BitacoraCargaMasivaSPFiltradoDTO bitacoraFiltradoDTO) {
		this.bitacoraFiltradoDTO = bitacoraFiltradoDTO;
	}
	public BitacoraCargaMasivaSPFiltradoDTO getBitacoraFiltradoDTO() {
		return bitacoraFiltradoDTO;
	}

	public String getNumSerie() {
		return numSerie;
	}




	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}




	public String getUsuarioCarga() {
		return usuarioCarga;
	}




	public void setUsuarioCarga(String usuarioCarga) {
		this.usuarioCarga = usuarioCarga;
	}




	public Date getFechaInicioCarga() {
		return fechaInicioCarga;
	}




	public void setFechaInicioCarga(Date fechaInicioCarga) {
		this.fechaInicioCarga = fechaInicioCarga;
	}




	public Date getFechaFinCarga() {
		return fechaFinCarga;
	}




	public void setFechaFinCarga(Date fechaFinCarga) {
		this.fechaFinCarga = fechaFinCarga;
	}




	public String getEstatus() {
		return estatus;
	}




	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}




	public String getCodigoError() {
		return codigoError;
	}



	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

}

