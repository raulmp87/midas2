package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import static mx.com.afirme.midas.sistema.UtileriasWeb.obtenerRutaDocumentosAnexos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.StringUtil;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico.EstatusArchivo;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaSPDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoService;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/suscripcion/cotizacion/auto/cargaMasivaServicioPublico")
public class CargaMasivaServicioPublicoAction extends BaseAction implements Preparable{

	private static final long serialVersionUID = 1L;
	
	private BigDecimal idToControlArchivo;
	private ControlArchivoDTO controlArchivo;
	private Long resultadoConteo;
	private String resultadoDetalle;
	private String[] lista;
	
	private static final String[] COLUMNAS_GRID_CONFIG_CARMASSERPUB = {"idArchivoCargaMasiva", "nombreArchivoCargaMasiva", "fechaCreacion", "codigoUsuarioCreacion", "descripcionEstatus"};

	private static final String	LOCATION_CONT_CARGA_MASIVA_JSP	= "/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/contenedorCargaMasivaServicioPublico.jsp";
	private static final String	LOCATION_ARCHIVOSCARGAMASIVASGRID_JSP	= "/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/archivosCargaMasivaGrid.jsp";
	private static final String LOCATION_MOSTAR_CONTENEDOR_DETALLE ="/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/cargaMasivaServicioPublicoDetalle.jsp";
    private static final String LOCATION_MOSTARCONTENEDOR_DETALLE_GRID ="/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/detalleCargaMasivaGrid.jsp";
    private static final String LOCATION_MOSTARERROR_DETALLE ="/jsp/suscripcion/cotizacion/auto/cargamasivaserviciopublico/cargaMasivaSPublicoErrorDetalle.jsp";
	
	@Autowired
	@Qualifier("cargaMasivaServicioPublicoEJB")
	private CargaMasivaServicioPublicoService cargaMasivaServicioPublicoService;
	
	List<CargaMasivaServicioPublico> listArchivosAdjuntos;
	CargaMasivaServicioPublicoDTO archivoAdjunto;
	List<CargaMasivaSPDetalleDTO> detalle = new ArrayList<CargaMasivaSPDetalleDTO>();
	Long idDetalle;
	
	/**
	 * Método implementado para mostrar la pantalla de carga masiva del modulo de servicio publico
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @return SUCCES
	 */
	@Action(value = "mostrarContCargaMasiva", results = @Result(name = SUCCESS, location = LOCATION_CONT_CARGA_MASIVA_JSP))
	public String mostrarContCargaMasiva() {
		return SUCCESS;
	}
	
	/*******************************************MÉTODOS PREPARE PARA EJECUCIÓN DE ACTIONS***************************************************/
	
	@Override
	public void prepare() throws Exception {
	}
	
	public void prepareObtenerArchivosAdjuntos(){
		if(archivoAdjunto == null){
			archivoAdjunto = new CargaMasivaServicioPublicoDTO();
		}
	}
	
	public void prepareValidaCarga(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/****************************************************************WEB METOD ACTION*****************************************************************/
	
	@Action (value = "obtenerArchivosAdjuntos", results = {@Result(name = SUCCESS, location = LOCATION_ARCHIVOSCARGAMASIVASGRID_JSP), @Result(name = INPUT, location = LOCATION_ARCHIVOSCARGAMASIVASGRID_JSP)})
	public String obtenerArchivosAdjuntos(){
		prepareBusquedaPaginada();		
		
		listArchivosAdjuntos = cargaMasivaServicioPublicoService.consultaArchivosCargaMasivaService(archivoAdjunto);
		
		resultadoConteo = cargaMasivaServicioPublicoService.conteoBusquedaArchivosService(archivoAdjunto);
		
		if(super.getPosStart().intValue() == 0){
			super.setTotalCount(resultadoConteo);
		}
		
		return SUCCESS;
	}
	
	@Action (value = "validaCarga", results = {@Result(name = SUCCESS, location = LOCATION_CONT_CARGA_MASIVA_JSP), @Result(name = INPUT, location = LOCATION_ARCHIVOSCARGAMASIVASGRID_JSP)})
	public String validaCarga() throws FileNotFoundException, IOException{
		File archivo = new File(obtenerRutaDocumentosAnexos() + controlArchivo.getNombreArchivoOriginal());
		
		if(archivo.getName().trim().toLowerCase().endsWith(".xls") || archivo.getName().trim().toLowerCase().endsWith(".xlsx")){
			String md5CheckSum = this.getMd5Checksum(new FileInputStream(archivo));
			
			CargaMasivaServicioPublico archivoCargaMasiva = new CargaMasivaServicioPublico();
			archivoCargaMasiva.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			archivoCargaMasiva.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			archivoCargaMasiva.setMd5CargaMasiva(md5CheckSum);
			archivoCargaMasiva.setNombreArchivoCargaMasiva(archivo.getName().trim());
			archivoCargaMasiva.setArchivo(FileUtils.readFileToByteArray(archivo));
			
			if(cargaMasivaServicioPublicoService.getMd5ArchivoService(md5CheckSum)){
				super.setTipoMensaje(MENSAJE_ERROR_VALDACION);
				super.setMensaje("El archivo ya ha sido agregado o esta en proceso de carga");
			}else{
				archivoCargaMasiva.setEstatusArchivoCargaMasiva(EstatusArchivo.PROCESO.getEstatusArchivo());
//				archivoCargaMasiva = cargaMasivaServicioPublicoService.saveArchivoService(archivoCargaMasiva);
				try {
					List<String> mensajes = cargaMasivaServicioPublicoService.saveDetalleArchivoService(archivoCargaMasiva, archivo);
					super.setMensajeListaPersonalizado("Resultado de la carga", mensajes, TIPO_MENSAJE_INFORMACION);
				} catch (Exception e) {	
					LOG.error( "CG actualizarDatos: " + e.toString());
					super.setMensajeError(MENSAJE_ERROR_GENERAL);
					return ERROR;
				}
			}
		}else{
			super.setTipoMensaje(MENSAJE_ERROR_VALDACION);
			super.setMensaje("El archivo debe ser en formato excel (.xls o .xlsx)");
		}
		archivo.delete();
		return SUCCESS;
	}
	
	/**
	 * Método implementado para mostrar la pantalla de detalle de la carga masiva servicio public
	 * @author JMN
	 * 
	 * @return SUCCES
	 */
	
	
	@Action(value = "mostrarContCargaMasivaDetalle", results = { @Result(name = SUCCESS, location = LOCATION_MOSTAR_CONTENEDOR_DETALLE),@Result(name = INPUT, location = LOCATION_MOSTAR_CONTENEDOR_DETALLE)})
	public String mostrarContCargaMasivaDetalle() {
		
		return SUCCESS;
	}
	
	
	@Action(value = "buscarContCargaMasivaDetalleGrid", results = {@Result(name = SUCCESS, location = LOCATION_MOSTARCONTENEDOR_DETALLE_GRID),  @Result(name = INPUT, location = LOCATION_MOSTARCONTENEDOR_DETALLE_GRID)})
	public String buscarContCargaMasivaDetalleGrid() {
		
		CargaMasivaSPDetalleDTO detalleDTO = new CargaMasivaSPDetalleDTO();
		
		prepareBusquedaDetallePaginada(detalleDTO);
		
		if(super.getPosStart().intValue() == 0){
			super.setTotalCount(cargaMasivaServicioPublicoService.contarDetalle(detalleDTO.getIdArchivo(),detalleDTO.getCount(), detalleDTO.getPosStart()));
		}
		
//		detalleDTO.setPosStart(super.getPosStart());
//		detalleDTO.setCount(super.getCount());
//		detalleDTO.setOrderByAttribute(decodeOrderBy());
		detalle = cargaMasivaServicioPublicoService.obtenerCargaMasivaDetalle(detalleDTO.getIdArchivo(),detalleDTO.getCount(), detalleDTO.getPosStart());
			
		return SUCCESS;
    }
	/**
	 * Método implementado para consulta de errores del detalle de la carga masiva servicio publico
	 * @author JMN
	 * 
	 * @return SUCCES
	 */
	
	
	@Action(value = "consultarErroresDetalle", results = {@Result(name = SUCCESS, location = LOCATION_MOSTARERROR_DETALLE),  @Result(name = INPUT, location = LOCATION_MOSTARERROR_DETALLE)})
	public String consultarErroresDetalle() {
		
		 resultadoDetalle = cargaMasivaServicioPublicoService.mostrarErrorDetalle(idDetalle);
		   lista = resultadoDetalle.split("\n");
		
		return SUCCESS;
    }

	/************************************************************UTILERIAS DE CARGA MASIVA****************************************************/
	
	private String getMd5Checksum(InputStream inputStream) {
		  try {
				MessageDigest md = MessageDigest.getInstance("MD5");

				byte[] dataBytes = new byte[1024];

				int nread = 0;

				while ((nread = inputStream.read(dataBytes)) != -1) {
					md.update(dataBytes, 0, nread);
				}
				;

				byte[] mdbytes = md.digest();

				// convert the byte to hex format
				StringBuffer sb = new StringBuffer("");
				for (int i = 0; i < mdbytes.length; i++) {
					sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16)
							.substring(1));
				}

				return sb.toString();
			} catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
	}
	  
	private void prepareBusquedaPaginada(){
		if(super.getCount() == null){
			super.setCount(20); 
		}
					
		if(super.getPosStart() == null){
			super.setPosStart(0);
		}
		
		archivoAdjunto.setPosStar(super.getPosStart());
		archivoAdjunto.setCount(super.getCount());
		archivoAdjunto.setOrderByAtribute(decodeOrderBy());
		archivoAdjunto.setUsuarioActual(usuarioService.getUsuarioActual().getNombreUsuario());
	}
	// BUSQUEDA PAGINADO DETALLE
	private void prepareBusquedaDetallePaginada(CargaMasivaSPDetalleDTO detalleDTO){
		if(super.getCount() == null){
			super.setCount(20); 
		}
					
		if(super.getPosStart() == null){
			super.setPosStart(0);
		}
		
		detalleDTO.setPosStart(super.getPosStart());
		detalleDTO.setCount(super.getCount());
		detalleDTO.setOrderByAtribute(decodeOrderBy());
        detalleDTO.setIdArchivo(idToControlArchivo.longValue());
	}
	
	private String decodeOrderBy(){
		String order = null;
		if(!StringUtil.isEmpty(super.getOrderBy())){
			order = COLUMNAS_GRID_CONFIG_CARMASSERPUB[Integer.valueOf(super.getOrderBy())];
			if(!StringUtil.isEmpty(super.getDirect())){
				order = order.concat(" ").concat(super.getDirect().startsWith("des")?"desc":"asc");
			}
		}
		return order;
	}

	/*****************************SET & GET**************************************/

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}
	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}
	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public List<CargaMasivaServicioPublico> getListArchivosAdjuntos() {
		return listArchivosAdjuntos;
	}
	public void setListArchivosAdjuntos(
			List<CargaMasivaServicioPublico> listArchivosAdjuntos) {
		this.listArchivosAdjuntos = listArchivosAdjuntos;
	}

	public Long getResultadoConteo() {
		return resultadoConteo;
	}

	public void setResultadoConteo(Long resultadoConteo) {
		this.resultadoConteo = resultadoConteo;
	}

	public List<CargaMasivaSPDetalleDTO> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<CargaMasivaSPDetalleDTO> detalle) {
		this.detalle = detalle;
	}

	public Long getIdDetalle() {
		return idDetalle;
	}

	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}
	

	public String getResultado() {
		return resultadoDetalle;
	}

	public void setResultado(String resultado) {
		this.resultadoDetalle = resultado;
	}

	public String[] getLista() {
		return lista;
	}

	public void setLista(String[] lista) {
		this.lista = lista;
	}

	

	
}

