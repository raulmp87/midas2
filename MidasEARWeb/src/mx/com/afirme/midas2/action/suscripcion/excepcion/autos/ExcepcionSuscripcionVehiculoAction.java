package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.estiloVehiculo.EstiloVehiculoService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionVehiculoAction extends
		ExcepcionSuscripcionAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5024744489512216337L;

	private Map<BigDecimal, String> marcas = null;
	
	private Map<String, String> estilos = null;
	
	private Map<Object, String> cvesTipoBien = null;
	
	private String cveTipoBien = null;
	
	//private BigDecimal lineaNegocioInciso;
	
	private BigDecimal marcaId = null;
	
	private String estiloId = null;
	
	private String amis = null;
	
	private String descripcionEstilo = null;
	
	private String descripcionEstiloId;
	
	private String descripcionBusqueda = null;
	
	private List<EstiloVehiculoDTO> resultadosBusquedaVehiculo = null; 
	
	//private List<NegocioSeccion> negocioSeccionList;
	
	private List<SeccionDTO> seccionList;
	
	private BigDecimal idToSeccion;
	
	private NegocioSeccionService negocioSeccionService;
	
	private EstiloVehiculoService estiloVehiculoService;
	
	private EntidadService entidadService;
	
	@Override
	public void prepare() throws Exception {

	}

	public void prepareMostrarVehiculo() {
		Object obj = null;
		excepcionId = super.obtenerExcepcion();		
		obj = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.AMIS);
		amis = obj != null ? obj.toString() : "";
		obj = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.DESCRIPCION_ESTILO);
		descripcionEstilo = obj != null ? obj.toString() : "";
		
		if(descripcionEstiloId == null && amis != null && amis != ""){
			descripcionEstiloId = estiloVehiculoService.getDescripcionPorClaveAmis(amis);
		}
		
		if(cvesTipoBien == null){
			cvesTipoBien = listadoService.listarCveTipoBien();
		}
		
		if(marcas == null){			
			marcas = new LinkedHashMap<BigDecimal, String>();
		}
		
		if(estilos == null){
			estilos = new LinkedHashMap<String, String>();
		}
		if(seccionList == null){
			//negocioSeccionList = negocioSeccionService.getSeccionListByCotizacionInciso(null);
			seccionList = negocioSeccionService.listarSeccionNegocioByClaveNegocio(this.cveNegocio);
		}
	}

	public String guardarVehiculo() {
		try {
			excepcionId = super.obtenerExcepcion();
			// Guardar las dem�s condiciones
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.AMIS, amis);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.DESCRIPCION_ESTILO, 
					descripcionEstilo != null ? descripcionEstilo.trim().toUpperCase() : null);
			super.setMensajeExitoPersonalizado("Condici�n agregada a la excepci�n");
			// Condici�n Agregada con exito
			super.setAgregada(true);
		} catch (Exception ex) {
			super.setMensajeError("No se puedo agregar la condici�n a la excepci�n en este momento");
		}
		return SUCCESS;
	}

	
	public void prepareBuscarVehiculo(){
		
		if(descripcionBusqueda != null && idToSeccion != null){
			SeccionDTO seccion = entidadService.findById(SeccionDTO.class, idToSeccion);
			//resultadosBusquedaVehiculo = listadoService.listarEstilosPorDescripcion(negSeccion.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien(),descripcionBusqueda,negSeccion.getIdToNegSeccion());
			resultadosBusquedaVehiculo = listadoService.listarEstilosPorDescripcionClaveTipoBien(descripcionBusqueda,seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		}else{
			resultadosBusquedaVehiculo = new ArrayList<EstiloVehiculoDTO>();
		}
	}
	
	public String buscarVehiculo() {
		return SUCCESS;
	}
	
	public String mostrarVehiculo() {
		return SUCCESS;
	}

	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("estiloVehiculoServiceEJB")
	public void setEstiloVehiculoService(EstiloVehiculoService estiloVehiculoService) {
		this.estiloVehiculoService = estiloVehiculoService;
	}

	public String getDescripcionEstilo() {
		return descripcionEstilo;
	}

	public void setDescripcionEstilo(String descripcionEstilo) {
		this.descripcionEstilo = descripcionEstilo;
	}

	public Map<Object, String> getCvesTipoBien() {
		return cvesTipoBien;
	}

	public void setCvesTipoBien(Map<Object, String> cvesTipoBien) {
		this.cvesTipoBien = cvesTipoBien;
	}

	public String getAmis() {
		return amis;
	}

	public void setAmis(String amis) {
		this.amis = amis;
	}

	public Map<BigDecimal, String> getMarcas() {
		return marcas;
	}

	public void setMarcas(Map<BigDecimal, String> marcas) {
		this.marcas = marcas;
	}

	public Map<String, String> getEstilos() {
		return estilos;
	}

	public void setEstilos(Map<String, String> estilos) {
		this.estilos = estilos;
	}

	public BigDecimal getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(BigDecimal marcaId) {
		this.marcaId = marcaId;
	}

	public String getEstiloId() {
		return estiloId;
	}

	public void setEstiloId(String estiloId) {
		this.estiloId = estiloId;
	}

	public String getCveTipoBien() {
		return cveTipoBien;
	}

	public void setCveTipoBien(String cveTipoBien) {
		this.cveTipoBien = cveTipoBien;
	}

	public String getDescripcionBusqueda() {
		return descripcionBusqueda;
	}

	public void setDescripcionBusqueda(String descripcionBusqueda) {
		this.descripcionBusqueda = descripcionBusqueda;
	}

	public List<EstiloVehiculoDTO> getResultadosBusquedaVehiculo() {
		return resultadosBusquedaVehiculo;
	}

	public void setResultadosBusquedaVehiculo(List<EstiloVehiculoDTO> resultadosBusquedaVehiculo) {
		this.resultadosBusquedaVehiculo = resultadosBusquedaVehiculo;
	}

	public String getDescripcionEstiloId() {
		return descripcionEstiloId;
	}

	public void setDescripcionEstiloId(String descripcionEstiloId) {
		this.descripcionEstiloId = descripcionEstiloId;
	}

	public void setSeccionList(List<SeccionDTO> seccionList) {
		this.seccionList = seccionList;
	}

	public List<SeccionDTO> getSeccionList() {
		return seccionList;
	}

	public void setIdToSeccion(BigDecimal idToSeccion) {
		this.idToSeccion = idToSeccion;
	}

	public BigDecimal getIdToSeccion() {
		return idToSeccion;
	}
	
}
