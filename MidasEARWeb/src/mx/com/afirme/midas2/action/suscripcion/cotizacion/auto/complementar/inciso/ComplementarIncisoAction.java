package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
public class ComplementarIncisoAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 4488737212217776540L;	
	private BigDecimal cotizacionId;
	private BigDecimal incisoId;	
	private IncisoService incisoService;
	private EntidadService entidadService;
	private ConfiguracionDatoIncisoService configuracionDatoIncisoService;
	private ClienteFacadeRemote clienteFacade;
	private IncisoCotizacionDTO inciso;
	private IncisoAutoCot incisoAutoCot;
	private Long incisoAutoCotId;
	private String numMotor;
	private String numSerie;
	private String numPlaca;
	private String descripcion;
	private String repuve;
	private Map<String, String> datosRiesgo;
	//Es necesario poner esta expresion para poder realizar las validaciones de Struts 2. Es el OGNL Expression
	private static String datosRiesgoExpresion = "datosRiesgo";
	private ClienteDTO cliente;
	private Short copiarDatos = 0;
	private boolean guardaDatoConductorInciso = true;
	private boolean guardadoExitoso = false;
	private Integer usuarioExterno = 2;
	private Short soloConsulta = 0;
	private Long numeroSecuencia = new Long(1);
	private boolean guardaObservacionesInciso = false;
	private String name;
	
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}
	
	@Autowired
	@Qualifier("configuracionDatoIncisoServiceEJB")
	public void setConfiguracionDatoIncisoService(ConfiguracionDatoIncisoService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}
	
	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void prepare() throws Exception {		
		inciso = entidadService.findById(IncisoCotizacionDTO.class,new IncisoCotizacionId(cotizacionId, incisoId));
		if(inciso != null){
			numeroSecuencia = inciso.getNumeroSecuencia();
		}
	}

	
	public void prepareMostrar(){	
				
	}
	
	public String mostrar(){
		if(inciso != null){
			guardaDatoConductorInciso = incisoService.infoConductorEsRequerido(inciso);
			try{
				
				Usuario usuario = usuarioService.getUsuarioActual();
				usuarioExterno = usuario.getTipoUsuario();
				
				guardaObservacionesInciso = incisoService.observacionesEsRequerido(inciso);
			}catch(Exception e){
			}
			
		if(inciso.getIncisoAutoCot().getPersonaAseguradoId() != null){
			try {
				ClienteGenericoDTO filtro = new ClienteGenericoDTO();
				filtro.setIdCliente(BigDecimal.valueOf(inciso.getIncisoAutoCot().getPersonaAseguradoId()));
				cliente = clienteFacade.loadByIdNoAddress(filtro);				
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(copiarDatos == 2 && cliente != null && cliente.getClaveTipoPersona().shortValue() != 2){
				if(cliente.getNombre() != null) inciso.getIncisoAutoCot().setNombreConductor(cliente.getNombre());
				if(cliente.getApellidoPaterno() != null) inciso.getIncisoAutoCot().setPaternoConductor(cliente.getApellidoPaterno());
				if(cliente.getApellidoMaterno() != null) inciso.getIncisoAutoCot().setMaternoConductor(cliente.getApellidoMaterno());
				if(cliente.getFechaNacimiento() != null) inciso.getIncisoAutoCot().setFechaNacConductor(cliente.getFechaNacimiento());
				if(cliente.getDescripcionOcupacion() != null) inciso.getIncisoAutoCot().setOcupacionConductor(cliente.getDescripcionOcupacion());
			}
			else if(cliente != null && cliente.getClaveTipoPersona().shortValue() == 2){
				copiarDatos = 1;
			}

		}else{
			cliente = new ClienteDTO();
		}
		}
		return SUCCESS;
	}
	
	public void prepareGuardar(){
		
	}
	
	public String guardar(){
		
		if(inciso != null && inciso.getIncisoAutoCot() != null){
			
			if(inciso.getIncisoAutoCot().getNombreConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(inciso.getIncisoAutoCot().getNombreConductor());
				inciso.getIncisoAutoCot().setNombreConductor(valor);
			}
			if(inciso.getIncisoAutoCot().getMaternoConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(inciso.getIncisoAutoCot().getMaternoConductor());
				inciso.getIncisoAutoCot().setMaternoConductor(valor);
			}
			if(inciso.getIncisoAutoCot().getPaternoConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(inciso.getIncisoAutoCot().getPaternoConductor());
				inciso.getIncisoAutoCot().setPaternoConductor(valor);
			}
			if(inciso.getIncisoAutoCot().getOcupacionConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(inciso.getIncisoAutoCot().getOcupacionConductor());
				inciso.getIncisoAutoCot().setOcupacionConductor(valor);
			}
			if(inciso.getIncisoAutoCot().getObservacionesinciso() != null){
				String valor = UtileriasWeb.parseEncodingISO(inciso.getIncisoAutoCot().getObservacionesinciso());
				inciso.getIncisoAutoCot().setObservacionesinciso(valor);
			}
			if(inciso.getIncisoAutoCot().getNombreAsegurado() != null){
				String valor = UtileriasWeb.parseEncodingISO(inciso.getIncisoAutoCot().getNombreAsegurado());
				inciso.getIncisoAutoCot().setNombreAsegurado(valor);
			}
		}

		if(datosRiesgo != null){
			validarDatosRiesgo();
			// Si no existen errores guarda la Configuracion de Dato Inciso
			if (getFieldErrors().isEmpty()) {
				// Obtener los controles dinamicos y poner el valor al control
				// que tenemos en el map.
				incisoService.guardarDatosAdicionalesPaquete(datosRiesgo,
						new BigDecimal(inciso.getIncisoAutoCot()
								.getNegocioSeccionId()), inciso.getId()
								.getIdToCotizacion(), inciso.getId()
								.getNumeroInciso());				
			} else {
				super.setMensajeListaPersonalizado(
						"Error al validar Datos del Paquete",
						new ArrayList<String>(1), BaseAction.TIPO_MENSAJE_ERROR);
				return SUCCESS;
			}
		}
		
		try{
			//validar numero de serie
			List<String> errors = NumSerieValidador.getInstance().validate(
					inciso.getIncisoAutoCot().getNumeroSerie());					
			if(errors.isEmpty()){
				incisoService.complementarDatosInciso(inciso);
				entidadService.save(inciso);
				guardadoExitoso = true;
			}else{
				//inciso.getIncisoAutoCot().setNumeroSerie("");
				incisoService.complementarDatosInciso(inciso);
				entidadService.save(inciso);
				super.setMensajeListaPersonalizado("Datos Guardados / Error al validar el n\u00FAmero de serie:", 
						errors, BaseAction.TIPO_MENSAJE_EXITO);
				guardadoExitoso = true;
				//return SUCCESS;
			}
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}
		//setNextFunction("cerrarComplementarDatosInciso");		
		//super.setMensajeExito();
		return SUCCESS;
	}
	
	
	public String guardarRiesgosCotizacion(){
		validarDatosRiesgo();
		if(name==null){
			name="datosRiesgo";
		}
		//Si no existen errores guarda la Configuracion de Dato Inciso
		if(getFieldErrors().isEmpty()){
			//Obtener los controles dinamicos y poner el valor al control que tenemos en el map.
			incisoService.guardarDatosAdicionalesPaquete(datosRiesgo, new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()), 
					inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso());
			setMensajeExito();
			setNextFunction("cerrarRiesgoRefrescarInciso("+inciso.getId().getIdToCotizacion()+","+inciso.getId().getNumeroInciso()+")");
		}else{
			
			super.setMensajeListaPersonalizado("Error al validar Datos del Paquete", 
					new ArrayList<String>(1), BaseAction.TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		
		return SUCCESS;
	}

	private void validarDatosRiesgo() {
		//Mapa en donde se guarda la dependencia a partir de un id padre se obtiene el id hijo
		Map<String, String> childs = new LinkedHashMap<String, String>();
		//Mapa en donde se guarda la dependencia a partir de un id hijo se obtiene el id padre
		Map<String, String> parents = new LinkedHashMap<String, String>();
		//Lista en donde se guardan los id de los elementos que ya han sido procesados
		List<String> processedElements = new ArrayList<String>();
		List<ControlDinamicoRiesgoDTO> controlDinamicos = new ArrayList<ControlDinamicoRiesgoDTO>();

		//Obtener los controles dinamicos y poner el valor al control que tenemos en el map.
		for(Entry<String, String> entry : datosRiesgo.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			ConfiguracionDatoIncisoId id = configuracionDatoIncisoService.getConfiguracionDatoIncisoId(key); 
			
			ControlDinamicoRiesgoDTO controlDinamicoRiesgoDTO = configuracionDatoIncisoService.findByConfiguracionDatoIncisoId(id);
			//Si no es null quiere decir que el control debe ser validado.
			if (controlDinamicoRiesgoDTO != null) { 
				controlDinamicoRiesgoDTO.setValor(value);
				// Lo agregamos a la lista para que sea procesado.
				controlDinamicos.add(controlDinamicoRiesgoDTO); 
			}
		}
		
		for (ControlDinamicoRiesgoDTO control: controlDinamicos) {
			String id = control.getId();
			//1. Validar si el control no ha sido procesado
			if (!processedElements.contains(id)) {
					validarPorTipoValidador(control);
					processedElements.add(id);
			}
		}
	}
	
	private void validarPorTipoValidador(ControlDinamicoRiesgoDTO controlDinamicoDTO) {
		String id = controlDinamicoDTO.getId();
		String valor = controlDinamicoDTO.getValor();
		Integer tipoValidador = controlDinamicoDTO.getTipoValidador();
		
		//Ahorita en este metodo se va a validar para todos los controles requerido.
		if (StringUtils.isNotBlank(valor)) {
			if (tipoValidador != null) {
				switch (tipoValidador){
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS: 
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 0);
					break;
				}			
			}
		} else {
			addFieldError(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
					getText("error.requerido"));
		}
	}
	
	private void validarDecimal(String fieldName, String numero, int maxEnteros, int maxDecimales) {
		if (!UtileriasWeb.isDecimalValido(numero, maxEnteros, maxDecimales)) {
			addFieldError(fieldName, getText("error.decimal", 
					new String[] { String.valueOf(maxEnteros), String.valueOf(maxDecimales) }));
		}
	}

	private boolean isControlHasFieldErrors(ControlDinamicoRiesgoDTO controlDinamicoDTO) {
		String id = controlDinamicoDTO.getId();
		List<String> fieldErrors = 
			getFieldErrors().get(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id));
		if (fieldErrors == null || fieldErrors.size() == 0) {
			return false;
		}
		return true;
	}
	
	private boolean isControlHijoVisible(ControlDinamicoRiesgoDTO parentControl) {
		Integer tipoDependencia = parentControl.getTipoDependencia();
		String id = parentControl.getId();
		String valor = parentControl.getValor();
		boolean isVisible = false;

		switch (tipoDependencia) {
		case ControlDinamicoRiesgoDTO.TIPO_DEPENDENCIA_ES_HORIZONTAL:
			isVisible = !configuracionDatoIncisoService.isHorizontal(id, valor);
			break;
		}
		return isVisible;
	}

	public BigDecimal getCotizacionId() {
		return cotizacionId;
	}


	public void setCotizacionId(BigDecimal cotizacionId) {
		this.cotizacionId = cotizacionId;
	}


	public BigDecimal getIncisoId() {
		return incisoId;
	}

	public IncisoAutoCot getIncisoAutoCot() {
		return incisoAutoCot;
	}

	public void setIncisoAutoCot(IncisoAutoCot incisoAutoCot) {
		this.incisoAutoCot = incisoAutoCot;
	}

	public Long getIncisoAutoCotId() {
		return incisoAutoCotId;
	}

	public void setIncisoAutoCotId(Long incisoAutoCotId) {
		this.incisoAutoCotId = incisoAutoCotId;
	}

	public String getNumMotor() {
		return numMotor;
	}

	public void setNumMotor(String numMotor) {
		this.numMotor = numMotor;
	}

	public String getNumSerie() {
		return numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public String getNumPlaca() {
		return numPlaca;
	}

	public void setNumPlaca(String numPlaca) {
		this.numPlaca = numPlaca;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getRepuve() {
		return repuve;
	}

	public void setRepuve(String repuve) {
		this.repuve = repuve;
	}

	public IncisoService getIncisoService() {
		return incisoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setIncisoId(BigDecimal incisoId) {
		this.incisoId = incisoId;
	}

	public IncisoCotizacionDTO getInciso() {
		return inciso;
	}

	public void setInciso(IncisoCotizacionDTO inciso) {
		this.inciso = inciso;
	}

	public void setDatosRiesgo(Map<String, String> datosRiesgo) {
		this.datosRiesgo = datosRiesgo;
	}

	public Map<String, String> getDatosRiesgo() {
		return datosRiesgo;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCopiarDatos(Short copiarDatos) {
		this.copiarDatos = copiarDatos;
	}

	public Short getCopiarDatos() {
		return copiarDatos;
	}

	public void setGuardaDatoConductorInciso(boolean guardaDatoConductorInciso) {
		this.guardaDatoConductorInciso = guardaDatoConductorInciso;
	}

	public boolean isGuardaDatoConductorInciso() {
		return guardaDatoConductorInciso;
	}

	public void setGuardadoExitoso(boolean guardadoExitoso) {
		this.guardadoExitoso = guardadoExitoso;
	}

	public boolean isGuardadoExitoso() {
		return guardadoExitoso;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}
	
	public void setUsuarioExterno(Integer usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}

	public Integer getUsuarioExterno() {
		return usuarioExterno;
	}

	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public Long getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setGuardaObservacionesInciso(boolean guardaObservacionesInciso) {
		this.guardaObservacionesInciso = guardaObservacionesInciso;
	}

	public boolean isGuardaObservacionesInciso() {
		return guardaObservacionesInciso;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
