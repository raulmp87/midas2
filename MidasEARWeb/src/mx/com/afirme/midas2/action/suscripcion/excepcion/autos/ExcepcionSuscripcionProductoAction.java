package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoValor;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionProductoAction extends ExcepcionSuscripcionAction implements Preparable{
	
	private static final long serialVersionUID = 4434248582524442211L;

	private Long negocioId;
	
	private Long productoId;
	
	private Long tipoPolizaId;
	
	private Long lineaNegocioId;
	
	private Long paqueteId;
	
	private Long coberturaId;
	
	private Map<Long, String> negocios = null;
	
	private Map<Long, String> productos = null;
	
	private Map<BigDecimal, String> tipoPolizas = null;
	
	private Map<BigDecimal, String> lineasNegocio = null;
	
	private Map<Long, String> paquetes = null;
	
	private Map<BigDecimal, String> coberturas = null;
	
	private Map<Object, String> tiposDeValorCondicion = null;
	
	private short tipoValorSumaAsegurada;
	
	private Double minDeducible;
	
	private Double maxDeducible;
	
	private Double sumaAsegurada;
		
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}
	
	public void prepareMostrarProducto(){
		excepcionId = super.obtenerExcepcion();
		Object value = null;
		if(excepcionId != null){
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.NEGOCIO);
			negocioId = value != null ? Long.valueOf(value.toString()) : null;
			
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.PRODUCTO);
			productoId = value != null ? Long.valueOf(value.toString()) : null;
			
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.TIPO_POLIZA);
			tipoPolizaId = value != null ? Long.valueOf(value.toString()) : null;
			
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.SECCION);
			lineaNegocioId = value != null ? Long.valueOf(value.toString()): null;
			
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.PAQUETE);
			paqueteId = value != null ? Long.valueOf(value.toString()) : null;
			
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.COBERTURA);
			coberturaId = value != null ? Long.valueOf(value.toString()) : null;
			
			List<String> values = excepcionService.obtenerCondicionValores(excepcionId, TipoCondicion.DEDUCIBLE);
			if(!values.isEmpty()){
				minDeducible = Double.valueOf(values.get(0));
				maxDeducible = Double.valueOf(values.get(1));
				
			}
						
			value = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.SUMA_ASEGURADA);
			if(value != null){
				tipoValorSumaAsegurada = excepcionService.obtenerTipoCondicionValor(excepcionId, TipoCondicion.SUMA_ASEGURADA);
			}
			sumaAsegurada = value != null ? Double.valueOf(value.toString()) : null;
		}

		if(negocios == null){
			negocios = listadoService.listarNegociosActivos(cveNegocio);
		}
		
		if(productos == null){
			if (negocioId != null)
				productos = listadoService.getMapNegProductoPorNegocio(negocioId);
			else
				productos =  new LinkedHashMap<Long, String>(); 
		}
		
		if(tipoPolizas == null){
			if (productoId != null)
				tipoPolizas = listadoService.getMapNegTipoPolizaPorNegProducto(productoId);
			else
				tipoPolizas =  new LinkedHashMap<BigDecimal, String>(); 
		}
		
		
		if(lineasNegocio == null){
			if (tipoPolizaId != null)
				lineasNegocio = listadoService.getMapNegSeccionPorNegTipoPoliza(BigDecimal.valueOf(tipoPolizaId));
			else
				lineasNegocio =  new LinkedHashMap<BigDecimal, String>(); 
		}
		
		if(paquetes == null){
			if (lineaNegocioId != null)
				paquetes = listadoService.getMapNegPaqueteSeccionPorNegSeccion(BigDecimal.valueOf(lineaNegocioId));
			else
				paquetes =  new LinkedHashMap<Long, String>(); 
		}
		if(coberturas == null){
			if (paqueteId != null){
				coberturas = listadoService.getMapCoberturaPorNegPaqueteSeccion(paqueteId);
			}else if(lineaNegocioId != null){
				coberturas = listadoService.getMapCoberturaPorNegocioSeccion(lineaNegocioId);
			}else{
				coberturas =  new LinkedHashMap<BigDecimal, String>();
			}
		}
		
		if(tiposDeValorCondicion == null){
			tiposDeValorCondicion = new LinkedHashMap<Object, String>();
			tiposDeValorCondicion.put(String.valueOf(TipoValor.VALOR_DIRECTO.valor()),  "Valor directo");
			tiposDeValorCondicion.put(String.valueOf(TipoValor.RANGO.valor()),  "Rango");
			tiposDeValorCondicion.put(String.valueOf(TipoValor.MAYOR_IGUAL.valor()),  "Mayor o Igual que");
			tiposDeValorCondicion.put(String.valueOf(TipoValor.MENOR_IGUAL.valor()),  "Menor o Igual que");
		}
	}
	
	public String mostrarProducto() {
		
		return SUCCESS;
	}
	
	public String guardarProducto(){
		try{
			excepcionId = super.obtenerExcepcion();
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.NEGOCIO, negocioId != null ? negocioId.toString() : null);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.PRODUCTO, productoId != null ? productoId.toString() : null);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.TIPO_POLIZA, tipoPolizaId != null ? tipoPolizaId.toString() : null);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.SECCION, lineaNegocioId != null ? lineaNegocioId.toString() : null);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.PAQUETE, paqueteId != null ? paqueteId.toString() : null);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.COBERTURA, coberturaId != null ? coberturaId.toString() : null);	
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.DEDUCIBLE, TipoValor.RANGO, 
					new String[]{minDeducible != null ? minDeducible.toString() : "", maxDeducible != null ? maxDeducible.toString() : ""});			
			TipoValor tipoValor = TipoValor.VALOR_DIRECTO;
			if(tipoValorSumaAsegurada == 1){
				tipoValor = TipoValor.RANGO;
			}else if(tipoValorSumaAsegurada == 2){
				tipoValor = TipoValor.MAYOR_IGUAL;
			}else if(tipoValorSumaAsegurada == 3){
				tipoValor = TipoValor.MENOR_IGUAL;
			}
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.SUMA_ASEGURADA, tipoValor, sumaAsegurada != null ? sumaAsegurada.toString() : null);
			super.setMensajeExitoPersonalizado("Condici�n agregada a la excepci�n");
			// Condici�n Agregada con exito
			super.setAgregada(true);
		}catch(Exception ex){
			super.setMensajeError("No se puedo agregar la condici�n a la excepci�n en este momento");
		}
		return SUCCESS;
	}
	

	public Map<Long, String> getNegocios() {
		return negocios;
	}

	public void setNegocios(Map<Long, String> negocios) {
		this.negocios = negocios;
	}

	public Map<Long, String> getProductos() {
		return productos;
	}

	public void setProductos(Map<Long, String> productos) {
		this.productos = productos;
	}

	public Map<Long, String> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(Map<Long, String> paquetes) {
		this.paquetes = paquetes;
	}

	public Map<BigDecimal, String> getCoberturas() {
		return coberturas;
	}

	public void setCoberturas(Map<BigDecimal, String> coberturas) {
		this.coberturas = coberturas;
	}
	
	public Map<BigDecimal, String> getLineasNegocio() {
		return lineasNegocio;
	}

	public void setLineasNegocio(Map<BigDecimal, String> lineasNegocio) {
		this.lineasNegocio = lineasNegocio;
	}

	public Long getExcepcionId() {
		return excepcionId;
	}

	public void setExcepcionId(Long excepcionId) {
		this.excepcionId = excepcionId;
	}

	public Long getNegocioId() {
		return negocioId;
	}

	public void setNegocioId(Long negocioId) {
		this.negocioId = negocioId;
	}

	public Long getProductoId() {
		return productoId;
	}

	public void setProductoId(Long productoId) {
		this.productoId = productoId;
	}

	public Long getLineaNegocioId() {
		return lineaNegocioId;
	}

	public void setLineaNegocioId(Long lineaNegocioId) {
		this.lineaNegocioId = lineaNegocioId;
	}

	public Long getPaqueteId() {
		return paqueteId;
	}

	public void setPaqueteId(Long paqueteId) {
		this.paqueteId = paqueteId;
	}

	public Long getCoberturaId() {
		return coberturaId;
	}

	public void setCoberturaId(Long coberturaId) {
		this.coberturaId = coberturaId;
	}

	public void setTipoPolizaId(Long tipoPolizaId) {
		this.tipoPolizaId = tipoPolizaId;
	}

	public Long getTipoPolizaId() {
		return tipoPolizaId;
	}

	public void setTipoPolizas(Map<BigDecimal, String> tipoPolizas) {
		this.tipoPolizas = tipoPolizas;
	}

	public Map<BigDecimal, String> getTipoPolizas() {
		return tipoPolizas;
	}

	public short getTipoValorSumaAsegurada() {
		return tipoValorSumaAsegurada;
	}

	public void setTipoValorSumaAsegurada(short tipoValorSumaAsegurada) {
		this.tipoValorSumaAsegurada = tipoValorSumaAsegurada;
	}

	public Double getMinDeducible() {
		return minDeducible;
	}

	public void setMinDeducible(Double minDeducible) {
		this.minDeducible = minDeducible;
	}

	public Double getMaxDeducible() {
		return maxDeducible;
	}

	public void setMaxDeducible(Double maxDeducible) {
		this.maxDeducible = maxDeducible;
	}

	public Double getSumaAsegurada() {
		return sumaAsegurada;
	}

	public void setSumaAsegurada(Double sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}

	public Map<Object, String> getTiposDeValorCondicion() {
		return tiposDeValorCondicion;
	}

	public void setTiposDeValorCondicion(Map<Object, String> tiposDeValorCondicion) {
		this.tiposDeValorCondicion = tiposDeValorCondicion;
	}
	
	
}
