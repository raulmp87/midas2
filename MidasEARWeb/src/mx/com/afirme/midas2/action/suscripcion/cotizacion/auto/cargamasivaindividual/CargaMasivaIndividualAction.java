package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.cargamasivaindividual;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;

import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.CargaMasivaIndividualAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasiva;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasivaIndividual;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoSoporteService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CargaMasivaIndividualAction extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private BigDecimal idToControlArchivo;
	private BigDecimal idToCargaMasivaIndAutoCot;
	private BigDecimal idToNegocio;
	private BigDecimal idToNegProducto;
	private BigDecimal idToNegTipoPoliza;
	private NegocioTipoPoliza negocioTipoPoliza;
	private String origen;
	private ControlArchivoDTO controlArchivo;
	private CargaMasivaIndividualAutoCot cargaMasivaIndividualAutoCot;
	private List<CargaMasivaIndividualAutoCot> cargaMasivaIndividualList;
	private List<DetalleCargaMasivaIndAutoCot> cargaMasivaDetalleIndividualList;
	private Boolean logErrors = false;
	private Short tipoCarga = 1;
	private Short claveTipo;
	
	private List<Negocio> negocioList;
	private List<NegocioProducto> productoList = new ArrayList<NegocioProducto>(1);
	private List<NegocioTipoPoliza> tiposPoliza = new ArrayList<NegocioTipoPoliza>(1);
	
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	private Locale locale = getLocale();
	
	private EntidadService entidadService;
	private CargaMasivaService cargaMasivaService;
	private NegocioService negocioService;
	private CotizacionService cotizacionService;
	private NegocioCondicionEspecialService negocioCondicionEspecialService;

	private CondicionEspecialService condicionEspecialService;
	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;

	private RenovacionMasivaService renovacionMasivaService;
	private SeguroObligatorioService seguroObligatorioService;
	private CalculoService calculoService;
	private ListadoService listadoService;
	
	private ClienteFacadeRemote clienteFacadeRemote;
	private DireccionMidasService direccionMidasService;
	
	private PersonaSeycosFacadeRemote personaSeycosFacadeRemote;
	
	private BancoMidasService bancoMidasService;
	
	@Autowired
	@Qualifier("bancoMidasServiceEJB")
	public void setBancoMidasService(BancoMidasService bancoMidasService) {
		this.bancoMidasService = bancoMidasService;
	}
	
	private ClientesApiService  clienteRest;
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}	
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}		
	
	@Autowired
	@Qualifier("renovacionMasivaServiceEJB")
	public void setRenovacionMasivaService(
			RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}	
		
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	
	@Autowired
	@Qualifier("negocioCondicionEspecialServiceEJB")
	public void setNegocioCondicionEspecialService(
			NegocioCondicionEspecialService negocioCondicionEspecialService) {
		this.negocioCondicionEspecialService = negocioCondicionEspecialService;
	}

	/*@Autowired
	public void setIncisoSoporteService(IncisoSoporteService incisoSoporteService) {
		this.incisoSoporteService = incisoSoporteService;
	}*/

	@Autowired
	@Qualifier("cargaMasivaServiceEJB")
	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}
	
	
	
	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}
	

	@Autowired
	@Qualifier("condicionEspecialCotizacionServiceEJB")
	public void setCondicionEspecialCotizacionService(
			CondicionEspecialCotizacionService condicionEspecialCotizacionService) {
		this.condicionEspecialCotizacionService = condicionEspecialCotizacionService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(
			ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Autowired
	@Qualifier("direccionMidasServiceEJB")
	public void setDireccionMidasService(
			DireccionMidasService direccionMidasService) {
		this.direccionMidasService = direccionMidasService;
	}
	
	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacadeRemote(
			PersonaSeycosFacadeRemote personaSeycosFacadeRemote) {
		this.personaSeycosFacadeRemote = personaSeycosFacadeRemote;
	}


	@Override
	public void prepare() throws Exception {
		if(idToNegTipoPoliza != null){
			negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
		}
		//negocios activos
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio("A");
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioList = negocioService.findByFilters(negocio);	
		if(idToNegocio != null){
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("negocio.idToNegocio", idToNegocio);
			parametros.put("productoDTO.claveEstatus",(short)1);
			productoList = entidadService.findByProperties(NegocioProducto.class, parametros);
		}
		if(idToNegProducto != null){
			tiposPoliza = entidadService.findByProperty(NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", idToNegProducto);
		}
	}

	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	public void prepareMostrarCargaMasivaCotizacion(){
	}
	
	public String mostrarCargaMasivaCotizacion(){
		return SUCCESS;
	}
	
	public String mostrarCargaMasivaEmision(){
		return SUCCESS;
	}
	
	public void prepareListarCargaIndividual(){
		if((claveTipo != null &&  idToNegocio != null && idToNegProducto != null && idToNegTipoPoliza != null) ||
				claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_EMISION)){
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("claveTipo", claveTipo);
			if(claveTipo != null &&  idToNegocio != null && idToNegProducto != null && idToNegTipoPoliza != null){
				parametros.put("idNegocio",idToNegocio);
				parametros.put("idToNegProducto",idToNegProducto);
				parametros.put("idToNegTipoPoliza",idToNegTipoPoliza);	
			}
			cargaMasivaIndividualList = entidadService.findByProperties(CargaMasivaIndividualAutoCot.class, parametros);
			//Sort
			if(cargaMasivaIndividualList != null && !cargaMasivaIndividualList.isEmpty()){
				Collections.sort(cargaMasivaIndividualList, 
						new Comparator<CargaMasivaIndividualAutoCot>() {				
							public int compare(CargaMasivaIndividualAutoCot n1, CargaMasivaIndividualAutoCot n2){
								return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
							}
						});
			}
		}else{
			cargaMasivaIndividualList = new ArrayList<CargaMasivaIndividualAutoCot>(1);
		}
		
	}
	
	public String listarCargaIndividual(){
		return SUCCESS;
	}
	
	public void prepareMostrarDetalleCargaIndividual(){
		if(idToCargaMasivaIndAutoCot != null){
			cargaMasivaIndividualAutoCot = entidadService.findById(CargaMasivaIndividualAutoCot.class, idToCargaMasivaIndAutoCot);
		}
	}
	
	public String mostrarDetalleCargaIndividual(){
		return SUCCESS;
	}
	
	public void prepareListarDetalleCargaIndividual(){
		if(idToCargaMasivaIndAutoCot != null){
			cargaMasivaIndividualAutoCot = entidadService.findById(CargaMasivaIndividualAutoCot.class, idToCargaMasivaIndAutoCot);
			cargaMasivaDetalleIndividualList = cargaMasivaService.getCargaMasivaDetalleIndividualList(idToCargaMasivaIndAutoCot);
		}else{
			cargaMasivaDetalleIndividualList = new ArrayList<DetalleCargaMasivaIndAutoCot>(1);
		}
		
	}
	
	public String listarDetalleCargaIndividual(){
		return SUCCESS;
	}
	
	public String descargarPlantilla(){
		GeneraExcelCargaMasivaIndividual plantilla = new GeneraExcelCargaMasivaIndividual(cargaMasivaService,clienteRest);
		    
			plantilla.setNegocioCondicionEspecialService(negocioCondicionEspecialService);
			plantilla.setNegocioTipoPoliza(negocioTipoPoliza);
			plantilla.setCargaMasivaService(cargaMasivaService);
			plantilla.setIdToNegocio(idToNegocio);
						
			try {
				plantillaInputStream = plantilla.generaPlantillaCargaMasiva(listadoService);
				contentType = "application/vnd.ms-excel";
				setFileName("CargaMasivaIndCompCot" + negocioTipoPoliza.getIdToNegTipoPoliza() + ".xls");	
			} catch (IOException e) {
				e.printStackTrace();
			}
		return SUCCESS;
	}
	
	public String descargarLogErrores(){
		GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			FileInputStream plantillaBytes = plantilla.descargaLogErrores(idToControlArchivo);
			
			plantillaInputStream = plantillaBytes;
			contentType = "application/text";
			setFileName("LogErroresCargaMasivaCot" + id + ".txt");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;		
	}
	
	public String descargarPolizasCargaMasiva(){
		if(idToCargaMasivaIndAutoCot != null){
			try {
				GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
				plantilla.setCargaMasivaService(cargaMasivaService);
				InputStream plantillaBytes = plantilla.descargarPolizasCargaMasiva(idToCargaMasivaIndAutoCot, locale);
				plantillaInputStream = plantillaBytes;
				contentType = "application/zip";
				setFileName("CargaMasiva_"+ idToCargaMasivaIndAutoCot +".zip");	
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
		return SUCCESS;
	}
	
	public void prepareValidaCargaIndividual(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public String validaCargaIndividual(){
		if(controlArchivo != null){
			CargaMasivaIndividualAutoCot cargaMasiva = cargaMasivaService.guardaCargaMasivaIndividual(idToNegocio, 
					idToNegProducto, idToNegTipoPoliza, controlArchivo, this.getUsuarioActual().getNombreUsuario(), tipoCarga, claveTipo);
			
			GeneraExcelCargaMasivaIndividual plantilla = new GeneraExcelCargaMasivaIndividual(cargaMasivaService,clienteRest);
			try {
				plantilla.setNegocioTipoPoliza(negocioTipoPoliza);
				plantilla.setCargaMasivaService(cargaMasivaService);
				plantilla.setCondicionEspecialService(condicionEspecialService);
				plantilla.setNegocioCondicionEspecialService(negocioCondicionEspecialService);
				plantilla.setCondicionEspecialCotizacionService(condicionEspecialCotizacionService);
				plantilla.setRenovacionMasivaService(renovacionMasivaService);
				plantilla.setSeguroObligatorioService(seguroObligatorioService);
				plantilla.setListadoService(listadoService);
				plantilla.setCalculoService(calculoService);
				plantilla.setDireccionMidasService(direccionMidasService);
				plantilla.setClienteFacadeRemote(clienteFacadeRemote);
				plantilla.setPersonaSeycosFacadeRemote(personaSeycosFacadeRemote);
				
				Usuario usuario = this.getUsuarioActual();
				plantilla.setIdUsuario(usuario.getId().longValue());
				plantilla.validaCargaMasiva(idToControlArchivo, tipoCarga, claveTipo, bancoMidasService);
				
				cargaMasivaService.guardaDetalleCargaMasivaIndividual(cargaMasiva, plantilla.getDetalleList());
				
				setLogErrors(plantilla.isHasLogErrors());
				if(!plantilla.isHasLogErrors()){
					if(tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL)){
						cargaMasiva.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_TERMINADO);
					}
					if(tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_SCHEDULE)){
						cargaMasiva.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_PENDIENTE);
					}
				}else{
					cargaMasiva.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_CON_ERROR);
				}
				entidadService.save(cargaMasiva);
				
			}catch(Exception e){
				e.printStackTrace();
				setLogErrors(plantilla.isHasLogErrors());
				cargaMasiva.setClaveEstatus(CargaMasivaIndividualAutoCot.ESTATUS_CON_ERROR);
				entidadService.save(cargaMasiva);
			}
		}
		String retorno = SUCCESS;
		if(claveTipo.equals(CargaMasivaIndividualAutoCot.TIPO_EMISION)){
			retorno = "emision";
		}
		return retorno;
	}
	
	public void prepareValidaEmisionIndividual(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getId() {
		return id;
	}


	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getOrigen() {
		return origen;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setLogErrors(Boolean logErrors) {
		this.logErrors = logErrors;
	}

	public Boolean getLogErrors() {
		return logErrors;
	}


	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public void setIdToCargaMasivaIndAutoCot(BigDecimal idToCargaMasivaIndAutoCot) {
		this.idToCargaMasivaIndAutoCot = idToCargaMasivaIndAutoCot;
	}

	public BigDecimal getIdToCargaMasivaIndAutoCot() {
		return idToCargaMasivaIndAutoCot;
	}

	public void setCargaMasivaIndividualAutoCot(
			CargaMasivaIndividualAutoCot cargaMasivaIndividualAutoCot) {
		this.cargaMasivaIndividualAutoCot = cargaMasivaIndividualAutoCot;
	}

	public CargaMasivaIndividualAutoCot getCargaMasivaIndividualAutoCot() {
		return cargaMasivaIndividualAutoCot;
	}

	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}

	public Short getClaveTipo() {
		return claveTipo;
	}

	public void setCargaMasivaIndividualList(
			List<CargaMasivaIndividualAutoCot> cargaMasivaIndividualList) {
		this.cargaMasivaIndividualList = cargaMasivaIndividualList;
	}

	public List<CargaMasivaIndividualAutoCot> getCargaMasivaIndividualList() {
		return cargaMasivaIndividualList;
	}

	public void setCargaMasivaDetalleIndividualList(
			List<DetalleCargaMasivaIndAutoCot> cargaMasivaDetalleIndividualList) {
		this.cargaMasivaDetalleIndividualList = cargaMasivaDetalleIndividualList;
	}

	public List<DetalleCargaMasivaIndAutoCot> getCargaMasivaDetalleIndividualList() {
		return cargaMasivaDetalleIndividualList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}


	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegProducto(BigDecimal idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public BigDecimal getIdToNegProducto() {
		return idToNegProducto;
	}
	
	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setProductoList(List<NegocioProducto> productoList) {
		this.productoList = productoList;
	}

	public List<NegocioProducto> getProductoList() {
		return productoList;
	}

	public void setTiposPoliza(List<NegocioTipoPoliza> tiposPoliza) {
		this.tiposPoliza = tiposPoliza;
	}

	public List<NegocioTipoPoliza> getTiposPoliza() {
		return tiposPoliza;
	}
	
}
