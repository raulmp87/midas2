package mx.com.afirme.midas2.action.suscripcion.emision;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.poliza.seguroobligatorio.GeneraSeguroObligatorio;
import mx.com.afirme.midas2.amis.alertas.AlertaPortProxy;
import mx.com.afirme.midas2.amis.alertas.RespuestaAlerta;
import mx.com.afirme.midas2.amis.alertas.SapAmisRespuestaAlertas;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.DocumentationException;
import mx.com.afirme.midas2.clientesapi.dto.DocumentoDTO;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.clientesapi.dto.InterviewException;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAcciones;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAlertas;
import mx.com.afirme.midas2.dao.amis.alertas.RespuestaSapAmisAlerta;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.PolizaAnexa;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.operacionessapamis.SapAmisService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudDataEnTramiteService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import com.opensymphony.xwork2.Preparable;
/**
 * Tambien desde autos indiviual
 */
@Component
@Scope("prototype")
public class EmisionAction extends BaseAction implements Preparable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(EmisionAction.class);
	private static final String COTIZACION_INVALIDA = "Contizaci\u00f3n invalida";

	private BigDecimal idToCotizacion;
	private Long id;
	private CotizacionDTO cotizacion;
	private PolizaDTO poliza;
	private BigDecimal cotizacionId;
	private Boolean aplicaEndoso = false;
	private EntidadService entidadService;
	private CotizacionService cotizacionService;
	private EmisionService emisionService;
	private IncisoService incisoService;
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	private ClienteFacadeRemote clienteFacadeRemote;
	private List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private SolicitudDataEnTramiteService solicitudDataEnTramiteService;
	private EntrevistaDTO entrevista;
	private RenovacionMasivaService renovacionMasivaService;
	private SeguroObligatorioService seguroObligatorioService;
	private CalculoService calculoService;
	private ListadoService listadoService;
	private Short soloConsulta = 0;
	private String forma;
	private BigDecimal numeroInciso;
	private boolean compatilbeExplorador;
	private String nameUser;
	private String linkFortimax;
	List<SapAmisRespuestaAlertas> sapAmisRespuestaAlertas;
	List<RespuestaAlerta> respuesConsultaLinea;
	SapAmisService servicioSapAmis;
	List<RespuestaSapAmisAlerta> respuestaSapAmisAlertas;
	List<DocumentoDTO> documentacion;

	private boolean correoObligatorio;
	private String correo;
	private String nombreContratante;
	private String fechaVencimiento;
	private String codigoSeguridad;
	private CuentaPagoDTO cuentaPagoDTO;
	private static final String MENSAJE_AMEX_PAGO_ANUAL = "Solo forma de pago Anual/Moneda Nacional, mediante American Express.";
	private ClientesApiService clienteRest;
	private InputStream fileInputStream;
	private String jspForm;
	private String pdfDownload;
	private MensajeDTO mensajeDTO;

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}

	public List<SapAmisRespuestaAlertas> getSapAmisRespuestaAlertas() {
		return sapAmisRespuestaAlertas;
	}

	public void setSapAmisRespuestaAlertas(
			List<SapAmisRespuestaAlertas> sapAmisRespuestaAlertas) {
		this.sapAmisRespuestaAlertas = sapAmisRespuestaAlertas;
	}

	@Override
	public void prepare() throws Exception {

	}

	public void prepareEmitir() {
		if (idToCotizacion != null) {
			cotizacion = entidadService.findById(CotizacionDTO.class,
					idToCotizacion);
			cotizacion.getSolicitudDTO().setEmail(correo);
			// para listado excepciones
			id = idToCotizacion.longValue();
		}
	}

	/**
	 * Invoca al cliente REST para almacenar la entrevista dentro de la
	 * aplicación cliente único Carga nuevamente la cotización para regresar al
	 * flujo de la emisión
	 * 
	 * @return regresa al flujo de emision
	 * 
	 * 
	 * */

	public String guardarEntrevista() {

		clienteRest.createInterview(cotizacion.getIdToPersonaContratante()
				.longValue(), entrevista);
		cotizacion = entidadService.findById(CotizacionDTO.class,
				cotizacion.getIdToCotizacion());
		cotizacionId = cotizacion.getIdToCotizacion();
		soloConsulta = 0;

		return SUCCESS;
	}

	/**
	 * Invoca al cliente REST para almacenar la documentacion dentro de la
	 * aplicación cliente único Carga nuevamente la cotización para regresar al
	 * flujo de la emisión
	 * 
	 * @return regresa al flujo de emision
	 * 
	 * 
	 * */
	public String guardarDocumentacion() {

		List<DocumentoDTO> listToBorrar = new ArrayList<DocumentoDTO>();

		for (int i = 0; i < documentacion.size(); i++) {
			DocumentoDTO documento = documentacion.get(i);
			if (documento.getSeleccionado() == null
					|| !documento.getSeleccionado()) {
				listToBorrar.add(documento);
			}
		}
		documentacion.removeAll(listToBorrar);

		clienteRest.saveDocument(cotizacion.getIdToPersonaContratante()
				.longValue(), documentacion);
		cotizacion = entidadService.findById(CotizacionDTO.class,
				cotizacion.getIdToCotizacion());
		cotizacionId = cotizacion.getIdToCotizacion();
		soloConsulta = 0;

		return SUCCESS;
	}
	/**
	 * Invoca al cliente REST para generar el pdf referente a la entrevista capturada
	 * flujo de la emisión
	 * 
	 * @return regresa el redirect hacia el action de descarga del archivo pdf de la entrevista
	 * 
	 * 
	 * */
	public String getFormatoEntrevista() {
		cotizacion = entidadService.findById(CotizacionDTO.class,
				idToCotizacion);
		ResponseEntity<Long> resp = clienteRest.clienteUnificado(null, Long
				.valueOf(cotizacion.getIdToPersonaContratante().longValue()));
		byte[] temp = null;
		try {
			temp = clienteRest.createPDFInterviewApp(
					resp.getBody(),
					"A",
					"484",
					String.valueOf(cotizacion.getValorTotalPrimas().setScale(2,
							BigDecimal.ROUND_HALF_UP)), null);
			LOG.info("Cliente " + temp);
		} catch (Exception e) {

			LOG.info("Entrevista error : " + e.getMessage());

		}
		if (temp != null) {
			fileInputStream = new ByteArrayInputStream(temp);

		} else {
			fileInputStream = new ByteArrayInputStream(new byte[1]);
		}
		return "pdfEntrevista";
	}

	public String emitir() {
		if (cotizacion != null) {
			
			String validarPrima = "";
			ClienteUnicoDTO clienteUnico = null;
			ResponseEntity<Long> resp = null;
			//Validaciones Prima Mayor
			LOG.info("-- Validar pais y ocupación de cliente " + cotizacion.getIdToPersonaContratante().longValue());
			try {
				resp = clienteRest.clienteUnificado(null, cotizacion
						.getIdToPersonaContratante().longValue());
				
				
				clienteUnico = clienteRest.findClienteUnicoById(resp.getBody());
				
				ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
				validarPrima = entityPrima.getBody();
				
				LOG.info("-- Tipo de cliente " + clienteUnico.getClaveTipoPersonaString());
				
				if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("1")){
				
					if(clienteUnico.getOcupacionCNSF() == null || clienteUnico.getOcupacionCNSF().equals("")
							|| clienteUnico.getIdPaisNacimiento() == null || clienteUnico.getIdPaisNacimiento().equals("")){
						
						
						
						if(validarPrima.contains("true")){
						
							setMensajeError("confirm:message:Favor de complementar el País de Nacimiento y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+cotizacion.getIdToPersonaContratante().longValue()+","+idToCotizacion+")");
							
							return ERROR;
						}
					}
				}else if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("2")){
					
					if(clienteUnico.getCveGiroCNSF() == null || clienteUnico.getCveGiroCNSF().equals("")
							|| clienteUnico.getIdPaisConstitucion() == null || clienteUnico.getIdPaisConstitucion().equals("")){
												
						if(validarPrima.contains("true")){
							setMensajeError("confirm:message:Favor de complementar el País de Constituci\u00f3n y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+cotizacion.getIdToPersonaContratante().longValue()+","+idToCotizacion+")");
							
							return ERROR;
						}
					}
				}
				
				
			} catch (HttpStatusCodeException e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- emitirAgente():Validaciones Prima Mayor ", e);
				return ERROR;
			} catch (Exception e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- emitirAgente():Validaciones Prima Mayor ", e);
				return ERROR;
			}	
			
			//Se intentara insertar insertar el conducto de cobro de cada inciso en CLIENTE_COB
			try {
				if(clienteUnico != null){
					List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
					for(IncisoCotizacionDTO inciso : incisos){	
						if(inciso.getIdMedioPago()!=null&&inciso.getConductoCobro()!=null&&inciso.getInstitucionBancaria()!=null){
							ClienteGenericoDTO cuenta = clienteMigracion(inciso,clienteUnico);
							clienteFacadeRemote.guardarDatosCobranza(cuenta, usuarioService.getUsuarioActual().getNombreUsuario(),true);
						}
					}
				}
			} catch (Exception e) {
				super.setMensajeError(e.getMessage());
				return ERROR;
			}
			
			LOG.info("Conducto de cobros insertados Correctamente" );

			if (cotizacion.getIdMedioPago()!=null && cotizacion.getIdMedioPago().intValue() == CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS
					.valor()
					&& (!(cotizacion.getIdFormaPago().intValue() == 1) || cotizacion
							.getIdMoneda().intValue() != 484)) {

				super.setMensajeError(MENSAJE_AMEX_PAGO_ANUAL);
				return ERROR;
			}
			long start = System.currentTimeMillis();
			LOG.info("Entrando a emitir cotizacion : "
					+ cotizacion.getIdToCotizacion());
			try {
				emisionService.validacionPreviaRecibos(cotizacion
						.getIdToCotizacion());
				TerminarCotizacionDTO validacion = cotizacionService
						.validarEmisionCotizacion(cotizacion
								.getIdToCotizacion());

				if (validacion != null) {
					if (validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE
							.getEstatus().shortValue()) {
						super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
						return ERROR;
					} else if (validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES
							.getEstatus().shortValue()) {
						excepcionesList = validacion.getExcepcionesList();
						super.setMensajeError(validacion.getMensajeError());
						return "excepciones";
					} else {
						super.setMensajeError(validacion.getMensajeError());
						return ERROR;
					}
				}
				emisionService.setDatosTarjetaEmision(fechaVencimiento,
						codigoSeguridad);
				
				try {
					clienteRest.findInterViews(cotizacion
							.getIdToPersonaContratante().longValue());
				} catch (InterviewException e) {

					if(validarPrima.contains("true")){
						entrevista = new EntrevistaDTO();
						entrevista.setMoneda(cotizacion.getIdMoneda().intValue());
						entrevista.setPep(true);
						entrevista.setCuentaPropia(false);
						entrevista.setIdToCotizacion(cotizacion.getIdToCotizacion());

						if (clienteUnico!= null && clienteUnico.getIdToPersonaString().compareTo("1") == 0) {
							return "entrevistasPF";
						} else {
	
							entrevista.setPep(false);
							entrevista.setCuentaPropia(true);
	
							return "entrevistasPM";
						}
					}
				}
				try {

					clienteRest.getDocumentacionFaltante(resp.getBody());

				}

				catch (DocumentationException e) {

					ClienteUnicoDTO cliente = clienteRest
							.findClienteUnicoById(resp.getBody());
					List<DocumentoDTO> documentacionTemp = clienteRest
							.getDocumentos(e.getMessage());
					documentacion = new ArrayList<DocumentoDTO>();
					linkFortimax = clienteRest.getLinkFortmax(resp.getBody());
					List<DocumentoDTO> expediente = clienteRest
							.getDocumentacion(resp.getBody());
					for (DocumentoDTO documentoTemp : documentacionTemp) {
						DocumentoDTO documento = new DocumentoDTO();
						for (DocumentoDTO docuExp : expediente) {
							if (documentoTemp.getId().toString()
									.compareTo(docuExp.getDocumentoId()) == 0) {
								if (docuExp.getFechaVigencia() != null) {
									String[] temp = docuExp.getFechaVigencia()
											.substring(0, 10).split("-");
									if (temp != null && temp.length == 3) {
										documento
												.setFechaVigencia(temp[2] + "/"
														+ temp[1] + "/"
														+ temp[0]);

									}

								}

								documento.setDescripcion(docuExp
										.getDescripcion());
								documento.setDocumentoId(docuExp
										.getDocumentoId());
								documento.setSeleccionado(true);

								break;
							}

						}
						documento.setNombre(documentoTemp.getNombre());
						documento.setRequerido(documentoTemp.getRequerido());
						documento.setDocumentoId(String.valueOf(documentoTemp
								.getId()));
						documentacion.add(documento);

					}

					return "documentacion";

				}

				Map<String, String> mensajeEmision = emisionService.emitir(
						cotizacion, false);
				String icono = mensajeEmision.get(PolizaDTO.ICONO_EMISION);
				String mensaje = mensajeEmision.get(PolizaDTO.MENSAJE_EMISON);
				if (icono.equals(PolizaDTO.ICONO_CONFIRM)) {
					// Se borran registros de datos de la solicitud si fuera una
					// solicitud de poliza en tramite
					solicitudDataEnTramiteService.deleteBySolicitud(cotizacion
							.getIdToCotizacion());
					saveDocumentoDigitalSolicitud();

					String idPoliza = mensajeEmision
							.get(PolizaDTO.IDPOLIZA_EMISON);
					poliza = entidadService.findById(PolizaDTO.class,
							BigDecimal.valueOf(Long.valueOf(idPoliza)));
					// Crea poliza seguro obligatorio
					String mensajeSO = this.generarSeguroObligatorio();
					super.setNextFunction(emisionService
							.setFunctionPrintPoliza(BigDecimal.valueOf(Long
									.valueOf(idPoliza))));
					super.setMensajeExitoPersonalizado("confirm:message:"
							+ mensaje + " "
							+ poliza.getNumeroPolizaFormateada() + mensajeSO
							+ " \u00BFDesea imprimir la poliza ahora\u003F");
					LOG.info("Saliendo de emitirCotizacion idCotizaci\u00f3n : "
							+ cotizacion.getIdToCotizacion()
							+ ((System.currentTimeMillis() - start) / 1000.0));

					// se agrega validación PEPS
					if (cotizacion.getIdToPersonaContratante() != null) {

						clienteRest.validatePeps(cotizacion
								.getIdToPersonaContratante().longValue(),
								poliza.getNumeroPolizaFormateada());

					}
					if (cotizacion.getIdToPersonaAsegurado() != null) {

						clienteRest.validatePeps(cotizacion
								.getIdToPersonaAsegurado().longValue(), poliza
								.getNumeroPolizaFormateada());

					}

					// correo
					enviarEmail();
					return SUCCESS;
				} else if (icono.equals(PolizaDTO.ICONO_ERROR)) {
					super.setMensajeError(mensaje);
					return ERROR;
				}
				return SUCCESS;
			} catch (RuntimeException e) {
				LOG.info("-- ERROR: " + e.getMessage());
				super.setMensajeError(MENSAJE_ERROR_GENERAL);
				return ERROR;
			}

		}
		super.setMensajeError(COTIZACION_INVALIDA);
		return ERROR;
	}

	private ClienteGenericoDTO clienteMigracion(IncisoCotizacionDTO inciso, ClienteDTO clienteDto) {
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		cliente.setIdCliente(inciso.getIdClienteCob());
		cliente.setIdConductoCobranza(inciso.getIdConductoCobroCliente());
		cliente.setNombreTarjetaHabienteCobranza(inciso.getIncisoAutoCot().getNombreAsegurado());
		cliente.setEmailCobranza("");
		cliente.setTelefonoCobranza("");
		cliente.setNombreCalleCobranza(clienteDto.getNombreCalle());
		cliente.setCodigoPostalCobranza(clienteDto.getCodigoPostal());
		cliente.setNombreColoniaCobranza(clienteDto.getNombreColonia());
		cliente.setIdTipoConductoCobro(inciso.getIdMedioPago().intValue());
		
		List<BancoEmisorDTO> bancos = bancoEmisorFacade.findAll();
		Integer idBancoCobranza = null;
		for(BancoEmisorDTO banco: bancos){
			if(inciso.getInstitucionBancaria().equals(banco.getNombreBanco())){
				idBancoCobranza =  banco.getIdBanco();
			}
		}
		cliente.setIdBancoCobranza(new BigDecimal(idBancoCobranza));
		cliente.setIdTipoTarjetaCobranza(inciso.getTipoTarjeta());			
		cliente.setNumeroTarjetaCobranza(bancoEmisorFacade.desEncriptaDatos(inciso.getNumeroTarjetaClave()));
		//Concidicon para cuando el conducto de cobro es del tipo tarjeta de credito
		if (inciso.getCodigoSeguridad() != null&&inciso.getFechaVencimiento()!=null) {
			cliente.setCodigoSeguridadCobranza(bancoEmisorFacade.desEncriptaDatos(inciso.getCodigoSeguridad()));
			cliente.setFechaVencimientoTarjetaCobranza(bancoEmisorFacade.desEncriptaDatos(inciso.getFechaVencimiento()).replace("/", ""));
		}	
		cliente.setTipoPromocion("");
		cliente.setDiaPagoTarjetaCobranza(new BigDecimal(0));				
		cliente.setRfcCobranza(clienteDto.getCodigoRFC());
		
		return cliente;
	}
	
	public void prepareEmitirAgente() {
		if (idToCotizacion != null) {
			cotizacion = entidadService.findById(CotizacionDTO.class,
					idToCotizacion);
			// para listado excepciones
			id = idToCotizacion.longValue();
		}
	}

	public String emitirAgente() {

		if (cotizacion != null) {
			
			String validarPrima = "";
			ClienteUnicoDTO clienteUnico = null;
			ResponseEntity<Long> resp = null;
			//Validaciones Prima Mayor
			LOG.info("-- Validar pais y ocupación de cliente " + cotizacion.getIdToPersonaContratante().longValue());
			try {
				resp = clienteRest.clienteUnificado(null, cotizacion
						.getIdToPersonaContratante().longValue());
				
				
				clienteUnico = clienteRest.findClienteUnicoById(resp.getBody());
				
				ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
				validarPrima = entityPrima.getBody();
				
				LOG.info("-- Tipo de cliente " + clienteUnico.getClaveTipoPersonaString());
				
				if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("1")){
				
					if(clienteUnico.getOcupacionCNSF() == null || clienteUnico.getOcupacionCNSF().equals("")
							|| clienteUnico.getIdPaisNacimiento() == null || clienteUnico.getIdPaisNacimiento().equals("")){
						
						if(validarPrima.contains("true")){
						
							setMensajeError("confirm:message:Favor de complementar el País de Nacimiento y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+cotizacion.getIdToPersonaContratante().longValue()+","+idToCotizacion+")");
							
							return ERROR;
						}
					}
				}else if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("2")){
					
					if(clienteUnico.getCveGiroCNSF() == null || clienteUnico.getCveGiroCNSF().equals("")
							|| clienteUnico.getIdPaisConstitucion() == null || clienteUnico.getIdPaisConstitucion().equals("")){
						
						
						if(validarPrima.contains("true")){
							setMensajeError("confirm:message:Favor de complementar el País de Constituci\u00f3n y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+cotizacion.getIdToPersonaContratante().longValue()+","+idToCotizacion+")");
							
							return ERROR;
						}
					}
				}
				
				
			} catch (HttpStatusCodeException e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- emitirAgente():Validaciones Prima Mayor ", e);
				return ERROR;
			} catch (Exception e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- emitirAgente():Validaciones Prima Mayor ", e);
				return ERROR;
			}	
			
			if (cotizacion.getIdMedioPago().intValue() == CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS
					.valor()
					&& (!(cotizacion.getIdFormaPago().intValue() == 1) || cotizacion
							.getIdMoneda().intValue() != 484)) {

				super.setMensajeError(MENSAJE_AMEX_PAGO_ANUAL);
				return ERROR;
			}

			try {
				clienteRest.findInterViews(cotizacion
						.getIdToPersonaContratante().longValue());
			} catch (InterviewException e) {

				if(validarPrima.contains("true")){

					entrevista = new EntrevistaDTO();
					entrevista.setMoneda(cotizacion.getIdMoneda().intValue());
					entrevista.setPep(true);
					entrevista.setCuentaPropia(false);
					entrevista.setIdToCotizacion(cotizacion.getIdToCotizacion());
					if (clienteUnico != null && clienteUnico.getIdToPersonaString().compareTo("1") == 0) {
						return "entrevistasPF";
					} else {
						entrevista.setPep(false);
						entrevista.setCuentaPropia(true);
						return "entrevistasPM";
					}
				}
			}
			try {

				clienteRest.getDocumentacionFaltante(resp.getBody());

			}

			catch (DocumentationException e) {

				// Se prepara la pantalla de captura de documentos
				ClienteUnicoDTO cliente = clienteRest.findClienteUnicoById(resp
						.getBody());
				List<DocumentoDTO> documentacionTemp = clienteRest
						.getDocumentos(e.getMessage());
				documentacion = new ArrayList<DocumentoDTO>();
				linkFortimax = clienteRest.getLinkFortmax(resp.getBody());
				List<DocumentoDTO> expediente = clienteRest
						.getDocumentacion(resp.getBody());
				for (DocumentoDTO documentoTemp : documentacionTemp) {
					DocumentoDTO documento = new DocumentoDTO();
					for (DocumentoDTO docuExp : expediente) {
						if (documentoTemp.getId().toString()
								.compareTo(docuExp.getDocumentoId()) == 0) {
							if (docuExp.getFechaVigencia() != null) {
								String[] temp = docuExp.getFechaVigencia()
										.substring(0, 10).split("-");
								if (temp != null && temp.length == 3) {
									documento.setFechaVigencia(temp[2] + "/"
											+ temp[1] + "/" + temp[0]);

								}

							}

							documento.setDescripcion(docuExp.getDescripcion());
							documento.setDocumentoId(docuExp.getDocumentoId());
							documento.setSeleccionado(true);

							break;
						}

					}
					documento.setNombre(documentoTemp.getNombre());
					documento.setRequerido(documentoTemp.getRequerido());
					documento.setDocumentoId(String.valueOf(documentoTemp
							.getId()));
					documentacion.add(documento);

				}

				return "documentacion";

			}

			try {
				TerminarCotizacionDTO validacion = cotizacionService
						.validarEmisionCotizacion(cotizacion
								.getIdToCotizacion());

				if (validacion != null) {
					if (validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE
							.getEstatus().shortValue()) {

						super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
						return ERROR;
					} else if (validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES
							.getEstatus().shortValue()) {

						excepcionesList = validacion.getExcepcionesList();
						super.setMensajeError(validacion.getMensajeError());
						return "excepciones";
					} else {
						super.setMensajeError(validacion.getMensajeError());
						return ERROR;
					}
				}
				if (cuentaPagoDTO != null
						&& cotizacion.getIdMedioPago().intValue() == CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS
								.valor()) {
					fechaVencimiento = cuentaPagoDTO.getFechaVencimiento();
					codigoSeguridad = cuentaPagoDTO.getCodigoSeguridad();
					emisionService.setDatosTarjetaEmision(fechaVencimiento,
							codigoSeguridad);
				}

				Map<String, String> mensajeEmision = emisionService.emitir(
						cotizacion, false);
				String icono = mensajeEmision.get(PolizaDTO.ICONO_EMISION);
				String mensaje = mensajeEmision.get(PolizaDTO.MENSAJE_EMISON);

				if (icono.equals(PolizaDTO.ICONO_CONFIRM)) {
					String idPoliza = mensajeEmision
							.get(PolizaDTO.IDPOLIZA_EMISON);
					poliza = entidadService.findById(PolizaDTO.class,
							BigDecimal.valueOf(Long.valueOf(idPoliza)));
					// Crea poliza seguro obligatorio
					String mensajeSO = this.generarSeguroObligatorio();
					super.setNextFunction(emisionService
							.setFunctionPrintPoliza(BigDecimal.valueOf(Long
									.valueOf(idPoliza))));

					super.setMensajeExitoPersonalizado("confirm:message:"
							+ mensaje + " "
							+ poliza.getNumeroPolizaFormateada() + mensajeSO
							+ " \u00BFDesea imprimir la poliza ahora\u003F");
					enviarEmail();
					// se agrega validación PEPS
					if (cotizacion.getIdToPersonaContratante() != null) {

						clienteRest.validatePeps(cotizacion
								.getIdToPersonaContratante().longValue(),
								poliza.getNumeroPolizaFormateada());

					}
					if (cotizacion.getIdToPersonaAsegurado() != null) {

						clienteRest.validatePeps(cotizacion
								.getIdToPersonaAsegurado().longValue(), poliza
								.getNumeroPolizaFormateada());

					}

					return SUCCESS;
				} else if (icono.equals(PolizaDTO.ICONO_ERROR)) {
					super.setMensajeError(mensaje);
					return ERROR;
				}
				return SUCCESS;
			} catch (RuntimeException e) {
				super.setMensajeError(MENSAJE_ERROR_GENERAL);
				LogDeMidasWeb.log("MENSAJE_ERROR_GENERAL", Level.INFO, null);
				return ERROR;
			}
		}
		super.setMensajeError(COTIZACION_INVALIDA);
		LogDeMidasWeb.log("COTIZACION_INVALIDA", Level.INFO, null);
		return ERROR;
	}

	public String editar() {
		return SUCCESS;
	}

	private String generarSeguroObligatorio() {
		String mensajeSO = "";
		try {
			if (seguroObligatorioService
					.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_EMISION)) {
				GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
				generaSeguroObligatorio
						.setRenovacionMasivaService(renovacionMasivaService);
				generaSeguroObligatorio
						.setSeguroObligatorioService(seguroObligatorioService);
				generaSeguroObligatorio.setListadoService(listadoService);
				generaSeguroObligatorio.setCalculoService(calculoService);
				generaSeguroObligatorio.creaPolizaSeguroObligatorio(poliza,
						PolizaAnexa.TIPO_AUTOMATICA);

				PolizaDTO polizaAnexa = seguroObligatorioService
						.obtienePolizaAnexa(poliza.getIdToPoliza());
				if (polizaAnexa != null) {
					mensajeSO = " \nSe creo poliza SO ("
							+ polizaAnexa.getNumeroPolizaFormateada() + ")\n";
				}
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return mensajeSO;
	}

	private void saveDocumentoDigitalSolicitud() {
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("solicitudDTO.idToSolicitud", cotizacion.getSolicitudDTO()
				.getIdToSolicitud());
		params.put("esCartaCobertura", 1);
		List<DocumentoDigitalSolicitudDTO> documentosDigitalesSolicitudDTO = entidadService
				.findByProperties(DocumentoDigitalSolicitudDTO.class, params);
		if (documentosDigitalesSolicitudDTO != null
				&& !documentosDigitalesSolicitudDTO.isEmpty()) {
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO = documentosDigitalesSolicitudDTO
					.get(0);
			documentoDigitalSolicitudDTO.setFiles(null);
			// Se borran los bytes del archivo carta cobertura
			entidadService.save(documentoDigitalSolicitudDTO);
		}
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@Autowired
	@Qualifier("emisionServiceEJB")
	public void setEmisionService(EmisionService emisionService) {
		this.emisionService = emisionService;
	}
	
	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("bancoEmisorFacadeRemoteEJB")
	public void setBancoEmisorFacade(BancoEmisorFacadeRemote bancoEmisorFacade) {
		this.bancoEmisorFacade = bancoEmisorFacade;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("solicitudDataEnTramiteServiceEJB")
	public void setSolicitudDataEnTramiteService(
			SolicitudDataEnTramiteService solicitudDataEnTramiteService) {
		this.solicitudDataEnTramiteService = solicitudDataEnTramiteService;
	}

	@Autowired
	@Qualifier("renovacionMasivaServiceEJB")
	public void setRenovacionMasivaService(
			RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}

	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(
			SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}

	public PolizaDTO getPoliza() {
		return poliza;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public void setExcepcionesList(
			List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAplicaEndoso() {
		return aplicaEndoso;
	}

	public void setAplicaEndoso(Boolean aplicaEndoso) {
		this.aplicaEndoso = aplicaEndoso;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public boolean isCompatilbeExplorador() {
		return compatilbeExplorador;
	}

	public void setCompatilbeExplorador(boolean compatilbeExplorador) {
		this.compatilbeExplorador = compatilbeExplorador;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public SapAmisService getServicioSapAmis() {
		return servicioSapAmis;
	}

	@Autowired
	@Qualifier("sapAmisServiceEJB")
	public void setServicioSapAmis(SapAmisService servicioSapAmis) {
		this.servicioSapAmis = servicioSapAmis;
	}

	public List<RespuestaAlerta> getRespuesConsultaLinea() {
		return respuesConsultaLinea;
	}

	public void setRespuesConsultaLinea(
			List<RespuestaAlerta> respuesConsultaLinea) {
		this.respuesConsultaLinea = respuesConsultaLinea;
	}

	@Autowired
	@Qualifier("mailServiceEJB")
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	private void enviarEmail() {
		try {
			String idCotizacion = poliza.getCotizacionDTO().getIdToCotizacion()
					+ "";
			sapAmisRespuestaAlertas = new ArrayList<SapAmisRespuestaAlertas>();
			AlertaPortProxy proxy = new AlertaPortProxy();
			respuesConsultaLinea = new ArrayList<RespuestaAlerta>();

			StringBuilder consultaNuemeroSerie = new StringBuilder("");
			for (int x = 0; x < poliza.getCotizacionDTO()
					.getIncisoCotizacionDTOs().size(); x++) {
				consultaNuemeroSerie.append(
						poliza.getCotizacionDTO().getIncisoCotizacionDTOs()
								.get(x).getIncisoAutoCot().getNumeroSerie())
						.append("|");
			}
			EnvioConsultaAlertas eca = servicioSapAmis
					.getInfoEnvioConsultaAlertas(consultaNuemeroSerie
							.substring(0, consultaNuemeroSerie.length() - 1));
			if (eca != null) {
				ArrayList<EnvioConsultaAcciones> listaAlertas = new ArrayList<EnvioConsultaAcciones>();
				for (int x = 0; x < eca.getVin().length; x++) {
					try {
						respuesConsultaLinea = proxy.consultaAlerta(
								eca.getUser(), eca.getPass(), eca.getVin()[x]);
						for (int y = 0; y < respuesConsultaLinea.size(); y++) {
							RespuestaSapAmisAlerta rsaa = new RespuestaSapAmisAlerta();
							rsaa.setAlerta(respuesConsultaLinea.get(y)
									.getDescripcion());
							rsaa.setSistema(respuesConsultaLinea.get(y)
									.getSistema());
							rsaa.setVin(eca.getVin()[x]);
							servicioSapAmis.getAlertasSapAmisEmision(rsaa,
									poliza);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						LogDeMidasWeb.log("Error de comunicacion Ws | ex: "
								+ ex.getMessage(), Level.INFO, null);
						respuesConsultaLinea = null;
					}
				}
			}
		} catch (Exception ex) {
			LogDeMidasWeb.log(ex.getMessage(), Level.INFO, null);
		}
	}

	public boolean isCorreoObligatorio() {
		return correoObligatorio;
	}

	public void setCorreoObligatorio(boolean correoObligatorio) {
		this.correoObligatorio = correoObligatorio;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public CuentaPagoDTO getCuentaPagoDTO() {
		return cuentaPagoDTO;
	}

	public void setCuentaPagoDTO(CuentaPagoDTO cuentaPagoDTO) {
		this.cuentaPagoDTO = cuentaPagoDTO;
	}

	public EntrevistaDTO getEntrevista() {
		return entrevista;
	}

	public void setEntrevista(EntrevistaDTO entrevista) {
		this.entrevista = entrevista;
	}

	public BigDecimal getCotizacionId() {
		return cotizacionId;
	}

	public void setCotizacionId(BigDecimal cotizacionId) {
		this.cotizacionId = cotizacionId;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public List<DocumentoDTO> getDocumentacion() {
		return documentacion;
	}

	public void setDocumentacion(List<DocumentoDTO> documentacion) {
		this.documentacion = documentacion;
	}

	public String getLinkFortimax() {
		return linkFortimax;
	}

	public void setLinkFortimax(String linkFortimax) {
		this.linkFortimax = linkFortimax;
	}

	public String getJspForm() {
		return jspForm;
	}

	public void setJspForm(String jspForm) {
		this.jspForm = jspForm;
	}

	public String getPdfDownload() {
		return pdfDownload;
	}

	public void setPdfDownload(String pdfDownload) {
		this.pdfDownload = pdfDownload;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}
}