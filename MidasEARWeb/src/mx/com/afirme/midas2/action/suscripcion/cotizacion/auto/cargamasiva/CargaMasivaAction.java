package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.cargamasiva;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasiva;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CargaMasivaAction extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private BigDecimal idToControlArchivo;
	private BigDecimal idToCargaMasivaAutoCot;
	private String origen;
	private CotizacionDTO cotizacion;
	private ControlArchivoDTO controlArchivo;
	private CargaMasivaAutoCot cargaMasivaAutoCot;
	private List<CargaMasivaAutoCot> cargaMasivaList;
	private List<DetalleCargaMasivaAutoCot> cargaMasivaDetalleList;
	private Boolean logErrors = false;
	private Short tipoCarga = 1;
	private Short tipoRegreso = 1;
	private BigDecimal idToDetalleCargaMasivaAutoCot;
	
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	
	private EntidadService entidadService;
	private CargaMasivaService cargaMasivaService;
	private NegocioCondicionEspecialService negocioCondicionEspecialService;
	private CondicionEspecialService condicionEspecialService;
	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	private ListadoService listadoService;
	private BancoMidasService bancoMidasService;
	private ClienteFacadeRemote clienteFacadeRemote;
	
	private static final Logger LOG = Logger.getLogger(CargaMasivaAction.class);
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("cargaMasivaServiceEJB")
	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	
	@Autowired
	@Qualifier("negocioCondicionEspecialServiceEJB")
	public void setNegocioCondicionEspecialService(NegocioCondicionEspecialService negocioCondicionEspecialService) {
		this.negocioCondicionEspecialService = negocioCondicionEspecialService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialCotizacionServiceEJB")
	public void setCondicionEspecialCotizacionService(
			CondicionEspecialCotizacionService condicionEspecialCotizacionService) {
		this.condicionEspecialCotizacionService = condicionEspecialCotizacionService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("bancoMidasServiceEJB")
	public void setBancoMidasService(BancoMidasService bancoMidasService) {
		this.bancoMidasService = bancoMidasService;
	}

	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Override
	public void prepare() throws Exception {
		if(id != null){
			cotizacion = entidadService.findById(CotizacionDTO.class, id);
		}
	}
	
	public void prepareMostrarCarga(){

	}
	
	public String mostrarCarga(){		
		return SUCCESS;
	}
	
	public void prepareListarCarga(){
		if(id != null){
			cargaMasivaList = entidadService.findByProperty(CargaMasivaAutoCot.class, "idToCotizacion", id);
			//Sort
			if(cargaMasivaList != null && !cargaMasivaList.isEmpty()){
				Collections.sort(cargaMasivaList, 
						new Comparator<CargaMasivaAutoCot>() {				
							public int compare(CargaMasivaAutoCot n1, CargaMasivaAutoCot n2){
								return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
							}
						});
			}
		}else{
			cargaMasivaList = new ArrayList<CargaMasivaAutoCot>(1);
		}
	}
	
	public String listarCarga(){
		return SUCCESS;
	}
	
	public String descargarPlantilla(){
		GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
		plantilla.setNegocioCondicionEspecialService(negocioCondicionEspecialService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
				plantillaInputStream = plantilla.generaPlantillaCargaMasiva(cotizacion);
				setFileName("CargaMasivaCot" + id + ".xls");	
			}
			if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
				plantillaInputStream = plantilla.obtienePlantillaCargaMasivaCompl(cotizacion,listadoService);
				setFileName("CargaMasivaCompCot" + id + ".xls");	
			}
			//plantillaInputStream = plantilla.obtienePlantillaCargaMasivaCompl(cotizacion);
			setFileName("CargaMasivaCompCot" + id + ".xls");
			contentType = "application/vnd.ms-excel";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String descargarLogErrores(){
		GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			FileInputStream plantillaBytes = plantilla.descargaLogErrores(idToControlArchivo);
			
			if(plantillaBytes == null){
				return ERROR;
			}
			
			plantillaInputStream = plantillaBytes;
			contentType = "application/text";
			setFileName("LogErroresCargaMasivaCot" + id + ".txt");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;		
	}
	
	public String descargarLogErroresInciso(){
		GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
		try {
			String mensajeErrorInciso = "";
			if(idToDetalleCargaMasivaAutoCot != null){
				DetalleCargaMasivaAutoCot detalle = entidadService.findById(DetalleCargaMasivaAutoCot.class, idToDetalleCargaMasivaAutoCot);
				mensajeErrorInciso = detalle.getMensajeError();
			}
			plantilla.setCargaMasivaService(cargaMasivaService);
			plantillaInputStream = plantilla.descargaLogErroresInciso(mensajeErrorInciso);
			contentType = "application/text";
			setFileName("LogErroresInciso" + idToDetalleCargaMasivaAutoCot + ".txt");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;		
	}
	
	public void prepareValidaCarga(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Almacena informacion del archivo como el estatus y nombre
	 * */
	public String validaCarga(){
		long start = System.currentTimeMillis();
		LOG.info("Inicio de validaCarga");
		if(controlArchivo != null){
			// Trae información del archivo
			CargaMasivaAutoCot cargaMasiva = cargaMasivaService.guardaCargaMasiva(id, controlArchivo, this.getUsuarioActual().getNombreUsuario(), tipoCarga);
			
			GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
			plantilla.setNegocioCondicionEspecialService(negocioCondicionEspecialService);
			plantilla.setCondicionEspecialService(condicionEspecialService);
			plantilla.setCondicionEspecialCotizacionService(condicionEspecialCotizacionService);
			
			try {
				//Guardar informacion complementaria de los incisos
				if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
					plantilla.validaCargaMasivaComplemento(cotizacion, controlArchivo.getIdToControlArchivo(), tipoCarga,bancoMidasService,clienteFacadeRemote,listadoService);
				}
				plantilla.setCargaMasivaService(cargaMasivaService);
				if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
					plantilla.validaCargaMasiva(cotizacion, idToControlArchivo, tipoCarga);
				}
				cargaMasivaService.guardaDetalleCargaMasiva(cargaMasiva, plantilla.getDetalleCargaMasivaAutoCotList());

				setLogErrors(plantilla.isHasLogErrors());
				if(!plantilla.isHasLogErrors()){
					if(tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL)){
						cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_TERMINADO);
					}
					if(tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_SCHEDULE)){
						cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_PENDIENTE);
					}
				}else{
					cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_CON_ERROR);
				}
				entidadService.save(cargaMasiva);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		LOG.info("validaCarga elapsed time idCotizacion: " + id + " " 
				+ " controlArchivo: " + controlArchivo + " " + ((System.currentTimeMillis() - start) / 1000.0));
		return SUCCESS;
	}
	
	public void prepareVerDetalleCarga(){
		if(idToCargaMasivaAutoCot != null){
			cargaMasivaAutoCot = entidadService.findById(CargaMasivaAutoCot.class, idToCargaMasivaAutoCot);
			if(cargaMasivaAutoCot.getClaveEstatus().equals(CargaMasivaAutoCot.ESTATUS_CON_ERROR)){
				setLogErrors(true);
			}
		}
	}
	
	public String verDetalleCarga(){
		return SUCCESS;
	}
	
	public void prepareListarDetalleCarga(){
		if(idToCargaMasivaAutoCot != null){
			cargaMasivaDetalleList = entidadService.findByProperty(DetalleCargaMasivaAutoCot.class, "idToCargaMasivaAutoCot", idToCargaMasivaAutoCot);
			//Sort
			if(cargaMasivaDetalleList != null && !cargaMasivaDetalleList.isEmpty()){
				try{
				Collections.sort(cargaMasivaDetalleList, 
						new Comparator<DetalleCargaMasivaAutoCot>() {				
							public int compare(DetalleCargaMasivaAutoCot n1, DetalleCargaMasivaAutoCot n2){
								if( n1.getNumeroInciso() == null || n2.getNumeroInciso() == null){
									return -1;
								}else{
									return n1.getNumeroInciso().compareTo(n2.getNumeroInciso());
								}
							}
						});
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}else{
			cargaMasivaDetalleList = new ArrayList<DetalleCargaMasivaAutoCot>(1);
		}
	}
	
	public String listarDetalleCarga(){
		return SUCCESS;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getOrigen() {
		return origen;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setCargaMasivaList(List<CargaMasivaAutoCot> cargaMasivaList) {
		this.cargaMasivaList = cargaMasivaList;
	}

	public List<CargaMasivaAutoCot> getCargaMasivaList() {
		return cargaMasivaList;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setLogErrors(Boolean logErrors) {
		this.logErrors = logErrors;
	}

	public Boolean getLogErrors() {
		return logErrors;
	}

	public void setCargaMasivaDetalleList(List<DetalleCargaMasivaAutoCot> cargaMasivaDetalleList) {
		this.cargaMasivaDetalleList = cargaMasivaDetalleList;
	}

	public List<DetalleCargaMasivaAutoCot> getCargaMasivaDetalleList() {
		return cargaMasivaDetalleList;
	}

	public void setCargaMasivaAutoCot(CargaMasivaAutoCot cargaMasivaAutoCot) {
		this.cargaMasivaAutoCot = cargaMasivaAutoCot;
	}

	public CargaMasivaAutoCot getCargaMasivaAutoCot() {
		return cargaMasivaAutoCot;
	}

	public void setIdToCargaMasivaAutoCot(BigDecimal idToCargaMasivaAutoCot) {
		this.idToCargaMasivaAutoCot = idToCargaMasivaAutoCot;
	}

	public BigDecimal getIdToCargaMasivaAutoCot() {
		return idToCargaMasivaAutoCot;
	}

	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoRegreso(Short tipoRegreso) {
		this.tipoRegreso = tipoRegreso;
	}

	public Short getTipoRegreso() {
		return tipoRegreso;
	}

	public void setIdToDetalleCargaMasivaAutoCot(
			BigDecimal idToDetalleCargaMasivaAutoCot) {
		this.idToDetalleCargaMasivaAutoCot = idToDetalleCargaMasivaAutoCot;
	}

	public BigDecimal getIdToDetalleCargaMasivaAutoCot() {
		return idToDetalleCargaMasivaAutoCot;
	}
}
