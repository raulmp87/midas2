package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.cobranza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.agentes.CotizadorAgentesAction;
import mx.com.afirme.midas2.domain.cobranza.cliente.CotizacionCobranza;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.cliente.CotizacionCobranzaService;
import mx.com.afirme.midas2.service.negocio.cliente.NegocioClienteService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mx.com.afirme.midas2.service.negocio.producto.mediopago.NegocioMedioPagoService;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;

import com.opensymphony.xwork2.Preparable;

@Namespace("/suscripcion/cotizacion/auto/cabranza")
@Component
@Scope("prototype")
@Results({ @Result(name = "input", location = "/jsp/error.jsp") })
public class CobranzaAction extends BaseAction implements Preparable {

	private static final Logger LOG = Logger.getLogger(CobranzaAction.class);
	
	/** Id de medio de pago agente o efectivo */
	private static final int MEDIOPAGO_AGENTE = 15;
	private static final String MEDIOPAGO_EFECTIVO = "EFECTIVO";
	private static final long serialVersionUID = 1L;
	private static final int subSringMessage = 27;
	private static final String MENSAJE_AMEX_PAGO_ANUAL = "Solo forma de pago Anual/Moneda Nacional, mediante American Express.";
	private BigDecimal idToCotizacion;
	private CotizacionDTO cotizacionDTO;
	private NegocioClienteService negocioClienteService;
	private ClienteDTO clienteDTO;
	private ClienteGenericoDTO clienteGenericoDTO;
	private Domicilio domicilio;
	private List<NegocioProducto> negocioProductos;
	private List<MedioPagoDTO> medioPagoDTOs;
	private Map<String, String> estadoMap;
	private Map<String, String> municipioMap;
	private Map<String, String> coloniasMap;
	private Map<String, String> paisesMap;
	private Map<Long, String> conductosDeCobro; 
	private ListadoService listadoService;
	private EntidadService entidadService;
	private DomicilioFacadeRemote domicilioFacadeRemote;
	private ClienteFacadeRemote clienteFacadeRemote;
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	private String idConductoCobro;
	private Integer idMedioPago;
	private List<BancoEmisorDTO> bancos;
	private Boolean datosIguales;
	private PolizaPagoService pagoService;
	private CuentaPagoDTO cuentaPagoDTO;
	private String tipoTarjeta;
	private String anio;
	private String mes;
	private Map<Boolean,String> mapDatosIguales = new HashMap<Boolean, String>(1);
	private Short soloConsulta = 0;
	private String tabActiva;
	private Integer idBanco;
	private Map<String, String> promociones;
	private CotizacionService cotizacionService;
	private EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO;
	private CalculoService calculoService;
	private Boolean cotizadorAgente = false;
	private Boolean newCotizadorAgente = false;
	private int forma;
	private BigDecimal idToPersonaContratante;
	
	private BigDecimal numeroInciso;
	private boolean compatilbeExplorador;
	
	//for AS360 project
	private Long conductoCobroId;
	private String usuarioTieneRolEjecutivo;
	private Map <String, String> listaDeEjecutivos;
	private String ejecutivoColoca;
	
	private String clati;

	@Autowired
	@Qualifier("CotizacionCobranzaEJB")
	private CotizacionCobranzaService cotizacionCobranzaService;
	
	@Autowired
	@Qualifier("negocioMedioPagoServiceEJB")
	private NegocioMedioPagoService negocioMedioPagoService;

	@Override
	public void prepare() throws Exception {
		bancos = bancoEmisorFacade.findAll();
	}

	/**
	 * Saca los medios de pago de un negocio
	 */
	public void prepareMostrar() {
		getCotizacion();
		if (cotizacionDTO != null) {

			final ResumenCostosDTO resumen = calculoService.obtenerResumenCotizacion(cotizacionDTO, true);
			cotizacionDTO.setPrimaNetaTotal(resumen.getPrimaNetaCoberturas());
			try{
				esquemaPagoCotizacionDTO = cotizacionService.findEsquemaPagoCotizacion(
						cotizacionDTO.getFechaInicioVigencia(),
						cotizacionDTO.getFechaInicioVigencia(),
						cotizacionDTO.getFechaFinVigencia(),
						Integer.valueOf(cotizacionDTO.getIdFormaPago().toString()),
						BigDecimal.valueOf(cotizacionDTO.getPrimaNetaTotal()),
						BigDecimal.valueOf(cotizacionDTO.getValorDerechosUsuario()*cotizacionDTO.getIncisoCotizacionDTOs().size()),
						BigDecimal.valueOf(cotizacionDTO.getPorcentajeIva()),
						Integer.valueOf(cotizacionDTO.getIdMoneda().toString()),
						Double.valueOf(cotizacionDTO.getPrimaNetaTotal().doubleValue() * (cotizacionDTO.getPorcentajePagoFraccionado() / 100)),
						true);
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
			}
			if (cotizacionDTO.getIdConductoCobroCliente() != null) {
				try {
					cuentaPagoDTO = pagoService.obtenerConductoCobro(idToCotizacion);
					if (cuentaPagoDTO != null){
						setIdMedioPago((int)cuentaPagoDTO.getTipoConductoCobro().valor());
						setDatosIguales(true);
						setIdConductoCobro(cotizacionDTO.getIdConductoCobroCliente().toString());
					}
				} catch (Exception e) {
					super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
					LOG.error(e.getMessage(), e);
				}
			}else if(cotizacionDTO.getIdMedioPago() != null){
				setIdMedioPago(cotizacionDTO.getIdMedioPago().intValue());
			}
			try{
				conductosDeCobro = listadoService.getConductosCobro(
						cotizacionDTO.getIdToPersonaContratante().longValue(),
						idMedioPago);
			} catch(RuntimeException exception) {
				conductosDeCobro = new HashMap<Long, String>(1);
			}			

			try {
				setNegocioProductos(entidadService.findByProperty(
						NegocioProducto.class, "negocio", cotizacionDTO
						.getSolicitudDTO().getNegocio()));
			} catch (RuntimeException e) {
				super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
				LOG.error(e.getMessage(), e);
				throw e;
			}
			for (Iterator<NegocioProducto> i = getNegocioProductos().iterator(); i.hasNext();) {
				NegocioProducto negocio = i.next();
				if (cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto().equals(negocio.getProductoDTO().getIdToProducto())) {
					List<NegocioMedioPago> mediosAsociados = this.negocioMedioPagoService.getRelationLists(negocio).getAsociadas();
					try {
						setMedioPagoDTOs(mediosAsociados);
						break;
					} catch (RuntimeException e) {
						super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
						LOG.error(e.getMessage(), e);
						throw e;
					}
				}
			}
		}
		prepareDomicilio();
	}
	
	public void prepareDomicilio() {
		// Pais-Municipio-Ciudad
		setEstadoMap(listadoService.getMapEstados(UtileriasWeb.KEY_PAIS_MEXICO));
		if (cuentaPagoDTO != null && cuentaPagoDTO.getDomicilio() != null) {
			// Carga los estados para Mexico
			setEstadoMap(listadoService.getMapEstados(UtileriasWeb.KEY_PAIS_MEXICO));
			
			if (cuentaPagoDTO.getDomicilio().getClaveEstado() != null) {
				setMunicipioMap(listadoService
						.getMapMunicipiosPorEstado(cuentaPagoDTO.getDomicilio().getClaveEstado()));
			}else{
				setMunicipioMap(new HashMap<String, String>(1));
			}
			if (cuentaPagoDTO.getDomicilio().getClaveCiudad() != null) {
				setColoniasMap(listadoService.getMapColonias(cuentaPagoDTO
						.getDomicilio().getClaveCiudad()));
			}else{
				setColoniasMap(new HashMap<String, String>(1));
			}
		}else{
			setMunicipioMap(new HashMap<String, String>(1));
			setColoniasMap(new HashMap<String, String>(1));
		}
	}
	
	@Action(value = "mostrar", results = { @Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/cobranza/cobranza.jsp")})
	public String mostrar() {
		mapDatosIguales.put(true, "SI");
		mapDatosIguales.put(false, "NO");
		return SUCCESS;
	}

	/**
	 * Obtiene los datos de la cuenta
	 */
	public void prepareComplementarInfoCuenta() {
		getCotizacion();
		// Obtiene el conducto de cobro de la cotizacion
		try {
			if (StringUtil.isEmpty(idConductoCobro)) {
				cuentaPagoDTO = pagoService.obtenerConductoCobro(idToCotizacion);
			} else {
				cuentaPagoDTO = pagoService
						.obtenerConductoCobro(
								cotizacionDTO.getIdToPersonaContratante(),
								Long.valueOf(idConductoCobro));
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@Action(value = "complementarInfoCuenta", results = {
			@Result(name = "tc", location = "/jsp/suscripcion/cotizacion/auto/cobranza/tc.jsp"),
			@Result(name = "domi", location = "/jsp/suscripcion/cotizacion/auto/cobranza/domi-afirme.jsp"),
			@Result(name = "tcAgente", location = "/jsp/suscripcion/cotizacion/auto/agente/include/tc.jsp"),
			@Result(name = "domiAgente", location = "/jsp/suscripcion/cotizacion/auto/agente/include/domi-afirme.jsp"),
			// START CONDUCTO DE COBRO AMERICAN EXPRESSS
			@Result(name = "amex", location = "/jsp/suscripcion/cotizacion/auto/cobranza/amexAgente.jsp"),
			@Result(name = "amexAgente", location = "/jsp/suscripcion/cotizacion/auto/agente/include/amexAgente.jsp")
			// END CONDUCTO DE COBRO AMERICAN EXPRESSS
			})
	public String complementarInfoCuenta() {
		if (cuentaPagoDTO == null) {
			cuentaPagoDTO = new CuentaPagoDTO();
		}
		
		switch (getIdMedioPago()) {
		case CuentaPagoDTO.TARJETA_CREDITO:
			// Tarjeta de credito
			if (cuentaPagoDTO.getDiaPago() == null) {
				Calendar calendar = Calendar.getInstance();
				cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
			}
			if(cuentaPagoDTO.getTipoTarjeta() != null) {
				tipoTarjeta = cuentaPagoDTO.getTipoTarjeta().valor();
			}
			if(cuentaPagoDTO.getCuentaNoEncriptada() != null){
				promociones = listadoService.getPromocionesTC(cuentaPagoDTO.getCuentaNoEncriptada());
			}else{
				//Prevenir error
				promociones = new HashMap<String, String>(1);
			}
			omitirAmexDeBancos();
			return (newCotizadorAgente?"tcAgente":"tc");
		case CuentaPagoDTO.DOMICILIACION:
			// Domiciliacion
			omitirAmexDeBancos();
			break;
		case CuentaPagoDTO.CUENTA_AFIRME:
			// Cuenta Afirme
			// Banco default AFIME
			getBancoByName(BancoDTO.BANCO_AFIRME);
			break;
	// START CONDUCTO DE COBRO AMERICAN EXPRESSS		
		case CuentaPagoDTO.AMERICAN_EXPRESS:
			getBancoByName(BancoDTO.BANCO_AMERICAN_EXPRESS);
			// Tarjeta de credito
			if (cuentaPagoDTO.getDiaPago() == null) {
				Calendar calendar = Calendar.getInstance();
				cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
			}
			if(cuentaPagoDTO.getTipoTarjeta() != null) {
				tipoTarjeta = cuentaPagoDTO.getTipoTarjeta().valor();
			}
			promociones = new HashMap<String, String>(1);
			return (newCotizadorAgente?"amexAgente":"amex");
	// START CONDUCTO DE COBRO AMERICAN EXPRESSS	
		default:
			// Cualquier otra cosa
		}
		return  getDomiSucces();
	}
	
	public String getDomiSucces(){
		return newCotizadorAgente?"domiAgente":"domi";
	}
	
	/**
	 * Obtiene los datos el titular
	 */
	public void prepareComplementarInfoTitular() {
		getCotizacion();
		try {
			cuentaPagoDTO = new CuentaPagoDTO();
			if (!datosIguales) {
				if (cotizacionDTO.getIdToPersonaContratante() != null && cotizacionDTO.getIdConductoCobroCliente() != null && cotizacionDTO.getIdConductoCobroCliente() > 0){
					if(conductoCobroId != null){
						cuentaPagoDTO = pagoService.obtenerConductoCobro(cotizacionDTO.getIdToPersonaContratante(), conductoCobroId);
					}else{
						cuentaPagoDTO = pagoService.obtenerDatosTitularSiEsIgualDatosCotizacion(cotizacionDTO.getIdToPersonaContratante());
						//cuentaPagoDTO = pagoService.obtenerConductoCobro(cotizacionDTO.getIdToPersonaContratante(), cotizacionDTO.getIdConductoCobroCliente());
					}
				}
			} else {
				if (StringUtil.isEmpty(idConductoCobro)) {
					if(conductoCobroId == null){
						cuentaPagoDTO = pagoService.obtenerDatosTitularSiEsIgualDatosCotizacion(cotizacionDTO.getIdToPersonaContratante());
						//cuentaPagoDTO = pagoService.obtenerConductoCobro(idToCotizacion);
					}else{
						cuentaPagoDTO = pagoService.obtenerConductoCobro(cotizacionDTO.getIdToPersonaContratante(), conductoCobroId);
					}
				} else {
					if(conductoCobroId == null){
						cuentaPagoDTO = pagoService.obtenerDatosTitularSiEsIgualDatosCotizacion(cotizacionDTO.getIdToPersonaContratante());
						//cuentaPagoDTO = pagoService.obtenerConductoCobro(cotizacionDTO.getIdToPersonaContratante(), Long.valueOf(idConductoCobro));
					}else{
						cuentaPagoDTO = pagoService.obtenerConductoCobro(cotizacionDTO.getIdToPersonaContratante(), conductoCobroId);
					}
					
				}								
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		prepareDomicilio();
	}
	
	@Action(value = "complementarInfoTitular", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/cobranza/datosTitular.jsp"),
			@Result(name = "datosTitularAgente", location = "/jsp/suscripcion/cotizacion/auto/agente/include/datosTitular.jsp")})
	public String complementarInfoTitular() {
		if(newCotizadorAgente){
			return "datosTitularAgente";
		}
		return SUCCESS;
	}
	
	@Action(value="obtenerDatosTarjeta",results={@Result(name=SUCCESS,type="json",params={"includeProperties","idMedioPago"})})
	public String obtenerDatosTarjeta(){
		getCotizacion();
		if (cotizacionDTO != null && idMedioPago!=null){
			try{
				clienteFacadeRemote.obtenerDatosTarjeta(cotizacionDTO.getIdToPersonaContratante(), idMedioPago, "SISTEMA");
			}catch(Exception ex){
				LOG.error(ex.getMessage(), ex);
			}
		}
		return SUCCESS;
	}
	
	@Action(value = "guardarCobranza", results = { @Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "iniciarComplementar","namespace","/suscripcion/cotizacion/auto/complementar","cotizacionId", "${idToCotizacion}","tabActiva","${tabActiva}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","tabActiva","${tabActiva}" }),
			@Result(name = "cotizadorAgente", type = "redirectAction", params = {
					"actionName", "mostrarContenedor","namespace","/suscripcion/cotizacion/auto/cotizadoragente","cotizacionId", "${idToCotizacion}","idToCotizacion", "${idToCotizacion}",
					"tabActiva","${tabActiva}","mensaje","${mensaje}","tipoMensaje","${tipoMensaje}" }),
			@Result(name = "newCotizadorAgente", type = "redirectAction", params = {
					"actionName", "mostrarContenedor","namespace","/suscripcion/cotizacion/auto/agentes", "mensaje","${mensaje}","tipoMensaje","${tipoMensaje}","idToCotizacion", "${idToCotizacion}","forma", "${forma}",
					"idToPersonaContratante", "${idToPersonaContratante}","numeroInciso", "${numeroInciso}","compatilbeExplorador", "${compatilbeExplorador}", "idMedioPago", "${idMedioPago}","datosIguales", "${datosIguales}",
					"incisoCotizacion.idConductoCobroCliente", "${idConductoCobro}","clati","${clati}"})
			})
	public String guardarCobranza() {
		tabActiva = "cobranza";
		getCotizacion();
		try{
			Short medioPago = idMedioPago.shortValue();
			setDatosConductoCobro(medioPago);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}		
		
		if (cotizacionDTO != null && cuentaPagoDTO != null) {

			if (idMedioPago.intValue() == CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS.valor() && 
					(!(cotizacionDTO.getIdFormaPago().intValue() == 1) || 
					cotizacionDTO.getIdMoneda().intValue() != MonedaDTO.MONEDA_PESOS)) {
				super.setMensajeError(MENSAJE_AMEX_PAGO_ANUAL);
				return getReturn();
			}
			
			if(!StringUtil.isEmpty(idConductoCobro)){
				cuentaPagoDTO.setIdConductoCliente(Long.valueOf(idConductoCobro));
			}			
			// Tipo de persona
			cuentaPagoDTO.setTipoPersona(cotizacionDTO.getSolicitudDTO().getClaveTipoPersona().toString());
			// Constrante
			cuentaPagoDTO.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
		
			try{
				if(cotizacionDTO.getIdToCotizacion()!=null && (ejecutivoColoca != null && !ejecutivoColoca.isEmpty())){
					cotizacionService.guardarEjecutivoColoca(cotizacionDTO.getIdToCotizacion(), ejecutivoColoca);
				}
				
				pagoService.guardarConductoCobro(cotizacionDTO.getIdToCotizacion(), cuentaPagoDTO);
				pagoService.guardarConductoCobroCotizacionAInciso(cotizacionDTO.getIdToCotizacion());
				
				if (!getIdMedioPago().equals(new Integer(CuentaPagoDTO.TipoCobro.EFECTIVO.valor())) && !StringUtil.isEmpty(idConductoCobro)){
					cotizacionCobranzaService.guardarDatosCotizacionCobranza(cotizacionDTO, new Integer(idConductoCobro), idMedioPago, datosIguales);
				}
			}catch(Exception e){
				String message = e.getMessage();

				if(message.indexOf("java.lang.RuntimeException:") != -1){
					super.setMensajeError(message.substring(message.indexOf("java.lang.RuntimeException:")+subSringMessage,message.length()));
				}else{
					super.setMensajeError(MENSAJE_ERROR_GENERAL);
				}
				return getReturn();
			}

			cuentaPagoDTO.setFechaVencimiento(mes+anio);
			
		}else{
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		super.setMensajeExito();		
		return getReturn();

	}
	
	private void setDatosConductoCobro(Short medioPago) {
		TipoCobro[] tiposCobro = CuentaPagoDTO.TipoCobro.values();
		for(TipoCobro tCobro: tiposCobro){
			Short tipoCobro = tCobro.valor();
			if(tipoCobro.equals(medioPago)){
				if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.TARJETA_CREDITO.valor())){
					if (!StringUtil.isEmpty(tipoTarjeta)){
						if (tipoTarjeta.equals(CuentaPagoDTO.TipoTarjeta.MASTERCARD.valor()) ){
							cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.MASTERCARD);
						}else if (tipoTarjeta.equals(CuentaPagoDTO.TipoTarjeta.VISA.valor()) ){
							cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.VISA);
						}else{
							cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.CREDIT_CARD);
						}
					}else{
						cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.CREDIT_CARD);
					}
				}else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor())){
					getBancoByName(BancoDTO.BANCO_AFIRME);
					cuentaPagoDTO.setIdBanco((long)idBanco);
				}else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.EFECTIVO.valor())){
					cuentaPagoDTO = new CuentaPagoDTO();
				}else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS.valor())){
					cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.AMERICAN_EXPRESS);
					cuentaPagoDTO.setIdBanco(BancoDTO.AMERICAN_EXPRESS.longValue());
				}
				cuentaPagoDTO.setTipoConductoCobro(tCobro);
				break;
			}			
		}
		if (cuentaPagoDTO.getDiaPago() == null) {
			Calendar calendar = Calendar.getInstance();
			cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
		}
	}
	
	private void getBancoByName(String nombreBanco){
		BancoEmisorDTO banco = bancoEmisorFacade.findByName(nombreBanco);
		cuentaPagoDTO.setIdBanco(banco.getIdBanco().longValue());
		idBanco = banco.getIdBanco();
	}
	
	private String getReturn(){
		if(cotizadorAgente){
			tabActiva = "cobranza";
			return "cotizadorAgente";
		}else if(newCotizadorAgente){
			forma = CotizadorAgentesAction.DATOS_COBRANZA_EMISION;
			idToPersonaContratante = cotizacionDTO.getIdToPersonaContratante();
			numeroInciso = BigDecimal.ONE;
			return "newCotizadorAgente";
		}else{
			return SUCCESS;
		}
	}

	// START OMITIR BANCO AMERICAN EXPRESSS	
	public void omitirAmexDeBancos(){
		List<BancoEmisorDTO> bancosDTO = bancoEmisorFacade.findAll();
		bancos = new ArrayList<BancoEmisorDTO>(1);
		for (BancoEmisorDTO bancoEmisor : bancosDTO) {
			if (!bancoEmisor.getIdBanco().equals(BancoDTO.AMERICAN_EXPRESS)) {
	            bancos.add(bancoEmisor);
	        }
	    }
	}
	
	// END OMITIR BANCO AMERICAN EXPRESSS	
	public void omitirAmex(){
		if (medioPagoDTOs != null) {
			for (MedioPagoDTO medioPagoDTO : medioPagoDTOs) {
				if (medioPagoDTO.getIdMedioPago().equals(CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS.valor())) {
					medioPagoDTOs.remove(medioPagoDTO);
				}
			}
		}
	}
	
	private void getCotizacion() {
		// Obtiene la cotizacion
		if (idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class,idToCotizacion);
		}		
	}	
		
	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public List<MedioPagoDTO> getMedioPagoDTOs() {
		return medioPagoDTOs;
	}

	public void setMedioPagoDTOs(List<NegocioMedioPago> mediosAsociados) {
		List<MedioPagoDTO> medioTmp = new ArrayList<MedioPagoDTO>();
		for (NegocioMedioPago negocioMedioPago : mediosAsociados){

			// El medio de pago "Agente" cambia de nombre por efectivo en el cotizador de agentes
			if( MEDIOPAGO_AGENTE == negocioMedioPago.getMedioPagoDTO().getKey() ){
				negocioMedioPago.getMedioPagoDTO().setDescripcion(MEDIOPAGO_EFECTIVO);
			}

			medioTmp.add(negocioMedioPago.getMedioPagoDTO());
		}
		
		this.medioPagoDTOs = medioTmp;
		
		if (!cotizacionDTO.getIdFormaPago().equals(1)) {
			omitirAmex();
		}
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public List<NegocioProducto> getNegocioProductos() {
		return negocioProductos;
	}

	public void setNegocioProductos(List<NegocioProducto> negocioProductos) {
		this.negocioProductos = negocioProductos;
	}

	public NegocioClienteService getNegocioClienteService() {
		return negocioClienteService;
	}

	@Autowired
	@Qualifier("negocioClienteEJB")
	public void setNegocioClienteService(
			NegocioClienteService negocioClienteService) {
		this.negocioClienteService = negocioClienteService;
	}

	public ClienteDTO getClienteDTO() {
		return clienteDTO;
	}

	public void setClienteDTO(ClienteDTO clienteDTO) {
		this.clienteDTO = clienteDTO;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<String, String> getEstadoMap() {
		return estadoMap;
	}

	public void setEstadoMap(Map<String, String> estadoMap) {
		this.estadoMap = estadoMap;
	}

	public Map<String, String> getMunicipioMap() {
		return municipioMap;
	}

	public void setMunicipioMap(Map<String, String> municipioMap) {
		this.municipioMap = municipioMap;
	}

	public Map<String, String> getColoniasMap() {
		return coloniasMap;
	}

	public void setColoniasMap(Map<String, String> coloniasMap) {
		this.coloniasMap = coloniasMap;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public DomicilioFacadeRemote getDomicilioFacadeRemote() {
		return domicilioFacadeRemote;
	}

	@Autowired
	@Qualifier("domicilioFacadeRemoteEJB")
	public void setDomicilioFacadeRemote(
			DomicilioFacadeRemote domicilioFacadeRemote) {
		this.domicilioFacadeRemote = domicilioFacadeRemote;
	}

	public ClienteFacadeRemote getClienteFacadeRemote() {
		return clienteFacadeRemote;
	}

	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	public String getIdConductoCobro() {
		return idConductoCobro;
	}

	public void setIdConductoCobro(String idConductoCobro) {
		this.idConductoCobro = idConductoCobro;
	}

	public Integer getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdMedioPago(Integer idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public ClienteGenericoDTO getClienteGenericoDTO() {
		return clienteGenericoDTO;
	}

	public void setClienteGenericoDTO(ClienteGenericoDTO clienteGenericoDTO) {
		this.clienteGenericoDTO = clienteGenericoDTO;
	}

	public List<BancoEmisorDTO> getBancos() {
		return bancos;
	}

	public void setBancos(List<BancoEmisorDTO> bancos) {
		this.bancos = bancos;
	}

	@Autowired
	@Qualifier("bancoEmisorFacadeRemoteEJB")
	public void setBancoEmisorFacade(BancoEmisorFacadeRemote bancoEmisorFacade) {
		this.bancoEmisorFacade = bancoEmisorFacade;
	}
	
	public PolizaPagoService getPagoService() {
		return pagoService;
	}
	
	@Autowired
	@Qualifier("polizaPagoServiceEJB")
	public void setPagoService(PolizaPagoService pagoService) {
		this.pagoService = pagoService;
	}

	public Map<String, String> getPaisesMap() {
		return paisesMap;
	}

	public void setPaisesMap(Map<String, String> paisesMap) {
		this.paisesMap = paisesMap;
	}

	public Boolean getDatosIguales() {
		return datosIguales;
	}

	public void setDatosIguales(Boolean datosIguales) {
		this.datosIguales = datosIguales;
	}


	public CuentaPagoDTO getCuentaPagoDTO() {
		return cuentaPagoDTO;
	}

	public void setCuentaPagoDTO(CuentaPagoDTO cuentaPagoDTO) {
		this.cuentaPagoDTO = cuentaPagoDTO;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public Map<Boolean, String> getMapDatosIguales() {
		return mapDatosIguales;
	}

	public void setMapDatosIguales(Map<Boolean, String> mapDatosIguales) {
		this.mapDatosIguales = mapDatosIguales;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}
	
	public String getTabActiva() {
		return tabActiva;
	}

	public void setTabActiva(String tabActiva) {
		this.tabActiva = tabActiva;
	}

	public Map<Long, String> getConductosDeCobro() {
		return conductosDeCobro;
	}
	
	public void setConductosDeCobro(Map<Long, String> conductosDeCobro) {
		this.conductosDeCobro = conductosDeCobro;
	}

	public Integer getIdBanco() {
		return idBanco;
	}

	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}

	public Map<String, String> getPromociones() {
		return promociones;
	}

	public void setPromociones(Map<String, String> promociones) {
		this.promociones = promociones;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	public EsquemaPagoCotizacionDTO getEsquemaPagoCotizacionDTO() {
		return esquemaPagoCotizacionDTO;
	}

	public void setEsquemaPagoCotizacionDTO(
			EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO) {
		this.esquemaPagoCotizacionDTO = esquemaPagoCotizacionDTO;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	public void setCotizadorAgente(Boolean cotizadorAgente) {
		this.cotizadorAgente = cotizadorAgente;
	}

	public Boolean getCotizadorAgente() {
		return cotizadorAgente;
	}

	public Boolean getNewCotizadorAgente() {
		return newCotizadorAgente;
	}

	public void setNewCotizadorAgente(Boolean newCotizadorAgente) {
		this.newCotizadorAgente = newCotizadorAgente;
	}

	public int getForma() {
		return forma;
	}

	public void setForma(int forma) {
		this.forma = forma;
	}

	public BigDecimal getIdToPersonaContratante() {
		return idToPersonaContratante;
	}

	public void setIdToPersonaContratante(BigDecimal idToPersonaContratante) {
		this.idToPersonaContratante = idToPersonaContratante;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public boolean isCompatilbeExplorador() {
		return compatilbeExplorador;
	}

	public void setCompatilbeExplorador(boolean compatilbeExplorador) {
		this.compatilbeExplorador = compatilbeExplorador;
	}
	
	public Long getConductoCobroId() {
		return conductoCobroId;
	}

	public void setConductoCobroId(Long conductoCobroId) {
		this.conductoCobroId = conductoCobroId;
	}
	
	public String getUsuarioTieneRolEjecutivo() {
		return usuarioTieneRolEjecutivo;
	}

	public void setUsuarioTieneRolEjecutivo(String usuarioTieneRolEjecutivo) {
		this.usuarioTieneRolEjecutivo = usuarioTieneRolEjecutivo;
	}
	
	public Map<String, String> getListaDeEjecutivos() {
		return listaDeEjecutivos;
	}

	public void setListaDeEjecutivos(Map<String, String> listaDeEjecutivos) {
		this.listaDeEjecutivos = listaDeEjecutivos;
	}
	
	public String getEjecutivoColoca() {
		return ejecutivoColoca;
	}

	public void setEjecutivoColoca(String ejecutivoColoca) {
		this.ejecutivoColoca = ejecutivoColoca;
	}

	public String getClati() {
		return clati;
	}

	public void setClati(String clati) {
		this.clati = clati;
	}

}