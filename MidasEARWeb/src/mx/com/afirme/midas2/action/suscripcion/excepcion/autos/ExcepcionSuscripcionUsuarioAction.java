package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.util.HashMap;
import java.util.Map;

import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionUsuarioAction extends
		ExcepcionSuscripcionAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3705842554242138322L;

	private String usuarioId = null;
	private Map<String, String> tipoUsuario = null;

	@Override
	public void prepare() throws Exception {

	}

	public void prepareMostrarUsuario() {
		excepcionId = super.obtenerExcepcion();
		if (excepcionId != null) {
			System.out.println("Consola - " + usuarioId);
			usuarioId = excepcionService.obtenerCondicionValor(excepcionId,
					TipoCondicion.USUARIO);
		}
		// Llena el mapa de tipos de usuario
		if (tipoUsuario == null) {
			tipoUsuario = new HashMap<String, String>();
			tipoUsuario.put("0", "Interno");
			tipoUsuario.put("1", "Externo");
		}
	}

	public String guardarUsuario() {
		try {
			excepcionId = super.obtenerExcepcion();
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.USUARIO,
					usuarioId);
			super.setMensajeExitoPersonalizado("Condici�n agregada a la excepci�n");
			// Condici�n Agregada con exito
			super.setAgregada(true);
		} catch (Exception ex) {
			super.setMensajeError("No se puedo agregar la condici�n a la excepci�n en este momento");
		}
		return SUCCESS;
	}

	public String mostrarUsuario() {
		return SUCCESS;
	}

	/**
	 * @param tipoUsuario
	 *            el tipoUsuario a establecer
	 */
	public void setTipoUsuario(Map<String, String> tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	/**
	 * @return el tipoUsuario
	 */
	public Map<String, String> getTipoUsuario() {
		return tipoUsuario;
	}

	/**
	 * @param usuarioId
	 *            el usuarioId a establecer
	 */
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	/**
	 * @return el usuarioId
	 */
	public String getUsuarioId() {
		return usuarioId;
	}

}
