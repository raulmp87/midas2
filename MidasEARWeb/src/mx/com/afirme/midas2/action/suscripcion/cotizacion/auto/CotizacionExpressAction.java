package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaCotizacionExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CotizacionExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.DatosCotizacionExpressDTO;
import mx.com.afirme.midas2.impresionesM2.GenerarReportes;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionExpressService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso1Checks;
import mx.com.afirme.midas2.validator.group.CotizacionExpressPaso2Checks;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.js.util.StringUtil;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizacionExpressAction extends BaseAction  implements Preparable, SessionAware {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3612326329601252054L;
	private static final String COTIZACIONES_EXPRESS_KEY = "cotizacionExpress";
	
	private BigDecimal idToNegSeccion;
	private DatosCotizacionExpressDTO datosCotizacionExpressDTO = new DatosCotizacionExpressDTO();
	private NegocioSeccion negocioSeccion;
	
	private CotizacionService cotizacionService;
	private CotizacionExpressService cotizacionExpressService;
	private EntidadService entidadService;
	private FormaPagoFacadeRemote formaPagoFacadeRemote;
	private ModeloVehiculoFacadeRemote modeloVehiculoFacadeRemote;
	private EstadoFacadeRemote estadoFacadeRemote;
	private MunicipioFacadeRemote municipioFacadeRemote;
	private Map<String, Object> session;
	private CotizacionExpressDTO cotizacionExpressDTO;
	private Long idPaquete;
	private Integer idFormaPago;
	private BigDecimal idNegocioSeccion;

	private Long id;
	private InputStream cotExpressInputStream;
	private String contentType;
	private String fileName;
	private String correoDestinatario;
	private String nombreDestinatario;
	private String idMonedaName;
	private List<BigDecimal> formaPagoMatriz = new ArrayList<BigDecimal>(1);
	private List<BigDecimal> paquetesMatriz = new ArrayList<BigDecimal>(1);
	private List<CoberturaCotizacionExpress> coberturaCotizacionExpressList = new ArrayList<CoberturaCotizacionExpress>(1);
	private Usuario usuario;
	private Boolean suscriptor;
	private Agente agente = new Agente();
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(
			CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@Autowired
	public void setCotizacionExpressService(
			CotizacionExpressService cotizacionExpressService) {
		this.cotizacionExpressService = cotizacionExpressService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("formaPagoEJB")
	public void setFormaPagoFacadeRemote(
			FormaPagoFacadeRemote formaPagoFacadeRemote) {
		this.formaPagoFacadeRemote = formaPagoFacadeRemote;
	}

	@Autowired
	@Qualifier("modeloVehiculoEJB")
	public void setModeloVehiculoFacadeRemote(
			ModeloVehiculoFacadeRemote modeloVehiculoFacadeRemote) {
		this.modeloVehiculoFacadeRemote = modeloVehiculoFacadeRemote;
	}
	
	@Autowired
	@Qualifier("estadoFacadeRemoteEJB")
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}
	
	@Autowired
	@Qualifier("municipioFacadeRemoteEJB")
	public void setMunicipioFacadeRemote(
			MunicipioFacadeRemote municipioFacadeRemote) {
		this.municipioFacadeRemote = municipioFacadeRemote;
	}
	
//	@Autowired
//	@Qualifier("agenteServiceEJB")
//	public void setAgenteService(AgenteService agenteService) {
//		this.agenteService = agenteService;
//	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}
	
	public void setIdFormaPago(Integer idFormaPago) {
		this.idFormaPago = idFormaPago;
	}
	
	public void setIdPaquete(Long idPaquete) {
		this.idPaquete = idPaquete;
	}
	
	public Integer getIdFormaPago() {
		return idFormaPago;
	}
	
	public Long getIdPaquete() {
		return idPaquete;
	}
		
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public InputStream getCotExpressInputStream() {
		return cotExpressInputStream;
	}

	public void setCotExpressInputStream(InputStream cotExpressInputStream) {
		this.cotExpressInputStream = cotExpressInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCorreoDestinatario() {
		return correoDestinatario;
	}

	public void setCorreoDestinatario(String correoDestinatario) {
		this.correoDestinatario = correoDestinatario;
	}

	public String getNombreDestinatario() {
		return nombreDestinatario;
	}

	public void setNombreDestinatario(String nombreDestinatario) {
		this.nombreDestinatario = nombreDestinatario;
	}
	
	public boolean getPermiteBuscar() {
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			//No se permite buscar ya que el unico agente para seleccionar es el agente que tiene asociado el usuario.
			return false;
		}
		return true;
	}

	@Override
	public void prepare() {		
		usuario = usuarioService.getUsuarioActual();
		// Valida el rol del usuario
		for(Iterator<Rol> i= usuario.getRoles().iterator(); i.hasNext();){
			Rol rol = i.next();
			if( rol.getDescripcion().equals(sistemaContext.getRolSuscriptorAutos())){
				setSuscriptor(true);
				break;
			}
		}
		if (id != null) {
			setCotizacionExpressDTO(buscarCotizacionExpressDTO(id));
			//Actualizar el paquete y la forma de pago.
			if (idPaquete != null) {
				getCotizacionExpressDTO().setPaquete(entidadService.findById(Paquete.class, idPaquete));
			}
			if (idFormaPago != null) {
				FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
				formaPagoDTO.setIdFormaPago(idFormaPago);
				getCotizacionExpressDTO().setFormaPago(formaPagoFacadeRemote.findById(formaPagoDTO));
			}
			if(idNegocioSeccion != null){
				getCotizacionExpressDTO().setNegocioSeccion(entidadService.findById(NegocioSeccion.class, idNegocioSeccion));
			}
		} else {
			setCotizacionExpressDTO(cotizacionExpressService.crearNuevaCotizacionExpressDTO());
		}
	}
	

	/**
	 * Inicializa las referencias del objeto cotizacion segun los ids que pudiera tener el objeto mandado.
	 * @param root utilizado como base para inicializar el objeto del dominio utilizado. De este objeto se obtienen los ids.
	 */
	private void inicializarReferencias(CotizacionExpressDTO root) {
		if (root != null) {
			EstadoDTO estado = root.getEstado();
			if (estado != null && !StringUtils.isBlank(estado.getStateId())) {
				getCotizacionExpressDTO().setEstado(estadoFacadeRemote.findById(estado.getStateId()));
			}
			
			MunicipioDTO municipio = root.getMunicipio();
			if (municipio != null && !StringUtils.isBlank(municipio.getMunicipalityId())) {
				getCotizacionExpressDTO().setMunicipio(municipioFacadeRemote.findById(municipio.getMunicipalityId()));
			}
			Agente agente = root.getAgente();
			if (agente != null && agente.getId() != null) {
				getCotizacionExpressDTO().setAgente(entidadService.findById(Agente.class, agente.getId()));
			}
			
			//ModeloVehiculoDTO
			ModeloVehiculoDTO modeloVehiculo = inicializarReferenciasVehiculo(root.getModeloVehiculo());
			if (modeloVehiculo != null) {
				getCotizacionExpressDTO().setModeloVehiculo(modeloVehiculo);
			}
			
		}
		
		//Actualizar el paquete y la forma de pago.
		if (idPaquete != null) {
			getCotizacionExpressDTO().setPaquete(entidadService.findById(Paquete.class, idPaquete));
		}
		if (idFormaPago != null) {
			FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
			formaPagoDTO.setIdFormaPago(idFormaPago);
			getCotizacionExpressDTO().setFormaPago(formaPagoFacadeRemote.findById(formaPagoDTO));
		}
		if(idNegocioSeccion != null){
			getCotizacionExpressDTO().setNegocioSeccion(entidadService.findById(NegocioSeccion.class, idNegocioSeccion));
		}
	}
	
	/**
	 * Inicializa las referencias del modelo vehiculo de acuerdo a los ids que pudiera tener asignados. Este metodo se podria poner en un servicio
	 * o clase utileria para que las demas acciones que necesiten hacer esta inicializacion no tengan que implementarlo.
	 * El orden es el siguiente:
	 * 1. Modelo.
	 * 2. Estilo
	 * 3. Marca
	 * @param modeloVehiculo que contiene solo los ids.
	 * @return el modeloVehiculo con las referencias incializadas.
	 */
	private ModeloVehiculoDTO inicializarReferenciasVehiculo(ModeloVehiculoDTO modeloVehiculo) {
		if (modeloVehiculo != null) {
			ModeloVehiculoId modeloVehiculoId = modeloVehiculo.getId();
			if (modeloVehiculoId != null && modeloVehiculoId.getModeloVehiculo() != null) {
				//FIXME:Esta manera de obtener el modeloVehiculo a partir de los otros campos va a ocasionar problemas
				//al momento de generar la busqueda express de vehiculos debido a que la manera en que se diseño no permite
				//obtener el modeloVehiculoId por completo sino que uno tiene que buscar tambien en estilo para poder construir
				//el id de ModeloVehiculoId. Se sugiere que que se cree un setStrId/getStrId asi como lo tiene EstiloVehiculoId.
				//Inicializar modelo.
				EstiloVehiculoId estiloVehiculoId = modeloVehiculo.getEstiloVehiculoDTO().getId();
				modeloVehiculoId.setClaveTipoBien(estiloVehiculoId.getClaveTipoBien());
				modeloVehiculoId.setClaveEstilo(estiloVehiculoId.getClaveEstilo());
				modeloVehiculoId.setIdMoneda(new BigDecimal(getCotizacionExpressDTO().getIdMoneda()));
				modeloVehiculoId.setIdVersionCarga(estiloVehiculoId.getIdVersionCarga());					
				modeloVehiculo = modeloVehiculoFacadeRemote.findById(modeloVehiculoId);					
			} 
		}
		
		return modeloVehiculo;
	}
	

	public String mostrar() {
		try{
			if (getCotizacionExpressDTO().getId() != null) {
				setDatosCotizacionExpressDTO(cotizacionService.obtenerDatosCotizacionExpressDTO(getCotizacionExpressDTO()));
			}	
		}catch(Exception ex){
			LOG.error("Express error", ex);
		}
		
		return SUCCESS;
	}
	
	
	
	public void validateGuardar() {
		addErrors(getCotizacionExpressDTO(), getValidationGroup(), this, "cotizacionExpressDTO");
	}
	
	private Class<?> getValidationGroup() {
		if (getCotizacionExpressDTO().getPaso() == 1) {
			return CotizacionExpressPaso1Checks.class;
		} else if (getCotizacionExpressDTO().getPaso() == 2) {
			return CotizacionExpressPaso2Checks.class;
		}
		return null;
	}
	
	public void prepareGuardar(){
	}
	
	public String guardar() {		
		inicializarReferencias(getCotizacionExpressDTO());	
		setCotizacionExpressDTO(guardarCotizacionExpressDTO(getCotizacionExpressDTO()));
		id = getCotizacionExpressDTO().getId();
		return getGuardarResultName();
	}
	
	/**
	 * Obtiene el result name de acuerdo al paso de la cotizacion express.
	 * @return
	 */
	private String getGuardarResultName() {
		if (getCotizacionExpressDTO().getPaso() == 1) {
			return "cotizacionExpress";
		} else if (getCotizacionExpressDTO().getPaso() == 2) {
			return "resumen";
		}
		return null;		
	}
	
	public void prepareCotizarFormalmente(){
		if(agente.getId() != null){
			 agente = entidadService.findById(Agente.class, agente.getId());
		}
	}
	
	public String cotizarFormalmente() {
		CotizacionExpress cotExpress = getCotizacionExpressDTO().getCotizacionActual();
		cotExpress.setAgente(agente);
		CotizacionDTO cotizacion = cotizacionExpressService.cotizarFormalmente(cotExpress);
		if( cotizacion != null ) {
			try{
				cotizacionService.calcularTodosLosIncisos(cotizacion.getIdToCotizacion());
			}catch(Exception e){
				e.printStackTrace();
			}
			
			setMensajeExitoPersonalizado(getText("midas.cotizacion.express.cotizacionFormal.exito", 
					new String[] {cotizacion.getSolicitudDTO().getNumeroSolicitud()}));
			super.setNextFunction("mostrarBandejaCotizacion("+cotizacion.getSolicitudDTO().getNumeroSolicitud().substring(4,(cotizacion.getSolicitudDTO().getNumeroSolicitud().toString().length()))+","+agente.getId()+")");
		}else{
			super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
		}
		
		return SUCCESS;
	}
	
	/**
	 * Metodo que encapsula el guardado. Esto podria delegar la llamada a un servicio si se desea guardar en base de datos.
	 * Por simplicidad ya que se esta guardando los datos en sesion se esta haciendo aqui mismo en un mismo metodo del action.
	 * @param cotizacionExpressDTO
	 * @return
	 */
	private CotizacionExpressDTO guardarCotizacionExpressDTO(CotizacionExpressDTO cotizacionExpressDTO) {
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			agente = usuarioService.getAgenteUsuarioActual();
		}
		
		//Generar un id en caso de no tener uno.
		if (cotizacionExpressDTO.getId() == null) {
//			cotizacionExpressDTO.setId(UUID.randomUUID().getLeastSignificantBits());	
			cotizacionExpressDTO.setId(1L);
		}
		
		//Cotizar para cada paquete y forma de pago.
		if (cotizacionExpressDTO.getPaso() == 1) {
			cotizacionExpressDTO.resetPaqueteFormaPago();//FIX PARA PODER RECARGAR EXPRESS -- FIX
			cotizacionExpressDTO = cotizacionExpressService.cotizarPaqueteFormaPago(cotizacionExpressDTO);
		}
		
		getCotizacionesExpressMap().put(cotizacionExpressDTO.getId(), cotizacionExpressDTO);
		
		return cotizacionExpressDTO;
	}
	
	/**
	 * Metodo que hace la busqueda de la cotizacion express. Esto podria delegar la llamada a un servicio para hacer la busqueda en base de datos.
	 * @param id
	 * @return
	 */
	private CotizacionExpressDTO buscarCotizacionExpressDTO(Long id) {
		return getCotizacionesExpressMap().get(id);
	}
	
	@SuppressWarnings("unchecked" )
	private Map<Long, CotizacionExpressDTO> getCotizacionesExpressMap() {
		Map<Long, CotizacionExpressDTO> cotizacionesExpressMap = (Map<Long, CotizacionExpressDTO>) session.get(COTIZACIONES_EXPRESS_KEY);
		if (cotizacionesExpressMap == null) {
			cotizacionesExpressMap = new HashMap<Long, CotizacionExpressDTO>();
			if (session.containsKey(COTIZACIONES_EXPRESS_KEY)) {
				session.remove(COTIZACIONES_EXPRESS_KEY);
			}
			session.put(COTIZACIONES_EXPRESS_KEY, cotizacionesExpressMap);
		}
		
		return cotizacionesExpressMap;
	}
	
	public String mostrarMatrizDatos(){
		if(getCotizacionExpressDTO().getNegocioSeccion() != null){
			setDatosCotizacionExpressDTO(cotizacionService.obtenerDatosCotizacionExpressDTO(getCotizacionExpressDTO()));
		}else{
			datosCotizacionExpressDTO.setEncabezados(new ArrayList<NegocioFormaPago>(1));
			datosCotizacionExpressDTO.setFila(new ArrayList<NegocioPaqueteSeccion>(1));
		}
		return SUCCESS;
	}
	
	public String mostrarResumen() {
		return SUCCESS;
	}
	
	public String mostrarCoberturaCotizacionDhtmlx() {
		try{
			if(cotizacionExpressDTO.getPaqueteFormaPagos() == null || cotizacionExpressDTO.getPaqueteFormaPagos().isEmpty()){
				cotizacionExpressDTO = cotizacionExpressService.cotizarPaqueteFormaPago(cotizacionExpressDTO);
			}
			coberturaCotizacionExpressList = cotizacionExpressDTO.getCotizacionActual().getCoberturaCotizacionExpresses();
		}catch(Exception e){
			e.printStackTrace();
		}
		if(coberturaCotizacionExpressList == null){
			coberturaCotizacionExpressList = new ArrayList<CoberturaCotizacionExpress>();
		}
		return SUCCESS;
	}
	
	public String imprimirCotizacionExpress(){
		GenerarReportes generarReportes = new GenerarReportes();
		Locale locale = getLocale();

		List<ResumenCotExpressDTO> cotizacionExpressList = new ArrayList<ResumenCotExpressDTO>();
		cotizacionExpressDTO.resetPaqueteFormaPago();
		cotizacionExpressList = cotizacionExpressService.inicializarDatosParaImpresion(getCotizacionExpressDTO());
		
		generarReportes.setEntidadService(entidadService);
		try {
			cotExpressInputStream = new ByteArrayInputStream(generarReportes.imprimirCotizacionExpress(cotizacionExpressList, locale, formaPagoMatriz, paquetesMatriz));
		}catch (Exception exception) {
			exception.printStackTrace();
		}
		
		contentType = "application/pdf";
		fileName = "CotizacionExpress.pdf";			
		return SUCCESS;		
	}
	
	public String capturarDatosCorreo(){
		return SUCCESS;
	}
	
	@SuppressWarnings("deprecation")
	public String enviarCotizacionPorCorreo(){
		ByteArrayAttachment attachment = null;
		String [] correosDestinatarios = null;
		GenerarReportes generarReportes = new GenerarReportes();
		List<String> destinatarios = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		List<ResumenCotExpressDTO> cotizacionExpressList = new ArrayList<ResumenCotExpressDTO>();
		Locale locale = getLocale();		
		try{
			if (getCotizacionExpressDTO().getPaso() == 1) {
				cotizacionExpressList = cotizacionExpressService.inicializarDatosParaImpresion(buscarCotizacionExpressDTO(1L));
			}		
			generarReportes.setEntidadService(entidadService);				
			attachment = new ByteArrayAttachment("CotizacionExpress.pdf", TipoArchivo.PDF, 
					generarReportes.imprimirCotizacionExpress(cotizacionExpressList, locale, formaPagoMatriz, paquetesMatriz));		
			if(attachment != null){
				correosDestinatarios = correoDestinatario.split(";");
				for(int i=0;i<=correosDestinatarios.length-1;i++){
					destinatarios.add(correosDestinatarios[i]);
				}	
				adjuntos.add(attachment);
				mailService.sendMail(destinatarios, getText("midas.correo.cotizacion.titulo"), 
						getText("midas.correo.cotizacion.cuerpo"), adjuntos, getText("midas.correo.cotizacion.titulo"), 
						getText("midas.correo.cotizacion.saludo", new String[]{nombreDestinatario}));	
			}		
		}catch(Exception ex){			
			setMensaje("Error al enviar el correo favor de intentarlo mas tarde");
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;
		}
		return SUCCESS;		
	}
	
	public String correoEnviado(){
		return SUCCESS;
	}
	
	public void setDatosCotizacionExpressDTO(DatosCotizacionExpressDTO datosCotizacionExpressDTO) {
		this.datosCotizacionExpressDTO = datosCotizacionExpressDTO;
	}

	public DatosCotizacionExpressDTO getDatosCotizacionExpressDTO() {
		return datosCotizacionExpressDTO;
	}

	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}
	
	public boolean isBusquedaAgenteVisible() {
		return cotizacionExpressService.isBusquedaAgenteVisible();
	}

	public void setFormaPagoMatriz(List<BigDecimal> formaPagoMatriz) {
		this.formaPagoMatriz = formaPagoMatriz;
	}

	public List<BigDecimal> getFormaPagoMatriz() {
		return formaPagoMatriz;
	}

	public void setPaquetesMatriz(List<BigDecimal> paquetesMatriz) {
		this.paquetesMatriz = paquetesMatriz;
	}
	
	public String getFormaPagoMatrizStr() {
		String str = null;
		if(this.formaPagoMatriz != null && !this.formaPagoMatriz.isEmpty()){
			str = StringUtils.join(this.formaPagoMatriz.iterator(),",");
		}
		return str;
	}

	public void setFormaPagoMatrizStr(String formaPagoMatrizStr) {
		if(!StringUtil.isEmpty(formaPagoMatrizStr)){
			String[] str = formaPagoMatrizStr.split(",");
			if(this.formaPagoMatriz.isEmpty() && str != null && str.length > 0){
				for(String value : str){
					this.formaPagoMatriz.add(new BigDecimal(value));
				}
			}
		}
	}

	public String getPaquetesMatrizStr(){
		String str = null;
		if(this.paquetesMatriz != null && !this.paquetesMatriz.isEmpty()){
			str = StringUtils.join(this.paquetesMatriz.iterator(),",");
		}
		return str;
	}
	
	public void setPaquetesMatrizStr(String paquetesMatriz) {
		if(!StringUtil.isEmpty(paquetesMatriz)){
			String[] str = paquetesMatriz.split(",");
			if(this.paquetesMatriz.isEmpty() && str != null && str.length > 0){
				for(String value : str){
					this.paquetesMatriz.add(new BigDecimal(value));
				}
			}
		}
	}

	public List<BigDecimal> getPaquetesMatriz() {
		return paquetesMatriz;
	}

	public void setIdMonedaName(String idMonedaName) {
		this.idMonedaName = idMonedaName;
	}

	public String getIdMonedaName() {
		return idMonedaName;
	}

	public void setCotizacionExpressDTO(CotizacionExpressDTO cotizacionExpressDTO) {
		this.cotizacionExpressDTO = cotizacionExpressDTO;
	}

	public CotizacionExpressDTO getCotizacionExpressDTO() {
		return cotizacionExpressDTO;
	}

	public void setCoberturaCotizacionExpressList(
			List<CoberturaCotizacionExpress> coberturaCotizacionExpressList) {
		this.coberturaCotizacionExpressList = coberturaCotizacionExpressList;
	}

	public List<CoberturaCotizacionExpress> getCoberturaCotizacionExpressList() {
		return coberturaCotizacionExpressList;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Boolean getSuscriptor() {
		return suscriptor;
	}

	public void setSuscriptor(Boolean suscriptor) {
		this.suscriptor = suscriptor;
	}

	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}

	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}


	
	
}
