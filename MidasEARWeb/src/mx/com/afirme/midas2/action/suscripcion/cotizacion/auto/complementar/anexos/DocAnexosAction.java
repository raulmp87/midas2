package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.anexos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.DocumentoAgente;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.DocumentoFortimax;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos.DocAnexosService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class DocAnexosAction extends BaseAction implements Preparable {


	private BigDecimal idToCotizacion;
	private DocAnexoCotDTO docAnexoCotDTO;
	private List<DocAnexoCotDTO> anexosList = new ArrayList<DocAnexoCotDTO>(1);
	private RespuestaGridRelacionDTO respuesta;
	private String gr_id;
	private Short soloConsulta = 0;
	private String urlIfimax;
	private List<DocumentoAgente> listaAuxDocumentosAgente=new ArrayList<DocumentoAgente>();
	private List<DocumentoFortimax> listaDocumentosFortimax = new ArrayList<DocumentoFortimax>();
	private String tipoDocumento;
	private String documentosFaltantes;

	private DocAnexosService docAnexosService;
	
	@Autowired
	@Qualifier("docAnexosServiceEJB")
	public void setDocAnexosService(
			DocAnexosService docAnexosService) {
		this.docAnexosService = docAnexosService;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	

	
	@Override
	public void prepare() throws Exception {
		
	}
	
	public void prepareVerAnexos(){
		
	}
	
	public String verAnexos(){
		return SUCCESS;
	}
	
	public void prepareObtenerAnexos(){
		if(idToCotizacion != null){
			anexosList = docAnexosService.cargaAnexosPorCotizacion(idToCotizacion);
		}
	}
	
	public String obtenerAnexos(){
		return SUCCESS;
	}
	
	public void prepareModificarAnexos(){
		if(docAnexoCotDTO != null){
			docAnexoCotDTO = docAnexosService.findById(docAnexoCotDTO.getId());
		}
	}
	
	public String modificarAnexos(){
		if(docAnexoCotDTO != null){
			docAnexosService.modificarDocAnexo(docAnexoCotDTO);
			respuesta = new RespuestaGridRelacionDTO();
			respuesta.setTipoRespuesta("update");
			respuesta.setIdOriginal(gr_id);
			respuesta.setIdResultado(gr_id);
		}
		return SUCCESS;
	}


	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}



	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setAnexosList(List<DocAnexoCotDTO> anexosList) {
		this.anexosList = anexosList;
	}

	public List<DocAnexoCotDTO> getAnexosList() {
		return anexosList;
	}

	public void setDocAnexoCotDTO(DocAnexoCotDTO docAnexoCotDTO) {
		this.docAnexoCotDTO = docAnexoCotDTO;
	}

	public DocAnexoCotDTO getDocAnexoCotDTO() {
		return docAnexoCotDTO;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setGr_id(String gr_id) {
		this.gr_id = gr_id;
	}

	public String getGr_id() {
		return gr_id;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}
	
	public String getUrlIfimax() {
		return urlIfimax;
	}

	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}

	public List<DocumentoAgente> getListaAuxDocumentosAgente() {
		return listaAuxDocumentosAgente;
	}

	public void setListaAuxDocumentosAgente(
			List<DocumentoAgente> listaAuxDocumentosAgente) {
		this.listaAuxDocumentosAgente = listaAuxDocumentosAgente;
	}

	public List<DocumentoFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public void setListaDocumentosFortimax(
			List<DocumentoFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumentosFaltantes() {
		return documentosFaltantes;
	}

	public void setDocumentosFaltantes(String documentosFaltantes) {
		this.documentosFaltantes = documentosFaltantes;
	}

}