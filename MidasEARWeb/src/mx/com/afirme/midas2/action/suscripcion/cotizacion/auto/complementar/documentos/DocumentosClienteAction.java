package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.documentos;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos.DocAnexosService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/complementar/documentos")
@Component
@Scope("prototype")
public class DocumentosClienteAction extends BaseAction  implements Preparable {

	private String urlIfimax;
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private String tipoDocumento;
	private String carpetaDocumento;
	private ClienteGenericoDTO cliente = new ClienteGenericoDTO();
	private String documentosFaltantes;
	private FortimaxService fortimaxService;
	private DocAnexosService docAnexosService;
	private ClienteFacadeRemote clienteFacade;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private CotizacionDTO cotizacion;
	private EntidadService entidadService;
	
	
	private static final long serialVersionUID = 7888790343597332941L;

	@Override
	public void prepare() throws Exception {
		
	}

	@Action(value="mostrarDocumentos",results={
			@Result(name=SUCCESS,location="/jsp/suscripcion/cotizacion/auto/complementar/anexos/documentoArticulo.jsp")
		})
	public String mostrarDocumentos(){
		try {			
			cotizacion=entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
			String []respExp=docAnexosService.generateExpedientClient(cotizacion.getIdToPersonaContratante().longValue());			
			cliente.setIdCliente(cotizacion.getIdToPersonaContratante()!=null?cotizacion.getIdToPersonaContratante():null);			
			cliente=clienteFacade.loadById(cliente);
			if(!documentoEntidadService.existeEstructuraPorEntidadAplicacion(cotizacion.getIdToPersonaContratante().longValue(), "CLIENTES")){				
				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorAplicacionTipoPersona("CLIENTES",cliente.getClaveTipoPersona().longValue());
					for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
						if(doc!=null){
							String[]respDoc=fortimaxService.generateDocument(cliente.getIdCliente().longValue(), "CLIENTES", doc.getNombreDocumentoFortimax()+"_"+cliente.getIdCliente().longValue(), doc.getCarpeta().getNombreCarpetaFortimax());
							if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")&&!respDoc[0].toString().equals("false")){ 
								DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
								docFortimax.setCatalogoDocumentoFortimax(doc);
								docFortimax.setExisteDocumento(0);
								docFortimax.setIdRegistro(cliente.getIdCliente().longValue());
								documentoEntidadService.save(docFortimax);
							}
						}
					}			
			}
			else{
				documentoEntidadService.sincronizarDocumentos(cotizacion.getIdToPersonaContratante().longValue(), "CLIENTES", "ARTICULO 140");
			}
			listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(cotizacion.getIdToPersonaContratante().longValue(), "CLIENTES", "ARTICULO 140");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(value="guardarDocumentos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","documentosFaltantes"})
		})
	public String guardarDocumentos() {
	try{		
		cotizacion=entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
		listaDocumentosFortimax=documentoEntidadService.getListaDocumentosFaltantes(cotizacion.getIdToPersonaContratante().longValue(), "CLIENTES", "ARTICULO 140");
		StringBuilder docsFaltantes = new StringBuilder();
		if(!listaDocumentosFortimax.isEmpty()){
			for(DocumentoEntidadFortimax docFaltante: listaDocumentosFortimax){
				docsFaltantes.append(docFaltante.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax());
				docsFaltantes.append(",");
			}
			setDocumentosFaltantes(docsFaltantes.toString().substring(0,docsFaltantes.length()-1));
		}
	} catch (Exception e) {
		e.printStackTrace();
		setMensajeError("Error al Guardar");
		return INPUT;
	}
	return SUCCESS;
	}
	
	@Action(value="matchDocumentosCliente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
	public String matchDocumentosCliente() {
		try {				
			cotizacion=entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
			documentoEntidadService.sincronizarDocumentos(cotizacion.getIdToPersonaContratante().longValue(), "CLIENTES", "ARTICULO 140");
			listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(cotizacion.getIdToPersonaContratante().longValue(), "CLIENTES", "ARTICULO 140");
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
		} catch (Exception e) {
			e.printStackTrace();
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value="generarLigaIfimax",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp")
		})
		public String generarLigaIfimax(){
		String []resp= new String[3];
		try {
			cotizacion=entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
			ClienteGenericoDTO cliente= new ClienteGenericoDTO();
			cliente.setIdCliente(cotizacion.getIdToPersonaContratante());
			cliente=clienteFacade.loadById(cliente);
//			List<CatalogoDocumentoFortimax> docsCarpeta=documentoCarpetaService.obtenerDocumentosRequeridosPorCarpetaConTipoPersona("CLIENTES", "ARTICULO 140", cliente.getClaveTipoPersona().longValue());		
//			resp=fortimaxService.generateLinkToDocument(cliente.getIdCliente().longValue(),"CLIENTES", docsCarpeta.get(0).getNombreDocumentoFortimax()+"_"+cliente.getIdCliente().longValue());
			resp=fortimaxService.generateLinkToDocument(cliente.getIdCliente().longValue(),"CLIENTES", "");
			urlIfimax=resp[0];
		} catch (Exception e) {
			e.printStackTrace();
			return INPUT;
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}	
//		if(resp[0]==null||resp[0].equals("")){			
//			return INPUT;
//		}
		return SUCCESS;
		
		}
	
	public String getUrlIfimax() {
		return urlIfimax;
	}

	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumentosFaltantes() {
		return documentosFaltantes;
	}

	public void setDocumentosFaltantes(String documentosFaltantes) {
		this.documentosFaltantes = documentosFaltantes;
	}
	
	public String getCarpetaDocumento() {
		return carpetaDocumento;
	}

	public void setCarpetaDocumento(String carpetaDocumento) {
		this.carpetaDocumento = carpetaDocumento;
	}

	public void setListaDocumentosFortimax(
			List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}

	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public ClienteGenericoDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteGenericoDTO cliente) {
		this.cliente = cliente;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}
	
	@Autowired
	@Qualifier("docAnexosServiceEJB")
	public void setDocAnexosService(
			DocAnexosService docAnexosService) {
		this.docAnexosService = docAnexosService;
	}

	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
