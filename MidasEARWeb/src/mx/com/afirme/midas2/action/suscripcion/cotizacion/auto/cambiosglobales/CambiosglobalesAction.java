package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.cambiosglobales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cambiosglobales.ConfiguracionPlantillaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cambiosglobales.CambiosglobalesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CambiosglobalesAction extends BaseAction  implements Preparable{
	private static final long serialVersionUID = 1L;	
	private BigDecimal id;
	private CotizacionDTO cotizacion;
	private NegocioSeccion negocioSeccion;
	private ListadoService listadoService;
	private List<CoberturaCotizacionDTO> coberturaCotizacionList;

	private Long idToNegPaqueteSeccion;
	private BigDecimal idToNegSeccion;
	private String comentarios;
	
	protected CotizacionFacadeRemote cotizacionFacadeRemote;
	private CoberturaSeccionDTO coberturaSeccionDTO;
	private CoberturaDTO coberturaDTO;
	private CambiosglobalesService cambiosglobalesService;
	private CoberturaCotizacionDTO coberturaCotizacionDTO;
	private IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote;
	private int inciso;
	private List<Paquete> paqueteList;
	private ConfiguracionPlantillaService configuracionPlantillaService;
	private ConfiguracionPlantillaNeg configuracionPlantillaNeg;

    public Long paquete_id;
	private NegocioSeccionService negocioSeccionService;
	private EntidadService entidadService;
	private List<NegocioSeccion> negocioSeccionList;
	private Map<Long, String> negocioPaqueteSeccionList;


	@Override
	public void prepare() throws Exception {
		
	}	

	public String listar(){		
		return SUCCESS;
	}


	public String verDetallelineaNegocio(){

		if(getId() != null){
			CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, getId());
			negocioSeccionList = negocioSeccionService.getSeccionListByCotizacion(cotizacionDTO);
		}	
		
		if(idToNegSeccion != null){
			negocioPaqueteSeccionList = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idToNegSeccion);
		}else{
			negocioPaqueteSeccionList = new LinkedHashMap<Long, String>();
		}
				
		return SUCCESS;
	}
	
	
	
	public String prepareMostrarCoberturaCotizacion(){
		if(getId()!=null){
	      cotizacion = cotizacionFacadeRemote.findById(getId());
	      }
		return SUCCESS;
	}
	
	public void cargarListadoCoberturas(){
		coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>();
		try{
			if(idToNegPaqueteSeccion != null){				
				inciso                  =incisoCotizacionFacadeRemote.contarIncisosPorCotizacion(getId());
				coberturaCotizacionList =cambiosglobalesService.obtenerCoberturasPlantilla(idToNegPaqueteSeccion,getId());
				if(getId() != null && idToNegSeccion != null && idToNegPaqueteSeccion != null){
					configuracionPlantillaNeg =  
						configuracionPlantillaService.obtenerPlantilla(getId(), idToNegSeccion, new BigDecimal(idToNegPaqueteSeccion));
					if(configuracionPlantillaNeg != null){
						comentarios = configuracionPlantillaNeg.getComentario();
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}


	public String mostrarCoberturaCotizacion(){
		cargarListadoCoberturas();
		return SUCCESS;
	}
	
	
    public String aplicarPlantilla(){
    	try{
    		List<String> invalidos = configuracionPlantillaService.aplicarPlantilla(getId(), idToNegSeccion, idToNegPaqueteSeccion, paquete_id);
    		if(!invalidos.isEmpty()){
    			super.setMensajeListaPersonalizado("La plantilla fue aplicada, excepto a los siguientes por antig\u00FCedad: ", 
    					invalidos, TIPO_MENSAJE_EXITO);
    		}else{
    			super.setMensajeExitoPersonalizado("La plantilla fue aplicada correctamente a los incisos seleccionados");
    		}
    	}catch(Exception e){
    		super.setMensajeError(e.getCause().getMessage());
    	}
    	super.setNextFunction("verDetalleCotizacionFormal("+id+")");
    	return SUCCESS;
    }
    
    
    public String getPaquete(){
    	paqueteList = cambiosglobalesService.getPaquete(getId());
    	return SUCCESS;
    }
    
    public String guardarConfiguracion(){
    	configuracionPlantillaNeg =configuracionPlantillaService.guardarConfiguracion(getId(), idToNegSeccion, new BigDecimal(idToNegPaqueteSeccion), comentarios);
    	configuracionPlantillaService.eliminarConfiguracionDeCoberturas(getId(), idToNegSeccion, idToNegPaqueteSeccion);
    	cambiosglobalesService.getCoberturas(coberturaCotizacionList,configuracionPlantillaNeg.getPlantillaId(),coberturaCotizacionDTO);
    	super.setMensajeExito();
      return SUCCESS;
    }
    
    
    public String reestablecerValoresDelNegocio(){
    	try{
    		System.out.println("ACTION");
    		cambiosglobalesService.reestablecerValoresDelNegocio(id, idToNegSeccion,idToNegPaqueteSeccion);    		
    		super.setMensajeExitoPersonalizado("Los valores fueron restablecidos en la cotizaci\u00F3n");
    	}catch(Exception ex){
    		super.setMensajeError(MENSAJE_ERROR_GENERAL);
    	}
      return SUCCESS;
    }

	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	


	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}
	public NegocioSeccion getNegocioSeccion() {
		return negocioSeccion;
	}


	public void setNegocioSeccion(NegocioSeccion negocioSeccion) {
		this.negocioSeccion = negocioSeccion;
	}

	
	public ListadoService getListadoService() {
		return listadoService;
	}
	
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionList() {
		return coberturaCotizacionList;
	}

	public void setCoberturaCotizacionList(
			List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		this.coberturaCotizacionList = coberturaCotizacionList;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}	
	
	public Long getIdToNegPaqueteSeccion() {
		return idToNegPaqueteSeccion;
	}

	public void setIdToNegPaqueteSeccion(Long idToNegPaqueteSeccion) {
		this.idToNegPaqueteSeccion = idToNegPaqueteSeccion;
	}

	public BigDecimal getIdToNegSeccion() {
		return idToNegSeccion;
	}

	public void setIdToNegSeccion(BigDecimal idToNegSeccion) {
		this.idToNegSeccion = idToNegSeccion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public CoberturaSeccionDTO getCoberturaSeccionDTO() {
		return coberturaSeccionDTO;
	}

	public void setCoberturaSeccionDTO(CoberturaSeccionDTO coberturaSeccionDTO) {
		this.coberturaSeccionDTO = coberturaSeccionDTO;
	}
	
	public CoberturaDTO getCoberturaDTO() {
		return coberturaDTO;
	}

	public void setCoberturaDTO(CoberturaDTO coberturaDTO) {
		this.coberturaDTO = coberturaDTO;
	}		

	public CoberturaCotizacionDTO getCoberturaCotizacionDTO() {
		return coberturaCotizacionDTO;
	}

	public void setCoberturaCotizacionDTO(
			CoberturaCotizacionDTO coberturaCotizacionDTO) {
		this.coberturaCotizacionDTO = coberturaCotizacionDTO;
	}
	
	public int getInciso() {
		return inciso;
	}

	public void setInciso(int inciso) {
		this.inciso = inciso;
	}	

	public List<Paquete> getPaqueteList() {
		return paqueteList;
	}

	public void setPaqueteList(List<Paquete> paqueteList) {
		this.paqueteList = paqueteList;
	}

	public ConfiguracionPlantillaNeg getConfiguracionPlantillaNeg() {
		return configuracionPlantillaNeg;
	}

	public void setConfiguracionPlantillaNeg(
			ConfiguracionPlantillaNeg configuracionPlantillaNeg) {
		this.configuracionPlantillaNeg = configuracionPlantillaNeg;
	}

	public Long getPaquete_id() {
		return paquete_id;
	}

	public void setPaquete_id(Long paquete_id) {
		this.paquete_id = paquete_id;
	}

	@Autowired
	@Qualifier("cotizacionFacadeRemoteEJB")
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}

	@Autowired
	@Qualifier("cambiosglobalesServiceEJB")
	public void setCambiosglobalesService(
			CambiosglobalesService cambiosglobalesService) {
		this.cambiosglobalesService = cambiosglobalesService;
	}

	@Autowired
	@Qualifier("incisoCotizacionFacadeRemoteEJB")
	public void setIncisoCotizacionFacadeRemote(
			IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote) {
		this.incisoCotizacionFacadeRemote = incisoCotizacionFacadeRemote;
	}

	@Autowired
	@Qualifier("configuracionPlantillaService")
	public void setConfiguracionPlantillaService(
			ConfiguracionPlantillaService configuracionPlantillaService) {
		this.configuracionPlantillaService = configuracionPlantillaService;
	}

	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public List<NegocioSeccion> getNegocioSeccionList() {
		return negocioSeccionList;
	}

	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}

	public Map<Long, String> getNegocioPaqueteSeccionList() {
		return negocioPaqueteSeccionList;
	}

	public void setNegocioPaqueteSeccionList(
			Map<Long, String> negocioPaqueteSeccionList) {
		this.negocioPaqueteSeccionList = negocioPaqueteSeccionList;
	}
}
