package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.autoexpedibles;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesDetalleAutoCot;
import mx.com.afirme.midas2.excels.GeneraExcelAutoExpedibles;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasiva;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AutoExpediblesAction extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private BigDecimal idToControlArchivo;
	private BigDecimal idToAutoExpediblesAutoCot;
	private BigDecimal idToNegocio;
	private BigDecimal idToNegProducto;
	private BigDecimal idToNegTipoPoliza;
	private NegocioTipoPoliza negocioTipoPoliza;
	private String origen;
	private ControlArchivoDTO controlArchivo;
	private AutoExpediblesAutoCot autoExpediblesAutoCot;
	private List<AutoExpediblesAutoCot> autoExpediblesList;
	private List<AutoExpediblesDetalleAutoCot> autoExpediblesDetalleList;
	private Boolean logErrors = false;
	private Short tipoCarga = 1;
	private Short claveTipo;
	
	private List<Negocio> negocioList;
	private List<NegocioProducto> productoList = new ArrayList<NegocioProducto>(1);
	private List<NegocioTipoPoliza> tiposPoliza = new ArrayList<NegocioTipoPoliza>(1);
	
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	
	private EntidadService entidadService;
	private CargaMasivaService cargaMasivaService;
	private NegocioService negocioService;
	 private ClientesApiService  clienteRest;
	   
		@Autowired
		@Qualifier("clientesApiServiceEJB")
		public void setClienteRest(ClientesApiService clienteRest) {
			this.clienteRest = clienteRest;
		}
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("cargaMasivaServiceEJB")
	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	@Override
	public void prepare() throws Exception {
		if(idToNegTipoPoliza != null){
			negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
		}
		//negocios activos
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio("A");
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioList = negocioService.findByFilters(negocio);	
		if(idToNegocio != null){
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("negocio.idToNegocio", idToNegocio);
			parametros.put("productoDTO.claveEstatus",(short)1);
			productoList = entidadService.findByProperties(NegocioProducto.class, parametros);
		}
		if(idToNegProducto != null){
			tiposPoliza = entidadService.findByProperty(NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", idToNegProducto);
		}
	}
	
	
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	public void prepareMostrarAutoExpediblesCotizacion(){
	}
	
	public String mostrarAutoExpediblesCotizacion(){
		return SUCCESS;
	}
	
	public String mostrarAutoExpediblesEmision(){
		return SUCCESS;
	}
	
	public void prepareListarAutoExpedibles(){
		if((claveTipo != null &&  idToNegocio != null && idToNegProducto != null && idToNegTipoPoliza != null) ||
				claveTipo.equals(AutoExpediblesAutoCot.TIPO_EMISION)){
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("claveTipo", claveTipo);
			if(claveTipo != null &&  idToNegocio != null && idToNegProducto != null && idToNegTipoPoliza != null){
				parametros.put("idNegocio",idToNegocio);
				parametros.put("idToNegProducto",idToNegProducto);
				parametros.put("idToNegTipoPoliza",idToNegTipoPoliza);	
			}
			autoExpediblesList = entidadService.findByProperties(AutoExpediblesAutoCot.class, parametros);
			//Sort
			if(autoExpediblesList != null && !autoExpediblesList.isEmpty()){
				Collections.sort(autoExpediblesList, 
						new Comparator<AutoExpediblesAutoCot>() {				
							public int compare(AutoExpediblesAutoCot n1, AutoExpediblesAutoCot n2){
								return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
							}
						});
			}
		}else{
			autoExpediblesList = new ArrayList<AutoExpediblesAutoCot>(1);
		}
		
	}
	
	public String listarAutoExpedibles(){
		return SUCCESS;
	}
	
	public void prepareMostrarDetalleAutoExpedibles(){
		if(idToAutoExpediblesAutoCot != null){
			autoExpediblesAutoCot = entidadService.findById(AutoExpediblesAutoCot.class, idToAutoExpediblesAutoCot);
		}
	}
	
	public String mostrarDetalleAutoExpedibles(){
		return SUCCESS;
	}
	
	public void prepareListarDetalleAutoExpedibles(){
		if(idToAutoExpediblesAutoCot != null){
			autoExpediblesAutoCot = entidadService.findById(AutoExpediblesAutoCot.class, idToAutoExpediblesAutoCot);
			autoExpediblesDetalleList = cargaMasivaService.getAutoExpediblesDetalleList(idToAutoExpediblesAutoCot);
		}else{
			autoExpediblesDetalleList = new ArrayList<AutoExpediblesDetalleAutoCot>(1);
		}
		
	}
	
	public String listarDetalleAutoExpedibles(){
		return SUCCESS;
	}
	
	public String descargarPlantilla(){
		GeneraExcelAutoExpedibles plantilla = new GeneraExcelAutoExpedibles(cargaMasivaService,clienteRest);
		
			plantilla.setNegocioTipoPoliza(negocioTipoPoliza);
			plantilla.setCargaMasivaService(cargaMasivaService);
			try {
				plantillaInputStream = plantilla.generaPlantillaAutoExpedibles();
				contentType = "application/vnd.ms-excel";
				setFileName("CargaMasivaInd" + negocioTipoPoliza.getIdToNegTipoPoliza() + ".xls");	
			} catch (IOException e) {
				e.printStackTrace();
			}
		return SUCCESS;
	}
	
	public String descargarLogErrores(){
		GeneraExcelCargaMasiva plantilla = new GeneraExcelCargaMasiva(cargaMasivaService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			FileInputStream plantillaBytes = plantilla.descargaLogErrores(idToControlArchivo);
			
			plantillaInputStream = plantillaBytes;
			contentType = "application/text";
			setFileName("LogErroresAutoExpedibles.txt");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;		
	}
	
	public void prepareValidaAutoExpedibles(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public String validaAutoExpedibles(){
		if(controlArchivo != null){
			AutoExpediblesAutoCot cargaMasiva = cargaMasivaService.guardaAutoExpedibles(idToNegocio, 
					idToNegProducto, idToNegTipoPoliza, controlArchivo, this.getUsuarioActual().getNombreUsuario(), tipoCarga, claveTipo);
			
			GeneraExcelAutoExpedibles plantilla = new GeneraExcelAutoExpedibles(cargaMasivaService,clienteRest);
			try {
				plantilla.setNegocioTipoPoliza(negocioTipoPoliza);
				plantilla.setCargaMasivaService(cargaMasivaService);
				Usuario usuario = this.getUsuarioActual();
				plantilla.setIdUsuario(usuario.getId().longValue());
				plantilla.validaAutoExpedibles(idToControlArchivo, tipoCarga, claveTipo);
				
				cargaMasivaService.guardaDetalleAutoExpedibles(cargaMasiva, plantilla.getDetalleList());
				
				setLogErrors(plantilla.isHasLogErrors());
				if(!plantilla.isHasLogErrors()){
					if(tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_NORMAL)){
						cargaMasiva.setClaveEstatus(AutoExpediblesAutoCot.ESTATUS_TERMINADO);
					}
					if(tipoCarga.equals(GeneraExcelCargaMasiva.TIPO_CARGA_SCHEDULE)){
						cargaMasiva.setClaveEstatus(AutoExpediblesAutoCot.ESTATUS_PENDIENTE);
					}
				}else{
					cargaMasiva.setClaveEstatus(AutoExpediblesAutoCot.ESTATUS_CON_ERROR);
				}
				
				entidadService.save(cargaMasiva);
				
			}catch(Exception e){
				e.printStackTrace();
				setLogErrors(plantilla.isHasLogErrors());
				cargaMasiva.setClaveEstatus(AutoExpediblesAutoCot.ESTATUS_CON_ERROR);
				entidadService.save(cargaMasiva);
			}
		}
		String retorno = SUCCESS;
		if(claveTipo.equals(AutoExpediblesAutoCot.TIPO_EMISION)){
			retorno = "emision";
		}
		return retorno;
	}
	
	public void prepareValidaEmisionIndividual(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getId() {
		return id;
	}


	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getOrigen() {
		return origen;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setLogErrors(Boolean logErrors) {
		this.logErrors = logErrors;
	}

	public Boolean getLogErrors() {
		return logErrors;
	}


	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public void setClaveTipo(Short claveTipo) {
		this.claveTipo = claveTipo;
	}

	public Short getClaveTipo() {
		return claveTipo;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}


	public void setIdToNegocio(BigDecimal idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public BigDecimal getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegProducto(BigDecimal idToNegProducto) {
		this.idToNegProducto = idToNegProducto;
	}

	public BigDecimal getIdToNegProducto() {
		return idToNegProducto;
	}
	
	public void setIdToNegTipoPoliza(BigDecimal idToNegTipoPoliza) {
		this.idToNegTipoPoliza = idToNegTipoPoliza;
	}

	public BigDecimal getIdToNegTipoPoliza() {
		return idToNegTipoPoliza;
	}

	public void setNegocioTipoPoliza(NegocioTipoPoliza negocioTipoPoliza) {
		this.negocioTipoPoliza = negocioTipoPoliza;
	}

	public NegocioTipoPoliza getNegocioTipoPoliza() {
		return negocioTipoPoliza;
	}

	public void setProductoList(List<NegocioProducto> productoList) {
		this.productoList = productoList;
	}

	public List<NegocioProducto> getProductoList() {
		return productoList;
	}

	public void setTiposPoliza(List<NegocioTipoPoliza> tiposPoliza) {
		this.tiposPoliza = tiposPoliza;
	}

	public List<NegocioTipoPoliza> getTiposPoliza() {
		return tiposPoliza;
	}

	public void setIdToAutoExpediblesAutoCot(BigDecimal idToAutoExpediblesAutoCot) {
		this.idToAutoExpediblesAutoCot = idToAutoExpediblesAutoCot;
	}

	public BigDecimal getIdToAutoExpediblesAutoCot() {
		return idToAutoExpediblesAutoCot;
	}

	public void setAutoExpediblesAutoCot(AutoExpediblesAutoCot autoExpediblesAutoCot) {
		this.autoExpediblesAutoCot = autoExpediblesAutoCot;
	}

	public AutoExpediblesAutoCot getAutoExpediblesAutoCot() {
		return autoExpediblesAutoCot;
	}

	public void setAutoExpediblesList(List<AutoExpediblesAutoCot> autoExpediblesList) {
		this.autoExpediblesList = autoExpediblesList;
	}

	public List<AutoExpediblesAutoCot> getAutoExpediblesList() {
		return autoExpediblesList;
	}

	public void setAutoExpediblesDetalleList(
			List<AutoExpediblesDetalleAutoCot> autoExpediblesDetalleList) {
		this.autoExpediblesDetalleList = autoExpediblesDetalleList;
	}

	public List<AutoExpediblesDetalleAutoCot> getAutoExpediblesDetalleList() {
		return autoExpediblesDetalleList;
	}

}
