package mx.com.afirme.midas2.action.suscripcion.excepcion.autos;

import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ExcepcionSuscripcionZonaAction extends ExcepcionSuscripcionAction implements Preparable{
	
	private static final long serialVersionUID = 4434248582524442211L;
	
	private String estadoId = null;
	
	private String municipioId = null;

	private Map<String, String> estados = null;
	
	private Map<String, String> municipios = null;

	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public void prepareMostrarZona(){
		excepcionId = super.obtenerExcepcion();
		if(excepcionId != null){
			estadoId = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.ESTADO);
			municipioId = excepcionService.obtenerCondicionValor(excepcionId, TipoCondicion.MUNICIPIO);			
		}
		if(estados == null){
			estados = listadoService.getMapEstadosMX();
		}
		if(municipios == null){
			if(estadoId != null){
				municipios = listadoService.getMapMunicipiosPorEstado(estadoId);
			}else{
				municipios =  new LinkedHashMap<String, String>(); 
			}
		}				
	}
	
	public String mostrarZona() {		
		return SUCCESS;
	}
	
	public String guardarZona(){
		try{
			excepcionId = super.obtenerExcepcion();	
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.ESTADO, estadoId);
			excepcionService.agregarCondicion(excepcionId, TipoCondicion.MUNICIPIO, municipioId);
			super.setMensajeExitoPersonalizado("Condici�n agregada a la excepci�n");
			// Condici�n Agregada con exito
			super.setAgregada(true);
		}catch(Exception ex){
			super.setMensajeError("No se puedo agregar la condici�n a la excepci�n en este momento");
		}
		return SUCCESS;
	}

	public String getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(String estadoId) {
		this.estadoId = estadoId;
	}

	public String getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(String municipioId) {
		this.municipioId = municipioId;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public Map<String, String> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(Map<String, String> municipios) {
		this.municipios = municipios;
	}
	
}
