package mx.com.afirme.midas2.action.suscripcion.solicitud.autorizacion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.IncisoNumeroSerieDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class AutorizacionBaseAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;

	private SolicitudAutorizacionService autorizacionService;
	private List<SolicitudExcepcionCotizacion> solicitudes = new ArrayList<SolicitudExcepcionCotizacion>(1);
	private List<SolicitudExcepcionDetalle> solicitudesDetalle;
	private SolicitudExcepcionCotizacion solicitudExcepcionCot = new SolicitudExcepcionCotizacion();
	private SolicitudExcepcionDetalle solicitudExcepcionDetalle;
	private Date fechaSolicitudHasta;
	private Short estatus;
	private Long idDetalle;
	private Long idSolicitud;
	private Long idExcepcion;
	private String observacion;
	private ExcepcionSuscripcionNegocioDescripcionDTO excepcion;
	private EntidadService entidadService;
	private IncisoService incisoService;
	private Long numeroSecuencia;
	private List<IncisoNumeroSerieDTO> incisoNumeroSerieDTOs;
	private String descripcionBusquedaUsuario;
	private List<Usuario> listResultUsuarios;
	private static final int REGISTROS_A_MOSTRAR_REN = 20;
	private Short tipoAutorizacion;
	private boolean usuarioControlDeshabilitado = false;
	
	@Autowired
	@Qualifier("solicitudAutorizacionService")
	public void setAutorizacionService(
			SolicitudAutorizacionService autorizacionService) {
		this.autorizacionService = autorizacionService;
	}
	
	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	@Autowired
	@Qualifier("entidadEJB")
	public void setCatalogoService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	

	@Override
	public void prepare() throws Exception {
		

	}
	public void prepareMostrarContenedor() {

	}

	public String mostrarContenedor() {
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			Usuario usuario = usuarioService.getUsuarioActual();
			solicitudExcepcionCot.setUsuarioSolicitante(usuario.getId().longValue());
			solicitudExcepcionCot.setNombreSolicitante(usuario.getNombreCompleto());
			setUsuarioControlDeshabilitado(true);
		}else{
			setUsuarioControlDeshabilitado(false);
		}
		return SUCCESS;
	}

	public void prepareFiltrarSolicitud() {

	}
	
	public String filtrarSolicitud() {
		
		
		return SUCCESS;
	}
	
	public String listarSolicitudesPaginado(){
		if(solicitudExcepcionCot != null){
			if(getTotalCount() == null){
				if(solicitudExcepcionCot.getId() == null){
					solicitudExcepcionCot.setId(idSolicitud);
				}
				if(solicitudExcepcionCot.getEstatus() == null){
					solicitudExcepcionCot.setEstatus(SolicitudAutorizacionService.EstatusSolicitud.EN_PROCESO.valor());				
				}else if(solicitudExcepcionCot.getEstatus().shortValue() == -1){
					solicitudExcepcionCot.setEstatus(null);
				}	
				setTotalCount(autorizacionService.listarSolicitudesCount(solicitudExcepcionCot, fechaSolicitudHasta));
				setListadoEnSession(null);
			}
			setPosPaginado(REGISTROS_A_MOSTRAR_REN);
		}
		return SUCCESS;
	}
	
	public void prepareListarSolicitudes(){
		
	}
	
	@SuppressWarnings("unchecked")
	public String listarSolicitudes() {
		if(solicitudExcepcionCot != null){
			if(!listadoDeCache()){
				if(solicitudExcepcionCot.getId() == null){
					solicitudExcepcionCot.setId(idSolicitud);
				}
				if(solicitudExcepcionCot.getEstatus() == null){
					solicitudExcepcionCot.setEstatus(SolicitudAutorizacionService.EstatusSolicitud.EN_PROCESO.valor());				
				}else if(solicitudExcepcionCot.getEstatus().shortValue() == -1){
					solicitudExcepcionCot.setEstatus(null);
				}			
				solicitudExcepcionCot.setPrimerRegistroACargar(getPosStart());
				solicitudExcepcionCot.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR_REN);
				solicitudes = autorizacionService.listarSolicitudes(solicitudExcepcionCot, fechaSolicitudHasta);
			}else{
				solicitudes = (List<SolicitudExcepcionCotizacion>) getListadoPaginado();
				setListadoEnSession(solicitudes);
			}
		}else {
			solicitudes = new ArrayList<SolicitudExcepcionCotizacion>();
		}		
		return SUCCESS;
	}
	
	public void prepareListarSolicitudesDetalle(){
		if(solicitudExcepcionCot == null || solicitudExcepcionCot.getId() == null){
			solicitudExcepcionCot = new SolicitudExcepcionCotizacion();
			solicitudExcepcionCot.setId(idSolicitud);
		}
		solicitudExcepcionCot = autorizacionService.obtenerSolicitud(solicitudExcepcionCot.getId());			
		solicitudesDetalle = autorizacionService.listarDetalleParaSolicitud(solicitudExcepcionCot.getId());
		for(SolicitudExcepcionDetalle item : solicitudesDetalle){
			if(item.getIdSeccion() != null){
				NegocioSeccion seccion = entidadService.findById(NegocioSeccion.class, item.getIdSeccion());
				if(seccion != null){
					item.setDescripcionSeccion(seccion.getSeccionDTO().getDescripcion());
				}
			}
			if(item.getIdCobertura() != null){
				CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, item.getIdCobertura());
				if(cobertura != null){
					item.setDescripcionCobertura(cobertura.getDescripcion());
				}
			}else{
				item.setDescripcionCobertura("NA");
			}
			if(item.getIdInciso() != null){
				IncisoCotizacionId id = new IncisoCotizacionId();
				id.setIdToCotizacion(solicitudExcepcionCot.getToIdCotizacion());
				id.setNumeroInciso(item.getIdInciso());
				IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
				if(inciso != null){
					item.setNumeroSecuencia(inciso.getNumeroSecuencia());
					item.setDescripcionInciso(inciso.getIncisoAutoCot().getDescripcionFinal());
				}
			}			
		}
	}
	
	public String listarSolicitudesDetalle(){		
		return SUCCESS;
	}
	
	public String actualizaSolicitudesDetalle(){
		try{
			if(estatus == 1){
				autorizacionService.autorizarDetalle(idSolicitud, idDetalle, observacion);
			}else if(estatus == 2){
				autorizacionService.rechazarDetalle(idSolicitud, idDetalle, observacion);
			}else if(estatus == 3){
				autorizacionService.cancelarDetalle(idSolicitud, idDetalle, observacion);
			}
			setMensajeExito();
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}		
		return SUCCESS;
	}
	
	public String terminarSolicitudAutorizacion(){
		try{
			idSolicitud = solicitudExcepcionCot.getId();
			Usuario usuario = this.getUsuarioActual();
			autorizacionService.terminarSolicitud(solicitudExcepcionCot.getId(), solicitudesDetalle, usuario.getId().longValue());
			super.setMensajeExitoPersonalizado(getText("midas.suscripcion.solicitud.autorizacion.solicitudTerminada", 
					new String[]{solicitudExcepcionCot.getId().toString()}));
		}catch(Exception ex){
			super.setMensajeError(getText("midas.suscripcion.solicitud.autorizacion.detallesEnProceso"));
		}
		return SUCCESS;
	}
	
	public String terminarSolicitudAutorizacionMasiva(){
		if(solicitudes != null && solicitudes.size() > 0 && tipoAutorizacion != null){
			try{
				Usuario usuario = this.getUsuarioActual();
				autorizacionService.terminarSolicitudMasiva(solicitudes, usuario.getId().longValue(), tipoAutorizacion);
				super.setMensajeExito();
			}catch(Exception ex){
				super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR + " - " + ex.getMessage());
			}
		}
		return SUCCESS;
	}
	
	public String verExcepcionDetalle(){
		setExcepcion(autorizacionService.verDetalleExcepcion(idExcepcion));
		return SUCCESS;
	}
	
	
	public String cancelarSolicitud(){
		try{
			Usuario usuario = this.getUsuarioActual();
			autorizacionService.cancelarSolicitud(idSolicitud, usuario.getId().longValue());
			super.setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(MENSAJE_ERROR_ELIMINAR);
		}
		return SUCCESS;
	}
	
	public void prepareVerExcepcionSerieDetalle() {

		if (solicitudExcepcionCot.getToIdCotizacion() != null) {
			// Obtiene la Cotizacion
			CotizacionDTO cotizacionDTO = entidadService.findById(
					CotizacionDTO.class,
					solicitudExcepcionCot.getToIdCotizacion());
			// Obtiene el inciso
			IncisoCotizacionDTO filtro = new IncisoCotizacionDTO();
			IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
			incisoCotizacionId.setIdToCotizacion(solicitudExcepcionCot
					.getToIdCotizacion());
			incisoCotizacionId.setNumeroInciso(solicitudExcepcionDetalle
					.getIdInciso());
			filtro.setId(incisoCotizacionId);
			filtro = incisoService.listarIncisosConFiltro(filtro).get(0);
						
			
			// Verifica si existen repetidos
			if (incisoService.existenNumeroSerieRepetidos(solicitudExcepcionCot
					.getToIdCotizacion(), filtro.getId().getNumeroInciso(), 
					filtro.getIncisoAutoCot().getNumeroSerie(), cotizacionDTO
					.getFechaInicioVigencia(), cotizacionDTO
					.getFechaFinVigencia())) {
				incisoNumeroSerieDTOs = incisoService.numerosSerieRepetidos(
						solicitudExcepcionCot.getToIdCotizacion(),
						filtro.getId().getNumeroInciso(),
						filtro.getIncisoAutoCot().getNumeroSerie(),
						cotizacionDTO.getFechaInicioVigencia(),
						cotizacionDTO.getFechaFinVigencia());
			}
		}
		// Crea una instancia si no existen repetidos
		if (incisoNumeroSerieDTOs == null) {
			incisoNumeroSerieDTOs = new ArrayList<IncisoNumeroSerieDTO>();
		}
	}
	
	public String verExcepcionSerieDetalle(){
		return SUCCESS;
	}
	
	
	public String listarUsuariosSolicitud() {
		if (descripcionBusquedaUsuario != null ){
			setListResultUsuarios(autorizacionService.getListUsuarios(descripcionBusquedaUsuario));
		}else{
			listResultUsuarios = new ArrayList<Usuario>();
		}
		return SUCCESS;
	}
	
	public List<SolicitudExcepcionCotizacion> getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(List<SolicitudExcepcionCotizacion> solicitudes) {
		this.solicitudes = solicitudes;
	}


	public void setSolicitudExcepcionCot(SolicitudExcepcionCotizacion solicitudExcepcionCot) {
		this.solicitudExcepcionCot = solicitudExcepcionCot;
	}
	public SolicitudExcepcionCotizacion getSolicitudExcepcionCot() {
		return solicitudExcepcionCot;
	}
	public void setSolicitudesDetalle(List<SolicitudExcepcionDetalle> solicitudesDetalle) {
		this.solicitudesDetalle = solicitudesDetalle;
	}
	public List<SolicitudExcepcionDetalle> getSolicitudesDetalle() {
		return solicitudesDetalle;
	}
	public void setSolicitudExcepcionDetalle(SolicitudExcepcionDetalle solicitudExcepcionDetalle) {
		this.solicitudExcepcionDetalle = solicitudExcepcionDetalle;
	}
	public SolicitudExcepcionDetalle getSolicitudExcepcionDetalle() {
		return solicitudExcepcionDetalle;
	}
	public void setFechaSolicitudHasta(Date fechaSolicitudHasta) {
		this.fechaSolicitudHasta = fechaSolicitudHasta;
	}
	public Date getFechaSolicitudHasta() {
		return fechaSolicitudHasta;
	}
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	public Short getEstatus() {
		return estatus;
	}
	public void setIdDetalle(Long idDetalle) {
		this.idDetalle = idDetalle;
	}
	public Long getIdDetalle() {
		return idDetalle;
	}
	public void setIdSolicitud(Long idSolicitud) {
		this.idSolicitud = idSolicitud;
	}
	public void setIdSolicitud(String idSolicitud) {
		if(idSolicitud != null & idSolicitud != ""){
			this.idSolicitud = Long.valueOf(idSolicitud);
		}
	}
	public Long getIdSolicitud() {
		return idSolicitud;
	}
	public void setIdExcepcion(Long idExcepcion) {
		this.idExcepcion = idExcepcion;
	}
		
	public Long getIdExcepcion() {
		return idExcepcion;
	}
	public void setExcepcion(ExcepcionSuscripcionNegocioDescripcionDTO excepcion) {
		this.excepcion = excepcion;
	}
	public ExcepcionSuscripcionNegocioDescripcionDTO getExcepcion() {
		return excepcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Long getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Long numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public List<IncisoNumeroSerieDTO> getIncisoNumeroSerieDTOs() {
		return incisoNumeroSerieDTOs;
	}

	public void setIncisoNumeroSerieDTOs(
			List<IncisoNumeroSerieDTO> incisoNumeroSerieDTOs) {
		this.incisoNumeroSerieDTOs = incisoNumeroSerieDTOs;
	}

	/**
	 * @return the listResultUsuarios
	 */
	public List<Usuario> getListResultUsuarios() {
		return listResultUsuarios;
	}

	/**
	 * @param listResultUsuarios the listResultUsuarios to set
	 */
	public void setListResultUsuarios(List<Usuario> listResultUsuarios) {
		this.listResultUsuarios = listResultUsuarios;
	}

	/**
	 * @return the descripcionBusquedaUsuario
	 */
	public String getDescripcionBusquedaUsuario() {
		return descripcionBusquedaUsuario;
	}

	/**
	 * @param descripcionBusquedaUsuario the descripcionBusquedaUsuario to set
	 */
	public void setDescripcionBusquedaUsuario(String descripcionBusquedaUsuario) {
		this.descripcionBusquedaUsuario = descripcionBusquedaUsuario;
	}

	public void setTipoAutorizacion(Short tipoAutorizacion) {
		this.tipoAutorizacion = tipoAutorizacion;
	}

	public Short getTipoAutorizacion() {
		return tipoAutorizacion;
	}

	public void setUsuarioControlDeshabilitado(boolean usuarioControlDeshabilitado) {
		this.usuarioControlDeshabilitado = usuarioControlDeshabilitado;
	}

	public boolean isUsuarioControlDeshabilitado() {
		return usuarioControlDeshabilitado;
	}
	
}
