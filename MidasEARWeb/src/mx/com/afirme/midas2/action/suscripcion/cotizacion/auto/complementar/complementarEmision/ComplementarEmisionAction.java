package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.complementar.complementarEmision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO.TIPO_AGRUPACION_RECIBOS;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.cliente.ClienteAsociadoJPAService;
import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;

@Component
@Scope("prototype")
public class ComplementarEmisionAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(ComplementarEmisionAction.class);
	
	private static final String BANDERA_ENCENDIDO = "1";
	
	private BigDecimal idToCotizacion;
	private BigDecimal idCliente;
	private CotizacionDTO cotizacionDTO;
	private ClienteDTO cliente;
	private MensajeDTO mensajeDTO;
	private EntidadService entidadService;
	private CotizacionService cotizacionService;
	private ClienteFacadeRemote clienteFacade;
	private ClienteAsociadoJPAService clienteAsociadoJPAService;
	private boolean negocioAsociado;
	private Long idToNegocio;
	private Negocio negocio;
	private Short soloConsulta = 0;
	private IncisoCotizacionDTO filtro;
	private BigDecimal idTipoImpresionCliente;
	private Map<Object, String> listCatalogoTipoImpresionCliente = new LinkedHashMap<Object, String>();	
	private static final int ID_CATALOGO_TIPO_IMPRESION_CLIENTE = 340;
	private List<TIPO_AGRUPACION_RECIBOS> listTipoAgrupacionRecibos = new ArrayList<CotizacionDTO.TIPO_AGRUPACION_RECIBOS>();
	private boolean habilitarTipoRecibos = false;
	private String tipoAgrupacionRecibos;
	private boolean recuotificada = false;	
	private boolean banderaModuloRecuotificacionActivo = false;
	
	private CobranzaIncisoService cobranzaIncisoService;
	
	private NegocioRecuotificacionService negocioRecuotificacionService;
	
	private ParametroGeneralService parametroGeneralService;
	
	
	private ClientesApiService clienteRest;
	
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
	
	
	@Autowired
	@Qualifier("cobranzaIncisoServiceEJB")
	public void setCobranzaIncisoService(CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}	
	
	@Autowired
	@Qualifier("parametroGeneralServiceEJB")	
	public void setParametroGeneralService(
			ParametroGeneralService parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}

	@Autowired
	@Qualifier("clienteAsociadoJPAServiceEJB")
	public void setClienteAsociadoJPAService(
			ClienteAsociadoJPAService clienteAsociadoJPAService) {
		this.clienteAsociadoJPAService = clienteAsociadoJPAService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("negocioRecuotificacionEJB")
	public void setNegocioRecuotificacionService(NegocioRecuotificacionService negocioRecuotificacionService) {
		this.negocioRecuotificacionService = negocioRecuotificacionService;
	}
	
	
	@Override
	public void prepare() throws Exception {
		
	}
	public void obtenerTipoImpresionCliente(int tipoGrupoValor){
		try {
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
					.getInstancia()
					.buscarPorPropiedad(
							"id.idGrupoValores",
							Integer.valueOf(tipoGrupoValor));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						listCatalogoTipoImpresionCliente.put(estatus.getId().getIdDato(),
								estatus.getDescripcion());
					}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void obtener(int tipoGrupoValor){
		try {
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
					.getInstancia()
					.buscarPorPropiedad(
							"id.idGrupoValores",
							Integer.valueOf(tipoGrupoValor));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						listCatalogoTipoImpresionCliente.put(estatus.getId().getIdDato(),
								estatus.getDescripcion());
					}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	public void prepareVerComplementarEmision(){
		obtenerTipoImpresionCliente(ComplementarEmisionAction.ID_CATALOGO_TIPO_IMPRESION_CLIENTE);
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			setIdToNegocio(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
			// Valida que una Cotizacion tenga Clientes asociados al negocio
			
			Map<String,Object> negocioClienteMap = new HashMap<String,Object>();
			negocioClienteMap.put("negocio.idToNegocio", idToNegocio);
			if (entidadService.findByPropertiesCount(NegocioCliente.class, negocioClienteMap).longValue() > 0){
				setNegocioAsociado(true);
			}
			if(cotizacionDTO.getIdToPersonaContratante() != null){
				try {
					filtro.setIdCliente(cotizacionDTO.getIdToPersonaContratante());
					filtro = clienteFacade.loadByIdNoAddress(filtro);
					setCliente(filtro);
					// Set nombre de Persona moral
					if(getCliente().getClaveTipoPersona() == 2){
						getCliente().setNombre(
								filtro.getNombreCompleto());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			setListadoEnSession(null);
			
			//Agrupacion 20131216 Habilitar combo de seleccion
			if(cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas().intValue() == 1){
				prepareMostrarClientesAsociados();
				habilitarTipoRecibos = negocio.getHabilitaAgrupacionRecibos() != null && 
					negocio.getHabilitaAgrupacionRecibos().booleanValue() ? true: false;
				if(habilitarTipoRecibos){
					for(TIPO_AGRUPACION_RECIBOS tipo : Arrays.asList(TIPO_AGRUPACION_RECIBOS.values())){
						if(tipo == TIPO_AGRUPACION_RECIBOS.UBICACION || tipo == TIPO_AGRUPACION_RECIBOS.AGRUPADO){
							listTipoAgrupacionRecibos.add(tipo);
						}
					} 					
				}
			}
			
			//20170104 Modulo recuotificacion Activo
			ParametroGeneralDTO parametro = (parametroGeneralService.findByDescCode(
					ParametroGeneralDTO.GRUPO_RECUOTIFICACION_AUTOS, ParametroGeneralDTO.PARAMETRO_RECUOTIFICACION_AUTOS_MODULO_ACTIVO));			
			banderaModuloRecuotificacionActivo = parametro != null && parametro.getValor().equals(BANDERA_ENCENDIDO);
		}
	}
	
	public String verComplementarEmision(){
		obtenerTipoImpresionCliente(ComplementarEmisionAction.ID_CATALOGO_TIPO_IMPRESION_CLIENTE);
		return SUCCESS;
	}
	
	public String seleccionarContratante(){
		obtenerTipoImpresionCliente(ComplementarEmisionAction.ID_CATALOGO_TIPO_IMPRESION_CLIENTE);
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);			
			if(cotizacionService.actualizarDatosContratanteCotizacion(idToCotizacion, idCliente, this.getUsuarioActual().getNombreUsuario()) == null){
				setMensajeError("confirm:message:Favor de complementar el C\u00F3digo postal en los datos fiscales del cliente seleccionado, \uu00BFDesea hacerlo ahora\u003F");
				setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				setNextFunction("complementarContratante("+idCliente+","+idToCotizacion+")");
				return SUCCESS;
			}
			cobranzaIncisoService.validarCambioContratanteCobranzaInciso(idToCotizacion);
			
			//Validaciones Prima Mayor
			LOG.info("-- Validar pais y ocupación de cliente " + idCliente.longValue());
			try {
				ResponseEntity<Long> resp = clienteRest.clienteUnificado(null, idCliente.longValue());
				
				
				ClienteUnicoDTO clienteUnico = clienteRest.findClienteUnicoById(resp.getBody());
				
				LOG.info("-- Tipo de cliente " + clienteUnico.getClaveTipoPersonaString());
				
				if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("1")){
				
					if(clienteUnico.getOcupacionCNSF() == null || clienteUnico.getOcupacionCNSF().equals("")
							|| clienteUnico.getIdPaisNacimiento() == null || clienteUnico.getIdPaisNacimiento().equals("")){
						
						ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
						String validarPrima = entityPrima.getBody();
						
						if(validarPrima.contains("true")){
						
							setMensajeError("confirm:message:Favor de complementar el País de Nacimiento y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+idCliente+","+idToCotizacion+")");
							return SUCCESS;
						}
					}
				}else if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("2")){
					
					if(clienteUnico.getCveGiroCNSF() == null || clienteUnico.getCveGiroCNSF().equals("")
							|| clienteUnico.getIdPaisConstitucion() == null || clienteUnico.getIdPaisConstitucion().equals("")){
						
						ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
						String validarPrima = entityPrima.getBody();
						
						if(validarPrima.contains("true")){
							setMensajeError("confirm:message:Favor de complementar el País de Constituci\u00f3n y la ocupaci\u00f3n, \uu00BFDesea hacerlo ahora\u003F");
							setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
							setNextFunction("complementarContratante("+idCliente+","+idToCotizacion+")");
							return SUCCESS;
						}
					}
				}
				
				
			} catch (HttpStatusCodeException e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- seleccionarContratante():Validaciones Prima Mayor ", e);
				return ERROR;
			} catch (Exception e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- seleccionarContratante():Validaciones Prima Mayor ", e);
				return ERROR;
			}	
			
			super.setMensajeExito();
			// Si el nuevo contratante tiene un iva valida si cambio
			try{
				boolean cambioIVA = cotizacionService.validaIVACotizacion(idToCotizacion, this.getUsuarioActual().getNombreUsuario());
				if(cambioIVA){		
					super.setMensajeExitoPersonalizado("IVA ajustado debido a domicilio fiscal del Contrante, favor de terminar la cotizaci\u00f3n.");
//					setNextFunction("cambiarEstatusEnProceso("+idToCotizacion+")");
					return "changeCotizacion";	
				}
			}catch(Exception ex){
				super.setMensajeError("EL CLIENTE SELECCIONADO NO CUENTA CON INFORMACION DEL DOMICILIO, POR LO CUAL EL SISTEMA TOMARA EL IVA DEL 16%.");
				super.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				return SUCCESS;
			}			
		}
		return SUCCESS;
	}
	public String seleccionarTipoImpresionCliente(){
		if(idToCotizacion != null){
			cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			cotizacionService.actualizarTipoImpresionCliente(idToCotizacion, idTipoImpresionCliente);
		}
		return SUCCESS;
	}
	//seleccionarTipoImpresionCliente
	
	/**
	 * Agrupacion. Seleccion de tipo de agrupacion 
	 * 20131212
	 */
	public String seleccionarTipoAgrupacionRecibos(){		
		try{
			if(idToCotizacion != null){			
				cotizacionService.actualizarTipoAgrupacionRecibos(idToCotizacion, tipoAgrupacionRecibos);
				if(tipoAgrupacionRecibos != null && tipoAgrupacionRecibos.equals(CotizacionDTO.TIPO_AGRUPACION_RECIBOS.AGRUPADO.getValue())){
					cobranzaIncisoService.limpiarCobranzaIncisos(idToCotizacion);					
				}
				super.setMensajeExitoPersonalizado("El tipo de agrupaci\u00F3n de recibos fue actualizado correctamente. Si seleccion\u00F3 AGRUPADO no podr\u00E1 realizar cobranza por inciso");
			}		
		}catch(NegocioEJBExeption ex){
			super.setMensajeError(ex.getMessageClean());
			super.setTipoMensaje(BaseAction.TIPO_MENSAJE_ERROR);			
		}		
		return SUCCESS;
	}
	
	/**
	 * Validar si la cotizacion ha sido recuotificada via json
	 * @return
	 */
	public String validarCotizacionRecuotificada(){
		recuotificada = cotizacionService.validarCotizacionRecuotificada(idToCotizacion);				
		return SUCCESS;
	}
	
	public void prepareMostrarClientesAsociados() {
		negocio = entidadService.findById(Negocio.class, idToNegocio);
	}
	public String mostrarClientesAsociados() {
		return SUCCESS;
	}
	
	public String validarDatosComplementarios() {
		if(idToCotizacion != null){
			mensajeDTO = cotizacionService.validarListaParaEmitir(idToCotizacion);
		}
		return SUCCESS;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}



	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setCotizacionDTO(CotizacionDTO cotizacionDTO) {
		this.cotizacionDTO = cotizacionDTO;
	}

	public CotizacionDTO getCotizacionDTO() {
		return cotizacionDTO;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}
	
	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public boolean isNegocioAsociado() {
		return negocioAsociado;
	}

	public void setNegocioAsociado(boolean negocioAsociado) {
		this.negocioAsociado = negocioAsociado;
	}

	public Long getIdToNegocio() {
		return idToNegocio;
	}

	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	/**
	 * @return the filtro
	 */
	public IncisoCotizacionDTO getFiltro() {
		return filtro;
	}

	/**
	 * @param filtro the filtro to set
	 */
	public void setFiltro(IncisoCotizacionDTO filtro) {
		this.filtro = filtro;
	}

	public void setIdTipoImpresionCliente(BigDecimal idTipoImpresionCliente) {
		this.idTipoImpresionCliente = idTipoImpresionCliente;
	}

	public BigDecimal getIdTipoImpresionCliente() {
		return idTipoImpresionCliente;
	}
	public void setListCatalogoTipoImpresionCliente(
			Map<Object, String> listCatalogoTipoImpresionCliente) {
		this.listCatalogoTipoImpresionCliente = listCatalogoTipoImpresionCliente;
	}

	public Map<Object, String> getListCatalogoTipoImpresionCliente() {
		return listCatalogoTipoImpresionCliente;
	}

	public List<TIPO_AGRUPACION_RECIBOS> getListTipoAgrupacionRecibos() {
		return listTipoAgrupacionRecibos;
	}

	public void setListTipoAgrupacionRecibos(
			List<TIPO_AGRUPACION_RECIBOS> listTipoAgrupacionRecibos) {
		this.listTipoAgrupacionRecibos = listTipoAgrupacionRecibos;
	}

	public boolean isHabilitarTipoRecibos() {
		return habilitarTipoRecibos;
	}

	public void setHabilitarTipoRecibos(boolean habilitarTipoRecibos) {
		this.habilitarTipoRecibos = habilitarTipoRecibos;
	}

	public String getTipoAgrupacionRecibos() {
		return tipoAgrupacionRecibos;
	}

	public void setTipoAgrupacionRecibos(String tipoAgrupacionRecibos) {
		this.tipoAgrupacionRecibos = tipoAgrupacionRecibos;
	}
	
	public boolean isRecuotificada() {
		return recuotificada;
	}
	
	public void setRecuotificada(boolean recuotificada) {
		this.recuotificada = recuotificada;
	}
	
	
	public boolean getBanderaModuloRecuotificacionActivo() {
		return banderaModuloRecuotificacionActivo;
	}

	public void setBanderaModuloRecuotificacionActivo(
			boolean banderaModuloRecuotificacionActivo) {
		this.banderaModuloRecuotificacionActivo = banderaModuloRecuotificacionActivo;
	}

	
}