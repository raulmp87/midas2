package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import mx.com.afirme.midas.agente.AgenteDN;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.direccion.DireccionDTO;
import mx.com.afirme.midas.direccion.DireccionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.busquedaPaginada.BusquedaPaginada;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
public class IncisoAction extends BaseAction implements Preparable,BusquedaPaginada{

	private static final long serialVersionUID = 7238046077636282324L;
	private static final Logger LOG = Logger.getLogger(IncisoAction.class);
	private IncisoService incisoService;
	private CotizacionService cotizacionService;
	private ListadoService listadoService;
	private EntidadService entidadService;
	private CoberturaService coberturaService;
	private ClienteFacadeRemote clienteFacadeRemote;
	private DireccionFacadeRemote direccionFacadeRemote;
	private NegocioSeccionService negocioSeccionService;
	private CalculoService calculoService;
	private int numeroCopias;
	private IncisoCotizacionDTO incisoCotizacion;
	private List<IncisoCotizacionDTO> incisoCotizacionList;
	private Set<String> descripcionesIncisos;
	private List<CoberturaCotizacionDTO> coberturaCotizacionList;
	private List<EstiloVehiculoDTO> resultadosBusquedaVehiculo;
	private List<NegocioSeccion> negocioSeccionList;
	private Map<Long, String> paquetes = null;
	private String descripcionInciso = null;
	private String lineaNegocioInciso = null;
	private String idMoneda = null;
	private ResumenCostosDTO 	resumenCostosDTO;
	private String name;
	private Boolean requerido;
	private Map<String, String> valores;
	private List<ControlDinamicoRiesgoDTO> controles;
	private ConfiguracionDatoIncisoService configuracionDatoIncisoService;
	private Integer radioAsegurado;
	private BigDecimal negocioSeccionId;
	private Short soloConsulta = 0;
	private boolean mostrarAjustarCaracteristicas = true;
	private Double primaTotalAIgualar;
	private String claveEstatus;
	private Map<Object, String> diasSalario = new LinkedHashMap<Object, String>();
	private static final int ID_CATALOGO_DIAS_SALARIO = 338;
	private BigDecimal idMarca;
	private Short modeloVehiculo;
	private Short numAcientos;
	private CotizacionDTO cotizacion;
	private Short esCobInciso = 0;
	private FronterizosService fronterizosService;
	private int validoVIN;
	private String VIN;
	private BigDecimal idNegocioSeccion;
	private HashMap<String, Map<String, String>> infoVehicular;
	private String idEstado;
	private String modeloSel;
	
	private CobranzaIncisoService cobranzaIncisoService;
	
	@Autowired
	@Qualifier("cobranzaIncisoServiceEJB")
	public void setCobranzaIncisoService(CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}	
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Autowired
	@Qualifier("direccionFacadeRemoteEJB")
	public void setDireccionFacadeRemote(DireccionFacadeRemote direccionFacadeRemote) {
		this.direccionFacadeRemote = direccionFacadeRemote;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}	
			
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("coberturaServiceEJB")
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}
	
	@Autowired
	@Qualifier("negocioSeccionServiceEJB")
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	
	@Autowired
	@Qualifier("calculoIncisoAutosServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("configuracionDatoIncisoServiceEJB")
	public void setConfiguracionDatoIncisoService(ConfiguracionDatoIncisoService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;	
	}
	@Override
	public void prepare() throws Exception {
		try {
			if(incisoCotizacion == null){
				incisoCotizacion = new IncisoCotizacionDTO();
				incisoCotizacion.setId(new IncisoCotizacionId());
			} else {
				if (incisoCotizacion.getId() != null
						&& incisoCotizacion.getId().getIdToCotizacion() != null
						&& incisoCotizacion.getId().getNumeroInciso() != null) {
					incisoCotizacion = entidadService.findById(
							IncisoCotizacionDTO.class, incisoCotizacion.getId());
				}
				if (incisoCotizacion.getId() != null && incisoCotizacion.getId().getNumeroInciso() == null
						&& incisoCotizacion.getNumeroSecuencia() != null) {
					List<IncisoCotizacionDTO> listTemp = incisoService
							.listarIncisosConFiltro(incisoCotizacion);
					if (listTemp != null && !listTemp.isEmpty()) {
						incisoCotizacion = listTemp.get(0);
					} else{
						incisoCotizacion = new IncisoCotizacionDTO();
					}
					
				}
				if (incisoCotizacion.getIncisoAutoCot() != null
						&& incisoCotizacion.getIncisoAutoCot()
								.getNegocioSeccionId() != null) {
					negocioSeccionId = BigDecimal.valueOf(incisoCotizacion
							.getIncisoAutoCot().getNegocioSeccionId());
					incisoCotizacion.setIdToSeccion(negocioSeccionId);
				}
				
				if (incisoCotizacion.getId() != null
						&& incisoCotizacion.getId().getIdToCotizacion() != null) {
					CotizacionDTO cotizacionDTO = entidadService.findById(
							CotizacionDTO.class, incisoCotizacion.getId()
									.getIdToCotizacion());
					incisoCotizacion.setCotizacionDTO(cotizacionDTO);
				}
				
				//Elimina incisos no asignados
				if (incisoCotizacion.getId() != null
						&& incisoCotizacion.getId().getIdToCotizacion() != null
						&& incisoCotizacion.getId().getNumeroInciso() == null) {
					incisoService.borrarIncisoAutoCotNoAsignados(incisoCotizacion.getId().getIdToCotizacion());
				}
			}
			obtenerDiasSalario();
			obtenerNumeroAsientos();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public String verDetalleInciso(){
		return SUCCESS;
	}
	
	public String verDetalleIncisoAgente(){
		return SUCCESS;
	}

	public String mostrarMultiplicarInciso(){
		return SUCCESS;
	}
	
	public void prepareMultiplicarInciso(){
		
	}
	
	public String multiplicarInciso(){
		int restan = numeroCopias;
		try {
			for (int i = 0; i < numeroCopias; i++) {
				incisoService.multiplicarInciso(incisoCotizacion.getId(),
						numeroCopias);
				restan--;
			}
		} catch (RuntimeException e) {
			setNumeroCopias(restan);
			super.setMensajeError("Ocurrio un error en el proceso de copiado, restan " + restan + " incisos por copiar.");
			throw e;
		}
		setMensajeExito();
		return SUCCESS;
	}
	

	
	public String mostrarIgualarInciso() {
		if(incisoCotizacion != null){
			Double primaTotal = incisoService.getValorPrimaTotal(incisoCotizacion);
			incisoCotizacion.setValorPrimaTotal(primaTotal);
		}
		return SUCCESS;
	}
	
	
	public String igualarInciso() {
		LogDeMidasWeb.log("igualarInciso", Level.FINEST, null);
		try{
			calculoService.igualarPrimaInciso(incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getId()
					.getNumeroInciso(), primaTotalAIgualar);
			setMensajeExito();
			setNextFunction("cerrarVentanaModal('igualarInciso');verDetalleCotizacion("
					+ incisoCotizacion.getId().getIdToCotizacion()
					+ ","
					+ claveEstatus + ")");
		} catch( RuntimeException ex) {
			setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String restaurarPrimaInciso() {
		LogDeMidasWeb.log("retaurarPrimaInciso", Level.FINEST, null);	
		try{
			calculoService.restaurarPrimaInciso(incisoCotizacion);
			setMensajeExito();
			setNextFunction("cerrarVentanaModal('igualarInciso');verDetalleCotizacion("
				+ incisoCotizacion.getId().getIdToCotizacion()
				+ ","
				+ claveEstatus + ")");
		} catch( RuntimeException ex) {
			setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	public void obtenerNumeroAsientos(){
		if(incisoCotizacion != null && incisoCotizacion.getIncisoAutoCot() != null &&
				incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
			EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
			estiloVehiculoId.valueOf(incisoCotizacion.getIncisoAutoCot().getEstiloId());
			EstiloVehiculoDTO estiloVehiculoDTO  =  entidadService.findById(EstiloVehiculoDTO.class, estiloVehiculoId);
			setNumAcientos(estiloVehiculoDTO.getNumeroAsientos());
		}
	}
	public String mostrarCorberturaCotizacion(){
		coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>(1);
		NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		BigDecimal codigoAgente = null;
		
		if(incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
			if(incisoCotizacion.getIncisoAutoCot().getEstadoId().trim().equals("")) {
				incisoCotizacion.getIncisoAutoCot().setEstadoId(null);
			}
		}
		if(incisoCotizacion.getIncisoAutoCot().getMunicipioId() != null){
			if(incisoCotizacion.getIncisoAutoCot().getMunicipioId().trim().equals("")) {
				incisoCotizacion.getIncisoAutoCot().setMunicipioId(null);
			}
		}
		if(cotizacion != null
				&& cotizacion.getSolicitudDTO() != null){
			codigoAgente = cotizacion.getSolicitudDTO().getCodigoAgente();
		}
			Long numeroSecuencia = new Long("1");
			numeroSecuencia = numeroSecuencia + incisoService.maxSecuencia(incisoCotizacion.getId().getIdToCotizacion());
			try{
			coberturaCotizacionList = coberturaService.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
					negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(), 
					negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
					negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
					negocioPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getCotizacionDTO().getIdMoneda().shortValue(), numeroSecuencia,
					incisoCotizacion.getIncisoAutoCot().getEstiloId(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null,
					 incisoCotizacion.getIncisoAutoCot().getNumeroSerie(),  incisoCotizacion.getIncisoAutoCot().getVinValido(), codigoAgente);					
			}catch(Exception e){
				e.printStackTrace();
			}
			return SUCCESS;
	}
	
	
	public String recalcularCoberturasInciso() throws SystemException {
		Long codigoAgente = incisoCotizacion.getCotizacionDTO().getSolicitudDTO().getCodigoAgente().longValue();
		Agente agente = entidadService.findById(Agente.class, codigoAgente);
		ValorCatalogoAgentes tipoCedula = agente.getTipoCedula();
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, negocioSeccionId);
		SeccionDTO seccion = negocioSeccion.getSeccionDTO();
		
		boolean esCedulaValida = AgenteDN.getInstancia().validarCedulaAgente(tipoCedula.getId(), AgenteDN.PROPIEDAD_SECCION, seccion.getIdToSeccion());
		if(!esCedulaValida) {
			String mensajeError = String.format("El agente de Tipo de Cedula %s No cuenta con permisos de venta para la linea de negocio %s seleccionada", tipoCedula.getClave(), seccion.getDescripcion());
			this.setMensaje(mensajeError);
			this.setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		IncisoCotizacionDTO incisoGuardado = null;
		boolean riesgosValidos = true;
		try{
			if(incisoCotizacion != null && incisoCotizacion.getId() != null){
				incisoGuardado = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
				if(incisoGuardado != null){
					NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
					SeccionDTO seccionDTO = negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO();
					if(incisoGuardado.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccionDTO.getIdToSeccion().longValue()){
						incisoService.borrarInciso(incisoCotizacion);
						incisoCotizacion.getId().setNumeroInciso(null);
					}
				}
			}
			incisoCotizacion = incisoService.prepareGuardarInciso(
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
					coberturaCotizacionList);
			if(incisoCotizacion.getId().getNumeroInciso() != null){	
				riesgosValidos = listadoService.validaDatosRiesgosCompletosCotizacion(
						incisoCotizacion.getId().getIdToCotizacion(),incisoCotizacion.getId().getNumeroInciso());
				if (incisoCotizacion.getIncisoAutoCot().getCodigoPostal()==null || incisoCotizacion.getIncisoAutoCot().getCodigoPostal().toString().trim()==""){
					String mensaje = String.format("El C\u00F3digo postal es obligatorio");
				    setMensajeError(mensaje);
				    return SUCCESS;
				}
				if(!riesgosValidos){
					setNextFunction("mostrarDatosRiesgo("
							+ incisoCotizacion.getId().getIdToCotizacion()
							+ ", " + incisoCotizacion.getId().getNumeroInciso()
							+ ");jQuery('#btnGuardar').css('display', 'none');jQuery('#btnDatosAdicionalesPaquete').show()");
					setMensajeExitoPersonalizado("Faltan guardar los datos adicionales del paquete");
				}else{
					super.setNextFunction(null);
					calculoService.calcular(incisoCotizacion);
				}				
				
			}else{
				setMensajeError("Error no se pudo realizar correctamente la accion solicitada");
			}	
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			LOG.error("Error al calcular el inciso", ex);
		}

		return SUCCESS;
	}
	
	 /**
	 * Carga los filtros del contenedor
	 */
	public void prepareMostrarContenedor(){
		super.removeListadoEnSession();
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, incisoCotizacion.getId().getIdToCotizacion());
		if(!cotizacionDTO.getIncisoCotizacionDTOs().isEmpty()){
			negocioSeccionList = negocioSeccionService.getSeccionListByCotizacionInciso(cotizacionDTO);
			if(descripcionesIncisos == null){
				descripcionesIncisos = listadoService.listarIncisosDescripcionByCotId(incisoCotizacion.getId().getIdToCotizacion());
			}
			if(paquetes == null){
				if (incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId() != null) {
					paquetes = listadoService.getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(incisoCotizacion.getId().getIdToCotizacion(),new BigDecimal(incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId()));
				} else {
					paquetes =  new LinkedHashMap<Long, String>();
				}
			}
		}else{
			negocioSeccionList = new ArrayList<NegocioSeccion>(1);
			paquetes =  new LinkedHashMap<Long, String>();
			descripcionesIncisos = new HashSet<String>();
		}
	}
	
	public String mostrarContenedor(){
		return SUCCESS;
	}
	
	/**
	 * Lista los incisos de una cotizacion
	 * @return
	 */
	public String listarIncisos(){
		try{
			incisoCotizacionList = incisoService.listarIncisosConFiltro(incisoCotizacion);		
			if(incisoCotizacionList.isEmpty()){
				setMensajeError("No existen incisos para esta cotizacion");
			}else{
				setMensajeExito();
				setIncisoCotizacionList(incisoCotizacionList);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String eliminarInciso(){
		Long numeroSecuencia = null;
		try{
			IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
			numeroSecuencia = incisoService.borrarInciso(inciso);
			if(numeroSecuencia != null ){
				 	cotizacionService.inicializaIgualacionCotizacion(incisoCotizacion.getId().getIdToCotizacion());
					incisoService.actualizaNumeroSecuencia(incisoCotizacion.getId().getIdToCotizacion(),numeroSecuencia);
					setMensajeExito();
			}else{
				setMensajeError(MENSAJE_ERROR_ELIMINAR);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			setMensajeError(MENSAJE_ERROR_ELIMINAR);
		}
		
		return SUCCESS;
	}
	
	
	/**
	 * Prepara el inciso para guardarlo
	 */
	public String guardarInciso(){
			IncisoCotizacionDTO incisoCotizacionDTO = null;
			incisoCotizacionDTO = incisoService.update(incisoCotizacion);
			removeListadoEnSession();
			if(incisoCotizacionDTO != null){
				setMensajeExito();
			}else{
				setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
			}
			super.setNextFunction(null);
		return SUCCESS;
	}
	
	public void prepareBuscarVehiculo(){

		if (lineaNegocioInciso != null && descripcionInciso != null) {
			NegocioSeccion negSeccion = entidadService.findById(
					NegocioSeccion.class, new BigDecimal(lineaNegocioInciso));
			// Resultado de la busqueda
			resultadosBusquedaVehiculo = listadoService
					.listarEstilosPorDescripcion(negSeccion.getSeccionDTO()
							.getTipoVehiculo().getTipoBienAutosDTO()
							.getClaveTipoBien(), descripcionInciso,
							negSeccion.getIdToNegSeccion(),
							BigDecimal.valueOf(Long.valueOf(idMoneda)));
		}
		
	}
	
	public String buscarVehiculo(){
		return SUCCESS;
	}
	
	public void prepareBuscarVehiculoAgente(){

		if (lineaNegocioInciso != null && idMarca != null && modeloVehiculo != null && descripcionInciso != null) {
			NegocioSeccion negSeccion = entidadService.findById(
					NegocioSeccion.class, new BigDecimal(lineaNegocioInciso));
			// Resultado de la busqueda
			resultadosBusquedaVehiculo = listadoService
					.listarEstilosPorDescripcion(negSeccion.getSeccionDTO()
							.getTipoVehiculo().getTipoBienAutosDTO()
							.getClaveTipoBien(), descripcionInciso,
							negSeccion.getIdToNegSeccion(), idMarca, modeloVehiculo,
							BigDecimal.valueOf(Long.valueOf(idMoneda)));
		}
		
	}
	
	public String buscarVehiculoAgente(){
		return SUCCESS;
	}
	
	public String mostrarResumenTotalesInciso(){
		resumenCostosDTO = incisoService.obtenerResumenInciso(incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getIncisoAutoCot(), incisoCotizacion, coberturaCotizacionList);
		return SUCCESS;
	}
	
	
	public void prepareObtenerCoberturasGuardadas(){
		if(incisoCotizacion.getId().getIdToCotizacion() != null && incisoCotizacion.getId().getNumeroInciso()!=null){	
			incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
		}
		
	}
	public void obtenerDiasSalario(){
		try {
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
					.getInstancia()
					.buscarPorPropiedad(
							"id.idGrupoValores",
							Integer.valueOf(ID_CATALOGO_DIAS_SALARIO));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						diasSalario.put(estatus.getId().getIdDato(),
								estatus.getDescripcion());
					}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	public String obtenerCoberturasGuardadas(){	
		Collections.sort(coberturaCotizacionList = coberturaService.getCoberturas(incisoCotizacion), new Comparator<CoberturaCotizacionDTO>() {
			@Override
			public int compare(CoberturaCotizacionDTO n1,
					CoberturaCotizacionDTO n2) {
				return n1
						.getCoberturaSeccionDTO()
						.getCoberturaDTO()
						.getNumeroSecuencia()
						.compareTo(
								n2.getCoberturaSeccionDTO().getCoberturaDTO()
										.getNumeroSecuencia());
			}
		});
		return SUCCESS;
	}
	/*
	@SuppressWarnings("unchecked")
	public String obtenerCoberturasGuardadas2(){
		
	    Collections.sort((List<CatalogoValorFijoDTO>)diasSalario = (List<CatalogoValorFijoDTO>)listadoService.listarCatalogoValorFijo(IncisoAction.ID_CATALOGO_TIPO_SOLICUD), new Comparator<CatalogoValorFijoComboDTO>() {
				@Override
				public int compare(CatalogoValorFijoComboDTO n1,
						CatalogoValorFijoComboDTO n2) {
					return n1
							.getDescription().compareTo(
							n2.getDescription());
							
				}
			});
			return SUCCESS;
		}
		*/
	@SuppressWarnings("unchecked")
	public String cargaControles(){
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		if(name == null){
			name="datosRiesgo";
		}
		if(requerido == null){
			requerido = true;
		}
		valores = (Map<String, String>) valueStack.findValue(name,Map.class);
		if(valores == null){
			valores = new LinkedHashMap<String, String>();
		}
		controles = configuracionDatoIncisoService.getDatosRiesgoCotizacion(valores,incisoCotizacion);
		return SUCCESS;
	}

	public String mostrarControlDinamicoRiesgo(){
		return SUCCESS;
	}
	
	/****************************
	 *  COMPLEMENTAR ASEGURADO  *
	 ****************************/
	
	public void prepareMostrarVentanaAsegurado(){
		if(incisoCotizacion.getCotizacionDTO() != null){
		  incisoCotizacion.setCotizacionDTO(entidadService.findById(CotizacionDTO.class,incisoCotizacion.getId().getIdToCotizacion()));
		}
		if(incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante() != null){
			radioAsegurado = 1;
		}
	}	
	public String mostrarVentanaAsegurado(){
		 return SUCCESS;
	}
	
	public String actualizarDatosAsegurados() throws Exception{
		
			if(incisoCotizacion.getCotizacionDTO() != null){
				  incisoCotizacion.setCotizacionDTO(entidadService.findById(CotizacionDTO.class,incisoCotizacion.getId().getIdToCotizacion()));
			}
			
			if(incisoCotizacion != null && incisoCotizacion.getIncisoAutoCot() != null && incisoCotizacion.getIncisoAutoCot().getNombreAsegurado() != null){
				String valor = UtileriasWeb.parseEncodingISO(incisoCotizacion.getIncisoAutoCot().getNombreAsegurado());
				incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(valor);
			}
			
			ClienteDTO cliente = null;
			DireccionDTO direccion = null;
			if(radioAsegurado == 1 && incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante() == null){
				super.setMensajeError("No existe un Contratante");
				return SUCCESS;
			}
			switch (radioAsegurado) {
			case 1:
				if (incisoCotizacion.getCotizacionDTO().getIdToPersonaContratante() != null) {
					cliente = clienteFacadeRemote.findById(incisoCotizacion
							.getCotizacionDTO().getIdToPersonaContratante(),
							"nombreUsuario");
				}
				if (cliente != null) {
					incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(
							cliente.getIdCliente().longValue());
					incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
							cliente.obtenerNombreCliente());
					incisoCotizacion.setIdClienteCob(cliente.getIdCliente());

				}
				incisoService.update(incisoCotizacion);
				super.setMensajeExito();
			case 2:
				incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(null);
				incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
						incisoCotizacion.getIncisoAutoCot().getNombreAsegurado()
								.toUpperCase());
				incisoService.update(incisoCotizacion);
				super.setMensajeExito();
			case 3:
				if (incisoCotizacion.getIncisoAutoCot().getPersonaAseguradoId() != null) {
					cliente = clienteFacadeRemote.findById(new BigDecimal(
							incisoCotizacion.getIncisoAutoCot()
									.getPersonaAseguradoId()), "nombreUsuario");
					
				}
				if (incisoCotizacion.getDireccionDTO() != null && incisoCotizacion.getDireccionDTO().getIdToDireccion() != null) {
					direccion = direccionFacadeRemote.findById(incisoCotizacion.getDireccionDTO().getIdToDireccion());
				}
				
				if (cliente != null) {
					incisoCotizacion.getIncisoAutoCot().setPersonaAseguradoId(
							cliente.getIdCliente().longValue());
					incisoCotizacion.getIncisoAutoCot().setNombreAsegurado(
							cliente.obtenerNombreCliente());
					incisoCotizacion.setIdClienteCob(cliente.getIdCliente());

				}
				if(direccion != null){
					incisoCotizacion.setDireccionDTO(direccion);
				}else{
					incisoCotizacion.setDireccionDTO(null);
				}
				
	
				incisoService.update(incisoCotizacion);
				super.setMensajeExito();
			}
		
		cobranzaIncisoService.validarCambioAseguradoCobranzaInciso(incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getId().getNumeroInciso());		
		return SUCCESS;
	}
	
	/**
	 * Muestra el grid con los resultados paginados filtrados
	 * 
	 * @return Action result
	 */
	@SuppressWarnings("unchecked")
	public String busquedaRapida() {
		try{
			//System.out.print("******************** Busqueda rapida incisos");
			List<IncisoCotizacionDTO> listado = new ArrayList<IncisoCotizacionDTO>();
			if (cleanCache) {
				removeListadoEnSession();
				removeMapEnSession();
				//System.out.print("******************** Remover sesion");
			}
			if (!super.listadoDeCache()) {
				//System.out.print("******************** Listado NO EN CACHE");
				incisoCotizacion.setPrimerRegistroACargar(super.getPosStart());
				incisoCotizacion
						.setNumeroMaximoRegistrosACargar(BaseAction.REGISTROS_A_MOSTRAR);
				//System.out.print("******************** BUSCAR CON FILTRO");
				listado = incisoService.listarIncisosConFiltro(incisoCotizacion);
				setIncisoCotizacionList(listado);
				setListadoEnSession(listado);
			} else {
				//System.out.print("******************** Listado EN CACHE");
				listado = (List<IncisoCotizacionDTO>) getListadoPaginado();
				setIncisoCotizacionList(listado);
				setListadoEnSession(listado);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		if(esCobInciso.intValue() == 1){
			return "cobInciso";
		}
		return SUCCESS;
	}
	
	/**
	 * Obtiene los incisos de una cotizacion
	 * aplicando filtros
	 * @autor martin
	 */
	@Override
	public String busquedaPaginada() {
		try{
			//System.out.print("******************** Busqueda paginada");
			if (getTotalCount() == null) {
				setTotalCount(incisoService
						.listarIncisosConFiltroCount(incisoCotizacion));
				setListadoEnSession(null);
				setPosPaginado();
			}
		}catch(Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		return SUCCESS;
	}
	/**
	 * Obtiene todos los incisos de una cot
	 * sin aplicar filtros
	 * @author maritn
	 */
	@Override
	public String busquedaRapidaPaginada() {
		try {			
			IncisoCotizacionDTO filtro = new IncisoCotizacionDTO();
			filtro.setId(new IncisoCotizacionId());
			if(incisoCotizacion.getId() != null){
				filtro.getId().setIdToCotizacion((incisoCotizacion.getId().getIdToCotizacion()));	
			}
			filtro.setIncisoAutoCot(new IncisoAutoCot());
			if (incisoCotizacion != null && incisoCotizacion.getIncisoAutoCot() != null){
				filtro.getIncisoAutoCot().setDescripcionFinal(incisoCotizacion.getIncisoAutoCot().getDescripcionFinal());
				filtro.getIncisoAutoCot().setPlaca(incisoCotizacion.getIncisoAutoCot().getPlaca());
				filtro.getIncisoAutoCot().setNombreAsegurado(incisoCotizacion.getIncisoAutoCot().getNombreAsegurado());
				filtro.getIncisoAutoCot().setNumeroSerie(incisoCotizacion.getIncisoAutoCot().getNumeroSerie());
				filtro.setNumeroSecuencia(incisoCotizacion.getNumeroSecuencia());
			}
			if(getTotalCount() == null){
				setTotalCount(incisoService.listarIncisosConFiltroCount(filtro));
				this.setListadoEnSession(null);
			}
			setPosPaginado();
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
		return SUCCESS;
	}
	
	public String eliminarIncisoMasivo(){
		Long numeroSecuencia = null;
		if(getIncisoCotizacionList() != null && !getIncisoCotizacionList().isEmpty()){
			boolean error = false;
			for(IncisoCotizacionDTO inciso : getIncisoCotizacionList()){
				
				IncisoCotizacionDTO filtro = new IncisoCotizacionDTO();
				IncisoCotizacionId filtroId = new IncisoCotizacionId();
				filtroId.setIdToCotizacion(getIncisoCotizacion().getId().getIdToCotizacion());
				filtroId.setNumeroInciso(inciso.getId().getNumeroInciso());
				filtro.setId(filtroId);
				filtro.setIncisoAutoCot(new IncisoAutoCot());
				
				try{
					inciso = entidadService.findById(IncisoCotizacionDTO.class, filtroId);
					numeroSecuencia = incisoService.borrarInciso(inciso);
					if(numeroSecuencia != null ){
							incisoService.actualizaNumeroSecuencia(getIncisoCotizacion().getId().getIdToCotizacion(),numeroSecuencia);
							setMensajeExito();
					}else{
						setMensajeError(MENSAJE_ERROR_ELIMINAR);
					}
				}catch(Exception ex){
					error = true;
					setMensajeError(MENSAJE_ERROR_ELIMINAR);
				}		
			}
			
			if(!error){
				setMensajeExito();
			}
		}
		
		return SUCCESS;
	}
	
	public String eliminarTodosIncisos(){	
		incisoCotizacionList = incisoService.findByCotizacionId(getIncisoCotizacion().getId().getIdToCotizacion());
		if(getIncisoCotizacionList() != null && !getIncisoCotizacionList().isEmpty()){
			boolean error = false;
			Iterator<IncisoCotizacionDTO> iterator = incisoCotizacionList.iterator();
			while(iterator.hasNext())
			{
				IncisoCotizacionDTO inciso = iterator.next();
				IncisoCotizacionId filtroId = new IncisoCotizacionId();
				filtroId.setIdToCotizacion(getIncisoCotizacion().getId().getIdToCotizacion());
				filtroId.setNumeroInciso(inciso.getId().getNumeroInciso());
				
				try{
					inciso = entidadService.findById(IncisoCotizacionDTO.class, filtroId);
					incisoService.borrarInciso(inciso);
					iterator.remove(); // works correctly
				}catch(Exception ex){
					error = true;
					setMensajeError(MENSAJE_ERROR_ELIMINAR);
				}		
			}
			
			//eliminar igualacion de primas existente
			if(!error){
				setMensajeExito();
			}
		}
		
		return SUCCESS;
	}
	
	public String mostrarInformacionVehicular(){
		
		try {
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, this.idNegocioSeccion);
			CotizacionDTO cotizacion = incisoCotizacion.getCotizacionDTO();
			
			if ( negocioSeccion != null && cotizacion != null ){
				
				infoVehicular = fronterizosService.mostrarInformacionVehicular( this.VIN, negocioSeccion, this.idMoneda, this.idEstado,
						incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getId().getNumeroInciso(), cotizacion);
			
				if ( infoVehicular != null ){
					if ( infoVehicular.containsKey("desactivada") ){
						super.setMensajeError("desactivada");
					}
					else if ( infoVehicular.containsKey("oSesas") ){
						super.setMensajeError(this.traeMensaje(infoVehicular));
					}
					else{
						super.setMensajeExitoPersonalizado("El n\u00FAmero de serie ingresado ha sido validado.");	
					}
				}
				else{
					super.setMensajeError("N\u00FAmero de serie incorrecto.");
				}
			}
			else{
				super.setMensajeError("No se pudo validar el n\u00FAmero de serie.");
			}			
		}		
		catch (Exception e) {
			LOG.error("No se pudo validar el numero de serie.", e);
			super.setMensajeError("No se pudo validar el numero de serie.");
		}		
		return SUCCESS;
	}
	
	private String traeMensaje( HashMap<String, Map<String, String>> infoVehicular ){
		Map<String,String> map = infoVehicular.get("oSesas");
		return map.get("ob");		
	}
	
	/**
	 * @param incisoCotizacionList el incisoCotizacionList a establecer
	 */
	public void setIncisoCotizacionList(List<IncisoCotizacionDTO> incisoCotizacionList) {
		this.incisoCotizacionList = incisoCotizacionList;
	}

	/**
	 * @return el incisoCotizacionList
	 */
	public List<IncisoCotizacionDTO> getIncisoCotizacionList() {
		return incisoCotizacionList;
	}

	public CotizacionService getCotizacionService() {
		return cotizacionService;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	public List<NegocioSeccion> getNegocioSeccionList() {
		return negocioSeccionList;
	}


	public void setNegocioSeccionList(List<NegocioSeccion> negocioSeccionList) {
		this.negocioSeccionList = negocioSeccionList;
	}

	public Set<String> getDescripcionesIncisos() {
		return descripcionesIncisos;
	}

	public void setDescripcionesIncisos(Set<String> descripcionesIncisos) {
		this.descripcionesIncisos = descripcionesIncisos;
	}

	public String getDescripcionInciso() {
		return descripcionInciso;
	}

	public void setDescripcionInciso(String descripcionInciso) {
		this.descripcionInciso = descripcionInciso;
	}

	public Map<Long, String> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(Map<Long, String> paquetes) {
		this.paquetes = paquetes;
	}

	public void setCoberturaCotizacionList(List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		this.coberturaCotizacionList = coberturaCotizacionList;
	}
	public List<CoberturaCotizacionDTO> getCoberturaCotizacionList() {
		return coberturaCotizacionList;
	}

	public int getNumeroCopias() {
		return numeroCopias;
	}

	public void setNumeroCopias(int numeroCopias) {
		this.numeroCopias = numeroCopias;
	}

	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}
	
	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}


	public List<EstiloVehiculoDTO> getResultadosBusquedaVehiculo() {
		return resultadosBusquedaVehiculo;
	}


	public void setResultadosBusquedaVehiculo(
			List<EstiloVehiculoDTO> resultadosBusquedaVehiculo) {
		this.resultadosBusquedaVehiculo = resultadosBusquedaVehiculo;
	}


	public String getLineaNegocioInciso() {
		return lineaNegocioInciso;
	}


	public void setLineaNegocioInciso(String lineaNegocioInciso) {
		this.lineaNegocioInciso = lineaNegocioInciso;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getRequerido() {
		return requerido;
	}


	public void setRequerido(Boolean requerido) {
		this.requerido = requerido;
	}


	public Map<String, String> getValores() {
		return valores;
	}


	public void setValores(Map<String, String> valores) {
		this.valores = valores;
	}


	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}


	public void setControles(List<ControlDinamicoRiesgoDTO> controles) {
		this.controles = controles;
	}

	public Integer getRadioAsegurado() {
		return radioAsegurado;
	}

	public void setRadioAsegurado(Integer radioAsegurado) {
		this.radioAsegurado = radioAsegurado;
	}

	public String getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	/**
	 * @return the mostrarAjustarCaracteristicas
	 */
	public boolean isMostrarAjustarCaracteristicas() {
		return mostrarAjustarCaracteristicas;
	}

	/**
	 * @param mostrarAjustarCaracteristicas the mostrarAjustarCaracteristicas to set
	 */
	public void setMostrarAjustarCaracteristicas(
			boolean mostrarAjustarCaracteristicas) {
		this.mostrarAjustarCaracteristicas = mostrarAjustarCaracteristicas;
	}

	/**
	 * @return the primaTotalAIgualar
	 */
	public Double getPrimaTotalAIgualar() {
		return primaTotalAIgualar;
	}

	/**
	 * @param primaTotalAIgualar the primaTotalAIgualar to set
	 */
	public void setPrimaTotalAIgualar(Double primaTotalAIgualar) {
		this.primaTotalAIgualar = primaTotalAIgualar;
	}

	/**
	 * @return the claveEstatus
	 */
	public String getClaveEstatus() {
		return claveEstatus;
	}

	/**
	 * @param claveEstatus the claveEstatus to set
	 */
	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}

	public void setDiasSalario(Map<Object, String> diasSalario) {
		this.diasSalario = diasSalario;
	}

	public Map<Object, String> getDiasSalario() {
		return diasSalario;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setIdMarca(BigDecimal idMarca) {
		this.idMarca = idMarca;
	}

	public BigDecimal getIdMarca() {
		return idMarca;
	}

	public void setModeloVehiculo(Short modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public Short getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setNumAcientos(Short numAcientos) {
		this.numAcientos = numAcientos;
	}

	public Short getNumAcientos() {
		return numAcientos;
	}

	public void setEsCobInciso(Short esCobInciso) {
		this.esCobInciso = esCobInciso;
	}

	public Short getEsCobInciso() {
		return esCobInciso;
	}
	
	@Autowired
	@Qualifier("fronterizosServiceEJB")
	public void setFronterizosService(FronterizosService fronterizosService) {
		this.fronterizosService = fronterizosService;
	}

	public int getValidoVIN() {
		return validoVIN;
	}

	public void setValidoVIN(int validoVIN) {
		this.validoVIN = validoVIN;
	}

	public String getVIN() {
		return VIN;
	}

	public void setVIN(String vIN) {
		VIN = vIN;
	}

	public BigDecimal getIdNegocioSeccion() {
		return idNegocioSeccion;
	}

	public void setIdNegocioSeccion(BigDecimal idNegocioSeccion) {
		this.idNegocioSeccion = idNegocioSeccion;
	}

	public HashMap<String, Map<String, String>> getInfoVehicular() {
		return infoVehicular;
	}

	public void setInfoVehicular(
			HashMap<String, Map<String, String>> infoVehicular) {
		this.infoVehicular = infoVehicular;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getModeloSel() {
		return modeloSel;
	}

	public void setModeloSel(String modeloSel) {
		this.modeloSel = modeloSel;
	}
}
