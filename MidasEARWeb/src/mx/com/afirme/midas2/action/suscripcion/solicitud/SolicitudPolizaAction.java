package mx.com.afirme.midas2.action.suscripcion.solicitud;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.sistema.Sistema;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.sistema.creadorxls.MidasXLSCreator;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.excels.GeneraExcelSolicitudesPoliza;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudPolizaService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones.GeneraPlantillaReporteSolicitudService;

import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class SolicitudPolizaAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = 1L;
	
	private static final int ID_CATALOGO_TIPO_SOLICUD = 47;
	
	private String id;
	private BigDecimal idToSolicitud;
	private SolicitudDTO solicitud;
	private List<SolicitudDTO> solicitudes = new ArrayList<SolicitudDTO>(1);
	private String tipoAccion;
	private String claveNegocio;
	private Integer idTcAgente;
	private Integer idMovimiento;
	private Short seleccion = (short)1;
	private Map<Long,String> agenteList = new HashMap<Long, String>();
	private List<Short> estatusList = new ArrayList<Short>(1);
	private Date fechaFinal;
	private Map<Long, String> oficinasList = new HashMap<Long, String>();
	private List<Negocio> negocioList = new ArrayList<Negocio>(1);
	private Map<Long, String> negocioProductoList = new LinkedHashMap<Long, String>();
	private Map<Object, String> tiposSolicitud = new LinkedHashMap<Object, String>();
	private Negocio negocio;
	private Usuario usuario;
	private SolicitudPolizaService solicitudPolizaService;
	private EntidadService entidadService;
	private AgenteService agenteService;
	private NegocioService negocioService;
	private ListadoService listadoService;
	private Map<Integer,String> listaEstatus = new LinkedHashMap<Integer, String>();
	private String nombreRazonSocial;
	private ControlArchivoDTO controlArchivo = new ControlArchivoDTO();
	private BigDecimal idToControlArchivo;
	private InputStream solicitudInputStream;
	private String     fileName;
	private String      contentType;
	private ComentariosService comentariosService;
	
	private Locale locale = getLocale();	
	private GeneraPlantillaReporteSolicitudService generaPlantillaReporteSolicitudService;
	private NegocioProducto negocioProducto = new NegocioProducto();
	private Short tipoBusqueda;
	private List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudList;
	private List<BigDecimal> idToControlArchivoList;
	private BigDecimal idToCACartaCobertura;
	private Short tipoConsulta = 0;	
	private Map<String, String> tipoOperaciones = new HashMap<String, String>();
	private Boolean tipoCabina;
	
	private FileManagerService fileManagerService;
	
	@Autowired
	@Qualifier("fileManagerServiceEJB")
	public void setFileManagerService(FileManagerService fileManagerService) {
		this.fileManagerService = fileManagerService;
	}	

	@Autowired
	@Qualifier("generaPlantillaReporteSolicitudServiceEJB")
	public void setGeneraPlantillaReporteSolicitudService(
			GeneraPlantillaReporteSolicitudService generaPlantillaReporteSolicitudService) {
		this.generaPlantillaReporteSolicitudService = generaPlantillaReporteSolicitudService;
	}

	@Autowired
	@Qualifier("solicitudPolizaServiceEJB")
	public void setSolicitudPolizaService(
			SolicitudPolizaService solicitudPolizaService) {
		this.solicitudPolizaService = solicitudPolizaService;
	}	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setCatalogoService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	
	@Autowired
	@Qualifier("agenteServiceEJB")
	public void setAgenteService(
			AgenteService agenteService) {
		this.agenteService = agenteService;
	}
	
	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(
			NegocioService negocioService) {
		this.negocioService = negocioService;
	}
	
	@Autowired
	@Qualifier("comentarioServiceEJB")
	public void setComentariosService(ComentariosService comentariosService) {
		this.comentariosService = comentariosService;
	}


	@Override
	public void prepare() throws Exception {

		tipoCabina = usuarioService.tienePermisoUsuarioActual("FN_M2_Cabina_Visibilidad_Consulta");
		if(id != null && !id.equals("")){
			solicitud = entidadService.findById(SolicitudDTO.class, new BigDecimal(id));			
			negocioProductoList = listadoService.getMapNegProductoPorNegocio(solicitud.getNegocio().getIdToNegocio());
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("productoDTO.idToProducto", solicitud.getProductoDTO().getIdToProducto());
			params.put("negocio.idToNegocio", solicitud.getNegocio().getIdToNegocio());
			List<NegocioProducto> negocioProductoList = entidadService.findByProperties(NegocioProducto.class, params);
			if(negocioProductoList != null && !negocioProductoList.isEmpty()){
				negocioProducto = negocioProductoList.get(0);
			}
			if (!tipoCabina){
				
				if (solicitud.getCodigoEjecutivo() != null) {
					Short uno = 1;
					agenteList = listadoService.getMapAgentesPorGerenciaOficinaPromotoria(null, solicitud.getCodigoEjecutivo().longValue(), null,uno);
				}
				//Obtiene agente actual
				if(solicitud.getCodigoAgente() != null){
					Agente agente = entidadService.findById(Agente.class, solicitud.getCodigoAgente().longValue());
					solicitud.setAgente(agente);	
					negocioList = negocioService.listarNegociosPorAgenteUnionNegociosLibres(solicitud.getCodigoAgente().intValue(), solicitud.getNegocio().getClaveNegocio());
				}
			}
			else {
				negocioList = negocioService.findAll();
			}
		}
		tipoOperaciones.put("1", "B\u00FAsqueda de Tr\u00E1mite");
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Emision_Solicitudes_Editar") && 
				!usuarioService.tienePermisoUsuarioActual("FN_M2_Cabina_Visibilidad_Consulta")) {
			tipoOperaciones.put("2", "Nuevo Tr\u00E1mite");
		}
		
		
		
		//SIMULACION ROL BORRAR
		usuario = this.getUsuarioActual();
			
	}
	
	@SuppressWarnings("deprecation")
	public void prepareMostrar(){
		this.getReturnFields(SolicitudPolizaAction.class, this);
		this.initSolicitud();		
		// Obtiene los estatus de la solicitud
		if(listaEstatus.isEmpty()){
			try {
				List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
						.getInstancia()
						.buscarPorPropiedad(
								"id.idGrupoValores",
								Integer.valueOf(Sistema.GRUPO_CLAVE_ESTATUS_SOLICITUD));
				if (!catalogoValorFijoDTOs.isEmpty()
						&& catalogoValorFijoDTOs != null) {
						for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
							listaEstatus.put(estatus.getId().getIdDato(),
									estatus.getDescripcion());
						}
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		if(solicitud == null){
			solicitud = new SolicitudDTO();
		}
		if(solicitud != null && solicitud.getClaveTipoPersona() == null){
			Short claveTipoPersonaFisica = 1;
			solicitud.setClaveTipoPersona(claveTipoPersonaFisica);
		}
		
	}
	public void prepareEditar(){
		this.initSolicitud();
	}	
	public void initSolicitud(){
		if (solicitud == null) {
			solicitud = new SolicitudDTO();
		}
		
		negocio = new Negocio();
		negocio.setClaveNegocio(claveNegocio);
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioList = new ArrayList<Negocio>(1);
		
		
		if (solicitud.getCodigoAgente() == null) {
			if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")  && 
					!usuarioService.tienePermisoUsuarioActual("FN_M2_Cabina_Visibilidad_Consulta")) {
				Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
				solicitud.setCodigoAgente(BigDecimal.valueOf(agenteUsuarioActual.getId()));
				solicitud.setNegocio(negocio);
			}
		}
		
		if(seleccion == 1){
			negocioList = negocioService.findByFilters(negocio);
		}
		//Obtiene agente actual
		if(solicitud != null && solicitud.getCodigoAgente() != null){
			Agente agente = entidadService.findById(Agente.class, solicitud.getCodigoAgente().longValue());
			solicitud.setAgente(agente);			
			solicitud.setCodigoEjecutivo(BigDecimal.valueOf(agente.getPromotoria().getEjecutivo().getId()));
			negocioList = negocioService.listarNegociosPorAgenteUnionNegociosLibres(solicitud.getCodigoAgente().intValue(), solicitud.getNegocio().getClaveNegocio());
		}
		
		if(solicitud != null && solicitud.getNegocio() != null && solicitud.getNegocio().getIdToNegocio() != null){
			negocioProductoList = listadoService.getMapNegProductoPorNegocio(solicitud.getNegocio().getIdToNegocio());
		}
		if (solicitud != null && solicitud.getCodigoEjecutivo() != null) {
			Short uno = 1;
			agenteList = listadoService.getMapAgentesPorGerenciaOficinaPromotoria(null, solicitud.getCodigoEjecutivo().longValue(), null,uno);
		}
		
		tiposSolicitud = listadoService.listarCatalogoValorFijo(SolicitudPolizaAction.ID_CATALOGO_TIPO_SOLICUD);
		setUsuario(usuarioService.getUsuarioActual());	
		oficinasList = listadoService.getMapEjecutivosResponsable();
		
	}
	public String mostrar(){
		Short claveTipoPersonaMoral = 2;
		if(solicitud != null && solicitud.getClaveTipoPersona() != null && solicitud.getClaveTipoPersona().equals(claveTipoPersonaMoral)){
			this.setNombreRazonSocial(solicitud.getNombrePersona());
		}
		this.getSolicitud().setTieneCartaCobertura(solicitud.getTieneCartaCobertura()!=null?solicitud.getTieneCartaCobertura():Integer.valueOf(0).shortValue()); //0: no tiene carta cobertura
		return SUCCESS;
	}
	public String editar(){
		return SUCCESS;
	}
	public void prepareBuscarSolicitud(){
		if(idTcAgente != null){
			AgenteDTO agente = agenteService.obtenerAgente(idTcAgente);
			solicitud.setNombreAgente(agente.getNombre());
		}
		if(nombreRazonSocial != null){
			solicitud.setNombrePersona(nombreRazonSocial);
		}
	}
	
	public void prepareBusquedaPaginada(){
		if(idTcAgente != null){
			AgenteDTO agente = agenteService.obtenerAgente(idTcAgente);
			solicitud.setNombreAgente(agente.getNombre());
		}
		if(nombreRazonSocial != null){
			solicitud.setNombrePersona(nombreRazonSocial);
		}
	}
	
	public String busquedaPaginada(){
		if(solicitud != null){
			if(getTotalCount() == null){
				if (!usuarioService.tieneRolUsuarioActual("Rol_M2_Mesa_Control")) {
					if (usuarioService.tienePermisoUsuarioActual("FN_M2_Solicitud_Listado_Usuario_Asignacion")) {
						Usuario usuarioActual = usuarioService.getUsuarioActual();
						solicitud.setCodigoUsuarioAsignacion(usuarioActual.getNombreUsuario());
					} else {
						Usuario usuarioActual = usuarioService.getUsuarioActual();
						solicitud.setCodigoUsuarioCreacion(usuarioActual.getNombreUsuario());
					}
				}
				solicitud.setClaveTipoPersona(null);
				setTotalCount(solicitudPolizaService.obtenerTotalPaginacion(solicitud, fechaFinal));
				setListadoEnSession(null);
			}
			setPosPaginado();
		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String buscarSolicitud(){
		if(solicitud != null){
			if(!listadoDeCache()){
				this.saveReturnFields(SolicitudPolizaAction.class, this);
				solicitud.setPrimerRegistroACargar(getPosStart());
				solicitud.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
				if (!usuarioService.tieneRolUsuarioActual("Rol_M2_Mesa_Control")) {
					if (usuarioService.tienePermisoUsuarioActual("FN_M2_Solicitud_Listado_Usuario_Asignacion")) {
						Usuario usuarioActual = usuarioService.getUsuarioActual();
						solicitud.setCodigoUsuarioAsignacion(usuarioActual.getNombreUsuario());
					}else{
						Usuario usuarioActual = usuarioService.getUsuarioActual();
						solicitud.setCodigoUsuarioCreacion(usuarioActual.getNombreUsuario());						
					}
				}
				solicitud.setClaveTipoPersona(null);
				solicitudes = solicitudPolizaService.buscarSolicitud(solicitud, solicitud.getFechaCreacion(), fechaFinal,negocioProducto.getIdToNegProducto());
				setListadoEnSession(solicitudes);
			}else{
				solicitudes = (List<SolicitudDTO>) getListadoPaginado();
				setListadoEnSession(solicitudes);
			}
		}
		return SUCCESS;
	}
	
	public String busquedaRapidaPaginada(){
		tipoCabina = usuarioService.tienePermisoUsuarioActual("FN_M2_Cabina_Visibilidad_Consulta");
		if(getTotalCount() == null){
			SolicitudDTO filtro = new SolicitudDTO();
			if (!usuarioService.tieneRolUsuarioActual("Rol_M2_Mesa_Control")) {
				if (usuarioService.tienePermisoUsuarioActual("FN_M2_Solicitud_Listado_Usuario_Asignacion")) {
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					filtro.setCodigoUsuarioAsignacion(usuarioActual.getNombreUsuario());
				} else  {
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					filtro.setCodigoUsuarioCreacion(usuarioActual.getNombreUsuario());
				}
			}
			setTotalCount(solicitudPolizaService.obtenerTotalPaginacion(filtro));
			setListadoEnSession(null);
		}
		setPosPaginado();
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String busquedaRapida(){
		tipoCabina = usuarioService.tienePermisoUsuarioActual("FN_M2_Cabina_Visibilidad_Consulta");
		SolicitudDTO filtro = new SolicitudDTO();
		if(!listadoDeCache()){
			filtro.setPrimerRegistroACargar(getPosStart());
			filtro.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
			
			if (!usuarioService.tieneRolUsuarioActual("Rol_M2_Mesa_Control")) {
				if (usuarioService.tienePermisoUsuarioActual("FN_M2_Solicitud_Listado_Usuario_Asignacion")) {
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					filtro.setCodigoUsuarioAsignacion(usuarioActual.getNombreUsuario());
				} else  {
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					filtro.setCodigoUsuarioCreacion(usuarioActual.getNombreUsuario());
				}
			}
			
			solicitudes = solicitudPolizaService.busquedaRapida(filtro);
			setListadoEnSession(solicitudes);
		}else{
			solicitudes = (List<SolicitudDTO>) getListadoPaginado();
			setListadoEnSession(solicitudes);
		}
		return SUCCESS;
	}	
	public void prepareAgregarSolicitud(){
		if(solicitud != null){
			NegocioProducto negProducto = entidadService.findById(NegocioProducto.class, negocioProducto.getIdToNegProducto());
			solicitud.setProductoDTO(negProducto.getProductoDTO());
			
			if(idTcAgente != null){
				AgenteDTO agente = agenteService.obtenerAgente(idTcAgente);
				solicitud.setNombreAgente(agente.getNombre());
				solicitud.setNombreOficinaAgente(agente.getNombreOficina()); 
			}
			solicitud.setFechaModificacion(new Date());
			
			if(nombreRazonSocial != null){
				solicitud.setNombrePersona(nombreRazonSocial);
			}
		}
	}
	
	public String agregarSolicitud(){
		if(id != null && !id.equals("")){
		    solicitud.setFechaModificacion(new Date());
			solicitud = solicitudPolizaService.actualizar(solicitud);
			setMensajeExito();
			return "update";
		}else{
			solicitud.setFechaCreacion(new Date());
			
			id = solicitudPolizaService.agregarSolicitud(solicitud).toString();
			solicitud.setIdToSolicitud(new BigDecimal(id));
			if(this.getIdToControlArchivoList()!=null){
	 			for(BigDecimal idToControlArchivoTemp : this.getIdToControlArchivoList()){
	 				try{
	 					ControlArchivoDTO controlArchivoTemp = entidadService.findById(ControlArchivoDTO.class, idToControlArchivoTemp);
	 	 				DocumentoDigitalSolicitudDTO documento = new DocumentoDigitalSolicitudDTO();
	 	 				documento.setCodigoUsuarioCreacion(this.getUsuarioActual().getNombreUsuario());
	 	 				documento.setCodigoUsuarioModificacion(this.getUsuarioActual().getNombreUsuario());
	 	 				documento.setControlArchivo(controlArchivoTemp);
	 	 				documento.setFechaCreacion(new Date());
	 	 				documento.setEsCartaCobertura((short)(idToControlArchivoTemp.equals(idToCACartaCobertura)?1:0));
	 	 				
	 	 				if(idToCACartaCobertura.equals(documento.getControlArchivo().getIdToControlArchivo()) && documento.getFiles()==null){
	 	 					String fileName = this.getFileName(controlArchivoTemp);
	 	 					byte[] file = fileManagerService.downloadFile(fileName,  idToControlArchivoTemp.toString());
			 				documento.setFiles(file);
			 				documento.setExtension((fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf(".")+1, fileName.length()));
		 				}
	 	 				
	 	 				documento.setFechaModificacion(new Date());
	 	 				documento.setNombreUsuarioCreacion(this.getUsuarioActual().getNombreCompleto());
	 	 				documento.setNombreUsuarioModificacion(this.getUsuarioActual().getNombreCompleto());
	 	 				SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, new BigDecimal(id));
	 	 				documento.setSolicitudDTO(solicitudDTO);
	 	 				documento.setIdToControlArchivo(idToControlArchivoTemp);
	 	 				entidadService.save(documento);
	 	 			}catch(Exception e){
	 	 				e.printStackTrace();
	 	 			}
	 			}
			}
			setMensajeExitoPersonalizado(MENSAJE_EXITO_SOL_INCOMPLETA+" (SOL-"+solicitud.getIdToSolicitud()+")");
			return SUCCESS;			
		}		

	}
	/**
	 * Cambia el estatus de una poliza a cancelada
	 * @return
	 */
	public String cancelarSolicitud() {
		if (id != null && !id.equals("")) {
			solicitud.setIdToSolicitud(solicitudPolizaService
					.cancelarSolicitud(new BigDecimal(id)));
			if (solicitud.getIdToSolicitud() != null) {
				setMensajeExito();
			}else {
				setMensajeError(BaseAction.MENSAJE_ERROR_ACTUALIZAR);
				return INPUT;
			}
	}
		return SUCCESS;		
	}
	

 public String generarImpresion(){
	try{
	    TransporteImpresionDTO transporte = generaPlantillaReporteSolicitudService.llenarSolicitudDTO(getIdToSolicitud(), locale);
	    solicitudInputStream = new ByteArrayInputStream(transporte.getByteArray());
		contentType = "application/pdf";
		fileName = "solicitud_"+getIdToSolicitud()+".pdf";	
		
	}catch(Exception e){
		e.printStackTrace();
		setMensaje("Error al imprimir favor de intentarlo mas tarde");
		setTipoMensaje(TIPO_MENSAJE_ERROR);
		return ERROR;
	}
	return SUCCESS;
   }
 
	public void prepareDescargaExcel(){
		if(solicitud == null){
			solicitud = new SolicitudDTO();
		}
		if(tipoBusqueda == null){
			tipoBusqueda = 1;
		}
		if(idTcAgente != null){
			AgenteDTO agente = agenteService.obtenerAgente(idTcAgente);
			solicitud.setNombreAgente(agente.getNombre());
		}
		if(nombreRazonSocial != null){
			solicitud.setNombrePersona(nombreRazonSocial);
		}
	}
	
 public String descargaExcel(){
	 GeneraExcelSolicitudesPoliza generaExcel = new GeneraExcelSolicitudesPoliza(comentariosService);
	 try {
		 Short uno = 1;
		 if(tipoBusqueda.equals(uno)){
			 solicitudes = solicitudPolizaService.busquedaRapida(new SolicitudDTO());
		 }else{
			 solicitud.setClaveTipoPersona(null);
			if (!usuarioService.tieneRolUsuarioActual("Rol_M2_Mesa_Control")) {
				if (usuarioService.tienePermisoUsuarioActual("FN_M2_Solicitud_Listado_Usuario_Asignacion")) {
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					solicitud.setCodigoUsuarioAsignacion(usuarioActual.getNombreUsuario());
				}else{
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					solicitud.setCodigoUsuarioCreacion(usuarioActual.getNombreUsuario());						
				}
			}
			 solicitudes = solicitudPolizaService.buscarSolicitud(solicitud, solicitud.getFechaCreacion(), fechaFinal,negocioProducto.getIdToNegProducto());
		 }
		solicitudInputStream = generaExcel.generaExcelSolicitud(solicitudes);
		contentType = "application/vnd.ms-excel";
		fileName = "ListaSolicitudes.xls";	
	} catch (Exception e) {
		e.printStackTrace();
		setMensaje("Error al crear Excel favor de intentarlo mas tarde");
		setTipoMensaje(TIPO_MENSAJE_ERROR);
		return ERROR;
	}
	 return SUCCESS;
 }
 
 	public String adjuntarDocumentos(){
 		if(this.getId() != null){
 			solicitud = entidadService.findById(SolicitudDTO.class, new BigDecimal(this.getId()));
 		}
 		return SUCCESS;
 	}
 	
 	public String buscarSolicitudDocumentos() throws SystemException{
 		this.documentoDigitalSolicitudList = new ArrayList<DocumentoDigitalSolicitudDTO>(1);
 		if(this.getId() != null){
 			documentoDigitalSolicitudList = entidadService.findByProperty(DocumentoDigitalSolicitudDTO.class, "solicitudDTO.idToSolicitud", new BigDecimal(this.getId()));
 			if(documentoDigitalSolicitudList != null){
	 			List<Comentario> comentarios = entidadService.findByProperty(Comentario.class, "solicitudDTO.idToSolicitud", new BigDecimal(this.getId()));
	 			for(Comentario comentario : comentarios){
	 				if(comentario.getDocumentoDigitalSolicitudDTO() != null){
	 					for(int i = 0; i < documentoDigitalSolicitudList.size(); i++){
	 						DocumentoDigitalSolicitudDTO documento = documentoDigitalSolicitudList.get(i);
	 						if(comentario.getDocumentoDigitalSolicitudDTO().getIdToDocumentoDigitalSolicitud().equals(documento.getIdToDocumentoDigitalSolicitud())){
	 							documentoDigitalSolicitudList.remove(i);
	 							break;
	 						}
	 					}
	 				}
	 			}
	 			if(idToCACartaCobertura!=null && !idToCACartaCobertura.equals(BigDecimal.ZERO)){
	 				for (DocumentoDigitalSolicitudDTO documento : documentoDigitalSolicitudList) {
		 				documento.setEsCartaCobertura((short)(idToCACartaCobertura.equals(documento.getIdToControlArchivo())?1:0));
		 				
		 				if(idToCACartaCobertura.equals(documento.getIdToControlArchivo()) && documento.getFiles()==null){
		 					ControlArchivoDTO controlArchivoTemp = entidadService.findById(ControlArchivoDTO.class, idToCACartaCobertura);
		 					String fileName = this.getFileName(controlArchivoTemp);
		 					byte[] file = fileManagerService.downloadFile(fileName,  idToCACartaCobertura.toString());
			 				documento.setFiles(file);
			 				documento.setExtension((fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf(".")+1, fileName.length()));
		 				}else{
		 					documento.setFiles(null);
		 				}
		 				
		 				entidadService.save(documento);
					}
	 			}
 			
 			
 			}else{
 				this.documentoDigitalSolicitudList = new ArrayList<DocumentoDigitalSolicitudDTO>(1);
 			}
 		}else if(this.getIdToControlArchivoList() != null){
 			for(BigDecimal idToControlArchivoTemp : this.getIdToControlArchivoList()){
 				ControlArchivoDTO controlArchivoTemp = entidadService.findById(ControlArchivoDTO.class, idToControlArchivoTemp);
 				DocumentoDigitalSolicitudDTO documento = new DocumentoDigitalSolicitudDTO();
 				documento.setControlArchivo(controlArchivoTemp);
 				documento.setIdToControlArchivo(idToControlArchivoTemp);
 				documento.setEsCartaCobertura((short)(idToControlArchivoTemp.equals(idToCACartaCobertura)?1:0));
 				documentoDigitalSolicitudList.add(documento);
 			}
 		}
 		return SUCCESS;
 	}
 	
	private byte[] downloadFileFromDisk(String fileName) {
		InputStream input = null;
		try {
			final String uploadFolder;
			if (System.getProperty("os.name").toLowerCase().indexOf("windows")!= -1) {
				uploadFolder = Sistema.UPLOAD_FOLDER;
			} else {
				uploadFolder = Sistema.LINUX_UPLOAD_FOLDER; 
			}
			final File file = new File(uploadFolder + fileName);
			input = new FileInputStream(file);
			return IOUtils.toByteArray(input);
		} catch (Exception e) {
			LOG.error("-- dowloadFileFromDisk()", e);
			return null;
		} finally {
			IOUtils.closeQuietly(input);
		}
	}
 	
	private String getFileName(ControlArchivoDTO controlArchivoDTO) {
		final String fileName = controlArchivoDTO.getNombreArchivoOriginal();
		final String extension = (fileName.lastIndexOf(".") == -1) ? "" : fileName
				.substring(fileName.lastIndexOf("."), fileName.length());
		return controlArchivoDTO.getIdToControlArchivo().toString() + extension;
	}
 	
 	public String adjuntarControlArchivo(){
 		if(this.getId() != null && this.getIdToControlArchivo() != null){
 			try{
 				DocumentoDigitalSolicitudDTO documento = new DocumentoDigitalSolicitudDTO();
 				documento.setCodigoUsuarioCreacion(this.getUsuarioActual().getNombreUsuario());
 				documento.setCodigoUsuarioModificacion(this.getUsuarioActual().getNombreUsuario());
 				ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, this.getIdToControlArchivo());
 				documento.setControlArchivo(controlArchivoDTO);
 				documento.setFechaCreacion(new Date());
 				documento.setFechaModificacion(new Date());
 				documento.setNombreUsuarioCreacion(this.getUsuarioActual().getNombreCompleto());
 				documento.setNombreUsuarioModificacion(this.getUsuarioActual().getNombreCompleto());
 				SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, new BigDecimal(this.getId()));
 				documento.setSolicitudDTO(solicitudDTO);
 				documento.setIdToControlArchivo(this.getIdToControlArchivo());
 				
 				entidadService.save(documento);
 			}catch(Exception e){
 				e.printStackTrace();
 			}
 		} 		
 		return SUCCESS;
 	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitudes(List<SolicitudDTO> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public List<SolicitudDTO> getSolicitudes() {
		return solicitudes;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setSeleccion(Short seleccion) {
		this.seleccion = seleccion;
	}

	public Short getSeleccion() {
		return seleccion;
	}

	public Map<Long, String> getAgenteList() {
		return agenteList;
	}

	public void setAgenteList(Map<Long, String> agenteList) {
		this.agenteList = agenteList;
	}

	public void setEstatusList(List<Short> estatusList) {
		this.estatusList = estatusList;
	}

	public List<Short> getEstatusList() {
		return estatusList;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setIdTcAgente(Integer idTcAgente) {
		this.idTcAgente = idTcAgente;
	}

	public Integer getIdTcAgente() {
		return idTcAgente;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setIdMovimiento(Integer idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public Integer getIdMovimiento() {
		return idMovimiento;
	}

	public Map<Long, String> getOficinasList() {
		return oficinasList;
	}

	public void setOficinasList(Map<Long, String> oficinasList) {
		this.oficinasList = oficinasList;
	}

	public void setNegocioList(List<Negocio> negocioList) {
		this.negocioList = negocioList;
	}

	public List<Negocio> getNegocioList() {
		return negocioList;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public Map<Long, String> getNegocioProductoList() {
		return negocioProductoList;
	}

	public void setNegocioProductoList(Map<Long, String> negocioProductoList) {
		this.negocioProductoList = negocioProductoList;
	}

	public Map<Object, String> getTiposSolicitud() {
		return tiposSolicitud;
	}

	public void setTiposSolicitud(Map<Object, String> tiposSolicitud) {
		this.tiposSolicitud = tiposSolicitud;
	}

	public Map<Integer, String> getListaEstatus() {
		return listaEstatus;
	}

	public void setListaEstatus(Map<Integer, String> listaEstatus) {
		this.listaEstatus = listaEstatus;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public InputStream getSolicitudInputStream() {
		return solicitudInputStream;
	}

	public void setSolicitudInputStream(InputStream solicitudInputStream) {
		this.solicitudInputStream = solicitudInputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}

	/**
	 * @return the negocioProducto
	 */
	public NegocioProducto getNegocioProducto() {
		return negocioProducto;
	}

	/**
	 * @param negocioProducto the negocioProducto to set
	 */
	public void setNegocioProducto(NegocioProducto negocioProducto) {
		this.negocioProducto = negocioProducto;
	}

	public void setTipoBusqueda(Short tipoBusqueda) {
		this.tipoBusqueda = tipoBusqueda;
	}

	public Short getTipoBusqueda() {
		return tipoBusqueda;
	}

	public void setDocumentoDigitalSolicitudList(
			List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudList) {
		this.documentoDigitalSolicitudList = documentoDigitalSolicitudList;
	}

	public List<DocumentoDigitalSolicitudDTO> getDocumentoDigitalSolicitudList() {
		return documentoDigitalSolicitudList;
	}

	public void setIdToControlArchivoList(List<BigDecimal> idToControlArchivoList) {
		this.idToControlArchivoList = idToControlArchivoList;
	}

	public List<BigDecimal> getIdToControlArchivoList() {
		return idToControlArchivoList;
	}

	public BigDecimal getIdToCACartaCobertura() {
		return idToCACartaCobertura;
	}

	public void setIdToCACartaCobertura(BigDecimal idToCACartaCobertura) {
		this.idToCACartaCobertura = idToCACartaCobertura;
	}

	public void setTipoConsulta(Short tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public Short getTipoConsulta() {
		return tipoConsulta;
	}

	public Map<String, String> getTipoOperaciones() {
		return tipoOperaciones;
	}

	public void setTipoOperaciones(Map<String, String> tipoOperaciones) {
		this.tipoOperaciones = tipoOperaciones;
	}
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public Boolean getTipoCabina() {
		return tipoCabina;
	}

	public void setTipoCabina(Boolean tipoCabina) {
		this.tipoCabina = tipoCabina;
	}

	

}
