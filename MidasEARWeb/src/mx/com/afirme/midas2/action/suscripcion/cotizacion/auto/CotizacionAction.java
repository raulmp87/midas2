package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.agente.AgenteDN;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo.CotizacionExtService;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CotizacionAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(CotizacionAction.class);
	private static final String COMPLEMENTAR_CREACION = "complementar";
	
	private BigDecimal id;
	private BigDecimal idToCotizacion;
	private BigDecimal cotizacionId;
	private String claveNegocio;
	private Date fechaFinal;
	private List<CotizacionDTO> cotizacionList = new ArrayList<CotizacionDTO>(1);
	private Negocio negocio;
    private CotizacionDTO cotizacion;
    private CotizacionService cotizacionService;
    private Map<Long, String> negocioList;
    private Map<Long, String> negocioProductoList;
    private Map<BigDecimal, String> negocioTipoPolizaList;
    private Map<BigDecimal, String> monedasList;
	private Map<String, String> estados;	
	private Map<String, String> municipios;
	private List<Negocio> negocioAgenteList = new ArrayList<Negocio>(1);
	private List<ProductoDTO> productoList = new ArrayList<ProductoDTO>(1);
	private ListadoService listadoService; 
	protected CotizacionFacadeRemote cotizacionFacadeRemote;
    private Map<Integer, String> formasdePagoList;
    private  List<NegocioDerechoPoliza> derechosPolizaList;
    private Double descuento;
    private Double descuentoFin;
	private NegocioService negocioService;
	private String descripcionBusquedaAgente = null;
	private List<AgenteView> agentesList;
	private Agente agente;
	private Boolean descuentoGlobal=false;
	private MensajeDTO mensajeDTO;
	private Short tipoAsegurado = (short)1;
	private String datosAsegurado;
	private String nombreAsegurado;
	private Short TipodePersona=(short)0;
	private List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
	private Map<Long, String> centrosEmisores;
	private Map<Long, String> oficinas;
	private Map<Long, String> promotorias;
	private Map<Long, String> agentes;
	private Map<Long, String> negocios;
	private BigDecimal centroEmisorId;
	private Long negocioId;
	private CotizacionExpress cotizacionExpress;
	private CalculoService calculoService;
	private IncisoCotizacionDTO incisoCotizacionDTO;
	private ResumenCostosDTO 	resumenCostosDTO;
	private BigDecimal totalPrima;
	private BigDecimal derechos;
	private BigDecimal iva;
	private BigDecimal primaTotal;
	private Boolean complementarCreacion = false;
	private NegocioProductoService negocioProductoService;
	private Map<Integer,String> listaEstatus = new LinkedHashMap<Integer, String>();
	private IncisoService incisoService;
	private EntidadService entidadService;
	private CentroEmisorService centroEmisorService;
	private BigDecimal valorPrimaTotalFin; 
	private Promotoria promotoria;
	private List<PromotoriaView> promotoriasDTOs = new ArrayList<PromotoriaView>(1);
	private String descripcionBusquedaPromotoria;
	private PromotoriaJPAService promotoriaService;
	private Promotoria filtroPromotoria = new Promotoria();
	private UsuarioService usuarioService;
	private Usuario usuario;
	private int fechaOperacionCompareTo;
	private String tabActiva;
	private boolean calcularPostGuardado = false;
	private BigDecimal primaAIgualar = null;
	private Short soloConsulta = 0;
	private AgenteMidasService agenteMidasService;
	private ValorCatalogoAgentesService catalogoService;
	private NegocioDerechosService negocioDerechosService;
	private String correo;
	private boolean correoObligatorio;
	private String nombreContratante;
	private Boolean esComplementar = false;
	private String isAgente;
	private CotizacionExtService cotizacionExtService;
	private List<ExcepcionSuscripcionReporteDTO> excepcionesList;

	//Almacenar listado de vigencias asociadas al negocio
	private Map<Integer, String> tiposVigenciaList = new HashMap<Integer, String>(1);
	private Integer vigenciaDefault = null;	
	
	public boolean isPromotoriaControlDeshabilitado() {
		return usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual") 
				|| usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual");
	}
	
	public boolean isAgenteControlDeshabilitado() {
		return usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual");
	}
	
	@Override
	public void prepare() throws Exception {
		usuario = usuarioService.getUsuarioActual();
			if (cotizacion != null && cotizacion.getSolicitudDTO() != null
					&& cotizacion.getSolicitudDTO().getIdToSolicitud() != null) {
				cotizacion.setSolicitudDTO(entidadService.findById(SolicitudDTO.class, cotizacion
						.getSolicitudDTO().getIdToSolicitud()));
			}
	}
	
	private void init(){
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			cotizacion.getSolicitudDTO().setCodigoAgente(BigDecimal.valueOf(agenteUsuarioActual.getId()));
		}
	
		
		negocioList = new HashMap<Long, String>();
		negocioProductoList = new HashMap<Long, String>();
		tiposVigenciaList = new HashMap<Integer, String>();
		if (cotizacion.getSolicitudDTO() != null
				&& cotizacion.getSolicitudDTO().getProductoDTO() != null
				&& cotizacion.getSolicitudDTO().getProductoDTO()
						.getIdToProducto() != null) {
			negocioProductoList = listadoService
					.getMapNegProductoPorNegocio(cotizacion.getSolicitudDTO()
							.getNegocio().getIdToNegocio());
			if (cotizacion.getNegocioTipoPoliza() == null) {
				cotizacion.setNegocioTipoPoliza(new NegocioTipoPoliza());
				cotizacion.getNegocioTipoPoliza().setNegocioProducto(
						new NegocioProducto());
			}
			cotizacion.getNegocioTipoPoliza().setNegocioProducto(
					entidadService.findById(NegocioProducto.class, cotizacion
							.getSolicitudDTO().getProductoDTO()
							.getIdToProducto().longValue()));
			negocioTipoPolizaList = listadoService
					.getMapNegTipoPolizaPorNegProducto(cotizacion
							.getNegocioTipoPoliza().getNegocioProducto()
							.getIdToNegProducto());	
			tiposVigenciaList = listadoService.obtenerTiposVigenciaNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			vigenciaDefault = listadoService.obtenerVigenciaDefault(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		}
		if (negocioTipoPolizaList == null) {
			negocioTipoPolizaList = new HashMap<BigDecimal, String>();
		} 
		monedasList = new HashMap<BigDecimal, String>();
		formasdePagoList = new HashMap<Integer, String>();
	}
	@SuppressWarnings("deprecation")
	public void prepareListar() {
		
		// negocios activos
		negocio = new Negocio();
		negocio.setClaveNegocio(claveNegocio);
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		negocioAgenteList = negocioService.findByFilters(negocio);

		if (usuarioService
				.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| usuarioService
						.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService
					.getAgenteUsuarioActual();
			if (usuarioService
					.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
				agente = agenteUsuarioActual;
				negocioAgenteList = negocioService
						.listarNegociosPorAgenteUnionNegociosLibres(agente
								.getId().intValue(), Negocio.CLAVE_NEGOCIO_AUTOS);
			}
			promotoria = agenteUsuarioActual.getPromotoria();
			// Extrañamente utilizan en el jsp promotoria.id como
			// promotoria.idPromotoria
			promotoria.setId(promotoria.getIdPromotoria());
		}

		if (cotizacion != null) {
			cotizacion.setTipoPolizaDTO(new TipoPolizaDTO());
		}
		if (listaEstatus.isEmpty()) {
			try {
				List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
						.getInstancia()
						.buscarPorPropiedad(
								"id.idGrupoValores",
								Integer.valueOf(CatalogoValorFijoDTO.GRUPO_CLAVE_ESTATUS_COTIZACION));
				if (!catalogoValorFijoDTOs.isEmpty()
						&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						if (estatus.getId().getIdDato() != 13) {
							listaEstatus.put(estatus.getId().getIdDato(),
									estatus.getDescripcion());
						}
					}
				}

			} catch (SystemException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		centrosEmisores = centroEmisorService.listarCentrosEmisoresMap();
		super.removeMapEnSession();
	}
	public String listar(){		
		return SUCCESS;
	}
	
	public String busquedaRapidaPaginada(){
		if (cotizacion == null){
			cotizacion = new CotizacionDTO();
			cotizacion.setClaveEstatus(new Short("10"));
		}
		cotizacion.setIdCentroEmisor(centroEmisorId);
		if(getTotalCount() == null){
			setTotalCount(cotizacionService.obtenerTotalFiltradoCotizacion(cotizacion));
			setListadoEnSession(null);
		}
		setPosPaginado();		
		return SUCCESS;
	}
	
	
	@SuppressWarnings("unchecked")
	public String listarFiltrado() throws Exception {
		if (cotizacion == null){
			cotizacion = new CotizacionDTO();
		}
		cotizacion.setPrimerRegistroACargar(getPosStart());
		cotizacion.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
		cotizacion.setIdCentroEmisor(centroEmisorId);
		Map<Integer,List<?>> map = (Map<Integer, List<?>>) getMapEnSession();
		// Si no Existe el mapa en session lo mete en session y crea la primera pagina
		if( map == null){
			List<CotizacionDTO> temp = cotizacionService.listarFiltrado(cotizacion);
			map = new HashMap<Integer, List<?>>();
			map.put(1, temp);
			cotizacionList = (List<CotizacionDTO>)map.get(getPosActual());
			setMapEnSession(map);
		}else{
			if(map.containsKey(getPosActual())){
				cotizacionList = (List<CotizacionDTO>)map.get(getPosActual());
			}else{
				List<CotizacionDTO> temp = cotizacionService.listarFiltrado(cotizacion);
				map.put(getPosActual(), temp);
				cotizacionList = (List<CotizacionDTO>) map.get(getPosActual());
				if(!setMapEnSession(map)){
					throw new Exception(BaseAction.MENSAJE_ERROR_GENERAL);
				}
			}
		}
			
		return SUCCESS;
	}
	
	public String busquedaPaginada(){
		if(cotizacion == null){
			cotizacion = new CotizacionDTO();
		}
		cotizacion.setIdCentroEmisor(centroEmisorId);
			if(getTotalCount() == null){
				if (cotizacion.getFechaCreacion() != null) {
					if (cotizacion.getFechaCreacion().after(fechaFinal)) {
						super.setMensaje("midas.suscripcion.cotizacion.mensaje.error");
						return SUCCESS;
					}
				}
			if (incisoCotizacionDTO != null && incisoCotizacionDTO.getIncisoAutoCot().getDescripcionFinal() !=  null && !incisoCotizacionDTO.getIncisoAutoCot().getDescripcionFinal().equals("")) {
				
				if (incisoCotizacionDTO != null && StringUtils.isNotBlank(incisoCotizacionDTO.getIncisoAutoCot().getDescripcionFinal())) { 
					cotizacion.setIncisoCotizacionDTOs(new LinkedList<IncisoCotizacionDTO>());
					cotizacion.getIncisoCotizacionDTOs().add(incisoCotizacionDTO);
				}
				setTotalCount ((long)cotizacionService.buscarCotizacion(cotizacion,
						cotizacion.getFechaCreacion(),
						fechaFinal, valorPrimaTotalFin, agente,
						promotoria,getDescuento(),getDescuentoFin()).size());
			} else {
				setTotalCount(cotizacionService.obtenerTotalFiltradoCotizacion(
						cotizacion, cotizacion.getFechaCreacion(), fechaFinal,
						valorPrimaTotalFin, agente, promotoria,getDescuento(),getDescuentoFin()));
				setListadoEnSession(null);
			}
			}
			setPosPaginado();		
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public String buscarCotizacion(){
		removeMapEnSession();
		if (cotizacion == null) {
			cotizacion = new CotizacionDTO();
		}
		cotizacion.setIdCentroEmisor(centroEmisorId);
		if (cotizacion.getFechaCreacion() != null) {
			if (cotizacion.getFechaCreacion().after(fechaFinal)) {
				super.setMensaje("midas.suscripcion.cotizacion.mensaje.error");
			    return SUCCESS;
		    }
	      }
		cotizacion.setPrimerRegistroACargar(getPosStart());
		// Busca por descripcion 
		if (incisoCotizacionDTO != null && StringUtils.isNotBlank(incisoCotizacionDTO.getIncisoAutoCot().getDescripcionFinal())) {
			cotizacion.setIncisoCotizacionDTOs(new LinkedList<IncisoCotizacionDTO>());
			if (cotizacion.getIdToCotizacion() != null) {
				incisoCotizacionDTO.setId(new IncisoCotizacionId());
				incisoCotizacionDTO.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());
			}
			cotizacion.getIncisoCotizacionDTOs().add(incisoCotizacionDTO);
			List<CotizacionDTO> cotizacionesFiltradas = cotizacionService.buscarCotizacion(cotizacion, cotizacion.getFechaCreacion(), fechaFinal, valorPrimaTotalFin, agente, promotoria,getDescuento(),getDescuentoFin());
			cotizacionList = cotizacionesFiltradas.subList(getPosStart(), getPosFinish());
			setListadoEnSession(cotizacionList);
			return  SUCCESS;
		}
		

		// Busca por los demas filtros
		cotizacion.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
		if (!listadoDeCache()) {
			cotizacionList = cotizacionService.buscarCotizacion(cotizacion, cotizacion.getFechaCreacion(), fechaFinal, valorPrimaTotalFin, agente, promotoria,getDescuento(),getDescuentoFin());
			setListadoEnSession(cotizacionList);
		} else {
			cotizacionList = (List<CotizacionDTO>) getListadoPaginado();
			setListadoEnSession(cotizacionList);
		}
		
		return SUCCESS;
	}	
	
	public String iniciarCotizacionFormal() {
		init();
		if(cotizacion.getSolicitudDTO().getCodigoAgente() != null){
			negocioAgenteList = negocioService
					.listarNegociosPorAgenteUnionNegociosLibres(
							Integer.valueOf(cotizacion.getSolicitudDTO()
									.getCodigoAgente().intValue()), "A");
			if(negocioAgenteList.size() == 1){
				if(cotizacion.getSolicitudDTO().getNegocio() == null){
					cotizacion.getSolicitudDTO().setNegocio(new Negocio());
				}
				cotizacion.getSolicitudDTO().getNegocio().setIdToNegocio(negocioAgenteList.get(0).getIdToNegocio());				
			}	
				final Agente filtroAgente = new Agente();
				filtroAgente.setId(cotizacion.getSolicitudDTO().getCodigoAgente().longValue());
				final List<AgenteView> agenteViewResult =  agenteMidasService.findByFilterLightWeight(filtroAgente);
				if (agenteViewResult != null && !agenteViewResult.isEmpty()) {
					agente = new Agente();
					agente.setIdAgente(agenteViewResult.get(0).getIdAgente());
					agente.setPersona(new Persona());
					agente.getPersona().setNombreCompleto(agenteViewResult.get(0).getNombreCompleto());
				}

		}
		return SUCCESS;
	}
	
	
	public void prepareCrearCotizacion(){
		if(cotizacion.getIdToCotizacion()!=null)
			cotizacion = cotizacionFacadeRemote.findById(cotizacion.getIdToCotizacion());
	}
	
	public String crearCotizacion() throws Exception {
	 Long codigoAgente = cotizacion.getSolicitudDTO().getCodigoAgente().longValue();
	 Agente agenteActual = entidadService.findById(Agente.class, codigoAgente);
	 ValorCatalogoAgentes tipoCedula = agenteActual.getTipoCedula();
	 BigDecimal idNegTipoPoliza = cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza();
	 NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idNegTipoPoliza);
	 TipoPolizaDTO tipoPoliza = negocioTipoPoliza.getTipoPolizaDTO();
	 
		 
	boolean esCedulaValida = AgenteDN.getInstancia().validarCedulaAgente(tipoCedula.getId(), AgenteDN.PROPIEDAD_TIPOPOLIZA, tipoPoliza.getIdToTipoPoliza());
	 if(!esCedulaValida) {
		 String mensajeError = String.format("El agente de Tipo de Cedula %s No cuenta con permisos de venta para el tipo de poliza %s seleccionada", tipoCedula.getClave(), tipoPoliza.getDescripcion());
		 this.setMensaje(mensajeError);
		 return ERROR;
	 }
	 if (cotizacion == null) {
		 cotizacion = new CotizacionDTO();
	 }
	 if (complementarCreacion){
		 cotizacion = cotizacionService.complementarCreacionCotizacion(cotizacion);
	 }else{
		 cotizacion =cotizacionService.crearCotizacion(cotizacion,null);
	 }
	 

	 return SUCCESS;	
	}


   public void prepareVerDetalleCotizacion(){
		  if(getId()!=null){
			  //Elimina incisos no guardados
			  incisoService.borrarIncisoAutoCotNoAsignados(getId());
			  
			  //Necesario ya que hay veces que trae la cotizacion con datos no actualizados.
			  cotizacion = entidadService.evictAndFindById(CotizacionDTO.class,getId());
			  
			  // Obtiene la comision cedida default del negocio
			  if (cotizacion.getSolicitudDTO().getNegocio().getPctPrimaCeder() != null && cotizacion.getPorcentajebonifcomision() == null) {
				cotizacion.setPorcentajebonifcomision(cotizacion.getSolicitudDTO().getNegocio().getPctPrimaCeder());  
			  }
			  // Obtiene el descuento global default
			  if (cotizacion.getSolicitudDTO().getNegocio().getPctDescuentoDefault() != null && cotizacion.getPorcentajeDescuentoGlobal() == null) {
				  cotizacion.setPorcentajeDescuentoGlobal(new Double(0));//Dejara de aplicarse el descuento global
			  }
			  setCotizacionId(id);
		  }
		  setListadoEnSession(null);
   }
   
	public String verDetalleCotizacion(){		
				
		Double porcentajeRecargoPagoFraccionado = null;	
		if(cotizacion!=null && cotizacion.getTipoPolizaDTO()!=null){
	    	formasdePagoList    = listadoService.getMapFormasdePago(cotizacion);
	    	//derechosPolizaList  = cotizacion.getSolicitudDTO().getNegocio().getNegocioDerechoPolizas();
	    	derechosPolizaList  = negocioDerechosService.obtenerDerechosPoliza(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
	    	if(cotizacion.getNegocioDerechoPoliza()==null){
	    		cotizacion.setNegocioDerechoPoliza(new NegocioDerechoPoliza());
		    	for(NegocioDerechoPoliza negocioDerechoPoliza: derechosPolizaList ){
		    		if(negocioDerechoPoliza.getClaveDefault()){
		    			cotizacion.getNegocioDerechoPoliza().setIdToNegDerechoPoliza(negocioDerechoPoliza.getIdToNegDerechoPoliza());
		    		}
		    	}
	    	}
	    	
	    	if(derechosPolizaList != null && !derechosPolizaList.isEmpty()){
				Collections.sort(derechosPolizaList, 
						new Comparator<NegocioDerechoPoliza>() {				
							public int compare(NegocioDerechoPoliza n1, NegocioDerechoPoliza n2){
								return n1.getImporteDerecho().compareTo(n2.getImporteDerecho());
							}
						});
	    	}
	    	cotizacion     = cotizacionService.descuentoGlobalCalculado(cotizacion);
	    	cotizacion = cotizacionService.getFechasDefaults(cotizacion);
	    	
	    	// Obtiene la fecha fin vigencia fija del negocio
			if (cotizacion.getSolicitudDTO().getNegocio()
					.getFechaFinVigenciaFija() != null) {
				cotizacion.setFechaFinVigencia(cotizacion.getSolicitudDTO()
						.getNegocio().getFechaFinVigenciaFija());
			}
	        negocioList = new LinkedHashMap<Long, String>();
	        if(cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas() == 0){
	        	List<IncisoCotizacionDTO> dtos = incisoService.findByCotizacionId(cotizacion.getIdToCotizacion());
	        	if(dtos != null && !dtos.isEmpty() ){
	        		incisoCotizacionDTO = dtos.get(0);
	        	}
	        }
	        
	        if(cotizacion.getPorcentajePagoFraccionado() == null && cotizacion.getIdFormaPago() != null && 
	        		cotizacion.getIdMoneda() != null){
	        	porcentajeRecargoPagoFraccionado = listadoService.getPctePagoFraccionado(cotizacion.getIdFormaPago().intValue(), 
	        			cotizacion.getIdMoneda().shortValue());
				cotizacion.setPorcentajePagoFraccionado(porcentajeRecargoPagoFraccionado);				
	        }
	        
	        //Set descripcion moneda
	        try{
	        	MonedaDTO moneda = entidadService.findById(MonedaDTO.class, cotizacion.getIdMoneda().shortValue());
	        	cotizacion.setDescripcionMoneda(moneda.getDescripcion());
	        }catch(Exception e){
	        	
	        }
	        //Valida cambiodescuento
//	        if(cotizacionService.calcularCambioDescuento(cotizacion.getIdToCotizacion())){
//	        	this.setMensajeExitoPersonalizado("Se recalcular\u00F3n los incisos por cambio de descuento aplicado.");
//	        }
	        tiposVigenciaList = listadoService.obtenerTiposVigenciaNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
	        vigenciaDefault = listadoService.obtenerVigenciaDefault(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			return SUCCESS;
		}else{
			init();
			if(cotizacion.getSolicitudDTO().getCodigoAgente() != null){				
				agente = entidadService.findById(Agente.class, cotizacion.getSolicitudDTO().getCodigoAgente().longValue());
				negocioAgenteList = negocioService.listarNegociosPorAgenteUnionNegociosLibres(Integer.valueOf(cotizacion.getSolicitudDTO().getCodigoAgente().intValue()),"A");
				negocioProductoList = listadoService.getMapNegProductoPorNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
				NegocioProducto negocioProducto = negocioProductoService.getNegocioProductoByCotizacion(cotizacion);
				cotizacion.setNegocioTipoPoliza(new NegocioTipoPoliza());
				cotizacion.getNegocioTipoPoliza().setNegocioProducto(negocioProducto);
				negocioTipoPolizaList = listadoService.getMapNegTipoPolizaPorNegProducto(negocioProducto.getIdToNegProducto());
				complementarCreacion = true;
			}
			return COMPLEMENTAR_CREACION;
		}
	}      

	public void prepareValidarDescuento(){
		if(getId()!=null){
		  cotizacion = cotizacionFacadeRemote.findById(getId());
		 }
	}
	
	
	public void prepareValidarFechas(){
		if(getId()!=null){
		  cotizacion = cotizacionFacadeRemote.findById(getId());
		 }
	}

	public void prepareMostrarVentanaAsegurado(){
		if(getId()!=null){
		  cotizacion = cotizacionFacadeRemote.findById(getId());
		 }
	}	
	public String mostrarVentanaAsegurado(){
		TipodePersona = ((Short) cotizacion.getSolicitudDTO().getClaveTipoPersona());	
		 return SUCCESS;
	}
	
	
	public void prepareAseguradoDatos(){
		if(getId()!=null){
		  cotizacion = cotizacionFacadeRemote.findById(getId());
		 }
	}
	public String aseguradoDatos(){
	    cotizacionService.getDatosAsegurado(cotizacion,getTipoAsegurado());
		 return SUCCESS;
	}	

	public void prepareActualizarDatosAsegurados(){
		if(cotizacion.getIdToCotizacion()!=null){
		  cotizacion = cotizacionFacadeRemote.findById(cotizacion.getIdToCotizacion());
		 }
	}
	
	public String actualizarDatosAsegurados(){
		nombreAsegurado = cotizacionService.actualizarAsegurado(cotizacion,getTipoAsegurado());	
		 return SUCCESS;
	}
	
	public void validateGuardarCotizacion(){
		List<String> mensajesError = new ArrayList<String>(1);
		if(cotizacion.getIdFormaPago() == null || cotizacion.getIdFormaPago().equals(new BigDecimal(-1))){
			mensajesError.add("Forma de Pago no debe ser nulo");
			addFieldError("cotizacion.idFormaPago", "");
		}
		if(cotizacion.getPorcentajePagoFraccionado() == null){
			mensajesError.add("Porcentaje de Pago Fraccionado no debe ser nulo");
			addFieldError("cotizacion.porcentajePagoFraccionado", "");
		}
		if(cotizacion.getFechaInicioVigencia() == null){
			mensajesError.add("Fecha Inicio Vigencia no debe ser nulo");
			addFieldError("cotizacion.fechaInicioVigencia", "");
		}
		if(cotizacion.getFechaFinVigencia() == null){
			mensajesError.add("Fecha Fin Vigencia no debe ser nulo");
			addFieldError("cotizacion.fechaFinVigencia", "");
		}
		if(cotizacion.getPorcentajeDescuentoGlobal() == null){
			mensajesError.add("Descuento Global no debe ser nulo");
			addFieldError("cotizacion.porcentajeDescuentoGlobal", "");
		}
		if(cotizacion.getPorcentajebonifcomision() == null){
			mensajesError.add("Comisi\u00F3n Cedida no debe ser nulo");
			addFieldError("cotizacion.porcentajebonifcomision", "");
		}
		if(cotizacion.getNegocioDerechoPoliza() != null){
			if(cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza() == null){
				mensajesError.add("Derecho Poliza no debe ser nulo");
				addFieldError("cotizacion.negocioDerechoPoliza.idToNegDerechoPoliza", "");
			}
		}
		formasdePagoList    = listadoService.getMapFormasdePago(cotizacion);
    	//derechosPolizaList  = cotizacion.getSolicitudDTO().getNegocio().getNegocioDerechoPolizas();
    	derechosPolizaList = negocioDerechosService.obtenerDerechosPoliza(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
    	if(derechosPolizaList != null && !derechosPolizaList.isEmpty()){
			Collections.sort(derechosPolizaList, 
					new Comparator<NegocioDerechoPoliza>() {				
						public int compare(NegocioDerechoPoliza n1, NegocioDerechoPoliza n2){
							return n1.getImporteDerecho().compareTo(n2.getImporteDerecho());
						}
					});
    	}
    	cotizacion = cotizacionService.descuentoGlobalCalculado(cotizacion);
    	cotizacion = cotizacionService.getFechasDefaults(cotizacion);
    	if(this.hasErrors()){
    		this.setMensajeListaPersonalizado("ERROR", mensajesError, MensajeDTO.TIPO_MENSAJE_ERROR);
    	}
	}
	
	
	public void prepareGuardarCotizacion(){
		if(cotizacion.getIdToCotizacion()!=null){
		  Date inicioVigencia = cotizacion.getFechaInicioVigencia();
		  Date finVigencia = cotizacion.getFechaFinVigencia();
		  //Dejara de aplicarse el descuento global
		  Double descuentoGlobal = 0d;
		  Double descuentoVolumen = cotizacionService.descuentoGlobalCalculado(cotizacion).getDescuentoVolumen();		 
		  cotizacion = cotizacionFacadeRemote.findById(cotizacion.getIdToCotizacion());
		  if((cotizacion.getFechaInicioVigencia() != null && cotizacion.getFechaFinVigencia() != null && 
				  (inicioVigencia.getTime() != cotizacion.getFechaInicioVigencia().getTime() || 
				  finVigencia.getTime() != cotizacion.getFechaFinVigencia().getTime())) ||
				  (descuentoGlobal != null && cotizacion.getPorcentajeDescuentoGlobal() == null) ||
				  (descuentoGlobal.doubleValue() != cotizacion.getPorcentajeDescuentoGlobal().doubleValue()) ||
				  (descuentoVolumen != null && (cotizacion.getDescuentoVolumen() == null || 
						  descuentoVolumen.doubleValue() != cotizacion.getDescuentoVolumen().doubleValue()))){
			  calcularPostGuardado = true;
			  cotizacion.setIgualacionNivelCotizacion(false);
		  }else{
			  calcularPostGuardado = false;
		  }
		  if(cotizacion.getTipoImpresionCliente() == null){
			  cotizacion.setTipoImpresionCliente(BigDecimal.ONE);
		  }
		 }
	}
	
	public String guardarCotizacion(){
		long start = System.currentTimeMillis();
		LOG.info("Entrando a guardarCotizacion");
		try{
			if( cotizacionService.guardarCotizacion(cotizacion) != null ){
	    	if(calcularPostGuardado){
	    		cotizacionService.calcularTodosLosIncisos(cotizacion.getIdToCotizacion());
	    	}
	    	//FIXME:Action chaining? Totally wrong.
	    	verDetalleCotizacion();
	    }else{
	    	super.setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
	    }	
			
		}catch(RuntimeException e){
			super.setMensajeError(e.getMessage());
		}
		LOG.info("Saliendo de guardarCotizacion : " 
				 + ((System.currentTimeMillis() - start) / 1000.0));
		 return SUCCESS;
	} 	


	public String ventanaEmails(){
		return SUCCESS;
	}
	
	public String ventanaAgentes(){
		return SUCCESS;
	}
	
	public String ventanaAgentesBuscar(){
		return SUCCESS;
	}
	
	public void prepareBuscarAgente(){
		if(descripcionBusquedaAgente != null){
			agentesList = listadoService.listaAgentesPorDescripcionLightWeightAutorizados(descripcionBusquedaAgente.trim().toUpperCase());
		}else{
			agentesList = new ArrayList<AgenteView>(1);
		}		
	}
	
	public String buscarAgente(){
		return SUCCESS;
	}
	
	public void prepareBuscarAgenteBuscar(){
		if(descripcionBusquedaAgente != null){
			agentesList = listadoService.listaAgentesPorDescripcionLightWeight(descripcionBusquedaAgente.trim().toUpperCase());
		}else{
			agentesList = new ArrayList<AgenteView>();
		}		
	}
	
	public String buscarAgenteBuscar(){
		return SUCCESS;
	}
	
	public void prepareVerEsquemaPago(){
		if(cotizacion.getIdToCotizacion()!=null){
			  cotizacion = cotizacionFacadeRemote.findById(cotizacion.getIdToCotizacion());
		}
	}
	
	public void prepareVerEsquemaPagoAgente(){
		if(cotizacion.getIdToCotizacion()!=null){
			  cotizacion = cotizacionFacadeRemote.findById(cotizacion.getIdToCotizacion());
		}
	}
	
	public String copiarCotizacion(){		
		setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		CotizacionDTO cotizacionDTO= new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(idToCotizacion);
		cotizacion = cotizacionService.copiarCotizacion(cotizacionDTO);
		if(cotizacion.getIdToCotizacion()!=null){
			setMensaje(MensajeDTO.MENSAJE_COPIAR_COTIZACION+idToCotizacion+" y se ha creado una nueva con el n\u00Famero:"+cotizacion.getIdToCotizacion());
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
			return SUCCESS;
		}
		else{
			setMensaje(MensajeDTO.MENSAJE_ERROR_COPIAR_COTIZACION);
			setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			return ERROR;
		}		
	}
	
	public String verEsquemaPago(){
		try{
			esquemaPagoCotizacionList=cotizacionService.setEsquemaPagoCotizacion(cotizacion,cotizacion.getFechaInicioVigencia(), cotizacion.getFechaInicioVigencia(),cotizacion.getFechaFinVigencia(),cotizacion.getValorTotalPrimas(), new BigDecimal(cotizacion.getValorDerechosUsuario()), new BigDecimal(cotizacion.getPorcentajeIva()), cotizacion.getValorPrimaTotal());
		}catch(Exception e){
			esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
		}
		return SUCCESS;
	}
	
	public String verEsquemaPagoAgente(){
		try{
			esquemaPagoCotizacionList=cotizacionService.setEsquemaPagoCotizacion(cotizacion,cotizacion.getFechaInicioVigencia(), cotizacion.getFechaInicioVigencia(),cotizacion.getFechaFinVigencia(),cotizacion.getValorTotalPrimas(), new BigDecimal(cotizacion.getValorDerechosUsuario()), new BigDecimal(cotizacion.getPorcentajeIva()), cotizacion.getValorPrimaTotal());
		}catch(Exception e){
			esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
		}
		return SUCCESS;
	}
	
	public String mostrarResumenTotalesCotizacion() {
		try {
			LOG.info("mostrarResumenTotalesCotizacion.cotizacion: " + cotizacion);
			String mensaje = "";//JFGG
			boolean statusExcepcion = false;
			try {//JFGG
				CotizacionDTO cotizacionDTO = cotizacionExtService.getCotizacion(cotizacion.getIdToCotizacion());
				LOG.info("mostrarResumenTotalesCotizacion.cotizacionDTO: " + cotizacionDTO);
				Map<String, Object> map = cotizacionExtService.evalueExcepcion(cotizacion, true);
				if(map!= null && !map.containsKey("statusExcepcion") && "true".equals(String.valueOf(map.get("statusExcepcion")))){
					 mensaje = String.valueOf(map.get("statusExcepcion"));
					 statusExcepcion = true;
				    setMensajeError(mensaje);
				}
			}catch(Exception e) {
				statusExcepcion = false;
				mensaje = "";
				LOG.error(e.getMessage(), e);
			}//JFGG
			resumenCostosDTO = calculoService.obtenerResumenCotizacion(cotizacion, esComplementar, statusExcepcion);//JFGG
			//JFGG
			resumenCostosDTO.setStatusExcepcion(statusExcepcion);
			resumenCostosDTO.setShowExcepcion(true);
			resumenCostosDTO.setMensajeExcepcion(mensaje);
			//JFGG
		}catch(Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	public String mostrarResumenTotalesCotizacionAgente() {
		try {
			LOG.info("mostrarResumenTotalesCotizacionAgente.cotizacion: " + cotizacion);
			
			String mensaje = null;//JFGG
			boolean statusExcepcion = false;
			try {
				CotizacionDTO cotizacionDTO = cotizacionExtService.getCotizacion(cotizacion.getIdToCotizacion());
				LOG.info("mostrarResumenTotalesCotizacionAgente.cotizacionDTO: " + cotizacionDTO);
				Map<String, Object> map = cotizacionExtService.evalueExcepcion(cotizacionDTO, true);
				if(map!= null && !map.containsKey("statusExcepcion") && "true".equals(String.valueOf(map.get("statusExcepcion")))){//JFGG
					 mensaje = String.valueOf(map.get("statusExcepcion"));
					 statusExcepcion = true;
				}
			} catch(Exception e) {
				statusExcepcion = false;
				mensaje = "";
				LOG.error(e.getMessage(), e);
			}//JFGG
			resumenCostosDTO = calculoService.obtenerResumenCotizacion(cotizacion, esComplementar, statusExcepcion);//JFGG
			//JFGG
			resumenCostosDTO.setStatusExcepcion(statusExcepcion);
			resumenCostosDTO.setShowExcepcion(true);
			resumenCostosDTO.setMensajeExcepcion(mensaje);
			//JFGG
		}catch(Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return SUCCESS;
	}
	
	public String validarDatosComplementarios() {
		long start = System.currentTimeMillis();
		LOG.info("Entrando a validarDatosComplementarios");
		if(idToCotizacion != null){
			mensajeDTO = cotizacionService.validarListaParaEmitir(idToCotizacion);
		}
		LOG.info("Saliendo de validarDatosComplementarios, idCotizacion: " + idToCotizacion + " " 
				 + ((System.currentTimeMillis() - start) / 1000.0));
		return SUCCESS;
	}
	
	public String actualizarEstatusCotizacion() {
		if(idToCotizacion != null){
			cotizacionService.actualizarEstatusCotizacion(idToCotizacion);
			nombreContratante = cotizacion.getSolicitudDTO().getNombreCompleto();
		}
		LOG.info(":::::::::::  [ INFO ]  :::::::::::::  CORREO ELECTRÓNICO CLIENTE " + correo);
		LOG.info(":::::::::::  [ INFO ]  :::::::::::::  CORREO CLIENTE " + nombreContratante);
		return SUCCESS;
	}
	
	public String cambiarEstatusEnProceso(){
		if(getId() != null){
			cotizacion = cotizacionFacadeRemote.findById(getId());
			cotizacionService.actualizarEstatusCotizacionEnProceso(getId());
		}
		if(getMensaje()== null){
			super.setMensajeExito();
		}
		if(isAgente!=null && "S".equals(isAgente)){
			return "agentsuccess";
		}else{
			return SUCCESS;
		}
	}
	
	public String cancelarCotizacion(){
		if(getId() != null){
			cotizacion = cotizacionFacadeRemote.findById(getId());
			cotizacionService.cancelarCotizacion(getId());
		}
		super.setMensajeExito();
		return SUCCESS;
	}
	
	/**
	 * Autocomplete en listarcotizacion.jsp
	 * @return
	 * @autor martin
	 */
	public String promotoria(){
		filtroPromotoria.setDescripcion(descripcionBusquedaPromotoria);
		if(descripcionBusquedaPromotoria != null){
			promotoriasDTOs = promotoriaService.findByFiltersView(filtroPromotoria);
		}
		return "promotoria";
	}
	
	
	public void prepareValidaFechaOperacion(){
		if(cotizacion.getIdToCotizacion()!=null){
		  cotizacion = cotizacionFacadeRemote.findById(cotizacion.getIdToCotizacion());
		 }
	}
	
	public String validaFechaOperacion(){
		 if(cotizacion == null){
		  	cotizacion = new CotizacionDTO();
		 }
		 fechaOperacionCompareTo=cotizacionService.validaFechaOperacion(cotizacion);
		if (fechaOperacionCompareTo==-1){
		 return SUCCESS;
		}
		return SUCCESS;
	}
	
	public String verIgualarPrimas(){		
		return SUCCESS;
	}
	
	public String igualarPrimas(){
		try{
			if(idToCotizacion == null){
				throw new RuntimeException("No existe cotizacion");
			}
			if(primaAIgualar == null){
				throw new RuntimeException("Prima Total debe existir y ser mayor a cero");
			}
			calculoService.igualarPrima(idToCotizacion, primaAIgualar.doubleValue(), true);
			super.setNextFunction("verDetalleCotizacion("+idToCotizacion+",0)");
			super.setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage());
		}
		return SUCCESS;	
	}
	
	public String restaurarPrimaCotizacion(){
		try{
			if(idToCotizacion == null){
				throw new RuntimeException("No existe cotizacion");
			}			
			calculoService.restaurarPrimaCotizacion(idToCotizacion);
			super.setNextFunction("verDetalleCotizacion("+idToCotizacion+",0)");
			super.setMensajeExito();
		}catch(Exception ex){
			super.setMensajeError(ex.getMessage());
		}
		return SUCCESS;	
	}
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public List<CotizacionDTO> getCotizacionList() {
		return cotizacionList;
	}

	public void setCotizacionList(List<CotizacionDTO> cotizacionList) {
		this.cotizacionList = cotizacionList;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}


	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Map<Long, String> getNegocioList() {
		return negocioList;
	}


	public void setNegocioList(Map<Long, String> negocioList) {
		this.negocioList = negocioList;
	}



	public Map<Integer, String> getFormasdePagoList() {
		return formasdePagoList;
	}

	public void setFormasdePagoList(Map<Integer, String> formasdePagoList) {
		this.formasdePagoList = formasdePagoList;
	}


	public  List<NegocioDerechoPoliza> getDerechosPolizaList() {
		return derechosPolizaList;
	}


	public void setDerechosPolizaList( List<NegocioDerechoPoliza> derechosPolizaList) {
		this.derechosPolizaList = derechosPolizaList;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
	
	public IncisoCotizacionDTO getIncisoCotizacionDTO() {
		return incisoCotizacionDTO;
	}

	public void setIncisoCotizacionDTO(IncisoCotizacionDTO incisoCotizacionDTO) {
		this.incisoCotizacionDTO = incisoCotizacionDTO;
	}

	@Autowired
	@Qualifier("cotizacionExtServiceEJB")
	public void setCotizacionExtService(CotizacionExtService cotizacionExtService) {//JFGG
		this.cotizacionExtService = cotizacionExtService;
	}
	
	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@Autowired
	@Qualifier("cotizacionFacadeRemoteEJB")
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}


	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}


	@Autowired
	@Qualifier("negocioServiceEJB")
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}	
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("negocioProductoServiceEJB")
	public void setNegocioProductoService(
			NegocioProductoService negocioProductoService) {
		this.negocioProductoService = negocioProductoService;
	}
	
	
	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}
	
	public Map<String, String> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(Map<String, String> municipios) {
		this.municipios = municipios;
	}
	
	public String verDetalleInciso(){
		estados = listadoService.getMapEstadosMX();
		municipios = new LinkedHashMap<String, String>();
		
		return SUCCESS;
	} 
	
    public Map<Long, String> getNegocioProductoList() {
		return negocioProductoList;
	}

	public void setNegocioProductoList(Map<Long, String> negocioProductoList) {
		this.negocioProductoList = negocioProductoList;
	}
	
	 public Map<BigDecimal, String> getNegocioTipoPolizaList() {
			return negocioTipoPolizaList;
		}

		public void setNegocioTipoPolizaList(Map<BigDecimal, String> negocioTipoPolizaList) {
			this.negocioTipoPolizaList = negocioTipoPolizaList;
		}

		public Map<BigDecimal, String> getMonedasList() {
			return monedasList;
		}

		public void setMonedasList(Map<BigDecimal, String> monedasList) {
			this.monedasList = monedasList;
		}

		public void setNegocioAgenteList(List<Negocio> negocioAgenteList) {
			this.negocioAgenteList = negocioAgenteList;
		}

		public List<Negocio> getNegocioAgenteList() {
			return negocioAgenteList;
		}

		public void setAgentesList(List<AgenteView> agentesList) {
			this.agentesList = agentesList;
		}

		public List<AgenteView> getAgentesList() {
			return agentesList;
		}

		public void setDescripcionBusquedaAgente(
				String descripcionBusquedaAgente) {
			this.descripcionBusquedaAgente = descripcionBusquedaAgente;
		}

		public String getDescripcionBusquedaAgente() {
			return descripcionBusquedaAgente;
		}

		public void setAgente(Agente agente) {
			this.agente = agente;
		}

		public Agente getAgente() {
			return agente;
		}

		public void setEsquemaPagoCotizacionList(
				List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList) {
			this.esquemaPagoCotizacionList = esquemaPagoCotizacionList;
		}

		public List<EsquemaPagoCotizacionDTO> getEsquemaPagoCotizacionList() {
			return esquemaPagoCotizacionList;
		}

		public Boolean getDescuentoGlobal() {
			return descuentoGlobal;
		}

		public void setDescuentoGlobal(Boolean descuentoGlobal) {
			this.descuentoGlobal = descuentoGlobal;
		}

		public List<ProductoDTO> getProductoList() {
			return productoList;
		}

		public void setProductoList(List<ProductoDTO> productoList) {
			this.productoList = productoList;
		}


		public Map<Long, String> getOficinas() {
			return oficinas;
		}


		public void setOficinas(Map<Long, String> oficinas) {
			this.oficinas = oficinas;
		}


		public Map<Long, String> getPromotorias() {
			return promotorias;
		}


		public void setPromotorias(Map<Long, String> promotorias) {
			this.promotorias = promotorias;
		}


		public Map<Long, String> getAgentes() {
			return agentes;
		}


		public void setAgentes(Map<Long, String> agentes) {
			this.agentes = agentes;
		}


		public Map<Long, String> getCentrosEmisores() {
			return centrosEmisores;
		}


		public void setCentrosEmisores(Map<Long, String> centrosEmisores) {
			this.centrosEmisores = centrosEmisores;
		}


		public Object getCentroEmisorId() {
			return centroEmisorId;
		}


		public void setCentroEmisorId(BigDecimal centroEmisorId) {
			this.centroEmisorId = centroEmisorId;
		}

		public Map<Long, String> getNegocios() {
			return negocios;
		}


		public void setNegocios(Map<Long, String> negocios) {
			this.negocios = negocios;
		}


		public Long getNegocioId() {
			return negocioId;
		}


		public void setNegocioId(Long negocioId) {
			this.negocioId = negocioId;
		}

		public Short getTipoAsegurado() {
			return tipoAsegurado;
		}
		

		public void setTipoAsegurado(Short tipoAsegurado) {
			this.tipoAsegurado = tipoAsegurado;
		}


		public String getDatosAsegurado() {
			return datosAsegurado;
		}


		public Double getDescuentoFin() {
			return descuentoFin;
		}

		public void setDescuentoFin(Double descuentoFin) {
			this.descuentoFin = descuentoFin;
		}

		public void setDatosAsegurado(String datosAsegurado) {
			this.datosAsegurado = datosAsegurado;
		}


		public String getNombreAsegurado() {
			return nombreAsegurado;
		}


		public void setNombreAsegurado(String nombreAsegurado) {
			this.nombreAsegurado = nombreAsegurado;
		}


		public Short getTipodePersona() {
			return TipodePersona;
		}


		public void setTipodePersona(Short tipodePersona) {
			TipodePersona = tipodePersona;
		}
		
		public CotizacionExpress getCotizacionExpress() {
			return cotizacionExpress;
		}

		public void setCotizacionExpress(CotizacionExpress cotizacionExpress) {
			this.cotizacionExpress = cotizacionExpress;
		}

		public BigDecimal getIdToCotizacion() {
			return idToCotizacion;
		}

		public void setIdToCotizacion(BigDecimal idToCotizacion) {
			this.idToCotizacion = idToCotizacion;
		}

		public ResumenCostosDTO getResumenCostosDTO() {
			return resumenCostosDTO;
		}

		public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
			this.resumenCostosDTO = resumenCostosDTO;
		}

		public BigDecimal getTotalPrima() {
			return totalPrima;
		}

		public void setTotalPrima(BigDecimal totalPrima) {
			this.totalPrima = totalPrima;
		}

		public BigDecimal getDerechos() {
			return derechos;
		}

		public void setDerechos(BigDecimal derechos) {
			this.derechos = derechos;
		}

		public BigDecimal getIva() {
			return iva;
		}

		public void setIva(BigDecimal iva) {
			this.iva = iva;
		}

		public BigDecimal getPrimaTotal() {
			return primaTotal;
		}

		public void setPrimaTotal(BigDecimal primaTotal) {
			this.primaTotal = primaTotal;
		}

		public Boolean getComplementarCreacion() {
			return complementarCreacion;
		}

		public void setComplementarCreacion(Boolean complementarCreacion) {
			this.complementarCreacion = complementarCreacion;
		}

		public Map<Integer, String> getListaEstatus() {
			return listaEstatus;
		}

		public void setListaEstatus(Map<Integer, String> listaEstatus) {
			this.listaEstatus = listaEstatus;
		}

		public void setValorPrimaTotalFin(BigDecimal valorPrimaTotalFin) {
			this.valorPrimaTotalFin = valorPrimaTotalFin;
		}

		public BigDecimal getValorPrimaTotalFin() {
			return valorPrimaTotalFin;
		}

		public Promotoria getPromotoria() {
			return promotoria;
		}

		public void setPromotoria(Promotoria promotoria) {
			this.promotoria = promotoria;
		}

		public List<PromotoriaView> getPromotoriasDTOs() {
			return promotoriasDTOs;
		}

		public void setPromotoriasDTOs(List<PromotoriaView> promotoriasDTOs) {
			this.promotoriasDTOs = promotoriasDTOs;
		}

		public String getDescripcionBusquedaPromotoria() {
			return descripcionBusquedaPromotoria;
		}

		public void setDescripcionBusquedaPromotoria(
				String descripcionBusquedaPromotoria) {
			this.descripcionBusquedaPromotoria = descripcionBusquedaPromotoria;
		}
		
		public int getFechaOperacionCompareTo() {
			return fechaOperacionCompareTo;
		}

		public void setFechaOperacionCompareTo(int fechaOperacionCompareTo) {
			this.fechaOperacionCompareTo = fechaOperacionCompareTo;
		}

		@Autowired
		@Qualifier("promotoriaJPAServiceEJB")
		public void setPromotoriaService(PromotoriaJPAService promotoriaService) {
			this.promotoriaService = promotoriaService;
		}

		public Usuario getUsuario() {
			return usuario;
		}

		public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
		}

		public String getTabActiva() {
			return tabActiva;
		}

		public void setTabActiva(String tabActiva) {
			this.tabActiva = tabActiva;
		}
		
		public boolean isCalcularPostGuardado() {
			return calcularPostGuardado;
		}

		public void setCalcularPostGuardado(boolean calcularPostGuardado) {
			this.calcularPostGuardado = calcularPostGuardado;
		}

		@Autowired
		@Qualifier("centroEmisorServiceEJB")
		public void setCentroEmisorService(CentroEmisorService centroEmisorService) {
			this.centroEmisorService = centroEmisorService;
		}
		
		@Autowired
		@Qualifier("usuarioServiceEJB")
		public void setUsuarioService(UsuarioService usuarioService) {
			this.usuarioService = usuarioService;
		}

		public BigDecimal getPrimaAIgualar() {
			return primaAIgualar;
		}

		public void setPrimaAIgualar(BigDecimal primaAIgualar) {
			this.primaAIgualar = primaAIgualar;
		}

		public void setSoloConsulta(Short soloConsulta) {
			this.soloConsulta = soloConsulta;
		}

		public Short getSoloConsulta() {
			return soloConsulta;
		}

		/**
		 * @return the agenteMidasService
		 */
		public AgenteMidasService getAgenteMidasService() {
			return agenteMidasService;
		}

		/**
		 * @param agenteMidasService the agenteMidasService to set
		 */
		@Autowired
		@Qualifier("agenteMidasEJB")
		public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
			this.agenteMidasService = agenteMidasService;
		}

		/**
		 * @return the catalogoService
		 */
		public ValorCatalogoAgentesService getCatalogoService() {
			return catalogoService;
		}

		/**
		 * @param catalogoService the catalogoService to set
		 */
		@Autowired
		@Qualifier("valorCatalogoAgentesServiceEJB")
		public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
			this.catalogoService = catalogoService;
		}

		public void setCotizacionId(BigDecimal cotizacionId) {
			this.cotizacionId = cotizacionId;
		}

		public BigDecimal getCotizacionId() {
			return cotizacionId;
		}
		
		
		@Autowired
		@Qualifier("negocioDerechosServiceEJB")
		public void setNegocioDerechosService(NegocioDerechosService negocioDerechosService) {
			this.negocioDerechosService = negocioDerechosService;
		}	
		public Boolean getEsComplementar() {
			return esComplementar;
		}

		public void setEsComplementar(Boolean esComplementar) {
			this.esComplementar = esComplementar;
		}

		public String getCorreo() {
			return correo;
		}

		public void setCorreo(String correo) {
			this.correo = correo;
		}

		public boolean isCorreoObligatorio() {
			return correoObligatorio;
		}

		public void setCorreoObligatorio(boolean correoObligatorio) {
			this.correoObligatorio = correoObligatorio;
		}

		public String getNombreContratante() {
			return nombreContratante;
		}

		public void setNombreContratante(String nombreContratante) {
			this.nombreContratante = nombreContratante;
		}

		public String getIsAgente() {
			return isAgente;
		}

		public void setIsAgente(String isAgente) {
			this.isAgente = isAgente;
		}
		/**
		 * JFGG
		 * @return List<ExcepcionSuscripcionReporteDTO>
		 */
		public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
			return excepcionesList;
		}
		/**
		 * JFGG
		 * @param excepcionesList
		 */
		public void setExcepcionesList(
				List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
			this.excepcionesList = excepcionesList;
		}

		/**
		 * @return the tiposVigenciaList
		 */
		public Map<Integer, String> getTiposVigenciaList() {
			return tiposVigenciaList;
		}

		/**
		 * @param tiposVigenciaList the tiposVigenciaList to set
		 */
		public void setTiposVigenciaList(Map<Integer, String> tiposVigenciaList) {
			this.tiposVigenciaList = tiposVigenciaList;
		}

		/**
		 * @return the vigenciaDefault
		 */
		public Integer getVigenciaDefault() {
			return vigenciaDefault;
		}

		/**
		 * @param vigenciaDefault the vigenciaDefault to set
		 */
		public void setVigenciaDefault(Integer vigenciaDefault) {
			this.vigenciaDefault = vigenciaDefault;
		}
		
}
