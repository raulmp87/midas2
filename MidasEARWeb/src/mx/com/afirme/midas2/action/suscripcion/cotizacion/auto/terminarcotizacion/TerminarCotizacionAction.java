package mx.com.afirme.midas2.action.suscripcion.cotizacion.auto.terminarcotizacion;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.poliza.inciso.GenerarIncisoEmision;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.DocumentationException;
import mx.com.afirme.midas2.clientesapi.dto.DocumentoDTO;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.clientesapi.dto.InterviewException;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.canalventa.CanalVentaNegocioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class TerminarCotizacionAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger
			.getLogger(TerminarCotizacionAction.class);

	private Long id;
	private String tipoAccion;
	private String claveNegocio;
	private CotizacionDTO cotizacion;
	private List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private Long solicitudId;
	private Boolean aplicaEndoso = false;
	private EntidadService entidadService;
	private CotizacionService cotizacionService;
	private SolicitudAutorizacionService autorizacionService;
	private Long idToNegocio;
	private String idDiv;

	private IncisoService incisoService;
	private String isAgente;
	private String pdfDownload;
	private Boolean newCotizadorAgente = false;
	private String forma;
	private BigDecimal numeroInciso;
	private boolean compatilbeExplorador; 
	private BigDecimal idToCotizacion;
	private String nameUser;

	private boolean correoObligatorio;
	private String correo;
	private String nombreContratante;
	private ClientesApiService clienteRest;
	private EntrevistaDTO entrevista;
	List<DocumentoDTO> documentacion;
	private String linkFortimax;
	private InputStream fileInputStream;
	private IncisoCotizacionDTO incisoCotizacion;
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public TerminarCotizacionAction() {
	}

	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}
	
	@Autowired
	@Qualifier("canalVentaNegocioServiceEJB")
	private CanalVentaNegocioService canalVentaNegocioService;

	@Override
	public void prepare() throws Exception {
		if (claveNegocio == null) {
			claveNegocio = Negocio.CLAVE_NEGOCIO_AUTOS;
		}
	}

	public void prepareTerminar() {
		cotizacion = entidadService.findById(CotizacionDTO.class,
				new BigDecimal(id));
		if (newCotizadorAgente) {
			idToCotizacion = BigDecimal.valueOf(id);
		}
	}
	/**
	 * Invoca al cliente REST para generar el pdf referente a la entrevista capturada
	 * flujo de la emisión
	 * 
	 * @return regresa el redirect hacia el action de descarga del archivo pdf de la entrevista
	 * 
	 * 
	 * */
	public String getFormatoEntrevista() {
	
		cotizacion =entidadService.findById(CotizacionDTO.class,idToCotizacion);			
	ResponseEntity<Long> resp = clienteRest.clienteUnificado(null, Long
				.valueOf(cotizacion.getIdToPersonaContratante().longValue()));

	byte[] temp =null;
	try{
	 temp = clienteRest.createPDFInterviewApp(resp.getBody(), "A",
				"484",String.valueOf(cotizacion.getValorTotalPrimas().setScale(2, BigDecimal.ROUND_HALF_UP)), null);
	}catch (Exception e){}	
		if(temp!=null){
			fileInputStream = new ByteArrayInputStream(temp);
				
		}else {fileInputStream=new ByteArrayInputStream(new byte[1] );}
		return "pdfEntrevista";

	}

	public String terminar() {
		// Valida las fechas de vigencia
		long start = System.currentTimeMillis();
		LOG.info("Entrando a terminarCotizacion-->" + id);
		// ID DE CLIENTE UNICO PARA TODAS LAS LLAMADAS A LOS SERVICIOS DE
		// VALIDACIONES
		
		if (isAgente!=null &&isAgente.compareTo("S")==0 &&cotizacion != null
				&& cotizacion.getIdToPersonaContratante() != null) {
			
			
			String validarPrima = "";
			ClienteUnicoDTO clienteUnico = null;
			ResponseEntity<Long> resp = null;
			//Validaciones Prima Mayor
			LOG.info("-- Validar pais y ocupación de cliente " + cotizacion.getIdToPersonaContratante().longValue());
			try {
				resp = clienteRest.clienteUnificado(null, cotizacion
						.getIdToPersonaContratante().longValue());
				
				
				clienteUnico = clienteRest.findClienteUnicoById(resp.getBody());
				
				ResponseEntity<String> entityPrima = clienteRest.validateClientePrima(Long.valueOf(clienteUnico.getIdClienteUnico()));
				validarPrima = entityPrima.getBody();
				
				LOG.info("-- Tipo de cliente " + clienteUnico.getClaveTipoPersonaString());
				
				if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("1")){
				
					if(clienteUnico.getOcupacionCNSF() == null || clienteUnico.getOcupacionCNSF().equals("")
							|| clienteUnico.getIdPaisNacimiento() == null || clienteUnico.getIdPaisNacimiento().equals("")){
						
						
						
						if(validarPrima.contains("true")){
						
							super.setMensajeError("El cliente tiene una prima acumulada mayor a 2,500 USD. Favor de complementar el Pais de Nacimiento y la ocupaci\u00f3n.");				
							return getPathError();	
						}
					}
				}else if(clienteUnico.getClaveTipoPersonaString() != null
						&& clienteUnico.getClaveTipoPersonaString().equals("2")){
					
					if(clienteUnico.getCveGiroCNSF() == null || clienteUnico.getCveGiroCNSF().equals("")
							|| clienteUnico.getIdPaisConstitucion() == null || clienteUnico.getIdPaisConstitucion().equals("")){
												
						if(validarPrima.contains("true")){

							super.setMensajeError("El cliente tiene una prima acumulada mayor a 2,500 USD. Favor de complementar el Pais de Constituci\u00f3n y la ocupaci\u00f3n.");				
							return getPathError();
						}
					}
				}
				
				
			} catch (HttpStatusCodeException e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- terminar():Validaciones Prima Mayor ", e);
				return getPathError();
			} catch (Exception e) {

				super.setMensajeError("No se encontro el usuario asociado a la poliza");
				LOG.error("---- terminar():Validaciones Prima Mayor ", e);
				return getPathError();
			}	
			
			
			try {
				clienteRest.findInterViews(cotizacion
						.getIdToPersonaContratante().longValue());
			} catch (InterviewException e) {

				if(validarPrima.contains("true")){
					entrevista = new EntrevistaDTO();
					entrevista.setMoneda(cotizacion.getIdMoneda().intValue());
					entrevista.setPep(true);
					entrevista.setCuentaPropia(false);
					entrevista.setIdToCotizacion(cotizacion.getIdToCotizacion());
					if (clienteUnico.getIdToPersonaString().compareTo("1") == 0) {
						return "entrevistasPF";
					} else {

						return "entrevistasPM";
					}
				}
			}
			try {

				clienteRest.getDocumentacionFaltante(resp.getBody());

			}

catch (DocumentationException e) {
				
				//Se prepara la pantalla de captura de documentos
				
				 List<DocumentoDTO> documentacionTemp = clienteRest.getDocumentos(e.getMessage());
				 documentacion= new ArrayList<DocumentoDTO>();
				 linkFortimax=clienteRest.getLinkFortmax(resp.getBody());
				 List<DocumentoDTO> expediente=clienteRest.getDocumentacion(resp.getBody());
				 for(DocumentoDTO documentoTemp:documentacionTemp){
					 DocumentoDTO  documento = new DocumentoDTO();
					 for(DocumentoDTO docuExp :expediente){
						 if(documentoTemp.getId().toString().compareTo(docuExp.getDocumentoId())==0 ){
							 if(docuExp.getFechaVigencia()!=null){
								 String []temp =docuExp.getFechaVigencia().substring(0,10).split("-"); 
								 if(temp!=null&&temp.length==3){
									 documento.setFechaVigencia(temp[2]+"/"+temp[1]+"/"+temp[0]);
									 
								 }

							 }
							 
							 documento.setDescripcion(docuExp.getDescripcion());
							 documento.setDocumentoId(docuExp.getDocumentoId());
							 documento.setSeleccionado(true);
								
							 break;
						 }
						 
					 }
					documento.setNombre(documentoTemp.getNombre());
					documento.setRequerido(documentoTemp.getRequerido());
					documento.setDocumentoId(String.valueOf(documentoTemp.getId()));
					documentacion.add(documento);
					
				 }
				 
					 return "documentacion";
				
			} 
		}
		if (cotizacion.getFechaInicioVigencia() == null
				|| cotizacion.getFechaFinVigencia() == null) {
			setMensajeError("confirm:message:Es necesario guardar la cotizaci\u00f3n antes de terminarla, \u00BFDeseas hacerlo ahora\u003F");
			setNextFunction("guardarCotizacion()");
			return getPathInput();
		}

		// Ajusta numero de Inciso
		try {
			GenerarIncisoEmision incisoEmision = new GenerarIncisoEmision();
			incisoEmision.setIncisoService(incisoService);
			incisoEmision.regeneraIncisos(cotizacion.getIdToCotizacion());
		} catch (Exception e) {
			super.setMensajeError("Error al ajustar Incisos");
			return getPathError();
		}

		TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService
				.terminarCotizacion(new BigDecimal(id), false);
		if (terminarCotizacionDTO != null) {
			String path = "";
			if (terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE
					.getEstatus().shortValue()) {
				super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
				path = getPathInput();
			} else if (terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES
					.getEstatus().shortValue()) {
				excepcionesList = terminarCotizacionDTO.getExcepcionesList();
				super.setMensajeError(terminarCotizacionDTO.getMensajeError());
				path = getPathError();
			} else {
				super.setMensajeError(terminarCotizacionDTO.getMensajeError());
				path = getPathInput();
			}
			return path;
		}
		super.setMensajeExito();
		LOG.info("Saliendo de terminarCotizacion, idCotizacion: " + id + " : "
				+ ((System.currentTimeMillis() - start) / 1000.0));
		return getPathSuccess();
	}

	private String getPathSuccess() {
		
		if(newCotizadorAgente){
			
			return "successAgente";
		}
		else if (isAgente != null && "S".equals(isAgente)) {
			return "successAgent";
		} else {
			return  SUCCESS;
		}
	}

	private String getPathError() {
		return newCotizadorAgente ? "inputAgente" : ERROR;
	}

	private String getPathInput() {
		return newCotizadorAgente ? "inputAgente" : INPUT;
	}

	public String enviarSolicitudes() {
		if (excepcionesList != null) {
			solicitudId = autorizacionService.guardarSolicitudDeAutorizacion(
					new BigDecimal(id), "", new Long(this.getUsuarioActual()
							.getId()),
					new Long(this.getUsuarioActual().getId()), claveNegocio,
					excepcionesList);
			setMensajeExitoPersonalizado(getText(
					"midas.suscripcion.solicitud.autorizacion.creacionSolicitud",
					new String[] { solicitudId.toString() }));
		}
		if (isAgente != null && "S".equals(isAgente)) {
			return "agentesuccess";
		} else {
			return SUCCESS;
		}
	}

	public String redireccionarCotizacion() {
		String path = null;
		Short estatus = null;
		if (id != null) {
			cotizacion = entidadService.findById(CotizacionDTO.class,
					BigDecimal.valueOf(id));
			if (cotizacion != null) {
				estatus = cotizacion.getClaveEstatus();
			}
		}
		if (estatus != null
				&& estatus.shortValue() == CotizacionDTO.ESTATUS_COT_TERMINADA) {
			path = "terminada";
		} else {
			path = "proceso";
		}
		return path;
	}

	@Autowired
	@Qualifier("incisoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}

	@Autowired
	@Qualifier("cotizacionServiceEJB")
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@Autowired
	@Qualifier("solicitudAutorizacionService")
	public void setAutorizacionService(
			SolicitudAutorizacionService autorizacionService) {
		this.autorizacionService = autorizacionService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio.trim();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdToNegocio() {
		return idToNegocio;
	}
	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}
	
	public void setIdDiv(String idDiv) {
		this.idDiv = idDiv;
	}
	public String getIdDiv() {
		return idDiv;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public void setExcepcionesList(
			List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public Long getSolicitudId() {
		return solicitudId;
	}

	public void setSolicitudId(Long solicitudId) {
		this.solicitudId = solicitudId;
	}

	public Boolean getAplicaEndoso() {
		return aplicaEndoso;
	}

	public void setAplicaEndoso(Boolean aplicaEndoso) {
		this.aplicaEndoso = aplicaEndoso;
	}

	public String getIsAgente() {
		return isAgente;
	}

	public void setIsAgente(String isAgente) {
		this.isAgente = isAgente;
	}

	public boolean isCompatilbeExplorador() {
		return compatilbeExplorador;
	}

	public void setCompatilbeExplorador(boolean compatilbeExplorador) {
		this.compatilbeExplorador = compatilbeExplorador;
	}

	public Boolean getNewCotizadorAgente() {
		return newCotizadorAgente;
	}

	public void setNewCotizadorAgente(Boolean newCotizadorAgente) {
		this.newCotizadorAgente = newCotizadorAgente;
	}

	public String getForma() {
		return forma;
	}

	public void setForma(String forma) {
		this.forma = forma;
	}

	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}

	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}

	public IncisoService getIncisoService() {
		return incisoService;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public boolean isCorreoObligatorio() {
		return correoObligatorio;
	}

	public void setCorreoObligatorio(boolean correoObligatorio) {
		this.correoObligatorio = correoObligatorio;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public EntrevistaDTO getEntrevista() {
		return entrevista;
	}

	public void setEntrevista(EntrevistaDTO entrevista) {
		this.entrevista = entrevista;
	}

	public List<DocumentoDTO> getDocumentacion() {
		return documentacion;
	}

	public void setDocumentacion(List<DocumentoDTO> documentacion) {
		this.documentacion = documentacion;
	}

	public String getLinkFortimax() {
		return linkFortimax;
	}

	public void setLinkFortimax(String linkFortimax) {
		this.linkFortimax = linkFortimax;
	}

	public IncisoCotizacionDTO getIncisoCotizacion() {
		return incisoCotizacion;
	}

	public void setIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		this.incisoCotizacion = incisoCotizacion;
	}

	public String getPdfDownload() {
		return pdfDownload;
	}

	public void setPdfDownload(String pdfDownload) {
		this.pdfDownload = pdfDownload;
	}
}