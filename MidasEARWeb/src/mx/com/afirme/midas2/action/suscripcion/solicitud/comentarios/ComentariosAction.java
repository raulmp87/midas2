package mx.com.afirme.midas2.action.suscripcion.solicitud.comentarios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ComentariosAction extends BaseAction  implements Preparable {

	private static final long serialVersionUID = 4024898056614770836L;

	
	private List<Comentario> comentariosList= new ArrayList<Comentario>();
	private ControlArchivoDTO controlArchivoDTO= new ControlArchivoDTO();
	private DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO; 
	private ComentariosService comentariosService;
	private Comentario comentario;
	private SolicitudDTO solicitudDTO = new SolicitudDTO();
	private EntidadService entidadService;
	private String nombreArchivo;
	private BigDecimal idToControlArchivo;	
	private String claveNegocio;
	private Short tipoComentario;
	private BigDecimal idToCotizacion;
	private CotizacionDTO cotizacion;
	private Short seleccion = (short)1;
	private static final Short TIPO_COMENTARIO_SOLICITUD = 0;
	private static final Short TIPO_COMENTARIO_LISTAR_SOLICITUD = 3;
	private static final Short TIPO_COMENTARIO_COTIZACION = 1;
	private static final Short TIPO_COMENTARIO_COTIZACION_CANCELACION = 2;
	private static final String CANCELAR_COTIZACION = "cancelar";
	
	public String getClaveNegocio() {
		return claveNegocio;
	}

	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public DocumentoDigitalSolicitudDTO getDocumentoDigitalSolicitudDTO() {
		return documentoDigitalSolicitudDTO;
	}

	public void setDocumentoDigitalSolicitudDTO(
			DocumentoDigitalSolicitudDTO documentoDigitalSolicitudDTO) {
		this.documentoDigitalSolicitudDTO = documentoDigitalSolicitudDTO;
	}

	public SolicitudDTO getSolicitudDTO() {
		return solicitudDTO;
	}

	public void setSolicitudDTO(SolicitudDTO solicitudDTO) {
		this.solicitudDTO = solicitudDTO;
	}

	@Autowired
	@Qualifier("comentarioServiceEJB")
	public void setComentariosService(ComentariosService comentariosService) {
		this.comentariosService = comentariosService;
	}
	
	public ControlArchivoDTO getControlArchivoDTO() {
		return controlArchivoDTO;
	}

	public void setControlArchivoDTO(ControlArchivoDTO controlArchivoDTO) {
		this.controlArchivoDTO = controlArchivoDTO;
	}

	public List<Comentario> getComentariosList() {
		return comentariosList;
	}

	public void setComentariosList(
			List<Comentario> comentariosList) {
		this.comentariosList = comentariosList;
	}	
	
	public Comentario getComentario() {
		return comentario;
	}

	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}

	@Override
	public void prepare() throws Exception {
		if(comentario!=null){
			if(comentario.getId()!=null) {
				comentario = entidadService.findById(Comentario.class, comentario.getId());
			}
		}		
	}
	
	public String mostrar (){
		if(tipoComentario.equals(TIPO_COMENTARIO_SOLICITUD) || tipoComentario.equals(TIPO_COMENTARIO_LISTAR_SOLICITUD)){
			if(solicitudDTO!=null){
				ServletActionContext.getContext().getSession().put("idSolicitud", solicitudDTO.getIdToSolicitud());
				System.out.println("Log Comentarios Action -- solicitudDTO.getIdToSolicitud()" + solicitudDTO.getIdToSolicitud());
				ServletActionContext.getContext().getSession().put("tipoComentario", tipoComentario);
			}			
		}else if(tipoComentario.equals(TIPO_COMENTARIO_COTIZACION) || tipoComentario.equals(TIPO_COMENTARIO_COTIZACION_CANCELACION)){
			if(idToCotizacion!=null){
				cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				ServletActionContext.getContext().getSession().put("idSolicitud", cotizacion.getSolicitudDTO().getIdToSolicitud());
				ServletActionContext.getContext().getSession().put("tipoComentario", tipoComentario);
				ServletActionContext.getContext().getSession().put("idToCotizacion", idToCotizacion);
			}
		}
		return SUCCESS;
	}
	
	public String obtenerComentarios() {
		BigDecimal solicitud = (BigDecimal) ServletActionContext.getContext()
				.getSession().get("idSolicitud");
		if (solicitud != null) {
			comentariosList = comentariosService
					.getComentarioPorSolicitud(solicitud);
			//Sort
			if(comentariosList != null && !comentariosList.isEmpty()){
				Collections.sort(comentariosList, 
						new Comparator<Comentario>() {				
							public int compare(Comentario n1, Comentario n2){
								return n1.getId().compareTo(n2.getId());
							}
						});
			}
		}
		return SUCCESS;
	}
	
	public String guardarComentarios(){
		Double solicitud=Double.valueOf(ServletActionContext.getContext().getSession().get("idSolicitud").toString()).doubleValue();		
		System.out.println("Log Comentarios Action -- solicitud " + solicitud);
		tipoComentario=Short.valueOf(ServletActionContext.getContext().getSession().get("tipoComentario").toString()).shortValue();
		solicitudDTO = entidadService.findById(SolicitudDTO.class,new BigDecimal(solicitud));
		comentario.setSolicitudDTO(solicitudDTO);
		comentario.setCodigoUsuarioCreacion("MIDAS");
		
		comentario.setValor(comentariosService.validaComentarioSolicitudCotizacion(comentario.getValor(), tipoComentario));
		
		comentariosService.guardarComentario(comentario);
		comentario.setValor("");
		if(tipoComentario.equals(TIPO_COMENTARIO_COTIZACION) || tipoComentario.equals(TIPO_COMENTARIO_COTIZACION_CANCELACION)){
			idToCotizacion=new BigDecimal(Double.valueOf(ServletActionContext.getContext().getSession().get("idToCotizacion").toString()).doubleValue());
			if(idToCotizacion!=null){
				cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			}
		}
		//Valida que existe documento en comentario
		//comentariosService.actualizaEstatusSolicitudPorComentario(BigDecimal.valueOf(solicitud));
		if(tipoComentario.equals(TIPO_COMENTARIO_COTIZACION_CANCELACION)){
			return CANCELAR_COTIZACION;
		}
		return SUCCESS;
	}
	
	public void prepareEliminarComentarios() throws Exception {
		comentario = entidadService.findById(Comentario.class, comentario.getId());		
	}
	
	public String eliminarComentarios(){
		comentariosService.eliminarComentario(comentario);
		comentario.setValor("");
		return SUCCESS;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setTipoComentario(Short tipoComentario) {
		this.tipoComentario = tipoComentario;
	}

	public Short getTipoComentario() {
		return tipoComentario;
	}

	public void setIdToCotizacion(BigDecimal idToCotizacion) {
		this.idToCotizacion = idToCotizacion;
	}

	public BigDecimal getIdToCotizacion() {
		return idToCotizacion;
	}

	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	/**
	 * @return the seleccion
	 */
	public Short getSeleccion() {
		return seleccion;
	}

	/**
	 * @param seleccion the seleccion to set
	 */
	public void setSeleccion(Short seleccion) {
		this.seleccion = seleccion;
	}
	
}
