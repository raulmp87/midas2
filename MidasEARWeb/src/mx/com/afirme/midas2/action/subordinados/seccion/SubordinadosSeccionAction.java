package mx.com.afirme.midas2.action.subordinados.seccion;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.domain.subordinados.DerechoEndosoSeccion;
import mx.com.afirme.midas2.domain.subordinados.DerechoPolizaSeccion;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.subordinados.SubordinadosSeccionService;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class SubordinadosSeccionAction extends ActionSupport implements
		Preparable {

	private static final long serialVersionUID = -954266949670629673L;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}
	
	public String mostrar() {
		return SUCCESS;
	}
	
	
	public String obtenerDerechosPoliza() {
		derechosPoliza = subordinadosSeccionService.obtenerDerechosPoliza(idSeccion);
		return SUCCESS;
	}
	
	public String obtenerDerechosEndoso() {
		derechosEndoso = subordinadosSeccionService.obtenerDerechosEndoso(idSeccion);
		return SUCCESS;
	}
	
	public String accionSobreDerechosPoliza() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = subordinadosSeccionService.relacionarDerechosPoliza(accion, idSeccion, numeroSecuencia, valor, claveDefault);
		return SUCCESS;
	}
	
	public String accionSobreDerechosEndoso() {
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		respuesta = subordinadosSeccionService.relacionarDerechosEndoso(accion, idSeccion, numeroSecuencia, valor, claveDefault);
		return SUCCESS;
	}
	
	private BigDecimal idSeccion;
	
	private RespuestaGridRelacionDTO respuesta;
	
	private SubordinadosSeccionService subordinadosSeccionService;
	
	private List<DerechoPolizaSeccion> derechosPoliza;
	
	private List<DerechoEndosoSeccion> derechosEndoso;
	
	private Long idDerecho;
	
	private Integer numeroSecuencia;
	
	private BigDecimal valor;
	
	private Short claveDefault;

	public BigDecimal getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(BigDecimal idSeccion) {
		this.idSeccion = idSeccion;
	}

	public RespuestaGridRelacionDTO getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(RespuestaGridRelacionDTO respuesta) {
		this.respuesta = respuesta;
	}

	public Integer getNumeroSecuencia() {
		return numeroSecuencia;
	}

	public void setNumeroSecuencia(Integer numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Short getClaveDefault() {
		return claveDefault;
	}

	public void setClaveDefault(Short claveDefault) {
		this.claveDefault = claveDefault;
	}
	
	public List<DerechoPolizaSeccion> getDerechosPoliza() {
		return derechosPoliza;
	}

	public List<DerechoEndosoSeccion> getDerechosEndoso() {
		return derechosEndoso;
	}

	public Long getIdDerecho() {
		return idDerecho;
	}

	public void setIdDerecho(Long idDerecho) {
		this.idDerecho = idDerecho;
	}

	@Autowired
	@Qualifier("subordinadosSeccionEJB")
	public void setSubordinadosSeccionService(
			SubordinadosSeccionService subordinadosSeccionService) {
		this.subordinadosSeccionService = subordinadosSeccionService;
	}
	
	
	

}
