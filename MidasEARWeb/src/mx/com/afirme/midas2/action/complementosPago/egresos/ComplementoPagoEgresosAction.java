package mx.com.afirme.midas2.action.complementosPago.egresos;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaPteComplMonitor;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionComplementoPagoService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/complementosPago/egresos")
public class ComplementoPagoEgresosAction extends BaseAction{
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(ComplementoPagoEgresosAction.class);

	 
	private File zipComplementosPago; 

	private Integer idProveedor;
	private String origenEnvio;
	private String UUID;
	private Long idFactura;
	private Long idLote;
	private Integer numComplementosCargados;
	private List<FacturaDTO> listaFacturas;
	private String extensionArchivo;
	private List<EnvioFactura>listaComplementosPago;
	private List<EnvioFactura>listaComplementosPagoCargados;
	private List<FacturaPteComplMonitor> litadoFacturaPteRecepcionComplementos;
	private Date fechaPagoInicio;
	private Date fechaPagoFin;
		
	private RecepcionFacturaService recepcionFacturaService;
	private EnvioFacturaService envioFacturaService;
	public RecepcionComplementoPagoService recepcionComplementoPagoService;
	
	private final String RESULTADOVALIDACION = "/jsp/siniestros/pagos/facturas/recepcion/recepcionFacturaResultadoValidacion.jsp";
	private final String GRIDCARGACOMPLEMENTOS = "/jsp/complementosPagoEgresos/cargaComplementoGrid.jsp";
	private final String GRIDFACTURAS = "/jsp/complementosPagoEgresos/facturaPendienteComplementoGrid.jsp";
	private final String MONITORCOMPLEMENTOPAGO = "/jsp/complementosPagoEgresos/monitor/contenedorMonitorComplementoPago.jsp";
	private final String GRIDFACTURASPTECOMPLEMENTOMONITOR = "/jsp/complementosPagoEgresos/monitor/facturaPteComplMonitorGrid.jsp";
	private final String GRIDFACTURASPTECOMPLEMENTOMONITORCOMP = "/jsp/complementosPagoEgresos/monitor/monitorPendientesComplementoPagoGrid.jsp";

	
	@Action(value="listarComplementosPago",results={
			@Result(name=SUCCESS,location=GRIDCARGACOMPLEMENTOS)
	})
	public String listarComplementosPago() {	
		if (UUID!=null && !UUID.trim().isEmpty()){
			//LLenar la lista de complemento de pago que estan relacionados con el UUID
			listaComplementosPago = envioFacturaService.ObtenerComplementosPagoFactura(UUID);
		}
		
		return SUCCESS;
	}	
	
	@Action(value="listarFacturas",results={
			@Result(name="SUCCESS",location=GRIDFACTURAS)
	})	
	public String listarFacturas() {
		try{
			if(idProveedor != null)
			{					
        		if(origenEnvio.equals("ZIPFACT")){
				listaFacturas= recepcionFacturaService.obtenerFacturasPendientesComplementoProveedor(idProveedor,origenEnvio);
        		}else if(origenEnvio.equals("COMP")){
            		listaFacturas= recepcionFacturaService.obtenerFacturasPendientesComplementoAgente(idProveedor);
        		}
        
			}
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		
		return "SUCCESS";	
	}
	
		
	@Action(value="cargarArchivoComplemento",results={
			@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","tipoMensaje,mensaje,numComplementosCargados,idLote"}),
			@Result(name=INPUT,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","tipoMensaje,mensaje,numComplementosCargados,idLote"})
	})
	public String cargarArchivoComplemento() {		
		try{
			numComplementosCargados = 0;
			idLote = 0L;
		if( this.recepcionFacturaService.isValidoXmlSolo() && extensionArchivo.equals("xml") ){
			super.setMensajeError(super.getText("midas.siniestros.pagos.facturas.complementosPago.solo.xml.error"));
			listaComplementosPagoCargados = new ArrayList<EnvioFactura>();
			return INPUT;
		}else{
			List<String> mensajes;
			idLote = recepcionComplementoPagoService.obtenerIdLoteCarga();
			listaComplementosPagoCargados = new ArrayList<EnvioFactura>();
			List<EnvioFactura> listaCompl = recepcionComplementoPagoService.procesarFacturasZip(idProveedor, zipComplementosPago, extensionArchivo,origenEnvio,idLote);
			for (EnvioFactura env : listaCompl) {
				if (env.isValid()){
					listaComplementosPagoCargados.add(env);
					this.setMensajeExito();			
				}
			}
			if (!listaComplementosPagoCargados.isEmpty()){
				numComplementosCargados=listaComplementosPagoCargados.size();
			}else{
			    mensajes = obtenerMensajeComplementos(this.TIPO_MENSAJE_ERROR,listaCompl);
				this.setMensajeListaPersonalizado("Error al cargar complementos de pagos",mensajes,this.TIPO_MENSAJE_ERROR);
			}
		}	
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		return SUCCESS;
	}
	
	
	
	@Action(value="listarComplementosCargados",results={
			@Result(name=SUCCESS,location=GRIDCARGACOMPLEMENTOS)
	})
	public String listarComplementosCargados() {	
		if (idLote!=null){
			//LLenar la lista de complemento de pago obteniendolos con el numero de lote de carga
			listaComplementosPago = envioFacturaService.ObtenerComplementosPagoFactura(idLote);
		}		
		return SUCCESS;
	}	
	
	private List<String> obtenerMensajeComplementos(String tipoMesaje,List<EnvioFactura> complementos){
		List<String>  listamensajes = new ArrayList<String>();
		for (EnvioFactura compl : complementos) {
			for (EnvioFacturaDet res : compl.getRespuestas()) {
				if ((res.getTipoRespuesta()==2 && tipoMesaje.equals(this.TIPO_MENSAJE_ERROR))
						||(res.getTipoRespuesta()==1 && tipoMesaje.equals(this.TIPO_MENSAJE_EXITO))){
					listamensajes.add(res.getDescripcionRespuesta());
				}				
			}
		}
		if (listamensajes.isEmpty()){
			return null;
		}
		return listamensajes;
	}
		
	
	@Action(value = "mostrarMonitorComplemento", results = {
			@Result(name = SUCCESS, location = MONITORCOMPLEMENTOPAGO)
		})
	public String mostrarMonitorComplemento(){
		return SUCCESS;
	}
	
	
	@Action(value = "listarPendientesMonitor", results = {
			@Result(name = SUCCESS, location = GRIDFACTURASPTECOMPLEMENTOMONITOR),
			@Result(name = INPUT, location = GRIDFACTURASPTECOMPLEMENTOMONITORCOMP)
	})
	public String listarPendientesMonitor(){
		try{
			litadoFacturaPteRecepcionComplementos = recepcionComplementoPagoService.obtenerFacturasPendientesComplementoMonitor(fechaPagoInicio,fechaPagoFin,origenEnvio);
		}catch(Exception ex){
			log.error(ex.getMessage(),ex);
		}
		if(origenEnvio.equals("COMP")){
			return INPUT;
		}
		return SUCCESS;
	}

	
	@Action(value="enviarNotificacionMonitor",results={
			@Result(name=SUCCESS,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","tipoMensaje,mensaje"}),
			@Result(name=INPUT,type="json",params={"contentType","text/plain","noCache","true","ignoreHierarchy","false","includeProperties","tipoMensaje,mensaje"})
	})
	public String enviarNotificacionMonitor(){
		try{
			recepcionComplementoPagoService.notificaRecepcionComplementoPago(origenEnvio);
			this.setMensaje("SE HAN ENVIADO EXITOSAMENTE LAS NOTIFICACIONES A LOS CORREOS CONFIGURADOS");
			this.setTipoMensaje("30");
		}catch(Exception ex){
			this.setMensaje("NO SE HA PODIDO ENVIAR LAS NOTIFICACIONES A CORREOS");
			this.setTipoMensaje("10");
			log.error(ex.getMessage(),ex);
			return INPUT;
		}		
		return SUCCESS;
	}

	
	/*Inicio Servicios*/
	@Autowired
	@Qualifier("recepcionFacturaServiceEJB")
	public void setRecepcionFacturaService(
			RecepcionFacturaService recepcionFacturaService) {
		this.recepcionFacturaService = recepcionFacturaService;
	}
	
	@Autowired
	@Qualifier("envioFacturaEJB")
	public void setEnvioFacturaService(EnvioFacturaService envioFacturaService) {
		this.envioFacturaService = envioFacturaService;
	}

	@Autowired
	@Qualifier("recepcionComplementoPagoServiceEJB")
	public void setRecepcionComplementoPagoService(
			RecepcionComplementoPagoService recepcionComplementoPagoService) {
		this.recepcionComplementoPagoService = recepcionComplementoPagoService;
	}
	
	public File getZipComplementosPago() {
		return zipComplementosPago;
	}

	
	public void setZipComplementosPago(File zipComplementosPago) {
		this.zipComplementosPago = zipComplementosPago;
	}	

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}
		
	public Integer getNumComplementosCargados() {
		return numComplementosCargados;
	}

	public void setNumComplementosCargados(Integer numComplementosCargados) {
		this.numComplementosCargados = numComplementosCargados;
	}

	public List<FacturaDTO> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(
			List<FacturaDTO> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public String getExtensionArchivo() {
		return extensionArchivo;
	}

	public void setExtensionArchivo(String extensionArchivo) {
		this.extensionArchivo = extensionArchivo;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public List<EnvioFactura> getListaComplementosPago() {
		return listaComplementosPago;
	}

	public void setListaComplementosPago(List<EnvioFactura> listaComplementosPago) {
		this.listaComplementosPago = listaComplementosPago;
	}

	public EnvioFacturaService getEnvioFacturaService() {
		return envioFacturaService;
	}

	public Integer getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}
	public String getOrigenEnvio() {
		return origenEnvio;
	}

	public void setOrigenEnvio(String origenEnvio) {
		this.origenEnvio = origenEnvio;
	}

	public List<EnvioFactura> getListaComplementosPagoCargados() {
		return listaComplementosPagoCargados;
	}

	public void setListaComplementosPagoCargados(
			List<EnvioFactura> listaComplementosPagoCargados) {
		this.listaComplementosPagoCargados = listaComplementosPagoCargados;
	}

	public Long getIdLote() {
		return idLote;
	}

	public void setIdLote(Long idLote) {
		this.idLote = idLote;
	}

	public List<FacturaPteComplMonitor> getLitadoFacturaPteRecepcionComplementos() {
		return litadoFacturaPteRecepcionComplementos;
	}

	public void setLitadoFacturaPteRecepcionComplementos(
			List<FacturaPteComplMonitor> litadoFacturaPteRecepcionComplementos) {
		this.litadoFacturaPteRecepcionComplementos = litadoFacturaPteRecepcionComplementos;
	}

	public Date getFechaPagoInicio() {
		return fechaPagoInicio;
	}

	public void setFechaPagoInicio(Date fechaPagoInicio) {
		this.fechaPagoInicio = fechaPagoInicio;
	}

	public Date getFechaPagoFin() {
		return fechaPagoFin;
	}

	public void setFechaPagoFin(Date fechaPagoFin) {
		this.fechaPagoFin = fechaPagoFin;
	}	
}
