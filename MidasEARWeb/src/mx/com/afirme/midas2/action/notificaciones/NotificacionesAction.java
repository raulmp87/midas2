package mx.com.afirme.midas2.action.notificaciones;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.MovimientosProcesos;
import mx.com.afirme.midas2.domain.notificaciones.ProcesosAgentes;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.notificaciones.NotificacionesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Namespace("/notificaciones")
@Component
@Scope("prototype")
public class NotificacionesAction extends CatalogoHistoricoAction implements Preparable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -141349776930327984L;

	private NotificacionesService notifService;
	private ValorCatalogoAgentesService catalogoService;
	private ConfiguracionNotificaciones configNotif;
	private EntidadService entidadService;
	private String otrosPuesto;
	private String otrosNombre;
	private String otrosCorreo;
	private List<DestinatariosNotificaciones>destinatariosList=new ArrayList<DestinatariosNotificaciones>();
	private Map<Long, String> movimientos = new LinkedHashMap<Long, String>();
	private List<ProcesosAgentes>procsAgtsList= new ArrayList<ProcesosAgentes>();
	private List<MovimientosProcesos>procsMovsList= new ArrayList<MovimientosProcesos>();
	private List<ValorCatalogoAgentes>modosEnvio= new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes>tiposDestinatario= new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes>tiposCorreoDestino= new ArrayList<ValorCatalogoAgentes>();
	private List<ConfiguracionNotificaciones>notificacionesList = new ArrayList<ConfiguracionNotificaciones>();
	
	
	@Action(value="verNotificaciones", results={
			@Result(name=SUCCESS,location="/jsp/notificaciones/configuracionNotificaciones.jsp")
	})
	public String verNotificaciones(){
		try {
			procsAgtsList=entidadService.findAll(ProcesosAgentes.class);		
			tiposDestinatario = catalogoService.obtenerElementosPorCatalogo("Tipos de Destinatario");
			tiposCorreoDestino = catalogoService.obtenerElementosPorCatalogo("Tipos Correos A Enviar");
			modosEnvio = catalogoService.obtenerElementosPorCatalogo("Modos de Envio");			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="gridNotificaciones", results={
			@Result(name=SUCCESS,location="/jsp/notificaciones/notificacionesGrid.jsp")
	})
	public String gridNotificaciones(){
		notificacionesList=notifService.findAllConfig();
		return SUCCESS;
	}

	@Action(value="cargarConfiguracion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configNotif.*,^configNotif\\.destinatariosList.*,^configNotif\\.destinatariosList\\idConfigNotificacion.*,^configNotif\\.destinatariosList\\idModoEnvio.*,^configNotif\\.destinatariosList\\idModoEnvio.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configNotif.*"})
		})
		public String cargarConfiguracion() {
			try {			
				setConfigNotif(notifService.findByIdConfig(configNotif.getId()));
				configNotif.setDestinatariosList(notifService.findDestinatariosByIdConfig(configNotif));
			} catch (Exception e) {
				e.printStackTrace();				
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="guardarConfiguracion",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configNotif.*,mensaje,tipoMensaje"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^configNotif.*"})
		})
		public String guardarConfiguracion() {
			try {			
				if(configNotif!=null && configNotif.getId()!=null){
					setMensaje("La configuracion ha sido actualizada exitosamente");
				}
				else{
					setMensaje("La configuracion ha sido creada exitosamente");
				}
				configNotif.setUsuario(getUsuarioActual().getNombreUsuario());
				notifService.saveConfigNotificaciones(configNotif);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			} catch (Exception e) {
				e.printStackTrace();				
				return INPUT;
			}
			return SUCCESS;
		}
	
	@Action(value="buscarNotificaciones", results={
			@Result(name=SUCCESS,location="/jsp/notificaciones/notificacionesGrid.jsp")
	})
	public String buscarNotificaciones(){
		notificacionesList=notifService.findByFilters(configNotif);
		return SUCCESS;
	}
	
	@Action(value="eliminarNotificacion", results={
			@Result(name=SUCCESS,location="/jsp/notificaciones/notificacionesGrid.jsp")
	})
	public String eliminarNotificacion(){
		notifService.eliminarNotificacion(configNotif);
		notificacionesList=notifService.findAllConfig();
		return SUCCESS;
	}
	
	@Autowired
	@Qualifier("notificacionesServiceEJB")
	public void setNotifService(NotificacionesService notifService) {
		this.notifService = notifService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	public List<MovimientosProcesos> getProcsMovsList() {
		return procsMovsList;
	}

	public void setProcsMovsList(List<MovimientosProcesos> procsMovsList) {
		this.procsMovsList = procsMovsList;
	}

	public ConfiguracionNotificaciones getConfigNotif() {
		return configNotif;
	}

	public void setConfigNotif(ConfiguracionNotificaciones configNotif) {
		this.configNotif = configNotif;
	}

	public List<ProcesosAgentes> getProcsAgtsList() {
		return procsAgtsList;
	}

	public void setProcsAgtsList(List<ProcesosAgentes> procsAgtsList) {
		this.procsAgtsList = procsAgtsList;
	}

	public List<ValorCatalogoAgentes> getModosEnvio() {
		return modosEnvio;
	}

	public void setModosEnvio(List<ValorCatalogoAgentes> modosEnvio) {
		this.modosEnvio = modosEnvio;
	}

	public List<ValorCatalogoAgentes> getTiposDestinatario() {
		return tiposDestinatario;
	}

	public void setTiposDestinatario(List<ValorCatalogoAgentes> tiposDestinatario) {
		this.tiposDestinatario = tiposDestinatario;
	}

	public List<ValorCatalogoAgentes> getTiposCorreoDestino() {
		return tiposCorreoDestino;
	}

	public void setTiposCorreoDestino(List<ValorCatalogoAgentes> tiposCorreoDestino) {
		this.tiposCorreoDestino = tiposCorreoDestino;
	}

	public Map<Long, String> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(Map<Long, String> movimientos) {
		this.movimientos = movimientos;
	}

	public String getOtrosPuesto() {
		return otrosPuesto;
	}

	public void setOtrosPuesto(String otrosPuesto) {
		this.otrosPuesto = otrosPuesto;
	}

	public String getOtrosNombre() {
		return otrosNombre;
	}

	public void setOtrosNombre(String otrosNombre) {
		this.otrosNombre = otrosNombre;
	}

	public String getOtrosCorreo() {
		return otrosCorreo;
	}

	public void setOtrosCorreo(String otrosCorreo) {
		this.otrosCorreo = otrosCorreo;
	}

	public List<DestinatariosNotificaciones> getDestinatariosList() {
		return destinatariosList;
	}

	public void setDestinatariosList(
			List<DestinatariosNotificaciones> destinatariosList) {
		this.destinatariosList = destinatariosList;
	}

	public List<ConfiguracionNotificaciones> getNotificacionesList() {
		return notificacionesList;
	}

	public void setNotificacionesList(
			List<ConfiguracionNotificaciones> notificacionesList) {
		this.notificacionesList = notificacionesList;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listarFiltrado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}

}
