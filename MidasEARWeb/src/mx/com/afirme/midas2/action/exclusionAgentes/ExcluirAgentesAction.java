package mx.com.afirme.midas2.action.exclusionAgentes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir;
import mx.com.afirme.midas2.service.Excluir.ExcluirService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/exclusionAgentes/agentes")
public class ExcluirAgentesAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger
			.getLogger(ExcluirAgentesAction.class);
	/**
	 * 
	 */

	/**
	 * Variables name (to be used in jsp with Languaje)
	 */
	private final String FAIL = "/jsp/excluirAgentes/agregarAgenteExcluir.jsp";
	private final String JSP_FORM = "/jsp/excluirAgentes/agregarAgenteExcluir.jsp";
	private final String JSP_FORM_LIST = "/jsp/excluirAgentes/listaAgenteExcluir.jsp";
	private final String NAMESPACE = "/exclusionAgentes/agentes";

	/**
	 * Main entity service
	 */

	private Excluir excluir;
	private List<Excluir> listExcluir = new ArrayList<Excluir>();

	private EntidadService entidadService;
	private ExcluirService excluirService;

	private Long id;
	/** Varaibles de Agregar */
	private String idAgenteAdd;
	private boolean chkComision;
	private boolean chkBono;
	/** Varaibles de Buscar */
	private String idAgenteSear;
	private boolean chkComisionSear;
	private boolean chkBonoSear;
	private boolean chkTodosSear;
	private Integer accion;

	/**
	 * EJB
	 */
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@Autowired
	@Qualifier("excluirServiceEJB")
	public void setExcluirService(ExcluirService excluirService) {
		this.excluirService = excluirService;
	}
	
	/**
	 * Default constructor
	 */
	public ExcluirAgentesAction() {
		super();
		LOG.info("ExcluirAgentesAction load.");
	}

	/**
	 * Mostrar Agentes Excluir pantall principal Action:
	 * 
	 */

	@Action(value = "mostrarExcluirAgentes", results = {
			@Result(name = SUCCESS, location = JSP_FORM),
			@Result(name = INPUT, location = FAIL) })
	public String mostrarExcluirAgentes() {
		return SUCCESS;
	}

	/**
	 * Guardar Agente Excluido
	 * 
	 * @return
	 */
	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarExcluirAgentes", "namespace",
					"/exclusionAgentes/agentes", "mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, location = FAIL) })
	public String guardarDatos() {
		try {
			LOG.info("save Registro");
			Excluir excluir = new Excluir();
			excluir.setIdAgente(idAgenteAdd);
			excluir.setBono(chkBono);
			excluir.setComision(chkComision);
			excluir.setId(id);
			excluir.setEstatus(1);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("idAgente", excluir.getIdAgente());
			Long isExist= entidadService.findByPropertiesCount(Excluir.class, params);
			LOG.info("isExist :"+isExist +"-"+accion);
			if(accion > 0){
				//si es mayor entonces actualiza 
				excluirService.saveExcluirAgente(excluir);
				setMensajeExitoPersonalizado("El agente se actualizó correctamente");
			}else{
				if(isExist > 0){
					setMensajeExitoPersonalizado("El agente ya existe");
				}else{
					excluirService.saveExcluirAgente(excluir);
					setMensajeExito();
				}
			}
			
		} catch (Exception e) {
			LOG.info("Error " + e);
			setMensajeError("Ocurrio un Error");
			return INPUT;
		}
		return SUCCESS;
	}

	/**
	 * Regrea la lista de Agentes Excluidos filtados por parametros.
	 * 
	 * @return
	 */
	@Action(value = "listarFiltrado", results = {
			@Result(name = SUCCESS, location = JSP_FORM_LIST),
			@Result(name = INPUT, location = JSP_FORM_LIST) })
	public String listarFiltrado() {
		boolean flag = false;
		try {
			Excluir excluir = new Excluir();
			excluir.setIdAgente(idAgenteSear);
			excluir.setComision(chkComisionSear);
			excluir.setBono(chkBonoSear);

			if (excluir.getIdAgente() != null
					&& excluir.getIdAgente().trim() != "") {
				flag = true;
			}
			if (chkTodosSear || chkComisionSear || chkBonoSear) {
				flag = true;
			}
			if (flag) {
				listExcluir = excluirService.getListExcluirAgentes(excluir);
			}

		} catch (Exception e) {
			LOG.info("Error " + e);
			return INPUT;
		}
		return SUCCESS;
	}

	/**
	 * Elimina el registro del agente seleccionado
	 * 
	 * @return
	 */
	@Action(value = "eliminar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "listarFiltrado", "namespace", NAMESPACE,
					"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "listarFiltrado", "namespace", NAMESPACE,
					"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String eliminar() {
		try {
			Excluir exc = new Excluir();
			exc.setId(id);
			excluirService.eliminarExcluir(exc);
			setMensajeExito();
		} catch (Exception e) {
			LOG.info("Error " + e);
			String mensaje = e.toString();
			mensaje = mensaje.substring(21);
			setMensajeError(mensaje);
			return INPUT;
		}
		return SUCCESS;
	}

	
	/**
	 * get's & Set's
	 * 
	 * @return
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdAgenteAdd() {
		return idAgenteAdd;
	}

	public void setIdAgenteAdd(String idAgenteAdd) {
		this.idAgenteAdd = idAgenteAdd;
	}

	public boolean getChkComision() {
		return chkComision;
	}

	public void setChkComision(boolean chkComision) {
		this.chkComision = chkComision;
	}

	public boolean getChkBono() {
		return chkBono;
	}

	public void setChkBono(boolean chkBono) {
		this.chkBono = chkBono;
	}

	public List<Excluir> getListExcluir() {
		return listExcluir;
	}

	public void setListExcluir(List<Excluir> listExcluir) {
		this.listExcluir = listExcluir;
	}

	public Excluir getExcluir() {
		return excluir;
	}

	public void setExcluir(Excluir excluir) {
		this.excluir = excluir;
	}

	public boolean isChkComisionSear() {
		return chkComisionSear;
	}

	public void setChkComisionSear(boolean chkComisionSear) {
		this.chkComisionSear = chkComisionSear;
	}

	public boolean isChkBonoSear() {
		return chkBonoSear;
	}

	public void setChkBonoSear(boolean chkBonoSear) {
		this.chkBonoSear = chkBonoSear;
	}

	public boolean isChkTodosSear() {
		return chkTodosSear;
	}

	public void setChkTodosSear(boolean chkTodosSear) {
		this.chkTodosSear = chkTodosSear;
	}

	public String getIdAgenteSear() {
		return idAgenteSear;
	}

	public void setIdAgenteSear(String idAgenteSear) {
		this.idAgenteSear = idAgenteSear;
	}

	public Integer getAccion() {
		return accion;
	}

	public void setAccion(Integer accion) {
		this.accion = accion;
	}
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}

}
