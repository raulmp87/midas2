package mx.com.afirme.midas2.action.movimientosmanuales;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.agtSaldoMovto.AgtSaldo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ConceptoContabilidad;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.domain.movimientosmanuales.MovimientosManuales;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.movimientosmanuales.MovimientosManualesService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/MovimientosManuales")
public class MovimientosManualesAction extends CatalogoHistoricoAction{

	private static final long serialVersionUID = 1L;
	private final String AGENTESALDOCATALOGO = "/jsp/movimientosManualesAgentes/altaMovimientoManual.jsp";
	private final String LISTAR_MOVIMIENTOS = "/jsp/movimientosManualesAgentes/aplicarMovimientosManuales.jsp";
	private final String RESULTADOS_BUSQUEDA_USUARIOS = "/jsp/suscripcion/solicitud/resultadoBusquedaUsuario.jsp";
	private final String MOVIMIENTOSMANUALESGRID="/jsp/movimientosManualesAgentes/listarMovimientosManualesGrid.jsp";
	private AgtSaldo agtSaldo= new AgtSaldo();  
	private Agente agente = new Agente();
	private AgenteMidasService agenteMidasService;
	private MovimientosManualesService movimientosManualesService;
	private EntidadService entidadService;
	private MovimientosManuales movimientoManual= new MovimientosManuales();
	private List<Usuario> listResultUsuarios;
	private String descripcionBusquedaUsuario;
	private UsuarioService usuarioService;
	List<MovimientosManuales> listaMovimientos = new ArrayList<MovimientosManuales>();
	private String idsSeleccionados;
	
	private Double cargo;
	private Double abono;
	private Double saldoFinal;
	private String fechaActual;
	private Usuario usuario;
	
	//Conceptos
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	//private ValorCatalogoAgentes conceptoMovimiento;	
	private ConceptoContabilidad conceptoMovimiento;	
	//private List<ValorCatalogoAgentes> listaConceptos = new ArrayList<ValorCatalogoAgentes>();
	private List<ConceptoContabilidad>listaConceptos = new ArrayList<ConceptoContabilidad>();
	private ValorCatalogoAgentes filtroConcepto = null;
	
	//Ramo
	private RamoFacadeRemote ramoFacadeRemote;
	private RamoDTO ramoMovimiento;	
	private List<RamoDTO> listaRamos = new ArrayList<RamoDTO>();
	private RamoDTO filtroRamo;	
		
	//Subramo
	private SubRamoFacadeRemote subRamoFacadeRemote;
	private SubRamoDTO subRamoMovimiento;	
	private List<SubRamoDTO> listaSubRamos = new ArrayList<SubRamoDTO>();
	private SubRamoDTO filtroSubRamo;
	
	
	public MovimientosManualesService getMovimientosManualesService() {
		return movimientosManualesService;
	}

	@Autowired
	@Qualifier("movimientosManualesServiceEJB")
	public void setMovimientosManualesService(
			MovimientosManualesService movimientosManualesService) {
		this.movimientosManualesService = movimientosManualesService;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public MovimientosManuales getMovimientoManual() {
		return movimientoManual;
	}

	public void setMovimientoManual(MovimientosManuales movimientoManual) {
		this.movimientoManual = movimientoManual;
	}

	public RamoDTO getRamoMovimiento() {
		return ramoMovimiento;
	}

	public void setRamoMovimiento(RamoDTO ramoMovimiento) {
		this.ramoMovimiento = ramoMovimiento;
	}

	public List<RamoDTO> getListaRamos() {
		return listaRamos;
	}

	public void setListaRamos(List<RamoDTO> listaRamos) {
		this.listaRamos = listaRamos;
	}

	public RamoDTO getFiltroRamo() {
		return filtroRamo;
	}

	public void setFiltroRamo(RamoDTO filtroRamo) {
		this.filtroRamo = filtroRamo;
	}

	public SubRamoDTO getSubRamoMovimiento() {
		return subRamoMovimiento;
	}

	public void setSubRamoMovimiento(SubRamoDTO subRamoMovimiento) {
		this.subRamoMovimiento = subRamoMovimiento;
	}

	public List<SubRamoDTO> getListaSubRamos() {
		return listaSubRamos;
	}

	public void setListaSubRamos(List<SubRamoDTO> listaSubRamos) {
		this.listaSubRamos = listaSubRamos;
	}

	public SubRamoDTO getFiltroSubRamo() {
		return filtroSubRamo;
	}

	public void setFiltroSubRamo(SubRamoDTO filtroSubRamo) {
		this.filtroSubRamo = filtroSubRamo;
	}

	public ValorCatalogoAgentes getFiltroConcepto() {
		return filtroConcepto;
	}

	public void setFiltroConcepto(ValorCatalogoAgentes filtroConcepto) {
		this.filtroConcepto = filtroConcepto;
	}

	public List<ConceptoContabilidad> getListaConceptos() {
		return listaConceptos;
	}

	public void setListaConceptos(List<ConceptoContabilidad> listaConceptos) {
		this.listaConceptos = listaConceptos;
	}	

	public ConceptoContabilidad getConceptoMovimiento() {
		return conceptoMovimiento;
	}

	public void setConceptoMovimiento(ConceptoContabilidad conceptoMovimiento) {
		this.conceptoMovimiento = conceptoMovimiento;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Double getCargo() {
		return cargo;
	}

	public void setCargo(Double cargo) {
		this.cargo = cargo;
	}

	public Double getAbono() {
		return abono;
	}

	public void setAbono(Double abono) {
		this.abono = abono;
	}

	public Double getSaldoFinal() {
		return saldoFinal;
	}

	public void setSaldoFinal(Double saldoFinal) {
		this.saldoFinal = saldoFinal;
	}	
	
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public AgtSaldo getAgtSaldo() {
		return agtSaldo;
	}

	public void setAgtSaldo(AgtSaldo agtSaldo) {
		this.agtSaldo = agtSaldo;
	}	
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
		
	}
	@Override
	public String guardar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=AGENTESALDOCATALOGO)
	})
	@Override
	public String listar() {		
		HttpServletRequest request = ServletActionContext.getRequest();		
		SistemaContext sistemaContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext()).getBean(SistemaContext.class);
		usuario = (Usuario)request.getSession().getAttribute(sistemaContext.getUsuarioAccesoMidas());
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaActual = sdf.format(Calendar.getInstance().getTime());
		
		return SUCCESS;
	}
	
	@Override
	public String verDetalle() {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.persona\\.nombreCompleto,^agente\\.tipoSituacion\\.valor,^agente\\.descripMotivoEstatusAgente,^agente\\.fechaAltaString,^agente\\.id,^agente\\.idAgente,cargo,abono,saldoFinal"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.persona\\.nombreCompleto,^agente\\.tipoSituacion\\.valor,^agente\\.descripMotivoEstatusAgente,^agente\\.fechaAltaString,^agente\\.id,^agente\\.idAgente,cargo,abono,saldoFinal"})
		})		
	public String obtenerAgente(){
		try {
			abono = 0D;
			cargo = 0D;
			saldoFinal = 0D;
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			// TODO Este ajuste es provisional debido a la urgencia del usuario, 
			// se establece como filtro la situacion Agente con valor de Autorizado
			ValorCatalogoAgentes valorCatAgentes = new ValorCatalogoAgentes(); 
			valorCatAgentes.setId(112L);
			valorCatAgentes.setIdRegistro(1302L);
			
			agente.setTipoSituacion(valorCatAgentes);
			//
			
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
			
			ValorCatalogoAgentes tipoAg = entidadService.findById(ValorCatalogoAgentes.class, agente.getIdTipoAgente());
			
			if(tipoAg!=null){				
				agente.setTipoAgente(tipoAg.getValor());
			}
			
			ValorCatalogoAgentes motivoStatus = new ValorCatalogoAgentes();
			motivoStatus.setId(agente.getIdMotivoEstatusAgente());
			motivoStatus = catalogoService.loadById(motivoStatus);
			agente.setDescripMotivoEstatusAgente(motivoStatus.getValor());
			String fechaString=(agente.getFechaAlta()!=null)?sdf.format(agente.getFechaAlta()):"";
			agente.setFechaAltaString(fechaString);
			
			//obtenemos los movimientos manuales del agente para mostrar el saldo final actual
			MovimientosManuales filtroMovMan = new MovimientosManuales();
			
			filtroMovMan.setAgente(agente);
			
			this.obtenerMovimientosManuales(filtroMovMan);			
				
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="obtenerConcepto",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^conceptoMovimiento\\.idConcepto,^conceptoMovimiento\\.descripcionConcepto,^conceptoMovimiento\\.claveCargoAbono"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^conceptoMovimiento\\.idConcepto,^conceptoMovimiento\\.descripcionConcepto,^conceptoMovimiento\\.claveCargoAbono"})
		})		
	public String obtenerConcepto(){
		try {			
			
			/*ConceptoAgentesCobranzasId conceptoAgentesCobranzasId = new ConceptoAgentesCobranzasId();
			conceptoAgentesCobranzasId.setClaveTipoConcepto("AGMAN");			
			conceptoAgentesCobranzasId.setIdConcepto(conceptoMovimiento.getId());*/
			
			Map<String,Object> parametros = new HashMap<String,Object>();
			parametros.put("idConcepto", conceptoMovimiento.getIdConcepto());
			parametros.put("claveTipoConcepto", "AGMAN");
			parametros.put("visible", Short.valueOf("1"));//solo movimientos con bandera visible activada
			
			List<ConceptoContabilidad>listaResultados = new ArrayList<ConceptoContabilidad>();
			
			listaResultados = entidadService.findByProperties(ConceptoContabilidad.class,parametros);	
			if(listaResultados.size()>0)
			{
				conceptoMovimiento = listaResultados.get(0);
			}
				
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="mostrarContenedorConcepto",results={
			@Result(name=SUCCESS,location="/jsp/movimientosManualesAgentes/concepto/conceptoCatalogo.jsp")
		})
	public String mostrarContenedorConcepto(){
		super.removeListadoEnSession();
		filtroConcepto = new ValorCatalogoAgentes();
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoConcepto",results={
			@Result(name=SUCCESS,location="/jsp/movimientosManualesAgentes/concepto/listarConceptosGrid.jsp"),		
			@Result(name=INPUT,location="/jsp/movimientosManualesAgentes/concepto/listarConceptosGrid.jsp")
	})
	public String listarFiltradoConcepto() {
		
		try {
			if(filtroConcepto!=null){					
				
				Map<String,Object> parametros = new HashMap<String,Object>();
				StringBuilder queryWhere = new StringBuilder();
				queryWhere.append(" and model.claveTipoConcepto LIKE '%AGMAN%' "); 
				parametros.put("visible", 1l);//solo movimientos con bandera visible activada				
				if(!filtroConcepto.getValor().isEmpty())
				{
					queryWhere.append(" and model.descripcionConcepto LIKE '%"+filtroConcepto.getValor()+"%' "); 
				}
				if(filtroConcepto.getIdRegistro() != null && filtroConcepto.getIdRegistro()>0 )
				{
					parametros.put("idConcepto", filtroConcepto.getIdRegistro());					
				}
				
				//listaConceptos = entidadService.findByPropertiesWithOrder(ConceptoAgentesCobranzas.class, parametros, "descripcionConcepto");
				listaConceptos =  entidadService.findByColumnsAndProperties(ConceptoContabilidad.class, "idConcepto,descripcionConcepto", parametros, new HashMap<String,Object>(), queryWhere, null);
			}
		} catch (Exception e) {		
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	
	@Action(value="obtenerRamo",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^ramoMovimiento\\.idTcRamo,^ramoMovimiento\\.codigo,^ramoMovimiento\\.descripcion,^ramoMovimiento\\.claveNegocio"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^ramoMovimiento\\.idTcRamo,^ramoMovimiento\\.codigo,^ramoMovimiento\\.descripcion,^ramoMovimiento\\.claveNegocio"})
		})		
	public String obtenerRamo(){
		try {						
			filtroRamo = new RamoDTO();
			filtroRamo.setCodigo(ramoMovimiento.getCodigo());
			filtroRamo.setIdTcRamo(ramoMovimiento.getIdTcRamo());
			ramoMovimiento = ramoFacadeRemote.listarFiltrado(filtroRamo).get(0);			
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="mostrarContenedorRamo",results={
			@Result(name=SUCCESS,location="/jsp/movimientosManualesAgentes/ramo/ramoCatalogo.jsp")
		})
	public String mostrarContenedorRamo(){		
		super.removeListadoEnSession();
		filtroRamo = new RamoDTO();
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoRamo",results={
			@Result(name=SUCCESS,location="/jsp/movimientosManualesAgentes/ramo/listarRamosGrid.jsp"),		
			@Result(name=INPUT,location="/jsp/movimientosManualesAgentes/ramo/listarRamosGrid.jsp")
	})
	public String listarFiltradoRamo() {		
		try {
			if(filtroRamo!=null){
				listaRamos = ramoFacadeRemote.listarFiltradoLike(filtroRamo);	
			}
		} catch (Exception e) {		
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action(value="obtenerSubRamo",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^subRamoMovimiento\\.idTcSubRamo,^subRamoMovimiento\\.codigoSubRamoMovsManuales,^subRamoMovimiento\\.descripcionSubRamo,^ramoMovimiento\\.codigo,"}),
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^subRamoMovimiento\\.idTcSubRamo,^subRamoMovimiento\\.codigoSubRamoMovsManuales,^subRamoMovimiento\\.descripcionSubRamo,^ramoMovimiento\\.codigo,"})
		})		
	public String obtenerSubRamo(){
		try {
			filtroSubRamo = new SubRamoDTO();
			if(subRamoMovimiento!=null){
				if(subRamoMovimiento.getCodigoSubRamo()!=null){
					String codigoSubRamo = subRamoMovimiento.getCodigoSubRamo().toString();
					if(codigoSubRamo.length()==1){
						codigoSubRamo="0"+codigoSubRamo;					
					}
					
					if(codigoSubRamo.length()==2){
						codigoSubRamo = ramoMovimiento.getCodigo().toString()+codigoSubRamo;
					}
					
					filtroSubRamo.setCodigoSubRamo( new BigDecimal(codigoSubRamo));
				}
				
				filtroSubRamo.setIdTcSubRamo(subRamoMovimiento.getIdTcSubRamo());
				filtroSubRamo.setRamoDTO(ramoMovimiento);
			}
		
			subRamoMovimiento = subRamoFacadeRemote.listarFiltradoLike(filtroSubRamo).get(0);				
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	@Action(value="mostrarContenedorSubRamo",results={
			@Result(name=SUCCESS,location="/jsp/movimientosManualesAgentes/subramo/subRamoCatalogo.jsp")
		})
	public String mostrarContenedorSubRamo(){		
		super.removeListadoEnSession();
		filtroSubRamo = new SubRamoDTO();
		return SUCCESS;
	}
	
	@Action(value="listarFiltradoSubRamo",results={
			@Result(name=SUCCESS,location="/jsp/movimientosManualesAgentes/subramo/listarSubRamosGrid.jsp"),		
			@Result(name=INPUT,location="/jsp/movimientosManualesAgentes/subramo/listarSubRamosGrid.jsp")
	})
	public String listarFiltradoSubRamo() {		
		try {
			if(filtroSubRamo!=null){
				if(filtroSubRamo.getCodigoSubRamo()!=null){
					String codigoSubRamo = filtroSubRamo.getCodigoSubRamo().toString();
					if(codigoSubRamo.length()==1){
						codigoSubRamo="0"+codigoSubRamo;					
					}
					
					codigoSubRamo = ramoMovimiento.getCodigo().toString()+codigoSubRamo;
					filtroSubRamo.setCodigoSubRamo( new BigDecimal(codigoSubRamo));
				}
				
				filtroSubRamo.setRamoDTO(ramoMovimiento);
				
				listaSubRamos = subRamoFacadeRemote.listarFiltradoLike(filtroSubRamo);	
			}
		} catch (Exception e) {		
			e.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	@Action
	(value = "guardar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarContenedor",
					"namespace","/fuerzaventa/MovimientosManuales",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarContenedor",
					"namespace","/fuerzaventa/MovimientosManuales",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String guardarMovimiento()
	{
		movimientoManual.setIdEmpresa(8L);
		movimientoManual.setCvetCptoAco("AGMAN");

		if(movimientoManual.getAgente().getIdAgente()!=null){
			agente = agenteMidasService.loadByClave(movimientoManual.getAgente());
		}
		ValorCatalogoAgentes moneda = null;
		
		try {
			moneda = valorCatalogoAgentesService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
		} catch (Exception e) {			
			e.printStackTrace();
			setMensajeError("Ha ocurrido un error al guardar el movimiento.");
			return INPUT;	
		}
		
		String codigoSubRamo = movimientoManual.getIdRamoContable().concat(movimientoManual.getIdSubramoContable());
		
		movimientoManual.setAgente(agente);
		movimientoManual.setMoneda(moneda); 
		movimientoManual.setEstatusRegistro((int) MigEndososValidosEd.CLAVE_PROCESO_NO_PROCESADO);
		movimientoManual.setSubRamoDTO(subRamoFacadeRemote.findByProperty("codigoSubRamo",new BigDecimal(codigoSubRamo)).get(0));
		movimientoManual.setIdRamoContable(String.format("%02d", Integer.parseInt(movimientoManual.getIdRamoContable())));
		movimientoManual.setIdSubramoContable(movimientoManual.getIdSubramoContable());	
		
		entidadService.save(movimientoManual);
		setMensajeExito();
				
		return SUCCESS;		
	}
	
	@Action(value="mostrarListadoMovimientosManuales",results={
			@Result(name=SUCCESS,location=LISTAR_MOVIMIENTOS)
	})	
	public String mostrarMovimientosManuales() {		
		
		if(movimientoManual.getIdUsuarioCreacion() == null || movimientoManual.getIdUsuarioCreacion().isEmpty())
		{
			usuario = this.getUsuarioSesion(); //Carga por default			
		}else
		{
			usuario = usuarioService.buscarUsuarioPorNombreUsuario(movimientoManual.getIdUsuarioCreacion());			
		}
		
		if(movimientoManual.getFechaInicioPeriodo() == null)
		{
			movimientoManual.setFechaInicioPeriodo(new Date());	//Carga por default			
		}
		
		if(movimientoManual.getFechaFinPeriodo() == null)
		{			
			movimientoManual.setFechaFinPeriodo(new Date()); //Carga por default				
		}		
		
		return SUCCESS;
	}
	
	@Action(value="buscarUsuarios",results={
			@Result(name=SUCCESS,location=RESULTADOS_BUSQUEDA_USUARIOS)
	})
	public String listarUsuarios()
	{
		if (descripcionBusquedaUsuario != null ){
			setListResultUsuarios(usuarioService.buscarUsuarioPorNombreCompleto(descripcionBusquedaUsuario));
		}else{
			listResultUsuarios = new ArrayList<Usuario>();
		}
		return SUCCESS;
	}
	
	@Action(value="buscarMovimientosManuales",results={
			@Result(name=SUCCESS,location=MOVIMIENTOSMANUALESGRID,params={"saldoFinal","${saldoFinal}"}),
			@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarListadoMovimientosManuales",
					"namespace","/fuerzaventa/MovimientosManuales",					
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String listarMovimientosManuales() 
	{		
		movimientoManual.setIdEmpresa(8L);
		
		try {
			this.obtenerMovimientosManuales(movimientoManual);
		} catch (Exception e) {
			setMensajeError("Ha ocurrido un error al obtener el listado de Movimientos.");
			e.printStackTrace();
			return INPUT;			
		}		
		
		return SUCCESS;
	}
	
	private void obtenerMovimientosManuales(MovimientosManuales filtroMovMan) throws Exception
	{
		abono = 0D;
		cargo = 0D;
		saldoFinal = 0D;
		
		listaMovimientos = movimientosManualesService.findByFilters(filtroMovMan);
		
		for(MovimientosManuales mov: listaMovimientos){
			if(mov.getNaturalezaConcepto().equals("A")){
				abono = abono + mov.getImporteMovto();
			}else{
				cargo = cargo + mov.getImporteMovto();
			}
			mov.getAgente().getPersona();
			
		}
		
		saldoFinal = abono - cargo;		
	}
	
	private Usuario getUsuarioSesion()
	{
		HttpServletRequest request = ServletActionContext.getRequest();		
		SistemaContext sistemaContext = WebApplicationContextUtils.getWebApplicationContext(request.getServletContext()).getBean(SistemaContext.class);
		return (Usuario)request.getSession().getAttribute(sistemaContext.getUsuarioAccesoMidas());
	}
	
	@Action(value="aplicarMovimientosManuales",results={
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarListadoMovimientosManuales",
					"namespace","/fuerzaventa/MovimientosManuales",
					"movimientoManual.idUsuarioCreacion","${movimientoManual.idUsuarioCreacion}",
					"movimientoManual.fechaInicioPeriodo","${movimientoManual.fechaInicioPeriodo}",
					"movimientoManual.fechaFinPeriodo","${movimientoManual.fechaFinPeriodo}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarListadoMovimientosManuales",
					"namespace","/fuerzaventa/MovimientosManuales",
					"movimientoManual.idUsuarioCreacion","${movimientoManual.idUsuarioCreacion}",
					"movimientoManual.fechaInicioPeriodo","${movimientoManual.fechaInicioPeriodo}",
					"movimientoManual.fechaFinPeriodo","${movimientoManual.fechaFinPeriodo}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String aplicarMovimientosManuales()
	{
		String[] idsMovimientosSeleccionados = null;
		usuario = this.getUsuarioSesion();
		int totalMovimientos = 0;
		int movimientosAplicados = 0;
		
		if(!idsSeleccionados.isEmpty()) {			
			idsMovimientosSeleccionados = idsSeleccionados.split(",");			
			
			totalMovimientos = idsMovimientosSeleccionados.length;
			
			movimientosAplicados = movimientosManualesService.aplicarMovimientosManuales(idsMovimientosSeleccionados, usuario.getNombreUsuario());			
			
			this.setMensajeExitoPersonalizado("Se aplicaron " + movimientosAplicados + "/" + totalMovimientos + 
					" movimientos manuales exitosamente." );
		}		
		
		return SUCCESS;
	}
	
	@Action(value="eliminarMovimientosManuales",results={
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarListadoMovimientosManuales",
					"namespace","/fuerzaventa/MovimientosManuales",
					"movimientoManual.idUsuarioCreacion","${movimientoManual.idUsuarioCreacion}",
					"movimientoManual.fechaInicioPeriodo","${movimientoManual.fechaInicioPeriodo}",
					"movimientoManual.fechaFinPeriodo","${movimientoManual.fechaFinPeriodo}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarListadoMovimientosManuales",
					"namespace","/fuerzaventa/MovimientosManuales",
					"movimientoManual.idUsuarioCreacion","${movimientoManual.idUsuarioCreacion}",
					"movimientoManual.fechaInicioPeriodo","${movimientoManual.fechaInicioPeriodo}",
					"movimientoManual.fechaFinPeriodo","${movimientoManual.fechaFinPeriodo}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String eliminarMovimientosManuales()
	{
		String[] idsMovimientosSeleccionados = null;
		int totalMovimientos = 0;
		int movimientosEliminados = 0;
		
		if(!idsSeleccionados.isEmpty()) {			
			idsMovimientosSeleccionados = idsSeleccionados.split(",");	
			
			totalMovimientos = idsMovimientosSeleccionados.length;
			
			movimientosEliminados = movimientosManualesService.eliminarMovimientosManuales(idsMovimientosSeleccionados);
			
			this.setMensajeExitoPersonalizado("Se eliminaron " + movimientosEliminados + "/" + totalMovimientos + 
			" movimientos manuales exitosamente." );
		}
		
		return SUCCESS;
	}
	
	public RamoFacadeRemote getRamoFacadeRemote() {
		return ramoFacadeRemote;
	}

	@Autowired
	@Qualifier("ramoFacadeRemoteEJB")
	public void setRamoFacadeRemote(RamoFacadeRemote ramoFacadeRemote) {
		this.ramoFacadeRemote = ramoFacadeRemote;
	}

	public SubRamoFacadeRemote getSubRamoFacadeRemote() {
		return subRamoFacadeRemote;
	}
	@Autowired
	@Qualifier("subRamoFacadeRemoteEJB")
	public void setSubRamoFacadeRemote(SubRamoFacadeRemote subRamoFacadeRemote) {
		this.subRamoFacadeRemote = subRamoFacadeRemote;
	}

	@Override
	public String listarFiltrado() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}	

	public ValorCatalogoAgentesService getValorCatalogoAgentesService() {
		return valorCatalogoAgentesService;
	}

	public List<MovimientosManuales> getListaMovimientos() {
		return listaMovimientos;
	}

	public void setListaMovimientos(List<MovimientosManuales> listaMovimientos) {
		this.listaMovimientos = listaMovimientos;
	}

	public String getDescripcionBusquedaUsuario() {
		return descripcionBusquedaUsuario;
	}

	public void setDescripcionBusquedaUsuario(String descripcionBusquedaUsuario) {
		this.descripcionBusquedaUsuario = descripcionBusquedaUsuario;
	}

	public List<Usuario> getListResultUsuarios() {
		return listResultUsuarios;
	}

	public void setListResultUsuarios(List<Usuario> listResultUsuarios) {
		this.listResultUsuarios = listResultUsuarios;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}
	
}
