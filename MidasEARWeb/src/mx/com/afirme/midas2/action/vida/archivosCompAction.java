package mx.com.afirme.midas2.action.vida;

import java.math.BigDecimal;
import java.sql.Date;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.vida.ArchivosCompService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/vida/cargaArchivos")
public class archivosCompAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 6132998255096489739L;
	public static final String exitoso = "Carga Finalizada.";
	public static final String error = "Error al guardar el archivo. Favor revisarlo!";
	
	private BigDecimal idToControlArchivo;
	
	private String resultado;
	
	//private String fechaCorte;
	
	private String tipoArchivo;
	
	private String mensaje;
	
	@SuppressWarnings("unused")
	private EntidadService entidadService;
	
	private ArchivosCompService archivosCompService;
	
	@Override
	public void prepare() {
		
	}
	
	@Action
	(value = "mostrar", results = { 
			@Result(name = SUCCESS, location = "/jsp/vida/archivoCompVida.jsp") })
	public String mostrarGeneracion() {	
		
		
		return SUCCESS;
	}
	
	@Action
	(value = "procesarinfo", results = { 
			@Result(name = SUCCESS, location = "/jsp/vida/archivoCompVida.jsp") })
	public String cargarInfo() {	
		
		//resultado = archivosCompService.procesarInfo(idToControlArchivo, tipoArchivo, fechaCorte);
		resultado = archivosCompService.procesarInfo(idToControlArchivo, tipoArchivo, "01/01/2000");
		
		if(resultado.equalsIgnoreCase("EXITOSO")){
			setMensaje(exitoso);
		}else if(resultado.equalsIgnoreCase("ERROR")){
			setMensaje(error);
		}else{
			setMensaje(resultado);
		}
			
		
		return SUCCESS;
	}	
	
	
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	
	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
/*
	public String getFechaCorte() {
		return fechaCorte;
	}
	
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	*/
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("archivosCompServiceEJB")
	public void setImporteVidaService(
			ArchivosCompService importeVidaService) {
		this.archivosCompService = importeVidaService;
	}
}
