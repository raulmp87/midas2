package mx.com.afirme.midas2.action.condicionesGenerales;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgenteView;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCentro;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgOrden;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgProveedor;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadHistoricoService;
import mx.com.afirme.midas2.service.condicionesGenerales.CondicionesGeneralesService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class CondicionesGeneralesAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;
	private static final Integer idRolAgente = 1;
	private static final Integer idRolAgenteM2 = 102006;
	private static final String TIPO_PRESTADOR_STR = "OTROS";

	private static final Logger LOG = Logger.getLogger(CondicionesGeneralesAction.class);
	private CondicionesGeneralesService condicionesGeneralesService;
	private PrestadorDeServicioService prestadorDeServicioService;
	private EntidadHistoricoService  entidadHistoricoService;
	private ListadoService listadoService;
	private List<CgProveedor> cgProveedorList = new ArrayList<CgProveedor>();
	private List<CgAgente> cgAgenteList = new ArrayList<CgAgente>();
	private List<CgAgenteView> cgAgenteViewList = new ArrayList<CgAgenteView>();
	private List<CgCentro> cgCentroList = new ArrayList<CgCentro>();
	private List<CgOrden> cgOrdenList = new ArrayList<CgOrden>();
	private List<PrestadorDeServicioService.PrestadorServicioRegistro> prestadorServicioList = new ArrayList<PrestadorDeServicioService.PrestadorServicioRegistro>();
	private List<CgCentro> cgCentrosProveedor = new ArrayList<CgCentro>();
	private List<CgCentro> centrosProveedorList = new ArrayList<CgCentro>();
	private Map<Long, String> gerenciaList = new LinkedHashMap<Long, String>();
	private Map<Long, String> ejecutivoList = new LinkedHashMap<Long, String>();
	private Map<Long, String> promotoriaList = new LinkedHashMap<Long, String>();
	private Map<String, String> paises = new LinkedHashMap<String, String>();
	private Map<String, String> estados = new LinkedHashMap<String, String>();
	private Map<String, String> ciudades = new LinkedHashMap<String, String>();
	private Map<String, String> colonias = new LinkedHashMap<String, String>();
	private Map<Long, String> cgCentrosList = new LinkedHashMap<Long, String>();
	private static List<ValorCatalogoAgentes> catalogoTipoSituacion=new ArrayList<ValorCatalogoAgentes>();
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;
	private CgProveedor cgProveedor;
	private String idCgProveedor;
	private CgAgente cgAgente;
	private String idCgAgente;
	private String idToAgente;
	private CgCentro cgCentro;
	private String idToCentro;
	private CgOrden cgOrden;
	private String idToOrden;
	private String idToControlArchivo;
	private String isAgente;	
	
	
	@Autowired
	@Qualifier("prestadorDeServicioEJB")
	public void setPrestadorDeServicioService(PrestadorDeServicioService prestadorDeServicioService) {
		this.prestadorDeServicioService = prestadorDeServicioService;
	}
	
	@Autowired
	@Qualifier("condicionesGeneralesServiceEJB")
	public void setCondicionesGeneralesService(CondicionesGeneralesService condicionesGeneralesService) {
		this.condicionesGeneralesService = condicionesGeneralesService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("entidadHistoricoEJB")
	public void setEntidadHistoricoService(EntidadHistoricoService entidadHistoricoService) {
		this.entidadHistoricoService = entidadHistoricoService;
	}

	@Override
	public void prepare() throws Exception {
		
	}
	
	
	
	
	/**-----------------PROVEEDOR-----------------------**/
	public String listarProveedor(){
		
		return SUCCESS;
	}
	
	public String traerListaProveedor(){
		
		try {
			
			cgProveedorList = condicionesGeneralesService.traerListaProveedor( cgProveedor );
			
		} catch (Exception e) {
			
			LOG.error( "CG traerListaProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String crearProveedor(){	
		
		try {
			this.llenarComboProveedor();
			
		} catch (Exception e) {
			
			LOG.error( "CG crearProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}

	private void llenarComboProveedor() throws Exception{
		
		PrestadorServicioFiltro filtro = new PrestadorServicioFiltro();
		filtro.setTipoPrestadorStr( TIPO_PRESTADOR_STR );
			prestadorServicioList = prestadorDeServicioService.buscar(filtro);
	}
	
	public String guardarProveedor(){
		
		int tipoAccion = 0;
		
		try{
			tipoAccion = this.traeTipoAccion();
			condicionesGeneralesService.guardarActualizarProveedor( cgProveedor, this.obtenerUsuario() );			
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG guardarProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			this.inicializaCrear( tipoAccion );
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	private void inicializaCrear( int tipoAccion ){
		
		try {
			if ( tipoAccion == 0 ){
				cgProveedor.setId( null );
			}
			else{
				this.traerCgProveedorPorId();	
			}	
			this.llenarComboProveedor();
			
		} catch (Exception e) {
			LOG.error( "CG inicializaCrear: " + e.toString() );
		}
	}
	
	private int traeTipoAccion(){
		
		if ( cgProveedor !=null && cgProveedor.getId() != null ){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	public String editarProveedor(){	
		
		try {
			this.traerCgProveedorPorId();
			this.llenarComboProveedor();
			this.traerCgCentros();
			
		} catch (Exception e) {
			
			LOG.error( "CG editarProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String eliminarProveedor(){
		
		try {
			this.traerCgProveedorPorId();
			condicionesGeneralesService.eliminarProveedor( cgProveedor );
			super.setMensajeExito();
			
		} catch (Exception e) {
			
			LOG.error( "CG eliminarProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	private void traerCgProveedorPorId() throws Exception{
		
		cgProveedor = condicionesGeneralesService.traerProveedor(Long.parseLong(idCgProveedor));
	}
	
	private String obtenerUsuario() throws Exception{
		
		return usuarioService.getUsuarioActual().getNombreUsuario();
		
	}
	
	private boolean isAgente() throws Exception{
		
		List<Rol> roles = usuarioService.getUsuarioActual().getRoles();
		
		for ( Rol item : roles ){			
			if ( item.getId() == idRolAgente || item.getId() == idRolAgenteM2) {				
				return true;				
			}
		}
		return false;
		
	}
	
	public String traerListaCProveedor(){
		
		try {
			
			this.traerCgProveedorPorId();
			centrosProveedorList = condicionesGeneralesService.traeCentrosProveedor( cgProveedor.getCentros() );
			
		} catch (Exception e) {
			
			LOG.error( "CG traerListaCProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String eliminarCentroProveedor(){
		
		try {
			
			this.traerCgProveedorPorId();
			condicionesGeneralesService.eliminarCentroProveedor( cgProveedor, idToCentro );
			super.setMensajeExito();
			
			
		} catch (Exception e) {
			
			LOG.error( "CG eliminarCentroProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
		
	}

	public String guardarCentroProveedor(){
		
		try {
			
			this.traerCgProveedorPorId();
			condicionesGeneralesService.guardarCentroProveedor( cgProveedor, idToCentro );
			super.setMensajeExito();
			
		} catch (Exception e) {
			
			LOG.error( "CG guardarCentroProveedor: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
		
	}
	
	
	/**-----------------AGENTE-----------------------**/
	public String listarAgente(){	
		this.inicializarAgente();
		return SUCCESS;
	}
	
	public String traerListaAgente(){
		
		try {
			cgAgenteViewList = condicionesGeneralesService.traerListaAgente( cgAgente );
			cgAgente = null;
			
		} catch (Exception e) {
			
			LOG.error( "CG traerListaAgente: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	private void inicializarAgente(){
		
		gerenciaList = listadoService.getMapGerencias();
		catalogoTipoSituacion=cargarCatalogo("Estatus de Agente (Situacion)");
		cgAgente = null;
		
	}
	
	protected List<ValorCatalogoAgentes> cargarCatalogo(String catalogo){
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("grupoCatalogoAgente.descripcion",catalogo);
		return entidadHistoricoService.findByProperties(ValorCatalogoAgentes.class, params);
	}
	
	public String editarAgente(){	

		try {
			this.traerCgAgentePorId();
			this.cargarCombosLugaresAgente();
			
		} catch (Exception e) {
			
			LOG.error( "CG editarAgente: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String actualizarAgente(){
		
		try{
			condicionesGeneralesService.actualizarAgente( cgAgente, this.obtenerUsuario() );
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG actualizarAgente: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	private void traerCgAgentePorId() throws Exception{
		
		cgAgente = condicionesGeneralesService.traerAgente( Long.parseLong( idToAgente ) );
		CgAgenteView cgAgenteView = new CgAgenteView();
		
		if ( cgAgente != null ){
     		cgAgenteView = condicionesGeneralesService.traerCgAgenteView( Long.parseLong( idToAgente ) ); 
			cgAgente.setCgAgenteView( cgAgenteView );
		}
	}
	
	public String actualizarDatos(){		

		try {			
			
			List<String> mensajes = condicionesGeneralesService.cargarInformacion( new BigDecimal( idToControlArchivo), this.obtenerUsuario() );

			super.setMensajeListaPersonalizado("Resultado de la carga", mensajes, TIPO_MENSAJE_INFORMACION);
			
			this.inicializarAgente();
			
		} catch (Exception e) {
			
			LOG.error( "CG actualizarDatos: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String descargarLayout(){
		
		try{
			
			InputStream plantillaBytes = condicionesGeneralesService.obtenerLayout();
			if(plantillaBytes == null){
				return ERROR;
			}
			plantillaInputStream = plantillaBytes;
			contentType = "application/vnd.ms-excel";
			setFileName("Layout Agentes Condiciones Generales.xls");	
			return SUCCESS;
			
		}
		catch( Exception e ){
			
			LOG.error( "CG descargarLayout: " + e.toString() );
			return ERROR;
		}
	}
	
	public String eliminarAgente(){
		
		try{
			condicionesGeneralesService.eliminarAgente( Long.parseLong( idToAgente ), this.obtenerUsuario() );
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG eliminarAgente: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	
	private void cargarCombosLugaresAgente() throws Exception{

		String idPais = "";
		String idEstado = "";
		String idCiudad = "";
		
		if ( cgAgente!= null ){
			
			idPais = cgAgente.getCvePais();
			idEstado = cgAgente.getCveEstado();
			idCiudad = cgAgente.getCveCiudad();			
		}
		
		paises = listadoService.getMapPaises();
		if(StringUtils.isBlank(idPais)){
			idPais="PAMEXI";
		}
		estados = listadoService.getMapEstados(idPais);
		if (StringUtils.isNotBlank(idEstado)) {
			ciudades = listadoService.getMapMunicipiosPorEstado(idEstado);						
			if (StringUtils.isNotBlank(idCiudad)) {
				colonias=listadoService.getMapColoniasSameValue(idCiudad);
			}
		}		
	}	
	
	
	
	/**--------------CENTRO EMISOR--------------------**/
	public String listarCentro(){	
		
		return SUCCESS;
	}	
	
	public String traerListaCentro(){
		
		try {

			cgCentroList = condicionesGeneralesService.traerListaCentro( cgCentro );
			
		} catch (Exception e) {
			
			LOG.error( "CG traerListaCentro: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String editarCentro(){	

		try {
			if ( idToCentro != null ){
				this.traerCgCentroPorId();
			}
			this.cargarCombosLugaresCentro();
			gerenciaList = listadoService.getMapGerencias();
			
		} catch (Exception e) {
			
			LOG.error( "CG editarCentro: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String actualizarGuardarCentro(){
		
		try{
			
			if ( idToCentro != null && !idToCentro.equals("") ){
				
				cgCentro.setId( Long.parseLong(idToCentro) );
				condicionesGeneralesService.actualizarCentro( cgCentro, this.obtenerUsuario() );
			}
			else{
				condicionesGeneralesService.guardarCentro( cgCentro, this.obtenerUsuario() );
				cgCentro = new CgCentro();
			}
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG actualizarGuardarCentro: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	public String eliminarCentro(){
		
		try{
			condicionesGeneralesService.eliminarCentro( Long.parseLong( idToCentro ), this.obtenerUsuario() );
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG eliminarCentro: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}

	private void traerCgCentroPorId() throws Exception{
		
		cgCentro = condicionesGeneralesService.traerCentro( Long.parseLong( idToCentro ) );
	}
	
	private void cargarCombosLugaresCentro() throws Exception{

		String idPais = "";
		String idEstado = "";
		String idCiudad = "";
		
		if ( cgCentro!= null ){
			
			idPais = cgCentro.getCvePais();
			idEstado = cgCentro.getCveEstado();
			idCiudad = cgCentro.getCveCiudad();			
		}
		
		paises = listadoService.getMapPaises();
		if(StringUtils.isBlank(idPais)){
			idPais="PAMEXI";
		}
		estados = listadoService.getMapEstados(idPais);
		if (StringUtils.isNotBlank(idEstado)) {
			ciudades = listadoService.getMapMunicipiosPorEstado(idEstado);						
			if (StringUtils.isNotBlank(idCiudad)) {
				colonias=listadoService.getMapColoniasSameValue(idCiudad);
			}
		}
		
	}
	
	public String activarCentro(){
		
		try{
			condicionesGeneralesService.activarCentro( Long.parseLong( idToCentro ), this.obtenerUsuario() );
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG activarCentro: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	
	
	/**-------------ORDEN DE DISTRIBUCION------------**/
	public String listarOrden(){
		return SUCCESS;
	}	

	public String traerListaOrden(){
		
		try {

			String usuario="";
			if ( this.isAgente() ){
				usuario = this.obtenerUsuario();
				
			}
			cgOrdenList = condicionesGeneralesService.traerListaOrden( cgOrden, usuario );
			
		} catch (Exception e) {
			
			LOG.error( "CG traerListaOrden: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String editarOrden(){

		try {
			
			if ( this.isAgente() ){
				
				isAgente = "true";				
			}
			else{
				isAgente = null;
				cgCentrosList = condicionesGeneralesService.getMapCentroEmisor();				
			}
			
			if ( idToOrden != null ){
				this.traerCgOrdenPorId();
			}
			
		} catch (Exception e) {
			
			LOG.error( "CG editarOrden: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	public String actualizarGuardarOrden(){
		
		try {
			if ( idToOrden != null && !idToOrden.equals("") ){
				//ACTUALIZAR
				//this.traerCgOrdenPorId();
			}
			else{
				condicionesGeneralesService.guardarOrden( cgOrden, this.obtenerUsuario(), this.isAgente() );
				cgOrden = new CgOrden();
			}
			super.setMensajeExito();
			
		} catch (Exception e) {
			
			LOG.error( "CG actualizarGuardarOrden: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
		
	}
	
	public String confirmarOrden(){
		
		try{
			condicionesGeneralesService.confirmarOrden( Long.parseLong( idToOrden ), this.obtenerUsuario() );
			super.setMensajeExito();
		}
		catch( Exception e ){
			
			LOG.error( "CG confirmarOrden: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
			return ERROR;
		}
		
		return SUCCESS;
	}
	
	private void traerCgOrdenPorId() throws Exception{
		
		cgOrden = condicionesGeneralesService.traerOrden( Long.parseLong( idToOrden ) );
	}

	
	
	/**-------------REPORTE DE INVENTARIO------------**/
	public String mostrarReporte(){

		try {
			this.traerCgCentros();
			
		} catch (Exception e) {
			LOG.error( "CG mostrarReporte: " + e.toString() );
			super.setMensajeError(MENSAJE_ERROR_GENERAL);
		}
		return SUCCESS;
	}
	
	private void traerCgCentros() throws Exception{
		
		cgCentrosList = condicionesGeneralesService.getMapCentroEmisor();
	}
	
	public String obtenerReporteInventario(){
		
		String path="";
		
		try{
			
			path = this.getContextPath();
			InputStream plantillaBytes = condicionesGeneralesService.obtenerReporteInventario( idToCentro, path );
			if(plantillaBytes == null){
				return ERROR;
			}
			plantillaInputStream = plantillaBytes;
			contentType = "application/vnd.ms-excel";
			setFileName("Reporte de Inventario por Centro Emisor.xls");	
			return SUCCESS;
			
		}
		catch( Exception e ){
			
			LOG.error( "CG obtenerReporteInventario: " + e.toString() );
			return ERROR;
		}
	}
	
	private String getContextPath(){
		
		ServletContext context = ServletActionContext.getServletContext();
		return context.getContextPath();
		
		
	}

	

	
	/*** SECCION getters y setters ***/
	
	public List<CgProveedor> getCgProveedorList() {
		return cgProveedorList;
	}

	public void setCgProveedorList(List<CgProveedor> cgProveedorList) {
		this.cgProveedorList = cgProveedorList;
	}

	public List<PrestadorDeServicioService.PrestadorServicioRegistro> getPrestadorServicioList() {
		return prestadorServicioList;
	}

	public void setPrestadorServicioList(
			List<PrestadorDeServicioService.PrestadorServicioRegistro> prestadorServicioList) {
		this.prestadorServicioList = prestadorServicioList;
	}

	public List<CgAgente> getCgAgenteList() {
		return cgAgenteList;
	}

	public void setCgAgenteList(List<CgAgente> cgAgenteList) {
		this.cgAgenteList = cgAgenteList;
	}

	public CgProveedor getCgProveedor() {
		return cgProveedor;
	}

	public void setCgProveedor(CgProveedor cgProveedor) {
		this.cgProveedor = cgProveedor;
	}

	public String getIdCgProveedor() {
		return idCgProveedor;
	}

	public void setIdCgProveedor(String idCgProveedor) {
		this.idCgProveedor = idCgProveedor;
	}

	public Map<Long, String> getGerenciaList() {
		return gerenciaList;
	}

	public void setGerenciaList(Map<Long, String> gerenciaList) {
		this.gerenciaList = gerenciaList;
	}

	public Map<Long, String> getEjecutivoList() {
		return ejecutivoList;
	}

	public void setEjecutivoList(Map<Long, String> ejecutivoList) {
		this.ejecutivoList = ejecutivoList;
	}

	public Map<Long, String> getPromotoriaList() {
		return promotoriaList;
	}

	public void setPromotoriaList(Map<Long, String> promotoriaList) {
		this.promotoriaList = promotoriaList;
	}

	public String getIdCgAgente() {
		return idCgAgente;
	}

	public void setIdCgAgente(String idCgAgente) {
		this.idCgAgente = idCgAgente;
	}

	public CgAgente getCgAgente() {
		return cgAgente;
	}

	public void setCgAgente(CgAgente cgAgente) {
		this.cgAgente = cgAgente;
	}

	public List<CgAgenteView> getCgAgenteViewList() {
		return cgAgenteViewList;
	}

	public void setCgAgenteViewList(List<CgAgenteView> cgAgenteViewList) {
		this.cgAgenteViewList = cgAgenteViewList;
	}

	public List<CgCentro> getCgCentroList() {
		return cgCentroList;
	}

	public void setCgCentroList(List<CgCentro> cgCentroList) {
		this.cgCentroList = cgCentroList;
	}

	public CgCentro getCgCentro() {
		return cgCentro;
	}

	public void setCgCentro(CgCentro cgCentro) {
		this.cgCentro = cgCentro;
	}

	public List<CgOrden> getCgOrdenList() {
		return cgOrdenList;
	}

	public void setCgOrdenList(List<CgOrden> cgOrdenList) {
		this.cgOrdenList = cgOrdenList;
	}

	public CgOrden getCgOrden() {
		return cgOrden;
	}

	public void setCgOrden(CgOrden cgOrden) {
		this.cgOrden = cgOrden;
	}

	public String getIdToOrden() {
		return idToOrden;
	}

	public void setIdToOrden(String idToOrden) {
		this.idToOrden = idToOrden;
	}

	public Map<String, String> getPaises() {
		return paises;
	}

	public void setPaises(Map<String, String> paises) {
		this.paises = paises;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public Map<String, String> getCiudades() {
		return ciudades;
	}

	public void setCiudades(Map<String, String> ciudades) {
		this.ciudades = ciudades;
	}

	public Map<String, String> getColonias() {
		return colonias;
	}

	public void setColonias(Map<String, String> colonias) {
		this.colonias = colonias;
	}

	public String getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(String idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public String getIdToAgente() {
		return idToAgente;
	}

	public void setIdToAgente(String idToAgente) {
		this.idToAgente = idToAgente;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getIdToCentro() {
		return idToCentro;
	}

	public void setIdToCentro(String idToCentro) {
		this.idToCentro = idToCentro;
	}

	public Map<Long, String> getCgCentrosList() {
		return cgCentrosList;
	}

	public void setCgCentrosList(Map<Long, String> cgCentrosList) {
		this.cgCentrosList = cgCentrosList;
	}

	public List<CgCentro> getCgCentrosProveedor() {
		return cgCentrosProveedor;
	}

	public void setCgCentrosProveedor(List<CgCentro> cgCentrosProveedor) {
		this.cgCentrosProveedor = cgCentrosProveedor;
	}

	public List<CgCentro> getCentrosProveedorList() {
		return centrosProveedorList;
	}

	public void setCentrosProveedorList(List<CgCentro> centrosProveedorList) {
		this.centrosProveedorList = centrosProveedorList;
	}

	public String getIsAgente() {
		return isAgente;
	}

	public void setIsAgente(String isAgente) {
		this.isAgente = isAgente;
	}

	public static List<ValorCatalogoAgentes> getCatalogoTipoSituacion() {
		return catalogoTipoSituacion;
	}

	public static void setCatalogoTipoSituacion(
			List<ValorCatalogoAgentes> catalogoTipoSituacion) {
		CondicionesGeneralesAction.catalogoTipoSituacion = catalogoTipoSituacion;
	}
	
}
