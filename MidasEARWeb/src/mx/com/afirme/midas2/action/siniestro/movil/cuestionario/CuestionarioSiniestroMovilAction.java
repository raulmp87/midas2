package mx.com.afirme.midas2.action.siniestro.movil.cuestionario;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestro.movil.cuestionario.PreguntaSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestro.movil.cuestionario.RespuestaNumericaSiniestroMovil;
import mx.com.afirme.midas2.service.siniestro.movil.cuestionario.CuestionarioSiniestroMovilService;

@Component
@Scope("prototype")
public class CuestionarioSiniestroMovilAction extends BaseAction {
	
	private static final Logger LOG = Logger.getLogger(CuestionarioSiniestroMovilAction.class);
	private static final long serialVersionUID = 3121156965146250377L;
	
	@Autowired
	private CuestionarioSiniestroMovilService cuestionarioSiniestroMovilService;
	
	private List<PreguntaSiniestroMovil> preguntas;
	private List<RespuestaNumericaSiniestroMovil> respuestas;
	
	/**
	 * /rest/cliente/siniestro/cuestionario/obtenerPreguntas.action
	 * @return json String
	 */
	public String obtenerPreguntas() {
		try {
			preguntas = cuestionarioSiniestroMovilService.obtenerPreguntas();
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			preguntas = new ArrayList<PreguntaSiniestroMovil>();
			addActionError(e.getLocalizedMessage());
			return INPUT;
		}
	}
	
	/**
	 * /rest/cliente/siniestro/cuestionario/responder.action
	 * @return json String
	 * @return
	 */
	public String responder() {
		try {
			boolean resultado = cuestionarioSiniestroMovilService.responder(respuestas);
			if (!resultado) {
				throw new Exception("Error al guardar las respuestas");
			}
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			return INPUT;
			
		}
	}

	public CuestionarioSiniestroMovilService getCuestionarioSiniestroMovilService() {
		return cuestionarioSiniestroMovilService;
	}

	public void setCuestionarioSiniestroMovilService(
			CuestionarioSiniestroMovilService cuestionarioSiniestroMovilService) {
		this.cuestionarioSiniestroMovilService = cuestionarioSiniestroMovilService;
	}

	public List<PreguntaSiniestroMovil> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<PreguntaSiniestroMovil> preguntas) {
		this.preguntas = preguntas;
	}

	public List<RespuestaNumericaSiniestroMovil> getRespuestas() {
		return respuestas;
	}

	public void setRespuestas(List<RespuestaNumericaSiniestroMovil> respuestas) {
		this.respuestas = respuestas;
	}

}
