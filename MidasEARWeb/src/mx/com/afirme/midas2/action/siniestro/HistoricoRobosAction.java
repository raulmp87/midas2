package mx.com.afirme.midas2.action.siniestro;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Namespace("/siniestros/cabina/reportecabina/historicoRobos")
@Component
@Scope("prototype")
public class HistoricoRobosAction extends BaseAction implements Preparable{
	
	private static final long serialVersionUID = 2611339004237425049L;
	
	private MovimientoSiniestroDTO filtroMovimientos;
	private Long reporteCabinaId;
	private Long idCobertura;
	private List<MovimientoSiniestroDTO> movimientos;
	private Map<String,String> tipoMovimientos;
	private List<AfectacionCoberturaSiniestroDTO> coberturas;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("estimacionCoberturaSiniestroServiceEJB")
	private EstimacionCoberturaSiniestroService estimacionCobSinService;
	
	@Autowired
	@Qualifier("movimientoSiniestroServiceEJB")
	private MovimientoSiniestroService movimientoSiniestroService;
	
	
	@Override
	public void prepare() throws Exception {
//		coberturas = estimacionCobSinService.obtenerCoberturasAfectacion(idToReporte);
		prepararTipoMovimientosRobo();
	}
	
	private void prepararTipoMovimientosRobo(){
		tipoMovimientos = new HashMap<String,String>();
		Map<String,String> tipoMovimientosRobo = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO_ROBO);
		Map<String,String> tipoMovimientosCompleto = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);
		
		for(String key: tipoMovimientosRobo.keySet()){
			String tipoMovimientosKey = tipoMovimientosRobo.get(key); 
			tipoMovimientos.put(tipoMovimientosKey, tipoMovimientosCompleto.get(tipoMovimientosKey));
		}
	}
	
	
	
	/**
	 * Muestra pantalla de la busqueda de historico de movimientos
	 * @return
	 */
	@Action(value = "mostrarHistoricoRobos", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/robos/contenedorHistoricoRobos.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/robos/contenedorHistoricoRobos.jsp")})
	public String mostrarHistoricoRobos() {			
			return SUCCESS;
	}
	
	/**
	 * Ejecuta la busqueda de los historicos de los movimientos
	 * @return
	 */
	@Action(value = "buscarHistoricoRobos", results = {
			@Result(name = SUCCESS, location = "/jsp/siniestros/robos/listadoHistoricoRoboGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/siniestros/robos/contenedorHistoricoRobos.jsp")})
	public String buscarHistoricoRobos() {
		try{
			if(reporteCabinaId != null){
				filtroMovimientos.setIdToReporte(reporteCabinaId);
				movimientos = movimientoSiniestroService.obtenerHistoricoMovimientosRobos(filtroMovimientos);
				return SUCCESS;
			}else{
				setMensajeError(MENSAJE_ERROR_GENERAL);
				return INPUT;
			}
			
		}catch(Exception ex){
			setMensajeError(MENSAJE_ERROR_GENERAL);
			return INPUT;
		}
	}

	/**
	 * @return the filtroMovimientos
	 */
	public MovimientoSiniestroDTO getFiltroMovimientos() {
		return filtroMovimientos;
	}

	/**
	 * @param filtroMovimientos the filtroMovimientos to set
	 */
	public void setFiltroMovimientos(MovimientoSiniestroDTO filtroMovimientos) {
		this.filtroMovimientos = filtroMovimientos;
	}

	/**
	 * @return the reporteCabinaId
	 */
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}

	/**
	 * @param reporteCabinaId the reporteCabinaId to set
	 */
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}

	/**
	 * @return the movimientos
	 */
	public List<MovimientoSiniestroDTO> getMovimientos() {
		return movimientos;
	}

	/**
	 * @param movimientos the movimientos to set
	 */
	public void setMovimientos(List<MovimientoSiniestroDTO> movimientos) {
		this.movimientos = movimientos;
	}

	/**
	 * @return the tipoMovimientos
	 */
	public Map<String, String> getTipoMovimientos() {
		return tipoMovimientos;
	}

	/**
	 * @param tipoMovimientos the tipoMovimientos to set
	 */
	public void setTipoMovimientos(Map<String, String> tipoMovimientos) {
		this.tipoMovimientos = tipoMovimientos;
	}

	/**
	 * @return the coberturas
	 */
	public List<AfectacionCoberturaSiniestroDTO> getCoberturas() {
		return coberturas;
	}

	/**
	 * @param coberturas the coberturas to set
	 */
	public void setCoberturas(List<AfectacionCoberturaSiniestroDTO> coberturas) {
		this.coberturas = coberturas;
	}

	/**
	 * @return the listadoService
	 */
	public ListadoService getListadoService() {
		return listadoService;
	}

	/**
	 * @param listadoService the listadoService to set
	 */
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	/**
	 * @return the estimacionCobSinService
	 */
	public EstimacionCoberturaSiniestroService getEstimacionCobSinService() {
		return estimacionCobSinService;
	}

	/**
	 * @param estimacionCobSinService the estimacionCobSinService to set
	 */
	public void setEstimacionCobSinService(
			EstimacionCoberturaSiniestroService estimacionCobSinService) {
		this.estimacionCobSinService = estimacionCobSinService;
	}

	/**
	 * @return the idCobertura
	 */
	public Long getIdCobertura() {
		return idCobertura;
	}

	/**
	 * @param idCobertura the idCobertura to set
	 */
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	
}
