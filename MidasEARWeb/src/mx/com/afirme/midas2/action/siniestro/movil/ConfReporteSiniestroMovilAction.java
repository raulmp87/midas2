package mx.com.afirme.midas2.action.siniestro.movil;

import java.util.Arrays;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ConfReporteSiniestroMovilAction extends BaseAction implements Preparable {
	
	private static final Logger LOG = Logger.getLogger(ConfReporteSiniestroMovilAction.class);

	private static final long serialVersionUID = -8627774979418729949L;
	
	@Autowired
	private DireccionMidasService direccionMidasService;
	
	@Autowired
	private ParametroGeneralService parametroGeneralService;
	
	private List<String> selectedStates;
	private List<EstadoMidas> states;
	
	@Override
	public void prepare() throws Exception {
		states = direccionMidasService
				.obtenerEstados(UtileriasWeb.KEY_PAIS_MEXICO);
	}

	public String mostrar() {
		selectedStates = obtenerEstadosGuardados();
		return SUCCESS;
	}
	
	public String guardar() {
		try {
			final ParametroGeneralId id = new ParametroGeneralId(
					ParametroGeneralDTO.GRUPO_CONFIGURACION_SINIESTRO_MOVIL,
					ParametroGeneralDTO.CODIGO_CONFIGURACION_ESTADOS_SINIESTRO_MOVIL);
			final ParametroGeneralDTO parametro = new ParametroGeneralDTO();
			parametro.setId(id);
			parametro.setValor(StringUtils.join(selectedStates.iterator(), ","));
			parametroGeneralService.save(parametro);
			addActionMessage("Guardado exitosamente");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError("Ocurrio un error al guardar la informacion, por favor verifica los datos y vuelve a intentar");
			return ERROR;
		}
		return SUCCESS;
	}
	
	private List<String> obtenerEstadosGuardados() {
		final ParametroGeneralDTO parametro = parametroGeneralService
				.findById(new ParametroGeneralId(
						ParametroGeneralDTO.GRUPO_CONFIGURACION_SINIESTRO_MOVIL,
						ParametroGeneralDTO.CODIGO_CONFIGURACION_ESTADOS_SINIESTRO_MOVIL));
		return Arrays.asList(parametro.getValor().split(","));
	}

	
	public DireccionMidasService getDireccionMidasService() {
		return direccionMidasService;
	}

	public void setDireccionMidasService(DireccionMidasService direccionMidasService) {
		this.direccionMidasService = direccionMidasService;
	}

	public ParametroGeneralService getParametroGeneralService() {
		return parametroGeneralService;
	}

	public void setParametroGeneralService(
			ParametroGeneralService parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}

	public List<String> getSelectedStates() {
		return selectedStates;
	}

	public void setSelectedStates(List<String> selectedStates) {
		this.selectedStates = selectedStates;
	}

	public List<EstadoMidas> getStates() {
		return states;
	}

	public void setStates(List<EstadoMidas> states) {
		this.states = states;
	}



}
