package mx.com.afirme.midas2.action.siniestro;

import java.math.BigDecimal;

import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.siniestro.ProductoReporteSiniestroMovil;
import mx.com.afirme.midas2.service.siniestro.ReporteSiniestroMovilService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;

@Component
@Scope("prototype")
public class ValidarNumeroPolizaAction extends BaseAction {
	
	private static final long serialVersionUID = -7240810865311652678L;
	
	private NumeroPolizaCompleto numeroPoliza;
	private BigDecimal numeroInciso;
	private String nombreCompletoAsegurado;
	private ProductoReporteSiniestroMovil productoReporteSiniestroMovil;
	@Autowired
	private ReporteSiniestroMovilService reporteSiniestroMovilService;

	@Autowired
	private PolizaFacadeRemote polizaFacadeRemote;
	
	public NumeroPolizaCompleto getNumeroPoliza() {
		return numeroPoliza;
	}
	
	@RequiredFieldValidator(message="requerido")
	public void setNumeroPoliza(NumeroPolizaCompleto numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	
	@RequiredFieldValidator(message="requerido")
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	public String getNombreCompletoAsegurado() {
		return nombreCompletoAsegurado;
	}
	
	public void setNombreCompletoAsegurado(String nombreCompletoAsegurado) {
		this.nombreCompletoAsegurado = nombreCompletoAsegurado;
	}
	
	public ProductoReporteSiniestroMovil getProductoReporteSiniestroMovil() {
		return productoReporteSiniestroMovil;
	}
	
	@RequiredFieldValidator(message="requerido")
	public void setProductoReporteSiniestroMovil(
			ProductoReporteSiniestroMovil productoReporteSiniestroMovil) {
		this.productoReporteSiniestroMovil = productoReporteSiniestroMovil;
	}
	
	public void validate() {
		String fieldName = "numeroPoliza";
		if (numeroPoliza != null) {
			PolizaDTO poliza = polizaFacadeRemote.find(numeroPoliza);
			if (poliza == null) {
				addFieldError(fieldName, "No se encontró la póliza.");
			} else {
				if (numeroInciso != null) {					
					if (!reporteSiniestroMovilService.existeInciso(poliza, numeroInciso.intValue())) {
						addFieldError(fieldName, "No se encontró el inciso.");
					} else {
						//Validar que el productoReporteSiniestro corresponda a uno de los productos de la póliza encontrada
						if (productoReporteSiniestroMovil != null) {
							if (productoReporteSiniestroMovil.getId().equals(ProductoReporteSiniestroMovil.ID_AUTOS)) {
								String claveNegocio = poliza.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getClaveNegocio();
								if (!claveNegocio.equals(NegocioSeguros.CLAVE_AUTOS)) {
									addFieldError(fieldName, "No es una póliza de autos.");
								}
							} else if (productoReporteSiniestroMovil.getId().equals(ProductoReporteSiniestroMovil.ID_CASA)) {
								if(!polizaFacadeRemote.esPolizaCasa(poliza.getIdToPoliza())) {
									addFieldError(fieldName, "No es una póliza de casa.");
								}	
							}
						}
					}
				}
			}
		}
	}
	
	public String execute() {
		return SUCCESS;
	}

}
