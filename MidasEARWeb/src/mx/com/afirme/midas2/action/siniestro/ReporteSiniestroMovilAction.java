package mx.com.afirme.midas2.action.siniestro;

import java.math.BigDecimal;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.cometd.ExternalEventBroadcaster;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.siniestro.ProductoReporteSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil.Estatus;
import mx.com.afirme.midas2.domain.siniestro.TipoSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.siniestro.ReporteSiniestroMovilService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.util.PagingList;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.cometd.bayeux.server.BayeuxServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
public class ReporteSiniestroMovilAction extends BaseAction implements Preparable {
	
	private final Logger LOG = Logger.getLogger(ReporteSiniestroMovil.class);
	private static final long serialVersionUID = -7424521022293651235L;
	
	@Autowired
	private PolizaFacadeRemote polizaFacadeRemote;
	@Autowired
	private ReporteSiniestroMovilService reporteSiniestroMovilService;
	@Autowired
	private ReporteCabinaService reporteCabinaService;
	@Autowired
	private ServicioSiniestroService servicioSiniestroService;
	@Autowired
	private AjustadorEstatusService ajustadorEstatusService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	private List<ReporteSiniestroMovil> reporteSiniestros;

	private NumeroPolizaCompleto numeroPoliza;
	private PolizaDTO poliza;
	private BigDecimal numeroInciso;
	private String nombreCompletoAsegurado;
	private String numeroCelular;
	private Double latitud;
	private Double longitud;
	private TipoSiniestroMovil tipoSiniestro;
	private String nombrePersonaReporta;
	
	private Long numeroConfirmacion;
	private String contentType = "";
	

	private Long id;
	private ReporteSiniestroMovil reporteSiniestroMovil;
	private Oficina oficina; 
	private Long estatusId;
	private String nombreUsuarioAsignado;
	private boolean includeDhtmlxHeader = true;
	private Integer pageSize = 10;
	private Integer count;
	private Integer total;	
	private Boolean clearAndLoad;
	
	private Boolean asignadosAMi;
	
	private String token;
	
	private ServicioSiniestro ajustador;
	private Boolean confirmado;
	private Coordenadas coordenadas;
	private Map<Long, String> oficinas;
	private Long idToReporteCabina;
	private String  cveOficina;
	private String consReporte;
	private String anioReporte;
	
	public void setAsignadosAMi(Boolean asignadosAMi) {
		this.asignadosAMi = asignadosAMi;
	}
	
	public Boolean getPaginationEnabled() {
		return estatusId != null && !Estatus.parse(estatusId).equals(Estatus.SIN_ASIGNAR);
	}
	
	public boolean isRestringirCabina() {
		return reporteSiniestroMovilService.isRestringirCabina();
	}
		
	@Override
	public List<Integer> getPages() {
		return super.getPages();
	}
	
	public Integer getPageSize() {
		return pageSize;
	}
	
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	public Integer getCount() {
		return count;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}
	
	public Integer getTotal() {
		return total;
	}
	
	public void setClearAndLoad(Boolean clearAndLoad) {
		this.clearAndLoad = clearAndLoad;
	}
	
	private static final String ESTATUS_INCORRECTO_ERROR = "No es posible cambiar el estatus debido a que actualmente el estatus es: %s. Esto sucede cuando alguien ya lo cambio antes que nosotros.";   		
	
	public boolean isIncludeDhtmlxHeader() {
		return includeDhtmlxHeader;
	}
	
	public int getCurrentPage() {
		if (getPosStart() == null) {
			return 1;
		}
		
		return (getPosStart() / getPageSize()) + 1;  
	}
	

	public List<ReporteSiniestroMovil> getReporteSiniestros() {
		if (reporteSiniestros == null) {
			
			if (id == null) {			
				//TODO:Converter para enums en base a un id y no al nombre del enum?
				if (reporteSiniestroMovil == null) {
					reporteSiniestroMovil = new ReporteSiniestroMovil();
				}
				reporteSiniestroMovil.setEstatus(null);
				reporteSiniestroMovil.setFechaCreacion(null);
				reporteSiniestroMovil.setEstatus(Estatus.parse(estatusId));
				if (reporteSiniestroMovil.getEstatus().equals(Estatus.SIN_ASIGNAR)) {
					reporteSiniestros = reporteSiniestroMovilService.findSinAsignar();
				} else {										
					if (reporteSiniestroMovil.getEstatus().equals(Estatus.ASIGNADA)) {
						
						if (asignadosAMi != null && asignadosAMi) {
							reporteSiniestroMovil.setNombreUsuarioAsignado(usuarioService.getUsuarioActual().getNombreUsuario());							
						}
					}					
					PagingList<ReporteSiniestroMovil> paging = reporteSiniestroMovilService.findByExamplePage(reporteSiniestroMovil, pageSize, getCurrentPage() - 1);
					total = paging.size();
					
					if ((clearAndLoad == null || !clearAndLoad) && getPosStart() == null) {
						setPosStart(0);
						//Only headers to initialize the grid.
						reporteSiniestros = new ArrayList<ReporteSiniestroMovil>();
					} else {
						includeDhtmlxHeader = false;
						reporteSiniestros = paging.getPage(getCurrentPage() - 1);	
					}
				}
			} else {
				includeDhtmlxHeader = false;
				loadReporteSiniestroMovil();
				reporteSiniestros = new ArrayList<ReporteSiniestroMovil>();
				if (reporteSiniestroMovil != null) {
					reporteSiniestros.add(reporteSiniestroMovil);
				}
			}
		}
		return reporteSiniestros;
	}
	
	public NumeroPolizaCompleto getNumeroPoliza() {
		return numeroPoliza;
	}
	
	public void setNumeroPoliza(NumeroPolizaCompleto numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}
	
	public BigDecimal getNumeroInciso() {
		return numeroInciso;
	}
	
	public void setNumeroInciso(BigDecimal numeroInciso) {
		this.numeroInciso = numeroInciso;
	}
	
	public String getNombreCompletoAsegurado() {
		return nombreCompletoAsegurado;
	}
	
	public void setNombreCompletoAsegurado(String nombreCompletoAsegurado) {
		this.nombreCompletoAsegurado = nombreCompletoAsegurado;
	}
	
	public String getNumeroCelular() {
		return numeroCelular;
	}
	
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
	
	public Double getLatitud() {
		return latitud;
	}
	
	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}
	
	public Double getLongitud() {
		return longitud;
	}
	
	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
	
	public TipoSiniestroMovil getTipoSiniestro() {
		return tipoSiniestro;
	}
	
	public void setTipoSiniestro(TipoSiniestroMovil tipoSiniestro) {
		this.tipoSiniestro = tipoSiniestro;
	}
	
	public String getNombrePersonaReporta() {
		return nombrePersonaReporta;
	}
	
	public void setNombrePersonaReporta(String nombrePersonaReporta) {
		this.nombrePersonaReporta = nombrePersonaReporta;
	}
	
	public Long getNumeroConfirmacion() {
		return numeroConfirmacion;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public ReporteSiniestroMovil getReporteSiniestroMovil() {
		return reporteSiniestroMovil;
	}
	
	public void setReporteSiniestroMovil(
			ReporteSiniestroMovil reporteSiniestroMovil) {
		this.reporteSiniestroMovil = reporteSiniestroMovil;
	}
	
	public Long getEstatusId() {
		return estatusId;
	}
	
	public void setEstatusId(Long estatusId) {
		this.estatusId = estatusId;
	}
	
	public void setNombreUsuarioAsignado(String nombreUsuarioAsignado) {
		this.nombreUsuarioAsignado = nombreUsuarioAsignado;
	}
			
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public ServicioSiniestro getAjustador() {
		return ajustador;
	}

	public void setAjustador(ServicioSiniestro ajustador) {
		this.ajustador = ajustador;
	}
	
	public Boolean getConfirmado() {
		return confirmado;
	}

	public void setConfirmado(Boolean confirmado) {
		this.confirmado = confirmado;
	}
	
	public Coordenadas getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(Coordenadas coordenadas) {
		this.coordenadas = coordenadas;
	}

	@Override
	public void prepare() {
	 oficinas =	listadoService.obtenerOficinasSiniestros();
	}
		
	@SkipValidation
	public String listar() {
		return SUCCESS;
	}
	
	@SkipValidation
	public String listarDhtmlx() {
		return SUCCESS;
	}

	public void validateGuardar() {
		if (numeroPoliza == null) {
			addFieldError("numeroPoliza", "requerido");
		}
		
		if (numeroInciso == null) {
			addFieldError("numeroInciso", "requerido");
		}
				
		if (StringUtils.isBlank(numeroCelular)) {
			addFieldError("numeroCelular", "requerido");			
		} else {
			if (!numeroCelular.matches("[\\d]{10}")) {
				addFieldError("numeroCelular", "El número debe ser de 10 dígitos");
			}
		}
		
		if (tipoSiniestro == null) {
			addFieldError("tipoSiniestro", "requerido");
		}
		
		if (StringUtils.isBlank(nombrePersonaReporta)) {
			addFieldError("nombrePersonaReporta", "requerido");
		}

		
		if (numeroPoliza != null) {
			poliza = polizaFacadeRemote.find(numeroPoliza);
			if (poliza == null) {
				addFieldError("numeroPoliza", "No se encontró la póliza.");
			} else {
				if (numeroInciso != null) {
					if (!reporteSiniestroMovilService.existeInciso(poliza, numeroInciso.intValue())) {
						addFieldError("numeroInciso", "No se encontró el inciso.");
					} else {
						//Validar que el tipoSiniestro y la poliza sean apropiados.
						if (tipoSiniestro != null) {
							if (tipoSiniestro.getProductoReporteSiniestroMovil().getId().equals(ProductoReporteSiniestroMovil.ID_AUTOS)) {
								String claveNegocio = poliza.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getClaveNegocio();
								if (!claveNegocio.equals(NegocioSeguros.CLAVE_AUTOS)) {
									addFieldError("numeroPoliza", "No es una póliza de autos.");
								}
							} else if (tipoSiniestro.getProductoReporteSiniestroMovil().getId().equals(ProductoReporteSiniestroMovil.ID_CASA)) {
								if(!polizaFacadeRemote.esPolizaCasa(poliza.getIdToPoliza())) {
									addFieldError("numeroPoliza", "No es una póliza de casa.");
								}								
							}
						}
					}
				}
			}
		}
	}
	
	public String guardar() {
    	ReporteSiniestroMovil reporteSiniestroMovil = reporteSiniestroMovilService.findReporteReciente(numeroCelular);
    	if (reporteSiniestroMovil == null) {
    		//No se encontro ningun reporte reciente entonces se procede a crear uno nuevo.
	    	reporteSiniestroMovil = new ReporteSiniestroMovil();
	    	reporteSiniestroMovil.setPolizaDTO(poliza);
	    	reporteSiniestroMovil.setNumeroInciso(numeroInciso);
	    	reporteSiniestroMovil.setNumeroCelular(numeroCelular);
	    	if (latitud != null && longitud != null) {
	    		reporteSiniestroMovil.setCoordenadas(new Coordenadas(latitud, longitud));
	    	}
	    	reporteSiniestroMovil.setTipoSiniestroMovil(tipoSiniestro);    	
	    	reporteSiniestroMovil.setNombrePersonaReporta(nombrePersonaReporta);
	    	
	    	Usuario usuario = usuarioService.buscarUsuarioRegistrado(token);
	    	reporteSiniestroMovil.setNombreUsuarioReporta(usuario.getNombreUsuario());
	    	
	    	reporteSiniestroMovil = reporteSiniestroMovilService.reportarSiniestro(reporteSiniestroMovil);
	    	
	    	
	    	
	    	//Notificar nuevo siniestro a cabina.
	    	ServletContext sc = ServletActionContext.getServletContext();
    		BayeuxServer bayeux = (BayeuxServer) sc.getAttribute(BayeuxServer.ATTRIBUTE);
    		ExternalEventBroadcaster broadcaster = new ExternalEventBroadcaster(bayeux);
    		broadcaster.onExternalEvent("{\"id\":" + reporteSiniestroMovil.getId() + "}","/siniestro/reporte-movil/nuevo/" + reporteSiniestroMovil.getCabina().getId());    	
    	}
    	
    	numeroConfirmacion = reporteSiniestroMovil.getId();
    	
    	String mensaje = "Reporte de siniestro "
			+ numeroConfirmacion
			+ " creado con éxito. Personal de Afirme se pondrá en contacto con usted al teléfono " 
			+ reporteSiniestroMovil.getNumeroCelular() + ". No use su línea durante este tiempo.";
    	
        
    	if (contentType.equals("txt")) {
    		//quitar acentos
    		mensaje = Normalizer.normalize(mensaje, Normalizer.Form.NFD);
    		mensaje = mensaje.replaceAll("[^\\p{ASCII}]", "");
    	}
    	
    	setMensajeExitoPersonalizado(mensaje);
		
		return SUCCESS;
    }
    
    

    private void loadReporteSiniestroMovil() {
    	if (id != null) {
    		reporteSiniestroMovil = reporteSiniestroMovilService.findById(id);
    	}
    }    
    
    public void prepareDetalle() {
    	loadReporteSiniestroMovil();
    }
    
    public String detalle() {
    	return SUCCESS;
    }
    
    	
	public void prepareCambiarEstatus2() {
    	loadReporteSiniestroMovil();    	
    }
	
    public void validateCambiarEstatus2() {
		if (id == null) {
			addFieldError("id", "requerido");
		} else {
			if (!reporteSiniestroMovil.getEstatus().equals(Estatus.SIN_ASIGNAR)) {
				addActionError(String.format(ESTATUS_INCORRECTO_ERROR, reporteSiniestroMovil.getEstatus().getNombre()));
			}
		}
				
		convertirErrorsAMensajeError();
    }
       
    
    public String cambiarEstatus2() {
    	reporteSiniestroMovil.setEstatus(Estatus.parse(2));    	
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_Siniestros_Reporte_Siniestros_Movil_Asignar_Reporte_Otros_Usuarios")
				&& !StringUtils.isBlank(nombreUsuarioAsignado)) {
    		reporteSiniestroMovil.setNombreUsuarioAsignado(nombreUsuarioAsignado);
    	} else {
    		reporteSiniestroMovil.setNombreUsuarioAsignado(usuarioService.getUsuarioActual().getNombreUsuario());
    	}
    	
    	reporteSiniestroMovil = reporteSiniestroMovilService.guardar(reporteSiniestroMovil);
    	
    	ServletContext sc = ServletActionContext.getServletContext();
		BayeuxServer bayeux = (BayeuxServer) sc.getAttribute(BayeuxServer.ATTRIBUTE);
		ExternalEventBroadcaster broadcaster = new ExternalEventBroadcaster(bayeux);
		broadcaster.onExternalEvent("{\"id\":" + reporteSiniestroMovil.getId() + "}","/siniestro/reporte-movil/asignado/" + reporteSiniestroMovil.getCabina().getId());		
		
    	return SUCCESS;
    }
        
    public void prepareCambiarEstatus3() {
    	loadReporteSiniestroMovil();    	
    }
    
    public void validateCambiarEstatus3() {
    	if (id == null) {
			addFieldError("id", "requerido");
			return;
		}
    	
    	if(!reporteSiniestroMovil.getEstatus().equals(Estatus.ASIGNADA)) {
			addActionError(String.format(ESTATUS_INCORRECTO_ERROR, reporteSiniestroMovil.getEstatus().getNombre()));
			return;
		}
		
		if (reporteSiniestroMovil.getFalsaAlarma() == null) {
			addFieldError("reporteSiniestroMovil.falsaAlarma", "requerido");
			return;
		}
		
		if (!reporteSiniestroMovil.getFalsaAlarma()) {
	    	if(reporteSiniestroMovil.getOficina() == null || reporteSiniestroMovil.getOficina().getId() == null) {
				addFieldError("reporteSiniestroMovil.oficina.id", "requerido");
	    	}
		}
    }
    
    public String cambiarEstatus3() {
        	if (reporteSiniestroMovil.getFalsaAlarma() != null && reporteSiniestroMovil.getFalsaAlarma()) {
        		reporteSiniestroMovil.setEstatus(Estatus.parse(3));
            	reporteSiniestroMovil = reporteSiniestroMovilService.guardar(reporteSiniestroMovil);
        		return INPUT;
        	}
        	oficina = reporteSiniestroMovil.getOficina();
        	reporteSiniestroMovil = reporteSiniestroMovilService.findById(reporteSiniestroMovil.getId());
        	reporteSiniestroMovil.setEstatus(Estatus.TERMINADA);
        	reporteSiniestroMovil.setOficina(oficina);
        	idToReporteCabina = reporteSiniestroMovilService.crearReporteCabina(reporteSiniestroMovil);
        	this.cveOficina = reporteSiniestroMovil.getReporteCabina().getClaveOficina();
        	this.anioReporte = reporteSiniestroMovil.getReporteCabina().getAnioReporte();
        	this.consReporte = reporteSiniestroMovil.getReporteCabina().getConsecutivoReporte();
        	return SUCCESS;
    }
    
    
	/**
	 * /siniestro/reporte-movil/obtenerDatosAjustador.action
	 * @return json String
	 * @return
	 */
    public String obtenerDatosAjustador() {
    	try {
			final ReporteSiniestroMovil reporteMovil = reporteSiniestroMovilService
					.findByIdWithCabin(reporteSiniestroMovil.getId());
			coordenadas = reporteMovil.getCoordenadas();
			ajustador = servicioSiniestroService.findById(reporteMovil
					.getReporteCabina().getAjustador().getId());
			return SUCCESS;
    	} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			ajustador = new ServicioSiniestro();
			return INPUT;
    	}   	
    }
    
    /**
	 * /siniestro/reporte-movil/obtenerUbicacionAjustador.action
	 * @return json String
	 * @return
	 */
    public String obtenerUbicacionAjustador() {
    	try {
			final ReporteSiniestroMovil reporteMovil = reporteSiniestroMovilService
					.findByIdWithCabin(reporteSiniestroMovil.getId());
			final ServicioSiniestro ajustador = servicioSiniestroService
					.findById(reporteMovil.getReporteCabina().getAjustador()
							.getId());
			final AjustadorEstatus estatus = ajustadorEstatusService.crearAjustadorEstatus(ajustador, null);
			coordenadas = estatus.getCoordenadas();
			return SUCCESS;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			ajustador = new ServicioSiniestro();
			coordenadas = new Coordenadas();
			return INPUT;
    	}   
    }
    
	/**
	 * /siniestro/reporte-movil/confirmarArribo.action
	 * @return json String
	 * @return
	 */
    public String confirmarArribo() {
    	try {
        	final ReporteSiniestroMovil reporteMovil = reporteSiniestroMovilService.findByIdWithCabin(reporteSiniestroMovil.getId());
        	reporteMovil.setArriboConfirmado(confirmado);
        	reporteSiniestroMovilService.guardar(reporteMovil);
        	return SUCCESS;
    	} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			ajustador = new ServicioSiniestro();
			return INPUT;
    	}   	
    }
    
	/**
	 * /siniestro/reporte-movil/confirmarPrimerArribo.action
	 * @return json String
	 * @return
	 */
    public String confirmarPrimerArribo() {
    	try {
        	final ReporteSiniestroMovil reporteMovil = reporteSiniestroMovilService.findByIdWithCabin(reporteSiniestroMovil.getId());
        	reporteMovil.setPrimeroEnArribar(confirmado);
        	reporteSiniestroMovilService.guardar(reporteMovil);
        	return SUCCESS;
    	} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			addActionError(e.getLocalizedMessage());
			ajustador = new ServicioSiniestro();
			return INPUT;
    	}   	
    }

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public String getCveOficina() {
		return cveOficina;
	}

	public void setCveOficina(String cveOficina) {
		this.cveOficina = cveOficina;
	}

	public String getConsReporte() {
		return consReporte;
	}

	public void setConsReporte(String consReporte) {
		this.consReporte = consReporte;
	}

	public String getAnioReporte() {
		return anioReporte;
	}

	public void setAnioReporte(String anioReporte) {
		this.anioReporte = anioReporte;
	}
}