package mx.com.afirme.midas2.action.siniestro;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.DefinicionSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestro.RoboService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.ExcelExporter;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


/**
 * @author usuario
 * @version 1.0
 * @created 28-jul-2014 12:50:26 p.m.
 */
@Component
@Scope("prototype")
@Namespace("/siniestros/cabina/reporteCabina/moduloRobos")
public class ModuloRobosAction extends BaseAction implements Preparable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1429106007186667498L;

	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;
	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	@Autowired
	@Qualifier("polizaSiniestroServiceEJB")
	private PolizaSiniestroService polizaSiniestroService;
	

	@Autowired
	@Qualifier("roboServiceEJB")
	private RoboService roboService;
	
	private TransporteImpresionDTO transporte;	
	
	private ReporteSiniestroRoboDTO reporteSiniestroRoboDTO;
	private DefinicionSiniestroRoboDTO definicionSiniestroRoboDTO;
	
	private Map<Long,String> oficinas;
	private Map<String,String> estados;
	private Map<String,String> municipios;
	private Map<String,String> tiposRobo;
	private Map<String,String> estatus;
	private Map<String,String> marca;
	private Map<String,String> localizador;
	private Map<String,String> recuperador;
	private Map<String,String> proveedor;
	private Map<String,String> causaMovimiento;
	private Map<String,String> tipoMovimientos;

	private Long reporteCabinaId;
	private Long idCobertura;
	private Long idEstimacion;
	private String urlOcra;
	
	private Boolean esPrimeraVes;
	
	private IncisoSiniestroDTO detalleInciso;

	/**
	 * Lista con el resultado de la busqueda de siniestros
	 */
	private List<ReporteSiniestroRoboDTO> listadoSiniestrosRobo= new ArrayList<ReporteSiniestroRoboDTO>();	
	
	/**
	 * Este atributo se usar� para la exportaci�n  a excel del listado de siniestros
	 * de robo
	 */
	private TransporteImpresionDTO  transporteImpresionDTO;
	private final String MOSTARCONTENEDORLISTADO ="/jsp/siniestros/robos/contenedorListadoRobos.jsp";
	private final String MOSTARLISTADOROBOS ="/jsp/siniestros/robos/listadoRobo.jsp";
	private final String MOSTRARCDEFINICIONROBO ="/jsp/siniestros/robos/contenedorDefinirRobo.jsp";
	private final String MOSTARLISTACOBERTURA ="listadoCobertura.jsp";

	/**
	 * Este metodo es el metodo de entrada del modulo, redirecciona al contenedor
	 * principal <b><i>contenedorListadoRobos.jsp</i></b>
	 */
	@Action(value="roboTotal",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDORLISTADO),
			@Result(name=INPUT,location=MOSTARCONTENEDORLISTADO)
			})
	public String roboTotal(){

		return SUCCESS;
	}
	/**
	 * Este metodo muestra el listado de robos 
	 * principal <b><i>listadoRobo.jsp</i></b>
	 */

	@Action(value="busquedasSiniestrosRobo",results={
			@Result(name=SUCCESS,location=MOSTARLISTADOROBOS),
			@Result(name=INPUT,location=MOSTARCONTENEDORLISTADO)
			})
	public String busquedasSiniestrosRobo(){		
		
		if(this.esPrimeraVes){
			listadoSiniestrosRobo = null;
		}else{
			listadoSiniestrosRobo = roboService.busquedaRobo(reporteSiniestroRoboDTO);
			//listadoSiniestrosRobo = null;
			if (null ==listadoSiniestrosRobo || listadoSiniestrosRobo.isEmpty()||  listadoSiniestrosRobo.size()<=0){
				this.setMensajeError("No Encontró Resultados");
			}
			
		}
		return SUCCESS;
		
	}
	
	
	
	/**
	 * Este metodo muestra el listado de Coberturas 
	 * principal <b><i>listadoRobo.jsp</i></b>
	 */

	@Action(value="busquedasCoberturas",results={
			@Result(name=SUCCESS,location=MOSTARLISTACOBERTURA),
			@Result(name=INPUT,location=MOSTARLISTACOBERTURA)
			})
	public String busquedasCoberturas(){
			return SUCCESS;
	}
	
	

	/**
	 * Exporta el resultado de la b�squeda a un archivo excel, debe coincidir con la
	 * listra desplegada en pantalla
	 */
	@Action(value="exportarListado",
		       results={@Result(name=SUCCESS,
		type="stream",
		params={"contentType","${transporte.contentType}",
		"inputName","transporte.genericInputStream",
		"contentDisposition",
		"attachment;filename=\"${transporte.fileName}\""})})
		public String exportarListado() {		
			listadoSiniestrosRobo = reporteCabinaService.buscarReportesRobo(reporteSiniestroRoboDTO);	
				 	
			ExcelExporter exporter = new ExcelExporter(ReporteSiniestroRoboDTO.class);	
			transporte = exporter.exportXLS(listadoSiniestrosRobo, "Listado Siniestros Rob");		
					
			return SUCCESS;
		}
	/**
	 * Este metodo maneja las peticiones de guardado desde la pantalla de seguimiento
	 * de robo, hace el llamado al servicio de RoboTotalServicio para enviar la
	 * actualizaci�n de datos a OCRA
	 */
	@Action(value="guardarSeguimientoRobo",results={
			@Result(name=SUCCESS,location=MOSTARCONTENEDORLISTADO),
			@Result(name=INPUT,location=MOSTRARCDEFINICIONROBO)
			})
	public String guardarSeguimientoRobo(){
		definicionSiniestroRoboDTO.setReporteCabinaid(this.reporteCabinaId) ;
		definicionSiniestroRoboDTO.setCoberturaId(this.idCobertura) ;
		try{
			 
			 roboService.guardarYProvisionar(definicionSiniestroRoboDTO, definicionSiniestroRoboDTO.getEnvioOcra());		
		}catch(Exception ex){
            super.setMensajeError(getText("Error al guardar la identificacion del siniestro: "+ ex.getMessage()));
            return INPUT;
		}
		try{
			roboService.envioOcra(definicionSiniestroRoboDTO);		
		}catch(Exception ex){
           super.setMensajeExitoPersonalizado(("[Detalle Guardado exitosamente.] "+ "        Estatus Envio a OCRA:"+ ex.getMessage()));
           return INPUT;
		}
	     super.setMensaje(getText("Detalle Guardado exitosamente."));
	     return SUCCESS;
	}
	
	public void prepareSeguimientoRoboTotal(){
		// # OBTENER URL PARA OCRA
		this.urlOcra = this.roboService.getValorParametroGlobal(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.WS_URL_OCRA_ADMIN );
	}
	
	/**
	 * Este metodo maneja las peticiones para mostrar el detalle de un siniestro
	 * catalogado como robo total, debe precargar la informaci�n en pantalla y
	 * direccionar al contenedor  <b><i>contenedorDefinirRobo.jsp</i></b>
	 */

	@Action(value="seguimientoRoboTotal",results={
			@Result(name=SUCCESS,location=MOSTRARCDEFINICIONROBO),
			@Result(name=INPUT,location=MOSTRARCDEFINICIONROBO)
			})
	public String seguimientoRoboTotal(){
		
		definicionSiniestroRoboDTO = roboService.consultarDefinicionSiniestroRobo(this.reporteCabinaId, this.idCobertura);		
		definicionSiniestroRoboDTO.setReporteCabinaid(this.reporteCabinaId) ;
		definicionSiniestroRoboDTO.setCoberturaId(this.idCobertura) ;
		detalleInciso = polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(this.reporteCabinaId);
		
		return SUCCESS;
	}

	@Override
	public void prepare() throws Exception {
		this.oficinas = listadoService.obtenerOficinasSiniestros();		
		this.estados= listadoService.getMapEstadosMX();
		this.municipios =new LinkedHashMap<String, String>();  
		this.municipios.put("","Seleccione..");	 
		this.tiposRobo= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ROBO);
		this.estatus =listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_VEHICULO);
		this.localizador = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.LOCALIZADOR);
		this.recuperador = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.RECUPERADOR);
		this.proveedor=new LinkedHashMap<String, String>();
        tipoMovimientos = new LinkedHashMap<String, String>();
        Map<String,String> tipoMovimientosRobo = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO_ROBO);
        Map<String,String> tipoMovimientosCompleto = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);        
        for(String key: tipoMovimientosRobo.keySet()){
              String tipoMovimientosKey = tipoMovimientosRobo.get(key); 
              tipoMovimientos.put(tipoMovimientosKey, tipoMovimientosCompleto.get(tipoMovimientosKey));
        }

		
	}

	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}

	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public RoboService getRoboService() {
		return roboService;
	}

	public void setRoboService(RoboService roboService) {
		this.roboService = roboService;
	}

	public TransporteImpresionDTO getTransporte() {
		return transporte;
	}

	public void setTransporte(TransporteImpresionDTO transporte) {
		this.transporte = transporte;
	}


	public ReporteSiniestroRoboDTO getReporteSiniestroRoboDTO() {
		return reporteSiniestroRoboDTO;
	}

	public void setReporteSiniestroRoboDTO(
			ReporteSiniestroRoboDTO reporteSiniestroRoboDTO) {
		this.reporteSiniestroRoboDTO = reporteSiniestroRoboDTO;
	}

	public List<ReporteSiniestroRoboDTO> getListadoSiniestrosRobo() {
		return listadoSiniestrosRobo;
	}

	public void setListadoSiniestrosRobo(
			List<ReporteSiniestroRoboDTO> listadoSiniestrosRobo) {
		this.listadoSiniestrosRobo = listadoSiniestrosRobo;
	}

	public TransporteImpresionDTO getTransporteImpresionDTO() {
		return transporteImpresionDTO;
	}

	public void setTransporteImpresionDTO(
			TransporteImpresionDTO transporteImpresionDTO) {
		this.transporteImpresionDTO = transporteImpresionDTO;
	}

	public Map<Long, String> getOficinas() {
		return oficinas;
	}

	public void setOficinas(Map<Long, String> oficinas) {
		this.oficinas = oficinas;
	}

	public Map<String, String> getEstados() {
		return estados;
	}

	public void setEstados(Map<String, String> estados) {
		this.estados = estados;
	}

	public Map<String, String> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(Map<String, String> municipios) {
		this.municipios = municipios;
	}

	

	public Map<String, String> getTiposRobo() {
		return tiposRobo;
	}

	public void setTiposRobo(Map<String, String> tiposRobo) {
		this.tiposRobo = tiposRobo;
	}

	public Map<String, String> getEstatus() {
		return estatus;
	}

	public void setEstatus(Map<String, String> estatus) {
		this.estatus = estatus;
	}

	

	

	public DefinicionSiniestroRoboDTO getDefinicionSiniestroRoboDTO() {
		return definicionSiniestroRoboDTO;
	}

	public void setDefinicionSiniestroRoboDTO(
			DefinicionSiniestroRoboDTO definicionSiniestroRoboDTO) {
		this.definicionSiniestroRoboDTO = definicionSiniestroRoboDTO;
	}
	

	public Map<String, String> getMarca() {
		return marca;
	}

	public void setMarca(Map<String, String> marca) {
		this.marca = marca;
	}

	public Map<String, String> getLocalizador() {
		return localizador;
	}

	public void setLocalizador(Map<String, String> localizador) {
		this.localizador = localizador;
	}

	public Map<String, String> getRecuperador() {
		return recuperador;
	}

	public void setRecuperador(Map<String, String> recuperador) {
		this.recuperador = recuperador;
	}

	public Map<String, String> getProveedor() {
		return proveedor;
	}

	public void setProveedor(Map<String, String> proveedor) {
		this.proveedor = proveedor;
	}

	public Map<String, String> getCausaMovimiento() {
		return causaMovimiento;
	}

	public void setCausaMovimiento(Map<String, String> causaMovimiento) {
		this.causaMovimiento = causaMovimiento;
	}
	public Long getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(Long idCobertura) {
		this.idCobertura = idCobertura;
	}
	public Long getReporteCabinaId() {
		return reporteCabinaId;
	}
	public void setReporteCabinaId(Long reporteCabinaId) {
		this.reporteCabinaId = reporteCabinaId;
	}
	public Map<String, String> getTipoMovimientos() {
		return tipoMovimientos;
	}
	public void setTipoMovimientos(Map<String, String> tipoMovimientos) {
		this.tipoMovimientos = tipoMovimientos;
	}
	public Long getIdEstimacion() {
		return idEstimacion;
	}
	public void setIdEstimacion(Long idEstimacion) {
		this.idEstimacion = idEstimacion;
	}
	public String getUrlOcra() {
		return urlOcra;
	}
	public void setUrlOcra(String urlOcra) {
		this.urlOcra = urlOcra;
	}
	/**
	 * @return the detalleInciso
	 */
	public IncisoSiniestroDTO getDetalleInciso() {
		return detalleInciso;
	}
	/**
	 * @param detalleInciso the detalleInciso to set
	 */
	public void setDetalleInciso(IncisoSiniestroDTO detalleInciso) {
		this.detalleInciso = detalleInciso;
	}
	public Boolean getEsPrimeraVes() {
		return esPrimeraVes;
	}
	public void setEsPrimeraVes(Boolean esPrimeraVes) {
		this.esPrimeraVes = esPrimeraVes;
	}
	
	
	
}