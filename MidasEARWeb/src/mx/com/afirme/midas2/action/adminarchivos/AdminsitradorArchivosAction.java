package mx.com.afirme.midas2.action.adminarchivos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.documentosFortimax.FortimaxDocument;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.dto.siniestros.ArchivosDigitalizadosSiniestroDTO;
import mx.com.afirme.midas2.service.fortimax.CatalogoAplicacionFortimaxService;
import mx.com.afirme.midas2.service.siniestro.AdministradorArchivosService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.util.StringUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace(value = "/siniestros/repositorioarchivos")
public class AdminsitradorArchivosAction  extends BaseAction implements Preparable  {

	private String cveProceso;
	private String subCarpeta;
	private String tituloVentana;
	private String idFortimax;
	private String liga ;
	private String aplicacion ;
	private String folioExpediente ;
	private String nombreArchivo ;
	private String nodo ;
	private Map<String, String> docFortimax =new LinkedHashMap<String, String>();
	private Map<String, String> listaDocProceso=new LinkedHashMap<String, String>();
	private List<ArchivosDigitalizadosSiniestroDTO> listaArchivos = new ArrayList<ArchivosDigitalizadosSiniestroDTO>();
	private InputStream inputStream;
	private String contentType;
	private String fileName;
	private String errorDesc;
	public  String DIGITALIZADO_SI = "SI";
	public  String DIGITALIZADO_NO = "NO";
	public  String DOCUMENTO= "D" ;
	public  String REQUIERE_fOLIO = "1";


	/**
	 * 
	 */
	private static final long serialVersionUID = -6985807894662035238L;

	@Autowired
	@Qualifier("reporteCabinaServiceEJB")
	private ReporteCabinaService reporteCabinaService;


	@Autowired
	@Qualifier("administradorArchivosEJB")
	private AdministradorArchivosService archivosService;//Borrar 

	@Autowired
	@Qualifier("catalogoAplicacionFortimaxServiceEJB")
	private   CatalogoAplicacionFortimaxService catApFortimaxService ;




	@Override
	public void prepare() throws Exception {
		
		
	}
	@Action(value="gridArchivo",results={
			@Result(name=SUCCESS,location="/jsp/adminarchivos/administradorArchivosGrid.jsp"),
			@Result(name=INPUT,location="/jsp/adminarchivos/administradorArchivosGrid.jsp"),
			@Result(name = ERROR, location = "/jsp/adminarchivos/errorFX.jsp")
			})
	public String gridArchivo(){
		try {	 
			ProcesosFortimax proceso = archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso));
			CatalogoAplicacionFortimax ap= catApFortimaxService.loadById(proceso.getIdAplicacion());
			Map<String, String> docFortimax =new LinkedHashMap<String, String>();
			Map<String, String> listaDocProceso=new LinkedHashMap<String, String>();
			if (proceso.getTipo().equalsIgnoreCase(AdministradorArchivosService.PROCESO_TIPO_LISTA)){
				listaDocProceso =archivosService.getListaArchivosByProceso(PROCESO_FORTIMAX.valueOf(cveProceso));
				docFortimax  =archivosService.getDocumentosAlmacenadosByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax));
				if ( !listaDocProceso.isEmpty() ){
					for (Map.Entry<String, String> entry : listaDocProceso.entrySet()) {
						ArchivosDigitalizadosSiniestroDTO dto =new ArchivosDigitalizadosSiniestroDTO();
						dto.setNombre(entry.getValue());
						dto.setNodo(entry.getKey());
						dto.setDigitalizado(DIGITALIZADO_NO);
						dto.setAplicacion(ap.getNombreAplicacion());
						dto.setCveProceso(proceso.getProceso());
						dto.setIdExpediente(idFortimax);
						dto.setDescripcion(proceso.getDescripcion());
						if (docFortimax.containsValue(dto.getNombre().trim()))
							dto.setDigitalizado(DIGITALIZADO_SI);	
						listaArchivos.add(dto);
					}
				}
			}else if (proceso.getTipo().equalsIgnoreCase(AdministradorArchivosService.PROCESO_TIPO_GENERICO)){
				docFortimax  =archivosService.getDocumentosAlmacenadosByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax));
				if (!docFortimax.isEmpty()){
					for (Map.Entry<String, String> entry : docFortimax.entrySet()) {
						ArchivosDigitalizadosSiniestroDTO dto =new ArchivosDigitalizadosSiniestroDTO();
						dto.setNombre(entry.getValue());
						dto.setDescripcion(proceso.getDescripcion());
						dto.setNodo(entry.getKey());
						dto.setDigitalizado(DIGITALIZADO_SI);
						dto.setAplicacion(ap.getNombreAplicacion());
						dto.setCveProceso(proceso.getProceso());
						dto.setIdExpediente(idFortimax);
						listaArchivos.add(dto);
					}
				}
			} 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			listaArchivos=null;
			return SUCCESS;
			
		}
		return INPUT;
	}
	
	

	
	public void prepareMostrarArchivos() throws Exception{
		ProcesosFortimax proceso = archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso));
		setTituloVentana(proceso.getDescripcion().toString());
	}
	@Action(value = "adminArchivos", 
			results = { 
			@Result(name = SUCCESS, location = "/jsp/adminarchivos/contenedorAdministradorArchivos.jsp"),
			@Result(name = INPUT, location = "/jsp/adminarchivos/contenedorAdministradorArchivos.jsp"),
			@Result(name = ERROR, location = "/jsp/adminarchivos/errorFX.jsp")
			})
			public String mostrarArchivos(){
		
			try {
				this.generarExpediente();
			} catch (Exception e) {
				errorDesc=e.getMessage();
				return ERROR;
				
			}	
		return SUCCESS;
	}
	

	  private void generarExpediente()throws Exception{
		
		try { 		 	 
			/*GENERAR EXPEDIENTE */ 
			FortimaxDocument document = new FortimaxDocument ();
			ProcesosFortimax proceso=archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso));				
			if ( null!=proceso.getRequiereFolio() && proceso.getRequiereFolio().toString().equalsIgnoreCase(REQUIERE_fOLIO) ){ 
				if(StringUtil.isEmpty(subCarpeta)){
					  
					throw new Exception("El Proceso Requiere folio de Documento");
  
				}  
			 	 
			}
			CatalogoAplicacionFortimax ap= catApFortimaxService.loadById(proceso.getIdAplicacion());			
			document.setAplicacion(ap.getNombreAplicacion());// Nombre de la Gaveta
			document.setId(new Long (idFortimax));
			if(StringUtil.isEmpty(folioExpediente))
				document.setFolio(idFortimax.toString());
			else
				document.setFolio(folioExpediente);
					document.setFieldValues(null);
					this.setIdFortimax(idFortimax);
					this.setCveProceso(cveProceso);					
				archivosService.generarExpediente(document); 		
				tituloVentana= archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso)).getDescripcion().toString();				
				} catch (Exception e) {					
						throw new Exception(e.getMessage());

				}  		
	}
	
	
	
	@Action (value = "verExpediente", results = { 
			@Result(name = SUCCESS, location = "/jsp/adminarchivos/ligaIfimax.jsp"),
			@Result(name = ERROR, location = "/jsp/adminarchivos/errorFX.jsp")})
			public String verExpediente(){
		//	TODO: adjuntar doc
			FortimaxDocument documento;
			if ( !StringUtil.isEmpty( this.nombreArchivo) ){
				try {
					this.generarExpediente();
				} catch (Exception e) {
					this.errorDesc= e.getMessage();
					return ERROR;
				}
			}
			try {
				documento = archivosService.generarDocumentoByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax), subCarpeta);
		
			} catch (Exception e1) {
				
				e1.printStackTrace();
				errorDesc=e1.getMessage();
				return ERROR; 
				
			} 
			/* obtener nodo documento generado */
			boolean isByDocument= false;
			if ( !StringUtil.isEmpty( this.nombreArchivo) ){
				documento.setNombreArchivo(this.nombreArchivo);
				isByDocument=true;
			}
			
			try {
				ProcesosFortimax proceso = archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso));
				if( (proceso.getTipo().equalsIgnoreCase(AdministradorArchivosService.PROCESO_TIPO_GENERICO)) || isByDocument){
					documento.setNodo(archivosService.getNodo( new Long (idFortimax), documento.getAplicacion(), documento.getNombreArchivo(), DOCUMENTO, true));
				}else{
					 documento.setNodo(null);
					 documento.setNombreArchivo(null);
				 }
				 
			} catch (Exception e) {
				documento.setNodo(null);
				 documento.setNombreArchivo(null);
			}
			
			
			
			
			try {
				
				 liga =archivosService.generarLigaByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax),documento.getNombreArchivo(),false);
				 System.out.print(liga);
			} catch (Exception e) {				
				
				try {
					 documento.setNodo(null);	
					 liga =archivosService.generarLigaByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax),null,false);
					 System.out.print(liga);
				} catch (Exception e1) {
					errorDesc= e.getMessage();	
					return ERROR;
				}
				
			}
			if( StringUtil.isEmpty(liga) ){
				errorDesc="Error al Generar Liga Ifimax";		
				return ERROR;
			}
			return SUCCESS;
	}
	
	
	
	@Action (value = "verDocumento", results = { 
			@Result(name = SUCCESS, location = "/jsp/adminarchivos/ligaIfimax.jsp"),
			@Result(name = ERROR, location = "/jsp/adminarchivos/errorFX.jsp")})
			public String verDocumento(){
		//	TODO: adjuntar doc
			FortimaxDocument documento;
			/*GENERAR EXPEDIENTE */ 
			FortimaxDocument document = new FortimaxDocument ();
			ProcesosFortimax proceso;			
			
			try{
			proceso = archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso));	
			if (null!=proceso.getRequiereFolio() && proceso.getRequiereFolio().toString().equalsIgnoreCase(REQUIERE_fOLIO) ){
				if(StringUtil.isEmpty(subCarpeta)){
					errorDesc= "El Proceso Requiere folio de Documento";
					return ERROR;
				}
				
			}
			CatalogoAplicacionFortimax ap= catApFortimaxService.loadById(proceso.getIdAplicacion());			
			document.setAplicacion(ap.getNombreAplicacion());// Nombre de la Gaveta
			document.setId(new Long (idFortimax));
			
			if(StringUtil.isEmpty(folioExpediente))
				document.setFolio(idFortimax.toString());
			else
				document.setFolio(folioExpediente);
					document.setFieldValues(null);
					this.setIdFortimax(idFortimax);
					this.setCveProceso(cveProceso);					
				archivosService.generarExpediente(document); 		
				tituloVentana= archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso)).getDescripcion().toString();				
				} catch (Exception e) {
					if (e.getMessage().contains("FMX-DOC-WS-1013"))
						errorDesc=e.getMessage();		
						return ERROR;

				} 
 
			try {
				documento = archivosService.generarDocumentoByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax), subCarpeta);
				
			} catch (Exception e1) {
				
				e1.printStackTrace();
				errorDesc=e1.getMessage();
				return ERROR; 
				
			} 
			/* obtener nodo documento generado */
			try {
					documento.setNodo(archivosService.getNodo( new Long (idFortimax), documento.getAplicacion(),this.nombreArchivo, DOCUMENTO, true));
					 documento.setNombreArchivo(null);
				
				 
			} catch (Exception e) {
				documento.setNodo(null);
			}
			try {
				
				 liga =archivosService.generarLigaByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax),this.nombreArchivo,false);
				 System.out.print(liga);
			} catch (Exception e) {				
				
				try {
					 documento.setNodo(null);	
					 liga =archivosService.generarLigaByProceso(PROCESO_FORTIMAX.valueOf(cveProceso), new Long (idFortimax),null,false);
					 System.out.print(liga);
				} catch (Exception e1) {
					errorDesc= e.getMessage();	
					return ERROR;
				}
				
			}
			if( StringUtil.isEmpty(liga) ){
				errorDesc="Error al Generar Liga Ifimax";		
				return ERROR;
			}
			return SUCCESS;
	}
	
	
	
	
	
	@Action(value="descargarDocumento",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","inputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})
	}) 
	public String descargarDocumento(){
		try {
			System.out.print("descargarDocumento");
			ProcesosFortimax proceso = archivosService.getProceso(PROCESO_FORTIMAX.valueOf(cveProceso));
			FortimaxDocument document = new FortimaxDocument();
			CatalogoAplicacionFortimax ap= catApFortimaxService.loadById(proceso.getIdAplicacion());
			document.setNombreArchivo(nombreArchivo);
			document.setTipoDoc("D"); 
			document.setAplicacion(ap.getNombreAplicacion()); 
			document.setId(new Long (idFortimax));			
			archivosService.descargarDocumentoFortimax(document);
			inputStream =new ByteArrayInputStream(document.getArchivoDescarga().getByteArray());
			contentType = "application/pdf";
			fileName = nombreArchivo+".pdf";
		} catch (Exception e) {
			return null;
		}
		return SUCCESS;
	}


	public String getCveProceso() {
		return cveProceso;
	}


	public void setCveProceso(String cveProceso) {
		this.cveProceso = cveProceso;
	}


	public String getSubCarpeta() {
		return subCarpeta;
	}


	public void setSubCarpeta(String subCarpeta) {
		this.subCarpeta = subCarpeta;
	}


	public ReporteCabinaService getReporteCabinaService() {
		return reporteCabinaService;
	}


	public void setReporteCabinaService(ReporteCabinaService reporteCabinaService) {
		this.reporteCabinaService = reporteCabinaService;
	}


	public String getTituloVentana() {
		return tituloVentana;
	}


	public void setTituloVentana(String tituloVentana) {
		this.tituloVentana = tituloVentana;
	}


	public String getIdFortimax() {
		return idFortimax;
	}


	public void setIdFortimax(String idFortimax) {
		this.idFortimax = idFortimax;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	public Map<String, String> getDocFortimax() {
		return docFortimax;
	}

	public void setDocFortimax(Map<String, String> docFortimax) {
		this.docFortimax = docFortimax;
	}

	public Map<String, String> getListaDocProceso() {
		return listaDocProceso;
	}

	public void setListaDocProceso(Map<String, String> listaDocProceso) {
		this.listaDocProceso = listaDocProceso;
	}

	public List<ArchivosDigitalizadosSiniestroDTO> getListaArchivos() {
		return listaArchivos;
	}

	public void setListaArchivos(List<ArchivosDigitalizadosSiniestroDTO> listaArchivos) {
		this.listaArchivos = listaArchivos;
	}


	public String getAplicacion() {
		return aplicacion;
	}


	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}


	public String getNombreArchivo() {
		return nombreArchivo;
	}


	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}


	public String getNodo() {
		return nodo;
	}


	public void setNodo(String nodo) {
		this.nodo = nodo;
	}


	public InputStream getInputStream() {
		return inputStream;
	}


	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}


	public String getContentType() {
		return contentType;
	}


	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getErrorDesc() {
		return errorDesc;
	}


	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	public String getFolioExpediente() {
		return folioExpediente;
	}
	public void setFolioExpediente(String folioExpediente) {
		this.folioExpediente = folioExpediente;
	}
	


}
