package mx.com.afirme.midas2.action.auth.portal;

import java.util.Arrays;

public abstract class RestResultDTO {
	
	protected String mensaje;
	protected boolean success;
	protected String[] actionErrors;

	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}	
	
	public String[] getActionErrors() {
		return actionErrors;
	}
	public void setActionErrors(String[] actionErrors) {
		this.actionErrors = actionErrors;
	}

	@Override
	public String toString() {
		return "RestResultDTO [mensaje=" + mensaje + ", success=" + success
				+ ", actionErrors=" + Arrays.toString(actionErrors) + ", data="
				+ "]";
	}
	


}
