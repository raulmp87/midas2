package mx.com.afirme.midas2.action.auth.portal;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.auth.portal.PortalMensajes.Type;
import mx.com.afirme.midas2.service.seguridad.UsuarioService.LoginParameter;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.asm.dto.CreatePortalUserParameterDTO;
import com.asm.dto.SendResetPasswordTokenDTO;
import com.opensymphony.xwork2.ActionContext;

@Component
@Scope("prototype")
public class PortalAuthAction extends BaseAction {
	
	private static final Logger LOG = Logger.getLogger(PortalAuthAction.class);
	private static final String errorPasswordASM = "The given password";
	private static final String errorPasswordCustom = "Password incorrecto";
	private static final String errorUserInactiveASM ="is not active";
	private static final String errorUserInactiveCustom ="El usuario no esta activo";
	private static final String errorEmailConfirmedASM = "has not confirmed his email";
	private static final String errorEmailConfirmedCustom ="El email no ha sido confirmado";
	private static final long serialVersionUID = -6751530900645780736L;

	private String username;
	private String password;
	private String email;
	private String retypeEmail;
	private String retypePassword;
	private String cellPhone;
	private String firstName;
	private String fatherLastName;
	private String motherLastName;
    private PortalMensajes portalMensaje;
	
	public String execute() {
		return SUCCESS;
	}	
	
	public String doLogin() {		
		final LoginParameter loginParameter = new LoginParameter();
		loginParameter.setUsuario(username);
		loginParameter.setPassword(password);
		loginParameter.setIpAddress(ServletActionContext.getRequest().getRemoteHost());
		loginParameter.setApplicationId(5);		
		try {
			Usuario usuario = usuarioService.login(loginParameter);			
			usuario = usuarioService.buscarUsuarioRegistrado(usuario.getToken());
			ActionContext.getContext().getSession().put(sistemaContext.getUsuarioAccesoMidas(), usuario);	
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			LOG.info("ERROR AL AUTENTICARSE:  "+e.getMessage());
			if(e.getMessage().contains(errorPasswordASM) || e.getMessage().contains(errorPasswordCustom)){
				setPortalMensaje(new PortalMensajes(Type.DANGER, "La contraseña introducida es incorrecta, favor de validar lo datos."));
			}else if(e.getMessage().contains(errorUserInactiveASM) || e.getMessage().contains(errorUserInactiveCustom)){
				setPortalMensaje(new PortalMensajes(Type.DANGER, "El usuario se encuentra inactivo, favor de comunicarse a atención a clientes."));
			}else if(e.getMessage().contains(errorEmailConfirmedASM) || e.getMessage().contains(errorEmailConfirmedCustom)){
				setPortalMensaje(new PortalMensajes(Type.DANGER, "El email no ha sido confirmado. Revise su correo y de click en la liga para confirmar."));
			}else {
				setPortalMensaje(new PortalMensajes(Type.DANGER,"Ocurrio un error al autenticar."));				
			}
			return INPUT;
		}
		return SUCCESS;
	}
	
	public String createUser(){
		CreatePortalUserParameterDTO createPortalUserDTO = new CreatePortalUserParameterDTO();
		createPortalUserDTO.setFirstName("|"+firstName);
		LOG.info("fistName:: "+createPortalUserDTO.getFirstName());
		createPortalUserDTO.setFatherLastName(fatherLastName);
		createPortalUserDTO.setMotherLastName(motherLastName);
		createPortalUserDTO.setCellPhone(cellPhone);
		createPortalUserDTO.setEmail(email);
		createPortalUserDTO.setRetypeEmail(retypeEmail);
		createPortalUserDTO.setPassword(password);
		createPortalUserDTO.setRetypePassword(retypePassword);
		try{
			if(createPortalUserDTO.getEmail() != null && createPortalUserDTO.getRetypeEmail() != null && createPortalUserDTO.getPassword() != null && createPortalUserDTO.getRetypePassword() != null){
				if(createPortalUserDTO.getEmail().equals(createPortalUserDTO.getRetypeEmail()) && createPortalUserDTO.getPassword().equals(createPortalUserDTO.getRetypePassword())){
					@SuppressWarnings("unused")
					Usuario userNew = usuarioService.createPortalUser(createPortalUserDTO);					
				}
			}
		}catch(Exception ex){
			LOG.error(ex.getMessage(), ex);
			if(ex.getMessage().contains("email:")){
				setPortalMensaje(new PortalMensajes(Type.WARNING, "El mail ya se encuentra registrado."));
			}else if(ex.getMessage().contains("cellPhone:")){
				setPortalMensaje(new PortalMensajes(Type.WARNING, "El telefono debe ser de 10 digitos."));
			}
			return INPUT;
		}
		setPortalMensaje(new PortalMensajes(Type.SUCCESS, "La cuenta se ha creado con exito"));
		return SUCCESS;
	}
	
	public String restaurarPassword(){
		SendResetPasswordTokenDTO sendResetPassTokenDTO = new SendResetPasswordTokenDTO();
		sendResetPassTokenDTO.setEmail(email);
		try{
			if(sendResetPassTokenDTO.getEmail() != null){
				usuarioService.sendResetPasswordToken(sendResetPassTokenDTO);
				setPortalMensaje(new PortalMensajes(Type.SUCCESS, "Se ha enviado un correo a su cuenta para restaurar la contraseña."));
			}			
		}catch(Exception ex){
			LOG.error("Error inesperado al enviar correo de reseteo de contraseña::= "+ ex.getMessage(), ex);
			setPortalMensaje(new PortalMensajes(Type.WARNING, "Ocurrio un error inesperado. Favor de intentar mas tarde."));
			return INPUT;
		}
		return SUCCESS;
	}
	
	public String mostrar() {
		return SUCCESS;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRetypeEmail() {
		return retypeEmail;
	}

	public void setRetypeEmail(String retypeEmail) {
		this.retypeEmail = retypeEmail;
	}

	public String getRetypePassword() {
		return retypePassword;
	}

	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFatherLastName() {
		return fatherLastName;
	}

	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}

	public String getMotherLastName() {
		return motherLastName;
	}

	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}

	public PortalMensajes getPortalMensaje() {
		return portalMensaje;
	}

	public void setPortalMensaje(PortalMensajes portalMensaje) {
		this.portalMensaje = portalMensaje;
	}
}