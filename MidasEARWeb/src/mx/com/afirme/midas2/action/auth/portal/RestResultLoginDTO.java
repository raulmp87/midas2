package mx.com.afirme.midas2.action.auth.portal;

import java.util.Arrays;

import mx.com.afirme.midas2.action.auth.AuthAction.LoginResponse.Usuario;


public class RestResultLoginDTO extends RestResultDTO {

	private Result data;

	public Result getData() {
		return data;
	}

	public void setData(Result data) {
		this.data = data;
	}
	
	public static class Result {

		private String token;
		private Usuario usuario;

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public Usuario getUsuario() {
			return usuario;
		}

		public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
		}

		@Override
		public String toString() {
			return "Result [token=" + token + ", usuario="
					+ usuario + "]";
		}

	}

	@Override
	public String toString() {
		return "RestResultLoginDTO [data=" + data + ", mensaje=" + mensaje
				+ ", success=" + success + ", actionErrors="
				+ Arrays.toString(actionErrors) + "]";
	}


}
