package mx.com.afirme.midas2.action.auth;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.auth.AuthAction.LoginResponse.Usuario;
import mx.com.afirme.midas2.domain.movil.ajustador.Ajustador;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarContactoParameter;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilMidasService;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService.LoginParameter;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.asm.dto.CreatePasswordUserParameterDTO;
import com.asm.dto.CreatePortalUserParameterDTO;
import com.asm.dto.ResendConfirmationEmailDTO;
import com.asm.dto.SearchUserParametersDTO;
import com.asm.dto.SendResetPasswordTokenDTO;
import com.asm.dto.UserUpdateDTO;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

/**
 * Servicios REST para autenticacion.
 */
@Component
@Scope("prototype")
public class AuthAction extends BaseAction implements Preparable, ModelDriven<Object>, ServletRequestAware {

	private String id;
	private Object model;
	private Object data;
	private HttpServletRequest request;	
	private String token;
	private String resetToken;
	private String confirmationCode;
	private String user;
	private String currentPassword;
	private String newPassword;
	private String confirmNewPassword;
	private String username;
	private Integer userId;
	private Integer applicationId;
	private String url;	
	private boolean midas;
	
	private AjustadorMovilService ajustadorMovilService;
	private AjustadorMovilMidasService ajustadorMovilMidasService;
	private ClientePolizasService polizasService;
	
	public void setPolizasService(ClientePolizasService polizasService) {
		this.polizasService = polizasService;
	}
	
	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
			
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}
	
	@Autowired
	public void setAjustadorMovilService(
			AjustadorMovilService ajustadorMovilService) {
		this.ajustadorMovilService = ajustadorMovilService;
	}
	@Autowired
	public void setAjustadorMovilMidasService(
			AjustadorMovilMidasService ajustadorMovilMidasService) {
		this.ajustadorMovilMidasService = ajustadorMovilMidasService;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getResetToken() {
		return resetToken;
	}
	
	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}
		
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Object getData() {
		return data;
	}

	@Override
	public void prepare() throws Exception {
		
	}

	public void prepareLogin() {
		LoginParameter loginParameter = new LoginParameter();
		loginParameter.setIpAddress(request.getRemoteHost());
		model = loginParameter;
	}
	
	public void prepareLogout() {
		model = new MarcarContactoParameter();
	}
	
	public void prepareUpdateUser() {
		model = new UserUpdateDTO();
	}
	
	public void prepareActivateDeactivateUser() {
		model = new ActivateDeactivateUserParameter();
	}
	
	public void prepareGetUserList() {
		model = new SearchUserParametersDTO();
	}
		
	@Override
	public Object getModel() {
		return model;
	}
	
	public static class LoginResponse {		
		public static class Usuario {
			private Integer id;
			private String nombreUsuario;
			private Ajustador ajustador;
			private List<Rol> roles;
			private String nombreCompleto;
			private String nombre;
			private String apellidoPaterno;
			private String apellidoMaterno;
			private String cellPhone;
			private String email;
			private String promoCode;
			private String deviceUuid;
			
			public Integer getId() {
				return id;
			}
			public void setId(Integer id) {
				this.id = id;
			}
			public String getNombreUsuario() {
				return nombreUsuario;
			}
			public void setNombreUsuario(String nombreUsuario) {
				this.nombreUsuario = nombreUsuario;
			}
			public Ajustador getAjustador() {
				return ajustador;
			}
			public void setAjustador(Ajustador ajustador) {
				this.ajustador = ajustador;
			}
			public void setRoles(List<Rol> roles) {
				this.roles = roles;
			}
			public List<Rol> getRoles() {
				return roles;
			}
			public String getNombreCompleto() {
				return nombreCompleto;
			}
			public void setNombreCompleto(String nombreCompleto) {
				this.nombreCompleto = nombreCompleto;
			}
			public String getNombre() {
				return nombre;
			}
			public void setNombre(String nombre) {
				this.nombre = nombre;
			}
			public String getApellidoPaterno() {
				return apellidoPaterno;
			}
			public void setApellidoPaterno(String apellidoPaterno) {
				this.apellidoPaterno = apellidoPaterno;
			}
			public String getApellidoMaterno() {
				return apellidoMaterno;
			}
			public void setApellidoMaterno(String apellidoMaterno) {
				this.apellidoMaterno = apellidoMaterno;
			}
			public String getCellPhone() {
				return cellPhone;
			}
			public void setCellPhone(String cellPhone) {
				this.cellPhone = cellPhone;
			}
			public String getEmail() {
				return email;
			}
			public void setEmail(String email) {
				this.email = email;
			}
			public String getPromoCode() {
				return promoCode;
			}
			public void setPromoCode(String promoCode) {
				this.promoCode = promoCode;
			}
			public String getDeviceUuid() {
				return deviceUuid;
			}
			public void setDeviceUuid(String deviceUuid) {
				this.deviceUuid = deviceUuid;
			}			
		}
		
		private Usuario usuario;
		private String token;
		public Usuario getUsuario() {
			return usuario;
		}
		public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
		}
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
	}
	
	public static class ActivateDeactivateUserParameter {

		private Integer userId;
		private Boolean active;
		
		public Integer getUserId() {
			return userId;
		}
		
		public void setUserId(Integer userId) {
			this.userId = userId;
		}
		
		public Boolean getActive() {
			return active;
		}
		
		public void setActive(Boolean active) {
			this.active = active;
		}
	}
	
	public String login() {
		mx.com.afirme.midas.sistema.seguridad.Usuario usuario = usuarioService.login((LoginParameter)model);
		LoginResponse response = new LoginResponse();
		response.setToken(usuario.getIdSesionUsuario());
		Usuario u = new Usuario();
		u.setId(usuario.getId());
		u.setNombreUsuario(usuario.getNombreUsuario());
		if (midas){
			u.setAjustador(ajustadorMovilMidasService.getAjustador(usuario));
		} else {
			u.setAjustador(ajustadorMovilService.getAjustador(usuario));
		}
		u.setRoles(usuario.getRoles());
		u.setNombreCompleto(usuario.getNombreCompleto());
		u.setNombre(usuario.getNombre());
		u.setApellidoPaterno(usuario.getApellidoPaterno());
		u.setApellidoMaterno(usuario.getApellidoMaterno());
		u.setCellPhone(usuario.getTelefonoCelular());
		u.setPromoCode(usuario.getPromoCode());
		u.setDeviceUuid(usuario.getDeviceUuid());
		u.setEmail(usuario.getEmail());
		response.setUsuario(u);
		response.setToken(usuario.getToken());
		
		data = response;
		return SUCCESS;
	}
	
	public void validateLogout() {
		if (StringUtils.isBlank(token)) {
			addFieldError("token", "requerido");
		}
	}

	public String logout() {
		usuarioService.deshabilitarToken(token);
		return SUCCESS;		
	}
	
	public void prepareCreatePortalUser() {
		model = new CreatePortalUserParameterDTO();
	}
	
	public String createPortalUser() {
		usuarioService.createPortalUser((CreatePortalUserParameterDTO)model);
		return SUCCESS;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;		
	}
	
	
	public void prepareResendConfirmationEmail() {
		model = new ResendConfirmationEmailDTO();
	}
	
	public String resendConfirmationEmail() {
		ResendConfirmationEmailDTO resendConfirmationEmail = (ResendConfirmationEmailDTO) model;
		usuarioService.resendConfirmationEmail(resendConfirmationEmail);
		return SUCCESS;
	}
	
	public String confirmEmail() {
		int userId = usuarioService.confirmEmail(confirmationCode);
		
		if (usuarioService.isUserNeedsToSetPassword(userId)) {
			url = usuarioService.getResetPasswordUrl(userId);
			return "redirect";
		}
		return SUCCESS;
	}
	
	public String changePassword() {
		usuarioService.changePassword(currentPassword, newPassword);
		return SUCCESS;
	}

	
	public void validateChangePassword2() {
		if (!StringUtils.isBlank(newPassword) && !StringUtils.isBlank(confirmNewPassword) && !newPassword.equals(confirmNewPassword)) {
			addActionError("Los passwords no coinciden");
		}
	}
	
	public String changePassword2() {
		usuarioService.resetPassword(resetToken, newPassword);
		return SUCCESS;
	}
	
	public void prepareResetPassword() {
		addFieldErrorAlias("email", "user");
		model = new SendResetPasswordTokenDTO();
	}
	
	public String resetPassword() {
		SendResetPasswordTokenDTO sendResetPasswordToken = (SendResetPasswordTokenDTO) model;
		if (!StringUtils.isBlank(user)) {
			sendResetPasswordToken.setEmail(user);
		}
		usuarioService.sendResetPasswordToken(sendResetPasswordToken);
		return SUCCESS;
	}
		
	public String updateUser() {
		usuarioService.updateUser((UserUpdateDTO) model);
		return SUCCESS;
	}
	
	public String activateDeactivateUser() {
		ActivateDeactivateUserParameter parameter = (ActivateDeactivateUserParameter) model;
		usuarioService.activateDeactivateUser(parameter.getUserId(), parameter.getActive(), usuarioService.getUsuarioActual().getId());
		return SUCCESS;
	}
	
	public String getUserList() {
		data = usuarioService.getUserList((SearchUserParametersDTO) model);
		return SUCCESS;
	}
	public boolean isMidas() {
		return midas;
	}

	public void setMidas(boolean midas) {
		this.midas = midas;
	}
	
	public String resendConfirmationEmail2() {
		usuarioService.resendConfirmationEmail(userId);
		return SUCCESS;
	}
	
	public String resetPassword2() {
		usuarioService.sendResetPasswordToken(userId);
		return SUCCESS;
	}
	
	public void prepareCreatePasswordUser() {
		model = new CreatePasswordUserParameterDTO();
	}
	
	public String createPasswordUser() {
		data = usuarioService.createPasswordUser((CreatePasswordUserParameterDTO) model);
		return SUCCESS;
	}
	  
	public String findUserIdByUsername() {
		data = usuarioService.findUserIdByUsername(username);
		return SUCCESS;
	}
	
	
	public String isUserHasAccessToApplication() {
		data = usuarioService.isUserHasAccessToApplication(userId, applicationId);
		return SUCCESS;
	}

	public String isUserPortalUser() {
		data = usuarioService.isUserPortalUser(userId);
		return SUCCESS;		
	}

	public String getUserApplications() {
		data = usuarioService.getUserApplications(userId);
		return SUCCESS;
	}

}
