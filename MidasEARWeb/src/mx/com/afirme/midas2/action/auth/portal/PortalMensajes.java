package mx.com.afirme.midas2.action.auth.portal;

public class PortalMensajes {

	public enum Type {
		
		SUCCESS("success"), INFO("info"), WARNING("warning"), DANGER("danger");

		private final String type;

		private Type(String type) {
			this.type = type;
		}

		public String getType() {
			return this.type;
		}
	}

	private Type type;
	private String message;

	public PortalMensajes(Type type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type.getType();
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}