package mx.com.afirme.midas2.action.vehiculo;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.vehiculo.ValorComercialVehiculo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.excels.GeneraExcelCargaValorComercialVehiculos;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.migracion.EstadoMigracion;
import mx.com.afirme.midas2.service.vehiculo.ValorComercialVehiculoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/vehiculo/valorComercialVehiculo")
public class ValorComercialVehiculoAction extends BaseAction implements Preparable {
	
	
	/**
	 * 
	 */
	private static final long                       serialVersionUID           = 1L;
	private Long 				   					idToControlArchivo;
	private ValorComercialVehiculo 					valorComercialVehiculo;
	private List<ValorComercialVehiculo> 			valorComercialVehiculoGrid;
	private ControlArchivoDTO      					controlArchivo;
	private GeneraExcelCargaValorComercialVehiculos hojaCargaValorComercial;
	private Boolean   							    logErrors                  = false;
	private Boolean   							    esMasiva                   = false;
	private InputStream                             plantillaInputStream;
	private String                                  contentType;
	private String                                  fileName;
	private List<Integer>                           ctgAnios                   = new ArrayList<Integer>();
	private List<Integer>                           ctgModelos                 = new ArrayList<Integer>();
	private Map<Integer,String>                     ctgMeses		           = new TreeMap<Integer,String>();
	private TransporteImpresionDTO                  transporteExcel;
	private int                                     cantidadDatosProcesados;
	private Long									keySumasAseguradas;
	private boolean                                 rolUsuario;
	private int 									tamanioGrid;
    private static EstadoMigracion 				    estado                     = EstadoMigracion.APAGADA;
	

	@Autowired
	@Qualifier("valorComercialVehiculoEJB")
	private ValorComercialVehiculoService valorComercialVehiculoService;
	
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;

	@Override
	public void prepare() throws Exception {

		this.ctgModelos = this.valorComercialVehiculoService.obtenerAnioModelos();
		this.ctgAnios = this.valorComercialVehiculoService.obtenerAniosValorComercial();

		// # VALIDACION PERMISOS PARA DAR BAJA REGISTRO
		String[] nombrePermiso = {"Rol_M2_Administrador","Rol_M2_Gerente_Tecnico"};
		this.rolUsuario = this.usuarioService.tienePermisoUsuarioActual(nombrePermiso);
		
	}

	
	@Action(value="mostrar", results={
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculo.jsp"),
			@Result(name = INPUT, location = "/jsp/vehiculo/valorComercialVehiculo.jsp")
		})
	public String mostrar(){
		return SUCCESS;
	}
	
	public void prepareValidaCarga(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(BigDecimal.valueOf( idToControlArchivo) );
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	@Action(value="validaCarga", results={
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculo.jsp"),
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculo.jsp")
		})
	public String validaCarga(){
		
		String mensaje = "";
		
		try{
			
			if (estado.equals(EstadoMigracion.INICIADA)) {
				
				mensaje = "En este momento se esta procesando una carga, espere unos minutos y vuelva a intentar.";
				super.setMensajeError(mensaje);
				
				throw new RuntimeException(mensaje);
			}	
			// # ENCENDER PROCESO PARA QUE NO PUEDA OTRA INSTANCIA ACCESAR AL RECURSO
			estado = EstadoMigracion.INICIADA;
		
			if( this.controlArchivo != null ){
				this.hojaCargaValorComercial = new GeneraExcelCargaValorComercialVehiculos(
															BigDecimal.valueOf(  this.idToControlArchivo ), 
															this.valorComercialVehiculoService, 
															this.entidadService,
															this.usuarioService
												);
			
				this.hojaCargaValorComercial.validaCargaMasiva(BigDecimal.valueOf( this.idToControlArchivo) );
			
				mensaje = "Se procesaron: "+this.hojaCargaValorComercial.getCantidadDatosProcesados()+" datos con éxito. Si aplica, es necesario revise el archivo de Log";
				super.setMensajeExitoPersonalizado(mensaje);
			
				if ( this.hojaCargaValorComercial.isHasLogErrors() ){
					// # SE SETEA LA VARIABLE EN LA VISTA CON VALOR REGRESADO, AL MOMENTO DE VOLVER A PINTAR LA PAGINA SE ESCRIBE EL VALOR Y CON JS SE DESCARGA EL LOG
					this.setLogErrors(this.hojaCargaValorComercial.isHasLogErrors());
				}
			
			}
		
			// # APAGAR EL PROCESO PARA QUE YA OTRA INSTANCIA PUEDA HACER USO
			estado = EstadoMigracion.APAGADA;
		
			esMasiva = true;
		
		} catch (Exception e){
			//System.out.println(e);
			super.setMensajeError("Hubo un error en la carga. Recomendaciones: +revise que sea el formato correcto. +revise que las cabeceras del archivo sean correctas.");
			estado = EstadoMigracion.APAGADA;
		}finally{
			estado = EstadoMigracion.APAGADA;
		}
		
		return SUCCESS;
	}
	
	
	@Action(value="buscar", results={
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculoGrid.jsp"),
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculoGrid.jsp")
		})
	public String buscar(){
		
		this.valorComercialVehiculoGrid = this.valorComercialVehiculoService.findByFilters(this.valorComercialVehiculo);

		tamanioGrid = this.valorComercialVehiculoGrid.size();
		
		if ( tamanioGrid == 0 ){
			super.setMensajeError("No hay datos para la búsqueda realizada");
		}
		
		return SUCCESS;
	}	
	
	
	@Action(value="desactivar", results={
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculo.jsp"),
			@Result(name = SUCCESS, location = "/jsp/vehiculo/valorComercialVehiculo.jsp")
		})
	public String desactivar(){
		
		ValorComercialVehiculo uValorComercialVehiculo = new ValorComercialVehiculo();
		uValorComercialVehiculo.setId(this.keySumasAseguradas);
		
		if ( this.valorComercialVehiculoService.desactivarRegistro(uValorComercialVehiculo) == 1 ){
			super.setMensaje("El registro seleccionado fue desactivado con éxito ");
		}else{
			super.setMensajeError("Lo sentimos, el registro seleccionado no pudo ser desactivado ");
		}

		return SUCCESS;
	}	
	
	
	@Action(value = "descargarLogErrores", 
			results = { @Result(
								name = "success", 
								type = "stream", 
								params = {
								        "contentType", "${contentType}",
								        "inputName", "plantillaInputStream",
								        "contentDisposition", "attachment;filename=\"${fileName}\""
										}
								),
						@Result(
								name = "error", 
								location = "/jsp/errorImpresiones.jsp"
								)
	})	
	public String descargarLogErrores(){
		
		this.hojaCargaValorComercial = new GeneraExcelCargaValorComercialVehiculos();
		try {
			FileInputStream plantillaBytes = this.hojaCargaValorComercial.descargaLogErrores(new BigDecimal(idToControlArchivo));
			
			if(plantillaBytes == null){
				return ERROR;
			}
			
			plantillaInputStream = plantillaBytes;
			contentType = "application/text";
			setFileName("LogErroresCargaMasivaValoresAuto" + this.idToControlArchivo + ".txt");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}
	
	
	/***
	 * Exportar excel
	 * @return
	 */
	@Action(value="exportarExcel",
			results={@Result(
					name=SUCCESS,
					type="stream",
					params={
							"contentType","${transporteExcel.contentType}",
							"inputName","transporteExcel.genericInputStream",
							"contentDisposition",
							"attachment;filename=\"${transporteExcel.fileName}\""
						}
				)
			}
	)
	public String exportarExcel(){
		
		this.valorComercialVehiculoGrid = this.valorComercialVehiculoService.findByFilters(this.valorComercialVehiculo);
		
		ExcelExporter exporter = new ExcelExporter(ValorComercialVehiculo.class);
		this.transporteExcel = exporter.exportXLS(valorComercialVehiculoGrid, "Sumas_Aseguradas");
				
		return SUCCESS;

	}
	

	public ValorComercialVehiculoService getValorComercialVehiculoService() {
		return valorComercialVehiculoService;
	}

	public void setValorComercialVehiculoService(
			ValorComercialVehiculoService valorComercialVehiculoService) {
		this.valorComercialVehiculoService = valorComercialVehiculoService;
	}

	public Long getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(Long idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public ValorComercialVehiculo getValorComercialVehiculo() {
		return valorComercialVehiculo;
	}

	public void setValorComercialVehiculo(
			ValorComercialVehiculo valorComercialVehiculo) {
		this.valorComercialVehiculo = valorComercialVehiculo;
	}

	public Boolean getLogErrors() {
		return logErrors;
	}

	public void setLogErrors(Boolean logErrors) {
		this.logErrors = logErrors;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public List<Integer> getCtgAnios() {
		return ctgAnios;
	}

	public Map<Integer, String> getCtgMeses() {
		return ctgMeses;
	}

	public void setCtgAnios(List<Integer> ctgAnios) {
		this.ctgAnios = ctgAnios;
	}

	public void setCtgMeses(Map<Integer, String> ctgMeses) {
		this.ctgMeses = ctgMeses;
	}

	public List<ValorComercialVehiculo> getValorComercialVehiculoGrid() {
		return valorComercialVehiculoGrid;
	}

	public void setValorComercialVehiculoGrid(
			List<ValorComercialVehiculo> valorComercialVehiculoGrid) {
		this.valorComercialVehiculoGrid = valorComercialVehiculoGrid;
	}

	public TransporteImpresionDTO getTransporteExcel() {
		return transporteExcel;
	}

	public void setTransporteExcel(TransporteImpresionDTO transporteExcel) {
		this.transporteExcel = transporteExcel;
	}

	public int getCantidadDatosProcesados() {
		return cantidadDatosProcesados;
	}

	public void setCantidadDatosProcesados(int cantidadDatosProcesados) {
		this.cantidadDatosProcesados = cantidadDatosProcesados;
	}

	public Long getKeySumasAseguradas() {
		return keySumasAseguradas;
	}

	public void setKeySumasAseguradas(Long keySumasAseguradas) {
		this.keySumasAseguradas = keySumasAseguradas;
	}


	public boolean isRolUsuario() {
		return rolUsuario;
	}


	public void setRolUsuario(boolean rolUsuario) {
		this.rolUsuario = rolUsuario;
	}


	public List<Integer> getCtgModelos() {
		return ctgModelos;
	}


	public void setCtgModelos(List<Integer> ctgModelos) {
		this.ctgModelos = ctgModelos;
	}


	public int getTamanioGrid() {
		return tamanioGrid;
	}


	public void setTamanioGrid(int tamanioGrid) {
		this.tamanioGrid = tamanioGrid;
	}


	public Boolean getEsMasiva() {
		return esMasiva;
	}


	public void setEsMasiva(Boolean esMasiva) {
		this.esMasiva = esMasiva;
	}


}
