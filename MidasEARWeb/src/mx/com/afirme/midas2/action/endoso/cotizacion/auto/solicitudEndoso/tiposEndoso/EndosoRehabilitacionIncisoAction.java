package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionInciso")
public class EndosoRehabilitacionIncisoAction extends EndosoBaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer numeroEndoso;	
	private Date fechaIniVigenciaEndoso;	
	private Long polizaId;
	private String accionEndoso;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS;
	
	private EntidadService entidadService;
	private EndosoService endosoService;	
    private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
    private CalculoService calculoService;

	private BitemporalCotizacion cotizacion;
	private PolizaDTO polizaDTO;
	private List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>(1);
	private String idsSeleccionados;
	private ResumenCostosDTO resumenCostosDTO;
	
	private String actionNameOrigen;
    private String namespaceOrigen;
	
    private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
    
	@Autowired
    @Qualifier("listadoIncisosDinamicoServiceEJB")
	public void setListadoIncisosDinamicoService(
			ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}		
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}	
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@Override
	public void prepare() throws Exception {
		
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));		
	}
	
	@Action
	(value = "mostrarRehabilitacionInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/rehabilitacionInciso/endosoRehabilitacionInciso.jsp"),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","${actionNameOrigen}",
					"namespace","${namespaceOrigen}",
					"idPolizaBusqueda","${polizaId}",								
					"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})
	public String mostrarRehabilitacionInciso() {	
		
		cotizacion = endosoService.getCotizacionEndosoRehabilitacionInciso(new BigDecimal(polizaId), fechaIniVigenciaEndoso,accionEndoso);
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
				
		return SUCCESS;
	}
	
	public void prepareCotizar() 
    {	
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@SuppressWarnings("unchecked")
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarRehabilitacionInciso",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionInciso",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})
	public String cotizar(){
		
		List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>();

		IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
		listaIncisosCotizacion = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, polizaId, fechaIniVigenciaEndoso, true, 0);
		
		
		String[] continuitiesStringIds = null;
		
		if(!getElementosSeleccionados().isEmpty()) {
			continuitiesStringIds = getElementosSeleccionados().split(",");			
		}
		
		String marcaQuitarSeleccion = "-"; //TODO ponerlo como constante en algun lado
		
		for (String continuityStringId : continuitiesStringIds) {
			if(continuityStringId.indexOf(marcaQuitarSeleccion) > -1) continue;
			idsSeleccionados +=continuityStringId + ",";
		}
		
		idsSeleccionados = idsSeleccionados.replaceAll(", ", "");
		if(!idsSeleccionados.isEmpty()) {
			continuitiesStringIds = idsSeleccionados.split(",");			
		}
		
		endosoService.guardaCotizacionEndosoRehabilitacionInciso(continuitiesStringIds, listaIncisosCotizacion, cotizacion);		
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();
		return SUCCESS;
	}
	
	public void prepareEmitir() 
	{		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
						"namespace","${namespaceOrigen}",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() 
	{		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if(endoso!=null)
		{
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}	
		
		return SUCCESS;
	}
	
	@Action
	(value = "cancelar", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
						"namespace","${namespaceOrigen}",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"esRetorno", "${esRetorno}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String cancelar() 
	{		
		this.setEsRetorno(1);
		return SUCCESS;
	}

	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public List<BitemporalInciso> getListaIncisosCotizacion() {
		return listaIncisosCotizacion;
	}

	public void setListaIncisosCotizacion(
			List<BitemporalInciso> listaIncisosCotizacion) {
		this.listaIncisosCotizacion = listaIncisosCotizacion;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

}
