package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto")
public class EndosoInclusionTextoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date fechaIniVigenciaEndoso;
	private BitemporalCotizacion biCotizacion;
	private Collection<BitemporalTexAdicionalCot> textosAdicionales = new ArrayList<BitemporalTexAdicionalCot>(1);
	private BitemporalTexAdicionalCot biTexAdicionalCot = new BitemporalTexAdicionalCot();
	private Integer polizaId;
	private String accionEndoso;
	private String textos;	
	private PolizaDTO polizaDTO;
	
	private String actionNameOrigen;
    private String namespaceOrigen;
	
	private EndosoService endosoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntidadService entidadService;

	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	

	@Override
	public void prepare() throws Exception {
		 polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		 
	}
	
	@Action (value = "mostrarInclusionTexto", results = { 
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionTexto/endosoInclusionTexto.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarInclusionTexto() {		
		biCotizacion = endosoService.getCotizacionEndosoIT(new BigDecimal(polizaId), fechaIniVigenciaEndoso, accionEndoso);
		return SUCCESS;		
	}
	
	
	public void prepareEmitir() {
		biCotizacion = endosoService.prepareCotizacionEndoso(biCotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=ERROR, type="redirectAction", params={"actionName","mostrarInclusionTexto",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso", "${fechaIniVigenciaEndoso}",
					"accionEndoso", "${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje", "${tipoMensaje}"}),
					
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"namespaceOrigen","${namespaceOrigen}",
						"actionNameOrigen","${actionNameOrigen}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() {
		
		// TODO Validar Solicitud de Autorizacion
		/*List<SolicitudExcepcionCotizacion> lista = solicitudAutorizacionService.listarSolicitudesInvalidas(new BigDecimal(biCotizacion.getEntidadContinuity().getNumero()));
		if(!lista.isEmpty()){
			
			setMensajeError("Esta cotizaci\u00F3n no puede ser emitida debido a que existen solicitudes de autorizacion " +
					"pendientes. Favor de revisar la bandeja de solicitudes.");
			
			return ERROR;
		}*/
		
		if (endosoService.isBiTextAdicionalCotAutorizado(biCotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso))) {
			EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(biCotizacion.getContinuity().getId(), 
					TimeUtils.getDateTime(fechaIniVigenciaEndoso), biCotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
			if (endoso != null) {
				setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO + String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
			}
			return SUCCESS;
		} else {
			setMensajeError("Los textos adicionales deben estar autorizados para emitir el endoso. Favor de revisar la bandeja de solicitudes");
			return ERROR;
		}	
		
	}	
		
	@Action (value = "cancelar", results = { 
		@Result(name=SUCCESS, type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"esRetorno", "${esRetorno}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar() {
		//TODO 
		System.out.println("cancelar()");
		this.setEsRetorno(1);
		if(this.getActionNameOrigen() == null || this.getActionNameOrigen().isEmpty()){
			try{
				this.setActionNameOrigen(ServletActionContext.getContext().getSession().get("actionNameOrigen").toString());
				this.setNamespaceOrigen(ServletActionContext.getContext().getSession().get("namespaceOrigen").toString());
			}catch(Exception e){
				this.setActionNameOrigen("listar");
				this.setNamespaceOrigen("/endoso/cotizacion/auto");
			}
		}
		return SUCCESS;
	}
	
	@Action
	(value = "obtenertexAdicionalGrid", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionTexto/endosoInclusionTextoGrid.jsp") })
	public String obtenertexAdicionalGrid() {
		textosAdicionales = endosoService.getLstBiTexAdicionalCot(biCotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		return SUCCESS;
	}

	
	@Action
	(value = "accionTextoAdicional", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionTexto/endosoInclusionTexto.jsp") })
	public String accionTextoAdicional(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		return SUCCESS;
	}
	
	@Action
	(value = "eliminarTextoAdicional", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarInclusionTexto",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${biCotizacion.value.fechaInicioVigencia}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
					@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarInclusionTexto",
							"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto",
							"polizaId","${polizaId}",
							"fechaIniVigenciaEndoso","${biCotizacion.value.fechaInicioVigencia}",
							"namespaceOrigen","${namespaceOrigen}",
							"actionNameOrigen","${actionNameOrigen}",
							"accionEndoso","${accionEndoso}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
					})
	public String eliminarTextoAdicional(){
		if(biTexAdicionalCot != null && biTexAdicionalCot.getContinuity() != null){
			endosoService.deleteBitemporalTexAdicionalCot(biTexAdicionalCot);
		}
		return SUCCESS;
	}
	
	
	public void prepareCotizar() {
		biCotizacion = endosoService.prepareCotizacionEndoso(biCotizacion.getContinuity().getId(), 
															TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarInclusionTexto",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${biCotizacion.value.fechaInicioVigencia}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
					@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarInclusionTexto",
							"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto",
							"polizaId","${polizaId}",
							"fechaIniVigenciaEndoso","${biCotizacion.value.fechaInicioVigencia}",
							"namespaceOrigen","${namespaceOrigen}",
							"actionNameOrigen","${actionNameOrigen}",
							"accionEndoso","${accionEndoso}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
					})
	
	public String cotizar() {	
		
		//Solicitud de Autorizacion
		/*List<ExcepcionSuscripcionReporteDTO> excepcionesList = new ArrayList<ExcepcionSuscripcionReporteDTO>();
		ExcepcionSuscripcionReporteDTO excepcionSuscripcionReporteDTO = new ExcepcionSuscripcionReporteDTO();
		ExcepcionSuscripcion excepcionSuscripcion = entidadService.findById(ExcepcionSuscripcion.class, 7L); //Endoso de Inclusion de Texto
		
		excepcionSuscripcionReporteDTO.setIdExcepcion(excepcionSuscripcion.getId());
		excepcionSuscripcionReporteDTO.setDescripcionExcepcion(excepcionSuscripcion.getDescripcion());
		excepcionSuscripcionReporteDTO.setIdToCotizacion(new BigDecimal(biCotizacion.getEntidadContinuity().getNumero()));		
		excepcionesList.add(excepcionSuscripcionReporteDTO);
		
		Long idSolicitud = null;
		if (!biCotizacion.getContinuity().getCotizaciones().hasRecordsInProcess())
		{
			idSolicitud = autorizacionService.guardarSolicitudDeAutorizacion(new BigDecimal(biCotizacion.getEntidadContinuity().getNumero()), "", 
					new Long(this.getUsuarioActual().getId()), new Long(this.getUsuarioActual().getId()), 
					excepcionSuscripcion.getCveNegocio(), excepcionesList);			
		}*/
		
		Long idSolicitud = null;
		
		idSolicitud = endosoService.generarSolicitudAutorizacion(biCotizacion, new Long(this.getUsuarioActual().getId()), new Long(this.getUsuarioActual().getId()));
		
		endosoService.guardaCotizacionInclusionTexto(textos, biCotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		if (!accionEndoso.equals(TipoAccionDTO.getAltaIncisoEndosoCot())) {
			setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		}		
		
		if(idSolicitud == null)
		{
			setMensajeExito();			
		}
		else
		{
			setMensajeExitoPersonalizado("Acci\u00F3n realizada correctamente. Se ha creado una solicitud de Autorizaci\u00F3n con el Id = " + idSolicitud);			
		}		
		
		return SUCCESS;		
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public Collection<BitemporalTexAdicionalCot> getTextosAdicionales() {
		return textosAdicionales;
	}

	public void setTextosAdicionales(
			Collection<BitemporalTexAdicionalCot> textosAdicionales) {
		this.textosAdicionales = textosAdicionales;
	}

	public BitemporalTexAdicionalCot getBiTexAdicionalCot() {
		return biTexAdicionalCot;
	}

	public void setBiTexAdicionalCot(BitemporalTexAdicionalCot biTexAdicionalCot) {
		this.biTexAdicionalCot = biTexAdicionalCot;
	}

	public Integer getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Integer polizaId) {
		this.polizaId = polizaId;
	}

	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public String getTextos() {
		return textos;
	}

	public void setTextos(String textos) {
		this.textos = textos;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

}
