package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso.EndosoPorPeticionModel.EndosoDummy;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionEndoso")
public class EndosoCancelacionPorPeticionAction extends BaseAction implements Preparable, ModelDriven<EndosoPorPeticionModel>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4435863423727077535L;
	private EndosoPorPeticionModel model;
	private EndosoService endosoService;

	private final String key = "MiListadoEndosos";
	
	@Override
	public EndosoPorPeticionModel getModel() {
		return model;
	}

	private void validaModel(){
		if(model == null)
			model = new EndosoPorPeticionModel();
	}
	public EndosoService getEndosoService() {
		return endosoService;
	}
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	@Action(value = "obtenerEndososPaginado", results= {
		@Result(name =SUCCESS, location="/jsp/suscripcion/solicitud/paginadoGrid.jsp")	
	})  
	
	public String obtenerEndososPaginado(){
		if(getTotalCount() == null)
		{
			setTotalCount(10l);
			setListadoEnSession(null);
		}
		setPosPaginado();
		return SUCCESS;
	}
	
	@Action(value ="obtenerEndosos", results ={
			@Result(name= SUCCESS, location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticionGrid.jsp")
	}
	)
	
	public String obtenerEndosos(){
		validaModel();
		loadEndosos();
		setListadoEnSession(model.getEndosos() );
		persisteEndosos();
		return SUCCESS;
	}
	
	@Action (value = "mostrarCancelacionPorPeticion", results={
		@Result(name=SUCCESS, location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticion.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})	
	public String mostrarCancelacionPorPeticion(){
		try {
			validaModel();
			if(model.getActionNameOrigen() != null || model.getNamespaceOrigen()  != null){
				ServletActionContext.getContext().getSession().put("actionNameOrigenCEP", model.getActionNameOrigen()  );
				ServletActionContext.getContext().getSession().put("namespaceOrigenCEP", model.getNamespaceOrigen());			
			} 
//			ResourceBundle res =  java.util.ResourceBundle.getBundle("globalmessages");
//			res.getString("midas.endosos.cotizacionEndosoListado.vencidas");
///			prepare();	
		} catch (Exception e) {
			this.setMensajeError(e.getMessage());
			return ERROR;
		}	
		
		return SUCCESS;
	}

	public void prepareMostrarCancelacionPorPeticion(){
		validaModel();
		model.setNoEndoso(666l)               ;
		formatNoEndoso();
		model.setNoPoliza(898989l);
		model.setNoPolizaFormateado("66-A7878-89");
		model.setTituloPage( getText("midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPorPeticion.titulo"));
		model.setIsCancelacionWindow( true);
	

		model.setTotalPrimas(new BigDecimal("0"));
		model.setDescuentoComis(new BigDecimal("0"));
		model.setPrimaNeta(new BigDecimal("0"));
		model.setRecargo(new BigDecimal("0"));
		model.setDerechos(new BigDecimal("0"));
		model.setIva(new BigDecimal("0"));
		model.setPrimaTotalPeriodo(new BigDecimal("0"));
	
	}	
	@Action (value = "cancelar", results = { 
			//@Result(name=SUCCESS,type="redirectAction", params={"actionName","${actionNameOrigen}","namespace","${namespaceOrigen}"})
			@Result(name=SUCCESS, location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticion.jsp")		
	})
	public String cancelar() 
	{		
		try {
			if (endosoService!= null) {
				List<EndosoDummy> endodosFallidos = new ArrayList<EndosoDummy>();
				List<EndosoDummy> endosoSel = obtenerEndososSeleccionados();
				List<String> listaMsg = new  ArrayList<String>();
				if(endosoSel != null ){

					for (EndosoDummy dummy : endosoSel) {
						try {

							endosoService.getCotizacionEndosoCEAPDummy(new BigDecimal(dummy.getNoEndoso()), Calendar.getInstance().getTime());

						} catch (NegocioEJBExeption e) {
							listaMsg.add( e.getMessageClean());
							endodosFallidos.add(dummy);
						}catch(Exception ex){
							listaMsg.add(  "Fallo endoso "+dummy.getNoEndoso());
							endodosFallidos.add(dummy);
						}
					}
					//se resetea el valor.
					restauraEndosos(model.getEndosos());
					//procesa los endosos faliidos para que se muestren con el cliente.
					procesaEndososFallidos(endodosFallidos,listaMsg);
				}
			}
		} finally{
			setListadoEnSession(model.getEndosos());
			persisteEndosos();
		}
		return SUCCESS;
	}
	public void prepareCancelar(){
		prepareMostrarCancelacionPorPeticion();

	}
	@Action  
	(value = "emitir", results = { 
			//@Result(name=SUCCESS,type="redirectAction", params={"actionName","${actionNameOrigen}","namespace","${namespaceOrigen}"})
			@Result(name =SUCCESS, location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticion.jsp")
	})
	public String emitir() 
	{		
		try {
			if (endosoService!= null) {
				List<EndosoDummy> endodosFallidos = new ArrayList<EndosoDummy>();
				List<EndosoDummy> endosoSel = obtenerEndososSeleccionados();
				List<String> listaMsg = new  ArrayList<String>();
				if(endosoSel != null ){

					for (EndosoDummy dummy : endosoSel) {
						try {

							endosoService.getCotizacionEndosoCEAPDummy(new BigDecimal(dummy.getNoEndoso()), Calendar.getInstance().getTime());

						} catch (NegocioEJBExeption e) {
							listaMsg.add( e.getMessageClean());
							endodosFallidos.add(dummy);
						}catch(Exception ex){
							listaMsg.add(  "Fallo endoso "+dummy.getNoEndoso());
							endodosFallidos.add(dummy);
						}
					}
					//se resetea el valor.
					restauraEndosos(model.getEndosos());
					//procesa los endosos faliidos para que se muestren con el cliente.
					procesaEndososFallidos(endodosFallidos,listaMsg);
				}
			}
		} finally{
			setListadoEnSession(model.getEndosos());
			persisteEndosos();
		}
		return SUCCESS;
	}
	public void prepareEmitir(){
		prepareMostrarCancelacionPorPeticion();

	}
	@Action  
	(value = "cotizar", results = {
			//@Result(name=SUCCESS,type="redirectAction", params={"actionName","${actionNameRedirect}","namespace","${namespaceRedirect}"})
			@Result(name =SUCCESS, location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticion.jsp")
	})
	public String cotizar() 
	{
		try {
			if (endosoService!= null) {
				List<EndosoDummy> endodosFallidos = new ArrayList<EndosoDummy>();
				List<EndosoDummy> endosoSel = obtenerEndososSeleccionados();
				List<String> listaMsg = new  ArrayList<String>();
				if(endosoSel != null ){

					for (EndosoDummy dummy : endosoSel) {
						try {

							Thread.sleep(5);
							endosoService.getCotizacionEndosoCEAPDummy(new BigDecimal(dummy.getNoEndoso()), Calendar.getInstance().getTime());

						} catch (NegocioEJBExeption e) {
							listaMsg.add( e.getMessageClean());
							endodosFallidos.add(dummy);
						}catch(Exception ex){
							listaMsg.add(  "Fallo endoso "+dummy.getNoEndoso());
							endodosFallidos.add(dummy);
						}
					}
					//se resetea el valor.
					restauraEndosos(model.getEndosos());
					//procesa los endosos faliidos para que se muestren con el cliente.
					procesaEndososFallidos(endodosFallidos,listaMsg);
				}
			}
		} finally{
			setListadoEnSession(model.getEndosos());
			persisteEndosos();
		}
		
		return SUCCESS;
	}
	
	public void prepareCotizar(){
		prepareMostrarCancelacionPorPeticion();
	}
	
	private Boolean redirectPageEndoso = false;
	public Boolean getRedirectPageEndoso() {
		return redirectPageEndoso;
	}
	public void setRedirectPageEndoso(Boolean redirectPageEndoso) {
		this.redirectPageEndoso = redirectPageEndoso;
	}
	@Action  
	(value = "cotizarDummy", results = {
			@Result(name =SUCCESS, location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticionGrid.jsp"),
			@Result(name =ERROR, location="/"),
			@Result(name =LOGIN, location="/")
	})
	public String cotizarDummy() 
	{		
		try {
			if (endosoService != null) {
				List<EndosoDummy> endodosFallidos = new ArrayList<EndosoDummy>();
				List<EndosoDummy> endosoSel = obtenerEndososSeleccionados();
				List<String> listaMsg = new  ArrayList<String>();
				if(endosoSel != null ){

					for (EndosoDummy dummy : endosoSel) {
						try {

							endosoService.getCotizacionEndosoCEAPDummy(new BigDecimal(dummy.getNoEndoso()), Calendar.getInstance().getTime());

						} catch (NegocioEJBExeption e) {
							listaMsg.add( e.getMessageClean());
							endodosFallidos.add(dummy);
						}catch(Exception ex){
							listaMsg.add(  "Fallo endoso "+dummy.getNoEndoso());
							endodosFallidos.add(dummy);
						}
					}
					//se resetea el valor.
					restauraEndosos(model.getEndosos());
					//procesa los endosos faliidos para que se muestren con el cliente.
					procesaEndososFallidos(endodosFallidos,listaMsg);
					redirectPageEndoso = listaMsg.size()==0;
					super.setNextFunction("evalRedirectPageEndoso();");
				}
			}
		} finally{
			setListadoEnSession(model.getEndosos());
			persisteEndosos();
		}
		return SUCCESS;
	}
	public void prepareCotizarDummy(){
		prepareMostrarCancelacionPorPeticion();

	}

	private List<EndosoDummy> obtenerEndososSeleccionados(){
		List<EndosoDummy> lista = new ArrayList<EndosoDummy>();

		loadEndosos();
		String[] idsRes = null;
		if(model.getEndosoIdsApply() != null && model.getEndosoIdsApply().length() > 0){
			if(model.getEndosoIdsApply().contains(","))
				idsRes = this.model.getEndosoIdsApply().split(",");
			else
				idsRes = new String[]{model.getEndosoIdsApply()};

			List<Long> ids = model.getEndosos().get(0).obtenerNoEndoso(idsRes);
			for (Long id : ids) {
				for (EndosoDummy endoso : model.getEndosos()) {
					if(endoso.getNoEndoso().longValue() ==  id.longValue()){
						lista.add(endoso);
						break;
					}
				}
			}
		}
		return lista;
	}
	
	
	
	private void updateDatosParaRegreso(){
		model.setActionNameOrigen( ServletActionContext.getContext().getSession().get("actionNameOrigenCEP").toString()) ;
		model.setNamespaceOrigen(ServletActionContext.getContext().getSession().get("namespaceOrigenCEP").toString());
		// se establece a nulo.
		ServletActionContext.getContext().getSession().put("actionNameOrigenCEP", null);
		ServletActionContext.getContext().getSession().put("namespaceOrigenCEP", null);
		
	}
	
	@Action  
	(value = "consultarEndoso", results = { 
			@Result(name=SUCCESS,location="/jsp/endosos/solicitudEndoso/tiposEndoso/cancelarEndoso/endosoCancelacionPorPeticion.jsp")})
	public String consultarEndoso() 
	{		
		
		if (model.getNoEndoso() == 5) {
			formatNoEndoso();    
			model.setEndosos( (List<EndosoDummy>) ServletActionContext.getContext().getSession().get(LISTADO_PAGINADO_ACTUAL) );
			return ERROR ;
		}
		
		return SUCCESS;
	}
	public void prepareConsultarEndoso(){
		prepareMostrarCancelacionPorPeticion();
	}
	
	
	@Override
	public void prepare() throws Exception {
		//model = new EndosoPorPeticionModel();
	}
//	public void prepareMostrarCancelacionPorPeticion() throws Exception {
//		noEndoso = 666l   ;
//		formatNoEndoso();
//		loadEndosos();
//	}
	private void formatNoEndoso(){
		DecimalFormat format = new DecimalFormat("000000");
		model.setNoEndosoFormateado(format.format(model.getNoEndoso()));
	}
	
	private void persisteEndosos(){
		ServletActionContext.getRequest().getSession().setAttribute(key,model.getEndosos());
	}
	private void loadEndosos(){
		Object obj = ServletActionContext.getRequest().getSession().getAttribute(key);
		if(obj == null){
			model.loadEndosos("Rehabilitación");
			persisteEndosos();
		}else{
			model.setEndosos((List<EndosoDummy>)obj);
		}
	}
	public void procesaEndososFallidos(List<EndosoDummy> endodosFallidos,List<String> listaMensajes){
		if (endodosFallidos.size() > 0) {
			super.setMensajeListaPersonalizado(
					getText("midas.endosos.solicitudEndoso.tiposEndoso.cancelacionPorPeticion.mensajeTitulo"),  
					listaMensajes, BaseAction.TIPO_MENSAJE_INFORMACION);
			super.setNextFunction("obtenerEndosos();");

			for (EndosoDummy dummy : endodosFallidos) {
				dummy.setStatus(false);
			}
		}

	}
	public void restauraEndosos(List<EndosoDummy> listaEndosos){
		for (EndosoDummy dummy : listaEndosos) {
			dummy.setStatus(true);
			dummy.setSelected(false);
		}
	}


	
}


