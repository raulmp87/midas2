package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas2.action.BaseAction;

public class EndosoBaseAction extends BaseAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String obtenerIdsSeleccionados(String[] continuitiesStringIds, String idsSeleccionados) {
		StringBuilder ids = new StringBuilder(idsSeleccionados);
		for (String continuityStringId : continuitiesStringIds) {
			if(continuityStringId.indexOf(EndosoDTO.MARCAQUITARSELECCION) > -1) continue;
			ids.append(continuityStringId).append(",");
		}
		return ids.toString();
	}

}
