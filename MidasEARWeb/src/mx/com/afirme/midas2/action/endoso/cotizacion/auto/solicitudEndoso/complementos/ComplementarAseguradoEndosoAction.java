package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.complementos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarAsegurado")
public class ComplementarAseguradoEndosoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String TIPO_CUENTA_DEBITO = "DEBITO";
	private static final String TIPO_CUENTA_CUENTA_CLABE = "CUENTA CLABE";
	private static final String TIPO_CUENTA_CUENTA_BANCARIA = "CUENTA BANCARIA";
	
	private Long incisoContinuityId;
	
	private Date validoEn;
	
	private BitemporalCotizacion biCotizacion;
	private BitemporalInciso biInciso;	
	private BitemporalAutoInciso biAutoInciso;	
	
	private IncisoViewService incisoService;	
	private EntidadBitemporalService entidadBitemporalService;
	private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	private BancoEmisorFacadeRemote bancoEmisorFacadeRemote;
	
	private String origen;
	
	private Map<BigDecimal, String> clientesCobro = new HashMap<BigDecimal, String>();
	private List<MedioPagoDTO> medioPagoDTOs = new ArrayList<MedioPagoDTO>(1);
	private Map<Long, String> conductosDeCobro = new HashMap<Long, String>();
	private List<BancoEmisorDTO> bancosCobro = new ArrayList<BancoEmisorDTO>();
	
	private BigDecimal idClienteCob;
	private BigDecimal idMedioPago;
	private Long idConductoCobroCliente;
	private String idBancoCobro;
	private String tipoTarjeta;
	private String numeroTarjeta;
	private String codigoSeguridad;
	private String fechaVencimiento;
	private String conductoCobroNuevo;
	
	private CobranzaIncisoService cobranzaIncisoService;
	private ListadoService listadoService;
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	
	private Integer radioAsegurado;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}	
	
	@Autowired
	@Qualifier("cobranzaIncisoServiceEJB")
	public void setCobranzaIncisoService(CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}

	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Autowired
	@Qualifier("bancoEmisorFacadeRemoteEJB")
	public void setBancoEmisorFacade(BancoEmisorFacadeRemote bancoEmisorFacade) {
		this.bancoEmisorFacade = bancoEmisorFacade;
	}
	
	@Override
	public void prepare() throws Exception {
	}
	
	public void prepareMostrarVentanaMedioPago() throws Exception {
		biInciso = incisoService.getIncisoInProcess(incisoContinuityId, validoEn);
		biAutoInciso = incisoService.getAutoIncisoInProcess(biInciso, validoEn);
		biCotizacion = incisoService.getCotizacionInProcess(biInciso, validoEn);				
				
		initVentanaMedioPago();
	}
	
	public void initVentanaMedioPago(){
		if(biInciso != null){
			idClienteCob = biInciso.getValue().getIdClienteCob();
			idMedioPago = biInciso.getValue().getIdMedioPago();
			idConductoCobroCliente = biInciso.getValue().getIdConductoCobroCliente();
			
			numeroTarjeta 		= (biInciso.getValue().getNumeroTarjetaClave()!=null)?bancoEmisorFacadeRemote.desEncriptaDatos(biInciso.getValue().getNumeroTarjetaClave()).trim():"";
			codigoSeguridad 	= (biInciso.getValue().getCodigoSeguridad()!=null)?bancoEmisorFacadeRemote.desEncriptaDatos(biInciso.getValue().getCodigoSeguridad()).trim():"";
			fechaVencimiento	= (biInciso.getValue().getFechaVencimiento()!=null)?bancoEmisorFacadeRemote.desEncriptaDatos(biInciso.getValue().getFechaVencimiento()).trim():"";			
		}
		if(idClienteCob != null && idMedioPago != null){
			conductosDeCobro = listadoService.getConductosCobro(idClienteCob.longValue(), idMedioPago.intValue());
		}
		if(biCotizacion != null && biCotizacion.getValue().getPersonaContratanteId() != null){
			BigDecimal idPersonaContratante = new BigDecimal(biCotizacion.getValue().getPersonaContratanteId());
			BigDecimal idPersonaAsegurado = null;
			if(biAutoInciso != null && biAutoInciso.getValue().getPersonaAseguradoId() != null){
				idPersonaAsegurado = new BigDecimal(biAutoInciso.getValue().getPersonaAseguradoId());
			}
			clientesCobro = cobranzaIncisoService.getClientesCobro(idPersonaContratante, idPersonaAsegurado);
		}else{
			clientesCobro = new HashMap<BigDecimal, String>();
		}
		try{
			medioPagoDTOs = cobranzaIncisoService.getMedioPagosView();
			bancosCobro = listadoService.getBancosList();
		}catch(Exception e){
		}
	}
	
	@Action
	(value = "mostrarVentanaMedioPago", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarMedioPago.jsp") })
	public String mostrarVentanaMedioPago() {		
		return SUCCESS;
	}
	
	
	public void prepareActualizarDatosMedioPago() throws Exception {
		biInciso = incisoService.getIncisoInProcess(incisoContinuityId, validoEn);
		biAutoInciso = incisoService.getAutoIncisoInProcess(biInciso, validoEn);
		biCotizacion = incisoService.getCotizacionInProcess(biInciso, validoEn);
	}
	
	@Action
	(value = "actualizarDatosMedioPago", results = {					
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarMedioPago.jsp") })	
	public String actualizarDatosMedioPago() throws Exception {

		if (idMedioPago == null) {
			super.setMensajeError("Seleccione un medio de Pago");
			return SUCCESS;
		}

		try{
		if (idClienteCob != null && idConductoCobroCliente != null) {
			
			biInciso.getValue().setIdClienteCob(idClienteCob);
			biInciso.getValue().setIdConductoCobroCliente(idConductoCobroCliente);
			biInciso.getValue().setIdMedioPago(idMedioPago);
			
			if(conductoCobroNuevo.equals("Agregar nuevo...")){
				if (idMedioPago.intValue() == MedioPagoDTO.MEDIO_PAGO_TARJETA) {
					biInciso.getValue().setConductoCobro(MedioPagoDTO.PAGO_TARJETA);
					biInciso.getValue().setInstitucionBancaria(idBancoCobro);
					biInciso.getValue().setTipoTarjeta(tipoTarjeta);
					biInciso.getValue().setNumeroTarjetaClave(bancoEmisorFacadeRemote.encriptaDatos(numeroTarjeta));
					biInciso.getValue().setCodigoSeguridad(bancoEmisorFacadeRemote.encriptaDatos(codigoSeguridad));
					biInciso.getValue().setFechaVencimiento(bancoEmisorFacadeRemote.encriptaDatos(fechaVencimiento));
				} else if (idMedioPago.intValue() == MedioPagoDTO.MEDIO_PAGO_DOMICILIADA|| idMedioPago.intValue() == MedioPagoDTO.MEDIO_PAGO_CUENTAAFIRME) {
					if (idMedioPago.intValue() == MedioPagoDTO.MEDIO_PAGO_DOMICILIADA) {
						biInciso.getValue().setConductoCobro(MedioPagoDTO.PAGO_DOMICILIADA);
					} else {
						biInciso.getValue().setConductoCobro(MedioPagoDTO.PAGO_CUENTAAFIRME);
					}
					
					if (numeroTarjeta.length()==16){
						tipoTarjeta = TIPO_CUENTA_DEBITO;
					}else if(numeroTarjeta.length()==18){
						tipoTarjeta = TIPO_CUENTA_CUENTA_CLABE;
					}else{
						tipoTarjeta = TIPO_CUENTA_CUENTA_BANCARIA;
					}
					
					biInciso.getValue().setInstitucionBancaria(idBancoCobro);
					biInciso.getValue().setTipoTarjeta(tipoTarjeta);
					biInciso.getValue().setNumeroTarjetaClave(bancoEmisorFacadeRemote.encriptaDatos(numeroTarjeta));
				}
			}
		} else if (idMedioPago != null&& idMedioPago.intValue() == MedioPagoDTO.MEDIO_PAGO_AGENTE.intValue()) {
			biInciso.getValue().setIdClienteCob(null);
			biInciso.getValue().setIdConductoCobroCliente(null);
			biInciso.getValue().setIdMedioPago(idMedioPago);
		}

		entidadBitemporalService.saveInProcess(biInciso,TimeUtils.getDateTime(validoEn));

		if(conductoCobroNuevo.equals("Agregar nuevo...")){
			BigDecimal idPersonaContratante = new BigDecimal(biCotizacion.getValue().getPersonaContratanteId());
			ClienteDTO cliente = null;
			ClienteGenericoDTO cuenta = null;
			if (idPersonaContratante != null) {
				cliente = clienteFacadeRemote.findById(idPersonaContratante,"nombreUsuario");
			}
			if (cliente != null) {
				if (biInciso.getValue().getIdMedioPago() != null&& biInciso.getValue().getConductoCobro() != null&& biInciso.getValue().getInstitucionBancaria() != null) {
					cuenta = clienteMigracion(biCotizacion, biInciso.getValue(),cliente);
					clienteFacadeRemote.guardarDatosCobranza(cuenta, usuarioService.getUsuarioActual().getNombreUsuario(), true);
				}
			}
		}
		
		super.setMensajeExito();

		}catch(Exception e) {
			
			super.setMensajeError((e.getMessage()!=null)?e.getMessage():"Error al guardar Conducto de Cobro");
		}
		setNextFunction("closeVentanaMedioPago()");
		initVentanaMedioPago();
		return SUCCESS;
	}

	private ClienteGenericoDTO clienteMigracion(BitemporalCotizacion biCotizacion,Inciso inciso, ClienteDTO clienteDto) {
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		cliente.setIdCliente(inciso.getIdClienteCob());
		cliente.setIdConductoCobranza(inciso.getIdConductoCobroCliente());
		cliente.setNombreTarjetaHabienteCobranza(biCotizacion.getValue().getNombreContratante());
		cliente.setEmailCobranza("");
		cliente.setTelefonoCobranza("");
		cliente.setNombreCalleCobranza(clienteDto.getNombreCalle());
		cliente.setCodigoPostalCobranza(clienteDto.getCodigoPostal());
		cliente.setNombreColoniaCobranza(clienteDto.getNombreColonia());
		cliente.setIdTipoConductoCobro(inciso.getIdMedioPago().intValue());
		
		List<BancoEmisorDTO> bancos = bancoEmisorFacade.findAll();
		Integer idBancoCobranza = null;
		for(BancoEmisorDTO banco: bancos){
			if(inciso.getInstitucionBancaria().equals(banco.getNombreBanco())){
				idBancoCobranza =  banco.getIdBanco();
			}
		}
		cliente.setIdBancoCobranza(new BigDecimal(idBancoCobranza));
		cliente.setIdTipoTarjetaCobranza(inciso.getTipoTarjeta());			
		cliente.setNumeroTarjetaCobranza(bancoEmisorFacade.desEncriptaDatos(inciso.getNumeroTarjetaClave()));
		//Concidicon para cuando el conducto de cobro es del tipo tarjeta de credito
		if (inciso.getCodigoSeguridad() != null&&inciso.getFechaVencimiento()!=null) {
			cliente.setCodigoSeguridadCobranza(bancoEmisorFacade.desEncriptaDatos(inciso.getCodigoSeguridad()));
			cliente.setFechaVencimientoTarjetaCobranza(bancoEmisorFacade.desEncriptaDatos(inciso.getFechaVencimiento()).replace("/", ""));
		}	
		cliente.setTipoPromocion("");
		cliente.setDiaPagoTarjetaCobranza(new BigDecimal(0));				
		cliente.setRfcCobranza(clienteDto.getCodigoRFC());
		
		return cliente;
	}
	
	
	public void prepareMostrarVentanaAsegurado() throws Exception {
		biInciso = incisoService.getIncisoInProcess(incisoContinuityId, validoEn);
		biAutoInciso = incisoService.getAutoIncisoInProcess(biInciso, validoEn);
		biCotizacion = incisoService.getCotizacionInProcess(biInciso, validoEn);
		if(biCotizacion.getValue().getPersonaContratanteId() != null){
			radioAsegurado = 1;
		}
	}
	
	@Action
	(value = "mostrarVentanaAsegurado", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarAsegurado.jsp") })
	public String mostrarVentanaAsegurado() {
		origen = "2";		
		return SUCCESS;
	}
	
	
	public void prepareActualizarDatosAsegurados() throws Exception {
		biInciso = incisoService.getIncisoInProcess(incisoContinuityId, validoEn);
		biAutoInciso = incisoService.getAutoIncisoInProcess(biInciso, validoEn);
		biCotizacion = incisoService.getCotizacionInProcess(biInciso, validoEn);
	}
	
	@Action
	(value = "actualizarDatosAsegurados", results = {					
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarAsegurado.jsp") })
	
	public String actualizarDatosAsegurados() throws Exception {
				
		ClienteDTO cliente = null;
		
		if(biAutoInciso != null && biAutoInciso.getValue() != null && biAutoInciso.getValue().getNombreAsegurado() != null){
			String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getNombreAsegurado());
			biAutoInciso.getValue().setNombreAsegurado(valor);
		}
		
		if (radioAsegurado == 1 && biCotizacion.getValue().getPersonaContratanteId() == 0) {
			super.setMensajeError("No existe un Contratante");
			return SUCCESS;
		}
		switch (radioAsegurado) {
		case 1:
			if (biCotizacion.getValue().getPersonaContratanteId() != 0) {
				cliente = clienteFacadeRemote.findById(BigDecimal.valueOf(biCotizacion.getValue().getPersonaContratanteId()),
																		"nombreUsuario");
			}
			if (cliente != null) {
				biAutoInciso.getValue().setPersonaAseguradoId(cliente.getIdCliente().longValue());
				biAutoInciso.getValue().setNombreAsegurado(cliente.obtenerNombreCliente());			
			}
			
			entidadBitemporalService.saveInProcess(biAutoInciso, TimeUtils.getDateTime(validoEn));

			super.setMensajeExito();
			setNextFunction("closeVentanaAseguradoEndosoCambioDatos()");
			return SUCCESS;
			
		case 2:
			biAutoInciso.getValue().setPersonaAseguradoId(null);
			if(biAutoInciso.getValue().getNombreAsegurado() != null){
				String nombre = biAutoInciso.getValue().getNombreAsegurado().toUpperCase();
				biAutoInciso.getValue().setNombreAsegurado(nombre);
			}
			entidadBitemporalService.saveInProcess(biAutoInciso, TimeUtils.getDateTime(validoEn));
			
			super.setMensajeExito();
			setNextFunction("closeVentanaAseguradoEndosoCambioDatos();");
			return SUCCESS;
			
		case 3:
			if (biAutoInciso.getValue().getPersonaAseguradoId() != 0) {
				cliente = clienteFacadeRemote.findById(new BigDecimal(biAutoInciso.getValue().getPersonaAseguradoId()), "nombreUsuario");
				
			}
			
			if (cliente != null) {
				biAutoInciso.getValue().setPersonaAseguradoId(cliente.getIdCliente().longValue());
				biAutoInciso.getValue().setNombreAsegurado(cliente.obtenerNombreCliente());
			}
			entidadBitemporalService.saveInProcess(biAutoInciso, TimeUtils.getDateTime(validoEn));
			
			super.setMensajeExito();
			setNextFunction("closeVentanaAseguradoEndosoCambioDatos()");
			return SUCCESS;
		}
		return SUCCESS;
	}
	
	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}	

	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public BitemporalInciso getBiInciso() {
		return biInciso;
	}

	public void setBiInciso(BitemporalInciso biInciso) {
		this.biInciso = biInciso;
	}

	public BitemporalAutoInciso getBiAutoInciso() {
		return biAutoInciso;
	}

	public void setBiAutoInciso(BitemporalAutoInciso biAutoInciso) {
		this.biAutoInciso = biAutoInciso;
	}

	public Integer getRadioAsegurado() {
		return radioAsegurado;
	}

	public void setRadioAsegurado(Integer radioAsegurado) {
		this.radioAsegurado = radioAsegurado;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public void setClientesCobro(Map<BigDecimal, String> clientesCobro) {
		this.clientesCobro = clientesCobro;
	}

	public Map<BigDecimal, String> getClientesCobro() {
		return clientesCobro;
	}

	public void setMedioPagoDTOs(List<MedioPagoDTO> medioPagoDTOs) {
		this.medioPagoDTOs = medioPagoDTOs;
	}

	public List<MedioPagoDTO> getMedioPagoDTOs() {
		return medioPagoDTOs;
	}

	public void setConductosDeCobro(Map<Long, String> conductosDeCobro) {
		this.conductosDeCobro = conductosDeCobro;
	}

	public Map<Long, String> getConductosDeCobro() {
		return conductosDeCobro;
	}

	public void setIdClienteCob(BigDecimal idClienteCob) {
		this.idClienteCob = idClienteCob;
	}

	public BigDecimal getIdClienteCob() {
		return idClienteCob;
	}

	public void setIdMedioPago(BigDecimal idMedioPago) {
		this.idMedioPago = idMedioPago;
	}

	public BigDecimal getIdMedioPago() {
		return idMedioPago;
	}

	public void setIdConductoCobroCliente(Long idConductoCobroCliente) {
		this.idConductoCobroCliente = idConductoCobroCliente;
	}

	public Long getIdConductoCobroCliente() {
		return idConductoCobroCliente;
	}

	public List<BancoEmisorDTO> getBancosCobro() {
		return bancosCobro;
	}

	public void setBancosCobro(List<BancoEmisorDTO> bancosCobro) {
		this.bancosCobro = bancosCobro;
	}

	public String getIdBancoCobro() {
		return idBancoCobro;
	}

	public void setIdBancoCobro(String idBancoCobro) {
		this.idBancoCobro = idBancoCobro;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	
	public String getConductoCobroNuevo() {
		return conductoCobroNuevo;
	}

	public void setConductoCobroNuevo(String conductoCobroNuevo) {
		this.conductoCobroNuevo = conductoCobroNuevo;
	}	
}
