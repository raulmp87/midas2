package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.complementos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarcondicionespecial")
public class ComplementarCondicionEspEndosoAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 1L;

	private Long incisoContinuityId;
	
	private Long condicionId;
	
	private Date validoEn;
	
	private BitemporalCotizacion biCotizacion;
	
	private BitemporalInciso biInciso;	
	
	private CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO;
	
	private String nombreCondicion;
	
	private String accionEndoso;
	
	private List<CondicionEspecialBitemporalDTO> condicionesAsociadas = new ArrayList<CondicionEspecialBitemporalDTO>();
	
	private List<CondicionEspecialBitemporalDTO> condicionesDisponibles = new ArrayList<CondicionEspecialBitemporalDTO>();
	
	private List<CondicionEspecial> resultadosCondicion;
	
	
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	private IncisoViewService incisoService;	
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	private EntidadBitemporalService entidadBitemporalService;	
	
	@Autowired
	@Qualifier("condicionEspecialBitemporalServiceEJB")
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;	
	

	@Override
	public void prepare() throws Exception {
		if (TipoAccionDTO.getEditar().equals(accionEndoso) || TipoAccionDTO.getVer().equalsIgnoreCase(accionEndoso)) {
			biInciso = incisoService.getIncisoInProcess(incisoContinuityId, validoEn);
		} else if (TipoAccionDTO.getConsultarEndosoCot().equals(accionEndoso)) {
			biInciso = incisoService.getInciso(incisoContinuityId, validoEn);
		}
	}	
	
	@Action (value = "mostrarCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarCondicionEspecial.jsp") })
	public String mostrarCondiciones() {	
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerCondicionesAsociadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarCondicionEspecialAsociadas.jsp") })
	public String obtenerCondicionesAsociadas() {
		
		condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesIncisoAsociadas(incisoContinuityId, validoEn);
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerCondicionesDisponibles", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarCondicionEspecialDisponibles.jsp") })
	public String obtenerCondicionesDisponibles() {
		
		condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesDisponiblesInciso(condicionesDisponibles,incisoContinuityId, validoEn);
		
		return SUCCESS;
	}
	
	@Action
	(value = "relacionarCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp") })
	public String relacionarCondiciones(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		
		if (accion.equals(ROW_INSERTED)) {
			condicionEspecialBitemporalService.guardarCondicionInciso(incisoContinuityId, condicionEspecialBitemporalDTO, validoEn);
		} else if (accion.equals(ROW_DELETED)) {
			condicionEspecialBitemporalService.eliminarCondicionInciso(condicionEspecialBitemporalDTO, validoEn);
		}
		
		return SUCCESS;
	}

	@Action (value = "buscarCondicion", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarCondicionEspecialDisponibles.jsp") })
	public String buscarCondicion() {	

		condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesIncisoNombreCodigo(nombreCondicion, incisoContinuityId, validoEn);
		
		return SUCCESS;
		
	}
	
	@Action (value = "asociarTodasCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarCondicionEspecial.jsp") })
	public String asociarTodasCondiciones() {	

		condicionEspecialBitemporalService.asociarCondicionesInciso(incisoContinuityId, validoEn, nombreCondicion);
		
		return SUCCESS;
		
	}
	
	@Action (value = "eliminarTodasCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/complementarCondicionEspecial.jsp") })
	public String eliminarTodasCondiciones() {	

		condicionEspecialBitemporalService.eliminarCondicionesInciso(incisoContinuityId, validoEn);
		
		return SUCCESS;
		
	}

	public Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	public void setIncisoContinuityId(Long incisoContinuityId) {
		this.incisoContinuityId = incisoContinuityId;
	}
	
	public Long getCondicionId() {
		return condicionId;
	}

	public void setCondicionId(Long condicionId) {
		this.condicionId = condicionId;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public BitemporalInciso getBiInciso() {
		return biInciso;
	}

	public void setBiInciso(BitemporalInciso biInciso) {
		this.biInciso = biInciso;
	}

	public CondicionEspecialBitemporalDTO getCondicionEspecialBitemporalDTO() {
		return condicionEspecialBitemporalDTO;
	}

	public void setCondicionEspecialBitemporalDTO(
			CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO) {
		this.condicionEspecialBitemporalDTO = condicionEspecialBitemporalDTO;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}
	
	public List<CondicionEspecialBitemporalDTO> getCondicionesAsociadas() {
		return condicionesAsociadas;
	}

	public void setCondicionesAsociadas(
			List<CondicionEspecialBitemporalDTO> condicionesAsociadas) {
		this.condicionesAsociadas = condicionesAsociadas;
	}

	public List<CondicionEspecialBitemporalDTO> getCondicionesDisponibles() {
		return condicionesDisponibles;
	}

	public void setCondicionesDisponibles(
			List<CondicionEspecialBitemporalDTO> condicionesDisponibles) {
		this.condicionesDisponibles = condicionesDisponibles;
	}

	public IncisoViewService getIncisoService() {
		return incisoService;
	}

	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}

	public EntidadBitemporalService getEntidadBitemporalService() {
		return entidadBitemporalService;
	}

	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	public CondicionEspecialBitemporalService getCondicionEspecialBitemporalService() {
		return condicionEspecialBitemporalService;
	}

	public void setCondicionEspecialBitemporalService(
			CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}

	public String getNombreCondicion() {
		return nombreCondicion;
	}

	public void setNombreCondicion(String nombreCondicion) {
		this.nombreCondicion = nombreCondicion;
	}

	public List<CondicionEspecial> getResultadosCondicion() {
		return resultadosCondicion;
	}

	public void setResultadosCondicion(List<CondicionEspecial> resultadosCondicion) {
		this.resultadosCondicion = resultadosCondicion;
	}
	
	
	
	
}
