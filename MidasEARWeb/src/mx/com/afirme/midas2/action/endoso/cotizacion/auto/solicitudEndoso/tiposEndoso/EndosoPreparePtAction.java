package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/preparePT")
public class EndosoPreparePtAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6795537364982720622L;

	
	private String idMidas;
	private String pllaveFiscal;
	
	private BigDecimal pId_Cotizacion;
	private BigDecimal pId_Solicitud_Canc;
	
	private BigDecimal pIdVersionPol;
	private BigDecimal pIdVersionPolCanc;
	private String pCveTEndoso;
	private String pCveLTFolioCE;

    private BigDecimal pnum_poliza;
    private BigDecimal pnum_renov; 
    private BigDecimal pnum_endoso; 

    private BigDecimal pId_Recibo;        
    private String     pId_RecibosAjusta;
    
    private BigDecimal varPrimaNeta;      
    private BigDecimal varBonifComis;       
    private BigDecimal varRecargos;   
    private BigDecimal varBonifComisRPF;    
    private BigDecimal varDerechos;         
    private BigDecimal varIva;      
    private BigDecimal varPrimaTotal;
    private BigDecimal varComisPN;        
    private BigDecimal varComisRPF;
    

    
    private EndosoService endosoService;
    
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}

	
	@Action(value = "mostrarPreparePT", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String mostrarPreparePT() {
		return SUCCESS;
	}
	
	@Action(value = "bloquearMigracionSeycos", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String bloquearMigracionSeycos() {
		endosoService.bloquearMigracionSeycos(idMidas);
		return SUCCESS;
	}
	
	@Action(value = "igualarEndosoMidas", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String igualarEndosoMidas() {
		endosoService.igualarEndosoMidas(pnum_poliza, pnum_renov, pnum_endoso, 
				varPrimaNeta, varBonifComis, varRecargos, varBonifComisRPF, 
				varDerechos, varIva, varPrimaTotal, varComisPN, varComisRPF);
		return SUCCESS;
	}

	@Action(value = "migrarEndosoMidas", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String migrarEndosoMidas() {
		endosoService.migrarEndosoMidas(idMidas);
		return SUCCESS;
	}

	@Action(value = "regeneraPrimaRecibo", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String regeneraPrimaRecibo() {
		endosoService.regeneraPrimaRecibo(pId_Recibo, 
				varPrimaNeta, varBonifComis, varRecargos, varBonifComisRPF, 
				varDerechos, varIva, varPrimaTotal, varComisPN, varComisRPF);
		return SUCCESS;
	}
	
	@Action(value = "regeneraPrimaReciboLista", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String regeneraPrimaReciboLista() {
		
		String[] recibos = pId_RecibosAjusta.split(",");
		
		for(int i = 0; i < recibos.length; i++) {
			try{
				endosoService.regeneraPrimaRecibo(new BigDecimal(recibos[i].trim()), 
						varPrimaNeta, varBonifComis, varRecargos, varBonifComisRPF, 
						varDerechos, varIva, varPrimaTotal, varComisPN, varComisRPF);
			}catch(Exception e){
				e.printStackTrace();
			}
		};
		return SUCCESS;
	}
	
	@Action(value = "regeneraRecibo", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String regeneraRecibo() {
		endosoService.regeneraRecibo(pId_Recibo);
		return SUCCESS;
	}
	
	@Action(value = "setLlaveFiscal", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String setLlaveFiscal() {
		String[] identificador = idMidas.split(",");		
		endosoService.setLlaveFiscal(new BigDecimal(identificador[0]), new BigDecimal(identificador[1]), pllaveFiscal);
		return SUCCESS;
	}

	@Action(value = "reprocesaEndosoSeycos", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String reprocesaEndosoSeycos() {
		endosoService.reprocesaEndosoSeycos(pId_Cotizacion, pId_Solicitud_Canc);
		return SUCCESS;
	}
	
	@Action(value = "migrarEndosoCFP", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String migrarEndosoCFP() {
		endosoService.migrarEndosoCFP(idMidas);
		return SUCCESS;
	}
	
	@Action(value = "generaReciboEndoso", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/preparePT/endosoPreparePT.jsp")
		})
	public String generaReciboEndoso() {
		endosoService.generaReciboEndoso(pId_Cotizacion, pId_Solicitud_Canc,  pIdVersionPol, pIdVersionPolCanc,
				pCveTEndoso, pCveLTFolioCE);
		return SUCCESS;
	}
	
	public void setIdMidas(String idMidas) {
		this.idMidas = idMidas;
	}


	public String getIdMidas() {
		return idMidas;
	}
	
	
	public String getPllaveFiscal() {
		return pllaveFiscal;
	}

	public void setPllaveFiscal(String pllaveFiscal) {
		this.pllaveFiscal = pllaveFiscal;
	}

	public BigDecimal getPId_Cotizacion() {
		return pId_Cotizacion;
	}

	public void setPId_Cotizacion(BigDecimal pId_Cotizacion) {
		this.pId_Cotizacion = pId_Cotizacion;
	}

	public BigDecimal getPId_Solicitud_Canc() {
		return pId_Solicitud_Canc;
	}

	public void setPId_Solicitud_Canc(BigDecimal pId_Solicitud_Canc) {
		this.pId_Solicitud_Canc = pId_Solicitud_Canc;
	}	


	public void setPnum_poliza(BigDecimal pnum_poliza) {
		this.pnum_poliza = pnum_poliza;
	}


	public BigDecimal getPnum_poliza() {
		return pnum_poliza;
	}


	public void setPnum_renov(BigDecimal pnum_renov) {
		this.pnum_renov = pnum_renov;
	}


	public BigDecimal getPnum_renov() {
		return pnum_renov;
	}


	public void setPnum_endoso(BigDecimal pnum_endoso) {
		this.pnum_endoso = pnum_endoso;
	}


	public BigDecimal getPnum_endoso() {
		return pnum_endoso;
	}


	public void setVarPrimaNeta(BigDecimal varPrimaNeta) {
		this.varPrimaNeta = varPrimaNeta;
	}


	public BigDecimal getVarPrimaNeta() {
		return varPrimaNeta;
	}


	public void setVarBonifComis(BigDecimal varBonifComis) {
		this.varBonifComis = varBonifComis;
	}


	public BigDecimal getVarBonifComis() {
		return varBonifComis;
	}


	public void setVarRecargos(BigDecimal varRecargos) {
		this.varRecargos = varRecargos;
	}


	public BigDecimal getVarRecargos() {
		return varRecargos;
	}


	public void setVarBonifComisRPF(BigDecimal varBonifComisRPF) {
		this.varBonifComisRPF = varBonifComisRPF;
	}


	public BigDecimal getVarBonifComisRPF() {
		return varBonifComisRPF;
	}


	public void setVarDerechos(BigDecimal varDerechos) {
		this.varDerechos = varDerechos;
	}


	public BigDecimal getVarDerechos() {
		return varDerechos;
	}


	public void setVarIva(BigDecimal varIva) {
		this.varIva = varIva;
	}


	public BigDecimal getVarIva() {
		return varIva;
	}


	public void setVarPrimaTotal(BigDecimal varPrimaTotal) {
		this.varPrimaTotal = varPrimaTotal;
	}


	public BigDecimal getVarPrimaTotal() {
		return varPrimaTotal;
	}


	public void setVarComisPN(BigDecimal varComisPN) {
		this.varComisPN = varComisPN;
	}


	public BigDecimal getVarComisPN() {
		return varComisPN;
	}


	public void setVarComisRPF(BigDecimal varComisRPF) {
		this.varComisRPF = varComisRPF;
	}


	public BigDecimal getVarComisRPF() {
		return varComisRPF;
	}

	public void setPId_Recibo(BigDecimal pId_Recibo) {
		this.pId_Recibo = pId_Recibo;
	}

	public BigDecimal getPId_Recibo() {
		return pId_Recibo;
	}

	public void setPId_RecibosAjusta(String pId_RecibosAjusta) {
		this.pId_RecibosAjusta = pId_RecibosAjusta;
	}

	public String getPId_RecibosAjusta() {
		return pId_RecibosAjusta;
	}

	public void setpIdVersionPol(BigDecimal pIdVersionPol) {
		this.pIdVersionPol = pIdVersionPol;
	}

	public BigDecimal getpIdVersionPol() {
		return pIdVersionPol;
	}

	public void setpIdVersionPolCanc(BigDecimal pIdVersionPolCanc) {
		this.pIdVersionPolCanc = pIdVersionPolCanc;
	}

	public BigDecimal getpIdVersionPolCanc() {
		return pIdVersionPolCanc;
	}

	public void setpCveTEndoso(String pCveTEndoso) {
		this.pCveTEndoso = pCveTEndoso;
	}

	public String getpCveTEndoso() {
		return pCveTEndoso;
	}

	public void setpCveLTFolioCE(String pCveLTFolioCE) {
		this.pCveLTFolioCE = pCveLTFolioCE;
	}

	public String getpCveLTFolioCE() {
		return pCveLTFolioCE;
	}

}
