package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima")
public class EndosoAjustePrimaAction  extends BaseAction implements Preparable {

	private static final long serialVersionUID = -560884790604901549L;
	
	
    private RecuotificacionService recuotificacionService;
	
	@Autowired
	@Qualifier("recuotificacionServiceEJB")
	public void setRecuotificacionService(RecuotificacionService recuotificacionService) {
		this.recuotificacionService = recuotificacionService;
	}

	@Action(value = "mostrarAjustePrima", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/ajustePrima/endosoAjustePrima.jsp"),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "${actionNameOrigen}", "namespace",
					"${namespaceOrigen}", "idPolizaBusqueda", "${polizaId}",
					"numeroPolizaFormateado",
					"${polizaDTO.numeroPolizaFormateada}", "mensaje",
					"${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String mostrarAjustePrima() {
		
		if((getActionNameOrigen() != null && !getActionNameOrigen().isEmpty()) || (getNamespaceOrigen() != null && !getNamespaceOrigen().isEmpty()))
		{
			ServletActionContext.getContext().getSession().put("actionNameOrigen", getActionNameOrigen());
			ServletActionContext.getContext().getSession().put("namespaceOrigen", getNamespaceOrigen());			
		}
		
		if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())) {
						
			cotizacionContinuityId = entidadContinuityService
				.findContinuityByBusinessKey(CotizacionContinuity.class, "numero", polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue()).getId();
						
			resumenCostosDTO = calculoService.resumenCotizacionEndoso(
					polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(),
					TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			
			resumenCostosEndosoAjustar = calculoService.resumenCostosEndoso(polizaDTO.getIdToPoliza(), controlEndosoCotEAP.getNumeroEndosoAjusta());
			
		} else {
			//Valida que no haya otra cotizacion en proceso
			endosoService.getCotizacionEndosoCancelacionEndoso(polizaDTO.getIdToPoliza());
			resumenCostosDTO = new ResumenCostosDTO();
			resumenCostosEndosoAjustar = new ResumenCostosDTO();
		}
		
		siguienteEndoso = endosoService.getNumeroEndoso(polizaDTO.getIdToPoliza(), "%06d");		
		
		return SUCCESS;
	}
	
	
	@Action(value = "emitir", results = { @Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "mostrarDefTipoModificacion", "namespace",
			"/endoso/cotizacion/auto/solicitudEndoso",
			"numeroPolizaFormateado", "${polizaDTO.numeroPolizaFormateada}",
			"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String emitir() {
		
		try{
			
			BigDecimal grupoParametro = new BigDecimal("22");
			LOG.info("Obteniendo valores de parametros de configuracion de recuotificacion");
			Integer banderaRecuotificacion = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, new BigDecimal("22030"));
			Integer banderaPrevioRecibos = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, new BigDecimal("22040"));
			
			if (banderaRecuotificacion.intValue() == 1){
			
			BigDecimal idToCotizacion = polizaDTO.getCotizacionDTO().getIdToCotizacion();			
			LOG.info("Validando Previo de recibos para cotizacion: " + idToCotizacion);
			//SE AGREGA VALIDACION DE PREVIO DE RECIBOS AL EMITIR
			//ParametroGeneralDN parametroGeneralDN = ParametroGeneralDN.getINSTANCIA();
			//ParametroGeneralId idpg = new ParametroGeneralId();
			//idpg.setIdToGrupoParametroGeneral(new BigDecimal("22"));
			//idpg.setCodigoParametroGeneral(new BigDecimal("22010"));
			//ParametroGeneralDTO parametroGeneralDTO = parametroGeneralDN.getPorId(idpg);
		
			boolean previoDeRecibos = recuotificacionService.validarPrevioDeRecibos(idToCotizacion);
			if (previoDeRecibos){
				LOG.info("Existe Previo de recibos, validando saldos de Recuotificacion");
				Map<String, Object> res = recuotificacionService.validaTotalesDeCotizacionSalir(idToCotizacion);
				BigDecimal resultado = (BigDecimal) res.get("outIdEjecucion");
				if (resultado.toString().equals("0")){
					LOG.info("No existen saldos, continuando con emision");
				}else{
					LOG.info("Existen saldos por asignar a PP, pausando emision");
					super.setMensajeError("Existen saldos de Recuotificacion. Verifique.");					
					return SUCCESS;
				}
			}else{
				LOG.info("No existe previo de recuotificacion, validando previo de recibos");
				if(banderaPrevioRecibos.intValue() == 1){
					LOG.info("Bandera de Recuotificacion encendida, Pausando emision");
					super.setMensajeError("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");					
					return SUCCESS;
				}else{
					LOG.info("Bandera de Recuotificacion apagada, continuando con la emision.");
				}	
			}
			
			}else{
				LOG.info("Bandera de recuotificacion deshabilitada, continuando con el flujo natural de la operacion");
			}
			
		}catch(Exception e){
			LOG.info("Error en validacion previa re recuotificacion: " + e.getMessage());
			super.setMensajeError("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");					
			return SUCCESS;
		}
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(
							cotizacionContinuityId, 
							TimeUtils.getDateTime(fechaIniVigenciaEndoso), 
							SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);
		String numeroSolicitudCheque = "";
		if(endoso!=null){
			if(endoso.getSolictudCheque()!=null){
				numeroSolicitudCheque = ". N\u00Famero solicitud Seycos del cheque: "+endoso.getSolictudCheque(); 
			}
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue())+numeroSolicitudCheque);
		}
		return SUCCESS;
	}
	
	@Action(value = "cotizar", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
				"actionName", "mostrarAjustePrima", 
				"namespace", "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima",
				"actionNameOrigen", "${actionNameOrigen}",
				"namespaceOrigen", "${namespaceOrigen}",	
				"polizaId", "${polizaId}", 
				"accionEndoso",	"${accionEndoso}", 
				"numeroEndoso",	"${numeroEndoso}", 
				"mensaje", "${mensaje}", 
				"tipoMensaje", "${tipoMensaje}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarAjustePrima", 
					"namespace", "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima",
					"actionNameOrigen", "${actionNameOrigen}",
					"namespaceOrigen", "${namespaceOrigen}",
					"polizaId", "${polizaId}", 
					"accionEndoso",	"${accionEndoso}", 
					"numeroEndoso",	"${numeroEndoso}", 
					"mensaje", "${mensaje}", 
					"tipoMensaje", "${tipoMensaje}" })
			})
	public String cotizar() {
		endosoService.guardaCotizacionAjustePrima(polizaDTO.getCotizacionDTO().getIdToCotizacion(), polizaDTO.getIdToPoliza(), controlEndosoCotEAP, numeroEndoso, tipoFormaPago);
		setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		setMensajeExito();
		setListadoEnSession(null);
		return SUCCESS;
	}

	@Action(value = "cancelar", results = { @Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "mostrarDefTipoModificacion", "namespace",
			"/endoso/cotizacion/auto/solicitudEndoso",
			"numeroPolizaFormateado", "${polizaDTO.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String cancelar() {
		System.out.println("cancelar()");
		this.setEsRetorno(1);
		return SUCCESS;
	}
	
	@Action(value = "obtenerEndososPaginado", results = { @Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") })
	public String obtenerEndososPaginado() {
		
		Collection<EndosoDTO> listado = new ArrayList<EndosoDTO>();
		if (getTotalCount() == null) {
			listado = endosoService.findForAdjust(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue());
			setLstEndosoDto(listado);
			setTotalCount(new Integer(listado.size()).longValue());
		}
		setPosPaginado();
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "obtenerEndosos", results = { @Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/cancelacionEndoso/endosoCancelacionEndosoGrid.jsp") })
	public String obtenerEndosos() {
		
		Collection<EndosoDTO> listado = new ArrayList<EndosoDTO>();

		if (!super.listadoDeCache()) {

			listado = endosoService.findForAdjust(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue());

		} else {
			listado = (List<EndosoDTO>) getListadoPaginado();
		}

		setLstEndosoDto(listado);
		setListadoEnSession(listado);

		return SUCCESS;
	}
	
	
	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		
		if (numeroEndoso != null) {
			EndosoId endosoId = new EndosoId(polizaDTO.getIdToPoliza(), numeroEndoso);
    		EndosoDTO endoso = entidadService.findById(EndosoDTO.class, endosoId);
    		fechaIniVigenciaEndoso = endoso.getValidFrom();
		}
		
		controlEndosoCotEAP = endosoService.getControlEndosoCotEAP(polizaDTO.getCotizacionDTO().getIdToCotizacion(), numeroEndoso);
		if (controlEndosoCotEAP == null) {
			controlEndosoCotEAP = new ControlEndosoCot();
		} else {
			//1: Igual poliza, 2: Anual
			if (controlEndosoCotEAP.getPorcentajePagoFraccionado().doubleValue() == 0) {
				tipoFormaPago = 2; 
			} else {
				tipoFormaPago = 1; 
			}
		}
		
		
	}
		
	private Date fechaIniVigenciaEndoso;
	private ControlEndosoCot controlEndosoCotEAP;
	private Collection<EndosoDTO> lstEndosoDto = new ArrayList<EndosoDTO>(1);
	private Long polizaId;
	private Long cotizacionContinuityId;
	private String accionEndoso;
	private PolizaDTO polizaDTO;
	private ResumenCostosDTO resumenCostosDTO;
	private ResumenCostosDTO resumenCostosEndosoAjustar;
	private String siguienteEndoso;
	private Short numeroEndoso;
	private Integer tipoFormaPago;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA;
	
	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public ControlEndosoCot getControlEndosoCotEAP() {
		return controlEndosoCotEAP;
	}

	public void setControlEndosoCotEAP(ControlEndosoCot controlEndosoCotEAP) {
		this.controlEndosoCotEAP = controlEndosoCotEAP;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public Collection<EndosoDTO> getLstEndosoDto() {
		return lstEndosoDto;
	}

	public void setLstEndosoDto(Collection<EndosoDTO> lstEndosoDto) {
		this.lstEndosoDto = lstEndosoDto;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	
	public ResumenCostosDTO getResumenCostosEndosoAjustar() {
		return resumenCostosEndosoAjustar;
	}

	public void setResumenCostosEndosoAjustar(
			ResumenCostosDTO resumenCostosEndosoAjustar) {
		this.resumenCostosEndosoAjustar = resumenCostosEndosoAjustar;
	}

	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}

	public String getSiguienteEndoso() {
		return siguienteEndoso;
	}

	public void setSiguienteEndoso(String siguienteEndoso) {
		this.siguienteEndoso = siguienteEndoso;
	}
		
	public Short getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}
	
	public Integer getTipoFormaPago() {
		return tipoFormaPago;
	}

	public void setTipoFormaPago(Integer tipoFormaPago) {
		this.tipoFormaPago = tipoFormaPago;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	
	
	
	
	private EndosoService endosoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntidadService entidadService;
	private EntidadContinuityService entidadContinuityService;
	private CalculoService calculoService;
	
	@Autowired
	@Qualifier("entidadContinuityServiceEJB")
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}

	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

}
