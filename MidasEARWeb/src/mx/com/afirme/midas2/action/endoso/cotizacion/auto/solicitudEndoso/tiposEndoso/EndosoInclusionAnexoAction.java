package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.tipoEndoso.EndosoInclusionAnexoCotService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo")
public class EndosoInclusionAnexoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date fechaIniVigenciaEndoso;
	private BitemporalCotizacion biCotizacion;
	private Collection<BitemporalDocAnexoCot> biDocAnexosAsociados = new ArrayList<BitemporalDocAnexoCot>(1);
	private Collection<BitemporalDocAnexoCot> biDocAnexosDisponibles = new ArrayList<BitemporalDocAnexoCot>(1);
	private BitemporalDocAnexoCot biDocAnexoCot = new BitemporalDocAnexoCot();
	private Integer polizaId;
	private String accionEndoso;
	private String anexos;
	private PolizaDTO polizaDTO;
	
	private EndosoService endosoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntidadService entidadService;
	private EndosoInclusionAnexoCotService endosoInclusionAnexoCotService;
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("endosoInclusionAnexoCotServiceEJB")
	public void setEndosoInclusionAnexoCotService(
			EndosoInclusionAnexoCotService endosoInclusionAnexoCotService) {
		this.endosoInclusionAnexoCotService = endosoInclusionAnexoCotService;
	}

	@Override
	public void prepare() throws Exception {
		 polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		 
	}
	

	@Action (value = "mostrarInclusionAnexo", results = {
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionAnexo/endosoInclusionAnexo.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarInclusionAnexo() {		
		biCotizacion = endosoService.getCotizacionEndosoIA(new BigDecimal(polizaId), fechaIniVigenciaEndoso, accionEndoso);
		return SUCCESS;		
	}
	
	
	public void prepareEmitir() {		
		biCotizacion = endosoService.prepareCotizacionEndoso(biCotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
			
	public String emitir() {
	
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(biCotizacion.getContinuity().getId(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso), biCotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO + String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		return SUCCESS;		
	}
	
	public void prepareCotizar() {
		biCotizacion = endosoService.prepareCotizacionEndoso(biCotizacion.getContinuity().getId(), 
															TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action						
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarInclusionAnexo",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${biCotizacion.value.fechaInicioVigencia}",
					"accionEndoso","${accionEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})
	
	public String cotizar() {		
		endosoService.guardaCotizacionInclusionAnexo(biCotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		setMensajeExito();
		return SUCCESS;		
	}
		
	@Action (value = "cancelar", results = {
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","mostrarDefTipoModificacion",
			"namespace","/endoso/cotizacion/auto/solicitudEndoso",
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar() {
		//TODO 
		System.out.println("cancelar()");
		this.setEsRetorno(1);
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerDocAnexosAsociados", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionAnexo/endosoInclusionAnexoAsociadosGrid.jsp") })
	public String obtenerDocAnexosAsociados() {
		biDocAnexosAsociados = getLstBiDocAnexosAsociados(biCotizacion, fechaIniVigenciaEndoso);
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerDocAnexosDisponibles", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionAnexo/endosoInclusionAnexoDisponiblesGrid.jsp") })
	public String obtenerDocAnexosDisponibles() {
		biDocAnexosDisponibles = endosoInclusionAnexoCotService.getLstBiDocAnexosDisponibles(biCotizacion.getContinuity().getId(), 
															TimeUtils.getDateTime(fechaIniVigenciaEndoso), 
															getLstBiDocAnexosAsociados(biCotizacion, fechaIniVigenciaEndoso));
		return SUCCESS;
	}
	
	private Collection<BitemporalDocAnexoCot> getLstBiDocAnexosAsociados(BitemporalCotizacion biCotizacion, Date fechaIniVigenciaEndoso) {
		Collection<BitemporalDocAnexoCot> lstBiDocAnexosAsociados = endosoInclusionAnexoCotService.getLstBiDocAnexosAsociados(
														biCotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		return lstBiDocAnexosAsociados;
	}

	
	@Action
	(value = "accionAnexoAdicional", results = { 
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp") })
	public String accionAnexoAdicional(){
		String accion = ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		endosoInclusionAnexoCotService.accionInclusionAnexo(biDocAnexoCot, biCotizacion, 
													TimeUtils.getDateTime(fechaIniVigenciaEndoso), accion);
		return SUCCESS;
	}
	
	
	

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}	

	public Integer getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Integer polizaId) {
		this.polizaId = polizaId;
	}

	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}	

	public BitemporalDocAnexoCot getBiDocAnexoCot() {
		return biDocAnexoCot;
	}

	public void setBiDocAnexoCot(BitemporalDocAnexoCot biDocAnexoCot) {
		this.biDocAnexoCot = biDocAnexoCot;
	}

	public Collection<BitemporalDocAnexoCot> getBiDocAnexosAsociados() {
		return biDocAnexosAsociados;
	}

	public void setBiDocAnexosAsociados(
			Collection<BitemporalDocAnexoCot> biDocAnexosAsociados) {
		this.biDocAnexosAsociados = biDocAnexosAsociados;
	}

	public Collection<BitemporalDocAnexoCot> getBiDocAnexosDisponibles() {
		return biDocAnexosDisponibles;
	}

	public void setBiDocAnexosDisponibles(
			Collection<BitemporalDocAnexoCot> biDocAnexosDisponibles) {
		this.biDocAnexosDisponibles = biDocAnexosDisponibles;
	}

	public String getAnexos() {
		return anexos;
	}

	public void setAnexos(String anexos) {
		this.anexos = anexos;
	}	

}
