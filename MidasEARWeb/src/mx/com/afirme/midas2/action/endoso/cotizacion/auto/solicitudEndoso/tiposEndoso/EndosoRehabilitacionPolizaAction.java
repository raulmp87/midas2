package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza")
public class EndosoRehabilitacionPolizaAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroPolizaFormateado;
	
	private BigDecimal polizaId;
	private PolizaDTO polizaDTO;
	private Date fechaIniVigenciaEndoso;
	private String accionEndoso;	
	
	private CotizacionEndosoDTO cotizacionEndoso;
	private BitemporalCotizacion cotizacion;
	private ResumenCostosDTO resumenCostosDTO;
	
	private EndosoPolizaService endosoPolizaService;
	private CalculoService calculoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntidadService entidadService;
	private EntidadBitemporalService entidadBitemporalService;

	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class, polizaId);
	}
	
	public void prepareMostrarRehabilitacionPoliza(){
		// se obtiene la información que se tiene que mostrar.
		polizaDTO = entidadService.findById(PolizaDTO.class, polizaId);
		cotizacionEndoso = endosoPolizaService.getCotizacionEndosoRehabilitacionPoliza(getPolizaId(), fechaIniVigenciaEndoso, accionEndoso, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO);
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action (value = "mostrarRehabilitacionPoliza", results = { 
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoRehabilitacionPoliza.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"accionEndoso","${accionEndoso}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarRehabilitacionPoliza() {
		
		return SUCCESS;
	}	
	
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarRehabilitacionPoliza",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza",
					"polizaId","${polizaId}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}" ,
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"	
			})})
	public String cotizar() {
		
		endosoPolizaService.guardaCotizacionEndosoRehabilitacionPoliza(polizaId,fechaIniVigenciaEndoso, cotizacionEndoso.getRequiereCorrimientoVigencias(), accionEndoso);
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();
		return SUCCESS;
	}
	
	public void prepareEmitir(){
		
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() {
		cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(fechaIniVigenciaEndoso), SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA);
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		return SUCCESS;
	}	
	
	@Action (value = "cancelar", results = { 
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"numeroPolizaFormateado", "${polizaDTO.numeroPolizaFormateada}",
			"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
			"polizaId","${polizaId}",
			"esRetorno", "${esRetorno}",
			"accionEndoso","${accionEndoso}"
		})
	})
	public String cancelar(){
		this.setEsRetorno(1);
		return SUCCESS;
	}	

	
	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}

	public BigDecimal getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}
	
	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionEndosoDTO getCotizacionEndoso() {
		return cotizacionEndoso;
	}

	public void setCotizacionEndoso(CotizacionEndosoDTO cotizacionEndoso) {
		this.cotizacionEndoso = cotizacionEndoso;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}
	

	@Autowired
	@Qualifier("endosoPolizaServiceEJB")
	public void setEndosoPolizaService(EndosoPolizaService endosoPolizaService) {
		this.endosoPolizaService = endosoPolizaService;
	}	
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}	
}
