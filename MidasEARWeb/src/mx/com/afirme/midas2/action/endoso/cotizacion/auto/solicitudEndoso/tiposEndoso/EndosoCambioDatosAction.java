package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.poliza.seguroobligatorio.GeneraSeguroObligatorio;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.DocumentationException;
import mx.com.afirme.midas2.clientesapi.dto.DocumentoDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos")
public class EndosoCambioDatosAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date fechaIniVigenciaEndoso;
	private String nombreContratante;
	private Long nuevoContratanteId;
	private String numeroPolizaFormateada; 
    private PolizaDTO polizaDTO;
    private String accionEndoso;
    private Boolean aplicaEndoso = true;
    private Long polizaId;
    private BitemporalCotizacion cotizacion;
    private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS;
	private	List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private Long id;
    
    private EntidadService entidadService;
	private EndosoService endosoService;
    private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
    
    private SeguroObligatorioService seguroObligatorioService;
    
    private CalculoService calculoService;
    private ClientesApiService  clienteRest;
	private String linkFortimax;
	private List<DocumentoDTO>  documentacion;

    @Autowired
    	@Qualifier("clientesApiServiceEJB")
    	public void setClienteRest(ClientesApiService clienteRest) {
    		this.clienteRest = clienteRest;
    	}
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}		
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}	
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	@Override
	public void prepare() throws Exception {
		
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));		
	}
	
	@Action (value = "mostrarCambioDatos", results = { 
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/cambioDatos/endosoCambioDatos.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarCambioDatos() {
		cotizacion = endosoService.getCotizacionEndosoCambioDatos(new BigDecimal(polizaId), fechaIniVigenciaEndoso,accionEndoso);		
		ResumenCostosDTO resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		return SUCCESS;
	}
	
	@Action (value = "cancelar", results = {
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"esRetorno", "${esRetorno}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar() {
		this.setEsRetorno(1);
		return SUCCESS;
	}
	
	public void prepareCotizar() {		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		//Actualizar Datos Contratante /endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos/
		if (nuevoContratanteId != null) {
			ClienteGenericoDTO clienteGenerico = endosoService.getClienteGenerico(new BigDecimal(nuevoContratanteId));
			cotizacion.getValue().setDomicilioContratanteId(BigDecimal.valueOf(clienteGenerico.getIdDomicilioConsulta()));
			cotizacion.getValue().setPersonaContratanteId(clienteGenerico.getIdCliente().longValue());
			cotizacion.getValue().setNombreContratante(clienteGenerico.getNombreCompleto());
			cotizacion.getValue().getSolicitud().setClaveTipoPersona(clienteGenerico.getClaveTipoPersona());			
		}
	}
	/**
	 * Invoca al cliente REST para almacenar la documentacion dentro de la
	 * aplicación cliente único Carga nuevamente la cotización para regresar al
	 * flujo de la emisión
	 * 
	 * @return regresa al flujo de emision
	 * 
	 * 
	 * */
	@Action
	(value = "guardarDocumentacion", results = { 
			@Result(name=SUCCESS,location= "/jsp/endosos/solicitudEndoso/tiposEndoso/cambioDatos/endosoCambioDatos.jsp"
					)
						})
	public String guardarDocumentacion(){
		
		
		List<DocumentoDTO> listToBorrar = new ArrayList<DocumentoDTO>();
		for (int i=0;i<documentacion.size();i++){
				DocumentoDTO documento =documentacion.get(i);
				if(documento.getSeleccionado()==null||!documento.getSeleccionado()){
					listToBorrar.add(documento);
				}
			}
		 documentacion.removeAll(listToBorrar);
		 clienteRest.saveDocument(polizaDTO.getCotizacionDTO().getIdToPersonaContratante().longValue(), documentacion);
		
		 cotizacion = endosoService.getCotizacionEndosoCambioDatos(new BigDecimal(polizaId), fechaIniVigenciaEndoso,"2");		
			ResumenCostosDTO resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
					TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			
			return SUCCESS;
		}
	
	
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarCambioDatos",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})
	public String cotizar() {			
		endosoService.guardaCotizacionEndosoCambioDatos(cotizacion);
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();		
		
		return SUCCESS;
	}
	
	public void prepareEmitir() {
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
						"namespace","${namespaceOrigen}",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"}),
						@Result(name="documentacion",location="/jsp/cliente/catalogoClientes/documentacionClienteUnico.jsp"
								),
						@Result(name=ERROR, location = "/jsp/suscripcion/cotizacion/auto/resultadoTerminarCotizacion.jsp")
						})
			
	public String emitir() {
		
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.validarEmisionCotizacion(cotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		if (terminarCotizacionDTO != null) {
			excepcionesList = 	terminarCotizacionDTO.getExcepcionesList();
			id = cotizacion.getContinuity().getNumero().longValue();
			super.setMensajeError(terminarCotizacionDTO.getMensajeError());				
			return ERROR;			
		}
		ResponseEntity<Long> resp=clienteRest.clienteUnificado(null,cotizacion.getValue().getPersonaContratanteId());
			
		try{
			 clienteRest.getDocumentacionFaltante(resp.getBody());
			 
		}
		
		catch (DocumentationException e) {
			
			//Se prepara la pantalla de captura de documentos
			 ClienteUnicoDTO cliente=clienteRest.findClienteUnicoById(resp.getBody());
			 List<DocumentoDTO> documentacionTemp = clienteRest.getDocumentos(e.getMessage());
			 documentacion= new ArrayList<DocumentoDTO>();
			 linkFortimax=clienteRest.getLinkFortmax(resp.getBody());
			 List<DocumentoDTO> expediente=clienteRest.getDocumentacion(resp.getBody());
			 for(DocumentoDTO documentoTemp:documentacionTemp){
				 DocumentoDTO  documento = new DocumentoDTO();
				 for(DocumentoDTO docuExp :expediente){
					 if(documentoTemp.getId().toString().compareTo(docuExp.getDocumentoId())==0 ){
						 if(docuExp.getFechaVigencia()!=null){
							 String []temp =docuExp.getFechaVigencia().substring(0,10).split("-"); 
							 if(temp!=null&&temp.length==3){
								 documento.setFechaVigencia(temp[2]+"/"+temp[1]+"/"+temp[0]);
								 
							 }

						 }
						 
						 documento.setDescripcion(docuExp.getDescripcion());
						 documento.setDocumentoId(docuExp.getDocumentoId());
						 documento.setSeleccionado(true);
							
						 break;
					 }
					 
				 }
				documento.setNombre(documentoTemp.getNombre());
				documento.setRequerido(documentoTemp.getRequerido());
				documento.setDocumentoId(String.valueOf(documentoTemp.getId()));
				documentacion.add(documento);
				
			 }
			 
				 return "documentacion";
			
		} 
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso != null) {			
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
			//Emite Endoso Datos SO
			try{
				if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_CAMBIO_DATOS)){
					GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
					generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
					generaSeguroObligatorio.creaPolizaAnexaEndosoCambioDatos(polizaDTO, endoso.getId().getNumeroEndoso().shortValue());
				}
				//se agrega validación PEPS
				if(cotizacion.getValue().getPersonaContratanteId()!=null){
					
					clienteRest.validatePeps(cotizacion.getValue().getPersonaContratanteId(),endoso.getDescripcionNumeroEndoso());
						
				}
					if(cotizacion.getValue().getPersonaAseguradoId()!=null){
					
					clienteRest.validatePeps(cotizacion.getValue().getPersonaAseguradoId(),endoso.getDescripcionNumeroEndoso());
						
				}
				
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}		
		return SUCCESS;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public String getNombreContratante() {
		return nombreContratante;
	}

	public void setNombreContratante(String nombreContratante) {
		this.nombreContratante = nombreContratante;
	}

	public String getNumeroPolizaFormateada() {
		return numeroPolizaFormateada;
	}

	public void setNumeroPolizaFormateada(String numeroPolizaFormateada) {
		this.numeroPolizaFormateada = numeroPolizaFormateada;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Boolean getAplicaEndoso() {
		return aplicaEndoso;
	}

	public void setAplicaEndoso(Boolean aplicaEndoso) {
		this.aplicaEndoso = aplicaEndoso;
	}

	public Long getNuevoContratanteId() {
		return nuevoContratanteId;
	}

	public void setNuevoContratanteId(Long nuevoContratanteId) {
		this.nuevoContratanteId = nuevoContratanteId;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public void setExcepcionesList(
			List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public String getLinkFortimax() {
		return linkFortimax;
	}
	public void setLinkFortimax(String linkFortimax) {
		this.linkFortimax = linkFortimax;
	}
	public List<DocumentoDTO> getDocumentacion() {
		return documentacion;
	}
	public void setDocumentacion(List<DocumentoDTO> documentacion) {
		this.documentacion = documentacion;
	}

}
