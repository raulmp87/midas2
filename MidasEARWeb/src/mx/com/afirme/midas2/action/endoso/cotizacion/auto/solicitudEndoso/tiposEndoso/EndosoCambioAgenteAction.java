package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente")
public class EndosoCambioAgenteAction extends BaseAction implements Preparable {

	/** serialVersionUID **/
	private static final long serialVersionUID = 4897078044999633920L;

	private Date fechaIniVigenciaEndoso;
	private List<Map<String, Object>> listaAgentes;
	private Integer idAgenteACambiar;
	private Long idAgenteNuevo;
	private String motivoCambio;
	private Long polizaId;
	private String accionEndoso;
	private String nombreAgente;
	private PolizaDTO poliza;
	private BitemporalCotizacion cotizacion;
	private Long codigoAgenteOriginal;

	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	private EndosoService endosoService;
	@Autowired
	@Qualifier("entidadEJB")
	private EntidadService entidadService;
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	@Autowired
	@Qualifier("agenteMidasEJB")
	private AgenteMidasService agenteMidasService;
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	private ValorCatalogoAgentesService catalogoService;

	@Override
	public void prepare() throws Exception {
		poliza = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));
	}	

	/**
	 * Metodo inicial, carga los datos en pantalla
	 * @return
	 * @throws Exception 
	 */
	@Action(value = "mostrarCambioAgente", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoCambioAgente.jsp"),
			@Result(name = INPUT, type = "redirectAction", params = { 
					"actionName", "${actionNameOrigen}", 
					"namespace", "${namespaceOrigen}", 
					"idPolizaBusqueda", "${polizaId}",
					"numeroPolizaFormateado", "${poliza.numeroPolizaFormateada}",
					"mensaje", "${mensaje}",
					"tipoMensaje", "${tipoMensaje}"}) 
			})
	public String mostrarCambioAgente() throws Exception {
		
		if (fechaIniVigenciaEndoso == null) {
			fechaIniVigenciaEndoso = LocalDate.now().toDate();
		}		

		cotizacion = endosoService.getCotizacionEndosoCambioAgente(BigDecimal.valueOf(polizaId),
				fechaIniVigenciaEndoso, accionEndoso);
		
		if (!TipoAccionDTO.getNuevoEndosoCot().equalsIgnoreCase(accionEndoso)) {
			nombreAgente = cotizacion.getValue().getSolicitud().getNombreAgente() + '-' + 
							getAgente(cotizacion.getValue().getSolicitud().getCodigoAgente().longValue()).getIdAgente();			
		}
		
		// La cotizacion valida
		final BitemporalCotizacion cotizacionTemp = cotizacion.getContinuity().getCotizaciones()
				.get(TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		//nombreAgente = cotizacion.getValue().getSolicitud().getNombreAgente();
		cotizacion.getValue().getSolicitud().setNombreAgente(cotizacionTemp.getValue().getSolicitud().getNombreAgente());
		codigoAgenteOriginal = getAgente(cotizacionTemp.getValue().getSolicitud().getCodigoAgente().longValue()).getIdAgente();
		// Solo los agentes activos
		final ValorCatalogoAgentes tipoSituacion = catalogoService.obtenerElementoEspecifico(
				"Estatus de Agente (Situacion)", "AUTORIZADO");
		final Map<String, Object> filters = new HashMap<String, Object>();
		//Solo valida por negocio en caso de que negocio este limitado
		Map<String, Object> filtersCount = new HashMap<String, Object>();
		filtersCount.put("negocio.idToNegocio", cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
		Long count = entidadService.findByPropertiesCount(NegocioAgente.class, filtersCount);		
		if(count.floatValue() > 0){
			filters.put("idNegocio", cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
		}
		
		filters.put("idTipoSituacion", tipoSituacion.getId());
		final List<AgenteView> agentesView = agenteMidasService.findByFilterLightWeight(filters);
		if (CollectionUtils.isNotEmpty(agentesView)) {

			listaAgentes = new ArrayList<Map<String, Object>>();
			for (AgenteView agente : agentesView) {
				final Map<String, Object> agenteMap = new HashMap<String, Object>();
				agenteMap.put("label", agente.getNombreCompleto() +  '-' + agente.getIdAgente());
				agenteMap.put("id", agente.getId());
				listaAgentes.add(agenteMap);
			}
		}	
		
		return SUCCESS;
	}
	/**
	 * Cancelar la creación del endoso
	 * @return
	 */
	@Action(value = "cancelar", results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "${actionNameOrigen}",
			"namespace", "${namespaceOrigen}",
			"numeroPolizaFormateado", "${poliza.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje", "${mensaje}",
			"tipoMensaje", "${tipoMensaje}" })
			})
	public String cancelar() {
		this.setEsRetorno(1);
		LogDeMidasWeb.getLogger().log(Level.FINEST, "EndosoCambioAgenteAction.cancelar()");
		return SUCCESS;
	}
	
	public void prepareCotizar() {
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		cotizacion.getValue().getSolicitud().setComentarioList(new ArrayList<Comentario>());
	}
	
	@Action(value = "cotizar", results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarCambioAgente", 
			"namespace", "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente", 
			"polizaId", "${polizaId}",
			"codigoAgenteOriginal", "${codigoAgenteOriginal}",
			"fechaIniVigenciaEndoso", "${fechaIniVigenciaEndoso}",
			"accionEndoso", "${accionEndoso}",
			"mensaje", "${mensaje}",
			"cotizacion.value.solicitud.codigoAgente", "${cotizacion.value.solicitud.codigoAgente}",
			"nombreAgente", "${nombreAgente}",
			"tipoMensaje", "${tipoMensaje}",
			"actionNameOrigen", "${actionNameOrigen}",
			"namespaceOrigen", "${namespaceOrigen}"})
			})
	public String cotizar() {
		BitemporalCotizacion cotizacionTemp = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity()
				.getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		if (cotizacion.getValue().getSolicitud().getCodigoAgente().equals(cotizacionTemp.getValue().getSolicitud().getCodigoAgente())) {
			setMensajeError("Debe seleccionar una agente diferente al actual");			
		} else {
			
			Agente agenteDestino = getAgente(cotizacion.getValue().getSolicitud().getCodigoAgente().longValue());
			
			cotizacion.getValue().getSolicitud().setNombreAgente(agenteDestino.getPersona().getNombreCompleto());
			
			if(agenteDestino.getPromotoria() != null)
			{
				cotizacion.getValue().getSolicitud().setNombreOficinaAgente(agenteDestino.getPromotoria().getDescripcion());
				cotizacion.getValue().getSolicitud().setCodigoEjecutivo(new BigDecimal(agenteDestino.getPromotoria().getEjecutivo().getIdEjecutivo()));
				cotizacion.getValue().getSolicitud().setNombreEjecutivo(agenteDestino.getPromotoria().getEjecutivo().getPersonaResponsable().getNombreCompleto());
				cotizacion.getValue().getSolicitud().setIdOficina(new BigDecimal(agenteDestino.getPromotoria().getEjecutivo().getGerencia().getId()));
				cotizacion.getValue().getSolicitud().setNombreOficina(agenteDestino.getPromotoria().getEjecutivo().getGerencia().getPersonaResponsable().getNombreCompleto());					
			}
			
			endosoService.guardaCotizacionEndosoCambioAgente(cotizacion);
			setMensajeExito();
			setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		}
		

		return SUCCESS;
	}
	
	public void prepareEmitir() {
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
	}
	
	/**
	 * Emitir el endoso de cambio de agente
	 * @return
	 */
	@Action(value = "emitir", results = { @Result(name = SUCCESS, type = "redirectAction", params = { 
			"actionName", "mostrarDefTipoModificacion",
			"namespace", "/endoso/cotizacion/auto/solicitudEndoso",
			"numeroPolizaFormateado","${poliza.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"})
			})
	public String emitir() {
		LogDeMidasWeb.getLogger().log(Level.FINEST, "EndosoCambioAgenteAction.emitir()");
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		final EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaIniVigenciaEndoso), cotizacion.getValue()
						.getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso == null) {
			setMensajeError(BaseAction.MENSAJE_ERROR_GENERAL);
		} else {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO
					+ String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		return SUCCESS;
	}
	
	private Agente getAgente(Long idAgente) {
		Agente agente = null;
		List<AgenteView> agenteViewList = new ArrayList<AgenteView>();
		Agente filtroAgente = new Agente();
		filtroAgente.setIdAgente(idAgente);
		agenteViewList = agenteMidasService.findByFilterLightWeight(filtroAgente);
		if (agenteViewList != null && !agenteViewList.isEmpty()) {
			AgenteView agenteTmp = agenteMidasService.findByFilterLightWeight(filtroAgente).get(0);
			agente = new Agente();
			agente.getPersona().setNombreCompleto(agenteTmp.getNombreCompleto());
			agente.setIdAgente(agenteTmp.getIdAgente());
		} else {
			agente = entidadService.findById(Agente.class, idAgente);			
		}
		
		return agente;
	}
	
	/* ************** Getters & setters ************** */
	/**
	 * @return the fechaIniVigenciaEndoso
	 */
	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	/**
	 * @param fechaIniVigenciaEndoso the fechaIniVigenciaEndoso to set
	 */
	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	/**
	 * @return the listaAgentes
	 */
	public List<Map<String, Object>> getListaAgentes() {
		return listaAgentes;
	}

	/**
	 * @param listaAgentes the listaAgentes to set
	 */
	public void setListaAgentes(List<Map<String, Object>> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	/**
	 * @return the idAgenteACambiar
	 */
	public Integer getIdAgenteACambiar() {
		return idAgenteACambiar;
	}

	/**
	 * @param idAgenteACambiar the idAgenteACambiar to set
	 */
	public void setIdAgenteACambiar(Integer idAgenteACambiar) {
		this.idAgenteACambiar = idAgenteACambiar;
	}

	/**
	 * @return the motivoCambio
	 */
	public String getMotivoCambio() {
		return motivoCambio;
	}

	/**
	 * @param motivoCambio the motivoCambio to set
	 */
	public void setMotivoCambio(String motivoCambio) {
		this.motivoCambio = motivoCambio;
	}

	/**
	 * @return the polizaId
	 */
	public Long getPolizaId() {
		return polizaId;
	}

	/**
	 * @param polizaId the polizaId to set
	 */
	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	/**
	 * @return the idAgenteNuevo
	 */
	public Long getIdAgenteNuevo() {
		return idAgenteNuevo;
	}

	/**
	 * @param idAgenteNuevo the idAgenteNuevo to set
	 */
	public void setIdAgenteNuevo(Long idAgenteNuevo) {
		this.idAgenteNuevo = idAgenteNuevo;
	}

	/**
	 * @return the accionEndoso
	 */
	public String getAccionEndoso() {
		return accionEndoso;
	}

	/**
	 * @param accionEndoso the accionEndoso to set
	 */
	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	/**
	 * @param endosoService the endosoService to set
	 */
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	/**
	 * @param entidadService the entidadService to set
	 */
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/**
	 * @return the poliza
	 */
	public PolizaDTO getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}

	/**
	 * @return the cotizacion
	 */
	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	/**
	 * @param cotizacion the cotizacion to set
	 */
	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	/**
	 * @param emisionEndosoBitemporalService the emisionEndosoBitemporalService to set
	 */
	public void setEmisionEndosoBitemporalService(EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	/**
	 * @param agenteMidasService the agenteMidasService to set
	 */
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	/**
	 * @return the nombreAgente
	 */
	public String getNombreAgente() {
		return nombreAgente;
	}

	/**
	 * @param nombreAgente the nombreAgente to set
	 */
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	/**
	 * @param catalogoService the catalogoService to set
	 */
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	public Long getCodigoAgenteOriginal() {
		return codigoAgenteOriginal;
	}

	public void setCodigoAgenteOriginal(Long codigoAgenteOriginal) {
		this.codigoAgenteOriginal = codigoAgenteOriginal;
	}
	
	
	
}
