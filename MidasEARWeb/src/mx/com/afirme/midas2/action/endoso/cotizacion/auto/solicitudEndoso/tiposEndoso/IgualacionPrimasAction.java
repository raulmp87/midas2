package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas")
public class IgualacionPrimasAction  extends BaseAction implements Preparable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal polizaId;
	private BitemporalCotizacion cotizacion;
	
	private BigDecimal primaAIgualar;
	private Boolean soloNegativos = false;
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}

	@Action (value = "mostrarIgualacionPrimas", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/igualacionPrimas.jsp")
	})
	public String mostrarIgualacionPrimas() {
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, getPolizaId());
		ControlEndosoCot cec = calculoService.obtenerControlEndosoCotizacion(poliza.getCotizacionDTO().getIdToCotizacion().intValue());
		short tipoEndoso = cec.getSolicitud().getClaveTipoEndoso().shortValue();
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA ||
				tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
			soloNegativos = true;
		}
		return SUCCESS;
	}
	
	public void prepareIgualarPrima() {
		//se carga la cotización.
		cotizacion = endosoService.prepareCotizacionEndosoEV(getPolizaId(), TipoAccionDTO.getEditarEndosoCot());
	}
	
	@Action
	(value = "igualarPrima", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarIgualacionPrimas",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas",
					"polizaId","${polizaId}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"nextFunction","${nextFunction}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"
			}),
			@Result(name=INPUT,type="redirectAction", 
					params={"actionName","mostrarIgualacionPrimas",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas",
					"polizaId","${polizaId}",
					"mensaje","${mensaje}",
					"nextFunction","${nextFunction}",
					"tipoMensaje","${tipoMensaje}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"
			})
	}) //TODO Definir Result
	public String igualarPrima() 
	{
		calculoService.igualarPrimaEndoso(new Long(cotizacion.getContinuity().getNumero()), 
				primaAIgualar);
		super.setMensajeExito();
		return SUCCESS;
	}
	

	public void prepareRevertirIgualacionPrima() {
		//se carga la cotización.
		cotizacion = endosoService.prepareCotizacionEndosoEV(getPolizaId(),  TipoAccionDTO.getEditarEndosoCot());
	}
	
	@Action
	(value = "revertirIgualacionPrima", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarIgualacionPrimas",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas",
					"polizaId","${polizaId}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"nextFunction","${nextFunction}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"
			}),
			@Result(name=INPUT,type="redirectAction", 
					params={"actionName","mostrarIgualacionPrimas",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/igualacionPrimas",
					"polizaId","${polizaId}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"nextFunction","${nextFunction}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"
			})
	}) //TODO Definir Result
	public String revertirIgualacionPrima() 
	{
		calculoService.restaurarPrimaEndosoMovimiento(new BigDecimal(cotizacion.getContinuity().getNumero()));		
		super.setMensajeExito();
		return SUCCESS;
	}
	
	
	public BigDecimal getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}


	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public void setPrimaAIgualar(BigDecimal primaAIgualar) {
		this.primaAIgualar = primaAIgualar;
	}
	public BigDecimal getPrimaAIgualar() {
		return primaAIgualar;
	}

	private CalculoService calculoService;
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	private EndosoService endosoService;
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	private EntidadService entidadService;

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setSoloNegativos(Boolean soloNegativos) {
		this.soloNegativos = soloNegativos;
	}

	public Boolean getSoloNegativos() {
		return soloNegativos;
	}
}
