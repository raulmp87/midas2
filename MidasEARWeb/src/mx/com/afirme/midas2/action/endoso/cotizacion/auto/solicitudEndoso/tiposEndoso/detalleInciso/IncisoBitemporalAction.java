package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso.detalleInciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import mx.com.afirme.midas.catalogos.valorfijo.ValorFijoDN;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.cobertura.CoberturaBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso")
public class IncisoBitemporalAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1835041344943759953L;
	private static String datosRiesgoExpresion = "datosRiesgo";
	private Long polizaId;
	private Integer tipoEndoso;
	private String accionEndoso;
	private Date validoEn;
	private String name;
	private Map<String, String> valores;
	private List<ControlDinamicoRiesgoDTO> controles;
	private Map<String, String> datosRiesgo;
	private BitemporalCotizacion bitemporalCotizacion;
	private BitemporalInciso bitemporalInciso = new BitemporalInciso();
	private BitemporalAutoInciso bitemporalAutoInciso = new BitemporalAutoInciso();
	private List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionList;
	private Collection<BitemporalCoberturaSeccion> biCoberturaSeccionList;
	private Collection<BitemporalInciso> incisosInProcessList;
	private int numeroCopias;
	private int accionRecalcular = 0;
	
	private Double primaTotalAIgualar;
	
	private Map<Object, String> diasSalario = new LinkedHashMap<Object, String>();
	private static final int ID_CATALOGO_DIAS_SALARIO = 338;
	
	@Autowired
	@Qualifier("calculoIncisoAutosServiceEJB")
	private CalculoService calculoService;

	@Override
	public void prepare() throws Exception {
		obtenerDiasSalario();
	}
	
	public void obtenerDiasSalario(){
		try {
			List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = ValorFijoDN
					.getInstancia()
					.buscarPorPropiedad(
							"id.idGrupoValores",
							Integer.valueOf(ID_CATALOGO_DIAS_SALARIO));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
					for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
						diasSalario.put(estatus.getId().getIdDato(),
								estatus.getDescripcion());
					}
			}
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}
	
	@Action
	(value = "verDetalleInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/contenedorVehiculoBitemporal.jsp") })
	public String verDetalleInciso(){
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), TimeUtils.getDateTime(validoEn));
		if(tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO)
		{		
			
			bitemporalInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalInciso.class, bitemporalInciso.getContinuity().getId(), bitemporalCotizacion.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
			if(bitemporalInciso.getContinuity().getAutoIncisoContinuity() != null)
			{
				bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalInciso.getContinuity().getAutoIncisoContinuity().getId(), bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));			
			}
			else
			{
				bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, new AutoIncisoContinuity().getId(), bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));							
			}
			
		}
		else
		{
			bitemporalInciso = entidadBitemporalService.getByKey(IncisoContinuity.class,bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
			bitemporalAutoInciso = entidadBitemporalService.getByKey(AutoIncisoContinuity.class,bitemporalInciso.getContinuity().getAutoIncisoContinuity().getId(),TimeUtils.getDateTime(validoEn));
		}
		
		return SUCCESS;
	}
	
	public void prepareCalcularInciso(){
		
		if(tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO)
		{
			bitemporalInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalInciso.class, bitemporalInciso.getContinuity().getId(), bitemporalCotizacion.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
			bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalAutoInciso.getContinuity().getId(), bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));	
		}
		else
		{
			bitemporalInciso = entidadBitemporalService.getByKey(IncisoContinuity.class,bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
			bitemporalAutoInciso = entidadBitemporalService.getByKey(AutoIncisoContinuity.class,bitemporalInciso.getContinuity().getAutoIncisoContinuity().getId(),TimeUtils.getDateTime(validoEn));
		}
		 
	}
	
	@Action
	(value = "calcularInciso", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","verDetalleInciso",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso",
						"polizaId","${polizaId}",
						"validoEn","${validoEn}",
						"accionEndoso","${accionEndoso}",
						"tipoEndoso","${tipoEndoso}",
						"bitemporalInciso.continuity.id","${bitemporalInciso.continuity.id}",
						"nextFunction","${nextFunction}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String calcularInciso(){
		if(bitemporalAutoInciso.getValue().getEstadoId() != null){
			if(bitemporalAutoInciso.getValue().getEstadoId().trim().equals("")) {
				bitemporalAutoInciso.getValue().setEstadoId(null);
			}
		}
		if(bitemporalAutoInciso.getValue().getMunicipioId() != null){
			if(bitemporalAutoInciso.getValue().getMunicipioId().trim().equals("")) {
				bitemporalAutoInciso.getValue().setMunicipioId(null);
			}
		}
		if(bitemporalAutoInciso.getValue().getModificadoresDescripcion() != null){
			if(bitemporalAutoInciso.getValue().getModificadoresDescripcion().trim().equals("")) {
				bitemporalAutoInciso.getValue().setModificadoresDescripcion(null);
			}
		}
		bitemporalInciso = incisoService.prepareGuardarIncisoBorrador(bitemporalInciso, bitemporalAutoInciso, datosRiesgo, bitemporalCoberturaSeccionList, TimeUtils.getDateTime(validoEn));
		try{
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
			bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), TimeUtils.getDateTime(validoEn));
			
			int riesgosValidos = configuracionDatoIncisoService.validaDatosComplementariosIncisoBorrador(bitemporalCotizacion, bitemporalInciso, tipoEndoso.shortValue(), validoEn);
			if(riesgosValidos == 1){
				setNextFunction("mostrarDatosRiesgo("
						+ bitemporalInciso.getContinuity().getId()
						+ ", '" + mx.com.afirme.midas.sistema.UtileriasWeb.getFechaString(validoEn) + "', " + tipoEndoso + " , '" + accionEndoso + "' "
						+ ");ocultarBotonGuardar();jQuery('#btnDatosAdicionalesPaquete').show()");
					
				setMensajeExitoPersonalizado("Faltan guardar los datos adicionales del paquete");
			}else if(riesgosValidos == 2){
					setNextFunction("mostrarDatosConductor("
							+ bitemporalInciso.getContinuity().getId()
							+ ", '" + mx.com.afirme.midas.sistema.UtileriasWeb.getFechaString(validoEn) + "', " + accionEndoso + " , '" + tipoEndoso + "' "
							+ ");ocultarBotonGuardar();jQuery('#btnDatosConductor').show()");
						
					setMensajeExitoPersonalizado("Faltan guardar los datos del conductor");
			}else{
				super.setNextFunction(null);
				//Mantiene descuendo de igualacion en caso de ser endoso de movimientos
				if(accionEndoso != TipoAccionDTO.getConsultarEndosoCot() && tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS && tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO){
					calculoService.restaurarPrimaEndoso(new BigDecimal(bitemporalCotizacion.getContinuity().getNumero()),TimeUtils.getDateTime(validoEn), tipoEndoso);
				}
				bitemporalInciso = calculoService.calcular(bitemporalInciso, TimeUtils.getDateTime(validoEn), tipoEndoso, false);	
				setMensajeExito();
			}
		}catch(Exception e){
			
		}
				
		return SUCCESS;
		
	}
	
	public void prepareAsociarIncisoCotizacion()
	{
		bitemporalInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalInciso.class, bitemporalInciso.getContinuity().getId(), bitemporalCotizacion.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
		bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalAutoInciso.getContinuity().getId(), bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));		
	}
	
	
	@Action
	(value = "asociarIncisoCotizacion", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/contenedorVehiculoBitemporal.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/contenedorVehiculoBitemporal.jsp")})
	public String asociarIncisoCotizacion(){
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		if(tipoEndoso!=SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS){
			incisoService.asociarIncisoEnCotizacion(bitemporalInciso, TimeUtils.getDateTime(validoEn));
		}
		setMensajeExito();
		return SUCCESS;
		
	}
	
	@Action
	(value = "eliminarInciso", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","verDetalleTipoEndoso",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"tipoEndoso","${tipoEndoso}",
						"idPolizaBusqueda","${polizaId}",
						"fechaIniVigenciaEndoso","${validoEn}",
						"accionEndoso","${accionEndoso}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String eliminarInciso(){
		
		incisoService.bajaInciso(bitemporalInciso.getEntidadContinuity().getId(), TimeUtils.getDateTime(validoEn));
		incisoService.reasignarSecuencia(bitemporalInciso.getContinuity().getCotizacionContinuity().getId(), bitemporalInciso.getContinuity().getNumero());
		
		setMensajeExito();
		return SUCCESS;
		
	}
	
	@Action
	(value = "mostrarCoberturasInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/coberturas/coberturaIncisoGrid.jsp"),
			@Result(name = "detalleAlterno", location= "/jsp/poliza/auto/incisos/coberturaCotizacionGrid.jsp")})
	public String mostrarCoberturasInciso(){
		
		DateTime dtValidoEn = TimeUtils.getDateTime(validoEn);
		
		if(bitemporalAutoInciso.getValue().getEstadoId() != null){
			if(bitemporalAutoInciso.getValue().getEstadoId().trim().equals("")) {
				bitemporalAutoInciso.getValue().setEstadoId(null);
			}
		}
		if(bitemporalAutoInciso.getValue().getMunicipioId() != null){
			if(bitemporalAutoInciso.getValue().getMunicipioId().trim().equals("")) {
				bitemporalAutoInciso.getValue().setMunicipioId(null);
			}
		}
		if(bitemporalAutoInciso.getValue().getModificadoresDescripcion() != null){
			if(bitemporalAutoInciso.getValue().getModificadoresDescripcion().trim().equals("")) {
				bitemporalAutoInciso.getValue().setModificadoresDescripcion(null);
			}
		}
		
		if(tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO)
		{
			bitemporalAutoInciso.getEntidadContinuity().getParentContinuity().setId(bitemporalInciso.getEntidadContinuity().getId());
			bitemporalCoberturaSeccionList = coberturaBitemporalService.mostrarCoberturas(bitemporalAutoInciso, bitemporalCotizacion.getContinuity().getId(), dtValidoEn);
			return SUCCESS;	
		}
		else
		{
			biCoberturaSeccionList = incisoViewService.getLstCoberturasByInciso(bitemporalInciso.getEntidadContinuity().getId(), validoEn);
			return "detalleAlterno";
		}
	
	}
	
	
	@SuppressWarnings("unchecked")
	@Action
	(value = "cargaDatosRiesgoInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/controlDinamicoRiesgoDetalle.jsp") })
	public String cargaDatosRiesgoInciso(){
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		if (name == null) {
			name="datosRiesgo";
		}
		valores = (Map<String, String>) valueStack.findValue(name,Map.class);
		if (valores == null) {
			valores = new LinkedHashMap<String, String>();
		}
		
		if (tipoEndoso != null) {
			controles = configuracionDatoIncisoService.getDatosRiesgo(bitemporalInciso.getContinuity().getId(), validoEn, 
																			valores, tipoEndoso.shortValue(), accionEndoso, true);
		} else {
			controles = configuracionDatoIncisoService.getDatosRiesgoCotizacion(bitemporalInciso.getContinuity().getId(), 
																						validoEn, valores, accionEndoso, true);
		}
		
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarControlDinamicoRiesgo", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/detalleInciso/datosRiesgo/contenedorRiesgoBitemporal.jsp") })
	public String mostrarControlDinamicoRiesgo() {
		if (accionEndoso != null && accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot())) {
			accionEndoso = TipoAccionDTO.getEditar();
		}
		return SUCCESS;
	}
	
	
	@Action
	(value = "guardarDatosRiesgo", results = {								 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarControlDinamicoRiesgo",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/detalleInciso",
						"bitemporalInciso.continuity.id","${bitemporalInciso.continuity.id}",
						"validoEn","${validoEn}",
						"mensaje","${mensaje}",
						"tipoEndoso","${tipoEndoso}",
						"tipoMensaje","${tipoMensaje}",
						"nextFunction","${nextFunction}"})})
	public String guardarDatosRiesgo(){
		validarDatosRiesgo();
		//Si no existen errores guarda la Configuracion de Dato Inciso
		if(getFieldErrors().isEmpty()){
			//Obtener los controles dinamicos y poner el valor al control que tenemos en el map.
			//TODO Cambiar a servicios y entidades de bitemporalidad
			/*incisoService.guardarDatosAdicionalesPaquete(datosRiesgo, new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()), 
					inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso());*/
			
			configuracionDatoIncisoService.guardarDatosAdicionalesPaquete(datosRiesgo, 
													bitemporalInciso.getContinuity().getId(), validoEn);
			setMensajeExito();
			setNextFunction("cerrarRiesgoRefrescarInciso()");
		}else{			
			super.setMensajeListaPersonalizado("Error al validar Datos del Paquete", 
					new ArrayList<String>(1), BaseAction.TIPO_MENSAJE_ERROR);
			return INPUT;
		}
		
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarMultiplicarInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/multiplicarInciso.jsp") })
	public String mostrarMultiplicarInciso(){
		return SUCCESS;
	}
	
	@Action
	(value = "multiplicarInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/multiplicarInciso.jsp") })
	public String multiplicarInciso(){
		
		int restan = numeroCopias;
		try {
			for (int i = 0; i < numeroCopias; i++) {
				incisoService.multiplicarInciso(bitemporalInciso.getEntidadContinuity().getId(), TimeUtils.getDateTime(validoEn));
				restan--;
			}
		} catch (RuntimeException e) {
			setNumeroCopias(restan);
			super.setMensajeError("Ocurrio un error en el proceso de copiado, restan " + restan + " incisos por copiar.");
			throw e;
		}
		setMensajeExito();
		
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarIgualarInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarInciso.jsp") })
	public String mostrarIgualarInciso() {
		return SUCCESS;
	}
	
	@Action
	(value = "igualarInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarInciso.jsp"), 
			@Result(name = INPUT, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarInciso.jsp")})
	public String igualarInciso() {
		LogDeMidasWeb.log("igualarInciso", Level.FINEST, null);		
		
		calculoService.generarCoberturasInProcessParaIgualacionPrimas(bitemporalInciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
				
		calculoService.igualarPrimaIncisoEndoso(bitemporalInciso.getContinuity().getId(), primaTotalAIgualar,
				TimeUtils.getDateTime(validoEn),tipoEndoso);
		setMensajeExito();
		setNextFunction("funcionRefrescarAltaInciso()");
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarIgualarMasivo", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarMasivo.jsp") })
	public String mostrarIgualarMasivo() {
		//Obtiene total de incisos
		incisosInProcessList = new ArrayList<BitemporalInciso>(1);
		try{
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("continuity.cotizacionContinuity.numero", polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue());		
			incisosInProcessList = entidadBitemporalService.listarFiltrado(BitemporalInciso.class, params, TimeUtils.getDateTime(validoEn), null, true, "continuity.numero");

		}catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action
	(value = "igualarMasivo", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarMasivo.jsp"), 
			@Result(name = INPUT, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarMasivo.jsp")})
	public String igualarMasivo() {
		//LogDeMidasWeb.log("igualarMasivo", Level.FINEST, null);		
		if(incisosInProcessList != null && !incisosInProcessList.isEmpty()){
			for(BitemporalInciso inciso : incisosInProcessList){
				if(inciso != null && inciso.getContinuity() != null && inciso.getContinuity().getId() != null &&
						inciso.getValue() != null && inciso.getValue().getValorPrimaIgualacion() != null && inciso.getValue().getValorPrimaIgualacion() > 0){
					
					calculoService.generarCoberturasInProcessParaIgualacionPrimas(inciso.getContinuity().getId(),TimeUtils.getDateTime(validoEn));
							
					calculoService.igualarPrimaIncisoEndoso(inciso.getContinuity().getId(), inciso.getValue().getValorPrimaIgualacion(),
							TimeUtils.getDateTime(validoEn),tipoEndoso);
				}
			}
			setMensajeExito();
			setNextFunction("funcionRefrescarAltaInciso()");
		}				
		return SUCCESS;
	}
	
	
	@Action
	(value = "restaurarPrimaInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarInciso.jsp"), 
			@Result(name = INPUT, location = "/jsp/suscripcion/cotizacion/auto/detalle/igualarInciso.jsp")})
	public String restaurarPrimaInciso() {
		LogDeMidasWeb.log("igualarInciso", Level.FINEST, null);		
		
		calculoService.restaurarPrimaIncisoEndoso(bitemporalInciso,TimeUtils.getDateTime(validoEn),tipoEndoso, false);
		setMensajeExito();
		setNextFunction("funcionRefrescarAltaInciso()");
		return SUCCESS;
	}

	@SuppressWarnings("unused")
	private void validarDatosRiesgo() {
		//Mapa en donde se guarda la dependencia a partir de un id padre se obtiene el id hijo
		Map<String, String> childs = new LinkedHashMap<String, String>();
		//Mapa en donde se guarda la dependencia a partir de un id hijo se obtiene el id padre
		Map<String, String> parents = new LinkedHashMap<String, String>();
		//Lista en donde se guardan los id de los elementos que ya han sido procesados
		List<String> processedElements = new ArrayList<String>();
		List<ControlDinamicoRiesgoDTO> controlDinamicos = new ArrayList<ControlDinamicoRiesgoDTO>();

		//Obtener los controles dinamicos y poner el valor al control que tenemos en el map.
		for(Entry<String, String> entry : datosRiesgo.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			ConfiguracionDatoIncisoId id = configuracionDatoIncisoService.getConfiguracionDatoIncisoId(key); 
			
			ControlDinamicoRiesgoDTO controlDinamicoRiesgoDTO = configuracionDatoIncisoService.findByConfiguracionDatoIncisoId(id);
			
			if (controlDinamicoRiesgoDTO != null) { //Si no es null quiere decir que el control debe ser validado.
				controlDinamicoRiesgoDTO.setValor(value);
				controlDinamicos.add(controlDinamicoRiesgoDTO); // Lo agregamos a la lista para que sea procesado.
			}
		}
		
		for (ControlDinamicoRiesgoDTO control: controlDinamicos) {
			String id = control.getId();
			//1. Validar si el control no ha sido procesado
			if (!processedElements.contains(id)) {
					validarPorTipoValidador(control);
					processedElements.add(id);
			}
		}
	}
	
	private void validarPorTipoValidador(ControlDinamicoRiesgoDTO controlDinamicoDTO) {
		String id = controlDinamicoDTO.getId();
		String valor = controlDinamicoDTO.getValor();
		Integer tipoValidador = controlDinamicoDTO.getTipoValidador();
		
		//Ahorita en este metodo se va a validar para todos los controles requerido.
		if (StringUtils.isNotBlank(valor)) {
			if (tipoValidador != null) {
				switch (tipoValidador){
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS: 
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 0);
					break;
				}			
			}
		} else {
			addFieldError(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
					getText("error.requerido"));
		}
	}
	
	private void validarDecimal(String fieldName, String numero, int maxEnteros, int maxDecimales) {
		if (!UtileriasWeb.isDecimalValido(numero, maxEnteros, maxDecimales)) {
			addFieldError(fieldName, getText("error.decimal", 
					new String[] { String.valueOf(maxEnteros), String.valueOf(maxDecimales) }));
		}
	}
	
	//verDetalleTipoEndosoOrigenInciso
	

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public Integer getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Integer tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getValores() {
		return valores;
	}

	public void setValores(Map<String, String> valores) {
		this.valores = valores;
	}

	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}

	public void setControles(List<ControlDinamicoRiesgoDTO> controles) {
		this.controles = controles;
	}

	public Map<String, String> getDatosRiesgo() {
		return datosRiesgo;
	}

	public void setDatosRiesgo(Map<String, String> datosRiesgo) {
		this.datosRiesgo = datosRiesgo;
	}

	public BitemporalCotizacion getBitemporalCotizacion() {
		return bitemporalCotizacion;
	}

	public void setBitemporalCotizacion(BitemporalCotizacion bitemporalCotizacion) {
		this.bitemporalCotizacion = bitemporalCotizacion;
	}

	public BitemporalInciso getBitemporalInciso() {
		return bitemporalInciso;
	}

	public void setBitemporalInciso(BitemporalInciso bitemporalInciso) {
		this.bitemporalInciso = bitemporalInciso;
	}

	public BitemporalAutoInciso getBitemporalAutoInciso() {
		return bitemporalAutoInciso;
	}

	public void setBitemporalAutoInciso(BitemporalAutoInciso bitemporalAutoInciso) {
		this.bitemporalAutoInciso = bitemporalAutoInciso;
	}
	
	public List<BitemporalCoberturaSeccion> getBitemporalCoberturaSeccionList() {
		return bitemporalCoberturaSeccionList;
	}

	public void setBitemporalCoberturaSeccionList(
			List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionList) {
		this.bitemporalCoberturaSeccionList = bitemporalCoberturaSeccionList;
	}

	private EntidadBitemporalService entidadBitemporalService;
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService;
	private EntidadService entidadService;
	private CoberturaBitemporalService coberturaBitemporalService;

	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("configuracionDatoIncisoBitemporalServiceEJB")
	public void setConfiguracionDatoIncisoService(ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("coberturaBitemporalServiceEJB")
	public void setCoberturaBitemporalService(
			CoberturaBitemporalService coberturaBitemporalService) {
		this.coberturaBitemporalService = coberturaBitemporalService;
	}
	
	private IncisoService incisoService;
	
	@Autowired
	@Qualifier("incisoEndosoServiceEJB")
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	private IncisoViewService incisoViewService;

	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}

	public Collection<BitemporalCoberturaSeccion> getBiCoberturaSeccionList() {
		return biCoberturaSeccionList;
	}

	public void setBiCoberturaSeccionList(
			Collection<BitemporalCoberturaSeccion> biCoberturaSeccionList) {
		this.biCoberturaSeccionList = biCoberturaSeccionList;
	}

	public int getNumeroCopias() {
		return numeroCopias;
	}

	public void setNumeroCopias(int numeroCopias) {
		this.numeroCopias = numeroCopias;
	}

	/**
	 * @return the calculoService
	 */
	public CalculoService getCalculoService() {
		return calculoService;
	}

	/**
	 * @param calculoService the calculoService to set
	 */
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	/**
	 * @return the primaTotalAIgualar
	 */
	public Double getPrimaTotalAIgualar() {
		return primaTotalAIgualar;
	}

	/**
	 * @param primaTotalAIgualar the primaTotalAIgualar to set
	 */
	public void setPrimaTotalAIgualar(Double primaTotalAIgualar) {
		this.primaTotalAIgualar = primaTotalAIgualar;
	}

	public void setAccionRecalcular(int accionRecalcular) {
		this.accionRecalcular = accionRecalcular;
	}

	public int getAccionRecalcular() {
		return accionRecalcular;
	}

	public void setDiasSalario(Map<Object, String> diasSalario) {
		this.diasSalario = diasSalario;
	}

	public Map<Object, String> getDiasSalario() {
		return diasSalario;
	}

	public void setIncisosInProcessList(Collection<BitemporalInciso> incisosInProcessList) {
		this.incisosInProcessList = incisosInProcessList;
	}

	public Collection<BitemporalInciso> getIncisosInProcessList() {
		return incisosInProcessList;
	}
}
