/**
 * 
 */
package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

/**
 * @author admin
 *
 */
@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial")
public class EndosoInclusionCondicionesEspecialesAction  extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String SEPARADOR_CONDICION_ESPECIAL		= ",";

	/**
	 * 
	 */
	
	private String accionEndoso;
	
	private String codigosInciso;
	
	private Date fechaIniVigenciaEndoso;
	
	private Short nivelAplicacion;
	
	private Integer polizaId;
	
	private Long cotizacionContinuityId;
	
	private PolizaDTO polizaDTO;
	
	private BitemporalCotizacion biCotizacion;
	
	private CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO;
	
	private String nombreCondicion;
	
	private List<CondicionEspecialBitemporalDTO> condicionesAsociadas;
	
	private List<CondicionEspecialBitemporalDTO> condicionesDisponibles;
	
	private EntidadService entidadService;
	
	private EndosoService endosoService;
	
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	private	List<ExcepcionSuscripcionReporteDTO> excepcionesList;
    private Long id;
	
	
	
	
	/*
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	@Autowired
	@Qualifier("endosoCondicionEspecialesServicesEJB")
	private EndosoCondicionesEspecialesService endosoCondicionesEspecialesService;*/
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}


	@Autowired
	@Qualifier("condicionEspecialBitemporalServiceEJB")
	public void setCondicionEspecialBitemporalService(CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}
	
	

	public EndosoInclusionCondicionesEspecialesAction(){
		

	}
	
	public void finalize() throws Throwable {
		super.finalize();
	}


	/*@Result(name= INPUT, type="redirectAction",
			params={"actionName","mostrarInclusionCondiciones","namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial",
					"mensaje","${mensaje}","tipoMensaje","${tipoMensaje}",							
					"polizaId","${polizaId}", "fechaIniVigenciaEndoso", "${fechaIniVigenciaEndoso}", "accionEndoso", "${accionEndoso}" })						
		})*/
	
	
	
	@Action
	(value = "accionRelacionarCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String accionRelacionarCondiciones(){
		
		NivelAplicacion nvlAplicacion			= getNivelAplicacionSelected();
		String accion 							= ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		List<Long> listIncisosContinuityId 		= null;	
		
		if (accion.equals(ROW_INSERTED)) {
			
			if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
				condicionEspecialBitemporalService.guardarCondicionCotizacion(cotizacionContinuityId, condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
			}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
				listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
				
				for(Long incisoContinuityId: listIncisosContinuityId){
					condicionEspecialBitemporalService.guardarCondicionInciso(incisoContinuityId, condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
				}
				
			}
			
		} else if (accion.equals(ROW_DELETED)) {
			
			if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
				condicionEspecialBitemporalService.eliminarCondicionCotizacion(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
			}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
				listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
				
				for(Long incisoContinuityId: listIncisosContinuityId){
					
					condicionEspecialBitemporalDTO.setIdContinuity(incisoContinuityId);
					condicionEspecialBitemporalService.eliminarCondicionIncisoInclusionEndoso(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
					
				}
				
			}
			
		}
		
		return SUCCESS;
	}
	
	
	
	@Action
	(value = "accionRelacionarTodasCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
		public String accionRelacionarTodasCondiciones(){
			NivelAplicacion nvlAplicacion			= getNivelAplicacionSelected();
			List<Long> listIncisosContinuityId 		= null;
			
			if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
				
				if(nombreCondicion == null){
					condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesDisponiblesCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
				}else{
					condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesCotizacionNombreCodigo(nombreCondicion, cotizacionContinuityId, fechaIniVigenciaEndoso);
				}
				for(CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : condicionesDisponibles){
					condicionEspecialBitemporalService.guardarCondicionCotizacion(cotizacionContinuityId, condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
				}
				
				
			}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
				listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
				this.condicionesDisponibles 	= new ArrayList<CondicionEspecialBitemporalDTO>();
				
				if(nombreCondicion == null){				
					for(Long incisoContinuityId:listIncisosContinuityId){
						this.condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesDisponiblesInciso(condicionesDisponibles, incisoContinuityId, fechaIniVigenciaEndoso) ;
						
						for(CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : condicionesDisponibles){
							condicionEspecialBitemporalService.guardarCondicionInciso(incisoContinuityId, condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
						}
					}
					
				}else{
					for(Long incisoContinuityId:listIncisosContinuityId){
						this.condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesIncisoNombreCodigo(nombreCondicion, incisoContinuityId, fechaIniVigenciaEndoso) ;
						
						for(CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : condicionesDisponibles){
							condicionEspecialBitemporalService.guardarCondicionInciso(incisoContinuityId, condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
						}
						
					}
					
				}
				
				
			}
		 return SUCCESS;	
			
		}
	
	
	
	@Action (value = "buscarCondicion", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecialDisponibleGrid.jsp"),
			@Result(name = INPUT,   location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String buscarCondicion() {	
		
		NivelAplicacion nvlAplicacion			= getNivelAplicacionSelected();
		List<Long> listIncisosContinuityId 		= null;
		
		if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
			condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesCotizacionNombreCodigo(nombreCondicion, cotizacionContinuityId, fechaIniVigenciaEndoso);
		}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
			
			listIncisosContinuityId = getListIncisosContinuityId(codigosInciso);
			this.condicionesDisponibles 	= new ArrayList<CondicionEspecialBitemporalDTO>();
			
			for(Long incisoContinuityId:listIncisosContinuityId){
				this.condicionesDisponibles.addAll( condicionEspecialBitemporalService.obtenerCondicionesIncisoNombreCodigo(nombreCondicion, incisoContinuityId, fechaIniVigenciaEndoso) );
			}
			
		}
	 return SUCCESS;	
	}
	
	

	@Action (value = "cancelar", results = {
			@Result(name=SUCCESS, type="redirectAction", params={
				"actionName","mostrarDefTipoModificacion",
				"namespace","/endoso/cotizacion/auto/solicitudEndoso",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"esRetorno", "${esRetorno}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
			}),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
		})
		public String cancelar() {
			//TODO 
			System.out.println("cancelar()");
			this.setEsRetorno(1);
			return SUCCESS;
		}


	/**
	 * Método que eliminará las condiciones especiales asociadas, al momento de hacer
	 * clic en el botón Limpiar o cambiar de nivel de aplicación.
	 * Invocar al método removerInclusiones de EndosoCondicionesEspecialesService
	 */
	@Action
	(value = "eliminarPrevios", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String eliminarPrevios(){
		NivelAplicacion nvlAplicacion 									= getNivelAplicacionSelected();
		List<CondicionEspecialBitemporalDTO> listCondicionesEliminar 	= null;
		List<Long> listIncisosContinuityId 								= null;
		
		
	if( NivelAplicacion.POLIZA.equals(nvlAplicacion)){
		listCondicionesEliminar =  condicionEspecialBitemporalService.obtenerCondicionEspCotInProcess(cotizacionContinuityId, fechaIniVigenciaEndoso);
		
		for( CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : listCondicionesEliminar ){
			condicionEspecialBitemporalService.eliminarCondicionCotizacion(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
		}
			
		}else if( NivelAplicacion.INCISO.equals(nvlAplicacion)){
			
			listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
			
			for(Long incisoContinuityId: listIncisosContinuityId){
				listCondicionesEliminar =  condicionEspecialBitemporalService.obtenerCondicionEspIncInProcess(incisoContinuityId, fechaIniVigenciaEndoso);
				
				for( CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : listCondicionesEliminar ){
					condicionEspecialBitemporalService.eliminarCondicionIncisoInclusionEndoso(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
				}
			}
			
			
			
		}
		
		
		return SUCCESS;
	}

	
	
	/**
	 * Método que prepara el mostrar la pantalla de Inclusión.
	 * Preparar el bitemporal de Cotizacion con el método
	 * getCotizacionEndosoInclusionCondiciones de EndosoService
	 */
	@Action
	(value = "mostrarInclusionCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String mostrarInclusionCondiciones(){
		polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		biCotizacion = endosoService.getCotizacionEndosoInclusionCondiciones(new BigDecimal(polizaId), fechaIniVigenciaEndoso, accionEndoso);

		if(this.nivelAplicacion == null){
			this.nivelAplicacion = NivelAplicacion.POLIZA.getNivel();
		}
		return SUCCESS;
	}
	
	
	
	//NivelAplicacion nvlAplicacion 				= nivelAplicacion == null ? NivelAplicacion.POLIZA : NivelAplicacion.INCISO;
	
	@Action
	(value = "obtenerCondicionesAsociadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspeccialAsociadasGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String obtenerCondicionesAsociadas(){	
		
		NivelAplicacion nvlAplicacion 				= getNivelAplicacionSelected();
		List<Long> listIncisosContinuityId 			= null;	
		this.condicionesAsociadas 					= new ArrayList<CondicionEspecialBitemporalDTO>();
		
		if( NivelAplicacion.POLIZA.equals(nvlAplicacion)){
			if(cotizacionContinuityId != null){
				this.condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesAsociadasCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
			}
			
		}else if( NivelAplicacion.INCISO.equals(nvlAplicacion)){
			
			listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
			this.condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesIncisoAsociadas(listIncisosContinuityId, fechaIniVigenciaEndoso);
			
		}
		
		
		return SUCCESS;
	}




	@Action
	(value = "obtenerCondicionesDisponibles", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecialDisponibleGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String obtenerCondicionesDisponibles(){
		
		NivelAplicacion nvlAplicacion 				= getNivelAplicacionSelected();
		List<Long> listIncisosContinuityId 			= null;
		this.condicionesDisponibles 				= new ArrayList<CondicionEspecialBitemporalDTO>();
	
		if( NivelAplicacion.POLIZA.equals(nvlAplicacion)){
			if(cotizacionContinuityId != null){
				this.condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesDisponiblesCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
			}
		}else if( NivelAplicacion.INCISO.equals(nvlAplicacion)){
			
			listIncisosContinuityId = getListIncisosContinuityId(codigosInciso);
			
			for(Long incisoContinuityId:listIncisosContinuityId){
				condicionesDisponibles = condicionEspecialBitemporalService.obtenerCondicionesDisponiblesInciso(
														condicionesDisponibles, incisoContinuityId, fechaIniVigenciaEndoso) ;
			}
			
		}
		
		return SUCCESS;
	}
	
	
	

	@Override
	public void prepare() throws Exception {
		 polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		 
	}

	/**
	 * Método que prepara el bitemporal de la Cotización, utilizar el método
	 * prepareCotizacionEndoso de EndosoService
	 */
	public void prepareCotizar(){
		biCotizacion = endosoService.prepareCotizacionEndoso(cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	
	@Action						
	(value = "cotizar", results = { 
			@Result(name=SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp")
	})
	public String cotizar() {		
		endosoService.guardaCotizacionInclusionCondiciones(biCotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		setMensajeExito();
		return SUCCESS;		
	}

	/**
	 * Método que prepara el bitemporal de la Cotización, utilizar el método
	 * prepareCotizacionEndoso de EndosoService
	 */
	public void prepareEmitir(){
		biCotizacion = endosoService.prepareCotizacionEndoso(cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}"}),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/endosoInclusionCondicionEspecial.jsp"),
			@Result(name=ERROR, location = "/jsp/suscripcion/cotizacion/auto/resultadoTerminarCotizacion.jsp")
						
	})
	public String emitir() {
		
		System.out.println("PROBANDO");
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.validarEmisionCotizacionCondicionesGenerales(biCotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		if (terminarCotizacionDTO != null) {
			excepcionesList = 	terminarCotizacionDTO.getExcepcionesList();
			id = biCotizacion.getContinuity().getNumero().longValue();
			super.setMensajeError(terminarCotizacionDTO.getMensajeError());				
			return ERROR;			
		}
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(biCotizacion.getContinuity().getId(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso), biCotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO + String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		return SUCCESS;		
	}


	
	
	/** ********************************************************
	 ** ******************* GETTER & SETTER ********************
	 ** ******************************************************** */

	public String getAccionEndoso() {
		return accionEndoso;
	}


	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}


	public String getCodigosInciso() {
		return codigosInciso;
	}


	public void setCodigosInciso(String codigosInciso) {
		this.codigosInciso = codigosInciso;
	}


	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}


	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}


	public Short getNivelAplicacion() {
		return nivelAplicacion;
	}


	public void setNivelAplicacion(Short nivelAplicacion) {
		this.nivelAplicacion = nivelAplicacion;
	}

	public void setNivelAplicacion(String nivelAplicacion) {
		this.nivelAplicacion = Short.valueOf(nivelAplicacion);
	}

	public Integer getPolizaId() {
		return polizaId;
	}


	public void setPolizaId(Integer polizaId) {
		this.polizaId = polizaId;
	}


	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}


	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}


	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}


	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public List<CondicionEspecialBitemporalDTO> getCondicionesAsociadas() {
		return condicionesAsociadas;
	}

	public void setCondicionesAsociadas(
			List<CondicionEspecialBitemporalDTO> condicionesAsociadas) {
		this.condicionesAsociadas = condicionesAsociadas;
	}

	public List<CondicionEspecialBitemporalDTO> getCondicionesDisponibles() {
		return condicionesDisponibles;
	}

	public void setCondicionesDisponibles(
			List<CondicionEspecialBitemporalDTO> condicionesDisponibles) {
		this.condicionesDisponibles = condicionesDisponibles;
	}	

	

	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}
	
	

	public CondicionEspecialBitemporalDTO getCondicionEspecialBitemporalDTO() {
		return condicionEspecialBitemporalDTO;
	}

	public void setCondicionEspecialBitemporalDTO(
			CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO) {
		this.condicionEspecialBitemporalDTO = condicionEspecialBitemporalDTO;
	}

	public String getNombreCondicion() {
		return nombreCondicion;
	}

	public void setNombreCondicion(String nombreCondicion) {
		this.nombreCondicion = nombreCondicion;
	}
	
	
	
	
	
	private NivelAplicacion getNivelAplicacionSelected(){
		NivelAplicacion nvlAplicacion = null;
		
		
		if(nivelAplicacion != null){
			if(nivelAplicacion == NivelAplicacion.POLIZA.getNivel()){
				nvlAplicacion = NivelAplicacion.POLIZA;
			}else if (nivelAplicacion == NivelAplicacion.INCISO.getNivel()){
				nvlAplicacion =  NivelAplicacion.INCISO;
			}
			
		}else{
			nvlAplicacion = NivelAplicacion.POLIZA;
		}
		
		return nvlAplicacion;
	}

	
	
	private List<Long> getListIncisosContinuityId(String codigosInciso){
		
		List<Long> listIncisosContinuityId 	= new ArrayList<Long>();
		String [] incisos 					= codigosInciso.split( SEPARADOR_CONDICION_ESPECIAL );
		int inciso 							= 0;
		int tamano 							= incisos.length;
		Long incisoContinuityId 			= null;
		
		
			for(int iterador = 0 ; iterador <= tamano -1; iterador++){
				inciso = Integer.parseInt(  incisos[iterador] );
				
				incisoContinuityId = condicionEspecialBitemporalService.getIdIncisoContinuity(inciso, cotizacionContinuityId, fechaIniVigenciaEndoso);
				listIncisosContinuityId.add(incisoContinuityId);
			}
		
		return listIncisosContinuityId;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public void setExcepcionesList(
			List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	
	
}
