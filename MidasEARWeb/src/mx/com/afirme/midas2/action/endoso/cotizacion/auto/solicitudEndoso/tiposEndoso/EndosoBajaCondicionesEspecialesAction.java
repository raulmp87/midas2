/**
 * 
 */
package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

/**
 * @author admin
 *
 */
@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial")
public class EndosoBajaCondicionesEspecialesAction  extends BaseAction implements Preparable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String SEPARADOR_CONDICION_ESPECIAL		= ",";

	/**
	 * 
	 */
	
	private String accionEndoso;
	private String codigosInciso;
	private Date fechaIniVigenciaEndoso;
	private Short nivelAplicacion;
	private Integer polizaId;
	private PolizaDTO polizaDTO;
	private BitemporalCotizacion biCotizacion;
	private boolean alta;
	private Long cotizacionContinuityId;
	private CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO;
	
	
	private List<CondicionEspecialBitemporalDTO> condicionesAsociadas;
	private List<CondicionEspecialBitemporalDTO> condicionesEliminadas;
	
	private EntidadService entidadService;
	private EndosoService endosoService;
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	
	
	
	/*@Autowired
	@Qualifier("condicionEspecialBitemporalServiceEJB")
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	@Autowired
	@Qualifier("endosoCondicionEspecialesServicesEJB")
	private EndosoCondicionesEspecialesService endosoCondicionesEspecialesService;*/
	
	
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}


	@Autowired
	@Qualifier("condicionEspecialBitemporalServiceEJB")
	public void setCondicionEspecialBitemporalService(CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}




	public EndosoBajaCondicionesEspecialesAction(){
		
	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	/**
	 * Método con la lógica para asociar o desasociar una condición especial. Invocar
	 * el método accionInclusionCondicion de EndosoCondicionesEspecialesService
	 */
	@Action
	(value = "accionEliminarRelacionCondicion", results = { 
			@Result(name = SUCCESS, location = "/jsp/relaciones/respuestaAsociacion.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
	public String accionEliminarRelacionCondicion(){
		NivelAplicacion nvlAplicacion			= getNivelAplicacionSelected();
		String accion 							= ServletActionContext.getRequest().getParameter("!nativeeditor_status");
		List<Long> listIncisosContinuityId 		= null;	
		
		if (accion.equals(ROW_INSERTED)) {
			
			if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
				condicionEspecialBitemporalService.eliminarCondicionCotizacion(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
			}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
				listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
				
				for(Long incisoContinuityId: listIncisosContinuityId){
					
					condicionEspecialBitemporalDTO.setIdContinuity(incisoContinuityId);
					condicionEspecialBitemporalService.eliminarCondicionIncisoBajaEndoso(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
				}
				
			}
			
		} else if (accion.equals(ROW_DELETED)) {
			
			if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
				condicionEspecialBitemporalService.asociarNuevamenteCondicionCotizacion(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
			}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
				listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
				
				for(Long incisoContinuityId: listIncisosContinuityId){
					condicionEspecialBitemporalDTO.setIdContinuity(incisoContinuityId);
					condicionEspecialBitemporalService.asociarNuevamenteCondicionIncisoBajaEndoso(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
				}
				
			}
			
		}
		
		return SUCCESS;
	}
	
	
	

	
	@Action (value = "cancelar", results = {
			@Result(name=SUCCESS, type="redirectAction", params={
				"actionName","mostrarDefTipoModificacion",
				"namespace","/endoso/cotizacion/auto/solicitudEndoso",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"esRetorno", "${esRetorno}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
			}),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
		})
		public String cancelar() {
			//TODO 
			System.out.println("cancelar()");
			this.setEsRetorno(1);
			return SUCCESS;
		}
	
	
	
	
	@Action
	(value = "accionEliminarTodasCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
		public String accionEliminarTodasCondiciones(){
			NivelAplicacion nvlAplicacion			= getNivelAplicacionSelected();
			List<Long> listIncisosContinuityId 		= null;
			
			if( NivelAplicacion.POLIZA.equals(nvlAplicacion) ){
				
				this.condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesAsociadasCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
					
				
				for(CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : condicionesAsociadas){
					if(condicionEspecialBitemporalDTO.getObligatoria().equals(0)){
						condicionEspecialBitemporalService.eliminarCondicionCotizacion(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
					}
				}
				
				
			}else if ( NivelAplicacion.INCISO.equals(nvlAplicacion) ){
				
				listIncisosContinuityId 		= getListIncisosContinuityId(codigosInciso);
				
				for(Long  incisoContinuityId : listIncisosContinuityId){
					this.condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesIncisoAsociadas( incisoContinuityId , fechaIniVigenciaEndoso);
					
					for(CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO :condicionesAsociadas){
						if(condicionEspecialBitemporalDTO.getObligatoria().equals(0)){
							condicionEspecialBitemporalDTO.setIdContinuity(incisoContinuityId);
							condicionEspecialBitemporalService.eliminarCondicionIncisoBajaEndoso(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
						}
					}
				}
			}
		 return SUCCESS;	
			
		}
	
	
	


	/**
	 * Método que eliminará las condiciones especiales asociadas, al momento de hacer
	 * clic en el botón Limpiar o cambiar de nivel de aplicación.
	 * Invocar al método removerInclusiones de EndosoCondicionesEspecialesService
	 */
	@Action
	(value = "eliminarPrevios", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
	public String eliminarPrevios(){
		NivelAplicacion nvlAplicacion 									= getNivelAplicacionSelected();
		List<CondicionEspecialBitemporalDTO> listCondicionesAsociar 	= null;
		List<Long> listIncisosContinuityId 								= null;
		
		
	if( NivelAplicacion.POLIZA.equals(nvlAplicacion)){
		listCondicionesAsociar = condicionEspecialBitemporalService.obtenerCondicionesEliminadasCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
		
		
		for( CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : listCondicionesAsociar ){
			condicionEspecialBitemporalService.asociarNuevamenteCondicionCotizacion(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
		}
			
		}else if( NivelAplicacion.INCISO.equals(nvlAplicacion)){
			
			listIncisosContinuityId = getListIncisosContinuityId(codigosInciso);
			this.condicionesEliminadas 	= new ArrayList<CondicionEspecialBitemporalDTO>();
			
			for(Long incisoContinuityId:listIncisosContinuityId){
				listCondicionesAsociar = condicionEspecialBitemporalService.obtenerCondicionesEliminadasInciso(incisoContinuityId, fechaIniVigenciaEndoso) ;
				
				for( CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO : listCondicionesAsociar ){
					condicionEspecialBitemporalDTO.setIdContinuity(incisoContinuityId);
					condicionEspecialBitemporalService.asociarNuevamenteCondicionIncisoBajaEndoso(condicionEspecialBitemporalDTO, fechaIniVigenciaEndoso);
				}
			}
			
			
		}
		
		
		return SUCCESS;
	}
	
	

	
	
	
	
	
	@Action
	(value = "mostrarBajaCondiciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
	public String mostrarBajaCondiciones(){
		polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		biCotizacion = endosoService.getCotizacionEndosoBajaCondiciones(new BigDecimal(polizaId), fechaIniVigenciaEndoso, accionEndoso);
		if(this.nivelAplicacion == null){
			this.nivelAplicacion = NivelAplicacion.POLIZA.getNivel();
		}
		return SUCCESS;
	}

	
	

	@Action
	(value = "obtenerCondicionesAsociadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecialAsociadasGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
	public String obtenerCondicionesAsociadas(){
		
		NivelAplicacion nvlAplicacion 				= getNivelAplicacionSelected();
		List<Long> listIncisosContinuityId 			= null;
		this.condicionesAsociadas 					= new ArrayList<CondicionEspecialBitemporalDTO>();
		
		if( NivelAplicacion.POLIZA.equals(nvlAplicacion)){
			if(cotizacionContinuityId != null){
				this.condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesAsociadasCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
			}
			
		}else if( NivelAplicacion.INCISO.equals(nvlAplicacion)){
			
			listIncisosContinuityId = getListIncisosContinuityId(codigosInciso);
			this.condicionesAsociadas = condicionEspecialBitemporalService.obtenerCondicionesIncisoAsociadas(listIncisosContinuityId, fechaIniVigenciaEndoso);
			
		}
		
		
		return SUCCESS;
	}

	
	@Action
	(value = "obtenerCondicionesEliminadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecialEliminadasGrid.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
	public String obtenerCondicionesEliminadas(){
		
		NivelAplicacion nvlAplicacion 				= getNivelAplicacionSelected();
		List<Long> listIncisosContinuityId 			= null;
		this.condicionesEliminadas 					= new ArrayList<CondicionEspecialBitemporalDTO>();
	
		if( NivelAplicacion.POLIZA.equals(nvlAplicacion)){
			if(cotizacionContinuityId != null){
				this.condicionesEliminadas = condicionEspecialBitemporalService.obtenerCondicionesEliminadasCotizacion(cotizacionContinuityId, fechaIniVigenciaEndoso);
			}
		}else if( NivelAplicacion.INCISO.equals(nvlAplicacion)){
			
			listIncisosContinuityId = getListIncisosContinuityId(codigosInciso);
			
			for(Long incisoContinuityId:listIncisosContinuityId){
				this.condicionesEliminadas.addAll( condicionEspecialBitemporalService.obtenerCondicionesEliminadasInciso(incisoContinuityId, fechaIniVigenciaEndoso) );
			}
			
		}
		
		return SUCCESS;
	}


	@Override
	public void prepare() throws Exception {
		 polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		 
	}

	/**
	 * Método que prepara el bitemporal de la Cotización, utilizar el método
	 * prepareCotizacionEndoso de EndosoService
	 */
	public void prepareCotizar(){
		biCotizacion = endosoService.prepareCotizacionEndoso(cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	
	@Action						
	(value = "cotizar", results = { 
			@Result(name=SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
	})
	public String cotizar() {		
		endosoService.guardaCotizacionBajaCondiciones(biCotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		setMensajeExito();
		return SUCCESS;		
	}
	
	

	/**
	 * Método que prepara el bitemporal de la Cotización, utilizar el método
	 * prepareCotizacionEndoso de EndosoService
	 */
	public void prepareEmitir(){
		biCotizacion = endosoService.prepareCotizacionEndoso(cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}"}),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoBajaCondicionEspecial/endosoBajaCondicionEspecial.jsp")
						
	})
	public String emitir() {
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(biCotizacion.getContinuity().getId(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso), biCotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO + String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		return SUCCESS;		
	}

	
	
	/** ********************************************************
	 ** ******************* GETTER & SETTER ********************
	 ** ******************************************************** */

	public String getAccionEndoso() {
		return accionEndoso;
	}


	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}


	public String getCodigosInciso() {
		return codigosInciso;
	}


	public void setCodigosInciso(String codigosInciso) {
		this.codigosInciso = codigosInciso;
	}


	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}


	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}


	public Short getNivelAplicacion() {
		return nivelAplicacion;
	}


	public void setNivelAplicacion(Short nivelAplicacion) {
		this.nivelAplicacion = nivelAplicacion;
	}


	public Integer getPolizaId() {
		return polizaId;
	}


	public void setPolizaId(Integer polizaId) {
		this.polizaId = polizaId;
	}


	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}


	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}


	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}


	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public List<CondicionEspecialBitemporalDTO> getCondicionesAsociadas() {
		return condicionesAsociadas;
	}

	public void setCondicionesAsociadas(
			List<CondicionEspecialBitemporalDTO> condicionesAsociadas) {
		this.condicionesAsociadas = condicionesAsociadas;
	}

		
	
	
	
	
	
	public List<CondicionEspecialBitemporalDTO> getCondicionesEliminadas() {
		return condicionesEliminadas;
	}

	public void setCondicionesEliminadas(
			List<CondicionEspecialBitemporalDTO> condicionesEliminadas) {
		this.condicionesEliminadas = condicionesEliminadas;
	}

	public boolean isAlta() {
		return alta;
	}

	public void setAlta(boolean alta) {
		this.alta = alta;
	}

	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}

	public CondicionEspecialBitemporalDTO getCondicionEspecialBitemporalDTO() {
		return condicionEspecialBitemporalDTO;
	}

	public void setCondicionEspecialBitemporalDTO(
			CondicionEspecialBitemporalDTO condicionEspecialBitemporalDTO) {
		this.condicionEspecialBitemporalDTO = condicionEspecialBitemporalDTO;
	}

	
	private NivelAplicacion getNivelAplicacionSelected(){
		NivelAplicacion nvlAplicacion = null;
		
		
		if(nivelAplicacion != null){
			if(nivelAplicacion == NivelAplicacion.POLIZA.getNivel()){
				nvlAplicacion = NivelAplicacion.POLIZA;
			}else if (nivelAplicacion == NivelAplicacion.INCISO.getNivel()){
				nvlAplicacion =  NivelAplicacion.INCISO;
			}
			
		}else{
			nvlAplicacion = NivelAplicacion.POLIZA;
		}
		
		return nvlAplicacion;
	}
	
	
	private List<Long> getListIncisosContinuityId(String codigosInciso){
			
			List<Long> listIncisosContinuityId 	= new ArrayList<Long>();
			String [] incisos 					= codigosInciso.split( SEPARADOR_CONDICION_ESPECIAL );
			int inciso 							= 0;
			int tamano 							= incisos.length;
			Long incisoContinuityId 			= null;
			
			for(int iterador = 0 ; iterador < tamano; iterador++){
				inciso = Integer.parseInt(  incisos[iterador] );
				
				incisoContinuityId = condicionEspecialBitemporalService.getIdIncisoContinuity(inciso, cotizacionContinuityId, fechaIniVigenciaEndoso);
				listIncisosContinuityId.add(incisoContinuityId);
			}
			
			return listIncisosContinuityId;
		}
		
	
	
	}
	
	