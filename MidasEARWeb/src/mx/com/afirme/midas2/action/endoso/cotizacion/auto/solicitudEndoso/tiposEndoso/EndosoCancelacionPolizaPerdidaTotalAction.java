package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal")
public class EndosoCancelacionPolizaPerdidaTotalAction extends BaseAction implements Preparable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL;

	
	private PolizaDTO polizaDTO;
	private Long polizaId;
	private Short motivoEndoso;
	private BitemporalCotizacion cotizacion;
	private ResumenCostosDTO resumenCostosDTO;
	private String accionEndoso;
	private Date fechaIniVigenciaEndoso;	
	private Long idSiniestroPT;
	private String tipoIndemnizacion;
	private String numeroSiniestro;
	private String actionNameOrigen;
    private String namespaceOrigen;
    private String elementoSeleccionado;
	private TransporteImpresionDTO transporteImpresion;
	
	private CalculoService calculoService;
	private EntidadService entidadService;
	private EndosoService endosoService;	
	private EntidadBitemporalService entidadBitemporalService;
	private EndosoPolizaService endosoPolizaService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private ImpresionEndosoService impresionEndosoService; 
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}	

	@Autowired
	@Qualifier("endosoPolizaServiceEJB")
	public void setEndosoPolizaService(EndosoPolizaService endosoPolizaService) {
		this.endosoPolizaService = endosoPolizaService;
	}
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}	
	
	@Autowired
	@Qualifier("impresionEndosoService")
	public void setImpresionEndosoService(ImpresionEndosoService impresionEndosoService) {
		this.impresionEndosoService = impresionEndosoService;
	}
	
	@Override
	public void prepare() throws Exception {
		
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));	
		
	}
	
	@Action (value = "mostrarCancelacionPolizaPerdidaTotal", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endososPerdidaTotal/endososPerdidaTotal.jsp"),
			@Result(name=INPUT,type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"idPolizaBusqueda","${polizaId}",								
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
			})
		})
		public String mostrarCancelacionPolizaPerdidaTotal() {	
		
		if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())) {

			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(),  
															TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
					TimeUtils.getDateTime(fechaIniVigenciaEndoso));	
			
		} else {
			endosoService.getCotizacionEndosoPerdidaTotal(polizaDTO.getIdToPoliza(), new Date(), tipoEndoso);
			resumenCostosDTO = new ResumenCostosDTO();
		}
		
		cotizacion = new BitemporalCotizacion();
		cotizacion.getValue().setNumeroEndoso(endosoService.getNumeroEndoso(polizaDTO.getIdToPoliza(), "%06d"));	
		
			return SUCCESS;
		}
	
	@Action (value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarCancelacionPolizaPerdidaTotal",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"accionEndoso","${accionEndoso}",
					"motivoEndoso","${motivoEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarCancelacionPolizaPerdidaTotal",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"accionEndoso","${accionEndoso}",
					"motivoEndoso","${motivoEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String cotizar(){
		
		IncisoContinuity incisoContinuity = new IncisoContinuity();
		incisoContinuity.setId(Long.valueOf(elementoSeleccionado));
		String[] continuitiesStringIds ={elementoSeleccionado};
		
		cotizacion = endosoService.prepareCotizacionEndosoPerdidaTotal(new BigDecimal(polizaId), fechaIniVigenciaEndoso, 
				tipoEndoso, tipoIndemnizacion, accionEndoso, incisoContinuity);
		
	
		endosoPolizaService.guardaCotizacionEndosoCancelacionPoliza(cotizacion,cotizacion.getValue().getSolicitud().getIdToSolicitud(), 
				fechaIniVigenciaEndoso);
		
		//Se remueve el bitemporal a nivel inciso.
		endosoService.guardaCotizacionEndosoBajaInciso(continuitiesStringIds, cotizacion);
		
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();	   
		
		return SUCCESS;
	}
	
	@Action(value="imprimirDesgloseCoberturas",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${transporteImpresion.contentType}",
					"inputName","transporteImpresion.genericInputStream",
					"contentDisposition","attachment;filename=\"${transporteImpresion.fileName}\""}),
					@Result(name = ERROR, location = "/jsp/errorImpresiones.jsp", params = {
							"mensaje", "${mensaje}" })})
	public String imprimirDesgloseCoberturas()
	{
		try{		
			
			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
															TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			
			cotizacion.getValue().setNumeroEndoso(endosoService.getNumeroEndoso(polizaDTO.getIdToPoliza(), "%d"));
			
			transporteImpresion = impresionEndosoService.imprimirDesgloseCoberturas(cotizacion, elementoSeleccionado, polizaDTO, 
					resumenCostosDTO, fechaIniVigenciaEndoso);
			
		}catch(Exception e)
		{
			LOG.error("imprimirDesgloseCoberturas error", e);
			setMensaje(getText("midas.componente.error.message"));
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return ERROR;			
		}	
		
		return SUCCESS;
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
					    "namespace","${namespaceOrigen}",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() 
	{
		cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
														TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndosoPerdidaTotal(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue(),
				idSiniestroPT);
				
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		
		return SUCCESS;
	}	
	
	@Action (value = "cancelar", results = { 
			@Result(name=SUCCESS, type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"esRetorno", "${esRetorno}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
			})
		})
		public String cancelar() {
			this.setEsRetorno(1);
			return SUCCESS;
		}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public Short getMotivoEndoso() {
		return motivoEndoso;
	}

	public void setMotivoEndoso(Short motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public String getNumeroSiniestro() {
		return numeroSiniestro;
	}

	public void setNumeroSiniestro(String numeroSiniestro) {
		this.numeroSiniestro = numeroSiniestro;
	}

	public String getTipoIndemnizacion() {
		return tipoIndemnizacion;
	}

	public void setTipoIndemnizacion(String tipoIndemnizacion) {
		this.tipoIndemnizacion = tipoIndemnizacion;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

	public String getElementoSeleccionado() {
		return elementoSeleccionado;
	}

	public void setElementoSeleccionado(String elementoSeleccionado) {
		this.elementoSeleccionado = elementoSeleccionado;
	}

	public TransporteImpresionDTO getTransporteImpresion() {
		return transporteImpresion;
	}

	public void setTransporteImpresion(TransporteImpresionDTO transporteImpresion) {
		this.transporteImpresion = transporteImpresion;
	}

	public Long getIdSiniestroPT() {
		return idSiniestroPT;
	}

	public void setIdSiniestroPT(Long idSiniestroPT) {
		this.idSiniestroPT = idSiniestroPT;
	}

}
