package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.DateUtils.Indicador;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia")
public class EndosoExtensionVigenciaAction extends BaseAction implements Preparable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal polizaId;
	private PolizaDTO poliza;
	private Date fechaIniVigenciaEndoso;
	private Date fechaExtensionVigencia;
	private Date fechaIniExtensionVigenciaValida;
	private String accionEndoso;
	private BigDecimal primaNetaIgualar;
	
	private Integer noEndosoFormateado = 77777; //TODO Remover valores inicializacion Dummy
	private String numeroPolizaFormateado;
	private String numeroEndosoFormateado;
	
	private String actionNameOrigen;
	private String namespaceOrigen;
	
	private BitemporalCotizacion cotizacion;
	//Componente que incluyo muestra 
	private ResumenCostosDTO resumenCostosDTO;
	 
	private Boolean invocacionInterna;
	//Fin Componente que incluyo muestra 
	
	

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		poliza = entidadService.findById(PolizaDTO.class, polizaId);
	}
	public void prepareMostrarExtensionVigencia(){
	}
	
	@Action (value = "mostrarExtensionVigencia", results = { 
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoExtensionVigencia.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",		
			"numeroPolizaFormateado","${numeroPolizaFormateado}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}",
			"nextFunction","${nextFunction}"
			})
	})
	public String mostrarExtensionVigencia() {
		// se valida el estado de la poliza.
//		ControlEndosoCot cec =  endosoService.obtenerControlEndosoCot(getPolizaId());
//		if(cec == null ||cec.getClaveEstatusCot() == CotizacionDTO.ESTATUS_COT_EMITIDA){
//			accionEndoso = TipoAccionDTO.getNuevoEndosoCot();
//		}else if(cec.getClaveEstatusCot() == CotizacionDTO.ESTATUS_COT_EN_PROCESO ){
//			accionEndoso = TipoAccionDTO.getEditarEndosoCot();
//		}
//		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		
		try {
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class,getPolizaId());
			numeroPolizaFormateado = polizaDTO.getNumeroPolizaFormateada();
			//se obtiene la cotización
			cotizacion = endosoService.getCotizacionEndosoEV(getPolizaId(),accionEndoso);
			
			// se obtiene el resumen de la cotización.
			resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
					TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()));
			numeroEndosoFormateado = cotizacion.getValue().getNumeroEndoso();
			
			if(!TipoAccionDTO.getNuevoEndosoCot().equalsIgnoreCase(accionEndoso))
			{				
				ControlEndosoCot controlEndosoCot = null;
				Map<String, Object> values = new LinkedHashMap<String, Object>();
				values.put("cotizacionId", cotizacion.getEntidadContinuity().getBusinessKey());
				values.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
				
				List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class, values);
				
				if (!controlEndosoCotList.isEmpty() && controlEndosoCotList.get(0) != null) {
					controlEndosoCot = controlEndosoCotList.get(0);
					fechaIniVigenciaEndoso = controlEndosoCot.getValidFrom();
				}else{
					fechaIniVigenciaEndoso = cotizacion.getValidityInterval().getInterval().getStart().toDate();
				}
				
				fechaExtensionVigencia = cotizacion.getValidityInterval().getInterval().getEnd().toDate();				
			}
			else
			{
				fechaIniVigenciaEndoso = cotizacion.getValue().getFechaFinVigencia();				
			}
			
			fechaIniExtensionVigenciaValida = DateUtils.add(fechaIniVigenciaEndoso, Indicador.Days, 1);			
			
		} catch (NegocioEJBExeption e) {
			// Si el mostrar es invocado desde afuera de este action entonces 
			if(invocacionInterna == null){
				String msg = this.getText(e.getErrorCode());
				if(msg != null){
					if(e.getValues() != null && e.getValues().length > 0)
						msg = String.format(msg, e.getValues());
				}else{
					msg= e.getMessageClean();
				}
				this.setMensaje(msg);
				this.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				return INPUT;
			}
		}
		return SUCCESS;
	}
	
	@Action (value = "refrescarResumenCostos", results  = {
			@Result(name=SUCCESS ,location="/jsp/poliza/auto/resumenTotales.jsp")
	})
		public String refrescarResumenCostos() {
		//se carga la cotización.
		cotizacion = endosoService.prepareCotizacionEndosoEV(getPolizaId(), TipoAccionDTO.getEditarEndosoCot());
		// se obtiene el resumen de la cotización.
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()));
		
		return SUCCESS;
	}
	
	
	public void prepareIgualarPrima() {
		//se carga la cotización.
		cotizacion = endosoService.prepareCotizacionEndosoEV(getPolizaId(), accionEndoso);
	}
	
	@Action
	(value = "previsualizarCobranza", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarExtensionVigencia","namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia"})})
	public String previsualizarCobranza() 
	{
		//TODO 
		System.out.println("previsualizarCobranza()");
		
		return SUCCESS;
	}
	
	@Action (value = "cancelar", results = { 
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"polizaId","${polizaId}",
			"numeroPolizaFormateado","${numeroPolizaFormateado}",
			"esRetorno", "${esRetorno}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar() {
		invocacionInterna = true;
		//TODO 
		System.out.println("cancelar()");
		this.setEsRetorno(1);
		return SUCCESS;
	}
	public void prepareEmitir() {
		//se carga la cotización.
		cotizacion = endosoService.prepareCotizacionEndosoEV(getPolizaId(), accionEndoso);
	}
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
					"namespace","${namespaceOrigen}",
					"polizaId","${polizaId}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"fechaIniVigenciaEndoso","${cotizacion.value.fechaInicioVigencia}",
					"accionEndoso","${accionEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"invocacionInterna","${invocacionInterna}"
			}),
			@Result(name=INPUT,type="redirectAction", 
					params={"actionName","mostrarExtensionVigencia",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia",
					"accionEndoso","${accionEndoso}",
					"polizaId","${polizaId}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}",
					"invocacionInterna","${invocacionInterna}"
			})
	}) //TODO Definir Result
	public String emitir() {
		invocacionInterna = true;
		//se obtiene la cotización.
		DateTime validoEn = null;
		ControlEndosoCot controlEndosoCot = null;
		Map<String, Object> values = new LinkedHashMap<String, Object>();
		values.put("cotizacionId", cotizacion.getEntidadContinuity().getBusinessKey());
		values.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);		
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class, values);
		
		if (!controlEndosoCotList.isEmpty() && controlEndosoCotList.get(0) != null) {
			controlEndosoCot = controlEndosoCotList.get(0);
			validoEn = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
		}else{
			validoEn = cotizacion.getValidityInterval().getInterval().getStart();
		}
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),validoEn,cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if(endoso!=null){
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		if(numeroPolizaFormateado == null || numeroPolizaFormateado.isEmpty()){
			numeroPolizaFormateado = poliza.getNumeroPolizaFormateada();
		}
		return SUCCESS;
	}
	
	public void prepareCotizar() {
		//se carga la cotización.
		cotizacion = endosoService.prepareCotizacionEndosoEV(getPolizaId(), accionEndoso);
	}
	
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarExtensionVigencia",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia",
					"accionEndoso","${accionEndoso}",
					"polizaId","${polizaId}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}",
					"invocacionInterna","${invocacionInterna}",
					"fechaExtensionVigencia","${fechaExtensionVigencia}"
			}),
			@Result(name=INPUT,type="redirectAction", 
					params={"actionName","mostrarExtensionVigencia",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia",
					"accionEndoso","${accionEndoso}",
					"polizaId","${polizaId}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}",
					"invocacionInterna","${invocacionInterna}",
					"fechaExtensionVigencia","${fechaExtensionVigencia}"
			})
	}) //TODO Definir Result
	public String cotizar() {
		invocacionInterna = true;
		// se realiza el guardado.
		endosoService.guardaCotizacionEndosoEV(cotizacion, fechaExtensionVigencia);
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();
		return SUCCESS;
	}
	
	 
	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}
	public BigDecimal getPolizaId() {
		return polizaId;
	}
	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}
	
	public void setNoEndosoFormateado(Integer noEndosoFormateado) {
		this.noEndosoFormateado = noEndosoFormateado;
	}
	public Integer getNoEndosoFormateado() {
		return noEndosoFormateado;
	}
	public void setNumeroEndosoFormateado(String numeroEndosoFormateado) {
		this.numeroEndosoFormateado = numeroEndosoFormateado;
	}
	public String getNumeroEndosoFormateado() {
		return numeroEndosoFormateado;
	}
	
	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}
	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}
	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}
	public String getActionNameOrigen() {
		return actionNameOrigen;
	}
	 
	public void setFechaExtensionVigencia(Date fechaExtensionVigencia) {
		this.fechaExtensionVigencia = fechaExtensionVigencia;
	}
	public Date getFechaExtensionVigencia() {
		return fechaExtensionVigencia;
	}
	public void setFechaIniExtensionVigenciaValida(
			Date fechaIniExtensionVigenciaValida) {
		this.fechaIniExtensionVigenciaValida = fechaIniExtensionVigenciaValida;
	}
	public Date getFechaIniExtensionVigenciaValida() {
		return fechaIniExtensionVigenciaValida;
	}

	public BigDecimal getPrimaNetaIgualar() {
		return primaNetaIgualar;
	}

	public void setPrimaNetaIgualar(BigDecimal primaNetaIgualar) {
		this.primaNetaIgualar = primaNetaIgualar;
	}

	
	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}
	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}
	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}
	
	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}
	
	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}
	public String getAccionEndoso() {
		return accionEndoso;
	}
	
	public void setInvocacionInterna(Boolean invocacionInterna) {
		this.invocacionInterna = invocacionInterna;
	}
	public Boolean getInvocacionInterna() {
		return invocacionInterna;
	}
	
	
	private CalculoService calculoService;
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
//	private CotizacionBitemporalService cotizacionBitemporalService;
//	@Autowired
//	@Qualifier("cotizacionBitemporalServiceEJB")
//	public void setCotizacionBitemporalService(
//			CotizacionBitemporalService cotizacionBitemporalService) {
//		this.cotizacionBitemporalService = cotizacionBitemporalService;
//	}
	
	private EndosoService endosoService;
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;

	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}
	public PolizaDTO getPoliza() {
		return poliza;
	}
	
	
}
