package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza")
public class EndosoCancelacionPolizaAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String numeroPolizaFormateado;
	
	private BigDecimal polizaId;
	private BigDecimal idToSolicitud;
	private Date fechaIniVigenciaEndoso;
	private String accionEndoso;
	
	private BitemporalCotizacion cotizacion;
	private ResumenCostosDTO resumenCostosDTO;
	private PolizaDTO polizaDTO;
	
	private Integer tipoEndoso;
	private Short motivoEndoso;
	
	private BigDecimal primaTotalIgualar;
	private BigDecimal primaTotalPoliza;
	
	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class, polizaId);
		setEsExterno(this.usuarioService.getUsuarioActual().isOutsider());
	}
	public BigDecimal getPolizaId(){
		//TODO:falta codificar el como obtener la poliza.
		return polizaId;
	}
	
	@Action(value = "mostrarCancelacionPoliza", results = { 
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoCancelacionPoliza.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarCancelacionPoliza() {
		// se obtiene la información que se tiene que mostrar.
		//cotizacion = endosoService.getCotizacionEndosoCECPoliza(getPolizaId(), fechaIniVigenciaEndoso, accionEndoso);
		
		if (motivoEndoso == null) motivoEndoso = 0;//FIXME Cambiar getCotizacionEndosoCancelacionPoliza para que no reciba un tipo nativo de parametro y agregar
		//logica para que se pase el motivoendoso al editar una cotizacion
		
		cotizacion = endosoPolizaService.getCotizacionEndosoCancelacionPoliza(getPolizaId(), fechaIniVigenciaEndoso, accionEndoso, motivoEndoso);
		
		if(idToSolicitud == null){
			idToSolicitud = cotizacion.getValue().getSolicitud().getIdToSolicitud();
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cotizacionId", cotizacion.getEntidadContinuity().getBusinessKey());
		params.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		List<ControlEndosoCot> controlesEnProceso = entidadService.findByProperties(ControlEndosoCot.class, params);
		if(controlesEnProceso != null && !controlesEnProceso.isEmpty()){
			primaTotalIgualar = controlesEnProceso.get(0).getPrimaTotalIgualar();
		}else{
			primaTotalIgualar = null;
		}
		// se obtiene la fecha
		//fechaIniVigenciaEndoso = cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso();
		
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(
				cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		//Obtiene prima de la poliza
		ResumenCostosDTO resumenCostosEmitidos = calculoService.resumenCostosEmitidos(polizaId, null, null);
		primaTotalPoliza = new BigDecimal(resumenCostosEmitidos.getPrimaTotal()).negate();
		
		return SUCCESS;
	}

	public void prepareMostrarCancelacionPoliza(){
		
	}
	
	@Action
	(value = "previsualizarCobranza", results = {
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarCancelacionPoliza","namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza"})})
	public String previsualizarCobranza() 
	{
		//TODO 
		System.out.println("previsualizarCobranza()");
		
		return SUCCESS;
	}
	
	@Action (value = "cancelar", results = { 
		@Result(name=SUCCESS,  type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"esRetorno", "${esRetorno}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar(){
		this.setEsRetorno(1);
		return SUCCESS;
	}
	
/*	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","${actionNameOrigen}",
					"namespace","${namespaceOrigen}",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${cotizacion.value.fechaInicioVigencia}",
					"accionEndoso","${accionEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})*/
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() {
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaIniVigenciaEndoso),
				cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		String numeroSolicitudCheque = "";
		if(endoso!=null){
			if(endoso.getSolictudCheque()!=null){
				numeroSolicitudCheque = ". N\u00Famero solicitud Seycos del cheque: "+endoso.getSolictudCheque(); 
			}
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue())+numeroSolicitudCheque);
		}
		return SUCCESS;
	}
	public void prepareEmitir() {
		//cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		cotizacion = endosoPolizaService.prepareBitemporalEndoso(cotizacion.getContinuity().getId(), 
				 TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarCancelacionPoliza",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza",
					"polizaId","${polizaId}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"motivoEndoso","${motivoEndoso}",
					"idToSolicitud","${idToSolicitud}",
					"primaTotalIgualar","${primaTotalIgualar}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}" ,
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"	
			})})
	public String cotizar() {
		
		//se realiza la cotización.
		endosoPolizaService.guardaCotizacionEndosoCancelacionPoliza(cotizacion,getIdToSolicitud(), 
				fechaIniVigenciaEndoso, primaTotalIgualar);
		
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		
		setMensajeExito();
		
		return SUCCESS;
	}

	public void prepareCotizar() {
		
		cotizacion = endosoPolizaService.prepareBitemporalEndoso(cotizacion.getContinuity().getId(), 
				 TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
	}
	
	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}
	
	public void setIdToSolicitud(BigDecimal idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}
	public BigDecimal getIdToSolicitud() {
		return idToSolicitud;
	}
	
//	public Integer getNumeroEndoso() {
//		return numeroEndoso;
//	}
//
//	public void setNumeroEndoso(Integer numeroEndoso) {
//		this.numeroEndoso = numeroEndoso;
//	}
//
//	public BigDecimal getTotalPrimas() {
//		
//		totalPrimas = new BigDecimal(500.99); //TODO Remover valores Dummy en getters
//		
//		return totalPrimas;
//	}
//
//	public void setTotalPrimas(BigDecimal totalPrimas) {
//		this.totalPrimas = totalPrimas;
//	}
//
//	public BigDecimal getDescuentoComis() {
//		
//		descuentoComis = new BigDecimal(4500.00);
//		
//		return descuentoComis;
//	}
//
//	public void setDescuentoComis(BigDecimal descuentoComis) {
//		this.descuentoComis = descuentoComis;
//	}
//
//	public BigDecimal getPrimaNeta() {
//		
//		primaNeta = new BigDecimal(20000.75);
//		
//		return primaNeta;
//	}
//
//	public void setPrimaNeta(BigDecimal primaNeta) {
//		this.primaNeta = primaNeta;
//	}
//
//	public BigDecimal getRecargo() {
//		
//		recargo = new BigDecimal(100.00);
//		
//		return recargo;
//	}
//
//	public void setRecargo(BigDecimal recargo) {
//		this.recargo = recargo;
//	}
//
//	public BigDecimal getDerechos() {
//		
//		derechos = new BigDecimal(2000.00);
//		
//		return derechos;
//	}

//	public void setDerechos(BigDecimal derechos) {
//		this.derechos = derechos;
//	}
//
//	public BigDecimal getIva() {
//		
//		iva = new BigDecimal(943.25);
//		
//		return iva;
//	}
//
//	public void setIva(BigDecimal iva) {
//		this.iva = iva;
//	}
//
//	public BigDecimal getPrimaTotalPeriodo() {
//		
//		primaTotalPeriodo = new BigDecimal(43232.00);
//		
//		return primaTotalPeriodo;
//	}
//
//	public void setPrimaTotalPeriodo(BigDecimal primaTotalPeriodo) {
//		this.primaTotalPeriodo = primaTotalPeriodo;
//	}

	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}
	

	public String getAccionEndoso() {
		return accionEndoso;
	}
	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}
	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}
	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}
	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}
	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}
	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Integer getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Integer tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	
	
	
	public Short getMotivoEndoso() {
		return motivoEndoso;
	}
	public void setMotivoEndoso(Short motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}



	private EndosoPolizaService endosoPolizaService;

	@Autowired
	@Qualifier("endosoPolizaServiceEJB")
	public void setEndosoPolizaService(EndosoPolizaService endosoPolizaService) {
		this.endosoPolizaService = endosoPolizaService;
	}
	
	private CalculoService calculoService;
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;

	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	private EntidadService entidadService;
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setPrimaTotalIgualar(BigDecimal primaTotalIgualar) {
		this.primaTotalIgualar = primaTotalIgualar;
	}
	public BigDecimal getPrimaTotalIgualar() {
		return primaTotalIgualar;
	}
	public void setPrimaTotalPoliza(BigDecimal primaTotalPoliza) {
		this.primaTotalPoliza = primaTotalPoliza;
	}
	public BigDecimal getPrimaTotalPoliza() {
		return primaTotalPoliza;
	}
	
}
