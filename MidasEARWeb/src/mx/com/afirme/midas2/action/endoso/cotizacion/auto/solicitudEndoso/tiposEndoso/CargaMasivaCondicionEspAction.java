package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesDetalleAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEsp;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.CargaMasivaIndividualAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso.DescripcionEtiqueta;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.excels.GenerarExcelCargaMasivaCondiciones;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cargaMasivaCondicionEsp")
public class CargaMasivaCondicionEspAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal  id;
	private BigDecimal  idToControlArchivo;
	private BigDecimal  idToCargaMasivaAutoCot;
	private BigDecimal  idToDetalleCargaMasivaAutoCot;
	private String      estatusCargaMasiva = "Por procesar documento";
	private String      contentType;
	private String      fileName;
	private Boolean     logErrors = false;
	private InputStream plantillaInputStream;
	private String      numeroPolizaFormateado;
	private Long        cotizacionContinuityId;
	private String      fechaIniVigenciaEndoso;
	private Long        idToSolicitud;
	private String      listaIncisos = "0";
	private Long        polizaId;
	
	private CargaMasivaService                 cargaMasivaService;
	private NegocioCondicionEspecialService    negocioCondicionEspecialService;
	private CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	private EntidadService                     entidadService;
	private IncisoViewService                  incisoViewService;
	private CondicionEspecialService           condicionEspecialService;
	private EntidadContinuityService           entidadContinuityService;
	private List<CargaMasivaCondicionesEsp>    cargaMasivaList;
	private ControlArchivoDTO                  controlArchivo; // # GUARDA EN BD DATOS GENERALES DEL DOCUMENTO SUBIDO
	private GenerarExcelCargaMasivaCondiciones plantilla = null;
	
	private Short tipoCarga = 1;
	private Short tipoRegreso;
	private short accionEndoso;     // # 0-Alta | 1-BAJA | 2-Todas en el excel
	private short nivelAplicacion ; // # 0-Poliza | 1-Inciso | 2-Todas 

	@Autowired
	@Qualifier("cargaMasivaServiceEJB")
	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	
	@Autowired
	@Qualifier("negocioCondicionEspecialServiceEJB")
	public void setNegocioCondicionEspecialService(NegocioCondicionEspecialService negocioCondicionEspecialService) {
		this.negocioCondicionEspecialService = negocioCondicionEspecialService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialServiceEJB")
	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}
	
	@Autowired
	@Qualifier("entidadContinuityServiceEJB")
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialCotizacionServiceEJB")
	public void setCondicionEspecialCotizacionService(
			CondicionEspecialCotizacionService condicionEspecialCotizacionService) {
		this.condicionEspecialCotizacionService = condicionEspecialCotizacionService;
	}
	
	@Autowired
	@Qualifier("condicionEspecialBitemporalServiceEJB")
	public void setCondicionEspecialBitemporalService(
			CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}	
	
	
	
	@Override
	public void prepare() throws Exception {
		
		// # OBTENER NUMERO POLIZA FORMATEADO
		PolizaDTO polizaDTO = cargaMasivaService.getPolizaByStringId(String.valueOf( this.id ));
		this.polizaId = Long.valueOf( this.id.toString() );
		this.numeroPolizaFormateado = polizaDTO.getNumeroPolizaFormateada();
		
		/*if( this.getNivelAplicacion() == 1 ){
			this.listaIncisos = plantilla.getListaIncisos(); // # OBTIENE LOS INCISOS DEL EXCEL
		}*/
	}

	public CargaMasivaCondicionEspAction(){

	}

	/**
	 * Método que descarga el archivo donde se detallan los errores que existieron
	 * durante la importación del archivo.
	 * Obtener el archivo del método descargaLogErrores de
	 * GeneraExcelCargaMasivaCondiciones
	 */
	
	@Action(value = "descargarLogErrores", 
			results = { @Result(
								name = "success", 
								type = "stream", 
								params = {
								        "contentType", "${contentType}",
								        "inputName", "plantillaInputStream",
								        "contentDisposition", "attachment;filename=\"${fileName}\""
										}
								),
						@Result(
								name = "error", 
								location = "/jsp/errorImpresiones.jsp"
								)
	})	
	public String descargarLogErrores(){
		
		plantilla = new GenerarExcelCargaMasivaCondiciones(cargaMasivaService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			FileInputStream plantillaBytes = plantilla.descargaLogErrores(idToControlArchivo);
			
			if(plantillaBytes == null){
				return ERROR;
			}
			
			plantillaInputStream = plantillaBytes;
			contentType = "application/text";
			setFileName("LogErroresCargaMasivaCot" + id + ".txt");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	/**
	 * Descargar el archivo excel para la importación.
	 * *Obtener la plantilla a través del método generaPlantillaCargaMasiva de
	 * GeneraExcelCargaMasivaCondiciones
	 */
	@Action(value = "descargarPlantilla", 
			results = { @Result(
								name = "success", 
								type = "stream", 
								params = {
								        "contentType", "${contentType}",
								        "inputName", "plantillaInputStream",
								        "contentDisposition", "attachment;filename=\"${fileName}\""
										}
								) 
	})
	public String descargarPlantilla(){
		
		plantilla = new GenerarExcelCargaMasivaCondiciones(cargaMasivaService,accionEndoso,nivelAplicacion,cotizacionContinuityId,numeroPolizaFormateado,this.getObtenerIdToNegocio() );
		
		// # INYECTAR EJB 
		plantilla.setNegocioCondicionEspecialService(negocioCondicionEspecialService);
		plantilla.setIncisoViewService(incisoViewService);
		
		
		long idToNegocio = 0;
		idToNegocio = this.getObtenerIdToNegocio();
		
		if(idToNegocio > 0){
			plantilla.setIdToNegocio(idToNegocio);
			try {
				plantillaInputStream = plantilla.generaPlantillaCargaMasiva();
				
				// # OBTIENE LOS INCISOS DEL EXCEL
				this.listaIncisos = plantilla.getListaIncisos();
				
				setFileName("CargaMasivaEndoso" + numeroPolizaFormateado + ".xls");
				contentType = "application/vnd.ms-excel";
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return SUCCESS;
		}else{
			return ERROR;
		}
	}
	
	private long getObtenerIdToNegocio(){
		CotizacionContinuity cotContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, cotizacionContinuityId);
		BigDecimal idToCotizacion = BigDecimal.valueOf( cotContinuity.getNumero() );
		return condicionEspecialCotizacionService.getIdNegocio(idToCotizacion);
	}

	/**
	 * Mostrar pantalla
	 */
	@Action(value="mostrarContenedor", results={
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/cargaMasivaCondicionEsp.jsp"),
			@Result(name = INPUT, location= "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/cargaMasivaCondicionEsp.jsp")
	})
	public String mostrarContenedor(){
		return SUCCESS;
	}

	/**
	 * preparar el listado de los archivos cargados.
	 * *Obtener el listado mediante el método obtenerCargasMasivasCondiciones de
	 * CargaMasivaService
	 */
	public void prepareListarCarga(){
		
		if(id != null){
			cargaMasivaList = entidadService.findByProperty(CargaMasivaCondicionesEsp.class, "idToSolicitud", id);
			
			//Sort
			if(cargaMasivaList != null && !cargaMasivaList.isEmpty()){
				Collections.sort(cargaMasivaList, 
						new Comparator<CargaMasivaCondicionesEsp>() {				
							public int compare(CargaMasivaCondicionesEsp n1, CargaMasivaCondicionesEsp n2){
								return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
							}
						});
			}
		}else{
			cargaMasivaList = new ArrayList<CargaMasivaCondicionesEsp>(1);
		}
		
	}
	
	/**
	 * Método para listar las cargas masivas
	 */
	@Action(value="listarCarga", results={
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/cargaMasivaCondicionEspGrid.jsp"),
			@Result(name = INPUT, location= "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/cargaMasivaCondicionEspGrid.jsp")
	})
	public String listarCarga(){
		return SUCCESS;
	}	

	/**
	 * prepare del método ValidaCarga. Instanciar el objeto ControlArchivoDTO
	 */
	public void prepareValidaCarga(){
		try {
			controlArchivo = ControlArchivoDN.getInstancia().getPorId(idToControlArchivo);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método que llama al servicio para validar la carga y si es aprobada se guarda.
	 * *Se debe guardar la carga masiva a través del método
	 * guardarCargaMasivaCondiciones de CargaMasivaService
	 * *Invocar el método validarCargaMasiva de GeneraExcelCargaMasivaCondiciones para
	 * validar el archivo a importar.
	 * *Guardar el detalle de la carga masiva a través del método
	 * guardaDetalleCargaMasivaCondiciones de CargaMasivaService
	 */
	@Action(value="validaCarga", results={
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/cargaMasivaCondicionEsp.jsp"),
			@Result(name = INPUT, location= "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoInclusionCondicionEspecial/cargaMasivaCondicionEsp.jsp")
	})
	public String validaCarga(){
		
		if(controlArchivo != null){
			
			plantilla = new GenerarExcelCargaMasivaCondiciones(cargaMasivaService,accionEndoso,nivelAplicacion,cotizacionContinuityId,numeroPolizaFormateado,this.getObtenerIdToNegocio());
			
			// # SALVA INFORMACION DEL EXCEL SUBIDO Y POR LEER
			CargaMasivaCondicionesEsp cargaMasivaCondicionesEsp = 
											cargaMasivaService.
													guardaCargaMasivaCondiciones(
																id, 
																controlArchivo, 
																this.getUsuarioActual().getNombreUsuario(),
																CargaMasivaCondicionesEsp.ESTATUS_EN_PROCESO
													);
			
			// # INYECTAR VALORES DE EJB Y DATOS A USAR EN EL PROCESO
			plantilla.setValidoEn( this.convierteFecha(this.getFechaIniVigenciaEndoso()) );
			plantilla.setNegocioCondicionEspecialService(negocioCondicionEspecialService);
			plantilla.setCondicionEspecialService(condicionEspecialService);
			plantilla.setCondicionEspecialBitemporalService(condicionEspecialBitemporalService);
			plantilla.setIncisoViewService(incisoViewService);
			plantilla.setIdCargaMasivaCondiciones( cargaMasivaCondicionesEsp.getId() );
			plantilla.setIdToCotizacion(Long.valueOf( id.toString() ));
			
			try{
				plantilla.validaCargaMasiva(idToControlArchivo, tipoCarga);
				
				
				// # SALVA ESTATUS DE LA PLANTILLA DESPUES DE SER LEIDA COMPLETAMENTE
				if(plantilla.isHasLogErrors()){
					
					// # SETEO DE VARIABLE PARA PODER DESCARGAR EL LOG DE LA VISTA
					this.setLogErrors(plantilla.isHasLogErrors());
					cargaMasivaCondicionesEsp.setClaveEstatus(CargaMasivaCondicionesEsp.ESTATUS_CON_ERROR);
					this.setEstatusCargaMasiva("CON ERROR");
				}else{
					
					cargaMasivaCondicionesEsp.setClaveEstatus(CargaMasivaCondicionesEsp.ESTATUS_TERMINADO );
					this.setEstatusCargaMasiva("TERMINADO");
				}
				
				this.listaIncisos = plantilla.getListaIncisos(); // # OBTIENE LOS INCISOS DEL EXCEL
				
				entidadService.save(cargaMasivaCondicionesEsp);
			}catch(Exception e){
				e.printStackTrace();
			}

		}
		return SUCCESS;

	}
	
	private Date convierteFecha(String fecha){
		String[] aFecha = fecha.split("/");
		if(aFecha.length > 0){
			String fechaTemp = aFecha[2]+"/"+aFecha[1]+"/"+aFecha[0];
			try {
				return new SimpleDateFormat("yyyy/MM/dd").parse(fechaTemp);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}else{
			return null;
		}
	}
	
	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public List<CargaMasivaCondicionesEsp> getCargaMasivaList() {
		return cargaMasivaList;
	}

	public void setCargaMasivaList(List<CargaMasivaCondicionesEsp> cargaMasivaList) {
		this.cargaMasivaList = cargaMasivaList;
	}

	public BigDecimal getIdToDetalleCargaMasivaAutoCot() {
		return idToDetalleCargaMasivaAutoCot;
	}

	public void setIdToDetalleCargaMasivaAutoCot(
			BigDecimal idToDetalleCargaMasivaAutoCot) {
		this.idToDetalleCargaMasivaAutoCot = idToDetalleCargaMasivaAutoCot;
	}

	public Boolean getLogErrors() {
		return logErrors;
	}

	public void setLogErrors(Boolean logErrors) {
		this.logErrors = logErrors;
	}

	public Short getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(Short tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public BigDecimal getIdToCargaMasivaAutoCot() {
		return idToCargaMasivaAutoCot;
	}

	public void setIdToCargaMasivaAutoCot(BigDecimal idToCargaMasivaAutoCot) {
		this.idToCargaMasivaAutoCot = idToCargaMasivaAutoCot;
	}

	/*public CargaMasivaAutoCot getCargaMasivaAutoCot() {
		return cargaMasivaAutoCot;
	}

	public void setCargaMasivaAutoCot(CargaMasivaAutoCot cargaMasivaAutoCot) {
		this.cargaMasivaAutoCot = cargaMasivaAutoCot;
	}*/

	/*public short getEstatusMovimiento() {
		return estatusMovimiento;
	}

	public void setEstatusMovimiento(short estatusMovimiento) {
		this.estatusMovimiento = estatusMovimiento;
	}*/

	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}

	public short getAccionEndoso() {
		return accionEndoso;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}
	
	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setAccionEndoso(short accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public void setIdToSolicitud(Long idToSolicitud) {
		this.idToSolicitud = idToSolicitud;
	}
	
	public Long getIdToSolicitud() {
		return idToSolicitud;
	}

	public void setNivelAplicacion(Short nivelAplicacion) {
		this.nivelAplicacion = nivelAplicacion;
	}
	
	public Short getNivelAplicacion() {
		return nivelAplicacion;
	}

	public String getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(String fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public String getEstatusCargaMasiva() {
		return estatusCargaMasiva;
	}

	public void setEstatusCargaMasiva(String estatusCargaMasiva) {
		this.estatusCargaMasiva = estatusCargaMasiva;
	}

	public Short getTipoRegreso() {
		return tipoRegreso;
	}

	public void setTipoRegreso(Short tipoRegreso) {
		this.tipoRegreso = tipoRegreso;
	}

	public String getListaIncisos() {
		return listaIncisos;
	}

	public void setListaIncisos(String listaIncisos) {
		this.listaIncisos = listaIncisos;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

}
