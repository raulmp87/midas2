package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@ParentPackage("json-default")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente")
public class EndosoCambioAgenteJson extends BaseAction {

	/** serialVersionUID **/
	private static final long serialVersionUID = 1471133138190494661L;
	protected String nombreAgente;
	private List<Map<String, Object>> listaAgentes;
	@Autowired
	@Qualifier("agenteMidasEJB")
	private AgenteMidasService agenteMidasService;
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	private ValorCatalogoAgentesService catalogoService;
	
	@Action(value = "buscarAgente", results = { @Result(name = "success", type = "json", params = {
			"root", "listaAgentes" }) })
	public String buscarAgente() throws Exception {
		listaAgentes = new ArrayList<Map<String, Object>>();
		if (StringUtils.isNotEmpty(nombreAgente)) {
			// Solo los agentes activos
			final ValorCatalogoAgentes tipoSituacion = catalogoService.obtenerElementoEspecifico(
					"Estatus de Agente (Situacion)", "AUTORIZADO");
			final Map<String, Object> filters = new HashMap<String, Object>();
			filters.put("nombreCompleto", nombreAgente);			
			filters.put("idTipoSituacion", tipoSituacion.getId());
			final List<AgenteView> agentesView = agenteMidasService.findByFilterLightWeight(filters);
			if (CollectionUtils.isNotEmpty(agentesView)) {
				for (AgenteView agente : agentesView) {
					final Map<String, Object> agenteMap = new HashMap<String, Object>();
					agenteMap.put("label", agente.getNombreCompleto() +  '-' + agente.getIdAgente());
					agenteMap.put("id", agente.getId());
					listaAgentes.add(agenteMap);
				}
			}

		}
		return SUCCESS;
	}
	
	/**
	 * @return the listaAgentes
	 */
	public List<Map<String, Object>> getListaAgentes() {
		return listaAgentes;
	}

	/**
	 * @param listaAgentes the listaAgentes to set
	 */
	public void setListaAgentes(List<Map<String, Object>> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}
	
	/**
	 * @param agenteMidasService the agenteMidasService to set
	 */
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}


	/**
	 * @return the nombreAgente
	 */
	public String getNombreAgente() {
		return nombreAgente;
	}
	/**
	 * @param nombreAgente the nombreAgente to set
	 */
	public void setNombreAgente(String nombreAgente) {
		this.nombreAgente = nombreAgente;
	}

	/**
	 * @param catalogoService the catalogoService to set
	 */
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
}
