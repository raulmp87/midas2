package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso;

import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_EXCLUSION_TEXTO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.UtileriasWeb;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso")
public class DefinirSolicitudEndosoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CatalogoValorFijoDTO> tiposEndoso = new ArrayList<CatalogoValorFijoDTO>(1);
	private Map<Integer, String> motivosEndoso = new HashMap<Integer, String>();
	private Integer tipoEndoso;
	private Short  motivoEndoso;
	private String namespace;
	private String methodName;
	private EntidadService entidadService;
	private Integer idPolizaBusqueda;
	private Integer numSolicitudBusqueda;    
    private Date fechaIniVigenciaEndoso;
    private String accionEndoso;
    PolizaDTO poliza;
    private Boolean filtrar= false;
    private CotizacionEndosoDTO filtrosBusqueda;
    private CotizacionEndosoService cotizacionEndosoService;
    private List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>(1);
    private List<MovimientoEndoso> listaMovimientoEndoso = new ArrayList<MovimientoEndoso>(1);
    private String numeroPolizaFormateado;  
    private Long negocioDerechoEndosoId;
    
    private String actionNameOrigen;
    private String namespaceOrigen;
 
	private BitemporalCotizacion cotizacion;
	
	private String ultimoEndosoValidFromDateTimeStr;
	private String ultimoEndosoRecordFromDateTimeStr;
	private Short ultimoEndosoClaveTipoEndoso;
	
	
	private EntidadBitemporalService entidadBitemporalService;
	
	private EndosoService endosoService;
    private EntidadContinuityService entidadContinuityService;
    private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;    
    private UsuarioService usuarioService;
    
    @Autowired
    @Qualifier("movimientoEndosoBitemporalServiceEJB")
    public void setMovimientoEndosoBitemporalService(
			MovimientoEndosoBitemporalService movimientoEndosoBitemporalService) {
		this.movimientoEndosoBitemporalService = movimientoEndosoBitemporalService;
	}

	@Autowired
    @Qualifier("cotizacionEndosoServiceEJB")
    public void setCotizacionEndosoService(
			CotizacionEndosoService cotizacionEndosoService) {
		this.cotizacionEndosoService = cotizacionEndosoService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("usuarioServiceEJB")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 
	 * @return
	 */
	@Action
	(value = "mostrarDefTipoModificacion", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/definirSolicitudEndoso.jsp"),
			@Result(name = INPUT, location= "/jsp/endosos/solicitudEndoso/definirSolicitudEndoso.jsp")})
	public String mostrarDefTipoModificacion() {

		if ((actionNameOrigen != null && !actionNameOrigen.isEmpty()) || (namespaceOrigen != null && !namespaceOrigen.isEmpty())) {
			ServletActionContext.getContext().getSession().put("actionNameOrigen", actionNameOrigen);
			ServletActionContext.getContext().getSession().put("namespaceOrigen", namespaceOrigen);			
		}
		
		tipoEndoso = null;
		
		// SIN: Endosos Perdida Total
		String[] rolesPorValidar = {"Rol_M2_Administrador",
				"Rol_M2_Gerente_Tecnico",
				"Rol_M2_Coordinador_Suscripcion",
				"Rol_M2_Suscriptor"};
		
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("id.idGrupoValores", 331);
		StringBuilder queryWhere = new StringBuilder();
		
		if(usuarioService.tieneRolUsuarioActual(rolesPorValidar))
		{
			queryWhere.append(" and model.id.idDato NOT IN (27) "); //Se excluyen endosos de Cambio Conducto Cobro
		}
		else
		{
			queryWhere.append(" and model.id.idDato NOT IN (24,25,26,27) "); //Se excluyen endosos de Perdida total
		}
		
		Integer usuarioExterno = 2;
		Usuario usuario = usuarioService.getUsuarioActual();
		if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
			usuarioExterno = usuario.getTipoUsuario();
		}
		
		if(usuarioExterno == 2) {
			queryWhere.append(" and model.id.idDato NOT IN (19) "); //Se excluyen endoso de Ajuste de Prima para usuarios externos
		}
		
		tiposEndoso = entidadService.findByColumnsAndProperties(CatalogoValorFijoDTO.class, "id,descripcion", 
				parametros, new HashMap<String,Object>(), queryWhere, null);
		
		if (numeroPolizaFormateado != null && numeroPolizaFormateado.length() > 0) {
			String[] elementosNumPoliza = numeroPolizaFormateado.split("-");
			Map<String,Object> mapa = new HashMap<String,Object>();
			mapa.put("codigoProducto", StringUtils.rightPad(elementosNumPoliza[0].substring(0,2),8)); //Ajuste para compatibilidad con tipo Char(8) de la columna en la BD
			mapa.put("codigoTipoPoliza", StringUtils.rightPad(elementosNumPoliza[0].substring(2),8)); //Ajuste para compatibilidad con tipo Char(8) de la columna en la BD
			mapa.put("numeroPoliza", Integer.valueOf(elementosNumPoliza[1]));
			mapa.put("numeroRenovacion", Integer.valueOf(elementosNumPoliza[2]));
			List<PolizaDTO> listaRes = entidadService.findByProperties(PolizaDTO.class, mapa);
			
			cotizacion = new BitemporalCotizacion();
			
			if (!listaRes.isEmpty()) {
				poliza = listaRes.get(0);
//				cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
//												CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion());
								
				cotizacion = endosoService.obtenerCotizacionCancelacion(poliza, null);
				
				
				
				if (cotizacion == null) {
					try{
						cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
							CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion(), TimeUtils.getDateTime(poliza.getCotizacionDTO().getFechaInicioVigencia()));
					}catch(Exception e){
						e.printStackTrace();
						setMensajeError("Error al obtener datos de la poliza");
						setNextFunction("iniciaDefinirSolicitudEndoso();");
						setTipoMensaje(TIPO_MENSAJE_INFORMACION);
						return INPUT;	
					}
				}
			}
			
			if (poliza == null) {
				setMensajeError(getText("midas.endosos.solicitudEndoso.definirSolicitudEndoso.mensaje.polizaNoExiste"));
				setNextFunction("iniciaDefinirSolicitudEndoso();");
				setTipoMensaje(TIPO_MENSAJE_INFORMACION);
				return INPUT;			
			}
			
			double dias = UtileriasWeb.obtenerDiasEntreFechas(new Date(), poliza.getCotizacionDTO().getFechaInicioVigencia());
			Date fechaInicio = new Date();
			if(dias > 0){
				fechaInicio = poliza.getCotizacionDTO().getFechaInicioVigencia();
			}
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndoso(poliza.getIdToPoliza().longValue(), fechaInicio);

			if (ultimoEndosoDTO != null) {
				ultimoEndosoValidFromDateTimeStr = ultimoEndosoDTO.getValidFrom().getTime() + "";
				ultimoEndosoRecordFromDateTimeStr = ultimoEndosoDTO.getRecordFrom().getTime() + "";
				ultimoEndosoClaveTipoEndoso = ultimoEndosoDTO.getClaveTipoEndoso();
			} else {
				ultimoEndosoValidFromDateTimeStr = fechaInicio.getTime() + "";
				ultimoEndosoRecordFromDateTimeStr = fechaInicio.getTime() + "";
			}
		}
		
		
		
		return SUCCESS;
	}
	
	@Action(value="verDetalleTipoEndoso",results={								 
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","${methodName}",
					"namespace","${namespace}",
					"polizaId","${idPolizaBusqueda}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"numeroPolizaFormateado","${numeroPolizaFormateado}",
					"motivoEndoso","${motivoEndoso}",
					"negocioDerechoEndosoId","${negocioDerechoEndosoId}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}",
					"nextFunction","${nextFunction}",
					"actionNameOrigen","${actionNameOrigen}",
					"namespaceOrigen","${namespaceOrigen}"}),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName","${methodName}",
					"namespace","${namespace}",
					"polizaId","${idPolizaBusqueda}",
					"idPolizaBusqueda","${idPolizaBusqueda}",
					"numeroPolizaFormateado","${poliza.numeroPolizaFormateada}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"
						})
	})	
	public String verDetalleTipoEndoso()
	{ 
		//Valida no tenga un endoso pendiente por migrar
		if(endosoService.validarEndososPorMigrarSeycos(new BigDecimal(idPolizaBusqueda))){
			poliza = entidadService.findById(PolizaDTO.class,new BigDecimal(idPolizaBusqueda));
			numeroPolizaFormateado = poliza.getNumeroPolizaFormateada();
			namespace= "/endoso/cotizacion/auto/solicitudEndoso";
			methodName = "mostrarDefTipoModificacion";
			actionNameOrigen = null;
			namespaceOrigen = null;			
			throw new NegocioEJBExeption("NE_29",new Object[]{});
		}
		
		//Valida Rehabilitacion de polizas canceladas automaticamente
		if(tipoEndoso.shortValue() == CVE_TIPO_ENDOSO_REHABILITACION_POLIZA &&
				endosoService.validaRehabilitacionCAP(new BigDecimal(idPolizaBusqueda))){
			poliza = entidadService.findById(PolizaDTO.class,new BigDecimal(idPolizaBusqueda));
			numeroPolizaFormateado = poliza.getNumeroPolizaFormateada();
			namespace= "/endoso/cotizacion/auto/solicitudEndoso";
			methodName = "mostrarDefTipoModificacion";
			actionNameOrigen = null;
			namespaceOrigen = null;			
			throw new NegocioEJBExeption("NE_30",new Object[]{});			
		}
		
		if(endosoService.validarRestriccionEndososPolizaFlotillaMigrada(new BigDecimal(idPolizaBusqueda)))
		{
			switch(tipoEndoso.shortValue())
			{
			    case CVE_TIPO_ENDOSO_ALTA_INCISO: 
			    	    namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso";
	                    methodName = "mostrarAltaInciso";
	                    break;
	                
			    case CVE_TIPO_ENDOSO_BAJA_INCISO:
				    	namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaInciso";
		                methodName = "mostrarBajaInciso";
		                break;	    	
			   
			    case CVE_TIPO_ENDOSO_CAMBIO_AGENTE: 
			    	    namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioAgente";
			            methodName = "mostrarCambioAgente";
			            break;
			            
			    case CVE_TIPO_ENDOSO_CAMBIO_DATOS: 
			    	    namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioDatos";
	                    methodName = "mostrarCambioDatos";
	                    break;
			    
			    case CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: 
			    	    namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago";
			            methodName = "mostrarCambioFormaPago";
			            break;
			            
			    case CVE_TIPO_ENDOSO_CANCELACION_POLIZA: 
			    	    namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPoliza";
			            methodName = "mostrarCancelacionPoliza";
			            break;
			            
			    case CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA: 
			    	    namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/extensionVigencia";
			            methodName = "mostrarExtensionVigencia";
			            break;
			            
			    case CVE_TIPO_ENDOSO_REHABILITACION_POLIZA: 
		    	     	namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionPoliza";
		    	     	methodName = "mostrarRehabilitacionPoliza";
		    	     	break;
	                
			    case CVE_TIPO_ENDOSO_INCLUSION_ANEXO: 
		    	     	namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionAnexo";
		    	     	methodName = "mostrarInclusionAnexo";
		    	     	break;
			     
			    case CVE_TIPO_ENDOSO_INCLUSION_TEXTO: 
			    	     namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionTexto";
	                     methodName = "mostrarInclusionTexto";
	                     break;

	            case CVE_TIPO_ENDOSO_DE_MOVIMIENTOS: 
		    	     	namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos";
		    	     	methodName = "mostrar";
		    	     	break;
		    	     	
	            case CVE_TIPO_ENDOSO_CANCELACION_ENDOSO: 
	    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso";
	    	     		methodName = "mostrarCancelacionEndoso";
	    	     		break;
	    	     	
	            case CVE_TIPO_ENDOSO_REHABILITACION_INCISOS:
	            		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/rehabilitacionInciso";
	            		methodName = "mostrarRehabilitacionInciso";
	            		break;
	    	     	
	            case CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO: 
	    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoRehabilitacionEndoso";
	    	     		methodName = "mostrarRehabilitacionEndoso";
	    	     		break;
	    	     	
	            case CVE_TIPO_ENDOSO_AJUSTE_PRIMA: 
	    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/ajustePrima";
	    	     		methodName = "mostrarAjustePrima";
	    	     		break;
	    	     		
	            case CVE_TIPO_ENDOSO_EXCLUSION_TEXTO: 
    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto";
    	     		methodName = "mostrarExclusionTexto";
    	     		break;	
    	     		
	            case CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES: 
    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/inclusionCondicionEspecial";
    	     		methodName = "mostrarInclusionCondiciones";
    	     		break;
    	     		
	            case CVE_TIPO_ENDOSO_BAJA_CONDICIONES: 
    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaCondicionEspecial";
    	     		methodName = "mostrarBajaCondiciones";
    	     		break;
    	     		
	            case CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL: 
    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/bajaIncisoPerdidaTotal";
    	     		methodName = "mostrarBajaIncisoPerdidaTotal";
    	     		break;		
    	     	
	            case CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL: 
    	     		namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cancelacionPolizaPerdidaTotal";
    	     		methodName = "mostrarCancelacionPolizaPerdidaTotal";
    	     		break;

	            case CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS:	
	            	namespace = "/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/desagrupacionRecibos";
    	     		methodName = "mostrarDesagrupacionRecibos";
    	     		break;
	    	     	
			    default:
			}			
		}
		else
		{
			//El endoso no puede proceder debido a la restriccion Flotilla Migrada
			poliza = entidadService.findById(PolizaDTO.class,new BigDecimal(idPolizaBusqueda));
			numeroPolizaFormateado = poliza.getNumeroPolizaFormateada();
			namespace= "/endoso/cotizacion/auto/solicitudEndoso";
			methodName = "mostrarDefTipoModificacion";
			actionNameOrigen = null;
			namespaceOrigen = null;			
			throw new NegocioEJBExeption("_",getText("midas.endosos.solicitudEndoso.definirSolicitudEndoso.candadoFlotillaMigrada"));			
			
		}
		
				
		if(this.getTipoMensaje() != null && this.getTipoMensaje().equals("30") && this.getMensaje().endsWith("realizada correctamente")){
			this.setMensajeExito();
		}
		//se realiza para que pueda pintarse el grid despues de mandar una excepción 
		setNextFunction("iniciaDefinirSolicitudEndoso();");
		return SUCCESS;			
	}	
		
	@Action (value = "busquedaRapidaPaginada", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") 
	})
	public String busquedaRapidaPaginada() {
		
		if (idPolizaBusqueda != null){
			if(filtrar){
				filtrosBusqueda = new CotizacionEndosoDTO();
				PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(idPolizaBusqueda));
				filtrosBusqueda.setNumeroPoliza(poliza.getNumeroPoliza());
				filtrosBusqueda.setNumeroCotizacionEndoso(poliza.getCotizacionDTO().getIdToCotizacion());
				if(getTotalCount() == null){
					setTotalCount(cotizacionEndosoService.obtenerTotalPaginacionCotizacionesEndosoPoliza(filtrosBusqueda));
					setListadoEnSession(null);
				}
				setPosPaginado();
			}
		}
		
		return SUCCESS;
	}
	@SuppressWarnings("unchecked")
	@Action
	(value = "busquedaRapida", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/definirSolicitudEndosoGrid.jsp") })
	public String busquedaRapida(){
		

		if (idPolizaBusqueda != null){
			if(filtrar){
				if(!listadoDeCache()){
					filtrosBusqueda = new CotizacionEndosoDTO();
					PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(idPolizaBusqueda));
					filtrosBusqueda.setNumeroPoliza(poliza.getNumeroPoliza());
					filtrosBusqueda.setNumeroCotizacionEndoso(poliza.getCotizacionDTO().getIdToCotizacion());
					filtrosBusqueda.setPrimerRegistroACargar(getPosStart());
					filtrosBusqueda.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
					listaCotizacionesEndoso = cotizacionEndosoService.buscarCotizacionesEndosoPoliza(filtrosBusqueda);
					setListadoEnSession(listaCotizacionesEndoso);
				}else{
					listaCotizacionesEndoso = (List<CotizacionEndosoDTO>) getListadoPaginado();
					setListadoEnSession(listaCotizacionesEndoso);
				}
			}
		}
		
		return SUCCESS;
	}
	/*
	@Action
	(value = "validarNumeroPoliza", results = { 
	        @Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarDefTipoModificacion","namespace","/endoso/cotizacion/auto/solicitudEndoso","numeroPolizaFormateado","${numeroPolizaFormateado}","fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}","tipoEndoso","${tipoEndoso}"})})
	public String validarNumeroPoliza() 
	{	
		return SUCCESS;
	}*/
	
	@Action
	(value = "cancelar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","${actionNameOrigen}","namespace","${namespaceOrigen}","esRetorno","${esRetorno}"})})
	public String cancelar() 
	{		
		actionNameOrigen = ServletActionContext.getContext().getSession().get("actionNameOrigen").toString();
		namespaceOrigen = ServletActionContext.getContext().getSession().get("namespaceOrigen").toString();
		
		return SUCCESS;
	}
	
	@Action
	(value = "consultarEndoso", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarDefTipoModificacion","namespace","/endoso/cotizacion/auto/solicitudEndoso"})})	
	public String consultarEndoso() 
	{
		//TODO 
		System.out.println("consultarEndoso()");
		
		return SUCCESS;
	}
	
	@Action
	(value = "consultarCotizacion", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarDefTipoModificacion","namespace","/endoso/cotizacion/auto/solicitudEndoso"})})	
	public String consultarCotizacion() 
	{
		//TODO 
		System.out.println("consultarCotizacion()");
		
		return SUCCESS;
	}
	
	@Action
	(value = "cancelarCotizacion", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","${actionNameOrigen}","namespace","${namespaceOrigen}","mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}","idPolizaBusqueda","${idPolizaBusqueda}","numeroPolizaFormateado","${numeroPolizaFormateado}"})})
	public String cancelarCotizacion() 
	{		
		endosoService.deleteCotizacionEndoso(cotizacion.getEntidadContinuity().getId());
		CotizacionContinuity continuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class,cotizacion.getContinuity().getId());
		cotizacion.setContinuity(continuity);
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", continuity.getNumero());
		properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);
		
		if(controlEndosoCotList.isEmpty())
		{
			properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_TERMINADA);
			controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);				
		}
		
		Iterator<ControlEndosoCot> itControlEndosoCot = controlEndosoCotList.iterator();
		
		ControlEndosoCot controlEndosoCot = null;
		Boolean movimientosBorrados = false;
		while(itControlEndosoCot.hasNext())
		{	
			controlEndosoCot = itControlEndosoCot.next();
			if(!movimientosBorrados){
				movimientoEndosoBitemporalService.borrarMovimientos(controlEndosoCot);
				movimientosBorrados = true;
			}
			entidadService.remove(controlEndosoCot);			
		}		
		
		//Remover Solicitudes de Autorizacion
		endosoService.cancelarSolicitudesAutorizacion(cotizacion);
		
		setMensajeExito();
		
		return SUCCESS;
	}
	
	@Action
	(value = "mostrarMovimientos", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/ventanaMovimientosEndoso.jsp") })
	public String mostrarMovimientos() 
	{		
		CotizacionContinuity continuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class,cotizacion.getContinuity().getId());
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", continuity.getNumero());
		properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);
		
		if(controlEndosoCotList.isEmpty())
		{
			properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_TERMINADA);
			controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);				
		}
		
		if(controlEndosoCotList != null && !controlEndosoCotList.isEmpty()){
			Map<String,Object> props = new HashMap<String,Object>();
			props.put("controlEndosoCot.id", controlEndosoCotList.get(0).getId());

			listaMovimientoEndoso = entidadService.findByPropertiesWithOrder(MovimientoEndoso.class,props,"id");
		}
		
		return SUCCESS;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("entidadContinuityServiceEJB")
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}
	
	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Integer getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Integer tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public List<CatalogoValorFijoDTO> getTiposEndoso() {
		return tiposEndoso;
	}

	public void setTiposEndoso(List<CatalogoValorFijoDTO> tiposEndoso) {
		this.tiposEndoso = tiposEndoso;
	}	

	public Integer getNumSolicitudBusqueda() {
		return numSolicitudBusqueda;
	}

	public void setNumSolicitudBusqueda(Integer numSolicitudBusqueda) {
		this.numSolicitudBusqueda = numSolicitudBusqueda;
	}	

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Integer getIdPolizaBusqueda() {
		return idPolizaBusqueda;
	}

	public void setIdPolizaBusqueda(Integer idPolizaBusqueda) {
		this.idPolizaBusqueda = idPolizaBusqueda;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public PolizaDTO getPoliza() {
		return poliza;
	}

	public void setPoliza(PolizaDTO poliza) {
		this.poliza = poliza;
	}

	public String getNumeroPolizaFormateado() {
		return numeroPolizaFormateado;
	}

	public void setNumeroPolizaFormateado(String numeroPolizaFormateado) {
		this.numeroPolizaFormateado = numeroPolizaFormateado;
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}

	public List<CotizacionEndosoDTO> getListaCotizacionesEndoso() {
		return listaCotizacionesEndoso;
	}

	public void setListaCotizacionesEndoso(
			List<CotizacionEndosoDTO> listaCotizacionesEndoso) {
		this.listaCotizacionesEndoso = listaCotizacionesEndoso;
	}

	public Map<Integer, String> getMotivosEndoso() {
		return motivosEndoso;
	}

	public void setMotivosEndoso(Map<Integer, String> motivosEndoso) {
		this.motivosEndoso = motivosEndoso;
	}

	public Short getMotivoEndoso() {
		return motivoEndoso;
	}

	public void setMotivoEndoso(Short motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}

	public String getUltimoEndosoValidFromDateTimeStr() {
		return ultimoEndosoValidFromDateTimeStr;
	}

	public void setUltimoEndosoValidFromDateTimeStr(
			String ultimoEndosoValidFromDateTimeStr) {
		this.ultimoEndosoValidFromDateTimeStr = ultimoEndosoValidFromDateTimeStr;
	}

	public String getUltimoEndosoRecordFromDateTimeStr() {
		return ultimoEndosoRecordFromDateTimeStr;
	}

	public void setUltimoEndosoRecordFromDateTimeStr(
			String ultimoEndosoRecordFromDateTimeStr) {
		this.ultimoEndosoRecordFromDateTimeStr = ultimoEndosoRecordFromDateTimeStr;
	}

	public Short getUltimoEndosoClaveTipoEndoso() {
		return ultimoEndosoClaveTipoEndoso;
	}

	public void setUltimoEndosoClaveTipoEndoso(Short ultimoEndosoClaveTipoEndoso) {
		this.ultimoEndosoClaveTipoEndoso = ultimoEndosoClaveTipoEndoso;
	}

	public void setNegocioDerechoEndosoId(Long negocioDerechoEndosoId) {
		this.negocioDerechoEndosoId = negocioDerechoEndosoId;
	}

	public Long getNegocioDerechoEndosoId() {
		return negocioDerechoEndosoId;
	}

	public void setListaMovimientoEndoso(List<MovimientoEndoso> listaMovimientoEndoso) {
		this.listaMovimientoEndoso = listaMovimientoEndoso;
	}

	public List<MovimientoEndoso> getListaMovimientoEndoso() {
		return listaMovimientoEndoso;
	}	
	
}
