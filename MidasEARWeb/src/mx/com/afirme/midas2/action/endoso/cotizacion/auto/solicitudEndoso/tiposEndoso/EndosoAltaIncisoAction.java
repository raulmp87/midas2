package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDN;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.poliza.seguroobligatorio.GeneraSeguroObligatorio;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.excels.GeneraExcelCargaMasivaEndosoAltaInciso;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.altainciso.complementario.CargaMasivaComplementarioService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso")
public class EndosoAltaIncisoAction extends EndosoBaseAction implements Preparable {

	private static final long serialVersionUID = 1L;	
	private static final Logger LOG = Logger.getLogger(EndosoAltaIncisoAction.class);

	private Date fechaIniVigenciaEndoso;	
	private Long polizaId;
	private String accionEndoso;
	private PolizaDTO polizaDTO;
	private BitemporalCotizacion cotizacion;
	private List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>(1);	
	private Short tipoEndoso;
	private ResumenCostosDTO resumenCostosDTO;
	private Long nuevoContratanteId;
	private MensajeDTO mensajeDTO;
	private	List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private Boolean aplicaEndoso = true;
	private Long negocioDerechoEndosoId;
	private TransporteImpresionDTO archivo;
	private BigDecimal idToControlArchivo;
	private Map<String, String> bancos;
	private String actionNameOrigen;
    private String namespaceOrigen;
    private Long id;
    
	private EntidadService entidadService;
	private EndosoService endosoService;
	private CalculoService calculoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntidadBitemporalService entidadBitemporalService;
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService;
	private Map<Long, String> derechosEndosoList = new HashMap<Long, String>();
	private ListadoService listadoService;
	private RecuotificacionService recuotificacionService;
	
	private SeguroObligatorioService seguroObligatorioService;
	
	private CargaMasivaComplementarioService cargaMasivaComplementarioService;
	
	private CargaMasivaService cargaMasivaService;
	
	private IncisoService incisoServiceBitemporal;
	private ClientesApiService clienteRest;

	private BancoMidasService bancoMidasService;	
	private IncisoViewService incisoService;
	private ClienteFacadeRemote clienteFacadeRemote;

	@Autowired
	@Qualifier("bancoMidasServiceEJB")
	public void setBancoMidasService(BancoMidasService bancoMidasService) {
		this.bancoMidasService = bancoMidasService;
	}
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("clientesApiServiceEJB")
	public void setClienteRest(ClientesApiService clienteRest) {
		this.clienteRest = clienteRest;
	}

	@Autowired
	@Qualifier("incisoEndosoServiceEJB")
	public void setIncisoService(IncisoService incisoServiceBitemporal) {
		this.incisoServiceBitemporal = incisoServiceBitemporal;
	}
	
	@Autowired
	@Qualifier("cargaMasivaServiceEJB")
	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}
	
	@Autowired
	public void setCargaMasivaComplementarioService(CargaMasivaComplementarioService cargaMasivaComplementarioService) {
		this.cargaMasivaComplementarioService = cargaMasivaComplementarioService;
	}
	
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}
	
	@Autowired
	@Qualifier("recuotificacionServiceEJB")
	public void setRecuotificacionService(RecuotificacionService recuotificacionService) {
		this.recuotificacionService = recuotificacionService;
	}
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Autowired
	@Qualifier("configuracionDatoIncisoBitemporalServiceEJB")
	public void setConfiguracionDatoIncisoBitemporalService(
			ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService) {
		this.configuracionDatoIncisoBitemporalService = configuracionDatoIncisoBitemporalService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}		
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	
	private List<CargaMasivaAutoCot> cargaMasivaEndosoAltaList;
	private ControlArchivoDTO controlArchivo;
	private InputStream plantillaInputStream;
	private String contentType;
	private String fileName;	
	private Boolean logErrors = false;
	
	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));
		if(tipoEndoso == null){
			tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO;
		}
	}
	
	@Action(value = "mostrarAltaInciso", results = {
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/endosoAltaInciso.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarAltaInciso() {
		
		if((actionNameOrigen != null && !actionNameOrigen.isEmpty()) || (namespaceOrigen != null && !namespaceOrigen.isEmpty()))
		{
			ServletActionContext.getContext().getSession().put("actionNameOrigen", actionNameOrigen);
			ServletActionContext.getContext().getSession().put("namespaceOrigen", namespaceOrigen);			
		}
		
		cotizacion = endosoService.getCotizacionEndosoAltaInciso(new BigDecimal(polizaId), fechaIniVigenciaEndoso,accionEndoso);
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		derechosEndosoList = listadoService.getMapDerechosEndoso(cotizacion.getValue().getSolicitud());
		
		if(negocioDerechoEndosoId != null){
			cotizacion.getValue().setNegocioDerechoEndosoId(negocioDerechoEndosoId);
		}
		return SUCCESS;
	}
	
	@Action (value = "cancelar", results = { 
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar(){	
		this.setEsRetorno(1);
		
		if(this.getActionNameOrigen() == null || this.getActionNameOrigen().isEmpty()){
			try{
				this.setActionNameOrigen(ServletActionContext.getContext().getSession().get("actionNameOrigen").toString());
				this.setNamespaceOrigen(ServletActionContext.getContext().getSession().get("namespaceOrigen").toString());
			}catch(Exception e){
				LOG.error(e);
				this.setActionNameOrigen("listar");
				this.setNamespaceOrigen("/endoso/cotizacion/auto");
			}
		}
		
		return SUCCESS;
	}
	
	public void prepareCotizar() {		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
	@Action (value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarAltaInciso",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String cotizar(){
		
		actionNameOrigen = ServletActionContext.getContext().getSession().get("actionNameOrigen").toString();
		namespaceOrigen = ServletActionContext.getContext().getSession().get("namespaceOrigen").toString();

		endosoService.validaPrimaNetaCeroEndosoCotizacion(tipoEndoso, polizaId, fechaIniVigenciaEndoso);
		
		endosoService.guardaCotizacionEndosoMovimientos(cotizacion);
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();		
		
		return SUCCESS;
	}
	
	public void prepareTerminar(){		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
	//TODO Definir los parametros y Result Correspondientes
	@Action(value = "terminar", results = {
			
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDetalle",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"polizaId","${polizaId}",
						"tipoEndoso","${tipoEndoso}",
						"accionEndoso","${accionEndoso}",
						"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
						"cotizacion.continuity.id","${cotizacion.continuity.id}",
						"namespaceOrigen","${namespaceOrigen}",
						"actionNameOrigen","${actionNameOrigen}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"}),
			@Result(name=ERROR, location = "/jsp/suscripcion/cotizacion/auto/resultadoTerminarCotizacion.jsp")
			})
	public String terminar() {
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.terminarCotizacionEndoso(cotizacion,  TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		if (terminarCotizacionDTO != null) {
			id = cotizacion.getContinuity().getNumero().longValue();
			excepcionesList = 	terminarCotizacionDTO.getExcepcionesList();
			super.setMensajeError(terminarCotizacionDTO.getMensajeError());				
			return ERROR;			
		}		
		setMensajeExito();
		
		return SUCCESS;
	}
	
	public void prepareRegresarAProceso() {		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
	@Action (value = "regresarAProceso", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarAltaInciso",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String regresarAProceso(){
				
		endosoService.regresarAProcesoCotizacionEndoso(cotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();

		setMensajeExito();		
		
		return SUCCESS;
	}
	
	@Action (value = "mostrarComplementarEmision", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/altaIncisoComplementarEmision.jsp") })
	public String mostrarComplementarEmision() {
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		return SUCCESS;
	}
	
	public void prepareEmitir() {
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		//Actualizar Datos Contratante
		if(nuevoContratanteId != null)
		{
			ClienteGenericoDTO clienteGenerico = endosoService.getClienteGenerico(new BigDecimal(nuevoContratanteId));
			cotizacion.getValue().setDomicilioContratanteId(BigDecimal.valueOf(clienteGenerico.getIdDomicilioConsulta()));
			cotizacion.getValue().setPersonaContratanteId(clienteGenerico.getIdCliente().longValue());
			cotizacion.getValue().setNombreContratante(clienteGenerico.getNombreCompleto());
			cotizacion.getValue().getSolicitud().setClaveTipoPersona(clienteGenerico.getClaveTipoPersona());
			entidadBitemporalService.saveInProcess(cotizacion,TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		}		
	}
	
	@Action(value = "emitir", results = { 
		@Result(name=SUCCESS,type="redirectAction", 
				params={"actionName","${actionNameOrigen}",
					"namespace","${namespaceOrigen}",
					"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
		@Result(name=ERROR, location = "/jsp/suscripcion/cotizacion/auto/resultadoTerminarCotizacion.jsp")
	})
	public String emitir() {
		
		try{
			
			BigDecimal grupoParametro = new BigDecimal("22");
			LOG.info("Obteniendo valores de parametros de configuracion de recuotificacion");
			Integer banderaRecuotificacion = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, new BigDecimal("22030"));
			Integer banderaPrevioRecibos = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, new BigDecimal("22040"));
			
			if (banderaRecuotificacion.intValue() == 1){
			
				BigDecimal idToCotizacion = new BigDecimal(cotizacion.getContinuity().getNumero().longValue());
				LOG.info("Validando Previo de recibos para cotizacion: " + idToCotizacion);
				//SE AGREGA VALIDACION DE PREVIO DE RECIBOS AL EMITIR
				//ParametroGeneralDN parametroGeneralDN = ParametroGeneralDN.getINSTANCIA();
				//ParametroGeneralId idpg = new ParametroGeneralId();
				//idpg.setIdToGrupoParametroGeneral(new BigDecimal("22"));
				//idpg.setCodigoParametroGeneral(new BigDecimal("22010"));
				//ParametroGeneralDTO parametroGeneralDTO = parametroGeneralDN.getPorId(idpg);
		
				boolean previoDeRecibos = recuotificacionService.validarPrevioDeRecibos(idToCotizacion);
				if (previoDeRecibos){
					LOG.info("Existe Previo de recibos, validando saldos de Recuotificacion");
					Map<String, Object> res = recuotificacionService.validaTotalesDeCotizacionSalir(idToCotizacion);
					BigDecimal resultado = (BigDecimal) res.get("outIdEjecucion");
					if (resultado.toString().equals("0")){
						LOG.info("No existen saldos, continuando con emision");
					}else{
						LOG.info("Existen saldos por asignar a PP, pausando emision");
						super.setMensajeError("Existen saldos de Recuotificacion. Verifique.");					
						return ERROR;
					}
				}else{
					LOG.info("No existe previo de recuotificacion, validando previo de recibos");
					if(banderaPrevioRecibos.intValue() == 1){
						LOG.info("Bandera de Recuotificacion encendida, Pausando emision");
						super.setMensajeError("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");					
						return ERROR;
					}else{
						LOG.info("Bandera de Recuotificacion apagada, continuando con la emision.");
					}	
				}
			}else{
				LOG.info("Bandera de recuotificacion apagada, continuando con el flujo natural de la operacion");
			}
		}catch(Exception e){
			LOG.info("Error en validacion previa re recuotificacion: " + e.getMessage());
			super.setMensajeError("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");					
			return ERROR;
		}
		
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.validarEmisionCotizacion(cotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		if (terminarCotizacionDTO != null) {
			excepcionesList = 	terminarCotizacionDTO.getExcepcionesList();
			id = cotizacion.getContinuity().getNumero().longValue();
			super.setMensajeError(terminarCotizacionDTO.getMensajeError());				
			return ERROR;			
		}
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
			//Emite Endoso Alta SO
			try{
				if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_ALTA_INCISO)){
					GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
					generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
					generaSeguroObligatorio.creaPolizaAnexaEndosoAltaInciso(polizaDTO, endoso.getId().getNumeroEndoso().shortValue());
				}
				
				
				if(polizaDTO.getCotizacionDTO().getIdToPersonaContratante()!=null){
					
					clienteRest.validatePeps(polizaDTO.getCotizacionDTO().getIdToPersonaContratante().longValue(),endoso.getDescripcionNumeroEndoso());
						
				}
					if(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado()!=null){
					
					clienteRest.validatePeps(polizaDTO.getCotizacionDTO().getIdToPersonaAsegurado().longValue(),endoso.getDescripcionNumeroEndoso());
						
				}
			}catch(Exception e){
				LOG.error(e);
			}	
		}
		
		if(this.getActionNameOrigen() == null || this.getActionNameOrigen().isEmpty()){
			try{
				this.setActionNameOrigen(ServletActionContext.getContext().getSession().get("actionNameOrigen").toString());
				this.setNamespaceOrigen(ServletActionContext.getContext().getSession().get("namespaceOrigen").toString());
			}catch(Exception e){
				this.setActionNameOrigen("listar");
				this.setNamespaceOrigen("/endoso/cotizacion/auto");
			}
		}
		
		return SUCCESS;
	}
	
	@Action (value = "salir", results = { 
			@Result(name = SUCCESS, type="redirectAction", 
					params={"actionName","listar",
					"namespace","/endoso/cotizacion/auto"})
	})
	public String salir() {		
		
		return SUCCESS;
	}

	@Action
	(value = "mostrarDetalle", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/detalle/contenedor.jsp") })
	public String mostrarDetalle(){		
		return SUCCESS;
	}
	
	public void prepareValidarDatosComplementarios(){		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
	@Action (value = "validarDatosComplementarios", results = { 
			@Result(name=SUCCESS, type="redirectAction", params={
				"actionName","mostrarComplementarEmision",
				"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"mensajeDTO.mensaje","${mensajeDTO.mensaje}",
				"mensajeDTO.visible","${mensajeDTO.visible}",		
				"cotizacion.continuity.id","${cotizacion.continuity.id}",
				"accionEndoso","${accionEndoso}",
				"tipoEndoso","${tipoEndoso}",
				"polizaId","${polizaId}",
				"namespaceOrigen","${namespaceOrigen}",
				"actionNameOrigen","${actionNameOrigen}",
				"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"				
			})
		})
	public String validarDatosComplementarios() {
		
	    mensajeDTO = configuracionDatoIncisoBitemporalService.validarListaParaEmitir(cotizacion, tipoEndoso, fechaIniVigenciaEndoso);
		/*mensajeDTO = new MensajeDTO();
		mensajeDTO.setMensaje("Se tienen complementados 10 Incisos");
		mensajeDTO.setVisible(true);*/
		
		return SUCCESS;
	}
	
	@Action (value = "redireccionarEndoso", results = { 
			@Result(name="successTerminar", type="redirectAction", params={
					"actionName","mostrarAltaInciso",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name="successComplementar", type="redirectAction", params={
					"actionName","mostrarDetalle",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
					"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
					"polizaId","${polizaId}",
					"tipoEndoso","${tipoEndoso}",
					"accionEndoso","${accionEndoso}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"cotizacion.continuity.id","${cotizacion.continuity.id}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name = "successTerminarMovimientos", type = "redirectAction", params = {
					"actionName",
					"mostrar",
					"namespace",
					"/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos",
					"polizaId", "${polizaId}", "fechaIniVigenciaEndoso",
					"${fechaIniVigenciaEndoso}", "accionEndoso",
					"${accionEndoso}", "namespaceOrigen", "${namespaceOrigen}",
					"actionNameOrigen", "${actionNameOrigen}", "mensaje",
					"${mensaje}", "tipoMensaje", "${tipoMensaje}" }),
			@Result(name = "successComplementarMovimientos", type = "redirectAction", params = {
					"actionName",
					"mostrarDetalle",
					"namespace",
					"/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
					"numeroPolizaFormateado",
					"${polizaDTO.numeroPolizaFormateada}", "polizaId",
					"${polizaId}", "tipoEndoso", "${tipoEndoso}",
					"accionEndoso", "${accionEndoso}",
					"fechaIniVigenciaEndoso", "${fechaIniVigenciaEndoso}",
					"cotizacion.continuity.id", "${cotizacion.continuity.id}",
					"namespaceOrigen", "${namespaceOrigen}",
					"actionNameOrigen", "${actionNameOrigen}", "mensaje",
					"${mensaje}", "tipoMensaje", "${tipoMensaje}" })
		})
	public String redireccionarEndoso() {
		String result = null;
		ControlEndosoCot controlEndosoCot = endosoService.obtenerControlEndosoCotCotizacion(
																new BigDecimal(cotizacion.getContinuity().getNumero()));
		cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
													CotizacionContinuity.BUSINESS_KEY_NAME, cotizacion.getContinuity().getNumero(), 
													TimeUtils.getDateTime(controlEndosoCot.getValidFrom()));		
		
		tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		fechaIniVigenciaEndoso = controlEndosoCot.getValidFrom();
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		
		if(tipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)){
			if (controlEndosoCot.getClaveEstatusCot() == CotizacionDTO.ESTATUS_COT_EN_PROCESO) {
				result = "successTerminar";
			} else if (controlEndosoCot.getClaveEstatusCot() == CotizacionDTO.ESTATUS_COT_TERMINADA) {			
				result = "successComplementar";
			}
		} else if(tipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS)){
			if (controlEndosoCot.getClaveEstatusCot() == CotizacionDTO.ESTATUS_COT_EN_PROCESO) {
				result = "successTerminarMovimientos";
			} else if (controlEndosoCot.getClaveEstatusCot() == CotizacionDTO.ESTATUS_COT_TERMINADA) {			
				result = "successComplementarMovimientos";
			}
		}
		
		return result;
	}
	
	@Action
	(value = "descargarPlantillaComplementarios", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Descarga de Plantilla", "xls"})},
			results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${archivo.contentType}",
					"inputName", "archivo.genericInputStream",
					"contentDisposition", "attachment;filename=${archivo.fileName}"})}) 
	public String descargarPlantillaComplementarios() {
		
		BigDecimal idCotizacion  = polizaDTO.getCotizacionDTO().getIdToCotizacion();
		
		archivo = cargaMasivaComplementarioService.obtenerPlantilla(idCotizacion,TimeUtils.getDateTime(fechaIniVigenciaEndoso),listadoService);
		
		return SUCCESS;
		
	}
	
	@Action
	(value = "procesarPlantillaComplementarios", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Carga Masiva Datos Complementarios", "xls"})},
			results = {
			@Result(name = ERROR, type = "stream", params = {
					"contentType", "${archivo.contentType}",
					"inputName", "archivo.genericInputStream",
					"contentDisposition", "attachment;filename=${archivo.fileName}"})}) 
	public String procesarPlantillaComplementarios() {
		
		BigDecimal idCotizacion  = polizaDTO.getCotizacionDTO().getIdToCotizacion();
		
		archivo = cargaMasivaComplementarioService.procesar(idCotizacion, idToControlArchivo, TimeUtils.getDateTime(fechaIniVigenciaEndoso),bancoMidasService,incisoService,listadoService,clienteFacadeRemote);
		
		if (archivo != null) {
			
			return ERROR;
			
		}
		
		return NONE;
		
	}
	
	@Action (value = "mostrarCargaMasivaAltaInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/cargaMasiva/endosoAltaIncisoCargaMsva.jsp") })
	public String mostrarCargaMasivaAltaInciso() {		
		
		return SUCCESS;
	}
	
	@Action (value = "listarCargaMasivaAltaInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/cargaMasiva/endosoAltaIncisoCargaMsvaGrid.jsp") })
	public String listarCargaMasivaAltaInciso() {	
		if(polizaDTO != null){
			cargaMasivaEndosoAltaList = entidadService.findByProperty(CargaMasivaAutoCot.class, "idToCotizacion", polizaDTO.getCotizacionDTO().getIdToCotizacion());
			//Sort
			if(cargaMasivaEndosoAltaList != null && !cargaMasivaEndosoAltaList.isEmpty()){
				Collections.sort(cargaMasivaEndosoAltaList, 
						new Comparator<CargaMasivaAutoCot>() {				
							public int compare(CargaMasivaAutoCot n1, CargaMasivaAutoCot n2){
								return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
							}
						});
			}
		}else{
			cargaMasivaEndosoAltaList = new ArrayList<CargaMasivaAutoCot>(1);
		}		
		return SUCCESS;
	}
	
	@Action
	(value = "descargarPlantillaCargaMsva", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Descarga de Plantilla", "xls"})},
			results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${contentType}",
					"inputName", "plantillaInputStream",
					"contentDisposition", "attachment;filename=${fileName}"})})
	public String descargarPlantillaCargaMsva(){
		GeneraExcelCargaMasivaEndosoAltaInciso plantilla = new GeneraExcelCargaMasivaEndosoAltaInciso(cargaMasivaService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			
			setPlantillaInputStream(plantilla.generaPlantillaCargaMasiva(polizaDTO.getCotizacionDTO()));
			setFileName("CargaMasivaEndAI" + id + ".xls");
			setContentType("application/vnd.ms-excel");
		} catch (IOException e) {
			LOG.error(e);
		}
		return SUCCESS;
	}
	
	@Action
	(value = "descargarLogErroresCargaMsva", 
			interceptorRefs={@InterceptorRef(value="downloadFileStack", params={"Descarga de Plantilla", "xls"})},
			results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${contentType}",
					"inputName", "plantillaInputStream",
					"contentDisposition", "attachment;filename=${fileName}"})}) 	
	public String descargarLogErroresCargaMsva(){
		GeneraExcelCargaMasivaEndosoAltaInciso plantilla = new GeneraExcelCargaMasivaEndosoAltaInciso(cargaMasivaService);
		try {
			plantilla.setCargaMasivaService(cargaMasivaService);
			FileInputStream plantillaBytes = plantilla.descargaLogErrores(idToControlArchivo);			
			if(plantillaBytes == null){
				return ERROR;
			}
			setPlantillaInputStream(plantillaBytes);
			setContentType("application/text");
			setFileName("LogErroresCargaMasivaCot" + id + ".txt");	
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;		
	}
	
	public void prepareProcesaCargaMasivaAltaInciso(){
		try {
			polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));
			cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME,
					polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			setControlArchivo(ControlArchivoDN.getInstancia().getPorId(idToControlArchivo));
		} catch (SystemException e) {
			LOG.error(e);
		}
	}
	
	@Action (value = "procesaCargaMasivaAltaInciso", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/altaInciso/cargaMasiva/endosoAltaIncisoCargaMsva.jsp") })
	public String procesaCargaMasivaAltaInciso(){
		long start = System.currentTimeMillis();
		LOG.info("Inicio de procesaCargaMasivaAltaInciso");
		
		
		if(controlArchivo != null){		
			// Trae información del archivo
			CargaMasivaAutoCot cargaMasiva = cargaMasivaService.guardaCargaMasiva(polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
					controlArchivo, this.getUsuarioActual().getNombreUsuario(), (short) 1);
			
			GeneraExcelCargaMasivaEndosoAltaInciso plantilla = new GeneraExcelCargaMasivaEndosoAltaInciso(cargaMasivaService);
			
			try {
				
				//Limpia datos en proceso de la cotizacion
				entidadBitemporalService.rollBackContinuity(cotizacion.getContinuity().getId());
				
				plantilla.setCargaMasivaService(cargaMasivaService);
				plantilla.setIncisoServiceBitemporal(incisoServiceBitemporal);
				
				plantilla.validaCargaMasiva(cotizacion, idToControlArchivo, fechaIniVigenciaEndoso);
				cargaMasivaService.guardaDetalleCargaMasiva(cargaMasiva, plantilla.getDetalleCargaMasivaAutoCotList());
				
				setLogErrors(plantilla.isHasLogErrors());
				if(!plantilla.isHasLogErrors()){
					cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_TERMINADO);

					endosoService.validaPrimaNetaCeroEndosoCotizacion(tipoEndoso, polizaId, fechaIniVigenciaEndoso);					
					//endosoService.guardaCotizacionEndosoMovimientos(cotizacion);
					
					accionEndoso = TipoAccionDTO.getEditarEndosoCot();
				}else{
					cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_CON_ERROR);
				}
				entidadService.save(cargaMasiva);
			}catch(Exception e){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_CON_ERROR);
				entidadService.save(cargaMasiva);
				e.printStackTrace();
			}
		}
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		LOG.info("procesaCargaMasivaAltaInciso elapsed time idCotizacion: " + polizaId + " " 
				+ " controlArchivo: " + controlArchivo + " " + ((System.currentTimeMillis() - start) / 1000.0));
				
		return SUCCESS;
	}	
	
	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}


	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public List<BitemporalInciso> getListaIncisosCotizacion() {
		return listaIncisosCotizacion;
	}

	public void setListaIncisosCotizacion(
			List<BitemporalInciso> listaIncisosCotizacion) {
		this.listaIncisosCotizacion = listaIncisosCotizacion;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public Long getNuevoContratanteId() {
		return nuevoContratanteId;
	}

	public void setNuevoContratanteId(Long nuevoContratanteId) {
		this.nuevoContratanteId = nuevoContratanteId;
	}

	public Boolean getAplicaEndoso() {
		return aplicaEndoso;
	}

	public void setAplicaEndoso(Boolean aplicaEndoso) {
		this.aplicaEndoso = aplicaEndoso;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public void setExcepcionesList(
			List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Map<Long, String> getDerechosEndosoList() {
		return derechosEndosoList;
	}

	public void setDerechosEndosoList(Map<Long, String> derechosEndosoList) {
		this.derechosEndosoList = derechosEndosoList;
	}


	public void setNegocioDerechoEndosoId(Long negocioDerechoEndosoId) {
		this.negocioDerechoEndosoId = negocioDerechoEndosoId;
	}


	public Long getNegocioDerechoEndosoId() {
		return negocioDerechoEndosoId;
	}

	public TransporteImpresionDTO getArchivo() {
		return archivo;
	}

	public void setArchivo(TransporteImpresionDTO archivo) {
		this.archivo = archivo;
	}

	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	
	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	public void setCargaMasivaEndosoAltaList(
			List<CargaMasivaAutoCot> cargaMasivaEndosoAltaList) {
		this.cargaMasivaEndosoAltaList = cargaMasivaEndosoAltaList;
	}

	public List<CargaMasivaAutoCot> getCargaMasivaEndosoAltaList() {
		return cargaMasivaEndosoAltaList;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContentType() {
		return contentType;
	}

	public void setPlantillaInputStream(InputStream plantillaInputStream) {
		this.plantillaInputStream = plantillaInputStream;
	}

	public InputStream getPlantillaInputStream() {
		return plantillaInputStream;
	}

	public void setControlArchivo(ControlArchivoDTO controlArchivo) {
		this.controlArchivo = controlArchivo;
	}

	public ControlArchivoDTO getControlArchivo() {
		return controlArchivo;
	}

	public void setLogErrors(Boolean logErrors) {
		this.logErrors = logErrors;
	}

	public Boolean getLogErrors() {
		return logErrors;
	}
	
}

