package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/desagrupacionRecibos")
public class EndosoDesagrupacionRecibosAction extends BaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS;
	
	private PolizaDTO polizaDTO;
	private Long polizaId;
	private BitemporalCotizacion cotizacion;
	private Date fechaIniVigenciaEndoso;
	private String accionEndoso;
	private String actionNameOrigen;
    private String namespaceOrigen;
    private String elementoSeleccionado;
    BitemporalInciso bitemporalInciso;
    BitemporalAutoInciso bitemporalAutoInciso;
    
	private IncisoViewService incisoService;
    private CalculoService calculoService;
	private EntidadService entidadService;
	private EndosoService endosoService;
	private EntidadBitemporalService entidadBitemporalService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private ImpresionEndosoService impresionEndosoService;
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("impresionEndosoService")
	public void setImpresionEndosoService(ImpresionEndosoService impresionEndosoService) {
		this.impresionEndosoService = impresionEndosoService;
	}	
    
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	@Override
	public void prepare() throws Exception {
		
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));		
	}
	
	@Action (value = "mostrarDesagrupacionRecibos", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoDesagrupacionRecibos/endosoDesagrupacionRecibos.jsp"),
			@Result(name=INPUT,type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"idPolizaBusqueda","${polizaId}",								
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
			})
		})
		public String mostrarDesagrupacionRecibos() {	
			
		if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())) {

			cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
															TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
					TimeUtils.getDateTime(fechaIniVigenciaEndoso));			
			
		} else {
			endosoService.getCotizacionEndosoDesagrupacionRecibos(new BigDecimal(polizaId), 
					fechaIniVigenciaEndoso,tipoEndoso);
			cotizacion = new BitemporalCotizacion();			
		}
		
		cotizacion.getValue().setNumeroEndoso(endosoService.getNumeroEndoso(polizaDTO.getIdToPoliza(), "%06d"));	
					
		return SUCCESS;
		}
	
	@Action (value = "cancelar", results = {
			@Result(name=SUCCESS, type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"mensaje","${mensaje}",
				"esRetorno", "${esRetorno}",
				"tipoMensaje","${tipoMensaje}"
			})
		})
		public String cancelar() {
			this.setEsRetorno(1);
			return SUCCESS;
		}
	
	public void prepareCotizar(){
		bitemporalInciso = incisoService.getIncisoInProcess(Long.valueOf(elementoSeleccionado), fechaIniVigenciaEndoso);
		bitemporalAutoInciso = incisoService.getAutoIncisoInProcess(incisoService.getIncisoInProcess(Long.valueOf(elementoSeleccionado), 
				fechaIniVigenciaEndoso), fechaIniVigenciaEndoso);
	}
		
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarDesagrupacionRecibos",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/desagrupacionRecibos",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
					@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarDesagrupacionRecibos",
							"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/desagrupacionRecibos",
							"polizaId","${polizaId}",
							"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
							"namespaceOrigen","${namespaceOrigen}",
							"actionNameOrigen","${actionNameOrigen}",
							"accionEndoso","${accionEndoso}",
							"motivoEndoso","${motivoEndoso}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
					})
	public String cotizar() {	
		
		cotizacion = endosoService.prepareCotizacionEndosoDesagrupacionRecibos(new BigDecimal(polizaId), 
				fechaIniVigenciaEndoso,accionEndoso);
				
		IncisoContinuity incisoContinuity = new IncisoContinuity();
		incisoContinuity.setId(Long.valueOf(elementoSeleccionado));
		
		endosoService.guardaCotizacionEndosoDesagrupacionRecibos(polizaDTO, cotizacion, incisoContinuity, bitemporalAutoInciso);
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();		
		
		return SUCCESS;
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
					    "namespace","${namespaceOrigen}",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() {
		
		cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
														TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
				TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso != null) {
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}		
		return SUCCESS;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

	public String getElementoSeleccionado() {
		return elementoSeleccionado;
	}

	public void setElementoSeleccionado(String elementoSeleccionado) {
		this.elementoSeleccionado = elementoSeleccionado;
	}

	public BitemporalInciso getBitemporalInciso() {
		return bitemporalInciso;
	}

	public void setBitemporalInciso(BitemporalInciso bitemporalInciso) {
		this.bitemporalInciso = bitemporalInciso;
	}

	public BitemporalAutoInciso getBitemporalAutoInciso() {
		return bitemporalAutoInciso;
	}

	public void setBitemporalAutoInciso(BitemporalAutoInciso bitemporalAutoInciso) {
		this.bitemporalAutoInciso = bitemporalAutoInciso;
	}
}
