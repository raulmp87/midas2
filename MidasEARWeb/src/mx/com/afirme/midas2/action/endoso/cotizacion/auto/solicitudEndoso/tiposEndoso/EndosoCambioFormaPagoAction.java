package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago")
public class EndosoCambioFormaPagoAction extends BaseAction implements Preparable {

	/** serialVersionUID **/
	private static final long serialVersionUID = 5140497784044585846L;
	//Objetos de mapeo en el action
	private Long polizaId;
	private Date fechaIniVigenciaEndoso;
	private String formaPagoVigente;	
	private Integer idFormaPagoNueva;
	private String accionEndoso;
	private  Map<Integer, String> formasdePago = new LinkedHashMap<Integer, String>();
	private PolizaDTO polizaDTO;
	/** Objeto que tiene el resumen de la cotizacion, con los totales del endoso */
	private ResumenCostosDTO resumenCostosDTO;
	private Boolean mostrarPctRecargoPagoFraccionado = false;
	
	private SolicitudDTO solicitud = new SolicitudDTO();
	
	//Objetos de Bitemporalidad
	private BitemporalCotizacion cotizacion;
	
	
	//Servicios
	private ListadoService listadoService; 
	private EndosoService endosoService;
	private EntidadService entidadService;
	/** Servicio para calcular el resumen de la cotizacion */
	private CalculoService calculoService;
	private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	

	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));
		if(polizaDTO != null){
			try{
			if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado()){
				mostrarPctRecargoPagoFraccionado = true;
			}
			}catch(Exception e){
				
			}
		}
	}
	
	@Action (value = "mostrarCambioFormaPago", results = { 
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoCambioFormaPago.jsp"),
		@Result(name=INPUT, type="redirectAction", params={
				"actionName","${actionNameOrigen}",
				"namespace","${namespaceOrigen}",
				"idPolizaBusqueda","${polizaId}",								
				"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
				"mensaje","${mensaje}",
				"tipoMensaje","${tipoMensaje}"
		}) 
	})
	public String mostrarCambioFormaPago() {
		
		cotizacion = endosoService.getCotizacionEndosoCFP(new BigDecimal(polizaId), fechaIniVigenciaEndoso,accionEndoso);
		BitemporalCotizacion cotizacionValida = cotizacion.getContinuity().getCotizaciones().get(TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		formasdePago = listadoService.getMapFormasdePago(cotizacion.getValue().getSolicitud());
		formaPagoVigente = formasdePago.get(cotizacionValida.getValue().getFormaPago().getId());
		/** RMP: se obtiene la información para llenar la tabla informativa con el resumen de la cotizacion */
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
				
		
		
		return SUCCESS;
	}
	
	public void prepareEmitir() {
		BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), solicitud);
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()));
		cotizacion.getValue().setSolicitud(solicitud);
	}
	
	@Action
	(value = "emitir", results = { 
		@Result(name=SUCCESS,type="redirectAction", 
				params={"actionName","mostrarDefTipoModificacion",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso",
					"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})
	public String emitir() {
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if(endoso!=null){
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
		}
		return SUCCESS;
	}
	
	public void prepareCotizar() {
		BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), solicitud);
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()));
		cotizacion.getValue().setSolicitud(solicitud);
	}
	
	@Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarCambioFormaPago",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${cotizacion.value.fechaInicioVigencia}",
					"accionEndoso","${accionEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})})
	public String cotizar() {
	
		BitemporalCotizacion cotizacionTemp = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()));
		if(cotizacion.getValue().getFormaPago().getId().equals(cotizacionTemp.getValue().getFormaPago().getId())){
			setMensajeError("Debe seleccionar una forma de pago diferente a la actual");
			setAccionEndoso(TipoAccionDTO.getNuevoEndosoCot());
		}else{
			FormaPagoDTO formaPagoDTO = entidadService.findById(FormaPagoDTO.class,
					cotizacion.getValue().getFormaPago().getId());
			if(!mostrarPctRecargoPagoFraccionado){
				cotizacion.getValue().setPorcentajePagoFraccionado(movimientoEndosoBitemporalService.getPctFormaPago(formaPagoDTO.getIdFormaPago(), 
					cotizacion.getValue().getMoneda().getIdTcMoneda()));
			}
			cotizacion.getValue().setFormaPago(formaPagoDTO);
			try {
				endosoService.guardaCotizacionEndoso(cotizacion);
				setMensajeExito();
				setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
			} catch (NegocioEJBExeption e) {
				// Si el mostrar es invocado desde afuera de este action entonces 				
				this.setMensaje(this.getText(e.getMessage()));
				this.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				return SUCCESS;
			}
		}
		
		return SUCCESS;
	}
		
	@Action(value = "cancelar", results = { 
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar(){
		//TODO 
		System.out.println("cancelar()");
		this.setEsRetorno(1);
		return SUCCESS;
	}
		
	@Action
	(value = "previsualizarCobranza", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarCambioFormaPago","namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/cambioFormaPago"})})
	public String previsualizarCobranza() 
	{
		//TODO 
		System.out.println("previsualizarCobranza()");
		
		return SUCCESS;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public String getFormaPagoVigente() {
		return formaPagoVigente;
	}

	public void setFormaPagoVigente(String formaPagoVigente) {
		this.formaPagoVigente = formaPagoVigente;
	}

	public Integer getIdFormaPagoNueva() {
		return idFormaPagoNueva;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public void setIdFormaPagoNueva(Integer idFormaPagoNueva) {
		this.idFormaPagoNueva = idFormaPagoNueva;
	}

	public Map<Integer, String> getFormasdePago() {
		return formasdePago;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public void setFormasdePago(Map<Integer, String> formasdePago) {
		this.formasdePago = formasdePago;
	}

	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}
	
	/**
	 * @return the resumenCostosDTO
	 */
	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}
	/**
	 * @param resumenCostosDTO
	 *            the resumenCostosDTO to set
	 */
	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	/**
	 * @param calculoService
	 *            the calculoService to set
	 */
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	/**
	 * @return the calculoService
	 */
	public CalculoService getCalculoService() {
		return calculoService;
	}
	
	@Autowired
	@Qualifier("movimientoEndosoBitemporalServiceEJB")
	public void setMovimientoEndosoBitemporalService(
			MovimientoEndosoBitemporalService movimientoEndosoBitemporalService) {
		this.movimientoEndosoBitemporalService = movimientoEndosoBitemporalService;
	}

	public void setMostrarPctRecargoPagoFraccionado(
			Boolean mostrarPctRecargoPagoFraccionado) {
		this.mostrarPctRecargoPagoFraccionado = mostrarPctRecargoPagoFraccionado;
	}

	public Boolean getMostrarPctRecargoPagoFraccionado() {
		return mostrarPctRecargoPagoFraccionado;
	}
}
