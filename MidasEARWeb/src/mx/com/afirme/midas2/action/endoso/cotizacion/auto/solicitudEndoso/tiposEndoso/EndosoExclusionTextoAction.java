package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto")
public class EndosoExclusionTextoAction extends EndosoBaseAction implements Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	private Integer numeroEndoso;	
	private Date fechaIniVigenciaEndoso;	
	private Long polizaId;
	private String accionEndoso;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_EXCLUSION_TEXTO;
	
	private EntidadService entidadService;
	private EndosoService endosoService;	
    private EmisionEndosoBitemporalService emisionEndosoBitemporalService;

	private BitemporalCotizacion cotizacion;
	private PolizaDTO polizaDTO;
	private Collection<BitemporalTexAdicionalCot> listaTextosAdicionales = new ArrayList<BitemporalTexAdicionalCot>(1);
	private Collection<BitemporalTexAdicionalCot> listaTextosAdicionalesProc = new ArrayList<BitemporalTexAdicionalCot>(1);
	private String idsSeleccionados;
	
	private String actionNameOrigen;
    private String namespaceOrigen;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}		
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}	

	@Override
	public void prepare() throws Exception {
		
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));	
		
	}
	
	@Action (value = "mostrarExclusionTexto", results = {
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoExclusionTexto/endosoExclusionTexto.jsp"),
		@Result(name=INPUT,type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrarExclusionTexto() {	
		cotizacion = endosoService.getCotizacionEndosoExclusionTexto(new BigDecimal(polizaId), fechaIniVigenciaEndoso, accionEndoso);				
		return SUCCESS;
	}
	
	@Action
	(value = "obtenertexAdicionalGrid", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoExclusionTexto/endosoExclusionTextoGrid.jsp") })
	public String obtenertexAdicionalGrid() {
		listaTextosAdicionales = endosoService.getLstBiTexAdicionalCotNotInProcess(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		return SUCCESS;
	}
	
	@Action
	(value = "obtenertexAdicionalProcGrid", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/endosoExclusionTexto/endosoExclusionTextoProcGrid.jsp") })
	public String obtenertexAdicionalProcGrid() {
		listaTextosAdicionalesProc = endosoService.getLstBiTexAdicionalCotProcess(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		return SUCCESS;
	}
	
	@Action (value = "cancelar", results = { 
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String cancelar() {
		this.setEsRetorno(1);
		return SUCCESS;
	}	
	
	public void prepareCotizar(){		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action (value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrarExclusionTexto",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"accionEndoso","${accionEndoso}",
					"motivoEndoso","${motivoEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
			@Result(name=INPUT,type="redirectAction", params={"actionName","mostrarExclusionTexto",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/exclusionTexto",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"namespaceOrigen","${namespaceOrigen}",
					"actionNameOrigen","${actionNameOrigen}",
					"accionEndoso","${accionEndoso}",
					"motivoEndoso","${motivoEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"})
	})
	public String cotizar(){
		String[] continuitiesStringIds = null;
		
		if(!getElementosSeleccionados().isEmpty()) {
			continuitiesStringIds = getElementosSeleccionados().split(",");			
		}
		
		if(continuitiesStringIds != null)
			idsSeleccionados = obtenerIdsSeleccionados(continuitiesStringIds, idsSeleccionados);
		
		
		idsSeleccionados = idsSeleccionados.replaceAll(", ", "");
		if(!idsSeleccionados.isEmpty()) {
			continuitiesStringIds = idsSeleccionados.split(",");			
		}
		
		endosoService.guardaCotizacionEndosoExclusionTexto(continuitiesStringIds, cotizacion);		
		
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();
		return SUCCESS;
	}
	
	public void prepareEmitir() {
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","${actionNameOrigen}",
					    "namespace","${namespaceOrigen}",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() {
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if(endoso!=null){
			setMensajeExito();
		}	
		return SUCCESS;
	}	

	public Integer getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Integer numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}
	
	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

	public void setListaTextosAdicionales(Collection<BitemporalTexAdicionalCot> listaTextosAdicionales) {
		this.listaTextosAdicionales = listaTextosAdicionales;
	}

	public Collection<BitemporalTexAdicionalCot> getListaTextosAdicionales() {
		return listaTextosAdicionales;
	}

	public void setListaTextosAdicionalesProc(
			Collection<BitemporalTexAdicionalCot> listaTextosAdicionalesProc) {
		this.listaTextosAdicionalesProc = listaTextosAdicionalesProc;
	}

	public Collection<BitemporalTexAdicionalCot> getListaTextosAdicionalesProc() {
		return listaTextosAdicionalesProc;
	}

	
	
}
