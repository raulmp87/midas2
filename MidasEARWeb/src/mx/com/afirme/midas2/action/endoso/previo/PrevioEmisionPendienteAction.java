package mx.com.afirme.midas2.action.endoso.previo;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.danios.reportes.endosocancelable.ReporteEndososCancelables;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/endoso/previo")
public class PrevioEmisionPendienteAction extends BaseAction implements Preparable {
	
	@Action (value = "mostrarPrevioEmisionContenedor", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/previoEmisionPendienteContenedor.jsp") 
	})
	public String mostrarPrevioEmisionContenedor(){
		return SUCCESS;		
	} 
	
	@Action (value = "mostrarPrevioCancelacionDanios", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/previoCancelacionDanios.jsp") 
	})
	public String mostrarPrevioCancelacionDanios(){
		return SUCCESS;		
	} 
	

	@Action (value = "generarPrevioCancelacionDanios", results = { 			
			@Result(name= SUCCESS, type="stream" , params={ 
					"contentType","${contentType}",
					"inputName","previoEmisionInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name= ERROR, location="/jsp/error.jsp")	})			
	public String generarPrevioCancelacionDanios(){
		byte[] byteArray = null;		
		try {
			if(fechaCorteDanios == null){
				throw new RuntimeException("Invalid date");
			}			
			ReporteEndososCancelables reporteCancelables = new ReporteEndososCancelables(fechaCorteDanios);			
			byteArray = reporteCancelables.obtenerReporte("");			
			previoEmisionInputStream = new ByteArrayInputStream(byteArray);
			contentType = "application/xls";
			DateFormat formato = new SimpleDateFormat("ddMMyyyy");
			fileName = "cancelacionesPendientesDanios_" + formato.format(fechaCorteDanios) + ".xls";									
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;		
	} 
	
	@Action (value = "mostrarPrevioCancelacion", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/previoCancelacion.jsp") 
	})
	public String mostrarPrevioCancelacion() {
		
		tiposEmisiones = new LinkedHashMap<Short, String>();
		tiposEmisiones.put(TipoEmision.CANC_POL_AUTOS.getTipoEmision(), "Póliza");
		tiposEmisiones.put(TipoEmision.CANC_END_AUTOS.getTipoEmision(), "Endoso");		
		
		try {
			
			jobRunning = isCancelacionesJobInProgress();
			
		} catch (SchedulerException e) {
			
			e.printStackTrace();
		}					
		
		return SUCCESS;
	}
	
	
	@Action (value = "listarCancelacionesPaginado", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") 
	})
	public String listarCancelacionesPaginado() {
		
		if (fechaValidacion != null) {			
			
			filtros.setFechaValidacion(fechaValidacion);
			filtros.setAuxiliarFlag(fileName);//filtro.setNumeroPoliza(fileName);//TODO cambiar con nombres mas apropiados. Esto es para evitar la invocacion del SP de Seycos cuando no sea una busqueda explicita
			if(getTotalCount() == null){
				setTotalCount(emisionPendienteService.obtenerTotalPaginacionCancelacionesPendientesAutos(filtros));
				setListadoEnSession(null);
			}
			setPosPaginado();
			
		}
		
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action
	(value = "listarCancelaciones", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/previoCancelacionGrid.jsp") })
	public String listarCancelaciones() {
		
		if (fechaValidacion != null) {
			
			if(!listadoDeCache()){
				
				filtros.setFechaValidacion(fechaValidacion);
				filtros.setPrimerRegistroACargar(getPosStart());
				filtros.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
				emisionesPendientes = emisionPendienteService.buscarCancelacionesPendientesAutos(filtros);
				setListadoEnSession(emisionesPendientes);
			}else{
				emisionesPendientes = (List<EmisionPendiente>) getListadoPaginado();
				setListadoEnSession(emisionesPendientes);
			}			
		}
		
		return SUCCESS;
	}
		
	@Action(value ="getExcelCancelacionesPendientes", results = {
			@Result(name= SUCCESS, type="stream" , params={ 
					"contentType","${contentType}",
					"inputName","previoEmisionInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""
			}),
			@Result(name= INPUT, location="/jsp/error.jsp")	})
	public String getExcelCancelacionesPendientes() {
		
		if (fechaValidacion != null) {
			SimpleDateFormat formato = new SimpleDateFormat("ddMMyyyy");
			
			//filtros = new EmisionPendiente();
			filtros.setFechaValidacion(fechaValidacion);
			
			TransporteImpresionDTO respuesta = emisionPendienteService.getExcelCancelacionesPendientesAutos(filtros);
			
			previoEmisionInputStream = new ByteArrayInputStream(respuesta.getByteArray());
			contentType = "application/xls";
			fileName = "cancelacionesPendientesAutos_" + formato.format(fechaValidacion) + ".xls";	
			
		}
		
		return SUCCESS;
	}
		
	@Action
	(value = "actualizarBloqueo", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/previoCancelacionGrid.jsp") })
	public String actualizarBloqueo() {
		
		emisionPendienteService.actualizaBloqueo(id, bloqueo);
		return SUCCESS;
	}
	
	@Action
	(value = "emitirPendiente", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/previoCancelacionGrid.jsp") })
	public String emitirPendiente() {
		
		emisionPendienteService.emitePendiente(id);
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action
	(value = "aplicarAccionSeleccionadas", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/previoCancelacionGrid.jsp") })
	public String aplicarAccionSeleccionadas() {
		
		String[] continuitiesStringIds = null;
		idsSeleccionados = idsSeleccionados.replaceAll(", ", "");
		if(!idsSeleccionados.isEmpty()) 
		{
			continuitiesStringIds = idsSeleccionados.split(",");			
		}		
		
		if(accionSeleccionada == CANCELAR_SELECCIONADAS)
		{
			for(String idString:continuitiesStringIds)
			{
				emisionPendienteService.emitePendiente(Long.valueOf(idString));						
			}
			
		}else if(accionSeleccionada == BLOQUEAR_DESBLOQUEAR_SELECCIONADAS)
		{				
			for(String idString:continuitiesStringIds)
			{
				emisionPendienteService.actualizaBloqueo(Long.valueOf(idString), bloqueo);								
			}			
		}				
		
		setMensajeExito();
		return SUCCESS;
	}
	
	@Action(value = "iniciarCancelacionAutomatica", results = {
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarPrevioCancelacion",
						"namespace","/endoso/previo",						
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"}),
			@Result(name = INPUT, location = "/jsp/endosos/previo/cancelacion/previoCancelacionGrid.jsp") })
	public String iniciarCancelacionAutomatica() {
		try {
			
			if(!isCancelacionesJobInProgress())
			{
				System.out.println("Scheduler Name:: "
						+ scheduler.getSchedulerName());
				String[] groups = scheduler.getJobGroupNames();
				JobDataMap parameters = new JobDataMap();
				parameters.put("dias", 0);
				for (String jobGroup : groups) {
					String[] jobNames = scheduler.getJobNames(jobGroup);
					for (String jobName : jobNames) {
						if (jobName.equalsIgnoreCase(TAREA_CANCELACION_POLIZAS)
								|| jobName.equalsIgnoreCase(TAREA_CANCELACION_ENDOSOS)) {							
							scheduler.triggerJobWithVolatileTrigger(jobName,
									jobGroup,  parameters);
						}
					}
				}				
			}
			else
			{
				setMensaje("No se puede arrancar el proceso de Cancelación Automática, ya que existe un proceso en curso");
				return SUCCESS;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			setMensajeError("Ha ocurrido un error al intentar arrancar el proceso de Cancelación Automática "
					+ e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);
			return INPUT;
		}

		setMensajeExito();
		return SUCCESS;
	}	
	
	@Action(value = "monitorearProcesoCancelacionAutomatica", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/previo/cancelacion/monitoreoCancelacionAutomatica.jsp") })
	public String monitorearProcesoCancelacionAutomatica() {		
		
		EmisionPendiente filtro = new EmisionPendiente();
		filtro.setFechaValidacion(TimeUtils.getDateTime(new Date()).toDate());		
		cifras = emisionPendienteService.monitorearAvanceEmisionPendiente(filtro);		
		
		return SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}
	
	
	private static final long serialVersionUID = -4466888571785761637L;

	private Long id;
	private Date fechaValidacion;
	private Date fechaCorteDanios;	
	private Short bloqueo;
	private EmisionPendiente filtros = new EmisionPendiente();
	private List<EmisionPendiente> emisionesPendientes = new ArrayList<EmisionPendiente>();

	private InputStream previoEmisionInputStream;
	private String contentType;
	private String fileName;
	private String idsSeleccionados;
	private int accionSeleccionada;
	private static final int CANCELAR_SELECCIONADAS = 1;
	private static final int BLOQUEAR_DESBLOQUEAR_SELECCIONADAS = 2;
	private Map<Short, String> tiposEmisiones = new LinkedHashMap<Short, String>();
	private static final String TAREA_CANCELACION_POLIZAS = "tareaEmiteEndososCancelacionPolizaAuto";
	private static final String TAREA_CANCELACION_ENDOSOS = "tareaEmiteEndososCancelacionEndosoAuto";
	
	private Scheduler scheduler;
	private boolean jobRunning;
	Map<String, Long> cifras = new LinkedHashMap<String, Long>();	

	@Autowired
	@Qualifier("schedulerFactoryBeanEndosos")
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}	

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getFechaValidacion() {
		return fechaValidacion;
	}

	public void setFechaValidacion(Date fechaValidacion) {
		this.fechaValidacion = fechaValidacion;
	}

	public Short getBloqueo() {
		return bloqueo;
	}
	
	public void setBloqueo(Short bloqueo) {
		this.bloqueo = bloqueo;
	}
	
	public List<EmisionPendiente> getEmisionesPendientes() {
		return emisionesPendientes;
	}
	
	public void setEmisionesPendientes(List<EmisionPendiente> emisionesPendientes) {
		this.emisionesPendientes = emisionesPendientes;
	}
		
	public InputStream getPrevioEmisionInputStream() {
		return previoEmisionInputStream;
	}

	public void setPrevioEmisionInputStream(InputStream previoEmisionInputStream) {
		this.previoEmisionInputStream = previoEmisionInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	private EmisionPendienteService emisionPendienteService;
	
	@Autowired
	@Qualifier("emisionPendienteServiceEJB")
	public void setEmisionPendienteService(
			EmisionPendienteService emisionPendienteService) {
		this.emisionPendienteService = emisionPendienteService;
	}


	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}


	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}


	public int getAccionSeleccionada() {
		return accionSeleccionada;
	}


	public void setAccionSeleccionada(int accionSeleccionada) {
		this.accionSeleccionada = accionSeleccionada;
	}


	public Map<Short, String> getTiposEmisiones() {
		return tiposEmisiones;
	}


	public void setTiposEmisiones(Map<Short, String> tiposEmisiones) {
		this.tiposEmisiones = tiposEmisiones;
	}


	public EmisionPendiente getFiltros() {
		return filtros;
	}


	public void setFiltros(EmisionPendiente filtros) {
		this.filtros = filtros;
	}

	public boolean getJobRunning() {
		return jobRunning;
	}

	public void setJobRunning(boolean jobRunning) {
		this.jobRunning = jobRunning;
	}

	public Map<String, Long> getCifras() {
		return cifras;
	}


	public void setCifras(Map<String, Long> cifras) {
		this.cifras = cifras;
	}	
	
	private boolean isCancelacionesJobInProgress() throws SchedulerException
	{		
		boolean jobRunning = false;
		
			List<JobExecutionContext> jobExecutionContextscheduler = scheduler.getCurrentlyExecutingJobs();
			Iterator<JobExecutionContext> itCurrentJobsRunning = jobExecutionContextscheduler.iterator();
			while(itCurrentJobsRunning.hasNext())
			{
				JobExecutionContext jobExecutionContext = itCurrentJobsRunning.next(); 
				if(jobExecutionContext.getJobDetail().getName().equalsIgnoreCase(TAREA_CANCELACION_POLIZAS) || 
						jobExecutionContext.getJobDetail().getName().equalsIgnoreCase(TAREA_CANCELACION_ENDOSOS))
				{
					jobRunning = true;
					break;
				}				
			}
		return jobRunning;		
	}

	public Date getFechaCorteDanios() {
		return fechaCorteDanios;
	}

	public void setFechaCorteDanios(Date fechaCorteDanios) {
		this.fechaCorteDanios = fechaCorteDanios;
	}	
		
}
