package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.complementos;

import java.math.BigDecimal;
import java.util.Date;

import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.cliente.ClienteAsociadoJPAService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/complementos")
public class DatosContratanteAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	

	private Date fechaIniVigenciaEndoso;	

	private PolizaDTO polizaDTO;
	private BitemporalCotizacion cotizacion;
	private Long cotizacionContinuityId;
	
	private boolean negocioAsociado;
	private Long idToNegocio;
	private Negocio negocio;
	private ClienteDTO cliente;
	private BigDecimal idCliente;
	
	private EntidadService entidadService;
	private EntidadBitemporalService entidadBitemporalService;
	private EndosoService endosoService;
	private ClienteFacadeRemote clienteFacade;
	private ClienteAsociadoJPAService clienteAsociadoJPAService;
	
	private String accionEndoso;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@Autowired
	@Qualifier("clienteAsociadoJPAServiceEJB")
	public void setClienteAsociadoJPAService(
			ClienteAsociadoJPAService clienteAsociadoJPAService) {
		this.clienteAsociadoJPAService = clienteAsociadoJPAService;
	}
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	
	@Override
	public void prepare() throws Exception {
		
	}	
	
	public void prepareVerDatosContratante(){
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, cotizacionContinuityId, 
																	TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		isClienteAsociado(cotizacion);
		
			if (cotizacion.getValue().getPersonaContratanteId() != null) {
				try {
					filtro.setIdCliente(BigDecimal.valueOf(cotizacion.getValue().getPersonaContratanteId()));
					filtro = clienteFacade.loadByIdNoAddress(filtro);
					setCliente(filtro);
					// Set nombre de Persona moral
					if (getCliente().getClaveTipoPersona() == 2) {
						getCliente().setNombre(filtro.getNombreCompleto());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			setListadoEnSession(null);		
	}	
	
	@Action
	(value = "verDatosContratante", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/datosContratante.jsp") })
	public String verDatosContratante() {
		return SUCCESS;
	}
	
	
	@Action
	(value = "seleccionarContratante", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/datosContratante.jsp"),
			@Result(name=INPUT, type="redirectAction", params={
					"actionName","verDatosContratante",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/complementos",
					"cotizacionContinuityId","${cotizacionContinuityId}",								
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"
				})
	})
	public String seleccionarContratante() {
		if (cotizacionContinuityId != null) {
			cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			
			ClienteGenericoDTO clienteGenerico = endosoService.getClienteGenerico(idCliente);
			if (!endosoService.actualizarDatosContratanteCotizacion(cotizacion, clienteGenerico, 
					this.getUsuarioActual().getNombreUsuario(), fechaIniVigenciaEndoso)) {
				
				setMensajeError("No es posible continuar con la emisi\u00F3n del endoso ya que  el domicilio del contratante se encuentra incompleto ó cambia el IVA ya registrado");
				setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				return INPUT;
			}
			cliente = clienteGenerico;	
			isClienteAsociado(cotizacion);
			
			super.setMensajeExito();
			setNextFunction("parent.closeVentana()");
				// Si el nuevo contratante tiene un iva valida si cambio
			/*try{
				boolean cambioIVA = cotizacionService.validaIVACotizacion(idToCotizacion, this.getUsuarioActual().getNombreUsuario());
				if(cambioIVA){		
					super.setMensajeExitoPersonalizado("IVA ajustado debido a domicilio fiscal del Contrante, favor de terminar la cotizaci\u00f3n.");
//					setNextFunction("cambiarEstatusEnProceso("+idToCotizacion+")");
					return "changeCotizacion";	
				}
			} catch(Exception ex) {
				super.setMensajeError("Hubo un problema al verificar el iva, debido a que el domicilio del contratante est\u00e1 incorrecto");
				super.setTipoMensaje(BaseAction.TIPO_MENSAJE_INFORMACION);
				return SUCCESS;
			}*/		
		}
		return SUCCESS;
	}
	
	
	public void prepareMostrarClientesAsociados() {
		negocio = entidadService.findById(Negocio.class, idToNegocio);
	}
	
	@Action
	(value = "mostrarClientesAsociados", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/complementar/complementarEmision/clientesAsociados.jsp") })
	public String mostrarClientesAsociados() {
		return SUCCESS;
	}
	
	private void isClienteAsociado(BitemporalCotizacion cotizacion) {
		setIdToNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
		
		if (!clienteAsociadoJPAService.listarClientesAsociados(idToNegocio).isEmpty()) {
			setNegocioAsociado(true);
		}
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}


	public boolean isNegocioAsociado() {
		return negocioAsociado;
	}


	public void setNegocioAsociado(boolean negocioAsociado) {
		this.negocioAsociado = negocioAsociado;
	}


	public Long getIdToNegocio() {
		return idToNegocio;
	}


	public void setIdToNegocio(Long idToNegocio) {
		this.idToNegocio = idToNegocio;
	}


	public Negocio getNegocio() {
		return negocio;
	}


	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}


	public ClienteDTO getCliente() {
		return cliente;
	}


	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public BigDecimal getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}
	
	

}

