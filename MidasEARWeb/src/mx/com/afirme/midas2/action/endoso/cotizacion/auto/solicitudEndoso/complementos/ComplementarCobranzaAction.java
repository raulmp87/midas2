package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.complementos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;


@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarCobranza")
public class ComplementarCobranzaAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long cotizacionContinuityId;	
	private Date validoEn;
	private Short radioCobranza;
	private BitemporalCotizacion biCotizacion;
	private String accionEndoso;
	private Short tipoEndoso;
	private String numeroPolizaFormateadoReferencia;
	 
	private EndosoService endosoService;
	private EntidadBitemporalService entidadBitemporalService;
	private EntidadService entidadService;

	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	
	@Override
	public void prepare() throws Exception {
	}
	
	public void prepareMostrarPrevisualizarCobranza()  {
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
			biCotizacion = entidadBitemporalService.getByKey(CotizacionContinuity.class, 
																			cotizacionContinuityId, TimeUtils.getDateTime(validoEn));
		} else {
			biCotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, 
					cotizacionContinuityId, TimeUtils.getDateTime(validoEn));
		}
		
		
		if (biCotizacion != null && biCotizacion.getValue().getClaveAfectacionPrimas() != null) {
			radioCobranza = biCotizacion.getValue().getClaveAfectacionPrimas();
		}
	}
	
	@Action
	(value = "mostrarPrevisualizarCobranza", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/previsualizarCobranza.jsp") })
	public String mostrarPrevisualizarCobranza() {
		
		if(radioCobranza.equals(Cotizacion.CVE_AFECTACION_ABONO_POLIZA) && biCotizacion.getValue().getPolizaReferencia()!= new Long(0)){
			
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class,  new BigDecimal(biCotizacion.getValue().getPolizaReferencia()));
			if (poliza != null) {
				numeroPolizaFormateadoReferencia = poliza.getNumeroPolizaFormateada();
			}
		}
	
		return SUCCESS;
	}
	
	
	@Action
	(value = "guardarCobranza", results = {					
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/complementos/previsualizarCobranza.jsp"),
			@Result(name = INPUT, location = "/jsp/endosos/solicitudEndoso/complementos/previsualizarCobranza.jsp")})
	
	public String guardarCobranza() throws Exception {
		Long idToPolizaReferenciada = null;
		if(radioCobranza.equals(Cotizacion.CVE_AFECTACION_ABONO_POLIZA)){
			if (numeroPolizaFormateadoReferencia != null && numeroPolizaFormateadoReferencia.trim().length() > 0) {
				String[] elementosNumPoliza = numeroPolizaFormateadoReferencia.split("-");
				Map<String,Object> mapa = new HashMap<String,Object>();
				mapa.put("codigoProducto", StringUtils.rightPad(elementosNumPoliza[0].substring(0,2),8)); //Ajuste para compatibilidad con tipo Char(8) de la columna en la BD
				mapa.put("codigoTipoPoliza", StringUtils.rightPad(elementosNumPoliza[0].substring(2),8)); //Ajuste para compatibilidad con tipo Char(8) de la columna en la BD
				mapa.put("numeroPoliza", Integer.valueOf(elementosNumPoliza[1]));
				mapa.put("numeroRenovacion", Integer.valueOf(elementosNumPoliza[2]));
				List<PolizaDTO> listaRes = entidadService.findByProperties(PolizaDTO.class, mapa);
				if (!listaRes.isEmpty()) {
					PolizaDTO poliza = listaRes.get(0);
					idToPolizaReferenciada = poliza.getIdToPoliza().longValue();
				}else{
					setMensajeError(getText("midas.endosos.solicitudEndoso.definirSolicitudEndoso.mensaje.polizaNoExiste"));
					setTipoMensaje(TIPO_MENSAJE_INFORMACION);
					return INPUT;
				}
				
			}else{
				setMensajeError(getText("midas.endosos.solicitudEndoso.definirSolicitudEndoso.mensaje.polizaReferenciaRequerida"));
				setTipoMensaje(TIPO_MENSAJE_INFORMACION);
				return INPUT;
			}
		}		
		endosoService.guardarCobranzaCotizacion(cotizacionContinuityId,idToPolizaReferenciada, TimeUtils.getDateTime(validoEn), radioCobranza, tipoEndoso);
			
		super.setMensajeExito();
		//setNextFunction("closeVentanaAseguradoEndosoCambioDatos()");
		
		return SUCCESS;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}

	public Short getRadioCobranza() {
		return radioCobranza;
	}

	public void setRadioCobranza(Short radioCobranza) {
		this.radioCobranza = radioCobranza;
	}

	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public String getNumeroPolizaFormateadoReferencia() {
		return numeroPolizaFormateadoReferencia;
	}

	public void setNumeroPolizaFormateadoReferencia(
			String numeroPolizaFormateadoReferencia) {
		this.numeroPolizaFormateadoReferencia = numeroPolizaFormateadoReferencia;
	}
	
	

}
