package mx.com.afirme.midas2.action.endoso.cotizacion.auto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.SolicitudPolizaService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto")
public class CotizacionEndosoAction extends BaseAction implements Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<CatalogoValorFijoDTO> tiposEndoso = new ArrayList<CatalogoValorFijoDTO>(1);
	private List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>(1);
	private Integer numeroPoliza;
	private String nombreAsegurado;
	private String numeroCotizacion;	
	private Integer tipoEndoso;
	private Boolean conflictoNoSerie;
	private Date fechaCotizacionInicio;
	private Date fechaCotizacionFin;
    private EntidadService entidadService;    
    private CotizacionEndosoDTO filtrosBusqueda;
    private CotizacionEndosoService cotizacionEndosoService;
    private Boolean filtrar= false;
    private String idsSeleccionados;    
    
    @Autowired
    @Qualifier("cotizacionEndosoServiceEJB")
    public void setCotizacionEndosoService(
			CotizacionEndosoService cotizacionEndosoService) {
		this.cotizacionEndosoService = cotizacionEndosoService;
	}

	//Test    
	private SolicitudPolizaService solicitudPolizaService;
	
	@Autowired
	@Qualifier("solicitudPolizaServiceEJB")
	public void setSolicitudPolizaService(
			SolicitudPolizaService solicitudPolizaService) {
		this.solicitudPolizaService = solicitudPolizaService;
	}
    
    private List<SolicitudDTO> solicitudes = new ArrayList<SolicitudDTO>(1);
    
    public void setSolicitudes(List<SolicitudDTO> solicitudes) {
		this.solicitudes = solicitudes;
	}

	public List<SolicitudDTO> getSolicitudes() {
		return solicitudes;
	}
	//
   
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub

	}
	
	@Action
	(value = "listar", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/cotizacionEndosoListado.jsp") })
	public String listar() 
	{
		tiposEndoso = entidadService.findByProperty(CatalogoValorFijoDTO.class, "id.idGrupoValores", 331);
		this.getReturnFields(CotizacionEndosoAction.class, this);
		return SUCCESS;
	}
	
	@Action
	(value = "busquedaRapidaPaginada", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") })
	public String busquedaRapidaPaginada() {	
		
		
		return SUCCESS;
	}
	
	@Action
	(value = "busquedaPaginada", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") })
	public String busquedaPaginada()
	{								
		if (filtrosBusqueda != null){
			if(filtrar){
				if(getTotalCount() == null){
					setTotalCount(cotizacionEndosoService.obtenerTotalPaginacionCotizacionesEndoso(filtrosBusqueda));
					setListadoEnSession(null);
				}
				setPosPaginado();
			}
		}
		
				
		return SUCCESS;
	}
	
	@Action
	(value = "busquedaRapida", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/cotizacionEndosoListadoGrid.jsp") })
	public String busquedaRapida(){
		
		
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action
	(value = "buscarSolicitud", results = { 
			@Result(name = SUCCESS, location = "/jsp/endosos/cotizacionEndosoListadoGrid.jsp") })
	public String buscarSolicitud(){				
		if (filtrosBusqueda != null){
			if(filtrar){
				if(!listadoDeCache()){
					this.saveReturnFields(CotizacionEndosoAction.class, this);
					filtrosBusqueda.setPrimerRegistroACargar(getPosStart());
					filtrosBusqueda.setNumeroMaximoRegistrosACargar(REGISTROS_A_MOSTRAR);
					listaCotizacionesEndoso = cotizacionEndosoService.buscarCotizacionesEndosoFiltrado(filtrosBusqueda);
					setListadoEnSession(listaCotizacionesEndoso);
				}else{
					listaCotizacionesEndoso = (List<CotizacionEndosoDTO>) getListadoPaginado();
					setListadoEnSession(listaCotizacionesEndoso);
				}
			}
		}
		
		return SUCCESS;
	}
	
	@Action
	(value = "verCotizacion", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","listar","namespace","/endoso/cotizacion/auto"})})
	public String verCotizacion() 
	{
		//TODO 
		System.out.println("verCotizacion()");
		
		return SUCCESS;
	}
	
	@Action
	(value = "editarCotizacion", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","listar","namespace","/endoso/cotizacion/auto"})})
	public String editarCotizacion() 
	{
		//TODO 
		System.out.println("editarCotizacion()");
		
		return SUCCESS;
	}
	
	@Action
	(value = "imprimirCotizacion", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","listar","namespace","/endoso/cotizacion/auto"})})
	public String imprimirCotizacion() 
	{
		//TODO 
		System.out.println("imprimirCotizacion()");
		
		return SUCCESS;
	}
	
	@Action
	(value = "cancelarCotizacion", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","listar","namespace","/endoso/cotizacion/auto"})})
	public String cancelarCotizacion() 
	{
		//TODO 
		System.out.println("cancelarCotizacion()");
		
		return SUCCESS;
	}
	
	@Action
	(value = "emitir", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","listar","namespace","/endoso/cotizacion/auto"})})
	public String emitir() 
	{
		//TODO 
		System.out.println("emitir()");
		
		return SUCCESS;
	}	
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public Integer getNumeroPoliza() {
		return numeroPoliza;
	}

	public void setNumeroPoliza(Integer numeroPoliza) {
		this.numeroPoliza = numeroPoliza;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	public String getNumeroCotizacion() {
		return numeroCotizacion;
	}

	public void setNumeroCotizacion(String numeroCotizacion) {
		this.numeroCotizacion = numeroCotizacion;
	}	

	public Integer getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Integer tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public Boolean getConflictoNoSerie() {
		return conflictoNoSerie;
	}

	public void setConflictoNoSerie(Boolean conflictoNoSerie) {
		this.conflictoNoSerie = conflictoNoSerie;
	}

	public Date getFechaCotizacionInicio() {
		return fechaCotizacionInicio;
	}

	public void setFechaCotizacionInicio(Date fechaCotizacionInicio) {
		this.fechaCotizacionInicio = fechaCotizacionInicio;
	}

	public Date getFechaCotizacionFin() {
		return fechaCotizacionFin;
	}

	public void setFechaCotizacionFin(Date fechaCotizacionFin) {
		this.fechaCotizacionFin = fechaCotizacionFin;
	}	

	public List<CatalogoValorFijoDTO> getTiposEndoso() {
		return tiposEndoso;
	}

	public void setTiposEndoso(List<CatalogoValorFijoDTO> tiposEndoso) {
		this.tiposEndoso = tiposEndoso;
	}

	public CotizacionEndosoDTO getFiltrosBusqueda() {
		return filtrosBusqueda;
	}

	public void setFiltrosBusqueda(CotizacionEndosoDTO filtrosBusqueda) {
		this.filtrosBusqueda = filtrosBusqueda;
	}

	public List<CotizacionEndosoDTO> getListaCotizacionesEndoso() {
		return listaCotizacionesEndoso;
	}

	public void setListaCotizacionesEndoso(
			List<CotizacionEndosoDTO> listaCotizacionesEndoso) {
		this.listaCotizacionesEndoso = listaCotizacionesEndoso;
	}

	public Boolean getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(Boolean filtrar) {
		this.filtrar = filtrar;
	}

	public String getIdsSeleccionados() {
		return idsSeleccionados;
	}

	public void setIdsSeleccionados(String idsSeleccionados) {
		this.idsSeleccionados = idsSeleccionados;
	}

}
