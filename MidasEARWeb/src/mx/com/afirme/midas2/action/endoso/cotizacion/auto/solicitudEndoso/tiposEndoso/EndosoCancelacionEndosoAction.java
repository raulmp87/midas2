package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso")
public class EndosoCancelacionEndosoAction extends BaseAction implements
		Preparable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date fechaIniVigenciaEndoso;
	private BitemporalCotizacion biCotizacion;
	private Collection<EndosoDTO> lstEndosoDto = new ArrayList<EndosoDTO>(1);
	private Long polizaId;
	private Long cotizacionContinuityId;
	private String accionEndoso;
	private PolizaDTO polizaDTO;
	private ResumenCostosDTO resumenCostosDTO;
	private String siguienteEndoso;
	private Short numeroEndoso;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO;

	private EndosoService endosoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private EntidadService entidadService;
	private EntidadBitemporalService entidadBitemporalService;
	private CalculoService calculoService;
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));		

	}

	@Action(value = "mostrarCancelacionEndoso", results = {
			@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/cancelacionEndoso/endosoCancelacionEndoso.jsp"),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "${actionNameOrigen}", "namespace",
					"${namespaceOrigen}", "idPolizaBusqueda", "${polizaId}",
					"numeroPolizaFormateado",
					"${polizaDTO.numeroPolizaFormateada}", "mensaje",
					"${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String mostrarCancelacionEndoso() {
		
		if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())) {

			biCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
															TimeUtils.getDateTime(fechaIniVigenciaEndoso));	
			if(biCotizacion==null){
				biCotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
						CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion(), 
																TimeUtils.getDateTime(fechaIniVigenciaEndoso));	
			}
			cotizacionContinuityId = biCotizacion.getContinuity().getId();
			resumenCostosDTO = calculoService.resumenCotizacionEndoso(
					biCotizacion.getEntidadContinuity().getBusinessKey(),
					TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		} else {
			endosoService.getCotizacionEndosoCancelacionEndoso(polizaDTO.getIdToPoliza());
			resumenCostosDTO = new ResumenCostosDTO();
		}
		
		siguienteEndoso = endosoService.getNumeroEndoso(polizaDTO.getIdToPoliza(), "%06d");		
		
		return SUCCESS;
	}

	public void prepareEmitir() {
		biCotizacion = endosoService.prepareCotizacionEndoso(cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
	}

	@Action(value = "emitir", results = { @Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "mostrarDefTipoModificacion", "namespace",
			"/endoso/cotizacion/auto/solicitudEndoso",
			"numeroPolizaFormateado", "${polizaDTO.numeroPolizaFormateada}",
			"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String emitir() {

		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(
							cotizacionContinuityId, 
							TimeUtils.getDateTime(fechaIniVigenciaEndoso), 
							SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO);
		String numeroSolicitudCheque = "";
		if(endoso!=null){
			if(endoso.getSolictudCheque()!=null){
				numeroSolicitudCheque = ". N\u00Famero solicitud Seycos del cheque: "+endoso.getSolictudCheque(); 
			}
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue())+numeroSolicitudCheque);
		}
		return SUCCESS;
	}

	public void prepareCotizar() {
		polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
	}

	@Action(value = "cotizar", results = { 
			@Result(name = SUCCESS, type = "redirectAction", params = {
				"actionName",
				"mostrarCancelacionEndoso",
				"namespace",
				"/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso",
				"polizaId", "${polizaId}", "fechaIniVigenciaEndoso",
				"${fechaIniVigenciaEndoso}", "accionEndoso",
				"${accionEndoso}", "mensaje", "${mensaje}", "tipoMensaje",
				"${tipoMensaje}" }),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName",
					"mostrarCancelacionEndoso",
					"namespace",
					"/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/endosoCancelacionEndoso",
					"polizaId", "${polizaId}", "fechaIniVigenciaEndoso",
					"${fechaIniVigenciaEndoso}", "accionEndoso",
					"${accionEndoso}", "mensaje", "${mensaje}", "tipoMensaje",
					"${tipoMensaje}" })
			})
	public String cotizar() {
		
		biCotizacion = endosoService.prepareCotizacionEndosoCancelacion(polizaDTO, SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO,
				TimeUtils.getDateTime(fechaIniVigenciaEndoso), 
				accionEndoso, new Long(numeroEndoso), SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO);
		
		endosoService.guardaCotizacionCancelacionEndoso(biCotizacion, TimeUtils.getDateTime(fechaIniVigenciaEndoso), 
															polizaDTO.getIdToPoliza(), numeroEndoso, accionEndoso, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO);
		setAccionEndoso(TipoAccionDTO.getEditarEndosoCot());
		setMensajeExito();
		return SUCCESS;
	}

	@Action(value = "cancelar", results = { @Result(name = SUCCESS, type = "redirectAction", params = {
			"actionName", "mostrarDefTipoModificacion", "namespace",
			"/endoso/cotizacion/auto/solicitudEndoso",
			"numeroPolizaFormateado", "${polizaDTO.numeroPolizaFormateada}",
			"esRetorno", "${esRetorno}",
			"mensaje", "${mensaje}", "tipoMensaje", "${tipoMensaje}" }) })
	public String cancelar() {
		System.out.println("cancelar()");
		this.setEsRetorno(1);
		return SUCCESS;
	}

	@Action(value = "obtenerEndososPaginado", results = { @Result(name = SUCCESS, location = "/jsp/suscripcion/solicitud/paginadoGrid.jsp") })
	public String obtenerEndososPaginado() {
		
		Collection<EndosoDTO> listado = new ArrayList<EndosoDTO>();
		if (getTotalCount() == null) {
			listado = endosoService.findForCancel(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue());
			setLstEndosoDto(listado);
			setTotalCount(new Integer(listado.size()).longValue());
		}
		setPosPaginado();
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(value = "obtenerEndosos", results = { @Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/cancelacionEndoso/endosoCancelacionEndosoGrid.jsp") })
	public String obtenerEndosos() {
		
		Collection<EndosoDTO> listado = new ArrayList<EndosoDTO>();

		if (!super.listadoDeCache()) {

			listado = endosoService.findForCancel(polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue());

		} else {
			listado = (List<EndosoDTO>) getListadoPaginado();
		}

		setLstEndosoDto(listado);
		setListadoEnSession(listado);

		return SUCCESS;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public Collection<EndosoDTO> getLstEndosoDto() {
		return lstEndosoDto;
	}

	public void setLstEndosoDto(Collection<EndosoDTO> lstEndosoDto) {
		this.lstEndosoDto = lstEndosoDto;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public Long getCotizacionContinuityId() {
		return cotizacionContinuityId;
	}

	public void setCotizacionContinuityId(Long cotizacionContinuityId) {
		this.cotizacionContinuityId = cotizacionContinuityId;
	}

	public String getSiguienteEndoso() {
		return siguienteEndoso;
	}

	public void setSiguienteEndoso(String siguienteEndoso) {
		this.siguienteEndoso = siguienteEndoso;
	}

	public Short getNumeroEndoso() {
		return numeroEndoso;
	}

	public void setNumeroEndoso(Short numeroEndoso) {
		this.numeroEndoso = numeroEndoso;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}
	
	

}
