package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.action.poliza.seguroobligatorio.GeneraSeguroObligatorio;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos")
public class EndosoMovimientosAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = -6297934584242462933L;
	
	private	List<ExcepcionSuscripcionReporteDTO> excepcionesList;
	private Long id;
	private Boolean aplicaEndoso = true;
	
	
	private RecuotificacionService recuotificacionService;
	
	private SeguroObligatorioService seguroObligatorioService;
	
	@Autowired
	@Qualifier("seguroObligatorioServiceEJB")
	public void setSeguroObligatorioService(SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}
	
	@Autowired
	@Qualifier("recuotificacionServiceEJB")
	public void setRecuotificacionService(RecuotificacionService recuotificacionService) {
		this.recuotificacionService = recuotificacionService;
	}
	
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService;
	
	@Autowired
	@Qualifier("configuracionDatoIncisoBitemporalServiceEJB")
	public void setConfiguracionDatoIncisoBitemporalService(
			ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService) {
		this.configuracionDatoIncisoBitemporalService = configuracionDatoIncisoBitemporalService;
	}
	
	@Override
	public void prepare() throws Exception {
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));
		setEsExterno(this.usuarioService.getUsuarioActual().isOutsider());
	}
	
	public void prepareMostrar() {
	//	this.setNextFunction("validar(); return false;");
		polizaDTO = entidadService.findById(PolizaDTO.class,new BigDecimal(polizaId));	
		cotizacion = endosoService.getCotizacionEndosoMovimientos(new BigDecimal(polizaId), fechaIniVigenciaEndoso, accionEndoso);
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
	}	

	@Action (value = "mostrar", results = {
		@Result(name = SUCCESS, location = "/jsp/endosos/solicitudEndoso/tiposEndoso/movimientos/detalle.jsp"),
		@Result(name = INPUT, type = "redirectAction", params = {
			"actionName","${actionNameOrigen}",
			"namespace","${namespaceOrigen}",
			"idPolizaBusqueda","${polizaId}",								
			"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
			"mensaje","${mensaje}",
			"tipoMensaje","${tipoMensaje}"
		})
	})
	public String mostrar() {
		
		if((getActionNameOrigen() != null && !getActionNameOrigen().isEmpty()) || (getNamespaceOrigen() != null && !getNamespaceOrigen().isEmpty()))
		{
			ServletActionContext.getContext().getSession().put("actionNameOrigen", getActionNameOrigen());
			ServletActionContext.getContext().getSession().put("namespaceOrigen", getNamespaceOrigen());			
		}
		
		if (cotizacion.getValue().getSolicitud().getCodigoAgente() != null) {				
			agente = entidadService.findById(Agente.class, cotizacion.getValue().getSolicitud().getCodigoAgente().longValue());
		}
		
		formasdePago = listadoService.getMapFormasdePago(cotizacion.getValue().getSolicitud());	
		
		derechosEndosoList = listadoService.getMapDerechosEndoso(cotizacion.getValue().getSolicitud());
		
		if(accionEndoso != null && accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot())){
			mensajeDTO = configuracionDatoIncisoBitemporalService.validarListaParaEmitir(cotizacion, tipoEndoso, fechaIniVigenciaEndoso);
		}
		
		return SUCCESS;
	}
	
	@Action(value = "cancelar", results = {
		@Result(name=SUCCESS, type="redirectAction", params={
			"actionName", "${actionNameOrigen}",
			"namespace", "${namespaceOrigen}",
			"numeroPolizaFormateado", "${polizaDTO.numeroPolizaFormateada}",
			"mensaje", "${mensaje}",
			"esRetorno", "${esRetorno}",			
			"tipoMensaje", "${tipoMensaje}"
		})
	})
	public String cancelar() {
		this.setEsRetorno(1);
		if(this.getActionNameOrigen() == null || this.getActionNameOrigen().isEmpty()){
			try{
				this.setActionNameOrigen(ServletActionContext.getContext().getSession().get("actionNameOrigen").toString());
				this.setNamespaceOrigen(ServletActionContext.getContext().getSession().get("namespaceOrigen").toString());
			}catch(Exception e){
				this.setActionNameOrigen("listar");
				this.setNamespaceOrigen("/endoso/cotizacion/auto");
			}
		}
		return SUCCESS;
	}
	
    public void prepareCotizar() 
    {		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
    @Action
	(value = "cotizar", results = { 
			@Result(name=SUCCESS,type="redirectAction", params={"actionName","mostrar",
					"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos",
					"actionNameOrigen", "${actionNameOrigen}",
					"namespaceOrigen", "${namespaceOrigen}",
					"polizaId","${polizaId}",
					"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
					"accionEndoso","${accionEndoso}",
					"mensaje","${mensaje}",
					"tipoMensaje","${tipoMensaje}"}),
					@Result(name=INPUT,type="redirectAction", params={"actionName","mostrar",
							"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/movimientos",
							"polizaId","${polizaId}",
							"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
							"namespaceOrigen","${namespaceOrigen}",
							"actionNameOrigen","${actionNameOrigen}",
							"accionEndoso","${accionEndoso}",
							"motivoEndoso","${motivoEndoso}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}"})
	})
	public String cotizar() 
	{	
    	endosoService.validaPrimaNetaCeroEndosoCotizacion(tipoEndoso, polizaId, fechaIniVigenciaEndoso);
    	
		endosoService.guardaCotizacionEndosoMovimientos(cotizacion);
		accionEndoso = TipoAccionDTO.getEditarEndosoCot();
		setMensajeExito();		
		
		return SUCCESS;
	}
    
	public void prepareTerminar(){		
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
	@Action(value = "terminar", results = {
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDetalle",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso/tiposEndoso/altaInciso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"polizaId","${polizaId}",
						"tipoEndoso","${tipoEndoso}",
						"accionEndoso","${accionEndoso}",
						"fechaIniVigenciaEndoso","${fechaIniVigenciaEndoso}",
						"cotizacion.continuity.id","${cotizacion.continuity.id}",
						"namespaceOrigen","${namespaceOrigen}",
						"actionNameOrigen","${actionNameOrigen}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"}),
			@Result(name=ERROR, location = "/jsp/suscripcion/cotizacion/auto/resultadoTerminarCotizacion.jsp")
			})
	public String terminar() {
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.terminarCotizacionEndoso(cotizacion,  TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		
		if (terminarCotizacionDTO != null) {
			setId(cotizacion.getContinuity().getNumero().longValue());
			setExcepcionesList(terminarCotizacionDTO.getExcepcionesList());
			super.setMensajeError(terminarCotizacionDTO.getMensajeError());			
			return ERROR;			
		}		
		setMensajeExito();
		
		return SUCCESS;
	}
	
	public void prepareEmitir() 
	{
		cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(fechaIniVigenciaEndoso));		
	}
	
	@Action
	(value = "emitir", results = {
			@Result(name=SUCCESS,type="redirectAction", 
					params={"actionName","mostrarDefTipoModificacion",
						"namespace","/endoso/cotizacion/auto/solicitudEndoso",
						"numeroPolizaFormateado","${polizaDTO.numeroPolizaFormateada}",
						"mensaje","${mensaje}",
						"tipoMensaje","${tipoMensaje}"})})
	public String emitir() 
	{
		
		try{
			
			BigDecimal grupoParametro = new BigDecimal("22");
			LOG.info("Obteniendo valores de parametros de configuracion de recuotificacion");
			Integer banderaRecuotificacion = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, new BigDecimal("22030"));
			Integer banderaPrevioRecibos = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, new BigDecimal("22040"));
			
			if (banderaRecuotificacion.intValue() == 1){
				
			
			BigDecimal idToCotizacion = new BigDecimal(cotizacion.getContinuity().getNumero().longValue());
			LOG.info("Validando Previo de recibos para cotizacion: " + idToCotizacion);
			//SE AGREGA VALIDACION DE PREVIO DE RECIBOS AL EMITIR
			//ParametroGeneralDN parametroGeneralDN = ParametroGeneralDN.getINSTANCIA();
			//ParametroGeneralId idpg = new ParametroGeneralId();
			//idpg.setIdToGrupoParametroGeneral(new BigDecimal("22"));
			//idpg.setCodigoParametroGeneral(new BigDecimal("22010"));
			//ParametroGeneralDTO parametroGeneralDTO = parametroGeneralDN.getPorId(idpg);
		
			boolean previoDeRecibos = recuotificacionService.validarPrevioDeRecibos(idToCotizacion);
			if (previoDeRecibos){
				LOG.info("Existe Previo de recibos, validando saldos de Recuotificacion");
				Map<String, Object> res = recuotificacionService.validaTotalesDeCotizacionSalir(idToCotizacion);
				BigDecimal resultado = (BigDecimal) res.get("outIdEjecucion");
				if (resultado.toString().equals("0")){
					LOG.info("No existen saldos, continuando con emision");
				}else{
					LOG.info("Existen saldos por asignar a PP, pausando emision");
					super.setMensajeError("Existen saldos de Recuotificacion. Verifique.");					
					return SUCCESS;
				}
			}else{
				LOG.info("No existe previo de recuotificacion, validando previo de recibos");
				if(banderaPrevioRecibos.intValue() == 1){
					LOG.info("Bandera de Recuotificacion encendida, Pausando emision");
					super.setMensajeError("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");					
					return SUCCESS;
				}else{
					LOG.info("Bandera de Recuotificacion apagada, continuando con la emision.");
				}	
			}
			}else{
				LOG.info("Bandera de recuotificacion deshabilitada, continuando con el flujo natural de la operacion");
			}
		}catch(Exception e){
			LOG.info("Error en validacion previa re recuotificacion: " + e.getMessage());
			super.setMensajeError("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");					
			return SUCCESS;
		}
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(fechaIniVigenciaEndoso),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if(endoso!=null){
			setMensajeExitoPersonalizado(BaseAction.MENSAJE_EXITO_ENDOSO+String.format("%06d", endoso.getId().getNumeroEndoso().intValue()));
			//Emite Endoso Datos SO
			try{
				if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_CAMBIO_DATOS)){
					GeneraSeguroObligatorio generaSeguroObligatorio = new GeneraSeguroObligatorio();
					generaSeguroObligatorio.setSeguroObligatorioService(seguroObligatorioService);
					generaSeguroObligatorio.creaPolizaAnexaEndosoCambioDatos(polizaDTO, endoso.getId().getNumeroEndoso().shortValue());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}		
		
		return SUCCESS;
	}	
	
	private Long polizaId;
	private Date fechaIniVigenciaEndoso;
	private BitemporalCotizacion cotizacion;
	private Agente agente;
	private MensajeDTO mensajeDTO;
	private Map<Integer, String> formasdePago = new HashMap<Integer, String>();
	private BitemporalInciso inciso;
	private ResumenCostosDTO resumenCostosDTO;
	private String accionEndoso;
	private Short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS;
	private PolizaDTO polizaDTO;
	private Map<Long, String> derechosEndosoList = new HashMap<Long, String>();
	private Short motivoEndoso;
	
	public Long getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(Long polizaId) {
		this.polizaId = polizaId;
	}

	public Date getFechaIniVigenciaEndoso() {
		return fechaIniVigenciaEndoso;
	}

	public void setFechaIniVigenciaEndoso(Date fechaIniVigenciaEndoso) {
		this.fechaIniVigenciaEndoso = fechaIniVigenciaEndoso;
	}

	public BitemporalCotizacion getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(BitemporalCotizacion cotizacion) {
		this.cotizacion = cotizacion;
	}
	
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public MensajeDTO getMensajeDTO() {
		return mensajeDTO;
	}

	public void setMensajeDTO(MensajeDTO mensajeDTO) {
		this.mensajeDTO = mensajeDTO;
	}
	
	public Map<Integer, String> getFormasdePago() {
		return formasdePago;
	}

	public void setFormasdePago(Map<Integer, String> formasdePago) {
		this.formasdePago = formasdePago;
	}
	
	public Map<Long, String> getDerechosEndosoList() {
		return derechosEndosoList;
	}

	public void setDerechosEndosoList(Map<Long, String> derechosEndosoList) {
		this.derechosEndosoList = derechosEndosoList;
	}

	public BitemporalInciso getInciso() {
		return inciso;
	}

	public void setInciso(BitemporalInciso inciso) {
		this.inciso = inciso;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}
	

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}


	private EndosoService endosoService;
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	private EntidadService entidadService;
	
	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
		
	private ListadoService listadoService;
	
	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	private CalculoService calculoService;
	
	@Autowired
	@Qualifier("calculoServiceEJB")
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	@Autowired
	@Qualifier("emisionEndosoBitemporalServiceEJB")
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	public Short getTipoEndoso() {
		return tipoEndoso;
	}

	public void setTipoEndoso(Short tipoEndoso) {
		this.tipoEndoso = tipoEndoso;
	}

	public PolizaDTO getPolizaDTO() {
		return polizaDTO;
	}

	public void setPolizaDTO(PolizaDTO polizaDTO) {
		this.polizaDTO = polizaDTO;
	}

	public void setExcepcionesList(List<ExcepcionSuscripcionReporteDTO> excepcionesList) {
		this.excepcionesList = excepcionesList;
	}

	public List<ExcepcionSuscripcionReporteDTO> getExcepcionesList() {
		return excepcionesList;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setAplicaEndoso(Boolean aplicaEndoso) {
		this.aplicaEndoso = aplicaEndoso;
	}

	public Boolean getAplicaEndoso() {
		return aplicaEndoso;
	}
	
	public Short getMotivoEndoso() {
		return motivoEndoso;
	}

	public void setMotivoEndoso(Short motivoEndoso) {
		this.motivoEndoso = motivoEndoso;
	}
}
