package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.tiposEndoso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;

public class EndosoPorPeticionModel {
	private Long noEndoso;
	private String noEndosoFormateado;
	private Long noPoliza;
	private String noPolizaFormateado;
	private List<EndosoDummy> endosos;

	private String endosoIdsApply;

	private String tituloPage;
	private String actionNameOrigen;
	private String namespaceOrigen;
	private Boolean isCancelacionWindow;

	private EndosoService endosoService;

	// *************************************
	// Componente que incluyo muestra
	private BigDecimal totalPrimas;
	private BigDecimal descuentoComis;
	private BigDecimal primaNeta;
	private BigDecimal recargo;
	private BigDecimal derechos;
	private BigDecimal iva;
	private BigDecimal primaTotalPeriodo;

	private List<RegistroFuerzaDeVentaDTO> endososList = new ArrayList<RegistroFuerzaDeVentaDTO>(
			1); // TODO cambiar por el DTO correcto

	public BigDecimal getTotalPrimas() {
		return totalPrimas;
	}

	public void setTotalPrimas(BigDecimal totalPrimas) {
		this.totalPrimas = totalPrimas;
	}

	public BigDecimal getDescuentoComis() {
		return descuentoComis;
	}

	public void setDescuentoComis(BigDecimal descuentoComis) {
		this.descuentoComis = descuentoComis;
	}

	public BigDecimal getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(BigDecimal primaNeta) {
		this.primaNeta = primaNeta;
	}

	public BigDecimal getRecargo() {
		return recargo;
	}

	public void setRecargo(BigDecimal recargo) {
		this.recargo = recargo;
	}

	public BigDecimal getDerechos() {
		return derechos;
	}

	public void setDerechos(BigDecimal derechos) {
		this.derechos = derechos;
	}

	public BigDecimal getIva() {
		return iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getPrimaTotalPeriodo() {
		return primaTotalPeriodo;
	}

	public void setPrimaTotalPeriodo(BigDecimal primaTotalPeriodo) {
		this.primaTotalPeriodo = primaTotalPeriodo;
	}

	public List<RegistroFuerzaDeVentaDTO> getEndososList() {
		return endososList;
	}

	// Componente que incluyo muestra
	// *************************************

	public void setNoEndosoFormateado(String noEndosoFormateado) {
		this.noEndosoFormateado = noEndosoFormateado;
	}

	public void setNoPolizaFormateado(String noPolizaFormateado) {
		this.noPolizaFormateado = noPolizaFormateado;
	}

	public void setTituloPage(String tituloPage) {
		this.tituloPage = tituloPage;
	}

	public void setIsCancelacionWindow(Boolean isCancelacionWindow) {
		this.isCancelacionWindow = isCancelacionWindow;
	}

	public void setEndososList(List<RegistroFuerzaDeVentaDTO> endososList) {
		this.endososList = endososList;
	}

	public Long getNoEndoso() {
		return noEndoso;
	}

	public void setNoEndoso(Long noEndoso) {
		this.noEndoso = noEndoso;
	}

	public List<EndosoDummy> getEndosos() {
		return endosos;
	}

	public void setEndosos(List<EndosoDummy> endosos) {
		this.endosos = endosos;
	}

	public String getActionNameOrigen() {
		return actionNameOrigen;
	}

	public void setActionNameOrigen(String actionNameOrigen) {
		this.actionNameOrigen = actionNameOrigen;
	}

	public String getNamespaceOrigen() {
		return namespaceOrigen;
	}

	public void setNamespaceOrigen(String namespaceOrigen) {
		this.namespaceOrigen = namespaceOrigen;
	}

	public String getEndosoIdsApply() {
		return endosoIdsApply;
	}

	public void setEndosoIdsApply(String endosoIdsApply) {
		this.endosoIdsApply = endosoIdsApply;
	}

	public String getTituloPage() {
		return tituloPage;
	}

	public boolean getIsCancelacionWindow() {
		return isCancelacionWindow;
	}

	public Long getNoPoliza() {
		return noPoliza;
	}

	public void setNoPoliza(Long noPoliza) {
		this.noPoliza = noPoliza;
	}

	public String getNoPolizaFormateado() {
		return noPolizaFormateado;
	}

	public String getNoEndosoFormateado() {
		return noEndosoFormateado;
	}

	public void loadEndosos(String comm) {

		endosos = new ArrayList<EndosoDummy>();

		endosos.add(new EndosoDummy(false, new Long("1"), new Date(),
				new Integer("1"), new BigDecimal(123.4547), comm, false));
		endosos.add(new EndosoDummy(false, new Long("2"), new Date(),
				new Integer("2"), new BigDecimal(13.4542), comm, false));
		endosos.add(new EndosoDummy(false, new Long("3"), new Date(),
				new Integer("2"), new BigDecimal(23.4543), comm, false));

		endosos.add(new EndosoDummy(false, new Long("4"), new Date(),
				new Integer("1"), new BigDecimal(123.4547), comm, false));
		endosos.add(new EndosoDummy(false, new Long("5"), new Date(),
				new Integer("2"), new BigDecimal(13.4542), comm, false));
		endosos.add(new EndosoDummy(false, new Long("6"), new Date(),
				new Integer("2"), new BigDecimal(23.4543), comm, false));

		// se generan los ids.
		endosos.get(0).generarIdComposicion(endosos);

	}

	public class EndosoDummy {
		private boolean selected;
		private Long noEndoso;
		private Date fechaEmision;
		private int tipoMovimiento;
		private BigDecimal primaEndoso;
		private String comentarios;

		private String noEndosoFormateado;
		private String primaEndosoFormateado;
		private String styleRow;

		private String idComposicion;

		public EndosoDummy() {
		}

		public String getNoEndosoFormateado() {

			return noEndosoFormateado;
		}

		public void generarIdComposicion(List<EndosoDummy> lista) {
			for (int i = 0; i < lista.size(); i++) {
				EndosoDummy dd = lista.get(i);
				dd.setIdComposicion(i + "_" + dd.noEndoso.longValue());
			}
		}

		public List<Long> obtenerNoEndoso(String[] idComposites) {
			List<Long> ids = new ArrayList<Long>();
			for (int i = 0; i < idComposites.length; i++) {
				String[] res = idComposites[i].split("_");
				ids.add(Long.parseLong(res[1]));
			}
			return ids;
		}

		public String getPrimaEndosoFormateado() {
			return primaEndosoFormateado;
		}

		public EndosoDummy(boolean selected, Long noEndoso, Date fechaEmision,
				int tipoMovimiento, BigDecimal primaEndoso, String comentarios,
				boolean tieneProblemas) {
			super();
			this.selected = selected;
			this.noEndoso = noEndoso;
			this.fechaEmision = fechaEmision;
			this.tipoMovimiento = tipoMovimiento;
			this.primaEndoso = primaEndoso;
			this.comentarios = comentarios;

			DecimalFormat format = new DecimalFormat("000000");
			noEndosoFormateado = format.format(noEndoso);

			format.applyPattern("#0.00");
			primaEndosoFormateado = "$" + format.format(primaEndoso);

			if (tieneProblemas) {
				styleRow = "background:red";
			}
		}

		public boolean isSelected() {
			return selected;
		}

		public void setSelected(boolean selected) {
			this.selected = selected;
		}

		public Long getNoEndoso() {
			return noEndoso;
		}

		public void setNoEndoso(Long noEndoso) {
			this.noEndoso = noEndoso;
		}

		public Date getFechaEmision() {
			return fechaEmision;
		}

		public void setFechaEmision(Date fechaEmision) {
			this.fechaEmision = fechaEmision;
		}

		public int getTipoMovimiento() {
			return tipoMovimiento;
		}

		public void setTipoMovimiento(int tipoMovimiento) {
			this.tipoMovimiento = tipoMovimiento;
		}

		public BigDecimal getPrimaEndoso() {
			return primaEndoso;
		}

		public void setPrimaEndoso(BigDecimal primaEndoso) {
			this.primaEndoso = primaEndoso;
		}

		public String getComentarios() {
			return comentarios;
		}

		public void setComentarios(String comentarios) {
			this.comentarios = comentarios;
		}

		public String getStyleRow() {
			return styleRow;
		}

		public void setStyleRow(String styleRow) {
			this.styleRow = styleRow;
		}

		public void setStatus(boolean good) {
			if (good)
				this.styleRow = "";
			else
				this.styleRow = "background:red";
		}

		public String getIdComposicion() {
			return idComposicion;
		}

		public void setIdComposicion(String idComposicion) {
			this.idComposicion = idComposicion;
		}

	}

}
