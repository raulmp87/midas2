package mx.com.afirme.midas2.action.endoso.cotizacion.auto.solicitudEndoso.complementos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * @author Lizeth De La Garza
 *
 */
@Component
@Scope("prototype")
@Namespace("/endoso/cotizacion/auto/solicitudEndoso/complementos/complementarIncisoEndoso")
public class ComplementarIncisoEndosoAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 4488737212217776540L;	

	private ClienteFacadeRemote clienteFacade;
	private Map<String, String> datosRiesgo;
	//Es necesario poner esta expresion para poder realizar las validaciones de Struts 2. Es el OGNL Expression
	private static String datosRiesgoExpresion = "datosRiesgo";
	private ClienteDTO cliente;
	private Short copiarDatos = 0;
	private String name;

	private boolean guardadoExitoso = false;
	
	
	private EntidadService entidadService;
	private IncisoViewService incisoService;
	private EntidadBitemporalService entidadBitemporalService;
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService;
	
	private Map<String, String> valores;
	private List<ControlDinamicoRiesgoDTO> controles;
	private Collection<BitemporalInciso> biIncisoList;
	private BitemporalCotizacion biCotizacion;
	private BitemporalInciso biInciso;	
	private BitemporalAutoInciso biAutoInciso;	
	
	private ResumenCostosDTO 	resumenCostosDTO;
	
	private BigDecimal polizaId;
	private Long cotizacionId;
	private Long incisoId;
	private Date validoEn;
	private Date recordFrom;
	private Long validoEnMillis;
	private Long recordFromMillis;
	private boolean mostrarDatoConductorInciso = true;
	private String accionEndoso;
	private Short claveTipoEndoso;
	private Short soloConsulta = 0;
	private boolean mostrarSoloConductor = false;
	private Integer usuarioExterno = 2;
	
	@Autowired
	@Qualifier("clienteFacadeRemoteEJB")
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}
	
	@Autowired
	@Qualifier("incisoAutoServiceEJB")
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("entidadBitemporalServiceEJB")
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@Autowired
	@Qualifier("configuracionDatoIncisoBitemporalServiceEJB")
	public void setConfiguracionDatoIncisoBitemporalService(ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService) {
		this.configuracionDatoIncisoBitemporalService = configuracionDatoIncisoBitemporalService;
	}

	@Override
	public void prepare() throws Exception {		
		
	}

	
	public void prepareMostrar(){	
				
	}
	
	@Action
	(value = "mostrar", results = { 
			@Result(name = SUCCESS, location = "/jsp/poliza/auto/emision/contenedorVehiculo.jsp") })
	public String mostrar() {

		Usuario usuario = usuarioService.getUsuarioActual();
		if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
			usuarioExterno = usuario.getTipoUsuario();
		}
		
		if (TipoAccionDTO.getEditar().equals(accionEndoso) || TipoAccionDTO.getVer().equalsIgnoreCase(accionEndoso)) {
			biInciso = incisoService.getIncisoInProcess(incisoId, validoEn);
			biAutoInciso = biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(TimeUtils.getDateTime(validoEn));
			mostrarDatoConductorInciso = incisoService.infoConductorEsRequerido(incisoId, validoEn, true);//Ajuste para traer los inProcess
			accionEndoso = TipoAccionDTO.getEditar();
		} else if (TipoAccionDTO.getConsultarEndosoCot().equals(accionEndoso)) {
			
			if (recordFromMillis == null) {
				biInciso = incisoService.getInciso(incisoId, validoEn);
				biAutoInciso = incisoService.getAutoInciso(incisoId, validoEn);
			}			
			
			if (biInciso == null) {
				biInciso = incisoService.getInciso(incisoId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()), 
						EndosoDTO.TIPO_ENDOSO_CANCELACION);
			}
			
			if (biAutoInciso == null) {
				biAutoInciso = incisoService.getAutoInciso(incisoId, new Date(getValidoEnMillis()), new Date(getRecordFromMillis()), 
						EndosoDTO.TIPO_ENDOSO_CANCELACION);
			}
			DateTime recordFromDT = null;
			if (recordFromMillis != null) {
				recordFromDT = new DateTime(getRecordFromMillis());
			}
			
			mostrarDatoConductorInciso = incisoService.infoConductorEsRequerido(incisoId, new DateTime(getValidoEnMillis()),
																											recordFromDT, false);
		}		
			
		if (biAutoInciso.getValue().getPersonaAseguradoId() != null && biAutoInciso.getValue().getPersonaAseguradoId() > 0) {
			try {
				ClienteGenericoDTO filtro = new ClienteGenericoDTO();
				filtro.setIdCliente(BigDecimal.valueOf(biAutoInciso.getValue().getPersonaAseguradoId()));
				cliente = clienteFacade.loadByIdNoAddress(filtro);				
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (copiarDatos == 2 && cliente != null && cliente.getClaveTipoPersona().shortValue() != 2) {
				if(cliente.getNombre() != null) biAutoInciso.getValue().setNombreConductor(cliente.getNombre());
				if(cliente.getApellidoPaterno() != null) biAutoInciso.getValue().setPaternoConductor(cliente.getApellidoPaterno());
				if(cliente.getApellidoMaterno() != null) biAutoInciso.getValue().setMaternoConductor(cliente.getApellidoMaterno());
				if(cliente.getFechaNacimiento() != null) biAutoInciso.getValue().setFechaNacConductor(cliente.getFechaNacimiento());
				if(cliente.getDescripcionOcupacion() != null) biAutoInciso.getValue().setOcupacionConductor(cliente.getDescripcionOcupacion());
			} else if (cliente != null && cliente.getClaveTipoPersona().shortValue() == 2) {
				copiarDatos = 1;
				//super.setMensajeError("No se puede copiar una persona moral");
			}

		} else {
			cliente = new ClienteDTO();
		}
		
		if (claveTipoEndoso != null && claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS) {
			mostrarSoloConductor = true;
		}
		
		return SUCCESS;
	}
	
	
	@SuppressWarnings("unchecked")
	@Action
	(value = "cargaControles", results = { 
			@Result(name = SUCCESS, location = "/jsp/suscripcion/cotizacion/auto/controlDinamicoRiesgoDetalle.jsp") })
	public String cargaControles() {
		ValueStack valueStack = ActionContext.getContext().getValueStack();
		name = "datosRiesgo";

		valores = (Map<String, String>) valueStack.findValue(name,Map.class);
		if (valores == null) {
			valores = new LinkedHashMap<String, String>();
		}
		
		if (recordFromMillis != null && validoEnMillis != null) {
			controles = configuracionDatoIncisoBitemporalService.getDatosRiesgo(incisoId, new Date(validoEnMillis), new Date(recordFromMillis),	
					valores, cotizacionId, null, claveTipoEndoso, false);
		} else {
			controles = configuracionDatoIncisoBitemporalService.getDatosRiesgo(incisoId, validoEn, valores, cotizacionId, accionEndoso, true);
		}

		return SUCCESS;
	}
	
	public void prepareGuardar(){
		biInciso = incisoService.getIncisoInProcess(incisoId, validoEn);
		biAutoInciso = incisoService.getAutoIncisoInProcess(incisoService.getIncisoInProcess(incisoId, validoEn), validoEn);
	}
	
	@Action
	(value = "guardar", results = { 
			@Result(name = SUCCESS, location = "/jsp/poliza/auto/emision/contenedorVehiculo.jsp") })
	public String guardar(){
		if(biAutoInciso != null && biAutoInciso.getValue() != null){
			
			if(biAutoInciso.getValue().getNombreConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getNombreConductor());
				biAutoInciso.getValue().setNombreConductor(valor);
			}
			if(biAutoInciso.getValue().getMaternoConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getMaternoConductor());
				biAutoInciso.getValue().setMaternoConductor(valor);
			}
			if(biAutoInciso.getValue().getPaternoConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getPaternoConductor());
				biAutoInciso.getValue().setPaternoConductor(valor);
			}
			if(biAutoInciso.getValue().getOcupacionConductor() != null){
				String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getOcupacionConductor());
				biAutoInciso.getValue().setOcupacionConductor(valor);
			}
			if(biAutoInciso.getValue().getObservacionesinciso() != null){
				String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getObservacionesinciso());
				biAutoInciso.getValue().setObservacionesinciso(valor);
			}
			if(biAutoInciso.getValue().getNombreAsegurado() != null){
				String valor = UtileriasWeb.parseEncodingISO(biAutoInciso.getValue().getNombreAsegurado());
				biAutoInciso.getValue().setNombreAsegurado(valor);
			}
		}
		
		
		if(datosRiesgo != null){
			validarDatosRiesgo();
			// Si no existen errores guarda la Configuracion de Dato Inciso
			if (getFieldErrors().isEmpty()) {
				// Obtener los controles dinamicos y poner el valor al control
				// que tenemos en el map.
				
				configuracionDatoIncisoBitemporalService.guardarDatosAdicionalesPaquete(datosRiesgo, 
															incisoId, validoEn);
				/*incisoService.guardarDatosAdicionalesPaquete(datosRiesgo,
						new BigDecimal(inciso.getIncisoAutoCot()
								.getNegocioSeccionId()), inciso.getId()
								.getIdToCotizacion(), inciso.getId()
								.getNumeroInciso());		*/		
			} else {
				super.setMensajeListaPersonalizado(
						"Error al validar Datos del Paquete",
						new ArrayList<String>(1), BaseAction.TIPO_MENSAJE_ERROR);
				return SUCCESS;
			}
		}
		
		try {
			//validar numero de serie
			List<String> errors = NumSerieValidador.getInstance().validate(biAutoInciso.getValue().getNumeroSerie());					
			if (errors.isEmpty()) {
				//incisoService.complementarDatosInciso(inciso);
				entidadBitemporalService.saveInProcess(biInciso, TimeUtils.getDateTime(validoEn));
				entidadBitemporalService.saveInProcess(biAutoInciso, TimeUtils.getDateTime(validoEn));
				super.setMensajeExito();
				guardadoExitoso = true;
			} else {
				//biAutoInciso.getValue().setNumeroSerie("");
				entidadBitemporalService.saveInProcess(biInciso, TimeUtils.getDateTime(validoEn));
				entidadBitemporalService.saveInProcess(biAutoInciso, TimeUtils.getDateTime(validoEn));
				//incisoService.complementarDatosInciso(inciso);
				super.setMensajeListaPersonalizado("Datos guardados / Error al validar el n\u00FAmero de serie:", 
						errors, BaseAction.TIPO_MENSAJE_EXITO);
				guardadoExitoso = true;
				//return SUCCESS;
			}
		} catch (Exception ex) {
			super.setMensajeError(MENSAJE_ERROR_ACTUALIZAR);
		}
		setNextFunction("cerrarComplementarDatosInciso()");				
		return SUCCESS;
	}	

	private void validarDatosRiesgo() {
		//Mapa en donde se guarda la dependencia a partir de un id padre se obtiene el id hijo
		Map<String, String> childs = new LinkedHashMap<String, String>();
		//Mapa en donde se guarda la dependencia a partir de un id hijo se obtiene el id padre
		Map<String, String> parents = new LinkedHashMap<String, String>();
		//Lista en donde se guardan los id de los elementos que ya han sido procesados
		List<String> processedElements = new ArrayList<String>();
		List<ControlDinamicoRiesgoDTO> controlDinamicos = new ArrayList<ControlDinamicoRiesgoDTO>();

		//Obtener los controles dinamicos y poner el valor al control que tenemos en el map.
		for(Entry<String, String> entry : datosRiesgo.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			ConfiguracionDatoIncisoId id = configuracionDatoIncisoBitemporalService.getConfiguracionDatoIncisoId(key); 
			
			ControlDinamicoRiesgoDTO controlDinamicoRiesgoDTO = configuracionDatoIncisoBitemporalService.findByConfiguracionDatoIncisoId(id);
			
			if (controlDinamicoRiesgoDTO != null) { //Si no es null quiere decir que el control debe ser validado.
				controlDinamicoRiesgoDTO.setValor(value);
				controlDinamicos.add(controlDinamicoRiesgoDTO); // Lo agregamos a la lista para que sea procesado.
			}
		}
		
		for (ControlDinamicoRiesgoDTO control: controlDinamicos) {
			String id = control.getId();
			//1. Validar si el control no ha sido procesado
			if (!processedElements.contains(id)) {
					validarPorTipoValidador(control);
					processedElements.add(id);
			}
		}
	}
	
	private void validarPorTipoValidador(ControlDinamicoRiesgoDTO controlDinamicoDTO) {
		String id = controlDinamicoDTO.getId();
		String valor = controlDinamicoDTO.getValor();
		Integer tipoValidador = controlDinamicoDTO.getTipoValidador();
		
		//Ahorita en este metodo se va a validar para todos los controles requerido.
		if (StringUtils.isNotBlank(valor)) {
			if (tipoValidador != null) {
				switch (tipoValidador){
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS: 
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO:
					validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 0);
					break;
				}			
			}
		} else {
			addFieldError(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
					getText("error.requerido"));
		}
	}
	
	private void validarDecimal(String fieldName, String numero, int maxEnteros, int maxDecimales) {
		if (!UtileriasWeb.isDecimalValido(numero, maxEnteros, maxDecimales)) {
			addFieldError(fieldName, getText("error.decimal", 
					new String[] { String.valueOf(maxEnteros), String.valueOf(maxDecimales) }));
		}
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setDatosRiesgo(Map<String, String> datosRiesgo) {
		this.datosRiesgo = datosRiesgo;
	}

	public Map<String, String> getDatosRiesgo() {
		return datosRiesgo;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCopiarDatos(Short copiarDatos) {
		this.copiarDatos = copiarDatos;
	}

	public Short getCopiarDatos() {
		return copiarDatos;
	}	

	public void setGuardadoExitoso(boolean guardadoExitoso) {
		this.guardadoExitoso = guardadoExitoso;
	}

	public boolean isGuardadoExitoso() {
		return guardadoExitoso;
	}

	public List<ControlDinamicoRiesgoDTO> getControles() {
		return controles;
	}

	public void setControles(List<ControlDinamicoRiesgoDTO> controles) {
		this.controles = controles;
	}

	public Collection<BitemporalInciso> getBiIncisoList() {
		return biIncisoList;
	}

	public void setBiIncisoList(Collection<BitemporalInciso> biIncisoList) {
		this.biIncisoList = biIncisoList;
	}

	public BitemporalCotizacion getBiCotizacion() {
		return biCotizacion;
	}

	public void setBiCotizacion(BitemporalCotizacion biCotizacion) {
		this.biCotizacion = biCotizacion;
	}

	public BitemporalInciso getBiInciso() {
		return biInciso;
	}

	public void setBiInciso(BitemporalInciso biInciso) {
		this.biInciso = biInciso;
	}

	public BitemporalAutoInciso getBiAutoInciso() {
		return biAutoInciso;
	}

	public void setBiAutoInciso(BitemporalAutoInciso biAutoInciso) {
		this.biAutoInciso = biAutoInciso;
	}

	public ResumenCostosDTO getResumenCostosDTO() {
		return resumenCostosDTO;
	}

	public void setResumenCostosDTO(ResumenCostosDTO resumenCostosDTO) {
		this.resumenCostosDTO = resumenCostosDTO;
	}

	public BigDecimal getPolizaId() {
		return polizaId;
	}

	public void setPolizaId(BigDecimal polizaId) {
		this.polizaId = polizaId;
	}

	public Long getCotizacionId() {
		return cotizacionId;
	}

	public void setCotizacionId(Long cotizacionId) {
		this.cotizacionId = cotizacionId;
	}

	public Date getValidoEn() {
		return validoEn;
	}

	public void setValidoEn(Date validoEn) {
		this.validoEn = validoEn;
	}

	public boolean isMostrarDatoConductorInciso() {
		return mostrarDatoConductorInciso;
	}

	public void setMostrarDatoConductorInciso(boolean mostrarDatoConductorInciso) {
		this.mostrarDatoConductorInciso = mostrarDatoConductorInciso;
	}

	public String getAccionEndoso() {
		return accionEndoso;
	}

	public void setAccionEndoso(String accionEndoso) {
		this.accionEndoso = accionEndoso;
	}

	public Long getIncisoId() {
		return incisoId;
	}

	public void setIncisoId(Long incisoId) {
		this.incisoId = incisoId;
	}

	public Map<String, String> getValores() {
		return valores;
	}

	public void setValores(Map<String, String> valores) {
		this.valores = valores;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSoloConsulta(Short soloConsulta) {
		this.soloConsulta = soloConsulta;
	}

	public Short getSoloConsulta() {
		return soloConsulta;
	}

	public Short getClaveTipoEndoso() {
		return claveTipoEndoso;
	}

	public void setClaveTipoEndoso(Short claveTipoEndoso) {
		this.claveTipoEndoso = claveTipoEndoso;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}

	public Long getValidoEnMillis() {
		return validoEnMillis;
	}

	public void setValidoEnMillis(Long validoEnMillis) {
		this.validoEnMillis = validoEnMillis;
	}

	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public boolean isMostrarSoloConductor() {
		return mostrarSoloConductor;
	}

	public void setMostrarSoloConductor(boolean mostrarSoloConductor) {
		this.mostrarSoloConductor = mostrarSoloConductor;
	}	

	public Integer getUsuarioExterno() {
		return usuarioExterno;
	}

	public void setUsuarioExterno(Integer usuarioExterno) {
		this.usuarioExterno = usuarioExterno;
	}
	
}
