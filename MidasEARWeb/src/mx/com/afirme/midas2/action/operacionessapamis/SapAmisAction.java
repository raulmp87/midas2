package mx.com.afirme.midas2.action.operacionessapamis;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.amis.alertas.AlertaPortProxy;
import mx.com.afirme.midas2.amis.alertas.RespuestaAlerta;
import mx.com.afirme.midas2.amis.alertas.RespuestaDetalleAlerta;
import mx.com.afirme.midas2.amis.alertas.SapAmisRespuestaAccionesAlertas;
import mx.com.afirme.midas2.amis.alertas.SapAmisRespuestaAlertas;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAcciones;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAlertas;
import mx.com.afirme.midas2.dao.amis.alertas.RespuestaSapAmisAccion;
import mx.com.afirme.midas2.dao.amis.alertas.RespuestaSapAmisAlerta;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesLog;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.operacionessapamis.SapAmisService;
import mx.com.afirme.midas2.service.operacionessapamis.sapamisbitacoras.SapAmisBitacoraService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;

@Component
@Scope("prototype")
@Namespace("/sapAmis/emision")
public class SapAmisAction extends BaseAction implements Preparable {
	private static final long serialVersionUID = 1L;
	

	// Variables de redireccion
	private final String SAPAMISEMISION = "/jsp/sapAmis/sapAmisEmision.jsp";
	private final String SAPAMISBITACORAEMISION = "/jsp/sapAmis/sapAmisBitacoraEmision.jsp";
	private final String SAPAMISBITACORASINIESTROS = "/jsp/sapAmis/sapAmisBitacoraSiniestros.jsp";
	private final String SAPAMISCONSULTAALERTASEMISION = "/jsp/sapAmis/sapAmisConsultaAlertasEmision.jsp";
	private final String SAPAMISDETALLEALERTASEMISION = "/jsp/sapAmis/sapAmisDetalleAlertaEmision.jsp";
	private final String SAPAMISGRIDBITACORAEMISION = "/jsp/sapAmis/SapAmisBitacorasEmisionGrid.jsp";
	private final String SAPAMISGRIDBITACORASINIESTROS = "/jsp/sapAmis/SapAmisBitacorasSiniestrosGrid.jsp";
	private final String SAPAMISBITACORAEMISIONDETALLEALERTAS = "/jsp/sapAmis/sapAmisBitacoraEmisionDetAlert.jsp";
	private final String SAPAMISBITACORASINIESTROSDETALLEALERTAS = "/jsp/sapAmis/sapAmisBitacoraSiniestrosDetAlert.jsp";
	private final String SAPAMISALERTASLINEAGRID = "/jsp/sapAmis/SapAmisAlertasLineaGrid.jsp";
	private final String SAPAMISEJECUCIONESMANUALES = "/jsp/sapAmis/sapAmisEjecucionesManuales.jsp";
	private final String SAPAMISLISTADOLOG = "/jsp/sapAmis/sapAmisEjecucionesManualesGrid.jsp";
	private final String SAPAMISACCIONESALERTAS = "/jsp/sapAmis/sapAmisAccionesAlertas.jsp";
	private final String SAPAMISACCIONESALERTASGRID = "/jsp/sapAmis/sapAmisAccionesAlertasGrid.jsp";
	private final String SAPAMISACCIONESALERTASGRIDCELDA = "/jsp/sapAmis/sapAmisAccionesAlertasGridCelda.jsp";
	private final String SAPAMISACCIONESALERTASEXISTEN= "/jsp/sapAmis/sapAmisAccionesAlertasExisten.jsp";	
	private final String SAPAMISACCIONESALERTASNUEVOCOTIZADOR = "/jsp/suscripcion/cotizacion/auto/agente/include/alertas.jsp";

	private List<SapAmisBitacoraEmision> listaBitacoraEmision = new ArrayList<SapAmisBitacoraEmision>();
	private List<SapAmisBitacoraSiniestros> listaBitacoraSiniestros = new ArrayList<SapAmisBitacoraSiniestros>();

	SapAmisBitacoraService sapAmisBitacoraService;

	EntidadService entidadService;
    private SapAmisUtilsService sapAmisUtilsService;

	// Propiedad para llenar el detalle de alertas de la Emision
	private SapAlertasistemasEnvio detalleAlertasBitacoraEmision;
	// Propiedad para Consulta de alerta en linea
	List<RespuestaAlerta> respuesConsultaLinea;
	// Propiedad para Consulta de Detalle alerta en linea
	private RespuestaDetalleAlerta detalleAlertaLinea;
	
	List<SapAmisRespuestaAlertas> sapAmisRespuestaAlertas;
	SapAmisRespuestaAlertas sapAmisRespuestaAlertasAgergar;

	// Campos de la BitacoraEmision
	private String idEncabezadoAlertas;
	private String bitacoraPoliza;
	private String bitacoraVin;
	private String bitacoraFechaEnvio;
	private String estatusEnvio;
	private String cesvi;
	private String cii;
	private String emision;
	private String ocra;
	private String prevencion;
	private String pt;
	private String csd;
	private String siniestro;
	private String sipac;
	private String valuacion;

	private String fechaInicioEjecucionManual;
	private String fechaFinEjecucionManual;
	private int modulo;
	private String mensaje;
	private Long idsession;

	// Campo para la consulta de Alerta en Linea
	private String consultaNuemeroSerie;

	private GenerarPlantillaReporte generarPlantillaReporteService;
	private InputStream reporteAgenteStream;
	private String contentType;
	private String fileName;
	private Integer tipoReporte;

	//Variables necesarias para agregar Acciones Luis Ibarra 13/10/2015
	private String idAccionSapAmis;
	private String descripcionAccionSapAmis;
	private String alertaAccionSapAmis;
	private String sistemaAccionSapAmis;
	private String statusAccionSapAmis;
	private String existenAcciones;
	//Variables necesarias para consultar acciones Luis Ibarra 29/10/2015
	private String idCotizacion;
	private String idRelacionSapAmis;
	SapAmisService servicioSapAmis;
	private String nuevoCotizador;
	List<RespuestaSapAmisAlerta> respuestaSapAmisAlertas;

	@Action(value = "mostrarContenedor", results = { @Result(name = SUCCESS, location = SAPAMISEMISION) })
	public String mostrarContenedor() {
		return SUCCESS;
	}

	// Accion que cargar el primer Tab Bitacora de Envios
	@Action(value = "mostrarBitacoraSiniestros", results = { @Result(name = SUCCESS, location = SAPAMISBITACORASINIESTROS) })
	public String mostrarBitacoraSiniestros() {
		LogDeMidasWeb.log("Acceso al accion para cargar la pestaña de la Bitacora Siniestros", Level.INFO, null);
		return SUCCESS;
	}

	// Accion que cargar el primer Tab Bitacora de Envios
	@Action(value = "mostrarBitacoraEmision", results = { @Result(name = SUCCESS, location = SAPAMISBITACORAEMISION) })
	public String mostrarBitacoraEmision() {
		LogDeMidasWeb.log("Acceso al accion para cargar la pestaña de la Bitacora", Level.INFO, null);
		return SUCCESS;
	}

	// Accion que cargar el primer Tab Consulta Alertas
	@Action(value = "mostrarConsultaAlertasEmision", results = { @Result(name = SUCCESS, location = SAPAMISCONSULTAALERTASEMISION) })
	public String mostrarConsultaAlertasEmision() {
		LogDeMidasWeb.log("Acceso al accion para cargar la pestaña de la Consulta Alertas", Level.INFO, null);
		return SUCCESS;
	}

	// Accion que cargar el primer Tab Detalle de Alertas
	@Action(value = "mostrarDetalleAlertasEmision", results = { @Result(name = SUCCESS, location = SAPAMISDETALLEALERTASEMISION) })
	public String mostrarDetalleAlertasEmision() {
		LogDeMidasWeb.log("Acceso al accion para cargar la pestaña de la Detalle de Alertas", Level.INFO, null);
		return SUCCESS;
	}

	// Accion para mostrar el grid de la Bitacora de Envios Siniestros
	@Action(value = "gridBitacoraSiniestros", results = { @Result(name = SUCCESS, location = SAPAMISGRIDBITACORASINIESTROS) })
	public String gridBitacoraSiniestros() {
		LogDeMidasWeb.log("Acceso al accion para el grid de la bitacora de Siniestros", Level.INFO, null);
		return SUCCESS;
	}

	// Accion para mostrar el grid de la Bitacora de Envios Emision
	@Action(value = "gridBitacoraEmision", results = { @Result(name = SUCCESS, location = SAPAMISGRIDBITACORAEMISION) })
	public String gridBitacoraEmision() {
		LogDeMidasWeb.log("Acceso al accion para el grid de la bitacora de Emision", Level.INFO, null);
		return SUCCESS;
	}

	// Accion para mostrar el grid de la Bitacora de Envios Emision Filtrado
	@Action(value = "gridBitacoraEmisionFiltrado", results = { @Result(name = SUCCESS, location = SAPAMISGRIDBITACORAEMISION) })
	public String gridBitacoraEmisionFiltrado() {
		// LogDeMidasWeb.log(bitacoraPoliza + bitacoraVin + bitacoraFechaEnvio +
		// estatusEnvio + cesvi + cii + emision + ocra + prevencion + pt + csd +
		// siniestro + sipac + valuacion);
		// Hacer llamada al EJB que llenara la lista a mostrar
		listaBitacoraEmision = sapAmisBitacoraService.obtenerBitacoraEmisionFiltrada(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);
		LogDeMidasWeb.log("Acceso al accion para el grid de la bitacora de Emision Filtrado", Level.INFO, null);
		return SUCCESS;
	}

	// Accion para mostrar el grid de la Bitacora de Envios Emision Filtrado
	@Action(value = "gridBitacoraSiniestrosFiltrado", results = { @Result(name = SUCCESS, location = SAPAMISGRIDBITACORASINIESTROS) })
	public String gridBitacoraSiniestrosFiltrado() {
		// Hacer llamada al EJB que llenara la lista a mostrar

		// Datos de rastreo de de datos
		LogDeMidasWeb.log("Datos de Rastreo de informacion recibida desde la pantalla", Level.INFO, null);
		LogDeMidasWeb.log("bitacoraPoliza: " + bitacoraPoliza, Level.INFO, null);
		LogDeMidasWeb.log("bitacoraVin: " + bitacoraVin, Level.INFO, null);
		LogDeMidasWeb.log("bitacoraFechaEnvio: " + bitacoraFechaEnvio, Level.INFO, null);
		LogDeMidasWeb.log("estatusEnvio: " + estatusEnvio, Level.INFO, null);

		listaBitacoraSiniestros = sapAmisBitacoraService.obtenerBitacoraSiniestrosFiltrada(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);

		LogDeMidasWeb.log("Acceso al accion para el grid de la bitacora de Emision Filtrado", Level.INFO, null);
		return SUCCESS;
	}

	// Accion que cargar el Detalle de las alertas recivido en la bistacora de
	// Emision
	@Action(value = "mostrarDetalleAlertasBitacoraEmision", results = { @Result(name = SUCCESS, location = SAPAMISBITACORAEMISIONDETALLEALERTAS) })
	public String mostrarDetalleAlertasBitacoraEmision() {
		detalleAlertasBitacoraEmision = sapAmisBitacoraService.obtenerAlertas(idEncabezadoAlertas);
		LogDeMidasWeb.log(detalleAlertasBitacoraEmision.getValue(), Level.INFO, null);
		if (detalleAlertasBitacoraEmision != null) {
			LogDeMidasWeb.log(Integer.toString(detalleAlertasBitacoraEmision.getSapAlertasCesvis().size()), Level.INFO, null);
			LogDeMidasWeb.log(Integer.toString(detalleAlertasBitacoraEmision.getSapAlertasEmisions().size()), Level.INFO, null);
		} else {
			LogDeMidasWeb.log("Objeto encabezado vacio", Level.INFO, null);
		}
		LogDeMidasWeb.log("Acceso al accion para cargar la Pantalla de bitacora Detalle de Alertas", Level.INFO, null);
		return SUCCESS;
	}

	// Accion que cargar el Detalle de las alertas recivido en la bistacora de
	// Siniestros
	@Action(value = "mostrarDetalleAlertasBitacoraSiniestros", results = { @Result(name = SUCCESS, location = SAPAMISBITACORASINIESTROSDETALLEALERTAS) })
	public String mostrarDetalleAlertasBitacoraSiniestros() {
		detalleAlertasBitacoraEmision = sapAmisBitacoraService.obtenerAlertas(idEncabezadoAlertas);
		LogDeMidasWeb.log(detalleAlertasBitacoraEmision.getValue(), Level.INFO, null);
		if (detalleAlertasBitacoraEmision != null) {
			LogDeMidasWeb.log(Integer.toString(detalleAlertasBitacoraEmision.getSapAlertasCesvis().size()), Level.INFO, null);
			LogDeMidasWeb.log(Integer.toString(detalleAlertasBitacoraEmision.getSapAlertasEmisions().size()), Level.INFO, null);
		} else {
			LogDeMidasWeb.log("Objeto encabezado vacio", Level.INFO, null);
		}
		LogDeMidasWeb.log("Acceso al accion para cargar la Pantalla de bitacora Detalle de Alertas", Level.INFO, null);
		return SUCCESS;
	}

	// Muestra pantalla de consulta en linea
	@Action(value = "gridConsultaLinea", results = { @Result(name = SUCCESS, location = SAPAMISALERTASLINEAGRID) })
	public String gridConsultaLinea() {		
		
		return SUCCESS;
	}
	
	// Accion para realizar la consula de alertas en linea
	@Action(value = "consultaAlertasLinea", results = { @Result(name = SUCCESS, location = SAPAMISALERTASLINEAGRID) })
	public String consultaAlertasLinea() {
		AlertaPortProxy proxy = new AlertaPortProxy();
		respuesConsultaLinea = new ArrayList<RespuestaAlerta>();
		String[] login = sapAmisUtilsService.obtenerAccesos();
		if (consultaNuemeroSerie != null) {
			try {
				respuesConsultaLinea = proxy.consultaAlerta(login[0], login[1], consultaNuemeroSerie);
			} catch (Exception ex) {
				LogDeMidasWeb.log("Error de comunicacion Ws | ex: " + ex.getMessage(), Level.INFO, null);
			}
		}
		return SUCCESS;
	}
	
	
	@Action(value = "mostrarGridAccionesAlertas", results = { @Result(name = SUCCESS, location = SAPAMISACCIONESALERTASGRID) })
	public String mostrarGridAccionesAlertas(){
		
		return SUCCESS;
	}
	
	@Action(value = "agregarNuevaAccion", results = { @Result(name = SUCCESS, location = SAPAMISACCIONESALERTASGRIDCELDA) })
	public String agregarNuevaAccion(){
		String methodName = "agregarNuevaAccion():: ";
		SapAmisAcciones saa = new SapAmisAcciones();
		saa.setDescripcionAccion(descripcionAccionSapAmis);
		saa.setEstatus(new Long(0));
		LogDeMidasWeb.log(methodName + "idRelacionSapAmis=" + idRelacionSapAmis, Level.INFO, null);
		SapAmisAccionesRelacion sar = entidadService.findByProperty(SapAmisAccionesRelacion.class, "idSapAmisAccionesRelacion", Long.parseLong(idRelacionSapAmis)).get(0);
		LogDeMidasWeb.log(methodName + "Sar =" + sar.getVin(), Level.INFO, null);
		saa.setSapAmisAccionesRelacion(sar);
		idAccionSapAmis = (Long) entidadService.saveAndGetId(saa) + "";
		LogDeMidasWeb.log(methodName + "Resultado=" + idAccionSapAmis, Level.INFO, null);
		saa.setIdSapAmisAcciones(Long.parseLong(idAccionSapAmis));
		
		SapAmisAccionesLog log = new SapAmisAccionesLog();
		
		log.setTipoModificacion(1);
		log.setUsuario("USUARIO");
		log.setSapAmisAcciones(saa);
		log.setFecha(new Date());
		
		long idLog = (Long) entidadService.saveAndGetId(log);
		LogDeMidasWeb.log(methodName + "final=" + idLog, Level.INFO, null);
		
		return SUCCESS;
	}
	
	
	// Action para revisar si existen alertas
	@Action(value = "consultarSiHayAlertas", results = { @Result(name = SUCCESS, location = SAPAMISACCIONESALERTASEXISTEN) })
	public String consultarSiHayAlertas() {
		llenarGridAccionesAlertas();
		return SUCCESS;
	}
	
	
	// Accion para realizar la consula de alertas en linea
	@Action(value = "llenarGridAccionesAlertas", results = { @Result(name = SUCCESS, location = SAPAMISACCIONESALERTASGRID), 
															 @Result(name = INPUT, location = SAPAMISACCIONESALERTASNUEVOCOTIZADOR ) })
	public String llenarGridAccionesAlertas() {
		sapAmisRespuestaAlertas = new ArrayList<SapAmisRespuestaAlertas>();
		AlertaPortProxy proxy = new AlertaPortProxy();
		respuesConsultaLinea = new ArrayList<RespuestaAlerta>();
		EnvioConsultaAlertas eca = servicioSapAmis.getInfoEnvioConsultaAlertas(consultaNuemeroSerie);
		if(eca != null){
			ArrayList<EnvioConsultaAcciones> listaAlertas =  new ArrayList<EnvioConsultaAcciones>();
			for(int x = 0; x<eca.getVin().length; x++){
				try {
					respuesConsultaLinea = proxy.consultaAlerta(eca.getUser(), eca.getPass(), eca.getVin()[x]);
					EnvioConsultaAcciones ecAccion =  new EnvioConsultaAcciones();
					ecAccion.setVin(eca.getVin()[x]);
					RespuestaSapAmisAlerta[] rsaa = new RespuestaSapAmisAlerta[respuesConsultaLinea.size()];
					for(int y=0;y<respuesConsultaLinea.size();y++){
						rsaa[y] = new RespuestaSapAmisAlerta();
						rsaa[y].setAlerta(respuesConsultaLinea.get(y).getDescripcion());
						rsaa[y].setSistema(respuesConsultaLinea.get(y).getSistema());
						rsaa[y].setVin(eca.getVin()[x]);
					}
					ecAccion.setAlertas(rsaa);
					ecAccion.setIdCotizacion(this.idCotizacion);
					listaAlertas.add(ecAccion);
				} catch (Exception ex) {
					ex.printStackTrace();
					respuesConsultaLinea = null;
				}
			}
			this.respuestaSapAmisAlertas = servicioSapAmis.getAlertasSapAmisCotizacion(listaAlertas);
			if(respuestaSapAmisAlertas != null && respuestaSapAmisAlertas.size() > 0) {
				this.existenAcciones = "si";
				sapAmisRespuestaAlertas =  new ArrayList<SapAmisRespuestaAlertas>();
				for(int cantAlertas = 0; cantAlertas<respuestaSapAmisAlertas.size();cantAlertas++){
					SapAmisRespuestaAlertas alerta = new SapAmisRespuestaAlertas();
					alerta.setAlerta(respuestaSapAmisAlertas.get(cantAlertas).getAlerta());
					alerta.setIdAlerta(respuestaSapAmisAlertas.get(cantAlertas).getIdAlerta());
					alerta.setIdRelacion(respuestaSapAmisAlertas.get(cantAlertas).getIdRelacion());
					alerta.setSistema(respuestaSapAmisAlertas.get(cantAlertas).getSistema());
					alerta.setVin(respuestaSapAmisAlertas.get(cantAlertas).getVin());
					if(respuestaSapAmisAlertas.get(cantAlertas).getAcciones() != null && respuestaSapAmisAlertas.get(cantAlertas).getAcciones().size()>0) {
						for(int cantAcciones=0;cantAcciones<respuestaSapAmisAlertas.get(cantAlertas).getAcciones().size();cantAcciones++){
							Iterator<RespuestaSapAmisAccion> i = respuestaSapAmisAlertas.get(cantAlertas).getAcciones().iterator();
							ArrayList<SapAmisRespuestaAccionesAlertas> accionesList = new ArrayList<SapAmisRespuestaAccionesAlertas>();
							while(i.hasNext()) {
								SapAmisRespuestaAccionesAlertas acciones = new SapAmisRespuestaAccionesAlertas();
								
								RespuestaSapAmisAccion rsaaa = i.next();
								acciones.setIdAccion(rsaaa.getIdAccion());
								acciones.setDescAccion(rsaaa.getDescAccion());
								accionesList.add(acciones);
							}
							alerta.setAcciones(accionesList);
						}
					}
					sapAmisRespuestaAlertas.add(alerta);
					
				}
			} else {
				this.existenAcciones = "no";
			}
		}
		if(nuevoCotizador.equals("0")){
			return SUCCESS;
		} else {
			return INPUT;
		}
	}

	// Accion para realizar la consula de Detalle alertas en linea
	@Action(value = "buscaDetAlertLinea", results = { @Result(name = SUCCESS, location = SAPAMISDETALLEALERTASEMISION) })
	public String buscaDetAlertLinea() {
		String methodName = "buscaDetAlertLinea():: ";
		AlertaPortProxy proxy = new AlertaPortProxy();
		detalleAlertaLinea = new RespuestaDetalleAlerta();
		// Se obtiene el usuario y password de la base de datos
		String[] login = sapAmisUtilsService.obtenerAccesos();
		if (consultaNuemeroSerie != null) {
			try {
				LogDeMidasWeb.log(methodName + "consultaNuemeroSerie:" + consultaNuemeroSerie, Level.INFO, null);
				detalleAlertaLinea = proxy.detalleAlerta(login[0], login[1], consultaNuemeroSerie);
			} catch (Exception ex) {
				LogDeMidasWeb.log(methodName + "Error de comunicacion Ws", Level.SEVERE, null);
				LogDeMidasWeb.log(methodName + ex.getMessage(), Level.SEVERE, null);
			}
		}
		return SUCCESS;
	}

	@Action(value = "exportarToPDF", results = { @Result(name = SUCCESS, type = "stream", params = { "contentType", "${contentType}", "contentDisposition", "attachment;filename=\"${fileName}\"", "inputName", "reporteAgenteStream" }), @Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = { "mensaje", "${mensaje}" }) })
	public String exportarToPdf() {
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService().imprimirReporteDetalleAlertasSAP(idEncabezadoAlertas);

			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron informacion.");
				return "EMPTY";
			}
			setContentType("application/pdf");
			if (getTipoReporte() == null || getTipoReporte() == 1) {
				setFileName("DetalleAlertas.pdf");
			} else {
				setFileName("DetalleAlertas.pdf");
			}
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "exportarDetLineaToPDF", results = { @Result(name = SUCCESS, type = "stream", params = { "contentType", "${contentType}", "contentDisposition", "attachment;filename=\"${fileName}\"", "inputName", "reporteAgenteStream" }), @Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = { "mensaje", "${mensaje}" }) })
	public String exportarDetLineaToPDF() {
		try {

			/*
			 * Configuracion de Usuario y contraseña que consume los WS de
			 * BackEnd
			 */
			/* Configuracion para liberacion */
			/*
			 * TransporteImpresionDTO transporte =
			 * getGenerarPlantillaReporteService()
			 * .imprimirReporteDetalleAlertasLineaSAP(consultaNuemeroSerie,
			 * wsUsuario, wsPassword);
			 */
			/* Configuracion para el ambiente de capacitacion */

			// Se obtiene el usuario y password de la base de datos
			String[] login = sapAmisUtilsService.obtenerAccesos();

			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService().imprimirReporteDetalleAlertasLineaSAP(consultaNuemeroSerie, login[0], login[1]);

			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron informacion.");
				return "EMPTY";
			}
			setContentType("application/pdf");
			if (getTipoReporte() == null || getTipoReporte() == 1) {
				setFileName("DetalleAlertas.pdf");
			} else {
				setFileName("DetalleAlertas.pdf");
			}
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "exportarBitacoraEmisionFiltrado", results = { @Result(name = SUCCESS, type = "stream", params = { "contentType", "${contentType}", "contentDisposition", "attachment;filename=\"${fileName}\"", "inputName", "reporteAgenteStream" }), @Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = { "mensaje", "${mensaje}" }) })
	public String exportarBitacoraEmisionFiltrado() {
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService().imprimirBitacoraEmisionSAP(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);

			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron informacion.");
				return "EMPTY";
			}
			setContentType("application/xls");
			if (getTipoReporte() == null || getTipoReporte() == 1) {
				setFileName("reportePruebas.xls");
			} else {
				setFileName("reportePruebas.xls");
			}
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}

	@Action(value = "exportarBitacoraSiniestrosFiltrado", results = { @Result(name = SUCCESS, type = "stream", params = { "contentType", "${contentType}", "contentDisposition", "attachment;filename=\"${fileName}\"", "inputName", "reporteAgenteStream" }), @Result(name = INPUT, location = "/jsp/reportesAgentes/reporteError.jsp", params = { "mensaje", "${mensaje}" }) })
	public String exportarBitacoraSiniestrosFiltrado() {
		try {
			TransporteImpresionDTO transporte = getGenerarPlantillaReporteService().imprimirBitacoraSiniestrosSAP(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);
			if (transporte != null) {
				setReporteAgenteStream(new ByteArrayInputStream(transporte.getByteArray()));
			} else {
				setMensaje("No se obtuvieron informacion.");
				return "EMPTY";
			}
			setContentType("application/xls");
			if (getTipoReporte() == null || getTipoReporte() == 1) {
				setFileName("reportePruebas.xls");
			} else {
				setFileName("reportePruebas.xls");
			}
		} catch (RuntimeException error) {
			error.printStackTrace();
			return INPUT;
		}
		return SUCCESS;
	}
	
	/**
	 * @author Eduardo.Chavez
	 * 2015-09-30
	 */
	@Action(value = "mostrarContenidoAccionesAlertas", results = { @Result(name = SUCCESS, location = SAPAMISACCIONESALERTAS) })
	public String mostrarContenidoAccionesAlertas() {
		return SUCCESS;
	}
	
	


	public List<SapAmisBitacoraEmision> getListaBitacoraEmision() {
		return listaBitacoraEmision;
	}

	public void setListaBitacoraEmision(List<SapAmisBitacoraEmision> listaBitacoraEmision) {
		this.listaBitacoraEmision = listaBitacoraEmision;
	}

	public String getBitacoraPoliza() {
		return bitacoraPoliza;
	}

	public void setBitacoraPoliza(String bitacoraPoliza) {
		this.bitacoraPoliza = bitacoraPoliza;
	}

	public String getBitacoraVin() {
		return bitacoraVin;
	}

	public void setBitacoraVin(String bitacoraVin) {
		this.bitacoraVin = bitacoraVin;
	}

	public String getBitacoraFechaEnvio() {
		return bitacoraFechaEnvio;
	}

	public void setBitacoraFechaEnvio(String bitacoraFechaEnvio) {
		this.bitacoraFechaEnvio = bitacoraFechaEnvio;
	}

	public String getEstatusEnvio() {
		return estatusEnvio;
	}

	public void setEstatusEnvio(String estatusEnvio) {
		this.estatusEnvio = estatusEnvio;
	}

	public String getCesvi() {
		return cesvi;
	}

	public void setCesvi(String cesvi) {
		this.cesvi = cesvi;
	}

	public String getCii() {
		return cii;
	}

	public void setCii(String cii) {
		this.cii = cii;
	}

	public String getEmision() {
		return emision;
	}

	public void setEmision(String emision) {
		this.emision = emision;
	}

	public String getOcra() {
		return ocra;
	}

	public void setOcra(String ocra) {
		this.ocra = ocra;
	}

	public String getPrevencion() {
		return prevencion;
	}

	public void setPrevencion(String prevencion) {
		this.prevencion = prevencion;
	}

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}

	public String getCsd() {
		return csd;
	}

	public void setCsd(String csd) {
		this.csd = csd;
	}

	public String getSiniestro() {
		return siniestro;
	}

	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	public String getSipac() {
		return sipac;
	}

	public void setSipac(String sipac) {
		this.sipac = sipac;
	}

	public String getValuacion() {
		return valuacion;
	}

	public void setValuacion(String valuacion) {
		this.valuacion = valuacion;
	}

	public SapAmisBitacoraService getSapAmisBitacoraService() {
		return sapAmisBitacoraService;
	}

	public String getIdEncabezadoAlertas() {
		return idEncabezadoAlertas;
	}

	public void setIdEncabezadoAlertas(String idEncabezadoAlertas) {
		this.idEncabezadoAlertas = idEncabezadoAlertas;
	}

	@Autowired
	@Qualifier("sapAmisBitacoraEJB")
	public void setSapAmisBitacoraService(SapAmisBitacoraService sapAmisBitacoraService) {
		this.sapAmisBitacoraService = sapAmisBitacoraService;
	}

	public SapAlertasistemasEnvio getDetalleAlertasBitacoraEmision() {
		return detalleAlertasBitacoraEmision;
	}

	public void setDetalleAlertasBitacoraEmision(SapAlertasistemasEnvio detalleAlertasBitacoraEmision) {
		this.detalleAlertasBitacoraEmision = detalleAlertasBitacoraEmision;
	}

	public String getConsultaNuemeroSerie() {
		return consultaNuemeroSerie;
	}

	public void setConsultaNuemeroSerie(String consultaNuemeroSerie) {
		this.consultaNuemeroSerie = consultaNuemeroSerie;
	}

	public List<RespuestaAlerta> getRespuesConsultaLinea() {
		return respuesConsultaLinea;
	}

	public void setRespuesConsultaLinea(List<RespuestaAlerta> respuesConsultaLinea) {
		this.respuesConsultaLinea = respuesConsultaLinea;
	}

	public RespuestaDetalleAlerta getDetalleAlertaLinea() {
		return detalleAlertaLinea;
	}

	public void setDetalleAlertaLinea(RespuestaDetalleAlerta detalleAlertaLinea) {
		this.detalleAlertaLinea = detalleAlertaLinea;
	}

	public List<SapAmisBitacoraSiniestros> getListaBitacoraSiniestros() {
		return listaBitacoraSiniestros;
	}

	public void setListaBitacoraSiniestros(List<SapAmisBitacoraSiniestros> listaBitacoraSiniestros) {
		this.listaBitacoraSiniestros = listaBitacoraSiniestros;
	}

	public GenerarPlantillaReporte getGenerarPlantillaReporteService() {
		return generarPlantillaReporteService;
	}

	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporteService(GenerarPlantillaReporte generarPlantillaReporteService) {
		this.generarPlantillaReporteService = generarPlantillaReporteService;
	}

	public InputStream getReporteAgenteStream() {
		return reporteAgenteStream;
	}

	public void setReporteAgenteStream(InputStream reporteAgenteStream) {
		this.reporteAgenteStream = reporteAgenteStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(Integer tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	

	public SapAmisService getServicioSapAmis() {
		return servicioSapAmis;
	}
	@Autowired
	@Qualifier("sapAmisServiceEJB")
	public void setServicioSapAmis(SapAmisService servicioSapAmis) {
		this.servicioSapAmis = servicioSapAmis;
	}

	public String getFechaInicioEjecucionManual() {
		return fechaInicioEjecucionManual;
	}

	public void setFechaInicioEjecucionManual(String fechaInicioEjecucionManual) {
		this.fechaInicioEjecucionManual = fechaInicioEjecucionManual;
	}

	public String getFechaFinEjecucionManual() {
		return fechaFinEjecucionManual;
	}

	public void setFechaFinEjecucionManual(String fechaFinEjecucionManual) {
		this.fechaFinEjecucionManual = fechaFinEjecucionManual;
	}
//
//	public List<SapAmisEjecucionLog> getListSapAmisEjecucionLog() {
//		return listSapAmisEjecucionLog;
//	}
//
//	public void setListSapAmisEjecucionLog(List<SapAmisEjecucionLog> listSapAmisEjecucion) {
//		this.listSapAmisEjecucionLog = listSapAmisEjecucion;
//	}

	public int getModulo() {
		return modulo;
	}

	public void setModulo(int modulo) {
		this.modulo = modulo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Long getIdsession() {
		return idsession;
	}

	public void setIdsession(Long idsession) {
		this.idsession = idsession;
	}

	public List<SapAmisRespuestaAlertas> getSapAmisRespuestaAlertas() {
		return sapAmisRespuestaAlertas;
	}

	public void setSapAmisRespuestaAlertas(List<SapAmisRespuestaAlertas> sapAmisRespuestaAlertas) {
		this.sapAmisRespuestaAlertas = sapAmisRespuestaAlertas;
	}
	public List<SapAmisRespuestaAlertas> getSapAmisRespuestaAccionesAlertas() {
		return sapAmisRespuestaAlertas;
	}

	public String getIdAccionSapAmis() {
		return idAccionSapAmis;
	}

	public void setIdAccionSapAmis(String idAccionSapAmis) {
		this.idAccionSapAmis = idAccionSapAmis;
	}

	public String getDescripcionAccionSapAmis() {
		return descripcionAccionSapAmis;
	}

	public void setDescripcionAccionSapAmis(String descripcionAccionSapAmis) {
		this.descripcionAccionSapAmis = descripcionAccionSapAmis;
	}

	public String getAlertaAccionSapAmis() {
		return alertaAccionSapAmis;
	}

	public void setAlertaAccionSapAmis(String alertaAccionSapAmis) {
		this.alertaAccionSapAmis = alertaAccionSapAmis;
	}

	public String getSistemaAccionSapAmis() {
		return sistemaAccionSapAmis;
	}

	public void setSistemaAccionSapAmis(String sistemaAccionSapAmis) {
		this.sistemaAccionSapAmis = sistemaAccionSapAmis;
	}
	
	public String getStatusAccionSapAmis() {
		return statusAccionSapAmis;
	}

	public void setStatusAccionSapAmis(String statusAccionSapAmis) {
		this.statusAccionSapAmis = statusAccionSapAmis;
	}
	
	public String getExistenAcciones() {
		return existenAcciones;
	}

	public void setExistenAcciones(String existenAcciones) {
		this.existenAcciones = existenAcciones;
	}

	public String getIdCotizacion() {
		return idCotizacion;
	}

	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}

	public String getIdRelacionSapAmis() {
		return idRelacionSapAmis;
	}

	public void setIdRelacionSapAmis(String idRelacionSapAmis) {
		this.idRelacionSapAmis = idRelacionSapAmis;
	}

	public String getNuevoCotizador() {
		return nuevoCotizador;
	}

	public void setNuevoCotizador(String nuevoCotizador) {
		this.nuevoCotizador = nuevoCotizador;
	}

    @Autowired
    @Qualifier("sapAmisUtilsServiceEJB")
    public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
        this.sapAmisUtilsService = sapAmisUtilsService;
    }
}