package mx.com.afirme.midas2.action.impuestosestatales;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionCedularesView;
import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionImpuestos;
import mx.com.afirme.midas2.dto.impuestosestatales.RetencionImpuestosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.impuestosestatales.RetencionImpuestosService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/fuerzaventa/retencionImpuestos")
public class RetencionImpuestosAction extends CatalogoHistoricoAction implements Preparable {
	private static final long serialVersionUID = -5765400450389488968L;
	private static final Logger LOG = Logger.getLogger(RetencionImpuestos.class); 

	private RetencionImpuestosService retencionImpuestosService;
	private GenerarPlantillaReporte generarPlantillaReporteService;
	
	private final String JSP_FORM = "/jsp/impuestosEstatales/administracionRetencionImpuesto.jsp";
	private final String JSP_FORM_LIST = "/jsp/impuestosEstatales/retencionImpuestosGrid.jsp";
	private final String JSP_FORM_HIST = "/jsp/impuestosEstatales/retencionImpuestosHistorial.jsp";
	private final String JSP_FORM_HIST_LIST = "/jsp/impuestosEstatales/retencionImpuestosHistorialGrid.jsp";
	private final String JSP_FORM_LIST_MONITOR = "/jsp/impuestosEstatales/monitorRetencion.jsp";
	private final String JSP_FORM_LIST_MONITOR_GRID = "/jsp/impuestosEstatales/monitorRetencionGrid.jsp";
	private final String JSP_FORM_REPORTE_RETENCIONIMPUESTOS = "/jsp/impuestosEstatales/reporteRetencionImpuestos.jsp";

	private List<RetencionImpuestosDTO> listaConfiguracionesRetencionesHistory = new ArrayList<RetencionImpuestosDTO>();
	private List<RetencionImpuestosDTO> listaConfiguracionesRetenciones = new ArrayList<RetencionImpuestosDTO>();
	private List<RetencionCedularesView> listaConfiguracionesRetencionesMonitor = new ArrayList<RetencionCedularesView>();
	private List<EstadoDTO> listaEstados = new ArrayList<EstadoDTO>();
	private List<ValorCatalogoAgentes> listaTipoComisiones = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaTipoPersonalidadesJuridicas = new ArrayList<ValorCatalogoAgentes>();
	private Map<Integer, Integer> listadoAnios = new HashMap<Integer, Integer>();
	private Map<Integer, String> listadoMeses = new HashMap<Integer, String>(); 
	
	private RetencionImpuestosDTO filtroConfiguracion;
	private String load;

	@Autowired
	@Qualifier("listadoServiceEJB")
	private ListadoService listadoService;

	@Autowired
	@Qualifier("generarPlantillaReporteEJB")
	public void setGenerarPlantillaReporteService(GenerarPlantillaReporte generarPlantillaReporteService) {
		this.generarPlantillaReporteService = generarPlantillaReporteService;
	}

	@Autowired
	@Qualifier("retencionImpuestosEJB")
	public void setRetencionImpuestosService(RetencionImpuestosService retencionImpuestosService) {
		this.retencionImpuestosService = retencionImpuestosService;
	}

	@Override
	public void prepare() throws Exception {
		listaTipoComisiones = cargarCatalogoLightWeight("Tipo Comision");
		listaTipoPersonalidadesJuridicas = cargarCatalogoLightWeight("Personalidad Juridica");
		listaEstados = listadoService.listarEstadosMX();
	}
	
	public RetencionImpuestosAction() {
		super();
	}
	
	@Action(value = "mostrarRetencionImpuestos", results = {
			@Result(name = SUCCESS, location = JSP_FORM) })
	public String mostrarRetencionImpuestos() {
		return SUCCESS;
	}
	
	@Override
	@Action(value = "guardar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarRetencionImpuestos", "namespace", "/fuerzaventa/retencionImpuestos", "mensaje", "${mensaje}", "filtroConfiguracion.id", "${filtroConfiguracion.id}"}),
			@Result(name = INPUT, type = "redirectAction", params = {
					"actionName", "mostrarRetencionImpuestos", "namespace", "/fuerzaventa/retencionImpuestos", "mensaje", "${mensaje}"})})
	public String guardar() {
		try {
			retencionImpuestosService.guardarNuevaConfiguracion(filtroConfiguracion);
			setMensajeExito();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			onError(e);
		}
		return result;
	}

	@Action(value = "eliminar", results = {
			@Result(name = SUCCESS, type = "redirectAction", params = {
					"actionName", "mostrarRetencionImpuestos", "namespace", "/fuerzaventa/retencionImpuestos", "mensaje", "${mensaje}"}),
					@Result(name = INPUT, type = "redirectAction", params = {
							"actionName", "mostrarRetencionImpuestos", "namespace", "/fuerzaventa/retencionImpuestos", "mensaje", "${mensaje}"})})
	@Override
	public String eliminar() {
		try {
			retencionImpuestosService.desactivarConfiguracion(filtroConfiguracion);
			setMensajeExito();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			onError(e);
		}
		return result;
	}

	@Override
	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=JSP_FORM_LIST),
			@Result(name=INPUT,location=JSP_FORM_LIST)
	})
	public String listarFiltrado() {
		if(load.equals("true")) {
			listaConfiguracionesRetenciones = retencionImpuestosService.findByFiltersView(filtroConfiguracion);
		}
		return SUCCESS;
	}

	@Action(value="mostrarHistorico",results={
			@Result(name=SUCCESS,location=JSP_FORM_HIST),
			@Result(name=INPUT,location=JSP_FORM_HIST)
	})
	public String mostrarHistorico() {
		return SUCCESS;
	}
	
	@Action(value = "listarHistorico", results = {
			@Result(name = SUCCESS, location = JSP_FORM_HIST_LIST)})
	public String listarHistorico() {
		listaConfiguracionesRetencionesHistory = retencionImpuestosService.listarHistorico(filtroConfiguracion);
		return SUCCESS;
	}
	
	@Action(value = "monitorRetencionImpuestos", results = {
			@Result(name = SUCCESS, location = JSP_FORM_LIST_MONITOR) })
	public String monitorRetencionImpuestos() {
		return SUCCESS;
	}
	
	@Action(value = "listarMonitorRetencionImpuestos", results = {
			@Result(name = SUCCESS, location = JSP_FORM_LIST_MONITOR_GRID),
			@Result(name=INPUT,location=JSP_FORM_LIST)})
	public String listarMonitorRetencionImpuestos() {
		try {
			listaConfiguracionesRetencionesMonitor = retencionImpuestosService.listarMonitorRetencionImpuestos();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return SUCCESS;
	}
	
	@Action(value = "mostrarReporteRetencionImpuestos", results = {
			@Result(name = SUCCESS, location = JSP_FORM_REPORTE_RETENCIONIMPUESTOS) })
	public String mostrarReporteRetencionImpuestos() {
		listadoAnios = listadoService.getMapYears(10);
		listadoMeses = listadoService.getMapMonths();
		return SUCCESS;
	}
	
	@Override
	public String verDetalle() { return null; }

	@Override
	public String listar() { return null; }
	
	public List<EstadoDTO> getListaEstados() {
		return listaEstados;
	}
	public void setListaEstados(List<EstadoDTO> listaEstados) {
		this.listaEstados = listaEstados;
	}
	public List<ValorCatalogoAgentes> getListaTipoComisiones() {
		return listaTipoComisiones;
	}
	public void setListaTipoComisiones(List<ValorCatalogoAgentes> listaTipoComisiones) {
		this.listaTipoComisiones = listaTipoComisiones;
	}
	public List<RetencionImpuestosDTO> getListaConfiguracionesRetenciones() {
		return this.listaConfiguracionesRetenciones;
	}
	public void setListaConfiguracionesRetenciones(List<RetencionImpuestosDTO> listaConfiguracionesRetenciones) {
		this.listaConfiguracionesRetenciones = listaConfiguracionesRetenciones;
	}
	public List<RetencionCedularesView> getListaConfiguracionesRetencionesMonitor() {
		return listaConfiguracionesRetencionesMonitor;
	}
	public void setListaConfiguracionesRetencionesMonitor(
			List<RetencionCedularesView> listaConfiguracionesRetencionesMonitor) {
		this.listaConfiguracionesRetencionesMonitor = listaConfiguracionesRetencionesMonitor;
	}
	public List<ValorCatalogoAgentes> getListaTipoPersonalidadesJuridicas() {
		return listaTipoPersonalidadesJuridicas;
	}
	public void setListaTipoPersonalidadesJuridicas(List<ValorCatalogoAgentes> listaTipoPersonalidadesJuridicas) {
		this.listaTipoPersonalidadesJuridicas = listaTipoPersonalidadesJuridicas;
	}
	public GenerarPlantillaReporte getGenerarPlantillaReporteService() {
		return generarPlantillaReporteService;
	}
	public void setFiltroConfiguracion(RetencionImpuestosDTO filtroConfiguracion) {
		this.filtroConfiguracion = filtroConfiguracion;
	}
	public RetencionImpuestosDTO getFiltroConfiguracion() {
		return this.filtroConfiguracion;
	}
	public List<RetencionImpuestosDTO> getListaConfiguracionesRetencionesHistory() {
		return listaConfiguracionesRetencionesHistory;
	}
	public void setListaConfiguracionesRetencionesHistory(
			List<RetencionImpuestosDTO> listaConfiguracionesRetencionesHistory) {
		this.listaConfiguracionesRetencionesHistory = listaConfiguracionesRetencionesHistory;
	}
	public String getLoad() {
		return load;
	}
	public void setLoad(String load) {
		this.load = load;
	}
	public Map<Integer, Integer> getListadoAnios() {
		return listadoAnios;
	}
	public void setListadoAnios(Map<Integer, Integer> listadoAnios) {
		this.listadoAnios = listadoAnios;
	}
	public Map<Integer, String> getListadoMeses() {
		return listadoMeses;
	}
	public void setListadoMeses(Map<Integer, String> listadoMeses) {
		this.listadoMeses = listadoMeses;
	}

}
