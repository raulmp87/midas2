package mx.com.afirme.midas2.action.cargos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.cargos.ConfigCargosDao.estatusMoviemientoCargos;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.cargos.DetalleCargos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.dto.Cargos.DetalleCargosView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.cargos.ConfigCargosService;
import mx.com.afirme.midas2.service.cargos.DetalleCargosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.CarpetaAplicacionFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/cargos/cargosAgentes")
public class cargosAction extends CatalogoHistoricoAction{

/**
 * 
 */
	private static final long serialVersionUID = 1L;
	private final String CARGOSCATALOGO ="/jsp/cargos/cargosCatalogo.jsp";
	private final String CARGOSGRID ="/jsp/cargos/cargosGrid.jsp";
	private final String CARGOSDETALLE ="/jsp/cargos/cargosDetalle.jsp";
	private final String CATALOGOSGRID = "/jsp/cargos/catalogoCargosGrid.jsp";
	
	private ConfigCargos cargos = new ConfigCargos();
	private ConfigCargos filtrar = new ConfigCargos();
	private List<ValorCatalogoAgentes> listaTiposCargos = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaPlazo = new ArrayList<ValorCatalogoAgentes>();
	private DetalleCargosService detalleCargosService;
	private ConfigCargosService configCargosService;
	private EntidadService entidadService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private List<DetalleCargos> listaCargos = new ArrayList<DetalleCargos>();
	private List<ValorCatalogoAgentes> listaEstatusMovimiento= new ArrayList<ValorCatalogoAgentes>();
	private String detalleListaCargos;
	private FortimaxService fortimaxService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private CarpetaAplicacionFortimaxService carpetaAplicacionService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private String urlIfimax;
	private ImpresionesService impresionesService;
	private InputStream detalleCargoInputStream;
	private String contentType;
	private String fileName;
	private InputStream tablaAmortizacionInputStream;
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	
	private List<GerenciaView>  listGerencia = new ArrayList<GerenciaView>();
	private List<PromotoriaView>listPromotoria = new ArrayList<PromotoriaView>();
	private List<EjecutivoView>listEjecutivo = new ArrayList<EjecutivoView>();
	private List<CentroOperacionView> listCentroOperacion = new ArrayList<CentroOperacionView>();
	private GerenciaJPAService gerenciaJpaService;
	private EjecutivoService ejecutivoService;
	private CentroOperacionService centroOperacionService;
	private PromotoriaJPAService promotoriaService;
	private static List<ValorCatalogoAgentes> catalogoTipoAgente = new ArrayList<ValorCatalogoAgentes>();
	private AgenteMidasService agenteMidasService;
	private Agente agente = new Agente();
	private List<DetalleCargosView> listaFiltrado = new ArrayList<DetalleCargosView>();
	private List<EntregoDocumentosView> listaDocumentosFortimaxView = new ArrayList<EntregoDocumentosView>();
	private String mensajeError; 
	
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		listaEstatusMovimiento = valorCatalogoAgentesService.obtenerElementosPorCatalogo("Estatus Cargo de Agentes");
		listaPlazo = valorCatalogoAgentesService.obtenerElementosPorCatalogo("Periodos de Ajuste de Bonos");
		
//		listaTiposCargos = valorCatalogoAgentesService.obtenerElementosPorCatalogo("Tipos de Movimiento");
		listaTiposCargos = cargarCatalogo("Tipos de Cargos");
		listGerencia = gerenciaJpaService.getList(true);
		listEjecutivo = ejecutivoService.getList(true);
		listCentroOperacion = centroOperacionService.getList(true);
		listPromotoria = promotoriaService.getList(true);
		catalogoTipoAgente = valorCatalogoAgentesService.obtenerElementosPorCatalogo("Clasificacion de Agente");//Tipo de Agente
		if(getIdTipoOperacion()==null){
		 setIdTipoOperacion(130L);
		}
		prepareVerDetalle();
	}

	public void prepareMostrarContenedor(){
		listaPlazo = cargarCatalogo("Periodos de Ajuste de Bonos");
		listaTiposCargos = cargarCatalogo("Tipos de Cargos");
		listaEstatusMovimiento = cargarCatalogo("Estatus Cargo de Agentes");		
	}
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=CARGOSCATALOGO)
		})
		public String mostrarContenedor(){		
			return SUCCESS;
		}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle","namespace","/cargos/cargosAgentes","cargos.id","${cargos.id}","tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",	"tipoMensaje","${tipoMensaje}",
							"idRegistro","${cargos.id}", "idTipoOperacion","${idTipoOperacion}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/cargos/cargosAgentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${cargos.id}"})
	})
	@Override
	public String guardar() {
		try {
			TipoAccionHistorial tipoAccionHistorial=TipoAccionHistorial.ALTA;
			String keyMessage= "midas.cargosAgentes.historial.accion";
			
			if(cargos.getId()!=null){
				tipoAccionHistorial=TipoAccionHistorial.CAMBIO;
			}
			
			Agente agentes = entidadService.getReference(Agente.class, cargos.getAgente().getId());			
			cargos.setAgente(agentes);
			ValorCatalogoAgentes estatus = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus Cargo de Agentes","PENDIENTE");
			cargos.setEstatusCargo(estatus);
			cargos = configCargosService.save(cargos);
			
			detalleCargosService.llenarYGuardarListaDetalles(detalleListaCargos, cargos);
			guardarHistorico(TipoOperacionHistorial.CARGOS_AGENTES, cargos.getId(),keyMessage,tipoAccionHistorial);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			setIdTipoOperacion(130L);
			tipoAccion="4";
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=CATALOGOSGRID),
			@Result(name=INPUT,location=CATALOGOSGRID)
		})
	@Override
	public String listarFiltrado() {

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 
			listaFiltrado = configCargosService.findByFilters(filtrar);		
			
			for(DetalleCargosView obj:listaFiltrado){				 
				 obj.setFechaString(sdf.format(obj.getFechaAltaCargo()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location=CARGOSDETALLE),
			@Result(name=INPUT,location=CARGOSDETALLE)
	})
	@Override
	public String verDetalle() {
		listaPlazo = cargarCatalogo("Periodos de Ajuste de Bonos");
		listaTiposCargos = cargarCatalogo("Tipos de Cargos");
		listaEstatusMovimiento = cargarCatalogo("Estatus Cargo de Agentes");
					
		try {
			if(cargos != null && cargos.getId() != null){
				cargos = configCargosService.loadById(cargos);
				cargos.setAgente(concatDescripId(cargos.getAgente()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if("CANCELADO".equals("cargos.getEstatusCargo().getValor()")){
			setTipoAccion("2");
		}
		return SUCCESS;
	}
	
	@Action(value="cargarGridCargos",results={
			@Result(name=SUCCESS,location=CARGOSGRID),
			@Result(name=INPUT,location=CARGOSGRID)
	})
	public String cargarGridCargos(){
		try {
			if(cargos != null && cargos.getId() != null){
					listaCargos = detalleCargosService.loadByIdConfigCargos(cargos);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(value="aplicarMovimiento",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/cargos/cargosAgentes",
							"cargos.id","${cargos.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${cargos.id}", 
							"idTipoOperacion","${idTipoOperacion}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/cargos/cargosAgentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${cargos.id}", 
							"idTipoOperacion","${idTipoOperacion}"})
	})
	public String aplicarMovimiento(){
		try {
			List<DetalleCargos> listaDetalle = new ArrayList<DetalleCargos>();
			listaDetalle = detalleCargosService.loadByIdConfigCargos(cargos);
			cargos = configCargosService.updateEstatus(cargos, estatusMoviemientoCargos.APLICADO.getValue());
			detalleCargosService.updateEstatusDetalleCargos(listaDetalle.get(0), "ACTIVO");
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
			
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="cancelarMovimiento",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/cargos/cargosAgentes",
							"cargos.id","${cargos.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${cargos.id}",
							"idTipoOperacion","${idTipoOperacion}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/cargos/cargosAgentes",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${cargos.id}", 
							"idTipoOperacion","${idTipoOperacion}"})
	})
	public String cancelarMovimiento(){
		try {
			if(cargos.getId()!=null){
				detalleCargosService.cancelarMovimientoAplicado(cargos);
			}
			cargos = configCargosService.updateEstatus(cargos,estatusMoviemientoCargos.CANCELADO.getValue());
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
			
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="imprimirComprobanteCargo",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","detalleCargoInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})})	
					
	public String imprimirComprobanteCargo() {
		
		try {
			Locale locale= getLocale();
			cargos = configCargosService.loadById(cargos);
			TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirComprobanteCargo(cargos, locale);				
			detalleCargoInputStream=new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType = "application/pdf";
			fileName = "cargos.pdf";
				
			String []expediente=new String[1];
			expediente[0]=cargos.getId().toString();			
//			fortimaxService.uploadFile("CARGO_"+cargos.getAgente().getId(), transporteImpresionDTO, "AGENTES",expediente , "01 ALTA DE AGENTES");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return SUCCESS;	
	}
	
	public void prepareObtenerAgente(){
		agente = new Agente();
	}

	@Action(value="obtenerAgente",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.vencimientoCedulaString,^agente\\.numeroCedula,^agente\\.vencimientoFianzaString,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente,^agente\\.clasificacionAgentes\\.id,^agente\\.clasificacionAgentes\\.valor,mensajeError"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^agente\\.id,^agente\\.idAgente,agente\\.tipoAgente,^agente\\.idTipoAgente,^agente\\.numeroFianza,^agente\\.vencimientoCedulaString,^agente\\.numeroCedula,^agente\\.vencimientoFianzaString,^agente\\.persona\\.nombreCompleto,^agente\\.persona\\.rfc,^agente\\.tipoSituacion\\.valor,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.centroOperacion\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.gerencia\\.descripcion,^agente\\.promotoria\\.descripcion,^agente\\.promotoria\\.ejecutivo\\.personaResponsable\\.nombreCompleto,^agente\\.direccionAgente,^agente\\.clasificacionAgentes\\.id,^agente\\.clasificacionAgentes\\.valor,mensajeError"})
		})
		
		public String obtenerAgente(){
		try {
			if(agente.getId()!=null){
				agente = agenteMidasService.loadById(agente);
			}else if(agente.getIdAgente()!=null){
				agente = agenteMidasService.loadByClave(agente);
			}
			
			String ok = validarAgenteEstatus(agente);
			if (ok.equals("OK")){
				ValorCatalogoAgentes tipoAg = entidadService.findById(ValorCatalogoAgentes.class, agente.getIdTipoAgente());
				if(tipoAg!=null){
					agente.setTipoAgente(tipoAg.getValor());
				}
					agente = concatDescripId(agente);
			}else{
				mensajeError=ok;
				agente = null;
			}
			onSuccess();
		} catch (Exception e) {
			onError(e);
		}
		return SUCCESS;
	}
	
	public String validarAgenteEstatus(Agente obj){
		String validarAgente = "OK";
		try {
			ValorCatalogoAgentes motivoEstatus =  valorCatalogoAgentesService.obtenerElementoEspecifico("Motivo de Estatus del Agente","NUEVO");
			ValorCatalogoAgentes situacion = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus de Agente (Situacion)", "RECHAZADO");
			ValorCatalogoAgentes situacionPendAut = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus de Agente (Situacion)", "PENDIENTE POR AUTORIZAR");
			
			if(obj!=null && obj.getId()!=null){
				Long motEstatusAgente = obj.getIdMotivoEstatusAgente();
				Long motEstatus = motivoEstatus.getId();
				if(motEstatusAgente.equals(motEstatus) && obj.getTipoSituacion().getId() == situacion.getId()){
					validarAgente="La Situación del Agente es: "+situacion.getValor()+" ["+motivoEstatus.getValor()+"], no es posibe realizar el cargo";
				}
				if(obj.getTipoSituacion().getId()==situacionPendAut.getId()){
					validarAgente="La Situación del Agente es: "+situacionPendAut.getValor()+", no es posibe realizar el cargo";
				}
			}else{
				validarAgente = "El Agente  no existe";
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return validarAgente;
	}
	
	public Agente concatDescripId(Agente agente){
		try {
			/**Centro Operacion*/
			Long idCentroOperacion = agente.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().getIdCentroOperacion();
			String descripcionCentroOperacion=idCentroOperacion.toString()+" - "+ agente.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().getDescripcion();
			agente.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().setDescripcion(descripcionCentroOperacion);
			/**Gerencia*/
			Long id_Gerencia=agente.getPromotoria().getEjecutivo().getGerencia().getIdGerencia();
			String descripcionGerencia =id_Gerencia+" - "+agente.getPromotoria().getEjecutivo().getGerencia().getDescripcion();
			agente.getPromotoria().getEjecutivo().getGerencia().setDescripcion(descripcionGerencia);
			/**promotoria*/
			Long id_Promotoria=agente.getPromotoria().getIdPromotoria();
			String descripcionPromotoria =id_Promotoria+" - "+agente.getPromotoria().getDescripcion();
			agente.getPromotoria().setDescripcion(descripcionPromotoria);
			/**Ejecutivo*/
			Long id_Ejecutivo=agente.getPromotoria().getEjecutivo().getIdEjecutivo();
			String descripcionEjecutivo =id_Ejecutivo+" - "+agente.getPromotoria().getEjecutivo().getPersonaResponsable().getNombreCompleto();
			agente.getPromotoria().getEjecutivo().getPersonaResponsable().setNombreCompleto(descripcionEjecutivo);
			/**Tipo de agente*/
			ValorCatalogoAgentes tipoAg = entidadService.findById(ValorCatalogoAgentes.class, agente.getIdTipoAgente());
			if(tipoAg!=null){
				agente.setTipoAgente(tipoAg.getValor());
			}
			/**Domicilio*/
//			if(!agente.getPersona().getDomicilios().isEmpty() || agente.getPersona().getDomicilios().get(0)!=null){
//				Domicilio dom = agente.getPersona().getDomicilios().get(0);
//				String domic = dom.getCalleNumero()+" "+dom.getNombreColonia()+" "+" "+dom.getCodigoPostal()+" "+dom.getCiudad()+" "+dom.getEstado();
//				agente.setDireccionAgente(domic);
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agente;
	}

	
	/*************************************************************
	 *  metodo para generar los documentos que se deben digitalizar al guardar un cargo,
	 *  antes de ser autorizado los documentos deberan estar digitalizados
	 * *********************************************************************/
	@Action(value="generarDocCargosEnFortimax",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"})
	})
	public String generarDocCargosEnFortimax(){
		try {
			configCargosService.crearYGenerarDocumentosFortimax(cargos);			
			listaDocumentosFortimaxView = configCargosService.consultaEstatusDocumentos(cargos.getId(),cargos.getAgente().getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return SUCCESS;
	}
	
	/*************************************************************
	 *  metodo para consultar los documentos que estan guardados en la base de datos y armar html para
	 *  saber si ya estan digitalizados
	 * *********************************************************************/
	@Action(value="mostrarDocumentosADigitalizar",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
	})
	public String mostrarDocumentosADigitalizar(){
		try {
			listaDocumentosFortimaxView = configCargosService.consultaEstatusDocumentos(cargos.getId(),cargos.getAgente().getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return SUCCESS;
	}
	
	@Action(value="generarLigaIfimaxCargos",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp"),
			@Result(name="errorExpediente",location=ERROR)
		})
	public String generarLigaIfimaxCargos(){
		String []resp= new String[3];		
		
		try {			
	
			resp=fortimaxService.generateLinkToDocument(cargos.getAgente().getId(),"AGENTES", "");			
			urlIfimax=resp[0];	
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(resp[2].contains("No existe el Expediente")){
			setMensaje(resp[2]);
			return "errorExpediente";
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
	}	
	

	@Action(value="auditarDocumentosCargosDigitalizar",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"})
		})
		public String auditarDocumentosCargosDigitalizar() {
			try {				
				
				configCargosService.auditarDocumentosEntregadosCargos(cargos.getId(),cargos.getAgente().getId(),"AGENTES");

				listaDocumentosFortimaxView = configCargosService.consultaEstatusDocumentos(cargos.getId(),cargos.getAgente().getId());
				
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
/***************get y set************************/
	@Autowired
	@Qualifier("detalleCargosServiceEJB")
	public void setDetalleCargosService(DetalleCargosService detalleCargosService) {
		this.detalleCargosService = detalleCargosService;
	}

	@Autowired
	@Qualifier("configCargosServiceEJB")
	public void setConfigCargosService(ConfigCargosService configCargosService) {
		this.configCargosService = configCargosService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public Agente getAgente() {
		return agente;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}
	public List<ValorCatalogoAgentes> getListaTiposCargos() {
		return listaTiposCargos;
	}

	public void setListaTiposCargos(
			List<ValorCatalogoAgentes> listaTiposCargos) {
		this.listaTiposCargos = listaTiposCargos;
	}

	public List<ValorCatalogoAgentes> getListaPlazo() {
		return listaPlazo;
	}

	public void setListaPlazo(List<ValorCatalogoAgentes> listaPlazo) {
		this.listaPlazo = listaPlazo;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}

	public List<ValorCatalogoAgentes> getListaEstatusMovimiento() {
		return listaEstatusMovimiento;
	}

	public void setListaEstatusMovimiento(
			List<ValorCatalogoAgentes> listaEstatusMovimiento) {
		this.listaEstatusMovimiento = listaEstatusMovimiento;
	}
	public String getDetalleListaCargos() {
		return detalleListaCargos;
	}

	public void setDetalleListaCargos(String detalleListaCargos) {
		this.detalleListaCargos = detalleListaCargos;
	}

	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	@Autowired
	@Qualifier("carpetaAplicacionFortimaxServiceEJB")
	public void setCarpetaAplicacionService(
			CarpetaAplicacionFortimaxService carpetaAplicacionService) {
		this.carpetaAplicacionService = carpetaAplicacionService;
	}
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}

	public String getUrlIfimax() {
		return urlIfimax;
	}

	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}

	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}

	public InputStream getDetalleCargoInputStream() {
		return detalleCargoInputStream;
	}

	public void setDetalleCargoInputStream(InputStream detalleCargoInputStream) {
		this.detalleCargoInputStream = detalleCargoInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public InputStream getTablaAmortizacionInputStream() {
		return tablaAmortizacionInputStream;
	}

	public void setTablaAmortizacionInputStream(InputStream tablaAmortizacionInputStream) {
		this.tablaAmortizacionInputStream = tablaAmortizacionInputStream;
	}

	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public void setListaDocumentosFortimax(	List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}
	@Autowired
	@Qualifier("gerenciaJPAServiceEJB")
	public void setGerenciaJpaService(GerenciaJPAService gerenciaJpaService) {
		this.gerenciaJpaService = gerenciaJpaService;
	}
	@Autowired
	@Qualifier("ejecutivoServiceEJB")
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	
	@Autowired
	@Qualifier("centroOperacionServiceEJB")
	public void setCentroOperacionService(
			CentroOperacionService centroOperacionService) {
		this.centroOperacionService = centroOperacionService;
	}
	
	@Autowired
	@Qualifier("promotoriaJPAServiceEJB")
	public void setPromotoriaService(PromotoriaJPAService promotoriaService) {
		this.promotoriaService = promotoriaService;
	}

	public List<GerenciaView> getListGerencia() {
		return listGerencia;
	}

	public void setListGerencia(List<GerenciaView> listGerencia) {
		this.listGerencia = listGerencia;
	}

	public List<PromotoriaView> getListPromotoria() {
		return listPromotoria;
	}

	public void setListPromotoria(List<PromotoriaView> listPromotoria) {
		this.listPromotoria = listPromotoria;
	}

	public List<EjecutivoView> getListEjecutivo() {
		return listEjecutivo;
	}

	public void setListEjecutivo(List<EjecutivoView> listEjecutivo) {
		this.listEjecutivo = listEjecutivo;
	}

	public List<CentroOperacionView> getListCentroOperacion() {
		return listCentroOperacion;
	}

	public void setListCentroOperacion(List<CentroOperacionView> listCentroOperacion) {
		this.listCentroOperacion = listCentroOperacion;
	}

	public static List<ValorCatalogoAgentes> getCatalogoTipoAgente() {
		return catalogoTipoAgente;
	}

	public static void setCatalogoTipoAgente(
			List<ValorCatalogoAgentes> catalogoTipoAgente) {
		cargosAction.catalogoTipoAgente = catalogoTipoAgente;
	}

	public ConfigCargos getCargos() {
		return cargos;
	}

	public void setCargos(ConfigCargos cargos) {
		this.cargos = cargos;
	}

	public ConfigCargos getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(ConfigCargos filtrar) {
		this.filtrar = filtrar;
	}

	public List<DetalleCargos> getListaCargos() {
		return listaCargos;
	}

	public void setListaCargos(List<DetalleCargos> listaCargos) {
		this.listaCargos = listaCargos;
	}

	public List<DetalleCargosView> getListaFiltrado() {
		return listaFiltrado;
	}

	public void setListaFiltrado(List<DetalleCargosView> listaFiltrado) {
		this.listaFiltrado = listaFiltrado;
	}
	
	@Autowired
	@Qualifier("personaSeycosFacadeRemoteEJB")
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}

	public List<EntregoDocumentosView> getListaDocumentosFortimaxView() {
		return listaDocumentosFortimaxView;
	}

	public void setListaDocumentosFortimaxView(
			List<EntregoDocumentosView> listaDocumentosFortimaxView) {
		this.listaDocumentosFortimaxView = listaDocumentosFortimaxView;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
}