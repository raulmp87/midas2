package mx.com.afirme.midas2.action.prestamos;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas2.action.catalogos.fuerzaventa.CatalogoHistoricoAction;
import mx.com.afirme.midas2.dao.prestamos.ConfigPrestamoAnticipoDao.EstatusMovimientoPrestamoAnticipo;
import mx.com.afirme.midas2.dao.prestamos.PagarePrestamoAnticipoDao.EstatusPagarePrestamoAnticipo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.prestamos.ConfigPrestamoAnticipoView;
import mx.com.afirme.midas2.service.calculos.InterfazMizarService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.prestamos.ConfigPrestamoAnticipoService;
import mx.com.afirme.midas2.service.prestamos.PagarePrestamoAnticipoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Namespace("/prestamos/prestamosAnticipos")
public class prestamosAnticiposAction extends CatalogoHistoricoAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String PRESTAMOSCATALOGO ="/jsp/prestamos/prestamosCatalogo.jsp";
	private final String PRESTAMOSGRID ="/jsp/prestamos/prestamosGrid.jsp";
	private final String PRESTAMOSDETALLE ="/jsp/prestamos/prestamosDetalle.jsp";
	private final String PAGARESGRID = "/jsp/prestamos/pagaresGrid.jsp";
	private final String TABLA_AMORTIZACION = "/jsp/prestamos/tablaAmortizacion.jsp";
	
	private ConfigPrestamoAnticipo prestamoAnticipo = new ConfigPrestamoAnticipo();
	private ConfigPrestamoAnticipo filtrar = new ConfigPrestamoAnticipo();
	private List<ValorCatalogoAgentes> listaTipoMovimiento = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaPlazo = new ArrayList<ValorCatalogoAgentes>();
	private List<ValorCatalogoAgentes> listaPlazoInteres = new ArrayList<ValorCatalogoAgentes>();
	private PagarePrestamoAnticipo pagarePrestamoAnticipo;
	private PagarePrestamoAnticipoService pagarePrestamoAnticipoService;
	private ConfigPrestamoAnticipoService configPrestamoAnticipoService;
	private EntidadService entidadService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private List<PagarePrestamoAnticipo> listaPagares = new ArrayList<PagarePrestamoAnticipo>();
	private List<ConfigPrestamoAnticipoView> listaconfigPrestamoAnticipoView= new ArrayList<ConfigPrestamoAnticipoView>();
	private List<ValorCatalogoAgentes> listaEstatusMovimiento= new ArrayList<ValorCatalogoAgentes>();
	private String pagares;
	private List<PagarePrestamoAnticipo> listPagarePrestamoAnticipo = new ArrayList<PagarePrestamoAnticipo>();
	private FortimaxService fortimaxService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private String urlIfimax;
	private ImpresionesService impresionesService;
	private InputStream pagareInputStream;
	private String contentType;
	private String fileName;
	private InputStream tablaAmortizacionInputStream;
	private List<DocumentoEntidadFortimax> listaDocumentosFortimax = new ArrayList<DocumentoEntidadFortimax>();
	private List<EntregoDocumentosView> listaDocumentosFortimaxView = new ArrayList<EntregoDocumentosView>();
	private InterfazMizarService interfazMizarService;
	private AgenteMidasService agenteMidasService;
	private Long numPagares;
	
	//*******************************fortimax**************************
	
	private List<DocumentosAgrupados>listDocAgrupado = new ArrayList<DocumentosAgrupados>();
	private CatalogoDocumentoFortimax documentoFortimax;
	private String nombreCarpeta;
	private String nombreDocumento;
	private String nombreGaveta;
	private String documentosFaltantes;
	private String idDocumentosString;
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
//		listaEstatusMovimiento = valorCatalogoAgentesService.obtenerElementosPorCatalogo("Estatus del Movimiento de Prestamo / Anticipo");
//		listaTipoMovimiento = cargarCatalogo("Concepto Movimiento Agente","PRESTAMOS","ANTICIPO DE BONOS","ANTICIPO DE COMISIONES");
//		listaPlazo = valorCatalogoAgentesService.obtenerElementosPorCatalogo("Periodos de Ajuste de Bonos");
	}

	public void prepareMostrarContenedor(){
		listaPlazo = cargarCatalogo("Periodos de Ajuste de Bonos");
		listaPlazoInteres=cargarCatalogo("Periodos de Ajuste de Bonos");
		listaTipoMovimiento = cargarCatalogo("Concepto Movimiento Agente","PRESTAMOS","ANTICIPO DE BONOS","ANTICIPO DE COMISIONES");
		listaEstatusMovimiento = cargarCatalogo("Estatus del Movimiento de Prestamo / Anticipo");		
	}
	
	@Action(value="mostrarContenedor",results={
			@Result(name=SUCCESS,location=PRESTAMOSCATALOGO)
		})
		public String mostrarContenedor(){		
			return SUCCESS;
		}
	
	@Action(value="guardar",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"prestamoAnticipo.id","${prestamoAnticipo.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"})
	})
	@Override
	public String guardar() {
		try {
			Agente agente = entidadService.findById(Agente.class, prestamoAnticipo.getAgente().getId());			
			//Agente agente = entidadService.getReference(Agente.class, prestamoAnticipo.getAgente().getId());
			prestamoAnticipo.setAgente(agente);
			Boolean registroNuevo = (prestamoAnticipo.getId()==null)?true:false;
			prestamoAnticipo = configPrestamoAnticipoService.save(prestamoAnticipo);
			pagarePrestamoAnticipoService.llenarYGuardarListaPagares(pagares, prestamoAnticipo);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
			if(registroNuevo){
				Map<String, Map<String, List<String>>> mapCorreos = configPrestamoAnticipoService
						.obtenerCorreos(
								agente.getId(),
								GenericMailService.P_PRESTAMOS_ANTICIPOS,
								GenericMailService.M_PRESTAMOS_ANTICIPOS_ALTA_REGISTO);
				configPrestamoAnticipoService.enviarCorreo(prestamoAnticipo.getId(), mapCorreos, agente
						.getPersona().getNombreCompleto(), "ALTAREGISTRO",
						GenericMailService.T_GENERAL);
			}
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Override
	public String eliminar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Action(value="listarFiltrado",results={
			@Result(name=SUCCESS,location=PRESTAMOSGRID),
			@Result(name=INPUT,location=PRESTAMOSGRID)
		})
	@Override
	public String listarFiltrado() {

		try {
			listaconfigPrestamoAnticipoView = configPrestamoAnticipoService.findByFilters(filtrar);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}

	@Action(value="verDetalle",results={
			@Result(name=SUCCESS,location=PRESTAMOSDETALLE),
			@Result(name=INPUT,location=PRESTAMOSDETALLE)
	})
	@Override
	public String verDetalle() {
		
		listaPlazo = cargarCatalogo("Periodos de Ajuste de Bonos");
		listaPlazoInteres=cargarCatalogo("Periodos de Ajuste de Bonos");
		listaTipoMovimiento = cargarCatalogo("Concepto Movimiento Agente","PRESTAMOS","ANTICIPO DE BONOS","ANTICIPO DE COMISIONES");
		listaEstatusMovimiento = cargarCatalogo("Estatus del Movimiento de Prestamo / Anticipo");
					
		try {
			if(prestamoAnticipo != null && prestamoAnticipo.getId() != null){
				prestamoAnticipo = configPrestamoAnticipoService.loadById(prestamoAnticipo);			
			
//			String []respExp=agenteMidasService.generateExpedientAgent(prestamoAnticipo.getAgente().getId());			
//			if(!documentoEntidadService.existeEstructuraPorEntidadAplicacionYCarpeta(prestamoAnticipo.getAgente().getId(), "AGENTES","PRESTAMOS")){
//				List<CatalogoDocumentoFortimax>listCatalogoDocs = documentoCarpetaService.obtenerDocumentosPorCarpeta("AGENTES","PRESTAMOS");
//					for(CatalogoDocumentoFortimax doc:listCatalogoDocs){
//						if(doc!=null){
//							String []respDoc=fortimaxService.generateDocument(prestamoAnticipo.getAgente().getId(), "AGENTES", doc.getNombreDocumentoFortimax()+"_"+prestamoAnticipo.getAgente().getId(), doc.getCarpeta().getNombreCarpetaFortimax());
//							if(respDoc[0].toString()!=null&&!respDoc[0].toString().equals("")){
//								DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
//								docFortimax.setCatalogoDocumentoFortimax(doc);
//								docFortimax.setExisteDocumento(0);
//								docFortimax.setIdRegistro(prestamoAnticipo.getAgente().getId());
//								documentoEntidadService.save(docFortimax);
//							}
//						}
//					}	
//				}	
			}
//			else{
//				documentoEntidadService.sincronizarDocumentos(prestamoAnticipo.getAgente().getId(), "AGENTES", "PRESTAMOS");
//			}
//			listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(prestamoAnticipo.getAgente().getId(), "AGENTES", "PRESTAMOS");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return SUCCESS;
	}
	
	@Action(value="cargaPagaresGrid",results={
			@Result(name=SUCCESS,location=PAGARESGRID),
			@Result(name=INPUT,location=PAGARESGRID)
	})
	public String cargaPagaresGrid(){
		try {
			if(prestamoAnticipo != null && prestamoAnticipo.getId() != null){
					listaPagares = pagarePrestamoAnticipoService.loadByIdConfigPrestamo(prestamoAnticipo);
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="solicitatChequeDeMovimiento",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"prestamoAnticipo.id","${prestamoAnticipo.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"})
	})
	public String solicitatChequeDeMovimiento(){
		try {
			configPrestamoAnticipoService.solicitarChequeDelMovimiento(prestamoAnticipo);	
//			pagarePrestamoAnticipoService.aplicaPagare(pagares,prestamoAnticipo, pagarePrestamoAnticipo);
			interfazMizarService.importarSolicitudesChequesAMizar(prestamoAnticipo.getId());
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
			
		} catch (Exception e) {			
			setMensaje(e.getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(value="autorizarMovimiento",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"prestamoAnticipo.id","${prestamoAnticipo.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"})
	})
	public String autorizarMovimiento(){
		try {
			tipoAccion="4";
			configPrestamoAnticipoService.autorizarMovimiento(prestamoAnticipo);	
			pagarePrestamoAnticipoService.aplicaPagare(pagares,prestamoAnticipo, pagarePrestamoAnticipo);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			
		} catch (Exception e) {			
			setMensaje(e.getCause().getMessage());
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(value="cancelarMovimiento",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"prestamoAnticipo.id","${prestamoAnticipo.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"})
	})
	public String cancelarMovimiento(){
		try {
			configPrestamoAnticipoService.updateEstatus(prestamoAnticipo,EstatusMovimientoPrestamoAnticipo.CANCELADO.getValue());			
			List<PagarePrestamoAnticipo> listaPagares = pagarePrestamoAnticipoService.loadByIdConfigPrestamo(prestamoAnticipo);
			pagarePrestamoAnticipoService.updateEstatusPagare(listaPagares, EstatusPagarePrestamoAnticipo.CANCELADO.getValue());
			
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
			
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(value="tablaAmortizacion", results ={
			@Result(name = SUCCESS, location = TABLA_AMORTIZACION),
			@Result(name = SUCCESS, location = TABLA_AMORTIZACION)
	})
	public String tablaAmortizacion(){
		
		return SUCCESS;
	}
	
	@Action(value="imprimirTablaAmortizacion",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","tablaAmortizacionInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})})	
					
	public String imprimirTablaAmortizacion() {
		Locale locale= getLocale();
		
		TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirTablaAmortizacion(prestamoAnticipo, locale);				
		tablaAmortizacionInputStream=new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
		contentType = "application/pdf";
		fileName = "Tabla Amortizacion.pdf";		
		return SUCCESS;		
	}
	
	@Action(value="activarPagare",results={
			@Result(name=SUCCESS,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"prestamoAnticipo.id","${prestamoAnticipo.id}",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"}),
			@Result(name=INPUT,type="redirectAction",
					params={"actionName","verDetalle",
							"namespace","/prestamos/prestamosAnticipos",
							"tipoAccion","${tipoAccion}",
							"mensaje","${mensaje}",
							"tipoMensaje","${tipoMensaje}",
							"idRegistro","${prestamoAnticipo.id}"})
	})
	public String activarPagare(){
		try {
			pagarePrestamoAnticipoService.aplicaPagare(pagares,prestamoAnticipo, pagarePrestamoAnticipo);
			setMensaje(MENSAJE_EXITO);
			setTipoMensaje(TIPO_MENSAJE_EXITO);
			tipoAccion="4";
		} catch (Exception e) {
			setMensaje(MENSAJE_ERROR_GENERAL);
			setTipoMensaje(TIPO_MENSAJE_ERROR);			
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	

	@Action(value="generarLigaIfimax",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp")
		})
	public String generarLigaIfimax(){
		String []resp= new String[3];
		try {			
//			List<CatalogoDocumentoFortimax> docsCarpeta=documentoCarpetaService.obtenerDocumentosPorCarpeta("AGENTES", "PRESTAMOS");			
//			resp=fortimaxService.generateLinkToDocument(prestamoAnticipo.getAgente().getId(),"AGENTES", docsCarpeta.get(0).getNombreDocumentoFortimax()+"_"+prestamoAnticipo.getAgente().getId());
			resp=fortimaxService.generateLinkToDocument(prestamoAnticipo.getAgente().getId(),"AGENTES", "");
			urlIfimax=resp[0];		
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
	}	
	
	@Action(value="imprimirPagare",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","pagareInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})})	
					
	public String imprimirPagare() {
		
		try {
			Locale locale= getLocale();
			PagarePrestamoAnticipo pagare = pagarePrestamoAnticipoService.loadById(pagarePrestamoAnticipo);
			TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirPagarePrestamoAnticipo(pagare, locale);				
			pagareInputStream=new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType = "application/pdf";
			fileName = "pagare.pdf";		
				
//			String []expediente=new String[1];
//			expediente[0]=pagarePrestamoAnticipo.getId().toString();			
//			fortimaxService.uploadFile("PAGARE_"+pagarePrestamoAnticipo.getAgente().getId(), transporteImpresionDTO, "AGENTES",expediente , "01 ALTA DE AGENTES");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return SUCCESS;	
	}
	
	@Action(value="imprimirPagarePrincipal",results={
			@Result(name=SUCCESS,type="stream",
					params={"contentType","${contentType}","inputName","pagareInputStream",
					"contentDisposition","attachment;filename=\"${fileName}\""})
	})
	public String imprimirPagarePrincipal(){
		try {
			Locale locale= getLocale();
			ConfigPrestamoAnticipo pagarePrincipal = configPrestamoAnticipoService.loadById(prestamoAnticipo);
			TransporteImpresionDTO transporteImpresionDTO = impresionesService.imprimirPagarePrincipalPrestamoAnticipo(pagarePrincipal, locale);				
			pagareInputStream=new ByteArrayInputStream(transporteImpresionDTO.getByteArray());
			contentType = "application/pdf";
			fileName = "pagarePrincipal.pdf";	
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return SUCCESS;
	}
	
	@Action(value="matchDocumentos",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String matchDocumentos() {
			try {				
				documentoEntidadService.sincronizarDocumentos(prestamoAnticipo.getAgente().getId(), "AGENTES", "PRESTAMOS");
				listaDocumentosFortimax=documentoEntidadService.getListaDocumentosGuardadosPorEntidadYCarpeta(prestamoAnticipo.getAgente().getId(), "AGENTES", "PRESTAMOS");
				setMensaje(MENSAJE_EXITO);
				setTipoMensaje(TIPO_MENSAJE_EXITO);
			} catch (Exception e) {
				e.printStackTrace();
				setMensaje(MENSAJE_ERROR_GENERAL);
				setTipoMensaje(TIPO_MENSAJE_ERROR);
				return INPUT;
			}
			return SUCCESS;
		}
	
	//*************FORTIMAX****************
	public void prepareMostrarNDocumentosMismo(){						
//		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
//		parametros.put("nombreDocumento", getNombreDocumento());
//		parametros.put("carpeta.nombreCarpeta", getNombreCarpeta());	
//		List<CatalogoDocumentoFortimax>document=new ArrayList<CatalogoDocumentoFortimax>();
//		document=entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
//		String idString=" ";
//		for(CatalogoDocumentoFortimax fortimax: document){
//			if(!idString.equals("")){
//				idString = idString+", "+fortimax.getId().toString();
//			}else{
//				idString = fortimax.getId().toString();
//			}
//		}
//		setDocumentoFortimax(document.get(0));
	}
	
	@Action(value="mostrarNDocumentosMismo",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
		})
		public String mostrarNDocumentosMismo(){
		
		try {
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
//			parametros.put("nombreDocumento", getNombreDocumento());
			parametros.put("carpeta.nombreCarpeta", getNombreCarpeta());	
			List<CatalogoDocumentoFortimax>document=new ArrayList<CatalogoDocumentoFortimax>();
			document=entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
			//************************************************************
			for(CatalogoDocumentoFortimax fortimax: document){
				if(fortimax.getNombreDocumento()!="PAGARES"){
					DocumentosAgrupados docs = new DocumentosAgrupados();
					HashMap<String,Object> properties = new HashMap<String,Object>();
					properties.put("idEntidad",prestamoAnticipo.getAgente().getId());
					properties.put("idDocumento",fortimax.getId());
					List<DocumentosAgrupados>documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
					if(documentoCrear.isEmpty()){
						docs.setIdAgrupador(1L);
						docs.setIdEntidad(prestamoAnticipo.getAgente().getId());
						documentoEntidadService.saveDocumentosAgrupados(docs, 1, getNombreDocumento(), getNombreGaveta(), getNombreCarpeta());
					}else{
						docs.setIdAgrupador(documentoCrear.get(0).getIdAgrupador());
						docs.setIdEntidad(prestamoAnticipo.getAgente().getId());
					}					
					documentosFaltantes=documentoEntidadService.sincronizarGrupoDocumentos(getNombreDocumento(), getNombreGaveta(), getNombreCarpeta(), docs);
					if(documentosFaltantes==null||documentosFaltantes.equals("")){
						docs.setIdAgrupador(documentoCrear.get(0).getIdAgrupador()+1);
						docs.setIdEntidad(prestamoAnticipo.getAgente().getId());
						documentoEntidadService.saveDocumentosAgrupados(docs, 1, getNombreDocumento(), getNombreGaveta(), getNombreCarpeta());
					}
					documentoCrear=entidadService.findByPropertiesWithOrder(DocumentosAgrupados.class, properties, "id DESC");
					for(DocumentosAgrupados doc:documentoCrear){
						DocumentoEntidadFortimax docFortimax= new DocumentoEntidadFortimax();
						CatalogoDocumentoFortimax catDocFortimax = new CatalogoDocumentoFortimax();
						catDocFortimax.setNombreDocumento(doc.getNombre());
						catDocFortimax.setNombreDocumentoFortimax(doc.getNombre());
						docFortimax.setCatalogoDocumentoFortimax(catDocFortimax);
						docFortimax.setExisteDocumento(doc.getExisteDocumento());
						listaDocumentosFortimax.add(docFortimax);
					}
				}
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	/*************************************************************
	 *  metodo para generar los documentos que se deben digitalizar al guardar un prestamo o anticipo,
	 *  antes de ser autorizado los documentos deberan estar digitalizados
	 * *********************************************************************/
	@Action(value="generarDocPrestamosEnFortimax",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"})
	})
	public String generarDocPrestamosEnFortimax(){
		try {
			configPrestamoAnticipoService.crearYGenerarDocumentosFortimax(prestamoAnticipo, numPagares);
			listaDocumentosFortimaxView = configPrestamoAnticipoService.consultaEstatusDocumentos(prestamoAnticipo.getId(),prestamoAnticipo.getAgente().getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return SUCCESS;
	}
	
	/*************************************************************
	 *  metodo para consultar los documentos que estan guardados en la base de datos y armar html para
	 *  saber si ya estan digitalizados
	 * *********************************************************************/
	@Action(value="mostrarDocumentosADigitalizar",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimax.*"})
	})
	public String mostrarDocumentosADigitalizar(){
		try {
			listaDocumentosFortimaxView = configPrestamoAnticipoService.consultaEstatusDocumentos(prestamoAnticipo.getId(),prestamoAnticipo.getAgente().getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return SUCCESS;
	}
	
	@Action(value="generarLigaIfimaxPrestamos",results={
			@Result(name=SUCCESS,location="/jsp/catalogos/fuerzaventa/agente/documentos/ligaIfimax.jsp"),
			@Result(name=INPUT,location="/jsp/catalogos/fuerzaventa/agente/documentos/sitioMantenimiento.jsp"),
			@Result(name="errorExpediente",location=ERROR)
		})
	public String generarLigaIfimaxPrestamos(){
		String []resp= new String[3];		
		
		try {			
	
			resp=fortimaxService.generateLinkToDocument(prestamoAnticipo.getAgente().getId(),"AGENTES", "");			
			urlIfimax=resp[0];	
		} catch (Exception e) {			
			e.printStackTrace();			
			return INPUT;
		}
		if(resp[2].contains("No existe el Expediente")){
			setMensaje(resp[2]);
			return "errorExpediente";
		}
		if(urlIfimax==null||urlIfimax.equals("")){			
			return INPUT;
		}			
		return SUCCESS;
		
	}	
	

	@Action(value="auditarDocumentosPrestamosDigitalizar",results={
			@Result(name=SUCCESS,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"}),
			@Result(name=INPUT,type="json",params={"noCache","true","ignoreHierarchy","false","includeProperties","^listaDocumentosFortimaxView.*"})
		})
		public String auditarDocumentosPrestamosDigitalizar() {
			try {				
				
				configPrestamoAnticipoService.auditarDocumentosEntregadosPrestamos(prestamoAnticipo.getId(),prestamoAnticipo.getAgente().getId(),"AGENTES");

				listaDocumentosFortimaxView = configPrestamoAnticipoService.consultaEstatusDocumentos(prestamoAnticipo.getId(),prestamoAnticipo.getAgente().getId());
				
			} catch (Exception e) {
				e.printStackTrace();
				return INPUT;
			}
			return SUCCESS;
		}
/***************get y set************************/
	public ConfigPrestamoAnticipo getPrestamoAnticipo() {
		return prestamoAnticipo;
	}

	public void setPrestamoAnticipo(ConfigPrestamoAnticipo prestamoAnticipo) {
		this.prestamoAnticipo = prestamoAnticipo;
	}

	public List<ValorCatalogoAgentes> getListaTipoMovimiento() {
		return listaTipoMovimiento;
	}

	public void setListaTipoMovimiento(
			List<ValorCatalogoAgentes> listaTipoMovimiento) {
		this.listaTipoMovimiento = listaTipoMovimiento;
	}

	public List<ValorCatalogoAgentes> getListaPlazo() {
		return listaPlazo;
	}

	public void setListaPlazo(List<ValorCatalogoAgentes> listaPlazo) {
		this.listaPlazo = listaPlazo;
	}

	public List<ValorCatalogoAgentes> getListaPlazoInteres() {
		return listaPlazoInteres;
	}

	public void setListaPlazoInteres(List<ValorCatalogoAgentes> listaPlazoInteres) {
		this.listaPlazoInteres = listaPlazoInteres;
	}

	public PagarePrestamoAnticipo getPagarePrestamoAnticipo() {
		return pagarePrestamoAnticipo;
	}

	public void setPagarePrestamoAnticipo(
			PagarePrestamoAnticipo pagarePrestamoAnticipo) {
		this.pagarePrestamoAnticipo = pagarePrestamoAnticipo;
	}	
	
	public List<PagarePrestamoAnticipo> getListaPagares() {
		return listaPagares;
	}

	public void setListaPagares(List<PagarePrestamoAnticipo> listaPagares) {
		this.listaPagares = listaPagares;
	}

	@Autowired
	@Qualifier("pagarePrestamoAnticipoServiceEJB")
	public void setPagarePrestamoAnticipoService(
			PagarePrestamoAnticipoService pagarePrestamoAnticipoService) {
		this.pagarePrestamoAnticipoService = pagarePrestamoAnticipoService;
	}

	@Autowired
	@Qualifier("configPrestamoAnticipoServiceEJB")
	public void setConfigPrestamoAnticipoService(
			ConfigPrestamoAnticipoService configPrestamoAnticipoService) {
		this.configPrestamoAnticipoService = configPrestamoAnticipoService;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("valorCatalogoAgentesServiceEJB")
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}
	public ConfigPrestamoAnticipo getFiltrar() {
		return filtrar;
	}

	public void setFiltrar(ConfigPrestamoAnticipo filtrar) {
		this.filtrar = filtrar;
	}
	public List<ConfigPrestamoAnticipoView> getListaconfigPrestamoAnticipoView() {
		return listaconfigPrestamoAnticipoView;
	}

	public void setListaconfigPrestamoAnticipoView(
			List<ConfigPrestamoAnticipoView> listaconfigPrestamoAnticipoView) {
		this.listaconfigPrestamoAnticipoView = listaconfigPrestamoAnticipoView;
	}

	public List<ValorCatalogoAgentes> getListaEstatusMovimiento() {
		return listaEstatusMovimiento;
	}

	public void setListaEstatusMovimiento(
			List<ValorCatalogoAgentes> listaEstatusMovimiento) {
		this.listaEstatusMovimiento = listaEstatusMovimiento;
	}
	public List<PagarePrestamoAnticipo> getListPagarePrestamoAnticipo() {
		return listPagarePrestamoAnticipo;
	}

	public void setListPagarePrestamoAnticipo(
			List<PagarePrestamoAnticipo> listPagarePrestamoAnticipo) {
		this.listPagarePrestamoAnticipo = listPagarePrestamoAnticipo;
	}
	public String getPagares() {
		return pagares;
	}

	public void setPagares(String pagares) {
		this.pagares = pagares;
	}

	@Autowired
	@Qualifier("fortimaxEJB")
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	@Autowired
	@Qualifier("documentoCarpetaFortimaxServiceEJB")
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	@Autowired
	@Qualifier("documentoEntidadFortimaxServiceEJB")
	public void setDocumentoEntidadService(
			DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}

	public String getUrlIfimax() {
		return urlIfimax;
	}

	public void setUrlIfimax(String urlIfimax) {
		this.urlIfimax = urlIfimax;
	}

	@Autowired
	@Qualifier("impresionesServiceEJB")
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}

	public InputStream getPagareInputStream() {
		return pagareInputStream;
	}

	public void setPagareInputStream(InputStream pagareInputStream) {
		this.pagareInputStream = pagareInputStream;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public InputStream getTablaAmortizacionInputStream() {
		return tablaAmortizacionInputStream;
	}

	public void setTablaAmortizacionInputStream(InputStream tablaAmortizacionInputStream) {
		this.tablaAmortizacionInputStream = tablaAmortizacionInputStream;
	}

	public List<DocumentoEntidadFortimax> getListaDocumentosFortimax() {
		return listaDocumentosFortimax;
	}

	public void setListaDocumentosFortimax(	List<DocumentoEntidadFortimax> listaDocumentosFortimax) {
		this.listaDocumentosFortimax = listaDocumentosFortimax;
	}
	@Autowired
	@Qualifier("interfazMizarServiceEJB")
	public void setInterfazMizarService(InterfazMizarService interfazMizarService) {
		this.interfazMizarService = interfazMizarService;
	}
	
	@Autowired
	@Qualifier("agenteMidasEJB")
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public List<DocumentosAgrupados> getListDocAgrupado() {
		return listDocAgrupado;
	}

	public void setListDocAgrupado(List<DocumentosAgrupados> listDocAgrupado) {
		this.listDocAgrupado = listDocAgrupado;
	}

	public String getNombreCarpeta() {
		return nombreCarpeta;
	}

	public void setNombreCarpeta(String nombreCarpeta) {
		this.nombreCarpeta = nombreCarpeta;
	}

	public String getNombreDocumento() {
		return nombreDocumento;
	}

	public void setNombreDocumento(String nombreDocumento) {
		this.nombreDocumento = nombreDocumento;
	}

	public String getNombreGaveta() {
		return nombreGaveta;
	}

	public void setNombreGaveta(String nombreGaveta) {
		this.nombreGaveta = nombreGaveta;
	}

	public String getIdDocumentosString() {
		return idDocumentosString;
	}

	public void setIdDocumentosString(String idDocumentosString) {
		this.idDocumentosString = idDocumentosString;
	}

	public CatalogoDocumentoFortimax getDocumentoFortimax() {
		return documentoFortimax;
	}

	public void setDocumentoFortimax(CatalogoDocumentoFortimax documentoFortimax) {
		this.documentoFortimax = documentoFortimax;
	}

	public Long getNumPagares() {
		return numPagares;
	}

	public void setNumPagares(Long numPagares) {
		this.numPagares = numPagares;
	}

	public List<EntregoDocumentosView> getListaDocumentosFortimaxView() {
		return listaDocumentosFortimaxView;
	}

	public void setListaDocumentosFortimaxView(
			List<EntregoDocumentosView> listaDocumentosFortimaxView) {
		this.listaDocumentosFortimaxView = listaDocumentosFortimaxView;
	}
	
	
}
