package mx.com.afirme.midas2.action.busquedaPaginada;
/**
 * Describe el comportamiento de la 
 * busqueda paginada.
 * @author Mart�n
 * 14/05/2012
 */
public interface BusquedaPaginada {
	/**
	 * Obtiene los incisos de una cotizacion aplicando filtros
	 * 
	 * @return Action result
	 */
	public String busquedaPaginada();

	/**
	 * Muestra el grid con los resultados paginados
	 * filtrados
	 * @return Action result
	 */
	public String busquedaRapida();

	/**
	 * Obtiene todos los incisos de una cot sin aplicar filtros
	 * 
	 * @return Action Result
	 */
	public String busquedaRapidaPaginada();
}
