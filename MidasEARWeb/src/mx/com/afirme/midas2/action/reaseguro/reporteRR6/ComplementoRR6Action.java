package mx.com.afirme.midas2.action.reaseguro.reporteRR6;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.logging.Level;

import mx.com.afirme.midas.sistema.LogDeMidasWeb;
import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.reaseguro.reporteRR6.ComplementoRR6Service;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/reaseguro/reporteRR6/carga")
public class ComplementoRR6Action extends BaseAction implements Preparable {

	private static final long serialVersionUID = 6132998255096489739L;
	public static final String exitoso = "Carga Finalizada.";
	public static final String bloqueado = "Carga Bloqueada.";
	public static final String existente = "Carga Existente.";
	public static final String error = "Error al guardar el archivo. Favor revisarlo!";
	private static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
	public static final String rtrc = "IDCONTRATO,NEGOCIO,ESTATUS";
	public static final String rtrf = "IDCONTRATO,CIUDAD,SECTOR";
	public static final String NOMBREARCHIVO = "Plantilla.xls";
	public static final String USUARIO_ACCESO_MIDAS = "usuarioAccesoMIDAS";
	public ByteArrayOutputStream outputStream;
	
	
	private BigDecimal idToControlArchivo;
	
	private String resultado;
	
	private String fechaCorte;
	
	private String tipoArchivo;
	
	private String mensaje;
	
	private String tipoContrato;
	
	private String accion;
	
	private ControlDescripcionArchivoDTO archivo;
	
	@SuppressWarnings("unused")
	private EntidadService entidadService;
	
	private ComplementoRR6Service complementoRR6Service;


	@Override
	public void prepare() {
		
	}
	
	@Action
	(value = "mostrarRR6", results = { 
			@Result(name = SUCCESS, location = "/jsp/reaseguro/reporteRR6/cargaRR6.jsp") })
	public String mostrarGeneracion() {	
		
		
		return SUCCESS;
	}
	
	@Action
	(value = "procesarinfo", results = { 
			@Result(name = SUCCESS, type = "json", params = { "noCache",
					"true", "ignoreHierarchy", "false", "includeProperties", "mensaje"})})
	public String cargarInfo() {
		
		String usuario = this.getUsuarioActual().getNombreUsuario();
		
		resultado = complementoRR6Service.procesarInfo(idToControlArchivo, tipoArchivo, tipoContrato, fechaCorte, usuario, accion);
		
		if(resultado.equalsIgnoreCase("EXITOSO")){
			setMensaje(exitoso);
		}else if(resultado.equalsIgnoreCase("BLOQUEADO")){
			setMensaje(bloqueado);
		}else if(resultado.equalsIgnoreCase("EXISTENTE")){
			setMensaje(existente);
		}else if(!resultado.equalsIgnoreCase("EXITOSO")){
			
			setMensaje(error+ "<br>"+resultado);
		}
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerPlantilla", results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", CONTENT_TYPE_EXCEL,
					"inputName", "archivo.inputStream",
					"contentDisposition", "attachment;filename="+NOMBREARCHIVO})}) 
	public String obtenerPlantilla() {
		
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
	        HSSFSheet sheet = workbook.createSheet("REPORTERR6");  
	
	        HSSFRow rowhead = sheet.createRow((short)0);
	        
	        HSSFFont font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            cs.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            cs.setFillPattern(CellStyle.SOLID_FOREGROUND); 
            cs.setBorderBottom(CellStyle.BORDER_THIN);
            cs.setBorderTop(CellStyle.BORDER_THIN);
            cs.setBorderRight(CellStyle.BORDER_THIN);
            cs.setBorderLeft(CellStyle.BORDER_THIN);
            Cell cell = null;
            
            ComplementoRR6Helper cabecera= new ComplementoRR6Helper();
            	        
	        String[] arry = cabecera.getCabecera(Integer.parseInt(tipoArchivo)).split(",");
	        
	        for(int i = 0; i < arry.length; i++)
	        {
	        	cell = rowhead.createCell(i);
            	cell.setCellValue(arry[i]);
            	cell.setCellStyle(cs);
            	sheet.autoSizeColumn(i);
	        }
	    		
	        	        
	        outputStream = new ByteArrayOutputStream();
		    workbook.write(outputStream);
		    InputStream inputStream = (new ByteArrayInputStream(outputStream.toByteArray()));
		    archivo = new ControlDescripcionArchivoDTO();
		    archivo.setInputStream(inputStream);
	        
		} catch ( Exception ex ) {
			LogDeMidasWeb.log("Excepci&oacute;n al generar platilla de carga del Reporte RR6: "
					+ ex.getMessage(), Level.ALL, ex);
        }finally {
		
		if (outputStream != null) {
			try {
				outputStream.flush();
		        outputStream.close();
				
			} catch (IOException e) {
				LogDeMidasWeb.log("Error obteniendo plantilla RR6.", Level.SEVERE, e);				
			}
		}
		
	}
				
		return SUCCESS;
	}
	
		
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	
	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}
	
	public String getTipoArchivo() {
		return tipoArchivo;
	}
	
	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	
	public String getTipoContrato() {
		return tipoContrato;
	}
	
	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}
	
	public String getAccion() {
		return accion;
	}
	
	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}
	
	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}
	
	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public ControlDescripcionArchivoDTO getArchivo() {
		return archivo;
	}

	
	public void setArchivo(ControlDescripcionArchivoDTO archivo) {
		this.archivo = archivo;
	}
	

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("complementoRR6ServiceEJB")
	public void setComplementoRR6Service(
			ComplementoRR6Service complementoRR6Service) {
		this.complementoRR6Service = complementoRR6Service;
	}
	
	
	
}
