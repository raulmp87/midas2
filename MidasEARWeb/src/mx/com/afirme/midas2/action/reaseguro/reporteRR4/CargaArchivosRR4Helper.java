package mx.com.afirme.midas2.action.reaseguro.reporteRR4;

public class CargaArchivosRR4Helper {
	
	public static final int RESQREASXL = 1;
	public static final int RESQREAS = 2;
	public static final int RESQREASVIDA = 4;
	public static final int RCNSFREAS = 3;
	
	public static final String RESQ = "IDCONTRATO,ANIO,CER,TIPOCOBERTURA,NIVEL,ORDENENTRADA,RETENCION,CAPLIMITE,RETADICIONAL,REINSTALACION,LLAVE,LLAVERETENCION,CLAVEREAS,PARTREAS,CALIFICACION";	
	public static final String RCNSFACT = "CVEREASEGURADOR,NOMBREREASEGURADOR,PAIS,ESTADO,CIUDAD,RIESGOSAUTORIZADO,FECHAALTA,FECHABAJA";
	
	public String getCabecera(int reporte) {		
	
		String head = "";
		
		switch(reporte) {
		case (RESQREASXL):{
			head = RESQ;
			break;
		}
		case (RESQREAS):{
			head = RESQ;
			break;
		}
		case (RESQREASVIDA):{
			head = RESQ;
			break;
		}
		case (RCNSFREAS):{
			head = RCNSFACT;
			break;
		}
	}				
	return head;
  }
}
