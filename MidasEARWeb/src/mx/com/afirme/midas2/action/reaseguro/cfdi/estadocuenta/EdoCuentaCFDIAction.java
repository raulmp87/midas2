package mx.com.afirme.midas2.action.reaseguro.cfdi.estadocuenta;

import java.math.BigDecimal;
import java.util.List;

import mx.com.afirme.midas2.action.BaseAction;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Contrato;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.SubRamo;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.EstadoCuenta;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.reaseguro.cfdi.estadocuenta.EdoCuentaCFDIService;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.opensymphony.xwork2.Preparable;

@Component
@Scope("prototype")
@Namespace("/reaseguro/edocuentacfdi/generacion")
public class EdoCuentaCFDIAction extends BaseAction implements Preparable {

	private static final long serialVersionUID = 6132998255096489739L;


	@Override
	public void prepare() throws Exception {
		
		estadoCuenta.setSubRamo(new SubRamo());
		
	}
	
	@Action
	(value = "mostrar", results = { 
			@Result(name = SUCCESS, location = "/jsp/reaseguro/cfdi/estadocuenta/generacion.jsp") })
	public String mostrarGeneracion() {	
		
		contratos = entidadService.findByProperty(Contrato.class, "claveNegocio", claveNegocio); //TODO agregar a la descripcion la clave del contrato
		
		return SUCCESS;
	}
	
	@Action
	(value = "obtenerPlantilla", results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${archivo.extension}",
					"inputName", "archivo.inputStream",
					"contentDisposition", "attachment;filename=${archivo.nombreArchivo}"})}) 
	public String obtenerPlantilla() {	
		
		archivo = edoCuentaCFDIService.obtenerPlantilla(tipoPlantilla, contrato);
		
		return SUCCESS;
	}
	
	@Action
	(value = "procesarinfo", results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${archivo.extension}",
					"inputName", "archivo.inputStream",
					"contentDisposition", "attachment;filename=${archivo.nombreArchivo}"})})
	public String cargarInfo() {	
		
		archivo = edoCuentaCFDIService.procesarInfo(idToControlArchivo, claveNegocio);
		
		return SUCCESS;
	}
	
	@Action
	(value = "/reaseguro/edocuentacfdi/impresion/mostrar", results = { 
			@Result(name = SUCCESS, location = "/jsp/reaseguro/cfdi/estadocuenta/impresion.jsp") })
	public String mostrarImpresion() {	
		
		contratos = entidadService.findByProperty(Contrato.class, "claveNegocio", claveNegocio); //TODO agregar a la descripcion la clave del contrato
		
		subRamos = entidadService.findAll(SubRamo.class);
		
		reaseguradores = edoCuentaCFDIService.findReasCoas();
		
		return SUCCESS;
	}
	
	@Action
	(value = "/reaseguro/edocuentacfdi/impresion/buscar", results = { 
			@Result(name = SUCCESS, location = "/jsp/reaseguro/cfdi/estadocuenta/edoCuentaCFDIGrid.jsp") })
	public String buscar() {	
		
		estadosCuenta = edoCuentaCFDIService.filtrar(contrato, estadoCuenta, folio);
		
		return SUCCESS;
	}
	
	@Action
	(value = "/reaseguro/edocuentacfdi/impresion/imprimir", results = { 
			@Result(name = SUCCESS, type = "stream", params = {
					"contentType", "${archivo.extension}",
					"inputName", "archivo.inputStream",
					"contentDisposition", "attachment;filename=${archivo.nombreArchivo}"})})
	public String imprimir() {	
		
		archivo = edoCuentaCFDIService.imprimir(folio, esPDF);
		
		return SUCCESS;
	}
	
	private String claveNegocio;
	
	private BigDecimal idToControlArchivo;
	
	private Contrato contrato = new Contrato();
	
	private List<Contrato> contratos = new ArrayListNullAware<Contrato>();
	
	private Integer tipoPlantilla;
	
	private ControlDescripcionArchivoDTO archivo;
	
	private EntidadService entidadService;
	
	private EdoCuentaCFDIService edoCuentaCFDIService;
	
	private Integer esPDF;
	
	private List<EstadoCuenta> estadosCuenta = new ArrayListNullAware<EstadoCuenta>();
	
	private EstadoCuenta estadoCuenta = new EstadoCuenta();
	
	private List<String> reaseguradores = new ArrayListNullAware<String>();
	
	private List<SubRamo> subRamos = new ArrayListNullAware<SubRamo>();
	
	private BigDecimal folio;
	

	public String getClaveNegocio() {
		return claveNegocio;
	}


	public void setClaveNegocio(String claveNegocio) {
		this.claveNegocio = claveNegocio;
	}

	
	public BigDecimal getIdToControlArchivo() {
		return idToControlArchivo;
	}

	
	public void setIdToControlArchivo(BigDecimal idToControlArchivo) {
		this.idToControlArchivo = idToControlArchivo;
	}

	
	public Contrato getContrato() {
		return contrato;
	}


	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}


	public List<Contrato> getContratos() {
		return contratos;
	}


	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}


	public Integer getTipoPlantilla() {
		return tipoPlantilla;
	}


	public void setTipoPlantilla(Integer tipoPlantilla) {
		this.tipoPlantilla = tipoPlantilla;
	}


	public ControlDescripcionArchivoDTO getArchivo() {
		return archivo;
	}

	
	public void setArchivo(ControlDescripcionArchivoDTO archivo) {
		this.archivo = archivo;
	}
	
	
	public Integer getEsPDF() {
		return esPDF;
	}

	public void setEsPDF(Integer esPDF) {
		this.esPDF = esPDF;
	}

	public List<EstadoCuenta> getEstadosCuenta() {
		return estadosCuenta;
	}

	public void setEstadosCuenta(List<EstadoCuenta> estadosCuenta) {
		this.estadosCuenta = estadosCuenta;
	}

	public EstadoCuenta getEstadoCuenta() {
		return estadoCuenta;
	}

	public void setEstadoCuenta(EstadoCuenta estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}
	
	public List<String> getReaseguradores() {
		return reaseguradores;
	}

	public void setReaseguradores(List<String> reaseguradores) {
		this.reaseguradores = reaseguradores;
	}

	public List<SubRamo> getSubRamos() {
		return subRamos;
	}

	public void setSubRamos(List<SubRamo> subRamos) {
		this.subRamos = subRamos;
	}

	public BigDecimal getFolio() {
		return folio;
	}

	public void setFolio(BigDecimal folio) {
		this.folio = folio;
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Autowired
	@Qualifier("edoCuentaCFDIServiceEJB")
	public void setEdoCuentaCFDIService(
			EdoCuentaCFDIService edoCuentaCFDIService) {
		this.edoCuentaCFDIService = edoCuentaCFDIService;
	}
	
}
