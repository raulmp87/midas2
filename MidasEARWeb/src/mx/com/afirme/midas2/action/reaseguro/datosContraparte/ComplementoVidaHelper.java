package mx.com.afirme.midas2.action.reaseguro.datosContraparte;


public class ComplementoVidaHelper {
	
	public static final int PENDIENTES = 1;
	public static final int FACULTADAS = 2;	
	public static final int AUTOMATICOS = 3;
	public static final int SISE = 4;
	public static final int AJUSTES = 5;
	
	public static final String SINIESTROS_PENDIENTES = "POLIZA, MONEDA, ANIO_SINIESTRO,NUM_SINIESTRO, RESERVA," 
		+"COSTO_SINIESTRO, AFIRME, HANNOVER_LIFE-RE, MAPFRE-RE, STATUS, INICIO_VIGENCIA, COBERTURA";
	public static final String POLIZAS_FACULTADAS = "POLIZA, MONEDA, ANIO_SINIESTRO,NUM_SINIESTRO, RESERVA," 
		+"COSTO_SINIESTRO, AFIRME, MONTO_DISTRIBUCION, STATUS, RGRE, REASEGURADOR";
	public static final String CONTRATOS_AUTOMATICOS = "CONTRATO, REASEGURADORES, RGRE, PARTICIPACION, INICIO_VIGENCIA," 
		+"FIN_VIGENCIA, COBERTURA";
	public static final String DISTRIBUCION_SISE = "SINIESTRO, REASEGURADOR, CLAVE, RESERVA, MONEDA";
	public static final String AJUSTES_MANUALES = "SINIESTRO, REASEGURADOR, CNSF, MONEDA, RESERVA_PENDIENTE_REAS, ANIO, SISTEMA";
	
	
	public String getCabecera(int reporte) {
		
	
		String head = "";
		
		switch(reporte) {
		case (PENDIENTES):{
			head = SINIESTROS_PENDIENTES;
			break;
		}
		case (FACULTADAS):{
			head = POLIZAS_FACULTADAS;
			break;
		}
		case (AUTOMATICOS):{
			head = CONTRATOS_AUTOMATICOS;
			break;
		}
		case (SISE):{
			head = DISTRIBUCION_SISE;
			break;
		}
		case (AJUSTES):{
			head = AJUSTES_MANUALES;
			break;
		}
		
	}		
		
	return head;
  }

}
