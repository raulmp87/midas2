package mx.com.afirme.midas2.action;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Sender;

import mx.com.afirme.midas2.util.apn.SendAPNs;

public class PushNotificationAction extends BaseAction {
	
	private static final long serialVersionUID = 1L;
	
	private InputStream inputStream;
	
	private String type;
	private String message;
	private String to;
	private String from;
	private String receiverId;
	private String senderId;
	private String title;
	private String event;
	private String certificatePath;
	private String env;
	private String password;
	

	public String push() {
		inputStream = new  ByteArrayInputStream(SUCCESS.getBytes());
		
		try {
			Map<String, String> data = new HashMap<String, String>();
			data.put("sender", from);
			data.put("senderName", to);
			
			if (message == null) {
				message = "Hola Mundo";
			}
			if (event == null) {
				event = "test";
			}

			if ("I".equals(type)) {
				data.put("message",  message);
				data.put("evento", event);
				data.put("title", title);
				SendAPNs apn = new SendAPNs();
				if (certificatePath == null) {
					certificatePath = sistemaContext.getRutaCertificadoClienteMovil() + sistemaContext.getNombreCertificadoClienteSuite();
				}
				if (env == null) {
					env = sistemaContext.getClienteSuiteEnvironment();
				}
				if (password == null) {
					password = sistemaContext.getPasswordCertificadoClienteSuite();
				}
				
				apn.sendAPN(data, receiverId, certificatePath, password
						, message, env);
			} else {
				final Builder builder = new Builder();
				builder.addData("message",  message);
				builder.addData("evento", event);
				builder.addData("title", title);
				if (data != null) {
					for (Map.Entry<String, String> entry : data.entrySet()) {
						builder.addData(entry.getKey(), entry.getValue());
					}
				}
				if (senderId == null) {
					senderId = sistemaContext.getClienteMovilSenderKey();
				}
				final Sender sender = new Sender(senderId);
				Object gcmResult = sender.send(builder.build(), receiverId, 2);
				LOG.info("--push() result: " +  gcmResult);
			}
		} catch (Exception e) {
			inputStream = new  ByteArrayInputStream(ERROR.getBytes());
			LOG.error("Ocurrio un error al enviar la notificacion: "+ e.getMessage());	
		}
		
		return SUCCESS;
	}

	
	public InputStream getInputStream() {
	    return inputStream;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getTo() {
		return to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public String getFrom() {
		return from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getReceiverId() {
		return receiverId;
	}


	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}


	public String getSenderId() {
		return senderId;
	}


	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getEvent() {
		return event;
	}


	public void setEvent(String event) {
		this.event = event;
	}


	public String getCertificatePath() {
		return certificatePath;
	}


	public void setCertificatePath(String certificatePath) {
		this.certificatePath = certificatePath;
	}


	public String getEnv() {
		return env;
	}


	public void setEnv(String env) {
		this.env = env;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	} 
	
	
}
