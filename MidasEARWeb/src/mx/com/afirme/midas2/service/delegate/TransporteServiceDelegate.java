package mx.com.afirme.midas2.service.delegate;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.service.TransporteService;

import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@RemoteProxy(name="transporteService")
public class TransporteServiceDelegate {

	TransporteService service;

	@Autowired
	@Qualifier("transporteEJB")
	public void setService(TransporteService service) {
		this.service = service;
	}
	
	public CoberturaDTO obtenerCobertura(Long id){
		return service.obtenerCobertura(id);
	}
	
	@Deprecated
	public AgenteDTO obtenerAgente(Integer id){
		return service.obtenerAgente(id);
	}
	
	public Agente obtenerDatosAgente(Long id){
		return service.obtenerDatosAgente(id);
	}
	
	public Agente obtenerDatosAgenteClave(Long idAgente){
		return service.obtenerDatosAgenteClave(idAgente);
	}
	
	
}
