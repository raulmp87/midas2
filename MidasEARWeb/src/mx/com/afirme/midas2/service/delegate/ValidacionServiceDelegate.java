package mx.com.afirme.midas2.service.delegate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.excepcion.ExcepcionDeAccesoADatos;
import mx.com.afirme.midas.wsCliente.curp.ValidaCURPDN;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;
import mx.com.afirme.midas2.service.ValidacionService;

import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.afirme.validation.curp.ValidateCurp;

@RemoteProxy(name="validacionService")
public class ValidacionServiceDelegate implements ValidacionService {

	private ValidacionService validacionService;

	public ValidacionServiceDelegate() {

	}

	@Override
	public Map<Boolean,Double> validaDescuentoGlobal(Double porcentaje,
			BigDecimal idToCotizacion) {
		return validacionService.validaDescuentoGlobal(porcentaje,
				idToCotizacion);
	}
	
	@Override
	public Boolean validaFechaCotizacion(String fechaInicioVigencia,
			String fechaFinVigencia, BigDecimal idToCotizacion) {
		return validacionService.validaFechaCotizacion(fechaInicioVigencia, fechaFinVigencia, idToCotizacion);
	}
	
	public boolean numeroAgenteValido(Long numeroAgente){
	    	return validacionService.numeroAgenteValido(numeroAgente);
	}
	
	public Boolean validaCURPPersonaAgentes(String estadoNacimiento,String codigoCURP,String fechaNacimiento,
											String nombre, String apellidoPaterno,	String apellidoMaterno, 
											String sexo, String claveTipoPersona) {
		boolean valorReturn = true;
		if(estadoNacimiento!=null 	&& !estadoNacimiento.equals("") &&
		   codigoCURP!=null       	&& !codigoCURP.equals("")       &&
		   fechaNacimiento!=null  	&& !fechaNacimiento.equals("")  &&
		   nombre!=null       		&& !nombre.equals("")       	&&
		   apellidoPaterno!=null    && !apellidoPaterno.equals("")  &&
		   apellidoMaterno!=null    && !apellidoMaterno.equals("")  &&
		   sexo!=null       		&& !sexo.equals("")       		&&
		   claveTipoPersona!=null   && claveTipoPersona.equals("1")){		
		ValidateCurp curpVO = new ValidateCurp();
		curpVO.setBornState(estadoNacimiento);
		curpVO.setCapturedCurp(codigoCURP);
		curpVO.setPersonFirstLastName(apellidoPaterno);
		curpVO.setPersonSecondLastName(apellidoMaterno);
		curpVO.setBirthDate(fechaNacimiento);
		curpVO.setPersonName(nombre);
		if(sexo!= null && sexo.equals("M")){
			curpVO.setPersonGender("MASCULINO");
		}else{
			curpVO.setPersonGender("FEMENINO");
		}
		try {
			curpVO = ValidaCURPDN.getInstancia().valida(curpVO);
			valorReturn = curpVO.isValid();
		} catch (ExcepcionDeAccesoADatos e) {	
			valorReturn =false;
			e.printStackTrace();
		} catch (SystemException e) {
			valorReturn = false;
			e.printStackTrace();
		}
		}
		else{
			if(claveTipoPersona!=null && claveTipoPersona.equals("2")){
				valorReturn=true;
			}
			else{
				valorReturn=false;
			}
		} 
		return valorReturn;
	}	

	@Override
	public Map<Integer, Integer> validaFechaCotizacion(Date fechaInicioVigencia,
			Date fechaFinVigencia, BigDecimal idToCotizacion) {
		return validacionService.validaFechaCotizacion(fechaInicioVigencia, fechaFinVigencia, idToCotizacion);
	}
	
	@Override
	public Boolean obtenerLineasNegocioAsociadas(BigDecimal idToPoliza) {
		return validacionService.obtenerLineasNegocioAsociadas(idToPoliza);
	}
	
	@Override 
	public boolean validarExisteCrecimiento(Long idConfiguracion){
		return validacionService.validarExisteCrecimiento(idConfiguracion);
	}
	
	@Override
	public Boolean validaSupMetaXConfigBono(Long idConfigBono) {
		return validacionService.validaSupMetaXConfigBono(idConfigBono);
	}
	
	@Override
	public Boolean existeRangoConfigurado(Long idConfigBono) {
		return validacionService.existeRangoConfigurado(idConfigBono);
	}
	
	@Override
	public Boolean validaTamanioCampoBonificacion(Long idConfigBono) {
		// TODO Auto-generated method stub
		return validacionService.validaTamanioCampoBonificacion(idConfigBono);
	}
	
	@Override
	public Map<Boolean, String> validaDescuentoPorEstado(Double porcentaje,
			Long idToNegocio, String idToEstado, BigDecimal idToCotizacion, Long idToNegPaqueteSeccion) {
		return validacionService.validaDescuentoPorEstado(porcentaje,
				idToNegocio, idToEstado, idToCotizacion, idToNegPaqueteSeccion);
	}
	@Override
	public Boolean validaRangosGridAmis(List<ConfigBonoRangoClaveAmis> lista) {
		// TODO Auto-generated method stub
		return validacionService.validaRangosGridAmis(lista);
	}
	
	
	/**
	 * Valida que una cotizacion de poliza tenga asignado un id de contratante 
	 * @param idToCotizacion
	 * @return retorna verdadero si el atributo idToPersonaContratante cuenta con un valor diferente de nulo
	 */
	@Override
	public Boolean validarContratanteAsignadoCotizacion(Long idToCotizacion)
	{		
		return validacionService.validarContratanteAsignadoCotizacion(idToCotizacion);
	}
	
	/* Set & Get */
	@Autowired
	@Qualifier("validacionServiceEJB")
	public void setValidacionService(ValidacionService validacionService) {
		this.validacionService = validacionService;
	}





}
