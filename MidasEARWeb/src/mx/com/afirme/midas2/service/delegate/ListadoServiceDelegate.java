package mx.com.afirme.midas2.service.delegate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.service.ListadoService;

import org.directwebremoting.annotations.RemoteProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@RemoteProxy(name = "listadoService")
public class ListadoServiceDelegate implements ListadoService {
    
	private static final Logger LOG = LoggerFactory.getLogger(ListadoServiceDelegate.class);
	@Override
	public List<MonedaDTO> getListarMonedas() {
		return listadoService.getListarMonedas();
	}

	@Override
	public List<SeccionDTO> getListarSeccionesVigentesAutos() {
		return listadoService.getListarSeccionesVigentesAutos();
	}

	@Override
	public List<ValorSeccionCobAutos> getListarValorSeccionCobAutosPorSeccion(
			Long idToSeccion) {
		return listadoService
				.getListarValorSeccionCobAutosPorSeccion(idToSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapCoberturasSeccionPorSeccion(
			BigDecimal idToSeccion) {
		return listadoService.getMapCoberturasSeccionPorSeccion(idToSeccion);
	}

	@Override
	public Map<Long, String> getMapPaquetesPorSeccion(BigDecimal idToSeccion) {
		return listadoService.getMapPaquetesPorSeccion(idToSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapTipoUsoVehiculoPorSeccion(
			BigDecimal idToSeccion) {
		return listadoService.getMapTipoUsoVehiculoPorSeccion(idToSeccion);
	}

	@Override
	public Map<Short, String> getMapTipoLimiteSumaAseg() {
		return listadoService.getMapTipoLimiteSumaAseg();
	}

	@Override
	public Map<AgrupadorTarifaId, String> getMapAgrupadorTarifaPorNegocio(
			String claveNegocio) {
		return listadoService.getMapAgrupadorTarifaPorNegocio(claveNegocio);
	}

	@Override
	public Map<BigDecimal, String> getMapLineaNegocioPorAgrupadorTarifa(
			String idToAgrupadorTarifaFromString) {
		return listadoService.getMapLineaNegocioPorAgrupadorTarifa(idToAgrupadorTarifaFromString);
	}

	@Override
	public Map<Short, String> getMapMomendaPorAgrupadorTarifaLineaNegocio(
			String idToAgrupadorTarifaFromString, BigDecimal idToSeccion) {
		return listadoService.getMapMomendaPorAgrupadorTarifaLineaNegocio(idToAgrupadorTarifaFromString, idToSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapTipoServicioVehiculoPorTipoVehiculo(
			BigDecimal idTcTipoVehiculo) {
		return listadoService.getMapTipoServicioVehiculoPorTipoVehiculo(idTcTipoVehiculo);
	}

	@Override
	public Map<BigDecimal, String> getMapTipoVehiculoPorSeccion(
			BigDecimal idToSeccion) {
		return listadoService.getMapTipoVehiculoPorSeccion(idToSeccion);
	}

	@Override
	public List<ServVehiculoLinNegTipoVeh> getListarServVehiculoLinNegTipoVehPorSeccion(
			Long idToSeccion) {

		return listadoService.getListarServVehiculoLinNegTipoVehPorSeccion(idToSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapAgrupadoresPorNegocio(
			String claveNegocio, Long idMoneda){
		return listadoService.getMapAgrupadoresPorNegocio(claveNegocio,idMoneda);
	}

	@Override
	public Map<BigDecimal, String> getMapAgrupadoresPorMonedaPorSeccion(String claveNegocio, BigDecimal idSeccion,Long idMoneda){
		return listadoService.getMapAgrupadoresPorMonedaPorSeccion(claveNegocio, idSeccion, idMoneda);
	}

	@Override
	public Map<Long, String> getMapMonedas() {
		return listadoService.getMapMonedas();
	}

	@Override
	public Map<String, String> getMapMunicipiosPorEstado(String stateId) {
		return listadoService.getMapMunicipiosPorEstado(stateId);
	}

	@Override
	public List<EstadoDTO> listarEstadosMX() {
		return this.listadoService.listarEstadosMX();
	}

	@Override
	public Map<Object, String> listarOficinas() {
		return this.listadoService.listarOficinas();
	}

	private ListadoService listadoService;

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService){
		this.listadoService = listadoService;
	}

	@Override
	public String getClaveTipoDetalleGrupo(Long idGrupo) {
		return this.listadoService.getClaveTipoDetalleGrupo(idGrupo);
	}

	@Override
	public List<ProductoDTO> getListarProductos(String claveNegocio, boolean mostrarInactivos) {
		return this.listadoService.getListarProductos(claveNegocio, mostrarInactivos);
	}

	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorSeccion(BigDecimal idToNegSeccion) {
		return this.listadoService.getMapNegocioPaqueteSeccionPorSeccion(idToNegSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapNegocioSeccionPorTipoPoliza(
			BigDecimal idToTipoPoliza) {
		return this.listadoService.getMapNegocioSeccionPorTipoPoliza(idToTipoPoliza);
	}

	@Override
	public Map<Object,String> listarAgentes(){
		return this.listadoService.listarAgentes();
	}
	
	@Override
	public Map<Object, String> listarGerencias() {
		return this.listadoService.listarGerencias();
	}

	@Override
	public Map<Object, String> listarOficinasPorGerencia(String id) {
		return this.listadoService.listarOficinasPorGerencia(id);
	}

	@Override
	public Map<Object, String> listarPromotoriasPorOficina(String id) {
		return this.listadoService.listarPromotoriasPorOficina(id);
	}

	@Override
	public Map<Object, String> listarAgentesPorPromotoria(String id) {
		return this.listadoService.listarAgentesPorPromotoria(id);
	}

	@Override
	public Map<Object, String> listarAgentesPorGerencia(String id) {
		return this.listadoService.listarAgentesPorGerencia(id);
	}

	@Override
	public Map<Object, String> listarAgentesPorOficina(String id) {
		return this.listadoService.listarAgentesPorOficina(id);
	}

	@Override
	public Map<String, String> listarEstilosPorMarcaCveTipoBien(
			BigDecimal idTcMarcaVehiculo, String claveTipoBien) {
		return this.listadoService.listarEstilosPorMarcaCveTipoBien(
				idTcMarcaVehiculo, claveTipoBien);
	}

	@Override
	public Map<BigDecimal, String> listarMarcasPorCveTipoBien(
			String claveTipoBien) {
		return this.listadoService.listarMarcasPorCveTipoBien(claveTipoBien);
	}

	@Override
	public Map<BigDecimal, String> listarTiposVehiculo(String claveTipoBien) {
		return this.listadoService.listarTiposVehiculo(claveTipoBien);
	}

	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(BigDecimal idToSeccion) {
		return this.listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(idToSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapMonedaPorNegTipoPoliza(BigDecimal id){
		return this.listadoService.getMapMonedaPorNegTipoPoliza(id);
	}

	@Override
	public Map<Long, String> listarNegociosActivos(String cveNegocio) {
		return listadoService.listarNegociosActivos(cveNegocio);
	}

	@Override
	public Map<BigDecimal, String> getMapProductos(Long idToNegocio, Integer activo) {
		activo=1;
		return listadoService.getMapProductos(idToNegocio, activo);
	}

	@Override
	public Map<String, String> getMapEstadosMX() {
		return listadoService.getMapEstadosMX();
	}
	@Override
	public Map<BigDecimal, String> getMapSeccionPorTipoPoliza(BigDecimal idToTipoPoliza){
		return listadoService.getMapNegocioSeccionPorTipoPoliza(idToTipoPoliza);
	}

	public Map<BigDecimal, String> getMapTipoPolizaPorProducto(BigDecimal idToProducto){
		return listadoService.getMapTipoPolizaPorProducto(idToProducto);
	}

	public Map<BigDecimal, String> getMapNegocioCoberturaPaqueteSeccionPorPaquete(Long idToNegPaqueteSeccion) {
		return listadoService.getMapNegocioCoberturaPaqueteSeccionPorPaquete(idToNegPaqueteSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapMarcaVehiculoPorTipoUsoNegocioSeccion(BigDecimal idToNegSeccion) {

		return listadoService.getMapMarcaVehiculoPorTipoUsoNegocioSeccion(idToNegSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapTipoUsoVehiculoByNegocio (BigDecimal idToNegSeccion){
		return listadoService.getMapTipoUsoVehiculoByNegocio (idToNegSeccion);
	}
	
	@Override
	public Map<BigDecimal, String> getMapTipoUsoVehiculoPorEstiloVehiculo(String estiloVehiculoId, BigDecimal idToNegSeccion){
		return listadoService.getMapTipoUsoVehiculoPorEstiloVehiculo(estiloVehiculoId, idToNegSeccion);
	}

	@Override
	public Map<String, String> getMapTipoUsoVehiculoPorEstiloVehiculoString(String estiloVehiculoId, BigDecimal idToNegSeccion){
		return listadoService.getMapTipoUsoVehiculoPorEstiloVehiculoString(estiloVehiculoId, idToNegSeccion);
	}
	@Override
	public Map<Long, String> getMapNegProductoPorNegocio(Long idToNegocio){
		return listadoService.getMapNegProductoPorNegocio(idToNegocio);
	}

	@Override
	public Map<BigDecimal, String> getMapNegTipoPolizaPorNegProducto(Long idToNegProducto){
		return listadoService.getMapNegTipoPolizaPorNegProducto(idToNegProducto);
	}

	@Override
	public Map<BigDecimal, String> getMapNegSeccionPorNegTipoPoliza(BigDecimal idToNegTipoPoliza){
		return listadoService.getMapNegSeccionPorNegTipoPoliza(idToNegTipoPoliza);
	}

	@Override
	public Map<Long, String> getMapNegPaqueteSeccionPorNegSeccion(BigDecimal idToNegSeccion){
		return listadoService.getMapNegPaqueteSeccionPorNegSeccion(idToNegSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapNegCoberturaPaquetePorNegPaqueteSeccion(Long idToNegPaqueteSeccion){
		return listadoService.getMapNegCoberturaPaquetePorNegPaqueteSeccion(idToNegPaqueteSeccion);
	}

	@Override
	public Map<Short, Short> getMapModeloVehiculoPorTipoUsoEstilo(BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion){
		return listadoService.getMapModeloVehiculoPorTipoUsoEstilo(idMoneda,claveEstilo, idToNegSeccion);
	}

	@Override
	public Map<Long, String> listarNegociosPorAgente(Integer id) {
		return listadoService.listarNegociosPorAgente(id);
	}
	
	@Override
	public Map<Long, String> getNegociosPorAgente(Integer idAgente, String claveNegocio){
		return listadoService.getNegociosPorAgente(idAgente, claveNegocio);
	}
	
	@Override
	public Map<Long, String> getNegociosPorAgenteParaAutos(Integer idAgente){
		return listadoService.getNegociosPorAgenteParaAutos(idAgente);
	}

	@Override
	public Map<BigDecimal, String> getMapCoberturaPorNegPaqueteSeccion(Long arg0) {
		return listadoService.getMapCoberturaPorNegPaqueteSeccion(arg0);
	}

	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculo(BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion,BigDecimal idMoneda){
		return listadoService.getMapEstiloVehiculoPorMarcaVehiculo(idTcMarcaVehiculo, idToNegSeccion, idMoneda);
	}

	@Override
	public Map<Long, String> getMapPaquetePorNegocioSeccion(
			BigDecimal idToNegSeccion) {
		return listadoService.getMapPaquetePorNegocioSeccion(idToNegSeccion);
	}

	@Override
	public Map<Object, String> listarCveTipoBien() {
		return listadoService.listarCveTipoBien();
	}

	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String descripcion) {
		return listadoService.listarEstilosPorDescripcion(descripcion);
	}

	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String tipoBien,String descripcion, BigDecimal idToNegSeccion, BigDecimal idMoneda) {
		return listadoService.listarEstilosPorDescripcion(tipoBien,descripcion, idToNegSeccion, idMoneda);
	}

	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String claveTipoBien,String descripcion,BigDecimal idToNegSeccion) {
		return listadoService.listarEstilosPorDescripcion(claveTipoBien,descripcion,idToNegSeccion);
	}

	@Override
	public Map<Integer, String> getMapFormasdePago(CotizacionDTO cotizacion) {
		return listadoService.getMapFormasdePago(cotizacion);
	}

	@Override
	@Deprecated
	public List<AgenteDTO> listarAgentesPorDescripcion(String arg0) {
		return listadoService.listarAgentesPorDescripcion(arg0);
	}
	
	public List<Agente> listaAgentesPorDescripcion(String descripcion){
		return listadoService.listaAgentesPorDescripcion(descripcion);
	}

	@Override
	public List<AgenteView> listaAgentesPorDescripcionLightWeight(String descripcion){
		return listadoService.listaAgentesPorDescripcionLightWeight(descripcion);
    }

	@Override
	public Map<Long, Double> getMapDerechosPoliza(CotizacionDTO arg0) {
		return listadoService.getMapDerechosPoliza(arg0);
	}

	@Override
	public Map<Object, String> listarLineasDeNegocioPorCotizacionId(
			BigDecimal idToCotizacion) {
		return listadoService.listarLineasDeNegocioPorCotizacionId(idToCotizacion);
	}

	@Override
	public Set<String> listarIncisosDescripcionByCotId(BigDecimal idToCotizacion) {

		return listadoService.listarIncisosDescripcionByCotId(idToCotizacion);
	}

	@Override
	public Map<Object, String> listarCentrosEmisores() {
		return listadoService.listarCentrosEmisores();
	}

	@Override
	public Map<Object, String> listarGerenciasPorCentroEmisor(String arg0) {
		return listadoService.listarGerenciasPorCentroEmisor(arg0);
	}

	@Override
	public Map<Long, String> listarNegociosPorAgente(Integer idTcAgente, String cveTipoNegocio,
			Integer status) {
		return listadoService.listarNegociosPorAgente(idTcAgente, cveTipoNegocio, status);
	}

	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(
			BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion,
			BigDecimal idMoneda) {
		return listadoService
				.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(
						idTcMarcaVehiculo, idToNegSeccion, idMoneda);
	}
	
	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(
			BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId) {
		return listadoService
				.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(
						idTcMarcaVehiculo, idToNegSeccion, idMoneda, autoIncisoContinuityId);
	}
	
	public  Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMonedaByDescription(
			Integer modelo, String descripcionVehiculo, BigDecimal idToNegSeccion){
		return listadoService.getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMonedaByDescription(
				modelo, descripcionVehiculo, idToNegSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapMarcaVehiculoPorNegocioSeccion(
			BigDecimal idToNegSeccion) {
		return listadoService
				.getMapMarcaVehiculoPorNegocioSeccion(idToNegSeccion);
	}
	
	@Override
	public Map<String, String> getMapMarcaVehiculoPorNegocioSeccionString(
			BigDecimal idToNegSeccion){
		return listadoService
				.getMapMarcaVehiculoPorNegocioSeccionString(idToNegSeccion);
	}
	
	@Override
	public Map<Short, Short> getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(
			BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion) {
		return listadoService
				.getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(idMoneda,
						claveEstilo, idToNegSeccion);
	}

	@Override
	public Map<String, String> getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(BigDecimal idMoneda, 
			String claveEstilo, BigDecimal idToNegSeccion){
		return listadoService.getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(
				idMoneda, claveEstilo, idToNegSeccion);
	}
	@Override
	public Map<Object, String> listarCatalogoValorFijo(Integer catalogoId) {
		return listadoService.listarCatalogoValorFijo(catalogoId);
	}

	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(
			BigDecimal idToCotizacion, BigDecimal idToNegSeccion) {
		return listadoService
				.getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(
						idToCotizacion, idToNegSeccion);
	}

	@Override
	public Map<Short, String> getMapMonedaPorAgrupadorTarifa(
			String idToAgrupadorTarifaFromString) {
		return listadoService
				.getMapMonedaPorAgrupadorTarifa(idToAgrupadorTarifaFromString);
	}

	@Override
	public Map<BigDecimal, String> getMapCoberturaPorNegocioSeccion(
			Long idToNegocio) {
		return listadoService.getMapCoberturaPorNegocioSeccion(idToNegocio);
	}

	@Override
	public Map<String, String> getMapColonias(String idMunicipio) {
		return listadoService.getMapColonias(idMunicipio);
	}

	@Override
	public Map<String, String> getMapEstados(String idPais) {
		return listadoService.getMapEstados(idPais);
	}

	@Override
	public Map<String, String> getMapPaises() {
		return listadoService.getMapPaises();
	}

	@Override
	public String getCodigoPostal(String idColonia) {
		return listadoService.getCodigoPostal(idColonia);
	}

	

	@Override
	public Map<Long, String> getMapEjecutivosPorGerencia(Long idGerencia) {
		return listadoService.getMapEjecutivosPorGerencia(idGerencia);
	}

	@Override
	public Map<Long, String> getMapEjecutivos() {
		return listadoService.getMapEjecutivos();
	}

	@Override
	public Map<Long, String> getMapGerenciasPorCentroOperacion(
			Long idCentroOperacion) {
		return listadoService
				.getMapGerenciasPorCentroOperacion(idCentroOperacion);
	}

	@Override
	public Map<Long, String> getMapPromotoriasPorEjecutivo(Long idEjecutivo) {
		return listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo);
	}

	@Override
	public String getCodigoPostalByColonyNameAndCityId(String colonia,
			String idMunicipio) {
		return listadoService.getCodigoPostalByColonyNameAndCityId(colonia,
				idMunicipio);
	}

	@Override
	public Map<String, String> getMapColoniasSameValue(String idMunicipio) {
		return listadoService.getMapColoniasSameValue(idMunicipio);
	}

	@Override
	public Map<Long, String> getMapGerencias() {
		return listadoService.getMapGerencias();
	}

	@Override
	public List<ProductoBancario> getListaProductosBancarios() {
		return listadoService.getListaProductosBancarios();
	}

	@Override
	public List<ProductoBancario> getListaProductosBancariosPorBanco(
			Long idBanco) {
		return listadoService.getListaProductosBancariosPorBanco(idBanco);
	}

	@Override
	public Map<String, String> getEstadosPorNegocioId(Long idToNegocio,
			Boolean cotizacionExpress) {
		return listadoService.getEstadosPorNegocioId(idToNegocio,
				cotizacionExpress);
	}

	@Override
	public Map<String, String> getMunicipiosPorEstadoId(
			BigDecimal idToCotizacion, String idToEstado) {
		return listadoService.getMunicipiosPorEstadoId(idToCotizacion,
				idToEstado);
	}

	@Override
	public Map<BigDecimal, Integer> getListaVersionesPorCodigoProducto(
			String codigo) {
		return listadoService.getListaVersionesPorCodigoProducto(codigo);
	}

	@Override
	public Map<Long, String> getMapAgentesPorPromotoria(Long idPromotoria) {
		return listadoService.getMapAgentesPorPromotoria(idPromotoria);
	}

	@Override
	public Map<Long, String> getMapAgentesPorGerenciaOficinaPromotoria(
			Long gerencia, Long oficina, Long idPromotoria, Short soloAutorizado) {
		return listadoService.getMapAgentesPorGerenciaOficinaPromotoria(
				gerencia, oficina, idPromotoria, soloAutorizado);
	}

	@Override
	public Map<Long, String> getConductosCobro(Long idCliente,
			Integer idTipoConductoCobro) {
		return listadoService.getConductosCobro(idCliente, idTipoConductoCobro);
	}

	@Override
	public Map<Integer, String> getMapFormasdePago(SolicitudDTO solicitudDTO) {
		return listadoService.getMapFormasdePago(solicitudDTO);
	}

	@Override
	public BigDecimal getTipoUsoDefault(BigDecimal negocioSeccion) {
		return listadoService.getTipoUsoDefault(negocioSeccion);
	}

	@Override
	public BigDecimal getTipoServicioDefault(BigDecimal negocioSeccion) {
		return listadoService.getTipoServicioDefault(negocioSeccion);
	}

	@Override
	public Long getCountNegocioEstiloVehiculo(BigDecimal idToNegocioSeccion,
			BigDecimal idMoneda) {
		return listadoService.getCountNegocioEstiloVehiculo(idToNegocioSeccion, idMoneda);
	}

	@Override
	public List<Negocio> listaNegociosPorDescripcion(String descripcion){
		return listadoService.listaNegociosPorDescripcion(descripcion);
	}

	@Override
	public String getEstadoIdPorCp(String cp){
		return listadoService.getEstadoIdPorCp(cp);
	}

	@Override
	public Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda) {
		return listadoService.getPctePagoFraccionado(idFormaPago, idMoneda);
	}

	@Override
	public Map<Long, String> getMapDerechosEndoso(SolicitudDTO solicitud) {
		return listadoService.getMapDerechosEndoso(solicitud);
	}

	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(
			short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn,
			BigDecimal idToNegSeccion) {

		return listadoService.getMapNegocioPaqueteSeccionPorLineaNegocio(claveTipoEndoso, cotizacionContinuityId, validoEn,idToNegSeccion);
	}

	@Override
	public Map<BigDecimal, String> getMapNegTipoPolizaPorNegProductoFlotillaOIndividual(
			Long idToNegProducto, int aplicaFlotilla) {
		return listadoService.getMapNegTipoPolizaPorNegProductoFlotillaOIndividual(idToNegProducto, aplicaFlotilla);
	}
	
	public Map<String, String> getMapNegTipoPolizaPorNegProductoFlotillaOIndividualString(Long idToNegProducto, 
			int aplicaFlotilla){
		return listadoService.getMapNegTipoPolizaPorNegProductoFlotillaOIndividualString(idToNegProducto, aplicaFlotilla);
	}

	@Override
	public Map<BigDecimal,String> getAgrupadorTarifaById(
			BigDecimal idToAgrupadorTarifa) {
		return listadoService.getAgrupadorTarifaById(idToAgrupadorTarifa);
	}

	@Override
	public Map<BigDecimal, String> getMapNegTipoPolizaAutoExpediblePorNegProducto(Long idToNegProducto){
		return listadoService.getMapNegTipoPolizaAutoExpediblePorNegProducto(idToNegProducto);
	}

	@Override
	public Long getNegocioProducto(Long idToNegocio, BigDecimal idToProducto) {
		return listadoService.getNegocioProducto(idToNegocio, idToProducto);
	}
	
	@Override
	public Map<String, String> getPromocionesTC(String numTarjeta){
		return listadoService.getPromocionesTC(numTarjeta);
	}

	@Override
	public List<SeccionDTO> getListarSeccionesVigentesAutosUsables() {
		return listadoService.getListarSeccionesVigentesAutosUsables();
	}

	@Override
	public Map<Long, String> getMapPromotorias() {
		return listadoService.getMapPromotorias();
	}

	@Override
	public List<GenericaAgentesView> listaLineaNegocio(String arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub
		LOG.info(">> listaLineaNegocio() arg0="+arg0 + "arg1="+arg1+ "arg2="+arg2);
		return listadoService.listaLineaNegocio(arg0, arg1, arg2);
	}

	@Override
	public List<GenericaAgentesView> listaProducto(String arg0, String arg1) {
		// TODO Auto-generated method stub
		LOG.info(">> listaProducto(2)  arg0="+arg0 + "arg1="+arg1);
		return listadoService.listaProducto(arg0, arg1);
	}

	@Override
	public List<GenericaAgentesView> listaRamo(String arg0, String arg1) {
		// TODO Auto-generated method stub
		LOG.info(">> listaRamo() arg0="+arg0 + "arg1="+arg1);
		return listadoService.listaRamo(arg0, arg1);
	}

	@Override
	public List<GenericaAgentesView> listaSubramo(String arg0, String arg1) {
		// TODO Auto-generated method stub
		LOG.info(">> listaSubramo() arg0="+arg0 + "arg1="+arg1);
		return listadoService.listaSubramo(arg0, arg1);
	}

	@Override
	public List<GenericaAgentesView> listaCoberturas(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return listadoService.listaCoberturas(arg0, arg1);
	}

	@Override
	public int obtieneNumeroDeRiesgosBitemporal(Long incisoContinuityId, Date validoEn, Short tipoEndoso) {
		return listadoService.obtieneNumeroDeRiesgosBitemporal(incisoContinuityId, validoEn, tipoEndoso);
	}
	
	@Override
	public boolean mostrarDatosConductor(Long incisoContinuityId, Date validoEn) {
		return listadoService.mostrarDatosConductor(incisoContinuityId, validoEn);
	}

	@Override
	public List<AgenteView> listaAgentesPorDescripcionLightWeightAutorizados(
			String descripcion) {
		return listadoService.listaAgentesPorDescripcionLightWeightAutorizados(descripcion);
	}
	
	@Override
	public Map<Long,String> getMapEjecutivosResponsable(){
		return listadoService.getMapEjecutivosResponsable();
	}
	
	
	@Override
	public boolean validaDatosRiesgosCompletosCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		return listadoService.validaDatosRiesgosCompletosCotizacion(idToCotizacion, numeroInciso);
	}

	@Override
	public boolean validaDatosRiesgoCompletos(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, Short nivelConfiguracionRiesgos) {
		return listadoService.validaDatosRiesgoCompletos(idToCotizacion, numeroInciso, nivelConfiguracionRiesgos);
	}

	@Override
	public boolean requiereDatosRiesgoInciso(BigDecimal idToCotizacion,
			BigDecimal numeroInciso, Short nivelConfiguracionRiesgos) {
		return listadoService.requiereDatosRiesgoInciso(idToCotizacion, numeroInciso, nivelConfiguracionRiesgos);
	}

	@Override
	public boolean requiereDatosRiesgoIncisoCotizacion(
			BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		return listadoService.requiereDatosRiesgoIncisoCotizacion(idToCotizacion, numeroInciso);
	}
	
	@Override
	public Map<Integer, String> listadoMotivoEndoso(Long tipoEndoso, Long polizaId) {
		return listadoService.listadoMotivoEndoso(tipoEndoso, polizaId);
	}

	@Override
	public boolean validaDatosComplementariosIncisoBorrador(Long polizaId, Long incisoContinuityId, Short claveTipoEndoso, Date validoEn){
		return listadoService.validaDatosComplementariosIncisoBorrador(polizaId, incisoContinuityId, claveTipoEndoso, validoEn);
	}
	/**
	 * Obtiene el listado de elementos de un catalogo de ValorCatalogoAgentes
	 * @param catalogo
	 * @return
	 */
	@Override
	public List<ValorCatalogoAgentes> getCatalogoAgentes(String catalogo){
		return listadoService.getCatalogoAgentes(catalogo);
	}
	/**
	 * Obtiene el id del elemento catalogo
	 * @param catalogo
	 * @param valor
	 * @return
	 */
	@Override
	public Long getIdElementoValorCatalogoAgentes(String catalogo,String valor){
		return listadoService.getIdElementoValorCatalogoAgentes(catalogo,valor);
	}

	@Override
	public Map<Long, String> getMapMovimientosPorProceso(BigDecimal idProceso) {		
		return listadoService.getMapMovimientosPorProceso(idProceso);
	}

	@Override
	public List<Gerencia> listarGerenciasPorCentroOperacion(String id) {
		return listadoService.listarGerenciasPorCentroOperacion(id);
	}

	@Override
	public List<Ejecutivo> listarEjecutivoPorGerencia(String id) {
		return listadoService.listarEjecutivoPorGerencia(id);
	}

	@Override
	public List<Promotoria> listarPromotoriaPorEjecutivo(String id) {
		return listadoService.listarPromotoriaPorEjecutivo(id); 
	}

	@Override
	public List<ValorCatalogoAgentes> listarTipoPromotoriaByPromotoria(String id) {
		return listadoService.listarTipoPromotoriaByPromotoria(id);
	}

	@Override
	public List<GenericaAgentesView> listaProducto(String listaId) {
		LOG.info(">> listaProducto(1) listaId="+listaId);
		return listadoService.listaProducto(listaId);
	}
	@Override
	public List<GenericaAgentesView> listaProductoSeycos(String arg0, String arg1) {
		LOG.info(">> listaProductoSeycos(1) listaId="+arg0 + arg1);
		return listadoService.listaProductoSeycos(arg0, arg1);
	}
	@Override
	public List<GenericaAgentesView> listaSubramoSeycos(String arg0, String arg1) {
		// TODO Auto-generated method stub
		LOG.info(">> listaSubramo() arg0="+arg0 + "arg1="+arg1);
		return listadoService.listaSubramoSeycos(arg0, arg1);
	}
	@Override
	public List<GenericaAgentesView> listaLineaNegocioSeycos(String arg0, String arg1, String arg2) {
		// TODO Auto-generated method stub
		LOG.info(">> listaLineaNegocio() arg0="+arg0 + "arg1="+arg1+ "arg2="+arg2);
		return listadoService.listaLineaNegocioSeycos(arg0, arg1, arg2);
	}
	@Override
	public List<GenericaAgentesView> listaRamoSeycos(String arg0, String arg1) {
		// TODO Auto-generated method stub
		LOG.info(">> listaRamoSeycos() arg0="+arg0 + "arg1="+arg1);
		return listadoService.listaRamoSeycos(arg0, arg1);
	}
	
	@Override
	public List<GenericaAgentesView> listaGerenciaSeycos() {
		LOG.info(">> listaGerenciaSeycos(1");
		return listadoService.listaGerenciaSeycos();
	}
	
	@Override
	public List<DatosSeycos> listarAgenteSeycos(String agente, String idGerencias) {
		LOG.info(">> listarAgenteSeycos(1,2)");
		return listadoService.listarAgenteSeycos(agente, idGerencias);
	}
	
	@Override
	public List<GenericaAgentesView> listaRamo(String listaId) {
		LOG.info(">> listaRamo()" + listaId);
		return listadoService.listaRamo(listaId);
	}

	@Override
	public List<GenericaAgentesView> listaSubramo(String listaId) {
		LOG.info(">> listaSubramo() listaid="+listaId );
		 return listadoService.listaSubramo(listaId);
	}

	@Override
	public List<GenericaAgentesView> listaLineaNegocio(String listaId,String listaId2) {
		LOG.info(">> listaLineaNegocio() listaId="+listaId + "listaId2="+listaId2);
		return listadoService.listaLineaNegocio(listaId,listaId2);
	}

	@Override
	public List<GenericaAgentesView> listaCoberturas(String listaId) {
		return listadoService.listaCoberturas(listaId);
	}

	@Override
	public Map<Long, String> mapValorCatalogoAgente(String nombreCatalogo) {
		return listadoService.mapValorCatalogoAgente(nombreCatalogo);
	}

	@Override
	public Map<Integer, String> getMapMonths() {
		return listadoService.getMapMonths();
	}

	@Override
	public Map<Integer, Integer> getMapYears(int range) {
		return listadoService.getMapYears(range);
	}

	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcionClaveTipoBien(String arg0, String arg1) {
		return listadoService.listarEstilosPorDescripcionClaveTipoBien(arg0, arg1);
	}

	@Override
	public Map<Integer, String> getMapFormasdePagoByNegTipoPoliza(
			BigDecimal arg0) {
		return listadoService.getMapFormasdePagoByNegTipoPoliza(arg0);
	}

	@Override
	public Integer getPrimeraFormasdePagoByNegTipoPoliza(Long idToNegProducto) {
		return listadoService.getPrimeraFormasdePagoByNegTipoPoliza(idToNegProducto);
	}

	@Override
	public Map<Long, Double> getMapDerechosPolizaByNegocio(BigDecimal arg0) {
		return listadoService.getMapDerechosPolizaByNegocio(arg0);
	}
	
	@Override
	public Map<Long, Double> getMapDerechosPolizaByNegocio(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		return listadoService.getMapDerechosPolizaByNegocio(idToCotizacion, numeroInciso);
	}
	
	@Override
	public Map<Long, Double> getMapDerechosPolizaByConfiguracion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String estado, String municipio, Long idTipoUso){
		return listadoService.getMapDerechosPolizaByConfiguracion( idNegocio,  idTipoPoliza,  idSeccion, 
			 idPaquete,  idMoneda,   estado,  municipio,  idTipoUso);
	}

	@Override
	public boolean mostrarPagoFraccionado(Long arg0) {
		return listadoService.mostrarPagoFraccionado(arg0);
	}

	@Override
	public Map<Short, Short> getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(
			BigDecimal arg0, BigDecimal arg1, BigDecimal arg2) {
		return listadoService.getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(arg0,arg1,arg2);
	}

	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMoneda(
			BigDecimal arg0, Short arg1, BigDecimal arg2, BigDecimal arg3,
			Long arg4) {
		return listadoService.getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMoneda(arg0,arg1,arg2,arg3,arg4);
	}

	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAgente(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcion, BigDecimal idAgrupadorPasajeros){
		return listadoService.getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAgente(
				idTcMarcaVehiculo, modeloVehiculo, idToNegSeccion, idMoneda, 
				autoIncisoContinuityId, descripcion, idAgrupadorPasajeros);
	}

	@Override
	public List<EstiloVehiculoDTO> getListarEstilo(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo,
			BigDecimal idToNegSeccion, BigDecimal idMoneda,
			Long autoIncisoContinuityId, String descripcion) {
		return listadoService.getListarEstilo(idTcMarcaVehiculo,
				modeloVehiculo, idToNegSeccion, idMoneda,
				autoIncisoContinuityId, descripcion);
	}

	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String arg0,
			String arg1, BigDecimal arg2, BigDecimal arg3, Short arg4,
			BigDecimal arg5) {
		return listadoService.listarEstilosPorDescripcion(arg0, arg1, arg2, arg3, arg4, arg5);
	}

	@Override
	public Long getDerechoPolizaDefault(BigDecimal arg0) {
		return listadoService.getDerechoPolizaDefault(arg0);
	}
	
	@Override
	public Long getDerechoPolizaDefault(BigDecimal idToCotizacion, BigDecimal numeroInciso){
		return listadoService.getDerechoPolizaDefault(idToCotizacion, numeroInciso);
	}
	
	@Override
	public Long getDerechoPolizaDefaultByConfiguracion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String estado, String municipio, Long idTipoUso){
		return listadoService.getDerechoPolizaDefaultByConfiguracion(
				idNegocio,  idTipoPoliza,  idSeccion,	 idPaquete,  idMoneda,   estado,  municipio,  idTipoUso);
	}

	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo, boolean orderByDescription) {
		return listadoService.obtenerCatalogoValorFijo(tipo, orderByDescription);
	}

	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo) {
		return listadoService.obtenerCatalogoValorFijo(tipo);
	}
	
	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo, String codigoPadreId){
		return listadoService.obtenerCatalogoValorFijo(tipo, codigoPadreId);
	}

	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo, String codigoPadreId, boolean orderByDescription) {
		return listadoService.obtenerCatalogoValorFijo(tipo, codigoPadreId, orderByDescription);
	}

	@Override
	public Map<Long, String> obtenerOficinasSiniestros() {
		return listadoService.obtenerOficinasSiniestros();
	}
	
	@Override
	public Map<Long, String> obtenerOficinasSiniestrosSinFiltro(){
		return listadoService.obtenerOficinasSiniestrosSinFiltro();
	}

	@Override
	public String getCodigoPostalPorColoniaMidas(String idColonia) {
		return listadoService.getCodigoPostalPorColoniaMidas(idColonia);
	}

	@Override
	public Map<String, String> getMapColoniasPorCiudadMidas(String idCiudad) {
		return listadoService.getMapColoniasPorCiudadMidas(idCiudad);
	}

	@Override
	public Map<String, String> getMapEstadosPorPaisMidas(String idPais) {
		return listadoService.getMapEstadosPorPaisMidas(idPais);
	}

	@Override
	public Map<String, String> getMapMunicipiosPorEstadoMidas(String idEstado) {
		return listadoService.getMapMunicipiosPorEstadoMidas(idEstado);
	}

	@Override
	public Map<String, String> getMapPaisesMidas() {
		return listadoService.getMapPaisesMidas();
	}
	
	@Override
	public List<ColoniaMidas> getColoniasPorCPMidas(String cp) {
		return listadoService.getColoniasPorCPMidas(cp);
	}
	
	@Override
	public Map<String, String> getMapColoniasPorCPMidas(String cp) {
		return listadoService.getMapColoniasPorCPMidas(cp);
	}
	
	@Override
	public Map<String, String> getMapCiudadesPorCPMidas(String cp){
		return listadoService.getMapCiudadesPorCPMidas(cp);
	}
	
	@Override
	public String getEstadoPorIdColoniaMidas(String idColonia){
		return listadoService.getEstadoPorIdColoniaMidas(idColonia);
	}
	
	@Override
	public String getEstadoPorCPMidas(String cp){
		return listadoService.getEstadoPorCPMidas(cp);
	}

	@Override
	public String getCiudadPorCPMidas(String cp){
		return listadoService.getCiudadPorCPMidas(cp);
	}

	
	public Map<String, String> getMapTipoPrestador(){
		return listadoService.getMapTipoPrestador();
	}
	
	public Map<Long, String> getMapCiaDeSeguros(){
		return listadoService.getMapCiaDeSeguros();
	} 

    @Override
	public boolean getAplicaDescuentoNegocioPaqueteSeccion(Long idToNegPaqueteSeccion) {
		return listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion);
	}
	
	@Override
	public Double getPcteDescuentoDefaultPorEstado(BigDecimal idToCotizacion, String stateId) {
		return listadoService.getPcteDescuentoDefaultPorEstado(idToCotizacion, stateId);
	}
	
	@Override
	public Double getPcteDescuentoMaximoPorEstado(BigDecimal idToCotizacion, Long idToNegocio, String stateId) {
		return listadoService.getPcteDescuentoMaximoPorEstado(idToCotizacion, idToNegocio, stateId);
	}
	
	@Override
	public Map<String, String> getMapMovimientosNotificaciones(String idProceso){
		return listadoService.getMapMovimientosNotificaciones(idProceso);
	}

	@Override
	public Double getPcteDescuentoDefaultPorNegocio(Long idToNegocio, String stateId) {
		
		return listadoService.getPcteDescuentoDefaultPorNegocio(idToNegocio, stateId);
	}
	
	@Override
	public Map<String, String> obtenerCatalogoValorFijoByStr(String tipo,
			String codigoPadreId) {
		return listadoService.obtenerCatalogoValorFijoByStr(tipo, codigoPadreId);		
	}
	
	@Override
    public Map<String, String> obtenerTerminosAjuste(
                String codigoTipoSiniestro, String codigoResponsabilidad) {
          return listadoService.obtenerTerminosAjuste(codigoTipoSiniestro, codigoResponsabilidad);
    }

	@Override
	public Map<String, String> obtenerAjustadoresValuacionCrucero() {
		return listadoService.obtenerAjustadoresValuacionCrucero();
	}

	@Override
	public Map<String, String> obtenerPiezasPorSeccionAutomovil(String codigoSeccion) {
		return listadoService.obtenerPiezasPorSeccionAutomovil(codigoSeccion);
	}

	@Override
	public Map<String, String> obtenerValuadores() {
		return listadoService.obtenerValuadores();
	}

	@Override
    public Map<String, String> obtenerCoberturaPorSeccion( Long idToSeccion ) {
          return listadoService.obtenerCoberturaPorSeccion( idToSeccion );
    }
	@Override
	public Map<Long, String> getMapAjustadoresPorOficina(Oficina oficina,
			Boolean soloActivos) {
		return listadoService.getMapAjustadoresPorOficina(oficina, soloActivos);
	}
	
	@Override
	public Map<String, String> getMapAjustadoresPorOficinaId(Long oficinaId) {
		return listadoService.getMapAjustadoresPorOficinaId(oficinaId);
	}

	@Override
	public Map<Long, String> getMapHorariosLaborales(Boolean soloActivos) {
		return listadoService.getMapHorariosLaborales(soloActivos);
	}

	@Override
	public Map<String, String> obtenerTiposPaseAtencion() {
		return obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PASE_ATENCION);
	}

	@Override
	public Map<Long, String> listarConceptosPorCobertura(
			Long idCoberturaCabina,String cveSubTipoCalculoCobertura, short categoria,String tipoConcepto) {
		return listadoService.listarConceptosPorCobertura(idCoberturaCabina, cveSubTipoCalculoCobertura,categoria,tipoConcepto);
	}

	@Override
	public Map<String, String> getCoberturasOrdenCompra(
			Long idReporteCabina, String tipo) {
		return listadoService.getCoberturasOrdenCompra(idReporteCabina, tipo);
	}
	
	@Override
	public Map<String, String> getCoberturasReporte(Long reporteCabinaId, String tipoSiniestro, 
			String terminoAjuste,String tipoResponsabilidad, Boolean soloAfectadas, Boolean soloAfectables) {
		return listadoService.getCoberturasReporte(reporteCabinaId,  tipoSiniestro, 
		 terminoAjuste, tipoResponsabilidad,  soloAfectadas,  soloAfectables);
	}
 
	@Override
	public Map<Long, String> getMapPrestadorPorTipo(String tipo) {
		return listadoService.getMapPrestadorPorTipo(tipo);
	}

	@Override
	public Map<Long, String> getListasTercerosAfectadorPorCobertura(
			Long coberturaReporteCabinaId,String cveSubTipoCalculo) {
		return listadoService.getListasTercerosAfectadorPorCobertura(coberturaReporteCabinaId,cveSubTipoCalculo);

	}

	@Override
	public Map<Long, String> obtenerMapSecciones() {
		return listadoService.obtenerMapSecciones();
	}

	@Override
	public Map<Long, String> listarConceptosPorCoberturaSeccion(Long idCoberturaSeccion, Long idSeccion, String cveSubTipoCalculoCobertura,short tipoConcepto) {
		return listadoService.listarConceptosPorCoberturaSeccion(idCoberturaSeccion,  idSeccion, cveSubTipoCalculoCobertura, tipoConcepto);
	}

	@Override
	public Map<Long, String> obtenerMapConceptosGastoAjuste() {
		return listadoService.obtenerMapConceptosGastoAjuste();
	}

	@Override
	public Map<Long, String> getMapConceptosAjuste() {
		return listadoService.getMapConceptosAjuste();
	}
	@Override
	public Map<String, String> obtenerCoberturaPorSeccionConDescripcion( Long idToSeccion ){
		return listadoService.obtenerCoberturaPorSeccionConDescripcion(idToSeccion);
	}

	@Override
	public Map<Long, String> listarConceptosPorCoberturaOrdenCompra(
			Long idCoberturaCabina, String cveSubTipoCalculoCobertura,
			short categoria, String tipoConcepto, boolean aplicaPagoDaños,Long idTipoPrestador) {
		return listadoService.listarConceptosPorCoberturaOrdenCompra(idCoberturaCabina, cveSubTipoCalculoCobertura, categoria, tipoConcepto, aplicaPagoDaños,idTipoPrestador);
	}

	@Override
	public Map<String, String> getMapNegProductoPorNegocioString(
			Long idToNegocio) {
		return listadoService.getMapNegProductoPorNegocioString(
				idToNegocio);
	}

	@Override
	public Map<String, String> getMapFormasdePagoByNegTipoPolizaString(
			BigDecimal idToNegTipoPoliza) {
		return listadoService.getMapFormasdePagoByNegTipoPolizaString(
				idToNegTipoPoliza);
	}
	
	@Deprecated
	@Override
	public Map<String, String> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad){
		return listadoService.obtenerTipoDePasePorTipoEstimacion(codigoTipoEstimacion,codigoTipoResponsabilidad);
	}

	@Override
	public Map<String, String> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad , String codigoCausaSiniestro, String codigoTerminoAjuste){
		return listadoService.obtenerTipoDePasePorTipoEstimacion(codigoTipoEstimacion,codigoTipoResponsabilidad, codigoCausaSiniestro, codigoTerminoAjuste);
	}

	@Override
	public Map<BigDecimal, String> getMapVigencias() {
		return listadoService.getMapVigencias();
	}
	
	@Override
	public Map<BigDecimal, String> getMapProgramaPagos(BigDecimal arg0, BigDecimal arg1) {
		return listadoService.getMapProgramaPagos(arg0, arg1);
	}
	
	@Override
	public String getIncisosProgPago(BigDecimal idToPoliza, BigDecimal idProgPago) {
		return listadoService.getIncisosProgPago(idToPoliza, idProgPago);
	}

	@Override
	public Map<Long, String> getMapOficinaJuridico() {
		return listadoService.getMapOficinaJuridico();

	}
	
	@Override
	public Map<String, String> obtenerUsuariosJuridico()
	{
		return listadoService.obtenerUsuariosJuridico();	
	}
	
	

	@Override
	public Map<Long, String> obtenerJuridicoDelegaciones() {
		 return listadoService.obtenerJuridicoDelegaciones();
	}

	@Override
	public Map<Long, String> obtenerJuridicoMotivos() {
		return listadoService.obtenerJuridicoMotivos();
	}

	@Override
	public Map<Long, String> obtenerJuridicoProcedimientos() {
		return listadoService.obtenerJuridicoProcedimientos();
	}
	
	@Override
	public Map<Long, String> obtenerTipoPrestadorGastoAjuste() {
		return listadoService.obtenerTipoPrestadorGastoAjuste();
	}

	@Override
	public Map<String, String> getMapTipoPrestadorAfectacionReserva() {
		return listadoService.getMapTipoPrestadorAfectacionReserva();
	}

	@Override
	public Map<Long, String> getMapEjecutivosPorGerencia(Long idGerencia, Long idProvision) {
		return listadoService.getMapEjecutivosPorGerencia(idGerencia, idProvision);
	}

	@Override
	public Map<Long, String> getMapLineaNegocioPorProducto(Long idProducto, Long idProvision) {
		return listadoService.getMapLineaNegocioPorProducto(idProducto, idProvision);
	}

	@Override
	public Map<Long, String> getMapProductoPorLineaVenta(Long idLineaVenta, Long idProvision) {
		return listadoService.getMapProductoPorLineaVenta(idLineaVenta, idProvision);
	}

	@Override
	public Map<Long, String> getMapPromotoriasPorEjecutivo(Long idEjecutivo, Long idProvision) {
		return listadoService.getMapPromotoriasPorEjecutivo(idEjecutivo, idProvision);
	}

	@Override
	public Map<Long, String> obtenerMapConceptosReembolsoGastoAjuste() {
		return listadoService.obtenerMapConceptosReembolsoGastoAjuste();
	}
	
	@Override
	public Map<Integer, Integer> getMapYears(int year, int range) {
		return listadoService.getMapYears(year, range);
	}

	@Override
	public Map<String, String> obtenerCatalogoPorCodigo(TIPO_CATALOGO tipo,
			String codigo) {
		return listadoService.obtenerCatalogoPorCodigo(tipo, codigo);
	}

	@Override
	public Map<Long, String> obtenerAbogados() {
		return listadoService.obtenerAbogados();
	}

	@Override
	public Map<Long, String> getMapBancosMidas() {
		return listadoService.getMapBancosMidas();
	}

	@Override
	public Map<Long, String> getMapCuentasBancoMidas(Long bancoId) {
		return listadoService.getMapCuentasBancoMidas(bancoId);
	}

	@Override
	public Map<Long, String> getMapCuentasAcreedorasSiniestros() {		
		return listadoService.getMapCuentasAcreedorasSiniestros();
	}
	
	@Override
	public Map<Long, String> getMapCuentasManualesSiniestros(){
		return listadoService.getMapCuentasManualesSiniestros();
	}

	@Override
	public Map<Long, String> getCtgBancosValidosSalvamentos() {
		return this.getCtgBancosValidosSalvamentos();
	}

	@Override
	public Map<String, String> getMediosPago(Negocio negocio){
		return listadoService.getMediosPago(negocio);
	}

	@Override
	public Map<String, String> getMediosPagoSO(Negocio negocio){
		return listadoService.getMediosPago(negocio);
	}

	@Override
	public Map<String, String> getBancos(){
		return listadoService.getBancos();
	}
	
	@Override
	public List<BancoEmisorDTO> getBancosList(){
		return listadoService.getBancosList();
	}
	
	@Override
	public Map<String, Object> getSpvData(Long idToNegocio){
		return listadoService.getSpvData(idToNegocio);
	}

	@Override
	public BigDecimal getIdToFormaDePagoByNegocioValid(BigDecimal idToNegTipoPoliza, BigDecimal idToNegTipoPolizaCom){
		return listadoService.getIdToFormaDePagoByNegocioValid(idToNegTipoPoliza, idToNegTipoPolizaCom);
	}
	
	@Override
	public Map<Long, String> getMapCaTipoMoneda(){
		return listadoService.getMapCaTipoMoneda();
	}
	
	@Override
	public  String getDescripionValorFijo(Integer claveTipoDeducible, int key){
		return listadoService.getDescripionValorFijo(claveTipoDeducible, key);
	}
	
	@Override
	public  Map<String, String> listarNegocioSeccionPorProductoNegocioTipoPoliza(
			BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza){
		return listadoService.listarNegocioSeccionPorProductoNegocioTipoPoliza(
				idToProducto, idToNegocio, idToTipoPoliza);
	}
	
	@Override
	public Map<BigDecimal, String> getMapTipoServicioVehiculo(BigDecimal idToNegSeccion){
		return listadoService.getMapTipoServicioVehiculo(idToNegSeccion);
	}

	@Override
	public Map<Long, String> listarConceptosPorCobertura(
			Long idCoberturaCabina, String cveSubTipoCalculoCobertura,
			short categoria, String tipoConcepto, boolean esValidoHGS) {
		return  listadoService.listarConceptosPorCobertura(idCoberturaCabina, cveSubTipoCalculoCobertura, categoria, tipoConcepto, esValidoHGS);
	}
	
	public Map<Long,Double> listaImportesDeDerechosPorNegocio(Long idToNegocio, String tipoDerecho){
		return listadoService.listaImportesDeDerechosPorNegocio(idToNegocio,tipoDerecho);
	}

	public boolean imprimirLeyendaUMA(Date fechaInicioVigencia){
		return listadoService.imprimirLeyendaUMA(fechaInicioVigencia);
	}
	
	public String ObtieneMerchantPortalPagosWeb (){
		return listadoService.ObtieneMerchantPortalPagosWeb();
	}
	
	public String ObtieneUrlBackPortalPagosWeb (){
		return listadoService.ObtieneUrlBackPortalPagosWeb();
	}
	@Override
	public Map<Long, Integer> obtenerListadoVersionesRecuotificacionNegocio(
			Long idToNegocio) {
		return listadoService.obtenerListadoVersionesRecuotificacionNegocio(idToNegocio);
	}

	@Override
	public CiudadDTO getMunicipioPorCp(String cp) {
		return listadoService.getMunicipioPorCp(cp);
	}

	/**
	 * Obtener el listado de Tipos de vigencia asignados a un negocio
	 * @param idToNegocio
	 * @return
	 */
	@Override
	public Map<Integer, String> obtenerTiposVigenciaNegocio(Long idToNegocio) {
		return listadoService.obtenerTiposVigenciaNegocio(idToNegocio);
	}
	
	/**
	 * Obtener vigencia default
	 * @param idToNegocio
	 * @return
	 */
	@Override
	public Integer obtenerVigenciaDefault(Long idToNegocio){
		return listadoService.obtenerVigenciaDefault(idToNegocio);
	}
}