package mx.com.afirme.midas2.service.delegate;

import mx.com.afirme.midas2.util.ValidadorJs;

import org.directwebremoting.annotations.RemoteProxy;

@RemoteProxy(name="validadorJs")
public class ValidadorJsServiceDelegate implements ValidadorJs{
	
	ValidadorJs validadorJs;
	
	@Override
	public String generarValidacionJs(String id, String dependenciaId, 
			Integer tipoValidador, Integer tipoDependencia) {
		return validadorJs.generarValidacionJs(id, dependenciaId, tipoValidador, tipoDependencia);
	}
	
	@Override
	public String generaTipoDependenciaJs(String id, String dependenciaId, Integer tipoDependencia, boolean prepareForEval) {
		return validadorJs.generaTipoDependenciaJs(id, dependenciaId, tipoDependencia, prepareForEval);
	}

}
